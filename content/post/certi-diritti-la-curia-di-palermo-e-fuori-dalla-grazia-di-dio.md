---
title: 'Certi Diritti: la curia di Palermo è fuori dalla grazia di dio'
date: Sat, 07 May 2011 11:10:43 +0000
draft: false
tags: [Politica]
---

A Palermo i Radicali saranno vicini alla comunità lbgt in preghiera fuori dalla chiesa perchè cacciati.  
  
Comunicato Stampa Associazione Radicale Certi Diritti  
  
Palermo, 7 maggio 2011

“Fuori dalla grazia di Dio”.Questo il commento di Sergio Rovasio,segretario dell’Associazione radicale Certi Diritti, in relazione al NO della Curia di Palermo alla veglia di preghiera per le vittime dell’omofobia organizzata per il 12 maggio- e in vista del Pride-dal gruppo delle lesbiche e dei gay cristiani“Ali d’aquila”.  
“Il divieto-prosegue Rovasio-frutto del cieco pregiudizio clericale   delle gerarchie ecclesiastiche nei confronti degli omosessuali, contribuisce ad alimentare il clima omofobico   nei confronti di gay,lesbiche,trans  considerati figli di un dio minore da emarginare, isolare, ghettizzare perfino nel  momento di una  preghiera religiosa “.  
  
Anche per stigmatizzare la reazione della Curia palermitana,i Radicali di Palermo dell’Associazione radicale David Kato saranno vicini agli amici di Ali d’aquila nel corso della veglia ecumenica di preghiera per le vittime dell'omofobia che si svolgerà  in  Piazza Pace (Porto/Ucciardone ) antistante alla Parrocchia S. Lucia, giovedì 12 maggio, alle ore 20,45.  
  
Sergio Rovasio  
Associazione radicale Certi Diritti  
  
Donatella Corleo  
Associazione radicale David Kato- Radicali Palermo  
tel.334 3752066