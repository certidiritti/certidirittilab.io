---
title: 'Froci schifosi andatevene a casa!'
date: Tue, 17 Jan 2012 20:50:27 +0000
draft: false
tags: [Politica]
---

di Claudio Barazzetta, membro del Comitato Nazionale di Radicali Italiani e della Giunta di Segreteria dell’Associazione Enzo Tortora Radicali Milano

Milano, 16 gennaio 2011

“Froci schifosi andatevene a casa!”  
E’ quanto mi sono sentito urlare da un piccolo gruppo di forse ventenni, uomini e donne, la sera di domenica 15 c.m. in via Marghera a Milano, mentre salutavo, in auto, un mio amico con un bacio più o meno appassionato.

Per mia volontà di evitare reazioni del gruppo e possibili conseguenze per il mio accompagnatore, non ho risposto alla provocazione, ridendoci sopra con una battuta “ok, da me o da te?”

E’ inaccettabile che nella città che si definisce la più moderna ed europea delle città italiane, possano accadere simili episodi, in cui qualcuno si sente in diritto-dovere di insultare pesantemente un individuo, ancora e solamente per il proprio orientamento sessuale. Questo atteggiamento denota una totale mancanza di rispetto nei confronti del prossimo, una palese violazione alla libertà di ognuno e una mancanza di quella cultura di tolleranza e civiltà che dovrebbe caratterizzare la società tutta - un sano vivi e lascia vivere - aggravato dal fatto che il legislatore, sia nazionale che locale, continua a non prendere nessuna iniziativa per combattere l’omofobia e le discriminazioni nei confronti di larga parte di cittadini.

Fa specie inoltre che tale episodio sia accaduto il giorno dopo la dichiarazione televisiva del sindaco Giuliano Pisapia, in cui manifestava la volontà, già chiaramente espressa nel programma elettorale, di istituire il registro delle unioni civili per la città di Milano entro l’anno.

Si avverte ancora più forte la necessità che le istituzioni intervengano urgentemente per creare una cultura civile della tolleranza e adottino quei provvedimenti volti a creare tale cultura: in quanto membro del Comitato Nazionale di Radicali Italiani, da sempre impegnati nelle battaglie su questo fronte, con le forze dell’Associazione Enzo Tortora – Radicali Milano e dell’Associazione Radicale Certi Diritti, faremo ancor più pressione affinché simili episodi non si abbiano più a ripetere, spingendo per l’adozione di tutti quei provvedimenti necessari a combattere tali gravi discriminazioni, anche solo verbali.

Monitoreremo e faremo pressione con ogni mezzo sull’operato del Sindaco e dell’Amministrazione Comunale in tal senso.