---
title: 'Certi Diritti: Apple censura la parola ''omosessuali'' su iTunes'
date: Wed, 25 May 2011 09:57:56 +0000
draft: false
tags: [Politica]
---

**A pochi giorni dal lancio della prima Applicazione lgbt per iPhone e iPad abbiamo scoperto che su iTunes la parola “omosessuale” è censurata. Avviato un dialogo per l'eliminazione di questa odiosa censura.**

Roma, 26 maggio 2011

Comunicato Stampa dell'Associazione Radicale Certi Diritti

L'Associazione Radicale Certi Diritti è vittima di “omofobia tecnologica” per mano di una delle aziende più friendly?  
A pochi giorni dal lancio della App per IPhone di Certi Diritti, abbiamo ricevuto numerose segnalazioni da parte degli utenti Apple: su iTunes risulta che la parola “omosessuale” è censurata! Infatti, la frase “diritti civili delle persone omosessuali e transessuali” è diventata “diritti civili delle persone o*********i e transessuali”.

L'Associazione Radicale Certi Diritti si scusa con tutti coloro che sono incappati in questo episodio dopo che hanno  scaricato la nostra App dal loro IPhone o IPad, ancor più se si pensa che la App è stata lanciata praticamente in occasione della Giornata Internazionale contro l’Omofobia.  
Vedere la scritta omosessuali censurata in questo modo, soprattutto in un contesto positivo come quello dell'informazione legata ai diritti civili, è a dir poco un episodio increscioso.

Abbiamo subito chiesto alla Apple, famosa in tutto il mondo per essere gay friendly, di rimuovere la censura e di aiutarci a spiegare ai nostri utenti come sia potuta a accadere una cosa simile.  
Inoltre, abbiamo invitato il responsabile marketing di ITunes Italia a partecipare agli eventi Business dell’Europride, così che le politiche di Marketing, possano acquisire una maggiore consapevolezza riguardo le tematiche LGBT, per affermare una maggiore coerenza nell’impatto sociale delle campagne di promozione dell'immagine aziendale.

Attendiamo una risposta da Apple Italia, specialmente in questi giorni in cui alcuni nostri politici, ispirati dall'odio del fondamentalismo religioso, stanno sdoganando pratiche illiberali e illecite di pressing mediatico e istituzionale al fine di intimorire le aziende gayfriendly.

L'App Certi Diritti è scaricabile gratuitamente.