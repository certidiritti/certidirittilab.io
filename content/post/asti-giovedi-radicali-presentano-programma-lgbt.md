---
title: 'Asti, giovedì Radicali presentano programma LGBT'
date: Tue, 11 Oct 2011 20:52:25 +0000
draft: false
tags: [Politica]
---

Conferenza stampa con Enzo Cucco e Salvatore Grizzanti.  
  
Asti, 10 ottobre 2011

Giovedì mattina alle ore 11:00 presso il bar Novecento di via Ospedale, 14 ad Asti si terrà una conferenza stampa di presentazione del documento redatto dall’Associazione radicale Adelaide Aglietta e dall’Associazione radicale Certi Diritti 'Asti LGBT, agenda 2012': si tratta di una serie di proposte alla prossima amministrazione comunale in materia di lotta all’omofobia e piena parità delle persone lesbiche, gay, bisessuali e transessuali come singoli e come famiglie.

Alla conferenza stampa interverranno:

**Enzo Cucco**  
Membro del direttivo di Certi Diritti, già assessore regionale alla programmazione sanitaria (giunta Brizio) e all’ambiente (giunta Bresso) – biografia a questo link: [http://enzocucco.blogspot.com/p/qualche-informazione-sulla-mia-vita.html](http://enzocucco.blogspot.com/p/qualche-informazione-sulla-mia-vita.html)

**Salvatore Grizzanti**  
Tesoriere dell’Associazione radicale Adelaide Aglietta e membro del Comitato nazionale di Radicali Italiani