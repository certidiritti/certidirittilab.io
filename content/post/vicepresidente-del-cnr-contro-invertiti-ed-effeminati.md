---
title: 'Vicepresidente del CNR contro "invertiti ed effeminati"'
date: Tue, 12 Apr 2011 10:41:34 +0000
draft: false
tags: [Politica]
---

Dopo aver definito lo Tsunami "una voce della bontà di Dio" il vicepresidente del Consiglio nazionale delle ricerche Roberto De Mattei esprime tesi allucinanti su 'invertiti', 'prostitute' e uomini 'effemminati' che  'non avranno accesso al regno di dio'. Interrogazione dei deputati radicali.  
  
Roma, 12 aprile 2011  
  
Comunicato Stampa dell'Associazione Radicale Certi Diritti  
  
I deputati Radicali Pd hanno oggi depositato una interrogazione urgente al Governo italiano su quanto dichiarato dal Vice Presidente del Cnr; come alcuni media hanno recentemente documentato, non contento di aver sostenuto a Radio Maria tesi antiscientifiche e ispirate ad una visione catto-integralista, sia riguardo l'evoluzionismo e sia  riguardo la tragedia umana dello Tsunami: "Le grandi catastrofi sono una voce terribile ma paterna della bontà di Dio", è anche intervenuto sulla stessa emittente esprimendo tesi allucinanti su 'invertiti', 'prostitute' e uomini 'effemminati' che  'non avranno accesso al regno di dio'. La sua nomina a Vice Presidente del Cnr è stata fatta nel 2006 dall'allora Minsitro della Pubblica Istruzione Letizia Moratti ed è stato poi riconfermato in questa carica nel 2008.  In base a che cosa? Quale curriculum? Quali particolari ricerche e studi scientifici?  
  
Qui di seguito il testo integrale della interrogazione urgente al Governo italiano, primo firmatario il deputato Maurizio Turco:  
  
  
Al Ministro della Pubblica Istruzione  
Al Presidente del Consiglio dei Minsitri  
  
Per sapere – premesso che:  
  
Secondo quanto riportato da diversi quotidiani, il 16 marzo 2011, nel corso della tramissione ‘Radici cristiane’ su Radio Maria, il vicepresidente del Consiglio Nazionale delle Ricerche (Cnr) Roberto De Mattei, per spiegare agli ascoltatori il senso escatologico dello tsunami in Giappone ha detto che “Le grandi catastrofi sono una voce terribile ma paterna della bontà di Dio” e “sono talora esigenza della sua giustizia della quale sono giusti castighi”;  
  
Nel 2009 il Cnr ha pubblicato un volume “Evoluzionismo. Il tramonto di un’ipotesi” (Cantagalli), costato 9.000 euro di finanziamento pubblico che raccoglieva gli atti di un seminario organizzato da Roberto De Mattei sulle teorie creazioniste; il vicepresidente del Cnr ritiene che tali teorie poggino su una solida base scientifica; a differenza invece di quanto sostengono gli evoluzionisti.  Nella difesa di queste sue teorie Roberto De Mattei aveva affermato di trovare “incredibilmente incoerente che ci si possa dichiarare cristiani ed evoluzionisti” e che “Adamo ed Eva sono personaggi storici e progenitori dell’ umanità”.  
  
In un’altra trasmissione di Radio Maria (http://www.youtube.com/watch?v=5ydJtaltai4) Roberto De Mattei ha letto e commentato alcuni testi di Salviano di Marsiglia con l’intento evidente di sostenere la tesi che, oggi come allora, sarebbero stati gli scandali di una società libertina ad aver provocato il fallimento della politica e delle istituzioni, perché alcuni “pochi invertiti” avrebbero “infettato” il tessuto sociale con la loro assenza di moralità, nelle sue dichiarazioni inoltre si dice, tra l’altro che “Cartagine, la capitale dell’Africa romana, contendeva ad Alessandria e ad Antiochia il primato della dissolutezza e godeva della reputazione di essere il paradiso degli omosessuali”. “A Cartagine quel vizio era una vera peste, anche se i travestiti non erano moltissimi. Succedeva, però, che l’effeminatezza di alcuni pochi contagiava la maggioranza. Si sa, per quanto pochi siano ad assumere atteggiamenti svergognati, sono molti a contagiarsi con le oscenità di quella minoranza. Un’unica prostituta fa fornicare molti uomini e lo stesso succede con l’abominevole presenza di pochi invertiti: infettano un bel po’ di gente. Gli uomini effeminati e gli omosessuali non avranno accesso al Regno di Dio”.  
  
A seguito di queste dichiarazioni è stato anche lanciato un appello che ha raccolto decine di migliaia di firme, nel quale si chiedono le dimissioni di Roberto De Mattei dalla carica di Vice Presidente del CNR nominato a tale carica dall’allora Ministro della pubblica istruzione Letizia Moratti nel 2004, poi ri-confermato nel 2008;  
  
Per sapere:  
  
-       Se il Ministro è a conoscenza di quanto dichiarato dal Vice Presidente del Cnr, così come richiamato in premessa;  
  
-       quali sono le ragioni scientifiche per le quali Roberto De Mattei è stato confermato Vice Presidente del Cnr nel 2008;  
  
-       se corrisponde al vero la notizia che per sostenere le sue tesi antiscientifiche riguardo l’origine dell’uomo nel 2009 il Cnr ha pubblicato un volume con le tesi antievoluzioniste di Roberto De Mattei, costato 9.000 Euro di denaro pubblico;  
  
-       se non ritenga il Governo che le tesi di Roberto De Mattei siano in netto contrasto vista la sua duplice veste di rappresentante autorevole di un ente pubblico di scienza, rispetto alle sue personali convinzioni religiose, e se questo non causi un grave danno alla comunità scientifica e per l’immagine dell’Italia anche all’estero;  
  
-       quale iniziativa urgente intende prendere il Governo affinché frasi sconvenienti, offensive e lesive della dignità di centinaia di migliaia di cittadini italiani espresse da Roberto De Mattei, non abbiano più a ripetersi, per lo meno nella veste di Vice Presidente del Cnr.  
  
Maurizio Turco  
Marco Beltrandi  
Rita Bernardini  
Maria Antonietta Farina Coscioni  
Matteo Mecacci  
Elisabetta Zamparutti