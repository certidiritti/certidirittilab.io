---
title: 'Il blog di Certi Diritti'
date: Mon, 16 Mar 2009 18:06:25 +0000
draft: false
tags: [Senza categoria]
---

Questo è il blog dei gruppi studenteschi Certi Diritti. Questo blog è a disposizione di tutti gli utenti che svolgono attività inerenti gli argomenti cari a Certi Diritti.

Per i bloggers che scrivono è importante che evidenzino la loro città e eventualmente la loro università affinché i lettori siano consapevoli del ruolo che svolge chi pubblica la notizia.

Tutti i visitatori possono commentare gli articoli che sono nei blog.

Per l'inserimento dei contenuti, consiglio di fare una breve introduzione accompagnata da un immagine e poi dopo aver fatto un interruzione di pagina con il "leggi tutto" situato in basso al text-box continuare l'articolo.

Grazie a tutti quanti per il vostro contributo,

Giovanni Bastianelli