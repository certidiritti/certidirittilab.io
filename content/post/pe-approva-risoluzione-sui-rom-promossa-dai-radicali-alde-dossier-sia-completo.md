---
title: 'PE APPROVA RISOLUZIONE SUI ROM PROMOSSA DAI RADICALI - ALDE. DOSSIER SIA COMPLETO'
date: Thu, 10 Jul 2008 13:28:33 +0000
draft: false
tags: [Bambini ROM, Comunicati stampa, Impronte Digitale, ROM, Schedatura]
---

Il Parlamento Europeo ha approvato oggi con 336 favorevoli, 220 contrari e 77 astenuti la risoluzione sui Rom a seguito della presentazione di una serie di interrogazioni dei deputati Cappato e Mohacsi. I gruppi del Partito popolare e dell'Europa delle Nazioni avevano chiesto di rinviare il voto a settembre ma la mozione è stata rigettata con 293 favorevoli e 316 contrari.  
[Continua...](index.php?option=com_content&task=view&id=147&Itemid=55)  
  
  
  
**Dichiarazione di Marco Cappato, eurodeputato radicale e Marco Perduca, senatore radicale eletto nel PD** e promotore dell'intergruppo parlamentare di amicizia coi popoli Rom:  
_“Le preoccupazioni destate dalla gravità della situazione italiana sono tali che per la prima volta la Commissione europea ha deciso di raccogliere informazioni da uno Stato membro dell'Unione su politiche nazionali in potenziale violazione degli obblighi in materia di diritti umani. Prendiamo atto che le risposte preliminari del Ministro Maroni non sono state sufficienti e salutiamo il suo impegno a presentare un rapporto entro la fine del mese di luglio, che non mancheremo di integrare nelle prossime settimane. Occorre tenere alta l'attenzione politica, ma anche popolare, specie in vista delle visite nei campi nomadi quando la Commissione libertà pubbliche del PE vi si recherà su invito del Ministro.”  
  
_  
**Dichiarazione di Marco Cappato, eurodeputato Radicale:**  
_"Nel giorno in cui il PE approva la risoluzione sui Rom, alla quale si é arrivati a seguito delle iniziative che abbiamo preso con la collega Mohacsi al PE, bisogna prendere atto del fatto che l'Italia é osservata speciale, in Europa e nel mondo. Il Consiglio d'Europa, il commissario ai diritti umani, la Commissione europea, il PE, l'UNICEF, tutti guardano l'Italia ed esaminano le dichiarazioni e proposte del governo, esponendo giudizi critici.  
Il governo Berlusconi ed il Ministro Maroni hanno oggi l'opportunità di fare quello che i governi precedenti non hanno mai fatto: lanciare una politica nazionale di accoglienza ed integrazione a favore dei Rom, anche col sostegno di fondi europei. Per fare questo non servono schedature etniche, religiose o impronte digitali, decreti e ordinanze che proclamano lo "stato d'emergenza", deroghe alle leggi ordinarie, supercommissari. Non servono neppure proposte che violano il diritto comunitario e i principi del diritto, come quelle contenute nel pacchetto sicurezza. Il governo deve fare il suo lavoro e governare i fenomeni sociali, nel pieno rispetto dello Stato di diritto e dei diritti umani di tutti"._  
  
Puoi leggere [il testo completo](index.php?option=com_docman&task=doc_download&gid=27&Itemid=79) della risoluzione e qui [per i voti](index.php?option=com_docman&task=doc_download&gid=28&Itemid=79) .