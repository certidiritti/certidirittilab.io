---
title: 'Certi Diritti condanna la strisciante omofobia delle mozioni presentate e approvate in alcuni Comuni del Nord Est'
date: Thu, 10 Apr 2014 12:39:56 +0000
draft: false
tags: [Politica]
---

Dopo lo sventato tentativo a Trento, una mozione delirante contro il matrimonio ugualitario e i diritti delle persone LGBTI è stata approvata al comune di Cassola, in provincia di Vicenza, ed è stata presentata al comune di Bassano del Grappa, sempre in provincia di Vicenza.

Il documento fa strage in un sol colpo dei principali trattati sui diritti umani, della costituzione italiana e di una vasta giurisprudenza nazionale e internazionale per cui esistono vari tipi di famiglie, tutte egualmente degne di protezione, e per cui anche le persone omosessuali e transessuali hanno diritto alla vita famigliare.

Tra tutte le citazioni possibili basta riferirsi alla sentenza della Corte Costituzionale n. 138/2010 per la quale «L’art. 2 Cost. dispone che la Repubblica riconosce e garantisce i diritti inviolabili dell’uomo, sia come singolo sia nelle formazioni sociali ove si svolge la sua personalità e richiede l’adempimento dei doveri inderogabili di solidarietà politica, economica e sociale. Orbene, per formazione sociale deve intendersi ogni forma di comunità, semplice o complessa, idonea a consentire e favorire il libero sviluppo della persona nella vita di relazione, nel contesto di una valorizzazione del modello pluralistico. In tale nozione è da annoverare anche l’unione omosessuale, intesa come stabile convivenza tra due persone dello stesso sesso, cui spetta il diritto fondamentale di vivere liberamente una condizione di coppia, ottenendone – nei tempi, nei modi e nei limiti stabiliti dalla legge – il riconoscimento giuridico con i connessi diritti e doveri».

Per il comune di Cassola invece la famiglia è solo ed esclusivamente quella fondata sul matrimonio tra un uomo e una donna, con buona pace di conviventi (etero o omosessuali), famiglie monogenitoriali o omogenitoriali.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: "La furia omofoba del documento arriva, come sempre accade, a voler travolgere i diritti di tutti auspicando l'esclusione di affidamento e adozione per conviventi e single, a prescindere dal loro orientamento sessuale. Questa strisciante omofobia che confonde persino orientamento sessuale e genere dimostrando un'abissale ignoranza va stroncata sul nascere da tutte le forze liberali di questo paese".

Comunicato stampa dell'Associazione Radicale Certi Diritti.

[IL TESTO DELLA MOZIONE APPROVATA](http://www.certidiritti.org/wp-content/uploads/2014/04/D.G.C.-n.-61.pdf)