---
title: '2008-0140-Discrimination-ST06608.EN10'
date: Wed, 17 Feb 2010 15:49:20 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 17 February 2010

Interinstitutional File:

2008/0140 (CNS)

6608/10

LIMITE

SOC 129

JAI 148

MI 56

  

  

  

  

  

NOTE

from :

The General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

6092/10 SOC 75 JAI 108 MI 39

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached a note from the delegation of Cyprus with a view to the meeting of the Working Party on Social Questions on 18 February 2010.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**Note from the delegation of CYPRUS**

**Subject: Recital 15**

**Cyprus** would be interested to hear the views of Member States and the Commission on to the following suggestion concerning Recital 15:

15\.     Actuarial and risk factors related to disability and to age are used in the provision of insurance, banking and other financial services. These should not be regarded as constituting discrimination where service providers have shown **to the competent authorities**, by relevant actuarial principles, accurate statistical data or medical knowledge, that  such factors are determining factors for the assessment of risk.

**Justification**

This addition is necessary so that Recital 15 complies with Article 2 paragraph 7(a). Otherwise it would not be clear to whom the service providers would "have shown….. that such factors are determining factors for the assessment of risk”.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_