---
title: 'COMITATO DI RADICALI ITALIANI: APPROVATA ALL''UNANIMITÀ MOZIONE PARTICOLARE SU DIRITTO AL MATRIMONIO CIVILE TRA PERSONE DELLO STESSO SESSO'
date: Sun, 18 Apr 2010 19:34:57 +0000
draft: false
tags: [Affermazione Civile, Amore Civile, Comunicati stampa, MATRIMONIO, Mozione, Radicali Italiani]
---

Si è conclusa oggi, a Roma, l'assise del [**Comitato nazionale**](http://www.radicali.it/view.php?id=132189) di Radicali Italiani, con l'approvazione all'unanimità (due astensioni) della **[mozione generale](http://www.radicali.it/view.php?id=155939)**. (**[Audiovideo](http://www.radioradicale.it/scheda/301486)** dei lavori del Comitato)

Il Comitato  ha votato all'unanimità anche la **mozione particolare** (a prima firma di Yuri Guaiana, rappresentante dell'Associazione Radicale Certi Diritti presso il Comitato) sul diritto al matrimonio civile tra persone dello stesso sesso e sul rilancio della proposta di riforma del diritto di famiglia "**[Amore civile](http://www.amorecivile.it/)**".

**[  
](http://www.radioradicale.it/scheda/301486)**

Di seguito il testo della mozione:

Il Comitato nazionale di Radicali italiani, riunitosi in Roma, via di Torre Argentina, dal 16 al 18 aprile 2010

**preso atto** della recente sentenza della Corte Costituzionale con la quale, pur respingendo i rilievi di costituzionalità in merito all’impossibilità, per le coppie di persone dello stesso sesso, di unirsi in matrimonio civile, la Corte ha:

§       chiarito che le unioni tra persone dello stesso sesso godono delle stesse garanzie costituzionali delle unioni tra persone di sesso opposto;

§       chiarito che spetta al legislatore intervenire nel vuoto normativo esistente, scegliendo la forma che meglio tuteli le garanzie per queste coppie, senza escludere la scelta del matrimonio civile;

§       dichiarato di poter intervenire da subito, anche in assenza di legislazione specifica, per tutelare i singoli diritti di queste coppie, laddove si dovessero registrare situazioni di disparità e discriminazione, come già avvenuto nei casi di more uxorio;

**ricordato** che questo risultato è stato possibile grazie alla campagna di “Affermazione civile” condotta dall’Associazione radicale Certi diritti e dall'Avvocatura lgbt - Rete Lenford, che hanno saputo coinvolgere e sostenere molte coppie italiane nella loro scelta di civiltà e uguaglianza; ed hanno inoltre saputo coinvolgere centinaia tra giuristi e avvocati di primo piano, i quali hanno sostenuto le ragioni della campagna per il diritto al matrimonio di fronte alle Corti italiane e nel Paese, alimentando una vera e propria campagna di rinnovamento culturale e di riforma del diritto;

**ricordando** che Radicali italiani ha fatto propria, con l'adozione di vari documenti anche congressuali, la campagna di “Affermazione civile”, volta ad affermare il diritto di uguaglianza in relazione all'istituto del matrimonio civile, e il progetto di riforma del diritto di famiglia denominato “Amore civile”, che rappresenta la migliore piattaforma laica, riformista e garantista su questi temi, al servizio, e a disposizione, delle forze politiche e sociali italiane;

**ritenendo** che la sentenza della Corte Costituzionale renda prioritaria la necessità di rimuovere le discriminazioni ai danni delle coppie dello stesso sesso e apra nuove possibilità per affermare il diritto di uguaglianza, anche con l'apertura dell'istituto del matrimonio civile alle coppie omosessuali;

**invita**

i deputati e i senatori radicali a presentare in Parlamento il progetto di riforma del diritto di famiglia “Amore civile” e, pur considerando la difficoltà della situazione politica, a promuovere tutte le azioni finalizzate alla sua approvazione, anche parziale, così come in parte esplicitato nelle motivazioni della suddetta sentenza della Corte Costituzionale;

**impegna**

Radicali Italiani, in stretta collaborazione con i deputati e i senatori, radicali e non, a continuare le iniziative di promozione del progetto di riforma “Amore civile” nella società e nelle Istituzioni, anche collaborando ad individuare puntuali iniziative politiche (di affermazione o di disobbedienza civile) che diano corpo alle istanze riformatrici ivi contenute;

a sostenere e a promuovere in tutte le sedi la campagna per il diritto al matrimonio tra persone dello stesso sesso così come richiamato dagli articoli 21 della Carta europea dei diritti dell’uomo; 19 del nuovo Trattato della Comunità europea, così come modificato dal Trattato di Lisbona; e 3 della nostra Carta costituzionale; tenuto conto che tale iniziativa ha come obiettivo la piena applicazione del diritto all'uguaglianza e la lotta alle discriminazioni;

**auspica**

che i dirigenti radicali sottoscrivano l'appello per il riconoscimento del diritto al matrimonio civile tra persone dello stesso sesso, promosso dal Comitato “Sì, lo voglio”.