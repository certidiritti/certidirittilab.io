---
title: 'Parlamento Europeo approva risoluzione contro omo-transfobia molto importante. Il testo integrale'
date: Thu, 24 May 2012 08:32:28 +0000
draft: false
tags: [Europa]
---

Il documento chiede ai paesi membri anche leggi e iniziative concrete su unioni civili e matrimonio tra persone dello stesso sesso.

Roma, 24 maggio 2012

Il Parlamento Europeo ha votato questa mattina una importantissima e storica Risoluzione contro l’omofobia e la transfobia in Europa. Il testo è stato approvato con 430 voti a favore, 105 contrari e 59 astensioni.

Il testo condanna con forza tutte le discriminazioni basate sull'orientamento sessuale e sull'identità di genere e deplora vivamente che tuttora, all'interno dell'Unione europea, i diritti fondamentali delle persone LGBT non siano sempre rispettati appieno; invita pertanto gli Stati membri a garantire la protezione di lesbiche, gay, bisessuali e transgender dai discorsi omofobi di incitamento all'odio e dalla violenza e ad assicurare che le coppie dello stesso sesso godano del medesimo rispetto, dignità e protezione riconosciuti al resto della società; esorta gli Stati membri e la Commissione a condannare con fermezza i discorsi d'odio omofobi o l'incitamento all'odio e alla violenza nonché ad assicurare che la libertà di manifestazione, garantita da tutti i trattati sui diritti umani, sia effettivamente rispettata.

Il testo della Risoluzione chiede anche alla Commissione europea di rivedere la decisione quadro sul razzismo e la xenofobia per rafforzarne e ampliarne il campo di applicazione, onde includere i reati di odio basati sull'orientamento sessuale, l'identità di genere e l'espressione di genere; invita la Commissione a garantire che la discriminazione in relazione all'orientamento sessuale sia proibita in tutti i settori, completando il pacchetto antidiscriminazione basato sull'articolo 19 del trattato sul funzionamento dell'Unione europea; invita la Commissione e gli Stati membri a garantire che la direttiva 2004/38/CE sulla libera circolazione sia attuata senza discriminazioni basate sull'orientamento sessuale e a proporre misure per riconoscere reciprocamente gli effetti dei documenti di stato civile in base al principio del riconoscimento reciproco; invita la Commissione a garantire che la relazione annuale sull'applicazione della Carta dei diritti fondamentali comprenda una strategia per rafforzare la protezione dei diritti fondamentali nell'UE, includendo informazioni integrali ed esaustive sull'incidenza dell'omofobia negli Stati membri nonché le soluzioni e le azioni proposte per superarla.

Il testo della Risoluzione è uno dei più importanti documenti votati dal Parlamento Europeo. L’Associazione Radicale Certi Diritti ringrazia tutti coloro che lo hanno preparato, firmato, depositato e votato, dall’intergruppo Lgbt, ai funzionari, ai Deputati europei, anche quelli del Ppe.

Il testo integrale della Risoluzione qui di seguito:

_Il Parlamento europeo_,

–   visti la Dichiarazione universale dei diritti dell'uomo, il Patto internazionale sui diritti civili e politici, la Convenzione sui diritti del fanciullo e la Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali,

–   visti l'articolo 2, l'articolo 3, paragrafo 5, e gli articoli 6, 7, 21 e 27 del trattato sull'Unione europea, gli articoli 10 e 19 del trattato sul funzionamento dell'Unione europea e la Carta dei diritti fondamentali dell'UE,

–   visto lo strumentario per la promozione e la tutela dell'esercizio di tutti i diritti umani da parte di lesbiche, gay, bisessuali e transgender (LGBT), adottato dal gruppo di lavoro "Diritti umani" del Consiglio dell'Unione europea,

–   viste la risoluzione 1728 dell'Assemblea parlamentare del Consiglio d'Europa, del 29 aprile 2010, sulla discriminazione basata sull'orientamento sessuale e l'identità di genere, e la raccomandazione CM/Rec(2010)5 del Comitato dei ministri, del 31 marzo 2010, sulle misure per combattere la discriminazione fondata sull'orientamento sessuale o l'identità di genere,

–   vista la relazione dell'Agenzia dell'Unione europea per i diritti fondamentali del novembre 2010 in materia di omofobia, transfobia e discriminazioni basate sull'orientamento sessuale e l'identità di genere,

–   vista la sua risoluzione del 18 aprile 2012 sui diritti umani nel mondo e la politica dell'Unione europea in materia, comprese le conseguenze per la politica strategica dell'UE in materia di diritti umani,

–   vista la sua risoluzione del 14 dicembre 2011 sul prossimo vertice UE-Russia,

–   vista la sua risoluzione del 28 settembre 2011 sui diritti umani, l'orientamento sessuale e l'identità di genere nel quadro delle Nazioni Unite,

–   vista la sua risoluzione del 19 gennaio 2011 sulla violazione della libertà di espressione e sulle discriminazioni basate sull'orientamento sessuale in Lituania,

–   vista la sua risoluzione del 17 settembre 2009 sulla legge lituana sulla tutela dei minori contro gli effetti dannosi della pubblica informazione,

–   viste le sue precedenti risoluzioni sull'omofobia, in particolare quelle del 26 aprile 2007 sull'omofobia in Europa, del 15 giugno 2006 sull'intensificarsi della violenza razzista e omofoba in Europa, del 18 gennaio 2006 sull'omofobia in Europa,

–   visto l'articolo 110, paragrafi 2 e 4, del suo regolamento,

A. considerando che l'Unione europea si fonda sui valori del rispetto della dignità umana, della libertà, della democrazia, dell'uguaglianza, dello Stato di diritto e del rispetto dei diritti umani, compresi i diritti delle persone appartenenti a minoranze, e che essa afferma e promuove tali valori nelle sue relazioni con il resto del mondo;

B.  considerando che l'omofobia consiste nella paura e nell'avversione irrazionali provate nei confronti dell'omosessualità femminile e maschile e di lesbiche, gay, bisessuali e transgender (LGBT) sulla base di pregiudizi, ed è assimilabile al razzismo, alla xenofobia, all'antisemitismo e al sessismo; che si manifesta nella sfera pubblica e privata sotto diverse forme, tra cui incitamento all'odio e istigazione alla discriminazione, scherno e violenza verbale, psicologica e fisica, persecuzioni e uccisioni, discriminazioni a violazione del principio di uguaglianza e limitazione ingiustificata e irragionevole dei diritti, e spesso si cela dietro motivazioni fondate sull'ordine pubblico, sulla libertà religiosa e sul diritto all'obiezione di coscienza;

C. considerando che in Russia sono state approvate leggi penali e amministrative contro la "propaganda dell'omosessualità" nelle regioni di Rjazan' nel 2006, Archangel'sk nel 2011 e Kostroma e San Pietroburgo nel 2012, e che le regioni di Novosibirsk, Samara, Kirov, Krasnojarsk e Kaliningrad stanno considerando l'adozione di norme simili; che dette leggi prevedono sanzioni fino a 1 270 euro per le persone fisiche e fino a 12 700 euro per le associazioni e le imprese; che la Duma federale sta considerando l'introduzione di una legge analoga;

D. considerando che in Ucraina sono all'esame del parlamento due progetti di legge presentati nel 2011 e nel 2012 nell'ottica di introdurre il reato di "diffusione dell'omosessualità", che includerebbe l'organizzazione di riunioni, parate, azioni, dimostrazioni e manifestazioni di massa intese a diffondere intenzionalmente informazioni positive sull'omosessualità; che le sanzioni proposte prevedono multe e pene detentive fino a cinque anni; che la commissione per la libertà di espressione e di informazione sostiene tali progetti di legge;

E.  considerando che in Moldova le città di Bălți, Sorochi, Drochia, Cahul, Ceadîr Lunga e Hiliuţi come pure i distretti di Anenii Noi e Basarabeasca hanno recentemente adottato leggi intese a vietare la "propaganda aggressiva di orientamenti sessuali non tradizionali" e in un caso le "attività musulmane", e che tali misure sono già state dichiarate incostituzionali dalla Cancelleria di Stato nel caso di Chetriş;

F.  considerando che in Lituania resta giuridicamente difficile stabilire se la pubblica informazione possa promuovere l'accettazione dell'omosessualità in base alla legge sulla tutela dei minori contro gli effetti dannosi della pubblica informazione, quale modificata nel 2010;

G. considerando che in Lettonia un membro del consiglio municipale di Riga ha recentemente presentato un progetto di legge inteso a vietare la "propaganda dell'omosessualità" per impedire lo svolgimento della marcia dell'orgoglio baltico (Baltic Pride) del marzo 2012, e che questa proposta non è ancora stata esaminata;

H. considerando che in Ungheria il partito di estrema destra Jobbik ha recentemente presentato diversi progetti di legge intesi a introdurre il nuovo reato di "diffusione dei disturbi del comportamento sessuale" e che il partito Fidesz ha presentato al consiglio municipale di Budapest un'ordinanza locale per "limitare le marce oscene" prima del gay pride di Budapest; che le proposte sono successivamente state abbandonate;

I.   considerando che la delegazione dell'UE in Moldova ha espresso "profondo rammarico e viva preoccupazione" in relazione a tali "manifestazioni di intolleranza e discriminazione";

J.   considerando che la Commissione ha asserito il proprio impegno ad assicurare il rispetto dei diritti umani e delle libertà fondamentali nell'UE, dichiarando che l'omofobia non sarà tollerata in Europa;

K. considerando che negli Stati membri e nei paesi terzi continuano a verificarsi casi di omofobia, che si manifesta sotto forma di omicidi, interdizione delle marce per l'orgoglio omosessuale (gay pride) e delle manifestazioni per l'uguaglianza, utilizzo pubblico di un linguaggio aggressivo, minaccioso e improntato all'odio, incapacità della polizia di assicurare un'adeguata protezione e violente manifestazioni autorizzate di gruppi omofobi;

L.  considerando che il Parlamento europeo ribadisce il proprio impegno a favore dell'uguaglianza e della non discriminazione in base all'orientamento sessuale e all'identità di genere nell'UE, in particolare per quanto concerne l'approvazione della direttiva del Consiglio recante applicazione del principio di parità di trattamento fra le persone indipendentemente dalla religione o le convinzioni personali, la disabilità, l'età o l'orientamento sessuale, il cui avanzamento è stato bloccato a causa delle obiezioni di alcuni Stati membri, nonché per quanto riguarda le future proposte per il riconoscimento reciproco degli effetti dei documenti di stato civile, la prossima revisione della decisione quadro sul razzismo e la xenofobia per includere il reato di omofobia e una tabella di marcia globale che assicuri l'uguaglianza senza discriminazioni sulla base dell'orientamento sessuale e dell'identità di genere;

**Situazione nell'Unione europea**

1.  condanna con forza tutte le discriminazioni basate sull'orientamento sessuale e sull'identità di genere e deplora vivamente che tuttora, all'interno dell'Unione europea, i diritti fondamentali delle persone LGBT non siano sempre rispettati appieno; invita pertanto gli Stati membri a garantire la protezione di lesbiche, gay, bisessuali e transgender dai discorsi omofobi di incitamento all'odio e dalla violenza e ad assicurare che le coppie dello stesso sesso godano del medesimo rispetto, dignità e protezione riconosciuti al resto della società; esorta gli Stati membri e la Commissione a condannare con fermezza i discorsi d'odio omofobi o l'incitamento all'odio e alla violenza nonché ad assicurare che la libertà di manifestazione, garantita da tutti i trattati sui diritti umani, sia effettivamente rispettata;

2.  chiede alla Commissione di rivedere la decisione quadro sul razzismo e la xenofobia per rafforzarne e ampliarne il campo di applicazione, onde includere i reati di odio basati sull'orientamento sessuale, l'identità di genere e l'espressione di genere;

3.  invita la Commissione a garantire che la discriminazione in relazione all'orientamento sessuale sia proibita in tutti i settori, completando il pacchetto antidiscriminazione basato sull'articolo 19 del trattato sul funzionamento dell'Unione europea;

4.  invita la Commissione e gli Stati membri a garantire che la direttiva 2004/38/CE sulla libera circolazione sia attuata senza discriminazioni basate sull'orientamento sessuale e a proporre misure per riconoscere reciprocamente gli effetti dei documenti di stato civile in base al principio del riconoscimento reciproco;

5.  richiama l'attenzione sulle conclusioni della relazione dell'Agenzia dell'Unione europea per i diritti fondamentali intitolata "Omofobia, transfobia e discriminazioni basate sull'orientamento sessuale e l'identità di genere"; invita la Commissione e gli Stati membri a dare seguito nella massima misura possibile ai pareri contenuti in detta relazione;

6.  chiede alla Commissione di esaminare attentamente i futuri risultati dell'inchiesta LGBT europea dell'Agenzia per i diritti fondamentali e di intraprendere azioni appropriate;

7.  invita la Commissione a garantire che la relazione annuale sull'applicazione della Carta dei diritti fondamentali comprenda una strategia per rafforzare la protezione dei diritti fondamentali nell'UE, includendo informazioni integrali ed esaustive sull'incidenza dell'omofobia negli Stati membri nonché le soluzioni e le azioni proposte per superarla;

8.  chiede nuovamente alla Commissione di stabilire una tabella di marcia globale che assicuri l'uguaglianza senza discriminazioni sulla base dell'orientamento sessuale e dell'identità di genere;

9.  ritiene che i diritti fondamentali delle persone LGBT sarebbero maggiormente tutelati se esse avessero accesso a istituti giuridici quali coabitazione, unione registrata o matrimonio; plaude al fatto che sedici Stati membri offrono attualmente queste opportunità e invita gli altri Stati membri a prendere in considerazione tali istituti;

**Leggi omofobe e libertà di espressione in Europa**

10. esprime viva preoccupazione per gli sviluppi che limitano la libertà di espressione e di associazione in base a idee infondate in materia di omosessualità e transessualità; ritiene che gli Stati membri dell'UE debbano essere esemplari nell'applicazione e nella protezione dei diritti fondamentali in Europa;

11. deplora il fatto che leggi di questo tipo siano già state usate per arrestare e multare i cittadini, compresi i cittadini eterosessuali che esprimono sostegno, tolleranza o accettazione verso lesbiche, gay, bisessuali e transgender; deplora altresì la legittimazione dell'omofobia e talvolta della violenza operata da tali leggi, come nel caso del violento attacco a un autobus di attivisti LGBT avvenuto il 17 maggio 2012 a San Pietroburgo;

12. condanna la violenza e le minacce che hanno caratterizzato il gay pride di Kiev del 20 maggio 2012, in cui due leader della manifestazione sono stati percossi, causando la cancellazione della sfilata; ricorda che gli accordi dell'UE sono subordinati al rispetto dei diritti fondamentali, come stabilito nei trattati, e invita quindi l'Ucraina a introdurre una legislazione che vieti la discriminazione, anche quella basata sull'orientamento sessuale; ritiene che gli attuali sviluppi in Ucraina non siano conformi a tali condizioni; invita le autorità ucraine a ritirare immediatamente i pertinenti progetti di legge, a proporre una legislazione che vieta la discriminazione, compresa quella basata sull'orientamento sessuale, e a impegnarsi per garantire la sicurezza del gay pride di Kiev del prossimo anno;

13. sottolinea che il termine "propaganda" è raramente definito; è costernato dal fatto che le reti di informazione si sono dimostrabilmente censurate, che i cittadini sono intimiditi e temono di esprimere le proprie opinioni e che le associazioni e le società che utilizzano simboli gay-friendly, come gli arcobaleni, possono essere perseguite;

14. sottolinea che queste leggi e queste proposte non sono conformi al Patto internazionale sui diritti civili e politici che vieta le leggi e le pratiche discriminatorie[**(1)**](#_part1_def1) basate sull'orientamento sessuale, e cui aderiscono Russia, Ucraina, Moldova e tutti gli Stati membri dell'UE; chiede al Consiglio d'Europa di indagare su tali violazioni dei diritti umani, di verificarne la compatibilità con gli impegni connessi all'appartenenza al Consiglio d'Europa e la Convenzione europea per i diritti dell'uomo, prendendo le misure adeguate;

15. sottolinea inoltre che l'istruzione è fondamentale ed esprime quindi la necessità di un'educazione sessuale appropriata, accessibile e rispettosa; esorta gli Stati membri e la Commissione a intensificare la lotta all'omofobia attraverso l'istruzione, nonché adottando misure amministrative, giuridiche e legislative;