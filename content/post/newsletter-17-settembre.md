---
title: 'Newsletter 17 Settembre'
date: Sat, 17 Sep 2011 11:22:15 +0000
draft: false
tags: [Politica]
---

**![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)**

oggi si svolgerà a Roma una grande manifestazione, che abbiamo promosso con il gruppo facebook ‘Vaticano paga tu’, Radicali Italiani e tanti altri, contro i privilegi vaticani. Per una nuova breccia. L’**appuntamento è a Porta Pia a partire dalle 15,30**.

Qualche giorno fa **Massimo D’Alema** in un’intervista a Zoro ha affermato che i diritti delle persone lgbt in tempo di crisi possono aspettare e che il matrimonio gay è contrario alla Costituzione. Gli abbiamo risposto.

Grazie a Certi diritti la **censura di Rai1 sul matrimonio gay è arrivata alla Commissione Europea**, che intanto ha affermato che negare solo agli uomini gay di donare il sangue è contrario alla legge europea: sono gli individui che hanno comportamenti sessuali a rischio a non poter donare il sangue, non le categorie.

In questo numero troverai due notizie su **come il fenomeno della prostituzione viene affrontato in Germania e Svizzera**, mentre nel nostro Paese, in nome di uno pseudo – moralismo becero, continuiamo a permettere che migliaia di persone vengano sfruttate dalla malavita.

Ti annunciamo infine che il nostro V Congresso si terrà a **Milano il 3 e 4 dicembre**.  
Vieni ed avrai l’occasione per conoscerci e per discutere con noi!

Buona lettura

**[Iscriviti alla newsletter >](newsletter/newsletter)**

**[Per i diritti e la liberazione sessuale dell’Italia. Sostieni Certi Diritti >](iscriviti)**

ASSOCIAZIONE
============

**[17 settembre a Roma una breccia fiscale contro i privilegi vaticani >](17-settembre-a-roma-breccia-fiscale-contro-i-privilegi-vaticani)**

**D'Alema contro i matrimoni tra persone dello stesso sesso: il compagno di Casini e Giovanardi sbaglia persino le citazioni sulla Costituzione.  
[Leggi >](dalema-contro-i-matrimoni-tra-persone-dello-stesso-sesso-il-compagno-di-casini-e-giovanardi-sbaglia-persino-le-citazioni-sulla-costituzione)**

**Occorre fare chiarezza:[ il matrimonio gay non è contrario alla Costituzione >](certi-diritti-occorre-fare-chiarezza-sulla-sentenza-138/2010-nessun-paletto-ai-matrimoni-gay)**

**Per la prima volta anche le coppie gay nel censimento Istat.   
[Guida rapida al questionario >](http://www.gay.it/faicontareiltuoamore/)**

**Quanti ce ne sono come me? [Un italiano a Londra ci scrive >](quanti-ce-ne-sono-come-me-un-italiano-a-londra-ci-scrive)**

**Grazie a Certi Diritti la censura Rai sul matrimonio gay arriva alla Commissione Europea.  
[Leggi >](grazie-a-certi-diritti-la-censura-rai-sul-matrimonio-gay-arriva-alla-commissione-europea)**

**Unioni civili: radicali, interrogazione al Parlamento europeo.   
[Leggi > ](http://www.agenparl.it/articoli/news/politica/20110908-unioni-civili-radicali-interrogazione-al-parlamento-europeo) **

**Governo italiano contrario alla registrazione delle civil partnership presso i consolati inglesi.  
[Leggi >](governo-italiano-contrario-alla-registrazione-delle-civil-partnership-presso-i-consolati-inglesi)**

**Bonino: politico etero preferito dai gay.   
[Leggi > ](bonino-politico-etero-preferito-dai-gay) **

**Emma Bonino premiata al Gay Village. Il suo messaggio.  
[Leggi >](emma-bonino-premiata-dal-gay-village-il-suo-messaggio)**

**Ragazza muore per pratica bondage-shibari: occorre informazione, basta ipocrisia.   
[Leggi > ](ragazza-muore-per-pratica-bondage-shibari-occorre-informazione-basta-ipocrisia) **

**Bologna: una via per Marcella Di Folco, la prima trans a sconfiggere i benpensanti.   
[Leggi >](http://www.ilfattoquotidiano.it/2011/09/07/bologna-una-via-per-marcella-di-folco-la-prima-trans-a-sconfiggere-i-benpensanti/155903/)**

RASSEGNA STAMPA
===============

**"Zoro intervista D'Alema...governare con l'UDC???..e i diritti civili???"****  
[Guarda il video>](http://www.youtube.com/watch?v=6wH9ZphxSe4&feature=player_embedded)**

**Scalfarotto, Lo Giudice, Alicata e Santacroce lanciano il manifesto per una rete lgbt nel PD.   
[Leggi il manifesto >](http://wordwrite.wordpress.com/2011/09/15/manifesto-per-una-rete-lgbt-nel-pd-come-aderire-e-prima-lista-di-aderenti/)**

**Milano. "Fuori Salone per le lesbiche” la svolta: patrocinio del Comune.  
[Leggi>](http://www.milanotoday.it/politica/fuori-salone-lesbiche-patrocinio-gratuito-milano.html)**

**Milano, lesbica picchiata, per il pm è omofobia.  
[Leggi>](http://www.lettera43.it/attualita/25657/milano-lesbica-picchiata-per-il-pm-e-omofobia.htm)**

**La Curia sfratta Zanardi l’accusatore dei preti.  
[Leggi>](http://www.ilsecoloxix.it/p/savona/2011/09/09/AOwFCk2-zanardi_accusatore_sfratta.shtml)**

**Bisogna tassare le prostitute.  
[Leggi>](http://www.ilfattoquotidiano.it/2011/09/11/bisogna-tassare-le-prostitute/156515/)**

**Nel censimento entrano le coppie gay.   
[Leggi>](http://www3.lastampa.it/cronache/sezioni/articolo/lstp/419509/)**

**Ora il Pd lancia il censimento delle coppie gay: "Riconosciamo i loro diritticome per gli ebrei"  
[Leggi>](http://www.ilgiornale.it/interni/ora_pd_lancia_censimento_coppie_gay_ci_sono_solo_30mila_ebrei_e_hanno_loro_diritti/08-09-2011/articolo-id=544558-page=0-comments=1)**

**L’Italia dei Diritti denuncia traffico di permessi di soggiorno falsi per prostitute nigeriane.  
[Leggi>](http://www.julienews.it/notizia/politica/litalia-dei-diritti-denuncia-traffico-di-permessi-di-soggiorno-falsi-per-prostitute-nigeriane/86698_politica_0.html)**

**Napolitano nomina nuova Giudice della Corte Costituzionale contraria ai matrimoni gay.   
[Leggi>](http://www.gayfreedom.it/?p=6274)**

**Il Pdl si scaglia contro Madonna perché ha criticato Berlusconi.  
[Leggi>](http://www.tuttouomini.it/il-pdl-si-scaglia-contro-madonna-perche-ha-criticato-berlusconi/22875/)**

**Madonna: Giovanardi, e' pro famiglie _gay_, quindi contro nostra cultura.   
[Leggi>](http://www.agenparl.it/articoli/news/politica/20110912-madonna-giovanardi-e-pro-famiglie-gay-quindi-contro-nostra-cultura)**

‎**Gay: radicali, Gran Bretagna chieda immediate scuse dell’Italia.   
[Leggi>](http://www.agenparl.it/articoli/news/esteri/20110912-gay-radicali-gran-bretagna-chieda-immediate-scuse-dell-italia)**

**La Commissione Europea multa l’Italia per la patente negata ai gay.   
[Leggi>](http://www.gaywave.it/articolo/la-commissione-europea-multa-l-italia-per-la-patente-negata-ai-gay/33509/)**

**La Commissione Europea afferma che negare solo agli uomini gay di donare il sangue è contrario alla legge europea. ** **  
[Leggi in inglese> ](http://www.lgbt-ep.eu/press-releases/european-commission-banning-gay-men-donating-blood-against-eu-law/?utm_source=feedburner&utm_medium=email&utm_campaign=Feed%3A+intergrouplgbt+%28The+European+Parliament%27s+Intergroup+on+LGBT+Rights%29)**

**Aids, GB: gay ‘in astinenza’ possono donare il sangue.   
[Leggi>](http://www.agenziaradicale.com/index.php?option=com_content&task=view&id=12805&Itemid=97)**

**Brian Paddick, gay, candidato sindaco a Londra.  
[Leggi>](http://www.queerblog.it/post/12015/brian-paddick-gay-candidato-sindaco-a-londra)**

**Per le prostitute di Bonn arriva il ticket.   
[Leggi>](http://www.corriere.it/esteri/11_agosto_29/ticket-prostitute-burchia_0d328aba-d253-11e0-a205-8c1e98b416f7.shtml)**

**Francia, matrimonio fra due donne: una è transessuale.   
[Leggi>](http://www.lunico.eu/2011091351413/esteri/francia-matrimonio-fra-due-donne-una-e-transessuale.html)**

**Scozia: è prossima l'approvazione del matrimonio gay?   
[Leggi>](http://www.queerblog.it/post/12039/scozia-e-prossima-lapprovazione-del-matrimonio-gay)**

**Israele concede per la prima volta la cittadinanza al coniuge non ebreo di una coppia gay.   
[Leggi>](http://www.queerblog.it/post/12079/israele-concede-per-la-prima-volta-la-cittadinanzia-al-coniuge-non-ebreo-di-una-coppia-gay)**

** “Siamo ovunque!”: l’urlo dei gay iraniani.  
[Leggi>](http://www.giornalettismo.com/archives/146951/siamo-ovunque-lurlo-dei-gay-iraniani/)**

**Egitto: V per Verifica della Verginità .  
[Leggi>](http://it.globalvoicesonline.org/2011/09/egitto-v-per-verifica-della-verginita/)**

**Omosessualità E Diritti: Il Ghana Non E' Un Paese Per Gay.   
[Leggi>](http://www.paid2write.org/attualita_gossip/omosessualita_diritti_il_ghana_non_un_paese_per_gay_17509.html)**

**L'esercito argentino autorizza il primo matrimonio omosessuale.  
[Leggi>](http://rottasudovest.typepad.com/rotta_a_sud_ovest/2011/09/lesercito-argentino-autorizza-il-primo-matrimonio-omosessuale.html)**

**Tammy Baldwin, prima senatrice lesbica della storia degli USA?   
[Leggi>](http://www.queerblog.it/post/12025/tammy-baldwin-prima-senatrice-lesbica-della-storia-degli-usa)**

**Gli studenti gay contro l’omertà della scuola Usa.  
[Leggi>](http://www.giornalettismo.com/archives/147547/gli-studenti-gay-contro-lomerta-della-scuola-usa/)**

**Canada, concorso alla radio in palio c'è una moglie russa.  
[Leggi>](http://www.lastampa.it/_web/cmstp/tmplrubriche/finestrasullamerica/grubrica.asp?ID_blog=43&ID_articolo=2181&ID_sezione=58)**

STUDI, RICERCHE E MATERIALI INFORMATIVI
=======================================

**Fai contare il tuo amore. **Guida rapida per le coppie conviventi LGBT al censimento 2011**.  
[Leggi>](http://www.gay.it/faicontareiltuoamore/)**

**Fedeltà di Coppia: non è poi così bistrattata, soprattutto tra gay …  
[Leggi> ](http://www.donna10.it/fedelta-di-coppia-non-e-poi-cosi-bistrattata-soprattutto-tra-gay-13447.html) **

**Svizzera.** **Violenza contro le prostitute: quali deterrenti?****  
[Leggi>](http://www.swissinfo.ch/ita/societa/Violenza_contro_le_prostitute:_quali_deterrenti_.html?cid=31065150)**

**Una testimonianza dal Regno Unito sui diritti LGBT.  
[Leggi>](http://www.digayproject.org/Home/una_testimonianza.php?c=4710&m=9&l=it)**

**Stati Uniti.****Dieci anni che hanno creato due paesi distanti, su immigrati, gay, religione.   
[Leggi>](http://www.europaquotidiano.it/dettaglio/129164/dieci_anni_che_hanno_creato_due_paesi_distanti_su_immigrati_gay_religione)**

LIBRI E CULTURA
===============

**Milano. Le donne parlano tante lingue. _Un progetto di Usciamo dal Silenzio al Teatro Franco Parenti dal 15 al 21 settembre_.   
[Leggi>](http://www.teatrofrancoparenti.com/spettacolo?i=369)**

**L'orgoglio sessuale a scuola.   
[Leggi>](http://affaritaliani.libero.it/cronache/l_orgoglio_sessuale_a_scuola_inizio_anno70911.html)**

**Internet, la scuola, il bullismo …   
[Leggi>](http://www.italnews.info/2011/09/08/internet-la-scuola-il-bullismo/)**

**Italia razzista? Ma chi dà l’esempio?  
[Leggi>](http://www.ilfattoquotidiano.it/2011/09/08/italia-razzista-ma-chi-da-lesempio/156060/)**

**L'Ultimo Terrestre – Recensione del film.  
[Leggi>](http://www.everyeye.it/cinema/articoli/l-ultimo-terrestre_recensione_14819)**

**Film. ****"La bocca del lupo" conquista il Moma e la critica del New York Times****.[Leggi>](http://www.cittadigenova.com/Genova/Cultura-e-Spettacolo/-La-bocca-del-lupo-conquista-il-Moma-e-42598.aspx)**

**Da rivedere: Littizzetto. La Chiesa contro la depenalizzazione dell'omosessualità.  
[Guarda il video>](http://www.youtube.com/watch?v=ZocgcOJZQZg&feature=related)**

**Gianfranco Meneo racconta le "sessualità disobbedienti".   
[Leggi>](http://www.barlettalife.it/magazine/notizie/gianfranco-meneo-racconta-le-sessualita-disobbedienti/)**

**David Yallop_ , [Misteri della fede – Lo scandalo dei preti pedofili e la copertura della Chiesa](http://www.nuovimondi.info/misteri-della-fede/), _Nuovi Mondi, € 13,50**

**Claudio Rossi Marcelli, _['Hello daddy!'](http://www.ibs.it/code/9788804612971/rossi-marcelli-claudio/hello-daddy-storie.html)_, Strade Blu Mondadori**

[www.certidiritti.it](http://www.certidiritti.it/)
==================================================