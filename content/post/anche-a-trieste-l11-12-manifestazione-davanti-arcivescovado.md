---
title: 'ANCHE A TRIESTE L''11-12 MANIFESTAZIONE DAVANTI ARCIVESCOVADO'
date: Mon, 08 Dec 2008 09:19:05 +0000
draft: false
tags: [Comunicati stampa]
---

Anche a Trieste l' Associazione radicale certi diritti e il circolo arcobaleno arcigay arcilesbica trieste, promuovono pe ril giorno giovedì 11 dicembre una fiaccolata sit-in davanti alla sede dell'Arcivescovado in Via Cavana 16, dalle ore 18 alle ore 19, contro la grave decisione del Vaticano di non sostenere la proposta francese di depenalizzazione dell'omosessualità all'Onu.

In diverse città italiane sono previste manifestazioni e sit-in davanti alle sedi degli arcivescovadi per denunciare l'omofobia vaticana.