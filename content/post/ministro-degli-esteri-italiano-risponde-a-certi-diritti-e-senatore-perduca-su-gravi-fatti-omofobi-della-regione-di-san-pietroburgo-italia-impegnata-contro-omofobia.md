---
title: 'Ministro degli Esteri italiano risponde a Certi Diritti e Senatore Perduca su gravi fatti omofobi della Regione di San Pietroburgo: Italia impegnata contro omofobia'
date: Thu, 23 Feb 2012 20:16:18 +0000
draft: false
tags: [Russia]
---

Il capo delegazione Ue ha espresso preoccupazioni riguardo per leggi omofobe. L'Italia promuove iniziative in ambito Ue e Onu contro le discriminazioni lgbti.

Roma, 23 febbraio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Il Ministro degli Esteri italiano, Giulio Terzi di Sant’Agata ha risposto ad una lettera inviata dal Segretario dell’Associazione Radicale Certi Diritti Yuri Guaiana e dal Senatore Radicale-Pd Marco Perduca sulla grave legge omofoba che sta per essere approvata dalla Regione di San Pietroburgo e che accosta e sanziona la diffusione di testi sull’omosessualità al pari della pedofilia.

Nella lettera il Ministro degli Esteri ci informa che la vicenda è seguita tramite l’Ambasciata italiana a Mosca e attraverso le istituzioni europee, che intrattengono con quel Governo un regolare dialogo istituzionalizzato sui diritti umani nel quadro del partenariato strategico UE-Russia. Nella lettera il Ministro segnala che il tema della proposta di legge contro gli omosessuali  è stato esplicitamente evocato nell’ultima sessione svoltasi a Bruxelles il 29 novembre scorso e che in tale occasione gli interlocutori russi hanno assicurato che Mosca intende rispettare tutti i suoi impegni internazionali.  
Lo scorso 9 febbraio il Capo della Delegazione UE presso la Federazione Russa ha  espresso la preoccupazione dell’Unione Europea in merito al progetto di legge in questione.

Inoltre, il Ministro degli Esteri, ci segnala che la vicenda sarà seguita attentamente anche sul piano bilaterale dei rapporti UE-Russia e attraverso l’Ambasciata italiana a Mosca che già aveva monitorato lo svolgimento di manifestazioni del gay pride  nella Federazione Russa, per verificare il livello di tutela del diritto di espressione degli appartenenti alla comunità LGBTI e che tale azione si inserisce nel più ampio impegno dell’Italia per contrastare ogni forma di discriminazione, comprese quelle basate sull’orientamento sessuale.

A questo riguardo, si legge nella lettera,  in ambito Unione Europea, l’Italia ha sostenuto l’elaborazione di un “toolkit on the rights of LGBT persons” per intervenire nei Paesi terzi ove tali discriminazioni si verifichino. In ambito ONU, l’Italia ha aderito alla Dichiarazione “cross-regional” sottoscritta da 85 Paesi su questo argomento e sostenuto un’analoga Risoluzione adottata dal Consiglio Diritti Umani nel 2011 su iniziativa sudafricana.  
  

**iscriviti alla newsletter >[  
](newsletter/newsletter)[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**