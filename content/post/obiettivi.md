---
title: 'Obiettivi'
date: Wed, 07 Nov 2012 16:10:34 +0000
draft: false
tags: [Politica]
---

**Riforma del diritto di famiglia**

  
**Matrimonio egualitario**

  
**Diritti delle coppie conviventi**

  
**Diritti delle persone transessuali**

  
**Legalizzazione della prostituzione**

  
**Approvazione direttiva europea antidiscriminazione**

  
**Depenalizzazione dell'omosessualità**