---
title: 'MANIFESTAZIONE CONTRO OMOFOBIA VATICANA: L''AUDIOVIDEO DI RADIO RADICALE'
date: Sun, 07 Dec 2008 08:06:17 +0000
draft: false
tags: [Senza categoria]
---

Al seguente link l'audiovideo di Radio Radicale della fiaccolata sit-in promossa da Arcilesbica, Arcigay Roma e Associazione Radicale Certi Diritti in Piazza Pio XII sabato 6 dicembre contro l'omofobia vaticana

[http://www.radioradicale.it/scheda/268383](http://www.radioradicale.it/scheda/268383)