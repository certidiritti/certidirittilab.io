---
title: 'MOBILITAZIONE NONVIOLENTA PER LA GIUSTIZIA: L''ASSOCIAZIONE RADICALE CERTI DIRITTI PARTECIPA ALLE INIZIATIVE E SOTTOSCRIVE LA LETTERA AL PRESIDENTE DELLA REPUBBLICA'
date: Thu, 19 Jul 2012 09:29:53 +0000
draft: false
tags: [Politica]
---

Anche l'Associazione Radicale Certi Diritti partecipa e aderisce alla quattro giorni di mobilitazione nonviolenta sulla drammatica situazione della Giustizia e delle Carceri nel nostro paese.

La grave situazione del sovraffollamento degli istituti penitenziari e dell'ingolfamento della giustizia impone iniziative e azioni che vedono in questi giorni coinvolte in tutta Italia personalità e decine di migliaia di cittadini, insieme ai detenuti e i loro familiari, i direttori degli Istituti, gli agenti di polizia penitenziaria, gli psicologi, gli educatori, il personale sanitario e ammnistrativo, le associazioni che di carcere si occupano da anni, i volontari e i cappellani che condividono l'ennesimo allarme lanciato da Marco Pannella e che richiama urgentemente la politica "alla più grande questione istituzionale e sociale del nostro Paese", cioè lo stato drammatico della Giustizia civile e penale.

L'Associazione Radicale Certi Diritti ha firmato la lettera aperta al Presidente della Repubblica promossa dal Prof. Andrea Pugiotto e sottoscritta da oltre cento professori ordinari di Diritto Costituzionale, di diritto Penale e di Procedura Penale. La lettera è un importante e straordinario documento culturale, scientifico e politico che chiede al Presidente di farsi forte di una sua prerogativa prevista dal comma 2, art. 87 della nostra Costituzione: il messaggio alle Camere, per favorire un processo deliberativo in Parlamento attraverso la formalizzazione delle sue preoccupazioni istituzionali e costituzionali.

E' possibile firmare l'appello al seguente link: www.amnistiasubito.it