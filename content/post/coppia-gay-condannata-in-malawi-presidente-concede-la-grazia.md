---
title: 'COPPIA GAY CONDANNATA IN MALAWI: PRESIDENTE CONCEDE LA GRAZIA'
date: Sat, 29 May 2010 12:24:42 +0000
draft: false
tags: [Comunicati stampa]
---

**MALAWI, COPPIA GAY GRAZIATA DAL PRESIDENTE DELLO STATO. LE PRESSIONI INTERNAZIONALI FUNZIONANO, ORA OCCORRE CONTINUARE LA LOTTA PER LA DEPENALIZZAZIONE DELL’OMOSESSUALITA’ NEI PAESI CHE PREVEDONO LA GALERA E LA PENA DI MORTE.  
  
Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:ù**

**“La notizia che il Presidente del Malawi Bingu Wa Mutharika ha graziato la coppia gay che alcuni giorni fa era stata condannata a 14 anni di lavori forzati non può che renderci felici. E’ evidente  che le pressioni internazionali che molti stati democratici avevano attivato nei confronti dello Stato africano hanno funzionato. In Italia si erano mobilitati i deputati radicali con una interrogazione parlamentare urgente, primo firmatario Matteo Mecacci.  Anche l’Unione Europea e l’Amministrazione del Governo Usa si erano attivamente mobilitati per condannare questa assurda condanna, frutto di ignoranza, pregiudizio e demagogia. Ci auguriamo che ora venga rilanciata quanto prima la campagna promossa dalla Francia in sede Onu, per la depenalizzazione dell’omosessaulità in tutto il mondo. Ci sono ancora 70 paesi che prevedono la galera o la pena di morte per le persone omosessuali”.  
**