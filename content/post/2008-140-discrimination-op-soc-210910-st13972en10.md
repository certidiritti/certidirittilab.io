---
title: '2008-140-Discrimination-OP-SOC-210910-ST13972.EN10'
date: Thu, 30 Sep 2010 10:50:34 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 30 September 2010

Interinstitutional File:

2008/0140 (CNS)

13972/10

LIMITE

SOC 571

JAI 765

MI 322

  

  

  

  

  

OUTCOME OF PROCEEDINGS

from :

The Working Party on Social Questions

on :

21 September 2010

No. prev. doc. :

13505/10 SOC 523 JAI 728 MI 299

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

**I.         INTRODUCTION**

At its meeting on 21 September 2010, the Working Party on Social Questions continued its examination of the above proposal. The discussion focused on the provisions concerning equal treatment in the area of _financial services_, based on a note from the Presidency that had been distributed prior to the meeting (doc. 13505/10)[\[1\]](#_ftn1).

A majority of delegations broadly welcomed the Presidency's efforts to advance the work through in-depth discussion on specific themes.

**II.        MAIN ITEMS DISCUSSED**

CZ questioned the need for the proposal and expressed the view that it conflicted with the principles of subsidiarity and proportionality. Reiterating its own general reservation, DE supported the remarks by CZ, and revoiced its concerns regarding the cost implications of the provisions and the lack of legal uncertainty. MT similarly reiterated its concerns with respect to proportionality and the need for legal certainty.

Cion recalled that directives necessarily afforded the Member States a certain degree of discretion.

**The independent study by CIVIC Consulting (doc. 13505/10, Annex I)**

DE expressed the view that the study did not demonstrate that there was a need for anti-discrimination legislation in the field of financial services at the EU level, given that the number of complaints recorded was very limited as compared with the number of contracts successfully concluded. FR also felt that the number of complaints cited in the report was small.

Others (IE, PL, SE, Cion) welcomed the independent study as useful input to the discussion.

**The Presidency's drafting suggestions (doc. 13505/10, Annex II)**

Many delegations (BG, ES, IE, LU, HU, CY, NL, AT, PL, PT, SK, FI, SE and Cion) welcomed the Presidency's drafting suggestions as a useful basis for further discussion.

DE saw no need for new legislation on equal treatment in the area of _financial services,_ and raised a number of concerns, including the diversity of the rules in force in the different Member States, and the need to distinguish clearly between insurance and banking, as well as between private insurance and social security insurance. Cion recalled that social security fell outwith the scope of this specific provision.

  

Some delegations (CZ, DE FR, IT, MT and UK) felt that the provisions concerning proportionate differences of treatment were too detailed, and lacked legal certainty. CY, FR and IT also questioned the need for the transparency provisions, UK warning against any publication requirement.

Further comments are set out in the footnotes to the Annex.

**III.    CONCLUSION**

All delegations maintained general scrutiny reservations on the proposal and the Presidency's drafting suggestions.

Delegations were informed that the next meeting of the Working Party, provisionally scheduled for 19 October 2010, would focus on the provisions concerning equal treatment in the area of _housing_. To prepare for the discussion, the Presidency distributed a questionnaire[\[2\]](#_ftn2) addressing this topic and invited delegations to submit written **replies by 1 October 2010**.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

_Recital 15_[\[3\]](#_ftn3)

Actuarial and risk factors related to disability and to age are used in the provision of insurance, banking and other financial services. These should not be regarded as constituting discrimination where service providers are able to show by relevant actuarial principles **and** **\[…\]** statistical data or medical knowledge, that  such factors are determining factors for the assessment of risk[\[4\]](#_ftn4). **Differences of treatment on grounds of age and disability should be proportionate. \[Age limits and age bands in financial services can be proportionate differences of treatment on grounds of age if they are set in a reasonable manner.\]**[\[5\]](#_ftn5) **Consumers and relevant judicial and complaints bodies should have the right to be informed**[\[6\]](#_ftn6) **about the reasons justifying differential treatment, \[in advance or upon request,\]**[\[7\]](#_ftn7) **and the information provided should be \[useful and understandable to a general public\]**[\[8\]](#_ftn8)**.**

_Article 2_

_Concept of discrimination_

7.      Notwithstanding paragraph 2, in the provision of financial services, \[Member States may provide[\[9\]](#_ftn9) that\] proportionate differences in treatment on the grounds of age **or disability** shall not be considered discrimination for the purposes of this Directive, if age **or the health condition underlying[\[10\]](#_ftn10) the disability** is a determining factor in the assessment of risk for the service in question and this assessment is based on **\[…\]** actuarial principles and[\[11\]](#_ftn11) **relevant and reliable**[\[12\]](#_ftn12) statistical data, or, \[**where no such data is available\]**[\[13\]](#_ftn13) **for a certain health condition**, on relevant **and reliable** medical knowledge.[\[14\]](#_ftn14)

**Providers of financial services who decide to apply proportionate differences of treatment on the grounds of age or disability shall provide**[\[15\]](#_ftn15) **information on the reasons justifying those differences of treatment.**[\[16\]](#_ftn16)

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) Further written replies to the Presidency's questionnaire on the issue of financial services (doc. 11903/10 + COR 1) have been distributed as follows: 13249/10 (BG), 13456/10 (DK), 13701/10 (PT), 13764/10 (PL) and 13864/10 (FI).

[\[2\]](#_ftnref2) 13883/10  
  

[\[3\]](#_ftnref3) CZ, DE, FR, IT, MT and UK found the provisions overly detailed, warning that undefined terms such as "proportionate" and "reasonable" manner were a source of legal uncertainty (also see Article 2(7)). However, FR and UK supported the mention of "age bands". Cion recalled that the concept of proportionality was well established in EU legislation, and that it was to be understood in a broad sense.

[\[4\]](#_ftnref4) MT and AT: align wording with Article 2(7).

[\[5\]](#_ftnref5) CZ and CY suggested deleting the words in square brackets.

[\[6\]](#_ftnref6) CY, FR and IT questioned the need for the transparency provisions, as consumer protection legislation already exists, the information to be provided might not necessarily be helpful to consumers, and new requirements could place an excessive administrative burden on companies. NL and UK raised the issue of the confidentiality of certain data, UK warning against any publication requirement. Cion pointed put that the provision was lightly formulated and did not require companies to _publish_ data or to reveal internal statistics, but merely to inform consumers of, for example, the way in which higher risks associated with advanced age were taken into account by insurers.

[\[7\]](#_ftnref7) CY, NL and SK: delete words in square brackets; Cion was prepared to accept this suggestion. CZ: delete "in advance … general public".

[\[8\]](#_ftnref8) AT suggested replacing the words in square brackets with "clear and comprehensible".

[\[9\]](#_ftnref9) CY, FR and IT did not support the term "may", as it would allow the Member States to adopt different approaches, which would harm the internal market in financial services. Cion undertook to reflect on this question and reminded delegations that Article 6 provided that "Member States may introduce" more favourable provisions.

[\[10\]](#_ftnref10) IT: reservation.

[\[11\]](#_ftnref11) MT preferred "or".

[\[12\]](#_ftnref12) IT: reservation.

[\[13\]](#_ftnref13) EE, SE and UK suggested deleting the words in square brackets. Cion welcomed taking into account the fact that, for certain rare medical conditions, no statistics were available.

[\[14\]](#_ftnref14) MT and UK called for broader wording that would capture all sources of reliable and relevant data. MT: reservation on Article 2(7).

[\[15\]](#_ftnref15) AT suggested adding "clear and comprehensible".

[\[16\]](#_ftnref16) CY and FR favoured deleting this paragraph. FR: reservation. AT suggested adding: "This information is to be made available to consumers and relevant judicial and complaints bodies by appropriate means."