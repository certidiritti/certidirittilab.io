---
title: 'CONFERENZA GOVERNATIVA SULLA FAMIGLIA'
date: Fri, 05 Nov 2010 11:01:43 +0000
draft: false
tags: [Comunicati stampa]
---

**L’8 NOVEMBRE, ALLE ORE 9,30 SIT-IN E CONFERENZA STAMPA DEI RADICALI CON EMMA BONINO  IN VIA GIOVANNI GATTAMELATA, 5 MILANO.  
**

**A SEGUIRE CONTRO-CONFERENZA SULLE FAMIGLIE ALL’UNIVERSITA’ DI MILANO**

**Lunedì 8 novembre, dalle ore 9,30, in occasione dell’inaugurazione della Conferenza governativa sulla famiglia, i radicali terranno davanti alla sede del Milano Convention Centre, Via Giovanni Gattamelata, 5, un sit-in con una Conferenza Stampa, per illustrare le proposte di Riforma del Diritto di Famiglia totalmente escluse dalla Conferenza.**

**Alla Conferenza Stampa parteciperanno Emma Bonino, vice presidente del Senato, Rita Bernardini, deputata radicale del gruppo PD della Camera, Mario Staderini, segretario di Radicali Italiani, Marco Cappato, segretario dell’Associazione Luca Coscioni, Sergio Rovasio, segretario dell’Associazione radicale Certi Diritti, Marco Perduca, senatore radicale del Gruppo Pd.  
**

  
**Al Sit-in hanno assicurato la loro partecipazione esponenti e rappresentanti di associazioni e forze politiche di diversi schieramenti, impegnati nella promozione e difesa dei diritti civili.  
**

**  
Al termine della Conferenza Stampa si aprirà,  presso l’Università di Milano, Facoltà di Scienze Politiche  (Via Conservatorio – 7),  la Conferenza “Le famiglie italiane, tra politica, società, diritto ed economia” alla quale parteciperanno parlamentari e dirigenti radicali con esperti e giuristi.   
**

Qui di seguito il programma:  
  
**Promosso e organizzato dall’Associazione Radicale Certi Diritti e Radicali Italiani**

**"Le famiglie italiane,  tra politica, società, diritto ed economia"  
**  
**Lunedì 8 novembre 2010 - dalle 10:30 alle 19:00  
Sala Lauree  
**

**Università degli Studi di Milano - Facoltà di Scienze Politiche  
via Conservatorio, 7, Milano  
**  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
  
Il dibattito pubblico è sempre più sclerotizzato attorno a una definizione fallace di famiglia tradizionale che esclude inevitabilmente una grande quantità di cittadini italiani. Le implicazioni di quest’impostazione sono politiche, giuridiche, sociali ed economiche: ad esempio i cittadini omosessuali si vedono negato il diritto fondamentale di sposarsi e di costituire una famiglia; la realtà delle famiglie omogenitoriali continua a essere negata; le coppie che decidono di porre fine al loro rapporto sono vessate da innumerevoli pastoie, gli individui sono sempre più marginalizzati da un modello organicistico di welfare state. Di fronte a questa situazione insostenibile l’Associazione Radicale Certi Diritti apre uno spazio di dibattito e confronto non su cosa debba o non debba essere considerato famiglia (al singolare), ma sulla realtà delle famiglie italiane, sui loro cambiamenti, su come il diritto possa riconoscere e regolare questa composita realtà e sulla misura nella quale il welfare state debba farsene carico.

  
**Dopo gli auguri di buon lavoro del preside della facoltà di Scienze politiche, prof. Daniele Checchi e il saluto introduttivo di Sergio Rovasio, segretario dell’Associazione radicale Certi Diritti, il seminario si articolerà in tre sessioni:  
**

  
1.      DI COSA PARLIAMO QUANDO PARLIAMO DI FAMIGLIE?  
Evoluzione di una definizione tra storia, antropologia, cultura e scienze. Coordina **Paola Ronfani**, ordinario di Sociologia Giuridica, della Devianza e Mutamento Sociale. Ne parlano:

  
**Enrichetta Buchli**, didatta e docente della Scuola di Psicoterapia  
**Stefano Ciccone**, fondatore dell’associazione e rete nazionale Maschile Plurale  
**Giuseppina La Delfa**, presidente dell’Associazione Famiglie Arcobaleno

**Chiara Lalli**, docente di Logica e Filosofia della Scienza e di Epistemologia delle Scienze Umane  
  
  
2.      WELFARE A MISURA DI CAMBIAMENTI:  
Spunti di riflessione e tracce per un lavoro futuro sui principali cambiamenti delle famiglie italiane e sul loro rapporto con il welfare state. Coordina Emma Bonino, vice presidente del Senato. Ne parlano:

**Valeria Manieri**, Radicali Italiani  
**Giorgio Vaccaro**, avvocato e presidente dell’Associazione circolo psicogiuridico.  
  
  
3.      AMORE CIVILE:  
Una proposta di riforma del diritto di famiglia italiano. Coordina **Marilisa D’Amico**, ordinario di Diritto Costituzionale e avvocato cassazionista. Ne parlano:

**Ileana Alesso**, avvocato amministrativista e cassazionista, formatrice e docente in master universitari  
**Massimo Clara**, avvocato civilista e cassazionista, formatore presso pubbliche amministrazioni e istituzioni ospedaliere

**Bruno De Filippis**, esperto di diritto di famigliaElfo Frassino, presidente del Coordinamento Nazionale Associazioni e Comunità di Ricerca Etica Interiore Spirituale  
**Filomena Gallo**, avvocato specializzata in Diritto Minorile, Diritto di Famiglia, Diritto pubblico e vice segretario dell’Associazione Luca Coscioni

**Diego Sabatinelli**, segretario della Lega Italiana per il Divorzio Breve  
  
Porteranno il loro saluto **Cesare Rimin**i, avvocato matrimonialista, giornalista e scrittore, **Marco Perduca**, senatore Radicale, **Paolo Patané**, presidente nazionale di Arcigay, **Luca Trentin**, segretario nazionale di Arcigay, **Rita De Santis**, Presidente dell’Agedo.

  
Per informazioni:[www.certidiritti.it](undefined/)  
[yuri.guaiana@unimi.it](mailto:yuri.guaiana@unimi.it)  
[segretario@certidiritti.it](mailto:segretario@certidiritti.it)

  
+39 3404694701

+39 337798942