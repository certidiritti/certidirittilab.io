---
title: 'CERTI DIRITTI PARTECIPA AL GAY PRIDE BALTICO IL 7 E 8 MAGGIO'
date: Wed, 05 May 2010 10:35:11 +0000
draft: false
tags: [baltico, certi diritti, Comunicati stampa, gay pride, lituania, matrimonio gay, Ottavio Marzocchi, protezione dei minori]
---

**CERTI DIRITTI PARTECIPA AL GAY PRIDE BALTICO IL 7 E 8 MAGGIO CON CARTELLONI FAVOREVOLI AL MATRIMONIO GAY, ATTO POTENZIALMENTE PUNITO DALLA CONTESTATA LEGGE LITUANA SULLA “PROTEZIONE DEI MINORI DALLE INFORMAZIONI PUBBLICHE NOCIVE"**

_**Il responsabile per le questioni europee dell’associazione Certi Diritti Ottavio Marzocchi e l’iscritto spagnolo Joaquin Nogueroles Garcia parteciperanno al Gay Pride Baltico che si terrà nei giorni venerdi 7 (conferenza) e sabato 8 maggio (Marcia) a Vilnius in Lituania.**_

Marzocchi, collaboratore del gruppo parlamentare dell’Alleanza dei Democratici e dei Liberali per l’Europa per la commissione libertà pubbliche del PE, accompagnerà l’Eurodeputata olandese liberale Sophie In’t Veld, con la quale anima il programma "ALDE 4 Equality", che sponsorizza la partecipazione degli eurodeputati liberali ai Gay Prides più problematici, dove l’aiuto ed il supporto internazionale sono più necessari, per monitorare il rispetto da parte delle autorità della libertà di manifestazione e il dovere di assicurare una protezione adeguata ai partecipanti dei Gay Prides. Marzocchi era già stato arrestato a Mosca assieme a Marco Cappato, ex Eurodeputato della Lista Bonino, nel 2007, ed ha partecipato anche a gay prides a Riga (Lettonia), Zagabria (Croazia) e Varsavia (Polonia).

La situazione in Lituania é particolarmente problematica a seguito dell’approvazione di una contestata legge sulla protezione dei minori dalle informazioni pubbliche nocive, che potrebbe essere applicata per colpire qualunque attività o dichiarazione favorevole alle unioni o matrimoni tra persone dello stesso sesso che siano accessibili ai minori. Le autorità locali sono anche ostili al gay pride ed utilzzano in modo pretestuoso le contro-manifestazioni di estrema destra per tentare di bloccare all'ultimo momento il Gay Pride. Proprio oggi i tribunali si pronunceranno sull'ennesimo ricorso al riguardo.

Certi Diritti annuncia che parteciperà alla manifestazione del’8 maggio con cartelloni favorevoli al matrimonio gay in Europa - atto potenzialmente punito dalla legge - e per assicurare che lo Stato di Diritto, la democrazia, i diritti umani e le libertà fondamentali dell’individuo siano rispettati ovunque: in Lituania, in Italia, in Europa e nel mondo.