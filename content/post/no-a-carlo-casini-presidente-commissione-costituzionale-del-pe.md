---
title: 'No a Carlo Casini presidente commissione costituzionale del PE'
date: Mon, 16 May 2011 07:28:54 +0000
draft: false
tags: [Europa]
---

**L'associazione radicale Certi Diritti chiede ai parlamentari europei di sfiduciare Carlo Casini da presidente della commissione costituzionale del Parlamento europeo. L'eurodeputato UDC e leader del movimento per la vita ha chiesto la censura di un videogioco, colpevole di minacciare l'educazione dei bambini, e paragonato le coppie gay alla pornografia.**  
  
Bruxelles - Roma, 16 maggio 2011  
  
Comunicato Stampa dell'Associazine Radicale Certi Diritti  
  
A seguito delle gravi e patetiche dichiarazioni di Carlo Casini, leader del Movimento per la Vita, eurodeputato UDC, nonché Presidente della Commissione Costituzionale del Parlamento europeo,  l'Associazione Radicale Certi Diritti lancia un appello ai parlamentari europei di tutti gli schieramenti perché chiedano le dimissioni del parlamentare da Presidente della Commissione Costituzionale del PE  e non rinnovino il suo mandato in occasione della revisione degli incarichi di presidenza di commissione prevista per la metà mandato del PE.  
  
Le dichiarazioni in merito al videogioco "The Sims" sono in completo contrasto con i valori dell'Unione europea di libertà, tolleranza, eguaglianza e lotta alle discriminazioni contro le persone LGBT, valori che il Parlamento europeo promuove in tutte le sue risoluzioni e che il Presidente del PE Jerkzy Buzek ha ribadito in occasione del festeggiamento della giornata mondiale contro l'omofobia. Carlo Casini, nelle sue dichiarazioni, chiede la censura del videogioco perché "molto pericoloso" e perché "minaccia l'educazione dei bambini", la diffusione ha risvolti di carattere "igienico-sanitario" (?) e infine compara le coppie dello stesso sesso alla pornografia, concludendo con l'intenzione di "intervenire in questa materia a livello europeo". Le sue dichiarazioni rincorrono quelle recentissime del sottosegretario Carlo Giovanardi del PdL, nel tentativo disperato di portare voti fondamentalisti all'UDC.  
  
Certi Diritti chiede ai parlamentari europei se ritengono opportuno politicamente che il Presidente della loro Commissione Costituzionale faccia tali dichiarazioni che alimentano l'odio e il pregiudizio proprio alla vigilia della giornata mondiale contro l'omofobia e che sono in netto contrasto con le sue competenze..  
  
  
  
Occorre anche ricordare che secondo informazioni non smentite il Presidente Casini avrebbe collaborato ufficialmente con il Governo ungherese di estrema destra nell'elaborazione della nuova Costutizione che prevede, tra l'altro, il divieto di matrimonio tra persone dello stesso sesso ed il cancellamento dell'orientamento sessuale dalle cause proibite di discriminazione.  
  
Chiediamo alle istituzioni ed ai parlamentari europei di non rinnovare il mandato di Presidente della Commissione Costituzionale del PE e di ripetere quanto hanno fatto in occasione della doppia bocciatura di Rocco Buttiglione alla Commissione europea, ritenendo inopportuno che persone che si ispirano ad una visione della società di tipo religioso-fondamentalista possano accedere a posizioni di influenza politica e rappresentare le istituzioni europee.  
  
  
  
  
Nota: la Commissione Costituzionale ha le seguenti competenze di politiche fondamentali per l'Unione europea:  
1.    gli aspetti istituzionali del processo d'integrazione europea, in particolare nel quadro della preparazione e dei lavori delle convenzioni e delle conferenze intergovernative;  
2.    l'applicazione del trattato UE e la valutazione del suo funzionamento;  
3.    le conseguenze istituzionali dei negoziati per l'allargamento dell'Unione europea;  
4.    le relazioni interistituzionali, compreso l'esame, sulla base dell'articolo 127, paragrafo 2, del regolamento, degli accordi interistituzionali ai fini della loro approvazione da parte del Parlamento;  
5.    la procedura elettorale uniforme;  
6.    i partiti politici a livello europeo, fatte salve le competenze dell'Ufficio di presidenza;  
7.    la constatazione dell'esistenza di una violazione grave e persistente da parte di uno Stato membro dei principi comuni agli Stati membri;  
8.    l'interpretazione e l'applicazione del regolamento e le proposte di modifica del medesimo.