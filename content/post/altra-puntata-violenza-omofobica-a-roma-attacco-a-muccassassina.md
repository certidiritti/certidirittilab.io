---
title: 'ALTRA PUNTATA VIOLENZA OMOFOBICA A ROMA: ATTACCO A MUCCASSASSINA'
date: Wed, 26 Aug 2009 10:32:16 +0000
draft: false
tags: [Comunicati stampa]
---

GAY- ATTACCO AL QUBE, ROVASIO: SOLIDARIETÀ AL CIRCOLO MARIO MIELI. SI INTERVENGA SUBITO CONTRO ENNESIMO ATTACCO OMOFOBICO

Roma, 26 agosto 2009

• Dichiarazione di Sergio Rovasio, segretario dell’Associazione radicale Certi Diritti

Come purtroppo già previsto gli atti di violenza omofobica continuano. Questa volta è stato colpito il luogo simbolo della vita notturna gay di Roma,anche questa volta la nostra associazione non può che esprimere tutta la sua solidarietà e vicinanza agli amici del Circolo Mario Mieli di Roma, organizzatore di Muccassassina, e all’amico Shlomo, proprietario del locale Qube. Ormai non passa giorno senza che la cronaca registri azioni di violenza omofobica, evidentemente non più atti isolati. Non rimane che fare appello al Governo italiano, alla prefettura di Roma, alle forze dell’ordine, alla magistratura affinché vengano perseguite adeguatamente tali violenze. Gli amici del Circolo Mario Mieli impegnati da sempre nella difesa dei diritti della comuntà Lgbt, meritano il massimo sostegno e protezione, ci auguriamo che le autorità sappiano muoversi in tal senso e non attendano che accada qualcosa di più grave.