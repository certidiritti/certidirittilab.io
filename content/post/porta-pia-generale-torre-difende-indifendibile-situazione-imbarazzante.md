---
title: 'PORTA PIA: GENERALE TORRE DIFENDE INDIFENDIBILE. SITUAZIONE IMBARAZZANTE'
date: Tue, 23 Sep 2008 14:41:43 +0000
draft: false
tags: [Comunicati stampa]
---

PORTA PIA: IL GENERALE TORRE DIFENDE L’INDIFENDIBILE, L’ IMBARAZZO NON CANCELLA LA BRUTTA FIGURA FATTA, L’ ONORE DELLE ARMI SI FA RICORDANDO TUTTI I CADUTI, NON QUELLI DELLA PARTE AVVERSA. I 49 BERSAGLIERI RICORDATI NELLA CERIMONIA RADICALE DELLE ORE 16, QUELLA VERA!

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

“Sabato 20 settembre la nostra Associazione Radicale Certi Diritti, è stata la prima a denunciare quanto il Generale Antonino Torre aveva detto nell’ambito della commemorazione della Breccia di Porta Pia, svoltasi dalle ore 10. La cerimonia, dove lui è interventuo, era quella ‘istituzionale’, con i rappresnentanti del Comune, della Provincia di Roma e della Regione Lazio. Siamo tra quelli che, secondo quanto scrive il Generale sul Corriere della Sera di oggi, “si sono strappati le vesti” dopo aver sentito il suo intervento. Il Generale Torre, oggi, su Il Corriere della Sera, ripete ancora che è ‘falso che i bersaglieri non sono stati ricordati’ e che lui ha fatto quella commemorazione “d’intesa e su sollecitazione dell’associazione Bersaglieri”. Al di là delle altre considerazioni che, francamente, ci sembrano dei tentativi di difesa di una grave situazione di imbarazzo, vorremmo porre al generale due semplici domande:

1) per quale motivo, alla commemorazione ‘istituzionale’ delle ore 10 il Generale Torre è intervenuto a nome di un’Associazione senza che gli organizzatori permettessero alle altre Associazioni presenti di intervenire? Difatti, le Associazioni presenti (Uaar, Certi Diritti, Fondazione Massimo Consoli, Mazziniani d’Italia, ecc) hanno potuto esprimere il loro dissenso con un corale ‘Buuuuuu’, ‘Buuu’ perchè i tecnici del Comune avevano staccato immediatamente l’impianto audio;

2) perché il Generale Torre continua a ripetere che si è voluto “riconoscere l’onore delle armi” ai 16 soldati papalini, che come tutti sanno erano mercenari, senza neanche degnare di alcun onore i 49 bersaglieri? Difatti, i nomi dei bersaglieri di Porta Pia, sono stati letti dai radicali nella cerimonia, diciamo, quella vera, svoltasi alle ore 16 dello stesso giorno con i deputati Maria Antonietta Farina Coscioni, Mario Pepe e Maurizio Turco.

Nessuno dei rappresentanti istituzionali aveva fatto la mattina alcunA menzione, né prima, né dopo il suo intervento, dei 49 caduti di parte italiana. Almeno un ricordo dei loro nomi si poteva fare! O no, Signor Generale?”.