---
title: 'Questura di Roma rilascia per la prima volta documento di soggiorno per motivi familiari a ragazzo extra-ue sposato con italiano ad Oslo'
date: Tue, 16 Oct 2012 09:31:24 +0000
draft: false
tags: [Diritto di Famiglia]
---

Comunicato Stampa dell'associazione Radicale Certi Diritti

Roma, 16 ottobre 2012

Idan è israeliano e vive da dieci anni a Roma con Emanuele, lo scorso luglio si sono sposati a Oslo.  
Una settimana fa, accompagnato dal marito e da due membri dell'associazione radicale Certi Diritti che ha seguito il caso, **Idan si è recato alla Questura di Roma e ha richiesto la carta di soggiorno come familiare di cittadino comunitario in base alle norme sulla libera circolazione in Europa**, recepite in Italia con il d.lgs. n. 30/2007, e ai pronunciamenti della Corte europea dei diritti dell'uomo sull’argomento. La carta di soggiorno è stata rilasciata ieri. **E' il sesto caso in Italia, dopo quelli di Reggio Emilia, Rimini e Milano, e il primo a Roma**.

Continua così il cammino verso il riconoscimento del matrimonio eugualitario iniziato **[nel 2008 dall'associazione radicale Certi Diritti con la campagna di Affermazione Civile](campagne-certi-diritti/itemlist/category/86-affermazione-civile)**, che nel 2010 ha portato alla sentenza 138/2010 della Corte costituzionale che ha affermato "la necessità di un trattamento omogeneo tra la condizione della coppia coniugata e quella della coppia omosessuale".  
E' proprio la sentenza 138/10 che il Tribunale di Reggio Emilia ha citato nell’ordinare alla prefettura il rilascio del (primo) titolo di soggiorno (previsto per i coniugi di cittadini di Paesi membri dell’Unione europea) ad una coppia dello stesso sesso sposata all'estero (sentenza del 13/02/2012).

E i risultati positivi sono subito arrivati: dopo il caso pilota a Reggio Emilia, dove è stato necessario il ricorso al giudice, **[a Milano, Rimini e ora Roma la Questura ha subito rilasciato i permessi di soggiorno](http://www.certidiritti.org/2012/08/31/questura-di-milano-conferma-lorientamento-del-tribunale-di-reggio-emilia-il-coniuge-dello-stesso-sesso-di-un-cittadino-europeo-ha-il-diritto-di-risiedere-legalmente-in-italia/)** alla persona di origine extraeuropea della coppia dello stesso sesso.

Ora **la campagna di [Affermazione civile](campagne-certi-diritti/itemlist/category/86-affermazione-civile) continua con cause pilota con l'obiettivo di far esprimere le Corti su quegli ambiti in cui le coppie dello stesso sesso stabilmente conviventi hanno diritto ad un trattamento uguale a quello delle coppie sposate**: la pensione, il lavoro, le tasse, la salute, ecc...

Tutto questo però non sarebbe possibile senza coppie consapevoli dei propri diritti e determinate a volerli vedere riconosciuti. Quindi l'associazione radicale Certi Diritti non può che ringraziarle, anche perchè di queste coppie ce ne sarà sempre più bisogno.  
**L'associazione ad esempio sta cercando una coppia di italiani dello stesso sesso in cui solo uno dei componenti abbia un lavoro dipendente per avviare una nuova causa pilota**. Chi fosse disponibile può scrivere a [info@certidiritti.it.](mailto:info@certidiritti.it.)

Intanto Certi Diritti, dopo **[l'incontro con la ministra Cancellieri](http://www.certidiritti.org/2012/06/15/associazione-radicale-certi-diritti-incontra-ministro-degli-interni-su-circolare-amato-a-breve-la-risposta/)** e **[le interrogazioni dei parlamentari radicali](http://www.certidiritti.org/2012/07/11/parlamentari-radicali-depositano-interrogazione-urgente-per-ritiro-circolare-amato-che-alimenta-discriminazioni/)**, **continua a chiedere al governo il ritiro della Circolare Amato del 2007** n. 55 che vieta ai Comuni la trascrizione dei matrimoni tra persone dello stesso sesso contratti all’estero anche da cittadini italiani per ragioni di 'ordine pubblico'. Solo così sarà eliminata la contraddizione per la quale si rilasciano permessi di soggiorno per motivi familiari a coniugi dello stesso sesso senza che il loro matrimonio contratto all’estero venga riconosciuto.