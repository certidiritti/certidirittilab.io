---
title: 'GRAZIE A BERLUSCONI E FINI PER LA SVOLTA MUSICALE'
date: Thu, 27 Mar 2008 20:17:18 +0000
draft: false
tags: [BERLUSCONI, Comunicati stampa, FINI, RADICALI]
---

Roma, 8 marzo 2008

• Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti

Con grande stupore questa mattina abbiamo assistito all’inaugurazione della campagna elettorale della destra italiana con musiche, danze e canzoni inequivocabili.

Che si tratti finalmente della svolta europea arrivata anche in Italia? Berlusconi e Fini, questa mattina, al Palalido di Milano, hanno inaugurato il loro più importante intervento elettorale con la canzone “Ymca” dei Village People” facendo ballare 8 mila persone. Non contenti hanno poi proseguito con i brani “Enola Gay” e “Sarà perché ti amo” dei Ricchi e Poveri, qualcosa di più di un semplice fatto casuale, crediamo. Sembra che abbiano voluto mandare un messaggio importante con l’arte della musica.

Il brano ‘Ymca’ è considerato l’inno dei gay in tutto il mondo da quando i Village People decisero di dedicare una canzone alla comunità lgbt ispirandosi ai centri sportivi e di accoglienza della Young Men’s Christian Association diffusi in tutti gli Usa e in vari paesi del mondo, noti luoghi di rimorchio gay. Con le canzoni ‘Enola Gay’ e ‘Sarà perché ti amo’ abbiamo la conferma che si è voluto dare un messaggio agli elettori della Cdl. Certo, se avessero messo anche le canzoni ‘Tuca Tuca’ o ‘Tanti auguri’ della Carrà o, perché no, ‘Pensiero Stupendo’ di Patty Pravo, sarebbe stato meglio ma siamo comunque contenti della svolta berlusconian-finiana che segnaleremo subito all’”International Lesbian and Gay Association”. Finalmente anche in Italia si può sperare in una destra che garantisce i diritti per le persone lgbt, come nella Spagna di Rajoy e nella Francia di Sarkozy, o no?