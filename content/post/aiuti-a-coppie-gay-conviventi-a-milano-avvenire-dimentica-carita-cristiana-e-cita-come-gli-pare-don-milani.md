---
title: 'Aiuti a coppie gay conviventi a Milano: Avvenire dimentica carità cristiana e cita come gli pare don Milani'
date: Sun, 29 Jan 2012 13:30:39 +0000
draft: false
tags: [Politica]
---

Avvenire dimentica carità cristiana e usa memoria selettiva per ricordare le parole di don Milani. Il quotidiano dei vescovi di rilegga gli Artt. 2 e 3 della Costituzione italiana.

Roma, 29 gennaio 2012  
   
Comunicato Stampa dell'Associazione Radicale Certi Diritti  
   
Gli attacchi di Avvenire alla decisione della Giunta Pisapia per aver aperto il fondo anti-crisi anche alla coppie dello stesso sesso sono pretestuosi e gratuiti. L'Avvenire cita a sproposito la famosa frase di don Milani secondo cui «La peggiore ingiustizia è trattare in maniera uguale situazioni differenti». Evidentemente per Avvenire c'è un amore di serie A e uno di serie B e se una coppia è formata da persone dello stesso sesso la crisi economica le colpisce in maniera diversa. Alla faccia della carità cristiana. Chissà come mai poi Avvenire non ricorda mai che don Milani tolse il crocifisso dalle pareti delle scuole di San Donato di Calenzano e di Barbiana, dove insegnava, dimostrando che si può essere cattolici senza pretendere d'imporre alcunché al prossimo. Impari Avvenire la lezione più profonda di don Lorenzo Milani e la smetta di pretendere che la morale cattolica si traduca in leggi e atti amministrativi. L'Italia è nata laica e tornerà ad esserlo.  
   
Sull'art. 29 della Costituzione, poi, il significato della norma non è quello di riconoscere il fondamento della famiglia in una sorta di diritto naturale, come pretende Avvenire, bensì quello di affermare la preesistenza e l’autonomia della famiglia dallo Stato così imponendo dei limiti al potere del legislatore statale, come emerge dagli atti del dibattito svolto in seno all’Assemblea costituente nel ricordo degli abusi in precedenza compiuti a difesa di una certa tipologia di famiglia. Peraltro, che la tutela della tradizione non rientri nelle finalità dell’art. 29 e che famiglia e matrimonio siano istituti aperti alle trasformazioni sarebbe dimostrato dall’evoluzione che ne ha interessato la disciplina dal 1948 ad oggi.  
   
Oltre a una memoria selettiva nei confronti di don Milani, Avvenire legge selettivamente anche la nostra Costituzione e si dimentica l'art. 2 - che riconosce le formazioni sociali ove si svolge la personalità del singolo e richiede l'adempimento dei doveri di solidarietà politica, economica e sociale - e l'art. 3 che impone di rimuovere gli ostacoli di ordine economico e sociale che limitano di fatto la libertà e l'uguaglianza dei cittadini.

**ISCRIVITI ALLA NEWSLETTER DI CERTI DIRITTI >****[  
http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**