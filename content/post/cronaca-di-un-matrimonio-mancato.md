---
title: 'CRONACA DI UN MATRIMONIO MANCATO'
date: Thu, 25 Jun 2009 10:59:04 +0000
draft: false
tags: [Comunicati stampa]
---

Carmelo e Giuseppe sono una coppia da 15 anni.  
Hanno celebrato questo anniversario poche settimane fa, in due modi: riunendo le famiglie e gli amici attorno a loro, per festeggiare insieme, e aderendo all'iniziativa di [Affermazione civile](index.php?option=com_content&view=article&id=41&Itemid=72) promossa da **Certi Diritti** e da **Rete Lenford**.  
Entrambi si sono iscritti a Certi Diritti, facendo proprie le nostre battaglie e contribuendo fattivamente a costruire con noi un altro possibile.  
Giuseppe ha condiviso con noi quanto ha scritto dopo essersi recato in comune a chiedere la pubblicazione degli atti di matrimonio insieme a Carmelo. Pubblichiamo qui di seguito il suo testo.  
  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
Cronaca di un matrimonio mancato  
  
Alla fine stringo un pezzo di carta con un timbro del comune mentre Carmelo mi riprende con la telecamera dicendo che non devo essere malinconico. Sì, perché abbiamo appena presentato la domanda e, per noi due, è il giorno del nostro matrimonio, quel pezzo di carta è il nostro certificato di unione davanti allo stato italiano. Gli amici, oramai, dicono che ci siamo sposati. Io ho provato molto tristezza per ciò che siamo stati costretti a subire.  
Ho sentito sulla mia pelle - anche lì, dentro quelle mura in cui la legge diventa materiale e dovrebbe essere uguale per tutti - la discriminazione. Piccoli aghi invisibili che si sono infilati silenziosi, apparentemente innocui, dentro il mio corpo. Gli sguardi degli impiegati, la perplessità dei loro occhi, i gesti impacciati, le mani intrecciate, una piega della bocca. Ho registrato tutto e anziché provare gioia non riesco a scrollarmi di dosso l’amarezza.  
Il primo è l’uomo - un tipo magro con la faccia ricoperta da pelle spessa e rigata - che dietro un vetro mi guarda perplesso quando gli chiedo dove posso presentare la domanda di matrimonio. Forse è lui poco credibile come personaggio, messo lì per dare informazioni che non conosce. Dopo aver intimato a Carmelo di spegnere la telecamera, mi dice di andare al primo piano. Quale stanza? La trova scritta nel tabellone alla fine delle scale, risponde già guardando verso la donna alle mie spalle.  
Dopo le scale, cerchiamo la stanza numero 11 ma dopo aver svoltato un paio di volte, lungo corridoi tutti uguali su cui si aprivano porte di legno scuro, ci rendiamo conto che le informazioni scritte non sono esatte. Allora domandiamo a un impiegato che dopo averci guardato dubbioso, fa cenno di seguirlo. Poi si ferma di colpo e domanda chi deve fare domanda di matrimonio, improvvisamente consapevole di cosa gli stiamo chiedendo.  
“Noi due”, rispondiamo in coro.  
“Eh, no! Non è possibile, - ritorna indietro, senza neanche farci entrare nell’ufficio - la legge lo vieta”.  
“Siete obbligati ad accettare la domanda”, insisto.  
L’uomo, in camicia bianca larga che gli nasconde la pancia prominente da quarantenne sedentario, inizia ad alterarsi. Io alzo la voce, arrabbiato per non essere neanche ammesso a riempire il modulo. Ci accompagna nell’ufficio del dirigente, le segretarie entrano subito in agitazione. La vita lenta di un ufficio pubblico alterata da due uomini che vogliono sposarsi a dispetto della legge.  
“Non c’è il dirigente, dovete aspettare”, una donna con una camicia a fiori e il seno prosperoso si alza di scatto dalla sedia con i braccioli traballanti, la voce le si incrina; “Restate nel corridoio”.  
Da una stanza attigua entra un altro impiegato che guarda perplesso la scena senza dire nulla. Poi allargando le braccia, a soccorso della collega, che è rimasta in piedi intrecciando le dita grassocce, dice: “Non sappiamo quando arriva il dirigente, ci sono diversi uffici sparsi della città”.  
“Chiamiamo i carabinieri allora”, sbotta Carmelo prendendo il cellulare.  
“Non perdiamo la calma - l’uomo cerca di placare la situazione – cerchiamo di rintracciare un altro dirigente”. Una ragazza, rimasta fino a quel momento seduta a bocca serrata, si alza e come un fulmine esce dalla stanza.  
Restiamo in piedi - con altri impiegati che entrano nell’ufficio, si guardano attorno preoccupati dal trambusto e poi spariscono – sorvegliati a vista dalla donna prosperosa.  
Tutto finisce in fretta, con il dirigente che arriva, prima accalorato poi più disponibile, vedendoci determinati e pronti a chiamare l’avvocato. Scrivere la domanda in un foglio bianco, è la soluzione che propone e accettata dall’avvocato: non abbiamo nemmeno la possibilità di compilare un modulo del comune, tanto la nostra richiesta è assurda e priva di fondamento. “Comprendo le vostre necessità ma la legge non mi permette di fare altro”, sono le parole di quell’uomo che per un attimo mostra empatia per la nostra richiesta.  
E alla fine ci ritroviamo con la ricevuta del protocollo, un pezzo di carta che nulla rappresenta se non la voglia di affermare i nostri diritti, magra consolazione per un matrimonio che forse mai si farà.  
Giuseppe Di Grazia