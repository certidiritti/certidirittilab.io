---
title: 'SOLIDARIETA'' ALLO STUDENTE GAY AGGREDITO ALL''UNIVERSITA'' DI MILANO'
date: Wed, 17 Mar 2010 17:26:44 +0000
draft: false
tags: [Comunicati stampa]
---

**I Giovani dell’Associazione Radicale Certi Diritti esprimONO piena solidarietà allo studente del collettivo "gay-statale" dell'Università degli Studi di Milano, aggredito mentre che promuovevano una rassegna di cinema gay.**

"Un'estate calda è passata, una campagna pubblicitaria del Ministero delle Pari Opportunità è scivolata nel silenzio più totale e ancora la paura del "diverso" è rimasta. Televisione, giornali, mass media in genere hanno abolito questo tema. Noi sappiamo, invece, che ci sono giovani che soffrono e reclamano una vita da uguali, ma nel rispetto della propria diversità. Colpevoli di questo clima sono anche le Istituzioni, che continuano a considerare le persone lesbiche, omosessuali e transessuali cittadini di serie B, non meritevoli di una legge che in tutta Europa riconosce l'uguaglianza dei diritti.

Noi di Certi diritti valutiamo questo episodio come un segnale, una spia ancora accesa, il monito di un'emergenza che in questo Paese c'è da molti anni, ovvero l'intolleranza, l'omofobia e la transfobia. Per questo continueremo a non abbassare la guardia".

**Matteo Di Grande**

**Coordinamento "Giovani per i Diritti"**

**Associazione Radicale Certi Diritti**