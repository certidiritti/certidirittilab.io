---
title: 'URGENTE: LETTERA - APPELLO ALLE ASSOCIAZIONI LGBT SU PDL ANTIOMOFOBIA IN DISCUSSIONE ALLA CAMERA'
date: Sun, 04 Oct 2009 09:07:43 +0000
draft: false
tags: [Comunicati stampa]
---

DA ASSOCIAZIONE RADICALE CERTI DIRITTI ALLE ASSOCIAZIONI E AGLI ESPONENTI DEL MOVIMENTO LGBT ITALIANO

Urgente

Cari e care,

martedì 6 ottobre, alle ore 13, la Commissione Giustizia della Camera dei Deputati approverà il testo definitivo   contro l’omofobia che l’aula discuterà lunedì 12 e voterà martedì 13 ottobre. Come potete immaginare si tratta di una opportunità storica per il nostro Paese e, secondo noi, bisogna fare di tutto affinchè la proposta che verrà approvata dal Parlamento sia quella migliore per combattere nel modo più efficace omofobia e transfobia. Abbiamo quindi poche ore per tentare di contribuire a questo obiettivo, riconoscendo il grande lavoro che la deputata del Pd Paola Concia ha fatto e sta facendo per convincere la maggioranza ad inserire la proposta nell’agenda dei lavori di Commissione e quindi dell’aula.

Abbiamo predisposto con Rita Bernardini alcuni emendamenti che verranno anche firmati dagli altri deputati radicali pd, che vedremo anche con Paola, da presentare in Commissione e in Aula che speriamo anche altri firmino. Si tratta di proposte che tendono a migliorare, per quanto possibile, il lavoro finora svolto e portare a casa un risultato che si avvicini a quanto tutti noi ci aspettiamo ed il Paese necessita.

Questo è il senso delle nostre proposte, e vi preghiamo di leggerle e farci sapere cosa ne pensate, magari proponendo ulteriori modifiche ed eventuali azioni politiche a sostegno della proposta nel suo insieme, nel minor tempo possibile.

Come vedrete il primo emendamento è  più radicale rispetto ai successivi, ed ovviamente se  passasse gli altri decadrebbero. Rimarrebbe invece fissa la proposta dell’emendamento n. 6 perché ci sembra necessario predisporre iniziative per la prevenzione, informazione, contrasto e assistenza delle vittime.

La nostra proposta non vuole indurre alcuna aspettativa rispetto al risultato finale, che dipende come capirete facilmente, dal combinato di molti fattori, non tutti in relazione all’oggetto del provvedimento. Ma crediamo che si debba comunque tentare di migliorare il provvedimento, nell’interesse di tutti e di tutte.

Di seguito troverete il testo base votato venerdì scorso dalla Commissione Giustizia e a seguire gli emendamenti da noi proposti.

**Facciamo appello a ognuno, alle associazioni e alle varie realtà del movimento lgbt italiano affinchè i membri della Commissione Giustizia della Camera vengano sollecitati, via fax o via e-mail sulla necessità di migliorare il testo base. Il centralino della Camera dei deputati è 06-67601 (dove si può anche chiedere il numero di fax dei deputati). Al seguente link trovate i membri della Commissione Giustizia:** **[http://www.camera.it/organiparlamentarism/10085/242/4408/4482/commissionepermanentexml.asp](http://www.camera.it/organiparlamentarism/10085/242/4408/4482/commissionepermanentexml.asp)**

Grazie per la vostra attenzione e per il contributo che vorrete dare a questo importante obiettivo,

  

Sergio Rovasio

Enzo Cucco

Ottavio Marzocchi

**Associazione Radicale Certi Diritti**

[**www.certidiritti.it**](http://www.certidiritti.it/)

**Disposizioni in materia di reati commessi per finalità di discriminazione o di odio fondati sull'orientamento sessuale o sull'identità di genere C. 1658 Concia e C. 1882 Di Pietro.**

**TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

INTRODUZIONE NEL CODICE PENALE DELLA CIRCOSTANZA AGGRAVANTE INERENTE ALL'ORIENTAMENTO O ALLA DISCRIMINAZIONE SESSUALE

Art. 1.

1\. All'articolo 61, comma 1, del codice penale, dopo il numero 11-_ter_), è aggiunto il seguente:  
«11-_quater_) l'avere, nei delitti non colposi contro la vita e l'incolumità individuale, contro la personalità individuale, contro la libertà personale e contro la libertà morale, commesso il fatto per finalità inerenti all'orientamento o alla discriminazione sessuale della persona offesa dal reato».

Emendamento 1

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**ART. 1**

L’art. 1 è sostituito dal seguente:

“Art. 1-

1\. Dopo l’art. 1 della Legge n. 122/1993 è aggiunto il seguente:

art. 1-bis (_Discriminazione, odio o violenza per motivi connessi all’identità di genere, all’orientamento sessuale, all’età ed alla condizione di disabilità_)

Le previsioni di cui alla presente legge si applicano anche nei casi di atti di discriminazione, odio o violenza per motivi connessi all’identità di genere, all’orientamento sessuale, all’età ed alla condizione di disabilità della persona vittima del reato.

2\. All’art. 3, comma 1, Legge n. 122 del 1993, dopo la parola: “religioso”, sono aggiunte le seguenti: “o basato su orientamento sessuale, identità di genere, età o condizione di disabilità”

**Emendamento 2**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**ART. 1**

L’art. 1 è sostituito dal seguente:

“Art. 1

All’art. 61, comma 1, del codice penale, dopo il numero 11-ter) è aggiunto il seguente:

11-quater) l’aver commesso il fatto per motivi connessi a discriminazione, odio o violenza connessi all’identità di genere, all’orientamento sessuale ed alla condizione di disabilità della persona vittima del reato”

**Emendamento 3**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**ART. 1**

All’art. 1 le parole “nei delitti non colposi contro la vita e l’incolumità individuale, contro la personalità individuale, contro la libertà personale e contro la libertà morale” sono soppresse.

**Emendamento 4**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**ART. 1**

All’art. 1 sostituire le parole: “per finalità all’orientamento o alla discriminazione sessuale della persona offesa del reato”, con le seguenti: “per motivi connessi all’orientamento sessuale o all’identità di genere della persona vittima del reato. La circostanza aggravante si realizza quando il reato è preceduto, accompagnato o seguito da atti o parole che ledano l’onore della persona vittima del reato o di gruppi di persone di cui fa parte, a ragione del suo orientamento sessuale o identità di genere, vera o presunta”.

**Emendamento 5**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**ART. 1**

All’art. 1 sostituire le parole: “per finalità all’orientamento o alla discriminazione sessuale della persona offesa del reato”, con le seguenti: “per motivi connessi all’orientamento sessuale o all’identità di genere della persona vittima del reato”.

**Emendamento 6**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**ART. 1**

Dopo l’art. 1, è aggiunto il seguente:

“Art. 1-bis .

1\. Il Governo entro novanta giorni dall’entrata in vigore della presente legge, approva, sentite le Commissioni parlamentari competenti, un Piano triennale contro le discriminazioni, con riferimento a quanto previsto dall’art. 13 del Trattato dell’Unione, anche in coordinamento con le attività previste ai sensi delle Direttive 2000/43/EC e 2000/78/EC già recepite dall’ordinamento italiano.

2\. Il Piano deve prevedere obiettivi, risorse e metodi di valutazione per il monitoraggio, la prevenzione, il contrasto e l’assistenza alle vittime di discriminazione o atti di violenza connessi all’identità di genere, all’orientamento sessuale, alla religione o all’origine etnica della persona vittima del reato o della discriminazione, con particolare riferimento a specifiche campagne di comunicazione sociale ed iniziative di educazione sessuale nelle scuole e presso l’associazionismo giovanile.

3\. Il Piano deve inoltre prevedere la trasformazione dell’UNAR in Agenzia Nazionale contro le discriminazioni, che in autonomia dal Governo e con l’assegnazione alla stessa di adeguate risorse, diventi uno dei soggetti attuatori del Piano nazionale”.