---
title: 'La Vice Presidente della Commissione Europea risponde a intergruppo Lgbt del PE su omofobia di Stato in Camerun e Nigeria'
date: Sat, 21 Jan 2012 09:25:59 +0000
draft: false
tags: [Africa]
---

A dicembre diverse associazioni, tra cui l'associazione radicale Certi Diritti, aveva scritto alla vicepresidente della Commissione Europea Catherine Ashtion sulla grave situazione dei diritti civili e umani delle persone LGBT in Nigeria e Camerun.

Roma - Bruxelles, 21 gennaio 2012  
   
Comunicato Stampa dell'Associaizone Radicale Certi Diritti  
   
Lo scorso dicembre, su richiesta di alcune associazioni tra cui l'Associazione Radicale Certi Diritti, un gruppo di Europarlamentari, aderenti all’intergruppo sui diritti LGBT, ha scritto alla Vicepresidente della Commissione Europea sulla grave situazione dei diritti civili e umani delle persone LGBT in Nigeria e Camerun.  
   
In questi giorni è stata resa pubblica la risposta, arrivata il 20 dicembre scorso, nella quale la Vice Presidente della Commissione assicura che per quanto riguarda la Nigeria vi è una forte preoccupazione dell’UE nei confronti di una proposta di legge volta, tra l’altro, a criminalizzare chiunque sostenga organizzazioni o manifestazioni LGBT e che tale preoccupazione sarà manifestata all’incontro ministeriale UE-Nigeria che si terrà entro gennaio 2012.  
   
Riguardo il Camerun, Catherine Ashton ricorda che tutti i capi di missione dell'Unione Europea hanno espresso congiuntamente la loro preoccupazione per la sentenza che ha condannato a 5 anni di prigione tre uomini per aver avuto rapporti sessuali con persone dello stesso sesso. L’Unione Europea, grazie all’art. 8 dell’Accordo Cotonou, sta facendo pressioni sul governo per una riforma del codice penale che includa la decriminalizzazione dei rapporti sessuali tra adulti consenzienti dello stesso sesso. Nel dicembre 2010 ha inoltre finanziato un progetto finalizzato all’assistenza delle persone LGBT incarcerate o sotto processo oltre che per migliorare l’ambiente legale e anche sanitario del paese.  
   
La Vicepresidente della Commissione assicura infine di proseguire negli sforzi per il pieno riconoscimento dei diritti umani delle persone LGBT, pur considerando con grande attenzione ogni tipo di azione pubblica per evitare un’ulteriore aggravarsi di posizioni estremiste omofobe.