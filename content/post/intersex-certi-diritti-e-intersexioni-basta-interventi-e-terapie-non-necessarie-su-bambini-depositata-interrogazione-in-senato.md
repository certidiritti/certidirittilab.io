---
title: 'INTERSEX. CERTI DIRITTI E INTERSEXIONI: BASTA INTERVENTI E TERAPIE NON NECESSARIE SU BAMBINI. DEPOSITATA INTERROGAZIONE IN SENATO.'
date: Thu, 13 Oct 2016 21:26:10 +0000
draft: false
tags: [Intersex]
---

[![intersex-awareness-day](http://www.certidiritti.org/wp-content/uploads/2016/10/intersex-awareness-day-300x201.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/10/intersex-awareness-day.jpg) Il recente caso di Palermo di un bambino che ha subito un intervento chirurgico non necessario  per ragioni di salute rende ancora più urgente l’appello che da tempo Intersexioni e l’Associazione radicale Certi Diritti hanno lanciato: interrompere immediatamente la pratica di operare chirurgicamente o effettuare terapie non salva vita sui bambini e le bambine intersex, ovvero con anatomia sessuale atipica. Intanto la questione sbarca anche in Parlamento grazie a un'[interrogazione](http://www.senato.it/japp/bgt/showdoc/showText?tipodoc=Sindisp&leg=17&id=991855) del senatore Pd Sergio Lo Giudice - iscritto a Certi Diritti - che ha accolto nel suo documento i nostri quesiti ai ministeri della Salute e degli Affari Regionali. Tra le richieste di chiarimento sottolineiamo quelle:

*   di avviare un'azione di monitoraggio e raccolta dati sui casi di interessualità in Italia;
*   d'impegnarsi in una campagna di informazione rivolta alle famiglie;
*   di aprire al più presto un tavolo con le regioni per valutare l'aggiornamento dei percorsi diagnostici, terapeutici e assistenziali o l'adozione di linee guida specifiche per il trattamento di minori con variazioni nello sviluppo sessuale.

In Italia, come denunciato dal Comitato ONU sui Diritti delle Persone con Disabilità, continuano ad essere effettuate pratiche già riconosciute a livello scientifico non solo come inutili per la salute ma anche dannose in modo irreversibile. Su queste stesse posizioni si erano espresse la [FRA](http://fra.europa.eu/en/publication/2015/fundamental-rights-situation-intersex-people) (Agenzia dell'Unione Europea per i Diritti Fondamentali), il [Consiglio d’Europa](http://www.certidiritti.org/2015/05/15/consiglio-deuropa-e-fra-europa-e-italia-devono-proteggere-le-persone-intersex/) e l’[OMS](http://www.who.int/genomics/gender/en/index1.html#Gender%20Assignment%20of%20Intersex%20Infants%20and%20Children) (Organizzazione Mondiale della Salute). Per ulteriori informazioni:

*   [La precedente posizione dell’Associazione radicale Certi Diritti](http://www.certidiritti.org/2011/11/23/mozione-particolare-sui-diritti-delle-persone-intersex-%20approvata-dal-viii-congresso/).
*   [Il sito di Intersexioni](http://www.intersexioni.it/).
*   [Il documento del Comitato delle Nazioni Unite sui Diritti delle Persone disabili (p. 5-6, par. 45-46)](http://intersex.shadowreport.org/public/italy_IT_CRPD_concl-obs_CRPD_C_ITA_CO_1_25069_E.pdf).

Roma, 14 ottobre 2016