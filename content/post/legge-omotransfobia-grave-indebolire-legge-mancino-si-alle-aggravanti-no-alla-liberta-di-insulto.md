---
title: 'Legge omotransfobia: grave indebolire legge mancino. Sì alle  aggravanti, no alla libertà di insulto'
date: Fri, 20 Sep 2013 10:14:45 +0000
draft: false
tags: [Politica]
---

Comunicato stampa congiunto

16 settembre 2013

In vista del dibattito alla Camera sulla legge contro l'omotransfobia, i portavoce delle associazioni nazionali lgbt rivolgono un appello ai deputati del Partito Democratico: "Il Pd - spiegano - ha presentato un proprio emendamento che incredibilmente contiene da una parte le aggravanti (escluse dal testo licenziato dalla Commissione Giustizia, ndr) e dall’altra un pericoloso cavillo sulla libertà di espressione, pleonastico all'apparenza ma che rischia di complicare l'applicazione di una legge già di per sé poco efficace a sanzionare crimini e incitazioni all'odio. Sulla legge Mancino - proseguono - non si sono mai sollevate questioni di libertà di opinione, tra l’altro ben tutelate dalla legislazione vigente. L'accusa di liberticidio rivolta alla legge Mancino è frutto di una campagna di mistificazione prodotta ad hoc dalla destra e da parte dei cattolici, proprio ora che si tratta di estendere l'efficacia di quella norma anche a tutela delle  persone gay, lesbiche e trans. La realtà, lo racconta la cronaca ogni giorno, è che quella legge già faticosamente riesce a colpire la violenza più esplicita. Ora tocca al Pd chiarire la vicenda - esortano i portavoce - ritirando quell’emendamento, per proporre un testo che estenda le aggravanti e che sia in linea con le indicazioni europee e la Costituzione italiana".

Rita De Santis presidente nazionale Agedo, Flavio Romani presidente nazionale Arcigay, Paola Brandolini presidente nazionale ArciLesbica, Enzo Cucco presidente nazionale Associazione Radicale Certi Diritti, Aurelio Mancuso presidente nazionale Equality Italia, Giuseppina La Delfa presidente nazionale Famiglie Arcobaleno, Porpora Marcasciano Presidente nazionale Mit