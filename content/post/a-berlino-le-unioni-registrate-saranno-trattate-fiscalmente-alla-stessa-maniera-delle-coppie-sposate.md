---
title: 'A Berlino le unioni registrate saranno trattate fiscalmente alla stessa maniera delle coppie sposate'
date: Tue, 03 Apr 2012 08:18:10 +0000
draft: false
tags: [Europa]
---

Berlino-Roma, 3 aprile 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

L'Amministrazione Fiscale della città di Berlino ha deciso che sul piano economico-fiscale le coppie registrate, per ciò che riguarda gli aspetti fiscali dei redditi, verranno trattate come le coppie sposate. Infatti la legge tedesca sulla convivenza registrata non equipara a tutti gli effetti la convivenza registrata al matrimonio.

Le autorità tributarie federali calcolano l'Irpef a seconda se si è single o sposati. In base all'”Ehegattensplitting”, le coppie sposate hanno la possibilità di pagare meno imposte perchè il reddito della coppia viene sommato e poi diviso per due. Conseguenza: chi è sposato - sia che entrambi i coniugi lavorino, sia che lavori uno solo di loro - risparmia ogni anno diverse migliaia di euro di Irpef. Finora ciò valeva solo per le coppie sposate, e quindi eterosessuali, mentre da oggi, almeno a Berlino, questo varrà anche per le coppie dello stesso sesso.

Da ieri, 2 aprile 2012, la città-Stato di Berlino pone finalmente e coraggiosamente fine alla disparità di trattamento tra le unioni civili e le coppie sposate, almeno per quello che riguarda le imposte sul reddito e la scelta della fascia d'imposta sul reddito, senza aver voluto aspettare la decisione finale della Corte Costituzionale federale prevista per il 2013.

Sarà finalmente possibile proprio ciò che la coalizione liberal-democristiana non è riuscita a fare in due anni di governo. Nel governo federale non c'è ancora consenso sulle pari opportunità tributarie e l' eventualità che la Corte Costituzionale, nel 2013, decida di revocare questo diritto conquistato dalle coppie omosessuali è giudicata estremamente remota.

Tuttavia la soluzione più semplice e più auspicabile resta comunque la completa apertura dell'istituto del matrimonio anche alle coppie dello stesso sesso.

di Davide Miraglia