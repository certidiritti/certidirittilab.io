---
title: 'Aperta a Strasburgo conferenza internazionale sul mancato riconsocimento unioni civili e matrimonio gay all''interno dei paesi europei'
date: Fri, 18 Nov 2011 11:43:10 +0000
draft: false
tags: [Europa]
---

L'Associazione Radicale Certi Diritti partecipa alla Conferenza internazionale con una sua delegazione guidata da Yuri Guaiana, che illustrerà la campagna “Affermazione Civile 2.0” concentrandosi su uno dei casi che si stanno seguendo concernente le pensioni di reversibilità.

  
Comunicato Stampa dell'Associazione Radicale Certi Diritti  
Roma, 18 novembre 2011

Sono iniziati stamane a Strasburgo i lavori della Conferenza internazionale intitolata "La mancanza del riconoscimento reciproco di unioni civili e matrimonio omosessuale tra gli stati membri dell'Unione Europea e del Consiglio d'Europa: un ostacolo alla libertà di movimento delle persone?".

La Conferenza, che durerà due giorni, è organizzata dalla Fèdération Nationale de l'Autre Circle e ha il Patrocinio del Segretario Generale del Consiglio d'Europa. Parteciperanno relatori provenienti da molti paesi europei . Tra gli altri, sono presenti il Direttore per i diritti fondamentali e le politiche anti-discriminazione al Consiglio d'Europa, Ralf René Weingartner, il rappresentante dell'unità sulle tematiche LGBT del Consiglio d'Europa, Dag Robin Simonsen, il co-presidente dell'intergruppo LGBT del Parlamento europeo Michael Cashman, il program manager dell'Agenzia Europea per i diritti fondamentali, Matteo Bonini Baraldi.

Tra i temi trattati si segnalano: il riconoscimento delle unioni omosessuali in Europa, il ruolo della Corte Europea dei Diritti dell'Uomo, nonchè focus su alcune situazioni nazionali sull’Italia, la Spagna, l'Ungheria, l'Irlanda, l'Olanda, la Finlandia, la Francia e la Polonia.

L'Associazione Radicale Certi Diritti partecipa alla Conferenza internazionale con una sua delegazione guidata da Yuri Guaiana. Al termine, domenica 20 novembre, si svolgerà a Strasburgo un seminario dell’ILGA sulle cause pilota per combattere le discriminazioni contro le famiglie omosessuali in Europa. In questo contesto Certi Diritti illustrerà la campagna “Affermazione Civile 2.0” concentrandosi su uno dei casi che si stanno seguendo concernente le pensioni di reversibilità.