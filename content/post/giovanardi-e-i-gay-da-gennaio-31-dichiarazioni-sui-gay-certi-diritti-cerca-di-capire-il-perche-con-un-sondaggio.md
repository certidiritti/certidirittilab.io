---
title: 'Giovanardi e i gay: da gennaio 31 dichiarazioni sui gay. Certi Diritti cerca di capire il perchè con un sondaggio'
date: Sat, 07 Jul 2012 18:12:27 +0000
draft: false
tags: [Politica]
---

L'Associazione Radicale Certi Diritti ha deciso così di lanciare un sondaggio per capire quale può essere il perchè e invita tutti coloro che vogliono dare una risposta, tra quelle sotto elencate, a inviarle al seguente indirizzo e-mail: [info@certidiritti.it](mailto:%3Ca%20href=)

Roma, 7 luglio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Era da un po’ di giorni che Giovanardi non faceva dichiarazioni sulle persone gay ed eravamo molto preoccupati del suo silenzio. Ora siamo più sereni e tranquilli per la sua esternazione di oggi  sul  fatto che i gay nell’esercito non devono fare coming out e che devono dormire in camere separate dagli eterosessuali se “praticano la loro omosessualità”. Il grande spessore politico di quest’uomo rimarrà negli annali della storia per la sua capacità di esternare esclusivamente su questo  tema.  Nel 2012 ha fatto almeno 30 dichiarazioni sui gay, record assoluto, più della nostra Associazione. Quale sarà il motivo di tanto interesse per il mondo gay? **L’Associazione Radicale Certi Diritti ha deciso così di lanciare un sondaggio per capire quale può essere il perchè e invita tutti coloro che vogliono dare una risposta, tra quelle sotto elencate, a inviarle al seguente indirizzo e-mail: [info@certidiritti.it](mailto:info@certidiritti.it)**

**a)** Durante il suo  Sottosegretariato alla famiglia sono aumentati i i divorzi, le convivenze tra persone dello stesso sesso e sono diminuiti i matrimoni, lui, per disperazione o per vendetta, fa solo questo tipo di dichiarazioni;

**b)** Cerca di distrarre l’attenzione sul crollo del Pdl in tutti i sondaggi esternando sui gay;

**c)** Quando era piccolo le suore lo facevano vestire da bambina (dichiarazione del fratello gemello, medico, a “La Zanzara – Radio 24” del 14 febbraio 2012) e ora cerca di buttarla in caciara;

**d)** Quando faceva il militare nessuno voleva dormire in camerata con lui  e ora cerca di darsi una spiegazione;

**e)** Altro…

**inviaci la tua risposta a info@certidiritti.it  
  
RISULTATI DEL SONDAGGIO:  
  
a) 8%  
b) 0%  
c) 32%  
d) 15%  
e) 45%  
**