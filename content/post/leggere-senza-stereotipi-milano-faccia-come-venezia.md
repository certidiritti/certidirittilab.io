---
title: '"Leggere senza stereotipi": Milano faccia come Venezia'
date: Sun, 09 Feb 2014 14:05:37 +0000
draft: false
tags: [Politica]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti.

Milano, 9 febbraio 2014

L'Associazione Radicale Certi Diritti ha scritto all'assessore all'Educazione e all'Istruzione del Comune di Milano, Francesco Cappelli, per chiedere che il Comune di Milano faccia proprio il progetto «Leggere senza stereotipi» per il quale nelle biblioteche di 36 asili nido e 18 scuole materne del Comune di Venezia verranno distribuite 49 favole per bambini che raccontano storie come tante altre, ma senza cadere negli stereotipi più classici. C'è una storia di due pinguini maschi che allevano un cucciolo, quella di un cane che vuol essere una ballerina, la storia di un bambino di 6 anni affetto dalla sindrome di down, amico di una bimba epilettica, quella di una famiglia allargata con il nuovo compagno della mamma, la storia di una coppia divorziata con il bimbo che si trova a vivere con un genitore solo e quelle di famiglie di migranti e di bimbi adottati.

Il progetto è stato molto apprezzato da llda Curti assessore alle politiche delle Pari opportunità della città di Torino e responsabile della segreteria nazionale della rete Re.A.Dy, della quale Milano fa parte.

Yuri Guaiana, segretario dell'Associazione Radicale Certi diritti, dichiara: «Nel 2012 l'Associazione Radicale Certi Diritti aveva già lanciato una sottoscrizione pubblica per donare il libro Piccolo Uovo a tutte le biblioteche rionali di Milano. Riteniamo che ora il Comune debba andare avanti su questa strada per valorizzare tutte le diversità e non solo quelle basate su orientamento sessuale e identità di genere adottando un approccio orizzontale e di mainstreaming sostenuto anche a livello europeo».