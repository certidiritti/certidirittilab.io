---
title: '16 febbraio a Roma Associazione Coscioni e Certi Diritti organizzano incontro sul tema ''Bambini, adolescenti e transessualità'''
date: Fri, 15 Feb 2013 12:34:01 +0000
draft: false
tags: [Transessualità]
---

Presentazione del volume di Simona Giordano "Children with Gender Identity Disorder"Sabato 16 febbraio, dalle 16 alle 18.

"Associazione radicale Certi Diritti" e "Associazione Luca Coscioni" invitano alla discussione pubblica sul tema:

"Bambini, adolescenti e transessualità"  
Presentazione del volume di Simona Giordano "Children with Gender Identity Disorder"

Sabato 16 febbraio, dalle 16 alle 18  
presso il Partito Radicale  
via di Torre Argentina 76, Roma - III piano

Intervengono:

\- Simona Giordano, Università di Manchester  
\- Tito Flagella, avvocato  
\- Luca Chianura, psicologo  
\- Daniela Pompili, Direttivo Movimento Italiano Transessuale

Moderano:  
Yuri Guaiana, Segretario dell'Associazione radicale Certi diritti  
Filomena Gallo, candidata per la "Lista Amnistia Giustizia Libertà"

Per informazioni: 06-68979286, [info@associazionelucacoscioni.it](mailto:info@associazionelucacoscioni.it)