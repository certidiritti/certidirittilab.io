---
title: 'IL SOTTOSEGRETARIO DI STATO VATICANO GIOVANARDI SI SCONCERTI MENO...'
date: Wed, 22 Apr 2009 13:42:23 +0000
draft: false
tags: [Comunicati stampa]
---

IL SOTTOSEGRETARIO DELLO STATO VATICANO CARLO GIOVANARDI SI  SCONCERTI MENO E SI STUDI LE LEGGI DEI PAESI MEMBRI DELL’UNIONE EUROPEA ANZICHE’ LANCIARE ANATEMI CONTRO IL TRIBUNALE DI VENEZIA.  
   
Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti: “Il Sottosegretario dello Stato Vaticano on. Carlo Giovanardi farebbe bene a sconcertarsi di meno su quanto deciso dal Tribunale di Venezia che ha rinviato alla Consulta il ricorso presentato da una coppia gay aderente alla campagna di Affermazione Civile dell’Associazione Radicale Certi Diritti. La coppia aveva chiesto al Comune di Venezia le pubblicazioni per il matrimonio e il Tribunale ha ritenuto fondate le obiezioni dell’Avvocato di Rete Lenford che evidenziavano la discriminazione di cui sono vittime le coppie gay in Italia. L’on. Giovanardi dovrebbe avere maggior rispetto per il Diritto. Definire “provocatorio ed eversivo” l’operato dei magistrati di Venezia è molto grave. Si faccia un giro in Europa a studiare le leggi sul matrimonio gay e le unioni civili e vedrà anche lui che l’Italia non potrà rimanere ancora per molto tempo succube dei voleri del Vaticano. Ci auguriamo che la Corte Costituzionale confermi quanto già evidenziato dal Tribunale di Venezia e dia indicazioni alla classe politica di legiferare nell'interesse dei cittadini anzichè di qualche monsignore integralista".