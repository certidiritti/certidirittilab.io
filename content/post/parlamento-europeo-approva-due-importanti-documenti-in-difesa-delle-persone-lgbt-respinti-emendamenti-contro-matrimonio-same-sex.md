---
title: 'Parlamento Europeo approva due importanti documenti in difesa delle persone Lgbt. Respinti emendamenti contro matrimonio same-sex'
date: Tue, 13 Mar 2012 20:52:30 +0000
draft: false
tags: [Matrimonio egualitario]
---

Il coniuge spagnolo dello stesso sesso sposato, sulla base della legge spagnola, ad un italiano potrà ereditare i suoi beni, nonostante l'Italia non preveda il matrimonio tra le persone dello stesso sesso.

Roma, 13 marzo 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Il Parlamento Europeo ha approvato oggi due relazioni che difendono i diritti delle persone LGBTI. La relazione Lechner sulla "giurisdizione, la legge, il riconoscimento e l'applicazione delle decisioni e degli strumenti relativi alla successione e la creazione di un Certificato Europeo di Successione".

La Relazione si  richiama alla Carta Europea dei Diritti Fondamentali ed in particolare si esprime sulla proibizione di tutte le discriminazioni, incluse quelle basate sull'orientamento sessuale: il coniuge spagnolo dello stesso sesso sposato, sulla base della legge spagnola, ad un italiano potrà ereditare i suoi beni, nonostante l'Italia non preveda il matrimonio tra le persone dello stesso sesso.

Il Parlamento Europeo ha inoltre approvato la relazione della deputata Sophie In't Veld sull'eguaglianza tra le donne e gli uomini nella UE per il 2011. La relazione, nonostante il tentativo fallito da parte del PPE di cancellare i paragrafi più importanti sui diritti LGBTI, chiede alla Commissione ed agli Stati membri di "elaborare proposte per il mutuo riconoscimento delle unioni civili e delle famiglie dello stesso sesso".

Il documento approvato critica le definizioni restrittive adottate da in alcuni Stati membri sulla famiglia, ricordando il divieto di discriminazione previsto dalla Carta dei Diritti Fondamentali e chiede al Consiglio Europeo di sbloccare la direttiva anti-discriminazione ferma ormai da oltre tre anni. Il testo chiede  alla Commissione di lanciare una Roadmap per i diritti delle persone LGBTI e richiama il fatto che le famiglie nella UE sono diverse e composite, includendo anche le coppie dello stesso sesso, e che meritano uguale protezione all’interno dell’Unione Europea.

Il passaggio piu' nettamente a favore dei matrimoni gay e' contenuto nel paragrafo 7 della Relazione, che un emendamento di alcuni deputati del Ppe, respinto dall'aula, voleva cancellare.

L’Associazione Radicale Certi Diritti ringrazia i membri del Parlamento europeo per l'approvazione delle due relazioni che tutelano i diritti delle persone LGBTI, e chiede al Governo ed al Parlamento italiano di adottare leggi e politiche in questo senso, al di là degli Alfano e delle Bindi di turno, specularmente e vaticanamente conservatori rispetto alle tendenze più moderne che si affermano sempre più in Europa sul diritto di famiglia.

[ISCRIVITI ALLA NEWSLETTER >](newsletter/newsletter)