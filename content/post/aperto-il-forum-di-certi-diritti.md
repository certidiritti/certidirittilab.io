---
title: 'APERTO IL FORUM DI CERTI DIRITTI'
date: Wed, 18 Jun 2008 15:59:23 +0000
draft: false
tags: [Comunicati stampa]
---

**i****l sito [certidiritti.it](http://certidiritti.it/) sta crescendo ancora. Ora abbiamo creato all'interno di esso un forum dove potrai partecipare anche te in prima persona.  
Il forum è raggiungibile a [questo indirizzo](index.php?option=com_fireboard&Itemid=103)  
Visitatelo e fate crescere anche voi il VOSTRO sito scrivendoci i vostri commenti e facendoci parte dei vostri pareri.**  
  
  
  
[](http://certidiritti.it/)