---
title: 'X Congresso dell''Associazione Radicale Certi Diritti'
date: Fri, 14 Oct 2016 17:44:55 +0000
draft: false
---

[![14639673_10154402322165973_2231197951067742360_n](http://www.certidiritti.org/wp-content/uploads/2016/10/14639673_10154402322165973_2231197951067742360_n.png)](http://www.certidiritti.org/14639673_10154402322165973_2231197951067742360_n/) [](http://www.certidiritti.org/14639673_10154402322165973_2231197951067742360_n/) È [convocato](http://www.certidiritti.org/2016/10/08/x-congresso-dellassociazione-radicale-certi-diritti/) per il **18, 19 e 20 novembre 2016 a Torino** il X Congresso dell’Associazione Radicale Certi Diritti, il massimo organo deliberativo dell’Associazione, di cui stabilisce annualmente gli obiettivi d’azione. [![logo-citta-di-torino](http://www.certidiritti.org/wp-content/uploads/2016/10/logo-citta-di-torino-300x92.jpg)](http://www.certidiritti.org/x-congresso/logo-citta-di-torino/)       [![logoj](http://www.certidiritti.org/wp-content/uploads/2016/10/logoj-300x91.jpg)](http://www.certidiritti.org/x-congresso/logoj/) [Dona ora! Aiutaci a sostenere le spese congressuali >>>>](http://www.certidiritti.org/donazioni/)  

### IL PROGRAMMA

**18 novembre 2016** Sala delle Colonne del Municipio Via Milano 1

  ore 15,00 - Saluto del sen. Sergio **Lo Giudice**. ore 15,30 - _45 anni italiani. La storia del Fuori! e del Movimento di Liberazione_, tavola rotonda con giornalisti e rappresentanti delle associazioni lgbti italiane. Maurizio **Molinari** (La Stampa), Alessio **Falconio** (Radio Radicale), Angelo **Pezzana** (Fondatore del Fuori!) ne parlano con Gabriele **Piazzoni** di Arcigay, Silvia **Casassa** di Famiglie Arcobaleno, Antonio **Trinchieri** di Globe-MAE, Rosario **Coco**, ANDDOS, e Rita **De Santis** di AGEDO. Modera Enzo **Cucco**. ore 18,00 - Proiezione di un video in ricordo di Marco **Pannella**. ore 18:30 - Direttivo  

**19 novembre 2016** Sala delle Colonne del Municipio Via Milano 1

  ore 9 - Apertura del Congresso e saluto dei rappresentanti delle istituzioni ore 9,30 - Relazione del **Segretario** ore 10,30 - Saluto di Riccardo **Magi**, segretario di Radicali Italiani, Michele **Capano**, tesoriere di Radicali Italiani, Filomena **Gallo**, segretario dell'Associazione Luca Coscioni, Marco **Cappato**, tesoriere dell'Associazione Luca Coscioni e Alessandro **Battaglia**, Coordinamento Torino Pride GLBT. ore 11,30 - _Sessione dedicata alle politiche anti-discriminatorie in ambito locale_ Marco **Giusta**, Assessore alle Pari Opportunità, Città di Torino Nicky **Cambridge**, Corporate Policy Team, Brighton and Hove City Council, in rappresentanza del Coordinamento LGBTI europeo delle amministrazioni locali - Rainbow Cities. ore 12 - _La liberazione degli affetti: verso il matrimonio egualitario_ Gabriella **Friso**, responsabile Affermazione Civile dell’Associazione Radicale Certi Diritti Marco **Ciurcina**, avvocato del Foro di Torino Luca **Imarisio, **docente di diritto comparato, Università di Torino ore 13 - Pausa pranzo ore 14:00 - _La GPA, una questione di autodeterminazione_ Intervento video di Rachelle **Myers-Nelson**, gestante californiana Elisa **Serafini**, blogger su Linkiesta e reporter economica per il quotidiano L'Opinione ore 14:20 - _La liberazione dei corpi: verso la decriminalizzazione del lavoro sessuale_ Pia **Covre**, membro del direttivo di Certi Diritti e presidente del Comitato per i Diritti Civili delle Prostitute Francesco **Mangiacapra**, lavoratore sessuale Clémence **Zamora-Cruz**, membro della Steering Committee di Transgender Europe Ore 15:10 - _I diritti umani delle persone intersex_ Introduce Michela **Balocchi**, Marie Curie research Fellow, Università di Verona Daniela **Truffer** e Markus **Bauer**, Cofondatori di StopIGM.org, Zwischengeschlecht.org (Svizzera) Alessandro **Comeni**, presidente onorario di Certi Diritti, cofondatore di intersexioni e di OII Europe Ore 16 - _I diritti umani delle persone LGBTI in Europa e nel mondo_ Monica **Cerutti**, assessora ai Diritti e alla Cooperazione internazionale, Regione Piemonte Fabrizio **Petri**, presidente del Comitato Interministeriale Diritti Umani Daniele **Viotti**, eurodeputato e vicepresidente dell'Intergruppo LGBTI al PE Mariano **Giustino**, redattore di Radio Radicale Ore 17:10 - _Il pinkwashing, un concetto da decostruire_ Giovanni **Quer**, assegnista di ricerca presso il Comper Center dell’Università di Haifa ore 18 - Partecipazione alla Trans Freedon March in occasione del TDoR  

**20 novembre 2016** presso la Fondazione Fuori! Via Santa Chiara 1, 2° piano

  ore 9 - Relazione del **Tesoriere** ore 9, 15 - Saluto di Antonio **Trinchieri** di Globe-MAE, Marco **Marazzi**, coordinatore nazionale dei membri individuali ALDE Party, Giulio **Ercolessi**, Umanisti europei, Patrizio **Gonnella** della CILD e Igor **Boni**, Associazione Radicale Adelaide Aglietta. ore 9,45 - Inizio del dibattito generale ore 13,00 - Pausa pranzo ore 15 - Ripresa del dibattito generale, votazione dei documenti congressuali ed elezione delle cariche

### HOTEL SUGGERITI, MA NON CONVENZIONATI

**Hotel Diplomatic**

Via Cernaia, 42 (a due passi dalla stazione di Porta Susa e a 25 minuti a piedi dalla Sala delle Colonne del Municipio)

Telefono: 011 561 2444

**Hotel Dock Milano**

Via Cernaia, 46 (a due passi dalla stazione di Porta Susa e a 25 minuti a piedi dalla Sala delle Colonne del Municipio)

Telefono: 011 562 2622