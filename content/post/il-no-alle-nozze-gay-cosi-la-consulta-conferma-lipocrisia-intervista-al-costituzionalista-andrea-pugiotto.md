---
title: 'Il no alle nozze gay. "Così la consulta conferma l''ipocrisia"- Intervista al Costituzionalista Andrea Pugiotto'
date: Mon, 19 Apr 2010 11:46:06 +0000
draft: false
tags: [Senza categoria]
---

di **Delia Vaccarello** su [**_L’Unità_**](http://cerca.unita.it/data/PDF0115/PDF0115/text2/fork/ref/101093xm.HTM?key=Delia+Vaccarello&first=1&orderby=1&f=fir), 19 aprile 2010

Il docente di diritto costituzionale all’ateneo di Ferrara, Andrea Pugiotto, valuta la recente sentenza: «Si nega l’esistenza attuale di una discriminazione a danno delle coppie omosessuali»   
Internet, aborto, fecondazione assistita sono sotto l’ombrello della Costituzione pur se non discussi   
alla Costituente. Le nozze gay invece restano fuori. La sentenza della Consulta conferma il paradigma eterosessuale del matrimonio aprendo molto debolmente alle unioni civili. Ne parliamo con Andrea Pugiotto, ordinario di Diritto costituzionale all’Ateneo di Ferrara: «È come dire all’omosessuale: ti accolgo per quel che sei a patto che tu non esprima quel che sei». 

  
**Professor Pugiotto come valuta la sentenza? **  
«Ad una prima (e disincantata) lettura,la sentenza delude le aspirazioni della comunità Lgbt. E di chi vive le discriminazioni di alcuni come problema di tutti».   
**  
Perché? **  
«La Corte individua un “nucleo” nell’articolo 29 della Costituzione inclusivo del paradigma eterosessuale nel matrimonio, secondo l’intento del Costituente “che intese riferirsi al matrimonio nel significato tradizionale di detto istituto”.È un argomento debolissimo. Altrove la Corte ha incorporato in Costituzione “fenomeni e problematiche non considerati in alcun modo quando fu emanata”: aborto, fecondazione assistita, integrazione comunitaria, televisione, internet stanno sotto l’ombrello costituzionale. Eppure non si desumono dai lavori dell’Assemblea costituente».   
  
**Sorprende il legame necessario tra nozze e figli? **  
«Sì, sorprende il recupero della “potenziale finalità procreativa del matrimonio”. Eppure le coppie omosessuali possono avere figli. Eppure si celebrano nozze eterosessuali prive naturalmente di figli: la coppia di anziani, la coppia sterile, le nozze in punto di morte, l’uomo che sposa la donna in menopausa».   
  
**Quali conseguenze derivano dalla lettura dell’articolo 29 della Costituzione? **  
«Dato il suo “nucleo”, una legge che introducesse le nozze omosessuali sarebbe incostituzionale. In Italia non si potrebbe fare ciò che è stato possibile, ad esempio, in Spagna e in Portogallo e che né la Convenzione europea dei diritti Umani (Cedu) né la Carta di Nizza vietano».   
**  
Come intendere allora il rinvio alla discrezionalità legislativa fatto dalla Corte? **  
«Va incapsulato nella motivazione della sentenza, che funge da bussola. È esclusa l’estensione del regime matrimoniale. Sono ammessi regimi parafamiliari, ma senza alcun monito a fare presto: tutto è rimesso “nei tempi” (oltre che nei modi e nei limiti) che il Parlamento sceglierà. Non c’è alcun obbligo costituzionale ad intervenire: la Corte nega infatti l’esistenza attuale di una discriminazione a danno delle coppie gay».   
**  
Ma la Consulta sottolinea che l’unione gay è una formazione sociale da riconoscere in base all’art. 2 della Costituzione. **  
«Ciò però non vuol dire che il legislatore deve intervenire. Ma che solo il legislatore (e non la Corte) può regolare diritti e doveri della coppia gay: da qui l’inammissibilità della questione».   
**  
C’è però, nella sentenza, il riconoscimento del fondamento costituzionale delle unioni omosessuali. **  
«Scritto sotto dettatura: è la natura pluralistica della Costituzione e l’adesione all’Ue e alla Cedu ad imporlo. Ma l’effettività di quel riconoscimento è abbandonata alla “piena” discrezionalità del legislatore. La Corte potrà intervenire di rincalzo, “in particolari ipotesi” di differenze irragionevoli tra famiglia matrimoniale e unione omosessuale: il che conferma la loro irriducibilità ad un’identica disciplina».