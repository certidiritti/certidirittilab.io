---
title: 'Matrimoni gay, una sentenza nel vuoto'
date: Mon, 19 Apr 2010 10:09:27 +0000
draft: false
tags: [Senza categoria]
---

di **Ezio Menzione** su [_**Il Manifesto**_](http://www.ilmanifesto.it/), 16 Aprile 2010

A tambur battente, all'indomani della notizia che la Corte Costituzionale aveva rigettato la richiesta di due giudici (uno veneziano e uno trentino) di dichiarare costituzionalmente illegittima la limitazione dell'istituto del matrimonio alle sole coppie eterosessuali, è uscita la motivazione della sentenza stessa, e così possiamo avere un'idea più chiara di come abbia ragionato la Corte.

Ci sono luci ed ombre, come quasi sempre in queste supreme decisioni, ma il punto chiave del ragionamento è quello che ruota intorno all'art. 2 della Costituzione, la norma che "riconosce e garantisce i diritti inviolabili dell'uomo, sia come singolo sia nelle formazioni sociali ove si svolge la sua personalità" e richiede "l'adempimento dei doveri inderogabili di solidarietà politica, economica e sociale". Dato per scontato che l'unione fra due persone costituisce una formazione sociale, forse la più importante e fondante della nostra società, argomentavano i giudici che si erano rivolti alla Corte, quella fra due persone dello stesso sesso non può rimanere senza riconoscimento e senza tutela.

La Corte aderisce a questa impostazione: "Per formazione sociale deve intendersi ogni forma di comunità, semplice o complessa, idonea a consentire e favorire il libero sviluppo della persona nella vita di relazione, nel contesto della valorizzazione del modello pluralistico. In tale nozione è da annoverare anche l'unione omosessuale, intesa come stabile convivenza fra due persone dello stesso sesso, cui spetta il diritto fondamentale di vivere liberamente una condizione di coppia, ottenendone – nei temi, nei modi e nei limiti stabiliti dalla legge – il riconoscimento giuridico con i connessi diritti e doveri". Però, aggiunge subito dopo la Corte: "Si deve escludere che l'aspirazione a tale riconoscimento – che necessariamente postula una disciplina di carattere generale, finalizzata a regolare diritti e doveri dei componenti della coppia – possa essere realizzata soltanto attraverso una equiparazione delle unioni omosessuali al matrimonio".

Il discorso è chiaro: alla coppia omosessuale è dovuto riconoscimento e tutela, ma essi non necessariamente passano attraverso l'istituto del matrimonio. Non è poco: è la prima volta che un simile approdo viene fatto proprio da un consesso così autorevole (il massimo, all'interno della magistratura: il giudice stesso delle leggi). D'ora in poi nessuno potrà più dire che le unioni omosessuali sono legalmente (e quindi socialmente) irrilevanti: esse fondano la nostra società tanto quanto quelle eterosessuali. Né il legislatore potrà più ignorare tale dato.

La sentenza è più debole (anche se, a dir la verità, scontata) quando passa ad esaminare il matrimonio come istituto riservato a coppie di sesso diverso. Gli argomenti sembrano essere tre: la tradizione, il fatto che il matrimonio è riconosciuto in funzione della tutela dei figli, il fatto che in occidente, e soprattutto in Europa, i trattati riconoscono ai singoli stati il diritto di legiferare in materia. La tradizione è argomento fragile: ognuno si costruisce la tradizione che gli interessa. La filiazione è argomento destinato a crollare, con l'incalzare della filiazione assistita e la maternità surrogata. La normativa comunitaria è vero che lascia liberi gli stati in tema di matrimonio, ma da anni ormai impone – in assenza di matrimonio – forme equivalenti di riconoscimento della coppia omosessuale.

Era difficile aspettarsi che la Corte accogliesse in toto la questione di costituzionalità ed estendesse il matrimonio alla coppia omosessuale: e infatti non lo ha fatto. Ma dal suo ragionamento viene fuori con chiarezza il riconoscimento di un deficit gravissimo di tutela delle coppie omosessuali e quindi un forte richiamo al legislatore perché individui altre forme di garanzia dei diritti anche di questi soggetti e delle loro unioni affettive e sessuali.

Ora tocca al legislatore colmare quel vuoto in maniera adeguata e non con escamotage. La corte, peraltro, si riserva di passare al setaccio del giudizio di costituzionalità la futura normativa che sarà prodotta in materia.

Il Parlamento, quindi, è avvisato: si metta all'opra e lavori bene.

Noi staremo a vedere.  
Anzi, no, ci muoveremo perché ciò avvenga.