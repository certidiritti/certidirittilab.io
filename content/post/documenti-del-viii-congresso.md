---
title: 'Documenti del VIII Congresso'
date: Wed, 19 Nov 2014 21:11:48 +0000
draft: false
---

_[ORDINE DEI LAVORI](http://www.certidiritti.org/wp-content/uploads/2014/11/ORDINE-DEI-LAVORI.pdf)_

_[regolamento congressuale](http://www.certidiritti.org/wp-content/uploads/2014/11/regolamento-congressuale.pdf)_

_[Bilancio 2014](http://www.certidiritti.org/wp-content/uploads/2014/11/Bilancio-2014.pdf)_

_[PROPOSTA DI MOZIONE GENERALE](http://www.certidiritti.org/wp-content/uploads/2014/11/proposta-di-mozione-generale.pdf)_

_[Proposta di mozione particolare sui diritti delle persone intersessuali](http://www.certidiritti.org/wp-content/uploads/2014/11/Mozione-particolare-sui-diritti-delle-persone-intersessuali.pdf)_

_[Proposta di mozione particolare sulla gestazione per altri](http://www.certidiritti.org/wp-content/uploads/2014/11/Mozione-particolare-sulla-gestazione-per-altri.pdf)_

_[proposta di emendamento alla mozione generale](http://www.certidiritti.org/wp-content/uploads/2014/11/proposta-di-emendamento-alla-mozione-generale.pdf)_