---
title: 'CERTI DIRITTI PROMUOVE: CONVEGNO MATRIMONIO GAY VENERDI'' 9-10'
date: Wed, 30 Sep 2009 08:44:11 +0000
draft: false
tags: [Comunicati stampa]
---

**L'ASSOCIAZIONE RADICALE CERTI DIRITTI PROMUOVE**:

**Matrimonio Gay: Traguardo di Uguaglianza**

**Venerdì 9 ottobre 2009 ore 14.30 - 20**

**Camera dei deputati – Sala della Mercede**

**Via della Mercede, 55 – 00186 Roma  
**

Programma:

Ore 15

Modera: STEFANO CAMPAGNA, giornalista del tg1

**La storia di tre coppie gay e lesbiche che hanno deciso di chiedere  
il riconoscimento del matrimonio in Italia.**

Interviene MARIA TERESA MELI, Corriere della Sera

**Il matrimonio gay in Italia: traguardo di uguaglianza**

SERGIO ROVASIO, Segretario Associazione Radicale Certi Diritti

**Il matrimonio quale diritto fondamentale della persona**

Avv.  Alexander Schuster, Costituzionalista, Avvocatura per i diritti LGBT – Rete Lenford

**La campagna di Affermazione Civile**

Gian Mario FELICETTI, direttivo Associazione Radicale Certi Diritti

**Matrimonio e omosessualità in uno Stato laico**

LUIGI PANNARALE, ordinario di Sociologia del Diritto, Università degli Studi di Bari

Saluto dell’  Avv. ANTONIO ROTELLI, Presidente Avvocatura per i diritti LGBT - Rete Lenford

Ore 16.30

**La parola ai politici. Le proposte di legge e i disegni di legge depositati in Parlamento**

Modera: Giuliano Federico, direttore editoriale GAY.tv

Partecipano:

On. Anna Paola Concia, PD

On. Barbara Pollastrini, PD

Sen. Antonio Paravia, PDL

On. Beatrice Lorenzin, PDL

On. Rita Bernardini, Radicali-Pd

On. Marco Beltrandi, Radicali-Pd

Sen. Marco Perduca, Radicali-Pd

Sen. Donatella Poretti, Radicali-Pd

On. Fabio Evangelisti, IDV

Prof. Gianni Vattimo, eurodeputato IDV

Alessandro Zan, Sinistra e Libertà

Franco Grillini, direttore Gay Net

Ore 17.30

**Le associazioni LGBT(E) sul matrimonio gay: idee, proposte, iniziative**

Modera: GIANNI ROSSI BARILLI, direttore del mensile gay ‘Pride’

**Partecipano**: Agedo, Arcigay, Arcilesbica, CGIL - Nuovi Diritti, Circolo Mario Mieli, DiGayProject, CIG, Famiglie Arcobaleno, Amnesty Italia, GayLib, I-Ken Onlus, Milk Milano, We Have a Dream, Fondazione Massimo Consoli, Nuova Proposta – donne e uomini omosessuali cristiani, Polis Aperta, Studenti Luca Coscioni. **Sono invitati**: Arci, Comitato Torino Pride, GayToday.it, Queerblog, Gruppo Pesce, Renzo e Lucio – Lecco, MOS, AIESEC, Linfa, Lista Lesbica Italiana, Lila, ASA, Gay Statale – Università di Milano, KOB –Milano Bicocca, UDS, UAAR, i gruppi pro matrimonio gay di Facebook.

**Conclusioni:** ENZO CUCCO, direttore Fondazione Sandro Penna – Torino

Ore 18.45

**Esponenti della comunità LGBT a confronto**

Modera: DANIELE NARDINI, direttore news Gay.it

Partecipano: Francesco Bilotta – Rete Lenford, Frank Semenzi – editore Pride, Fabio Canino – artista, Aldo Brancacci – professore universitario Roma tre, Vladimir Luxuria – artista, Don Franco Barbero, Emiliano Zaino – Cassero di Bologna, Imma Battaglia – DiGayProject, Enrico Oliari – GayLib, Saverio Aversa, Ivan Scalfarotto – PD, Gianni Rossi Barilli, Paolo Patanè – Arcigay Sicilia, Mario Cirrito, Luca Trentini – Diritti Umani Arcigay, Christian Poccia, Cristiana Alicata – We Have a Dream, Alessandra Russo – Nuova Proposta, Carlo Santacroce – Presidente 3D-Democratici per pari Diritti e Dignità di lesbiche, gay, bisessuali e trans, Fabrizio Marrazzo – Arcigay Roma, Guido Allegrezza – We Have a Dream, Daniele Priori – Gay Lib, Pasquale Quaranta, Maria Gigliola Toniollo – Cgil Nuovi Diritti, Chiara Lalli, Andrea Maccarrone – Circolo Mario Mieli, Sergio Lo Giudice, Federico Boni – Spetteguless, Emanuele Sebastio – fondatore gruppo Facebook ‘Manifesto Diritto Gay’, Daniele Ventrelli – fondatore gruppo Facebook ‘Si al Matrimonio gay civile in Italia’.

Ore 19.45

**Matrimonio gay: perché è un traguardo di civiltà**

_Confermare entro il 7 ottobre la propria partecipazione al seguente indirizzo e-mail:_ **_[info@certidiritti.it](mailto:info@certidiritti.it)_**_  
specificando nell'oggetto: ‘Convegno 9 ottobre Matrimonio Gay’ e scrivendo il proprio nome e cognome.  
Per accedere alla Sala della Mercede occorre avere un documento d'identità e, per gli uomini, la giacca._

_Segreteria convegno: tel._ **_06-67609021_**