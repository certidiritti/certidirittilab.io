---
title: 'Allarme Ungheria: su diritti lgbt e legge bavaglio'
date: Sat, 22 Jan 2011 06:33:21 +0000
draft: false
tags: [Comunicati stampa]
---

Roma – Bruxelles, 21 gennaio 2011

**DIRITTI LGBTE, ALLARME UNGHERIA!**

**CERTI DIRITTI E MOLTE ALTRE ASSOCIAZIONI EUROPEE, CHIEDONO LA REVISIONE DELLA PROPOSTA DI RIFORMA DELLA COSTITUZIONE UNGHERESE E DELLA LEGGE SUI MEDIA.**

**Comunicato Stampa dell’Associazione Radicale Certi Diritti:**

Il Parlamento ungherese ha approvato lo scorso anno una proposta di riforma della Costituzione che proibirebbe il matrimonio tra persone dello stesso sesso. Inoltre, lo scorso dicembre, lo stesso Parlamento ha approvato la legge sui media, che é stata criticata dal Parlamento europeo, dall'OSCE, dalle associazioni per i diritti civili, dalle organizzazioni dei giornalisti e degli editori, e ribattezzata "legge bavaglio". La legge, oltre ad asservire i media pubblici allo Stato ed al governo in carica, prevede multe salatissime e sanzioni gravi per i media privati, sulla base di norme che lasciano un margine di manovra eccessivo alle autorità di controllo, nominate e controllate dal Governo.

La legge in particolare prevede al suo articolo 83, lettera c), che tra "gli obiettivi del servizio pubblico radiotelevisivo sia inclusa...la promozione ed il rafforzamento del... rispetto dell'istituzione del matrimonio e del valore della famiglia". L'applicazione di questa norma é ora nelle mani dell'onnipotente e conservatrice Annamaria Szalai, che ha nel 2009 lottato ferocemente per sanzionare la TV pubblica per avere trasmesso le immagini di una cerimonia civile tra due persone dello stesso sesso e conseguente bacio per "violazione dell'istituto del matrimonio", tutto ciò nonostante le coppie dello stesso sesso siano riconosciute in Ungheria.  La Szalai in passato ha votato contro tutte le sanzioni contro le affermazioni che incitano all'odio contro gli omosessuali - oltre ad essersi battuta per impedire la trasmissione dei Pokemon, a suo dire dannosi per i minori.

L’Associazione Radicale Certi Diritti, insieme a molte associazioni europee che si battono per la promozione e la difesa dei diritti civili, chiede l'urgente revisione del pacchetto di riforme alla Costituzione ungherese, come pure della legge ungherese sui media e si appella alle autorità ungheresi, europee ed internazionali perché non siano introdotte ed applicate in Ungheria norme che violano la libertà di espressione ed il divieto di discriminazione sulla base dell'orientamento sessuale.