---
title: 'BALTIC PRIDE: CERTI DIRITTI C’ERA. IL VIDEO'
date: Tue, 11 May 2010 21:32:09 +0000
draft: false
tags: [Comunicati stampa]
---

L’8 maggio si è svolto a Vilnius, in Lituania, il Baltic Pride. La manifestazione, autorizzata soltanto il giorno prima dopo che il tribunale amministrativo ne aveva in precedenza vietato lo svolgimento, ha fatto registrare momenti di grande tensione quando un gruppo di contestatori si è scontrato con le forze dell'ordine.

La polizia ha risposto con gas lacrimogeni agli attacchi di circa tremila oppositori che al grido di "morte agli omosessuali" e "Lituania ai lituani" hanno lanciato bottiglie, pietre e petardi contro i manifestanti, i giornalisti e le forze dell’ordine. Diciannove le persone finite in manette, tra loro anche due membri del parlamento.  
Per le strade di Vilnius anche una delegazione dell'associazione radicale Certi Diritti che già nei giorni scorsi aveva annunciato la partecipazione con striscioni per chiedere la revisione della legge omofobica sulla "protezione dei minori dalle informazioni nocive" e a favore del matrimonio gay.