---
title: 'Berlusconi ha confermato di non fare nulla sui diritti civili'
date: Sat, 26 Feb 2011 16:50:20 +0000
draft: false
tags: [Comunicati stampa]
---

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti

“Il Presidente del Consiglio Silvio Berlusconi non ha detto assolutamente nulla di straordinario, rispetto a quello che solitamente fa in tema di unioni civili e matrimonio tra persone dello stesso sesso, ovvero nulla.

A parole conferma quello che ha sempre fatto in tema di diritti civili e umani: nulla.  La scelta di andare nella direzione opposta rispetto a quello che hanno già fatto molti paesi europei e che chiede l’Unione Europea, il Consiglio d’Europa, il Parlamento Europeo, la maggioranza degli italiani  - che si sono sempre espressi favorevolmente in tema di unioni civili e di matrimonio tra persone dello stesso sesso -  viene così confermata. Del resto sappiamo bene che Berlusconi deve  riaccreditarsi un po’ con le gerarchie vaticane, che gli hanno dato addosso fino a ieri per i suoi comportamenti privati. Lo fa, purtroppo,  sulla pelle delle persone lesbiche e gay, alimentando così altro odio e pregiudizio”.