---
title: 'LA QUESTURA VIETA SAN GIOVANNI AL GAY PRIDE: NON E'' DEL VATICANO'
date: Thu, 29 May 2008 11:49:58 +0000
draft: false
tags: [Comunicati stampa]
---

**Dichiarazione di Rita Bernardini, deputata, Segretaria di Radicali Italiani,Marco Perduca, Senatore radicale – Pd e Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:  
  
**

"La questura di Roma, dopo aver confermato nel mese di aprile che Piazza San Giovanni in Laterano sarebbe stato il punto di arrivo del Gay Pride di Roma del prossimo 7 giugno, ha obiettato ieri che la piazza non si può usare perché alle 21 del 7 giugno ci sarà un concerto all'interno della Basilica.

Ricordiamo alla questura, e al Vaticano, che il Gay pride si svolgerà in orario pomeridiano e certamente per le ore 21 sarà concluso. Riteniamo quindi del tutto inacettabile questo diniego e ci auguriamo che le autorità rivedano questa loro decisione nella quale si potrebbe scorgere un profumo d'incenso per gratificare le gerarchie vaticane che, lo ricordiamo, non sono proprietarie della piazza. Oggi alle ore 17.30 una delegazione di radicali, guidata dal Senatore Marco Perduca, parteciperà alla Conferenza Stampa convocata presso la sede del Circolo Mario Mieli".