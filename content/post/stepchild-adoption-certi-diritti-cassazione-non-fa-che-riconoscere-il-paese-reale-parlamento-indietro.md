---
title: 'STEPCHILD ADOPTION. CERTI DIRITTI: CASSAZIONE NON FA CHE RICONOSCERE IL PAESE REALE. PARLAMENTO INDIETRO.'
date: Wed, 22 Jun 2016 19:58:01 +0000
draft: false
tags: [Diritto di Famiglia]
---

[![coppia-gay_AFFIDAMENTO](http://www.certidiritti.org/wp-content/uploads/2015/10/coppia-gay_AFFIDAMENTO-300x175.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/10/coppia-gay_AFFIDAMENTO.jpg)

"Confermando la sentenza della Corte d'Appello di Roma sull'adozione del figlio della convivente omosessuale, la Corte di Cassazione non fa che riconoscere il paese reale che nel 2016 vede come parte non più trascurabile le coppie gay con figli. Il Parlamento incassi l'ennesima lezione frutto dell'impegno di quelle coppie che hanno intrapreso le vie giudiziarie per vedersi riconosciuto un diritto fondamentale"

Lo dice Yuri Guaiana, segretario dell'Associazioen Radicale Certi Diritti.

"E' attesa una pioggia di richieste da parte delle famiglie omogenitoriali che rivendicheranno la cittadinanza dei loro figli, se ne faranno una ragione i cattofamilisti che hanno boicottato l'ex articolo 5 della legge sulle unioni civili"

Roma, 22 giugno 2016