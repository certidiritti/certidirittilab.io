---
title: 'Giornata mondiale contro l''Aids: Italia esca da figuracce fatte finora. Subito azioni su uso preservativo e iniziaitve legali a chi dice che non serve'
date: Wed, 30 Nov 2011 16:01:51 +0000
draft: false
tags: [Salute sessuale]
---

Il servizio pubblico e i media nazionali considerano ancora tabù la parola preservativo. Italia non ha pagato le quote 2009 e 2010 del Fondo Globale contro l'Aids. Domani iniziative in diverse città italiane.

Roma, 30 novembre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

IL  Governo italiano non ha rispettato gli impegni di pagamento 2009 e 2010 delle sue quote al Global Fund to Fight Aids, per un totale di 260 milioni di Euro, e non ha nemmeno versato i 30 milioni di dollari addizionali annunciati con grande solennità al G8 de L’Aquila dall’allora Presidente del Consiglio, Silvio Berlusconi.

Il Fondo è diventato il principale finanziatore di programmi di lotta contro l'AIDS, la tubercolosi e la malaria, salvando la vita a oltre 6,5 milioni di persone e ha contribuito a migliorare la salute materna e infantile anche attraverso la promozione dell’accesso ai servizi per la salute sessuale e riproduttiva e l’HIV/AIDS. L'Italia ha svolto un ruolo di primo piano nel lancio del Fondo Globale in occasione del Vertice G8 del 2001 a Genova. Il suo impegno è venuto meno negli ultimi due anni e non ha assunto alcun impegno finanziario per il triennio 2011-2013. Questo è anche il motivo per il quale l’Italia non è più titolare di un seggio unico al Consiglio di Amministrazione del Fondo.

Tutto questo avviene mentre le Nazioni Unite, con la Dichiarazione Politica dell’Assemblea Generale del 10 giugno 2011 (UNGASS 2011), chiede la sconfitta dell’HIV come  priorità fino al 2015. L’Italia intervenne con un discorso  ridicolo e patetico contro le politiche di “riduzione del danno”,  ispirato dall’allora Sottosegretario mulinaro bianco Giovanardi, e sostenuta soltanto da Vaticano, Iran e Russia.

A distanza di trent’anni l’unica iniziativa promossa dal Governo italiano, in occasione dell’ultima giornata contro l’Aids, è stata quella di invitare le persone tra i 30 e 40 anni a fare il test Hiv; in Italia non esistono campagne di informazione e prevenzione sulla diffusione dell’Hiv, nemmeno rivolte ai migranti con apposite iniziative, così come suggerito dalla Commissione nazionale di lotta contro l'Aids. Non si può nemmeno nominare pubblicamente la parola preservativo e alcune farmacie non li vendono in nome del diritto all'obiezione di coscienza. Il tutto mentre in tutta Europa si trovano preservativi in distribuzione di tutti i locali pubblici e nelle scuole.

Occorre assolutamente un cambio di rotta e lo chiediamo con gran forza al nuovo Governo. Innanzitutto tra le priorità, il rispetto degli impegni assunti in sede internazionale, l’avvio di campagne di prevenzione e informazione, mirate e con linguaggi appropriati (senza ipocrisie e censure), utilizzo della parola PRESERVATIVO in tutte le ore nei media del servizio pubblico, iniziative di sensibilizzazione  rivolte ai migranti, agli studenti e ai frequentatori di locali di ogni genere, lotta ad ogni forma di discriminazione nei confronti delle persone colpite da Hiv, persone detenute, persone omosessuali, lotta ad ogni forma di pregiudizio nei vari ambiti lavorativi, scolastici e accademici della popolazione sieropositiva, delle lavoratrici e dei lavoratori del sesso, l’avvio di iniziative politiche concrete sulla riduzione del danno riguardo le persone tossicodipendenti, così come richiesto dalla Dichiarazione di Vienna del 2010, e, infine, non perché meno importanti, azioni legali contro chi falsifica le informazioni riguardo le politiche di prevenzione dell’Aids. Ad esempio, contro chi dice che il preservativo non serve a combattere l’Aids.

In occasione della Giornata Mondiale contro l'Aids l'associazione radicale Certi Diritti distribuirà gratuitamente preservativi in diverse città italiane tra cui Milano, Roma, Torino, Verona e Trieste.

L'associazione ha aderito alla Condom Week di Milano e inoltre parteciperà il 30 Novembre a Roma al dibattito 'Chiesa, Hiv e Omosessualità' con Sergio Rovasio, segretario di Certi Diritti, e Simone Lanini, epidemiologo. A Torino il 1 dicembre sarà al presidio di fronte alla sede della Regione in Piazza Castello insieme al comitato "Che fine ha fatto il nuovo Amedeo di Savoia?" per chiedere conto del futuro dell'ospedale.