---
title: 'Questura rilascia permesso di soggiorno a uruguayano, sposato in Spagna con italiano, che aveva vinto ricorso al tribunale di Reggio Emilia'
date: Mon, 26 Mar 2012 06:13:52 +0000
draft: false
tags: [Diritto di Famiglia]
---

Roma, 26 marzo 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

La Questura di Reggio Emilia ha rilasciato il permesso di soggiorno a Rafael, il ragazzo uruguayano che si era sposato in Spagna con Flavio, un cittadino italiano, che lo scorso 13 febbraio aveva vinto il ricorso presentato al Tribunale di Reggio Emilia dopo che la Questura gli aveva negato il documento.

L’Associazione Radicale Certi Diritti aveva sostenuto il ricorso della coppia gay proprio a causa del fatto che la Questura di Reggio Emilia non rilasciava a Rafael il permesso di soggiorno perché in Italia il loro matrimonio non è riconosciuto. Ciò in violazione del Trattato di Nizza, sulla libera circolazione e del Trattato di Lisbona sulla lotta alle discriminazioni.

Nel ricorso  pur non richiedendo la trascrizione del matrimonio, materia che con il diritto di famiglia viene lasciata alla competenza esclusiva di ogni Stato membro dell’Unione Europea, si chiedeva l'applicazione delle norme che regolamentano la libera circolazione dei cittadini  europei e dei loro famigliari. Queste normative europee, ratificate dall'Italia, devono essere applicate anche nel nostro Paese.

Il Tribunale aveva quindi accolto il ricorso ai sensi del D. Lgs. 30/2007, legge che da' attuazione alla Direttiva 2004/38/CE, sul riconoscimento del diritto di soggiorno ai familiari (anche stranieri) dei cittadini dell'Unione europea.  Nel ricorso si era fatto riferimento alla  sentenza n. 1328/2011 della Corte di Cassazione che  afferma: a) la nozione di “coniuge” prevista dall’art. 2 d.lgs. n. 30/2007 deve essere determinata alla luce dell’ordinamento straniero in cui il vincolo matrimoniale è stato contratto e che b) lo straniero che abbia contratto in Spagna un matrimonio con un cittadino dell’Unione dello stesso sesso deve essere qualificato quale “familiare”, ai fini del diritto al soggiorno in Italia. 

La sentenza si è  richiamata  alla sentenza della Corte costituzionale n. 138 del 2010 che afferma, tra l’altro, che  l’unione omosessuale,  “intesa come stabile convivenza tra due persone dello stesso sesso”, spetta “il diritto fondamentale di vivere liberamente una condizione di coppia” e che il «diritto all’unità della famiglia che si esprime nella garanzia della convivenza del nucleo familiare (…) costituisce espressione di un diritto fondamentale della persona umana”.

Il rilascio del documento a Rafael da parte della Questura di Reggio Emilia è il primo documento nella storia italiana che dà efficacia al riconoscimento dello status famigliare delle coppie omosessuali,  un altro grande passo di civilità per il superamento delle diseguaglianze e delle discriminazioni.

**Nell'immagine il permesso di soggiorno di Rafael ottenuto per motivi familiari.**

**[SENTENZA STORICA DEL TRIBUNALE DI REGGIO EMILIA: CONIUGE DELLO STESSO SESSO DI UN ITALIANO SPOSATO IN SPAGNA HA DIRITTO A VIVERE IN ITALIA >](http://www.certidiritti.org/2012/02/14/storica-sentenza-tribunale-reggio-emilia-coniuge-stesso-sesso-di-un-italiano-sposato-in-spagna-ha-diritto-a-vivere-in-italia/)[  
](storica-sentenza-tribunale-reggio-emilia-coniuge-stesso-sesso-di-un-italiano-sposato-in-spagna-ha-diritto-a-vivere-in-italia)**

**[ISCRIVITI >](iscriviti)  
**

  
[permesso di soggiorno](http://www.certidiritti.org/wp-content/uploads/2012/03/carta_di_soggiorno_rafael_foto_volta_modicato.jpg)