---
title: 'Certi Diritti: Torino e San Pietroburgo si uniscono in un asse strategico liberticida'
date: Fri, 16 Nov 2012 10:33:44 +0000
draft: false
tags: [Russia]
---

Comunicato stampa dell’Associazione Radicale Certi Diritti

Roma, 16 novembre 2012

La Torino del democratico Fassino ha siglato ieri un accordo bilaterale di collaborazione con San Pietroburgo, dove il 29 febbraio scorso il Parlamento ha approvato una legge che sanziona la cosiddetta “propaganda dell’omosessualità” criminalizzando di fatto qualunque attività o informazione relativa alle persone LGBTI (Lesbiche, Gay, Bisessuali, Transessuali e Intersessuali) e alle relazioni tra persone dello stesso sesso, in patente violazione delle libertà di espressione e associazione, nonché degli impegni presi dalla Russia ratificando la Convenzione europea per la salvaguardia dei diritti dell’uomo e delle libertà fondamentali.

Suona addirittura come una beffa la dichiarazione che uno dei quattro settori di collaborazione sia proprio la cultura. Come può Torino pensare di collaborare sul piano culturale con una città che lede così gravemente il diritto umano fondamentale della libertà di espressione da censurare preventivamente le parole omosessuale, gay, lesbica, bisessuale, transessuale e intersessuale? Forse la ragione sta proprio nelle dichiarazione dello stesso Fassino che, tra le “ragioni e similitudini storiche, e allo stesso attuali”, individua solo l’industria dell’auto e la green economy, senza alcuna menzione dei diritti umani violati. Evidentemente, le ragioni dell’economia - verde, grigia o rossa che sia - valgono più delle condanne del Parlamento Europeo e del Consiglio d’Europa contro la sciagurata legge liberticida approvata nella ex capitale zarista anche per una Giunta di sinistra e sedicente democratica.

Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti, afferma: ≪Sono sconcertato dalla notizia. Dopo Venezia, Torino è la seconda città italiana a siglare un accordo di cooperazione con San Pietroburgo dopo il varo della legge contro la cosiddetta “propaganda dell’omosessualità”. Consiglio a Fassino di ascoltare le vibrate proteste e le preoccupate considerazioni di intellettuali e artisti come Moni Ovadia, Gad Lerner, Lella Costa, Elio De Capitani, Ferdinando Bruni, Alessandro Cecchi Paone e tanti altri che hanno aderito alla campagna lanciata dall’Associazione Radicale Certi Diritti per la sospensione del gemellaggio tra Milano e San Pietroburgo, per farsi un’idea della gravità della sua decisione. Sulla pagina Facebook Stopalgemellaggio si possono ascoltare anche le voci di alcune cittadine, attiviste e avvocate per i diritti umani di San Pietroburgo, per ricordarsi quanto valgono i diritti umani che, come dice Olga Lenkova, "possono solo esistere globalmente, ed è globalmente che dobbiamo lottare per loro". Torino è giustamente definita capitale dei diritti ed è all’avanguardia nella difesa dei diritti dei suoi cittadini LGBTI, ma non può ignorare le violazioni dei diritti umani da parte delle città con le quali stinge accordi bilaterali. Mi auguro che Torino torni sui suoi passi o, almeno, usi questo strumento per fare le debite pressioni affinché San Pietroburgo revochi questa legge sciagurata per la quale si è arrivati a multare la rockstar Madonna addirittura per 10.5 milioni di dollari e rilancio la campagna per la sospensione del gemellaggio dell’ex capitale zarista con Milano affinché almeno una città italiana dia un segno tangibile a favore dei diritti umani e delle libertà fondamentali≫.

  
Per saperne di più:

[http://www.certidiritti.it/notizie/comunicati-stampa/item/1584-certi-diritti-lancia-la-campagna-%20per-la-sospensione-del-gemellaggio-tra-milano-e-san-pietroburgo-in-occasione-del-45](http://www.certidiritti.org/2012/10/01/certi-diritti-lancia-la-campagna-per-la-sospensione-del-gemellaggio-tra-milano-e-san-pietroburgo-in-occasione-del-45-anniversario-della-sottoscrizione/)°-%20anniversario-della-sottoscrizione

  
[http://www.facebook.com/Stopalgemellaggio](http://www.facebook.com/Stopalgemellaggio)