---
title: 'dal blog di Amina, ragazza lesbica diventata eroina della rivolta in Siria'
date: Sun, 08 May 2011 21:09:46 +0000
draft: false
tags: [Medio oriente]
---

**Sono in clandestinità. Lunedi, mi sono incontrata con degli amici in un caffè. Abbiamo avuto una vivace discussione circa il futuro. Che cosa viene dopo Asad e come possiamo arrivarci ... come si fa a evitare una guerra civile. Cosa si può fare e cosa deve essere fatto ... Sono sicura che potete immaginarlo.**

[http://damascusgaygirl.blogspot.com/2011/05/gone-underground.html](http://damascusgaygirl.blogspot.com/2011/05/gone-underground.html)  
4 maggio 2011

C'era il wifi nel caffè e, mentre la conversazione trattava di altro, intanto leggevo le notizie,le e-mail gli amici, e così via ... ho anche composto un post o due per il mio blog.  
Un molto, molto caro (e, ovviamente, del tutto splendido!) Amico mi ha scritto dal Quebec che stava cercando di chiamarmi a casa e nessuno rispondeva. Sono rimasta sorpresa che mio padre fosse in giro. L'avevo lasciato a casa da solo ... e, beh, si è ritirato a Damasco, perché la sua salute non è quella di una volta. E, con mia madre fuori dal paese, mi sento estremamente responsabile di lui, faccio attenzione che abbia le sue pillole, non si strapazzi e così via ...  
Così, quando l'ha cercato più e più volte senza risposta, mi sono preoccupata.  
E visto che sono diventata un po' paranoica dopo che i servizi di sicurezza mi hanno fatto visita, ho deciso di utilizzare un telefono fisso piuttosto che il mio cellulare. Così, sono andata a chiamarlo da un telefono pubblico. Nessuna risposta sul numero di casa. Non risponde al suo cellulare, l'aveva spento (ilche non è che mi stupisca;  lascia la sua batteria sempre scarica).  
Ho deciso di chiamare il nostro portiere, lui mi ha detto che mio padre se n'era andato, dove era andato a e che avrei potuto chiamarlo li ...  
  
L'ho fatto.  
  
E lui mi ha detto:  
  
Sono tornati per te. Questa volta, non c'è nulla che io possa fare. Vai da qualche parte e non mi dire dove sei. Stai al sicuro. Io ti amo.  
  
Quindi, io non sono andato a casa lunedi notte, ho dormito a casa di un amico. Ieri, sono andata a casa per un po '. Come sono stata vicino a casa, ho visto che c'erano due di loro fuori della porta d'ingresso.  
C'è più di un modo per entrare in casa mia e alcuni sono piuttosto ben nascosti. Nemmeno tutta la famiglia li conosce tutti. Ho usato uno di quelli, ma sono entrata dentro. Ho potuto vedere che essi erano effettivamente stati lì (una possibilità, oppure il mio vecchio padre era diventato particolarmente incivile in mia assenza e non è riuscito a pulire).  
Ho pulito, ho riempito un sacchetto con alcuni cambi di vestiti, le lenti a contatto di ricambio e della soluzione, e così via. Ho preso su anche il mio portatile, dopo una breve verifica on line. Ho preso la carta SIM dal mio telefono e l'ho lasciato lì.  
  
Ho cambiato il mio abbigliamento e, senza dire esattamente come, io sono molto meno riconoscibile come me (ci sono alcuni vantaggi a vivere qui, se si vuole diventare anonimi).  
  
Sono andata via nel modo in cui ero arrivata.  
  
Ho vagato un po 'per la città, poi sono finita a casa un vecchio amico, in una zona dove è' sicuro '(un sobborgo elegante dove molti hanno beneficiato di vivere Assadism) per la notte.  
Sto cercando di capire il passo successivo.  
  
Se qui fossero venuti qui con un mandato scritto, non ce ne potrebbe essere uno con il mio nome. Credo di potergli sfuggire e fare anche un lavoro utile qui, ma mi rende la vita molto meno facile.  
  
Non ho alcun desiderio di essere un martire, neanche per la mia propria causa, così farò quello che posso per restare libera. Non è facile ... ma ...  
  
Per ora, ho scritto lontano da dove sono e sarò on line in modo irregolare  per mantenermi a distanza di sicurezza.  
Nel frattempo, si spera, per triste che può sembrare in questo momento, la strada della libertà non è mai apparsa più chiaro! La nostra rivoluzione vincerà, avremo presto una Siria libera e democratica. Me lo sento nelle mie ossa. La nostra maturità sta per manifestarsi  e noi ancora una volta stupiremo il mondo. Avremo una libera Siria e una nazione libera, sarà presto realtà. La rivoluzione avrà successo e schiaccerà il settarismo, il dispotismo, il sessismo, e tutto il peso morto di questi anni di amarezze, di divisione e di separazione, di oppressione e di tirannia.  
Saremo liberi!  
  
[http://damascusgaygirl.blogspot.com/2011/05/gone-underground.html](http://damascusgaygirl.blogspot.com/2011/05/gone-underground.html)

traduzione di Alba Montori