---
title: 'A Torino chiude l''ospedale Amedeo di Savoia?'
date: Mon, 21 Nov 2011 15:38:12 +0000
draft: false
tags: [Salute sessuale]
---

Il primo dicembre il comitato “Che fine ha fatto il nuovo Amedeo di Savoia” organizza un presidio alle ore 11 in Piazza Castello.

Il Comitato “Che fine ha fatto il nuovo Amedeo di Savoia” organizza per il giorno 1° dicembre 2011, ore 11 in Piazza Castello n. 165, Torino un presidio allo scopo di affermare che nessuna decisione sul futuro dell’ospedale Amedeo di Savoia possa essere presa senza un confronto con le associazioni dei pazienti, oltre che dei medici e degli infermieri che in quell’Ospedale lavorano.  
Da un anno il Comitato ha cercato un interlocutore istituzionale in grado di fornire notizie certe circa il futuro dell’ospedale e purtroppo dobbiamo constatare che nessuno ha risposto alle nostre ripetute richieste.  
Anche l’ultima riunione operativa della Consulta Regionale contro l’AIDS, che si è svolta nel settembre 2010, non ha prodotto alcunché di concreto. Il 14 di ottobre u.s. in previsione della Giornata Mondiale di lotta all’AIDS abbiamo richiesto all’assessore Monferino un incontro pubblico, ma ci ha risposto di non potere per quella data. A tutt’oggi non ha ancora indicato una data alternativa.  
Infine abbiamo appreso dai quotidiani che nel piano di riorganizzazione della rete ospedaliera piemontese per l’ospedale Amedeo di Savoia si prevede il trasferimento in altra struttura, e probabilmente anche lo smembramento dei servizi.  
   
Il futuro dell’Ospedale Amedeo di Savoia e delle malattie infettive in Piemonte non è solo interesse delle associazioni che si occupano di HIV/AIDS, ma di tutti e tutte, e a tutti e tutte noi chiediamo di sostenere questa iniziativa e venire al presidio!  
   
   
COMITATO “CHE FINE HA FATTO IL NUOVO AMEDEO DI SAVOIA?”

Anlaids Piemonte, Arcigay Torino, Arcobaleno Aids, Coordinamento Torino Pride lgbt, Associazione Radicale Certi Diritti, CTS Torino, Circolo GLBT Maurice, Gruppo Abele, Lila Piemonte, Sermig.

Persona di contatto:  
Stefano Patrucco  
cell. 338/491994  -  e-mail: [steupatrucco@gmail.com](mailto:steupatrucco@gmail.com)