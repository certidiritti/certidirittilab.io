---
title: 'Carta d''intenti del PD: tema delle unioni gay doveva essere affrontato in termini di diritto all''uguaglianza non di diritto delle minoranze'
date: Sun, 09 Sep 2012 14:27:44 +0000
draft: false
tags: [Politica]
---

Che tipo di riconoscimento giuridico s’impegnano a dare i democratici all’unione omosessuale?

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Roma, 9 settembre 2012

Il 9 settembre il Partito Democratico pubblica su “L’Unità” la Carta d'Intenti "dei democratici e dei progressisti". Il tema dell’omosessualità viene trattato nel paragrafo dei diritti dove il PD s’impegna a votare una "legge urgente contro l'omofobia" e a dare “sostanza normativa al principio riconosciuto dalla Corte costituzionale, per il quale una coppia omosessuale ha diritto a vivere la propria unione ottenendone il riconoscimento giuridico”.

Dichiarazione di Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti: Già la collocazione del tema nel paragrafo dedicato ai diritti è sbagliata, **la questione andava affrontata in quello dedicato all’uguaglianza con due semplici parole, come hanno fatto  i democratici americani: matrimonio egualitario**. I democratici nostrani ricorrono invece a lunghe circonlocuzioni ben lontane dal configurare un chiaro intento politico. Che tipo di riconoscimento giuridico s’impegnano a dare i democratici all’unione omosessuale? Oggi le coppie omosessuali rivendicano un solo diritto, caro Bersani, quello all’uguaglianza, come hanno capito i democratici americani, ma anche la sinistra europea, alla quale il PD si picca di appartenere, e persino i conservatori britannici.

Qui non si tratta di un problema di Costituzione, **[come hanno chiarito anche costituzionalisti vicini al PD](http://www.certidiritti.org/2012/09/08/la-costituzionalista-damico-la-costituzione-non-vieta-il-matrimonio-tra-persone-dello-stesso-sesso/)**: il legislatore potrebbe benissimo (e dovrebbe!) decidere oggi di dare alle coppie omosessuali il pieno accesso all’istituto del matrimonio civile, rispettando pienamente la costituzione. Il problema è di impostazione: vedere la questione nei termini più generali e appropriati del diritto all’uguaglianza che concerne tutti e non dei diritti delle minoranze (quella LGBTI in questo caso).

Se poi Bersani, nel suo intervento, sente il bisogno di giustificarsi per quello che definisce “il passo avanti compiuto sulle unioni gay”, affrettandosi a ricordare che esso non fa dimenticare al PD “il diritto del lavoratore in fabbrica a essere rappresentato anche se il suo sindacato non firma il contratto”, temo che il problema sia anche (ancora...) di cultura politica. Una cultura politica per la quale i diritti civili sono una sovrastruttura residuale, nella migliore delle ipotesi, se non addirittura lesiva della struttura dei diritti sociali. **[Provate a vedere la cosa nei termini in cui la vede il segretario di Stato americano, Hillary Clinton: i diritti delle persone LGBTI sono diritti umani, non diritti speciali!](http://www.certidiritti.org/2011/12/09/certi-diritti-pubblica-la-traduzione-italiana-integrale-del-discorso-di-hillary-clinton-per-la-giornata-mondiale-dei-diritti-umani/)** Questo forse vi aiuterà a correggere l’impostazione che date della questione e a rinnovare la vostra cultura politica, eliminando qualche incrostazione dal sapore decisamente troppo novecentesco.  
  
**[SOSTIENI L'ASSOCIAZIONE RADICALE CERTI DIRITTI >](partecipa/iscriviti)**