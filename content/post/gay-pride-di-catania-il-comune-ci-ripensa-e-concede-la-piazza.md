---
title: 'GAY PRIDE DI CATANIA: IL COMUNE CI RIPENSA E CONCEDE LA PIAZZA'
date: Tue, 01 Jul 2008 10:36:18 +0000
draft: false
tags: [Comunicati stampa]
---

Catania, 1 luglio 2008  
  
GAY PRIDE CATANIA: Il Comune ha cambiato idea e concederà Piazza dell'Università come punto d'arrivo del Gay Pride del prossimo 5 luglio.  Dopo le reazioni del movimento lgbt e di Certi Diritti, che aveva preannunciato iniziative nonviolnete a Catania davanti al Duomo, le autorità hanno saggiamente optato per il dialogo. Non concedere Piazza dell'Università al Gay pride, perchè troppo vicina al Duomo della città, sarebbe stato un fatto troppo ridicolo, l' ennesimo esmpio dell'asservimento politico al potere clericale.