---
title: 'VADEMECUM DEI POLIZIOTTI GAY CONTRO LE AGGRESSIONI'
date: Tue, 11 May 2010 14:00:31 +0000
draft: false
tags: [Senza categoria]
---

La legislazione italiana non prevede ancora una legge contro l'omofobia. Ma da oggi un vademecum dell'associazione Polis Aperta incoraggia chi subisce un'aggressione a sporgere denuncia.

"L'obiettivo del Vademecum - dichiara il presidente Nicola Cicchitti - e' quello di incoraggiare le vittime di insulti e aggressioni omotransfobiche a sporgere querela, nella convinzione che una maggiore emersione del fenomeno possa portare all'adozione, anche nel nostro Paese, di provvedimenti adatti a punire i crimini generati dall'odio e dall'ignoranza". Il vademecum e' scaricabile dal sito di Polis Aperta.

La conoscenza è la miglior difesa  
SCARICA IL VADEMECUM >