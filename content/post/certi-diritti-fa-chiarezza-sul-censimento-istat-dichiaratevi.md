---
title: 'Certi Diritti fa chiarezza sul censimento Istat. Dichiaratevi!'
date: Mon, 18 Apr 2011 12:21:34 +0000
draft: false
tags: [Diritto di Famiglia]
---

Censimento sì, o censimento no? Dopo il polverone di polemiche riguardo alla possibilità di censire, o meno, tutte le coppie conviventi dello stesso sesso, Certi Diritti fa luce su quanto sta accadendo. Da www.gay.tv  
Abbiamo parlato fin dall'inizio dell'importante possibilità che l'Istat censisse l'anno prossimo tutte le coppie conviventi comprese quelle dello stesso sesso (leggi QUI). Dopo il comunicato stampa Usi/Rdb (il Sindacato Nazionale Lavoratori della Ricerca dell'Unione Sindacale Italiana affiliato alle Rappresentanze Sindacali di Base) che smentiva il reale impegno dell'Istat e la bocciatura da parte del Garante della Privacy (leggi QUI) e la reazione di Arcigay... Ora Certi Diritti mette in luce tutto quello che è accaduto tra associazionu lgbt e l'Istituto di Statistica (leggi QUI).  
  
  
RIASSUNTO DEI FATTI  
  
A seguito di un incontro tra la dirigenza dell’Istat e nove associazioni LGBT (dunque non solo Arcigay), l’istituto di statistica aveva sottoposto per l’approvazione al Garante per la protezione dei dati personali (il “Garante della privacy”) una bozza di questionario in cui le forme famigliari venivano censite, come dieci anni fa, chiedendo ad ogni singolo individuo di ogni famiglia quale sia la sua relazione con il “capofamiglia”. Tra le 18 risposte possibili, oltre a “coniuge”, figlio, genitore e altre, c’erano sia “Convivente dell’intestatario, in coppia di sesso diverso” sia “Convivente dell’intestatario, in coppia dello stesso sesso”.  
  
Il Garante ha bocciato questa proposta con provvedimento n. 067 del 16 febbraio 2011, in quanto le due formulazioni citate (anche quella relativa alle coppie di persone di sesso diverso, dunque) avrebbero implicato “una raccolta di informazioni sensibili, in quanto idonee a rivelare la vita sessuale”. Quindi questa risposta dovrebbe essere volontaria, non obbligatoria, vanificando i soldi e il lavoro per il censimento. Di fronte a tale prospettiva, l’Istat ha deciso di unire le due opzioni di risposta, creando una modalità unica, che verosimilmente sarà simile a “convivente in coppia con l’intestatario” (per qualche ragione le parole precise non sono ancora state rese pubbliche).  
  
  
CONSEGUENZE  
  
La differenzianzione voluta dal Garante della Privacy fa molto discutere perché fa rientrare il dichiarare di essere una coppia di fatto (eterosessuale o omosessuale che sia) come un'informazione riguardante la sfera sessuale dei conviventi, mentre l'essere sposati no (quindi con un giudizio quasi valoriale). Questa decisione rischia di avere pesanti conseguenze negative per la possibilità di fare ricerca su molti temi sociali, economici, demografici,non autorizzando importanti raccolte dati.  
  
Ma Certi Diritti sottolinea come sia comunque possibile ricavare un quadro delle coppie dello stesso sesso anche in questa formula del censimento "ambigua". Basta prestare attenzione a due domande: quella sul rapporto con il capofamiglia e quella riguardante il proprio sesso. Incrociando i risultati delle due diverse domande obbligatorie, si può ottenere un quadro delle coppie omosessuali in Italia.  
  
Per cui è necessario da parte di Istat e delle associazioni lgbt una accurata diffusione dei corretti metodi di compilazione delle schede del censimento. E' necessario soprattutto che i conviventi non abbiano paura ad ammettere il priorio status di coppia, in modo da poter essere riconosciute e tenute in considerazione dalla Stato.

www.gay.tv