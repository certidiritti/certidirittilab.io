---
title: 'Fondamenti Giuridici dell''iniziativa'
date: Mon, 05 May 2008 15:51:12 +0000
draft: false
tags: [Senza categoria]
---

**_FONDAMENTI GIURIDICI  
___Perché Affermazione Civile?  
  
__**Sebbene ispirata alle esperienza di altre nazioni come il Canada e il Massachusetts, Affermazione Civile è una **assoluta novità nel panorama italiano** e si fonda su una lettura attenta, ragionata ed aperta della normativa italiana in tema di matrimonio.**Fino a pochi anni fa**In nessuna parte del mondo, fino a qualche decennio fa, si prevedeva la possibilità che l’unione tra persone dello stesso sesso fossero riconosciute dall'ordinamento. Poi, in Paesi quali Belgio, Olanda, Spagna, Canada e Sud Africa il matrimonio è stato esteso alle coppie formate da persone dello stesso sesso, mentre in altri, quali Regno Unito, Germania e Nuova Zelanda, sono state introdotte soluzioni analoghe ma con un nomi diversi.  
È bastato **superare una banale convenzione verbale**: marito e moglie, sono diventati semplicemente i coniugi,  rendendo ormai una realtà concreta il fatto che nella società attuale due persone dello stesso sesso si sposino.  
  
**Un pregiudizio, non un divieto  
**Contrariamente ad una comune percezione, **in Italia non esiste un divieto esplicito al matrimonio fra persone dello stesso sesso**. Infatti, il nostro Codice Civile non prevede come requisito di validità dell’atto matrimoniale la differenza di sesso tra i coniugi. È  piuttosto un'erronea interpretazione di altre norme (ad es. in materia di filiazione) che impedisci oggi alle coppie di omosessuali di sposarsi. Un regola non scritta, dunque, basata su una forzatura interpretativa che, per assurdo, seguendo lo stesso criterio, potrebbe addirittura arrivare, legittimamente, ad escludere dal matrimonio le persone sterili per malattia o per l’età avanzata!  
  
Vediamo più in dettaglio, dunque, dove si annidano le contraddizioni che caratterizzano l'attuale interpretazione. In buona sostanza, impedire alle persone dello stesso sesso di sposarsi, significa creare una serie di **gravi contrasti giuridici**:  

1.  verso il **rispetto della persona umana e dei suoi diritti fondamentali**, tra cui rientra il diritto di sposarsi e di fondare una famiglia si colloca in posizione primaria e fondamentale tanto da costituire un principio cardine del nostro sistema di diritto privato;
2.  verso il più generale **principio di non discriminazione** stabilito con una tale forza e semplicità dal secondo comma dell’art. 3 della Costituzione da scoraggiare, almeno in liena di principio, qualunque deragliamento interpretativo;
3.  verso il **principio di libertà e di autodeterminazione**, che è proprio di tutti gli stati democratici occidentali ed in base al quale lo Stato non può intromettersi in nessuna forma nelle scelte di vita dei cittadini (art. 13 della Costituzione).

**Un pregiudizio che si può sconfiggere...**  
Uno dei modi per modificare un’interpretazione errata come quella messa in evidenza è provocare una **nuova interpretazione giurisprudenziale**, ossia sollecitare i giudici ad esprimersi sul comportamento dell’ufficiale dello stato civile che negasse le pubblicazioni a due persone dello stesso sesso.  
A sostegno dell’opportunità del cambiamento dell’interpretazione si possono esprimere alcune considerazioni giuridiche:  

1.  la nozione di matrimonio nel nostro ordinamento non esiste;
2.  il nostro ordinamento non vieta di matrimonio tra persone dello stesso sesso e non prevede la diversità del sesso come requisito per contrarlo (ex art. 84 c.c.);
3.  le pubblicazioni di matrimonio per le coppie delle stesso sesso **sono rifiutate sulla base di circolari del Ministero degli interni, che si riferiscono all’ordine pubblico internazionale e non all’ordine pubblico interno**, perché relative alla trascrizione di matrimoni celebrati all’estero. Il che rende evidente l’uso strumentale di quelle circolari, che sono comunque contrarie allo spirito della nostra Costituzione, nonché della Carta di Nizza;
4.  l’interpretazione letterale delle norme che supporta l’atto di diniego alle pubblicazioni di matrimonio è contraria alla Costituzione italiana.

**... seguendo i principi costituzionali**  
L’opinione che senza una riforma legislativa le persone omosessuali in Italia non potranno mai accedere all’istituto matrimoniale è molto diffusa, soprattutto tra le associazioni italiane che si occupano diritti delle persone omosessuali in Italia. Ma, se il Parlamento non prende l’iniziativa e i cittadini rimangono senza tutela dei loro diritti civili, quali strade si possono percorrere? In questo caso, dato che si è di fronte all’assenza di una norma che regoli una specifica circostanza, è la giurisprudenza che deve formulare regole di comportamento alla luce dei principi iscritti nella Costituzione italiana e valide per tutti.  
E poichè non è conforme alla nostra Costituzione – per quanto si è detto poc’anzi – un provvedimento amministrativo che neghi ad una coppia omosessuale il diritto di contrarre matrimonio, **spetta alla giurisprudneza tutelare tale diritto in assenza di una specifica norma di legge**.  
  
Come ha stabilito la Corte d’Appello di Roma ([decr., 13 luglio 2006](http://www.eius.it/giurisprudenza/2006/106.asp), in Guida al diritto, n. 35, 2006, 59-60) in un caso di trascrizione di un matrimonio tra due persone dello stesso sesso celebrato in Olanda, si deve prendere atto che il matrimonio non è “definito” nella Costituzione italiana, né nel Codice civile e neppure nelle leggi speciali che nel tempo hanno regolamentato l’istituto. Pertanto, l’interprete è chiamato ad individuarne il contenuto essenziale con un’attenta considerazione dell’evoluzione che l’istituto possa avere avuto nel costume sociale, utilizzando tutti i criteri di interpretazione di cui all’art. 12 delle disposizioni sulla legge in generale, fra i quali il criterio evolutivo.  
Come ha riconosciuto la Corte d’Appello citata, vi è la possibilità di un’**interpretazione evolutiva dell’istituto, in mancanza di un divieto espresso rispetto alle nozze tra due persone dello stesso sesso**.  
È stato rilevato come il matrimonio tra due persone dello stesso sesso costituisca una novità antropologica assoluta. Mai prima degli ultimi anni ci si era posta la questione se due persone dello stesso sesso potessero accedere alle tutele che discendono dall’aver contratto matrimonio e quindi dallo status che i coniugi assumono in forza del negozio matrimoniale. È ben singolare che di fronte ad una fattispecie che non conosce precedenti nella storia del diritto, ci si richiami ad una **tradizione interpretativa che appare oggi del tutto inidonea** per focalizzare i requisiti per la validità e l’efficacia dell’atto matrimoniale.  
  
Un “inganno percettivo”  molto diffuso in Italia ha portato le persone omosessuali a non rivolgersi mai alla giustizia nella convinzione infondata che la legge vietasse espressamente a due persone dello stesso sesso di sposarsi. È proprio con **AFFERMAZIONE CIVILE** che vogliamo dimostrare che **anche le persone omosessuali possono e devono farsi valere come cittadini a pieno titolo di questo Paese**, chiedendo ai giudici di riconoscere loro il diritto di formare una famiglia, modificando un’intepretazione che non è più aderente all’evoluzione del contesto sociale.  
  
  
Francesco Bilotta

[![cosa_sono_le_pubblicazioni_degli_atti.png](http://www.certidiritti.org/wp-content/uploads/2008/04/cosa_sono_le_pubblicazioni_degli_atti.png "cosa_sono_le_pubblicazioni_degli_atti.png")](index.php?option=com_content&task=view&id=42&Itemid=71)

[![domande frequenti](http://www.certidiritti.org/wp-content/uploads/2008/04/faq.png "domande frequenti")](index.php?option=com_content&task=view&id=40&Itemid=70) [![i_fondamenti_giuridici.png](http://www.certidiritti.org/wp-content/uploads/2008/04/i_fondamenti_giuridici.png "i_fondamenti_giuridici.png")](index.php?option=com_content&view=article&id=51&catid=17&Itemid=81)