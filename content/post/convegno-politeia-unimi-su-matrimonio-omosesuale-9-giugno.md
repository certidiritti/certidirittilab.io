---
title: 'Convegno Politeia Unimi su matrimonio omosesuale 9 giugno'
date: Mon, 31 May 2010 13:40:03 +0000
draft: false
tags: [Senza categoria]
---

**_Matrimonio omosessuale._**

**_Diritto, Morale, Politica_**

**Milano, 9 giugno 2010** **− ore 15.00-18.30**

Sala di Rappresentanza del Rettorato

Università degli Studi di Milano

Via Festa del Perdono, 7 - Milano

Il diritto italiano esclude il matrimonio tra persone dello stesso sesso, oppure questa è un’interpretazione arbitraria del diritto vigente? Il matrimonio omosessuale è una questione che solleva problemi morali? Quali sono le ragioni politiche che fanno sì che, mentre nel resto del mondo occidentale si riconosce valore giuridico al matrimonio omosessuale o alle forme alternative di unione famigliare, in Italia sembra che l’argomento sia stato accantonato? E quale ruolo svolge la Chiesa Cattolica nella rimozione di questo tema dall’agenda politica? Il Convegno intende affrontare tali questioni con il contributo di giuristi, filosofi e sociologi, alla luce della sentenza 138/2010 della Corte Costituzionale.

Ore 14.45  Registrazione Partecipanti

Ore 15.00  Apertura del Lavori

Presiede:

**Patrizia Borsellino** (Università degli Studi di Milano-Bicocca)

Ore 15.15  I Sessione:

**Vittorio Angiolini** (Università degli Studi di Milano)

_Libertà di sviluppo della personalità in relazione al sesso_

**Barbara Pezzini** (Università degli Studi di Bergamo)

_Coppie same-sex e matrimonio: quale uguaglianza dopo la sent. 138/2010_

**Marilisa D’Amico** (Università degli Studi di Milano)

_Principi costituzionali e quadro europeo: una decisione ambigua_

Ore 16.15  Discussione

Ore 16.45  II Sessione:

**Paola Ronfani** (Università degli Studi di Milano)

_Famiglie e matrimoni dei nostri tempi_

**Chiara Lalli** (Università degli Studi di Cassino)

_I paradossi della sentenza 138/2010_

**Persio Tincani** (Università degli Studi di Bergamo)

_Difesa del matrimonio. Matrimonio omosessuale e principio del danno_

Ore 17.45  Discussione

**_La partecipazione è libera fino ad esaurimento posti_**
----------------------------------------------------------