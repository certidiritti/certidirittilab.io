---
title: 'MARTIN LUTHER KING DAY: PANNELLA E CERTI DIRITTI A REGINA COELI'
date: Mon, 19 Jan 2009 13:48:07 +0000
draft: false
tags: [Comunicati stampa]
---

MARTIN LUTHER KING DAY: MARCO PANNELLA RISPONDE ALL'APPELLO DEL PRESIDENTE OBAMA DI DEDICARE UNA GIORNATA ALLA PROPRIA COMUNITA', DALLE ORE 12 DI OGGI VISITA NEL CARCERE ROMANO DI REGINA COELI.  
  In occasione del Martin Luther King Day, il Presidente degli Stati Uniti Barak Obama sotto lo slogna 'Rinnoviamo l'America insieme', ha chiesto ai cittadini americani di dedicare la giornata di oggi, lunedì 19 gennaio, al servizio della propria comunità. Marco Pannella e il Partito Radicale Nonviolento Transnazionale e Transpartito, hanno risposto all'appello lanciando l'iniziativa agli iscritti e sostenitori del Partito Radicale Nonviolento.  
Marco Pannella, insieme a Diego Galli, Direttore del sito internet di Radio Radicale e Sharon Nizza, esponente dell'Associazione Radicale Certi Diritti,  si recheranno oggi, alle ore 12 nel Carcere  romano di Regina Coeli, dove svolgeranno per alcune ore attività di volontariato con i detenuti.  
   
Barack e Michelle Obama hanno annunciato che svolgeranno in prima persona attività di volontariato a Washington. Lo slogan dell'iniziativa è "Rinnoviamo l'America insieme". "Qualsiasi attività di servizio che organizzerai o a cui prenderai parte – si legge nel messaggio inviato via email a un indirizzario di 13 milioni di persone – pulire un parco, donare il sangue, fare volontariato per i senza tetto, o assistere un minore a rischio, puoi aiutare a iniziare questo importante viaggio.  
   
   
Sul sito internet del Partito Radicale è stato predisposto un modulo per raccogliere le adesioni, in cui si può dare un semplice sostegno ideale, oppure segnalare il luogo e l'organizzazione presso cui svolgere un'attività di volontariato:  
[http://www.radicalparty.org/supportobama/form.php](http://www.radicalparty.org/supportobama/form.php)