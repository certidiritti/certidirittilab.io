---
title: 'A Torino sabato 13 ottobre dibattito sulla prostituzione'
date: Thu, 11 Oct 2012 14:16:48 +0000
draft: false
tags: [Lavoro sessuale]
---

"Prostituzione: dalla legalizzazione di quella volontaria al contrasto di quella coatta. Esperienze europee a confronto" è il titolo del dibattito organizzato dalla consulta torinese per la laicità delle istituzioni in collaborazione con l'associazione radicale Certi Diritti e la cellula Coscioni di Torino.

  
**"Prostituzione: dalla legalizzazione di quella volontaria al contrasto di quella coatta. Esperienze europee a confronto"**

SABATO  
13 OTTOBRE 2012  
ore 15 - 19  
Sala delle Colonne,  
Municipio di Torino  
Piazza Palazzo di Città 1  
  

Programma

Presiede e conduce:  
**Tullio Monti**  
Coordinatore della Consulta Torinese per la Laicità delle Istituzioni

Introduzione:  
**Enzo Cucco**  
Presidente nazionale Associazione Radicale Certi Diritti  
  
**Alessandro Frezzato**  
Responsabile della Cellula torinese dell’Associazione Luca Coscioni

Partecipano:  
**Pia Covre**  
Comitato Diritti Civili delle Prostitute  
  
**Chiara Bertone**  
sociologa, Università del Piemonte Orientale  
  
**Daniela Danna**  
sociologa, Università di Milano  
  
**Sandeh Veet**  
Circolo di cultura GLBTQ Maurice  
  
**Rosanna Paradiso**  
Associazione Tampep  
  
[**Firma il manifesto appello per la  
legalizzazione della prostituzione >**](campagne-certi-diritti/itemlist/category/85-legalizzazione-prostituzione)