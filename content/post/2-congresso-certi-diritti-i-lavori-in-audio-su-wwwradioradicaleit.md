---
title: '2° CONGRESSO CERTI DIRITTI: I LAVORI IN AUDIO SU WWW.RADIORADICALE.IT'
date: Sun, 15 Mar 2009 08:28:32 +0000
draft: false
tags: [Comunicati stampa]
---

Su [www.radioradicale.it](http://www.radioradicale.it) Il 2° Congresso nazionale dell'Associazione Radicale Certi Diritti svoltosi a Bologna il giorno 14 marzo 2009 all'Hotel Europa.