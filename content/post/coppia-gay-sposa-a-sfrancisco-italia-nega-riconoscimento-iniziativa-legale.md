---
title: 'COPPIA GAY SPOSA A S.FRANCISCO, ITALIA NEGA RICONOSCIMENTO: INIZIATIVA LEGALE'
date: Mon, 07 Jul 2008 15:22:30 +0000
draft: false
tags: [Comunicati stampa]
---

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

"La scorsa settimana, a San Francisco, un cittadino italiano, naturalizzato Usa, A.Q., si è sposato con un cittadino Usa, B.S., una delle parti lese nella causa poi vinta per la decisione della Corte Suprema della California di eliminare la discriminazione che vietava il matrimonio tra persone dello stesso sesso. La causa è durata quattro anni e finalmente, a seguito della storica sentenza, la coppia ha potuto accedere all'istituto del matrimonio

Il cittadino italiano si è poi recato presso il consolato italiano per la trascrizione del suo matrimonio nel registro dell'Aire. Dopo due giorni gli uffici consolari italiani gli hanno reso la documentazione con la motivazione che in Italia il matrimonio si può fare solo tra persone di sesso diverso. Per lo Stato italiano egli è celibe nonostante sia regolarmente sposato in California.

Vista l'intenzione della coppia di vedersi riconosciuti i diritti di uguaglianza incardinando un'azione legale in Italia e in Europa, l'Associazione radicale Certi Diritti preannuncia sin da ora il suo pieno sostegno alla coppia neo sposata"