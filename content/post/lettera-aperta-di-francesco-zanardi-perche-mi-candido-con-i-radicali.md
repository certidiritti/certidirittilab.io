---
title: 'LETTERA APERTA DI FRANCESCO ZANARDI: PERCHE'' MI CANDIDO CON I RADICALI'
date: Sat, 13 Feb 2010 08:22:53 +0000
draft: false
tags: [Comunicati stampa]
---

Savona, 11 febbraio 2010

Lettera aperta di Francesco Zanardi.  
  
Care e cari amici,  
molti sono rimasti sorpresi della mia candidatura in Liguria, con la Lista Bonino-Pannella, per le prossime elezioni regionali. Sia i Verdi che i Radicali mi avevano chiesto nel corso del mio sciopero della fame se volevo essere candidato nelle loro liste.

Ho ritenuto di accettare di candidarmi nella Lista Bonino perché durante la mia iniziativa nonviolenta ho avuto dai radicali sostegno e aiuto di ogni genere. Mi hanno lasciato praticamente libero di gestire la mia azione senza assolutamente voler mettere il loro ‘cappello’ su quanto, con Manuel, abbiamo deciso di fare.  
L’aiuto dei radicali è iniziato ancora prima del 4 gennaio 2010, data di inizio del mio lungo sciopero della fame. Sergio Rovasio, Segretario dell’Associazione Radicale Certi Diritti, mi ha sempre aiutato e sostenuto in questa iniziativa. Poco prima di natale 2009 mi ha accompagnato in Questura per la notifica della manifestazione che avremmo poi svolto il 4 gennaio in Piazza Montecitorio, dove abbiamo dato inizio all’iniziativa nonviolenta.  
Grazie a Radio Radicale, che ha fatto le riprese audio-video di quell’evento, sono riuscito a far sapere a centinaia di migliaia di cittadini italiani della nostra iniziativa che soltanto due agenzie di stampa avevano annunciato. Al seguente link è possibile vedere quella manifestazione.  
[http://www.facebook.com/l/21df3;www.radioradicale.it/scheda/294306/conferenza-stampa-manifestazione-di-francesco-e-manuel-in-sciopero-della-fame-per-il-riconoscimento-della-](http://www.facebook.com/l/21df3;www.radioradicale.it/scheda/294306/conferenza-stampa-manifestazione-di-francesco-e-manuel-in-sciopero-della-fame-per-il-riconoscimento-della-)  
Persino il microfono, il palchetto e l’ombrellone me lo hanno prestato i radicali che hanno voluto così aiutarmi e sostenermi senza assolutamente voler strumentalizzare questo evento.  
In quei giorni ho anche parlato con Marco Pannella proprio per avere consigli e suggerimenti ed è stato uno dei pochissimi politici con cui sono riuscito a parlare. Come ho già ricordato su quasi mille parlamenari solo tre si sono degnati di spendere il loro tempo per la mia lotta: Ignazio Marino, Donatella Poretti e Paola Concia.  
Ho ritenuto quindi di candidarmi con la Lista Bonino-Pannella perché da sempre sono impegnati per le unioni civili e la libera sessualità delle persone. Si battono con i metodi della nonviolenza, la stessa che ho usato io per 35 giorni, affinché questo paese si metta ai passi dell’Europa.  
Spero che il vostro sostegno continui perché ho bisogno del vostro aiuto. Vi chiedo di starmi vicino così’ come avete fatto fino ad oggi. La nostra lotta ha ora una grande opportunità che ha bisogno di forza e sostegno di ogni tipo. Con Manuel siamo decisi e determinati a portare avanti questa lotta insieme a voi.  
Scrivetemi, mandatemi consigli, apriamo un confronto su quali proposte posso portare al Consiglio Regionale della Liguria per garantire diritti a chi non ne ha!  
Ho aperto un nuovo gruppo su Facebook con l’obbiettivo di fare informazione sulle varie situazioni regione per regione. [http://www.facebook.com/group.php?gid=295135768117](http://www.facebook.com/group.php?gid=295135768117)  
Grazie e forza!

Francesco Zanardi - [f.zanardi@gayitaliani.eu](mailto:f.zanardi@gayitaliani.eu)