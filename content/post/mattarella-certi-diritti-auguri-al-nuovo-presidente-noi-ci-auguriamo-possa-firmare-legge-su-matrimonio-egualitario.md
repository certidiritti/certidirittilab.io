---
title: 'Mattarella. Certi Diritti: auguri al nuovo Presidente, noi ci auguriamo possa firmare legge su matrimonio egualitario'
date: Tue, 03 Feb 2015 10:49:54 +0000
draft: false
tags: [Matrimonio egualitario]
---

[![20150201120221-mattarella](http://www.certidiritti.org/wp-content/uploads/2015/02/20150201120221-mattarella.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/02/20150201120221-mattarella.jpg)"'Garantire i diritti civili anche nella sfera personale e affettiva', queste le parole scelte dal Presidente Mattarella nel suo messaggio ai deputati, senatori e delegati regionali in seduta comune. A lui i nostri auguri di buon mandato, a noi l'augurio che sia lui il presidente che firmerà la legge sul matrimonio per tutti." Così Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti. "Le sentenze delle corti che dal 2010 ad oggi hanno scalfito un diritto di famiglia fossilizzato al 1975 chiedono un intervento del Legislatore a tutela di quei principi costituzionali sui quali oggi il Presidente ha giurato. Per Mattarella garantire la Costituzione è 'sostenere la famiglia, risorsa della società', adesso si restituisca urgentemente dignità a quelle coppie e a quelle famiglie condannate all'invisibilità"

Comunicato stampa dell'Associazione Radicale Certi Diritti