---
title: 'INTERROGAZIONE di Sonia ALFANO al Parlamento Europeo: Lotta all’omofobia ed alle discriminazioni basate sull’orientamento sessuale'
date: Wed, 03 Nov 2010 13:46:42 +0000
draft: false
tags: [Comunicati stampa, interrogazione parlamentare, OMOFOBIA, parlamento europeo, Sonia Alfano]
---

### [![europarlamento](http://www.certidiritti.org/wp-content/uploads/2010/11/europarlamento.jpg)](http://www.soniaalfano.it/parlamento/2010/11/03/lotta-allomofobia-ed-alle-discriminazioni-basate-sullorientamento-sessuale-nella-ue-e-ripetute-affermazioni-omofobe-del-presidente-del-consiglio-italiano/ "Permanent link to Lotta all’omofobia ed alle discriminazioni basate sull’orientamento sessuale nella UE e ripetute affermazioni omofobe del Presidente del Consiglio italiano")

Pubblichiamo l'interrogazione parlamentare sulle affermazioni omofobiche di Silvio Berlusconi depositata dall'onorevole **Sonia Alfano** (Italia dei Valori) al Parlamento europeo[](http://www.soniaalfano.it/parlamento/2010/11/03/lotta-allomofobia-ed-alle-discriminazioni-basate-sullorientamento-sessuale-nella-ue-e-ripetute-affermazioni-omofobe-del-presidente-del-consiglio-italiano/ "Permanent link to Lotta all’omofobia ed alle discriminazioni basate sull’orientamento sessuale nella UE e ripetute affermazioni omofobe del Presidente del Consiglio italiano")

In data 2 novembre 2010 in un’occasione pubblica il Presidente del Consiglio italiano Silvio Berlusconi, intervenendo a proposito della vicenda definita dalla stampa “Rubygate”, ha affermato pubblicamente: “meglio essere appassionato di belle ragazze che essere gay”. Tale affermazione, di chiara matrice omofoba, rappresenta solo la più recente di una lunga serie di esternazioni da parte di Berlusconi che mirano a offendere, facendone oggetto di pubblico scherno e derisione, le persone omosessuali.

Da anni ONG internazionali come Amnesty International, Human Rights Watch ed ILGA e nazionali come Arci Gay e **Certi Diritti** denunciano con preoccupazione l’incremento degli atti omofobi e di intolleranza nei confronti delle persone LGBT nel mondo e in Italia, mentre il Parlamento Europeo  ha più volte richiamato gli esponenti politici ad astenersi da affermazioni omofobiche che mettono a rischio la vita e l’incolumità delle persone LGBT. La Commissaria Viviane Reding ha affermato che attuerà una politica della “tolleranza zero” nei confronti dell’omofobia poichè contraria all’articolo 10 del TFUE che afferma che “l’Unione mira a combattere le discriminazioni fondate sull’orientamento sessuale”, all’art. 21 della Carta dei diritti fondamentali dell’Unione Europea che vieta le discriminazioni basate sull’orientamento sessuale ed all’articolo 19 del TUE che prevede che il Consiglio possa “prendere i provvedimenti opportuni per combattere le discriminazioni fondate sull’orientamento sessuale”. Allo stesso tempo, alcuni Stati membri, tra i quali l’Italia, pongono continui ostacoli all’adozione della direttiva orizzontale anti-discriminazioni, proposta dalla Commissione nel luglio 2008.

Considerando la situazione di divergenza tra i valori espressi dai Trattati e gli indirizzi politici dell’UE da una parte, e le affermazioni del Primo Ministro italiano Berlusconi e l’operato del governo italiano dall’altra:

\- qual é la posizione del Consiglio in merito all’omofobia nella UE e quali iniziative intende prendere per lottare contro questo fenomeno? A che punto sono i lavori del Consiglio in merito alla direttiva orizzontale contro le discriminazioni?

\- non ritiene il Consiglio che i rappresentanti delle istituzioni debbano astenersi da dichiarazioni pubbliche che abbiano un impatto negativo sulla situazione delle persone LGBT?

\- non ritiene il Consiglio necessario estendere l’applicazione della Decisione-Quadro 2008/913/GAI del 28 novembre 2008 sulla lotta contro il razzismo e la xenofobia, anche all’omofobia?

A questo link un intervento dell'onorevole Alfano sul suo blog: [**Berlusconi contro i gay: cosa ne pensano nell’UE?**](http://www.soniaalfano.it/blog/2010/11/03/berlusconi-contro-i-gay-cosa-ne-pensano-nell%E2%80%99ue/)