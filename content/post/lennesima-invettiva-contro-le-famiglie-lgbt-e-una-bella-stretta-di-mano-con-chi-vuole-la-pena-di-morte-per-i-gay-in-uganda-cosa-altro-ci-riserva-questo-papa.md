---
title: 'L''ennesima invettiva contro le famiglie lgbt e una bella stretta di mano con chi vuole la pena di morte per i gay in Uganda: cosa altro ci riserva questo papa?'
date: Fri, 14 Dec 2012 14:29:43 +0000
draft: false
tags: [Africa]
---

Gli consigliamo di pensarci dieci volte prima di dichiarare che le nostre famiglie feriscono qualcuno, lui sta ferendo milioni di persone incolpevoli, ed a rischio della vita.

Comunicato stampa Associazione radicale Certi Diritti

Roma, 14 dicembre 2012  
   
Con un tempismo utile per migliori cause il papa in poche ore riesce, per l'ennesima volta (e questa non è una notizia) a dichiarare che il matrimonio gay è una ferita alla giustizia e alla pace, e subito dopo stringe la mano alla speaker del Parlamento ugandese. la stessa che dichiarò, di fronte all'opinione pubblca mondiale, che ai cittadini ugandesi il Parlamnto avrebbe fatto un bel regalo di Natale, una legge nuova di zecca che prevede lapena di morte per gli omosessuali.  
   
Noi crediamo che questo papa, e sicuramen te il suo corpo diplomatico e i suoi portavoce, siano ormai senza vergogna e censura i peggiori sostenitori della repressione omosessuale nel mondo. A nulla valgono le tanto sperticate dichiarazioni sul vaticano che sarebbe contrario alla crim inalizzazione dell'omosessualità. Non solo il Vaticano si contraddice con le parole, ma anche coi fatti, accogliendo senza battere ciglio un tale campione del terrore e della repressione sessuale.  
   
Eppure sarebbe bastato un gesto, un piccolo gesto, lo stesso che il cerimoniale fa tutte le volte che riceve capi di stato e di governo o accogliendo nuovi ambasciatori, fino all'ultimo cercando di evitare divorziati, concubini e omosessuali. Bastava non stringere la mano alla Speaker ugandese perchè in Uganda il segnale sarebbe arrivato forte e chiaro a sostegno dei tanti deputati  che in quel paese cercano di resistere all'ondata fondamentalista e omofoba.  
   
Si vergogni il Papa, si vergnogni il Vaticano!. Sono loro che feriscono la libertà e il diritto di tutti, non solo dei milioni di omosessuali e transessuali nel mondo. No di certo le nostre famiglie!  
   
Enzo Cucco  
presidente Associazione radicale Certi Diritti