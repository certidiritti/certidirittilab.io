---
title: 'COPPIA GAY GIUNTA AL SESTO GIORNO DI SCIOPERO DELLA FAME: APPELLO'
date: Sat, 09 Jan 2010 09:03:07 +0000
draft: false
tags: [Comunicati stampa]
---

COPPIA GAY IN SCIOPERO DELLA FAME: APPELLO DELL’ASSOCIAZIONE RADICALE CERTI DIRITTI AL PRESIDENTE DELLA CAMERA DEI DEPUTATI E ALLA PRESDIENTE DELLA COMMISSIONE GIUSTIZIA.

COMUNICATO STAMPA DELL’ASSOCIAZIONE RADICALE CERTI DIRTTI

Roma, 9 gennaio 2010

Per la prima volta in Italia una coppia gay ha avviato una iniziativa nonviolenta per vedere riconosciuti i propri diritti, nel rispetto dell’eguaglianza e per l’affermazione della propria dignità.

Oggi, sabato 9 gennaio 2010, Francesco Zanardi e Manuel Incorvaia di Savona, sono giunti al sesto giorno di sciopero della fame nella quasi totale indifferenza mediatica. La loro iniziativa di dialogo è rivolta ai massimi rappresentanti delle istituzioni. Chiedono che vengano calendarizzate le varie proposte per il riconoscimento delle unioni civili e/o per il matrimonio gay.

Facciamo un appello al Presidente della Camera dei deputati, on. Gianfranco Fini e alla Presidente della Commissione Giustizia, on. Giulia Bongiorno, affinchè prendano contatti con Francesco e Manuel. Forse basterebbe questo per farli desistere da questa iniziativa.

Finora, a parte alcune rare eccezioni, la classe politica non ha manifestato alcuna attenzione a questa iniziativa nonviolenta. Sarebbe ora forse che qualcuno si muova per loro. E dia loro una qualche speranza.