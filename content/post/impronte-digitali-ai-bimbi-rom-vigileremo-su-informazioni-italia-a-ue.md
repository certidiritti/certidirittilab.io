---
title: 'IMPRONTE DIGITALI AI BIMBI ROM: VIGILEREMO SU INFORMAZIONI ITALIA A UE'
date: Tue, 08 Jul 2008 17:54:54 +0000
draft: false
tags: [Comunicati stampa]
---

**Il Senatore radicale Marco Perduca, dirigente Associazione Radicale Certi Diritti ha oggi dichiarato:  
  
**“Apprendiamo del chiarimento dei malintesi tra Maroni e il Commissario UE Jacques Barrot e dell’intenzione del ministro dell’Interno di fornire una documentazione dettagliata sull’attuazione delle misure adottate dal Governo italiano sul fronte della sicurezza.

Maroni sappia che noi radicali vigileremo affinché alla Commissione Europea non vengano trasmessi i soliti dati frutto di manipolazioni e mistificazioni, ma informazioni vere a partire dall’analisi del Centro d’Ascolto dell’Informazione Radiotelevisiva, che certifica la costruzione ad arte dell’emergenza rom e sicurezza in Italia, attraverso l’informazione gonfiata sugli episodi di violenza e criminalità”, ha dichiarato il senatore Perduca da Piazza dell’Esquilino, dove sta partecipando alla manifestazione “Prendetevi le nostre impronte”, organizzata da Arci e Aned, contro le discriminazioni nei riguardi del popolo rom.