---
title: '2008-0140-Discrimination-ST13496.EN09'
date: Mon, 21 Sep 2009 11:57:32 +0000
draft: false
tags: [Senza categoria]
---

  

asda  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 21 September 2009

Interinstitutional File:

2008/0140 (CNS)

13496/09

LIMITE

SOC 530

JAI 605

MI 346

  

  

  

  

  

NOTE

from :

General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

13049/09 SOC 504 JAI 564 MI 326

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached a note from the Finnish delegation with a view to the meeting of the Social Questions Working Party on 22 September 2009.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Note from Finland regarding the Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation - preliminary written comments on certain recitals included in doc. 13238/09 and on point (b) of Ireland's Proposal 3 (doc. 13046/09)**

Finland supports the Directive proposed which aims to guarantee to all persons a consistent minimum protection against discrimination, regardless of the discrimination ground, and the area in which the discrimination is considered to have occurred.

The following comments include the comments presented at the meeting of the Social Questions Working Party of 7 September 2009 with regard to Recitals 15, 17b and 17c of as well as the comments concerning Recital 17d (doc. 12792/09, now doc. 13238/09). Furthermore, it provides some further explanation of our preliminary comments on the Irish proposal concerning Article 2(6)(b) (doc. 13046/09, Proposal 3).

These comments are without prejudice to any further comments or proposals to be presented in the course of future discussions.

Text proposal for Recital 15:

At the meeting of the SQWP of 7 September 2009, the Finnish delegation expressed its view that the text of Recital 15 does not appear to be fully consistent with the wording of Article 2(7) of the draft Directive. Therefore we would suggest the following wording to be used in Recital 15.

"**15\. Actuarial and risk factors related to disability and to age are used in the provision of insurance, banking and other financial services. In the provision of these services, proportionate differences in treatment where, for the product in question, the use of age or disability is a determining factor in the assessment of risk based on relevant actuarial principles, accurate statistical data or medical knowledge shall not be considered discrimination for the purposes of this Directive.**"

  

Recital 17b:

Finland has doubts as to whether it is possible to fully separate the concept "access to" from the concepts of "conditions of eligibility" and "the substance of benefits".

Furthermore, the reference made in Recital 17b to benefits "funded by the State" might result in a situation where some complementary/parallel forms of social security would not be covered. It is our preliminary view that instead of using the criterion “provided directly by the State or (...) funded by the State”, the criterion to be used is whether the schemes in question are statutory or not. It should be taken into account that it is not always the state that provides these types of benefits, as municipalities and sometimes even private parties could provide the rights and benefits in question.

Finland notes that while Regulation 883/2004 is directly applicable in all the Member States, it is a coordinating Regulation and thus an entity separate from the content and the organisation of the national social security systems. With regard to the reference made to Regulation 883/2004 in the last sentence of Recital 17b, Finland would like to point out that the branches of social security covered by Regulation 883/2004 do not fully correspond to the national social security classification. For instance, reference to Regulation 883/2004 does not make it clear whether social services are covered. Furthermore, as also noted in Recital 17f, the Member States have the exclusive competence to organise their social protection systems (including social security). In the light of all this, Finland is of the view that the reference made to Regulation 883/2004 in the last sentence of Recital 17b does not add clarity to the text. Thus, Finland proposes that the reference made to Regulation 883/2004 and the last sentence of Recital 17b be deleted from the text. It is our concern that the inclusion of a reference to "the branches of social security defined by Regulation 883/2004" in a Directive which must be implemented in the national legislation, might further contribute to the infiltration of the classification used in Regulation 883/2004 into the national social security legislation.

  

Therefore it is proposed that Recital 17b be amended as follows:

(17b) This Directive covers access to social protection, which includes social security**, \[…\]** social assistance, **\[…\] and** health care, thereby providing comprehensive protection against discrimination in this field. **Consequently, the Directive applies with regard to (access to) rights and benefits which are derived from statutory schemes of general or special social security, social assistance and health care, whether these are provided by public authorities or by private parties in line with national legislation.** In this context, the Directive applies with regard to **\[…\]** benefits **in cash**, benefits in kind and services, irrespective of whether the schemes involved are contributory or non-contributory.

Finally, it is not clear to us whether the current formulations used in Article 3(1) and (2) of the Directive Proposal and the related Recitals cover social services.

Recital 17d:

It is the preliminary opinion of Finland that the present formulation of Recital 17d could be understood as suggesting that the freedom of contract is an absolute right for individuals not engaged in commercial or professional activity. However, in legal doctrine, freedom of contract is not an unqualified right but may be restricted in the interest of public policy, for instance in the interest of the protection of rights and health of individuals. Without disagreeing with the purpose of this recital in general, Finland would like to suggest  a slight revision of the first sentence,  such as replacing it with a sentence that reads **“Freedom of contract, including an individual’s freedom to choose a contractual partner for a transaction, is a generally recognized principle of law.**" Thus Recital 17d would read as follows:

**“Freedom of contract, including an individual’s freedom to choose a contractual partner for a transaction, is a generally recognized principle of law.** This Directive should not apply to economic transactions undertaken by individuals for whom these transactions do not constitute a professional or commercial activity. In this context, the concept of professional or commercial activity may be defined in accordance with the national laws and practice of the Member States."

  

The note from the Irish delegation (doc. 13046/09) - Proposal 3 (re. Article 2(6)(b)):

Finland has expressed its reservations concerning the proposal not to provide legal protection against age discrimination to persons under 18 years of age. We do understand that there is a legitimate need to allow for differences of treatment of minors where this is objectively and reasonably justified. This, however, is already fully taken into account in Article 2(6), and does not require the general exclusion of children and young people from protection against age discrimination. The explanation accompanying the Irish proposal states that the case for the existence of unequal treatment on the ground of age has not been made with respect to children and young people. The empirical evidence from Finland does not support such a conclusion, as there have been cases where children have been denied access, without an objective justification, for instance to grocery stores on account of their age. We would also like to point out that none of the applicable international and European instruments relating to the protection from age discrimination, including notably Directive 2000/78/EC, the EU Charter on Fundamental Rights, the European Convention on Human Rights and the UN Convention on the Rights of the Child, contains similar restriction clauses. This entails that the introduction of such a restriction in this Directive would in practice represent an entirely unprecedented development. Furthermore, the age 18 appears arbitrary in the international context, as the age of majority is not internationally agreed upon and varies from country to country between 9 and 25 years, and may in any case be subject to change in Europe as well. In conclusion, it is the preliminary opinion of Finland that differential treatment on the grounds of age, in a document that aims to eliminate unjustified differential treatment on the grounds of age, requires a very weighty justification. We are not convinced that such a justification has yet been presented.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_