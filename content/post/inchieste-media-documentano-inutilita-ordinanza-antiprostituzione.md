---
title: 'INCHIESTE MEDIA DOCUMENTANO INUTILITA'' ORDINANZA ANTIPROSTITUZIONE'
date: Fri, 10 Jul 2009 07:02:20 +0000
draft: false
tags: [Comunicati stampa]
---

**PROSTITUZIONE: LA PRESA IN GIRO DELL’ORDINANZA ANTIPROSTITUZIONE DI ROMA ALTRO NON E’ CHE PROPAGANDA UTILE A CLANDESTINIZZARE ANCORA DI PIU’ IL FENOMENO E COLPIRE MAGGIORMENTE LE PERSONE INDIFESE. LE NOSTRE PROPOSTE.**

Roma, 10 luglio 2009

_**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**_

“Quanto documentato in questi giorni da alcuni media (Corriere della Sera di oggi, venerdì 10 luglio e l’Avvenire di lunedì 6 luglio) sul fenomeno della prostituzione nella città di Roma, fenomeno fiorente più che mai nelle strade della Capitale, dimostra, se ancora ve ne fosse bisogno, che la delibera antiprostituzione, approvata con fanfare e fanfaroni circa 10 mesi fa, non era altro che propaganda ispirata alla più becera demagogia e populimo d’accatto.

L’ordinanza ha prodotto una maggiore clandestinizzazione del fenomeno, abbassato i prezzi delle prestazioni delle prostitute, aumentato le violenze e i soprusi contro le donne che si prostituiscono, senza denunce alle autorità; ha reso ancora più invisibile lo sfruttamento, i ricatti e le angherie verso persone deboli e povere.

La nostra proposta prevede che il fenomeno venga regolamentato con norme chiare e sicure in ambito sociale, sanitario e fiscale con iniziative di informazione e prevenzione, creando specifiche task force per combattere lo sfruttamento e la tratta delle persone e garantire assistenza e protezione alle persone vittime dello sfruttamento. Non ci sembrano proposte impossibili, certamente non demagogiche”.