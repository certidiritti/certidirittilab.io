---
title: 'DEPENALIZZAZIONE OMOSESSUALITA'' ONU: IPOCRISIA E FALSITA'' VATICANO'
date: Tue, 02 Dec 2008 13:28:37 +0000
draft: false
tags: [Comunicati stampa, DEPANALIZZAZIONE, IPOCRISIA, ONU, RATZINGER, vaticano]
---

**DEPENALIZZAZIONE OMOSESSUALITA’ ALL’ONU: LA VICINANZA DEL VATICANO ALLE TEOCRAZIE E AI REGIMI CHE PERSEGUITANO ANCHE CON LA MORTE GLI OMOSESSUALI E’ IN NETTO CONTRASTO CON QUANTO PREDICATO DALL’ALA PIU’ CONSERVATRICE DELLA CHIESA CATTOLICA. ENNESIMO ESEMPIO DI IPOCRISIA E FALSITA’.**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

“Quanto esplicitato ieri dal Vaticano è l’ennesima dimostrazione delle politiche ipocrite e omofobe delle gerarchie di potere del Vaticano che vanno persino in contrasto con l’ala più conservatrice della chiesa cattolica.

Difatti, al punto 10 della Lettera ai Vescovi della chiesa cattolica sulla cura pastorale delle persone omosessuali, che prendeva una posizione molto severa sulla questione, diffusa nell’ottobre 1986 dall’allora Prefetto della Congregazione per la dottrina della fede (ex Sant’uffizio), Cardinale Jospeh Ratzinger, si legge al punto 10):

‘Va deplorato con fermezza che le persone omosessuali siano state e siano ancora oggetto di espressioni malevole e di azioni violente. Simili comportamenti meritano la condanna dei pastori della Chiesa, ovunque si verifichino. Essi rivelano una mancanza di rispetto per gli altri, lesiva dei principi elementari su cui si basa una sana convivenza civile. La dignità propria di ogni persona dev'essere sempre rispettata

nelle parole, nelle azioni e nelle legislazioni’.

E’ certo gravissimo quanto comunicato ieri dal Vaticano contro le persone omosessuali, ma ancora più grave è questo ennesimo atteggiamento di dissociazione tra il predicato e il praticato; assistiamo, per l’ennesima volta, alla promozione di falsità con conseguente danno sul fronte dei diritti civili e umani di milioni di persone sulla terra; perseguitate, imprigionate e uccise per legge dai regimi autoritari e teocratici tanto cari al Vaticano”.