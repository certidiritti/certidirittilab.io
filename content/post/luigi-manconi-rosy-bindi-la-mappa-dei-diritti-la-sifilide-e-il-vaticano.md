---
title: 'Luigi Manconi, Rosy Bindi, la mappa dei diritti, la sifilide e il Vaticano'
date: Sat, 25 Aug 2012 13:56:12 +0000
draft: false
tags: [Politica]
---

![LogoCD](http://www.radicalparty.org/file/Logo_Cd_2012.JPG)

[**Certi Diritti insieme ad altre associazioni radicali aderiscono alla manifestazione di oggi a Roma**](http://www.certidiritti.org/2012/06/21/associazione-radicale-certi-diritti-e-altre-associazioni-radicali-aderiscono-a-manifestazione-contro-omofobia-e-gay-pride-di-roma/) contro l’omofobia dopo le gravi violenze avvenute la settimana scorsa nella capitale e al Roma Pride 2012 che sfilerà sabato pomeriggio.

Lo slogan di quest’anno è ‘Vogliamo tutto’. Leggi il documento politico >

Intanto al Consiglio regionale del Lazio Radicali, Sel e Pd hanno chiesto [**la calendarizzazione delle proposte di legge contro l'omofobia**](http://www.certidiritti.org/2012/06/21/consiglieri-radicali-sel-e-pd-intervenuti-al-consiglio-regionale-del-lazio-contro-violenza-omofobica-e-a-sostegno-roma-pride/) depositate da quasi due anni.

Certi Diritti denuncia: il farmaco contro la sifilide torna ad essere mutuabile (da fascia 'C' è passato alla fascia 'A') ma e’ ancora introvabile a Roma nonostante l’aumento preoccupante dei casi di sifilide.[**In Vaticano lo si trova ad un prezzo decuplicato >**](http://www.certidiritti.org/2012/06/21/il-farmaco-contro-la-sifilide-torna-ad-essere-mutuabile-ma-introvabile-lo-si-trova-pero-in-vaticano-a-un-prezzo-decuplicato/)

A Milano a seguito della[** lettera a Pisapia **](http://www.certidiritti.org/2012/06/15/se-il-vigile-urbano-ti-chiama-ricchione/)inviata da Leonardo Monaco della Segreteria di CertiDiritti, che denunciava il comportamento omofobo di un Vigile Urbano,[** risponde l’Assessore alla Sicurezza e Coesione sociale Marco Granelli. Presto i due si incontreranno**](http://www.certidiritti.org/2012/06/16/lettera-aperta-a-pisapia-la-risposta-dellassessore-granelli/).

Sempre a Milano l’avv. Massimo Clara del Direttivo di certi Diritti ha partecipato all’incontro “Riforme per una Milano laica ed europea” [**Ascolta su Radio Radicale >**](http://www.radioradicale.it/scheda/354762/riforme-per-una-milano-laica-ed-europea)  
La Giunta della Provincia di Gorizia ha approvato all’unanimità la delibera “Contrasto alla discriminazione basata su orientamento sessuale e identità di genere”. [**Plauso delle associazioni tra cui Certi Diritti.**](http://www.certidiritti.org/2012/06/19/provincia-di-gorizia-approva-allunanimita-documento-contro-la-discriminazione-lgbt/)

A Sassari all'incontro che anticipa la[** manifestazione prevista per il 23 giugno**](http://www.certidiritti.org/2012/06/19/provincia-di-gorizia-approva-allunanimita-documento-contro-la-discriminazione-lgbt/), promossa dal Mos - Movimento Omosessuale Sardo, ha partecipato Yuri Guaiana, segretario nazionale di CertiDiritti. [**Video-intervista >**](https://www.youtube.com/watch?feature=player_embedded&v=M_qyb5_6OYc)

[**L'impegno contro l'omofobia e transfobia in Europa**](http://www.agenziaradicale.com/index.php?option=com_content&task=view&id=14103&Itemid=53) è il titolo della conferenza stampa organizzata dall’associazione Radicale "Certi Diritti" e da Amnesty International. [**Ascolta su radio Radicale >**](http://www.radioradicale.it/scheda/354501/limpegno-contro-lomofobia-e-transfobia-in-europa)

Nei partiti continua la discussione sul matrimonio tra persone dello stesso sesso e nel PD[**l'incredibile Rosy Bindi dice ai Vescovi l'opposto di quanto detto da Bersani una settimana fa al Gay Pride. Sotto il comunicato trovi il documento della commissioneDiritti civili del Pd >**](http://www.certidiritti.org/2012/06/17/lincredibile-rosy-bindi-che-dice-ai-vescovi-lopposto-di-quanto-detto-da-bersani-al-gay-pride/)

[**Sul nostro sito puoi scaricare anche la mappa ILGA dei diritti lgbti nel mondo. CertiDiritti ha curato la versione italiana >**](http://www.certidiritti.org/2012/06/21/e-uscita-ledizione-2012-della-mappa-ilga-sui-diritti-lgbti-nel-mondo-scarica-la-versione-italiana-curata-dallassociazione-radicale-certi-diritti/)

Il nostro sito ospita il contributo alla discussione sui matrimoni gay del direttore di Tusciaweb che confessa: [**sono quattro anni che vivo con un uomo …**](http://www.certidiritti.org/2012/06/19/il-direttore-di-tusciaweb-si-confessa-sono-quattro-anni-che-vivo-con-un-uomo/)

Su Liberi.tv diritti in libertà. **[Approfondimento LGBT a cura di Riccardo Cristiano, conduce Gianni Colacione >](http://www.liberi.tv/webtv/2012/06/16/video/diritti-libert%C3%A0-approfondimento-lgbt-cura-riccardo-cristiano)**

Emma Bonino e i parlamentari radicali, Luigi Manconi, Mina Welby, Ivan Scalfarotto, Marilisa D’Amico e tanti altri si sono iscritti all’associazione radicale Certi Diritti anche nel 2012. [**Fallo anche tu >**](partecipa/iscriviti)

Non riceviamo finanziamenti pubblici e ha bisogno del tuo aiuto per portare avanti le battaglie controla sessuofobia e per i diritti civili.   
[**Sostieni Certi Diritti acquistando i nostri libri, le spillette e le shopping bag > **](http://www.facebook.com/media/set/?set=a.10151028265855973.481144.116671870972&type=1)

[www.certidiritti.org](http://www.certidiritti.org/)