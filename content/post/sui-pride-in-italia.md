---
title: 'Sui Pride in Italia'
date: Mon, 14 Oct 2013 07:41:45 +0000
draft: false
tags: [Movimento LGBTI]
---

Nota dell'Associazione Radicale Certi Diritti

Roma, 14 ottobre 2013

Da qualche tempo l'Associazione radicale Certi Diritti sostiene che in Italia non ci sia più bisogno di qualificare come "nazionale" uno tra i tanti Pride che nel Paese si svolgono. Non è così infatti nel resto del mondo dove i Pride si possono realizzare, ed appare sempre più anacronistico l'uso stesso di una parola - nazionale - le cui assonanze rinviano a tematiche che ci vedono perlomeno distanti. Così come il nostro Paese ha dimostrato che se le associazioni, soprattutto a livello locale, sono in grado di creare sinergie reali, il successo è assicurato, non soltanto dal punto di vista "groove" della manifestazione, ma anche dal punto di vista politico, sociale e culturale.

Infine, in assenza di adeguate sedi ove l'associazionismo possa assumere decisioni in merito secondo le più semplici regole della democrazia rappresentativa, ogni decisione relativa a questa scelta rischia di essere ostaggio di spinte distruttive, al di fuori degli interessi e dei bisogni della nostra comunità.

Ci asteniamo quindi , da oggi in avanti, dal prendere parte ad iniziative di confronto che abbiano come obiettivo quello di scegliere una sede nazionale per il Pride. Ed invitiamo le altre associazioni italiane a riflettere sui potenziali effetti negativi di incontri e scelte sul carattere "nazionale" dei Pride italiani, con le conseguenze potenzialmente nefaste sull'esile futuro, e ancora più esile incisività, del nostro movimento.

Speriamo affinché i Pride si diffondano in tutto il Paese con le caratteristiche e gli obiettivi che ciascuna comunità locale e realtà nazionale saprà creare, nell'interesse di tutti e tutte. Ed offriremo il nostro contributo secondo le nostre capacità, senza pregiudizio geografico, tematico o politico.