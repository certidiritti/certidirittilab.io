---
title: 'Uganda: torna in Parlamento legge contro omosessualità che prevede anche pena di morte. Governo italiano intervenga'
date: Thu, 22 Nov 2012 08:20:14 +0000
draft: false
tags: [Africa]
---

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Roma, 22 novembre 2012

Ieri il Parlamento ugandese a messo in ordine del giorno la proposta di legge, presentata da David Bahati il 14 ottobre 2009, per inasprire le pene contro l’omosessualità come aveva promesso nei giorni scorsi la presidente del Parlamento ugandese. Rebecca Kadaga ha anche promesso che la legge passerà entro il 15 dicembre, quando il Parlamento ugandese andrà in vacanza, come regalo di Natale al popolo ugandese che chiederebbe pressantemente “di risolvere il problema della promozione dell’omosessualità e del reclutamento dei minori”

La legge messa in ordine del giorno prevede addirittura la pena di morte per il reato di “omosessualità aggravata” che si configura quando un rapporto omosessuale viene consumato sotto l’effetto di alcool o stupefacenti, con un minore, un una persona sieropositiva, con una persona in una posizione di autorità sull’altra, con un disabile o se il rapporto omosessuale viene consumato ripetutamente. Il semplice reato di omosessualità prevede invece la pena dell’ergastolo e include qualunque atto omosessuale o il tentativo di contrarre un matrimonio omosessuale.

Questa proposta di legge è già stata condannata dai governi britannico, canadese, francese, svedese, tedesco e americano che hanno anche minacciato di ridurre gli aiuti destinati all’Uganda. Il 16 dicembre 2009 anche il Parlamento Europeo ha votato una risoluzione contro questa proposta di legge. Attualmente le relazioni tra persone dello stesso sesso sono punite in Uganda con 14 anni di prigione.

Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti, dichiara: “Siamo di fronte a un gravissimo attentato ai diritti umani e a un insulto alla memoria di David Kato Kisule, leader del movimento LGBTI ugandese e iscritto all’Associazione Radicale Certi Diritti, che venne ucciso mentre combatteva proprio contro questa legge. È incredibile che il Parlamento ugandese si occupi di questa proposta con questa disumana ostinazione quando il Paese è afflitto dal traffico degli esseri umani, da scandali di corruzione, in cui sono stati coinvolti la Presidenza del Consiglio ugandese e il Ministero per i Servizi Pubblici, nonché dalla soppressione della libertà di stampa nel Paese. Questi sono i problemi reali che il Parlamento dovrebbe affrontare. Insieme al Senatore Marco Perduca, Segretario della Commissione straordinaria per la tutela e la promozione dei diritti umani, ho scritto al Presidente del Consiglio Mario Monti e al Ministro degli Affari Esteri Giulio Terzi di Sant’Agata per chiedere un’immediata e decisa condanna di questa proposta di legge e di prendere tutte le misure necessarie per cercare d’impedirne l’approvazione”.

[FIRMA LA PETIZIONE PER FERMARE LA LEGGE SU ALLOUT.ORG >](http://www.allout.org/uganda-now?akid=965.622005.rzaeyG&rd=1&t=5&utm_campaign=uganda-now&utm_content=english&utm_medium=email&utm_source=actionkit)

**Nella foto David Kato Kisule, attivista gay ugandese e iscritto dell'Associazione radicale Certi Diritti assassinato a Kampala il 26 gennaio 2011.  
[In memory of David Kato Kisule >](david-kato)  
**