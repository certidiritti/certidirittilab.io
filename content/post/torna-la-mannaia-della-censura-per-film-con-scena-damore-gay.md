---
title: 'TORNA LA MANNAIA DELLA CENSURA PER FILM CON SCENA D''AMORE GAY'
date: Thu, 16 Oct 2008 13:29:39 +0000
draft: false
tags: [Comunicati stampa]
---

**LA CENSURA TORNA DI VOGA NELL’ ITALIA BACCHETTONA E FONDAMENTALISTA: IL FILM “UN ALTRO PIANETA”, PREMIATO A VENEZIA, RISCHIA DI ESSERE VIETATO AI 18ENNI PER UNA SCENA DI AMORE GAY. PROIETTERMO IL FILM E CI AUTODENUNCEREMO.**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

“Sembrava che in Italia l’ufficio censura dei film attraversasse un periodo di crisi anche per la maggiore consapevolezza che raccontare la realtà con film di qualità non vuol dire necessariamente essere pornografi o dispensatori di gratuite volgarità. Invece, nell’era  in cui si ridiscutono le conquiste raggiunte nel corso degli ultimi decenni ecco che il film “Un altro pianeta”, di Stefano Tummolini, con Antonio Merone protagonista, rischia di essere distribuito con il divieto di visione ai minori di 18 anni. Ma che Italia è mai questa? Più che all’Europa potremmo chiedere di far parte della Lega dei paesi fondamentalisti guidati dall’Iran.

Il film, premiato a Venezia con il Queer Lion Award, uscirà nelle sale i primi giorni di novembre, ci auguriamo senza censura altrimenti lo proietteremo pubblicamente, senza divieto per i minori, autodenunciandoci".