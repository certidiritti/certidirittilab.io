---
title: 'Ministro della Sanità si direbbe contrario a discriminazioni gay su donazione sangue: applichi Direttiva Europea'
date: Sun, 06 Nov 2011 19:22:23 +0000
draft: false
tags: [Politica]
---

Anche il ministro della Sanità sostiene che vietare di donare il sangue alle persone gay equivale ad un'odiosa discriminazione. Chi è a rischio lo è indipendentemente dall'orientamento sessuale. Anzichè attendere i pareri, il ministro applichi la circolare ministeriale 13.4.2005, oltre che il rispetto della direttiva della Commissione Europea.

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti

"Anche il Ministro della Sanità ha dischiarato oggi che non trova giusto vietare la donazione del sangue ad una persona omosessuale e che il Ministero si deve avvalere del parere delle opinioni dei poteri consultivi. Basterebbe ricordare a tutti gli incompetenti sulla materia che le persone a rischio sono tali indipendentemente dal proprio orientamento sessuale. Segnaliamo anche al Ministro e a chi al Policlinico ha vietato di donare il sangue ad una ragazza, perchè lesbica, che esiste una Direttiva della Commissione Europea (2004/33/EC) dove si precisa che i gruppi a rischio sono coloro che 'hanno comportamenti sessuali a rischio' indipendentemente dal loro orientamento sessuale, così come previsto dal Decreto Ministeriale 13.4.2005 All. 4.

A tutti gli ignoranti delle disposizioni europee e dello stesso Ministero della Sanità segnaliamo che vietare alle persone omosessuali di donare il sangue, senza tenere conto di logica e intelligenza, e delle stesse indicazioni scientifiche, equivale a promuovere ed alimentare forme di odio e discriminazioni inaccettabili".

“Sexual behaviour. Persons whose sexual behaviour puts them at high risk of acquiring severe infectious diseases that can be transmitted by blood”. \[Comportamenti sessuali. Persone i cui comportamenti sessuali siano tali da porle a rischio di acquisire gravi malattie infettive che possano essere trasmesse per via ematica\]

Consigliamo ai rappresentanti sanitari, che sembrano ispirati da una grave forma di talebanismo di stampo lumbard, di aggiornare le direttive datate 1985 che effettivamente prevedevano il divieto della donazione del sangue alle persone omosessuali, era all’incirca 25 anni fa! Nel frattempo qualcosa è cambiato nel mondo, persino il Ministro della Salute, Francesco Storace, nel 2005, di fronte ad un caso analogo, dispose l'apertura di un'inchiesta per accertare responsabilità amministrative o comportamenti sanzionabili penalmente dopo il rifiuto del Policlinico di Milano di prelevare il sangue ad un omosessuale donatore. "Quanto accaduto al Policlinico di Milano - disse il ministro - é inaccettabile e potrebbe configurare l'esistenza di un reato".