---
title: '2008-140-DiscriminationCOM-Proposal'
date: Wed, 02 Jul 2008 08:35:07 +0000
draft: false
tags: [Senza categoria]
---

EN
--

  

COMMISSION OF THE EUROPEAN COMMUNITIES

Brussels, 2.7.2008

COM(2008) 426 final

2008/0140 (CNS)

Proposal for a

COUNCIL DIRECTIVE

on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

(presented by the Commission)  
  
  
{SEC(2008) 2180}  
{SEC(2008) 2181}

  

EXPLANATORY MEMORANDUM

1.           Context of the proposal

**Grounds for and objectives of the proposal**

The aim of this proposal is to implement the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation outside the labour market. It sets out a framework for the prohibition of discrimination on these grounds and establishes a uniform minimum level of protection within the European Union for people who have suffered such discrimination.

This proposal supplements the existing EC legal framework under which the prohibition of discrimination on grounds of religion or belief, disability, age or sexual orientation applies only to employment, occupation and vocational training[\[1\]](#_ftn1).

**General context**

The Commission announced in its legislative and work programme adopted on 23 October 2007[\[2\]](#_ftn2) that it would propose new initiatives to complete the EU anti-discrimination legal framework.

The current proposal is presented as part of the ‘Renewed Social Agenda: Opportunities, access and solidarity in 21st century Europe'[\[3\]](#_ftn3), and accompanies the Communication ‘Non-Discrimination and Equal Opportunities: A Renewed Commitment’[\[4\]](#_ftn4).

The UN Convention on the Rights of Persons with Disabilities has been signed by the Member States and the European Community. It is based on the principles of non-discrimination, participation and inclusion in society, equal opportunities and accessibility_._ A proposal for the conclusion of the Convention by the European Community has been presented to the Council[\[5\]](#_ftn5).

**Existing provisions in the area of the proposal**

This proposal builds upon Directives 2000/43/EC, 2000/78/EC and 2004/113/EC[\[6\]](#_ftn6) which prohibit discrimination on grounds of sex, racial or ethnic origin, age, disability, sexual orientation, religion or belief[\[7\]](#_ftn7). Discrimination based on race or ethnic origin is prohibited in employment, occupation and vocational training, as well as in non-employment areas such as social protection, health care, education and access to goods and services, including housing, which are available to the public. Discrimination based on sex is prohibited in the same range of areas, with the exception of education and media and advertising. However, discrimination based on age, religion and belief, sexual orientation and disability is prohibited only in employment, occupation and vocational training.

Directives 2000/43/EC and 2000/78/EC had to be transposed into national law by 2003, with the exception of those provisions dealing with age and disability discrimination, for which an extra three years was available. A report on the implementation of Directive 2000/43/EC was adopted by the Commission in 2006[\[8\]](#_ftn8) and a report on the implementation of Directive 2000/78/EC was adopted on 19 June 2008[\[9\]](#_ftn9). All except one Member State have transposed these directives. Directive 2004/113/EC had to be transposed by the end of 2007.

As far as possible, the concepts and rules provided for in this proposal build on those used in the existing Directives based on Article 13 EC.

**Consistency with other policies and objectives of the Union**

This proposal builds upon the strategy developed since the Amsterdam Treaty to combat discrimination and is consistent with the horizontal objectives of the European Union, and in particular with the Lisbon Strategy for Growth and Jobs and the objectives of the EU Social Protection and Social Inclusion Process. It will help to further the fundamental rights of citizens, in line with the EU Charter of Fundamental Rights.

2.           Consultation of interested parties and impact assessment

**Consultation**

In preparing this initiative, the Commission sought to associate all stakeholders with a potential interest and care was taken to ensure that those who might want to comment would have the opportunity and time to respond. The European Year of Equal Opportunities for All provided a unique opportunity to highlight the issues and encourage participation in the debate.

Particular mention should be made of the public on-line consultation[\[10\]](#_ftn10), a survey of the business sector[\[11\]](#_ftn11), and a written consultation of, and meetings with, the social partners and European level NGOs active in the non-discrimination field[\[12\]](#_ftn12). The results of the public consultation and that of the NGOs were a call for legislation at EU level to increase the level of protection against discrimination although some argued for ground-specific directives in the area of disability and of sex. The European Business Test Panel consultation indicated that businesses believe it would be helpful to have the same level of protection from discrimination across the EU. The social partners representing business were against new legislation in principle, which they saw as increasing red tape and costs, while the trade unions were in favour.

The responses to the consultation highlighted concerns about how a new Directive would deal with a number of sensitive areas and also revealed misunderstandings about the limits or extent of Community competence. The proposed Directive addresses these concerns and makes explicit the limits of Community competence. Within these limits the Community has the power to act (Article 13 EC Treaty) and believes that action at EU level is the best way forward.

The responses also emphasised the specific nature of disability-related discrimination and the measures needed to address it. These are addressed in a specific Article.

Concerns have been expressed that a new Directive would bring costs for business but it should be emphasised that this proposal builds largely on concepts used in the existing directives with which economic operators are familiar. As to measures to deal with disability discrimination, the concept of reasonable accommodation is familiar to businesses since it was established in Directive 2000/78/EC. The Commission proposal specifies the factors to be taken into account when assessing what is 'reasonable'.

It was pointed out that, unlike the other two Directives, Directive 2000/78/EC does not require Member States to establish equality bodies. Attention was also drawn to the need to tackle multiple discrimination, for example by defining it as discrimination and by providing effective remedies. These issues go beyond the scope of this Directive but nothing prevents Member States taking action in these areas.

Finally, it was pointed out that the scope of protection from sex discrimination under Directive 2004/113/EC is not as extensive as in Directive 2000/43/EC and that this should be addressed in new legislation. The Commission does not take up this suggestion now since the date for transposition of Directive 2004/113/EC has only just passed. However the Commission will report in 2010 on the Directive’s implementation and can propose modifications then, if appropriate.

**Collection and use of expertise**

A study[\[13\]](#_ftn13) in 2006 showed that, on the one hand, most countries provide legal protection in some form that goes beyond the current EC requirements in most of the areas examined, and on the other hand, there was a good deal of variety between countries as to the degree and nature of the protection. It also showed that very few countries carried out ex-ante impact assessments on non-discrimination legislation. A further study[\[14\]](#_ftn14) looked at the nature and extent of discrimination outside employment in the EU, and the potential (direct and indirect) costs this may have for individuals and society.

In addition, the Commission has used the reports from the European Network of Independent Experts in the non-discrimination field, notably their overview ‘Developing Anti-Discrimination Law in Europe‘[\[15\]](#_ftn15) as well as a study on ’Tackling Multiple Discrimination: practices, policies and laws’[\[16\]](#_ftn16).

Also relevant are the results of a special Eurobarometer survey [\[17\]](#_ftn17) and a Eurobarometer flash survey in February 2008[\[18\]](#_ftn18).

**Impact assessment**

The impact assessment report[\[19\]](#_ftn19) looked at evidence of discrimination outside the labour market. It found that, while non-discrimination is recognised to be one of the fundamental values of the EU, in practice the level of legal protection to secure these values differs between Member States and between discrimination grounds. As result, those at risk of discrimination often find themselves less able to participate fully in society and the economy, with negative effects both for the individual and for broader society.

The report defined three objectives which any initiative should meet:

*   to increase protection from discrimination ;

*   to ensure legal certainty for economic operators and potential victims across the Member States;

*   to enhance social inclusion and promote the full participation of all groups in society and the economy.

Of the various measures identified that could help reach the objectives, six options were selected for further analysis, notably no new action at EU level; self-regulation; recommendations; and one or more directives prohibiting discrimination outside the employment sphere .

In any event, Member States will have to implement the UN Convention on the Rights of Persons with Disabilities which defines the denial of reasonable accommodation as discrimination. A legally binding measure which prohibits discrimination on grounds of disability entails financial costs because of the adaptations needed but there are also benefits from the fuller economic and social inclusion of groups currently facing discrimination.

The report concludes that a multi-ground directive would be the appropriate response, designed so as to respect the principles of subsidiarity and proportionality. A small number of Member States already have rather complete legislative protection while most others have some, but less comprehensive, protection. The legislative adaptation arising from new EC rules would therefore vary.

The Commission received many complaints about discrimination in the insurance and banking sector. The use of age or disability by insurers and banks to assess the risk profile of customers does not necessarily represent discrimination: it depends on the product. The Commission will initiate a dialogue with the insurance and banking industry together with other relevant stakeholders to achieve a better common understanding of the areas where age or disability are relevant factors for the design and pricing of the products offered in these sectors.

3.           Legal aspects

**Legal base**

The proposal is based on Article 13(1) EC Treaty.

**Subsidiarity and proportionality**

The principle of subsidiarity applies insofar as the proposal does not fall under the exclusive competence of the Community. The objectives of the proposal cannot be sufficiently achieved by the Member States acting alone because only a Community–wide measure can ensure that there is a minimum standard level of protection against discrimination based on religion or belief, disability, age or sexual orientation in all the Member States. A Community legal act provides legal certainty as to the rights and obligations of economic operators and citizens, including for those moving between the Member States. Experience with the previous directives adopted under Article 13(1) EC is that they had a positive effect in achieving a better protection against discrimination. In accordance with the principle of proportionality, the proposed directive does not go beyond what is necessary to achieve the objectives set.

Moreover, national traditions and approaches in areas such as healthcare, social protection and education tend to be more diverse than in employment-related areas. These areas are characterised by legitimate societal choices in areas which fall within national competence.

The diversity of European societies is one of Europe's strengths, and is to be respected in line with the principle of subsidiarity. Issues such as the organisation and content of education, recognition of marital or family status, adoption, reproductive rights and other similar questions are best decided at national level. The Directive does not therefore require any Member State to amend its present laws and practices in relation to these issues. Nor does it affect national rules governing the activities of churches and other religious organisations or their relationship with the state. So, for example, it will remain for Member States alone to take decisions on questions such as whether to allow selective admission to schools, or prohibit or allow the wearing or display of religious symbols in schools, whether to recognise same-sex marriages, and the nature of any relationship between organised religion and the state.

**Choice of instrument**

A directive is the instrument that best ensures a coherent minimum level of protection against discrimination across the EU, whilst allowing individual Member States that want to go beyond the minimum standards to do so. It also allows them to choose the most appropriate means of enforcement and sanctions. Past experience in the non-discrimination field is that a directive was the most appropriate instrument.

**Correlation table**

Member States are required to communicate to the Commission the text of national provisions transposing the directive as well as a correlation table between those provisions and the directive.

  

**European Economic Area**

This is a text of relevance to the European Economic Area and the Directive will be applicable to the non-EU Member States of the European Economic Area following a decision of the EEA Joint Committee

4.           Budgetary implications

The proposal has no implications for the Community budget.

5.           Detailed explanation of the specific provisions

Article 1: Purpose

The main objective of the directive is to combat discrimination based on religion or belief, disability, age or sexual orientation and to put into effect the principle of equal treatment, outside the field of employment. The directive does not prohibit differences of treatment based on sex which are covered by Articles 13 and 141 of the EC Treaty and related secondary legislation.

Article 2: Concept of discrimination

The definition of the principle of equal treatment is based on that contained in the previous directives adopted under Article 13(1) EC \[as well as relevant case law of the European Court of Justice\].

Direct discrimination consists of treating someone differently solely because of his or her age, disability, religion or belief and sexual orientation. Indirect discrimination is more complex in that a rule or practice which seems neutral in fact has a particularly disadvantageous impact upon a person or a group of persons having a specific characteristic. The author of the rule or practice may have no idea of the practical consequences, and intention to discriminate is therefore not relevant. As in Directives 2000/43/EC, 2000/78/EC and 2002/73/EC[\[20\]](#_ftn20), it is possible to justify indirect discrimination (if "that provision, criterion or practice is objectively justified by a legitimate aim and the means of achieving that aim are appropriate and necessary").

Harassment is a form of discrimination. The unwanted conduct can take different forms, from verbal or written comments, gestures or behaviour, but it has to be serious enough to create an intimidating, humiliating or offensive environment. This definition is identical to the definitions contained in the other Article 13 directives.

A denial of reasonable accommodation is considered a form of discrimination. This is in line with the UN Convention on the rights of people with disabilities and coherent with Directive 2000/78/EC. Certain differences of treatment based on age may be lawful, if they are justified by a legitimate aim and the means of achieving that aim are appropriate and necessary (proportionality test).

  

In the existing Article 13 EC directives exceptions to the prohibition of direct discrimination were allowed for "genuine and determining occupational requirements", for differences of treatment based on age, and in the context of sex discrimination, in access to goods and services. Although the current proposal does not cover employment, there will be differences of treatment in the areas mentioned in Article 3 that should be allowed. However, as exceptions to the general principle of equality should be narrowly drawn, the double test of a justified aim and proportionate way of reaching it (i.e. in the least discriminatory way possible) is required.

A special rule is added for insurance and banking services, in recognition of the fact that age and disability can be an essential element of the assessment of risk for certain products, and therefore of price. If insurers are not allowed to take age and disability into account at all, the additional costs will have to be entirely borne by the rest of the "pool" of those insured, which would result in higher overall costs and lower availability of cover for consumers. The use of age and disability in the assessment of risk must be based on accurate data and statistics.

The directive does not affect national measures based on public security, public order, the prevention of criminal offences, the protection of health and the rights and freedoms of others.

Article 3: Scope

Discrimination based on religion or belief, disability, age or sexual orientation is prohibited by both the public and private sector in:

*   social protection, including social security and health care;

*   social advantages;

*   education;

*   access to and supply of goods and services which are available to the public, including housing.

In terms of access to goods and services, only professional or commercial activities are covered. In other words, transactions between private individuals acting in a private capacity will not be covered: letting a room in a private house does not need to be treated in the same way as letting rooms in a hotel. The areas are covered only to the extent that the subject matter falls within the competences of the Community. Thus, for example, the organisation of the school system, activities and the content of education courses, including how to organise education for persons with disabilities, is a matter for the Member States, and they may provide for differences in treatment in access to religious educational institutions. For example, a school could arrange a special presentation just for children of a certain age, while a faith based school would be allowed to arrange school trips with a religious theme.

The text makes it clear that matters related to marital and family status, which includes adoption, are outside the scope of the directive. This includes reproductive rights. Member States remain free to decide whether or not to institute and recognise _legally_ registered partnerships. However once national law recognises such relationships as comparable to that of spouses then the principle of equal treatment applies[\[21\]](#_ftn21).

Article 3 specifies that the directive does not cover national laws relating to the secular nature of the State and its institutions, nor to the status of religious organisations. Member States may thus allow or prohibit the wearing of religious symbols in schools. Differences in treatment based on nationality are also not covered.

_Article 4:_ _Equal treatment of persons with disabilities_

Effective access for disabled people to social protection, social advantages, health care, education and access to and supply of goods and services which are available to the public, including housing, shall be provided by anticipation. This obligation is limited by the defence that if this would impose a disproportionate burden or would require major changes to the product or service, it does not need to be done.

In some cases individual measures of reasonable accommodation may be necessary to ensure effective access for a particular disabled person. As above, this is only the case if it would not impose a disproportionate burden. A non-exhaustive list is given of factors that could be taken into account in assessing whether the burden is disproportionate, thus allowing the specific situation of small and medium sized, and micro enterprises, to be taken into account.

The concept of reasonable accommodation already exists in the employment sphere under Directive 2000/78/EC, and Member States and businesses therefore have experience in applying it. What might be appropriate for a large corporation or public body may not be for a small or medium-sized company. The requirement to make reasonable accommodation does not only imply making physical changes but may entail an alternative means of providing a service.

_Article 5: Positive action_

This provision is common to all Article 13 directives. It is clear that in many cases, formal equality does not lead to equality in practice. It may be necessary to put in place specific measures to prevent and correct situations of inequality. The Member States have different traditions and practices regarding positive action, and this article lets Member States provide for positive action but does not make this an obligation.

Article 6: Minimum requirements

This provision is common to all Article 13 directives. It allows Member States to provide a higher level of protection than that guaranteed by the Directive, and confirms that there should be no lowering of the level of protection against discrimination already afforded by Member States when implementing the Directive.

Article 7: Defence of rights

This provision is common to all Article 13 directives. People should be able to enforce their right to non-discrimination. This article therefore provides that people who believe that they have been the victim of discrimination should be able to use administrative or judicial procedures, even after the relationship in which the discrimination is alleged to have taken place has ended, in accordance with the ruling of the European Court of Justice in the Coote[\[22\]](#_ftn22) case.

The right to effective legal protection is strengthened by allowing organisations, which have a legitimate interest in the fight against discrimination, to help victims of discrimination in judicial or administrative procedures. National rules on time limits for initiating actions are unaffected by this provision.

Article 8: Burden of proof

This provision is common to all Article 13 directives. In judicial procedures, the general rule is that a person who alleges something must prove it. However, in discrimination cases, it is often extremely difficult to obtain the evidence necessary to prove the case, as it is often in the hands of the respondent. This problem was recognised by the European Court of Justice[\[23\]](#_ftn23) and the Community legislator in Directive 97/80/EC[\[24\]](#_ftn24).

The shift of the burden of proof applies to all cases alleging breach of the principle of equal treatment, including those involving associations and organisations under Article 7(2). As in the earlier directives, this shift in the burden of proof does not apply to situations where the criminal law is used to prosecute allegations of discrimination.

Article 9: Victimisation

This provision is common to all Article 13 directives. Effective legal protection must include protection against retaliation. Victims may be deterred from exercising their rights due to the risk of retaliation, and it is therefore necessary to protect individuals against any adverse treatment due to the exercise of the rights conferred by the Directive. This article is the same as in Directives 2000/43/EC and 2000/78/EC.

Article 10: Dissemination of information

This provision is common to all Article 13 directives. Experience and polls show that individuals are badly or insufficiently informed of their rights. The more effective the system of public information and prevention is, the less need there will be for individual remedies. This replicates equivalent provisions in Directives 2000/43/EC, 2000/78/EC and 2002/113/EC.

Article 11: Dialogue with relevant stakeholders

This provision is common to all Article 13 directives. It aims to promote dialogue between relevant public authorities and bodies such as non-governmental organisations which have a legitimate interest in contributing to the fight against discrimination on grounds of religion or belief, disability, age or sexual orientation. A similar provision is contained in the previous anti-discrimination directives.

Article 12: Bodies for the promotion of equal treatment

This provision is common to two Article 13 directives. This article requires the Member States to have a body or bodies ("Equality Body") at national level to promote equal treatment of all persons without discrimination on the grounds of religion or belief, disability, age or sexual orientation.

It replicates the provisions of Directive 2000/43/EC in as far as they deal with access to and supply of goods and services, and builds on equivalent provisions in Directives 2002/73/EC[\[25\]](#_ftn25) and 2004/113/EC. It sets out minimum competences applicable to bodies at national level which should act independently to promote the principle of equal treatment. Member States may decide that these bodies be the same as those already established under the previous directives.

It is both difficult and expensive for individuals to mount a legal challenge if they think they have been discriminated against. A key role of the Equality Bodies is to give independent help to victims of discrimination. They must also be able to conduct independent surveys on discrimination and to publish reports and recommendations on issues relating to discrimination.

Article 13: Compliance

This provision is common to all Article 13 directives. Equal treatment involves the elimination of discrimination arising from any laws, regulations or administrative provision and the directive therefore requires the Member States to abolish any such provisions. As with earlier legislation, the directive also requires that any provisions contrary to the principle of equal treatment must be rendered null and void or amended, or must be capable of being so rendered if they are challenged.

Article 14: Sanctions

This provision is common to all Article 13 directives. In accordance with the case law of the Court of Justice[\[26\]](#_ftn26), the text provides that that there should be no upper limit on the compensation payable in cases of breach of the principle of equal treatment. This provision does not require criminal sanctions to be introduced.

Article 15: Implementation

This provision is common to all Article 13 directives. It gives the Member States a period of two years to transpose the directive into national law and to communicate to the Commission the texts of the national law. Member States may provide that the obligation to ensure effective access for disabled persons only applies four years after the adoption of the Directive.

_Article 16: Report_

This provision is common to all Article 13 directives. It requires the Commission to report to the European Parliament and the Council on the application of the Directive, on the basis of information from Member States. The report will take account of the views of the social partners, relevant NGOs and the EU Fundamental Rights Agency.

_Article 17: Entry into force_

This provision is common to all Article 13 directives. The Directive will enter into force on the day it is published in the Official Journal.

_Article 18: Addressees_

This provision is common to all Article 13 directives, making it clear that the Directive is addressed to the Member States.

  

2008/0140 (CNS)

Proposal for a

COUNCIL DIRECTIVE

on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

THE COUNCIL OF THE EUROPEAN UNION,

Having regard to the Treaty establishing the European Community, and in particular Article 13(1) thereof,

Having regard to the proposal from the Commission[\[27\]](#_ftn27),

Having regard to the opinion of the European Parliament[\[28\]](#_ftn28),

Having regard to the opinion of the European Economic and Social Committee[\[29\]](#_ftn29),

Having regard to the opinion of the Committee of the Regions[\[30\]](#_ftn30),

Whereas:

(1)               In accordance with Article 6 of the Treaty on European Union, the European Union is founded on the principles of liberty, democracy, respect for human rights and fundamental freedoms, and the rule of law, principles which are common to all Member States and it respects fundamental rights, as guaranteed by the European Convention for the Protection of Human Rights and Fundamental Freedoms and as they result from the constitutional traditions common to the Member States, as general principles of Community law.

(2)               The right to equality before the law and protection against discrimination for all persons constitutes a universal right recognised by the Universal Declaration of Human Rights, the United Nations Convention on the Elimination of all forms of Discrimination Against Women, the International Convention on the Elimination of all forms of Racial Discrimination, the United Nations Covenants on Civil and Political Rights and on Economic, Social and Cultural Rights, the UN Convention on the Rights of Persons with Disabilities, the European Convention for the Protection of Human Rights and Fundamental Freedoms and the European Social Charter, to which \[all\] Member States are signatories. In particular, the UN Convention on the Rights of Persons with Disabilities includes the denial of reasonable accommodation in its definition of discrimination.

(3)               This Directive respects the fundamental rights and observes the fundamental principles recognised in particular by the Charter of Fundamental Rights of the European Union. Article 10 of the Charter recognises the right to freedom of thought, conscience and religion; Article 21 prohibits discrimination, including on grounds of religion or belief, disability, age or sexual orientation; and Article 26 acknowledges the right of persons with disabilities to benefit from measures designed to ensure their independence.

(4)               The European Years of Persons with Disabilities in 2003, of Equal Opportunities for All in 2007, and of Intercultural Dialogue in 2008 have highlighted the persistence of discrimination but also the benefits of diversity.

(5)               The European Council, in Brussels on 14 December 2007, invited Member States to strengthen efforts to prevent and combat discrimination inside and outside the labour market[\[31\]](#_ftn31).

(6)               The European Parliament has called for the extension of the protection of discrimination in European Union law[\[32\]](#_ftn32).

(7)               The European Commission has affirmed in its Communication ‘Renewed social agenda: Opportunities, access and solidarity in 21st century Europe’[\[33\]](#_ftn33) that, in societies where each individual is regarded as being of equal worth, no artificial barriers or discrimination of any kind should hold people back in exploiting these opportunities.

(8)               The Community has adopted three legal instruments[\[34\]](#_ftn34) on the basis of article 13(1) of the EC Treaty to prevent and combat discrimination on grounds of sex, racial and ethnic origin, religion or belief, disability, age and sexual orientation. These instruments have demonstrated the value of legislation in the fight against discrimination_._ In particular, Directive 2000/78/EC establishes a general framework for equal treatment in employment and occupation on the grounds of religion or belief, disability, age and sexual orientation. However, variations remain between Member States on the degree and the form of protection from discrimination on these grounds beyond the areas of employment.

(9)               Therefore, legislation should prohibit discrimination based on religion or belief, disability, age or sexual orientation in a range of areas outside the labour market, including social protection, education and access to and supply of goods and services, including housing. It should provide for measures to ensure the equal access of persons with disabilities to the areas covered.

(10)           Directive 2000/78/EC prohibits discrimination in access to vocational training; it is necessary to complete this protection by extending the prohibition of discrimination to education which is not considered vocational training.

(11)           This Directive should be without prejudice to the competences of the Member States in the areas of education, social security and health care. It should also be without prejudice to the essential role and wide discretion of the Member States in providing, commissioning and organising services of general economic interest.

(12)              Discrimination is understood to include direct and indirect discrimination, harassment, instructions to discriminate and denial of reasonable accommodation.

(13)           In implementing the principle of equal treatment irrespective of religion or belief, disability, age or sexual orientation, the Community should, in accordance with Article 3(2) of the EC Treaty, aim to eliminate inequalities, and to promote equality between men and women, especially since women are often the victims of multiple discrimination.

(14)           The appreciation of the facts from which it may be presumed that there has been direct or indirect discrimination should remain a matter for the national judicial or other competent bodies in accordance with rules of national law or practice. Such rules may provide, in particular, for indirect discrimination to be established by any means including on the basis of statistical evidence.

(15)           Actuarial and risk factors related to disability and to age are used in the provision of insurance, banking and other financial services. These should not be regarded as constituting discrimination where the factors are shown to be key factors for the assessment of risk.

(16)           All individuals enjoy the freedom to contract, including the freedom to choose a contractual partner for a transaction. This Directive should not apply to economic transactions undertaken by individuals for whom these transactions do not constitute their professional or commercial activity.

(17)           While prohibiting discrimination, it is important to respect other fundamental rights and freedoms, including the protection of private and family life and transactions carried out in that context, the freedom of religion, and the freedom of association. This Directive is without prejudice to national laws on marital or family status, including on reproductive rights. It is also without prejudice to the secular nature of the State, state institutions or bodies, or education.

(18)           Member States are responsible for the organisation and content of education. The Commission Communication on Competences for the 21st Century: An Agenda for European Cooperation on Schools draws attention to the need for special attention to be paid to disadvantaged children and those with special educational needs. In particular national law may provide for differences in access to educational institutions based on religion or belief. . Member States may also allow or prohibit the wearing or display of religious symbols at school.

(19)              The European Union in its Declaration No 11 on the status of churches and non-confessional organisations, annexed to the Final Act of the Amsterdam Treaty, has explicitly recognised that it respects and does not prejudice the status under national law of churches and religious associations or communities in the Member States and that it equally respects the status of philosophical and non-confessional organisations. Measures to enable persons with disabilities  to have effective non-discriminatory access to the areas covered by this Directive play an important part in ensuring full equality in practice. Furthermore, individual measures of reasonable accommodation may be required in some cases to ensure such access. In neither case are measures required that would impose a disproportionate burden. In assessing whether the burden is disproportionate, account should be taken of a number of factors including the size, resources and nature of the organisation. The principle of reasonable accommodation and disproportionate burden are established in Directive 2000/78/EC and the UN Convention on Rights of Persons with Disabilities.

(20)           Legal requirements[\[35\]](#_ftn35) and standards on accessibility have been established at European level in some areas while Article 16 of Council Regulation 1083/2006 of 11 July 2006 on the European Regional Development Fund, the European Social Fund and the Cohesion Fund and repealing Regulation (EC) No 1260/1999[\[36\]](#_ftn36) requires that accessibility for disabled persons is one of the criteria to be observed in defining operations co-financed by the Funds. The Council has also emphasised the need for measures to secure the accessibility of cultural infrastructure and cultural activities for people with disabilities[\[37\]](#_ftn37).

(21)           The prohibition of discrimination should be without prejudice to the maintenance or adoption by Member States of measures intended to prevent or compensate for disadvantages suffered by a group of persons of a particular religion or belief, disability, age or sexual orientation. Such measures may permit organisations of persons of a particular religion or belief, disability, age or sexual orientation where their main object is the promotion of the special needs of those persons.

(22)           This Directive lays down minimum requirements, thus giving the Member States the option of introducing or maintaining more favourable provisions. The implementation of this Directive should not serve to justify any regression in relation to the situation which already prevails in each Member State.

(23)           Persons who have been subject to discrimination based on religion or belief, disability, age or sexual orientation should have adequate means of legal protection. To provide a more effective level of protection, associations, organisations and other legal entities should be empowered to engage in proceedings, including on behalf of or in support of any victim, without prejudice to national rules of procedure concerning representation and defence before the courts.

(24)           The rules on the burden of proof must be adapted when there is a prima facie case of discrimination and, for the principle of equal treatment to be applied effectively, the burden of proof must shift back to the respondent when evidence of such discrimination is brought. However, it is not for the respondent to prove that the plaintiff adheres to a particular religion or belief, has a particular disability, is of a particular age or has a particular sexual orientation.

(25)           The effective implementation of the principle of equal treatment requires adequate judicial protection against victimisation.

(26)           In its resolution on the Follow-up of the European Year of Equal Opportunities for All (2007), the Council called for the full association of civil society, including organisations representing people at risk of discrimination, the social partners and stakeholders in the design of policies and programmes aimed at preventing discrimination and promoting equality and equal opportunities, both at European and national levels.

(27)           Experience in applying Directives 2000/43/EC and 2004/113/EC show that protection from discrimination on the grounds covered by this Directive would be strengthened by the existence of a body or bodies in each Member State, with competence to analyse the problems involved, to study possible solutions and to provide concrete assistance for the victims.

(28)           In exercising their powers and fulfilling their responsibilities under this Directive, these bodies should operate in a manner consistent with the United Nations Paris Principles relating to the status and functioning of national institutions for the protection and promotion of human rights.

(29)           Member States should provide for effective, proportionate and dissuasive sanctions in case of breaches of the obligations under this Directive.

(30)           In accordance with the principles of subsidiarity and proportionality as set out in Article 5 of the EC Treaty, the objective of this Directive, namely ensuring a common level of protection against discrimination in all the Member States, cannot be sufficiently achieved by the Member States and can therefore, by reason of the scale and impact of the proposed action, be better achieved by the Community. This Directive does not go beyond what is necessary in order to achieve those objectives.

(31)           In accordance with paragraph 34 of the interinstitutional agreement on better law-making, Member States are encouraged to draw up, for themselves and in the interest of the Community, their own tables, which will, as far as possible, illustrate the correlation between the Directive and the transposition measures and to make them public.

HAS ADOPTED THIS DIRECTIVE:

Chapter 1  
GENERAL PROVISIONS

Article 1  
Purpose

This Directive lays down a framework for combating discrimination on the grounds of religion or belief, disability, age, or sexual orientation, with a view to putting into effect in the Member States the principle of equal treatment other than in the field of employment and occupation.

Article 2  
Concept of discrimination

1\. For the purposes of this Directive, the "principle of equal treatment" shall mean that there shall be no direct or indirect discrimination on any of the grounds referred to in Article 1.

2\. For the purposes of paragraph 1:

(a) direct discrimination shall be taken to occur where one person is treated less favourably than another is, has been or would be treated in a comparable situation, on any of the grounds referred to in Article 1;

(b) indirect discrimination shall be taken to occur where an apparently neutral provision, criterion or practice would put persons of a particular religion or belief, a particular disability, a particular age, or a particular sexual orientation at a particular disadvantage compared with other persons, unless that provision, criterion or practice is objectively justified by a legitimate aim and the means of achieving that aim are appropriate and necessary.

3\. Harassment shall be deemed to be a form of discrimination within the meaning of paragraph 1, when unwanted conduct related to any of the grounds referred to in Article 1 takes place with the purpose or effect of violating the dignity of a person and of creating an intimidating, hostile, degrading, humiliating or offensive environment.

4\. An instruction to discriminate against persons on any of the grounds referred to in Article 1 shall be deemed to be discrimination within the meaning of paragraph 1.

5\. Denial of reasonable accommodation in a particular case as provided for by Article 4 (1)(b) of the present Directive as regards persons with disabilities shall be deemed to be discrimination within the meaning of paragraph 1.

6\. Notwithstanding paragraph 2, Member States may provide that differences of treatment on grounds of age shall not constitute discrimination, if, within the context of national law, they are justified by a legitimate aim, and if the means of achieving that aim are appropriate and necessary. In particular, this Directive shall not preclude the fixing of a specific age for access to social benefits, education and certain goods or services.

7\. Notwithstanding paragraph 2, in the provision of financial services Member States may permit proportionate differences in treatment where, for the product in question, the use of age or disability is a key factor in the assessment of risk based on relevant and accurate actuarial or statistical data.

8\. This Directive shall be without prejudice to general measures laid down in national law which, in a democratic society, are necessary for public security, for the maintenance of public order and the prevention of criminal offences, for the protection of health and the protection of the rights and freedoms of others.

Article 3  
Scope

1\. Within the limits of the powers conferred upon the Community, the prohibition of discrimination shall apply to all persons, as regards both the public and private sectors, including public bodies, in relation to:

(a) Social protection, including social security and healthcare;

(b) Social advantages;

(c) Education;

(d) Access to and supply of goods and other services which are available to the public, including housing.

Subparagraph (d) shall apply to individuals only insofar as they are performing a professional or commercial activity.

2\. This Directive is without prejudice to national laws on marital or family status and reproductive rights.

3\. This Directive is without prejudice to the responsibilities of Member States for the content of teaching, activities and the organisation of their educational systems, including the provision of special needs education. Member States may provide for differences in treatment in access to educational institutions based on religion or belief.

4\. This Directive is without prejudice to national legislation ensuring the secular nature of the State, State institutions or bodies, or education, or concerning the status and activities of churches and other organisations based on religion or belief. It is equally without prejudice to national legislation promoting equality between men and women.

5\. This Directive does not cover differences of treatment based on nationality and is without prejudice to provisions and conditions relating to the entry into and residence of third-country nationals and stateless persons in the territory of Member States, and to any treatment which arises from the legal status of the third-country nationals and stateless persons concerned.

Article 4  
Equal treatment of persons with disabilities

1\. In order to guarantee compliance with the principle of equal treatment in relation to persons with disabilities:

a) The measures necessary to enable persons with disabilities to have effective non-discriminatory access to social protection, social advantages, health care, education and access to and supply of goods and services which are available to the public, including housing and transport, shall be provided by anticipation, including through appropriate modifications or adjustments. Such measures should not impose a disproportionate burden, nor require fundamental alteration of the social protection, social advantages, health care, education, or goods and services in question or require the provision of alternatives thereto.

b) Notwithstanding the obligation to ensure effective non-discriminatory access and where needed in a particular case, reasonable accommodation shall be provided unless this would impose a disproportionate burden.

2\. For the purposes of assessing whether measures necessary to comply with paragraph 1 would impose a disproportionate burden, account shall be taken, in particular, of the size and resources of the organisation, its nature, the estimated cost, the life cycle of the goods and services, and the possible benefits of increased access for persons with disabilities. The burden shall not be disproportionate when it is sufficiently remedied by measures existing within the framework of the equal treatment policy of the Member State concerned.

3\. This Directive shall be without prejudice to the provisions of Community law or national rules covering the accessibility of particular goods or services.

Article 5  
Positive action

With a view to ensuring full equality in practice, the principle of equal treatment shall not prevent any Member State from maintaining or adopting specific measures to prevent or compensate for disadvantages linked to religion or belief, disability, age, or sexual orientation.

Article 6  
Minimum requirements

1\. Member States may introduce or maintain provisions which are more favourable to the protection of the principle of equal treatment than those laid down in this Directive.

2\. The implementation of this Directive shall under no circumstances constitute grounds for a reduction in the level of protection against discrimination already afforded by Member States in the fields covered by this Directive.

CHAPTER II  
REMEDIES AND ENFORCEMENT

Article 7  
Defence of rights

1\. Member States shall ensure that judicial and/or administrative procedures, including where they deem it appropriate conciliation procedures, for the enforcement of obligations under this Directive are available to all persons who consider themselves wronged by failure to apply the principle of equal treatment to them, even after the relationship in which the discrimination is alleged to have occurred has ended.

2\. Member States shall ensure that associations, organisations or other legal entities, which have a legitimate interest in ensuring that the provisions of this Directive are complied with, may engage, either on behalf or in support of the complainant, with his or her approval, in any judicial and/or administrative procedure provided for the enforcement of obligations under this Directive.

3\. Paragraphs 1 and 2 shall be without prejudice to national rules relating to time limits for bringing actions as regards the principle of equality of treatment.

Article 8  
Burden of proof

1\. Member States shall take such measures as are necessary, in accordance with their national judicial systems, to ensure that, when persons who consider themselves wronged because the principle of equal treatment has not been applied to them establish, before a court or other competent authority, facts from which it may be presumed that there has been direct or indirect discrimination, it shall be for the respondent to prove that there has been no breach of the prohibition of discrimination.

2\. Paragraph 1 shall not prevent Member States from introducing rules of evidence which are more favourable to plaintiffs.

3\. Paragraph 1 shall not apply to criminal procedures.

4\. Member States need not apply paragraph 1 to proceedings in which the court or competent body investigates the facts of the case.

5\. Paragraphs 1, 2, 3 and 4 shall also apply to any legal proceedings commenced in accordance with Article 7(2).

Article 9  
Victimisation

Member States shall introduce into their national legal systems such measures as are necessary to protect individuals from any adverse treatment or adverse consequence as a reaction to a complaint or to proceedings aimed at enforcing compliance with the principle of equal treatment.

Article 10  
Dissemination of information

Member States shall ensure that the provisions adopted pursuant to this Directive, together with the relevant provisions already in force, are brought to the attention of the persons concerned by appropriate means throughout their territory.

Article 11  
Dialogue with relevant stakeholders

With a view to promoting the principle of equal treatment, Member States shall encourage dialogue with relevant stakeholders, in particular non-governmental organisations, which have, in accordance with their national law and practice, a legitimate interest in contributing to the fight against discrimination on the grounds and in the areas covered by this Directive.

Article 12  
Bodies for the Promotion of Equal treatment

1\. Member States shall designate a body or bodies for the promotion of equal treatment of all persons irrespective of their religion or belief, disability, age, or sexual orientation. These bodies may form part of agencies charged at national level with the defence of human rights or the safeguard of individuals' rights, including rights under other Community acts including Directives 2000/43/EC and 2004/113/EC.

2\. Member States shall ensure that the competences of these bodies include:

–      without prejudice to the right of victims and of associations, organizations or other legal entities referred to in Article 7(2), providing independent assistance to victims of discrimination in pursuing their complaints about discrimination,

–      conducting independent surveys concerning discrimination,

–      publishing independent reports and making recommendations on any issue relating to such discrimination.

CHAPTER III  
FINAL PROVISIONS

Article 13  
Compliance

Member States shall take the necessary measures to ensure that the principle of equal treatment is respected and in particular that:

(a) any laws, regulations and administrative provisions contrary to the principle of equal treatment are abolished;

(b) any contractual provisions, internal rules of undertakings, and rules governing profit-making or non-profit-making associations contrary to the principle of equal treatment are, or may be, declared null and void or are amended.

Article 14  
Sanctions

Member States shall lay down the rules on sanctions applicable to breaches of the national provisions adopted pursuant to this Directive, and shall take all measures necessary to ensure that they are applied. Sanctions may comprise the payment of compensation, which may not be restricted by the fixing of a prior upper limit, and must be effective, proportionate and dissuasive.

Article 15  
Implementation

1\. Member States shall adopt the laws, regulations and administrative provisions necessary to comply with this Directive by …. at the latest \[two years after adoption\]. They shall forthwith inform the Commission thereof and shall communicate to the Commission the text of those provisions and a correlation table between those provisions and this Directive.

When Member States adopt these measures, they shall contain a reference to this Directive or be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.

2\. In order to take account of particular conditions, Member States may, if necessary, establish that the obligation to provide effective access as set out in Article 4 has to be complied with by … \[at the latest\] four \[years after adoption\].

Member States wishing to use this additional period shall inform the Commission at the latest by the date set down in paragraph 1 giving reasons.

Article 16  
Report

1\. Member States and national equality bodies shall communicate to the Commission, by …. at the latest and every five years thereafter, all the information necessary for the Commission to draw up a report to the European Parliament and the Council on the application of this Directive.

2\. The Commission's report shall take into account, as appropriate, the viewpoints of the social partners and relevant non-governmental organizations, as well as the EU Fundamental Rights Agency. In accordance with the principle of gender mainstreaming, this report shall, inter alias, provide an assessment of the impact of the measures taken on women and men. In the light of the information received, this report shall include, if necessary, proposals to revise and update this Directive.

Article 17  
Entry into force

This Directive shall enter into force on the day of its publication in the Official Journal of the European Union.

Article 18  
Addressees

This Directive is addressed to the Member States.

Done at Brussels,

For the Council

The President

  

* * *

[\[1\]](#_ftnref1) Directive 2000/43/EC of 29 June 2000 implementing the principle of equal treatment between persons irrespective of racial or ethnic origin, OJ L 180 of 19.7.2000, p.22 and Directive 2000/78/EC of 27 November 2000 establishing a general framework for equal treatment in employment and occupation, OJ L 303 of 2.12.2000, p. 16

[\[2\]](#_ftnref2) COM (2007) 640

[\[3\]](#_ftnref3) COM (2008) 412

[\[4\]](#_ftnref4) COM (2008) 420

[\[5\]](#_ftnref5) \[ COM (2008) XXX \]

[\[6\]](#_ftnref6) Directive 2004/113/EC of 13 December 2004 implementing the principle of equal treatment between men and women in the access to and supply of goods and services, OJ L 373 of 21.12.2004, p.37

[\[7\]](#_ftnref7) Directive 2000/43/EC of 29 June 2000 implementing the principle of equal treatment between persons irrespective of racial or ethnic origin (OJ L 180 of 19.7.2000), Directive 2000/78/EC of 27 November 2000 establishing a general framework for equal treatment in employment and occupation (OJ L 303 of 2.12.2000)

[\[8\]](#_ftnref8) COM (2006) 643 final

[\[9\]](#_ftnref9) COM (2008) 225

[\[10\]](#_ftnref10) The full results of the consultation can be accessed at: [http://ec.europa.eu/employment\_social/fundamental\_rights/news/news_en.htm#rpc](#rpc)

[\[11\]](#_ftnref11) [http://ec.europa.eu/yourvoice/ebtp/consultations/index_en.htm](http://ec.europa.eu/yourvoice/ebtp/consultations/index_en.htm)

[\[12\]](#_ftnref12) [http://ec.europa.eu/employment\_social/fundamental\_rights/org/imass_en.htm#ar](#ar)

[\[13\]](#_ftnref13) [http://ec.europa.eu/employment\_social/fundamental\_rights/pdf/pubst/stud/mapstrand1_en.pdf](http://ec.europa.eu/employment_social/fundamental_rights/pdf/pubst/stud/mapstrand1_en.pdf)

Will be available on:[http://ec.europa.eu/employment\_social/fundamental\_rights/org/imass_en.htm](http://ec.europa.eu/employment_social/fundamental_rights/org/imass_en.htm)

[\[15\]](#_ftnref15) [http://ec.europa.eu/employment\_social/fundamental\_rights/public/pubst_en.htm#leg](#leg)

[\[16\]](#_ftnref16) [http://ec.europa.eu/employment\_social/fundamental\_rights/pdf/pubst/stud/multdis_en.pdf](http://ec.europa.eu/employment_social/fundamental_rights/pdf/pubst/stud/multdis_en.pdf)

and [http://ec.europa.eu/public\_opinion/archives/eb\_special_en.htm](http://ec.europa.eu/public_opinion/archives/eb_special_en.htm)

[\[18\]](#_ftnref18) Flash Eurobarometer 232; [http://ec.europa.eu/public\_opinion/flash/fl\_232_en.pdf](http://ec.europa.eu/public_opinion/flash/fl_232_en.pdf)

Will be available on:[http://ec.europa.eu/employment\_social/fundamental\_rights/org/imass_en.htm](http://ec.europa.eu/employment_social/fundamental_rights/org/imass_en.htm)

[\[20\]](#_ftnref20) OJ L269 of 5.10.2002

[\[21\]](#_ftnref21) Judgment of the ECJ of 1.4.2008 in case C-267/06 Tadao Maruko

[\[22\]](#_ftnref22) Case C-185/97 \[1998\] ECR I-5199

[\[23\]](#_ftnref23) Danfoss, **Case 109/88****.** **\[1989\] ECR** _03199_

[\[24\]](#_ftnref24) OJ L.14, 20.1.1998

[\[25\]](#_ftnref25) Directive 2002/73/EC amending Council Directive 76/207/EEC on the implementation of the principle of equal treatment for men and women as regards access to employment, vocational training and promotion, and working conditions, OJ L 269 of 5.10.2002, p.15

[\[26\]](#_ftnref26) Cases C-180/95 Draehmpaehl, ECR 1997 I p.2195 and C-271/91 Marshall ECR 1993 I P.4367

[\[27\]](#_ftnref27) OJ C , , p. .

[\[28\]](#_ftnref28) OJ C , , p. .

[\[29\]](#_ftnref29) OJ C , , p. .

[\[30\]](#_ftnref30) OJ C , , p. .

[\[31\]](#_ftnref31) Presidency conclusions of the Brussels European Council of 14 December 2007, point 50.

[\[32\]](#_ftnref32) Resolution of 20 May 2008 P6_TA-PROV(2008)0212

[\[33\]](#_ftnref33) COM (2008) 412

[\[34\]](#_ftnref34) Directive 2000/43/EC, Directive 2000/78/EC and Directive 2004/113/EC

[\[35\]](#_ftnref35) Regulation (EC) No. 1107/2006 and Regulation (EC) No 1371/2007

[\[36\]](#_ftnref36) OJ L 210, 31.7.2006, p.25. Regulation as last amended by Regulation (EC) No 1989/2006 (OJ L 411, 30.12.2006, p.6)

[\[37\]](#_ftnref37) OJ C 134, 7.6.2003, p.7