---
title: 'Matteo Mainardi'
date: Thu, 21 Jan 2016 17:24:22 +0000
draft: false
---

[![matteo-mainardi-400-bn](http://www.certidiritti.org/wp-content/uploads/2016/01/matteo-mainardi-400-bn-300x300.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/01/matteo-mainardi-400-bn.jpg)Nato a Pesaro nel 1989, dopo aver studiato _Scienze politiche e delle organizzazioni_ a Bologna, si trasferisce a Roma dove lavora e vive. Si occupa soprattutto di strumenti di iniziativa popolare comunali, regionali e nazionali. Negli ultimi anni ha coordinato le campagne [Eutanasia Legale](http://www.eutanasialegale.it/) e [Legalizziamo!](http://www.legalizziamo.it/) promosse dall'Associazione Luca Coscioni.