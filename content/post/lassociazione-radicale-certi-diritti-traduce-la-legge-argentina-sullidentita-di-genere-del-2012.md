---
title: 'L''Associazione Radicale Certi Diritti traduce la Legge argentina sull''identità di genere del 2012'
date: Sun, 04 May 2014 11:22:00 +0000
draft: false
tags: [Transessualità]
---

[![transparent_transgender_symbol](http://www.certidiritti.org/wp-content/uploads/2014/05/transparent_transgender_symbol-267x300.jpeg)](http://www.certidiritti.org/wp-content/uploads/2014/05/transparent_transgender_symbol.jpeg)L’Associazione Radicale Certi Diritti è impegnata da tempo nella lotta per garantire dignità ed inserimento sociale alle persone transessuali. Sosteniamo sia la via giudiziaria che affermi una corretta interpretazione della legge 164 approvata nel 1982 secondo la quale non sarebbe necessaria la riassegnazione dei genitali e la sterilizzazione chirurgica per poter cambiare ufficialmente genere e nome sui documenti, sia la necessità che il Parlamento riformi con urgenza la materia attraverso un nuovo provvedimento legislativo sul modello argentino. L’Argentina nel 2012 ha approvato una legge sull’Identità di genere, che nel panorama mondiale è la più avanzata e rispettosa delle persone transessuali per la semplicità delle procedure previste e per il sostegno alla transizione di cui si fa carico la Stato. Riteniamo quindi importante far conoscere questa legge anche in Italia mettendone a disposizione la sua [traduzione](http://www.certidiritti.org/wp-content/uploads/2014/05/certi-diritti-legge-argentina-su-identità-di-genere-2012.pdf), curata per l'Associazione Radicale Certi Diritti da Gabriella Friso.

Yuri Guaiana, segretario dell'Associazione Radicale Certi diritti, afferma: "È ora che anche nel nostro Paese si tolgano dall’emarginazione e si tutelino dalle violenze transfobiche queste persone con normative rispettose della loro identità".