---
title: 'Finalmente all''interno dei partiti si discute di matrimonio gay: sostegno a Ivan Scalfarotto per posizione chiara. Si tenga conto dell''Unione Europea'
date: Fri, 15 Jun 2012 10:38:30 +0000
draft: false
tags: [Matrimonio egualitario]
---

Elezioni e matrimonio tra persone stesso sesso: finalmente se ne discute all'interno dei partiti, speriamo con azioni conseguenti. La posizione di dialogo di Ivan Scalfarotto va sostenuta perchè chiara e concreta. Si tenga conto di quanto l'Unione Europea chiede ai paesi membri.

Roma, 15 giugno 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

La buona notizia è che il tema del diritto al matrimonio civile tra persone dello stesso sesso anima le discussioni interne dei partiti, ed in particolare del PD. Se queste discussioni partoriranno non solo proposte ma concreti passi avanti per il nostro Paese lo vedremo. Sicuramente è importante che i partiti prendano posizione su una questione cruciale per i diritti ed i doveri di tutti, ovvero per l'applicazione di quel principio di uguaglianza che vale per le minoranze così come per le maggioranze.

La cattiva notizia è che il dibattito è viziato da polemiche che nulla hanno a che vedere con il tema in questione: la competizione elettorale è sana, soprattutto se si esercita su questioni concrete. E se il PD riuscisse a mantenere la competizione elettorale interna sui binari del confronto politico sarebbe cosa buona, per quel partito e per tutti. Ecco perchè riteniamo importanti posizioni come quella di Ivan Scalfarotto che continua a tentare il dialogo pur essendo da sempre e senza tentennamenti a favore del diritto al matrimonio civile per le persone dello stesso sesso continuando a dialogare (per quello che è possibile) con chi non è su queste posizioni.  
   
Per riformare l'Italia c'è bisogno della riforma dei partiti, che passa attraverso dibattiti come questo. Speriamo che tutte le formazioni politiche italiane si pongano la questione ed offrano soluzioni, magari confrontandosi prima con le organizzazioni che, come noi, soluzioni da proporre ne hanno.  
Ci auguriamo anche che ogni dibattito e conseguente decisione tenga conto di quanto l’Unione Europea continua a chiedere anche all’Italia sul tema dei diritti civili che include anche una Riforma complessiva del diritto di famiglia.