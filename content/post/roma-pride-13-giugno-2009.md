---
title: 'ROMA PRIDE, 13 GIUGNO 2009'
date: Mon, 30 Mar 2009 15:48:58 +0000
draft: false
tags: [Comunicati stampa]
---

13 giugno 2009: Roma Pride
--------------------------

  
**Sabato 13 giugno a Roma sfilerà il corteo** della comunità lgbtq (lesbica, gay, bisessuale, transessuale e queer) una "festa mobile" che ogni anno pone alla società e alla città le sue rivendicazioni: **PARITA'** dei diritti, riconoscimento della **DIGNITA'** delle persone lgbtq e **LAICITA'** delle istituzioni. Una festa che come sempre non è solo lgbtq, anche di tutta la società, le istituzioni e le persone che nelle rivendicazioni LGBTQ riconoscono il senso quasi tangibile della loro stessa libertà, in una prospettiva di rispetto reciproco e accettazione di tutte le forme di individuale diversità come una ricchezza per tutti.  
  
Il **2009**, per la comunità lgbtq è un anniversario particolarmente significativo: nel **1969** i [moti di Stonewall](http://it.wikipedia.org/wiki/Moti_di_Stonewall), a New York, diedero il via ad una serie di manifestazioni di protesta in tutto il mondo che chiedevano diritti e dignità per le persone omosessuali e transessuali. **A quarant’anni da quella storica ribellione ancora molto lavoro deve essere fatto per la piena libertà delle persone lgbtq.** In Italia nulla è cambiato in questi ultimi anni e la situazione è anzi divenuta particolarmente grave: nessuna legge a tutela delle persone lgbtq, nessun riconoscimento di diritti civili e sociali; **l’Italia è un’ombra grigia nel panorama della comunità europea**.  
**A questo grigiore, segno dell’arretratezza culturale e dell’inerzia politica, il RomaPride 2009 risponderà con una grande manifestazione pacifica, plurale e condivisa che, al grido di “LIBERI TUTTI, LIBERE TUTTE****!**” saprà essere voce di tutti i cittadini e tutte le cittadine che richiedono diritti civili e sociali per tutti.  
Ultimamente c’è **un’allarmante affinità tra la mancanza di diritti** per le persone omosessuali e transessuali e le nuove, **crescenti limitazioni di libertà** che colpiscono tutti e tutte, come quella esemplare sulla scelta di come morire.  
  
“**Liberi tutti, libere tutte**!” sarà quindi anche una manifestazione contro le gravi ed irricevibili posizioni vaticane ed una protesta su come l’attuale governo sta gestendo improvvidamente **i temi della sicurezza, del testamento biologico, del lavoro, dei migranti e della crisi economica in atto**.  
  
Come ogni anno, a corollario della **manifestazione del 13 giugno** ci saranno eventi culturali, artistici e sportivi aperti a tutta la cittadinanza.  
  
Certi Diritti sostiene il RomaPride 2009, partecipando Coordinamento Roma Pride insieme a tutte le realtà che sono indicate nel [sito del Roma Pride](http://www.romapride.it/). Inoltre, Certi Diritti aderisce al [Torino Pride](http://www.torinopride2006.it/index.php?id=186) del 16 maggio, al [Genova Pride](http://www.genovapride.it/) del 27 giugno e alle altre manifestazioni analoghe che, come ogni anno si terranno localmente.