---
title: '16 maggio a Roma serata contro l''omofobia e la transfobia'
date: Tue, 10 May 2011 00:38:03 +0000
draft: false
tags: [Politica]
---

**In occasione della giornata internazionale contro l'omofobia, il 17 maggio l'associazione radicale Certi Diritti sarà ricevuta dal Presidente della Camera Gianfranco Fini insieme all'On. Concia e alle altre associazioni lgbt nazionali. Il giorno prima la nostra associazione organizza una serata a Roma. Il programma.**  
**16 maggio - via di Torre Argentina, 76 ROMA**

**Ospite della serata Cristian Friscina, il ragazzo pugliese a cui non hanno rinnovato la patente perchè gay.**  
  
ORE 18  
Presentazione del libro di **Franco Buffoni**  
  
**LAICO ALFABETO IN SALSA GAY PICCANTE.**  
**L’ORDINE DEL CREATO E LE CREATURE DISORDINATE**  
  
modera **Alessandro Litta Modignani**, giornalista e membro comitato nazionale Radicali Italiani.Ne discutono con l’autore:  
**Sergio Rovasio**, Segr Associazione Radicale Certi Diritti  
**Dario Accolla, Francesco Paolo del Re, Andrea Maccarrone**  
  
ORE 19,30  
**APERITIVO con prodotti tipici marchigiani**  
  
ORE 20,30  
Dopo l’anteprima nazionale al Festival del cinema Glbt di Torino  
Proiezione del documentario  
  
**FUORI! STORIA DEL PRIMO MOVIMENTO**  
**OMOSESSUALE IN ITALIA (1971-2011)**  
  
curato da **Angelo Pezzana** ed **Enzo Cucco**  
Segue dibattito con associazioni ed esponenti del movimento Lgbte.  
  
**Hanno assicurato finora la loro partecipazione:**

  
**On Rita Bernardini, deputata Radicale - Pd; On. Paola Concia, deputata Pd; Paolo Patanè e Luca Trentini, presidente e segretario di Arcigay; Sergio Stanzani, Presidente del Partito Radicale Nonviolento;  Rita De Santis, presidente Agedo; Aurelio Mancuso, presidente Equality; Famiglie Arcobaleno; Rete Genitori Rainbow ; Mario Staderini, Segretario di Radicali Italiani; Cristiana Alicata, esponente del Pd; Rocco Berardo, Consigliere Regionale Lista Bonino Pannella, Federalisti europei alla Regione Lazio; Alba Montori, Fondazione Massimo Consoli; Imma Battaglia, e Laura Annibali, Di Gay Project; Rossana Praitano, Presidente del Circolo Mario Mieli;  Enrico Oliari, presidente Gay Lib; Nuova Proposta; Libellula; Gigliola Toniollo e Salvatore Marra,Cgil Nuovi Diritti; Andrea Rubera, Nuova Proposta; Rete Genitori Rainbow**