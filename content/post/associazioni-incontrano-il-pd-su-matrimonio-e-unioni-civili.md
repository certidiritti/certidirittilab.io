---
title: 'Associazioni incontrano il PD su matrimonio e unioni civili'
date: Wed, 26 Feb 2014 13:51:09 +0000
draft: false
tags: [Movimento LGBTI]
---

Comunicato stampa congiunto

26 febbraio 2014 alle ore 13.36

Roma, 26 febbraio 2014 - Si è svolto oggi l'incontro tra una delegazione del Partito Democratico, rappresentato da Davide Faraone, della segreteria nazionale responsabile scuola e welfare e i parlamentari Lumia, Lo Giudice e Scalfarotto, firmatari di proposte di legge sui temi "matrimonio egualitario e unioni civili" e Paola Concia della Direzione Nazionale Pd con le Associazioni nazionali LGBT Arcigay, ArciLesbica, Certi Diritti, Equality Italia, Agedo, Famiglie Arcobaleno, MIT Movimento Identità Transessuale.

In questa occasione le Associazioni hanno ribadito la loro ferma richiesta che il matrimonio civile sia esteso a tutti i cittadini e alle cittadine, dunque anche alle coppie dello stesso sesso.

Il Pd ha confermato la sua posizione di voler sostenere una proposta di legge sulla scorta del "modello tedesco", istituto equipollente al matrimonio e che include i diritti patrimoniali e la step child adoption, ovvero il diritto all'adozione da parte del genitore non biologico dei figli nati all'interno di una coppia dello stesso sesso.

Le Associazioni LGBT hanno preso atto delle posizioni del PD, che ha anche espresso la volontà di costruire una interlocuzione con loro, ma hanno precisato con forza che i diritti non sono negoziabili e né oggetto di compromessi e che una proposta di legge dovrà prima di ogni cosa soddisfare i bisogni concreti delle persone lgbt e la dignità giuridica dello loro coppie.

Dopo Scelta Civica e PD Le Associazioni LGBT proseguiranno gli incontri con le forze politiche che hanno presentato proposte di legge in Parlamento.

Fiorenzo Gimelli - Agedo

Flavio Romani – Arcigay

Giuliana Ruoti - ArciLesbica

Yuri Guaiana – Associazione Radicale Certi Diritti

Aurelio Mancuso – Equality Italia

Luca Possenti – Famiglie Arcobaleno

Cathy La Torre – MIT Movimento Identità Transessuale