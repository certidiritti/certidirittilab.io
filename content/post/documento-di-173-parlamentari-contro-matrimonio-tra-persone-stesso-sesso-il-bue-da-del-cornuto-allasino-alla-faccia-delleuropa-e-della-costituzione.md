---
title: 'Documento di 173 parlamentari contro matrimonio tra persone stesso sesso: il bue dà del cornuto all''asino. Alla faccia dell''Europa e della Costituzione'
date: Tue, 14 Aug 2012 06:06:56 +0000
draft: false
tags: [Matrimonio egualitario]
---

Accusano gli altri di confondere il dibattito con questioni ideologiche sollevando proprio questioni di tipo ideologico. E straparlano di Stato liberale.

Roma, 10 agosto 2012

Comunicato Stampa dell'Associazione Radicale Certi Diritti

Non c'è miglior sordo di chi non vuole ascoltare, e pare proprio che i 173 deputati e senatori del Pdl che hanno sottoscritto il documento contro il diritto al matrimonio tra persone dello stesso sesso ne siano i campioni. Impermeabili ai principi della Carta dei diritti fondamentali e dei Trattati europei, allergici alle senteze della corte costituzionale e della cassazione, ciechi sordi e muti di fronte ai cambiamenti della nostra società, si aggrappano a qualche risposta ideologica sul ruolo della famiglia e contro i desideri per non rispondere all'unica domanda a cui dovrebbero tentare di dare risposta: perchè?

Perchè alle persone omosessuali è negato il diritto al matrimonio, visto che di diritto si tratta secondo numerose e autorevoli sentene della corte costituzionale emesse anni fa? In che cosa sono diverse le persone omosessuali dalle persone eterosessuali? Perchè nascondere che la funzione sociale di coppie di persone dello stesso sesso non solo è riconosciuta da sentenze del più alto grado, ma è nella natura delle cose- Di quelel cose che lo Stato liberale di cui straparlano, dovrebbe occuparsi e non infischiarsene.

Perchè continuare a nascondere la verità agli italiani parlando di desideri e diritti, senza chiarire che la richiesta del matrimonio civile è anche assunzione di responsabilità e di doveri, nei confrotni del proprio partner e dei propri figli? Perchè ipocritamente e idelogicamente ci si scaglia contro gli inutili (secondo loro) registri delle unioni senza essere ideologici fino in fondo e chiedere la cancellazione delle norme sull'ISE e sull'assistenza familiare che prevedono già oggi, e senza alcuna legge nazionale come dalle loro signorie , la partecipazione del convivente (quale che sia la natura della convivenza) alle spese assistenziali?

Non è forse ideologica la vostra idea di famiglia e di società, ovvero quanto di più distante dalla natura e dalla scienza ci possa essere, che giustifica tale sequela di luoghi comuni?

Lupi travestiti da agnelli, ecco cosa siete voi 173. E grazie per averlo scritto questo documento: rimarrà nella storia italiana come il più chiaro ed esplicito documento reazionario, nemmeno conservatore, in materia di diritti indviduali e familiari. Grazie per aver fatto chiarezza, quindi. Ora sarà più chiaro a tutti chi è ideologico e chi non lo è.