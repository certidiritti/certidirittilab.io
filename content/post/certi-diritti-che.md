---
title: 'Certi diritti che'
date: Fri, 14 Feb 2014 14:18:51 +0000
draft: false
---

### Certi Diritti che le coppie conviventi non sanno di avere

[![libro2](http://www.certidiritti.org/wp-content/uploads/2014/02/libro2.jpg)](http://www.certidiritti.org/wp-content/uploads/2014/02/libro2.jpg)Le coppie conviventi, non unite nel “sacro” vincolo del matrimonio, vengono definite “coppie di fatto”. “Di fatto” e non “di diritto”, solo perché legislatori inadempienti non le hanno mai regolate, diversamente da quanto avviene in qualsiasi altra parte del mondo civile. Concepito come un manuale di sopravvivenza, il libro indica gli strumenti legalmente utilizzabili per realizzare, almeno in parte, i diritti di coppia: in che modo tutelarsi per restare insieme nel caso la vita conduca uno dei due in ospedale o in carcere, per conservare la casa, ottenere risarcimenti o congedi, stipulare convenzioni e assicurazioni, garantire che i figli non subiscano danni e discriminazioni. Per le coppie composte da persone dello stesso sesso, sono suggeriti soluzioni e rimedi che consentano importanti diritti.