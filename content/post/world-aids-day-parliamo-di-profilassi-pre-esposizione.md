---
title: 'World Aids day: parliamo di profilassi pre-esposizione'
date: Fri, 01 Dec 2017 13:20:01 +0000
draft: false
tags: [Salute sessuale]
---

In occasione del 1° Dicembre, Giornata mondiale per la lotta all'Hiv/Aids, vi proponiamo la registrazione del panel «Prep, uno strumento in più» che si è tenuto pochi giorni fa a Roma nel corso del nostro XI Congresso. La profilassi pre-esposizione è una via in più da aprire ufficialmente anche nel nostro Paese per affrontare la dura sfida al contagio da Hiv.