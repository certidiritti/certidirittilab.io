---
title: 'Presentato a Cosenza dall''Associazione Radicale Certi Diritti il libro-manuale di sopravvivenza per le coppie di fatto'
date: Mon, 25 Jun 2012 07:45:57 +0000
draft: false
tags: [Diritto di Famiglia]
---

Comunicato stampa dell’Associazione Radicale Certi Diritti

Cosenza, 25 giugno 2012

Sabato, presso la libreria Ubik di Cosenza l’Associazione Radicale Certi Diritti ha presentato il libro “Certi diritti che le coppie conviventi non sanno di avere” di Bruno De Filippis, Gabriella Friso, Gian Mario Felicetti e Filomena Gallo. Si sono incontrati a parlarne il segretario nazionale dell’Associazione Yuri Guaiana, Marco Marchese che ha moderato l’incontro, Lavinia Durantini attivista dell’Eos Arcigay Cosenza e l’Avvocata Fernanda Gigliotti del Gruppo Pd 25 aprile. Durante la serata è intervenuta anche Anna Maria D’Andrea dell’Associazione Polis Aperta.

Come tutelare i propri diritti quando si è una coppia di fatto? Esiste un modo, visto il vuoto legislativo italiano, per barcamenarsi quando si vive una relazione affettiva rispetto a una Legge che non c’è? In attesa che l’estensione del matrimonio civile sia una realtà anche per gay  e lesbiche, come ci si può tutelare nella vita di tutti giorni?

Yuri Guaiana ha spiegato come il manuale sia frutto della richiesta delle coppie che spesso chiedono come comportarsi in specifiche situazioni e come tutelarsi in un contesto di completo vuoto legislativo. Il manuale pur fornendo tante risposte e soluzioni pratiche, illustra molto bene il calvario che le coppie di fatto in Italia sono costrette a vivere in assenza di un riconoscimento giuridico. Per ovviare a questa situazione occorre una riforma complessiva del diritto di famiglia che, prima di tutto, sani la più odiosa discriminazione che impedisce alle coppie dello stesso sesso di accedere all’istituto del matrimonio civile e, contestualmente, offra altri istituti giuridici più flessibili per riconoscere le varie forme in cui oggi i cittadini italiani organizzano i propri affetti.

L’avv. Fernanda Gigliotti ha posto l’accento, senza nascondere la propria amarezza, su quanto distante sia l’Italia dal resto dell’Europa e la politica italiana dai cittadini.

E’ ora che il parlamento rispetti quanto chiesto dalla Corte Costituzionale con la sentenza 138/2010 e garantisca il diritto fondamentale delle coppie omosessuali e delle coppie non matrimoniali in genere, a vedere riconosciute giuridicamente le proprie unioni. Questo è quanto hanno chiesto all’unisono gli oratori dell’incontro, che è stato registrato da Liberi.tv, sito dal quale l’incontro potrà essere riascoltato integralmente.

  
**[Il video della presentazione del libro a Cosenza >](http://www.youtube.com/watch?v=145Wxm_75Xw&;feature=plcp)**