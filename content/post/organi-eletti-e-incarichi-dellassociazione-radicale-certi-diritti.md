---
title: 'ORGANI ELETTI E INCARICHI DELL''ASSOCIAZIONE RADICALE CERTI DIRITTI'
date: Sat, 27 Mar 2010 19:22:55 +0000
draft: false
tags: [certi diritti, direttivo, incarichi, organi, radicale, Senza categoria]
---

**Segretario****:**

Sergio Rovasio

[segretario@certidiritti.it](mailto:segretario@certidiritti.it)

**Presidente****:**

Rita Bernardini

[presidente@certidiritti.it](mailto:presidente@certidiritti.it)

**Tesoriere****:**

Giacomo Cellottini

[tesoriere@certidiritti.it](mailto:tesoriere@certidiritti.it)

**Rappresentante presso il "Comitato di Radicali Italiani":**

Maria Gigliola Toniollo

[radicali@certidiritti.it](mailto:radicali@certidiritti.it)

**Membri del Direttivo**: Gian Mario Felicetti (coordinatore), Jean Baptiste Poquelin, Maurizio Cecconi, Clara Comelli, Riccardo Cristiano, Enzo Cucco, Gabriella Friso, Maria Gigliola Toniollo, Matteo Di Grande, Yuri Guaiana, Eduardo Melfi, Ottavio Marzocchi, Alba Montori, Dario Vese.

ll Direttivo di Certi Diritti, riunito il giorno 27 marzo 2010, precisa e chiarisce che la rappresentanza dell'Associazione a tutti gli eventi, manifestazioni, riunioni operative e incontri promossi con altre realtà associative e/o con singole/i, sia locali che nazionali, può essere svolta solo ed esclusivamente da coloro che ricoprono cariche elettive nell'Associazione (Presidente, Segretario, Tesoriere e il Rappresentante di Certi Diritti presso il Comitato nazionale di Radicali Italiani), ovvero dalle persone indicate nella pagina del sito Organi ed incarichi per i temi e gli ambiti di loro competenza, salve eventuali decisioni diverse degli organi elettivi.

Fuori delle fattispecie appena indicate, chiunque partecipi a tali incontri ed eventi, pur essendo iscritto all'Associazione Certi Diritti, è invitato a precisare che i suoi interventi, se non concordati con gli organi dell'Associazione, sono fatti a titolo personale.

**Responsabile News Letter**

**Responsabile campagna "Affermazione Civile":**

Gabriella Friso

Gian Mario Felicetti

[newsletter@certidiritti.it](mailto:newsletter@certidiritti.it)

[affermazionecivile@certidiritti.it](mailto:affermazionecivile@certidiritti.it "blocked::mailto:affermazionecivile@certidiritti.it blocked::mailto: <script language='JavaScript' type='text/javascript'> <!-- var prefix = 'mailto:'; var suffix = ''; var attribs = ''; var path = 'hr' + 'ef' + '='; var addy61233 = 'ottavio.marzocchi' + '")

**Responsabile delle questioni ed iniziative europee:**

Ottavio Marzocchi

[questionieuropee@certidiritti.it](mailto:questionieuropee@certidiritti.it)

**Responsabile degli studi giuridici:**

Bruno de Filippis

[studigiuridici@certidiritti.it](mailto:studigiuridici@certidiritti.it)

**Rappresentanti presso il comitato "Sì, lo Voglio":**

Enzo Cucco

[silovoglio@certidiritti.it](mailto:silovoglio@certidiritti.it)

**Web-master****:**

Giovanni B.

[webmaster@certidiritti.it](mailto:webmaster@certidiritti.it)

{jcomments off}