---
title: 'Coppie gay: Gasparri e Giovanardi con lettera a Napolitano su sentenza Cassazione fanno propaganda politica e ignorano volutamente sentenza della Consulta'
date: Wed, 21 Mar 2012 12:39:20 +0000
draft: false
tags: [Diritto di Famiglia]
---

Roma, 21 marzo 2012  
Comunicato Stampa dell'Associazione Radicale Certi Diritti

I grandi geni della politica pidiellina Gasparri e Giovanardi, con la loro lettera al Capo dello Stato, in qualità di Presidente del Csm, lamentano che il Giudice della Cassazione Salvatore Di Palma con la sentenza della Corte di Cassazione n. 4184 del 15 marzo 2012, sui diritti delle coppie gay, ha “espresso opinioni del tutto personali su come il Parlamento dovrebbe operare in futuro".

Lorsignori, per farsi belli davanti al cardinale o monsignore di turno, dimenticano che anche la Corte Costituzionale con la sentenza 138/2010  ha detto in modo chiaro e netto che la classe politica deve dare riconoscimento alle coppie conviventi che hanno rilevanza costituzionale. Entrambe le sentenze chiariscono definitivamente che i diritti non riconosciuti per l’immobilismo della classe politica possono anche essere fondato motivo per ricorsi legali delle coppie che con queste leggi subiscono ingiuste discriminazioni.

E’ evidente che il loro nervosismo è dovuto al fatto che ormai in Italia, l’opinione pubblica, è sempre più consapevole della necessità di riconoscere le unioni e il matrimonio civile anche tra persone dello stesso sesso e le stesse forze politiche, pur con timidezze e ambiguità di ogni genere,  dovranno prima o poi affrontare la questione.

**[DAI FORZA AI DIRITTI! ISCRIVITI A CERTI DIRITTI >](iscriviti)**

[**RICEVI LA NEWSLETTER >**](newsletter/newsletter)