---
title: 'I NOSTRI GADGET'
date: Sat, 17 Sep 2011 13:34:25 +0000
draft: false
tags: [Politica]
---

**![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)**

Se vuoi, puoi acquistare i nostri gadget: magliette e spillette con il logo di Certi Diritti. 

In questo modo, oltre a contribuire con un pagamento, potrai regalare questi gadget ai tuoi amici, parenti, colleghi o persone che ti sono vicine. E puoi  indossarla in tutti i momenti in cui ritieni importante dare una voce alla rivendicazione dei diritti civili per tutte le persone in Italia. 

L'acquisto puoi farlo dal nostro sito, [questo il link](gadget/rokquickcart)