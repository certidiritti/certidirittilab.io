---
title: 'E'' morta Roberta Franciolini. Il ricordo di Sergio Rovasio'
date: Fri, 18 Mar 2011 22:05:04 +0000
draft: false
tags: [1982, bologna, certi diritti, Comunicati stampa, legge 164, marcella di folco, mit, pannella, radicale, rovasio, trans]
---

Il Segretario dell'Associazione Radicale Certi Diritti ricorda la storica militante del movimento italiano transessuali.  
  
Roma, 18 marzo 2011 Oggi è morta Roberta Franciolini, storica militante del movimento transessuale italiano, tra le fondatrici del Mit (Movimento Italiano Transessuali). Insieme a Marcella Di Folco, Gianna Parenti e Pina Bonanno, è stata tra le più attive militanti impegnate per la promozione e difesa dei diritti civili, in particolare a Roma, dove fino alla fine degli anni ’80 coordinava le animate riunioni settimanali del Mit che si svolgevano presso la sede storica dei Radicali in Via di Torre Argentina, 18.  
  
Quando nei primi mesi del 1982 si organizzarono mobilitazioni e manifestazioni in tutta Italia per l’approvazione della proposta di legge dei Radicali, n. 164  ‘Norme in materia di rettificazione di attribuzione di sesso’, Roberta  era la principale animatrice, con incluse risse e litigi, dei Sit-In in Piazza Montecitorio, occupata permanentemente dal movimento delle persone transessuali insieme ai sostenitori della proposta di legge. Il Gruppo Parlamentare Radicale, al 6° piano di Via Uffici del Vicario, fu letteralmente ‘occupato’ dal Mit e ne divenne l’ufficio stampa e la centrale delle iniziative di sensibilizzazione rivolte ai deputati di tutti i gruppi politici, con l’obiettivo di far approvare la legge in tempi brevi. In quei giorni Roberta e Marcella furono le principali animatrici dei corridoi e degli uffici del Palazzo.  
  
Roberta, era molto simile a Marcella, e forse per questo spesso litigavano. Ciò che le accomunava era certamente una forte testimonianza di generosità umana e politica che le fece diventare  il punto di riferimento di molte persone transessuali emarginate, discriminate, sfruttate e che subivano gravi violenze fisiche.  
  
Recentemente aveva espresso la sua intenzione di rimettersi in piazza per continuare la sua lotta per il superamento delle diseguaglianze ma problemi di salute glielo avevano impedito.  
  
Poco più di un anno fa fece uno dei suoi ultimi interventi a Roma presso la sede dei Radicali in occasione dell’iniziativa ‘Io sono una puttana’ promossa dall’Associazione Radicale Certi Diritti, insieme al Comitato per i Diritti Civili delle prostitute, e l’Associazione La Strega da bruciare, per commemorare la giornata mondiale per la lotta alla violenza contro le sex worker che si celebra ogni anno il 17 dicembre.  
  
L’Associazione Radicale Certi Diritti esprime il suo cordoglio per la scomparsa di Roberta, donna forte e straordinaria.  
  
Al seguente link uno dei suoi ultimi interventi pubblici:  
http://www.youtube.com/watch?v=ebGaOLF2Prc