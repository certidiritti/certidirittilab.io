---
title: 'Miseramente fallito il proibizionismo sulla prostituzione'
date: Sat, 29 Jan 2011 10:50:42 +0000
draft: false
tags: [alemanno, BERLUSCONI, Carfagna, certi diritti, cocaina, comune, Comunicati stampa, ordinanza, pia covre, Prostituzione, radio radicale, roma, Sergio Rovasio, tratta]
---

**Fallimento proibizionismo sulla prostituzione: oggi sui media tre diverse fonti di informano dell'incremento della prostituzione e della tratta nella sola città di Roma. Niente da dire Governo? Niente da dire signor sindaco Alemanno?**

**_Dichiarazione di Sergio Rovasio, Segretario dell’Associazione Radicale Certi Diritti_**

“Oggi, sfogliando alcuni quotidiani, ci è capitato di leggere articoli, con tre diverse autorevolissime fonti, che di fatto documentano del miserabile fallimento delle politiche proibizioniste sulla prostituzione.

Sarebbe ora che i grandi esperti della ‘sicurezza’, i grandi leader politici del populismo e della demagogia, gli incantatori di serpenti che ‘puliscono le strade’ da spettacoli ‘osceni e immondi’, ammettessero senza ipocrisie di come le loro politiche siano miseramente fallite. E invece tutti zitti, tutti silenti, tutti fintamente occupati a fare altro, magari a dover far fronte alle emergenze di qualche politico dello schieramento perbenista, coinvolto in scandali con prostitute e cocaina.

**Ecco le tre fonti che oggi troviamo sui media:**

1.  Il Presidente della Cassazione, Ernesto Lupo, ieri, in occasione dell’inaugurazione dell’Anno Giudiziario:  “Nel Lazio e, particolarmente, a Roma è in forte espansione il traffico di stupefacenti e appare consistente la criminalità di provenienza romena e nigeriana, che operano in particolare nei reati di tratta della persone e sfruttamento della prostituzione”.
2.  La Direzione Distrettuale Antimafia documenta maxi-blitz contro la “tratta delle bimbe” e grazie al loro operato si giunge ad un maxi-processo con decine di imputati che sfruttavano donne minorenni dell’est europeo, schiavizzate nelle strade di Roma e Ostia.
3.  Il Codici (Centro per i diritti dei cittadini) diffonde il terzo rapporto sulla criminalità organizzata dal titolo: “Le mani della criminalità sul Comune di Roma e Provincia”, “Droga e prostituzione, il grande business del malaffare”, vi è anche la mappatura aggiornata delle strade cittadine della prostituzione.

Possiamo umilmente chiedere quanti soldi sono stati spesi per la lotta alla prostituzione da quando è entrata in vigore l’ordinanza antiprostituzione del Comune di Roma del 16 settembre 2008? Possiamo sapere per quale motivo con la manovra economica del Governo sono stati tagliati i fondi per aiutare e proteggere le ragazze vittime della tratta della prostituzione, anche minorile? Con tutti i soldi destinati alle politiche del proibizionismo demagogico e populista, lor signori, hanno mai pensato che forse una minima parte può essere destinato ad uno studio sugli effetti delle politiche di legalizzazione e quindi regolamentazione del fenomeno? Con anche la certezza dell’abbattimento sicuro della criminalità e della commistione tra mercato della droga e tratta della prostituzione”.