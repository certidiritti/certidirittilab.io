---
title: 'Gay Pride di Mosca: vice sindaco lo autorizza'
date: Wed, 27 Apr 2011 08:21:24 +0000
draft: false
tags: [Africa]
---

Per anni la manifestazione è stata vietata. Certi Diritti si congratula con l'organizzatore Nikolay Alexeyev. La lotta per i diritti civili va avanti.

Comunicato Stampa dell’Associazione Radicale Certi Diritti  
Roma, 26 aprile 2011

La notizia che il Comune di Mosca ha autorizzato per la prima volta il Gay Pride, che si svolgerà il prossimo 28 maggio, ci rende felici e dimostra che l’impegno e la lotta per i diritti civili prima o poi porta a risultati concreti. Quattro anni fa, al Gay Pride di Mosca, vietato dalle autorità, diverse personalità europee ed esponenti del Partito Radicale Nonviolento, tra gli altri Marco Cappato, Deputato europeo, Ottavio Marzocchi, funzionario del Parlamento Europeo, l’allora  deputata italiana di Rifondazione Comunista Vladimir Luxuria ed alcuni Radicali moscoviti, subirono arresti e violenze, anche da parte di esponenti del fondamentalismo religioso. La colpa era quella di aver partecipato all’iniziaitiva per dare sostegno alla comunità locale. Negli anni successivi il copione si è ripetuto e Nikoly Alexeyev, quando partecipò lo scorso novembre al Congresso di Certi Diritti, annunciò  che quest’anno, se fosse stato opposto il divieto, si sarebbe fatto arrestare.  
L’Associazione Radicale Certi Diritti ha mandato un messaggio di sostegno e auguri a Nikolai Alexeyev. Anche a Mosca la lotta per il superamento delle diseguagliane va avanti.