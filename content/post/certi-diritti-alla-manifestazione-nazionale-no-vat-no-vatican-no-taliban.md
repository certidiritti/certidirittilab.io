---
title: 'CERTI DIRITTI ALLA MANIFESTAZIONE NAZIONALE NO VAT: NO VATICAN - NO TALIBAN'
date: Sat, 13 Feb 2010 08:48:29 +0000
draft: false
tags: [Comunicati stampa]
---

**L’ASSOCIAZIONE RADICALE CERTI DIRITTI OGGI ALLA MANIFESTAZIONE NAZIONALE NO VAT CON LO SLOGAN: NO VATICAN - NO TALIBAN.**

Oggi, sabato 13 febbraio, alle ore 14, l’Associazione Radicale Certi Diritti, parteciperà alla manifestazione nazionale No Vat con lo slogan No Vatican - No Taliban.

Appuntamento alle ore 14 in Piazza Bocca della Verità – Roma.

Certi Diritti partecipa oggi alla manifestazione No Vat contro l’invadente ingerenza delle gerarchie vaticane nella vita politica e istituzionale italiana e contro l’uso sempre più spudorato e strumentale delle religioni come ideologia per la conquista e l’occupazione del potere.

L’Associazione Radicale Certi Diritti intende promuovere la tolleranza e la libertà anche religiosa espressione della cultura laica e liberale contro tutti gli integralismi e totalitarismi, contro la cultura della proibizione e del divieto per la libertà, la responsabilità e l’uguaglianza delle persone.