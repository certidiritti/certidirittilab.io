---
title: 'Coppie stesso sesso: importante presa di posizione di Bersani, attendiamo però conferma dalla Bindi. La Corte ha detto anche altro...'
date: Tue, 31 Jul 2012 12:08:04 +0000
draft: false
tags: [Diritto di Famiglia]
---

Roma, 31 luglio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Le dichiarazioni di oggi del Segretario del Pd sulla necessità di dare sostanza normativa alle coppie conviventi dello stesso sesso,  richiamandosi al principio riconosciuto dalla Corte Costituzionale con la sentenza 138/2010, è molto importante. Speriamo che la Presidente del Pd Rosy Bindi ora non lo smentisca e non si inventi tesi giuridiche sul fatto che il riconoscimento dell’uguaglianza impone modifiche costituzionali. La Corte Costituzionale con quella sentenza, avuta grazie all’impegno delle coppie che parteciparono alla camapagna di Affermazione Civile, ha detto in modo chiaro che:

1)  il riconoscimento che l’unione omosessuale, come stabile convivenza, è una formazione sociale degna di garanzia costituzionale perché espressione del diritto fondamentale di vivere liberamente una condizione di coppia.

2) neppure il concetto di matrimonio è cristallizzato dall’Art. 29 della Costituzione e quindi non è precluso alla legge disciplinare il matrimonio tra gay, anche se restano possibili per il legislatore soluzioni diverse.

3) il legislatore deve intervenire e se non interviene la Corte potrà intervenire per ipotesi particolari, in cui sia necessario costituzionalmente un trattamento omogeneo tra la coppia coniugata e la coppia omosessuale.

Quando uscì' la sentenza il Prof. Vittorio Angiolini, Ordinario di Diritto Costituzionale all’Università Statale di Milano dichiarò che: “nelle motivazioni della sentenza della Corte costituzionale viene esplicitata la necessità di un riconoscimento costituzionale delle unioni omosessuali, resta al legislatore di provvedere senza discriminare le persone gay dalle persone eterosessuali”.  La Professoressa Marilisa D’Amico, Ordinario di Diritto Costituzionale all’Università Statale di Milano e l’Avvocato Massimo Clara, anche loro membri del Collegio di difesa davanti alla Corte,  dichiararono che “questa è una prima tappa in cui la Corte indica la strada costituzionale per il riconoscimento della piena uguaglianza fra coppie omosessuali ed eterosessuali e se il Parlamento non intervenisse sarebbe evidente il vulnus costituzionale per il riconoscimento delle unioni tra coppie gay”.