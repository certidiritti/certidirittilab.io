---
title: 'Udienza in Corte di Appello per l''unione tra persone dello stesso'
date: Sat, 21 May 2011 06:15:49 +0000
draft: false
tags: [Diritto di Famiglia]
---

Genova 19-05-11:

Si è tenuta questa mattina alle ore 9,30 presso la Corte di Appello di Genova l’udienza per il ricorso al diniego per l’unione civile tra Manuel Incorvaia e Francesco Zanardi, rilasciato dal comune di savona.

Questa campagna di Affermazione Civile, lanciata dall’associazione Radicale Certi Diritti nel 2007 sta procedendo facendo piccoli passi avanti attraversando tutto l’iter giuridico. L’udienza di ricorso si è svolta a porte chiuse, presente il nostro avvocato  Francesco Bilotta della rete di avvocatura per i diritti GLBT, Rete Lenford. I giudici hanno ascoltato le parti riservandosi di decidere. A nostro avviso un ottimo risultato, tenedo conto della legge Regionale Ligure dei verdi Morelli e Vasconi, dell’appello del nostro Presidente Giorgio Napolitano.

 Ricordo tra le altre cose che L’associazione Radicale Certi Diritti ha depositato una memoria di risposta alla consultazione pubblica lanciata dalla Commissione europea sul Libro verde "Adempimenti amministrativi per i cittadini - promuovere la libera circolazione dei documenti pubblici e il riconoscimento degli effetti degli atti di stato civile" del 14 dicembre 2010.

La comunicazione della Commissione europea aveva come obiettivo quello di fare una serie di proposte e sollevare domande in merito alle aspettative dei cittadini europei, delle ONG e delle istituzioni sui prossimi passi da compiere nell’ UE per abbattere gli ostacoli amministrativi opposti dagli Stati membri alla libera circolazione dei cittadini, promuovendo cosi la libera circolazione dei documenti pubblici ed il riconoscimento transnazionale degli effetti degli atti di stato civile.

Certi Diritti, nella sua risposta alla consultazione, ha denunciato il fatto che l’attuale situazione viola i diritti delle persone LGBT in quanto gli Stati membri compiono discriminazioni basate sull’orientamento sessuale nel non riconoscere i matrimoni e le partnership registrate delle coppie dello stesso sesso celebrate negli Stati membri ove questo è possibile, in violazione del mandato politico contenuto nei Trattati UE e nella Carta dei diritti fondamentali.

La Commissione ha quindi il dovere di proporre il superamento di tali violazioni e discriminazioni. L'Associazione Radicale Certi Diritti sollecita quindi la Commissione europea a proporre misure di mutuo riconoscimento dei documenti ed atti (ad esempio attraverso la soppressione dell’autenticazione, della legalizzazione e della postilla, un certificato europeo di stato civile e l’utilizzo di moduli europei plurilingue) e dei loro effetti (ad esempio attraverso il riconoscimento di pieno diritto).

Francesco Zanardi

Portavoce del movimento Gay Italiani