---
title: 'L’Associazione Radicale Certi Diritti cerca un nuovo logo'
date: Mon, 27 Sep 2010 04:45:51 +0000
draft: false
tags: [certi diritti, certidiritti, colori, Comunicati stampa, design, diritti civili, logo, sito web]
---

Abbiamo lanciato il concorso per trovare il nuovo logo dell'Associazione sul portale [www.12designer.com](http://www.12designer.com/it/project/2076/details/)

Aiutaci a diffondere la voce tra i designer grafici che conosci. C'è tempo fino al 26 ottobre 2010.

Ecco il bando del concorso, rintracciabile al seguente indirizzo web:

http://www.12designer.com/it/project/2076/details/

**Oggetto**: Nuovo logo dell’Associazione Radicale Certi Diritti

Il logo deve avere prioritariamente, queste caratteristiche: semplicità, riconoscibilità e riproducibilità. Indicazioni tecniche per il logo:

1\. Dovrebbe rappresentare una evoluzione, o in qualche modo richiamare riconoscibilmente il vecchio logo (In allegato).

2\. Non dovrebbe avere i colori e la simbologia classici del mondo lgbt ([en.wikipedia.org/wiki/LGBT_symbols):](http://en.wikipedia.org/wiki/LGBT_symbols%29:) per "vocazione", è aperta anche al contributo e ai temi delle persone non omosessuali o transessuali, in tutte le declinazioni.

Per quanto riguarda i colori si tenga presente che i tre più utilizzati e visibili in Italia relativi al mondo lgbt sono l’arcobaleno, il viola e il verde.  
In allegato i colori da non usare come primari.

3\. Deve essere tenuta presenta la necessità di riprodurre il logo in molti differenti contesti grafici e comunicativi: carta intestata, manifesti, bandiere, web e social networks, merchandising di ogni tipologia.

4\. Il logo non deve contenere il titolo dell’associazione, ma deve comunque essere affiancato ad esso.

**Nome completo e natura dell’organizzazione**: Associazione Radicale Certi Diritti

**Natura dell’organizzazione**: Associazione politica per la difesa dei diritti sessuali delle persone e per la difesa dei diritti delle persone omosessuali e transgenere. L’associazione si richiama alla tradizione politica ed al metodo radicale nella individuazione degli obiettivi da raggiungere e nelle modalità di iniziativa.

**Target**: Il target primario dell’organizzazione sono non solo le persone gay, lesbiche e transgenere (transessuali e non solo) ma, occupandosi l'Associazione di diritti sessuali della persona, l'intera popolazione e per altro verso, l'insieme delle persone che hanno a cuore i diritti civili delle minoranze.  
  
Grande attenzione deve essere data alla declinazione di genere (maschile e femminile).

In allegato alcuni manifesti storici radicali, simboli e loghi della galassia radicale che possono aiutare a comprendere meglio lo stile di comunicazione che ci caratterizza.

Per maggiori informazioni: [www.certidiritti.it](undefined/)

\# Il prezzo netto è inteso come comprendente il trasferimento dei diritti d'autore.