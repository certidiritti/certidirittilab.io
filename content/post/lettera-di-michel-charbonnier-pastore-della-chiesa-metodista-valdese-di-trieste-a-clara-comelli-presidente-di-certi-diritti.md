---
title: 'LETTERA DI MICHEL CHARBONNIER, PASTORE DELLA CHIESA METODISTA VALDESE DI TRIESTE, A CLARA COMELLI, PRESIDENTE DI CERTI DIRITTI'
date: Mon, 26 Apr 2010 13:49:18 +0000
draft: false
tags: [Comunicati stampa]
---

Gent. Sig.ra Comelli,  
  
Grazie per aver condiviso le informazioni sull'iniziativa, che mi trova profondamente solidale, come individuo e cittadino in primo luogo, poi in quanto cristiano, e infine come pastore della chiesa valdese e metodista di Trieste.

Le affermazioni del card. Bertone trasudano una mancanza di sensibilità che mi avvilisce e mi indigna, come dovrebbe avvilire ed indignare tutti e tutte coloro che si definiscono cristiani e cercano di vivere nella propria vita quell'Evangelo che è prima di tutto annuncio dell'amore incondizionato di Dio per tutti gli esseri umani.  
  
L'equazione mediatica tutta italiana secondo la quale il Vaticano esprime le opinioni dei cristiani nel loro insieme non fa che alimentare i pregiudizi e la supposta contrapposizione sui temi etici tra cristiani e non cristiani, contrapposizione che non corrisponde necessariamente a realtà. Va affermato chiaramente che, nonostante le opinioni del Vaticano, si può essere sinceramente e pienamente cristiani ed allo stesso tempo, anzi proprio per questo, credere che l’omosessualità è uno dei molteplici volti dell’amore, e come tale, possiede pari diritti e pari dignità rispetto a tutti gli altri. Dire o anche solo sottintendere che sia una malattia è frutto dell’ignoranza, e farlo chiamando in causa l’Evangelo significa ergersi con infinita arroganza a detentori di una Verità che invece non ci appartiene, ma che ci supera continuamente, e che siamo chiamati a ricercare ogni giorno con infinita umiltà.  
  
Se è vero che Dio è amore, allora l’arroganza umana di voler imporre dei limiti all’amore equivale al voler imporre dei limiti a Dio.  
  
Che tutto ciò venga da una chiesa cristiana mi offende, mi addolora, mi indigna.  
  
Purtroppo non sarò a Trieste domani, ma darò la più ampia diffusione all’informazione, e le assicuro ancora la mia piena adesione e solidarietà.  
  
  
Cordiali saluti,  
  
Michel Charbonnier