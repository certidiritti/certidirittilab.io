---
title: 'AFFERMAZIONE CIVILE: TRIBUNALE VENEZIA RINVIA DECISIONE ALLA CONSULTA'
date: Mon, 20 Apr 2009 13:19:50 +0000
draft: false
tags: [Affermazione Civile, Comunicati stampa, consulta, CORTE COSTITUZIONALE, GAY, tribunale]
---

AFFERMAZIONE CIVILE: RICONOSCIMENTO MATRIMONIO PER COPPIE GAY, LA CONSULTA CHIAMATA A DECIDERE SUL CASO DI UNA COPPIA DI VENEZIA  
   
Comunicato Stampa dell'Associazione Radicale Certi Diritti. Nell'ambito della iniziativa di Affermazione Civile, volta al riconoscimento del diritto al matrimonio civile per le coppie dello stesso sesso, e condotta dall'Associazione Radicale Certi Diritti in stretta collaborazione con L'Avvocatura per i diritti LGBT - Rete Lenford, va registrato un primo significativo e rilevantissimo risultato.  
Infatti il tribunale di Venezia sul ricorso di una coppia di uomini che aveva impugnato il diniego alle loro pubblicazioni presso lo stesso tribunale, ha rinviato lo stesso procedimento alla Corte Costituzionale.  
L'Associazione Radicale Certi Diritti apprende questa notizia con entusiasmo e fiducia; spera ora che la Consulta accolga la richiesta del Tribunale veneziano in ossequio al principio di uguaglianza e di non discriminazione sancito dalla nostra Carta fondamentale.  
L'Associazione continua a rimanere vicino a tutte le coppie che hanno aderito all'iniziativa di Affermazione Civile e si augura che altre se ne aggiungano per rafforzare questa importante battaglia di civiltà.