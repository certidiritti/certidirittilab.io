---
title: 'Forum Famiglie minaccia di portare un milione in piazza: Parlamento lavori sereno su unioni civili'
date: Mon, 23 Apr 2012 14:10:21 +0000
draft: false
tags: [Diritto di Famiglia]
---

Il Parlamento non si faccia intimidire dal Forum delle Famiglie. E' bene che le unioni civili vengano regolamentate, lo chiede l'Europa. Il Parlamento non obbedisca a diktat clericali.

Roma, 23 aprile 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Il Forum Famiglie torna all’attacco e ora minaccia di portare un milione di persone in piazza contro la decisione della Commissione Giustizia di avviare l’iter di discussione delle diverse proposte su unioni civili e matrimonio tra persone dello stesso sesso. E’ bene ricordare a lorsignori che l’Unione Europea, il Consiglio d’Europa e le stesse sentenze della Corte Costituzionale e della Corte di Cassazione sollecitano da tempo iniziative legislative per il riconoscimento di diritti ora negati per le coppie conviventi gay ed eterosessuali. Comprendiamo bene che per lorsignori questi argomenti non contano molto  ma sarebbe finalmente ora che tutti si rendano conto che ormai non si possono più rinviare temi che riguardano la qualità e la serenità della vita di milioni di persone.

Questi signori farebbero bene a occuparsi dei vari tagli economici fatti in questi anni contro le famiglie dai loro riferimenti politici e da come la famiglia è stata da loro difesa anziché minacciare manifestazioni pagate da parrocchie e poteri clerical-fondamentalisti.

Ci auguriamo che la Commissione Giustizia e il Parlamento tutto svolga serenamente il suo lavoro e non si faccia intimidire da questi signori.

  
**[Ora puoi contribuire all'associazione radicale Certi Diritti anche con una telefonata.Scopri come >](iscriviti)**