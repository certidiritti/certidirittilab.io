---
title: 'Pia Covre'
date: Thu, 28 Jan 2016 16:11:45 +0000
draft: false
---

Pia Covre, nata a Milano il 2 agosto 1947, è una prostituta ed attivista italiana per i diritti civili. Attualmente vive a Pordenone.

Da sempre è impegnata in attività di sostegno alle prostitute e nella sensibilizzazione dell'opinione pubblica e della classe politica sui problemi correlati.

Nel 1982, insieme a Carla Corso e ad altre colleghe, ha fondato il **Comitato per i diritti civili delle prostitute** (CDCP), al fine di dare aiuto «alle persone prostitute», e nel 1985 il giornale Lucciola.