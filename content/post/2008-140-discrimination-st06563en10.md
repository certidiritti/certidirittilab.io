---
title: '2008-140-Discrimination-ST06563.EN10'
date: Tue, 16 Feb 2010 15:41:57 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 16 February 2010

Interinstitutional File:

2008/0140 (CNS)

6563/10

LIMITE

SOC 120

JAI 140

MI 52

  

  

  

  

  

NOTE

from :

The General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

16063/09 SOC 706 JAI 836 MI 434

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached a note from the Dutch delegation with a view to the meeting of the Working Party on Social Questions on 18 February 2010.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**Text proposals by the Dutch delegation**

**Introduction**

In previous working group meetings, the Dutch delegation has raised questions with regard to the scope of the proposed Directive as defined in Article 3. Once the scope in Article 3 is clear, the other articles and in particular Articles 4, 4a, 4b and 15 can be discussed in greater depth.

The Dutch delegation has the following proposals concerning article and 3 and the recitals.

Additions are in bold and deletions are marked “**\[...\]** “

**Proposals**

**Article 3 paragraph 1 subsection d, last sub paragraph, and recital 17d**

This text proposal is based on article 3 paragraph 1 of directive 2004/113. The aim of this text proposal is to protect certain activities that are carried out inside the area of private and family life in the privacy of people’s homes. These situations should be protected. This is already laid down in recital 17 and 17d.

“subparagraph (d) shall apply to natural persons only insofar as they are performing a professional or commercial activity defined in accordance with national laws and practice and **insofar the goods and services are offered outside the area of private and family life.**”

Furthermore the delegation proposes that a sentence is added at the end of recital 17d.

“**This Directive should not apply to transactions carried out in the area of private and family life**”

**Recital 17c (new)**

At a previous meeting the representative of the Council Legal service has indicated that goods and services can only be seen as goods and services under the Treaty on the European Union when there is some form of remuneration. The Netherlands would like to explicitly mention this in the recital since only this type of goods and services falls within the scope of the directive as follows:

“**This directive covers the supply of goods and services which are available to the public. Considering the Treaty, this directive only covers goods and services for which remuneration is normally required. Properties such as buildings only fall under the scope of the directive if a good or service as covered by this directive is supplied from this property.**”  
**_Article 3 paragraph 2 subsection b_**

_Text proposal to follow._

**Article 3 paragraph 2 subsection d**

The Dutch delegation believes that it is important to clearly distinguish the aspects of education that fall under the exclusive competence of Member States. In the Netherlands children with special needs have a choice to attend special needs education and under circumstances the option tot receive special funding in order to attend mainstream education. It should be clarified that the conditions of eligibility concerning special education and the above mentioned special funding fall outside the scope of the Directive, as follows:

(d) the organisation of the Member States' educational systems, the content of teaching and educational activities, including **\[...\] the conditions of eligibility for special needs education or special funding for children with special needs who attend mainstream education**; and

**Recital 17a second paragraph**

The Member States are responsible for the organisation and content of systems of social protection, health care and education, as well as **conditions of eligibility** **for** social protection benefits, medical treatment and **different types of** education including **funding facilities for children with special needs**.

**Article 3 paragraph 6 (new)**

In a first reaction to the Dutch note on public space (15430/09), both the Council Legal Service and the Commission have stated that “public space” as defined in this note falls outside the scope of the Directive. The Dutch delegation wishes to clarify this in the body of the text and proposes the following text for a _new_ paragraph 6 to article 3.

“**This directive does not apply to the public space such as road- and water infrastructures.**”

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_