---
title: 'Facciamo chiarezza sul censimento, anche delle coppie gay'
date: Wed, 13 Apr 2011 16:48:57 +0000
draft: false
tags: [Politica]
---

<!\-\- @font-face { font-family: "ヒラギノ角ゴ Pro W3"; }p.MsoNormal, li.MsoNormal, div.MsoNormal { margin: 0cm 0cm 0.0001pt; font-size: 12pt; font-family: "Times New Roman"; }p.Intestazioneepidipagina, li.Intestazioneepidipagina, div.Intestazioneepidipagina { margin: 0cm 0cm 0.0001pt; font-size: 10pt; font-family: "Times New Roman"; color: black; }p.Corpo, li.Corpo, div.Corpo { margin: 0cm 0cm 0.0001pt; font-size: 12pt; font-family: "Times New Roman"; color: black; }div.Section1 { page: Section1; } -->

Roma, 13 aprile 2011

Il sindacato [USI RdB Ricerca](http://www.usirdbricerca.info/index.php?option=com_content&view=article&id=1221:listat-disattende-gli-impegni-assunti-con-larcigay-anche-il-prossimo-censimento-ignorera-le-coppie-conviventi-dello-stesso-sesso&catid=98:istat&Itemid=475) ha diffuso la notizia, rimbalzata su diversi media, che in occasione del prossimo censimento della popolazione (da tenere questo autunno) "l’Istat ignorerà le coppie di persone conviventi dello stesso sesso". Posta in questa termini, l’affermazione è falsa.

1) Rivediamo i fatti.

Anche a seguito di un incontro tra la dirigenza dell’Istat e nove associazioni LGBT (dunque non solo Arcigay), l’istituto di statistica aveva sottoposto per l’approvazione al Garante per la protezione dei dati personali (il “Garante della privacy”) una bozza di questionario in cui le forme famigliari venivano censite, come dieci anni fa, chiedendo ad ogni singolo individuo di ogni famiglia quale sia la sua relazione con il “capofamiglia”. Tra le 18 risposte possibili, oltre a “coniuge”, figlio, genitore e altre, c’erano sia “Convivente dell’intestatario, in coppia di sesso diverso” sia “Convivente dell’intestatario, in coppia dello stesso sesso”.

Il Garante ha bocciato questa proposta con provvedimento n. 067 del 16 febbraio 2011, in quanto le due formulazioni citate (anche quella relativa alle coppie di persone di sesso diverso, dunque) avrebbero implicato “una raccolta di informazioni sensibili, in quanto idonee a rivelare la vita sessuale”. Dunque, secondo il Garante la risposta a tale domanda (una delle più importanti di tutto il censimento) avrebbe dovuto essere non più obbligatoria ma solo opzionale: come a dire che l’enorme spesa pubblica per il censimento (circa 600 milioni di euro) doveva andare sprecata.

Di fronte a tale prospettiva, l’Istat ha deciso di unire le due opzioni di risposta, creando una modalità unica, che verosimilmente sarà simile a “convivente in coppia con l’intestatario” (per qualche ragione le parole precise non sono ancora state rese pubbliche). Questa modalità, con la parola “convivente”, rimane ovviamente distinta da chi è solo “coabitante”, in quanto il censimento delle convivenze in coppia (cioè persone legate da vincoli affettivi) è un obbligo per l’Istat, derivante da regolamenti europei e dunque non aggirabile.

2) Le conseguenze.

Occorre rimarcare l’effetto paradossale e alquanto curioso della decisione del Garante per la privacy, che anche per le persone eterosessuali ha stabilito che dichiarare di vivere in coppia con una persona del sesso opposto (ovvero in una cosiddetta coppia di fatto) è un’informazione che rientra nella “vita sessuale” dell’individuo, mentre dichiarare di essere sposato no. Questa decisione rischia di avere pesanti conseguenze negative per la possibilità di fare ricerca su molti temi sociali, economici, demografici, perché enti e istituti di ricerca potrebbero non esser più autorizzati a raccogliere o analizzare molti dati importanti.

Allo stesso tempo, la decisione non ha alcuna conseguenza immediata sulla possibilità di considerare nel censimento le coppie dello stesso sesso. Infatti, oltre al rapporto con il capofamiglia, ad ogni individuo è richiesto anche il suo sesso. Quindi, senza alcuna difficoltà (anche grazie alle capacità di calcolo ormai resa disponibile dall’informatica) è sempre possibile presentare qualsiasi elaborazione separatamente per le coppie di persone di sesso diverso e per le coppie di persone dello stesso sesso: basta incrociare i risultati delle due diverse domande, quella sul sesso di ognuno e quella sulla sua relazione con il capofamiglia. La risposta a entrambe le domande, peraltro, rimane obbligatoria.

Questa vicenda ricorda ancora una volta l’importanza di diffondere una corretta informazione sulle modalità di compilazione del questionario del censimento, da parte dell’Istat, nonché da parte delle associazioni LGBT di sottolineare l’importanza per le coppie di persone dello stesso sesso di dichiararsi come tali, sia per vedersi riconosciute come forma famigliare sia per permettere finalmente l’acquisizione di informazioni statistiche, fondamentali per una ricerca e per una politica in favore della dignità e dei diritti di ognuno/a.

Carlo D’Ippoliti