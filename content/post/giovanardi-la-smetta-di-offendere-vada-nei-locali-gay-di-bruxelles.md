---
title: 'GIOVANARDI LA SMETTA DI OFFENDERE, VADA NEI LOCALI GAY DI BRUXELLES'
date: Tue, 16 Jun 2009 09:09:23 +0000
draft: false
tags: [Comunicati stampa]
---

**GIOVANARDI LA SMETTA DI OFFENDERE LE PERSONE GAY CHE PARTECIPANO AI GAY PRIDE E DI DISCRIMINARE LE PERSONE STERILI. COMINCI ANCHE LUI A FREQUENTARE I LOCALI GAY E INCONTRERA’ TANTI SUOI COLLEGHI DEL PPE.**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

Il Senatore Carlo Giovanardi ha perso un’altra occasione per starsene zitto. Sostenere che il Gay Pride offende il sentimento religioso di tanti italiani dimostra come egli sia distante dalla realtà e dai nuovi fenomeni sociali italiani. Considerare poi la centralità della riproduzione di coppia come strumento di contrapposizione alla lobby gay, oltre che ridicolo è pure offensivo per le decine di migliaia di coppie sterili che ci sono in Italia.

Le forze politiche di destra che si sono affermate in Europa, ad eccezione dell’Italia, sono quelle che hanno legiferato quasi ovunque in favore delle unioni civili. Invitiamo Giovanardi a frequentare i locali gay di Bruxelles e Strasburgo, troverà molti deputati europei del PPE, gruppo che secondo lui ‘arginerà la lobby dei gay in Europa’. Da quelle parti si fa tutto alla luce del sole, o dei neon, altro che qui in Italia dove, di nascosto, si pecca in qualche albergo di Via Veneto, e poi comunque si è assolti o rieletti in qualche istituzione. Siamo poi d’accordissimo sui test antidroga all’ingresso delle discoteche, purche’ vengano estesi a tutti, anche a coloro che fanno dichiarazioni come le sue alla stampa”