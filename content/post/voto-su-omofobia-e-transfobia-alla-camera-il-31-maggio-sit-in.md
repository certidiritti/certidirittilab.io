---
title: 'VOTO SU OMOFOBIA E TRANSFOBIA ALLA CAMERA IL 31 MAGGIO. SIT-IN!'
date: Mon, 23 May 2011 15:35:10 +0000
draft: false
tags: [Politica]
---

**Certi Diritti invita le associazioni e i cittadini che hanno a cuore la lotta per il superamento delle diseguaglianze a promuovere insieme un presidio (sit-in, maratona oratoria) in Piazza Montecitorio a partire dalle ore 11 di martedì 31 maggio 2011.**

**Roma 23 maggio 2011**

**Comunicato Stampa dell'Associazione Radicale Certi Diritti**

La Conferenza dei Capigruppo della Camera dei deputati ha deciso che il voto sulla pdl contro l'omofobia e transfobia (A.C. 2802) si terrà martedì 31 maggio. La seduta di martedì 31 maggio(con votazioni) è convocata alle ore 10,30 con al primo punto il voto sul Decreto Legge 'Voto dei cittadini temporaneamente all'estero' in occasione dei referendum del 12 e 13 giugno 2011. 

A seguire il si voterà il provvedimento omofobia-transfobia (previo esame e votazione delle questioni pregiudiziali e della questione sospensiva presentate).

Proponiamo a tutte le forze politiche, Associazioni e i cittadini che hanno a cuore la lotta per il superamento delle diseguaglianze a promuovere insieme un presidio (sit-in, maratona oratoria) in Piazza Montecitorio a partire dalle ore 11 di martedì 31 maggio 2011.

Per eventuali adesioni, proposte, suggerimenti:  [info@certidiritti.it](mailto:info@certidiritti.it)