---
title: 'Io m''impegno: appello di Certi Diritti a cittadini e candidati per un''Italia più libera e senza sessuofobia'
date: Tue, 12 Feb 2013 12:03:03 +0000
draft: false
tags: [Politica]
---

Sottoscrivi i sette punti su www.iomimpegno.org e contribuisci alle battaglie dell'associazione.

Comunicato stampa dell’Associazione Radicale Certi Diritti.

Roma, 12 febbraio 2013

In vista delle elezioni politiche 2013, l’Associazione Radicale Certi Diritti chiede a tutte le candidate e a tutti i candidati alle prossime elezioni politiche, a prescindere dal partito di appartenenza, di sottoscrivere una pledge (richiesta d’impegno) per promuovere l’uguaglianza e combattere la discriminazione basata sull’orientamento sessuale e l’identità di genere.

La richiesta d’impegno s’inscrive in una più vasta campagna volta ad accrescere il sostegno alle battaglie dell’Associazione Radicale Certi Diritti e si rivolge anche a tutti i cittadini che possono dare corpo a questi temi nel modo più concreto: iscrivendosi per il 2013 all’Associazione Radicale Certi Diritti.

La pledge può essere letta e sottoscritta al sito **[http://www.iomimpegno.org](http://www.iomimpegno.org/.)**  
Essa consta di sette punti:

1 Riforma del diritto di famiglia: matrimonio egualitario, omogenitorialità, unioni libere, intese di solidarietà, comunità intenzionali, divorzio breve e riforma della legge 40.  
  
2 Fare in modo che la vita sessuale di tutti non sia condizionata da pregiudizi.  
  
3 Combattere i crimini d'odio con una legge quadro che li prevenga.  
  
4 Liberare le lavoratrici e i lavoratori del sesso da una legge che toglie dignità ed espone alla criminalità.  
  
5 Intervenire affinché le persone trans e intersessuali possano decidere liberamente e consapevolmente del proprio futuro.  
  
6 Diventare protagonista nella difesa dei diritti delle persone LGBTI nel mondo.  
  
7 Dare priorità a iniziative per la prevenzione delle malattie trasmesse sessualmente.