---
title: 'Bondi favorevole ai diritti coppie gay: qualcosa si muove in questo paese'
date: Tue, 04 May 2010 09:59:00 +0000
draft: false
tags: [bondi, brunetta, certi diritti, Comunicati stampa, coppie di fatto, FINI, GAY, rotondi, rovasio]
---

**CONSERVATORI BRITANNICI PER I DIRITTI DELLE COPPIE GAY. LA VERA NOTIZIA PERO’ E’ UN’ALTRA: DOPO FINI, BRUNETTA E ROTONDI ANCHE BONDI SI DICE FAVOREVOLE AI DIRITTI ALLE COPPIE DI FATTO… QUALCOSA SI MUOVE IN QUESTO STRANO PAESE.  
  
Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

“Non ci siamo certo meravigliati quando ieri, il leader dei conservatori britannici, David Cameron, a tre giorni dal voto, si è dichiarato favorevole al riconoscimento dei diritti delle coppie gay. Ciò che ci meraviglia invece è quanto dichiarato dal Ministro dei Beni culturali, coordinatore del Pdl, Sandro Bondi che, in una intervista rilasciata ad ‘A’ si è detto favorevole al riconoscimento dei diritti delle coppie di fatto, anche di quelle gay”. Il fatto che anch’egli riconosca che “la Chiesa dovrebbe cambiare atteggiamento” è certo un fatto molto importante. Ciò dimostra, ove ve ne fosse ancora bisogno, che la questione del riconoscimento dei diritti deve diventare quanto prima iniziativa della classe politica così come anche sollecitato dalla Corte Costituzionale nelle motivazioni della sentenza sui matrimoni gay dello scorso 22 aprile. Ci auguriamo che il Ministro Bondi dia seguito con atti concreti alle sue idee visto anche l’autorevole ruolo che egli ricopre e soprattutto perché avrà certamente il sostegno dei Ministri Brunetta, Rotondi e del Presidente Fini”.