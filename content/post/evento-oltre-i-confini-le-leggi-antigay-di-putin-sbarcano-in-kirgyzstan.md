---
title: 'EVENTO - Oltre i confini: le leggi antigay di Putin sbarcano in Kirgyzstan'
date: Tue, 29 Jul 2014 15:52:15 +0000
draft: false
tags: [Asia]
---

[![KIRG](http://www.certidiritti.org/wp-content/uploads/2014/07/KIRG-300x111.png)](http://www.certidiritti.org/wp-content/uploads/2014/07/KIRG.png)Le leggi contro la cosiddetta "propaganda dell'omosessualità tra i minori" hanno minato la libertà di espressione e di riunione di tutta la popolazione russa; la legge sugli "agenti stranieri" continua a compromettere il lavoro di promozione e tutela dei diritti umani in Russia delle ONG locali. Come queste leggi stanno interferendo nelle istituzioni e nella società dei paesi limitrofi alla Russia? Ne parliamo sabato 2 agosto 2014 dalle 16:00 con: - **Dastan Kasmamytov**, attivista per i diritti LGBTI in Kirgyzstan; - **Antonio Stango**, esperto di Russia ed Est Europa  Modererà **Leonardo Monaco**, Tesoriere dell'Associazione Radicale Certi Diritti c/o la sede del Partito Radicale a Roma, via di Torre Argentina 76 (citofono "Partito Radicale", 3° piano) Preannuncia la tua partecipazione attraverso l'[evento Facebook](https://www.facebook.com/events/798695760162296/ "Oltre i confini: le leggi antigay di Putin sbarcano in Kirgyzstan").