---
title: 'IL TESTO DELLA PROPOSTA FRANCESE ONU SU DEPENALIZZAZIONE OMOSESSUALITA'''
date: Fri, 05 Dec 2008 09:39:06 +0000
draft: false
tags: [Comunicati stampa]
---

Di seguito il testo integrale della proposta francese su depenalizzazione omosessualità all'Onu

Testo della dichiarazione:

We have the honour to make this statement on human rights, sexual orientation and gender identity on behalf of \[.\]

1 - We reaffirm the principle of universality of human rights, as enshrined in the Universal Declaration of Human Rights whose 60th anniversary is celebrated this year, Article 1 of which proclaims that "all human beings are born free and equal in dignity and rights";

2 - We reaffirm that everyone is entitled to the enjoyment of human rights without distinction of any kind, such as race, colour, sex, language, religion, political or other opinion, national or social origin, property, birth or other status, as set out in Article 2 of the Universal Declaration of Human Rights and Article 2 of the International Covenants on Civil and Political, Economic, Social and Cultural Rights, as well as in article 26 of the International Covenant on Civil and Political Rights;

3 - We reaffirm the principle of non-discrimination which requires that human rights apply equally to every human being regardless of sexual orientation or gender identity;

4 - We are deeply concerned by violations of human rights and fundamental freedoms based on sexual orientation or gender identity;

5 - We are also disturbed that violence, harassment, discrimination, exclusion, stigmatisation and prejudice are directed against persons in all countries in the world because of sexual orientation or gender identity, and that these practices undermine the integrity and dignity of those subjected to these abuses;

6 - We condemn the human rights violations based on sexual orientation or gender identity wherever they occur, in particular the use of the death penalty on this ground, extrajudicial, summary or arbitrary executions, the practice of torture and other cruel, inhuman and degrading treatment or punishment, arbitrary arrest or detention and deprivation of economic, social and cultural rights, including the right to health;

7 - We recall the statement in 2006 before the HumanRights Council by fifty four countries requesting the Presidentof the Council to provide an opportunity, at an appropriate future session ofthe Council, for discussing theseviolations;

8 - We commend the attention paid to these issues by special procedures of the Human Rights Council and treaty bodies and encourage them to continue to integrate consideration of human rights violations based on sexual orientation or gender identity within their relevant mandates;

9 - We welcome the adoption of Resolution AG/RES. 2435 (XXXVIII-O/08) on "Human Rights, Sexual Orientation, and Gender Identity" by the General Assembly of the Organization of American States during its 38th session in 3 June 2008;

11 - We urge States to take all the necessary measures, in particular legislative or administrative, to ensure that sexual orientation or gender identity may under no circumstances be the basis for criminal penalties, in particular executions, arrests or detention.

12 - We urge States to ensure that human rights violations based on sexual orientation or gender identity are investigated and perpetrators held accountable and brought to justice;

13 - We urge States to ensure adequate protection of human rights defenders, and remove obstacles which prevent them from carrying out their work on issues of human rights and sexual orientation and gender identity.