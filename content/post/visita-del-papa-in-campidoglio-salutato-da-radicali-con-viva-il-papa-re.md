---
title: 'VISITA DEL PAPA IN CAMPIDOGLIO: SALUTATO DA RADICALI CON "VIVA IL PAPA RE"!'
date: Mon, 09 Mar 2009 12:11:44 +0000
draft: false
tags: [Comunicati stampa]
---

**VISITA PAPA IN CAMPIDOGLIO: L'ASSOCIAZIONE CERTI DIRITTI SALUTA IL PASSAGGO DEL PAPA AL GRIDO DI "VIVA IL PAPA RE!". L'AUTO DEL PAPA HA APERTO IL FINESTRINO PROPRIO QUANDO E' PASSATA DAVANTI AI MILITANTI RADICALI.  
POCHISSIMI TURISTI A SALUTARE IL PAPA. PERCHE E' STATA SCELTA VIA DEL PLEBISCITO PER IL PASSAGGIO DEL CORTEO?**

Roma, 9 marzo 2009  
  
Il Segretario dell'Associazione Radicale Certi Diritti Sergio Rovasio, insieme ad Antonio Stango,  membro del Comitato di Radicali Italiani e Mario Staderini membro della Direzione di Radicali Italiani con alcuni militanti radicali, stamane, in occasione della visita del Papa in Campidoglio, al passaggio del Papa in Piazza Venezia alle ore 10.53, hanno salutato il Papa Ratzintger al grido di "Viva il Papa Re" per rimarcare la totale genuflessione della classe politica al potere della teocrazia vaticana. Proprio in quel momento si è aperto il finestrino della papamobile per permettere al Papa di salutare alcuni turisti giapponesi che poco prima avevano comprato le bandierine del Vaticano a 1 Euro da un immigrato.  
Abbiamo quindi salutato il Papa al grido di "Viva il Papa Re" per coerenza con le bandiere vaticane, appese da due giorni su tutti i palazzi di Corso Vittorio Emanuele e Piazza Venezia, a dimostrazione che effettivamente lo Stato italiano è tornato ad essere lo Stato ponitificio. Viva il Papa Re!

  
Sergio Rovasio, Segretario dell'Associazione Radicale Certi Diritti ha dichiarato:  
"Siamo rimasti sorpresi dalla Piazza Venezia praticamente semi-deserta (nonostante le strade fossero aperte ai pedoni) al passaggio della papamobile, v'erano soltanto poco più di un centinaio di turisti incuriositi. Non comprendiamo perché il corteo sia stato fatto passare da Via del Plebiscito che è la via di Roma dedicata allo storico Plebiscito del 2 ottobre 1870 che sancì Roma capitale d'Italia contro lo strapotere clericale. Forse si è voluta così rimarcare la laicità dello Stato?".