---
title: 'CHI è DIVERSO ??'
date: Wed, 02 Dec 2009 05:46:23 +0000
draft: false
tags: [Senza categoria]
---

**“Pensieri di Velluto”**Propone**  
CHI E’ DIVERSO ??**

*   Serata di presentazione – 15 Gennaio 2010 ?
*   Mostra Omofobia – a partire dal 16 Gennaio 2010 ?
*   Manifesti – ( con simboli di chi appoggia l’evento ??)
*   Varie : Incontri culturali e presentazione libri a tema; attivazione di gruppi aperti sull’identità sessuale;

APPOGGIATI da:

*   ARCI VARESE
*   ARCI GAY ?
*   ARCI LESBICA ?
*   LINEA LESBICA AMICA ?
*   GRUPPO SOGGETTVITA’ LESBICA ?
*   COORDINAMENTO ASSOCIAZIONI SARONNO
*   CERTI DIRITTI MILANO
*   PITTORE (esecutore quadri della mostra)
*   DONNE PER CAMBIARE – Saronno
*   A.GE.DO

Alcuni Partiti politici:

*   Sinistra Saronnese
*   Socialisti Italiani
*   Altri gruppi da concordare del centro Sinistra

Per chi aderirà.. sarete contattati per eventuali integrazioni al progetto e per eventuali interventi pubblici che saranno molto graditi.

I manifesti che pubblicizzeranno l’evento –vedi allegato- sono forniti da ARCI Varese.

**![razzismo1](http://www.certidiritti.org/wp-content/uploads/2009/12/razzismo1.jpg)  
**

**“Pensieri di Velluto”   
Propone  
Gruppo identità Sessuale**

*   Creazione di un gruppo di autocoscienza sulle problematiche di identità  sessuale (gratuito.. ) ;
*   Creazione di uno spazio aperto ma intimo e tranquillo, dove si danno, si raccolgono informazioni; si accoglie ogni differenza senza alcun giudizio… ma complicità ….;
*   Uno spazio dove trovare spunti, si diffondono informazioni, si creano gruppi di autocoscienza…ma anche organizzazione di feste culturali e non… un nuovo polo di riferimento in provincia per congiungere Varese  a Milano;
*   Creazione di un laboratorio di Danze popolari e meditative su temi legati alla storia del femminile;

PER INFORMAZIONI :

TELEFONO : 02/96702478 - FAX: 02/96115737

MAIL: [pensieri@circoliarci.it](mailto:pensieri@circoliarci.it)