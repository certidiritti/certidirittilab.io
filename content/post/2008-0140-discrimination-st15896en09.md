---
title: '2008-0140-Discrimination-ST15896.EN09'
date: Thu, 12 Nov 2009 13:32:20 +0000
draft: false
tags: [Senza categoria]
---

  

  

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 12 November 2009

15896/09

LIMITE

SOC 686

JAI 829

MI 425

  

  

  

  

  

NOTE

from :

General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

15430/09 SOC 655 JAI 789 MI 385

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached a note from the Austrian delegation with a view to the meeting of the Social Questions Working Party on 13 November 2009.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

AT proposal

Changes in relation to doc. 15320/09 are indicated as follows: additions are in **bold** and deletions are marked **\[…\]**.

Article 2 (7)

Notwithstanding paragraph 2, in the provision of financial services, proportionate differences in treatment **\[…\]** **shall not be considered discrimination for the purposes of this Directive, if this treatment is** based on relevant actuarial principles, accurate statistical data and medical knowledge and **\[…\]** **if these are determining factors in the assessment of risk.**

Explanatory Statement

1.  The above wording would guarantee a certain coherence with Directive 2004/114/EC and consequently a higher level of legal certainty.
2.  Insurers execute an assessment of risk in every individual case by using actuarial principles, statistical data and medical knowledge. There is no need for an additional indicator like age and disability. If age and disability are mentioned in Article. 2(7), we fear that differences in treatment would be allowed on those grounds _only_. It is our opinion that Article 2(7) as suggested by the Presidency would lead to the erroneous assumption that persons with disabilities or persons of a given age were generally at higher risk than others. This would result in discrimination and we therefore cannot accept such text within the Directive; it would contradict the principle of equal treatment.

Examples: A blind person could be healthier than a sighted person. Certainly a person with disabilities could be in poorer health than a person without disabilities. But in these cases the above-mentioned criteria would remain adequate for assessing the risks.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_