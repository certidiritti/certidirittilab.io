---
title: 'Spiaggia dell''Arenauta: continuano intimidazioni. Diversi gruppi chiedono chiarimenti'
date: Thu, 12 Jul 2012 07:39:53 +0000
draft: false
tags: [Politica]
---

**A Gaeta continuano le ronde di vigilantes armati. Dopo l'interrogazione parlamentare depositata al Senato, i consiglieri regionali radicali, Pd, socialisti, Verdi, IDV, SEL E FDS ne presentano una alla regione Lazio.  
**

**Ad un bagnante che praticava naturismo è stato ordinato di togliere il pareo e indossare il costume.  
**

Roma, 12 luglio 2012

Su quanto sta avvenendo alla Spiaggia dell’Arenauta di Gaeta, dopo l’interrogazione urgente a risposta scritta presentata la scorsa settimana dai Senatori Radicali Marco Perduca e Donatella Poretti, ne è stata depositata un’altra anche al Consiglio Regionale del Lazio rivolta alla Presidente Renata Polverini e agli Assessori al Turismo, all’Ambiente, agli Enti Locali e al Demanio della Regione Lazio. L’interrogazione urgente è firmata dai Consiglieri Regionali,  Giuseppe Rossodivita, Rocco Berardo, Radicali, Enzo Foschi e Tonino D’annibale, Pd, Angelo Bonelli, Verdi, Luciano  Romanzi, Psi, Luigi Nieri, Sel, Ivano Peduzzi e Fabio Nobile, Fds e da Annamaria Tedeschi di Idv.

Nel testo dell’interrogazione è stata ricostruita la vicenda che vede la Spiaggia dell’Arenauta oggetto di incredibili vicende relative alla ipotetica mancata messa in sicurezza di una parte della falesia. Tale situazione si protrae da tre anni, sin da quando nel luglio 2010 è stato persino ‘ingabbiato’ un ampio tratto della spiaggia, con la preclusione di ampi tratti, anche laddove non esiste alcun tipo di rischio. In particolare la limitazione all’accesso veniva effettuata in un tratto di spiaggia dell’Arenauta solitamente frequentato da persone che praticano il naturismo, e addirittura del tutto preclusa all’area solitamente frequentata da persone della comunità Lgbt. 

Le stesse strutture messe in opera sull’arenile, in violazione di tutte le leggi ambientaliste, a causa di diverse mareggiate, sono tutte crollate, sparse tra il litorale e il mare con conseguenti pericoli per i bagnanti e per le imbarcazioni; altre strutture sono state installate anche nell’estate 2011 con lo stesso risultato.

Nell’interrogazione viene anche segnalato che nello stesso tratto di spiaggia recintato nel 2011, e non nel 2012,  sono apparsi in questi giorni alcuni vigilantes armati di una società privata che vanno vicino ai bagnanti che praticano il naturismo e dicono loro di indossare il costume da bagno. Proprio ieri, mercoledì 11 luglio è stato da loro imposto ad un bagnante  il divieto di indossare in alternativa il pareo, che non andava bene, ‘ordinandogli’ di indossare il costume da bagno.

Secondo quanto denunciato dal Comitato per la Difesa e la Tutela della Spiaggia dell’Arenauta, dall’Associazione Radicale Certi Diritti, dall’Unione Naturisti della Campania e dall’Associazione Gaeta Ventura, tali azioni hanno l’obiettivo di spaventare e intimidire i bagnanti che praticano da decenni il naturismo e le centinaia di persone lesbiche e gay che frequentano la spiaggia.

L’Associazione Gaeta Ventura ha presentato oltre due anni fa al Comune di Gaeta un progetto per mettere in sicurezza con strutture di rete, al costo di circa 20.000 Euro, una zona limitata della falesia di pochi metri della spiaggia  considerata pericolosa senza ricevere nessuna risposta da parte degli amministratori locali.

Nell’interrogazione si chiede di conoscere per quale motivo da due anni ad inizio stagione vengono installate strutture composte da pali e reti per limitare l’accesso ad un tratto della spiaggia e quali sono i costi che ogni anno vengono sostenuti, per quale motivo se vi sono tratti della falesia vicina alla spiaggia dell’Arenauta dove è necessaria la messa in sicurezza non si interviene con i fondi previsti e se non ritengano che tali strutture installate in modo precario arrechino pericoli per i bagnanti e i mezzi di navigazione da loro utilizzati. Si chiede inoltre di sapere cosa è cambiato riguardo il supposto pericolo di frane e crolli della falesia nell’ estesa area oggetto della recinzione nel 2010 non più recintata nel 2012. Infine si chiede di sapere se non ritengano urgente intervenire per preservare il turismo in quell’area della spiaggia che per la sua peculiarità attira decine di migliaia di turisti italiani e stranieri ogni stagione estiva.

Il testo integrale dell’Interrogazione

**INTERROGAZIONE URGENTE  A RISPOSTA SCRITTA**

**Oggetto: Spiaggia dell’Arenuta (Gaeta – Lt), mancata messa in sicurezza falesia e turismo**

Per sapere – premesso che:

nel luglio 2010 un ampio tratto verso sud della Spiaggia dell’Arenauta (Gaeta – Lt) veniva letteralmente ‘ingabbiato’ da strutture composte da pali e reti che ne precludeva la fruizione a decine di migliaia di bagnanti;

secondo alcune informazioni tale preclusione all’accesso della spiaggia si rendeva necessario a causa del rischio di caduta massi, frane e crolli, mai verificatisi nel corso degli ultimi decenni anche secondo le dirette testimonianze di persone che frequentano la spiaggia da decenni;

in particolare la limitazione all’accesso veniva effettuata in un tratto di spiaggia dell’Arenauta solitamente frequentato da persone che praticano il naturismo, e addirittura del tutto preclusa all’area solitamente frequentata da persone della comunità Lgbt;

dopo alcune settimane, a causa di diverse mareggiate, le strutture installate erano quasi del tutto crollate, sparse tra il litorale e il mare con conseguenti pericoli per i bagnanti e per le imbarcazioni; altre strutture sono state installate anche nell’estate 2011 con lo stesso risultato;

alla fine di giugno 2012 in un tratto molto più limitato della spiaggia di pochi metri, sono state installate alcune strutture per limitare l’accesso dei bagnanti  in prossimità di una falesia inattiva, distante pochi metri dal mare;

nel tratto di spiaggia recintato nel 2011 e non nel 2012  sono apparsi in questi giorni alcuni vigilantes armati di una società privata che vanno vicino ai bagnanti che praticano il naturismo e dicono loro di indossare il costume da bagno;

l’Associazione Gaeta Ventura ha presentato oltre due anni fa al Comune di Gaeta un progetto per mettere in sicurezza con strutture di rete, al costo di circa 20.000 Euro, una zona di falesia che viene considerata pericolosa senza ricevere nessuna risposta da parte degli amministratori locali;

secondo alcune informazioni raccolte in loco la Regione Lazio non avrebbe i fondi per mettere in sicurezza una parte della falesia della spiaggia dell’Arenauta e questo spiegherebbe il motivo per il quale ad inizio della stagione estiva vengono installati pali e reti per limitare l’accesso alla spiaggia;

l’area di spiaggia interessata è l’unico tratto che non è stato fino ad oggi oggetto di speculazioni e/o colate di cemento ‘fin dentro il mare’ come invece è  avvenuto in parte della costa di Gaeta e dei Comuni limitrofi;

il tratto di Spiaggia dell’Arenauta è frequentato da decine di migliaia di turisti provenienti anche dall’estero  perché considerta un’area prediletta per  la pratica del ‘free climbing’ o dell’arrampicata sportiva, per la pratica del naturismo e luogo prediletto dalla comunità Lgbt, per queste ragioni  è segnalata in molte guide internazionali;

**i sottoscritti consiglieri regionali**

**INTERROGANO**

**la Presidente della Giunta, l’assessore ai Rapporti con gli Enti Locali e Politiche per la sicurezza, Assessore al Turismo e Marketing del ‘Made in Lazio’, Assessore all’Ambiente e Sviluppo sostenibile; Assessore alla Cultura, Arte e Sport, Assessore alle infrastrutture e Lavori Pubblici; Assessore alle Risorse Umane, Demanio e Patrimonio; **

per sapere:

per quale motivo da due anni ad inizio stagione vengono installate strutture composte da pali e reti per limitare l’accesso ad un tratto della spiaggia e quali sono i costi che ogni anno vengono sostenuti;

per quale motivo se vi sono tratti della falesia vicina alla spiaggia dell’Arenauta dove è necessaria la messa in sicurezza non si interviene con i fondi previsti;

se non ritengano che tali strutture installate necessariamente in modo precario arrechino pericoli per i bagnanti e i mezzi di navigazione da loro utilizzati e cosa è cambiato riguardo il supposto pericolo di frane e crolli della falesia nell’ estesa area oggetto della recinzione nel 2010 non più recintata nel 2012;

se corrisponde al vero la notizia che la mancata messa in sicurezza di una piccola parte della falesia è dovuta al fatto che non ci sono i fondi regionali necessari pur esistendo voci nel bilancio preventivo approvato dal Consiglio Regionale del Lazio e se non ritengano urgente stabilire un contatto con l’Associazione Gaeta Ventura per verificare la fattibilità del progetto da loro presentato al Comune di Gaeta oltre due anni fa ad un costo molto limitato;

se non ritengano urgente intervenire per preservare il turismo in quell’area della spiaggia che per la sua peculiarità attira decine di migliaia di turisti italiani e stranieri ogni stagione estiva;

I consiglieri regionali Radicali:

Giuseppe Rossodivita e  Rocco Berardo, Lista Bonino Pannella

Luciano Romanzi, Psi

Angelo Bonelli, Verdi

Luigi Nieri, Sel

Ivano Peduzzi, Fds

Enzo Foschi, Pd  

Tonino D’annibale, Pd

Fabio Nobile, Fds

Annamaria Tedeschi, Idv