---
title: 'Incontro con l''Oscad dell''Associazione Radicale Certi Diritti riguardo le disposizioni discriminatorie di Ministeri Interni e Difesa'
date: Wed, 11 Jul 2012 06:40:42 +0000
draft: false
tags: [Politica]
---

Il Prefetto Cirillo e una delegazione dell'Oscad incontra i rappresentanti dell'associazione. Confermata la richiesta di modifica della circolare discriminatoria del Ministero degli Interni verso le persone lgbt e del Ministero della Difesa su persone con hiv.

Roma, 10 luglio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Stamane presso il Dipartimento della Pubblica sicurezza una delegazione dell'OSCAD (Osservatorio per la Sicurezza contro gli atti Discriminatori) composta dal Prefetto Cirillo (Presidente e vice-Direttore generale del Dipartimento della Pubblica Sicurezza), dal questore Calabria e dai dottori Chirico e Iannuzzi,  ha incontrato i rappresentanti dell'Associazione radicale Certi Diritti, il senatore Radicale eletto nel Pd Marco Perduca, membro della Direzione ed Enzo Cucco Presidente.

L'incontro nasce da un esposto presentato recentemente  dall'Associazione all'OSCAD ed all'UNAR in merito ad [una circolare del Dipartimento della Pubblica sicurezza riguardo i criteri per i trasferimenti del personale di polizia](http://www.certidiritti.org/2012/05/22/circolare-discriminatoria-del-ministro-degli-interni-sui-trasferimenti-del-personale-di-polizia-eslcuse-coppie-dello-stesso-sesso-presentato-esposto/). In questa circolare è prevista una condizione di favore per i poliziotti che hanno famiglia, e che sono conviventi, a condizione che la convivenza sia tra persone di sesso opposto. Il testo rappresenta, quindi, una esplicita discriminazione nei confronti delle persone omosessuali. Sia secondo la normativa nazionale che secondo la normativa europea.

Durante l'incontro è stato brevemente esaminato un altro esposto, presentato sempre dall'Associazione Radicale Certi Diritti, in merito alla [denuncia della LILA sull'esclusione delle persone sieropositive dai bandi del Ministero della Difesa, anche per entrare nella banda musicale dell'Arma dei Carabinieri](http://www.certidiritti.org/2012/07/06/denuncia-della-lila-ministero-della-difesa-chiede-test-hiv-per-suonare-nella-banda-o-per-accedere-alle-scuole-dellesercito-esposto-allunar-e-alloscad/).

Anche in questo caso i rappresentanti dell'Associazione hanno esposto le ragioni per cui questa disposizione ministeriale è in contrasto con la normativa vigente e con la realtà delle persone con Hiv/Aids che, a differenza del passato, hanno una qualità di vita e di autonomia assolutamente compatibile con le professioni connesse all'esercito.

L'incontro è durato circa un'ora e mezza ed ha consentito alle parti di entrare nel merito delle questioni esposte. L'Associazione radicale Certi Diritti esprime il suo ringraziamento e la considerazione per l'attenzione e l'impegno dimostrato dal Prefetto Cirillo e da tutta la delegazione dell'OSCAD che ha colto le ragioni dei due esposti ed ha assicurato il proprio impegno per giungere, in tempi brevi, ad una soluzione dei problemi posti.