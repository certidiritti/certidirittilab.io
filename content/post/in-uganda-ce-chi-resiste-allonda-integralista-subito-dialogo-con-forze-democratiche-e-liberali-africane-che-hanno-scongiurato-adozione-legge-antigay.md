---
title: 'In Uganda c''è chi resiste all''onda integralista. Subito dialogo con forze democratiche e liberali africane che hanno scongiurato adozione legge antigay'
date: Sat, 15 Dec 2012 07:43:50 +0000
draft: false
tags: [Africa]
---

Nuovo appello al Parlamento, al governo italiano e all'Unione Europea.

Comunicato stampa Associazione radicale Certi Diritti

Roma, 15 dicembre 2012

In queste ultime due settimane l'Associazione Radicale Certi Diritti ha sviluppato una serie di contatti e incontri con esponenti politici e del Parlamento ugandese e con diplomatici italiani coinvolti nell'area, per monitorare l'andamento del dibattito parlamentare sulla proposta di legge che inasprisce pene contro le persone omosessuali e transessuali fino a introdurre la pena di morte.  
  
Nel fitto giro di contatti e colloqui avuti con politici di primo piano del Parlamento ugandese e del corpo diplomatico accreditato in Uganda ed in Africa, svolti in particolare dal Senatore Marco Perduca, membro della Commissione Diritti umani del Senato, dall'Onorevole Elisabetta Zamparutti e Sergio D'Elia, dell'AssociaziOne Nessuno Tocchi Caino, dal segretario nazionale Yuri Guaiana e da Sergio Rovasio, Consigliere Generale del Partito Radicale Nonviolento, abbiamo preso consapevolezza di più di **un riscontro positivo in merito alla fondatezza della posizione del Premier ugandese quando recentemente, davanti ad alcuni diplomatici europei, ha affermato di essere contrario alla eventuale approvazione della legge antiomosessuale**.  
  
Il progetto di legge denominato  **‘Kill the Gays bill’** per il momento giace in Commissione. **Questo non significa che la situazione sia risolta e che tutti i problemi siano stati appianati**. Significa soltanto che bisogna conoscere più a fondo la realtà ugandese, e africana e avviare quanto prima politiche di dialogo con coloro che fino ad oggi hanno dimostrato ragionevolezza e attenzione ai diritti civili e umani anche della comunità Lgbt(e). Comprendere meglio la posta in gioco ed evitare di dare nuovi argomenti alle forze integraliste e oscurantiste di quel paese che, tra l'altro, sostengono che l'impegno internazionale contro la legge omofoba sia una delle tante ingerenze occidentali e neocolonialiste in atto, alimentando così la demagogia e l'odio omofobo.  
  
L'impegno ed il sostegno internazionale non devono mancare, anzi si devono moltiplicare e rafforzare per evitare che il prossimo Parlamento prenda in esame ed approvi la proposta criminale. Ma si devono evitare toni di scontro,  valorizzando e sostenendo quelle forze politiche che si oppongono all'oscurantismo montante e sappiano interpretare, alla luce della cultura e della storia africana, i diritti fondamentali e il rispetto della dignitià di tutti e di tutte.