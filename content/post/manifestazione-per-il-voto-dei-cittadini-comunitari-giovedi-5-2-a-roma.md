---
title: 'MANIFESTAZIONE PER IL VOTO DEI CITTADINI COMUNITARI, GIOVEDI'' 5-2 A ROMA'
date: Tue, 03 Feb 2009 15:23:19 +0000
draft: false
tags: [Comunicati stampa]
---

Certi Diritti partecipa all'iniziativa:

CITTADINANZA E' SICUREZZA

GIOVEDI' 5 FEBBRAIO

ORE 16 - VIALE MAZZINI 14

MANIFESTAZIONE DI FRONTE ALLA RAI

\- solidarietà a tutte le donne vittime di violenza

\- no alla demonizzazione di intere comunità

\- per il rispetto del diritto di voto e di informazione dei cittadini europei

Non tutti lo sanno – e i primi a non saperlo sono proprio i diretti interessati – ma i cittadini comunitari residenti in Italia hanno diritto di voto sia alle elezioni europee che alle elezioni amministrative. Per dare una dimensione del fenomeno, i cittadini dell'Unione europea, provenienti dagli altri 26 paesi membri, che risiedono in Italia, sono 934.435. La comunità più presente è quella dei cittadini rumeni, ben 625.278; seguono i polacchi 90.218, i tedeschi 40.163, i Bulgari 33.477, i francesi 30.803, i britannici 26.448, gli spagnoli 17.354.

In vista delle prossime elezioni previste in giugno, le Istituzioni preposte devono mettere in atto quegli strumenti che consentano concretamente a tutti i cittadini europei residenti in Italia di poter votare. Il termine ultimo per l'iscrizione alle liste elettorali è il prossimo 9 marzo. Per questo manifesteremo di fronte alla Rai, per un'informazione corretta sull'esercizio del diritto di voto dei cittadini comunitari alle prossime elezioni europee e per il rispetto dei diritti della persona contro gli stereotipi razzisti.

La violenza si sconfigge con la cittadinanza.

Con una comune patria europea!

Saranno presenti i dirigenti e i parlamentari radicali e le comunità di cittadini europei provenienti dai 26 paesi dell'Unione.

http://it.youtube.com/watch?v=DxJGBofKM38