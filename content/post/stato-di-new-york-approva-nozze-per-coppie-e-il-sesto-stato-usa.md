---
title: 'Stato di New York approva nozze per coppie, è il sesto stato Usa'
date: Sat, 25 Jun 2011 11:48:28 +0000
draft: false
tags: [Americhe]
---

** Il cammino per il superamento delle diseguaglianze va avanti. Gerarchie cattoliche sconfitte dalla democrazia. Ringraziamo il coraggioso senatore repubblicano Mark Grisanti.**

**Dichiarazione di Mario Staderini, Segretario di Radicali Italiani e Sergio Rovasio, Segretario dell' Associazione Radicale Certi Diritti**

 Roma, 25 giugno 2011

  

“Eravamo certi che lo Stato di New York avrebbe approvato la legge che consente alla coppie gay di accedere all’istituto del matrimonio, divenendo così il sesto Stato Usa, dopo Connecticut, Iowa, Massachusetts, New Hampshire, Vermont e  District of Columbia.  
Alla Cristopher Street del Village, davanti al locale ‘Stonewall’, a Manatthan, città di  New York, ci fu il 28 giugno 1969 una rivolta della comunità gay contro i soprusi della polizia, che viene oggi ancora celebrata in tutto il mondo dalla comunità Lgbt. Non è certo casuale se il Senato dello Stato  ha voluto approvare il provvedimento proprio oggi,  25 giugno, il miglior modo per celebrare la data mondiale dell’orgoglio Lgbt.  
Il Governatore cattolico Andrew Cuomo, grande sostenitore della legge pro-gay,  non sarà scomunicato dalla chiesa cattolica -che su questa vicenda, oggi, ha fatto giusto qualche dichiarazione esprimendo il suo ‘disappunto’ e ‘delusione’ e nulla di più-; e non è certo un caso se non hanno convocato nessun ‘family day’, non hanno attivato le parrocchie per mandare pullman di fedeli e bambini davanti alla sede del Senato, non sono intervenuti con  fondi avuti dall’8Xmille (che esiste solo in Italia), né tanto meno nessun cardinale o monsignore ha osato paragonare l’omosessualità alla violenza sui minori, nessun parlamentare clerical-fondamentalista ha osato dire che il provvedimento viola la carta costituzionale.  
L’Italia è sempre più lontana dalla civiltà e dal progresso. I nostri politici imparino da quei quattro Senatori conservatori Repubblicani dello Stato di New York che hanno votato si al provvedimento e da quanto dichiarato dal Senatore Mark J. Grisanti, repubblicano conservatore di Buffalo che era stato eletto promettendo di opporsi alle nozze gay: alla fine ha votato ‘Sì’  dopo una lunghissima personale riflessione e ha avuto il coraggio di dichiarare: "Chiedo scusa a chi si sente offeso ma non posso negare a una persona, a un essere umano, a un contribuente, a un lavoratore, alla gente del mio Stato, di avere gli stessi diritti che io ho con mia moglie".  
Grazie al Senatore Mark J. Grisanti per il suo coraggio e per la sua umiltà. Lui non si deve certo scusare con nessuno, a differenza di molti politici italiani”.