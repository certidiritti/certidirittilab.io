---
title: 'Alemanno e Polverini non conoscono le competenze delle isitituzioni che governano. A San Valentino la miglior festa è l''avvio della firme per unioni civili'
date: Tue, 14 Feb 2012 13:14:08 +0000
draft: false
tags: [Diritto di Famiglia]
---

San Valentino: la festa per noi è l'inaugurazione della campagna per il riconoscimento delle unioni civili a Roma. A breve partirà una raccolta firme per la delibera di iniziativa popolare sulle unioni civili.

Roma, 14 febbraio 2012

Dichiarazione di Riccardo Magi, Segretario Radicali Roma e Sergio Rovasio, Associazione Radicale Certi Diritti – Roma  
   
“Riteniamo che il miglior modo per festeggiare San Valentino è il lancio della campagna di raccolta firme per una delibera di iniziativa popolare per il riconoscimento delle unioni civili che in questi giorni stiamo promuovendo, con decine di associazioni, nella città di Roma.

Questa è certamente la miglior risposta da dare al Sindaco di Roma che, non sappiamo se per ignoranza o semplice genuflessione al potere clericale, aveva dichiarato che su questa materia il Comune non ha alcuna competenza. In realtà i Comuni, così come hanno già fatto quelli di Torino e Napoli, con il riconoscimento delle coppie conviventi in base alla legge sulla famiglia anagrafica del 1989, hanno molte competenze riguardo l’assistenza, l’aiuto, e l’accesso agli stessi servizi previsti per le coppie sposate.  
E’ giunto il momento che Roma dimostri di non essere la Capitale dello stato vaticano ma piuttosto la Capitale di uno Stato laico e liberale.

Vorremmo infine ricordare alla Presidente della Regione Lazio Renata Polverini, che ieri a Napoli, folgorata sulla via di Alemanno, aveva dichiarato che anche la Regione non ha competenze su questi temi, che in ambito regionale ci sono un’infinità di interventi in favore delle famiglie, basta smetterla di far finta che molte di queste non esistano. La govenatrice del Lazio "ci è o ci fa". Non  conoscere le competenze della Regione sui temi che riguardano i diritti delle coppie conviventi, al pari di quelle sposate, e contro ogni forma di discriminazione, è un fatto molto grave”.