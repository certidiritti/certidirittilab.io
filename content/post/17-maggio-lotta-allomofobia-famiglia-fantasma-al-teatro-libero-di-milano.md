---
title: '17 MAGGIO LOTTA ALL''OMOFOBIA, FAMIGLIA FANTASMA AL TEATRO LIBERO DI MILANO'
date: Tue, 13 May 2008 20:29:50 +0000
draft: false
tags: [17 Maggio 2008, Amore Civile, certi diritti, Comunicati stampa, Giornata mondiale contro l'omofobia, la Famiglia Fantasma, riforma del diritto di famiglia]
---

Per la giornata mondiale contro l'omofobia, sabato 17 Maggio alle ore 19 al teatro libero di Milano, si terrà la presentazione del libro "La Famiglia Fantasma" sul diritto al matrimonio per le persone omosessuali. [Via Savona, 10 zona Porta Genova...](http://maps.google.it/maps?f=l&hl=it&geocode=&q=teatro+libero+milano&near=milano&ie=UTF8&ll=45.455856,9.168863&spn=0.006502,0.019548&z=16) Il 17 Maggio sarà la giornata conclusiva di una rassegna di teatro omosessuale - altrimenti detta sulle diversità - solitamente preceduta da una presentazione culturale (libri, radio ecc...) alle ore 19:00.

La presentazione del libro sarà l'occassione anche per parlare delle nuove proposte di legge elaborate dal convegno ["Amore Civile"](index.php?option=com_content&task=view&id=48&Itemid=56) e che a breve saranno depositate in parlamento per una riforma del diritto di famiglia.

Ovviamente, si parlerà anche dell'iniziativa di ["Affermazione Civile"](index.php?option=com_content&task=view&id=41&Itemid=72) che l'associazione "Certi Diritti" sta organizzando: coordinare il maggior numero possibile di richieste di pubblicazione degli atti da parte di coppie omosessuali presso i comuni di residenza. Il rifiuto che ne conseguirà sarà impugnato con l'aiuto di una rete di avvocati (Rete Lenford).

Al termine della presentazione per chi vuole si può proseguire con una chiacchierata in una pizzeria in zona navigli.

L'aapuntamento per tutti, dunque, è alle 19.

[![famiglia_fantasma_-_solo_copertina.jpg](http://www.certidiritti.org/wp-content/uploads/2008/05/famiglia_fantasma_-_solo_copertina.jpg "famiglia_fantasma_-_solo_copertina.jpg")](http://famigliafantasma.freewordpress.it/info/)