---
title: '2008-140-Discrimination-ST12981-RE01.EN08'
date: Fri, 26 Sep 2008 15:21:25 +0000
draft: false
tags: [Senza categoria]
---

  

  

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 26 September 2008

12981/1/08

REV 1

SOC 495

JAI 454

MI 308

  

  

  

  

  

REPORT

from :

The Presidency

to :

Council (EPSCO)

No. prev. doc. :

12956/08 SOC 494 JAI 451 MI 306

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

-    Policy debate

(Public deliberation)

**I.       INTRODUCTION**

On 2 July 2008, the Commission adopted a proposal for a Council Directive aiming to extend the protection against discrimination on the grounds of religion or belief, disability, age or sexual orientation to areas outside employment. Complementing existing EC legislation[\[1\]](#_ftn1) in this area, the proposed Directive would prohibit discrimination on the above-mentioned grounds in the following areas: social protection, including social security and healthcare; social advantages; education; and access to goods and services, including housing.

  

The Working Party on Social Questions examined the proposal on 18 July, 10 September and 15 September 2008. Most delegations have affirmed the importance of promoting equal treatment as a shared social value within the EU and have consequently welcomed the objectives of the proposal. However, these early discussions have also revealed divergent views, particularly in regard to questions of competence and legal certainty, one delegation especially questioning the need for further Community legislation in this area at this stage.

For the time being, all delegations have maintained general scrutiny reservations on the proposal. DK, FR, MT and UK have maintained parliamentary scrutiny reservations. CY and PL have maintained linguistic scrutiny reservations.

**II.      STATE OF PLAY**

The Social Questions Working Party has identified a number of interlinked issues that require further discussion, which are summarised below.

**1.** **Division of Competence, Legal Base and Subsidiarity**

A large number of delegations have underlined the importance of defining the division of competence between the Member States and the Community as clearly as possible (see also point 3. below). In particular, clarification has been requested in regard to the material scope set out in Article 3, which includes social protection, social advantages, education, and access to and supply of goods and services. Certain delegations have expressed the view that aspects of the proposal belong under Member States' competence. In this context, clarification has also been requested in regard to the legal base (Article 13 of the EC Treaty) and the related jurisprudence, particularly as concerns "the principle of equal treatment".

  

Affirming the legal base chosen, the Commission representative has recalled that several Directives have already been adopted on the basis of Community competence under Article 13, and emphasised that the principle of equal treatment is well established as a fundamental principle of EC law.

The Council Legal Service has undertaken to provide a written opinion[\[2\]](#_ftn2) concerning the proposed legal base and the Community's competence in the areas of education (Article 149 of the EC Treaty) and social regulation (Article 137 of the EC Treaty).

The discussions have also shown that, in certain areas, policies are established through national practice rather than legislation, and that this custom may be threatened by the introduction of EC legislation.

**2.** **Relationship with Existing Directives**

A large number of delegations have urged the need to clarify the relationship between the proposed Directive and existing Directives (in particular, Council Directives 2000/43/EC, 2000/78/EC and 2004/113/EC).

Certain delegations have also expressed the concern that the ongoing infringement proceedings related to the implementation of existing anti-discrimination Directives also have implications for the discussion on the current proposal. In this context, the Commission representative has provided information regarding the proceedings launched against certain Member States in connection with the implementation of Council Directives 2000/43/EC and 2000/78/EC, underlining the fact that all the Member States support equal treatment in principle and that many of the problems encountered have been technical in nature.

**3.** **Legal Certainty**

A large number of delegations have underlined the importance of legal certainty and of avoiding further cases having to be brought before the European Court of Justice (ECJ). Delegations have consequently suggested providing _definitions_ of certain terms and have underlined the importance of ensuring _consistency with existing legislation_. In particular, delegations have called for clarification of the terms used in defining _the scope_, including the idea of social advantages, the division between the public and private sectors and the notion of a professional or commercial activity (Article 3), as well as in regard to the terms used in the provisions on _disability_, including the notion of "reasonable accommodation" (Articles 2 and 4).

**4.           Proportionality and Impact Assessment**

A large number of delegations have requested clarification in regard to the justification for and envisaged impact of certain provisions. Several delegations have expressed concern in regard to the potential financial and administrative burden imposed by the provisions, particularly for SMEs and the self-employed. Certain delegations have questioned the timeliness and even the need for new legislation in this field, underlining the importance of respecting the principles of proportionality and subsidiarity.

Stressing that a Directive is needed with a view to establishing a level playing-field for businesses and ending the hierarchy between different discrimination grounds, the Commission representative has undertaken to present the impact assessment[\[3\]](#_ftn3) accompanying the proposal in greater detail.

**5.** **Disability**

Delegations have underlined the significance of the proposal in the context of the UN Convention on the Rights of Persons with Disabilities, which has been signed by all the Member States and is awaiting ratification[\[4\]](#_ftn4). Some delegations would have preferred more ambitious provisions on disability. On the other hand, the concerns expressed in regard to the lack of legal certainty and the potential cost of the provisions (see above) are closely related to the provisions on disability, and many delegations have requested that certain provisions be clarified, including the requirement to provide "effective non‑discriminatory access" by anticipation and the notion of "reasonable accommodation", particularly as regards housing and transport.

**6.           Specific Issues**

A large number of questions have also been raised in relation to specific issues that will require further discussion. These include the following:

-   gender mainstreaming

-   the provisions on family law and reproductive rights

-   national legislation ensuring the secular nature of the state

-   the length of the implementation period

-   the provisions on financial services.

Further details are set out in doc. 12956/08[\[5\]](#_ftn5).

  

**III.    QUESTIONS FOR THE MINISTERS' DEBATE**

The proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation is being discussed in the context of the Renewed Social Agenda adopted by the Commission on 2 July 2008. It is based on Article 13 of the Treaty. It also echoes the Charter of Fundamental Rights (Article 81) and a number of international commitments.

This proposal illustrates the importance of fighting discrimination as one of the priorities of that social agenda. It raises considerable expectations among the public, both at European level and at national level.

The Treaty legal basis underlying the proposal was introduced in 1997 by the Treaty of Amsterdam, came into force on 1 May 1999, and led to the adoption of several Directives covering all of the grounds of discrimination listed in Article 13 of the EC Treaty. The purpose of this new proposal is to extend the protection provided by those Directives against discrimination on four grounds: belief or religion, age, sexual orientation or disability, thus carrying forward the work begun ten years ago.

Combating discrimination is also a major topical issue in most of the Member States. Often at the heart of the national social policy debate, it is evidence of a profound aspiration for respect and reinforcement of the legal protection for victims of discrimination.

The transposition of the existing Directives has created a solid framework for the national legislator, offering a springboard for the extension and modernisation of such legal protection, whatever the grounds for discrimination. National legislation has also developed in accordance with each Member State's own political considerations and legal traditions.

  

Directive 2000/43/EC implementing the principle of equal treatment between persons irrespective of racial or ethnic origin, which covers not only the area of employment but also access to goods and services, education and social protection, is often cited as an example of legislation based on Article 13. With hindsight and in the light of experience of implementation, Member States can measure the effect of the establishment of minimum Community legal protection in the campaign against discrimination.

**On the basis of these general considerations, the Presidency therefore proposes, in particular, that work on the proposal for a Directive should proceed:**

-                 **with the same high ambition evident in the Treaty itself,**

-                 **using the experience in implementing the Directives already adopted to ensure security of rights,**

-                 **with an eye to a good match between national legislation and Community legislation, particularly in areas such as education and health which lie within the competence of the Member States.**

**Question 1(a)**

**_Do you share this general approach to the work ahead of us?_**

**Question 1(b)**

**_Is a broad scope covering the various grounds the option to be favoured if we are to maintain the level of ambition?_**

**Question 2**

**_How far should the principle of equal treatment set at Community level extend, particularly in areas in which subsidiarity is a consideration?_**

Article 4 relates specifically to equal treatment of persons with disabilities. It sets out the principle of effective access and reasonable accommodation. Moreover, the United Nations Convention on the Rights of Persons with Disabilities is in the process of being ratified.

**Question 3(a)**

**_Do you share the approach in the proposal to equal treatment of persons with disabilities (Article 4)?_**

**Question 3(b)**

**_Do you consider that the rights and obligations in the proposal fit properly into the framework of the United Nations_** **_Convention on the Rights of Persons with Disabilities?_**

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) In particular, Council Directives 2000/43/EC, 2000/78/EC and 2004/113/EC.

[\[2\]](#_ftnref2) To be distributed in due course.

[\[3\]](#_ftnref3) See doc. 11531/08 ADD 1; a summary appears in doc. 11531/08 ADD 2.

[\[4\]](#_ftnref4) See docs 12892/08 REV 1 and 12892/08 ADD 1 REV 1.

[\[5\]](#_ftnref5) To be distributed in due course.