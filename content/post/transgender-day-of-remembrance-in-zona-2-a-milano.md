---
title: 'Transgender Day of Remembrance in Zona 2 a Milano'
date: Sun, 23 Oct 2011 21:36:10 +0000
draft: false
tags: [Transessualità]
---

Dichiarazione di Yuri Guaiana, Gruppo Radicale - Federalista europeo, Vicepresidente Consiglio di Zona 2 a Milano e membro del direttivo di Certi Diritti.

Mentre il PdL affida la Commissione Pari Opportunità della Provincia di Salerno alla transessuale Michel Martina Castellana che ieri ha ottenuto il cambio del nome sul proprio documento d'identità, Libero e Il Giornale di Milano pubblicano due sciatti articoli di critica all'iniziativa della Zona 2 in occasione del Transgender Day of Rememberance (20 novembre) che, per chi avesse problemi con idiomi non padani, commemora la giornata mondiale in ricordo delle vittime dell'odio e del pregiudizio contro le persone transessuali e non l'orgoglio trans che invece si celebra insieme a quello lesbico e gay il 28 giugno.

I due articoli sono profondamente transfobici e giustificherebbero da soli l'iniziativa promossa dal Gruppo Radicale - Federalista Europeo. In entrambi i pezzi infatti si usano ancora solo gli articoli al maschile per riferirsi sia alle persone in transito dal femminile al maschile sia le persone in transito dal maschile al femminile, mostrando la più totale mancanza di rispetto per il genere della persona transessuale che è quello di destinazione del loro percorso, l'unico col quale deve concordare l'articolo in un italiano corretto e rispettoso delle singole individualità.

L'iniziativa di Zona 2 si rivolge a tutta la cittadinanza proprio per combattere l'ignoranza e il pregiudizio che stanno alla base di quella transfobia che è dilagata in 18 anni di totale indifferenza da parte dell'amministrazione di centro-destra e che ha portato a ripetute aggressioni a danno di cittadine milanesi transessuali, l'ultima in ordine di tempo l'11 luglio scorso, e l'Italia a detenere il triste primato al modo di maggior numero di vittime di transfobia, vittime sacrificate da un pregiudizio insensato.

Il consiglio di Zona 2 rivendica quindi il merito di aver preso autonomamente l'iniziativa facendosi così carico di una delle problematiche di una zona di Milano in cui molte persone transessuali vivono e sono costrette a esercitare l'unica professione a loro accessibile: la prostituzione. L'obiettivo primario è infatti quello d'innalzare il livello di conoscenza sulla transessualità, svincolandola possibilmente dalla prostituzione, dove è perennemente confinata.

Si precisa infine che l'iniziativa non prevede solo una mostra sulla rappresentazione storica, sociale e culturale del transessualismo ma anche una serata con la partecipazione di vari referenti istituzionali per iniziare a interrogarsi su come le politiche sociali possano contribuire al fiorire di una cultura della diversità e la proiezione di un video in cui cinque transessuali milanesi hanno voluto provare a rappresentare la loro realtà da un punto di vista inconsueto e molto umano facendo emergere uno spaccato esistenziale lontano dai concetti di trasgressione, prostituzione e spettacolarizzazione che solitamente i media accompagnano al concetto di transessualità.

Forse i membri del gruppo consiliare della Lega Nord dovrebbero andare a lezione dai loro colleghi di maggioranza a Salerno prima di assumere certe posizioni.

www.radicalisenzafissadimora.org