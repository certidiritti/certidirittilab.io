---
title: 'Il comune di Milano censura i siti d''informazione gay'
date: Tue, 07 Feb 2012 13:02:16 +0000
draft: false
tags: [Politica]
---

L’Associazione Radicale Certi Diritti, pur non essendo colpita dalla censura, non avendo la parola “gay” nell’URL, denuncia questa assurda politica del Comune di Milano.

UPDATE:  
Il Comune di Milano ringrazia l'Associazione Radicale Certi Diritti per la segnalazione riguardo all'impossibilità di accedere attraverso la rete Internet dell'Amministrazione comunale ad alcuni siti di informazione. Negli scorsi anni, sono state effettivamente introdotte regole che possono tradursi in restrizioni dal sapore discriminatorio. Questa Amministrazione interverrà rimuovendole, nel rispetto del principio della non discriminazione e del libero accesso alla rete Internet.

Milano - Non sono ancora passate 2 settimane dall’annuncio che in 50 sedi del Comune aperte ai milanesi si può navigare gratuitamente su Internet tramite connessione wireless, per l'intera durata di apertura al pubblico che già si scoprono inquietanti risvolti dell’operazione.

Utilizzando la rete comunale gratuita è infatti impossibile accedere ai principali siti di informazione LGBTI come gay.it, gay.tv e queerblog. A cadere sotto la scure del censore meneghino è anche il sito nazionale di Arcigay, ma curiosamente non quello della sezione milanese.

Le motivazioni sono le più varie e curiose: spaziano dal divieto di pornografia, a quello di materiale sessuale sino a quello più diffuso e ridicolo, ma anche inquietante: il divieto di abbigliamento provocatorio. Dobbiamo aspettarci che adesso il Comune decida anche come dobbiamo vestirci?

L’Associazione Radicale Certi Diritti, pur non essendo colpita dalla censura, non avendo la parola “gay” nell’URL, denuncia questa assurda politica del Comune di Milano.

E’ inaccettabile che si ricada ancora nella censura di siti d’informazione per il solo fatto di contenere la parola “gay” o “queer”, ma non la parola “lesbica” (il sito di arcilesbica è infatti accessibile) nell’URL. Non tutti i siti gay sono pornografici come non tutti i siti pornografici sono gay, è così difficile capirlo. Questa equivalenza non fa altro che confermare l’odioso pregiudizio che riduce l’omosessualità al suo aspetto puramente sessuale negandone l’aspetto affettivo e relazionale e ledendo la dignità di tutte le persone LGBTI(E). Per di più questa censura non funziona neanche bene, non si capirebbe infatti perché altrimenti il sito di Arcigay Milano sarebbe visitabile.

L’Associazione Radicale Certi Diritti è comunque contraria a ogni forma di censura, anche quella sessuofoba che colpisce i siti pornografici, e ne chiede l’immediata rimozione da parte Comune di Milano. Capiamo che sia necessario limitare l’uso della rete gratuita per salvaguardare l’ampiezza della banda a beneficio di tutti, ma quest’obiettivo è facilmente raggiungibile inibendo il download dei file superiori a una certa dimensione. Ogni altra limitazione è solo frutto di puritani pruriti sessuofobi e omofobi inaccettabili.

Il Gruppo Radicale Federalista Europeo ha già presentato un interrogazione urgente in Consiglio comunale per chiedere spiegazioni e l’immediata rimozione dei blocchi.