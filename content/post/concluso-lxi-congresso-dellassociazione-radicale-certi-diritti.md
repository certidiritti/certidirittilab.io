---
title: 'Concluso l''XI Congresso dell''Associazione Radicale Certi Diritti'
date: Mon, 20 Nov 2017 13:01:13 +0000
draft: false
tags: [Politica]
---

![23755110_10213107582955602_5661477362703410701_n](http://www.certidiritti.org/wp-content/uploads/2017/11/23755110_10213107582955602_5661477362703410701_n-225x300.jpg)Si è concluso nel pomeriggio di ieri l'11° congresso dell'Associazione Radicale Certi Diritti con la **conferma di Leonardo Monaco segretario e Dario Belmonte tesoriere e la nuova elezione di Yuri Guaiana presidente**. I congressisti hanno ringraziato **Enzo Cucco**, cofondatore e presidente di lungo corso dell'Associazione che ha deciso di non ricandidarsi per l'anno a venire. L'assemblea, riunitasi a Roma nella sede di Radicali Italiani dal 17 al 19 novembre, ha confermato nella [mozione generale approvata](http://www.certidiritti.org/mozione-generale-approvata-dallxi-congresso/) l'impegno per una riforma complessiva del diritto di famiglia attraverso l'attivazione del fondo per i contenziosi strategici, la promozione dei diritti umani delle persone intersex e la decriminalizzazione del lavoro sessuale. Numerosi gli spunti che hanno arricchito il dibattito nel corso della tre giorni: dalla conferenza precongressuale sul diritto di famiglia animata da Melita Cavallo, Filomena Gallo, Marilena Grassadonia e Angelo Schillaci ai panel della seconda giornata su intersessualità, immigrazione, Ucraina e Azerbaijan, sex work, PrEP e transessualità. **Audio/video a cura di Radio Radicale:**

*   ### [1ª giornata](https://www.radioradicale.it/scheda/525523/la-liberta-sessuale-non-e-unopinione-xi-congresso-dellassociazione-radicale-certi)
    
*   ### [2ª giornata - mattina](https://www.radioradicale.it/scheda/525524/la-liberta-sessuale-non-e-unopinione-xi-congresso-dellassociazione-radicale-certi)
    
*   ### [2ª giornata - pomeriggio](https://www.radioradicale.it/scheda/525799/la-liberta-sessuale-non-e-unopinione-xi-congresso-dellassociazione-radicale-certi)
    
*   ### [3ª ed ultima giornata](https://www.radioradicale.it/scheda/525525/la-liberta-sessuale-non-e-unopinione-xi-congresso-dellassociazione-radicale-certi-3a)