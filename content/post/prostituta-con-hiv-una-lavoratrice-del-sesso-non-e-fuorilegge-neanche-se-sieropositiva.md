---
title: 'Prostituta con HIV: una lavoratrice del sesso non è fuorilegge neanche se sieropositiva'
date: Sun, 01 Sep 2013 09:31:56 +0000
draft: false
tags: [Lavoro sessuale]
---

Comunicato stampa

Roma, 1 settembre 2013

Il caso della giovane lavoratrice del sesso fermata, incarcerata per lesioni personali gravissime e poi scarcerata rappresenta una grave violazione contro una persona sieropositiva. Una lavoratrice del sesso non è una fuori legge, neanche se sieropositiva. Sarebbe come dire che ogni persona consapevole di aver contratto il virus dell'HIV (e la giovane rumena non lo era affatto) deve rinunciare ad avere rapporti sessuali. Tutti sappiamo che è necessario usare il preservativo per proteggersi e non diffondere infezioni, ma alcuni clienti offrono compensi maggiorati per fare sesso non protetto, mettendo così a rischio le prostitute e anche le proprie compagne. Per ora è evidente che la sola che potrebbe aver avuto un danno da qualche sconosciuto cliente è la prostituta stessa che da qualcuno è pur stata infettata. Quindi vittima per ora anziché colpevole. Da sempre le associazioni che lavorano sul campo chiedono che le amministrazioni locali e le istituzioni sanitarie nazionali contribuiscano e attivino programmi di prevenzione rivolti ai gruppi vulnerabili. Lo stesso Comune di Bologna è responsabile per non aver dato continuità con costanza e stabilità al programma di prevenzione della Unità di Strada rivolto alle persone prostitute e vittime di tratta. Si apprende dai giornali che proprio nel caso in questione i carabinieri stavano indagando su un giro di sfruttamento da cui possiamo presumere che la giovane fosse costretta a lavorare e pagare subendo minacce e ritorsioni. Bel risultato i papponi liberi e le vittime denunciate. Merita un'aperta condanna l'affermazione dell'assessore Monti che fa bassa speculazione su questo caso per invocare la riapertura delle case chiuse. Chi conosce la storia sa che erano luoghi dove le donne si ammalavano e venivano sfruttate, si legga le lettere dalle case chiuse scritte alla sen. Merlin. Le democrazie a cui dice che dovremmo ispirarci hanno bandito il controllo sanitario obbligatorio per le prostitute e istituito la LIBERA professione. Se poi una lo vuole fare in un bordello è una scelta non un obbligo. Evidentemente un bel gruppo di politici incapaci di fare in Parlamento una buona e giusta legge di riforma che garantisca i Diritti delle lavoratrici e lavoratori del sesso cercano di creare allarme sociale per sostenere le loro tesi "casiniste".

Comitato per i Diritti Civili delle Prostitute

Movimento Identità Transessuale

Associazione Radicale Certi Diritti