---
title: 'Sentenza Corte di Giustizia europea a favore di coppia gay'
date: Tue, 10 May 2011 20:09:34 +0000
draft: false
tags: [Diritto di Famiglia]
---

**La Corte ha affermato che una coppia gay unita civilmente ha lo stesso diritto alla pensione di una coppia sposata. La sentenza ovviamente non riguarda gli stati come l'italia che non riconoscono alle coppie gay nessun diritto.**  
  
Comunicato Stampa dell'Associazione Radicale Certi  Diritti  
  
Lussemburgo - Roma, 10 maggio 2011

La Corte di Giustizia europea ha emesso oggi la sua sentenza pregiudiziale sul caso Römer che conferma la sua precedente giurisprudenza in tema di divieto di discriminazione basata sull'orientamento sessuale tra coppie dello stesso sesso e di sesso diverso in quegli Stati dove tali regimi sono sostanzialmente simili. Nel caso di specie, una pensione complementare di vecchiaia versata ad una persona legata ad un partner in un’unione civile, inferiore a quella concessa ad una persona sposata, costituisce una discriminazione fondata sulle tendenze sessuali qualora l’unione civile sia riservata a persone dello stesso sesso e si trovi in una situazione di diritto e di fatto paragonabile a quella del matrimonio.  
  
L'Associazione Radicale Certi Diritti si felicita per la sentenza, che purtroppo non riguarda gli Stati membri come l'Italia dove non é riconosciuto alcun diritto per le coppie dello stesso sesso. In Italia sono riconosciute solamente discriminazioni, violenze ed aggressioni verbali e fisiche, private o pubbliche, nelle strade o in televisione.  
  
  
**Qui di seguito il testo integrale della Dichairazione  della Corte di Giustizia:**  
  
Corte di giustizia dell’Unione europea  
Lussemburgo, 10 maggio 2011  
  
**Sentenza nella causa C-147/08 Römer / Freie und Hansestadt Hamburg**  
  
Una pensione complementare di vecchiaia versata ad una persona legata ad un partner in un’unione civile, inferiore a quella concessa ad una persona sposata, può costituire una discriminazione fondata sulle tendenze sessuali  
  
Tale ipotesi si verifica qualora l’unione civile sia riservata a persone dello stesso sesso e si trovi in una situazione di diritto e di fatto paragonabile a quella del matrimonio  
  
Il sig. Jürgen Römer ha lavorato per la Freie und Hansestadt Hamburg (Città di Amburgo, Germania) in qualità di impiegato amministrativo dal 1950 fino al sopravvenire della sua incapacità lavorativa il 31 maggio 1990. A partire dal 1969, egli ha vissuto ininterrottamente con il suo compagno, il sig. U., con il quale ha concluso un’unione civile registrata conformemente alla legge tedesca del 16 febbraio 2001 sulle unioni civili registrate. Il sig. Römer ha informato di tale circostanza il suo ex datore di lavoro con lettera in data 16 ottobre 2001.  
  
Successivamente, egli ha chiesto che l’importo della sua pensione complementare di vecchiaia fosse ricalcolato applicando uno scaglione tributario più favorevole, corrispondente a quello applicato ai beneficiari coniugati. Pertanto, nel mese di settembre 2001 l’importo mensile della sua pensione di vecchiaia avrebbe dovuto essere superiore di DEM 590,87 (EUR 302,11) se per determinare il suo ammontare fosse stato preso in considerazione lo scaglione tributario più favorevole. Con lettera 10 dicembre 2001, la Città di Amburgo ha rifiutato di applicare lo scaglione tributario più favorevole per calcolare l’importo della pensione di vecchiaia dell’interessato, in quanto soltanto i beneficiari coniugati, non stabilmente separati, e quelli aventi diritto ad assegni familiari o ad altre prestazioni analoghe hanno diritto a tale beneficio.  
  
Ritenendo di aver diritto ad essere trattato come un beneficiario coniugato non stabilmente separato ai fini del calcolo della sua pensione, e reputando che tale diritto discenda dalla direttiva 2000/78/CE che stabilisce un quadro generale per la parità di trattamento in materia di occupazione e di condizioni di lavoro \[1\], il sig. Römer ha adito l’Arbeitsgericht Hamburg (Tribunale del lavoro di Amburgo, Germania). Tale giudice ha chiesto alla Corte di giustizia di interpretare i principi generali e le disposizioni del diritto dell’Unione riguardanti le discriminazioni fondate sulle tendenze sessuali in materia di occupazione e di lavoro.  
  
Nella sua sentenza odierna, la Corte constata anzitutto che le pensioni complementari di vecchiaia – come quella oggetto della presente controversia – rientrano nell’ambito di applicazione della direttiva 2000/78.  
  
La Corte ricorda poi, in primo luogo, che la constatazione di una discriminazione fondata sulle tendenze sessuali esige che le situazioni in questione siano paragonabili, con specifico e concreto riferimento alla prestazione di cui trattasi.  
  
Al riguardo, la Corte osserva che la legge tedesca sulle unioni civili registrate ha introdotto, per le persone dello stesso sesso, l’istituto dell’unione civile, scegliendo di precludere a tali persone il matrimonio, che resta riservato alle sole persone di sesso diverso. A seguito del progressivo ravvicinamento del regime dell’unione civile a quello del matrimonio, non esiste più, ad avviso del giudice del rinvio, nell’ordinamento giuridico tedesco, alcuna differenza giuridica di rilievo tra questi due status personali. Infatti, la principale differenza che ancora permane consiste nel fatto che il matrimonio presuppone che i coniugi siano di sesso diverso, mentre l’unione civile registrata esige che i partner abbiano il medesimo sesso.  
  
Orbene, nel caso di specie, il beneficio della pensione complementare di vecchiaia presuppone non soltanto che il partner sia sposato, ma anche che egli non sia stabilmente separato dal suo coniuge, in quanto tale pensione mira a procurare un reddito sostitutivo a vantaggio dell’interessato e, indirettamente, delle persone che vivono con lui. A questo proposito, la Corte sottolinea che la legge tedesca sulle unioni civili registrate stabilisce che i partner dell’unione civile hanno l’obbligo reciproco di prestarsi soccorso e assistenza nonché quello di contribuire in maniera adeguata ai bisogni della comunità partenariale mediante il loro lavoro e il loro patrimonio, così come è previsto anche per i coniugi nel corso della loro vita in comune. Pertanto, a giudizio della Corte, i medesimi obblighi gravano sui partner dell’unione civile così come sui coniugi. Ne consegue che le due situazioni sono paragonabili.  
  
In secondo luogo, la Corte constata che, per quanto riguarda il criterio attinente ad un trattamento meno favorevole fondato sulle tendenze sessuali, risulta che la pensione del sig. Römer sarebbe stata aumentata qualora egli, anziché contrarre un’unione civile registrata con un uomo, si fosse sposato. Per di più, il trattamento più favorevole non è collegato né ai redditi dei componenti l’unione civile, né all’esistenza di figli, né ad altri elementi come quelli riguardanti i bisogni economici del partner. Inoltre, la Corte rileva che i contributi dovuti dal sig. Römer in rapporto alla pensione non erano in alcun modo correlati al suo stato civile, dal momento che egli era tenuto a contribuire alle spese pensionistiche versando una quota pari a quella dei suoi colleghi coniugati.  
  
Infine, per quanto riguarda gli effetti di una discriminazione fondata sulle tendenze sessuali, la Corte precisa, da un lato, che, a motivo del primato del diritto dell’Unione, il diritto alla parità di trattamento può essere rivendicato da un singolo nei confronti di un ente locale senza necessità di attendere che il legislatore nazionale renda la disposizione in questione conforme al diritto dell’Unione. Dall’altro lato, la Corte puntualizza che il diritto alla parità di trattamento può essere rivendicato da un singolo soltanto dopo la scadenza del termine di trasposizione della citata direttiva, ossia a partire dal 3 dicembre 2003.