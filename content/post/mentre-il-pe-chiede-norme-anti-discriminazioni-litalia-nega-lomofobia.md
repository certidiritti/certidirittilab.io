---
title: 'MENTRE IL PE CHIEDE NORME ANTI-DISCRIMINAZIONI,  L''ITALIA NEGA L''OMOFOBIA'
date: Tue, 20 May 2008 16:58:12 +0000
draft: false
tags: [Carfagna, Comunicati stampa, norme anti discriminazione, patrocinio, Pride, Sergio Rovasio, unione europea, violenza omofobica]
---

MENTRE IL PARLAMENTO EUROPEO CHIEDE UFFICIALMENTE NUOVE NORME EUROPEE ANTI-DISCRIMINAZIONI,  IL GOVERNO ITALIANO NEGA L'EVIDENZA DELLA VIOLENZA OMOFOBICA PER MOTIVI IDEOLOGICI.  
**Comunicato Stampa dell'Associazione radicale Certi Diritti.  
  
**Oggi il Parlamento Europeo ha approvato a grande maggioranza la relazione della deputata liberale Liz Lynne, che chiede alla Commissione europea di completare il pacchetto anti-discriminazioni basato sull'articolo 13 del Trattato CE, promuovendo una nuova direttiva che protegga anche le persone omosessuali dalle discriminazioni nel settore dei servizi, dei beni, dell'educazione, oltre che del lavoro. E' la nona volta che il PE si pronuncia in questo senso, e la Commissione si appresta in giugno a rendere pubblica la sua proposta al riguardo. [Certi Diritti ha lanciato un appello](http://www.radicalparty.org/lgbt_discrimination/form.php) disponibile ai siti www.certidiritti.it  e www.radicali.it  
   
Nel frattempo in Italia divampa la polemica sulla presunta assenza di discriminazioni contro le persone omosessuali in Italia e sul negato patrocinio del governo al Gay Pride, definito dalla Carfagna "folkloristico" ed "eccessivo". Che una manifestazione politica che chiede eguaglianza, unioni civili e matrimonio per le persone dello stesso sesso non possa essere appoggiata dal governo Berlusconi, oltre che prevedibile, é legittimo: si farà a meno della Carfagna e del patrocinio del Governo. Però a un Ministro della Repubblica, ed al Governo di cui fa parte, chiediamo di evitare dichiarazioni folkloristiche ed eccessive, soprattutto rispetto ad un'Europa che va esattamente e sempre di più nel senso opposto. Negare una realtà come lo è la violenza omofobica per motivi ideologici è un gravissimo errore.