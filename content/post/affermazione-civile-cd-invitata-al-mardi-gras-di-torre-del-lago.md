---
title: 'AFFERMAZIONE CIVILE: CD INVITATA AL MARDI GRAS DI TORRE DEL LAGO'
date: Fri, 07 Aug 2009 10:24:43 +0000
draft: false
tags: [Comunicati stampa]
---

Sergio Rovasio, Segretario dell'Associazione Radicale Certi Diritti, è stato invitato al Mardi Gras di Torre del Lago (Lucca) domenica 9 agosto per un incontro con la comunità lgbt sul tema dei diritti civili. All'incontro, che si svolgerà dalle 22 alle 23, parteciperà anche l'Avvocato Francesco Bilotta, co-fondatore di Avvocatura lgbt Rete Lenford e membro dell'Associazione Radicale Certi Diritti.

Tutti i dettagli ai seguenti link:

[http://www.mardigras.it/](http://www.mardigras.it/)

[http://www.gay.it/channel/musica/23225/MARDI-GRAS-2007-TUTTO-PRONTO-PER-IL-FESTIVAL-GAY.html](http://www.gay.it/channel/musica/23225/MARDI-GRAS-2007-TUTTO-PRONTO-PER-IL-FESTIVAL-GAY.html)

[http://www.gay.it/channel/attualita/27041/L-omofobia-non-ferma-il-My-Mardi-Gras-.html](http://www.gay.it/channel/attualita/27041/L-omofobia-non-ferma-il-My-Mardi-Gras-.html)