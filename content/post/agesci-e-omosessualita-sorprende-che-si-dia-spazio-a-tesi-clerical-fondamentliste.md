---
title: 'Agesci e omosessualità: sorprende che si dia spazio a tesi clerical-fondamentliste'
date: Sat, 05 May 2012 14:46:38 +0000
draft: false
tags: [Politica]
---

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Roma, 5 maggio 2012

L’Agesci è sempre stata un’associazione all’avanguardia riguardo i metodi pedagogici e di educazione ai diversi temi sociali, in particolare quelli sull’ambiente e sull’aiuto al prossimo. E’ davvero soprendente che negli  ‘atti finali e le relative conclusioni’ del seminario proposto dalla rivista dell’Agesci  si leggano espressioni tipiche della peggior cultura omofobica clerical-fondamentalista riguardo la condizione delle persone omosessuali. L’Agesci diventerebbe così uno dei peggiori centri di odio omofobico proprio mentre in Italia si avvia un dibattito politico serio e importante, anche sul piano istituzionale, per riconoscere diritti alle persone lesbiche e gay.

Era ora che Agesci iniziasse un percorso sull'omosessualità dovuto da troppo tempo ma, se il buongiorno si vede dal mattino, non c'è da stare allegri. Con il seminario trascritto negli atti recentemente pubblicati Agesci si è resa responsabile di aver invitato a riflettere su un tama importante e delicato come l'omosessualità persone con delle posizioni ideologiche e retrive degne del peggior oscurantismo.

L'Agesci farebbe bene a promuovere seminari di aggiornamento su quanto le istituzioni europee, il Consiglio d’Europa, l’Onu e tutti gli organismi scientifici internazionali, come l'American Psychological Association, promuovono riguardo questo tema, piuttosto che dare spazio a tesi assurde come quella di considerare un modello educativo migliore per ragazzine e ragazzini quello di un capo scout che mente (non fare coming out non è altro che un'incitazione alla menzogna destinata a creare solo enormi sofferenze) piuttosto che uno omosessuale.

Ci aguriamo che Agesci torni quanto prima sui suoi passi e inizi un serio cammino di tolleranza e attenzione maggiore ai problemi della vita di milioni di persone.