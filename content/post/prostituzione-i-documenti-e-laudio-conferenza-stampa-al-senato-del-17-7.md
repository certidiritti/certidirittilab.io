---
title: 'PROSTITUZIONE - I DOCUMENTI E L''AUDIO CONFERENZA STAMPA AL SENATO DEL 17-7'
date: Wed, 16 Jul 2008 12:21:44 +0000
draft: false
tags: [Conferenza Stampa al Senato, Prostituzione, Senza categoria]
---

Dal caso della prostituta di Parma costretta a pagare tasse per un lavoro che nessuna legge considera tale, alla legalizzazione della vendita di prestazioni sessuali.  
Dalla diretta voce dei protagonisti, le proposte di Radicali Italiani  
ROMA, GIOVEDI' 17 LUGLIO, ORE 15, SALA STAMPA DEL SENATO      
  
Hanno partecipato alla conferenza stampa:  
**Donatella Poretti**, Senatrice Radicale - PD  
**Marco Perduca**, Senatore Radicale - PD  
**Margherita**, accompagnatrice;  
**Luca Berni**, avvocato di una delle prostitute condannate;  
**Pia Covre**, Presidente Comitato Diritti Civili delle Prostitute;  
**Bruno Mellano**, Presidente di Radicali Italiani;  
**Sergio Rovasio**, Segretario dell'associazione Certi Diritti  
**Guido Allegrezza**, Direttivo di Certi Diritti  
  
Di seguito i testi di sintesi della Senatrice Donatella Poretti e di Guido Allegrezza  
  
Dal sito di Radio Radicale, l'audio integrale della Conferenza Stampa:  
[http://www.radioradicale.it/scheda/258555/prostituzione-e-fisco](http://www.radioradicale.it/scheda/258555/prostituzione-e-fisco)  
  

**Testo di sintesi della Senatrice radicale - Pd Donatella Poretti**:  
  
Fisco e prostituzione, accertamenti esattoriali, tasse e diritti negati, era il tema della conferenza stampa tenutasi oggi al Senato. Di fronte alle richieste dello Stato alle prostitute di pagare le tasse sulla loro attivita', diventa ancora piu' urgente varare una legge su un ambito lasciato senza alcuna regolamentazione, tanto urgente da avere depositato gia' dal primo giorno della legislatura, un disegno di legge in materia con il senatore Marco Perduca. Un ddl in tre articoli per il riconoscimento dell'attivita' di 'prestazione di servizi sessuali remunerati tra persone maggiorenni consenzienti'. Due i regolamenti previsti, uno per i controlli igienico e sanitari, l'altro per il fisco.  
E proprio sulle richieste di tasse alle prostitute che oggi abbiamo depositato una interrogazione parlamentare per chiedere al governo una 'interpretazione uniforme' alla quale l'Agenzia delle Entrate si attenga per il trattamento fiscale dei redditi derivanti dalla prostituzione. Questo sempre in attesa di una legge che ponga fine all'attuale situazione di incertezza, ma che provoca anche lo sfruttamento di tante donne.  
All'incontro ha preso parte anche Margherita, una 'accompagnatrice' che ha avuto una 'cartella' da 88 mila euro, presente anche l'avvocato, Luca Berni, che assiste una prostituta che ha ricevuto un'altra cartella esattoriale e che spiega di volere non un pagamento "fatto cosi'" ma un reddito di una attivita' riconosciuta, superando l'ipocrisia di fondo e una morale cattolica che mantengono una situazione che porta allo sfruttamento e al degrado. Bruno Mellano, presidente dei Radicali, ha evidenziato come siamo di fronte alla sconfitta della politica, tale da far emergere contraddizioni che potrebbero spingere per affrontare il tema.  
Come si puo' fare la dichiarazione dei redditi per una attivita' che non e' riconosciuta giuridicamente? A fronte del pagamento delle tasse deve esserci il riconoscimento dell'attivita' svolta dalle prostitute. In caso contrario lo Stato sarebbe paragonabile ad uno sfruttatore perche' 'esige' o 'estorce' balzelli dai proventi della prostituzione, attivita' che non riconosce. Noi chiediamo quindi al governo di darci una risposta perche e' una situazione che potrebbe portare a denunce". A preannunciare proprio una denuncia per sfruttamento da parte dello Stato e' stata Pia Covre, presidente del Comitato dei diritti civili delle prostitute e su questo ha gia' visto il sostegno di Sergio Rovasio, segretario dell'associazione Certi Diritti, che ha evidenziato come in Italia stiamo regredendo in tema di diritti civili.  
  
Il disegno di legge: [http://blog.donatellaporetti.it/?p=86](http://blog.donatellaporetti.it/?p=86)  
  
L'interrogazione: [http://blog.donatellaporetti.it/?p=170](http://blog.donatellaporetti.it/?p=170)  
  
  
**Testo di Sintesi di Guido Allegrezza, Direttivo di Certi Diritti:  
  
**_Un disegno di legge in pochi articoli sintetici, quasi scarni e buona parte dei problemi collegati alla prostituzione svanirebbero come un vampiro colpito da un raggio di sole. Lo hanno presentato i senatori radicali Donatella Poretti e Marco Perduca, adesso vedremo se i "vampiri benpensanti" ci staranno a farsi dissolvere da un raggio di sole.  
  
_Sia chiaro, se qualcuno ha ancora dei dubbi, in Italia la prostituzione non è reato. La legge Merlin del 1958 ha chiuso le case di tolleranza e prevede il divieto di qualsiasi attività tesa a consentire, favorire, agevolare la prostituzione, ma non considera la prostituzione in sè un reato. E l'intero sistema normativo non prevede che essa lo sia.  Addirittura è stato depenalizzato dal 1999 l'adescamento e trasfromato in illecito amministrativo. Certo sappiamo tutti che attorno alla prostituzione gira un intero sistema di malaffare e di criminalità, che sfruttano la prostituzione a proprio vantaggio. Ma sono fatti distinti. Anzi una regolarizzazione amminsitrativa delle professioni sessuali esercitate da donne, uomini e transgender avrebbe il duplice benefico effetto di garantire a questi professionisti tutte le tutele che derivano dallo svolgere una professione riconosciuta e regolata, togliere terreno alla criminalità, rendere più facile l'azione delle forze dell'ordine contro i reati di sfruttamento e violenza connessi alla prostituzione, liberare risorse da dedicare a temi ben più gravi ed infine anche a recuperare a tassazione un consistente porzione di redditi che oggi sfuggono alla tassazione.  
  
E l'occasione della conferenza presso la Sala Stampa del Senato che giovedì 17 luglio, hanno tenuto i due senatori insieme a Sergio Rovasio (Segretario di Certi Diritti), a Bruno Mellano (Presidente di Radicali Italiani) e Pia Covre (Presidente del Comitato diritti civili delle prostitute) partiva proprio da un problema fiscale.  
L'Agenzia delle Entrate di Parma ha avviato un procedimento di accertamento per redditi da attività illecite nei confronti di una signora che esercita liberamente la professione di accompagnatrice (si badi che nell'ambito dei lavori sessuali, non esiste solo la prostituzione finalizzata alla consumazione di un atto sessuale, ma un insieme di attività che sono in qualche modo connesse al sesso), chiedendo conto di un reddito per il triennio 2003-2005 pari a quasi 400.000 euro, dai quali deriverebbe un onere fiscale (fra imposte non pagate, sanzioni e interessi) pari a circa 90.000 euro da pagare con le usuali modalità (entro 60 giorni dalla notifica, rinuncia al ricorso e sconto del 50%, oppure attivazione del ricorso, pagamento in anticipo di una sorta di caparra e ipoteca sui beni registrati a garanzia della somma finale!).  
  
Margherita, questo il nome della nostra professionista, ha chiarito che intende pagare entro i 60 giorni, perché non vuole esporre a rischio i propri beni. L'avvocato Luca Berni, nel suo illuminante intervento ha chiarito che però si farà di tutto affinché il pagamento venga riconosciuto come dovuto per redditi da attività professionali perfettamente legali e non come reddito derivante da attività illecite (esiste una normativa a parte per questa forma di redditi). Pia Covre ha aggiunto che denuncerà lo Stato per sfruttamento della prostituzione se le cose non si muoveranno nella direzione indicata dall'avvocato Berni, anche perché non intervenire in questo caso, significherebbe accettare un precedente che aprirebbe la strada ad un'infinità di procedimenti da parte dell'Agenzia delle Entrate.  
  
Proprio per risolvere questo genere di problemi, i senatori radicali Porretta e Perduca hanno presentato il 29 aprile 2008 il disegno di legge n. 125, che è stato brevemente illustrato nella conferenza e che presenta il seguente testo:

*   Art. 1.  
    1\. L'attivita` di prestazione di servizi sessuali remunerati tra persone maggiorenni consenzienti è riconosciuta secondo le disposizioni della presente legge.  
    2\. La legge 20 febbraio 1958, n. 75, è abrogata.

*   Art. 2.  
    1\. La prestazione di servizi sessuali remunerativi può essere svolta in forma autonoma, dipendente o associata. I contratti che prevedono la prestazione di servizi sessuali remunerativi non rientrano nel campo di applicazione dell'articolo 1343 del codice civile.

*   Art. 3.  
    1\. La disciplina relativa ai controlli igienico-sanitari e alla sicurezza dei locali in cui è esercitata l'attività di prestazione dei servizi sessuali remunerativi è stabilita con regolamento adottato, ai sensi dell'articolo 17, comma 3, della legge 23 agosto 1988, n. 400, dal Ministro del lavoro e della previdenza sociale, di concerto con il Ministro della salute.  
    2\. Le disposizioni per la disciplina degli aspetti tributari sono stabilite con regolamento adottato, nelle forme previste dal comma 1, dal Ministro dell'economia e delle finanze.

Nel corso degli interventi dei relatori e del breve dialogo con il pubblico, sono emersi alcuni pareri interessanti:

*   Ancora una volta siamo di fronte alla sconfitta della politica. Speriamo che ci siano contraddizioni tali da spingere il governo ad affrontare il tema (Mellano)
*   Il ddl si compone di cinque articoli finalizzati a regolamentare il fenomeno della prostituzione, dando diritto di cittadinanza a tutti coloro che sono coinvolti nel fenomeno. Insomma, se si vuole tassare si deve regolamentare. Altrimenti saremmo di fronte ad un'etica di stato che si ispira, come al solito, a Oltretevere (Perduca)
*   Il fenomeno della prostituzione viene sempre declinato al femminile, mentre sarebbe opportuno che si mettesse in evidenza che vi è un ampio universo di prostituzione maschie e transgender e che la clientela in tanti casi è omosessuale o bisessuale (Allegrezza, Certi Diritti)
*   è intollerabile che permangano all'interno del nostro ordinamento forme di pregiudizio e di ipocrisia che alimentano fenomeni di criminalità, rendendo lo stato di fatto complice di fenomeni odiosi come lo sfruttamento della persone per attività sessuali (Rovasio, Certi Diritti)

A rileggere questa vicenda, tornano in mente le parole di una canzone di Fiorenzo Carpi dello spettacolo di Dario Fo "Settimo: ruba un po' meno". Nel "Canto delle svergognate" del 1964, parlando delle prostitute che venivano "emigrate" a colonizzare i nuovi territori, vero "faro di civiltà" e "vere dame di carità", e che vendevano "amore a sotto banco e sotto prezzo", si ricordava che:l'amore veniva "tassato e circa un terzo si prendea lo Stato: con questi soldi, han calcolato, si son pagati 'na corazzata, una corazzata e un incrociatore". Salvo poi, mettere nel conto anche le tasse sulla quindicina che i marinai spendevano appunto con le prostitute e che alla fine coprivano "tutte le spese dell'ammiragliato".  
  
  
**BOX  
In Europa, la legislazione è molto diversa a seconda del paese.  
Ecco i dati presentati dalla senatrice radicale Donatella Poretti.  
**

*   OLANDA: è legale dal 1815. Dal 2000 sono diventati legali anche i locali dove esercitarla. Sono anche disponibili 11 zone "speciali" all'aperto. Al di fuori si rischia l'arresto. Le prostitute per essere in regola debbono pagare le tasse.
*   BELGIO: è legale dal 1948, ma può essere perseguita se turba l'ordine pubblico. Punito anche lo sfruttamento. Generalmente la maggior parte dell'attività economica legata al sesso si svolge in bar a luci rosse e case private. Le prostitute debbono essere in regola anche con il fisco proprio come delle lavoratici autonome e possono godere anche di assistenza sociale.
*   GERMANIA: una legge legalizza l'attività di circa 400.000 lavoratrici del sesso assegnando loro le garanzie assicurative per malattia, disoccupazione e pensione. Il favoreggiamento non è punibile (sempre che non vi sia sfruttamento) e l'attività dei locali ad hoc è considerata lecita.
*   AUSTRIA: è consentita nelle case chiuse ed è obbligatoria una registrazione di esercizio. All'aperto è tollerata in alcune aree urbane ed extra-urbane.
*   SVIZZERA: è legale; nel Cantone Ticino viene anche esercitata all'interno di bar-alberghi. La normativa prevede anche la patente per affittacamere e la registrazione.
*   GRAN BRETAGNA: non è illegale ma è vietato l'adescamento e lo sfruttamento. Si svolge prevalentemente in locali e abitazioni private, meno frequentemente in strada.
*   SPAGNA: Le case chiuse sono illegali dal 1956 anche se di fatto si sono trasformate in ''club''. Dal 1995 la legge non vieta la prostituzione, ma punisce chi ricatta e sfrutta.
*   FRANCIA: Le case di tolleranza sono state chiuse nel 1946 con la legge Marthe Richard che non considera reato la prostituzione sulle strade. Il meretricio viene definito come un'attività che viola la tranquillità e l'ordine pubblico ed è per questo che si prevedono sanzioni contro l'adescamento e i clienti.
*   GRECIA: le prostitute debbono iscriversi in appositi registri e sottoporsi a visite mediche periodiche che autorizzano a svolgere il lavoro in veste quasi ufficiale.
*   SVEZIA: è uno dei Paesi piu' severi nei confronti del sesso a pagamento. Anche se non lo considera un reato, una legge in vigore dal gennaio 1999 ha introdotto le maniere forti nei confronti dei clienti. Se colti in flagrante rischiano da sei mesi a un anno di carcere. Non sono punibili le prostitute, ma è sanzionato l'adescamento.
*   IRLANDA: è considerata un reato. Niente case chiuse e sono previste ammende ed arresto sia per le prostitute sia per i clienti.