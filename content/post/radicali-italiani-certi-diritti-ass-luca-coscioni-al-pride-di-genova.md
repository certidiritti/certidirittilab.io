---
title: 'RADICALI ITALIANI, CERTI DIRITTI, ASS LUCA COSCIONI AL PRIDE DI GENOVA'
date: Thu, 25 Jun 2009 07:24:05 +0000
draft: false
tags: [Comunicati stampa]
---

**GAYPRIDE DI GENOVA: L’ADESIONE DI RADICALI ITALIANI, CERTI DIRITTI E ASSOCIAZIONE LUCA COSCIONI ALLA MANIFESTAZIONE DI SABATO 27 GIUGNO. GLI SLOGAN SARANNO: LAICITA’, LIBERTA’ E RESPONSABILITA’.**

In occasione del Gay Pride nazionale di Genova, che sfilerà sabato 27 giugno 2009, Radicali Italiani, l’Associazione Radicale Certi Diritti e l’Associazione Luca Coscioni hanno inviato al Comitato promotore la loro adesione. I radicali saranno presenti con un loro carro insieme a militanti e dirigenti per riaffermare i principi della laicità, libertà e responsabilità delle persone.

I radicali da sempre sono impegnati per la difesa e la promozione dei diritti civili e umani delle persone lesbiche, gay e transgender. Sin dal 1971 ospitarono in tutte le loro sedi il Fuori!, il primo movimento gay italiano per la liberazione sessuale. Dal 1982 esiste in Italia una legge, voluta dai radicali, che consente alle persone transessuali di vedere riconosciuti i loro diritti. I parlamentari radicali hanno depositato in questa Legislatura alcune proposte di legge per il riconoscimento dei diritti delle persone lgbt, per le unioni civili e per il matrimonio gay.

In occasione del Gay pride, dal Carro dell’Associazione Radicale Certi Diritti, verranno distribuiti preservativi e materiale sulla campagna di Affermazione Civile finalizzata all’accesso all’istituto del matrimonio per le coppie dello stesso sesso. Alcune decine di coppie in tutta Italia hanno deciso di intraprendere la via legale con il supporto di Certi Diritti e con la Rete Lenford, avvocatura per la difesa dei diritti lgbt, contro il diniego opposto dai Comuni al matrimonio gay.