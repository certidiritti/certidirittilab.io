---
title: 'Israele: bene la rimozione delle linee guida. Si elimini anche l''ultima inaccettabile discriminazione sui corpi delle vittime del''Hiv.'
date: Sat, 10 Dec 2016 12:52:03 +0000
draft: false
tags: [Medio oriente]
---

![download](http://www.certidiritti.org/wp-content/uploads/2016/12/download.jpg)Ringraziamo il giornale filo-governativo Israel Hayom per aver sollevato il caso delle odiose linee guida del ministro degli Esteri israeliano che prevedevano l'etichettatura dei cadaveri di persone morte di malattie infettive compreso l'AIDS. In aggiunta le linee guida prevedevano l'avvolgimento nel nylon e la sepoltura a dieci metri dagli altri corpi. La denuncia della Task Force contro l'AIDS e dei primari dei maggiori ospedali del Paese ha fatto fare immediata marcia indietro al ministro degli Esteri israeliano, l'ultra-ortodosso Yaakov Litzman, che tuttavia deve ancora eliminare l'odiosa direttiva che prevede l'etichettatura dei corpi. "Ci uniamo alla richiesta dell'associazione israeliana Task Force contro l'AIDS affinché anche l'ultima direttiva venga ritirata. L'accanimento sui corpi senza vita delle vittime dell'HIV è frutto di una furia ideologica veramente inquietante", dichiara Leonardo Monaco, segretario dell'Associzizone Radicale Certi Diritti.