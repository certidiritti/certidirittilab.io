---
title: 'UNIONI CIVILI A MILANO: LA CITTÀ SIA ALLA TESTA DI UNA CAMPAGNA PER IL MATRIMONIO EGUALITARIO.'
date: Thu, 04 Aug 2016 15:59:46 +0000
draft: false
tags: [Matrimonio egualitario]
---

[![Unioni-Civili-744x445](http://www.certidiritti.org/wp-content/uploads/2016/07/Unioni-Civili-744x445-300x179.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/07/Unioni-Civili-744x445.jpg)Finalmente anche Milano comincia a celebrare le unioni civili.

Ringraziamo il Sindaco Beppe Sala per aver deciso di celebrare lui le prime unioni indossando la fascia tricolore, dando un messaggio simbolico importante, che permette davvero di parlare di “celebrazione” delle unioni civili e non di mera “costituzione”, come fanno invece i decreti attuativi.

 Ora che questo primo importante passo è stato compiuto, chiediamo che Milano continui ad essere alla testa di un grande movimento di città e sindaci italiani per il matrimonio egualitario, come hanno fatto i sindaci americani di «[Mayors for the Freedom to Marry](http://www.freedomtomarry.org/pages/mayors-for-the-freedom-to-marry)».

_Dichiarazione di **Lorenzo Lipparini**, Assessore a Partecipazione, Cittadinanza attiva e Open data e segretario dell’Associazione Enzo Tortora - Radicali Milano e di **Yuri Guaiana**, segretario dell’Associazione Radicale Certi Diritti._

Milano, 4 agosto 2016