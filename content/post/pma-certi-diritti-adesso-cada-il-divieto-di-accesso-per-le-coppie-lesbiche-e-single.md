---
title: 'PMA. Certi Diritti: adesso cada il divieto di accesso per le coppie lesbiche e single'
date: Sun, 17 May 2015 13:31:06 +0000
draft: false
tags: [Diritto di Famiglia]
---

[![fecondazione-assistita-la-stimolazione-ormonale-381550_w1020h450c1](http://www.certidiritti.org/wp-content/uploads/2015/05/fecondazione-assistita-la-stimolazione-ormonale-381550_w1020h450c1-300x132.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/05/fecondazione-assistita-la-stimolazione-ormonale-381550_w1020h450c1.jpg)"Con il pronunciamento di qualche giorno fa della Corte Costituzionale cade parte di quello che resta della Legge 40: il divieto di accesso alle tecniche di procreazione medicalmente assistita per le coppie fertili portatrici di patologie genetiche e alla diagnosi pre-impianto, l'espressione più sadica di uno dei più odiosi errori del Parlamento italiano. Adesso si rimedi all'esplicita discriminazione sulla base dell'orientamento sessuale contenuta in quella legge: cada il divieto di accesso alla PMA alle coppie lesbiche e ai singoli." Così Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, sulla sentenza della Consulta. ​"​Il plauso di Certi Diritti va alle coppie ricorrenti che si sono pubblicamente esposte e al lavoro dell'Associazione Luca Coscioni e di Filomenta Gallo, che ha incardinato il ricorso"