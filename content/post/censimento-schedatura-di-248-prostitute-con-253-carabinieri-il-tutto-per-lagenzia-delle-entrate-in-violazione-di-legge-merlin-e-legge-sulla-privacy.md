---
title: 'Censimento/schedatura di 248 prostitute con 253 Carabinieri: il tutto per l''agenzia delle entrate. In violazione di legge Merlin e legge sulla privacy'
date: Fri, 13 Jan 2012 10:01:54 +0000
draft: false
tags: [Lavoro sessuale]
---

Violata la legge sulla privacy, la legge Merlin e la Convenzione Europea dei Diritti dell'uomo. Che senso ha inviare tutto all'agenzia delle entrate senza legalizzare?

Interrogazione urgente dei Parlamentari Radicali Rita Bernardini e Donatella Poretti.

Roma, 12 gennaio 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

E’ davvero sorprendente quanto avvenuto a Bologna. Il comando dei Carabinieri ha avviato un censimento/schedatura, senza precedenti, di 248 prostitute, consegnando loro un modulo dove vengono fatte domande incredibili del tipo: “svolge attività di meretricio da...”, “dichiarazione in merito alla presenza di sfruttatori…” e “compenso medio a prestazione…”.  Per svolgere questa operazione di indagine/schedatura sono stati messi in campo ben 253 Carabinieri in un periodo compreso di tre mesi. Il Comandante dell’Arma di Bologna, ha precisato che scopo di questo censimento è quello soprattutto di chiedere all'Agenzia delle Entrate di fare ulteriori verifiche e, in caso, di sottoporre quei patrimoni a una qualche forma di tassazione, come sarebbe possibile per qualsiasi attività di lavoro autonomo e a tutelare le donne che sono sulla strada:  ''tutto e' fatto nel rispetto della legge'' e si tratta di ''un modulo per capire chi sono le prostitute, in che condizioni vivono, se pagano affitti regolari”. A tal proposito il procuratore aggiunto Valter Giovannini ha spiegato che ''si tratta di assunzioni di informazioni con le quali i carabinieri possono trarre anche spunti investigativi” e che “se comunque qualche ragazza dovesse sentirsi schedata e pensasse di presentare un esposto, la Procura lo esaminerebbe”.

Ciò che forse è opportuno ricordare ai Carabinieri di Bologna è quanto dice la Legge Merlin, all’art. 7, ovvero che: “Le autorità di pubblica sicurezza, sanitarie e qualsiasi altra autorità amministrativa non possono procedere ad alcuna forma diretta o indiretta di registrazione, neanche mediante rilascio di tessere sanitarie, di donne che esercitano o siano sospettate di esercitare la prostituzione, né obbligarle a presentarsi periodicamente nei loro uffici" e che ci sono leggi di tutela della privacy (675/96) che vietano schedature di persone in base al loro comportamento sessuale. Inoltre con questa operazione vi è una evidente violazione dell’art. 8 della Convenzione europea dei Diritti dell’Uomo determinata dalla violazione del diritto al rispetto della vita privata delle prostitute censite/schedate.

L’Associazione Radicale Certi Diritti segnala ai Carabinieri di Bologna che la prostituzione in sé non è un reato e che semmai occorre sollecitare le forze politiche ad avviare politiche finalizzate alla legalizzazione e regolamentazione, come avviene in molti paesi europei, visto che il fenomeno è diffuso in tutta Italia e ogni politica repressiva del fenomeno non è servita a nulla.

I Parlamentari Radicali, prime firmatarie Rita Bernardini, alla Camera dei deputati e Donatella Poretti, al Senato, hanno oggi depositato il testo di una Interrogazione urgente a risposta scritta ai Ministri degli Interni, della Giustizia e della Difesa,  sulla incredibile vicenda.

**Qui di seguito il testo integrale dell'interrogiazione e il modulo dei Carabinieri utilizzato per il censimento/schedatura:**

Interrogazione urgente a risposta scritta

Al Ministro degli Interni

Al Ministro della Difesa

Al Ministro della Giustizia

Secondo notizie di stampa (Corriere di Bologna 11 gennaio 2012) i Carabinieri di Bologna, in tre mesi di controlli, con il dispiegamento di 253 uomini, divisi in 72 posti di controllo, hanno realizzato una mappa con un censimento del fenomeno della prostituzione a Bologna identificando 248 ragazze, facendo loro compilare un modulo (allegato);

Secondo i dati raccolti dai Carabinieri, il 98% delle prostitute sono di nazionalità rumena, l’1,8% russe, l’1,6% moldave e uruguaiane. L’età media si aggira intorno ai 26 anni;

Secondo quanto riferito nell’articolo l’Arma dei Carabinieri chiederà all'Agenzia delle Entrate di fare ulteriori verifiche e, in caso, di sottoporre quei patrimoni a una qualche forma di tassazione, come sarebbe possibile per qualsiasi attività di lavoro autonomo; Inoltre, i dati raccolti dai carabinieri in strada sono al vaglio della seconda sezione Misure di prevenzione dell'Arma, un gruppo di specialisti che applica le misure restrittive sui patrimoni di provenienza illecita, ma che si sta occupando anche di spulciare stili di vita e beni di proprietà delle lucciole per segnalare al Fisco le incongruenze rispetto al nulla dichiarato.

Il comandante provinciale dell'Arma, il colonnello Alfonso Manzo, secondo quanto riportato dalla Cronaca del Corriere di Bologna l’11 gennaio 2011, dice che si tratta di un'iniziativa avviata lo scorso settembre e finalizzata soprattutto a tutelare le donne che sono sulla strada e che  ''tutto e' fatto nel rispetto della legge'' e che si tratta di ''un modulo per capire chi sono le prostitute, in che condizioni vivono, se pagano affitti regolari”. A tal proposito il procuratore aggiunto Valter Giovannini ha spiegato che ''si tratta di assunzioni di informazioni con le quali i carabinieri possono trarre anche spunti investigativi” e che “se comunque qualche ragazza dovesse sentirsi schedata e pensasse di presentare un esposto, la Procura lo esaminerebbe”.

Secondo quanto dichiarato dalla fondatrice del Comitato per i diritti civili delle prostitute, “quello che stanno facendo a Bologna è un abuso, il censimento sulle prostitute portato avanti dai carabinieri in questi mesi è un’aperta violazione delle legge Merlin”; la legge Merlin all’art. 7 recita: "Le autorità di pubblica sicurezza, sanitarie e qualsiasi altra autorità amministrativa non possono procedere ad alcuna forma diretta o indiretta di registrazione, neanche mediante rilascio di tessere sanitarie, di donne che esercitano o siano sospettate di esercitare la prostituzione, né obbligarle a presentarsi periodicamente nei loro uffici".

Per sapere – premesso che:

-        in base a quali criteri di prevenzione e lotta al crimine si autorizza il dispiegamento di un numero così elevato di militari nella città di Bologna per un ‘censimento’ e/o  in città del fenomeno della prostituzione motivandolo con il fatto che "i carabinieri possono trarre anche spunti investigativi";

-        se non ritengano che la mancanza di diritti legati alla professione della prostituzione, da quelli sanitari a quelli previdenziali, impedisca a coloro che si prostituiscono di poter regolarmente pagare le tasse e che sia quindi in netto contrasto la decisione di trasmettere le informazioni raccolte all'Ufficio delle Entrate  per fare ulteriori verifiche delle prostitute censite/schedate;

-        se non ritengano che tale azione svolta dall’Arma dei Carabinieri a Bologna sia in netto contrasto con quanto previsto dall’art. 8 della Convenzione europea dei Diritti dell’Uomo vista la violazione del diritto al rispetto della vita privata delle prostitute censite/schedate e dalla Legge sulla privacy (n. 675/1996, Tutela delle persone e di altri soggetti rispetto al trattamento dei dati personali);

-        se non ritengano che in base al form allegato dell’Arma dei Carabinieri, utilizzato per il censimento/schedatura delle 248 prostitute, non si ravvedano gli estremi di una vera e propria schedatura riguardante un reato che non esiste nel codice penale e che ciò sia in aperta violazione dell’art. 7 della Legge Merlin;  
  
  
  
  

**LEGIONE CARABINIERI EMILIA ROMAGNA**

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

   
OGGETTO:  Annotazione di servizio relativa alle attività d’indagine volte al contrasto del fenomeno della prostituzione su strada.-------------\\\

=======================================================================

In data ____________ alle ore ________, in Bologna ___________________________________, i sottoscritti UffAg di P.G. __________________________________________________________, appartenenti al reparto in intestazione, da atto a chi di dovere che alle ore _______ del__________:-in via ________________________________ hanno proceduto al controllo della sottonotata persona esercitante l’attività del meretricio su strada: ------\\\

COGNOME E NOME:_____________________________________________________________

DATA E LUOGO DI NASCITA:_____________________________________________________

RESIDENZA (anche all’estero):______________________________________________________

DOMICILIO_____________________________________________________________________

RECAPITI TELEFONICI___________________________________________________________

ESTREMI DEL DOCUMENTO D’IDENTITÀ:________________________________________

La stessa nel corso del controllo, riferiva quanto segue:----------------------\

SI TROVA IN ITALIA DA:_________________________________________________________

SVOLGE L’ATTIVITA’ DI MERETRICE DA:_________________________________________

GUADAGNO MEDIO GIORNALIERO:\_\_\_\_\_\_\_\_COMPENSO MEDIO PRESTAZIONE\_\_\_\_\_\_\_

CANONE LOCAZIONE (o costi pernottamento se alloggiata in albergo o altro):_______________

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

DICHIARAZIONE IN MERITO ALLA PRESENZA DI SFRUTTATORI:__________________

ALTRE ATTIVITA’ LAVORATIVE SVOLTE (precisandone luogo eventuale datore di lavoro e i riferimenti in genere):______________________________________________________________

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

ACCERTAMENTI DOMICILIARI (verifica veridicità delle dichiarazioni, individuazione persone coabitanti, intestatari contratti locazione se esistenti, intestatari utenze ecc. dovrà precisarsi chi esegue gli accertamenti che sottoscriverà le attività svolte)____________________________________________________________________________

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

NOTE:_______________________________________________________________________________ ________________________________________________________________________________________________________________________________________________________________________

F.l.c.s. in data e luogo di cui sopra.---\