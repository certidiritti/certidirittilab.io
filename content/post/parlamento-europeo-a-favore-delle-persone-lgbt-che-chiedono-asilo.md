---
title: 'Parlamento europeo a favore delle persone lgbt che chiedono asilo'
date: Thu, 07 Apr 2011 08:39:22 +0000
draft: false
tags: [Europa]
---

La relazione del PE introduce numerose proposte per garantire le persone LGBT che fuggono dai loro paesi di origine perché perseguitati per orientamento sessuale o identità di genere. Il Consiglio europeo accolga la richiesta del Parlamento.

Bruxelles – Roma, 7 aprile 2011  
Comunicato Stampa dell’Associazione Radicale Certi Diritti

Il Parlamento Europeo  ha approvato ieri con una maggioranza di centro-sinistra (e per soli 8 voti di differenza: 314 favorevoli, 306 contrari e 48 astenuti) la Relazione Guillaume che emenda la proposta di Direttiva della Commissione europea sulle "Norme minime per le procedure applicate negli Stati membri ai fini del riconoscimento e della revoca della protezione internazionale".  
  
Oltre a rafforzare in generale le garanzie per i richiedenti asilo, la relazione del PE introduce numerose proposte per garantire le persone LGBT che fuggono dai loro paesi di origine perché perseguitati per orientamento sessuale o identità di genere. In particolare, si prevede che tali persone siano incluse nella categoria di "richiedenti asilo con bisogni speciali" più vulnerabili e quindi bisognosi di particolare sostegno, attenzione e protezione.

La formazione del personale, l'assistenza di esperti specializzati, la garanzia della riservatezza delle informazioni rispetto alla famiglia, per il  rispetto della dignità umana e dell'integrità in occasione degli esami fisici e i tempi di preparazione per le interviste, devono tenere conto in modo appropriato della situazione delle persone LGBT.

Col questo voto il  Parlamento Europeo ha chiuso la sua prima lettura della proposta di Direttiva, che é rimasta fino ad oggi bloccata in Consiglio per l'opposizione di alcuni Stati membri. La Commissione europea ha annunciato oggi che depositerà emendamenti di compromesso per cercare di avvicinare le posizioni del PE e del Consiglio Europeo per arrivare all'approvazione di una proposta ambiziosa il più presto possibile. L’Associazione Radicale Certi Diritti chiede al Consiglio di velocizzare l’approvazione di questo documento vista anche la grave situazione umanitaria di queste settimane.