---
title: 'Gay e nazismo: Giovanardi ha perso un''altra occasione per stare zitto, evidentemente ha problemi irrisolti'
date: Thu, 26 Apr 2012 12:44:23 +0000
draft: false
tags: [Politica]
---

Sterminio gay nei campi di concentramento: almeno in questa occasione Giovanardi poteva stare zitto invece di farci sapere la sua patetica e amena riflessione.

Roma, 26 aprile 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Non esiste in Italia un esponente politico come Giovanardi che un giorno si e l’altro pure parla di gay. E’ proprio un pallino che non gli esce dalla testa. Abbiamo già detto e lo ripetiamo che Freud avrebbe di che lavorare sul suo caso umano. Non sappiamo nemmeno se questo è determinato dal fatto che quando era piccolo le suore lo costringevano a vestirsi da bambina, come raccontò il suo fratello gemello Daniele Giovanardi lo scorso 15 febbraio. Sarebbe forse ora che risolvesse con la chiesa questo suo problema che evidentemente lo affligge assai.

Quello che poteva fare era, almeno in questa occasione, stare zitto. La morte di migliaia di persone omosessuali nei campi di concentramento nazisti non può valere di meno o di più di quella di milioni di ebrei, zingari,  vagabondi, mendicanti, prostitute, bambini, malati di mente, testimoni di geova, e molti altri esseri umani.  L’olocausto ha riguardato tutti gli esseri umani che ne sono stati vittime. Dire poi che “il movimento nazista era largamente rappresentato dai gay perché c’erano gay nazisti ai vertici del partito” è un’altra amenità che qualifica il personaggio politico. Vero è che gli omosessuali sono ovunque, anche nel Pdl e in Vaticano ma questo non vuole dire che in quei vertici vi è una rappresentanza visto che sono tutti velati e repressi!

Dire che l’olocausto ha riguardato solo gli ebrei è un falso storico.  Giovanardi sa bene che  il termine viene anche riferito per estensione a tutte quelle persone, gruppi etnici e religiosi ritenuti "indesiderabili" dalla dottrina nazista e di cui era quindi previsto ugualmente il totale annientamento: popolazioni ritenute "inferiori" delle regioni orientali europee occupate (secondo i progetti del Generalplan Ost), prigionieri di guerra sovietici, oppositori politici, Rom, Sinti, Jenisch, testimoni di Geova, pentecostali, omosessuali, malati di mente, portatori di handicap.

Noi di fronte a questa ennesima esternazione non possiamo che dire a Giovanardi: facevi meglio a stare zitto, almeno su questo.