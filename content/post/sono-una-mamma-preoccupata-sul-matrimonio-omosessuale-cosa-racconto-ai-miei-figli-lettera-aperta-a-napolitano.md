---
title: 'Sono una mamma preoccupata: sul matrimonio omosessuale cosa racconto ai miei figli? Lettera aperta a Napolitano'
date: Fri, 23 Mar 2012 13:23:31 +0000
draft: false
tags: [Matrimonio egualitario]
---

Caro Presidente,  
ho sentito dire alla televisione che il matrimonio civile è un contratto tra i due coniugi e lo stato, in cui la coppia si impegna a contribuire al futuro del paese. E che gli omosessuali non hanno diritto a sposarsi perché non possono fare figli, e quindi non contribuiscono al futuro del paese. Ne parlavano ieri in tarda serata alla trasmissione Matrix.

Caro Presidente,  
io sono una donna che lavora da circa 20 anni, ho 46 anni e ho costruito insieme a un’altra donna una famiglia: abbiamo 4 figli.  
Viviamo a Milano, nove anni fa ho aperto una piccola società di allestimenti scenografici per la moda, Meri ha perso il lavoro tre anni fa quando è nato il nostro quarto figlio e l’anno scorso ha aperto una piccolissima casa editrice per bambini: ogni anno versiamo allo stato il 50% di tutto quello che guadagniamo.  
Abbiamo entrambe la tessera del tram e della bicicletta dell’ATM.

Meri una volta alla settimana va in mensa scolastica a controllare il cibo in rappresentanza dei genitori, ed è eletta nel consiglio di scuola della scuola materna.  
Abbiamo una figlia in quarta elementare, due bimbi alla materna e l’ultimo al nido pubblico di zona: per tutti i nostri figli abbiamo sempre pagato la retta più alta per il nido.  
Diamo lavoro da 5 anni a una ragazza che ci aiuta in casa e a cui abbiamo fatto ottenere il permesso di soggiorno: ogni mese paghiamo per lei i contributi.

Questa mattina mi sono alzata e mentre Meri preparava la colazione, al tavolo con tutti i nostri bambini, ho cominciato a scriverle questa lettera. Volevo raccontarle la nostra vita, nella quale tutto il nostro tempo è dedicato ai nostri figli e al loro futuro.  
Non perché siamo un caso eccezionale, ma perché siamo genitori proprio come tutti gli altri.  
Mi domando perché gli omosessuali dovrebbero avere una natura diversa da quella umana: non siamo né meglio  né peggio di tutti gli altri italiani, abbiamo gli stessi desideri e le stesse paure: non crede che dovremmo avere anche gli stessi diritti e gli stessi doveri?  
Abbiamo concepito i nostri figli in un altro paese con l’inseminazione eterologa, ma abbiamo costruito la nostra vita qui perché siamo italiane. Crediamo nelle istituzioni, nella giustizia, soprattutto nella scuola.  
Ogni giorno cerchiamo di spiegare loro cosa è giusto e cosa sbagliato, ma ogni volta che dobbiamo rispondere alla domanda dei bambini  “ma perché voi non potete sposarvi?” è davvero difficile trovare una risposta.

Mi dica , Sig. Presidente, cosa devo rispondere ai miei figli? Pensa anche lei che gli omosessuali non contribuiscono al futuro del nostro paese?  
Con il cuore pieno di speranza per il futuro,

La saluto cordialmente  
Francesca