---
title: 'L''associazione'
date: Mon, 02 Dec 2013 19:53:22 +0000
draft: false
---

[![Logo dell'Associazione](http://www.certidiritti.org/wp-content/uploads/2013/12/logoCD-263x300.jpg)](http://www.certidiritti.org/wp-content/uploads/2013/12/logoCD.jpg)L’associazione radicale **Certi Diritti è nata nel 2008** ed è centro di iniziativa politica nonviolenta, giuridica e di studio per la promozione e la tutela dei **diritti civili inerenti le libertà e responsabilità sessuali**, nonché i **diritti di uguaglianza delle persone LGBTI**. Dal 2012 è uno dei soggetti costituenti del _Partito Radicale Nonviolento Transnazionale e Transpartito_.

Certi Diritti nasce da militanti radicali - tra cui Sergio Rovasio ed Enzo Cucco - con un gruppo di avvocati, tra i quali alcuni dei fondatori di _Rete Lenford – Avvocatura per i diritti Lgbt_. Si ispira alle **lotte nonviolente di Rosa Parks e Harvey Milk, nonché alla storia del _Fuori_**, la prima associazione omosessuale italiana, fondata da Angelo Pezzana nel 1971 e successivamente federata al _Partito Radicale_.

**L’attività dell’associazione è dedicata a Makwan Moloudzadeh e David Kato Kisule.** Makwan era un giovane iraniano accusato a 13 anni di omosessualità. Nel 2007, a 20 anni, è stato impiccato dal regime iraniano. David Kato era invece un attivista quarantenne, tra i padri del movimento LGBT in Uganda. Nel 2010 denunciò una rivista che aveva pubblicato il suo nome e fotografia identificandolo come omosessuale e chiedendone l'assassinio. Quell’anno, a novembre, partecipò al IV Congresso di Certi Diritti. Tornato in Uganda, all’inizio del 2011, venne ucciso in casa con colpi di martello. Makwan e Davide Kato sono il simbolo delle **persone che nel mondo, per via del loro orientamento, ancora subiscono violenze, carcere e torture, fino alla pena di morte**.

L'associazione promuove la **campagna "Affermazione civile"**, nel filone di **ricorsi nazionali e sovranazionali per l’acquisizione dei diritti delle persone LGBTI**. Grazie a questa campagna, Certi Diritti ha ottenuto in Corte costituzionale la **storica sentenza 138/2010**, con la quale la Consulta riconobbe la rilevanza costituzionale della coppia omosessuale.

Certi Diritti è **attiva sul piano transanazionale**, con particolare attenzione agli stati europei, alla Russia, l’Africa e il Medio Oriente. Nell’ambito di queste attività, **Yuri Guaiana nel maggio 2017 è stato arrestato a Mosca** mentre stava cercando di consegnare oltre due milioni di firme per chiedere un'indagine sulle condizioni degli omosessuali in Cecenia.

Su _Radio Radicale_ l’associazione, attraverso la voce di Leonardo Monaco, ogni martedì alle 23.30 cura **_Fuor di pagina_, una rassegna stampa interamente dedicata alle libertà sessuali**.

L’attività è svolta da volontari. Le risorse economiche sono quelle degli iscritti e dei sostenitori.

[**CONGRESSI NAZIONALI**](http://www.certidiritti.org/congressi/)
-----------------------------------------------------------------