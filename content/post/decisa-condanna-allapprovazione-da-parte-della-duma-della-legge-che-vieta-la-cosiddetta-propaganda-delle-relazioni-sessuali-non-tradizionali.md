---
title: 'Decisa condanna all''approvazione da parte della Duma della legge che vieta la cosiddetta “propaganda” delle “relazioni sessuali non tradizionali”'
date: Tue, 11 Jun 2013 20:03:22 +0000
draft: false
tags: [Russia]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti

Roma, 11 giugno 2013

[**LA LETTERA DI CERTI DIRITTI AL MIN. BONINO**](documenti/download/doc_download/82-lettera-a-min-bonino-su-legge-contro-propaganda-omosessuale-in-russia)

**[LA RISPOSTA DEL MIN. BONINO](documenti/download/doc_download/83-risposta-min-bonino-su-legge-propaganda-omosessuale-in-russia)**

L'Associazione Radicale Certi Diritti condanna nel modo più fermo e deciso la legge federale approvata oggi dalla Duma di Stato, il Parlamento della Federazione Russa, che vieta la cosiddetta "propaganda" delle "relazioni sessuali non tradizionali", estendendo il divieto già adottato in altre regioni quali quella di San Pietroburgo, Ryazan, Arkhangelsk e Kostroma. Tali leggi sono già state condannate da molte istituzioni internazionali ed europee, come il Consiglio d'Europa e il Parlamento europeo, nonché da tutte le organizzazioni non governative che si occupano di diritti umani. Dopo il voto odierno la proposta di legge dovrà essere vagliata dalla Camera Alta e poi promulgata dal Presidente. Se ciò avverrà, la Russia violerà definitivamente gl'impegni presi con la comunità internazionale per il rispetto dei diritti umani e si chiama fuori dalla comunità degli stati democratici.

L'Associazione Radicale Certi Diritti ha scritto alla ministra degli Affari Esteri, Emma Bonino, e al ministro per gli Affari Europei, Enzo Moavero Milanesi, affinché si muovano nelle sedi opportune e di concerto con i Ministri degli Esteri degli Stati membri dell'Unione Europea e del Consiglio d'Europa affinché si prendano azioni concrete contro la Russia, per sventare l'approvazione definitiva della legge e per il rispetto dei trattati internazionali sottoscritti dalla Federazione.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: ≪Il voto di oggi rappresenta l'ennesima violazione dei diritti umani da parte della Federazione Russa. Il divieto è una gravissima lesione del diritto alla libertà d'espressione e di manifestazione di tutti i cittadini russi, e non solo dei cittadini LGBTI, poiché permette la sospensione di tutte le attività dell'associazione colpevole e di levare sanzioni fino a un milione di rubli. In pericolo sono anche i diritti e le libertà dei cittadini di altri Paesi, i quali rischiano l'arresto fino a 15 giorni e l'espulsione dalla Federazione. In un sol colpo la Russia viola la Convenzione europea per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali, che aveva sottoscritto entrando a far parte del Consiglio d'Europa, e la sua stessa Costituzione che, all'art. 17, riconosce e garantisce "i diritti e le libertà della persona e del cittadino in accordo con i principi universalmente riconosciuti e con le norme del diritto internazionale". La legge include molti termini ambigui, come "atteggiamenti sessuali non tradizionali" e "immagini distorte di eguaglianza tra relazioni sessuali tradizionali e non tradizionali", che rendono la sua applicazione particolarmente arbitraria. Il voto odierno segue l'adozione delle leggi sui cosiddetti agenti stranieri che impongon a tutte le ONG finanziate da sostenitori non russi di registrarsi come "agenti stranieri", che in Russia è sinonimo di spia o traditore. La deriva autoritaria del Paese euro-asiatico è evidente e molto preoccupante≫.