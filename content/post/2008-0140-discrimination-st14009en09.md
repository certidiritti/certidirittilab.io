---
title: '2008-0140-Discrimination-ST14009.EN09'
date: Wed, 14 Oct 2009 12:11:18 +0000
draft: false
tags: [Senza categoria]
---

  

  

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 14 October 2009

14009/09

SOC 567

JAI 645

MI 362

  

  

  

  

  

NOTE

from :

The Presidency

to :

The Working Party on Social Questions

on :

22 October 2009

No. prev. doc. :

13049/09 SOC 504 JAI 564 MI 326

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached a set of Presidency drafting suggestions concerning the above proposal. This document is based on a consolidated text incorporating the most recent versions of different elements of the draft Directive, as found in docs. 13238/09[\[1\]](#_ftn1), 10072/09[\[2\]](#_ftn2) and 16594/08 ADD 1[\[3\]](#_ftn3).

Changes in relation to the earlier versions are indicated as follows: additions are in **bold** and deletions are marked **\[…\]**.

  

The changes mainly focus on the disability provisions (Articles 4, 4a, 4b, 7 and 15). The Presidency is aware of the need for further adjustments to the corresponding recitals.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

Proposal for a

COUNCIL DIRECTIVE

on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

THE COUNCIL OF THE EUROPEAN UNION,

Having regard to the Treaty establishing the European Community, and in particular Article 13(1) thereof,

Having regard to the proposal from the Commission[\[4\]](#_ftn4),

Having regard to the opinion of the European Parliament[\[5\]](#_ftn5),

Whereas:

  

(1)          In accordance with Article 6 of the Treaty on European Union, the European Union is founded on the principles of liberty, democracy, respect for human rights and fundamental freedoms, and the rule of law, principles which are common to all Member States and it respects fundamental rights, as guaranteed by the European Convention for the Protection of Human Rights and Fundamental Freedoms and as they result from the constitutional traditions common to the Member States, as general principles of Community law.

(2)          The right to equality before the law and protection against discrimination for all persons constitutes a universal right recognised by the Universal Declaration of Human Rights, the United Nations Convention on the Elimination of all forms of Discrimination Against Women, the International Convention on the Elimination of all forms of Racial Discrimination, the United Nations Covenants on Civil and Political Rights and on Economic, Social and Cultural Rights, the UN Convention on the Rights of Persons with Disabilities, the European Convention for the Protection of Human Rights and Fundamental Freedoms and the European Social Charter, to which \[all\] Member States are signatories. In particular, the UN Convention on the Rights of Persons with Disabilities includes the denial of reasonable accommodation in its definition of discrimination.

(3)          This Directive respects the fundamental rights and observes the fundamental principles recognised in particular by the Charter of Fundamental Rights of the European Union. Article 10 of the Charter recognises the right to freedom of thought, conscience and religion; Article 21 prohibits discrimination, including on grounds of religion or belief, disability, age or sexual orientation; and Article 26 acknowledges the right of persons with disabilities to benefit from measures designed to ensure their independence.

(4)          The European Years of Persons with Disabilities in 2003, of Equal Opportunities for All in 2007, and of Intercultural Dialogue in 2008 have highlighted the persistence of discrimination but also the benefits of diversity.

  

(5)          The European Council, in Brussels on 14 December 2007, invited Member States to strengthen efforts to prevent and combat discrimination inside and outside the labour market[\[6\]](#_ftn6).

(6)          The European Parliament has called for the extension of the protection of discrimination in European Union law[\[7\]](#_ftn7).

(7)          The European Commission has affirmed in its Communication ‘Renewed social agenda: Opportunities, access and solidarity in 21st century Europe’[\[8\]](#_ftn8) that, in societies where each individual is regarded as being of equal worth, no artificial barriers or discrimination of any kind should hold people back in exploiting these opportunities. Discrimination based on religion or belief, disability, age or sexual orientation may undermine the achievement of the objectives of the EC Treaty, in particular the attainment of a high level of employment and of social protection, the raising of the standard of living, and quality of life, economic and social cohesion and solidarity. It may also undermine the objective of abolishing of obstacles to the free movement of persons, goods and services between Member States.

(8)          The Community has adopted three legal instruments[\[9\]](#_ftn9) on the basis of article 13(1) of the EC Treaty to prevent and combat discrimination on grounds of sex, racial and ethnic origin, religion or belief, disability, age and sexual orientation. These instruments have demonstrated the value of legislation in the fight against discrimination_._ In particular, Directive 2000/78/EC establishes a general framework for equal treatment in employment and occupation on the grounds of religion or belief, disability, age and sexual orientation. However, variations remain between Member States on the degree and the form of protection from discrimination on these grounds beyond the areas of employment.

  

(9)     Therefore, legislation should prohibit discrimination based on religion or belief, disability, age or sexual orientation in a range of areas outside the labour market, including social protection, education and access to and supply of goods and services, including housing. Services should be taken to be those within the meaning of Article 50 of the EC Treaty.

(10)   Directive 2000/78/EC prohibits discrimination in access to vocational training; it is necessary to complete this protection by extending the prohibition of discrimination to education which is not considered vocational training.

(11)     Deleted.

(12)   Discrimination is understood to include direct and indirect discrimination, harassment, instructions to discriminate and denial of reasonable accommodation.

(12a) (new)  In accordance with the judgment of the Court of Justice in Case C-303/06[\[10\]](#_ftn10), it is appropriate to provide explicitly for protection from discrimination by association on all grounds covered by this Directive. Such discrimination occurs, _inter alia_, when a person is treated less favourably, or harassed, because, in the view of the discriminator, he or she is associated with persons of a particular religion or belief, disability, age or sexual orientation, for instance through his or her family, friendships, employment or occupation**.** Moreover, discrimination within the meaning of this Directive also includes direct discrimination or harassment based on assumptions about a person's religion or belief, disability, age or sexual orientation.

(12b) (new) Harassment is contrary to the principle of equal treatment, since victims of harassment cannot enjoy access to social protection, education and goods and services on an equal basis with others. Harassment can take different forms, including unwanted verbal, physical, or other non-verbal conduct. Such conduct may be deemed harassment in the meaning of this

  

Directive when it is either repeated or otherwise so serious in nature that it has the purpose or effect of violating the dignity of a person and of creating an intimidating, hostile, degrading, humiliating or offensive environment. In this context, the mere expression of a personal opinion or the display of religious symbols or messages are presumed as not constituting harassment[\[11\]](#_ftn11).

(13)   In implementing the principle of equal treatment irrespective of religion or belief, disability, age or sexual orientation, the Community should, in accordance with Article 3(2) of the EC Treaty, aim to eliminate inequalities, and to promote equality between men and women, especially since women are often the victims of multiple discrimination.

(14)   The appreciation of the facts from which it may be presumed that there has been direct or indirect discrimination should remain a matter for the national judicial or other competent bodies in accordance with rules of national law or practice. Such rules may provide, in particular, for indirect discrimination to be established by any means including on the basis of statistical evidence.

(14a) Differences in treatment in connection with age may be permitted under certain circumstances if they are objectively justified by a legitimate aim and the means of achieving that aim are appropriate and necessary. Such differences of treatment may include, for example, special age conditions regarding access to certain goods or services such as alcoholic drinks, arms, or driving licences.

(15)   Actuarial and risk factors related to disability and to age are used in the provision of insurance, banking and other financial services. These should not be regarded as constituting discrimination where service providers have shown, by relevant actuarial principles, accurate statistical data or medical knowledge, that  such factors are determining factors for the assessment of risk.

  

(16)     Deleted.

(17)   While prohibiting discrimination, it is important to respect other fundamental rights and freedoms, including the protection of private and family life and transactions carried out in that context, the freedom of religion, the freedom of association, the freedom of expression and the freedom of the press.

(17b) (new)  This Directive covers access to social protection, which includes social security, social assistance, and health care, thereby providing comprehensive protection against discrimination in this field. Consequently, the Directive applies with regard to access to rights and benefits which are derived from general or special social security,  social assistance and healthcare schemes, which are provided either directly by the State, or by private parties in so far as the provision of those benefits by the latter is funded by the State. In this context, the Directive applies with regard to benefits  in cash, benefits in kind and services, irrespective of whether the schemes involved are contributory or non-contributory. The abovementioned schemes include, for example, access to the branches of social security defined by Regulation 883/2004/EC on the coordination of social security systems[\[12\]](#_ftn12), as well as schemes providing for benefits or services granted for reasons related to the lack of financial resources or risk of social exclusion.

(17c) Deleted.

(17d) (new) All individuals enjoy the freedom to contract, including the freedom to choose a contractual partner for a transaction. This Directive should not apply to economic transactions undertaken by individuals for whom these transactions do not constitute a professional or commercial activity. In this context, the concept of professional or commercial activity may be defined in accordance with the national laws and practice of the Member States.

  

(17e) (new) This directive does not alter the division of competences between the European Community and the Member States in the areas of education and social protection, including social security, social assistance and health care. It is also without prejudice to the essential role and wide discretion of the Member States in providing, commissioning and organising services of general economic interest.

(17f)  (new) The exclusive competence of Member States with regard to the organisation of their social protection systems includes decisions on the setting up, financing and management of such systems and related institutions as well as on the substance and delivery  of benefits and health services and the conditions of eligibility. In particular Member States retain the possibility to reserve certain benefits or services to certain age groups or persons with disabilities. Moreover, this Directive is without prejudice to the powers of the Member States to organise their social protection systems in such a way as to guarantee their sustainability.

(17g) (new) The exclusive competence of Member States with regard to the content of teaching or activities and the organisation of national educational systems, including the provision of special needs education,  includes the setting up and management of educational institutions, the development of curricula and other educational activities and the definition of examination processes. In particular Member States retain the possibility to set age limits for certain education activities. However, there may be no discrimination in the access to educational activities, including the admission to and participation in classes or programmes and the evaluation of students' performance.

(17h) (new) This Directive does not apply to matters covered by family law including marital status and adoption, and laws on reproductive rights. It is also without prejudice to the secular nature of the State, state institutions or bodies, or education.

(18)     Deleted.

(19a) Persons with disabilities include those who have long‑term physical, mental, intellectual or sensory impairments which, in interaction with various barriers, may hinder their full and effective participation in society on an equal basis with others.

  

(19b) (new) Measures to enable persons with disabilities to have access, on an equal basis with others, to the areas covered by this Directive play an important part in ensuring full equality in practice. Such measures shall include appropriate modifications or adjustments and the identification and elimination of obstacles and barriers to accessibility. An example of modifications and adjustments which may amount to a disproportionate burden could be where significant structural changes are required to buildings or infrastructure which are protected under national rules on account of their historical, cultural or architectural value. In addition, individual measures of reasonable accommodation may be required in some cases to ensure access. In neither case are measures required that would impose a disproportionate burden. In assessing whether the burden is disproportionate, account should be taken of a number of factors including the size, resources and nature of the organisation. The principles of reasonable accommodation and disproportionate burden are established in Directive 2000/78/EC and the UN Convention on Rights of Persons with Disabilities.

(19c) (new) Measures to ensure access to persons with disabilities, on an equal basis with others, should not impose a disproportionate burden, nor require fundamental alteration. An alteration should be considered fundamental if it would change the nature of the social protection, social advantage, education, good, or service to such an extent that a different kind of social protection, social advantage, education, good or service was effectively provided or that access for persons without a disability was reduced.

(19d) (new) Improvement of access can be provided by a variety of means, including application of the "universal design" principle. “Universal design” means the design of products, environments, programmes and services to be usable by all people, to the greatest possible extent, without the need for adaptation or specialised design. “Universal design” should not exclude assistive devices for particular groups of persons with disabilities where this is needed.

  

(20)   Legal requirements[\[13\]](#_ftn13) and standards on accessibility have been established at European level in some areas while Article 16 of Council Regulation 1083/2006 of 11 July 2006 on the European Regional Development Fund, the European Social Fund and the Cohesion Fund and repealing Regulation (EC) No 1260/1999[\[14\]](#_ftn14) requires that accessibility for disabled persons is one of the criteria to be observed in defining operations co-financed by the Funds. The Council has also emphasised the need for measures to secure the accessibility of cultural infrastructure and cultural activities for people with disabilities[\[15\]](#_ftn15).

(21)   The prohibition of discrimination should be without prejudice to the maintenance or adoption by Member States of measures intended to prevent or compensate for disadvantages suffered by a group of persons of a particular religion or belief, disability, age or sexual orientation. Such measures may permit organisations of persons of a particular religion or belief, disability, age or sexual orientation where their main object is the promotion of the special needs of those persons.

(22)   This Directive lays down minimum requirements, thus giving the Member States the option of introducing or maintaining more favourable provisions. The implementation of this Directive should not serve to justify any regression in relation to the situation which already prevails in each Member State.

(23)   Persons who have been subject to discrimination based on religion or belief, disability, age or sexual orientation should have adequate means of legal protection. To provide a more effective level of protection, associations, organisations and other legal entities should be empowered to engage in proceedings, including on behalf of or in support of any victim, without prejudice to national rules of procedure concerning representation and defence before the courts.

  

(24)   The rules on the burden of proof must be adapted when there is a prima facie case of discrimination and, for the principle of equal treatment to be applied effectively, the burden of proof must shift back to the respondent when evidence of such discrimination is brought. However, it is not for the respondent to prove that the plaintiff adheres to a particular religion or belief, has a particular disability, is of a particular age or has a particular sexual orientation.

(25)   The effective implementation of the principle of equal treatment requires adequate judicial protection against victimisation.

(26)   In its resolution on the Follow-up of the European Year of Equal Opportunities for All (2007), the Council called for the full association of civil society, including organisations representing people at risk of discrimination, the social partners and stakeholders in the design of policies and programmes aimed at preventing discrimination and promoting equality and equal opportunities, both at European and national levels.

(27)   Experience in applying Directives 2000/43/EC and 2004/113/EC show that protection from discrimination on the grounds covered by this Directive would be strengthened by the existence of a body or bodies in each Member State, with competence to analyse the problems involved, to study possible solutions and to provide concrete assistance for the victims.

(28)   Deleted.

(29)   Member States should provide for effective, proportionate and dissuasive sanctions in case of breaches of the obligations under this Directive.

(30)   In accordance with the principles of subsidiarity and proportionality as set out in Article 5 of the EC Treaty, the objective of this Directive, namely ensuring a common level of protection against discrimination in all the Member States, cannot be sufficiently achieved by the Member States and can therefore, by reason of the scale and impact of the proposed action, be better achieved by the Community. This Directive does not go beyond what is necessary in order to achieve those objectives.

  

(31)   In accordance with paragraph 34 of the interinstitutional agreement on better law-making, Member States are encouraged to draw up, for themselves and in the interest of the Community, their own tables, which will, as far as possible, illustrate the correlation between the Directive and the transposition measures and to make them public.

Article 1  
Purpose

This Directive lays down a framework for combating discrimination on the grounds of religion or belief, disability, age, or sexual orientation, with a view to putting into effect in the Member States the principle of equal treatment within the scope of Article 3.

Article 2  
Concept of discrimination

1.       For the purposes of this Directive, the “principle of equal treatment” shall mean that  there shall be no direct or indirect discrimination on any of the grounds referred to in Article 1.

2.       For the purposes of paragraph 1, the following definitions apply:

(a)     direct discrimination shall be taken to occur where one person is treated less favourably than another is, has been or would be treated in a comparable situation, on any of the grounds referred to in Article 1;

(b)     indirect discrimination shall be taken to occur where an apparently neutral provision, criterion or practice would put persons of a particular religion or belief, a particular disability, a particular age, or a particular sexual orientation at a particular disadvantage compared with other persons, unless that provision, criterion or practice is objectively justified by a legitimate aim and the means of achieving that aim are appropriate and necessary.

  

3.       Harassment shall be deemed to be a form of discrimination within the meaning of paragraph 1, when unwanted conduct related to any of the grounds referred to in Article 1 takes place with the purpose or effect of violating the dignity of a person and of creating an intimidating, hostile, degrading, humiliating or offensive environment. In this context, the concept of harassment may be defined in accordance with the national laws and practice of the Member States.

3a.     Discrimination includes direct discrimination or harassment due to a person's association with persons of a certain religion or belief, persons with disabilities, persons of a given age or of a certain sexual orientation; or based on assumptions about a person's religion or belief, disability, age or sexual orientation.

4.       An instruction to discriminate against persons on any of the grounds referred to in Article 1 shall be deemed to be discrimination within the meaning of paragraph 1.

5.       Denial of reasonable accommodation in a particular case as provided for by Article 4 **\[…\]** of the present Directive as regards persons with disabilities shall be deemed to be discrimination within the meaning of paragraph 1.

6.       Notwithstanding paragraph 2, differences of treatment on grounds of age shall not constitute discrimination, if they are  objectively justified by a legitimate aim, and if the means of achieving that aim are appropriate and necessary. In this context Member States may specify aims which can be considered to be legitimate.

Such differences of treatment may include the fixing of a specific age for access to social protection, including social security, social assistance and healthcare; education; and certain goods or services which are available to the public.

6a.     (new) Notwithstanding paragraph 2, differences of treatment of persons with a disability shall not constitute discrimination, if they are aimed at protecting their health and safety and if the means of achieving that aim are appropriate and necessary.

  

7.       Notwithstanding paragraph 2, in the provision of financial services, proportionate differences in treatment where, for the service in question, the use of age or disability is a determining factor in the assessment of risk based on relevant actuarial principles, accurate statistical data or  medical knowledge shall not be considered discrimination for the purposes of this Directive.

8.       This Directive shall be without prejudice to measures laid down in national law which, in a democratic society, are necessary for public security, for the maintenance of public order and the prevention of criminal offences, for the protection of health and the protection of the rights and freedoms of others.

Article 3  
Scope

1.       Within the limits of the powers conferred upon the Community, the prohibition of discrimination shall apply to all persons, as regards both the public and private sectors, including public bodies, in relation to access to:

(a)          Social protection, including social security, social assistance and healthcare;

(b)          \[Deleted.\]

(c)          Education;

(d)     and the supply of, goods and other services which are available to the public, including housing.

Subparagraph (d) shall apply to natural persons only insofar as they are performing a professional or commercial activity defined  in accordance with national laws and practice.

2.       This Directive does not alter the division of competences between the European Community and the Member States. In particular it does not apply to:

  

(a)     matters covered by family law, including marital status and adoption, and laws on reproductive rights;

(b)     the organisation of  Member States' social protection    systems, including decisions on the setting up, financing and management of such systems and related institutions as well as on the substance and delivery of benefits and services and the conditions of eligibility;

(c)     the powers of Member States to determine the type of health services provided and the conditions of eligibility; and

(d)     the content of teaching or activities and the organisation of Member States' educational systems, including the provision of special needs education.

3.       Member States may provide that differences of treatment  based on religion or belief in respect of admission to educational institutions, the ethos of which is based on religion or belief, in accordance with national laws, traditions and practice, shall not constitute discrimination.

3a.     This Directive is without prejudice to national measures authorising or prohibiting the wearing of religious symbols.

4.       This Directive is without prejudice to national legislation ensuring the secular nature of the State, State institutions or bodies, or education, or concerning the status and activities of churches and other organisations based on religion or belief.

5.       This Directive does not cover differences of treatment based on nationality and is without prejudice to provisions and conditions relating to the entry into and residence of third-country nationals and stateless persons in the territory of Member States, and to any treatment which arises from the legal status of the third-country nationals and stateless persons concerned.

**_Article 4_**[_\[16\]_](#_ftn16)_  
**Reasonable accommodation for persons with disabilities**_

\[**…**\]

**In order to guarantee compliance with the principle of equal treatment in relation to persons with disabilities, reasonable accommodation shall be provided within the areas set out in Article 3(1), unless this would impose a disproportionate burden.**

**Reasonable accommodation means measures needed in a particular case to meet the specific needs of a person with a disability in order to ensure access on an equal basis with others.**

**Article 4a (new)**[\[17\]](#_ftn17)**Accessibility for** persons with disabilities

**Notwithstanding Article 4,** Member States shall take the necessary and appropriate measures to ensure access**ibility for** persons with disabilities, on an equal basis with others, within the areas set out in Article 3.

Such measures shall include **\[…\]** the identification and elimination of obstacles and barriers to accessibility**, including with regard to the physical environment and to information and communication technology and systems.**

Such measures should not impose a disproportionate burden, nor require fundamental alteration or the provision of alternatives.

**Article 4b (new)**[\[18\]](#_ftn18)**  
Common Provisions Regarding Reasonable Accommodation and Accessibility**

**1.** For the purposes of assessing whether measures necessary to comply with **Articles 4 and 4a** would impose a disproportionate burden, account shall be taken, in particular, of:

**a)** the size and resources of the organisation or enterprise,

**b)** **the** nature **of the organisation or enterprise**,

**c)** the estimated cost, **\[…\] and**

**d)** the possible benefits of increased access for persons with disabilities.

The burden shall not be deemed disproportionate when it is sufficiently remedied by measures existing within the framework of the equal treatment policy of the Member State concerned.

**2**.         Articles **4 and 4a** shall not apply to the design and manufacture of goods.

**3.       Articles 4 and 4a shall not require significant structural changes to buildings or infrastructures which are protected under national rules on account of their historical, cultural or architectural value.**

**4\.** This Directive shall be without prejudice to the provisions of Community law or national rules providing for detailed standards or specifications on the accessibility of particular goods or services, including public transport, as long as such rules do not restrict the application of **Articles 4 and 4a.**

Article 5  
Positive action

1.  With a view to ensuring full equality in practice, the principle of equal treatment shall not prevent any Member State from maintaining or adopting specific measures to prevent or compensate for disadvantages linked to religion or belief, disability, age, or sexual orientation.

2**.** (new) In particular, the principle of equal treatment shall be without prejudice to the right of Member States to maintain or adopt more favourable provisions for persons of a given age or for persons with disabilities as regards conditions for access to social protection, including social security, social assistance and healthcare; education; and certain goods or services which are available to the public, in order to promote their economic, cultural or social integration.

Article 6  
Minimum requirements

1.       Member States may introduce or maintain provisions which are more favourable to the protection of the principle of equal treatment than those laid down in this Directive.

2.       The implementation of this Directive shall under no circumstances constitute grounds for a reduction in the level of protection against discrimination already afforded by Member States in the fields covered by this Directive.

CHAPTER II  
REMEDIES AND ENFORCEMENT

Article 7  
Defence of rights

1.       Member States shall ensure that judicial and/or administrative procedures, including where they deem it appropriate conciliation procedures, for the enforcement of obligations under this Directive are available to all persons who consider themselves wronged by failure to apply the principle of equal treatment to them, even after the relationship in which the discrimination is alleged to have occurred has ended.

2.       Member States shall ensure that associations, organisations or other legal entities, which have, in accordance with the criteria laid down by their national law, a legitimate interest in ensuring that the provisions of this Directive are complied with, may engage, either on behalf or in support of the complainant, with his or her approval, in any judicial and/or administrative procedure provided for the enforcement of obligations under this Directive.

3.       Paragraphs 1 and 2 shall be without prejudice to national rules relating to time limits for bringing actions as regards the principle of equality of treatment.

**4.       (new) Paragraphs 1 and 2 shall not apply to obligations arising from Article 4a. Member States may determine that obligations arising from that provision shall be implemented through administrative or conciliation procedures.**

Article 8  
Burden of proof

1.       Member States shall take such measures as are necessary, in accordance with their national judicial systems, to ensure that, when persons who consider themselves wronged because the principle of equal treatment has not been applied to them establish, before a court or other competent authority, facts from which it may be presumed that there has been direct or indirect discrimination, it shall be for the respondent to prove that there has been no breach of the prohibition of discrimination.

2.       Paragraph 1 shall not prevent Member States from introducing rules of evidence which are more favourable to plaintiffs.

3.         Paragraph 1 shall not apply to criminal procedures.

4.         Member States need not apply paragraph 1 to proceedings in which the court or other competent body investigates the facts of the case.

5.       Paragraphs 1, 2, 3 and 4 shall also apply to any legal proceedings commenced in accordance with Article 7(2).

Article 9  
Victimisation

Member States shall introduce into their national legal systems such measures as are necessary to protect individuals from any adverse treatment or adverse consequence as a reaction to a complaint or to proceedings aimed at enforcing compliance with the principle of equal treatment

Article 10  
Dissemination of information

Member States shall ensure that the provisions adopted pursuant to this Directive, together with the relevant provisions already in force, are brought to the attention of the persons concerned by appropriate means throughout their territory.

Article 11  
Dialogue with relevant stakeholders

With a view to promoting the principle of equal treatment, Member States shall encourage dialogue with relevant stakeholders, which have, in accordance with their national law and practice, a legitimate interest in contributing to the fight against discrimination on the grounds and in the areas covered by this Directive.

Article 12  
Bodies for the Promotion of Equal treatment

1.       Member States shall designate a body or bodies for the promotion of equal treatment of all persons irrespective of their religion or belief, disability, age, or sexual orientation. These bodies may form part of agencies charged at national level with the defence of human rights or the safeguard of individuals' rights.

2.       Member States shall ensure that the competences of these bodies include:

**(a)** without prejudice to the right of victims and of associations, organizations or other legal entities referred to in Article 7(2), providing independent assistance to victims of discrimination in pursuing their complaints about discrimination,

**(b)** conducting independent surveys concerning discrimination, **and**

**(c)** publishing independent reports and making recommendations on any issue relating to such discrimination.

  

CHAPTER III  
FINAL PROVISIONS

Article 13  
Compliance

Member States shall take the necessary measures to ensure that the principle of equal treatment is respected within the scope of this Directive and in particular that:

(a)     any laws, regulations and administrative provisions contrary to the principle of equal treatment are abolished;

(b)     any contractual provisions, internal rules of undertakings, and rules governing profit-making or non-profit-making associations contrary to the principle of equal treatment are, or may be, declared null and void or are amended.

Article 14  
Sanctions

Member States shall lay down the rules on sanctions applicable to infringements of national provisions adopted pursuant to this Directive, and shall take all measures necessary to ensure that they are applied. Sanctions may comprise the payment of compensation, which may not be restricted by the fixing of a prior upper limit, and must be effective, proportionate and dissuasive.

Article 14a (new)  
Gender mainstreaming

In accordance with  the objective of  Article 3(2) of the EC Treaty, Member States shall, when implementing this Directive, take into account the objective of equality between men and women.

Article 15  
Implementation

1.       Member States shall adopt the laws, regulations and administrative provisions necessary to comply with this Directive by …. at the latest \[X years after adoption\]. They shall forthwith inform the Commission thereof and shall communicate to the Commission the text of those provisions.

When Member States adopt these measures, they shall contain a reference to this Directive or be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.

2.       In order to take account of particular conditions, Member States may, if necessary, establish that

**a)      the obligation to provide reasonable accommodation as set out in Article 4 has to be complied with by … at the latest \[X years after adoption\], and**

**b)** the obligation to **\[…\]** **ensur**e **\[…\]** access**ibility** as set out in Article 4**a** has to be complied with by … at the latest \[**10** years after adoption\] **regarding new buildings, facilities and infrastructure and by \[20 years after adoption\] regarding existing buildings, facilities and infrastructure**.

Member States wishing to use **any of** **\[…\] these** additional period**s** shall inform the Commission at the latest by the date set down in paragraph 1 giving reasons**. Member States shall also communicate to the Commission by the same date an action plan laying down the steps to be taken and the timetable for achieving the gradual implementation of Article 4a. They shall report on progress every two years starting from this date.**

Article 16  
Report

1.       Member States shall communicate to the Commission, by …. at the latest and every five years thereafter, all the information necessary for the Commission to draw up a report to the European Parliament and the Council on the application of this Directive.

2.       The Commission's report shall take into account, as appropriate, the viewpoints of national equality bodies and relevant stakeholders, as well as the EU Fundamental Rights Agency. In accordance with the principle of gender mainstreaming, this report shall, _inter alia_, provide an assessment of the impact of the measures taken on women and men. In the light of the information received, this report shall include, if necessary, proposals to revise and update this Directive.

Article 17  
Entry into force

This Directive shall enter into force on the day of its publication in the Official Journal of the European Union.

Article 18  
Addressees

This Directive is addressed to the Member States.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) Recitals 9, 11, 12a, 12b, 14a, 15, 16, 17, 17b, 17c, 17d, 17e, 17f, 17g, 17h, 18 and 28, and Articles 1-3, 5-18 and 14a.

[\[2\]](#_ftnref2) Recitals 19a, b, c and d and Article 4.

[\[3\]](#_ftnref3) The preamble, Recitals 1-8, Recital 10, Recital 12, Recitals 13-14, Recital 19, Recitals 20-27 and Recitals 29-31.

[\[4\]](#_ftnref4) OJ C , , p. .

[\[5\]](#_ftnref5) OJ C , , p. .

[\[6\]](#_ftnref6) Presidency conclusions of the Brussels European Council of 14 December 2007, point 50.

[\[7\]](#_ftnref7) Resolution of 20 May 2008 P6_TA-PROV(2008)0212

[\[8\]](#_ftnref8) COM (2008) 412

[\[9\]](#_ftnref9) Directive 2000/43/EC, Directive 2000/78/EC and Directive 2004/113/EC

[\[10\]](#_ftnref10) Case C-303/06, Coleman v. Attridge, judgment of 17 July 2008, nyr.

[\[11\]](#_ftnref11) Wording taken from Recital 17 (see doc. 12792/09, p. 3).

[\[12\]](#_ftnref12) OJ L 166, 30.4.2004, p. 1.

[\[13\]](#_ftnref13) Regulation (EC) No. 1107/2006 and Regulation (EC) No 1371/2007

[\[14\]](#_ftnref14) OJ L 210, 31.7.2006, p.25. Regulation as last amended by Regulation (EC) No 1989/2006 (OJ L 411, 30.12.2006, p.6).

[\[15\]](#_ftnref15) OJ C 134, 7.6.2003, p.7

[\[16\]](#_ftnref16) In the previous version, "reasonable accommodation" was addressed in Article 4(2). See doc. 10072/09.

[\[17\]](#_ftnref17) Article 4a is based on former Article 4(1).

[\[18\]](#_ftnref18) Article 4b is based on former Article 4(3) and (5).