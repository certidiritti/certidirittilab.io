---
title: '''Verso l''Europride'' due giorni all''Acrobax di Roma'
date: Thu, 19 May 2011 15:05:30 +0000
draft: false
tags: [Europa]
---

**26 e 27 maggio all'acrobax due giorni di arte, cultura, musica e politica. Il programma.**

**GIOVEDI' 26 MAGGI0 2011**

ore 18  
**Presentazione del libro 'QUANDO ERAVAMO FROCI' di Adrea Pini**  
a cura di Andrea Maccarrone

**LO STATO DEL MOVIMENTO GLBTI**  
Intervengono:  
Sergio Rovasio - Certi Diritti  
Andrea Maccarrone - Mario Mieli  
Elena Biagini - Facciamo Breccia  
Collettivo Suigeneris LGBTIQ della Sapienza  
Radio Onda Rossa

ore 21  
**PAUSA WINE BAR**

ore 21.30  
**DOCUMENTARIO**  
a cura di Buzz Intercultura Cineforum  
**'Dangerous Living: coming out in the Developing World' di John Scagliotti**

Alla fine del documentario:  
Breafing di 'Assimilazione e Integrazione: post-film'  
Metodologia GayCounseling in gruppo sul tema del Coming Out  
tenuto da Dr. Maurizio Palomba e Dr. Francesco Bertini

a cura di Acrobax Espo verrà allestita una mostra:  
Alba Montori, Luciano Lucchetti, Giovanni Bastianelli,  
Cristina Costa, Luciano Parisi, Massimo Crisafulli

**VENERDI' 27 MAGGIO 2011**

ore 18  
**POLITICHE DEL CORPO: GENERE, DIFFERENZA, QUEER**  
con la prof.ssa Giardini di Roma 3 e il lab. filosofico Sofiaroney.noblogs.org

E' il piano dell'uso dei corpi, anzitutto quello delle donne, che apre la sequela delle discriminazioni di genere, lesbo, trans, queer, ma anche quello della femminilizzazione del lavoro e delle rivendicazioni emancipazioniste fuori tempo massimo, nell'epoca della recrudescenza dei diktat sui corpi e del moralismo delle 'donne perbene'.  
Con la partecipazione di **Porpora Marcasciano**.

ore 21  
**CENA SOCIALE**  
per finanziare una campagna sul diritto delle diversità dei generi  
a cura di Certi Diritti

ore 22  
**PERFORMING ART NEL MONDO GLBT. OPEN SPACE**

Ciro Cascina e Luciano Parisi  
Reciproche indifferenze ovvero Pissipissibaubau, oggi non posso ci vediamo domani

Serafino Iorli  
Uno un po' così  
di e con Serafino Iorli

**PERFORMING ART MUSIC**  
con Erma Castriota, violino e voce

**NOTTE TRASH DJ SET**  
a cura di Darione di Acrobax

**curatore della due giorni: Luciano Parisi**