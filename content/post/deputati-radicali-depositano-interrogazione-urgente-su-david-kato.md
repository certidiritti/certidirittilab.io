---
title: 'Deputati radicali depositano interrogazione urgente su David Kato'
date: Tue, 01 Feb 2011 11:35:15 +0000
draft: false
tags: [Comunicati stampa, david kato, morte, RADICALI, Uganda]
---

L'interrogazione al governo italiano dei deputati radicali, primo firmatario Matteo Mecacci, sulla morte del nostro iscritto David Kato chiede interventi urgenti nei confronri dell'Uganda.

Roma, 1 febbraio 2011

**Sulla vicenda dell’assassinio di David Kato Kisule, esponente dei diritti civili e umani ugandese, iscritto all’Associazione Radicale Certi Diritti, barbaramente assassinato nella sua abitazione ugandese mercoledì 26 gennaio,  i deputati radicali, primo firmatario Matteo Mecacci, hanno  depositato alla Camera dei deputati una interrogazione urgente al Governo italiano affinchè intervenga nei confronti dell’Uganda  per scongiurare ulteriori atti di violenza contro gli attivisti dei diritti civili. Nell’interrogazione si chiede di intervenire nei diversi ambiti dell’Unione Europea e della Cooperazione internazionale affinchè vengano garantiti nel paese africano i diritti civili e umani delle persone, contro la violenza e l'odio del fondamentalismo religioso.**

**Qui di seguito il testo integrale dell’interrogazione urgente a risposta scritta:**

Interrogazione urgente a risposta scritta

Al Presidente del Consiglio Al Ministro degli Esteri

Per sapere – premesso che:

il 26 gennaio 2011 è stato barbaramente assassinato in Uganda, a Namataba - Mukono District, a circa 15 kilometri da Kampala, David Kato Kisule, esponente africano del movimento dei diritti civili, dirigente dell’Associazione Smug (Sexual Minorities Uganda), iscritto all’Associazione Radicale Certi Diritti;

il 16 ottobre 2010 la rivista ugandese Rolling Stone pubblicò in prima pagina le foto di 100 attivisti omosessuali (o presunti tali) ugandesi chiedendone l’arresto. Tra le 100 foto vi era anche quella di David Kato Kisule, l’esponente più noto del movimento. In Uganda, come in altri paesi africani, il clima di odio contro le persone omosessuali è alimentato dal fondamentalismo religioso dei predicatori evangelisti che trovano terreno molto fertile tra la popolazione che vive nella miseria e nella disperazione. L’Alta Corte ugandese, in un ricorso presentato dagli attivisti dell’Organizzazione Smug contro la rivista Rolling Stone, aveva dato ragione agli attivisti per i diritti delle persone lesbiche e gay condannando il giornale alla chisurua e al risarcimento dei danni causati alle persone omosesssuali;

David Kato Kisule,  lo scorso novembre 2010, grazie ad anche all’intervento dell’Ong Non c’è Pace Senza Giustizia, aveva partecipato a Roma ai lavori del IV Congresso dell’Associazione Radicale Certi Diritti e aveva denunciato le gravi persecuzioni di cui sono vittime le persone omosessuali in Uganda;

Molte Ong internazionali si erano mobilitate in diversi paesi del mondo contro questa barbarie. Il Parlamento Europeo, grazie alla campagna internazionale di Non c'è Pace Senza Giustizia, aveva approvato una Risoluzione di condanna nei confronti dell'Uganda. I parlamentari radicali avevano chiesto ripetutamente al Governo italiano di intervenire per scongiurare rischi e pericoli nel paese. David Kato dopo aver partecipato  a Roma ai lavori del IV Congresso di Certi Diritti, era stato anche audito a Bruxelles dalla Sottocommissione Diritti Umani del Parlamento Europeo;

David Kato Kisule, poco prima di essere ucciso, era stato invitato a partecipare ai lavori del 39° Congresso del Partito Radicale Nonviolento, transnazionale e transpartito che si svolgeranno a Chianciano dal 17 al 20 febbraio 2011 per aggiornare i congressisti dell’aggravamento della situazione, sul fronte dei diritti civili e umani in Uganda;

A seguito dell’assassinio di David Kato Kisule, il Presidente degli Stati Uniti Barack Obama, la Segretario di Stato, Hillary Clinton, il Presidente del Parlamento Europeo, il Sindaco di Parigi, autorità di Governo di molti paesi di tutto il mondo occidentale, Associazioni e Ong che operano nel campo dei diritti umani, hanno espresso forte condanna per il grave atto di violenza avvenuto in Uganda. In Italia soltanto Marco Pannella, leader dei radicali, alcuni parlamentari radicali e alcune Associazioni hanno ricordato del coraggioso impegno di David Kato Kisule;

Attualmente, grazie alle pressioni internazionali di Governi e Ong internazionali il Governo ugandese _ha bloccato l’iter della legge che prevede la condanna a morte delle persone omosessuali anche se nel_ codice penale gli atti omosessuali continuano ad essere gravermente perseguiti; la stessa Associazione ugandese Smug, insieme ad altri organisimi ugandesi e internazionali, chiedono che quanto prima il Governo intervenga per garantire l’incolumità degli altri attivisti omosessuali e vengano quanto prima cancellate le legge persecutorie nei confronti delle persone omosessuali;

Per sapere:

-      Per quale motivo il Governo italiano non è intervenuto per condannare il grave atto di violenza che ha causato la morte di David Kato Kisule;

-      Cosa intende fare il Governo italiano nei confronti delle autorità ugandesi per garantire la promozione e la tutela dei diritti civili e umani delle persone omosessuali;

-      Quali iniziative anche in ambito di Unione Europea intende promuovere il Governo italiano affinchè nei rapporti bilaterali con l’Uganda venga posto tassativamente come vincolo di ogni aiuto e cooperazione il rispetto dei diritti civili e umani;

-      Se i Governo non ritenga urgente intervenire nell’ambito della Cooperazione internazionale con l’Uganda affinchè vengano promosse apposite campagne contro il fondamentalismo religioso e che le regole promosse con fanatismo e odio  non diventino leggi dello Stato e venga mantenuta la necessaria distinzione tra leggi dello Stato e regole proposte dalle organizzazioni religiose.

**_On.li: Matteo Mecacci, Marco Beltrandi, Rita Bernardini, Maria Antonietta Farina Coscioni, Maurizio Turco, Elisbetta Zamparuttti._**