---
title: 'Libro: Laico alfabeto in salsa gay piccante, di Franco Buffoni'
date: Fri, 21 Jan 2011 21:57:11 +0000
draft: false
tags: [Politica]
---

![    * Poesia     * Narrativa e Saggistica     * Testo a fronte / Traduzioni     * Multimedia     * Libri e Links     * Contattami  Franco Buffoni  Stampa questa paginaSegnala questa pagina    English section L’intervista di Gianni Biondillo LAICO ALFABETO, Transeuropa](http://www.francobuffoni.it/upload/images/copertina_laibeto.jpg)**Laico alfabeto in salsa gay piccante. L'ordine del creato e le creature disordinate. Di Franco Buffoni**

Edizioni Transeuropa

Un viaggio tra omosessualità, ateismo, natura umana, diritti civili e libero pensiero, condotto da Marcello Rinaldi in dialogo con l’autore.