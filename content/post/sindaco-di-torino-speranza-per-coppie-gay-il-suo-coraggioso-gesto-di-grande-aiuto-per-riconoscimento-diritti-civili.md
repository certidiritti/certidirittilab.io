---
title: 'SINDACO DI TORINO SPERANZA PER COPPIE GAY. IL SUO CORAGGIOSO GESTO DI GRANDE AIUTO PER RICONOSCIMENTO DIRITTI CIVILI.'
date: Sat, 05 Dec 2009 17:42:40 +0000
draft: false
tags: [Comunicati stampa]
---

Comunicato Stampa Associazione Radicale Certi Diritti:

L'Associazione Radicale Certi Diritti ringrazia il Sindaco di Torino per il coraggioso gesto e per l'annuncio di voler sposare la coppia lesbica che chiede il riconoscimento dei suoi diritti. Sono gia' in corso in tutta Italia da due anni iniziative legali contro i Comuni che non accettano le pubblicazioni per il matrimonio per le coppie dello stesso sesso.

Due tribunali finora hanno riconosciuto fondati i ricorsi di alcune coppie e hanno rimesso alla Corte Costituzionale la decisione. La campagne di Affermazione Civile promossa in tutta Italia con Avvocatura lgbt, Rete Lenford, va avanti per il superamento delle diseguaglinze contro questa ottusq classe politica che fa finta di ignorare il problema del riconsocimento dei diritti.