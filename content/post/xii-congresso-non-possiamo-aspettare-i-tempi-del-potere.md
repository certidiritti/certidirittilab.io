---
title: 
date: 2018/10/18 17:35:56 +0000
tags:
- Politica

---
![XII CONGRESSO di Certi Diritti. "Non possiamo aspettare i tempi del potere". A Milano dal 23 al 25 novembre.](http://www.certidiritti.org/wp-content/uploads/2018/10/MIKES-LEATHErBoots.png)

Il XII Congresso di Certi Diritti inizia venerdì 23 novembre all'[_Open_, in Viale Monte Nero 6](https://goo.gl/maps/ZU3F9uuBV1t), e continua presso [_UP, Unità di Produzione_, in Via Andrea Cesalpino 7](https://goo.gl/maps/mkyqUFZkKt82).
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Movimento LGBTI, intersex, lavoro sessuale, diritto di famiglia, discriminazioni, laicità, Europa, Russia, Israele, Tunisia.

C'è chi annuncia di non poter _"aspettare i tempi della giustizia"_, noi ci riuniamo per ripartire proprio dalla giustizia e dai principi dello Stato di Diritto, attraverso le giurisdizioni nazionali e internazionali, perché **non possiamo aspettare i tempi del Potere**.

Il vuoto di Diritto si conferma nei vecchi e nei nuovi regimi il veicolo delle proibizioni che, oggi, appartengono alla visione politica transnazionale del controllo sulle nostre vite.

Il 23, il 24 e il 25 novembre 2018 è convocato il XII congresso dell'Associazione Radicale Certi Diritti, nel decimo anno dalla sua fondazione: pensiero, strategia, azione e dialogo nonviolento per impostare la la lotta per la libertà e la responsabilità sessuale di tutti, mentre queste sembrano essere oggi più che mai in pericolo.

* * *

[**PREANNUNCIA LA TUA PARTECIPAZIONE**](https://goo.gl/forms/24drtxR5XBgnQvqx2)
-------------------------------------------------------------------------------

[**ISCRIVITI A CERTI DIRITTI**](http://www.certidiritti.org/iscriviti/)
-----------------------------------------------------------------------

**[DONA PER LE SPESE CONGRESSUALI](http://www.certidiritti.org/donazioni/) **
-----------------------------------------------------------------------------

* * *

**IL PROGRAMMA 2018**
---------------------

### **Venerdì 23 novembre**

**Ore 19:00** **- ****Open**, Viale Monte Nero, 6 – Metro Linea 3 (gialla) fermata Porta Romana

**Presentazione libro** “Il lungo _inverno democratico_ nella Russia di Putin”.

**Intervengono:**

*   Anna **Zafesova**, _sovietologa e giornalista de “La Stampa”;_
*   Yuri **Guaiana**, _curatore del libro;_
*   Lorena **Villa,** _Fondazione Luigi Einaudi_.

### **Sabato 24 novembre**

**Ore 9:00-19:00 - ****UP**, Unità di Produzione, via Andrea Cesalpino 7 - Metro Linea 1 (rossa) fermata Gorla

**Dibattito generale**

**Nel corso del dibattito interverranno:**

Tavola rotonda sullo "Sviluppo Atipico dei Genitali" 

*   prof. Gianantonio **Manzoni**, _IRCCS Fondazione Cà Granda Ospedale Maggiore Policlinico Milano_
*   prof. Giacinto Antonio **Marrocco**, _Casa di Cura Salvator Mundi Roma_
*   prof. Girolamo **Mattioli**, _IRCCS Istituto Giannina Gaslini Genova_
*   On. Daniele **Viotti**, _europedputato S&D, co-presidente intergruppo LGBTI_

#MeeToo

*   Claudio **Uberti**, membro del Direttivo di Certi Diritti;
*   Giulia **Crivellini**, avvocato, membro del Direttivo di Certi Diritti.

Transnazionale

*   **_"Muri di separazione, garanzia di oppressione o garanzia di libertà"_**, Giulio **Ercolessi**, _European Humanist Federation;_
*   Mounir **Baatour**_, Presidente di Shams - Pour la dépénalisation de l'homosexualité en Tunisie (intervento in francese)._

Contenziosi strategici

*   **_"2019: l'anno della svolta?"_**, Alexander **Schuster**, _avvocato_.

Campagne

*   **_"Il gruppo Lufthansa è un alleato della comunità LGBTI? Il caso del prof. Marko Lens_"**, prof. Marko **Lens**, _chirurgo plastico_;

**Sono previsti inoltre gli interventi di:**

*   Barbara **Bonvicini**, _Segretaria dell'Associazione Enzo Tortora_
*   Marco **Cappato**, Tesoriere dell'Associazione Luca Coscioni
*   Hadi **Damien**, _Beirut Pride_
*   Alessandro **Fusacchia**, _Deputato di PiùEuropa (in collegamento)_
*   Sandro **Gallittu**, _CGIL Nuovi Diritti_
*   Luna Lara **Liboni**, Coalizione Italiana Libertà e Diritti Civili
*   Lorenzo **Lipparini**, _Assessore radicale a Partecipazione, Cittadinanza attiva e Open data per il Comune di Milano_;
*   Sergio **Lo Giudice**, _già senatore, responsabile dipartimento diritti del Partito Democratico_
*   Luigi **Manconi**, _Coordinatore UNAR (video)_
*   Silvja **Manzi**, _Segretaria di Radicali Italiani_
*   Fabrizio **Petri**, Coordinatore di GlobeMae nonché presidente del Comitato Interministeriale Diritti Umani;
*   Antonella **Soldo**, Tesoriera di _Radicali Italiani_
*   Michele **Usuelli**, _consigliere regionale Regione Lombardia di +Europa_
*   Federica **Vinci**, p_residente Volt Italia_
*   Francesco **Zaccagnini**, _Famiglie Arcobaleno_

**Ore 20:45 - ****Sinagoga Beth Shlomo**, Corso Lodi 8/C – Metro Linea 3 (gialla) fermata Porta Romana

**Dibattito:** “Tunisia: gli ebrei, le minoranze e il rapporto con Israele”. _In collaborazione con Associazione Milanese Pro Israele e Amici di Israele_

*   Mounir **Baatour**, _presidente del Partito Liberale tunisino_
*   Alessandro **Litta Modignani**, _presidente Associazione Milanese Pro Israele_
*   Davide **Romano**, _portavoce della sinagoga Beth Shlomo_
*   Yuri **Guaiana**, _presidente dell'associazione radicale Certi Diritti_

### **Domenica 25 novembre**

**Ore 9:00-15:00 - ****UP**, Unità di Produzione, via Andrea Cesalpino 7 - Metro Linea 1 (rossa) fermata Gorla

**Dibattito generale**

**Sono previsti inoltre gli interventi di:**

*   Emma **Bonino**, _Senatrice della Repubblica per PiùEuropa_
*   Irene **Dionisio**, _direttrice del Lovers Film Festival - Torino LGBTQI Visions_
*   Filomena **Gallo**, S_egretaria dell'Associazione Luca Coscioni_

_Presto pubblicheremo Ordine dei lavori, Regolamento del Congresso e Bilancio 2017-2018_