---
title: 'Grazie a Certi Diritti la censura Rai sul matrimonio gay arriva alla Commissione Europea'
date: Sat, 10 Sep 2011 07:38:01 +0000
draft: false
tags: [Matrimonio egualitario]
---

Sarà depositata lunedì da alcuni deputati del gruppo liberal-democratico un'interrogazione parlamentare urgente rivolta alla Commissione europea sugli episodi omofobi di censura di RAI1.

Bruxelles - Roma, 9 settembre 2011

L'Associazione Radicale Certi Diritti ringrazia i Deputati Europei del gruppo Liberal-Democratico al PE Sophie In't Veld, Sonia Alfano, Sarah Ludford,  Gianni Vattimo, Alexander Alvaro e Renate Weber per il deposito, annunciato per lunedi prossimo, di una interrogazione parlamentare urgente rivolta alla Commissione europea sugli episodi omofobi di censura vaticana di RAI 1 riguardo alcune scene di un  matrimonio e per un bacio  tra persone dello stesso sesso. Come denunciato dai media italiani e dalla comunità lgbt italiana, RAI 1 ha censurato una puntata della fiction "Un ciclone in convento" perché l'episodio trattava del matrimonio di due persone dello stesso sesso. Nell'interrogazione, gli eurodeputati ricordano la censura RAI contro Brokeback Mountain e chiedono se tali episodi omofobi nei media pubblici italiani siano in conformità con i valori e i principi europei di libertà di espressione, informazione e eguaglianza, come pure con le direttive europee sui media.  

Segue il testo dell'interrogazione:

"Homophobic censorship by the Italian public television RAI 1

The Italian public television channel RAI 1 has recently censored an episode of the German TV series "Um himmels willen" because of the fact it showed a same-sex couple getting married. RAI is not new to such homophobic censorship, since in December 2008 it censored images of the film by Ang Lee "Brokeback Mountain" where the two main characters kissed each other, while it broadcasted different-sex images.

Can the Commission illustrate if such homophobic censorship by a public television is in conformity with EU fundamental rights and values, such as freedom of information and of expression, as well as with the prohibition of discrimination based on sexual orientation, notably in connection with Directive 2010/13/EU of 10 March 2010 on the coordination of certain provisions laid down by law, regulation or administrative action in Member States concerning the provision of audiovisual media services (Audiovisual Media Services Directive)?"