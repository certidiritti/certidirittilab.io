---
title: 'MATRIMONIO GAY-CONSULTA. CERTI DIRITTI: LA LOTTA PER IL SUPERAMENTO DELLE DISEGUAGLIANZE VA AVANTI'
date: Wed, 14 Apr 2010 12:12:34 +0000
draft: false
tags: [Comunicati stampa]
---

**Comunicato Stampa dell’Associazione Radicale Certi Diritti:**

L’Associazione Radicale Certi Diritti prima di esprimere un giudizio di merito sulle decisioni della Consulta, attenderà le motivazioni della decisione della Corte Costituzionale riguardo il rigetto dei ricorsi fatti da alcune coppie gay per vedersi riconosciuta la possiibilità di sposarsi.

Esprimiamo tutti i nostri più sinceri ringraziamenti a quanti hanno condiviso con noi questa battaglia, in primis le coppie gay che hanno partecipato alla campagna di Affermazione Civile e gli Avvocati di Rete Lenford, che continuerà almeno fino a quando anche nel nostro ordinamento non sarà consentito alle coppie dello stesso sesso di sposarsi, come già avviene in diversi paesi europei.

Ringraziamo inoltre il Collegio di difesa, composto dai Professori Marilisa D’Amico, Enzo Zeno Zencovich, Vittorio Angiolini che insieme ad un team di una decina di avvocati hanno messo la loro professionalità al servizio di questa battaglia di civiltà e di uguaglianza assumendo la difesa della nostra Associazione e delle coppie gay davanti alla Corte Costituzionale.

Insieme ad Avvocatura Lgbt – Rete Lenford e alla Comunità lgbt(e) valuteremo come procedere e rilanciare la campagna di Affermazione Civile che insieme abbiamo promosso in tutta Italia due anni fa. Rafforzeremo con la nostra Associazione gli obiettivi di questa campagna per l’uguaglianza nel Comitato Si Lo Voglio insieme alle Associazioni lgbt italiane.