---
title: '9-10-11 APRILE A LIVORNO CONGRESSO TRANSGENDER, TRANSESSUALI E INTERSESSUATI'
date: Thu, 08 Apr 2010 12:08:52 +0000
draft: false
tags: [Comunicati stampa]
---

Nei giorni 9-10 e 11 Aprile si svolgerà a Livorno presso l'Hotel Villa Morrazzana (Via Eugenio Curiel, 110) il “**Congresso Italiano Transgender **Transessuali ed Intersessuati”. Sabato mattina sarà presente Sergio Rovasio, Segretario Associazione Radicale Certi Diritti.****

Il Congresso, organizzato e convocato da un gruppo di persone che si è costituito in Staff, con l'appoggio diretto dell'Associazione Trans Genere e dell'Associazione Crisalide PanGender ed indiretto di altre Associazioni, si presenta come l'occasione di un approfondito confronto sulla situazione dei diritti della persona, a partire dalle persone Transgender, Transessuali ed Intersessuate, nella nostra società Italiana.

Nelle due giornate di lavoro (10 e 11 aprile), oltre ad un confronto sulle questioni generali, saranno affrontati temi quali:

a) la validità e l'attualità dei protocolli per il percorso di transizione, la rete di conoscenza e di scambio di informazione sui vari centri, sugli specialisti sul territorio nazionale;

b) Proposte di legge attualmente ferme in Parlamento, in sostituzione o miglioramento dell'attuale legge 164/82, e relativa discussione sulle varie procedure legali;

c) La questione del “lavoro”: le proposte di legge al fine di superare l'attuale situazione di discriminazione, proposte di auto imprenditorialità e quali altre possibilità e prospettive;

d) Quale ruolo all'interno del più ampio movimento LGBT? Come porci di fronte al crescente clima di violenza? Quale rapporto con i mass-media?

Questi, in buona sostanza, i temi che saranno affrontati dal Congresso, il quale vedrà un ampio spazio per i contributi da parte di alcuni ospiti.

E’ prevista la presenza, tra gli altri, dell’ On. **Paola Concia**, **Vladimir Luxuria**, il presidente nazionale di Arcigay **Paolo Patanè**, la scrittrice e giornalista **Delia Vaccarello**, **Sergio Rovasio** segretario nazionale Certi Diritti.

Maggiori informazioni: 

[http://www.congressotransessualitransgende.blogspot.com/](http://www.congressotransessualitransgende.blogspot.com/)