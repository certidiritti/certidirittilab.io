---
title: 'RIUNIONE DI CERTI DIRITTI ROMA LUNEDI'' 15 FEBBRAIO ALLE ORE 21.00'
date: Fri, 12 Feb 2010 23:57:28 +0000
draft: false
tags: [Senza categoria]
---

Carissime e carissimi,

siamo reduci dal III° congresso nazionale della nostra associazione svoltosi a Firenze, laddove abbiamo ribadito tutti i nostri impegni, ovvero libertà, laicità, difesa del matrimonio fra persone dello stesso sesso, con particolare attenzione alla riforma del diritto di famiglia, preoccupandoci anche delle cosiddette “terapie riparative” e senza dimenticare il diritto ad un “giusto futuro”, così da approvare la costituzione del neonato coordinamento “Giovani per i Diritti”, il nostro investimento per il domani.

Siamo anche alla vigilia di una sentenza epocale che vedrà il 23 marzo p.v. la Corte Costituzionale pronunciarsi in merito alla legittimità dei ricorsi presentati dalle coppie dello stesso sesso che, avendo aderito alla campagna di “Affermazione Civile”, si sono visti rifiutare la licenza di matrimonio dai propri comuni di residenza, pronunciamento che comunque vada è già di per sé una vittoria incredibile, per il solo fatto di aver portato a discutere di matrimonio gay le più alte istituzioni dello Stato; proprio in previsione di questo evento, impensabile solo pochi anni fa, stiamo mettendo in cantiere una manifestazione nazionale che si dovrebbe tenere a ridosso della data della sentenza, magari domenica 21 marzo, inizio della primavera, la nostra “Primavera di Diritti”.

Ma è anche la vigilia del congresso di Arcigay, la più grande e potente associazione gaylesbica italiana, che potrebbe sancire al suo interno una svolta storica che di fatto cambierebbe tutti gli equilibri interni nel frastagliato movimento gay.

Come potete vedere di carne sul fuoco ce n’è tanta, per questo vi invitiamo a partecipare alla prossima riunione di Certi Diritti Roma, dove di tutto ciò si parlerà e che come al solito si terrà di lunedì, esattamente il 15 di questo mese, di nuovo nelle ospitali sale della sede del Partito Radicale, in Via di Torre Argentina 76, sempre  alle 21.00.

È un momento molto particolare per i diritti delle cittadine e dei cittadini omosessuali, per questo vi chiediamo di scendere in campo, insieme a noi, per dare un segnale forte a chi ancora si ostina a non voler capire.

Vi aspettiamo numerosi.

**CERTI DIRITTI Roma  
Coordinamento “Giovani per i Diritti”**  
Matteo Di Grande

> Per confermare la vostra partecipazione potete farlo a traverso il nostro calendario eventi presente nel sito a [questo link](tutti-gli-eventi/details/11-riunione-di-certi-diritti-roma-lunedi-15-febbraio.html)