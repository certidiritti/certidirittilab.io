---
title: 'INCONTRO CON LA COMUNITA'' LGBT ISRAELIANA. CORTE SUPREMA GARANTISCE DIRITTI'
date: Mon, 15 Jun 2009 08:11:45 +0000
draft: false
tags: [Comunicati stampa]
---

Appunti di viaggio in Israele in occasione del gay pride di Tel Aviv del 12 giugno 2009.

Invitato in qualita` di segretario dell\`Associazione Radicale Certi Diritti, da un\`organizzazione di studenti volontari dell\`Universita\` diTel Aviv, sono partito per una cinque giorni incredibili, di forte emozione.

**di Sergio Rovasio**

Tel Aviv e` un sogno per la comunita` lgbt israeliana e internazionale. Il panorama della vita notturna fa concorrenza alle citta` piu` gayfriendly d\`Europa e le supera in qualita\`, bellezza e classe. Anche il Gay pride annuale di Tel Aviv non e` da meno, specialmente quest\`anno che si festeggiano i 100 anni della nascita della citta\`.

Quasi 50.000 persone sono sfilate venerdi` 12 giugno con carri, creativita` e festa, il tutto e` stato straordinario e divertente come sempre accade per i gay pride. Il luogo di partenza era il parco dedicato a Golda Meir dove da pochi mesi e` stata aperta, finanziata dal Comune, una vera casa di accoglienza, informazionie, aiuto e sostegno della comunita` lgbt voluta da un giovanissimo leader del movimento gay, Etas Pinkas, impegnato da sempre su questo obiettivo sin da quando era Consigliere Comunale della Citta`. Un fatto che non ha eguali nella storia di Israele.

La situazione drammatica e di perenne conflitto che si vive in questa regione non fa pensare a questa ideale condizione di vita delle persone omosessuali, ben accettate nella citta` che si puo` ben definire la piu` gayfriendly del Medio Oriente.. Le associazioni sono impegnate nel campo dei diritti civili e umani anche per aiutare quei palestinesi che spesso hanno bisogno di maggiore assistenza e protezione. Sia a Tel Aviv con l\`Associazione Aguda, che a Gerusalemme con la Open House, operano anche volontari arabi per aiutare i loro fratelli omosessuali con maggiore difficolta\`.

Nell\`ambito del gay pride un\`organizzazione di studenti volontari, I.Pride, raccolta nello slogan \`Stand With Us\` ha messo intorno a un tavolo alcune personalita` israeliane e associazioni lgbt provenienti da vari paesi del mondo. E` qui che il Presidente di Aguda, Mike Amel, ci ha raccontato delle grandi difficolta` e diffidenze che, per motivi di sicurezza, vengono opposte dalle autorita` alla richiesta di protezione di alcuni cittadini palestinesi aiutati dai volontari della storica associazione lgbt cittadina. Durante la Conferenza e` intervenuto un parlamentare dichiaratamente gay del partito Meretz, Nitzan Horowitz, figura carismatica e di grande spessore intellettuale. Ci ha spiegato che solo una societa` laica, se democratica, puo` garantire i diritti civili e umani, ovviamente il paragone con l\`Italia era dovuto. Il fenomeno della commistione della religione con gli affari dello Stato qui e\` particolarmente sentito, per tutti andrebbe superato e risolto perche` principale ostacolo a leggi di civilta`.

In Israele la stragrande maggioranza dei diritti della comunita` lgbt sono riconosciuti  e questo lo si deve grazie alle carte bollate, cioe` la via legale, quella intrapresa in Italia dall`Associazione Radicale Certi Diritti con la campagna di Affermazione Civile. Sia in ambito civile che militare grazie alle decisioni della Corte Suprema vi sono stati adeguamenti normativi. Le leggi ancora non prevedono riconoscimenti espliciti ma alcune sentenze storiche hanno di fatto modificato in meglio la legislazione.

La fecondazione assistita per gli omosessuali in Israele e` possible;  ai margini del gay pride si  sono viste molte coppie di lesbiche e gay con in braccio un bambino. Le contestazioni e le grandi polemiche sulla Guerra, su Gaza e sulla condizione delle persone palestinesi vengono discusse animatamente anche con sit-in di fronte alla casa lgbt da altri esponenti del movimento. Anche se ci e` sembrato piu` logico che si organizzassero davanti alla sede del Governo o della Knesset.

Alla fine della manifestazione sono stati celebrati simbolicamente cinque matrimoni di coppie gay con, presente, il Sindaco di Tel Aviv, Ron Huldai, e il deputato gay Nitzan Horowitz in una cornice di grande emozione. Si andra` avanti ancora per 24 ore con i migliori Dj provenienti da tutto il mondo..