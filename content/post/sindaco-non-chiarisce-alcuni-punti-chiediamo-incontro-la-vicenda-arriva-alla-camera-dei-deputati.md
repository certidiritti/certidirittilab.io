---
title: 'Sindaco non chiarisce alcuni punti, chiediamo incontro. La vicenda arriva alla Camera dei deputati'
date: Tue, 03 Aug 2010 13:38:59 +0000
draft: false
tags: [arenauta, camera dei deputati, Comunicati stampa, gaeta, interrogazione, parlamento, rovasio, spiaggia]
---

**SPIAGGIA DELL’ARENAUTA: SINDACO DI GAETA NON CHIARISCE ALCUNI PUNTI RIGUARDO L’INGABBIAMENTO DELLA SPIAGGIA. DOPO IL CONSIGLIO REGIONALE LA VICENDA ARRIVA ALLA CAMERA DEI DEPUTATI CON UNA INTERROGAZIONE PARLAMENTARE URGENTE.**

**Gaeta, 3 agosto 2010-08-03**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti e Presidente del Comitato per la difesa e la tutela della spiaggia dell’Arenauta:**

“Le preoccupazioni del Sindaco di Gaeta sono comprensibili qualora vi fosse un rischio reale di caduta massi nella spiaggia dell’Arenauta che nessuno dei frequentatori della spiaggia ha mai visto.

Ciò che sorprende è che esistono molte strutture di dissuasione affinchè in un determinato punto di passaggio non vi sia transito di persone. Non comprendiamo la ragione per la quale viene installata una rete sulla battigia che alla prima mareggiata la farà cadere.  Stiamo documentando con foto e filmati quanto sta avvenendo nella spiaggia. Chidiamo un incontro urgente con il Sindaco di Gaeta".

**Dopo l’interrogazione rivolta alla Presidente della Regione Lazio Renata Polverini, oggi sei deputati radicali del Pd, prima firmataria l’on. Elisabetta Zamparutti, hanno depositato alla Camera dei deputati una interrogazione urgente a risposta scritta ai Ministri degli Interni e dell’Ambiente affinchè si faccia chiarezza sulla recinzione di parte della spiaggia dell’Arenauta.**

**Di seguito il testo integrale dell’interrogazione parlamentare.**

**Interrogazione urgente a risposta scritta**

**Al Ministro degli Interni**

**Al Ministero dell’Ambiente**

PREMESSO CHE:

-          giovedì 29 luglio, in un tratto della spiaggia dell’Arenauta di Gaeta (Latina), situata in un’area di particolare interesse ambientale, dove non vi sono state fino ad oggi speculazioni edilizie di nessun tipo e considerata ‘libera’ perché non data in concessione, sono iniziati dei lavori di ‘emergenza’ consistenti in una recinzione di tutta l’area con pali installati sulla battigia della spiaggia;

-          le decine di strutture di sostegno della recinzione sono state installate in punti della spiaggia in violazione delle più elementari regole ambientali;

-          I lavori sono iniziati con uno spiegamento di forze dell’ordine, polizia, carabinieri, guardia costiera ma stranamente senza la presenza di Vigili del fuoco o agenti della Forestale;

-          negli ultimi anni l’area interessata dai lavori è stata spesso oggetto di strane iniziative da parte di alcuni amministratori locali con l’obiettivo di spaventare e allontanare le persone che frequentano la spiaggia libera;

PER SAPERE:

\- quali sono le ragioni di questi interventi straordinari urgenti in un tratto di spiaggia dove non sono mai avvenuti, negli ultimi 30 anni, crolli, frane, cedimenti strutturali;

\- se non ritengano gli interroganti che tali interventi siano finalizzati a obiettivi diversi da quelli della ‘messa in sicurezza’ dell’area senza che vi siano mai stati pericoli per i frequentatori della spiaggia;

\- per quale motivo viene ‘ingabbiata’ con una recinzione un’area così estesa di spiaggia che corrisponde esattamente all’unico tratto della spiaggia dell’Arenauta, finora rimasta esente da forme di speculazione edilizia e  ancora non data in concessione ad alcun privato;

\- se non ritenga che le strutture della recinzione siano state installate  in modo del tutto irregolare rispetto a quanto previsto dalle norme di tutela ambientale che prevedono una distanza di almeno cinque metri rispetto alla battigia;

\- se non ritenga che questi interventi mirino a colpire le persone che frequentano quel tratto di spiaggia;

**I deputati radicali del Pd:**

**Elisabetta Zamparutti**

**Marco Beltrandi**

**Rita Bernardini**

**Maria Antonietta Farina Coscioni**

**Matteo Mecacci**

**Maurizio Turco**