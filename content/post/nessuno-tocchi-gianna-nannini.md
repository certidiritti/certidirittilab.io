---
title: 'NESSUNO TOCCHI GIANNA NANNINI'
date: Mon, 06 Dec 2010 15:30:08 +0000
draft: false
tags: [certi diritti, Comunicati stampa, giovanardi, nannini, rovasio, vaticano]
---

**NESSUNO TOCCHI GIANNA NANNINI, NEMMENO IL (MOLTO) SOTTOSEGRETARIO DI STATO VATICANO GIOVANARDI. SI PREOCCUPI PIUTTOSTO DEL FATTO CHE DA QUANDO E’ AL GOVERNO SONO AUMENTATI I DIVORZI E LE UNIONI FUORI DAL MATRIMONIO.**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

“Il (molto) Sottosegretario di Stato Vaticano Giovanardi non tocchi e non offenda la dignità e il coraggio di **Gianna Nannini**, che ha fatto una scelta all’insegna dell’amore e della speranza. E ‘ gravissimo e offensivo quanto da lui oggi dichiarato sulla maternità della cantante che è faccenda sua e di nessun altro. [Abbiamo già ricordato che da quando il (molto) Sottosegretario è al Governo](tutte-le-notizie/649-rovasio-giovanardi-pensi-al-fallimento-del-suo-modello-di-famiglia-piuttosto-che-tentare-di-influenzare-la-corte-costituzionale.html), in Italia sono aumentati i divorzi, le separazioni, le convivenze al di fuori del matrimonio. E’ evidente che  il (molto) Sottosegretario di Stato Vaticano dovrebbe dimettersi, vista la sua politica fondamentalista e fallimentare. Non è certo tra le sue competenze quella di intervenire sulle gravidanze delle cittadine italiane. [Nessuno Tocchi Gianna Nannini](tutte-le-notizie/822-nessuno-tocchi-gianna-nannini-che-non-piace-al-talebano-di-turno.html)”.