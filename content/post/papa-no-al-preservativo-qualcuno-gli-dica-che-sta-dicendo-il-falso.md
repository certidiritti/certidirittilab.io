---
title: 'PAPA: NO AL PRESERVATIVO. QUALCUNO GLI DICA CHE STA DICENDO IL FALSO'
date: Tue, 17 Mar 2009 15:52:05 +0000
draft: false
tags: [aids, Comunicati stampa, PAPA, PRESERVATIVO]
---

**GRAVISSIMO E FALSO QUANTO AFFERMA IL PAPA. QUALCUNO HA IL CORAGGIO DI DIRLO? O VOGLIAMO PER L'ENNESIMA VOLTA FARE FINTA DI NULLA? APPELLO AI MEDIA E ALLA CLASSE DIRIGENTE PER UN'AZIONE DI CORAGGIO E NON DI ENNESIMA IPOCRISIA.**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

“Il Papa ha oggi affermato che l'epidemia di Aids “non si può superare con la distribuzione dei preservativi che, anzi, aumentano i problemi' lo ha detto proprio mentre si sta recando in Africa dove è in corso una strage di vite a causa di questa malattia. Qualcuno in questo paese ha il coraggio di smentire una frase così falsa e antiscientifica? O dobbiamo assistere all'ennesima sfilata di ipocrisie e far finta che quanto detto dal Papa non sia falso?

Ci auguriamo che la nostra classe dirigente dimostri coraggio, coerenza e serietà smentendo quelle frasi che alimentano l'ignoranza e la morte. Facciamo appello ai media, agli editorialisti, ai Direttori di testate affinché tutte le cifre diffuse dalle organizzazioni di lotta all'aids vengano pubblicate e si documenti come il preservativo rimanga uno dei principali strumenti di lotta a questo male”.