---
title: 'DOPO LESBICHE E GAY ORA IL VATICANO CONTRO TRANSESSUALI'
date: Fri, 04 Apr 2008 01:23:44 +0000
draft: false
tags: [amore, Comunicati stampa, transessuali, transfobia, vaticano]
---

Certi Diritti: ora il Vaticano se la prende anche contro le persone transessuali, non bastavano gli anatemi contro lesbiche e gay?

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

“Le affermazioni contro le persone transessuali del Cardinale Javier Lozano Barragan, Presidente del Pontificio Consiglio per gli Operatori Sanitari, il Ministro della Salute della Città del Vaticano, sono sconcertanti e lasciano allibiti. Altro che transfobia! Sostenere infatti che “l’essere transessuale non va d’accordo con la morale cattolica e cristiana” o addirittura che tale condizione è “contro la natura umana”, e che “l’identità umana è uomo o donna, ma niente di intermedio” è la totale negazione di una realtà umana difficile, complessa che merita rispetto e magari anche amore cristiano.

Si potrebbe ricordare a sua Eminenza che sono decine di migliaia le persone transessuali che, in Italia, grazie alla legge dei radicali del 1982 che permette il cambio del nome e il cambio del sesso, hanno avviato un difficile percorso di transizione, volto al raggiungimento della propria identità, spesso pagando enormi sacrifici umani e in condizioni sociali molto difficili; molte di queste persone sono battezzate, cresimate e sposate in chiesa e vivono una vita ‘normale’. Cosa dovrebbero fare queste persone? Non sono forse anche loro figli/figlie di Dio? Devono autoeliminarsi per soddisfare il volere di qualche oligarchia cardinalizia?

Diciamo che la posizione del Ministro della Sanità vaticano è addirittura superata dalla teocrazia islamica iraniana, che riconosce alle persone transessuali l’assistenza sanitaria gratuita necessaria.

Insomma, ancora una volta un autorevole esponente delle gerarchia vaticana non ha perso l’occasione per offendere il messaggio cristiano di carità, rispetto e amore che anche le persone transessuali meritano”.