---
title: 'A PROPOSITO DI "LGBTQI(E)".... DI SERGIO LO GIUDICE'
date: Wed, 09 Jun 2010 09:10:45 +0000
draft: false
tags: [Comunicati stampa]
---

Pubblichiamo volentieri questo intervento di Sergio Lo Giudice:

Bolgogna, 8 giugno 2010

"Oggi è stata diffusa da Queer in action un nota in cui si fa riferimento ad una rassegna LGBTQI. Nell’intestazione della mail si fa riferimento ad una rassegna di cinema GLBTQI,  nella mail si parla di una rassegna GLBQI.

Il Pride di Milano di quest’anno si chiama TLGB. Nel 2001 era GLTQ nel 2002 GLTB, nel 2003 LGT nel logo, GLT nel manifesto. Nel 2004 GLBT ,  nel 2005 GLTB nel logo, LGBT nel manifesto, LGBTQ nei comunicati di Arcigay Milano. Il comitato organizzatore si definiva LGT.  
Quello di Napoli si chiama LGBTQI, ma l’iniziativa connessa di Arcilesbica si chiama Zone LGBTQ. Nella piattaforma del Pride si parla però di movimento LGBT. Se cerchi su googleNapoli Pride , la prima notizia che appare dice: Napoli Pride 2010 - Manifestazione nazionale GLBTQI.  
Il Pride di Roma ha  appena cambiato nome, da lgbtqi a LGBTIQ .  
Nel 2008 Il Catania Pride  era GLBTmentre il Bologna Pride era LGBT (ma nel 2005 era stato LGBTQ).  
Scusatemi, ma tutto questo mi sembra demenziale sul piano della comunicazione. La prossima volta che sento qualcuno lamentarsi perché questo o quel politico non sa nemmeno pronunciare la sigla lgbt , l’unica veramente internazionale (13.800.000risultati su Google, vi invito a controllare le altre ) gli tiro il collo".

  
[www.sergiologiudice.it/blog/2008/10/01/vedi-alla-voce-lgbt/](http://www.sergiologiudice.it/blog/2008/10/01/vedi-alla-voce-lgbt/)