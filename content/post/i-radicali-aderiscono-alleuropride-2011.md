---
title: 'I radicali aderiscono all''Europride 2011'
date: Tue, 29 Mar 2011 09:19:41 +0000
draft: false
tags: [Europa]
---

L'associazione Radicale Certi Diritti presenterà giovedì 7 aprile alla Domus Talenti di Roma (ore 19.30 via delle Quattro Fontane, 113) le iniziative organizzate tra aprile e giugno come contributo all'Europride.  

I Radicali, da sempre impegnati per la promozione e la difesa dei diritti civili e umani in Italia e nel mondo, aderiscono e partecipano all'Europride 2011 che si svolgerà quest'anno a Roma.

Da sempre riteniamo che la conquista dei diritti civili è possibile se le regole democratiche sono garantite, cosa che purtroppo non avviene in Italia così come abbiamo ben documentato nel dossier 'La peste italiana'. Riteniamo che il sistema partitocratico con la commistione dei privilegi e delle ingerenze vaticane impedisca la conquista di leggi di civiltà e moderne così come avviene in tutto il resto d'Europa.  

Ci auguriamo che questo importante evento sia di aiuto agli obiettivi e alle speranze che ci vedono insieme impegnati sullo stesso fronte, in Italia, in Europa e in molti paesi del mondo".

Radicali Italiani  
Associazione Radicale Certi Diritti  
Associazione Luca Coscioni  
Gruppo Lista Bonino Pannella, Federalisti Europei – Consiglio Regionale del Lazio  
Nessuno Tocchi Caino

**Emma Bonino**, Vice Presidente del Senato  
**Mario Staderini**, Segretario di Radicali Italiani  
**Marco Cappato**, Segretario Associazione Luca Coscioni  
**Rita Bernardini**, Deputata Radicale - Pd, Presidente Associazione Radicale Certi Diritti  
**Sergio Rovasio**, Segretario Associazione Radicale Certi Diritti  
**Sergio D’Elia**, Segretario Nessuno Tocchi Caino  
**Elisabetta Zamparutti**, Deputata Radicale – Pd, Tesoriere Nessuno Tocchi Caino,  
**Giuseppe Rossodivita**, Capogruppo Lista Bonino Pannella, Federalisti europei al Consiglio Regionale del Lazio  
**Rocco Berardo**, Consigliere Regionale lista Bonino Pannella, Federalisti europei al Consiglio Regionale del Lazio

foto del Roma Pride 2007 di Mihai Romanciuc