---
title: 'Sul sito una sezione dedicata alla memoria di David kato Kisule'
date: Sun, 06 Feb 2011 14:18:33 +0000
draft: false
tags: [arcigay, certi diritti, Comunicati stampa, current, david kato kisule, GAY, memory, OMOFOBIA, pannella, perduca, petizione, radio radicale, rovasio, smug, Uganda, video]
---

Certi Diritti ricorda David Kato Kisule, attivista gay ugandese e nostro iscritto, con una sezione del sito che contiene ricordi, dichiarazioni, agenzie di stampa e interventi dei parlamentari radicali sulla sua uccisione.

Se hai materiale su David Kato Kisule e vuoi condividerlo invialo a webmaster@certidiritti.it e lo pubblicheremo.

**[VAI ALLA SEZIONE SPECIALE IN MEMORIA DI DAVID >](campagne/in-memoria-di-david-kato.html)**