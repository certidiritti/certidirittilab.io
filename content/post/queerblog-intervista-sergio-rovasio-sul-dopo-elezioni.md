---
title: 'QUEERBLOG INTERVISTA SERGIO ROVASIO SUL DOPO-ELEZIONI'
date: Tue, 09 Jun 2009 08:15:00 +0000
draft: false
tags: [Comunicati stampa]
---

Sergio Rovasio (Radicali): "Cari gay e lesbiche lottiamo contro questa squallida politica".  

----------------------------------------------------------------------------------------------

Nel giugno del 1988, Sergio Rovasio ideò, insieme a Paolo Pietrosanti e Ivan Novelli la campagna “Pioggia artificiale sulla parata militare”. Durante la sfilata del 2 giugno, grazie alla tecnologia avrebbero fatto piovere su Via dei Fori imperiali. Quel giorno, protetti da chissà quale santo o diavolo, venne giù il diluvio per tutta la durata della manifestazione...

La stampa allibita quasi ci credette tanto che il Ministero della Difesa diramò un comunicato dicendo che non esisteva alcuna tecnologia segreta e che forse era un caso-burla quello di Rovasio & C. Ma per Sergio e i Radicali si aprirono le strade della comunicazione e dell’ascolto, seguita da altre splendide iniziative a favore dei diritti civili. Sergio Rovasio, torinese, è Radicale dall’età di 16 anni, ha vissuto a Bruxelles e New York e oggi a Roma dove presiede il gruppo parlamentare Radicale con la tenacia e la passione che ha in dote. E’ anche segretario nazionale di [Certi Diritti](http://www.certidiritti.it/), l’associazione che si occupa delle battaglie GLBT, aperta ad ogni iniziativa che possa aiutare le coppie e i singoli GLBT. E tra le tante altre cose, Sergio è anche un bel ragazzo, che non guasta di questi tempi. In nome dell’amicizia che ci lega da diversi anni, gli chiedo di parlarci di queste elezioni, dei Radicali e di cosa ci dobbiamo aspettare come popolo GLBT da questa nuova Europa.

L'interivista completa al seguente link

[http://www.queerblog.it/post/5420/sergio-rovasio-radicali-cari-gay-e-lesbiche-lottiamo-contro-questa-squallida-politica](http://www.queerblog.it/post/5420/sergio-rovasio-radicali-cari-gay-e-lesbiche-lottiamo-contro-questa-squallida-politica)