---
title: '2008-0140-Discrimination-ST12107-CO01.EN10'
date: Thu, 30 Sep 2010 15:18:47 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 30 September 2010

Interinstitutional File:

2008/0140 (CNS)

12107/10

COR 1

LIMITE

SOC 456

JAI 623

MI 242

  

  

  

  

  

CORRIGENDUM TO OUTCOME OF PROCEEDINGS

from :

The Working Party on Social Questions

on :

16 July 2010

No. prev. doc. :

9312/10 SOC 314 JAI 387 MI 128 + COR 1

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

1)         On p. 2, add the following new paragraph after paragraph 1:

DE had critical questions and was concerned by the fact that the study discussed was not the final version.

2)      On p. 2, paragraph 2 should read as follows:

Delegations gave their first reactions to the questions tabled by the Presidency, some (CZ, EL, ES, IE, IT, LT, HU, PL, PT, SK, SE, FI, DE) indicating that they required more time to study the issues.

  

3)      On p. 3, paragraph 8 should read as follows:

DE reiterated its general reservation. DE saw a need to clarify the term "financial services" in the interest of legal certainty. Moreover DE stressed that, in the light of the principle of subsidiarity, fighting discrimination in the area of financial services was best left to the Member States.

4)      On p. 5, the first sentence of the last paragraph should read as follows:

DK, LU and AT similarly preferred a cumulative formulation ("and").

**\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_**