---
title: 'X congresso di Radicali Italiani: approvate due mozioni particolari su lotta alle discriminazioni e legalizzazione della prostituzione'
date: Wed, 02 Nov 2011 12:53:13 +0000
draft: false
tags: [Politica]
---

 Roma, 2 novembre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

  
L’Associazione Radicale Certi Diritti ha presentato al X Congresso di Radicali Italiani due Mozioni particolari sul tema della lotta alle diverse forme di discriminazioni (primo firmatario Ottavio Marzocchi) e per la legalizzazione della prostituzione (primo firmatario Sergio Rovasio).  Le Mozioni particolari sono state approvate quasi all’unanimità dai congressisti. 

Qui di seguito i due testi delle Mozioni particolari approvate:

  
**Mozione particolare**  
**EGUAGLIANZA CONTRO LE DISCRIMINAZIONI**  
  
 Il X Congresso di Radicali italiani.  
- considerata la situazione di progressivo e gravissimo degrado della situazione dei diritti civili e umani e delle libertà fondamentali, della democrazia, dello Stato di diritto, del diritto all’eguaglianza in Italia e in Europa, come dimostrato dalle persecuzioni e discriminazioni sulla base dell’identità di genere, origine etnica, religione o convinzioni personali, disabilità, età ed orientamento sessuale;    
- Considerato che le diverse forme di discriminazione, persecuzione, violenze fisiche e verbali, private e pubbliche, con la partecipazione attiva e la connivenza passiva delle istituzioni e delle autorità, anche governative, sono sempre più diffuse, come peraltro dimostra l’incapacità delle autorità italiane ed europee di approvare leggi e politiche che permettano ed assicurino il superamento delle discriminazioni e l’attuazione del principio di eguaglianza verso tutti i cittadini;   
- Considerato che la proposta della Commissione Europea di direttiva “orizzontale” contro tutte le discriminazioni basate sul genere, origine etnica, religione o convinzioni personali, disabilità, età, e orientamento sessuale é bloccata da anni in sede di Consiglio dei Ministri europei, dove alcuni Governi, tra cui ovviamente l’Italia, insieme a Germania e Repubblica Ceca, oppongono ostacoli e veti che ne impediscono l’approvazione;   
- Considerato che in sempre più paesi europei i Rom, in modo sempre più simile a un passato che si sperava superato, sono perseguitati, espulsi, discriminati, mentre gli Stati dell’Unione e anche la Commissione europea continuano a non attuare piani di inclusione e di promozione dei diritti umani e delle libertà fondamentali del loro popolo;  
 - Considerato che lo Stato italiano non garantisce la laicità e la neutralità dello Stato rispetto alle religioni, permettendo commistioni, privilegi, finanziamenti, infiltrazioni, inquinamenti tra religione e Stato, imponendo di fatto una religione di Stato in numerosi settori pubblici e privati anche del ‘cosiddetto’ servizio pubblico;  
 - considerato che tale situazione si concretizza ad esempio con l’imposizione dei crocifissi nelle scuole pubbliche, nella sostanziale discriminazione delle persone di religioni diverse o che non professano alcuna religione;  
 - considerato che le autorità italiane hanno sostenuto davanti alla Corte europea dei diritti umani che il crocifisso è un simbolo culturale nazionale, rinnegandone ed umiliandone il significato religioso;    
- considerato che alcuni partiti italiani fanno campagne violentemente contro i musulmani;   
- Considerato che in numerosi Stati europei la democrazia e lo Stato di diritto sono gravemente a rischio, come dimostrato ad esempio dalla situazione in Ungheria, dove il governo ha assunto il controllo dei media ed ha modificato la Costituzione al fine di instaurare una dittatura della maggioranza sulla minoranza e considerato che tale processo degenerativo delle democrazie è comune in altri Stati europei oltre che in Italia, dove i Radicali denunciano una realtà da ‘democrazia reale’ e ‘nazionldemocraticismo’ e che hanno ben documentato con il dossier della  “peste italiana”.   
- Considerato che le istituzioni nazionali ed internazionali per la protezione dei diritti umani e le libertà fondamentali individuali contro le violazioni attuate dagli Stati, quali ad esempio la Corte europea dei diritti umani ed il Commissario per i diritti umani del Consiglio d’Europa, sono sotto attacco da parte dei governi nazionali, incluso quello italiano, per influenzarne le decisioni e decurtarne i poteri;  
  
Impegna  Radicali Italiani a rafforzare e rilanciare la lotta in Italia, in Europa e a livello internazionale per assicurare la promozione e l’attuazione del principio di eguaglianza contro tutte le discriminazioni:  
 - Sollecitando l’approvazione della Direttiva Europea contro le discriminazioni e la sua attuazione in Italia, inclusa la lotta contro l’omofobia, l’approvazione delle unioni civili,  l’accesso al matrimonio civile alle coppie formate da persone dello stesso sesso, la depatologizzazione della condizione delle persone transessuali;  
 - Monitorando la situazione dei Rom e degli immigrati in Italia e le politiche delle autorità nei loro confronti e  denunciando le violazioni dei diritti umani presso tutti gli organismi nazionali e internazionali;  
 - Rafforzando assieme al Partito Radicale Nonviolento, Transnazionale e Transpartito, la lotta per assicurare che gli organismi internazionali per la difesa dei diritti fondamentali individuali rafforzino i loro poteri contro le violazioni di tali diritti attuate dagli Stati e dalle loro autorità e chiedendo alla Commissione europea di assumere con coraggio le proprie responsabilità nel garantire, proteggere e promuovere i diritti fondamentali nell’UE;  
  
   
**mozione particolare**  
** LEGALIZZAZIONE DELLA PROSTITUZIONE**

 Il X Congresso di Radicali Italiani ritiene sempre più urgente sostenere tutte le iniziative finalizzate ad una Riforma delle politiche sul fenomeno della prostituzione che valorizzino l’autodeterminazione e la dignità delle persone; Considerato che si stima che il mercato nero della prostituzione sia la terza fonte di guadagno per il crimine internazionale, dopo armi e droga;  
la tratta della prostituzione vede schiavizzati migliaia di esseri umani, costretti a prostituirsi da organizzazioni criminali; sono vittime di questi atti criminali migliaia di personeanche minori;  Il Governo ha congelato e bloccato la discussione in Parlamento delle diverse proposte di legge, anche quella della Senatrice Radicale Donatella Poretti che prevede la legalizzazione; il proibizionismo sulla prostituzione  genera sempre di più obbrobri giuridici e amministrativi presentati e sostenuti dalle amministrazioni locali che promuovono politiche demagogiche e fallimentari;  
le centinaia di ordinananze comunali fatte per combattere la prostituzione, molte delle quali dichiarate illegittime dagli organi giuridici dello Stato, hanno dimostrato il loro fallimento su tutti i fronti; un esempio per tutte è quanto accaduto nella città di Roma, dove lo stesso Sindaco, dopo aver propagandato e applicato la sua ordinanza antiprostituzione per oltre due anni, ha dovuto dichiarare il fallimento  di tutta l’operazione;  
uno studio commissionato nel 2007 dal Dipartimento Pari Opportunità della Presidenza del Consiglio ha rilevato che sono circa nove milioni gli italiani che, con motivazioni e cadenze diverse, frequentano prostitute e prostituti.  
In Germania lo Stato introita quasi 4 miliardi di euro dalla tassazione della prostituzione; in Italia si stima che il mercato nero della prostituzione sia di oltre un miliardo di Euro senza contare tutte le risorse che lo Stato destina alla repressione del fenomeno; le forze dell’ordine impegnate in questa guerra fallimentare potrebbero dedicarsi con più risorse alla lotta alla tratta e alla lotta alla violenza contro le persone che si prostituiscono;  
  
 impegna Radicali Italiani a sostenere le iniziative finalizzate alla legalizzionee al riconoscimento del lavoro di prostituzione anche in Italia,gli obiettivi del Comitato per i Diritti Civili delle Prostitute e, in Europa, il Manifesto delle e dei Sex Workers, che con lo slogan ”Sex Workers need Human Rights, not Legal Wrong” chiedono maggiore dignità per l’attività che svolgono;