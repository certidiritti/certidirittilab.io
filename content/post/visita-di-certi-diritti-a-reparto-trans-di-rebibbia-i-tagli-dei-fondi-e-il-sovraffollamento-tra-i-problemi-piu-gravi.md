---
title: 'VISITA DI CERTI DIRITTI A REPARTO TRANS DI REBIBBIA: I TAGLI DEI FONDI E IL SOVRAFFOLLAMENTO TRA I PROBLEMI PIU’ GRAVI'
date: Sat, 15 Jan 2011 15:43:08 +0000
draft: false
tags: [Comunicati stampa]
---

L’Associazione Radicale Certi Diritti si impegna a fare visita alle 17 carceri italiane in cui sono recluse le persone transessuali.

Roma, 15 gennaio 2011

**Comunicato Stampa dell’Associazione Radicale Certi Diritti**

Questa mattina, sabato 15 gennaio,  una delegazione di radicali, composta da Rita Bernardini, deputata radicale del Pd e Presidente dell’Associazione Radicale Certi Diritti, Giuseppe Rossodivita, Consigliere Regionale radicale del Lazio, Leila Deianis, Presidente dell’Associazione Libellula e Sergio Rovasio, Segretario dell’Associazione Radicale Certi Diritti,  si sono recati in visita nel Carcere romano di Rebibbia, accompagnati dal Direttore, Carmelo Cantone.

La visita è stata fatta nel reparto delle persone transessuali all’interno del quale sono rinchiuse 25 persone, quasi tutte straniere. La condizione delle persone transessuali nel carcere è del tutto incompatibile rispetto ai bisogni e all’assistenza medica specialistica di cui necessitano. Tra i problemi più gravi vi è quello del sovraffollamento e della mancanza di attività lavorativa. In tutto il reparto vi sono solo due persone lavoratrici e l’assistenza sanitaria specialistica, come per tutta la popolazione detenuta in Italia, è del tutto inadeguata.

In tutta Italia le persone transessuali detenute sono 168 distribuite in 17 carceri italiane. La maggior parte delle persone transessuali detenute si trovano nelle carceri di Napoli, Roma, Firenze e Belluno. Quasi tutte sono persone extra-comunitarie e molte si trovano in regime detentivo per violazione della legge sull’immigrazione. Vi sono molte persone transessuali in condizioni di forte disagio. L’Associazione Radicale Certi Diritti si impegna a fare visita a tutti e 17 i reparti distribuiti in Italia.

Più in generale, nel solo Carcere di Rebibbia quest'anno sono stati fatti ingenti tagli ai fondi destinati dall’Amminstrazione Penitenziaria ai detenuti lavoratori per un importo complessivo di 650.000 Euro. Tra i più gravi problemi riscontrati nel carcere di Rebibbia, che ha nel Reparto Nuovo Complesso 1.670  detenuti, con una capienza di 1.200, vi è quello dei gravi tagli fatti alla manutenzione delle strutture che sono per tutto l’anno pari a 50.000 Euro. Come termine di paragone basti pensare che il Palazzo del Consiglio Regionale del Lazio, di Via della Pisana a Roma, ha una destinazione per la manutenzione di un importo di oltre 8 milioni e mezzo di Euro ogni anno.