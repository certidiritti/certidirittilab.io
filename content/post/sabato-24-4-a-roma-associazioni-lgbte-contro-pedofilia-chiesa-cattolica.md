---
title: 'SABATO 24-4 A ROMA ASSOCIAZIONI LGBT(E) CONTRO PEDOFILIA CHIESA CATTOLICA'
date: Fri, 23 Apr 2010 13:49:40 +0000
draft: false
tags: [Comunicati stampa]
---

**DOMANI, SABATO 24 APRILE, MANIFESTAZIONE PER VITTIME PEDOFILIA DELLA CHIESA CATTOLICA A ROMA, PIAZZA SS. APOSTOLI. VIETATO MANIFESTARE DI FRONTE AL VATICANO.**

**COMUNICATO STAMPA ASSOCIAZIONE RADICALE CERTI DIRITTI:**

  

Domani, sabato 24 aprile, l’Associazione Radicale Certi Diritti, insieme a decine di altre associazioni lgbt(e), tra le altre, Arcigay, DiGayProiect, Circolo Mario Mieli, Arcilesbica, Agedo, Famiglie Arcobaleno, Mit, co-promuovono una manifestazione a Roma in sostegno delle vittime innocenti della pedofilia nella chiesa cattolica. La manifestazione, contro l’omertà delle gerarchie cattoliche e le gravissime affermazioni del Camerlengo Bertone, che anzichè parlare degli scandali associa con gravissime e antiscientifiche tesi l’omosessualità alla pedofilia, si svolgerà in Piazza Ss. Apostoli a partire dalle ore 16.

La manifestazione si doveva svolgere nel territorio italiano antistante la Città del

Vaticano ma le autorità lo hanno impedito. Anche altre piazze pedonali vicine sono state vietate.

Manifestazione analoghe sono previste contemporaneamente in decine di altre città in tutto il mondo, davanti alle sedi delle nunziature apostoliche e chiese principali, grazie alla mobilitazione dell’International Lesbian and Gay Association che ha mobilitato le organizzazioni nazionali lgbt.