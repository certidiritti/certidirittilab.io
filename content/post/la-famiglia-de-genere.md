---
title: 'La famiglia de-genere'
date: Fri, 23 Apr 2010 19:13:23 +0000
draft: false
tags: [Senza categoria]
---

su matrimonio, omosessualità e Costituzione

di **Matteo Bonini Baraldi**

_[Mimesis Edizioni](http://www.mimesisedizioni.it/)_, 2010

Collana LGBT, a cura di Francesco Bilotta

Nel corso del 2009 varie coppie di persone dello stesso sesso hanno deciso di richiedere al proprio Comune di residenza le pubblicazioni matrimoniali. In seguito al rigetto della richiesta, i protagonisti di questa vicenda hanno perseguito le vie legali per affermare il loro diritto a contrarre matrimonio civile.

Piuttosto che avallare definizioni essenzialiste del concetto di famiglia, il volume analizza il quadro giuridico italiano e straniero in un'ottica innovativa, chiedendosi se l'esclusione dal matrimonio possa essere giustificata da un interesse pubblico preminente.

Ne emerge come, lungi dal costituire una questione rilevante solo per una scomoda minoranza, in termini di rispetto delle identità, la questione del matrimonio tra persone dello stesso sesso è problema che riguarda tutti, cittadini e non cittadini, uomini e donne, eterosessuali e omosessuali, perché interroga il concetto di persona e di libertà, la definizione dei ruoli di genere in una famiglia che cambia, il rapporto fra norma e corpo, il significato del principio di eguaglianza in una moderna democrazia.

Pubblicate i vostri commenti sul blog: [http://lafamigliadegenere.wordpress.com/](http://lafamigliadegenere.wordpress.com/)

Per i vostri ordini: [http://www.mimesisedizioni.it/archives/001313.html#001313](http://www.mimesisedizioni.it/archives/001313.html#001313)

Per contatti: [famigliadegenere@gmail.com](mailto:famigliadegenere@gmail.com)