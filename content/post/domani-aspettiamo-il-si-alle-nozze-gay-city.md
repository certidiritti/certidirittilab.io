---
title: 'Domani aspettiamo il “sì” alle nozze gay  - City'
date: Wed, 24 Mar 2010 11:05:10 +0000
draft: false
tags: [Senza categoria]
---

**IVAN SCALFAROTTO Vicepresidente del Pd, ha firmato l'appello "Sì, lo voglio!" per il matrimonio omosessuale. Con la speranza che domani diventi legale.**

[City](http://city.corriere.it/2010/03/22/interviste.shtml)

**Cosa succede domani, 23 marzo?**

La Corte costituzionale si riunisce per valutare se l’impossibilità di sposarsi, in Itala, per due persone dello stesso sesso sia conforme alla Costituzione oppure no. Se ne dichiarerà la incostituzionalità, dal giorno dopo, una coppia di due donne o una coppia di due uomini potrebbe andare a fare le pubblicazioni e sposarsi.

**In Italia?**

Sì, potrebbe succedere proprio domani.

**Non se ne è quasi sentito parlare...**

Non se ne vuole parlare. Credo che chi vuole negare pieni diritti delle persone gay e lesbiche sappia benissimo cosa succede questo 23 marzo, e che si stia anche muovendo.

**In che senso?**

Penso che la Corte costituzionale sia sottoposta a pressioni perché dica no al riconoscimento dei matrimoni. Però sono sicuro che non si farà influenzare.

**Ma chi è a fare pressioni, secondo lei?**

Credo che in Vaticano piuttosto che far passare il matrimonio gay farebbero le barricate.Il Nunzio apostolico di Parigi a gennaio ha persino spedito una lettera “riservata” con indicazioni di voto in proposito al parlamento europeo.

**Cosa conteneva?**

C’era scritto che il Vaticano aveva già chiesto ad alcuni europarlamentari, tra cui Luca Volontè dell’Udc, di non votare la risoluzione che indirizza i Paesi Ue a riconoscere le unioni gay, il diritto ad adottare e la potestà congiunta sui figli della coppie omosessuali. Con tanti di promemoria sugli emendamenti da respingere.

**Ma cosa c’entrano eventuali pressioni politiche col fatto che in Italia non c’è stato dibattito sulla decisione della Consulta?**

Intanto, ultimamente, i dibattiti in Italia sono stati soppressi tutti. Ma il punto è che quando si discute, oltre a sentire le argomentazioni contro, si ascoltano anche quelle a favore. E le nostre sono molto solide.

**Per esempio?**

Chi non vuole le nozze gay si basa solo su ragioni storiche. Ma se ciò che è tradizionale fosse anche giusto, allora le donne non dovrebbero votare. Fino al 1965 le donne non potevano neppure fare i magistrati! Sempre nel 1965 Franca Viola fu la prima donna a respingere il matrimonio riparatore: fino ad allora le donne potevano essere stuprate “gratis”, a condizione che il violentatore poi le sposasse.

**Al pronunciamento Costituzionale si è arrivati grazie a una battaglia giuridica del movimento gay.**

Sì, alcune coppie gay o lesbiche italiane sono andate in Comune e hanno detto: ci vorremmo sposare. L’ufficiale di Stato civile ha sempre rifiutato loro la pubblicazione di matrimonio, che è il presupposto per sposarsi. A quel punto le coppie hanno fatto ricorso, perché nella Costituzione non c’è scritto che il matrimonio è solo tra un uomo e una donna.

**Ah no?**

Si parla di coniugi. Nell’articolo 29 si dice che la famiglia è una società naturale basata sul matrimonio e che “il matrimonio è ordinato sull’eguaglianza morale e giuridica dei coniugi”.

**Neppure nel codice civile?**

Lì si fa riferimento al “marito” e alla “moglie” solo in articoli secondari. E tra le cause che “ostano” al matrimonio, si dice che i coniugi non devono essere consanguinei, o già sposati. Da nessuna parte si parla del loro sesso.

**In effetti quando in Spagna il premier José Luis Zapatero ha legalizzato le nozze gay, ha sostituito i termini marito e moglie con coniuge. Ma perché in Italia si è scelta la via legale invece che quella politica?**

Il Paese ha dei bisogni: compito della politica sarebbe interpretarli. Se non ci riesce, la gente si rivolge al magistrato.

**Lei quindi è dell’idea che la società sia più avanti del parlamento?**

Questo è un parlamento che non riesce neanche a dire che l’omofobia va condannata. Che parlamento è, un parlamento che non riesce a dire che l’odio è sbagliato?

**E a chi parla di “attacco alla famiglia tradizionale”, cosa dice?**

Quella che conosciamo oggi non è affatto la famiglia tradizionale! Nella famiglia tradizionale l’uomo aveva pieno potere su moglie e figli... se non è più così è perché la politica degli anni ‘70 ha avuto il coraggio di leggere la realtà per com’era. Oggi dobbiamo fare lo stesso: se ci sono due uomini o due donne che si amano e si proteggono e vivono come una famiglia, devono essere considerati una famiglia esattamente come tutte le altre.

**Nell’appello [“Sì lo voglio” vi rivolgete a tutti i cittadini. Ma quella per il matrimonio gay viene spesso vista come una battaglia solo omosessuale](http://www.affermazionecivile.it/appello/ "www.affermazionecivile.it"). È così?**

I gay e le lesbiche sono cittadini a pieni doveri ma non a pieni diritti. E la battaglia per l’uguaglianza non è patrimonio solo di chi ne è escluso. Non ci sarebbe mai stato un presidente nero degli Usa se non l’avessero votato anche i bianchi. Il matrimonio civile tra persone dello stesso sesso è un diritto fondamentale della persona, non può essere negato ancora.

**Secondo lei, alla fine, la Corte costituzionale cosa deciderà?**

Sono molto ottimista: secondo me la Costituzione parla chiaro. E il concetto di famiglia è cambiato.