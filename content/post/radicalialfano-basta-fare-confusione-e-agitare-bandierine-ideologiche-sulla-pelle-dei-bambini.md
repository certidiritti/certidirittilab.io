---
title: 'RADICALI/ALFANO: BASTA FARE CONFUSIONE E AGITARE BANDIERINE IDEOLOGICHE SULLA PELLE DEI BAMBINI.'
date: Wed, 06 Jan 2016 16:21:23 +0000
draft: false
tags: [Diritto di Famiglia]
---

**[![coppia-gay_AFFIDAMENTO](http://www.certidiritti.org/wp-content/uploads/2015/10/coppia-gay_AFFIDAMENTO-300x175.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/10/coppia-gay_AFFIDAMENTO.jpg)RADICALI/ALFANO: BASTA FARE CONFUSIONE E AGITARE BANDIERINE IDEOLOGICHE SULLA PELLE DEI BAMBINI.**

**Comunicato Stampa **dell’Associazione Luca Coscioni, **dell’Associazione Radicale Certi Diritti e di Radicali Italiani.**

Roma 6 gennaio 2015

Usa parole forti il ministro Alfano oggi su Avvenire dicendo che la _stepchild adoption_ (adozione del figlio del partner) porterebbe il Paese «verso il mercimonio più ripugnante che l’uomo abbia saputo inventare», confondendo ancora una volta piani completamente diversi quali l’adozione del figlio del partner e la maternità surrogata.

O il ministro ignora le leggi italiane oppure fa una speculazione, quella sì ripugnante, sulla pelle dei bambini. L’adozione del figlio del partner esiste infatti nell’ordinamento italiano sin dal 1983 (!) essendo prevista dalla legge sulle adozioni all’articolo 44, comma 1, lettera b), (legge 4 maggio 1983, n. 184). Il DDL Cirinnà, non fa altro che proporne l’estensione alle coppie dello stesso sesso unite civilmente. Perché mai allora Alfano solo oggi si preoccupa così tanto? L’unica risposta può trovarsi nella più bieca omofobia della sua politica. Una prospettiva agghiacciante per un ministro di un Paese occidentale e più degna di altre latitudini.

Ricordiamo inoltre al ministro che la giurisprudenza - dapprima sovranazionale (Corte europea dei diritti dell'uomo, sentenza X c. Austria del 19 febbraio 2013) e poi anche nazionale (da ultimo, Corte d'appello di Roma, 23 dicembre 2015) - già ha riconosciuto la possibilità di disporre l’adozione del figlio del partner all'interno di famiglie omogenitoriali. Il ministro quindi propone di violare la Convenzione europea dei diritti dell’uomo?

Inoltre, i giudici potrebbero continuare a disporre la _stepchild adoption_ a favore del partner omosessuale del genitore biologico del minore con il risultato di affidare un tema così delicato alla mera discrezionalità dei giudici infliggendo un altro colpo alla già precaria certezza del diritto in questo disgraziato Paese. Ma un ministro di una Repubblica liberal-democratica, non dovrebbe invece rafforzare lo Stato di Diritto?

Alfano, pensi a fare il ministro occupandosi di temi di sua competenza e la smetta di parlare a sproposito di maternità surrogata che è vietata in Italia dal 2004 e tale rimarrà anche dopo l’approvazione del DDL Cirinnà, così come è rimasta tale nonostante l’articolo 44, comma 1, lettera b) della legge sulle adozioni.

È però giunto il momento di dire una parola chiara anche sulla GPA (gestazione per altri) che Alfano vorrebbe vietare universalmente punendo chi vi ricorre addirittura con il carcere. I pruriti proibizionisti del ministro dell’Interno sono assai pericolosi poiché rischierebbero di alimentare un mercato clandestino che andrebbe a rafforzare proprio quelle sacche di sfruttamento che si vorrebbero eliminare.

Ciò che bisognerebbe fare, invece, sarebbe incoraggiare pratiche più trasparenti, legali e regolamentate nel pieno rispetto del principio di autodeterminazione dell’individuo, senza questi riflessi paternalisti francamente stucchevoli. Noi rifiutiamo l’idea di donne incapaci di fare delle scelte consapevoli e di vivere nel proprio corpo esperienze coerenti con il proprio pensiero e condividiamo l’articolata [**posizione dell’Associazione Radicale Certi Diritti**](http://www.certidiritti.org/2011/11/23/mozione-particolare-sulla-gestazione-per-altri-approvata-dal-viii-congresso/), per la quale, tra l’altro, è da ritenersi necessaria ed auspicabile anche una compensazione in denaro tutelata dalle leggi dello Stato nell’interesse della donna stessa.

Filomena Gallo, Associazione Luca Coscioni

Yuri Guaiana, Associazione Radicale Certi Diritti

Riccardo Magi, Radicali Italiani