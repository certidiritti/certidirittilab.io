---
title: 'L''amore Spiazza - Il video'
date: Fri, 12 Nov 2010 15:48:50 +0000
draft: false
tags: [Senza categoria]
---

Siamo felici di presentarvi questo video che promuove ["L'Amore Spiazza"](http://lamorespiazza.blogspot.com/), viaggi itineranti organizzati dal Coordinamento Arcobaleno LGBT, di Milano. 

Lo scorso anno abbiamo lasciato un segno nelle città di Magenta, Bergamo e Pavia... e quest'anno riprenderemo con nuove iniziative!