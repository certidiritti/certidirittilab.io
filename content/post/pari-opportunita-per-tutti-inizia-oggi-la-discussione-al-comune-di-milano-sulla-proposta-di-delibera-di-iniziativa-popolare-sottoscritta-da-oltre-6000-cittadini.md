---
title: 'Pari opportunità per tutti: inizia oggi la discussione al Comune di Milano sulla proposta di delibera di iniziativa popolare sottoscritta da oltre 6.000 cittadini'
date: Thu, 18 Apr 2013 07:18:24 +0000
draft: false
tags: [Politica]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Milano, 18 aprile 2013

Finalmente parte la discussione delle proposte di delibera d'iniziativa popolare sulle quali il comitato "Milano Radicalmente Nuova" ha raccolto oltre 6000 firme. In particolare oggi alle 14.30 le commissioni consiliari congiunte "Pari opportunità" e "Referendum approvati, Iniziativa Popolare, Digitalizzazione, Trasparenza, Agenda digitale" discuteranno la proposta in materia di "prevenzione, contrasto e assistenza alle vittime di discriminazione e per la promozione di pari opportunità per tutti".

La proposta chiede di "promuovere e sviluppare specifiche iniziative volte alla rimozione delle cause di discriminazione ed alla prevenzione di atti di violenza, maltrattamenti e discriminazione diretta ed indiretta, relative agli ambiti di cui all'articolo 19 del TFEU": etnia, religione, età, genere, disabilità, orientamento sessuale. La delibera inoltre chiede che venga adottato un "Piano comunale contro le discriminazioni e per le pari opportunità per tutti".

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: "Il comune di Milano ha già adottato un protocollo d'intesa con l'UNAR, questa proposta va nella direzione di dare piena attuazione a questo protocollo d'intesa con azioni concrete, possibilmente coordinate e non lasciate al caso o alla benevolenza di un singolo assessore. Per questo chiediamo che venga creata una specifica struttura, come già ha fatto la città di Bologna e varie regioni quali Piemonte, Emilia-Romagna, Liguria, Puglia, Toscana e Calabria. Gli enti locali hanno un ruolo fondamentale e, addirittura, prioritario nel prevenire e combattere le discriminazioni per la loro prossimità al territorio e la capillare azione culturale e di monitoraggio che possono svolgere. Spero che il Consiglio comunale voglia portare Milano al livello di altre città e regioni italiane ed europee, su temi fondamentali per la vita di tutti. Le discriminazioni e le pari opportunità, non riguardano infatti solo le minoranze, ma tutti i cittadini poiché tutti, nel corso della vita, potremmo essere portatori di un fattore di discriminazione: non a caso oltre 6000 cittadini hanno firmato questa proposta. Confido che il Consiglio comunale saprà ascoltare la voce di questi cittadini e saprà dare sostanza all'art. 3 della Costituzione nonché agli articoli 1 e 5 dello Statuto di Milano che parlano esplicitamente di pari opportunità e uguaglianza di trattamento senza distinzione di età, sesso, razza, lingua, religione, opinione e condizione personale o sociale, approvando questa delibera".