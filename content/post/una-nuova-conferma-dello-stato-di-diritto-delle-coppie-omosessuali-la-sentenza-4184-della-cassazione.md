---
title: 'UNA NUOVA CONFERMA DELLO STATO DI DIRITTO DELLE COPPIE OMOSESSUALI: LA SENTENZA 4184 DELLA CASSAZIONE'
date: Sun, 25 Mar 2012 09:36:31 +0000
draft: false
tags: [Diritto di Famiglia]
---

La sentenza 4184 della cassazione, del 13 Marzo 2012, è una nuova, fondamentale conquista per i diritti delle persone gay e lesbiche del nostro paese.

  

E’ una sentenza attesa da anni, sin da quando Antonio e Mario, coppia di Latina sposata in Olanda, chiese al proprio comune la trascrizione del matrimonio. A suo tempo ci pensò la circolare Amato a bloccare ogni cosa: vietato trascrivere per motivi di “ordine pubblico” i matrimoni contratti all’estero quando la coppia è formata da due persone dello stesso sesso.

Da qui è iniziato un lungo percorso giudiziario seguiti dall’avvocato Francesco Bilotta (socio e cofondatore della Rete Lenford). Pur perdendo il ricorso perché il matrimonio tra persone dello stesso sesso non è ancora contemplato dall’ordinamento, grazie alla sentenza che è molto articolata e argomentata, si ottengono dei buoni risultati a livello di diritti per le coppie omosessuali.   

  

Cosa afferma questa sentenza? Come premessa occorre dire che ripercorre molte delle istanze già espresse dalla Corte Costituzionale nella sentenza 138/2010. Tuttavia include anche alcune novità, e soprattutto è strutturata secondo una analisi molto approfondita e chiara, senza quegli ampi spazi di ambiguità presenti nella sentenza costituzionale.

Prendendo spunto dalle considerazioni pubblicate da Gattuso su questa sentenza, proviamo a sintetizzarne i pregi, consapevoli che solo grazie ad una analisi approfondita delle motivazioni si potrà poi individuare esattamente gli obiettivi raggiunti e le conquiste ancora da raggiungere nel campo del Diritto, così da rimodulare le varie iniziative giudiziarie in atto.

  

1) Il matrimonio non è solo tra uomo e donna. Per la nostra Costituzione, non ci sono vincoli di diritto naturale inteso come necessaria differenza tra i sessi, per accedere all’istituto matrimoniale.

  

2) Di conseguenza, il Parlamento è libero di legiferare sul matrimonio gay che **non è anticostituzionale** e in questo modo toglie alcune delle argomentazioni più invocate dai  nostri oppositori.

  

3) In particolare viene confutato il motivo di  "ordine pubblico" interno e internazionale, quello internazionale, in particolare, perché il matrimonio omosessuale non è "inesistente" o “non valido”semplicemente non può produrre effetti a causa dell’attuale ordinamento italiano.

  

4) Le coppie dello stesso sesso godono della piena garanzia alla vita famigliare (art. 8 della convenzione dei diritti dell'uomo)

  

5) Tutte le coppie che subiscono disparità di trattamento rispetto alle coppie sposate hanno diritto ad adire all'Autorità Giudiziaria

  

6) Questi diritti di cui si parla sono relativi sia alle persone che compongono la famiglia, sia alla famiglia stessa

  

E’ molto importante il fatto che questi argomenti sono in piena sintonia con quelle interpretazioni della sentenza della Corte costituzionale 138/2010 che Certi Diritti ha fatto proprie sin dall’inizio per incardinare le nuove strategie di iniziativa giudiziaria, che ci aspettiamo saranno rafforzate e confermate da questa sentenza.

  

Infatti, mantenendo come obiettivo il raggiungimento del Matrimonio civile per le coppie dello stesso sesso, Certi Diritti si muove per ottenere la revisione e modernizzazione del Diritto di famiglia che in Italia è stato adottato nel 1975 e da allora mai rimaneggiato:

*   siamo  in attesa di conoscere le decisioni della CEDU circa l’accoglimento del ricorso depositato sul diritto al matrimonio civile;
*   sosteniamo la proposta di riforma del Diritto di famiglia che dal 6 luglio 2010 è depositata ([n. 3067](http://www.camera.it/Camera/view/doc_viewer_full?url=http%3A//www.camera.it/_dati/leg16/lavori/stampati/pdf/16PDL0040880.pdf&back_to=http%3A//www.camera.it/126%3FPDL%3D3607%26leg%3D16%26tab%3D2), prima firmataria l'On. Rita Bernardini) che contiene il riconoscimento delle unioni non matrimoniali, la libertà di procreazione assistita, la possibilità del divorzio breve, ma questa proposta  non è ancora stata  neppure calendarizzata .
*   seguiamo varie iniziative politiche e istituzionali: la pubblicazione dei libri  **“_Dal cuore delle coppie al cuore del diritto_”** che contiene gli atti dell’udienza e la sentenza commentata della Corte Costituzionale 138/10 e **_“Certi diritti che le coppie conviventi non sanno di avere- per vivere consapevolmente in coppia o in attesa di giuste nozze_**”, la raccolta di firme in calce alle petizioni popolari che chiedono l’istituzione dei registri delle coppie non matrimoniali nelle maggiori città italiane, la richiesta di abrogazione della Circolare n. 55 del 2007 la cosiddetta Circolare Amato che vieta la trascrizione in Italia dei matrimoni tra persone dello stesso sesso celebrati all’estero.
*   Continuiamo a istruire nuove cause pilota per l’affermazione di ogni singolo diritto negato alle coppie non matrimoniali adottando specifiche strategie giudiziarie, parallelamente o congiuntamente ad iniziative simili promosse da  altre coppie e/o associazioni.