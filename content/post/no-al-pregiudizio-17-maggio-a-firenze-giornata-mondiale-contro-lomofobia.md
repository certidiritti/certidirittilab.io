---
title: 'NO AL PREGIUDIZIO, 17 MAGGIO A FIRENZE GIORNATA MONDIALE CONTRO L''OMOFOBIA'
date: Tue, 13 May 2008 20:16:21 +0000
draft: false
tags: [arcigay firenze, certi diritti, Comunicati stampa, everypnegrouop, Firenze, Giornata mondiale contro l'omofobia, makwan]
---

A Firenze, presso l’Altana (ex convento delle Leopoldine), [Piazza Tasso 1  
zona Porta Romana](http://maps.google.it/maps?q=piazza+tasso+1,+firenze&ie=UTF8&ll=43.769963,11.239293&spn=0.006694,0.019548&z=16) alle ore 21,15. Entrata libera e buffet di benvenuto.  
  
**DIRE NO AL PREGIUDIZIO:** un incontro aperto alla cittadinanza con persone che hanno vissuto sulla propria pelle la brutalità della discriminazione a causa del proprio orientamento sessuale o della propria identità di genere e un cortometraggio dedicato a Makwan, impiccato in Iran perché gay  
   
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
Con la partecipazione dell’Associazione Radicale “Certi Diritti”, che dedica le sue iniziative alla memoria di Makwan Moloudzadeh e a coloro che hanno sofferto abusi, discriminazioni e violenze a causa del proprio orientamento sessuale e i patrocini della  Provincia di Firenze  
![logomak.jpg](http://www.certidiritti.org/wp-content/uploads/2008/05/logomak.jpg "logomak.jpg")  
  
In ricordo di Makwan Moloudzadeh, 21enne giustiziato il 5 dicembre 2007 nel carcere di Kermanshah (Iran) con l'accusa di sodomia, in seguito a un rapporto omosessuale intrattenuto a 13 anni con un coetaneo, e di tutte le vittime dell’omofobia e della transfobia.  
  
**DIRE NO AL PREGIUDIZIO:** un incontro aperto alla cittadinanza con persone che hanno vissuto sulla propria pelle la brutalità della discriminazione a causa del proprio orientamento sessuale o della propria identità di genere  
  
Una serata per stare insieme, riflettere, interrogarsi  
e dire no all’omofobia e alla transfobia, sì al rispetto e ai diritti umani!  
  
Presenti esponenti delle autorità locali, della comunità omosessuale e transgender e una testimone lesbica iraniana.  
   
Seguirà la proiezione in anteprima internazionale del cortometraggio “Makwan. Lettera dal Paradiso” a cura del Gruppo EveryOne  
  
**Info**  
_www.arcigayfirenze.it :: www.everyonegroup.com  
Tel: 334 8429527 – 338 1747342  
17maggio@arcigayfirenze.it  
info@everyonegroup.com_  
  
![loghi.jpg](http://www.certidiritti.org/wp-content/uploads/2008/05/loghi.jpg "loghi.jpg")