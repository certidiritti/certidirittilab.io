---
title: 'BRAD PITT: SI AI MATRIMONI GAY, E'' UNA BATTAGLIA DI LIBERTA'''
date: Fri, 09 Jan 2009 17:02:35 +0000
draft: false
tags: [BRAD PITT, Comunicati stampa, GAY, GAY.TV, MATRIMONI]
---

DA [WWW.GAY.TV](http://www.GAY.TV)

08/01/2009)  L\`attore americano si è pesantemente sbilanciato con il magazine \`W` a favore dei matrimoni gay: "Cosa succederebbe se un giorno qualcuno dicesse:niente cattolici? O niente ebrei? O niente mormoni? Nessuno potrebbe sopportarlo, io per primo".

Le persone che negano a gay e lesbiche il diritto di vivere assieme legalmente non possono dirsi portatori di libertà. Questo è quanto ha dichiarato **Brad Pitt al magazine statunitense 'W'.** La star, che già [in settembre aveva donato 100mila dollari a favore dei matrimoni gay](http://www.gay.tv/ita/magazine/we_like/dettaglio.asp?i=6305) durante la battaglia per **la Proposition 8** ha infatti dichiarato che _"le persone contro il matrimonio gay non capiscono le libertà fondamentali che loro stessi godono"._

[http://www.gay.tv/ita/magazine/we\_like/dettaglio.asp?i=6672&chan=114wrd=BRAD\_PITT:_'CHI\_NON\_SOSTIENE\_I\_MATRIMONI\_GAY\_NON\_PUO'\_ESSERE\_PORTATORE\_DI_LIBERTA''](http://www.gay.tv/ita/magazine/we_like/dettaglio.asp?i=6672&chan=114wrd=BRAD_PITT:_'CHI_NON_SOSTIENE_I_MATRIMONI_GAY_NON_PUO'_ESSERE_PORTATORE_DI_LIBERTA'')