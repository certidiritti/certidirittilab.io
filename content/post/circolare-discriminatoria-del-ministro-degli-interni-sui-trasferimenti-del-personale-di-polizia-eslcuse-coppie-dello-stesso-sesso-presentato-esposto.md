---
title: 'Circolare discriminatoria del Ministro degli Interni sui trasferimenti del personale di polizia, eslcuse coppie dello stesso sesso. Presentato esposto'
date: Tue, 22 May 2012 11:23:05 +0000
draft: false
tags: [Diritto di Famiglia]
---

Riconosciute le coppie sposate e quelle conviventi, escluse quelle dello stesso sesso. Esposto al minsitro degli Interni, al capo della polizia, all'Unar e ALL'Oscad.

Roma, 22 maggio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Lo scorso 14 maggio è stata diffusa una Circolare del Dipartimento Pubblica sicurezza del Ministero dell’Interno (N. 333-A/9807.E.1/3368-2012 del 14 maggio 2012) avente per oggetto: “Disciplina della mobilità a domanda del personale della Polizia di Stato dei ruoli di sovrintendenti, assistenti, e agenti, che aspira a cambiare sede di servizio”.

L’Associazione Radicale Certi Diritti ha inviato una Lettera-Esposto al Ministro degli Interni, al Capo della Polizia, all’Unar, all’Oscad e, per conoscenza ai Sindacati di Polizia,  segnalando che  a pagina 6  della Circolare, nella nota n. 1, in merito alla “situazione familiare” vi è scritto testualmente che “I punteggi previsti per le esigenze del nucleo familiare si intendono estesi alle analoghe esigenze per le eventuali famiglie di fatto, intendendosi per tale quella costituita da due persone di sesso diverso che convivono, more uxorio, coabitando stabilmente insieme agli eventuali figli naturali riconosciuti o dichiarati dall’uno o da ambedue nella sede per cui si richiede il trasferimento, ovvero in sede limitrofa a quest’ultima. La coabitazione deve risultare da certificazione anagrafica.”

Pur essendo la circolare innovativa rispetto alla situazione normativa nazionale, in quanto prevede l’applicazione dei benefici previsti per le famiglie matrimoniali anche a quelle non matrimoniali, contiene in sé una condizione discriminatoria. Infatti la previsione del “sesso diverso” viola in modo manifesto l’art. 21, comma 1, punto b) del della Legge 4 novembre 2010, n. 183, “Misure atte a garantire pari opportunità, benessere di chi lavora e assenza di discriminazioni sul posto di lavoro”.

Nella Lettera-Esposto viene anche ricordata la sentenza della Corte Costituzionale n. 138/2010 che riconosce il valore costituzionale dell’unione omosessuale - in quanto formazione sociale riconosciuta e garantita dall’art. 2 cost. - “cui spetta il diritto fondamentale di vivere liberamente una condizione di coppia”.

L’Associazione Radicale Certi Diritti chiede che venga al più presto rivisto il contenuto del testo della Circolare escludendo la condizione discriminatoria in essa contenuta nei confronti delle persone omosessuali.