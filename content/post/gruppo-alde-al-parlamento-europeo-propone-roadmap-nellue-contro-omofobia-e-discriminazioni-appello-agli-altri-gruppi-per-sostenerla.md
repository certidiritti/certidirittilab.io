---
title: 'Gruppo Alde al Parlamento Europeo propone RoadMap nell''Ue contro omofobia e discriminazioni. Appello agli altri gruppi per sostenerla'
date: Tue, 13 Dec 2011 13:33:46 +0000
draft: false
tags: [Europa]
---

Il gruppo dei Liberali e Democratici al Parlamento europeo propone relazione su RoadMap contro l'omofobia nei paesi dell'Unione Europea.

Comunicato Stampa dell'Associazione Radicale Certi Diritti  
   
Roma-Bruxelles, 13 dicembre 2011  
   
Il gruppo ALDE (gruppo Liberale e Democratico Europeo) ha proposto oggi in sede di Commissione Libertà Pubbliche, Giustizia ed Affari Interni (LIBE) del Parlamento europeo, una relazione d'iniziativa sulla "Roadmap UE con azioni contro l'omofobia e le discriminazioni basate sull'orientamento sessuale".  Sempre su proposta del gruppo ALDE, il PE ha ripetutamente chiesto alla Commissione europea di elaborare una tabella di marcia contenente una serie di misure concrete per la lotta contro l'omofobia e le discriminazioni basate sull'orientamento sessuale, sia nella "risoluzione del PE del 19 gennaio 2011 sulla violazione della libertà di espressione e le discriminazioni basate sull'orientamento sessuale in Lituania", sia in quella "del 17 febbraio 2011 sull'Uganda: l'uccisione di David Kato".  
   
La Commissione europea ha risposto negativamente a questa richiesta del PE, come accaduto nel corso della conferenza organizzata dall'intergruppo del PE per i diritti LGBT lo scorso 8 Dicembre, alla quale é intervenuto Ottavio Marzocchi, responsabile per le questioni europee di Certi Diritti, nonché funzionario del gruppo ALDE per la commissione LIBE, criticando la Commissione europea per tale decisione, per la costante minimizzazione del potere di intervento della Commissione nelle questioni relative ai diritti umani negli Stati membri e per chiedere se la Commissione intendesse evitare nella sua futura proposta sul mutuo riconoscimento degli effetti dei documenti di stato civile nella UE la questione del matrimonio.  
   
Allo scopo di spingere la Commissione europea a elaborare tale Roadmap, il gruppo ALDE ha deciso di proporre di elaborare una relazione al riguardo al PE. L'Associazione Radicale Certi Diritti si felicita per l'iniziativa del gruppo ALDE e si augura che gli altri gruppi politici vogliano appoggiarla al fine di ottenere una pronuncia del Parlamento Europeo a favore di una Roadmap europea contro l'omofobia e le discriminazioni delle persone sulla base del loro orientamento sessuale.