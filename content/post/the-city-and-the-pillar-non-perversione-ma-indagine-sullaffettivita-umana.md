---
title: 'The City and the Pillar, non perversione ma indagine sull''affettività umana'
date: Thu, 02 Aug 2012 11:24:47 +0000
draft: false
tags: [Politica]
---

di Leonardo Monaco, Giunta di Segreteria dell'Associazione Radicale Certi Diritti

Impossibile ad un giorno dalla morte dello scrittore statunitense Gore Vidal – già ricordato come Radicale, antiproibizionista e attivista contro la pena di morte – non dedicare un capitolo a parte al grande contributo che offrì alla causa della liberazione sessuale e dell'emancipazione LGBT nell'immediato secondo Dopoguerra.

L'opera, che molti faticano a ricordare per eccesso di perbenismo e che ancora più persone considerano un tassello fondamentale della propria crescita, è una delle pietre miliari della letteratura di sviluppo: “The City and the Pillar”.

Pubblicato nel 1948 negli Stati Uniti sbarca in Italia un anno dopo con il titolo de “La città perversa” (a questa infelice traduzione seguiranno “Jim” e il più azzeccato “La statua di sale”).

Vidal, scrittore dichiartamente omosessuale (ma poco ci importa in questa analisi), ci offre un'opera che ha del sensazionale. Rompe gli schemi della letteratura moralista dell'epoca e dà alla luce un personaggio “omosessuale normale”. Una sfida a tutti i romanzieri dell'epoca che avevano già trattato il tema dell'omosessualità fino ad all'ora: Jim – il protagonista – è diverso dallo stereotipo dell'omosessuale in corpo 11 che ha retto fino alla prima metà del '900. Con la premessa che non si tratta in maniera più assoluta del primo romanzo gay della storia contemporanea, l'omosessualità in Vidal si discosta dal cliché della “sodomia” come sublimazione astratta del nulla a procedere.

Il gay, da ragazzo complessato e disturbato, violento contro sé e gli altri, che alla fine di un'approssimativa indagine intima condotta armato di poveri rudimenti di psicologia spicciola scopre la sua devianza e si autodistrugge facendo un dono all'umanità; diventa una meravigliosa pentola a pressione di sentimenti, passione, progetti, speranze. Specifico: un uomo normale, prima ancora di un “omosessuale normale”.

Jim è il mio vicino di casa, il mio collega. E' allo stesso tempo etero-gay-bisessuale-transessuale... proprio perché lasciandosi trasportare dalla Vita non sacrifica nulla di questa: Vidal crea un personaggio che si innamora, che vuole soddisfare l'appetito sessuale, che odia, ama, si ingelosisce; che dall'alto della sua imperfezione umana cede ai tranelli della vita (a volte cavandosela e a volte fallendo miseramente).

Il finale sconvolgente, da pugno nello stomaco, oggetto di forti critiche e troppo spesso travisato, è una potente metafora dell'essere umano standard (non re di vitù e neanche primo dei miserabili) che dà corpo alla frustrazione di non esser riuscito a raggiungere in pieno l'oggetto del desiderio.

Non annoveriamo questa opera nel filone della letteratura a tematica gay, affinché la volontà dell'autore di trasformare il diverso in normale non sia vanificata. Regalatelo ai vostri giovani amici o ai vostri figli come fosse un “Siddharta” o un “Giovane Holden”; ricordate questo grande autore leggendo questa estate il suo grande capolavoro.

*   Gore Vidal, _La statua di sale_, collana Fazi Tascabili, traduzione di Alessandra Osti, Fazi Editore, 2001, pp. 222. ISBN 8881121948
    
*   Gore Vidal, _The City and the Pillar and Seven Early Stories_ (1st ed.). New York: Random House, 1995 ISBN 978-0-679-43699-7.