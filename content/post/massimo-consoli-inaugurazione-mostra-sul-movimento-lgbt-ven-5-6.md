---
title: 'MASSIMO CONSOLI: INAUGURAZIONE MOSTRA SUL MOVIMENTO LGBT VEN 5-6'
date: Tue, 02 Jun 2009 06:37:43 +0000
draft: false
tags: [Comunicati stampa]
---

**Massimo Consoli: 40 anni di Movimento, 50 anni di Archivio**

Associazione Fondazione Luciano Massimo Consoli  
Arcigay Roma  
Comitato promotore Arcigay L´Aquila Massimo Consoli

presentano:  
MASSIMO CONSOLI: 40 ANNI DI MOVIMENTO 50 ANNI DI ARCHIVIO  
dal 5 giugno al 3 luglio 2009

con il patrocinio di:  
Comune di Roma - Assessorato alle Politiche Culturali  
Provincia di Roma - Assessorato alle Politiche Culturali  
Regione Lazio  
   
OPENING COCKTAIL  
venerdì 5 giugno | ore 19.00  
Arcigay Roma - via Nicola Zabaglia 14  
   
Massimo Consoli era un personaggio poliedrico. Aveva letto molto e molto viaggiato per conoscere di persona i vari movimenti, giornali, gruppi e personaggi gay e non-gay di tutto il mondo. Ha pubblicato numerosi libri tra saggi storici e religiosi, raccolte di poesia e commedie, tragedie e romanzi, racconti e traduzioni. Lo hanno descritto per lungo tempo come il  
"fondatore del movimento gay italiano": in realtà, l´unica definizione che gli piaceva era quella di "segretario di Dio"

[http://www.arcigayroma.it/arcigayroma/index.asp?id_dettaglio=1636&id=55&sezione=News](http://www.arcigayroma.it/arcigayroma/index.asp?id_dettaglio=1636&id=55&sezione=News)