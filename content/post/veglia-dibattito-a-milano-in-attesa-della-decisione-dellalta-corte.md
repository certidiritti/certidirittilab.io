---
title: 'VEGLIA-DIBATTITO A MILANO IN ATTESA DELLA DECISIONE DELL''ALTA CORTE'
date: Fri, 12 Mar 2010 23:32:04 +0000
draft: false
tags: [Senza categoria]
---

Il 23 marzo 2010 la Corte Costituzionale si pronuncerà circa l'incostituzionalità del divieto di celebrare matrimoni fra persone dello stesso sesso.

In particolare, la Corte si pronuncerà su due dei quattro ricorsi, presentati ai tribunali di Trento, Venezia, Ferrara e Firenze, da coppie di persone dello stesso sesso, per impugnare in sede giudiziale il rifiuto alle pubblicazioni matrimoniali opposto dai rispettivi comuni di residenza.

Questo risultato è stato reso possibile dalla campagna di affermazione civile, portata avanti a livello nazionale dall'associazione radicale Certi Diritti e da rete Lenford, avvocatura per i diritti LGBT.

Questa azione legale parte dal presupposto che in Italia il matrimonio civile fra persone dello stesso sesso non è esplicitamente vietato da nessuna disposizione del Codice Civile. Tale interpretazione sarebbe, di per sé, coerente con il principio di pari dignità sociale di tutti i cittadini, senza distinzione di condizioni personali e sociali, né confliggerebbe con l'articolo 29 Cost. che parla di diritti della famiglia intesa come società naturale fondatà sul matrimonio.

L'intento del C.I.G. Centro di Iniziativa Gay, Arcigay Milano ONLUS è quello di offrire alla comunità LGBT ed a chiunque ne fosse interessato un'occasione per approfondire questa importante tematica proprio nel giorno stesso dell'udienza insieme ad alcuni dei protagonisti di questa storica battaglia legale.

Mercoledi 23 marzo 2010, a partire dalle 21, presso la Libreria Claudiana, via Francesco Sforza 12, Milano, il C.I.G. Centro di Iniziativa Gay, Arcigay Milano organizza una "veglia-dibattito", con rinfresco, in attesa della decisione o per commentare a caldo la decisione dell'alta Corte, che infatti potrebbe essere sinteticamente comunicata già nella stessa serata.

Parteciperanno al dibattito, tra gli altri, gli avvocati Marilisa D'Amico e Vittorio Angiolini che la mattina del 23 marzo avranno trattato dinnanzi l'alta Corte i ricorsi, in qualità di legali di Certi Diritti ed altri. Daranno il loro contributo al dibattito anche l'avvocato Matteo Winkler, socio della Rete Lenford - Avvocatura per i diritti LGBT, e, in qualità di moderatore, Angelo Maria Perrino, direttore di Affaritaliani.it