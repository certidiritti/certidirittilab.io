---
title: 'Gay: Buttiglione come il suo compagno di sacrestia Giovanardi'
date: Thu, 28 Apr 2011 07:49:57 +0000
draft: false
tags: [Politica]
---

L’intellettuale clerico-fondamentalista Rocco Buttiglione, non contento della figuraccia del suo compagno di sacrestia Giovanardi su vicenda Ikea, nè di essere stato cacciato dal parlamento europeo per le sue posizione clerico-fondamentaliste, se ne inventa un'altra e denuncia che “le famiglie etero pagano le pensioni ai gay”. Si vergogni.

Roma, 28 aprile 2011

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti

L’intellettuale clerico-fondamentalista Rocco Buttiglione, compagno di sacrestia dell’altro grande genio politico, il (molto) Sottosegretario Carlo Giovanardi, che solo alcuni giorni fa ha fatto un'altra figuraccia planetaria per le sue dichiarazioni sull’anticostituzionalità di Ikea,  considerato intellettuale filosofo anche per aver fondato e insegnato in una  Università del Liechtentestein, attuale membro della Pontificia Accademia delle Scienze sociali, denuncia che “le famiglie etero pagano le pensioni ai gay”. Ciò che è grave è che gli italiani gli pagano pure lo stipendio di Vice Presidente della Camera per sentire queste grandi pensieri politico-sociali.

Le sue analisi di così grande spessore politico, di così importante acume intellettuale, sono davvero di  fondamentale importanza per il nostro paese. Il  grande politico Buttiglione, di forte e granitica coerenza politico-partitica, nato nella DC, passato poi al PPI, al CDU , all’UDR e poi passato all’UDC  e poi al PPE e rimasto infine Presidente dell’UDC, nel 2004 fu proposto da Berlusconi Commissario europeo per la Giustizia; fu  subito cacciato  dal Parlamento Europeo che lo aveva ‘sfiduciato’ per le sue gravi affermazioni sui gay peccatori e ‘disordinati’. Nel 2006 è stato anche sconfitto-cacciato da candidato Sindaco di Torino.  Non gli è bastato?  Oggi denuncia che "le famiglie tradizionali che fanno crescere i bambini, e li educano, pagano le tasse e i contributi anche per le pensioni e l’assistenza sanitaria di quelli che i bambini non li hanno avuti". E poi, non contento, e qui si nota il suo essere grande intellettuale, aggiunge: “Sennò da dove pensiamo che si prendano i soldi per pagare le pensioni ai gay?”.  Ciò che egli dovrebbe domandare, e domandarsi, è come sia possibile che gli italiani paghino a lui lo stipendio di Vice Presidente della Camera per sentire queste cose. Si vergogni.