---
title: 'PENSIAMO TROPPO E SENTIAMO POCO...'
date: Tue, 17 Jun 2008 07:29:42 +0000
draft: false
tags: [Senza categoria]
---

Parole tratte dal grande film di Charlie Chaplin, Il grande dittatore - Discorso all'umanità

  

Il grande dittatore è un film del 1940 diretto, prodotto e interpretato da [Charlie Chaplin](http://it.wikipedia.org/wiki/Charlie_Chaplin "Charlie Chaplin").

La sua prima edizione risale al 15 ottobre del 1940, nel pieno della seconda guerra mondiale.

Fonte: [Wikipedia](http://it.wikipedia.org/wiki/Il_grande_dittatore)

  

Versione moderna

  

  

  

  
versione originale