---
title: 'DIDORE'', PACS O GENOVEFFA, SI AVVII CAMMINO ANCHE CON ROTONDI E BRUNETTA'
date: Thu, 18 Sep 2008 12:10:13 +0000
draft: false
tags: [Comunicati stampa]
---

**CHE SI CHIAMINO PACS, GENOVEFFA O GIUSEPPE NON CI IMPORTA: CI INTERESSA CHE  SI MUOVA QUALCOSA SUL PIANO DEI DIRITTI. PRONTI A SOSTENERE I DIDORE MA COME PRIMO PASSO DI UN LUNGO CAMMINO.**

_**Dichiarazione di Antonella Casu, Segretaria di Radicali Italiani e Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**_

“Apprezziamo il lavoro che i Ministri Brunetta e Rotondi stanno per proporre al Parlamento che ha per finalità il riconoscimento di alcuni diritti oggi totalmente negati alle coppie gay ed eterosessuali conviventi. A noi che questo Disegno di Legge si chiami Genoveffa o Giuseppe non importa, ci importa che si riconoscano alcuni diritti. E’ evidente che questo non può che essere il primo passo di un lungo cammino che si concluderà soltanto quando anche le coppie gay potranno accedere all’istituto civile del matrimonio; finora le gerarchie vaticane e il fondamentlismo ideologico di cultura  autoritaria ne hanno impedito l’avvio. Saremo vigili e lavoreremo come sempre per completare il cammino che si potrebbe avviare con la proposta Didore”.