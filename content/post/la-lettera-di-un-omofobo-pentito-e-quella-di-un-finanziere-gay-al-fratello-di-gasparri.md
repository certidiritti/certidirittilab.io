---
title: 'La lettera di un omofobo pentito e quella di un finanziere gay al fratello di Gasparri'
date: Sat, 25 Aug 2012 14:00:55 +0000
draft: false
tags: [Politica]
---

![LogoCD](http://www.radicalparty.org/file/Logo_Cd_2012.JPG)

**Dai corpo ai tuoi diritti!               
**Continua la campagna iscrizioni dell’associazione radicale Certi Diritti, che non ricevendo finanziamenti pubblici, ha bisogno del contributo di iscritti e sostenitori per portare avanti le battaglie per i diritti civili e le libertà sessuali. **[Sul nostro sito trovi le nuove immagini >](http://www.certidiritti.org/2012/06/26/dai-corpo-ai-tuoi-diritti-parlamentari-artisti-ex-porno-divi-disabili-studenti-rifugiati-e-attivisti-politici-nudi-contro-la-sessuofobia/)  
**Puoi partecipare anche tu inviando una foto a **[info@certidiritti.it](mailto:<a href=)" target="_blank">[info@certidiritti.it](mailto:info@certidiritti.it)    **

Questa settimana abbiamo ricevuto **due contributi molto importanti** che ti invitiamo a leggere:

- la lettera di Marcello, iscritto a Certi Diritti, eterosessuale, che ha deciso di dare un 'risarcimento indiretto' di 300 euro. **[Scopri perché >](http://www.certidiritti.org/2012/07/02/sono-iscritto-a-certi-diritti-eterosessuale-vorrei-da-voi-la-tessera-gay-ad-honorem-e-vi-risarcisco-con-300-euro/)**

- la lettera aperta al [Vice Comandante dell’Arma dei Carabinieri Clemente Gasparri](http://www.certidiritti.org/2012/06/27/vice-comandante-dellarma-dei-carabinieri-offende-gay-polizia-gdf-magistrati-morti-per-suicidio-esposto-di-certi-diritti-a-unar-e-oscad/) scritta dall’Appuntato Scelto Marcello Strati: **[faccio parte della Guardia di Finanza e sono gay >](http://www.certidiritti.org/2012/07/03/lettera-aperta-al-vice-comandante-dellarma-dei-carabinieri-clemente-gasparri/)**

Compie due mesi **‘Fuor di pagina - la rassegna stampa di Certi Diritti’ **a cura del segretario Yuri Guaiana. Ora è disponibile anche in podcast su **[iTunes](http://itunes.apple.com/it/podcast/radioradicale-fuor-di-pagina/id530466683?mt=2.). [Ascolta su radio radicale >](http://www.radioradicale.it/rubrica/1004)**

Intanto a Milano l’ennesima **[aggressione omofoba: la denuncia di Certi Diritti >](http://www.certidiritti.org/2012/06/29/aggressione-omofoba-a-milano-la-denuncia-dellassociazione-radicale-certi-diritti/)**

Continua la raccolte firme per le **cinque proposte per una Milano Laica ed Europea. Il comitato promotore ha fatto il punto della situazione. ****[Ascolta su Radio Radicale >](http://www.radioradicale.it/scheda/355946/presentazione-delle-cinque-proposte-per-una-milano-laica-ed-europea)**

Arrivata l’estate, la sessuofobia si manifesta anchenella persecuzione di chi pratica il naturismo:**[forze dell'ordine, con supporto guardie private armate, si accaniscono contro chi pratica naturismo. Il Governo chiarisca:](http://www.certidiritti.org/2012/07/03/forze-dellordine-con-supporto-guardie-private-armate-si-accaniscono-contro-chi-pratica-naturismo-governo-chiarisca/) **interrogazione dei Parlamentari Radicali su proposta di Certi Diritti.      

Sul nostro sito trovi il testo della Risoluzione su diritti Lgbti in Africa approvata dal PE che chiede anche la depenalizzazione omosessualità. [**Brutta figura dei parlamentari italiani >**](http://www.certidiritti.org/2012/07/06/parlamento-europeo-approva-risoluzione-su-diritti-lgbti-in-africa-e-chiede-depenalizzazione-omosessualita-brutta-figura-dei-parlamentari-italiani/)

Puoi anche scaricare **[Froci and queer, studio comparativo delle forme di esclusione nei confronti di persone LGBT nelle società inglese ed italiana negli ultimi 30 anni (in inglese) >](http://www.certidiritti.org/2012/06/29/froci-and-queer/)**      

**[Coppie di fatto e coppie sposate, scopri le differenze](http://www.confinionline.it/ShowRassegna.aspx?Prog=28992), **con un intervento di** Marilisa D’Amico, **iscritta all’Associazione radicale Certi Diritti.

Intervento di Bruno De Filippis e della senatrice Donatella Poretti a Tv2000. **[Il video >](http://www.youtube.com/watch?v=c-BhN3dDftY)**  
Su Liberi.tv, nello spazio Certi Diritti, puoi ascoltare la conversazione di Riccardo Cristiano con il gruppo ‘Ponti sospesi’. Il titolo della puntata è:** [Ponti Sospesi... verso il dialogo - Fede e omosessualità, è tempo di credere >](http://www.liberi.tv/webtv/2012/07/01/video/ponti-sospesi-verso-dialogo-fede-e-omosessualit%C3%A0-%C3%A8-tempo)**

**[Firma il manifesto per la legalizzazione della prostituzione. Lo hanno già fatto Don Gallo, Oliviero Toscani e tanti altri >](campagne-certi-diritti/itemlist/category/85-legalizzazione-prostituzione) **

**  
[Non sei ancora iscritto a Certi Diritti? Da oggi per chi si iscrive alla nostra associazione in regalo il libro ‘Dal cuore delle coppie al cuore del diritto’, la nostra shopper bag e la mappa Ilga dei diritti lgbt nel mondo >](partecipa/iscriviti/itemlist/category/108)**

[www.certidiritti.org](http://www.certidiritti.org/)