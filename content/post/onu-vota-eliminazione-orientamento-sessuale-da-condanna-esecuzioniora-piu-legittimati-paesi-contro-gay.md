---
title: 'Onu vota eliminazione orientamento sessuale da condanna esecuzioni,ora più legittimati paesi contro gay'
date: Wed, 17 Nov 2010 12:12:15 +0000
draft: false
tags: [Comunicati stampa]
---

**ONU ELIMINA L’ORIENTAMENTO SESSUALE DALLA CONDANNA DELLE ESECUZIONI EXTRAGIUDIZIALI, SOMMARIE ED ARBITRARIE.  
RADICALI: senza comunita' di democrazie solo passi indietro su rispetto dei diritti umani. L’ITALIA SI MUOVA.**

Comunicato Stampa del Partito Radicale Nonviolento Transnazionale e Transpartito e dell’Associazione Radicale Certi Diritti.

Quando nel 2008 l’Assemblea Generale delle Nazioni Unite approvò una risoluzione che condanna le esecuzioni extragiudiziali, sommarie ed arbitrarie ed altre uccisioni, nel testo furono incluse anche le uccisioni causate dall’orientamento sessuale delle vittime.

Ieri, l'iniziativa di Marocco e Mali a nome dei paesi africani e islamici, ha fatto cancellare l’espressione ‘orientamento sessuale’ sostituendola con un più generico ‘ragioni discriminatorie su qualsiasi base’. Il voto è passato con 79 favorevoli e 70 contrari.

Grazie alla mancanza di quei "gruppi democratici" previsti dalla Comunita' delle Democrazie, questa grave modifica causerà una maggiore impunità per coloro che promuovono in ogni angolo della terra azioni violente contro le persone lesbiche, gay, transessuali e trans gender, specie in paesi dittatoriali o teocratici, ma anche laddove l'omosessualita' e' considerata un reato penale e dove tale violenza e' quindi legge dello Stato.

Non è un caso se proprio quei paesi che hanno deciso di togliere l’orientamento sessuale nel testo della Risoluzione, sono quelli tra i più omofobi e dove le persecuzioni non sono nemmeno denunciate perché facenti parte di una cultura dominante, ora sarà più facile per loro considerare reato o peccato l’omosessualità e quindi non considerare discriminatorie le violenze a causa dell’orientamento sessuale..

L'Italia, che fa parte del Comitato di pilotaggio delle Comunita' di Democrazie, deve rilanciare con forza quanto contenuto nella Dichiarazione di Varsavia del 2000 e cioe' un coordinamento all'ONU dei paesi democratici che di volta in volta organizzino i voti su tematiche relative ai diritti umani. Il Ministro Frattini, molto attivo contro le persecuzioni, specie se contro i cristiani, si assuma le sue responsabilita'.