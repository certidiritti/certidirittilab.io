---
title: 'TRANSESSUALI E PROSTITUTE HANNO QUALCOSA DA DIRE: ROMA, 3-11 ORE 15,30'
date: Sun, 01 Nov 2009 12:22:13 +0000
draft: false
tags: [Comunicati stampa]
---

**L’Associazione Radicale Certi Diritti promuove:**

**CONFERENZA STAMPA - INCONTRO PUBBLICO****.** Martedì 3 novembre,alle ore 15.30  in Via di Torre Argentina, 76 – TRANSESSUALI E PROSTITUTE HANNO QUALCOSA DA DIRE…. (E ANCHE I RADICALI CON LE LORO PROPOSTE DI RIFORMA)

**con:**

\- **Pia Covre**, Presidente Comitato Diritti civili delle Prostitute;

\- **Monica Rosellini**, Presidente La Strega da Bruciare;

\- **Marco Cappato**, Segretario Associazione Luca Coscioni;

\- **Leila Deianis**, Presidente Libellula Azione Trans;

\- **Sergio Rovasio**, Segretario Associazione Radicale Certi Diritti;

\- **Porpora Marcasciano**, Movimento Italiano Transessuali;

\- **Donatella Poretti**, Senatrice radicale - Pd;

\- **Andrea Maccarrone**, Presidente Circolo Mario Mieli di Roma;

**La denigrazione, l’offesa e l’ingiuria verso le prostitute e le transessuali lede la dignità e il rispetto dell’autodeterminazione dei cittadini nell’ambito dei comportamenti e delle scelte sessuali.**