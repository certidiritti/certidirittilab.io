---
title: 'AMORE CIVILE – CERTI DIRITTI IN RADIO. QUESTA NOTTE SU RADIO RADICALE LA TERZA PUNTATA'
date: Fri, 07 Jan 2011 16:36:08 +0000
draft: false
tags: [Comunicati stampa]
---

**AMORE CIVILE – CERTI DIRITTI IN RADIO. QUESTA NOTTE SU RADIO RADICALE LA TERZA PUNTATA SU: MATRIMONIO TRA PERSONE DELLO STESSO SESSO, LA CAMPAGNA DI AFFERMAZIONE CIVILE ANCHE ALLA LUCE DELLE ULTIME SENTENZE DELLA CORTE COSTITUZIONALE, UNIONI CIVILI E ALTRO…**

Roma, 7 gennaio 2010

Si svolgerà la notte tra venerdì 7 e sabato 8 gennaio, dalla mezzanotte circa, a Radio Radicale, la terza puntata della trasmissione ‘Amore Civile – Certi Diritti in Radio’, la trasmissione dell’Associazione Radicale Certi Diritti curata da Sergio Rovasio, nata la notte di natale.

La terza puntata tratterà del matrimonio tra persone dello stesso sesso e della campagna di Affermazione Civile sull’azione delle coppie lesbiche e gay che chiedono ai loro Comuni le pubblicazioni per il matrimonio. Parteciperanno alla trasmissione: la coppia gay di Latina sposatasi nel 2002 in Olanda che ha intentato un’azione legale contro l’Italia per vedersi riconosciuto il loro matrimonio; la coppia di Firenze che per prima ha promosso in Italia, con il supporto legale dell’avv. Francesco Bilotta, l’azione di Affermazione Civile; la Professoressa di Diritto Costituzionale all’Università di Milano Marilisa D’Amico sulle tre sentenze della Corte Costituzionale avute in seguito alla campagna di Affermazione Civile.

A seguire si parlerà delle diverse proposte di regolamentazione delle unioni civili, tra le altre si terrà anche un confronto tra il Giurista Bruno De Filippis, autore di una proposta di regolamentazione di Unione civile notarile di tipo privato e Enrico Oliari, Presidente di GayLib, promotore di una proposta di regolamentazione delle unioni omo-affettive.

La trasmissione radiofonica, settimanale, tratterà i temi relativi al diritto di famiglia, delle politiche proibizioniste in tema di prostituzione, della censura ideologica e fondamentalista in tema di informazione sessuale, della prevenzione delle malattie sessualmente trasmissibili, del pregiudizio e della violenza contro le persone transessuali e transgender, della promozione e difesa dei diritti civili e umani delle persone, anche riguardo il loro orientamento sessuale.

Per idee, suggerimenti, proposte, contattare la redazione di ‘Amore Civile – Certi Diritti in Radio’ al seguente indirizzo e-mail:

[info@certidiritti.it](mailto:info@certidiritti.it)