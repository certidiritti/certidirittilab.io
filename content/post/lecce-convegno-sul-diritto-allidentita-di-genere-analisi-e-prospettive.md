---
title: 'LECCE: CONVEGNO SUL DIRITTO ALL''IDENTITA'' DI GENERE, ANALISI E PROSPETTIVE'
date: Fri, 30 Oct 2009 10:10:10 +0000
draft: false
tags: [Comunicati stampa]
---

Il Circolo culturale L.G.B.T.E. "Pier Paolo Pasolini" organizza per il giorno venerdì 30 ottobre 2009, alle ore 17.30, presso la sala Dante dell'I.T.C. "O.G. Costa", in P.tta De Sanctis, 10, a Lecce, il Convegno dal titolo: "Il diritto all'identità di genere: analisi e prospettive". L'incontro è finalizzato ad analizzare dal punto di vista politico, giuridico, psicologico, culturale, sociale e giuslavoristico le tematiche legate all'identità di genere nella prospettiva della riforma della Legge 164/82.

  
  
Il Convegno sarà moderato dalla Dr. Alessandro Angelo PESCINA, dottore in Servizi Sociali, e, dopo il saluto iniziale di Roberto MANCUSO, presidente del Circolo Pasolini, avrà il contributo dei seguenti relatori:  
Dr. Salvatore ANTONACI, redazione di Resistenza Laica, che curerà l'aspetto relativo alla letteratura transessuale;  
Giacomo GRIPPA, Coordinatore Circolo UAAR di Lecce, che curerà gli aspetti teologici e religiosi;  
Dr.ssa Deborah ORLANDINI, Direttivo del Circolo "PIer Paolo Pasolini", che curerà gli aspetti giuslavoristici;  
Avv. Antonio ROTELLI, Presidente Avvocatura per i diritti LGBT - Rete Lenford, che curerà gli aspetti giuridici sia dal punto di vista dell'ordinamento italiano, sia dal punto di vista del diritto comunitario;  
Sergio ROVASIO, segretario dell'Associazione radicale "Certi Diritti", che curerà gli aspetti politico-legislativi;  
Prof. Alessandro TAURINO, docente di psicologia clinica presso l´Università di Bari, che curerà gli aspetti psicologici e clinici.  
  
Dopo le relazioni è prevista una fase interattiva con la partecipazione del pubblico mediante brevi domande ai relatori.  
Il Convegno si concluderà con le repliche dei relatori inderogabilmente alle ore 20 per problemi logistici, quindi si raccomanda la puntualità.  
  
La cittadinanza è invitata e tutte le associazioni LGBT sono sollecitate a portare il loro saluto.  
  
Roberto Mancuso  
presidente