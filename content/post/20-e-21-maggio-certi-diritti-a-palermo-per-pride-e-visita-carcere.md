---
title: '20 e 21 maggio Certi Diritti a Palermo per Pride e visita carcere'
date: Thu, 19 May 2011 07:21:14 +0000
draft: false
tags: [Politica]
---

**Una delegazione dell'Associazione Radicale Certi Diritti a Palermo per conferenza sui diritti lgbt in Italia, Pride 2011 e visita al carcere dell'Ucciardone, dove sono presenti anche detenute transessuali.**  

Roma, 19 maggio 2011  
  
L’Associazione Radicale Certi Diritti, venerdì  20 e sabato 21 maggio 2011, parteciperà con una delegazione composta da Rita Bernardini, deputata Radicale eletta nel Pd, Presidente, Sergio Rovasio, Segretario, Donatella Corleo, Segretaria dell’Associazione David Kato Kisule – Radicali Palermo e da Elio Polizzotto della Ong Nonc ‘è Pace senza Giustizia, al Pride 2011 di Palermo.  
   
**Venerdì 20 maggio**  
  
Ore 17 a Palermo, in via degli Schioppettieri  incontro su: **“Persone LGBT, omofobia e istituzioni: la situazione dei diritti delle persone LGBT in Italia.**  
Con la partecipazione di: Sergio Rovasio, segretario di Certi Diritti,  Rita Bernardini, presidente di Certi Diritti e deputata radicale eletta nelle liste del Pd, Guido Galipò (Dottore di ricerca in Diritti e libertà fondamentali-Università di Catania), Giuseppina La Delfa presidente di Famiglie Arcobaleno, Paolo Patané presidente di Arcigay, Giordana Curati, della segreteria nazionale di Arcilesbica, Francesca Marceca di Agedo, Imma Battaglia di Di’Gay Project, Natascia Esposito di Nuova Proposta (donne e uomini omosessuali cristiani). Carlo Cremona di I Ken.  
   
**Sabato 21 maggio**  
  
Ore 9.30 Visita al Carcere dell’Ucciardone di Rita Bernardini, Sergio Rovasio e Donatella Corleo.  
  
Ore 12,30 Conferenza Stampa della delegazione Radicale davanti al Carcere dell’Ucciardone.  
  
Ore 15.00 partecipazione dei Radicali alla Pride Parade (partenza da Piazza Magione): un lungo corteo colorato si snoderà per tutto il centro storico. Madrina del pride: Vladimir Luxuria.  Con Giulio Spatola, Mr Gay Europe 2010/2011.  Ore 20.00: concerto in piazza.