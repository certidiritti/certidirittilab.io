---
title: 'Da Certi Diritti buone vacanze e un consiglio di lettura'
date: Sat, 25 Aug 2012 14:12:34 +0000
draft: false
tags: [Politica]
---

![](http://www.radicalparty.org/file/Logo_Cd_2012.JPG)

questa è l’ultima newsletter che riceverai prima della pausa estiva. Ci ritroveremo a settembre ma potrai rimanere informato ascoltando [**Fuor di pagina**](http://www.radioradicale.it/scheda/358041/fuor-di-pagina-la-rassegna-stampa-di-certi-diritti), la rassegna stampa a cura del segretario Yuri Guaiana in onda su Radio Radicale il venerdì alle 6.30 e disponibile in podcast su [**iTunes**](http://itunes.apple.com/it/podcast/radioradicale-fuor-di-pagina/id530466683?mt=2.).

Dall’America all’Italia continua il dibattito sul** matrimonio egualitario**.        
Se Barack Obama lancia il video [**Legalize Love**](http://www.youtube.com/watch?v=spj9yMGW7Io) con il suo discorso a favore del matrimonio tra persone dello stesso sesso, Pierluigi Bersani in occasione della presentazione della Carta d’intenti delpartito cerca di far dimenticare le dichiarazioni del futuro alleato Casini e di suor Bindi: _“daremo sostanza normativa al principio riconosciuto dalla Corte costituzionale per il quale una coppia omosessuale ha diritto a vivere la propria unione ottenendone il riconoscimento giuridico”._L’associazione radicale Certi Diritti ricorda al segretario del PD che [**la Corte Costituzionale nella sentenza 138/10  ha detto anc he altro. Leggi >**](http://www.certidiritti.org/2012/07/31/coppie-stesso-sesso-importante-presa-di-posizione-di-bersani-attendiamo-pero-conferma-dalla-bindi-la-corte-ha-detto-anche-altro/)

Ascolta su Radio Radicale l’[**intervista a Sergio Rovasio sulle Unioni di fatto >**](http://www.radioradicale.it/scheda/357905/unioni-di-fatto-intervista-a-sergio-rovasio)

A Milano, sul matrimonio egualitario e il registro delle unioni civili, il segretario di Certi Diritti** [Yuri Guaiana replica a De Corato e Forte >](http://www.certidiritti.org/2012/07/31/matrimonio-egualitario-e-registro-unioni-civili-associazione-radicale-certi-diritti-replica-a-de-corato-e-forte/)  **

A Parma, dopo l’ennesima aggressione a una coppia gay, [**l’associazione Radicale Certi DirittiParma chiede**](http://www.parmapress24.it/day-news-2111-tetrallini_e_marola__dopo_l_aggressione_omofoba_incontro_urgente_con_il_sindaco_) che "l’amministrazione comunale prenda una posizione di netto dissenso e intervenga con atti istituzionali inequivocabili al fine di prevenire aggressioni basate su pregiudizi omofobi".

L’associazione radicale Certi Diritti ringrazia i 46 iscritti che hanno deciso di sostenere le campagne dell’associazione dopo il lancio della campagna [**‘Dai corpo ai tuoi diritti’**](http://www.facebook.com/media/set/?set=a.10151038148445973.482151.116671870972&type=1). Ma non basta.  
Aiutaci a far conoscere l’associazione ai tuoi amici e partecipa anche tu inviando la tua foto a[info@certidiritti.it](mailto:<a href=)" target="_blank">i**[nfo@certidiritti.it](mailto:nfo@certidiritti.it)**

[E’ possibile sostenere l’associazione anche acquistando le nostre spillette, le shopping bag e le t-shirt. Scopri tutti i gadget>](partecipa/gadget)  

Su Liberi.tv: [**Movimento 5 Stelle Cosenza - Energia, connettività, acqua, trasporti, ambiente e ...coppie di fatto**](http://www.liberi.tv/webtv/2012/07/27/video/movimento-5-stelle-cosenza-energia-connettivit%C3%A0-acqua-trasporti)

[**Dibattito - Omocrazia (Cosenza) Campagna contro l'omofobia "Immagina se la storia li avesse discriminati”**](http://www.liberi.tv/webtv/2012/07/30/video/dibattito-omocrazia-cosenza-campagnia-contro-lomofobia-immagina) e le [**Interviste **](http://www.liberi.tv/webtv/2012/07/30/video/interviste-omocrazia-cosenza-campagnia-contro-lomofobia)a cura di Riccardo Cristiano.

Certi diritti ha sottoscritto l’appello** [Chi ha paura dei diritti](http://www.laicitaediritti.org/chi-ha-paura-dei-diritti/)**[?](http://www.laicitaediritti.org/chi-ha-paura-dei-diritti/) Fallo anche tu!

Chiudiamo questa newsletter consigliandoti la lettera del capolavoro di Gore Vidal** ‘La statua di Sale’**. [**Il perché ce lo spiega Leonardo Monaco della giunta di segreteria di Certi Diritti>**](http://www.certidiritti.org/2012/08/02/the-city-and-the-pillar-non-perversione-ma-indagine-sullaffettivita-umana/)         

**Gore Vidal è morto due giorni fa**. Scrittore, saggista, drammaturgo, critico, è stato anche attore, scenografo e regista. Iscritto al Partito Radicale dal 1992 al 1995, ha dichiarato: “in Italia avete due partiti politici. Avete Pannella e poi tutti gli altri...”            
**Sul nostro sito il ricordo di Sergio Rovasio e Marco Perduca >**

Buone vacanze!  
[**www.certidiritti.org**](http://www.certidiritti.org/)