---
title: 'Destre e parte dei socialisti nominano Tonio Borg commissario europeo, ma sotto osservazione'
date: Wed, 21 Nov 2012 15:30:21 +0000
draft: false
tags: [Europa]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti

Roma, 21 November 2012

La plenaria del PE ha approvato oggi con voto segreto e con 386 favorevoli, 281 contrari e 28 astenuti, la nomina a Commissario europeo per la salute di Tonio Borg, attualmente Ministro maltese degli Esteri.

93 ONG tra le quali Ilga-Europe, Catholics for Choice, gli umanisti, le organizzazioni per i diritti dei migranti, delle donne, nonché l'Associazione Radicale Certi Diritti, si erano appellati agli Eurodeputati perché rigettassero la nomina di Borg, per le sue posizioni conservatrici, clericali e fondamentaliste, contrarie ai diritti LGBTI, delle donne, degli immigranti.

Fin dall'audizione in commissione parlamentare, mentre il gruppo PPE e le destre avevano dichiarato il loro appoggio al loro candidato, i gruppi Liberale e Democratico, Verde e Comunista avevano subito chiarito che non avrebbero appoggiato Borg. Il gruppo socialista aveva dato il suo ok a Borg inizialmente, chiedendo pero' impegni in merito al suo portafoglio. Ieri nel corso della riunione del gruppo socialista gli eurodeputati avevano votato a 2/3 per rigettare Borg. Il voto di oggi ha purtroppo dimostrato che la divisione dei socialisti ha causato l'approvazione di Borg a Commissario europeo.

L'Associazione Radicale Certi Diritti deplora che le destre e parte dei socialisti abbiano permesso che un candidato dalle vedute estremiste e fondamentaliste possa entrare a far parte della Commissione europea. Giochi di potere da grande coalizione tra PPE e Socialisti hanno portato alla vittoria di Pirro di Borg e dei suoi sponsors politici.

Allo stesso tempo l'Associazione Radicale Certi Diritti ritiene che sia un successo per il fronte laico, dei diritti civili delle persone LGBTI, delle donne e dei migranti il fatto che una nomina che doveva passare senza troppi problemi sia diventata invece problematica, dibattuta e contestata, in particolare nel gruppo socialista; che la questione abbia assillato Borg, Barroso, la Commissione, il governo di Malta, tutti impegnati per fare lobbying sui deputati europei e rassicurarli sulla futura "moderazione" di Borg; che Borg sia stato portato a fare inversione di marcia pubblicamente sulle sue visioni politiche attuali e sulla linea ed iniziative politiche che porterà avanti in seno alla Commissione; che Borg abbia preso impegni precisi sulla depatologizzazione del transessualismo a livello internazionale e sulla revisione delle regole sulla donazione di sangue da parte delle persone LGBTI.

Le sue mosse saranno sicuramente monitorate da vicino dal PE, dalle ONG, nonché dai Commissari suoi colleghi. Insomma, Tonio Borg è Commissario europeo, ma sotto stretta osservazione - anche a Malta, vedi caso Aliyev...