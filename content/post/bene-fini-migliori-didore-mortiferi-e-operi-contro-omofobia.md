---
title: 'BENE FINI: MIGLIORI DIDORE'' MORTIFERI  E OPERI CONTRO OMOFOBIA'
date: Wed, 13 May 2009 11:13:44 +0000
draft: false
tags: [Comunicati stampa]
---

UNIONI CIVILI E LOTTA OMOFOBIA: LE INTENZIONI DI FINI CI SEMBRANO BUONE. GLI DIAMO FIDUCIA PURCHE’ VADA OLTRE I DIDORE’ TROPPO MORTIFERI. SOSTENGA LOTTA CONTRO OMOFOBIA.  
   
Comunicato Stampa Associazione Radicale Certi Diritti

“La notizia che il Presidente della Camera dei Deputati incontra quattro associazioni italiane lgbt è positiva. Entra pienamente nello stile del Presidente Fini che sta tentando di riaccendere i riflettori sulla questione dei diritti civili nel nostro Paese.  
Non siamo tanto preoccupati nè scandalizzati di eventuali secondi fini nascosti nell'evento (l'uno per far dispetto a Berlusconi gli altri per far dispetto al PD), piuttosto, ci auguriamo che il Presidente sappia dimostrare nei fatti che si apre realmente una nuova stagione su questi temi.  
In particolare l'Associazione Radicale Certi Diritti, ritiene che siano due i banchi di prova su cui si verificherà se ci sono le condizioni per considerare riaperto il dossier sui diritti in Parlamento: trovare un canale per esaminare ed approvare al più presto il progetto di legge contro l'omofobia, firmato, tra gli altri, dai parlamentari radicali Rita Bernardini, Marco Beltrandi, Marco Mecacci, Elisabetta Zamparutti, Maurizio Turco; modificare la pdl sui Di.Do.Re, troppo mortifera e limitata al partner malato ed estendere i diritti previsti per le unioni di fatto almeno alle questioni relative alle volontà testamentarie ed alla reversibilità pensionistica del partner”.