---
title: 'FRANCESCHINI: COPPIE DI FATTO NON SONO FAMIGLIA. LETTERA DI ISCRITTO AL PD'
date: Thu, 23 Jul 2009 13:45:53 +0000
draft: false
tags: [Comunicati stampa]
---

Cari compagni e amici,  
  
ho appena ricevuto la tessera del PD, circolo di Londra. Il mio entusiasmo pero' e' stato subito smorzato dalle dichiarazione del segretario (NON eletto) del mio partito che, attardandosi su una raccapricciante ideologia foriera di tanti mostri nella storia dell'umanita', ha tenuto a precisare che le coppie di fatto (Franceschini non nomina nemmeno la parola 'omosessuale', forse crede sia una parolaccia) non sono famiglia.[http://www.unita.it/...famiglia](http://www.unita.it/news/politica/86710/franceschini_le_coppie_di_fatto_non_sono_una_famiglia)

Sulla candidatura di Marino, il NON-eletto segretario del PD Franceschini dice: "Io stimo Ignazio Marino, ma la sua candidatura rischia di essere tagliata troppo sui temi eticamente sensibili, temi sui quali bisogna cercare la strada della sintesi. Se invece si alzano barriere e ci si caratterizza per questo è pericoloso". [http://www.repubblica.it/2009/07/sezioni/politica/partito-democratico-30/franceschini-a-reptv/franceschini-a-reptv.html](http://www.repubblica.it/2009/07/sezioni/politica/partito-democratico-30/franceschini-a-reptv/franceschini-a-reptv.html)  
  
Sono cittadino Italiano, convivo da 15 anni con il mio compagno anche lui italiano e da tre anni abbiamo ufficializzato la nostra unione con il diritto inglese (viviamo a Londra) e ci siamo uniti in una civil partnership. Ma per il segretario del PD Franceschini noi non saremmo famiglia. Insieme a Binetti, Buttiglione, Berlusconi, Casini e molti altri ipocriti sepolcri inbiancati al di qua e al di la’ del Tevere, Franceschini allora impone la morale clericale (che NON e’ morale cattolica e ancor meno cristiana!) a tutti i cittadini Italiani. Ma Franceschini dovrebbe allora andare fino in fondo: dovrebbe dire che anche coloro che non si sono sposati in chiesa non sono famiglia e anche coloro che hanno divorziato per poi riunirsi con il rito civile, non sono famiglia. Dovrebbe andare fino in fondo in questa sua ideologia e allearsi a quanti vorrebbero una modifica "pro-life" della legge sull'interruzione della gravidanza. Dovrebbe uscire dall'ambiguita' ipocrita dell'opportunismo trasformistico italiano ed eseguire fino in fondo le direttive vaticane in materia di testamento biologico, ricerca sulle staminali, ecc.  
  
Il NON-eletto Franceschini fa' l'eco a una visione oscurantista che nulla ha a che fare con l’aggettivo democratico e con la normalita’ del resto del mondo occidentale a cui l’Italia e’ condannata a non appartenere mai a causa di questi ruderi della politica che si nascondono sotto le sottane dei gerarchi vaticani.  
  
La candidatura Marino e' in realta' rivoluzionaria nel nauseante panorama politico italiano; le falangi armate della partitocrazia e della chiesa simoniaca (quella parte della chiesa cattolica che nulla ha a che fare con Cristo e il suo messaggio di Caritas) si stanno unendo per dar battaglia: hanno capito che Marino e le sue idee appartengono all'orizzonte culturale della Riforma Protestante, dell'Illuminismo e della Rivoluzione Francese. In Italia, terra di contro-riforme e restaurazioni, quelle idee di liberta', uguaglianza e fratellanza sono fumo negli occhi per i nostri signorotti che ancora tengono il paese in una dimensione feudale, negano il diritto di cittadinanza, condannano il paese ad una tragica insignificanza per continuare ad avere il potere, il loro unico oggetto del desiderio.  
  
Nel mezzo di una illegalita' sconfinata e devastante, dove i gerarchi del PD non rispettano nemmeno lo statuto e le regole del loro (nostro) partito, dove a Napoli il PD permette l'aggressione fisica a chi osa chiedere una domanda [http://www.radioradicale.it/video-virale-aggrediti-per-una-domanda-al-convegno-del-pd-di-napoli](http://www.radioradicale.it/video-virale-aggrediti-per-una-domanda-al-convegno-del-pd-di-napoli) dove - credo unico caso al mondo - si nega il tesseramento a un cittadino italiano che vuole iscriversi a un partito (caso Grillo) privando quel cittadino del suo costituzionale diritto di associazione... Franceschini usa una violenza inaudita e non riconosce lo status di famiglia a milioni di italiani, alcuni dei quali hanno la tessera del PD e devono sentirsi insultare dal loro segretario nazionale.  
  
Davvero un bel Welcome! dal segretario (NON eletto) del mio partito nel giorno in cui ho ricevuto la tessera del PD. Roba da Family Day!  
  
Cordialmente,  
Gabriele Zamparini  
Londra