---
title: 'CERTI DIRITTI INVITA AGUDA DI TEL AVIV (CHE AIUTA ANCHE I GAY PALESTINESI) A SFILARE INSIEME AL PRIDE'
date: Tue, 15 Jun 2010 06:02:47 +0000
draft: false
tags: [Comunicati stampa]
---

**ASSOCIAZIONE  LGBT ISRAELIANA ESCLUSA DAL GAY PRIDE DI MADRID: IL FONDAMENTALISMO IDEOLOGICO COLPISCE AGUDA, ASSOCIAZIONE CHE AIUTA E PROTEGGE GAY PALESTINESI. CERTI DIRITTI INVITA AGUDA A SFILARE INSIEME AL GAY PRIDE NAZIONALE DI NAPOLI.**

Roma, 15 giugno 2010

Dichiarazione di **Sergio Rovasio,** Segretario dell'**Associazione Radicale Certi Diritti **

“Il fondamentalismo ideologico acceca tanto quanto quello religioso. E’ così che gli organizzatori del gay pride di Madrid  hanno ritirato l’invito rivolto ad Aguda, la più importante associazione lgbt israeliana, a manifestare con loro per le strade della Capitale spagnola. Tale scelta è stata fatta per colpire Israele.

E’ come se l’invito rivolto ad un’Associazione lgbt italiana venisse ritirato per colpire l’operato del Governo italiano.

**[Aguda](http://glbt.org.il/he/)** è conosciuta in Israele, insieme alla [**Open House**](http://www.joh.org.il/index.php?id=1803/) di Gerusalemme, perché sostiene chiunque chieda aiuto e svolge attività di volontariato per proteggere e sostenere i gay palestinesi che vivono in fuga una doppia condizione di clandestinità: dalla propria famiglia e dallo Stato di Israele.

La stessa Open House di Gerusalemme è un’organizzazione nella cui sede si vedono più giovani palestinesi che israeliani e comunque non vi sono conflitti tra di loro perché sono tutti accomunati dall’obiettivo dell’emancipazione e del superamento delle diseguaglianze. Ci sono gruppi di auto-aiuto e sostegno molto all’avanguardia e la stessa popolazione, eslcusi i soliti fondamentalisti del fanatismo religioso,  conosce bene le loro attività che si integrano in tutti i contesti sociali.

E’ bene ricordare che ad Haifa è stato appena aperto un nuovo **«telefono amico» in lingua araba**: una help-line che è il frutto del coraggio e dell'impegno dei militanti di **[Aswat](http://www.aswatgroup.org/english/)** (organizzazione per i diritti delle lesbiche arabe) e di **[Alqaws](http://www.alqaws.org/q/)** (associazione a tutela della diversità sessuale nella società palestinese di Gerusalemme).

L’Associazione Radicale Certi Diritti ritiene profondamente ingiusta la decisione degli organizzatori del Gay Pride di Madrid, certamente frutto di un grave errore politico. Abbiamo invitato i dirigenti di Aguda a sfilare con noi al Pride nazionale di Napoli che si terrà sabato 26 giugno per dimostrare loro vicinanza e solidarietà. Ci auguriamo che a Napoli possa partecipare il dirigente palestinese di Aguda che non  potrà andare a Madrid a causa del ritiro dell’invito”.