---
title: 'PROGRAMMA LAVORI III° CONGRESSO CERTI DIRITTI E INFO LOGISTICHE'
date: Mon, 14 Dec 2009 11:40:40 +0000
draft: false
tags: [Comunicati stampa]
---

**PROGRAMMA LAVORI III° CONGRESSO DELL’ASSOCIAZIONE RADICALE CERTI DIRITTI**

**SABATO 30 E DOMENICA 31 GENNAIO 2010.**

**FIRENZE, ISTITUTO VALDESE,**

**VIA DE’ SERRAGLI, 49 TEL. 055-212576**

**Sabato 30 gennaio**

**Ore 10** Apertura del III°Congresso Associazione Radicale Certi Diritti

**I lavori si apriranno con la presentazione, a cura di Giacomo Cellottini, del volume "Amore Civile, dal Diritto della tradizione al Diritto della ragione" (MIMESIS - Quaderni Loris Fortuna) con la presenza, tra gli altri, dei due principali autori: il giurista Bruno de Filippis e l’Avvocato Francesco Bilotta e alcune personalità del mondo politico e della società civile. La proposta di Riforma del Diritto di famiglia, raccolta nel volume, è il risultato di un lavoro durato quasi due anni, che ha visto tra i suoi principali protagonisti i parlamentari radicali e la nostra Associazione Certi Diritti.**

**Ore 13** Pausa pranzo.

**Ore 14** Ripresa dei lavori: - Relazioni di Segretario, Presidente e Tesoriere.

**Ore 15** Cerimonia di accensione della Fiaccola dei diritti, a cura di Francesco Zanardi e Manuel Incorvaia;

Ricordo in memoria di Enzo Francone, scomparso lo scorso 29 novembre, a cura di Enzo Cucco.

**Ore 15,30** Saluti esterni di personalità e dei rappresentanti delle Associazioni

Apertura iscrizioni a parlare iscritti e osservatori (iscritti 10’ – osservatori 5’)

**Ore 17,15** \- Relazione di Gian Mario Felicetti sulla campagna di Affermazione Civile, presentazione Comitato “Si lo voglio”. A seguire confronto tra le Associazioni sul tema.

**18,15 - Relazione su: Attualità omosessuali: Terapie Riparative e Omogenitorialità,** Prof. Vittorio Lingiardi, Psichiatra e Psicanalista, Università ‘La Sapienza’ - Roma

**Ore 20** \- Conclusione prima giornata dei lavori.

**Domenica 31 gennaio**

Ore 9 - Inizio lavori

\- **Prostituzione: Libertà e Antiproibizionismo** Pia Covre, Presidente Comitato Diritti Civili Prostitute;

\- **Laicità e Diritto**, Francesco Bilotta, Avvocato, co-fondatore Rete Lenford;

\- **Transessuali: ‘Emarginazione esclusione,discriminazione’**, Leila Deianis, Presidente Associazione Libellula – Roma;

\- **Il peggio non è mai morto: La battaglia per l'affermazione del diritto di genere in Uganda**, Niccolò Figà-Talamanca, Segretario Ong 'Non c'è Pace Senza Giustizia';

\- **Diritti civili e libertà: “l’Europa va avanti, l’Italia va indietro”**, Ottavio Marzocchi, funzionario della Commissione Libe del Parlamento Europeo – Bruxelles;

A seguire Dibattito Generale

**Ore 11.30** \- Termine presentazione mozioni, emendamenti, candidature per organi;

**Ore 13,30** Pausa Pranzo

**Ore 14.30 Ripresa lavori**: dibattito generale, votazione documenti, elezioni organi

**Ore 17 Conclusione lavori** – Riunione del Direttivo

**Hanno finora assicurato la loro presenza:**

**Niccolò Rinaldi,** deputato europeo Idv, Gruppo Alde; **Don Franco Barbero**, Comunità cristiane di Base; **Mina Welby**, Associazione Luca Coscioni; **Antonio Rotelli**, Presidente Avvocatura lgbt, Rete Lenford;  **Aurelio Mancuso**, Presidente di Arcigay; **Paolo Patanè**, Arcigay Sicilia, **Luca Trentin**, Responsabile Diritti Umani Arcigay; **Massimo Lensi**, consigliere provinciale pdl; **Agostino Fragai**, assessore Riforme Istituzionali Regione Toscana; **Alessia Ballini**, assessore provinciale Pari Opportunità; **Matteo Mecacci**, deputato radicale – pd;  **Donatella Poretti,** senatrice radicale – pd; **Marco Perduca**, Senatore radicale pd; **Mario Staderini,** Segretario Radicali Italiani; **Paola Concia**, deputata Pd;  **Ivan Scalfarotto**, Vice Presidente nazionale Assemblea Pd; **Pia Covre**, Presidente Comitato Diritti civili prostitute; **Monica Rossellini**, Ass. La Strega da   Bruciare,  **Francesco Piomboni**, Presidente Arcigay Firenze;  **Matteo Pegoraro** Co-Presidente Gruppo EveryOne; **Claudia Sterzi**, Segretaria Associazione Radicale Antiproibizionista; **Alfonso De Virgiliis**, Candidato Presidente Toscana Lista Bonino-Pannella;  **Leila Deianis**, Presidente Associazione Libellula, **Gianni Rossi Barilli**, Direttore di Pride; **Franco Grillini**, Direttore di Gaynews; **Marcella Di Folco**, Presidente Movimento Identità Transessuali; **Alessio De Giorgi**, Direttore di Gay.it; **Giuliano Federico**, Direttore di Gay.tv; **Fabianna Tozzi**, Presidente Associazione Trans Genere; **Regina** **Satariano**, Vice Presidente Associazione Trans Genere;  **Lady Limoncella**, ragazzo licenziato perché Drag Queen;  **Emiliano Zaino**, Presidente del Cassero di Bologna;  **Maria Gigliola Toniollo**, Cgil Nuovi Diritti; **Prof.sa** **Marilisa D’Amico**, Costituzionalista Università di Milano; **Prof. Andrea Pugiotto**, Costituzionalista Università di Ferrara; **Helena Velena**, esponente del movimento transgender;  **Felix Cossolo**, Direttore di Gayclubbing;  **Francesco e Manuel, la coppia gay di Savona** che dal 4 gennaio ha iniziato un’iniziativa nonviolenta per il riconoscimento della loro unione; **Darianna Saccomani** Associazione Crisalide Pan Gender; Prof. **Vittorio Lingiardi**, Psichiatra Università La Sapienza – Roma; **Niccolò Figà Talamanca**, Segretario Ong Non c’è  Pace senza Giustizia; Barbara Cupisti, regista, attrice, Premio David di Donatello 2008;  **Vanni Piccolo**  esponente storico del movimento lgbt;  **Alfredo Capuano**  Incaricato scuola Municipio X di Roma;  **Roberta Vanucci**, Segreteria nazionale Arcilesbica;  **Claudio Cipelletti**, regista, autore dei documentari Nessuno Uguale e Due volte genitori; **Carlo Santacroce**, **3D - Democratici per pari diritti e dignità Lgbt**;. Associazioni:  **Famiglie Arcobaleno**,  **Agedo**,  **GayLib**;  **Milk di Milano**,  **Ireos Firenze**;  **Studenti Associazione Luca Coscioni**;  **Azione Gay e Lesbica Firenze;  Fondazione Massimo Consoli; Coordinamento TorinoPride.**

INFORMAZIONI LOGISTICHE PER IL PERNOTTAMENTO A FIRENZE:

**La prenotazione della tipologia di stanza e il pagamento vanno effettuati direttamente presso Foresteria Valdese di Firenze, Via de'Serragli, 49 - 50124 Firenze Tel. +39.055.212576 Fax: +39.055.280274 e-mail:foresteriafirenze@diaconiavaldese.org specificando CONGRESSO CERTI DIRITTI e se si vuole usufruire o no della prima colazione.**

**Tutti i dettagli e altre news su foresteria valdese al seguente link: http://lnx.istitutogould.it/foresteria/**

ATTENZIONE:
-----------

> ### Visti i posti quasi esauriti al Centro Foresteria Valdese, abbiamo fatto una convenzione con un ottimo B&B di Firenze (centro storico), per la notte del 30 gennaio. Al seguenti link tutte le info: [www.bbgalileo2000.com](http://www.bbgalileo2000.com/)

### Precisare: Congresso Certi Diritti