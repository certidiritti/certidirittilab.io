---
title: 'CROCIFISSO OBBLIGATORIO:  LETTERA APERTA A ORGANISIMI EUROPEI'
date: Sat, 06 Feb 2010 07:52:17 +0000
draft: false
tags: [Comunicati stampa]
---

**_Al Consiglio d’Europa_**

**_All’Assemblea Parlamentare del Consiglio d’Europa_**

**_Alla Corte Europea dei Diritti_**

**Public letter on “Lautsi v. Italy” case**

**-**

**_Lettera aperta sul caso “Lautsi c. Italia”_**

Rome, February 2nd 2010.

**Dear Sir/Madame,**

_Roma, 2 febbraio 2010._

**_Egregi Signori, Gentili Signore,_**

We, Italian and European citizens and associations, are writing to you in order to let other Europeans listen to our voice loud and clear.

On November 3rd last the European Court of Human Rights, second section, took an important decision on the case Lautsi v. Italy, granting protection not only to the rights of Mrs. Lautsi and her kins but also to our rights and those of millions of Italian and European citizens.

The question under discussion was the imposition for students in Italy to attend lessons in classrooms dominated by the Crucifix, a religious symbol. This imposition has been found a violation of articles 9 of the European Convention for the protection of human rights and fundamental freedoms and 2 of the First Additional Protocol.

The political debate that followed in Italy has been vicious and violent against non-believers, non-Catholics, heterodox Catholics and, last but not least, the judges of the European Court of Human Rights.

Individually and on behalf of the thousands members of our groups and millions of other Italians we would like to thank the European Court and apologize for the insulting behaviour of Italian government members. We hereby dissociate ourselves from their speeches and comments.

Our country suffers more and more the political influence of the hierarchy of the Catholic church. The fewer people follow their directives the more they demand, call for privilege and taxpayers’ money, raise their voice in order to impose their will on non-Catholics’ lives and behaviours. Moreover most political leaders are keen to accept their requests disrespectful of rights and liberties, lives and personal stories, beliefs and choices of millions of citizens. That’s by now the rule, for example, regarding religious symbols, religious teaching in schools, legal protection for same sex couples, freedom of marriage, freedom of divorce, medically assisted procreation, advanced healthcare directives, living wills, public funding of Catholic religious activities.

Some of us are believers and we all do respect believers, but we cannot accept one religion, not even the most powerful, to be imposed to everyone.

This principle of religious freedom and secularity of public institutions (laicità) is enshrined in the Italian Constitution itself, is part of our history as Italian and European citizens. With the help of our fellow Europeans we will do our best to keep our pace on the road of freedom, democracy, equal social dignity for everybody.

Yours sincerely

_Vi scriviamo, come associazioni e come cittadini italiani ed europei per fare sentire chiaramente la nostra voce al resto dell’Europa._

_Lo scorso 3 Novembre la Corte Europea dei Diritti Umani, sezione seconda, ha preso una importante decisione nel caso Lautsi c. Italia, tutelando non solo i diritti della Sig.ra Lautsi e dei suoi figli ma anche quelli di milioni di cittadini italiani ed europei._

_La questione in discussione era l’imposizione per gli studenti difrequentare le lezioni in classi in cui domina il Crocifisso, un simbolo religioso. Una pratica che viola l’art. 9 della Convenzione europea per la protezione dei diritti umani e delle libertà fondamentali e l’art. 2 del Primo Protocollo Addizionale._

_Il dibattito che ne è seguito in Italia è stato aggressivo e violento contro non-credenti, i non cattolici, i cattolici non allineati e, ultimo ma non per importanza, contro i giudici della Corte Europea dei Diritti Umani._

_Individualmente ed a nome delle migliaiadi membri delle nostre associazioni vogliamo ringraziare la Corte e scusarci per il comportamento insultante di alcuni membri del Governo italiano dai quali sentiamo il dovere di dissociarci._

_Il nostro Stato soffre sempre più l’influenza politica delle gerarchie della Chiesa cattolica. Benché il numero diquanti seguono le direttive delle gerarchie sia in costante diminuzione, queste chiedono sempre maggiori privilegi e insistono per imporre le proprie visioni ai non cattolici ed ai non credenti. Inoltre la maggior parte dei leaders politici sono proni nell’accettare tali richieste senza riguardo per i diritti e le libertà, le vite e le storie personali, il credo e le scelte dimilioni di donne e uomini. Questo accade regolarmente, ad esempio, per quanto riguarda i simboli religiosi, l’insegnamento religioso nelle scuole, la protezione giuridica per le coppie dello stesso sesso, la libertà di matrimonio, la libertà di divorzio, la procreazione medicalmente assistita, le direttive anticipate di trattamento e le volontà difine vita, il finanziamento delle attività religiose della Chiesa cattolica._

_Alcuni di noi sono credenti e tutti noi rispettiamo i credenti, ma non possiamo accettare che una religione, neppure la più influente, sia imposta a tutti e tutte._

_Il principio di libertà religiosa e di laicità è racchiuso nella stessa Costituzione italiana, è parte della nostra storia come cittadini italiani e come cittadini europei. Faremo del nostro meglio per tenere i nostri passi sulla via della libertà, della democrazia dell’eguale dignità sociale per tutti e tutte, con l’aiuto di tutti i cittadini europei._

_Sinceramente vostri_

Coordinamento Nazionale delle Consulte per la Laicità delle Istituzioni – Tullio Monti;

UAAR - Unione degli atei agnostici e razionalisti – Raffaele Carcano;

Consulta Romana per la Laicità delle Istituzioni – Carlo Cosmelli;

Consulta Torinese per la Laicità delle Istituzioni – Tullio Monti;

Consulta Milanese per la Laicità delle Istituzioni – Donatella De Gaetano;

Consulta della Provincia di Pesaro ed Urbino per la Laicità delle Istituzioni - Raffaele A. Belviso;

Consulta Triestina per la Laicità delle Istituzioni – Gianni Bertossi;

Consulta Napoletana per la Laicità delle Istituzioni;

Fondazione Critica Liberale – Enzo Marzo;

Tavola Valdese – Maria Bonafede;

Associazione nazionale del libero pensiero Giordano Bruno – Maria Mantello;

Libera Uscita Associazione Nazionale – Giampiero Sestini;

Associazione “XXXI ottobre per una scuola laica e pluralista (promossa dagli evangelici italiani)” – Nicola Pantaleo;

FNISM – Federazione Nazionale degli Insegnanti – Gigliola Corduas;

Comitato Insegnati Evangelici Italiani (CIEI) – Lidia Goldoni;

CIDI Nazionale – Centro di Iniziativa Democratica degli Insegnanti – Sofia Toselli;

Italialaica – Mirella Sartori;

Democrazia Laica – Enrico Modigliani;

Società Laica e Plurale – Nico Sferragatta;

Associazione “per la Scuola della Repubblica” – Antonia Sani;

Federazione Giovanile Evangelica Italiana (FGEI) – Ilaria Valenzi;

Associazione Radicale Certi Diritti – Sergio Rovasio;

Associazione Famiglie Arcobaleno – Giuseppina La Delfa ;

WILPF Italia (Women's International League for Peace and Freedom) – Antonia Sani;

Comitato nazionale Scuola e Costituzione – Bruno Moretto;

Coordinamento Genitori Democratici – Angela Nava;

AMI Associazione Mazziniana Italiana;

Exit Italia – Emilio Coveri;

Comitato Torinese per la Laicità della Scuola – Cesare Pianciola;

CRIDES- Centro Romano di Iniziativa per la Difesa dei Diritti nella Scuola ;

Circolo Vegetariano VV.TT., Calcata – Paolo D’Arpini;

Associazione Viottoli - Comunità cristiana di base di Pinerolo – Paolo Sales;

Centro evangelico di cultura «Arturo Pascal», Torino - Jean-Jacques Peyronel;

Circolo Liberalsocialista Carlo Rosselli di Torino – Tullio Monti;

Associazione Culturale “La Meridiana”, Rivoli – Carlo Zorzi;

Associazione Radicale Adelaide Aglietta, Torino - Domenico Massano;

Associaziona Radicale Satyagraha, Torino – Stefano Mossino;

Associazione democratica Giuditta Tavani Arquati, Roma – Sandro Masini;

Associazione Iran Libero e Democratico, Torino – Yoosef Lesani;

Centro di Documentazione, Ricerca e Studi sulla Cultura Laica “Piero Calamandrei”, Torino-Onlus - Palmira Naydenova;

Arcigay, Napoli;

Associazione “Radicali Napoli - Ernesto Rossi”;

Cantiere per il bene comune, Napoli;

Cellula Coscioni di Napoli ;

Comitato Piero Gobetti, Napoli;

Exit Italia, sez. Napoli;

Libera Uscita, Napoli;

UAAR-Unione degli Atei e degli Agnostici Razionalisti, Circolo di Napoli;

UDI, Napoli;

Giovani Federalisti Europei, Torino - Elias Carlo Salvato;

Associazione Alternativa libertaria, Fano, Pesaro;

Associazione Omnibus, Fano;

Arcigay Agorà, comitato provinciale, Pesaro;

Centro sociale autogestito Oltrefrontiera, Pesaro;

Cgil Nuovi Diritti, Pesaro;

Circolo A. Labriola, Fano;

Circolo L. Polverari, Fano;

Circolo S. Allende, Fano;

Associazione La scala segreta, Fano;

Centro Donna, Urbino;

Movimento Radicalsocialista, Fano – Pesaro;

ANPI Provincia di Pesaro ed Urbino;

Associazione ZED, Torino;

Gruppo di Studi Ebraici, Torino – Franco Segre;

Agedo (Ass. Genitori e Amici di Omosessuali), Milano;

ANPI, sezione zona 1; C.I.G. Arcigay Milano;

Arcilesbica, circolo Zami, Milano;

Centro Culturale Protestante, Milano;

Associazione radicale Certi Diritti, Milano;

Circolo Carlo Rosselli, Milano;

Circolo La Riforma, Milano;

Circolo Giordano Bruno, Milano;

Ass. Culturale Marxista, Milano;

Associazione radicale Enzo Tortora, Milano;

Giuristi Democratici;, Milano;

Keshet, vita e cultura ebraica, Milano;

ICEI (Istituto di Cooperazione Economica Internazionale), Milano;

Ass. La Conta, Milano;

Ass. La Rosa Bianca, Milano;

Le Sarte di Corso Magenta, Milano;

Libera Università delle Donne, Milano;

Saveria Antiochia, Milano;

Omicron, Milano;

UAAR (Unione degli Atei e degli Agnostici Razionalisti) circolo di Milano;

AICS – Associazione Italiana Cultura e Sport, Comitato Provinciale di Torino;

Arcigay Torino;

Altera, Torino;

ARCI Nuova Associazione Torino;

Associazione Amici della Consulta Torinese per la Laicità delle Istituzioni;

Associazione Ippocrate, Torino;

Associazione Luca Coscioni Cellula di Torino;

AssoNatura, Cortazzone (AT);

Associazione Silvio Pilocane, Torino;

Associazione Socialdemocratica Giuseppe Saragat, Torino;

CUB Scuola, Torino;

Associazione Il Muretto, Torino;

CEMEA Piemonte;

Centro Evangelico di Cultura Lodovico e Paolo Paschetto, Torino;

Coordinamento TorinoPride GLBT;

FNISM– Federazione Nazionale degli Insegnanti, sez. Roma;

Famiglie Arcobaleno, Torino;

Circolo Ettore Valli, Torino;

Associazione “XXXI ottobre per una scuola laica e pluralista (promossa dagli evangelici italiani)”, sez. Torino;

UAAR-Unione degli Atei e degli Agnostici Razionalisti, Circolo di Torino;

Exit Italia, sez. Torino;

UISP Torino – Unione Italia Sport per Tutti;

COOGEN – Coordinamento Genitori Nidi, Materne, Elementari, Medie, Torino;

Casa delle donne di Torino;

Associazione Oltre il razzismo, Torino;

Unione Culturale Franco Antonicelli, Torino;

AMI Associazione Mazziniana Italiana, sez. Torino;

Il girasole, Torino;

Lambda, Torino;

Associazione nazionale del libero pensiero Giordano Bruno, sez. Torino;

FNISM– Federazione Nazionale degli Insegnanti, sez. Torino;

Liberacittadinanza, sede di Roma;

Centro Yoga Shanti Marga;

Circolo di cultura GLBT Maurice, Torino;

LIDH Italia – Ligue Interregionale de droit de l’homme, Torino;

Sotto la Mole, Torino;

Fondazione ReligionsFree Bancale Onlus, Civitavecchia (RM) – Vera Pegna;

Libera Uscita, Torino.