---
title: 'TRANSESSUALI NELLE CARCERI: INTERROGAZIONE DEI RADICALI SULLE CONDIZIONI'
date: Thu, 20 Aug 2009 08:57:24 +0000
draft: false
tags: [Comunicati stampa]
---

**TRANSESSUALI NELLE CARCERI: INTERROGAZIONE PARLAMENTARE DEI DEPUTATI RADICALI SULLE CONDIZIONI E IL TRATTAMENTO.**

**Roma, 20 agosto 2009**

Nell’ambito dell’iniziativa ‘Ferragosto nelle carceri’ promossa dalla deputata radicale Rita Bernardini e da  Radicali Italiani, i deputati radicali hanno oggi depositato, primo firmatario Matteo Mecacci, un’interrogazione parlamentare sulle condizioni delle persone transessuali nelle carceri italiane. Di seguito il testo integrale:

**Interrogazione urgente a risposta scritta**

**Al Ministro della Giustizia - Al Ministro per le Pari Opportunità**

Per sapere  - Premesso che:

-          Nei giorni 13, 14 e 15 agosto 2009 quasi 200 parlamentari e consiglieri regionali si sono recati in 185 istituti penitenziari per verificare le condizioni dei detenuti e di coloro che svolgono all’interno mansioni di Polizia e di assistenza;

-          in molte carceri vi sono alcuni reparti per detenuti che necessitano di particolare assistenza e protezione , tra gli altri quelli per le persone transessuali;

-          le persone transessuali per il raggiungimento del loro benessere psico-fisico necessitano di particolari attenzioni e cure sul piano dell’assistenza sanitaria e sociale;

-          le persone transessuali sono spesso oggetto di scherno da parte della società e hanno bisogno di tutela e protezione specifiche specialmente in una struttura carceraria;

-          l’identità femminile, ancorchè maschile per lo stato civile, o viceversa, si raggiunge con il supporto e l’aiuto di specialisti, per lo più medici, in un contesto sanitario e sociale molto difficile e complesso;

-          molte persone transessuali si trovano ad operare in un contesto sociale di emarginazione ed esclusione anche familiare che spesso le spinge alla prostituzione e talvolta in un ambito di illegalità;

Per Sapere:

-          quante sono le persone transessuali detenute nelle carceri italiane; quante straniere e, tra queste, quante extracomunitarie;

-          in quali carceri vi sono reparti ad hoc per le persone transessuali e quanti ve ne sono in ognuno di questi;

-          quale tipo di trattamento detentivo viene loro riservato;

-          quale tipo di trattamento sanitario viene garantito loro riguardo il loro stato psico-fisico;

-          quali accordi vi sono con le Asl di competenza per garantire un’assistenza adeguata alle loro esigenze;

-          quali forme di protezione sono garantite alle persone transessuali rispetto al resto della popolazione carceraria all’interno delle carceri;

-          quale tipo di contatto e di reinserimento viene loro garantito;

-          quale tipo di sostegno e aiuto da parte di associazioni di volontariato viene garantito alle persone transessuali abbandonate dalle loro famiglie.

  

**I deputati radicali:  Matteo Mecacci, Marco Beltrandi, Rita Bernardini, Maria Antonietta Farina Coscioni, Maurizio Turco, Elisabetta Zamparutti.**