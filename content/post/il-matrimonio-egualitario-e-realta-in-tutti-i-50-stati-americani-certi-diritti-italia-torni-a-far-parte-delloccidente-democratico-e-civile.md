---
title: 'Il matrimonio egualitario è realtà in tutti i 50 stati americani. Certi Diritti: Italia torni a far parte dell''occidente democratico e civile'
date: Fri, 26 Jun 2015 16:25:31 +0000
draft: false
tags: [Americhe]
---

[![6934872-usa-flag-wallpaper](http://www.certidiritti.org/wp-content/uploads/2015/06/6934872-usa-flag-wallpaper-150x150.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/06/6934872-usa-flag-wallpaper.jpg)"L'America ci ha regalato un'altra grande lezione dando corpo al principio fondatore di quella nazione: we are all created equal, siamo creati tutti uguali". Così il commento a caldo di Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, alla notizia della sentenza della Suprema Corte americana che ha esteso a tutto il territorio degli Stati uniti il matrimonio egualitario. "L'Italia deve svegliarsi e tornare a far parte della famiglia occidentale riconoscendo il matrimonio egualitario e prendendo in seria considerazione questo movimento sociale che dall'Irlanda agli Stati Uniti mostra un consenso generale per l'estensione del matrimonio civile alle coppie dello stesso sesso. Sveglia Italia! Libertà, fraternità, uguaglianza!", conclude Guaiana.

Vogliamo che anche l'Italia raggiunga presto questo traguardo! Sostienici con la tua [**iscrizione** ](http://www.certidiritti.org/iscriviti/)o con un [**contributo libero**](http://www.certidiritti.org/donazioni/)