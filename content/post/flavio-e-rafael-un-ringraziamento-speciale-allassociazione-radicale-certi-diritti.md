---
title: 'Flavio e Rafael: un ringraziamento speciale all’associazione radicale Certi Diritti'
date: Fri, 27 Jul 2012 09:54:42 +0000
draft: false
tags: [Diritto di Famiglia]
---

I due ragazzi avevano ottenuto il ricongiungimento familiare in Italia dopo essersi sposati in Spagna.

  
  
nota di Certi Diritti:

### L’ordinanza di Reggio Emilia che ha accolto il ricorso contro il diniego al ricongiungimento familiare ad una coppia dello stesso sesso, sposata in Spagna, non è stata impugnata dall’Avvocatura dello Stato ed è quindi definitiva. Diventa perciò un precedente giuridico importante nell’affermazione per le coppie omosessuali del “ _diritto fondamentale di vivere liberamente una condizione di coppia_” affermato nella sentenza della Corte Costituzionale 138/10.

  
**La lettera:**  
Vogliamo  ringraziare tutti a coloro che ci hanno aiutato e hanno difeso il nostro diritto di vivere insieme. Grazie a voi, in Italia  siamo la prima Famiglia gay che ha potuto “ricongiungersi ”, il cui legame matrimoniale  è stato riconosciuto dallo stato Italiano sebbene solo ai fini del ricongiungimento famigliare. A Rafael  è stata rilasciata la Carta europea  per lungo soggiornanti  perché  considerato  famigliare di un Cittadino comunitario.  
Dopo una grande battaglia legale vinta con una Sentenza storica del Tribunale di Reggio Emilia ringraziamo la Giudice Emilia Tanasi per il coraggio avuto difendendo il nostro diritto alla vita famigliare e per aver applicato l’indicazione della sentenza n. 1328/2011 della Corte di Cassazione e aver ribadito che   “la nozione di ‘coniuge’ prevista dall’art. 2 d.lgs. n. 30/2007 deve essere determinata alla luce dell’ordinamento straniero in cui il vincolo matrimoniale è stato contratto; lo straniero che abbia contratto in Spagna un matrimonio con un cittadino dell’Unione dello stesso sesso deve essere qualificato quale ‘familiare’, ai fini del diritto al soggiorno in Italia”  
Vogliamo ringraziare l’ Avv. Giulia Perin che ci ha sostenuto con grande competenza  e l’Avv. Mario Frenna per la sua disponibilità. Infine un ringraziamento speciale va a Gabriella Friso del Direttivo di Certi Diritti per il  grande aiuto e sostegno che ci ha dato. Se non l’ avessimo conosciuta grazie a Gian Mario Felicetti sempre di “Certi Diritti”, non saremmo in Italia ...  
Un grazie anche a tutti i mezzi di comunicazione e stampa che hanno diffuso  la nostra storia nelle  televisioni e nei giornali di tutto il mondo.  
Noi ci auguriamo che la nostra battaglia possa aiutare  tutte le altre coppie dello stesso sesso che, sposate all’ Estero,  vogliono far riconoscere il loro diritto di essere considerate famiglie  qui in Italia, e … non abbiate paura o vergogna, ma fatevi avanti e difendete  il vostro diritto di essere felici .  
  
Rafael  Ernesto A.R.  
Enrico Flavio M.  
  
**[STORICA SENTENZA DEL TRIBUNALE DI REGGIO EMILIA >>>](http://www.certidiritti.org/2012/02/14/storica-sentenza-tribunale-reggio-emilia-coniuge-stesso-sesso-di-un-italiano-sposato-in-spagna-ha-diritto-a-vivere-in-italia/)**