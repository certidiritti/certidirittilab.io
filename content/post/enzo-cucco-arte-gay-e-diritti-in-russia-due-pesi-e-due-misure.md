---
title: 'Enzo Cucco, arte gay e diritti in Russia: "Due pesi e due misure?"'
date: Tue, 02 Sep 2014 14:19:30 +0000
draft: false
tags: [Russia]
---

[![901927_10200945697794212_128058654_o](http://www.certidiritti.org/wp-content/uploads/2014/09/901927_10200945697794212_128058654_o.jpg)](http://www.certidiritti.org/wp-content/uploads/2014/09/901927_10200945697794212_128058654_o.jpg)

Ci ha messo un paio di ore di riflessione il Comune di Torino a togliere il suo patrocinio alla Internazionale d’arte lgbti organizzata da Artevision. E dal suo punto di vista ha fatto bene, per una immagine inutilmente offensiva e superficiale.

L’immagine usata (donna nuda che appoggia il suo piede su icona ortodossa) ha raggiunto il suo obiettivo provocatorio ( un po’ come le Femen, non trovate? Ma per loro solo encomi ….) mentre della sostanza della questione (che sia artistica o no la foto, per esempio, e quale sia la realtà delle donne e delle persone lgbti in Russia ) nessuno si indigna più di tanto.

Il Comune di Torino continua i suoi rapporti con la Russia e San Pietroburgo, alla faccia dell’ordine del giorno votato dal Consiglio comunale che condannava senza appello la legge antigay e il regime antidemocratico che quel Paese ha instaurato. Il Consiglio comunale votò un odg, fece la lettera formale di segnalazione all’ambasciatore (mai resa pèubblica peraltro .... ) tutto è tornato come prima e speriamoche, nelle sedi dove sarebbe stato possibile, i funzionari del Comune, l’Assessore alla Cultura, i dirigenti delle torinesi istituzioni culturali abbiano sollevato la questione.

Ma tranquilli, tra poco l’Europa entra in guerra contro Putin. E noi zitti e mosca, a fare i nostri affari come sempre. Come se ci fossero solo i musei, le orchestre ed i teatri di San Pietroburgo in Europa.

Noi chiediamo al Sindaco, ed al Comune, di essere coerenti: riprendere l' odg., rompere definitivamente le relazioni culturali con le Istituzioni russe e segnalare il comportamento molto più che offensivo del Governo e del parlamento nei confronti delle donne, dei persone lgbti, degli immigrati. Oggi anche degli ucraini.