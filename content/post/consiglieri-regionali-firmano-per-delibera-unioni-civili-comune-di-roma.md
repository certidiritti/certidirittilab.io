---
title: 'Consiglieri regionali firmano per delibera unioni civili comune di roma'
date: Thu, 23 Feb 2012 20:41:04 +0000
draft: false
tags: [Diritto di Famiglia]
---

I consiglieri regionali del Lazio di diversi gruppi (Radicali, Sel, Idv, Psi, Verdi, Pd, Federazione della sinistra) firmano la proposta di delibera di iniziativa popolare sulle unioni civili al comune di Roma. Continua la mobilitazione per la raccolta firme in tutta la città.

Roma, 23 febbraio 2012

I Consiglieri Regionali Giuseppe Rossodivita e Rocco Berardo, Radicali della Lista Bonino Pannella, Federalisti Europei; Angelo Bonelli dei Verdi; Fabio Peduzzi e Fabio Nobili della Federazione della Sinistra; Luigi Nieri e Luigi Zaratti di Sel; Enzo Foschi e Tonino D’Annibale del Pd; Vincenzo Maruccio, Annamaria Tedeschi, Claudio Bucci Giulia Rodano di Idv; Luciano Romanzi del Psi; hanno firmato oggi  la proposta di delibera di iniziativa popolare sulle Unioni Civili promossa da decine di  associazioni e organizzazioni politiche di Roma.

I promotori ringraziano i Consiglieri Regionali dei diversi gruppi che con questo gesto sostengono la proposta di delibera di iniziativa popolare per il riconoscimento delle Unioni Civili nel Comune di Roma che ha l’obiettivo di dare rilevanza civile alle tante coppie conviventi che oggi non possono accedere ai diversi servizi e aiuti del Comune di Roma. La raccolta firme è iniziata sabato 18 febbraio e proseguirà per tre mesi. Obiettivo dei promotori è di raccogliere decine di migliaia di firme, oltre alle 5.000 necessarie.

L’Iniziativa è promossa da Radicali Roma, Associazione Radicale Certi Diritti,  Circolo di cultura omosessuale Mario Mieli, Sel Roma Area metropolitana, Forum queer Sel, Uaar Roma, Giovani Idv, Arcigay Roma, Gay Center, Consulta romana per la laicità delle istituzioni, Arcilesbica Roma, Agedo Roma, Roma Rainbow Choir, QueerLab, PianetaQueer.it, Famiglie Arcobaleno, Yellow Sport, Fondazione Massimo Consoli, Gayroma.it, DiGayProject, Luiss Arcobaleno, Gay & Geo gruppo trekking Roma, A.F.F.I.Associazione Federativa Femminista Internazionale della Casa Internazionale delle Donne, Associazione Riprendiamoci la politica e PSI – Roma.  
   
Tutte le info sui punti di raccolta delle firme  e dettagli si possono trovare al seguente sito:

teniamofamiglia.blogspot.com     

e-mail: [roma.unionicivili@gmail.com](mailto:roma.unionicivili@gmail.com)  
  

**iscriviti alla newsletter >[  
](newsletter/newsletter)[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**