---
title: 'A Napoli due giorni sulla laicità e la dignità dei cittadini'
date: Fri, 14 Oct 2011 08:13:22 +0000
draft: false
tags: [Politica]
---

Si terranno il 14 e il 15 ottobre presso il ‘Maschio Angioino’ di Napoli due giorni di incontri dal titolo “Napoli laica: la dignità dei cittadini” voluti dalla Consulta Napoletana per la laicità delle Istituzioni per discutere della laicità a Napoli e della dignità dei cittadini.

I lavori del secondo giorno saranno dedicati all’omofobia. Alle ore 9.00 nell’Antisala dei Baroni si terrà l’incontro-dibattito “I diritti dell’arcobaleno umano” coordinato dal Prof. Claudio Finelli ed a cui parteciperanno, tra gli altri Antonella di Nocera (Assessore Cultura Comune di Napoli), Sergio Rovasio (Segretario Associazione Certi Diritti), Salvatore Simioli (Avvocato, ex Presidente Arcigay Napoli), Carlo Cremona (Presidente Iken Onlus), Antonello Sannino (Presidente Arcigay Salerno), Donata Ferrante (Responsabile sportello glbt Iken CGIL Avellino), Paolo Valerio (Professore di Psicologia clinica), Roberto Azzurro (Attore e regista).

Si discuterà, oltre che della bocciatura alla Camera di una legge contro l’omofobia, anche della richiesta del Registro delle Unioni Civili da predisporre nel Comune di Napoli.