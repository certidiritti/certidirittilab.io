---
title: 'FATE IN FRETTA! LE UNIONI CIVILI DEVONO ESSERE REGISTRATE SUBITO.'
date: Mon, 18 Jul 2016 21:53:00 +0000
draft: false
tags: [Diritto di Famiglia]
---

[](http://www.certidiritti.org/wp-content/uploads/2016/07/fate-in-fretta.pdf)[![mani-1024x683](http://www.certidiritti.org/wp-content/uploads/2016/07/mani-1024x683-300x200.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/07/mani-1024x683.jpg)Margherita ha 53 anni, una compagna da 25 anni e un tumore allo stadio terminale. Dario Guarise ha 73 anni, un compagno da 38 e anche lui è malato. Entrambi chiedono di accelerare l'iter di applicazione della legge Cirinnà.

"Margherita e Dario hanno dato voce alle tante coppie dello stesso sesso nella medesima situazione e li ringraziamo per questo. Ma i sindaci italiani cosa aspettano?", si chiede Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti.

"Ai sensi del comma 35 della legge 76/2016, le disposizioni sulle unioni civili acquistano efficacia a decorrere dalla data di entrata in vigore della legge. I comuni quindi **devono**, non solo possono, procedere da subito a registrare queste unioni", continua il segretario dell'Associazione Radicale Certi Diritti.

"I sindaci italiani possono fare qualcosa per dare a Margherita e alla sua compagna, come a Dario e al suo compagno, ciò che spetta loro: devono semplicemente rispettare la legge e registrare **subito** le unioni civili. Le coppie dello stesso sesso, comprese quelle sposate o unite civilmente all'estero non aspettino altro, preparino subito i documenti, [seguendo le nostre indicazioni](http://www.certidiritti.org/wp-content/uploads/2016/07/fate-in-fretta.pdf), e vadano in Comune. Non vi possono dire di no", conclude Guaiana.

Roma, 19 luglio 2016