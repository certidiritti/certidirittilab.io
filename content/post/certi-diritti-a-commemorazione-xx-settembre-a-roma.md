---
title: 'Laicità: commemorazione XX Settembre a Roma'
date: Wed, 19 Sep 2018 15:54:03 +0000
draft: false
tags: [Politica]
---

Anche quest'anno certi diritti celebrerà la ricorrenza laica della fine del potere temporale dei Papi con un **presidio a Roma, il 20 settembre 2018, dalle 16 alle 17, presso il Monumento alla Breccia di Porta Pia in Corso d’Italia**. La manifestazione è convocata con Radicali Italiani e Associazione Luca Coscioni. "Abrogazione della legge sull’interruzione volontaria di gravidanza, cancellazione della libertà sessuale, proibizionismo sulla ricerca scientifica… La risposta alle nuovi voci integraliste in Parlamento è democrazia, partecipazione, Europa." si legge nella nota. "Le limitazioni della libertà individuale, oggi, appartengono ufficialmente alla visione politica del controllo sulle nostre vite. Sta ai 'credenti' nella democrazia, nella partecipazione, nella scienza e nel Diritto unirsi in una proposta europea all’altezza del pericolo dei nuovi autoritarismi in ascesa." Hanno aderito alla manifestazione anche Chiesa Pastafariana italiana, UAAR-Roma e Circolo di cultura omosessuale Mario Mieli. ![20 settembre 2018-2](http://www.certidiritti.org/wp-content/uploads/2018/09/20-settembre-2018-2.png)