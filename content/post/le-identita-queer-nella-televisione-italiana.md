---
title: 'LE IDENTITA'' QUEER NELLA TELEVISIONE ITALIANA'
date: Mon, 03 Nov 2008 15:04:19 +0000
draft: false
tags: [Comunicati stampa]
---

Ricerca sulla rappresentazione di identità Queer negli ultimi 20 anni di televisione Italiana

Luca **MALICI** è un ricercatore italiano presso dell'Università di Birmingham (UK), che sta svolgendo una ricerca quali-quantitativa di 3 anni che guarda alla rappresentazione, circolazione e, soprattutto, ricezione di realtà queer (o meglio in Italia, LGBTQI) nella televisione generalista Italiana (Rai, Mediaset, La7/MTV Italia) degli ultimi 20 anni.

  
A livello internazionale esiste pochissima ricerca, soprattutto per quanto riguarda la ricezione o, come dicono qui, "audience studies".  
In Italia, questo tipo di studio sembra assumere particolare rilevanza se si guarda a come queste identità vengono ancora percepite dalla società eteronormativa e cattolica, insieme alla generale indifferenza istituzionale (politica e legislativa).  
I risultati del sondaggio rappresenteranno il punto di vista delle diverse comunità queer/LGBTQI. Dai dati che si otterranno nascerà una video-compilation e una ricerca etnografica con un campione di famiglie italiane, cui sarà richiesto il "loro" punto di vista su tali rappresentazioni in film/programmi, e sulle stesse identità in questione.

Il Dott. Malici ha recentemente creato un sondaggio online, parte fondamentale del progetto, dove si chiede il punto di vista della comunità LGBTQI Italiana. E' necessario dare la massima visibilità al sondaggio:

[http://www.polldaddy.com/s/43EA271E34C66969/](http://www.polldaddy.com/s/43EA271E34C66969/)

Il sondaggio è anonimo e richiede solo pochi minuti.

**Completando questo Survey darai un contributo significativo a questo studio. La tua opinione è molto importante!**