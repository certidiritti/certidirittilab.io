---
title: 'La guerra infinita agli omosessuali'
date: Sat, 28 Mar 2009 06:53:49 +0000
draft: false
tags: [Senza categoria]
---

Dopo un’interminabile serie di rifiuti e di diritti non riconosciuti, la situazione viene ulteriormente peggiorata. Lucio Malan (http://www.senato.it/leg/16/BGT/Schede/Attsen/00001407.htm), Senatore del PdL, sta raccogliendo le adesioni in Senato per depositare un disegno di legge teso ad integrare l’art. 29 della Costituzione per specificare che la famiglia come società naturale è unicamente quella fondata matrimonio fra UN uomo e UNA donna.

Se qualcuno desidera leggere il farneticante argomentare che accompagna il disegno di legge, può agevolmente farlo qui (http://lampidipensiero.wordpress.com/strumenti/documenti/malan-lucio-disengo-di-legge-di-modifica-dellart29-della-costituzione-febbraio-2009/).

Io mi limito a registrare e a consegnare al dibattito dei miei lettori gli aspetti che ritengo più eclatanti dei ragionamenti di Malan.

1\. Due piccioni con una fava.

Agitando il fantasma della famiglia poligamica (immediatamente associato alla presenza in Italia di 800.000 islamici) si mettono sullo stesso piano la poligamia e l’omosessualità. Un ragionamento arditissimo, per cui il vero obiettivo (impedire il matrimonio degli omosessuali) viene mascherato e “spinto” dalla ripulsa sociale nei confronti della poligamia, vista peraltro solo nella sua forma della poliginia, dato che la poliandria è un concetto completamente ignorato (nel senso antropologico del non conosciuto e non del non preso in considerazione).

2\. Nulla deve cambiare.

Richiamandosi alle intenzioni del legislatore costituente, Malan sostiene che non vi è nulla da interpretare e che indubbiamente questi volesse intendere letteralmente ciò che la proposta di Malan vuole affermare. In sostanza, non si tiene conto di oltre 60 anni di evoluzione della società, del costume e dei valori. Ma neanche dell’intero contesto internazionale che si è mosso modificando radicalmente il quadro giuridico.

3\. L’importante è non sapere.

I ragionamenti che pretenderebbero di affermare che nel corso della nostra storia non ci sono mai stati riconoscimenti pubblici e sanzionati da qualche forma di cerimoniale religioso o civile, fanno emergere un deciso profilo di ignoranza del nostro valente parlamentare, che in tutta evidenza, parla di cose sulle quali non ha condotto sufficienti approfondimenti. Oltre che al catechismo cattolico, infatti avrebbe fatto bene a documentarsi sull’eccellente raccolta di saggi internazionali curata dallo storico Robert Aldrich che si intitola Vita e cultura gay, Storia universale dell’omosessualità dall’antichità ad oggi (Cicero Editore - http://www.ciceroeditore.com/VitaCulturaGay.html), nel quale si portano ampi esempi di rituali (evidentemente da considerare nei rispettivi contesti di estrema repressione dell’omosessualità) in base ai quali coppie di omosessuali attestavano il loro profondo e duraturo rapporto. Dati i profili penali collegati alla sessualità “contro natura”, tali relazioni venivano comunemente intese come amicizie (termine che aveva un’accezione differente da quella odierna) in modo da salvaguardare l’integrità fisica dei partner, che erano talmente intimamente legati da condividere addirittura l’ultima dimora, esattamente come avveniva alle coppie eterosessuali.

4\. Matrimonio=figli.

Incommentabile assunto, soprattutto alla luce dello stato in cui versa oggi la famiglia tradizionale che tanto si intende proteggere, luogo fisico e psicologico, nel quale si continuano a perpetrare i più odiosi crimini di violenza nei confronti delle donne e dei minori: è questo l’ambiente ottimale per far germinare l’amore e garantire lo sviluppo delle nuove generazioni? Si tratta in tutta evidenza di una posizione antistorica e antisociale. E’ mio profondo convincimento che le varie forme che l’amore coniugale può prendere all’interno della società, nel rispetto dei principi inderogabili di uguaglianza, debbano essere riconosciute e trovare ampia tutela legale poiché non è compito dello Stato decidere se un rapporto di natura coniugale ed affettiva debba avere la preminenza rispetto agli altri. Ognuno deve poter essere libero di scegliere come vivere la propria affettività e come costruire i suoi legami parentali, atteso che non è la conservazione della specie l’obiettivo del matrimonio nelle società moderne, quanto piuttosto costituire il luogo e l’ambito in cui l’amore trova la sua realizzazione ANCHE attraverso la procreazione e in vista di una TUTELA ACCESSORIA dei diritti di natura patrimoniale, creati a difesa delle parti più deboli del rapporto. In sostanza, dunque, dopo essere passati tra roghi, torture, mutilazioni, prigioni e negazione sociale, la riforma Malan ci propone una nuova forma di barbarie: negare la dignità e la parità dell’amore omosessuale ed il suo riconoscimento sociale. Grazie senatore.