---
title: 'Anche i diritti civili nei programmi elettorali di Roma e Lazio. Incontro 16 novembre'
date: Mon, 12 Nov 2012 15:02:23 +0000
draft: false
tags: [Politica]
---

L'associazione radicale promuove un primo incontro con tutte le associazioni lgbte di Roma e Lazio con l'obiettivo di elaborare insieme un documento da proporre a tutte le forze politiche. L'appuntamento è per venerdì 16 novembre alle ore 19 presso la sede in via di Torre Argentina, 76 a Roma.

In vista delle scadenze elettorali a ROma nel Lazio, l'Associazione Radicale Certi Diritti invita tutte le associazioni Lgbte e gli esponenti della società civile interessati ad un primo incontro presso la sede di Via di Torre Argentina, 76 a Roma con l'obiettivo di elaborare insieme un documento da proporre alle forze politiche da inserire sia nel programma elettorale dei singoli partiti che nella coalizione che gli stessi formeranno.

**Questo primo incontro lo faremo venerdì 16 novembre 2012, in Via di Torre Argentina, 76 – Roma, alle ore 19, presso la sede dell’Associazione Radicale Certi Diritti (3° piano).**

**[L'EVENTO SU FACEBOOK >>>](http://www.facebook.com/events/247047838755506/)**

  
Tra i temi che vorremmo far inserire nei programmi elettorali vi sono:

\- Unioni civili;

\- Lotta alla violenza e alle discriminazioni;

\- Campagne di prevenzione sulle Mts;

\- Aiuto e sostegno alle persone transessuali;

\- Campagne di aiuto e sostegno alle persone Lgbt;

\- Altre idee e proposte;

  
**Conferma la tua partecipazione scrivendo a [info@certidiritti.it](mailto:info@certidiritti.it)**  
  

Sergio Rovasio - Associazione Radicale Certi Diritti

Giacomo Cellottini - Tesoriere Certi Diritti

Matteo Mainardi - Coordinatore Certi Diritti Roma