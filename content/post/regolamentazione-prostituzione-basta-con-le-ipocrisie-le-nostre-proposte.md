---
title: 'REGOLAMENTAZIONE PROSTITUZIONE: BASTA CON LE IPOCRISIE, LE NOSTRE PROPOSTE'
date: Sun, 13 Jul 2008 07:02:11 +0000
draft: false
tags: [Comunicati stampa]
---

Giovedì 17 luglio si terrà al Senato, presso la Sala Stampa, una Conferenza promossa dalla Senatrice radicale Donatella Poretti insieme all'Associaizone Radicale Certi Diritti sulla proposta di regolamentazione/legalizzazione della prostituzione. Alla Conferenza parteciperà anche la Presidente del Comitato per i diritti civili delle prostitute Pia Covre.