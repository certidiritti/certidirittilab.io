---
title: 'DECRIMINALIZZAZIONE OMOSESSUALITA'' ALL''ONU: CITTA'' DI TORINO ADERISCE'
date: Thu, 22 Apr 2010 11:16:58 +0000
draft: false
tags: [Comunicati stampa, ITALIA, OMOSESSUALITA', ONU, torino, unione europea decriminalizzazione]
---

Lunedì 19 aprile il Consiglio comunale di Torino ha approvato una mozione per sostenere l´iniziativa presso l´Onu con cui l´Italia aderisce alla proposta dell´Unione europea di decriminalizzazione universale dell´omosessualità.

Tale atto, che Sindaco e Giunta devono ora trasmettere al Governo, impegna le istituzioni cittadine a dare risonanza alla giornata contro l´omofobia, adottando iniziative utili a coinvolgere al massimo le istituzioni per l'affermazione di una cultura dei diritti delle persone che prescinda dai loro orientamenti sessuali.

Il Coordinamento Torino Pride LGBT si rallegra per tale deliberazione che, come dice il Coordinatore Daniele Viotti: "Ribadisce ulteriormente il ruolo importante che a Torino svolge il Comune. L'amministrazione cittadina si è spesso fatta promotrice di iniziative molto importanti per le persone Gay,Lesbiche, Bisessuali e Transessuali, soprattutto attraverso il Servizio LGBT del Settore Pari Opportunità che si impegna, ormai da anni, nel superamento di ogni discriminazione di matrice omofobica e tranfobica."L'approvazione di questa mozione", aggiunge Roberta Padovano del Direttivo del Coordinamento, "è un segno molto positivo che rafforzerà di sicuro il nostro impegno soprattutto nei campi della formazione e delle iniziative politiche per il riconoscimento di una piena cittadinanza delle persone GLBT"."Speriamo", aggiunge Christian Ballarin, altro membro del Direttivo,"che la buona collaborazione tra comune di Torino e regione Piemonte sulle tematiche GLBT prosegua anche con la nuova amministrazione regionale, come con la precedente, anche se le prime dichiarazioni del Presidente Cota non lasciano prevedere nulla di buono."

Ufficio Stampa del Coordinamento Torino Pride LGBT