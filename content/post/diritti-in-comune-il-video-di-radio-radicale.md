---
title: 'DIRITTI IN COMUNE - Il video di Radio Radicale'
date: Wed, 11 Jun 2008 17:16:03 +0000
draft: false
tags: [Comunicati stampa]
---

E' possibile [consultare il video](http://www.radioradicale.it/scheda/255573/diritti-in-comune-gli-omosessuali-italiani-e-la-pubblicazione-degli-atti-del-matrimonio) della conferenza stampa tenutasi a Milano venerdì 6 Giugno 2008, presso il palazzo comunale di via Marino.