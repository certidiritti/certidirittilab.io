---
title: 'Uganda: proposta di legge antigay di nuovo in Parlamento. In aumento gli atti di omofobia nel Paese'
date: Thu, 27 Oct 2011 14:05:00 +0000
draft: false
tags: [Africa]
---

Certi Diritti ha informato l'Ue, l'Osce e le ong che si occuoano della difesa dei diritti civili e umani in Uganda.

Roma, 27 ottobre 2011  
  
Comunicato Stampa dell’Associazione Radicale Certi Diritti

In Uganda la proposta di legge contro le persone gay è stata reinserita ieri nell'agenda parlamentare. Il testo della proposta di legge è rimasto più o meno lo stesso di quello della scorsa legislatura. Nel testo sono state incrementate le aggravanti per gli atti omosessuali con forti rischi che la pena di morte, in alcuni casi, tramite altri articoli di legge in vigore, si possa applicare molto facilmente.

Il principale sostenitore del disegno di legge, è il deputato David Bahati, che l'anno scorso ha difeso il disegno di legge per Rachel Maddow, oggi è il presidente del partito di governo NRM.

Le pressioni internazionali sembrano non avere in questo momento effetto sui parlamentari; per molti di loro tali pressioni non possono interferire con alcuni aspetti della cultura e delle credenze ugandesi. Il Governo ugandese, che è in difficoltà per cause collegate alla sua azione autoritaria, potrebbe cercare di usare nuovamente il decreto per sviare l'attenzione anche se molti paesi occidentali, donatori, hanno detto che in caso di approvazione della legge bloccherebbero gli aiuti.

Pare che se la proposta di legge arriva al voto, molto probabilmente verrà approvata, senza che il Presidente Yowery Musaveni possa opporsi, nemmeno con il suo potere di veto.

Alcuni attivisti di Ong locali segnalano che a Kampala c’è un incremento di atti di omofobia e molti di loro sono dovuti intervenire per garantire forme di protezione e aiuto alle persone omosessuali e ad alcune Associazioni. In questi giorni sono state attaccate violentemente due sedi di organizzazioni Lgbt e c’è stato un tentativo di aggressione nei locali di Sexual Minorities Uganda (l’Associazione nella quale militava David Kato Kisule). Inoltre è stata incendiata la casa di un attivista che fortunatamente e' riuscito a scappare dal suo letto prima che la casa venisse distrutta quasi. Un paio di locali di ritrovo per lesbiche e gay sono stati chiusi.

La leader ugandese di Smug, Kasha Jacqueline Nabagesera, è costretta a nascondersi per paura di rimanere a lungo nello stesso posto. La polizia e le forze di sicurezza regolarmente la fermano e la minacciano.

A Ginevra la delegazione del Governo ugandese, durante la revisione del rapporto sullo stato dei Diritti Umani, ha riaffermato che la proposta di legge e' un atto parlamentare e che in quanto tale il governo non può fare nulla per bloccarlo.

L’Associazione Radicale Certi Diritti ha immediatamente informato i partecipanti al Congresso di Ilga-Europe, i membri dell’Intergruppo Lgbt al Parlamento Europeo, Matteo Mecacci, Presidente della Commissione Democrazia, Diritti umani e questioni umanitarie dell'Assemblea parlamentare dell'Osce, di quanto è venuta a conoscenza nella speranza che si attivino quanto prima tutte le azioni politiche necessarie.

*nella foto il funerale di David Kato Kisule, attivista gay ugandese e iscritto di Certi Diritti, ucciso a Kampala il 26 gennaio 2011

**iscriviti alla newsletter >[  
http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**