---
title: '2008-0140-Discrimination-ST05790-CO01.EN10'
date: Wed, 17 Feb 2010 15:35:14 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 17 February 2010

Interinstitutional File:

2008/0140 (CNS)

5790/10

COR 1

LIMITE

SOC 49

JAI 84

MI 31

  

  

  

  

  

CORRIGENDUM TO THE OUTCOME OF PROCEEDINGS

from :

The Working Party on Social Questions

on :

22 January 2010

No. prev. doc. :

5188/10 SOC 13 JAI 16 MI 8

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

The first paragraph on p. 3 is replaced with the following:

"BE, BG, FR, HU, AT, PT, RO, SK, **FI,** SE, UK expressed their support for the ongoing work on the draft Directive, FR explaining that its main outstanding concerns related to age discrimination and the proposal's potential implications for secular education in France."

The second and third sentence of the third paragraph on p. 5 are replaced with the following:

"IT **and BG** also expressed doubts, **BG viewing the deletion of the corresponding text in Article 2(3a) in a positive light**."

The first sentence of the first paragraph on p. 6 is replaced with the following:

  

"DK, MT and UK raised the question as to whether "national regulations" (Article 2(6)) covered non-legislative rules such as executive orders."

The third sentence of the second paragraph on p. 6 is replaced with the following:

"**BG,** IT, MT and NL supported these suggestions."

The second sentence of the fourth paragraph on p. 6 is replaced with the following:

"**UK joined its voice to this view, recalling** its suggestion that minors be excluded from the age discrimination provisions."

\_\_\_\_\_\_\_\_\_\_\_\_\_\_