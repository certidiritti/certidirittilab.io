---
title: 'TRANSGENDER DAY OF REMEMBRANCE, L''ITALIA COMMEMORA CON LA MORTE DI BRENDA'
date: Fri, 20 Nov 2009 14:18:29 +0000
draft: false
tags: [Comunicati stampa]
---

Roma, 20 novembre 2009

_**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**_

_**

Purtroppo questa è l’Italia di oggi, che vive simili tragedie come se nulla fosse. Brenda sarà ‘usata’ da qualche lacchè di regime, da qualche viscido servo che strumentalizzerà la sua immagine per alimentare un po’ di cronaca nera parlando di ‘misteri italiani’, di cui non gliene importa davvero nulla, e nemmeno gli importa della condizione e del contesto nel quale le persone transessuali sono costrette a vivere.

Questi miserabili, che impongono il sonno della ragione a milioni di italiani, sono esattamente coloro che non parlano mai di diritti per le persone lesbiche, gay, transessuali ma anzi, impongono di loro una visione ridicola e  patetica, alimentando l’ipocrisia e la falsità: quelle del peccato e dell’assoluzione, del proibizionismo criminogeno, dell’omofobia e della transfobia, dell’odio e della violenza appunto.

Noi continueremo la nostra lotta nonviolenta per la difesa dei diritti civili e umani delle persone LGBT. Questa penosa classe dirigente offre oggi all’Italia la tragica morte di una persona transessuale, nel giorno del ‘Transgender Day of Remembrance’: un altro drammatico ‘record’ di cui vergognarsi”.

**_“E’ difficile credere che quella di Brenda sia una morte accidentale o un suicidio. Crediamo invece che quanto avvenuto sia esattamente quello che un banalissimo copione di film gialli avrebbe previsto  per ‘eliminare’ una persona ‘scomoda’, che poteva dare fastidio a qualcuno che si ritiene intoccabile, certamente ‘altolocato’ come molti lo sono in modo miserabile in questo paese.

**MORTE DI BRENDA: QUANTO AVVENUTO NEL ‘TRANSGENDER DAY OF REMEMBRANCE’ E’ UN ESEMPIO LAMPANTE DELLA PENOSA SITUAZIONE IN  CUI SI TROVA L’ITALIA**