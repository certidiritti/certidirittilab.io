---
title: 'Il Parlamento Europeo ha votato rapporto sui Diritti Dell''uomo, anche in difesa della comunità lgbt'
date: Fri, 17 Dec 2010 10:43:54 +0000
draft: false
tags: [Comunicati stampa]
---

**L'ASSOCIAZIONE RADICALE CERTI DIRITTI RINGRAZIA I DEPUTATI DEL PARLAMENTO EUROPEO PER LA CONTINUA AZIONE PER I DIRITTI DELLE PERSONE LGBT NELL'UE E NEL MONDO.**

**Roma, 17 dicembre 2010**

**Comunicato Stampa dell'Associazione Radicale Certi Diritti**

Il Parlamento europeo ha votato nel corso della sessione del 13-16 dicembre una serie di relazioni e risoluzioni che sostengono i diritti delle persone LGBT nella Unione Europea e nel mondo.

La relazione Andrikiene sulla situazione dei diritti umani nel mondo nel 2009, accoglie con favore le iniziative dell'UE a livello internazionale e nelle Nazioni Unite per la decriminalizzazione dell'omosessualità nel mondo e per la condanna di tali violazioni, chiede di implementare le misure contenute nel manuale per i diritti LGBT con l'obiettivo di difendere tali diritti nelle relazioni con i paesi terzi, condanna il progetto di legge Ugandese contro l'omosessualità, mentre felicita l'India per la decisione di decriminalizzare l'omosessualità.

La relazione Gal sugli aspetti istituzionali relativi ai diritti umani dopo il Trattato di Lisbona richiama le violazioni dei diritti delle coppie dello stesso sesso nell'UE e chiede azioni volte a combattere tutte le discriminazioni, come pure un meccasimo di "congelamento" delle leggi nazionali che non rispettino i diritti umani prima che queste entrino in vigore, nel caso in cui le istituzioni UE sollevino obiezioni.

La relazione d'urgenza sulla legge ugandese anti-omosessualità, che prevede l'introduzione della pena di morte per le persone LGBT, chiede alle autorità ugandesi di bloccare la procedura legislativa e di rivedere la legge attuale che criminalizza l'omosessualità.

Vari deputati europei hanno sollevato interrogazioni alla Commissione sulla necessità di assicurare il mutuo riconoscimento delle coppie dello stesso sesso e di prevedere una tabella di marcia sui diritti LGBT. La Commissione europea ha presentato martedi un documento di consultazione sulla prima questione.

Il Parlamento Europeo ha inoltre deciso di esaminare nel corso della sua sessione di gennaio una serie di interrogazioni sulla violazione dei diritti LGBT in Lituania e di adottare una risoluzione sul tema.

Certi Diritti ringrazia i parlamentari europei che hanno votato a favore di questi importanti documenti che hanno l'obiettivo di continuare ad agire per difendere i diritti delle persone LGBT nel mondo.