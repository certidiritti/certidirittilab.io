---
title: 'GIORNATA INTERNAZIONALE CONTRO L''OMOFOBIA: L''INCONTRO AL QUIRINALE'
date: Mon, 17 May 2010 10:32:39 +0000
draft: false
tags: [Comunicati stampa]
---

**GIORNATA INTERNAZIONALE CONTRO L’OMOFOBIA: CERTI DIRITTI RINGRAZIA IL PRESIDENTE NAPOLITANO. GLI INTERVENTI DEL PRESIDENTE DELLA REPUBBLICA E DEL MINISTRO CARFAGNA FANNO SPERARE. CONSEGNATE AL PRESIDENTE LE QUASI 13.000 FIRME PER CONFERIRE UNA ONOREFICIENZA A MARIA LUISA MAZZARELLA.**

Roma, 17 maggio 2010

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

"Il Presidente della Repubblica Giorgio Napoletano, oggi, in occasione della Giornata Internazionale contro l'Omofobia, grazie all’intervento dell’On. Paola Concia, ha ricevuto al Quirinale le Associazioni e gli esponenti della comunità lgbt italiana. Durante l’incontro vi sono stati gli interventi di Paola Concia, deputata del Pd, Rita De Santis, Presidente dell’Agedo e del Ministro Mara Carfagna. Al termine il Presidente Giorgio Napolitano ha sottolineato la necessità che la battaglia per il riconoscimento dei diritti venga fatta propria dagli stessi eterosessuali quale conquista di civiltà e progresso così come avvenuto per altre battaglie sui diritti civili, come quella per i diritti delle donne.

Abbiamo apprezzato le parole del Ministro Mara Carfagna che ha sottolineato la necessità di leggi e norme che tutelino maggiormente le persone oggi colpite da pregiudizio e violenza. Il ministro ha riconosciuto di aver superato il pregiudizio nei confronti delle persone lesbiche e gay e di riconoscere della necessità di fare leggi adeguate come quella per la lotta all’omofobia. **Occorre ora che il Governo sostenga le varie proposte giacenti in Parlamento e quella contro l’omofobia in discussione alla Commissione Giustizia della Camera dei deputati.**

L’Associazione Radicale Certi Diritti ringrazia l’On. Paola Concia e a Rita De Santis per aver saputo sintetizzare, nei loro interventi davanti al Presidente Giorgio Napolitano, le richieste relative al riconoscimento dei diritti e della necessità di superare ogni forma di discriminazione di tutta la comunità lgbt. I loro interventi sono pubblicati nel sito[www.certidiritti.it](http://www.certidiritti.it/)

Al termine dell’incontro Certi Diritti ha consegnato al Presidente della Repubblica le quasi 13.000 firme raccolte dal portale [www.gay.it](http://www.gay.it/) per conferire una onoreficienza a Maria Luisa Mazzarella che il 23 giugno 2009, a Napoli, fu aggredita e malmenata da un gruppo di bulli per aver difeso un amico omosessuale offeso e insultato poco prima.

**La Giornata** **Internazionale** **contro l’Omofobia (IDHAO,International Day Against Homophobia) è nata il 17 maggio 2005, a 15 anni esatti dalla rimozione dell'omosessualità dalla lista delle malattie mentali nella classificazione internazionale delle malattie pubblicata dall'Organizzazione mondiale della sanità. Nel 2007, in seguito ad alcune dichiarazioni di autorità polacche contro la comunità LGBT, l'Unione europea ha istituito ufficialmente la giornata contro l'omofobia sul suo territorio".**