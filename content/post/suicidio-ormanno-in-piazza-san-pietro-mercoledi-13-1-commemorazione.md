---
title: 'SUICIDIO ORMANNO IN PIAZZA SAN PIETRO: MERCOLEDI'' 13-1 COMMEMORAZIONE'
date: Mon, 11 Jan 2010 15:21:56 +0000
draft: false
tags: [Comunicati stampa]
---

Anche l'Associazione Radicale Certi Diritti aderisce e partecipa all'inizaitiva promossa da Arcigay Roma Mercoledì 13 gennaio 2010 per ricordare  Alfredo Ormanno che nel 1998 si tolse la vita in Piazza San Pietro in segno di protesta contro l'omofobia vaticana.

´UN FIORE PER ALFREDO ORMANDO´  
Mercoledì 13 gennaio 2010 | ore 18.30 | Piazza Pio XII  
(Piazza San Pietro)

Incontriamoci e portiamo un fiore per non dimenticare, mai  
Come ogni anno, la comunità lgbt ricorda Alfredo Ormando, che il 13 gennaio 1998 si tolse la vita dandosi fuoco in piazza San Pietro, in segno di protesta contro l'omofobia vaticana. Fu trasportato all'ospedale Sant'Eugenio dove morì dopo dieci giorni di atroce agonia.  
A dodici anni di distanza da quel tragico episodio che scosse profondamente la comunità lesbica e gay internazionale, è importante continuar e a riflettere su quel gesto disperato e doloroso.  
Arcigay promuove 'un fiore per Ormando': incontriamoci mercoledì 13 gennaio 2010 alle ore 18.30 in piazza Pio XII (nei pressi di piazza San Pietro). Portiamo un fiore per commemorarlo, riflettere e non dimenticare, mai.  
A SEGUIRE, SEGNALIAMO:  
Nuova Proposta presenta:  
proiezione del film:  
"Roma ad 000"  
Iniziativa in memoria di Alfredo Ormando  
Alle 20.30, dopo il ricordo in piazza Pio XII, in via Marianna Dionigi 59 la proiezione del film, film di Fausto Pisanelli sull´anno 2000 in cui si sono incrociati Giubileo e World Pride a Roma.