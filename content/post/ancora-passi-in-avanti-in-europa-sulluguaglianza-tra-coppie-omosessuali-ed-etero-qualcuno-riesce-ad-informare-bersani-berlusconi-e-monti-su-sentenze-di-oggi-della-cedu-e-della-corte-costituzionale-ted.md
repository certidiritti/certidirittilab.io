---
title: 'Ancora passi in avanti in Europa sull''uguaglianza tra coppie omosessuali ed etero: qualcuno riesce ad informare Bersani, Berlusconi e Monti su sentenze di oggi della CEDU e della Corte costituzionale tedesca?'
date: Tue, 19 Feb 2013 14:39:49 +0000
draft: false
tags: [Europa]
---

Dichiarazione dell’Associazione radicale Certi Diritti.

Roma, 19 febbraio 2013

Se qualcuno dei nostri politici avesse la bontà di leggere e capire cosa sta accadendo in Europa sull’uguaglianza tra coppie omosessuali e coppie eterosessuali forse le nostre aspettative per il prossimo parlamento sarebbero diverse.

Proprio oggi due sentenze hanno confermato, senza ombra di dubbio, che il principio di uguaglianza è al centro dell’attenzione dei tribunali e dei Parlamenti:

la Grande Chambre della Corte europea per i diritti umani di Strasburgo ha dichiarato che l’Austria viola gli articoli 14 e 8 della Convenzione europea dei diritti umani quando impedisce ad una dei due componenti della coppia dello stesso sesso di adottare il figlio naturale del proprio partner. Cosa che per una coppia sposata eterosessuale è invece possibile. Questo impedimento è discriminatorio per motivi connessi all’orientamento sessuale dei componenti della coppia.

La Corte ha inoltre ribadito chiaramente un concetto già espresso nel passato, ovvero che le famiglie composte da persone dello stesso sesso ed i figli che in essa crescono rientrano nella nozione di tutela della vita familiare di cui all’art. 8 della Convenzione.

la Corte costituzionale tedesca dichiara discriminatorio il divieto di adozione susseguente per le unioni solidali registrate (l’istituto tedesco approvato nel 2001 solo per le coppie di persone dello stesso sesso). La legge istitutiva delle unioni solidali registrate infatti vietava al partner la possibilità di adottare un figlio o una figlia precedentemente adottati dalla persona con cui ha sottoscritto una unione registrata, cosa che invece le coppie coniugate eterosessuali possono fare. Questa sentenza è l’ultima di una serie della Corte tedesca che interviene per garantire pari dignità e pari diritti tra le coppie eterosessuali che possono accedere al matrimonio ed alle coppie omosessuali che possono accedere solo all’unione solidale registrata.

Il nostro Parlamento, ed in particolare quelle forze che parlano di soli diritti individuali per le coppie omosessuali, dovranno prima o poi comprendere come si sta evolvendo il diritto in Europa, ed anche  in Italia. Nell’attesa l’Associazione radicale certi diritti continuerà la sua strategia di attivare cause pilota, ovunque sia possibile e necessario, con le coppie che vorranno fare questa scelta, perché la stessa si dimostra, purtroppo per noi, la strada principale per ottenere quel riconoscimento che ci è negato. Strategia che ha ottenuto significative vittorie anche nel nostro Paese.

[http://www.certidiritti.it/](http://www.certidiritti.it/)  
[http://www.iomimpegno.org/](http://www.iomimpegno.org/)