---
title: '2008-0140-Discrimination-ST16005.EN10'
date: Thu, 18 Nov 2010 13:34:12 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 18 November 2010

Interinstitutional File:

2008/0140 (CNS)

16005/10

LIMITE

SOC 741

JAI 928

MI 434

  

  

  

  

  

OUTCOME OF PROCEEDINGS

from :

The Working Party on Social Questions

on :

4 November 2010

No. prev. doc. :

13505/10 SOC 523 JAI 728 MI 299

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

**I          INTRODUCTION**

At its meetings on 19 October and 4 November 2010, the Working Party on Social Questions continued its examination of the above proposal. The discussion focused on the provisions concerning _housing_, based on a questionnaire[\[1\]](#_ftn1) and a set of drafting suggestions[\[2\]](#_ftn2) prepared by the Presidency.

Delegations continued to welcome the Presidency's efforts to advance the discussion through in-depth discussion on specific themes.

**II         MAIN ITEMS DISCUSSED**

DE reaffirmed its general reservation and expressed various concerns, including in respect of the legal basis and subsidiarity, the need for a thorough impact assessment and cost-benefit analysis, the burden that the proposed measures would impose on businesses (especially SMEs) and the lack of legal certainty. CZ and IT also reiterated their doubts regarding the need for the proposal.

**The Presidency's drafting suggestions (doc. 15174/10, Annex I)**

The Presidency explained that its intention in tabling drafting suggestions was to advance the discussion, and not to conclude an agreement at this stage.

Many delegations (including UK, EE, FI, IT, SE, EL, LU, AT, BG, NL, ES, PL and Cion) welcomed the Presidency's suggestions as a useful basis for further discussion.

**Article 3(1)(d) and Recitals 17 and 17d**

In the light of the fact that few, if any, Member States had defined "commercial or professional activity" in the relevant legislation, the Presidency deleted this term from the text.

Most delegations welcomed the reference to private and family life as the basis of the exception. Several delegations (EE, CZ, IT, EL, HU, NL, ES, MT, BG) felt that the concepts "available to the public" and "outside the context of private and family life" required further elucidation, for example, in the recitals. ES, AT and BG, however, also pointed out that a comprehensive list of examples was not feasible and that the details could be left to the national level. FI raised the possibility of delineating the personal scope in Article 4a.

Responding to questions, Cion confirmed that the current drafting was cumulative, only goods and services "which are available to the public _and_ which are offered outside the context of private and family life" being covered; Cion underlined that it was up to the Council to decide on this issue.

  

UK and MT saw a need to clarify that the Directive was mainly focused on the _provision_ of goods and services, rather than the goods and services themselves or physical access thereto, also recalling that the scope covered more than just housing (e.g. transport and infrastructure).

FR explained that the principle of anti-discrimination already entailed the protection of private life, and that defining the protection of private life as _an exception_ to that principle was incompatible with French law. RO shared this concern and sought assurances that the Directive would not necessitate a change to its anti-discrimination law. Cion acknowledged that fundamental human rights were at stake; while the issue could indeed be addressed in a recital, the Presidency's text provided useful clarity.

**Articles 3(1)(a) and 4(6-7)**

In the light of the fact that the Member States do not distinguish between private and social/public housing for the purposes of anti-discrimination legislation, the Presidency had deleted the words "private and social" from Article 4(6-7), while leaving the term "social housing" in Article 3(1)(a), as it formed part of social protection.

DE, UK and MT questioned the inclusion of "social housing" in Article 3(1)(a) on the grounds of subsidiarity. MT entered a scrutiny reservation.

**Article 4a(3) and Recital 20a**

The Presidency had sought to clarify the meaning of "reasonable accommodation" in the context of housing, which often involved a long-term relationship between landlords and tenants.

Several delegations (FI, CZ, UK, EL, EE, IE) questioned the wisdom of placing a sweeping obligation on landlords to accept _all_ structural alterations made to their properties. Delegations (FI, UK, EL, EE, IE) also asked who would be liable to fund the reversal of structural alterations once they were no longer needed. NL, RO, PL and MT agreed that clarification was required. DE called for greater legal certainty.

  

CY expressed the view that some of the cost of alterations made to buildings should be supported through the Structural Funds.

Welcoming the Presidency's text, which sought to balance the proprietors' rights with the rights of tenants, Cion explained that it would be up to the Member States to decide who should foot the bill for structural alterations and their later reversal, as well as on detailed modalities such as the consultation with the landlord, which need not be included in the Directive.

All delegations maintained general scrutiny reservations on the proposal and the Presidency's drafting suggestions.

Further comments are set out in the footnotes to the Annex.

**III       CONCLUSION**

Noting the broadly positive welcome given to the Presidency's drafting suggestions, as well as the need for further discussion, the Chair informed delegations that the Presidency would submit a Progress Report to the EPSCO Council on 6 December 2010[\[3\]](#_ftn3).

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

Proposal for a

COUNCIL DIRECTIVE

on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

…

(17)   While prohibiting discrimination, it is important to respect other fundamental rights and freedoms, including the protection of private and family life **\[…\]**, the freedom of religion, the freedom of association, the freedom of expression, the freedom of the press and the freedom of information.

…

(17d) All individuals enjoy the freedom to contract, including the freedom to choose a contractual partner for a transaction. The Directive only covers the access to and supply of goods and services, including housing, which are available to the public and which are offered outside the context of private and family life.

…

  

(20a) In addition to general measures to ensure accessibility, individual measures to provide reasonable accommodation play an important part in ensuring full equality in practice for persons with disabilities to the areas covered by this Directive. In the provision of housing, reasonable accommodation includes the duty of the provider to change \[practices, policies and procedures\][\[4\]](#_ftn4) that may represent barriers to persons with disabilities. The provider should not be required to make structural alterations to the premises or to pay for them, but should accept such alterations, if they are funded otherwise.

…

Article 3  
Scope

1.       Within the limits of the competences conferred upon the European Union, the prohibition of discrimination shall apply to all persons, as regards both the public and private sectors, including public bodies, in relation to:

(a)     social protection, including social security, social assistance, social housing and healthcare;

(b)

(c)          education;

(d)     access to and supply of goods and other services, including housing, which are available to the public and which are offered outside the context of private and family life.

...

Article 4

Accessibility for persons with disabilities

1.             Member States shall take the necessary and appropriate measures to ensure accessibility for persons with disabilities, on an equal basis with others, within the areas set out in Article 3. These measures should not impose a disproportionate burden.

1a.     Accessibility includes general anticipatory measures to ensure the effective implementation of the principle of equal treatment in all areas set out in Article 3 for persons with disabilities, on an equal basis with others\[, and with a medium or long-term commitment\].[\[5\]](#_ftn5)

2.             Such measures shall comprise the identification and elimination of obstacles and barriers to accessibility, \[as well as the prevention of new obstacles and barriers\] in the areas covered in this Directive.

3.

4.

5.

6.       Paragraphs 1 and 2 shall apply to housing only as regards the common parts[\[6\]](#_ftn6) of buildings with more than one[\[7\]](#_ftn7) housing unit. This paragraph shall be without prejudice to Article 4(7) and Article 4a.

  

7.       Member States shall progressively take the necessary measures to ensure that sufficient housing is accessible for people with disabilities.[\[8\]](#_ftn8)

_Article 4a  
Reasonable accommodation for persons with disabilities_

1.       In order to guarantee compliance with the principle of equal treatment in relation to persons with disabilities, reasonable accommodation shall be provided within the areas set out in Article 3, unless this would impose a disproportionate burden.

2.       Reasonable accommodation means necessary and appropriate modifications and adjustments where needed in a particular case, to ensure to persons with disabilities access on an equal basis with others.

3.  \[In the provision of housing, paragraphs 1 and 2 shall not require the provider to make structural alterations to the premises or to pay for them. \[Subject to\][\[9\]](#_ftn9) paragraphs 1 and 2, the provider shall accept such alterations, if they are funded otherwise[\[10\]](#_ftn10).\]

4.

_Article 4b  
Provisions concerning accessibility and reasonable accommodation_

1.       For the purposes of assessing whether measures necessary to comply with Articles 4 and 4a would impose a disproportionate burden, account shall be taken, in particular, of:

  

a)             the size and resources of the organisation or enterprise[\[11\]](#_ftn11);

b)             the estimated cost;

c)

d)            the life span of infrastructures and objects which are used to provide a service;

e)             the historical, cultural, artistic or architectural value of the movable or immovable property in question;

f)              whether the measure in question is impracticable or unsafe.

The burden shall not be deemed disproportionate when it is sufficiently remedied by measures existing within \[the framework of the disability policy of\] the Member State concerned.

2.       Articles 4 and 4a shall apply to the design and manufacture of goods[\[12\]](#_ftn12), unless this would impose a disproportionate burden. For the purpose of assessing whether a disproportionate burden is imposed in the design and manufacture of goods, consideration shall be taken of the criteria set out in article 4b(1).

3.       Articles 4 and 4a shall not apply where European Union law provides for detailed standards or specifications on the accessibility or reasonable accommodation regarding particular goods or services[\[13\]](#_ftn13).

…

Article 15

Implementation

1.       Member States shall adopt the laws, regulations and administrative provisions necessary to comply with this Directive by …. at the latest \[4 years after adoption\]. They shall forthwith inform the Commission thereof and shall communicate to the Commission the text of those provisions.

When Member States adopt these measures, they shall contain a reference to this Directive or be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.

2.             In order to take account of particular conditions, Member States may, if necessary, establish that the obligation to ensure accessibility as set out in Articles 4 and 4b has to be complied with by, at the latest, \[5 years after adoption\] regarding new buildings, facilities, vehicles[\[14\]](#_ftn14) and infrastructure[\[15\]](#_ftn15), as well as existing buildings[\[16\]](#_ftn16), facilities and infrastructure undergoing significant renovation[\[17\]](#_ftn17) and by \[20 years after adoption\] regarding all other existing buildings, facilities, vehicles and infrastructure[\[18\]](#_ftn18).

Member States wishing to use any of these additional periods shall inform the Commission at the latest by the date set down in paragraph 1 giving reasons. Member States shall also communicate to the Commission by the same date an action plan laying down the steps to be taken and the timetable for achieving the gradual implementation of Article 4 \[including its paragraph 7\]. They shall report on progress every two years starting from this date.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) 13883/10. A number of written replies have been distributed: 14452/10 (CZ), 14456/10 (SE), 14457/10 (BG), 14495/10 (SI), 14548/10 (FI), 14746/10 (DK), 14761/10 (UK), 14852/10 (FR), 14853/10 (NL), 14856/10 (AT), 15082/10 (PL), 15838/10 (IE) and 15175/10 (EL).

[\[2\]](#_ftnref2) 15174/10.

[\[3\]](#_ftnref3) 16335/10 (forthcoming).

[\[4\]](#_ftnref4) IE, MT and UK called for the words in square brackets to be clarified, IE expressing the view that the recital was too prescriptive and appeared to impose an anticipatory requirement.

[\[5\]](#_ftnref5) UK suggested deleting 4(1a).

[\[6\]](#_ftnref6) DE, UK and MT were unable to support "common parts". EE asked whether lifts were covered; Cion explained that the Member States enjoyed some discretion in defining the disproportionate burden and in that context could decide, for example, which buildings should have lifts, based on the number of floors.

[\[7\]](#_ftnref7) MT explained that Maltese legislation only covered buildings with more than twenty units.

[\[8\]](#_ftnref8) DE questioned the use of the terms "progressively" and "sufficient" and asked for the link with Article 15(2) to be clarified. UK and MT asked for the deletion of 4(7). EE also expressed concern.

[\[9\]](#_ftnref9) Cion explained that Article 4a(3) was without prejudice to Article 4(1-2).

[\[10\]](#_ftnref10) RO asked for clarification and entered a reservation, citing concern over the cost implications, especially for private owners.

[\[11\]](#_ftnref11) IT and MT called for clarification.

[\[12\]](#_ftnref12) SE, UK and MT recalled their concerns with respect to the inclusion of the design and manufacture of goods in the scope.

[\[13\]](#_ftnref13) FI recalled its written suggestion (doc. 8887/1/10 REV 1, footnote 52).

[\[14\]](#_ftnref14) DE and IT expressed doubts with respect to the inclusion of "vehicles".

[\[15\]](#_ftnref15) DE was unable to accept the inclusion of road infrastructure.

[\[16\]](#_ftnref16) DE was unable to accept the inclusion of existing buildings.

[\[17\]](#_ftnref17) FI felt that more time was needed for buildings undergoing significant renovation. MT asked for the term "significant renovation" to be clarified, particularly with respect to historic buildings.

[\[18\]](#_ftnref18) LU, UK, DE, IT and MT saw a need to clarify the link between Article 15(2) and Article 4a ("progressively"). Cion explained that the duty to provide reasonable accommodation would continue even after accessibility had been ensured, and that Article 15 did not create new obligations or a general duty to make all buildings accessible.