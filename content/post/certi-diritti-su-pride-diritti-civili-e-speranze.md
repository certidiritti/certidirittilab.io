---
title: 'CERTI DIRITTI SU PRIDE, DIRITTI CIVILI E SPERANZE'
date: Thu, 01 Jul 2010 10:38:06 +0000
draft: false
tags: [Comunicati stampa]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 1 luglio 2010 - La rivolta di Stonewall fu una dura reazione della comunità lgbt contro la polizia di New York che per l'ennesima volta aveva fatto una incursione nello storico locale Newyorkese del Greenwich Village di Manhattan, abitualmente frequentato da persone omosessuali.   

   "Stonewall" (così è di solito definito in breve l'episodio) è generalmente considerato da un punto di vista simbolico il momento di nascita del movimento di liberazione gay moderno in tutto il mondo. Per questo motivo il 28 giugno è stato scelto dal movimento LGBT come data della "giornata mondiale dell'orgoglio LGBT" o "Gay pride".  
   
     L’Associazione Radicale Certi Diritti, in occasione degli eventi che si svolgono per celebrare questa data, partecipa alle diverse iniziative che vengono promosse da diverse realtà associative, politiche e di singoli, in molte città italiane.

     Anche nella Capitale, dopo Torino, Milano, Palermo, Napoli sono previste alcune iniziative che culmineranno con il Roma Pride 2010 che sfilerà per le strade del centro sabato 3 luglio 2010.

L’Associazione Radicale Certi Diritti sostiene tutte le istanze e la iniziative che hanno per obiettivo il superamento delle diseguaglianze per la promozione e la difesa dei diritti per le persone lesbiche, gay, bisessuali, transessuali e che riguardano, anche, le persone eterosessuali.

Saremo sempre pronti a marciare a fianco di coloro che hanno a cuore il dialogo, la tolleranza e la partecipazione come metodo di iniziativa politica per raggiungere insieme gli obiettivi di civilità e di laicità nel nostro paese, per sconfiggere il fondamentalismo ideoogico e religioso così come la storia e la cultura politica dei radicali hanno sempre sostenuto.

     Siamo dispiaciuti delle gravi spaccature che si sono verificate in queste ultime settimane all’interno della comunità lgbt(e) di Roma e ci auguriamo che quanto prima le stesse vengano superate. Siamo a disposizione per qualsiasi iniziativa finalizzata al ricucire quei rapporti che devono vederci tutti uniti per raggiungere i traguardi per i quali ci troviamo impegnati sullo stesso fronte.  

  
         Alcuni iscritti e militanti dell’Associazione Radicale Certi Diritti parteciperanno al Roma Pride 2010 con cartelli e slogan sul matrimonio gay, sulle unioni civili, per una maggiore tutela delle persone transessuali, sulla lotta a ogni forma di violenza omo e trans fobica. Non abbiamo sottoscritto alcun documento proprio per evitare di alimentare dissensi e polemiche e per la stessa ragione non facciamo parte del Comitato organizzatore degli eventi. La spaccatura che si è determinata all’interno del movimento è anche un duro colpo alle speranze di centinaia di migliaia di persone e rischiano di alimentare tra i cittadini ulteriori diffidenze davvero non necessarie.

Uff. Stampa Certi Diritti: Tel. 06-68979250