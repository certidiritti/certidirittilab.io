---
title: 'Omofobia. Stupore per invito ad Amato a convegno Senato. Associazioni LGBTI non sono una claque'
date: Thu, 07 May 2015 08:12:00 +0000
draft: false
tags: [Movimento LGBTI]
---

[![6308653055_2fb99ddd73](http://www.certidiritti.org/wp-content/uploads/2015/05/6308653055_2fb99ddd73-300x168.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/05/6308653055_2fb99ddd73.jpg)La vice presidente del Senato Valeria Fedeli, ha organizzato per il 19 maggio 2015 in occasione della Giornata internazionale contro l'omofobia, un convegno dal titolo "Diritti omosessuali, diversità come valore", al quale parteciperanno i presidenti di Camera e Senato e anche Giuliano Amato giudice della Corte Costituzionale. Ricordiamo che Giuliano Amato, il 25 maggio del 2000, ebbe a dire, in risposta a una interpellanza di An sul World Pride, che la Parata LGBTI era "inopportuna" nell'anno del Giubileo ma che: "Purtroppo dobbiamo adattarci a una situazione nella quale vi è una Costituzione che ci impone vincoli e costituisce diritti". Non contento, da ministro dell'Interno, il 18 ottobre 2007, con la circolare n. 55 ha ordinato a tutti i prefetti e sindaci d’Italia di impedire la trascrizione dei matrimoni dello stesso sesso celebrati all’estero con queste parole: “La richiesta di trascrizione di un simile atto compiuto all’estero deve essere rifiutata perché in contrasto con l’ordine pubblico interno”. Forse la senatrice Fedeli vuole cogliere l'occasione della Giornata contro l'Omofobia e la Transfobia per chiedere conto a Giuliano Amato di questi episodi sicuramente effetti di questa patologia politica e sociale? Una scelta curiosa se si tiene conto del fatto che contemporaneamente, a nessuna delle associazioni LGBTI che da decenni combattono l'omofobia, la transfobia e il bullismo, è stata data la possibilità di prendere la parola. Siamo state invitate a far da spettatori e spettatrici ad un evento, al quale magari, si pretende si faccia da claque a una lista di oratori, (dei quali rispettiamo la funzione e il lavoro) ma che, se escludiamo il rappresentante di MAE, non pagano sulla loro pelle una piaga sociale che colpisce migliaia di cittadine e cittadini italiani Nei prossimi giorni ci riserveremo di decidere se e come far sentire la nostra voce, dentro o fuori questo convegno.  

Fiorenzo Gimelli presidente nazionale **Agedo**

Yuri Guaiana segretario nazionale **Associazione Radicale Certi Diritti**

Flavio Romani presidente nazionale **Arcigay**

Lucia Caponera vice presidente nazionale **ArciLesbica**

Andrea Maccarrone presidente **Circolo di Cultura Omosessuale Mario Mieli Roma**

Franco Grillini presidente nazionale **GayNet**

Aurelio Mancuso presidente nazionale **Equality Italia**

Giuseppina La Delfa presidente nazionale **Famiglie Arcobaleno**