---
title: 'Lunedì 11 marzo a Torino sit-in per la libertà in Russia e Uganda'
date: Wed, 06 Mar 2013 10:22:43 +0000
draft: false
tags: [Transnazionale]
---

lunedì 11 marzo 2013, ore 17,30 - 19,30  
di fronte al Consiglio comunale di Torino (Piazza del Municipio).

sit-in per la libertà in  
RUSSIA E UGANDA  
il Consiglio comunale di Torino si esprima, senza indugi e senza ambiguità, contro ogni forma di criminalizzazione delle persone omosessuali, a favore della libertà di espressione e di manifestazione in quei Paesi e ovunque.  
Globalizziamo i diritti per tutti e tutte!

organizzato da:

Associazione radicale Certi Diritti

Associazione radicale Adelaide Aglietta

Coordinamento Torino Pride lgbt

Consulta torinese per la laicità delle Istituzioni

Arcigay Ottavio Mai di Torino

Agedo Torino

Circolo Maurice lgbt