---
title: 'Ministro Giarda invia nota a Ministro degli Interni Cancellieri su richiesta fatta da esponenti Lgbt di ritirare la Circolare Amato'
date: Fri, 18 May 2012 08:51:55 +0000
draft: false
tags: [Politica]
---

La circolare Amato del 2007 vieta di trascrivere in Italia i matrimoni same-sex celebrati all'estero per motivi di "ordine pubblico".

Roma, 18 maggio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Il Ministro per i rapporti con il Parlamento, Piero Giarda, ha inviato al Ministro degli Interni una nota riguardo la richiesta fatta lo scorso 14 aprile, anniversario della sentenza della Corte Costituzionale 138/2010, di ritirare la Circolare Amato per la quale i Comuni non possono trascrivere i matrimoni delle coppie omosessuali sposate all’estero per ragioni di ‘ordine pubblico.

Lo ha reso noto lo stesso Ministro rispondendo ad una lettera firmata da Rita Bernardini, deputata radicale eletta nelle liste del PD, Anna Paola Concia, deputata PD, Yuri Guaiana, Segretario Associazione Radicale Certi Diritti, Paolo Patanè, presidente Arcigay, Paola Brandolini, presidente Arcilesbica, Luca Possenti, vice presidente Famiglie Arcobaleno e Rita De Santis, presidente AGEDO, nella quale veniva chiesto ai destinatari, Capigruppo di Camera e Senato, e tutti i membri del Governo, di attivarsi per ritirare la Circolare Amato n.55 del 18 ottobre 2007 (Protocollo n. 15100/397/0009861), con oggetto «Matrimoni contratti all’estero tra persone dello stesso sesso. Estratti plurilingue di atti dello stato civile» per la quale i Comuni, materia questa non applicabile al diritto di famiglia, come già ribadito in più occasioni dalla Corte di Cassazione.

La lettera era stata inviata lo scorso 14 aprile dall’Associazione Radicale Certi Diritti in occasione dell’anniversario della sentenza 138/2010 della Corte Costituzionale, insieme ad una  copia del libro "Dal cuore delle coppie al cuore del diritto. L'udienza alla Corte Costituzionale per il diritto al matrimonio tra persone dello stesso sesso".  Il libro raccoglie tutta la documentazione relativa a quella sentenza,  frutto della campagna di Affermazione civile che ha visto molte coppie dello stesso sesso mettersi in gioco direttamente per chiedere il riconoscimento dei propri diritti in dialogo con le istituzioni.

Ci auguriamo che il Ministro degli Interni consideri con attenzione questa nostra richiesta che, nei fatti, discrimina gravemente sul piano civile le coppie dello stesso sesso sposate all’estero in violazione del Trattato di Lisbona dell’Unione Europea sulla lotta alle discriminazioni e del Trattato di Nizza sulla libera circolazione.