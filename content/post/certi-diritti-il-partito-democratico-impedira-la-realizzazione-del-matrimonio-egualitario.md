---
title: 'Certi Diritti: il Partito Democratico impedirà la realizzazione del matrimonio egualitario'
date: Sun, 17 Feb 2013 15:35:40 +0000
draft: false
tags: [Matrimonio egualitario]
---

Comunicato stampa dell’Associazione Radicale Certi Diritti.

Roma, 17 febbraio 2013

Sabato scorso, a Roma, si è tenuta la Convention “Cambia Italia”, promossa da Agedo, Arcigay, Arcilesbica, Equality Italia e Famiglie Arcobaleno. In quel contesto Bersani si è impegnato ad approvare entro un anno una legge sulle unioni civili basata sul modello tedesco. Vendola e Ingroia, invece, hanno ribadito il loro impegno per il matrimonio egualitario. Anche il M5S si è ultimamente detto a favore del matrimonio ugualitario.

L’Associazione Radicale Certi Diritti non ha aderito all’iniziativa poiché si è deciso di non rivolgersi a tutti i candidati, ma solo ai supposti amici, e poiché la convocazione era troppo generica e non menzionava esplicitamente il matrimonio egualitario né il diritto di accesso per le persone trans al cambio di genere sui documenti anagrafici senza il bisogno d’intervento chirurgico. Rilanciamo, invece, la nostra richiesta d’impegno sottoscrivibile al sito [http://www.iomimpegno.org.](http://www.iomimpegno.org.)

Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti, dichiara: “Se SEL, M5S e Rivoluzione Civile sono a favore del matrimonio egualitario, una maggioranza in parlamento, secondo gli ultimi sondaggi resi pubblici, ci sarebbe, se il PD fosse d’accordo. Il PD, allora, con la sua ostinazione a favore SOLO delle unioni civili alla tedesca non garantirà un progresso sulla via dei diritti delle persone LGBTI, ma impedirà la piena realizzazione del diritto di eguaglianza in Italia. Il voto del PD a favore SOLO delle unioni civili alla tedesca non sarà un voto a favore dei diritti delle persone LGBTI, ma un voto contro il matrimonio egualitario”.

**[SOSTIENI CERTI DIRITTI >>>](partecipa/iscriviti)**