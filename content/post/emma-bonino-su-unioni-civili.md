---
title: 'EMMA BONINO SU UNIONI CIVILI'
date: Wed, 09 Apr 2008 13:47:35 +0000
draft: false
tags: [Comunicati stampa]
---

**Nota dell'Ufficio Stampa Lista Bonino/Radicali **

**Emma Bonino a Roma è intervenuta anche su scadenze politiche nazionali; sulle unioni civili occorre adeguarsi alla legislazione prevalente nella maggior parte dei paesi europei.**

"Il ministro Bonino, nel corso di una manifestazione elettorale a Roma, svoltasi domenica 6 aprile, cui era presente anche Giovanna Melandri, non si è limitata a sostenere esclusivamente i candidati al Comune ed alla Provincia di Roma, ma è intervenuta anche sulle scadenze della politica nazionale. In questo ambito ha fatto riferimento ai temi che più stanno a cuore ai radicali che si ritrovano nel Programma nazionale del PD: difesa della 194, riconoscimento dei diritti delle persone conviventi, testamento biologico.

Sulle unioni civili, sulle quali è ben nota la posizione dei radicali, ha ribadito che occorre intervenire per legge, perché anche il nostro paese possa adeguarsi alla legislazione prevalente nella maggior parte dei paesi europei. Nel dir ciò, non ha fatto altro che ribadire una posizione chiara e univoca di candidata al Parlamento, senza che questo volesse minimamente entrare nel merito dei programmi riguardanti il Comune di Roma e le intenzioni dei singoli candidati a Sindaco. Come tutti sanno i radicali si occupano da sempre di diritti civili, continueranno a farlo così come lo hanno fatto a Roma in questi ultimi mesi e come fanno sin da quando altri partiti, comunisti e non, espellevano propri esponenti a causa del loro orientamento sessuale”.