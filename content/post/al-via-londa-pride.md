---
title: 'Al via l''Onda Pride'
date: Sat, 11 Jun 2016 13:43:40 +0000
draft: false
tags: [Movimento LGBTI]
---

[![Ondapride2016](http://www.certidiritti.org/wp-content/uploads/2016/06/Ondapride2016-300x300.png)](http://www.certidiritti.org/wp-content/uploads/2016/06/Ondapride2016.png)Cominciano oggi in alcune città italiane le celebrazioni del mese dell'orgoglio omosessuale, si parte con la capitale e Pavia dove i nostri iscritti e simpatizzanti saranno presenti con le nostre bandiere per sfilare in corteo e chiedere uguali diritti.

"Il DDL Cirinnà è un primo passo, sicuramente ci fa uscire dalla condizione medievale in cui siamo rimasti per troppo tempo mentre le democrazie occidentali mettevano la freccia e superavano il nostro paese sulla strada della civiltà. Con questa legge il nostro paese rientra in carreggiata ma è solo un piccolo passo, noi ci fermeremo solo quando otterremo pari diritti e pari dignità che possono essere raggiunti solo con l'uguaglianza di tutti i cittadini, come tra l'altro recita la nostra Costituzione, e che ad oggi non vengono rispettati e garantiti", dichiara Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti.

Invitiamo tutti a cercarci nei cortei di:

Roma e Pavia di oggi;

Treviso, Firenze, Genova, Palermo e Varese il prossimo sabato;

Milano, Caserta, Perugia, Bologna, Cagliari e Latina il 25 giugno;

Catania e Taranto il 2 luglio;

Torino il 9 luglio;

Siracusa il 16 luglio;

Tropea e Rimini il 30 luglio;

Gallipoli il 19 agosto.

Ci vediamo in piazza per chiedere l'uguaglianza.