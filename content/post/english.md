---
title: 'English'
date: Tue, 23 Jun 2009 15:33:13 +0000
draft: false
tags: [Senza categoria]
---

**“CERTI DIRITTI”**

**RADICAL ASSOCIATION**

**Our priorities:**

... a strong commitment to disseminating and supporting the civil affirmation campaign for the recognition of the right to same-sex marriage, by increasing the number of couples who decide to support the initiative with their public choice and by promoting it in the wider Italian GLBT community.

... a strong commitment to achieving a common platform for all the GLBT movement united, in a secular way, to pursue common goals, beyond particular, legitimate but partial positions and ideals.

The association dedicates its activities to the memory of Makwan Moloudzadeh (the 21-year-old boy hanged in Iran on December 5 2007, because accused of having committed homosexual acts), and to all those who suffered abuses and violence or were discriminated against because of their sexual orientation.

**CIVIL AFFIRMATION: a concrete action for a right that nobody can deny to homosexual couples : marrying.**

In Italy, same-sex marriage is not banned, but homosexuals cannot marry.

No law rules gay marriage out, but registry offices forbid gay people to put up their banns. A right denied because of a thousand-year old prejudice, which has no justification in the Italian legal system, thanks to which, instead, gay marriages could legitimately be celebrated.

This is the goal of Civil Affirmation: breaking the bias and giving a chance to love between homosexuals who wish to marry.

An important initiative entirely within the frame of justice and the law with which Certi Diritti and the Network of lawyers, Lenford Network, follow, step by step, those same-sex couples who want to crown their project of love through the decision of the court.

This initiative draws its inspiration from the civil disobedience campaign of the non-violent struggle that in the past, like for instance in the United States against segregationist laws, brought, in democratic countries, to a substantial social progress in the field of civil rights and institutional reforms, through the abandoning of unjust laws that limit the opportunities and happiness of people.

**CIVIL LOVE : a proposal for the Reform of Family Law.**

Certi Diritti participated in the work of the Standing Conference for the Reform of family law, coordinating the development of the definition of different forms of family and partnership. It is a project addressed to the political Italian class as well, born within the "Civil Love" initiative, which wants to introduce some very important rights that nowadays are denied. Among these particularly relevant are those regarding the forms of family, adoption and children.