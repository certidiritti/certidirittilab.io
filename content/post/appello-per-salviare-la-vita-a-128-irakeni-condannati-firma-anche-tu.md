---
title: 'APPELLO PER SALVIARE LA VITA A 128 IRAKENI CONDANNATI. FIRMA ANCHE TU'
date: Thu, 02 Apr 2009 10:11:00 +0000
draft: false
tags: [Comunicati stampa]
---

Amnesty International e L'Associazione londinese Iraqi Lgbtq, hanno lanciato un appello per salvare 128 irakeni condannati a morte in Irak, alcuni di loro anche per il loro orientamento sessuale.

Puoi firmare la petizione al seguente link

[http://www.thepetitionsite.com/1/protect-iraqi-lgbt](http://www.thepetitionsite.com/1/protect-iraqi-lgbt)[](http://www.thepetitionsite.com)