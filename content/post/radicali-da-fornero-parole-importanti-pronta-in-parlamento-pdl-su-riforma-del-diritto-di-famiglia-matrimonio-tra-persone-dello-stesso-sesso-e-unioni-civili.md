---
title: 'Radicali: da Fornero parole importanti. Pronta in Parlamento pdl su riforma del diritto di famiglia, matrimonio tra persone dello stesso sesso e unioni civili'
date: Tue, 15 May 2012 13:23:49 +0000
draft: false
tags: [Diritto di Famiglia]
---

Comunicato Stampa di Radicali Italiani e dell’Associazione Radicale Certi Diritti

Roma, 15 maggio 2012

Di fronte alla situazione di blocco della politica contro i diritti delle famiglie non matrimoniali, con 2,5 milioni di italiani che vivono in famiglie di fatto e oltre 100mila omosessuali con figli, accogliamo con soddisfazione le parole del Ministro Fornero.

In Parlamento sono pronte dieci proposte e disegni di legge sul matrimonio tra persone dello stesso sesso, unioni civili, adozione e su una riforma complessiva del diritto di famiglia, che in Italia è ferma al 1975.

Insieme ad esperti e politici di altri schieramenti abbiamo elaborato una proposta di riforma, depositata alla Camera nel luglio 2010, che riguarda matrimonio tra persone dello stesso sesso, unioni civili, divorzio breve, scioglimento del legame tra separazione e divorzio, mediazione familiare, riconoscimento del diritto di adozione per persone single e istituzione della figura del “genitore elettivo”.

Sin dal 2005 ai Congressi e alle riunioni del Comitato nazionale di Radicali Italiani  sono stati votati a stragrande maggioranza documenti a favore delle unioni civili e per sostenere  le iniziative legali intraprese con la campagna di Affermazione Civile dall’associazione radicale Certi Diritti per il riconoscimento del matrimonio tra persone dello stesso sesso; grazie alle quali nel 2010 si è giunti alla storica sentenza 138/10 della Corte costituzionale. Sentenza che riconosce la rilevanza costituzionale delle unioni omosessuali e sollecita il Parlamento a varare una disciplina di carattere generale in materia. Ma come ha affermato Emma Bonino «i Radicali si impegnano da tempo per il riconoscimento delle convivenze e dei matrimoni tra omosessuali. Il vero problema è che spesso la politica è più indietro delle società e spesso lo è per ragioni di poteri e di influenze».