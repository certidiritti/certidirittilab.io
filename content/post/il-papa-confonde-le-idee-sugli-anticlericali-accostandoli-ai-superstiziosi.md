---
title: 'IL PAPA CONFONDE LE IDEE SUGLI ANTICLERICALI ACCOSTANDOLI AI SUPERSTIZIOSI'
date: Sun, 19 Oct 2008 10:58:59 +0000
draft: false
tags: [Comunicati stampa]
---

**IL PAPA VUOLE CONFONDERE LE IDEE: METTE SULLO STESSO PIANO SUPERSTIZIONE, PRATICHE SPIRITICHE E ANTICLERICALISMO. OCCORREREBBE MAGGIOR RISPETTO PER GLI ANTICLERICALI CHE HANNO COSTRUITO L’ITALIA.**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

“E’ davvero sorprendente quanto oggi ci dice il Papa riguardo la superstizione e le pratiche spiritiche, fenomeni che vengono accostati, in chiave negativa, all’anticlericalismo.

Sarebbe qui inutile ricordare che la chiesa cattolica è campata (e continua a campare) di superstizioni e credenze che nel corso dei secoli hanno anche alimentato violenze, ignoranza e ingiustizie sociali. Ciò che è ancora più grave è il tipo di accostamento che viene fatto all’anticlericalismo che, secondo quanto dice il Devoto-Oli, significa “opposizione all’ingerenza del potere ecclesiastico nella vita politica, sociale e culturale di un paese”.  Peraltro questa è una categoria alla quale ci vantiamo di appartenere e che ha visto da sempre tra le sue fila personalità come Giuseppe Garibaldi, Giuseppe Mazzini, Mario Pannunzio, Ernesto Rossi, Piero Calamandrei, Gaetano Salvemini,  Altiero Spinelli, Indro Montanelli, Marco Pannella, solo per citarne alcuni. Insomma, non proprio personalità dedite allo spiritismo e alla superstizione.

Tentare di confondere le idee con questi accostamenti ci pare più un gioco come quello dei treccartari piuttosto che quello di un fine teologo”.