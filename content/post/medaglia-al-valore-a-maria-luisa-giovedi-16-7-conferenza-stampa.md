---
title: 'MEDAGLIA AL VALORE A MARIA LUISA: GIOVEDI'' 16-7 CONFERENZA STAMPA'
date: Tue, 14 Jul 2009 12:43:19 +0000
draft: false
tags: [Comunicati stampa]
---

**Giovedì 16 luglio** alle **ore 13.00** nella **Sala Mappamondo di Montecitorio** – ingresso via della Missione, 4 – conferenza stampa per presentare **l’appello al Presidente della Repubblica** **Giorgio Napolitano**, **affinchè conferisca una medaglia al valore a Maria Luisa Mazzarella**, la studentessa napoletana oggetto di un duro atto di violenza verbale e fisica, per aver difeso un proprio amico omosessuale dall’aggressione di un gruppi di bulli.

L’iniziativa "Una medaglia per Maria Luisa" è stata promossa e coordinata da:

Alessio De Giorgi - presidente Gay.it

Sergio Rovasio - segretario di Certi Diritti

Salvatore Simioli - presidente Arcigay di Napoli

Carlo Cremona - presidente I-Ken onlus

I promotori dell’iniziativa, che saranno presenti alla conferenza stampa insieme all’On. Anna Paola Concia, firmataria dell’appello, hanno anche chiesto al Presidente Napolitano un incontro per poter illustrare l'appello e la grande mole di firme, oltre 12mila, raccolte dai promotori in pochi giorni tra la gente comune ma anche tra deputati, senatori e note personalità dello spettacolo, a testimonianza che è davvero alta la preoccupazione per il clima di intolleranza omofobica che sta crescendo nel nostro paese.