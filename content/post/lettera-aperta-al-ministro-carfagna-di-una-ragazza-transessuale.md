---
title: 'LETTERA APERTA AL MINISTRO CARFAGNA DI UNA RAGAZZA TRANSESSUALE'
date: Sun, 18 Jan 2009 08:02:55 +0000
draft: false
tags: [Carfagna, Comunicati stampa, Prostituzione, TRANSESSUALE, VIOLENZA]
---

Cari amici di Certi Diritti, vorrei portare alla vostra attenzione questa lettera aperta scritta da una persona che conosco:

Onorevole Ministro Carfagna,  
mi chiamo anagraficamente *** *** (Marina per gli amici) sono una ragazza transessuale, sono Psicologa e quasi Psicoterapeuta.  
Le scrivo a titolo personale, ma allo stesso tempo a nome di tutte le ragazze transessuali o transgender che non si sentono assolutamente tutelate in questo Paese.  
Mentre studiavo all'Università, in seguito ad una difficile situazione economica della mia famiglia, ho iniziato a prostituirmi e dopo un anno e mezzo di "gavetta" in strada, sono passata a ricevere in appartamento.  
Ho lasciato la strada perchè nel giro di pochi mesi sono stata nel seguente ordine:

aggredita fisicamente e rapinata da due ragazzi;  
aggredita fisicamente (mentre ero in macchina, per fortuna) da un gruppo di quattro ragazzi italiani , che, sotto effetti di droghe, mi hanno distrutto a calci il finestrino della macchina, e mi sono ritrovata sanguinante e ricoperta di vetri;  
aggredita verbalmente da una "collega" che, forse, per invidie lavorative, mi ha danneggiato la macchina con l'ausilio di un masso.

Questi episodi furono all'epoca denunciati, ma a distanza di quasi cinque anni, NON HO ANCORA AVUTO GIUSTIZIA.

Passata, a lavorare, in casa, la situazione è migliorata, ma non del tutto; ogni tanto si incontrano, sfortunatamente, persone aggressive che cercano di farti del male o di derubarti.  
Solo i casi più gravi sono stati da me denunciati:

il furto da me subito, di una ingente somma di denaro, da parte di un ragazzo, al quale avevo offerto la mia ospitalità data la sua situazione di disagio economico;  
e l'aggressione ed il danneggiamento da parte di un altro uomo, pochi giorni fa.

Vorrei precisare che le denuncie da me effettuate, non sono mai state denuncie verso ignoti, ma sono state sempre corredate, quando è stato possibile dei dato anagrafici dei miei aggressori, e in alternativa dei numeri di targa delle vetture sulle quali viaggiavano!!!

AD OGGI NON HO MAI AVUTO GIUSTIZIA!!!  
IO, COME MOLTE RAGAZZE TRANSESSUALI, CHE SI PROSTITUISCONO, MI TROVO AD ESSERE DOPPIAMENTE VITTIMA!!!  
SIAMO VITTIME DI CHI FISICAMENTE CI AGGREDISCE, MA ANCHE VITTIME DI UN SISTEMA GIUDIZIARIO CHE NON CI AIUTA!!!

Scrivo questa lettera aperta, perchè alla luce dell'escalation di violenza che noi, ragazze transessuali, stiamo subendo in quest'ultimo periodo, credo che sia necessario e giusto un interessamento reale del Ministero delle Pari Opportunità nei confronti di una categoria di persone, che allo stato attuale in Italia è molto, troppo discriminata!!!

Dopo l'omicidio di Milano e quello di Roma, dopo l'accoltellamento di qualche giorno fa, sempre a Roma, e l'assassinio di cui apprendo notizia oggi a CastelFranco Veneto,  
io mi sento di dire NON VOGLIO ESSERE LA PROSSIMA!!!

NON VOGLIO ESSERE LA PROSSIMA VITTIMA DI QUESTA VIOLENZA TRANSFOBICA CHE STA INVESTENDO L' ITALIA!!!

Ministro, le chiedo da donna a donna, di aiutarci affinchè queste violenze possano terminare!!!

La ringrazio per la cortese attenzione

Marina B.