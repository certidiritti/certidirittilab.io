---
title: 'Ragazza muore per pratica bondage-shibari: occorre informazione, basta ipocrisia'
date: Sun, 11 Sep 2011 15:12:43 +0000
draft: false
tags: [Salute sessuale]
---

L'informazione sessuale favorirebbe consapevolezza e responsabilizzazione. Basta con l'ipocrisia italiota dei pontificatori mulinaribianchi di turno.

Roma, 11 settembre 2011

Dichiarazione di Sergio Rovasio, Segretario dell’Associazione Radicale Certi Diritti

La tragedia della morte di una ragazza di 20 anni è sempre questione drammatica, sia se questo avviene in un grave incidente stradale, sia se avviene per un grave incidente durante un rapporto erotico sado-maso di tipo bondage-shibari, come avvenuto a Roma l’altro ieri. Sarà la magistratura a verificare se e quali sono le responsabilità di coloro che svolgevano tale pratica durante l’incidente.

Ciò che è incredibile è dover assistere al coro dei moralisti-pontificatori, clerical-fondamentalisti-mulinaribianchi di turno su quanto  vanno dicendo nei confronti di coloro che svolgono tali pratiche erotiche con consapevolezza e responsabilità.  La migliore definizione finora sentita è quella di ‘pervertiti’ oppure di persone che soffrono di ‘gravi solitudini’ o di comportamenti individuali ‘strani’ che non si è saputo cogliere precedentemente (per fare che?). E’ la solita valanga di ipocrisia che si riversa sull’opinione pubblica alimentando così orrore, paura e spavento per una pratica erotico-sessuale diffusissima, ad esempio, nei paesi del nord-europa e ancora considerata ‘peccaminosa’ in Italia.  
  
In questi giudizi si considera ‘fuori dalla norma’ il rapporto sado-maso solo perché non è conosciuto e quindi occorre demonizzarlo e criticarlo. Nessuno dice di quanto sia importante avviare anche in Italia campagne di informazione sessuale, così come avviene all’estero, nelle scuole e nelle università, dove si spieghi quali sono le diverse pratiche erotiche, anche quelle considerate rischiose e svolte con attenzione e consapevolezza.  Per i moralisti d'accatto clerical-fondamentlisti è meglio  l’ipocrisia del fare tutto di nascosto, senza iniziative finalizzate alla responsabilizzazione delle persone.