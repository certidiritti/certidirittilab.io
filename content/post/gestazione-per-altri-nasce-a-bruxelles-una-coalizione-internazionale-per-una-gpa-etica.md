---
title: 'GESTAZIONE PER ALTRI: NASCE A BRUXELLES UNA COALIZIONE INTERNAZIONALE PER UNA GPA ETICA.'
date: Tue, 27 Sep 2016 13:32:45 +0000
draft: false
tags: [Gestazione per altri]
---

**[![utero-in-affitto](http://www.certidiritti.org/wp-content/uploads/2016/09/utero-in-affitto-300x193.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/09/utero-in-affitto.jpg)Comunicato Stampa di Associazione Luca Coscioni, Associazione Radicale Certi Diritti, Famiglie Arcobaleno, UAAR. **

Gli scorsi 24 e 25 settembre, 9 associazioni - tra cui 4 italiane ([Associazione Luca Coscioni](https://www.google.it/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwjX9pfgpa_PAhWFNxQKHYf2DbQQFggeMAA&url=http%3A%2F%2Fwww.associazionelucacoscioni.it%2F&usg=AFQjCNEeilhWf52lD7J8Olvx6PwyocwL-g&sig2=1lcbPzrr8zR78I5hHbNcTA), [Associazione Radicale Certi Diritti](http://www.certidiritti.org/), [Famiglie Arcobaleno](https://www.google.it/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwjk5ezvpa_PAhVNlxQKHW64BwEQFggeMAA&url=http%3A%2F%2Fwww.famigliearcobaleno.org%2F&usg=AFQjCNFDSH-vcpL-t8kASH4zcsH9inPczA&sig2=TFOuBoBolJ2NyARc8WdYHQ), [UAAR](https://www.uaar.it/)) \- si sono ritrovate a Bruxelles in occasione della Seconda conferenza annuale sulle opzioni di genitorialità per le persone omosessuali europee organizzata dall’Associazione americana Men Having Babies e hanno creato una coalizione internazionale per una GPA etica. 

Le associazioni internazionali presenti erano [Men Having Babies](http://www.menhavingbabies.org/advocacy/ethical-surrogacy/#5), [Our Family Coalition](http://www.ourfamily.org/), [Meer dan Gewest](http://www.meerdangewenst.nl/), [Israeli Gay Fathers’ Association](http://gaydads.org.il/en/association-platform) e l'[ASBL Homoparentalités](https://docs.google.com/viewerng/viewer?url=http://homoparentalite.be/onewebmedia/Positions%2520GPA.pdf). 

Durante l’incontro è stato presentato un [documento](http://www.certidiritti.org/wp-content/uploads/2016/09/323161648-MHB-Ethical-Surrogacy-Italian-Statement-of-Principles.pdf) di sintesi dei parametri etici per il ricorso alla

gestazione per altri da parte di aspiranti genitori condiviso da tutte le associazioni presenti (l’Associazione Luca Coscioni non sottoscrive i punti 8, 9 e 10, mentre UAAR non sottoscrive il punto 10). 

Il documento è stato elaborato anche con il contributo di donne portatrici che fanno parte dell’Advisory Committee di  [Men Having Babies](http://www.menhavingbabies.org/advocacy/ethical-surrogacy/#5) e ricalca la [posizione](http://www.certidiritti.org/2011/11/23/mozione-particolare-sulla-gestazione-per-altri-approvata-dal-viii-congresso/) comune già adottata nel 2014 da Famiglie Arcobaleno e dall’Associazione Radicale Certi Diritti.

«Anche alla luce del rapporto approvato il 21 settembre dalla Commissione salute dell’Assemblea parlamentare del Consiglio d’Europa, è importante portare l’azione per la legalizzazione della GPA anche a livello transnazionale. Leggi che seguano stringenti principi etici e che eliminino il turismo procreativo sono assolutamente urgenti e necessarie per far sì che i diritti umani e l’autodeterminazione di tutte le parti coinvolte sia rispettata», dice **Yuri Guaiana**, segretario dell’Associazione Radicale Certi Diritti, che ha rappresentato le associazioni italiane a Bruxelles.

"E' dal 2005 che la nostra associazione - spiega **Marilena Grassadonia**, presidente di Famiglie Arcobaleno - si auspica una regolamentazione della Gestazione per Altri che sia rispettosa di tutte le persone coinvolte, come d'altronde è scritto nella [nostra carta etica](http://www.famigliearcobaleno.org/userfiles/file/Posizioni%20FA%20su%20temi%20eticamente%20sensibili.pdf). Da sempre ci battiamo per far conoscere le storie delle nostre famiglie, per raccontare come la Gpa possa essere un percorso d'amore. La coalizione internazionale per una Gpa etica è la risposta migliore a chi condanna senza conoscere. L'autodeterminazione della donna rimane, per noi, il faro a cui guardare per non perdersi nelle nebbie di un dibattito a volte strumentale. Ribadiamo che la mancanza di regole e il proibizionismo possono solo alimentare fenomeni di sfruttamento".

Il prossimo appuntamento sarà il 1 ottobre a Napoli dove, in occasione del congresso dell’Associazione Luca Coscioni, Famiglie Arcobaleno e Associazione Radicale Certi Diritti s’incontreranno per declinare i principi etici presentati a Bruxelles in una proposta di legge per l’Italia.

_Roma, 27 settembre 2016_