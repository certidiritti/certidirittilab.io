---
title: '25 febbraio Sergio Rovasio ospite di ''Oltre le Differenze'''
date: Wed, 23 Feb 2011 20:34:22 +0000
draft: false
tags: [Comunicati stampa]
---

Il format radiofonico di Antenna Radio Esse dedicato al mondo Lgbt ospiterà il segretario di Certi Diritti e Chiara Lalli per parlare di adozioni gay.

I gay possono o non possono adottare? E’ questa la domanda alla quale si cercherà di rispondere ad Oltre le differenze, il format radiofonico condotto da Natascia Maesi e Eleonora Sassetti e dedicato al mondo arcobaleno, nella puntata in onda, venerdì 25 febbraio alle 21 circa, sulle frequenze di Antenna Radio Esse (FM 91.25, 93.20, 93.50, 99.10) e in diretta online su www.antennaradioesse.it.     .

Si partirà dalla notizia della notizia della sentenza della Cassazione che convalida, seppur in forma non pienamente legittimante, l'adozione di una bimba russa da parte di una donna «sola» di Genova. Una sentenza ritenuta da molti un precedente che aprirebbe alle adozioni alle personee omosessuali. Gli ospiti che interverranno sull’argomento sono Sergio Rovasio, segretario dell'Associazione Certi Diritti e membro della segreteria nazionale dei Radicali e la docente, scrittrice e bioeticista Chiara Lalli, autrice del libro “ Buoni genitori. Storie di mamme e di papà gay”, pubblicato da Il Saggiatore nella collana Infrarossi.