---
title: 'A Milano fondo anticrisi anche alle coppie di fatto. Certi Diritti plaude alla decisione della giunta Pisapia'
date: Sat, 28 Jan 2012 19:12:44 +0000
draft: false
tags: [Diritto di Famiglia]
---

Dichiarazione di Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, sulla decisione della Giunta Pisapia di aprire il fondo anticrisi anche alle coppie di fatto.

Milano, 28 gennaio 2011

L'Associazione Radicale Certi Diritti plaude alla decisione della Giunta Pisapia di varare un provvedimento per aiutare le famiglie milanesi colpite dalla crisi "sposate o coabitanti nello stato di famiglia per sussistenza di vincolo affettivo al primo gennaio 2012", includendo così anche quelle conviventi di qualunque orientamento sessuale. Il provvedimento va esattamente nella direzione da noi sostenuta, nonché indicata dalla Rete AHEAD (Against Homophobia. Eurpean Local Administration Devices) alla quale si spera che il Comune di Milano decida di aderire, di rimozione di ogni forma di discriminazione e di promozione di pari opportunità per tutte le famiglie secondo la definizione delle stesse di cui all’articolo 4 del D.P.R. 223/1989, Nuovo Regolamento anagrafico della popolazione residente.

L'Associazione Radicale Certi Diritti e il Gruppo Radicale Federalista Europeo opereranno affinché questo primo passo nella giusta direzione venga seguito da tutti gli altri necessari a garantire condizioni di piena parità rispetto all'orientamento sessuale e all'identità di genere, attraverso provvedimenti varati dalla Giunta e dal Consiglio Comunale, ognuno nelle rispettive attribuzioni e competenze.  
  

**iscriviti alla newsletter >[  
](newsletter/newsletter)[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**