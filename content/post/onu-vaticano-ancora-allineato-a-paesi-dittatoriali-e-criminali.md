---
title: 'Onu: Vaticano ancora allineato a paesi dittatoriali e criminali'
date: Tue, 12 Jul 2011 20:08:00 +0000
draft: false
tags: [Politica]
---

**Il Vaticano, che un giorno sì e l’altro pure,  esprime giudizi di odio, fanatismo e pregiudizio  contro le persone omosessuali, e questo sarebbe veramente un caso paranoide da studiare, all’Onu si allea su molti temi con i peggiori paesi dittatoriali, criminali e corrotti; paesi responsabili delle peggiori violazioni  dei diritti umani, dalla tortura alla pena di morte.**

Rome, 11 luglio 2011

  
Comunicato Stampa dell’Associazione Radicale Certi Diritti

  
La posizione del Vaticano in tema di omofobia rasenta sempre di più una dimensione comica. Il Capo delegazione dello Stato teocratico Vaticano ha sostenuto che la Risoluzione votata recentemente, e per la prima volta, alla Commissione Diritti Umani dell’Onu,  di riconoscere l’orientmento sessuale e l’identita’ di genere tra i fondamentali Diritti Umani, puo’ essere il primo passo verso l’inclusione dei diritti Lgbt e che questo “e’ l’inizio di un percorso internazionale che mira alla limitazione della liberta’ di espressione dei leader religiosi sul tema. Con questa risoluzione i paesi potrebbero trovare giustificazione per equiparare i matrimoni gay e le unioni civili alle unioni tra uomo e donna". E questo sarebbe solo "il primo passo per arrivare a legalizzare anche le adozioni ed introdurre un'educazione sessuale in contrasto con i valori cristiani". "I termini 'orientamento sessuale' e 'identità di genere' - inoltre - non sono definiti dalla legislazione internazionale proprio perché non sono comportamenti esteriori ma sentimenti e pensieri e quindi non possono essere soggetti a leggi puntive".

  
Questo esponente della comicita’ vaticana dimentica di dire che:

1)      Il Vaticano rappresenta una religione, tra le tante di tutto il mondo, e francamente non si capisce perche’ deve ficcare il naso nelle questioni che non gli competono riguardo cosa, per un organismo internazionale o un’entita’ statuale, e’ un Diritto e cosa non lo e’ ;

2)      Quasi tutti i paesi democratici, oltre che organismi di rappresentanza internazionale, dal Parlamento Europeo al Consiglio d’Europa, tanto per fare due esempi, votano in continuazione documenti riguardo i Diritti delle comunita’ Lgbt(e) ovunque nel mondo;

3)      Il Vaticano, che un giorno si e l’altro pure,  esprime giudizi di odio, fanatismo e pregiudizio  contro le persone omosessuali, e questo sarebbe veramente un caso paranoide da studiare, all’Onu si allea su molti temi con i peggiori paesi dittatoriali, criminali e corrotti; paesi responsabili delle peggiori violazioni  dei diritti umani, dalla tortura alla pena di morte, responsabili di gravi violenze repressive contro i propri cittadini;

4)      l’orientamento sessuale o l’identita’ di genere sono una condizione della persona, non un sentimento o un pensiero, ed e’ giusto che un essere umano viva al meglio tale condizione, senza influenze causate dal pregiudizio, dall’odio e dalla violenza, che invece con queste tesi il Vaticano promuove e alimenta.

Tali emeriti esponenti Vaticani farebbero bene a rispondere delle loro gravi azioni politiche anziche’ fare dichiarazioni assurde, patetiche e ridicole. Arrivera’ il giorno in cui si chiedera’ scusa anche di questo comportamento, e sara’, anche in quel caso, troppo tardi. Avranno certamente sulla coscienza tutte quelle persone che oggi muoiono e soffrono a causa di questo pregiudizio ispirato dalle peggiori visioni del fondamentalismo religioso, al pari di tutti quei fanatici che  negano dignita’ e diritti agli esseri umani.