---
title: '8X1000 for dummies'
date: Sun, 15 May 2011 22:06:29 +0000
draft: false
tags: [Politica]
---

**L'avete già fatta la dichiarazione dei redditi? Andrea Bordoni dal suo blog vogliosposaretizianoferro.it ci spiega il meccanismo dell'8xmille.**  
  
Eccomi a parlare dell’8×1000.  
Ho visto uno sbadiglio. No, non fate così, ho qualche chicca davvero interessante per voi.  
L’avete già fatta la dichiarazione dei redditi? Mica tutti! Confidando nel fatto che siete come me e vivete in ostinata contraddizione al detto “Non rimandare a domani quello che potresti fare oggi”, siamo ancora in tempo. E non molti sanno che, ogni anno, possono firmare per l’8×1000 tutti i contribuenti, non solo quelli che fanno la dichiarazione dei redditi: basta il CUD…  
  
Ti chiedo:  
1) Sai come funziona davvero l’8×1000?  
3) Sai che c’è un’alta probabilità (più del 60%!) che tu abbia in tutti questi anni involontariamente destinato l’8×1000 alla Chiesa Cattolica (e quasi mai per opere di carità o umanitarie)? Come? Semplicemente non firmando. O addirittura quando hai firmato per lo Stato!  
4) Lo sai che la Chiesa Cattolica guadagna ogni anno, dalle nostre tasse, quasi un miliardo di euro? Sai cosa ci fa con tutti questi soldi?  
  
8 minuti del tuo tempo (ma anche meno) e saprai tutto questo, e potrai scegliere cosa fare concretamente e con cognizione di causa…  
  
Come funziona davvero l’8×1000  
Probabilmente pensi che, quando fai la scelta, l’8×1000 delle TUE tasse vada a chi decidi tu. Sbagliato! Lo Stato ogni anno raccoglie l’IRPEF e ne stanzia l’8×1000. Sembra una quota piccola, ma in realtà sono molti soldi: circa un miliardo di euro. Questi soldi vengono poi ripartiti a seconda delle scelte che sono state espresse: la tua firma conta come un voto e ha lo stesso valore di quella diciamo di Berlusconi o addirittura di Tiziano Ferro.  
Che fine fanno i soldi di chi non firma per nessuno? Vengono ripartiti a seconda dei voti di chi ha espresso la scelta. E sei persone su dieci non firmano.  
  
Riporto come esempio le scelte nella dichiarazione dei redditi del 2000:  
Nessuna scelta 60,40%  
Chiesa Cattolica 34,56%  
Stato 4,07%  
Valdesi 0,50%  
Ecc.  
  
I soldi sono stati spartiti così:  
  
Chiesa Cattolica 87,25%  
Stato 10,28%  
Valdesi 1,27%  
Ecc.  
  
La Chiesa Cattolica, con circa il 35% delle firme, prende l’87%. Cioè più di 900 milioni dei nostri euro. Se non scegli niente, se non firmi, la tua quota viene assegnata dagli altri. Sembra una di quelle scene da cartone animato, ma stavolta con protagonista la Chiesa Cattolica Romana: noi vogliamo offrirle un dolce, tagliamo sorridenti una generosa fetta di torta e lei, sbavando, invece della fetta, afferra tutto il resto e, lasciandoci a bocca aperta, lo butta giù.  
  
Tutto questo avviene perché quasi nessuno sa come funziona esattamente l’8×1000 e i mezzi di informazione si guardano bene dal dirlo; lo Stato non si fa nessuna pubblicità e tra le confessioni religiose solo la Chiesa Cattolica può permettersi costose campagne; infine chi non deve presentare la dichiarazione dei redditi (alcuni lavoratori dipendenti o i pensionati) spesso non sa che può scegliere anche lui a chi destinare la sua preferenza (tra poco ti dirò come, è semplicissimo). Pur essendo un meccanismo apparentemente basato sulla volontarietà, la mancanza di informazione e la ripartizione delle scelte inespresse di fatto vìolano questo principio.  
  
Non è finita qui!  
Se pensi che firmando per lo Stato fai la tua buona azione da cittadino, non destini nulla alle confessioni religiose, e che così quel denaro verrà usato per scopi sociali e umanitari, sbagli di grosso. Perché quei soldi sono nella disponibilità della Presidenza del Consiglio (sì, di Berlusconi, avete capito bene), che prende una grossa fetta dell’8×1000… e lo dà di sua iniziativa alla Chiesa Cattolica, finanziando restauri di chiese e conventi, nonché l’associazionismo cattolico!  
  
Un esempio concreto? Un link diretto? Non mi invento nulla, eccolo, direttamente dal sito del governo:  
  
http://www.governo.it/GovernoInforma/Dossier/ottopermille\_2009/dpcm\_2009/dpcm_%202009.pdf  
  
“Visto l’articolo 47, secondo comma, della legge 20 maggio 1985, n. 222, (…) una quota pari all’otto per mille dell’imposta sul reddito delle persone fisiche (…) è destinata, in parte, a scopi di interesse sociale e di carattere umanitario a diretta gestione statale e, in parte, a scopi di carattere religioso a diretta gestione della Chiesa Cattolica”.  
Dei 43 milioni 969 mila 406 euro che gli italiani hanno destinato allo Stato, la ripartizione 2009 ha previsto che alla Pontificia Università Gregoriana in Roma andassero 457 mila euro, al Fondo librario della Compagnia di Gesù, 498 mila euro, alla Diocesi di Cassano allo Ionio, 1 milione 142 mila euro, alla Confraternita di Santa Maria della Purità, Gallipoli, 368 mila euro ecc. ecc. per pagine e pagine. Così le chiese sono lucide ma le strade sono piene di buche, che l’altro giorno con lo scooter quasi quasi m’ammazzo.  
  
Cosa ci fa davvero la Chiesa Cattolica di tutti questi soldi  
Molti potrebbero dire: “Vabbe’, in fondo io do i soldi alla Chiesa per sostentare i poveri, per opere di carità e di bene, pazienza che il sistema che hanno escogitato per ottenerli non sia così democratico e trasparente. Il fine giustifica i mezzi”. Ebbene, le cose stanno in maniera molto diversa da quello che la stessa Chiesa, tramite i suoi tonitruanti spot pubblicitari, vuole farti credere (ma, sia detto per inciso, le bugie, comprese quelle di omissione, non erano peccati?). Solo una piccola percentuale dei soldi dell’8×1000 è destinata a interventi caritativi. Quasi l’80% serve al sostentamento del clero, a costruire nuove chiese (a riparare le vecchie ci pensa l’8×1000 dello Stato), a finanziare i tribunali ecclesiastici, alla manutenzione dell’incalcolabile patrimonio immobiliare, ecc. ecc.  
  
Ricordate la pubblicità dell’8×1000 imperniata sulla tragedia dello tsunami nel 2005? La campagna, affidata alla multinazionale Saatchi&Saatchi, costò alla Chiesa 9 milioni di euro. Fece guadagnare alla Chiesa più di 900 milioni di euro. La Chiesa donò alle vittime dello tsunami 3 milioni di euro, un terzo di quanto era costato lo spot, lo 0,3% (fonte CEI) della raccolta totale (cfr. C. Maltese, La questua. Quanto costa la Chiesa agli italiani, Feltrinelli).  
  
Uno scandaletto una tantum? Macché, è la prassi!  
In dettaglio (fonte CEI). Negli anni 2006 e 2007 la somma, pari in media a 960 milioni di euro (comprensiva di arretrati degli anni precedenti), è stata ripartita nel modo seguente: 35% – sostentamento dei sacerdoti; 12% – nuova edilizia di culto; 8% – beni culturali ecclesiastici; 16% – alle diocesi; 10% – altre iniziative nazionali (fondo per la catechesi, tribunali ecclesiastici, ecc.); 20% – interventi caritativi in Italia e nel terzo mondo.  
  
Come posso fare perché questo denaro non sia destinato dove non voglio?  
INNANZITUTTO, FIRMATE. Non c’è modo di non destinare quei soldi. Quindi bisogna prendere una decisione. Non siate quel 60% che non firma, e fa il gioco della maggioranza.  
Ma se non volete che i vostri soldi siano usati da confessioni religiose per il proprio sostentamento o in modi che non ritenete opportuni, né che lo stesso Stato le destini alla Chiesa Cattolica… come fare, visto che l’8×1000 si può dare solo allo Stato o a confessioni religiose?  
  
Una soluzione c’è. Il mio consiglio, e il consiglio anche della rivista MicroMega, e di sempre più persone e associazioni, è quello di firmare per la Chiesa Valdese.  
Badate bene, io non sono Valdese! Non me ne torna niente in tasca. E neanche a loro!… Nemmeno un euro viene destinato alle spese di culto.  
Sì, firmare per la Chiesa Valdese è giusto non solo perché è al passo coi tempi quanto a bioetica e diritti civili (ed è l’unica Chiesa cristiana in Italia ad aver riconosciuto le unioni omosessuali, dando il suo avallo alla benedizione di coppie gay), non solo perché i loro pastori possono sposarsi, e possono essere anche donne, e mille altre ragioni che la rendono, tra tutte le confessioni religiose che possono usufruire dell’8×1000, la più dignitosa, ma soprattutto perché la somma ricevuta dalla Chiesa Valdese (4,6 milioni di euro nel 2004) non può essere usata – per loro scelta – per alcuna attività religiosa, ma esclusivamente per progetti sociali, assistenziali e culturali.  
  
Ogni anno sul loro sito (http://www.chiesavaldese.org/index.php) viene pubblicato un resoconto molto dettagliato delle spese, che elenca tutti i progetti finanziati. Lo slogan dell’anno passato era, addirittura, FACCIAMO QUALCOSA DI LAICO. È paradossale… ed è vero (nel 2008, tra gli oltre 200 progetti finanziati, due riguardano le cellule staminali! Ce la vedo proprio la Chiesa Cattolica…)! Potete vedere la campagna di quest’anno cliccando qui.  
  
Fa’ girare al più presto queste parole condividendo il post su facebook o twitter o tramite mail (i pulsanti qui sotto ti consentono di farlo in un istante), perché la mancanza di informazione è alla base di tutto questo. Fallo subito, perché le firme per l’8×1000 si stanno facendo in questi giorni. E parlane con i tuoi amici e i tuoi parenti.  
  
Perché le cose possono cambiare. Stanno già cambiando. Sono già cambiate, rispetto a 8 minuti fa: sei arrivato alla fine e ora sai tutto questo, no?  
  
Un grazie speciale all’UAAR (Unione degli Atei e degli Agnostici Razionalisti), che si impegna moltissimo perché sempre più cittadini possano scegliere consapevolmente a chi destinare l’8×1000. Il loro sito è stata una delle fonti a cui ho attinto per questo post, e vi trovate tante altre informazioni. Visitatelo: http://www.uaar.it/laicita/otto\_per\_mille/  
  
ISTRUZIONI PER CHI NON FA LA DICHIARAZIONE DEI REDDITI MA HA UN CUD  
Se hai percepito solo redditi di pensione o di lavoro dipendente o assimilati e non devi presentare la dichiarazione dei redditi, puoi comunque effettuare la scelta per l’8 per mille (e quella del cinque per mille: non sono alternative) con il modulo allegato al CUD (di solito è l’ultimo foglio). Basta prendere il modulo, firmare nel riquadro corrispondente alla tua scelta (attenzione a non entrare in altri campi per non invalidarla), ricordarti di firmare anche in basso (dove specifichi che non devi fare dichiarazione dei redditi; anche questo è molto importante per non invalidare la tua scelta), e consegnare il modulo in busta chiusa presso un ufficio postale (è del tutto gratuito), o ad un ufficio di intermediazione finanziaria (CAF o commercialista). Sulla busta devono essere scritti il tuo cognome, nome, codice fiscale e la frase «SCELTA PER LA DESTINAZIONE DELL’OTTO E DEL CINQUE PER MILLE DELL’IRPEF». L’ufficio postale è tenuto a darti ricevuta. È di una semplicità sconcertante.

[www.vogliosposaretizianoferro.it](http://www.vogliosposaretizianoferro.it)