---
title: 'INTERROGAZIONE PARLAMENTARE SU BANDO DISCRIMINATORIO DEL MINISTERO AFFARI ESTERI'
date: Tue, 27 May 2008 19:37:24 +0000
draft: false
tags: [affari esteri, Bando di concorso, carriera diplomatica, Comunicati stampa, discriminazione, interrogazione parlamentare]
---

[Bando discriminatorio del ministero affari esteri per la carriera diplomatica](http://www.esteri.it/mae/doc_concorsi_gare/BandoDefinitivoConcorsoDiplomatico2008.pdf) , chi e' sposato e' avantaggiato. e chi non puo'  sposarsi ? violate le direttive europee.  
Interrogazione dei parlamentari radicali.  
Nel [bando pubblicato nei giorni scorsi dal Ministero degli Affari Esteri](http://www.esteri.it/mae/doc_concorsi_gare/BandoDefinitivoConcorsoDiplomatico2008.pdf) per la carriera diplomatica è previsto, come requisito di accesso, un'età inferiore ai 35 anni, innalzata di un anno nel caso il candidato sia coniugato. I parlamentari radicali, Elisabetta Zamparutti, Maria Antonietta Farina Coscioni, Matteo Mecacci, Donatella Poretti e Marco Perduca hanno depositato un'interrogazione sia alla Camera dei deputati che al Senato della Repubblica sulla vicenda. Violata la Direttiva Europea 2000/78/CE. Tra i punti elencati che dimostrano evidenti discriminazioni quello delle persone gay che non possono sposarsi.  
Di seguito il testo integrale dell'interrogazione:  
   
Interrogazione a risposta scritta Al Ministero per gli Affari Esteri - Al Ministero per le Pari Opportunità  
   
Premesso che:  
 Nel bando appena pubblicato dal Ministero per gli Affari Esteri italiano per accedere al concorso per la carriera diplomatica è previsto, come requisito di accesso, un'età inferiore ai 35 anni, innalzata di un anno nel caso il candidato sia coniugato.  
La direttiva europea 2000/78/CE, che stabilisce un quadro generale per la parità di trattamento in materia di occupazione e di condizioni di lavoro, vieta le discriminazioni fondate, tra l'altro, sulla religione o le convinzioni personali, sull'età o le tendenze sessuali, per quanto concerne l'occupazione e le condizioni di lavoro, ed in particolare le condizioni di accesso all'occupazione e al lavoro, sia dipendente che autonomo, compresi i criteri di selezione e le condizioni di assunzione indipendentemente dal ramo di attività e a tutti i livelli della gerarchia professionale, nonché alla promozione.  
Sono proibite discriminazioni indirette, che sussistono quando una disposizione, un criterio o una prassi apparentemente neutri possono mettere in una posizione di particolare svantaggio le persone, tra l'altro, di una particolare età o di una particolare tendenza sessuale, rispetto ad altre persone, a meno che tale disposizione, tale criterio o tale prassi siano oggettivamente giustificati da una finalità legittima e i mezzi impiegati per il suo conseguimento siano appropriati e necessari, e che per la natura di un'attività lavorativa o per il contesto in cui essa viene espletata, tale caratteristica costituisca un requisito essenziale e determinante per lo svolgimento dell'attività lavorativa, purché la finalità sia legittima e il requisito proporzionato.  
L'articolo 6 della direttiva prevede che gli Stati membri possano prevedere che le disparità di trattamento in ragione dell'età non costituiscano discriminazione laddove esse siano oggettivamente e ragionevolmente giustificate, nell'ambito del diritto nazionale, da una finalità legittima e i mezzi per il conseguimento di tale finalità siano appropriati e necessari.  
   
Per sapere:  
   
\- Non ritiene il Governo che la previsione di un limite di età di 35 anni per accedere al concorso per la carriera diplomatica, nonché la previsione dell'innalzamento di un anno nel caso in cui il candidato sia coniugato, sia discriminatoria, dato che questa non é oggettivamente e ragionevolmente giustificata da una finalità legittima e che i mezzi per il conseguimento di tale finalità non siano appropriati e necessari, oltre a non essere un requisito essenziale e determinante o proporzionato per lo svolgimento dell'attività lavorativa?  
\- Non ritiene il Governo che tale discriminazione sia basata non solo sull'età ma anche sull'orientamento sessuale, dato che in Italia le unioni tra persone dello stesso sesso non sono riconosciute?  
\- Non ritiene il governo che tale requisito, che non é basato su alcun titolo o esperienza lavorativa precedente, ma solamente sul suo stato civile di persona sposata o non sposata, sulle condizioni individuali sociali della persona, nonché sulle sue convinzioni rispetto al matrimonio, sia una discriminazione fondata anche sulle convinzioni personali e sulla religione?  
\- Non ritiene il governo che tale bando di concorso, come ogni pratica simile, metta l'Italia a rischio di infrazione della direttiva comunitaria?  
   
I DEPUTATI : Elisabetta Zamparutti, Maria Antonietta Farina Coscioni,  
  
I SENATORI: Donatella Poretti, Marco Perduca  
  
er info: sergio.rovasio@gmail.com