---
title: 'Certi Diritti Worksop held in ILGA EUROPE Congress'
date: Sun, 21 Jun 2009 07:29:54 +0000
draft: false
tags: [Senza categoria]
---

![Logo_certi_diritti](http://www.certidiritti.org/wp-content/uploads/2009/06/Logo_certi_diritti.jpg)

**WORKSHOP - October 29th** **18:00 to 19:30  
  
**

_associazione radicale_  **CERTI DIRITTI**

**_Globalization of the Same Sex Marriage Issue:_**

**_raising up a global awareness of the human rights values it entails._**

[freepeople-freemarriage@certidiritti.it](mailto:freepeople-freemarriage@certidiritti.it)

In almost all the democratic nations worldwide there are activists who have struggled and are struggling for the right to marriage for same sex couples - either lobbying complaisant politicians, or facing the juridical way.

The time may be ripe to overcome the national boundaries of such common initiatives, and raise the same-sex marriage issue at a global level in all democratic nations. In fact, same sex marriage opponents already operate at international level, and this makes it necessary to tune the communication toward the international organization and the public opinion, making it clearly aware of the human right significance of same sex marriage (similar to other issues in the past such us universal suffrage and free marriage for blacks).

The aim of the workshop is to start a comparison of different experiences and strategies on this item.

**Information:**

Gian Mario Felicetti  e-mail: [gianmario.felicetti@gmail.com](mailto:gianmario.felicetti@gmail.com)

mobile: +39.329.9045945

Ottavio Mazzocchi e-mail: [ottavio.marzocchi@europarl.europa.eu](mailto:ottavio.marzocchi@europarl.europa.eu)

Luigi Caribè e-mail: [lcaribe@hotmail.com](mailto:lcaribe@hotmail.com)

  

  

[English](internazionale/english.html)  [Español](internazionale/espanol.html)  [Portugues](internazionale/francais.html)  [Français](internazionale/portugues.html)[](component/content/article/389.html)[](component/content/article/389.html)