---
title: 'legge contro l''omofobia Liguria: governo sconfitto'
date: Thu, 31 Mar 2011 02:11:29 +0000
draft: false
tags: [Politica]
---

Il governo, sempre più arroccato su posizioni deliranti e ispirate al fondamentalismo religioso e ideologico, si era opposto alla legge regionale, che invece la Corte Costituzionale ha definito legittima. Decisione che fa fare passo avanti all'Italia.  
  
Roma, 30 marzo 2011  
  
Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti

  
L’assurda decisione del Governo italiano di opporsi alla Legge Regionale n.52 del 2009 "Norme contro le discriminazioni determinate dall' orientamento sessuale o dall' identità di genere", approvata il 29 ottobre 2009 dalla Regione Liguria "perché la legge eccede dalle competenze regionali perché solo lo Stato può decidere in materia di diritto civile" è stata oggi giustamente respinta dalla Corte Costituzionale.  
  
Ciò dimostra, se ancora ve ne fosse bisogno, di come **le azioni del Governo in tema di diritti civili e umani siano del tutto inesistenti che il riconoscimento dei diritti è persino osteggiato con assurde azioni legali senza alcun fondamento giuridico**.

Il governo ignora le gravi difficoltà in cui si trovano in Italia centinaia di migliaia di persone, vittime di violenze, atti di bullismo e gravi discriminazioni.  
**La Legge regionale 52/2009 della Liguria è in assoluto il miglior testo oggi esistente** in Italia in tema di lotta alle discriminazioni e azioni violente contro le persone più deboli e non possiamo che ringraziare le forze politiche, in primis i Verdi che l’hanno proposta, per il risultato ottenuto insieme a molte Associazioni.  
  
Proprio per queste ragioni **i Consiglieri Regionali della Lista Bonino Pannella al Consiglio Regionale del Lazio, Giuseppe Rossodivita e Rocco Berardo, hanno depositato nelle scorse settimane, la stessa identica proposta di legge**.  Il contenuto della proposta è di grande efficacia sul piano sociale, educativo, assistenziale e della prevenzione. Il Governo italiano ha fatto anche su questo una pessima figura. Per fortuna la politica ispirata dal fondamentalismo politico e religioso ne è uscita sconfitta.