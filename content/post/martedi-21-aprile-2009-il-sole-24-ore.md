---
title: 'MARTEDÌ 21 APRILE 2009, IL SOLE 24 ORE'
date: Wed, 22 Apr 2009 08:02:27 +0000
draft: false
tags: [Senza categoria]
---

**TEST CONSULTA PER LE NOZZE GAY**  
  
Il Tribunale di Venezia solleva il dubbio costituzionale sul vuoto di tutela  
  
di Beatrice Dalia  
  
ROMA Il divieto di nozze gay finisce alla Corte costituzionale. Nel silenzio del legislatore, sembra debba toccare al giudice delle leggi risolvere un effettivo vuoto di tutela alle unioni tra persone dello stesso sesso. Almeno è questa la via che tenta il Tribunale di Venezia con l'ordinanza 3 aprile 2009 (disponibile sul sito www. [guidaaldiritto.ilsole24ore.com](http://guidaaldiritto.ilsole24ore.com/)

), ipotizzando la violazione di ben quattro principi costituzionali a fronte di una sistematica lettura del Codice civile contraria al matrimonio tra persone «di orientamento omosessuale ».  
  
Il testo ampio e articolato mette in evidenza le contraddizioni di un ordinamento mai aggiornato rispetto all'impronta data dal legislatore del 1942 e dal riformatore del 1975. Nonostante, da allora, la famiglia abbia subito profonde metamorfosi nella sua struttura.  
  
L'intervento sollecitato alla Consulta ha la veste dell'ennesima " sfida" di principio, attivata da soggetti che si vedono vietata una prerogativa fondamentale. Venti coppie, tutte dislocate nel Centro Nord Italia, sono andate al proprio Comune di appartenenza chiedendo di procedere alla pubblicazione di matrimonio, ovviamente puntualmente rifiutata dall'ufficiale dello stato civile.  
  
Dall'impugnazione dei rifiuti sono scaturite altrettante cause e questa di Venezia è la prima che sfocia in una rimessione alla Corte costituzionale, rappresentando un'assoluta novità giurisprudenziale.  
  
I magistrati veneziani si sono detti consapevoli della singolare situazione socio/giuridica in cui si trova il nostro Paese: un impianto normativo che, di fatto, non prevede e non vieta le unioni gay; ma che non consente forzature «a fronte di una consolidata e ultramillenaria nozione di matrimonio come unione di un uomo e di una donna». Eppure, non si può non tener conto del fatto che negli ultimi decenni «si è assistito al superamento del monopolio detenuto dal modello di famiglia normale, tradizionale e al contestuale sorgere spontaneo di forme diverse, seppur minoritarie, di convivenza che chiedono protezione, si ispirano al modello tradizionale e come quello mirano ad essere considerate e disciplinate». Perciò, in nome di questa evoluzione culturale e della civiltà, secondo i giudici di Venezia, è necessario imporsi un ragionamento costituzionalmente orientato.  
  
Vietare la formalizzazione di un legame affettivo sarebbe atto contrario non solo ai diritti inviolabili della personalità, ma contrasterebbe anche con i principi fondamentali della parità di trattamento e quelli a protezione della famiglia; perché la «società naturale » di cui parla la Costituzione, per i giudici veneziani, non deve essere necessariamente formata da un uomo e una donna.  
  
Senza contare, poi, il rispetto dei vincoli derivanti dall'ordinamento comunitario e degli obblighi internazionali. A fronte di un contesto sovranazionale sempre più attento alla regolamentazione delle nuove relazioni affettive, il fermo impedimento italiano alle nozze gay rappresenterebbe un'ulteriore stonatura costituzionale. Il Tribunale, infatti, ricorda che sono numerosi gli atti delle Istituzioni eropee che da tempo invitano gli Stati a rimuovere gli ostacoli che si frappongono al matrimonio di coppie omosessuali o al riconoscimenti di istituti giuridici equivalenti.  
  
Sulla base di tutte queste considerazioni i giudici hanno ritenuto non manifestamente infondata la questione di legittimità costituzionale in relazione agli articoli 93, 96, 98, 107, 108, 143, 143bis e 156bis del Codice civile. Ancora una volta, dunque, la Corte costituzionale potrebbe vedersi costretta a fare i conti con l'Italia dei divieti.  
  
\-\-\-  
  
sezione: NORME E TRIBUTI data: 2009-04-21 - pag: 35  
  
Il provvedimento • Ordinanza del tribunale di Venezia depositata il 3 aprile 2009 È sulla base di tutte le considerazioni esposte che il Tribunale (di Venezia, ndr) è giunto al convincimento della della non manifesta infondatezza della questione di legittimità costituzionale delle norme di cui agli articoli 107, 108, 143, 143bis, 156bis e 231 Cc, laddove sistematicamente interpretate, non consentono che le persone di orientamento omosessuale possano contrarre matrimonio con persone dello stesso sesso; valuterà la Corte, qualora ritenesse la questione fondata, se vi sia la necessità di estendere la pronuncia anche ad altre disposizioni legislative interessate in via di consequenzialità ai sensi dell'articolo 27 della legge 87/1953. In punto di rilevanza, si osserva che l'applicazione delle norme indicate è evidentemente ineliminabile nell'iter logico-giuridico che questo remittente deve percorrere per la decisione: infatti, in caso di dichiarazione di fondatezza della questione così come sollevata, il rifiuto alle pubblicazioni - la cui richiesta dimostra inequivocabilmente la volontà di contrarre matrimonio dovrebbe ritenersi, in assenza di altra causa di rifiuto, illeggittima, mentre, in caso di non accoglimento, l'attuale stato della normativa imporrebbe una pronuncia di rigetto del ricorso. Per completezza si osserva che, a fronte del rifiuto alla pubblicazione da parte dell'ufficiale dello stato civile, essendo la pubblicazione una formalità necessaria per poter procedere alla celebrazione del matrimonio, non è individuabile alcun altro procedimento nell'ambito del quale valutare la questione.  
  
**MICHELE AINIS: «NESSUN IMPEDIMENTO AL MATRIMONION GAY»**  
E' docente di Diritto pubblico presso l'Università Roma Tre  
  
È un intervento costituzionale possibile, plausibile e obbligato quello chiesto dal Tribunale di Venezia, secondo Michele Ainis, ordinario di Istituzioni di Diritto pubblico all'Università Roma Tre.  
  
Ma è giusto che la tutela arrivi per via giurisdizionale?  
  
Questa è una fase in cui sulla Corte costituzionale si scaricano istanze che trovano sordo il legislatore; il quale non riesce a intervenire su temi che risultano laceranti più nel Palazzo che nella società civile. Certo, sarebbe meglio trovare una soluzione in via legislativa, ma alcuni casi reclamano una risposta giuridica. Quando c'è la paralisi del sistema legislativo è fisiologico che il ruolo di supplezza della Corte costituzionale si amplifichi.  
  
Quindi è possibile una lettura evolutiva del concetto di famiglia?  
  
Non c'è un impedimento testuale a riconoscere il diritto al matrimonio omosessuale. Anche perché sarebbe irrazionale impedire di sposarsi ai gay quando i transessuali posso farlo. C'è già stata l'evoluzione, in realtà, purché uno dei due modifichi il proprio sesso. Ed è irragionevole che casi simili vengano regolati in maniera dissimile.  
  
Il vero problema non è l'assenza di diritto ma la presenza del divieto?  
  
Non è un caso che sia nell'articolo 2 che nell'articolo 29 della Costituzione venga usato il verbo "riconoscere". Vuol dire che i miei diritti sono innati e non mi vengono riconosciuti da un sovrano e, quindi, che la famiglia preesiste allo Stato ed esiste al di là del diritto.  
  
Be. D.  
  
**GIULIO MARIA SALERNO: MATRIMONIO GAY, RICHIESTA INAMMISSIBILE**  
E' docente di diritto pubblico a Macerata  
  
Giulio Maria Salerno  
  
«La richiesta è inammissibile»  
  
Università Macerata  
  
Un'interpretazione innovativa delle norme sul matrimonio è da escludere per Giulio Maria Salerno, docente di Istituzioni di Diritto pubblico all'Università di Macerata.  
  
Un tentativo fallito in partenza?  
  
Il giudice non può chiedere di integrare le norme vigenti aggiungendo l'interpretazione richiesta, che è ben diversa da quella corrente. Perciò, la questione sollevata è palesemente inammissibile. Infatti, secondo l'insegnamento ormai costante della Corte, la legge deve essere interpretata e applicata in senso conforme alla Costituzione, e il giudice può chiedere l'intervento costituzionale solo qualora non riesca a trarre dalla legge un significato coerente con la Carta.  
  
Quindi la legislazione vigente non dà spazio a estensioni di tutela?  
  
Il riferimento al carattere "naturale" della famiglia contenuto nell'articolo 29 rende assolutamente evidente il fatto che nella famiglia protetta possano essere ricomprese soltanto le unioni di persone di sesso diverso; le sole infatti alla cui unione, per tradizione plurisecolare della nostra civiltà, si può dare il nome di matrimonio.  
  
Non c'è alcun margine allora per le nozze gay?  
  
Nulla esclude che il legislatore possa affiancare alla disciplina vigente un'altra che abbia per oggetto tali forme di libera esplicazione della personalità individuale, tutelata in via generale dall'articolo 2; ovviamente nei limiti costituzionali e di ragionevolezza che impediscono di attribuire a tali differenziate forme di unione i caratteri propri del matrimonio.  
  
Be. D.