---
title: 'I radicali Rocco Berardo e Sergio Rovasio in visita al carcere di Rebibbia'
date: Thu, 11 Aug 2011 14:06:14 +0000
draft: false
tags: [Politica]
---

Questa mattina una delegazione Radicale visiterà il carcere di Rebibbia. La delegazione è composta da  Rocco Berardo, Consigliere Regionale del Gruppo Lista Bonino Pannella, Federalisti Europei, accompagnato da Sergio Rovasio, Segretario dell'Associazione Radicale Certi Diritti.