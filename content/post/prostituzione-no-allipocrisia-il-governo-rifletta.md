---
title: 'PROSTITUZIONE: NO ALL''IPOCRISIA, IL GOVERNO RIFLETTA'
date: Fri, 12 Sep 2008 14:14:31 +0000
draft: false
tags: [Comunicati stampa]
---

**Prostituzione, Poretti: Non è con l'ipocrisia che si risolvono i problemi. No alla spazzatura sotto il tappeto**

Roma, 9 settembre 2008

• Intervento della senatrice Donatella Poretti, parlamentare Radicale-Partito Democratico

Punire e fare la faccia feroce con le prostitute di strada non è la soluzione per risolvere il fenomeno della prostituzione. Occorre intervenire prevedendo regole che integrino nel tessuto sociale e lavorativo chi sceglie di prostituirsi per mestiere. Cosi' l'ho gia' previsto in un disegno di legge presentato col sen. Marco Perduca (1), rifacendomi a quanto accade da anni in altri Paesi europei, con risultati concreti contro quelle criminalità efferate che nel nostro Paese godono (e continueranno a godere, secondo il Ddl del Governo) di impunità di fatto.  
  

La soluzione del Governo e' tipica di chi vuol nascondere la spazzatura sotto il tappeto. La prostituta/spazzatura -non interessa se per scelta o perché schiava- se é chiusa in casa non crea problemi, poco importano la sua condizione igienico-sanitarie, i suoi diritti e le sue tutele, basta non vedere.  
  

La mancanza di regolamentazione giuridica e fiscale equivale anche a nessuna tutela previdenziale e lavorativa, anche per chi esercitera' questo mestiere in una casa di propria proprieta'. Mentre non si capisce perché affittare una casa a chi si prostituisce per mestiere, debba comportare il rischio di favoreggiamento. La pezza sembra proprio non sia meglio del buco!  
  

Quando il ddl del Governo arriverà in Parlamento darò battaglia in questo senso, proponendo soluzioni adeguate e di respiro europeo. Solo una regolamentazione potra' separare la prostituzione coatta e minorile da quella volontaria, fornendo tutele e diritti e garantendo un efficace contrasto a quella criminalita' nazionale e internazionale che sullo sfruttamento e riduzione in schiavitu' di chi si prostuisce, realizza oggi floridi guadagni.  
  

(1) [http://blog.donatellaporetti.it/?p=86](http://blog.donatellaporetti.it/?p=86)