---
title: 'PERCHÉ CERTI DIRITTI PARTECIPA AL NAPOLI PRIDE'
date: Mon, 21 Jun 2010 20:54:25 +0000
draft: false
tags: [Comunicati stampa]
---

_"Se non ti mobiliti per difendere i diritti di qualcuno che in quel momento ne è privato, quando poi intaccheranno i tuoi, nessuno si muoverà per te. E ti ritroverai solo"_ (H. Milk)

Il 26 giugno si svolgerà a **Napoli** il Pride Nazionale, a cui l'Associazione Radicale Certi Diritti ha aderito. Crediamo sia importante esserci perchè . . .

![h-milk](http://www.certidiritti.org/wp-content/uploads/2010/06/h-milk.jpg "Harvey Milk")Oggi c’è chi ci vorrebbe di nuovo invisibili e paurosi, perché manifestare tutti insieme può aiutare chi non ha ancora il coraggio di vivere '[alla luce del sole](http://www.napolipride.com/)' e perché il Pride ci dà la possibilità di 'arruolare' nuove persone e far conoscere la nostre battaglie. Prima fra tutte la battaglia per il matrimonio tra persone dello stesso sesso, che dopo solo due anni ha condotto alla [**sentenza della Corte Costituzionale 138/2010**](tutte-le-notizie/686-comunicato-di-certi-diritti-e-del-collegio-di-difesa-su-motivazioni-sentenza.html), e che siamo fermamente intenzionati a portare avanti fino al raggiungimento dell'obiettivo.

Certi Diritti sfilerà con un carro che, attraverso immagini e slogan, vuole far conoscere la nostra storia e i nostri obiettivi. Ricorderemo il coraggio di Milk e di Rosa Parks, di Sylvia Rivera e di **Enzo Francone** che nel 2009, l'anno in cui ci ha lasciato, è stato il nostro tesoriere.

Denunceremo la **violenza contro lesbiche, gay e trans**, non solo in Italia, ma anche in quei Paesi dove ancora oggi le persone lgbt vengono arrestate o condannate a morte. Rivendicheremo l'**autodeterminazione** dell'individuo, denunceremo lo **strapotere vaticano** e il proibizionismo criminogeno e inutile sulla **prostituzione**. Chiederemo una legge che permetta finalmente alle **persone trans** il cambio del nome senza intervenire chirurgicamente (la cosiddetta 'piccola soluzione'). Distribuiremo anche **2 mila condom**, per ricordare la necessità di campagne di informazione sessuale nelle scuole, che abbiano il coraggio di parlare di metodi contraccettivi e di sensibilizzare i giovani in merito alle malattie sessualmente trasmissibili.     

**Per questo noi parteciperemo al Pride di Napoli**, come abbiamo partecipato a quello di Milano del 12 giugno ([qui le foto](http://www.flickr.com/photos/certidiritti)), e a quelli di Torino e [Palermo](http://www.facebook.com/#!/album.php?aid=7692&id=100000979490522&ref=mf), che si sono svolti sabato 19 giugno.

Tra gli appuntamenti  promossi a Napoli in occasione del Gay Pride, sabato mattina alle ore 10 presso la Fondazione SUD (Corso Umberto 35, 3 piano) si riunirà per la prima volta il **Comitato 'Sì lo Voglio'**, nato in seguito alla campagna di Affermazione Civile per raggiungere l'obiettivo del matrimonio civile per le coppie gay. A seguire, nello stesso luogo, l'Associazione Radicale Certi Diritti terrà la **riunione del Direttivo** aperta a tutti.

Ti aspettiamo a Napoli. . . cerca il nostro carro e percorri con noi la marcia verso i diritti.

Per info: 333 4801319