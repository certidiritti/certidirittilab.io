---
title: 'MATRIMONIO GAY: VESCOVO DI BOLOGNA PENSI A IMMORALITA'' DENTRO LA CHIESA'
date: Sat, 13 Feb 2010 16:32:23 +0000
draft: false
tags: [Comunicati stampa]
---

MATRIMONIO GAY: VESCOVO DI BOLOGNA PENSI ALLE IMMORALITA’ CHE PURTROPPO ATTANAGLIANAO LA CHIESA DI OGGI.

_**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**_

“Anche oggi abbiamo avuto da un autorevole rappresentante della gerarchia cattolica un’indicazione di contrarietà riguardo le libere scelte che dovrebbero avere i cittadini riguardo le unioni civili e il matrimonio gay.

Effettivamente ve n’era proprio bisogno, eravamo tutti molto disorientati sul tema. Quello che facciamo fatica a comprendere, dell’esternazione del Vescovo di Bologna, è la parte in cui egli considera “immorale” chi propone l’introduzione di questo ordinamento per le coppie gay o vota in Parlamento a favore di questa legge. Vogliamo tranquillizzare il Vescovo, al momento in Parlamento non è stata messa all’ordine del giorno nessuna delle proposte di legge dei deputati radicali su questo tema.

Ricordiamo al Vescovo che di immorale, persino dentro la chiesa cattolica, vi sono purtroppo altri atti, come quelli della violenza pedofila, diffusissimi non solo in Italia ma in molti paesi europei. Negli Stati Uniti alcune diocesi cattoliche hanno dovuto chiudere per bancarotta a causa dei danni che hanno dovuto pagare a seguito di condanne di Tribunali.

Occorre che il Vescovo di Bologna sia più rispettoso di ciò che molti Stati laicamente e democraticamente hanno già scelto in favore delle coppie gay. Anche in Italia prima o poi si dovrà legiferare in tal senso.

Il matrimonio civile per le coppie omosessuali non può avere nulla di immorale, così come il matrimonio civile per le coppie eterosessuali. E’ solo un problema di uguaglianza semmai. Non certo di questioni immorali”.