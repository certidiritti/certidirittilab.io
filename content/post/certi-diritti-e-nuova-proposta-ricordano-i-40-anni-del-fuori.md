---
title: 'Certi Diritti e Nuova Proposta ricordano i 40 anni del FUORI'
date: Wed, 09 Nov 2011 07:31:46 +0000
draft: false
tags: [Movimento LGBTI]
---

**Nuova Proposta** (Donne e Uomini omosessuali e transessuali cristiani) E l'**associazione radicale Certi Diritti** ricordano a Roma i 40 anni della nascita del Fuori!, il primo movimento italiano delle persone omosessuali e transessuali.

**Mercoledì 23 novembre 2011 ore 18**

_**(Roma – 7 novembre 2011) - L'Associazione Nuova Proposta - gruppo di omosessuali cristiani operante nella Capitale da oltre 20 anni per l’accoglienza delle persone che faticano a coniugare con serenità la propria fede e la propria affettività – e l’Associazione Radicale “Certi Diritti”, organizzano a Roma, il 23 novembre alle 18.00, presso la sede del Partito Radicale, in via di Torre Argentina 76, un momento di celebrazione dei 40 anni dalla nascita del FUORI! (Fronte Unitario Omosessuale Rivoluzionario Italiano), con la proiezione del documentario sulla storia del FUORI e la presentazione del libro “Un omosessuale normale”, autobiografia di Angelo Pezzana, fondatore del FUORI stesso.**_

All'interno del programma di attività 2011-2012 dell’Associazione Nuova Proposta, legato dal filo conduttore della riscoperta della verità e del’autenticità dal titolo, e di quello dell’Associazione Radicale “Certi Diritti”, viene organizzato un momento di celebrazione per i 40 anni dalla nascita del FUORI.

In programma la proiezione del documentario sulla storia di FUORI!, il primo movimento di liberazione omosessuale italiano, attraverso le storie, il volti,le parole dei protagonisti, il grande cambiamento, la visibilità, le battaglie e, infine l'identità, e la presentazione del libro “Un omosessuale normale”, autobiografia di Angelo Pezzana, fondatore del FUORI! Stesso, una sorta di diario nel quale l'autore racconta i momenti più significativi nella sua vita di omosessuale, ricordi d'infanzia, la prima percezione del sesso, i viaggi, le persone incontrate, illustri e sconosciute, la scoperta del mondo gay e la ricerca delle parole che non esistevano ancora. Per arrivare poi alla politica, all'omofobia, alla cronaca nera, alle leggi.

_**“Quest’anno, Nuova Proposta ha deciso di focalizzare il suo programma sul tema della Verità e dell’Autenticità, come percorso liberatorio di riappropriazione della bellezza della propria vita – ci dicono i rappresentanti del gruppo di omosessuali cristiani – e, quindi, ci è sembrato doveroso dedicare un momento di ringraziamento a coloro i quali, molti anni fa, per primi e nel silenzio globale sul tema dell’omosessualità e transessualità, hanno offerto di metterci la propria faccia per avviare un percorso di rivendicazione dei diritti per tutti noi: Angelo Pezzana e tutti i fondatori del FUORI!”**_