---
title: 'Italia sempre più debitrice del Fondo mondiale contro l''Aids. Sostegno alla richiesa di chiarimenti al Governo italiano su lotta all''Aids'
date: Fri, 20 Jul 2012 11:00:28 +0000
draft: false
tags: [Salute sessuale]
---

Grave denuncia della Lila: Italia ha abbandonato la lotta contro la diffusione dell'hiv.

Roma, 20 luglio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Da domenica 22 a a venerdì 27 luglio si svolgerà a Washington la XIX Conferenza internazionale sull’Aids alla quale parteciperanno esperti  e organizzazioni governative provenienti da tutto il mondo. Secondo quanto denunciato dalla Lega Italiana per la Lotta contro l’Aids e dall’Osservatorio italiano sull’Azione globale contro l’Aids, l’Italia ha abbandonato da tempo gli impegni presi dato che dal 2009 non versa più denaro al FGATM (Fondo Globale per la lotta contro l’Aids, la Tubercolosi e la Malaria) con un debito quindi di 260 milioni di euro rispetto agli impegni presi.  
  
Al G8 di Genova (luglio 2001) l'Italia si era impegnata a contribuire al Fondo con oltre 100 milioni di dollari all’anno  diventando così il secondo donatore dopo gli Stati Uniti ed acquisendo di diritto uno dei sette posti riservati nel Consiglio di Amministrazione. Tale situazione è aggravata da una mancanza di strategia per fronteggiare la pandemia nei paesi più poveri, l’impegno del nostro Paese per sconfiggere l’Aids si è infatti praticamente azzerato.

  
L'Agenzia delle Nazioni Unite per l'AIDS (UNAIDS) stima che 1,4 milioni di persone hanno iniziato la terapia antiretrovirale nel 2011, l'obiettivo globale è di raggiungere 15 milioni di persone in cura entro il 2015. Medici Senza Frontiere denuncia che allo stesso tempo, il dibattito internazionale invita sempre più gli Stati africani a mettere in campo le proprie risorse nazionali per l'emergenza HIV senza che ne abbiano la possibilità. I piani per incrementare il trattamento e migliorare la qualità delle cure rischiano di essere interamente demoliti a causa del ristagno del supporto internazionale.

Il Secondo rapporto pubblicato pochi giorni fa da UNAIDS informa che nel 2011 vi erano 34,2 milioni di persone sieropositive nel mondo, di cui il 69% nella sola Africa subsahariana. I programmi di lotta contro la malattia realizzati finora stanno producendo risultati: l’incidenza dell’Hiv a livello globale è in calo e l’accesso alla terapia antiretrovirale si sta espandendo. Si stima che 8 milioni di persone sieropositive che vivono nei paesi più poveri abbiano accesso ai farmaci salvavita, nel 2004 erano soltanto 700.000. Sono quasi 7 milioni, tuttavia, le persone che non ne possono beneficiare e la stabilizzazione della pandemia non è ancora consolidata: per ogni persona che inizia la terapia antiretrovirale ve ne sono due che contraggono l’Hiv. E’ fondamentale, pertanto, non abbassare la guardia e continuare a investire risorse per contrastare l’Aids; un calo dell’impegno finanziario metterà a rischio i risultati raggiunti finora.

Anche l’Asssociazione Radicale Certi Diritti sostiene la richiesta al Governo dell’Osservatorio Italiano sull’Azione Globale contro l’AIDS e la LILA che chiedono di definire una strategia che indichi il ruolo che il nostro Paese intende rivestire nei prossimi anni per contribuire a sconfiggere la pandemia. Il Governo italiano faccia sapere se intende rientrare nel novero dei global players che sostengono concretamente la lotta contro l’Aids o se ha deciso, a differenza degli altri Paesi del G8, di abbandonare al proprio destino milioni di bambini, donne e uomini che vivono nei paesi più poveri senza accesso ai farmaci salvavita.