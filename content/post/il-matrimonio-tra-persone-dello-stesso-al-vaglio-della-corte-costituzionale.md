---
title: 'IL MATRIMONIO TRA PERSONE DELLO STESSO AL VAGLIO DELLA CORTE COSTITUZIONALE.'
date: Wed, 17 Mar 2010 07:36:00 +0000
draft: false
tags: [Senza categoria]
---

Arcigay Palermo organizza e presenta il convegno “**_Il matrimonio tra persone dello stesso sesso al vaglio della Corte Costituzionale_**” che si inserisce nelle iniziative della campagna di affermazione civile promossa per il riconoscimento del matrimonio tra persone dello stesso sesso.

Il Convegno si svolgerà  il giorno 20 marzo alle ore 9,30, presso i saloni dell'Associazione Culturale "Stanze al Genio", via Garibaldi, 11 gentilmente concessi dal suo presidente, il dott. Pio Mellina, dopo il diniego - velato da ragioni anche burocratiche - di uno spazio pubblico da parte dell'Amministrazione Comunale di Palermo.

L'iscrizione al Convegno è consentita previa esibizione dell'invito o aderendo all'Associazione Culturale "Stanze al Genio" con un contributo di euro 5, fino ad esaurimento dei posti. Gli inviti sono disponibili presso il locale Blow up all'aperitivo  del martedì, alle ore 19,00. Per ulteriori informazioni è possibile contattare Arcigay Palermo, visitando il sito web [http://arcigaypalermo.wordpress.com](http://arcigaypalermo.wordpress.com/) e inviando una email all'indirizzo [palermo@arcigay.it](mailto:palermo@arcigay.it) oppure all'indirizzo [cyning@tiscali.it](mailto:cyning@tiscali.it).

Il Convegno si rivolge a tutti con lo scopo di discutere di un sistema normativo nazionale in cui le libertà fondamentali e i diritti umani e civili  siano riconosciuti, garantiti e tutelati senza discriminazioni. Il Convegno porta all’attenzione un tema di attualità, non più relegato a questione di natura soltanto etica-morale, ma anche e soprattutto giuridica, attraverso il dialogo di persone illuminate provenienti dal mondo della scienza, della politica e della religione. In questo percorso verso la rimozione dei condizionamenti e dei pregiudizi che stanno intorno al concetto di normalità, secondo il più diffuso luogo comune, fondamentale è la valorizzazione delle diversità.

Il tema assolutamente delicato prende spunto dalle questioni di legittimità costituzionale sollevate dai Tribunali di Ferrara e Venezia, nonchè dalle Corti di Appello di Trento e Firenze che hanno ritenuto contrario alla Costituzione il non consentire alle coppie di persone dello stesso sesso di contrarre matrimonio: il diritto di contrarre matrimonio – si legge nelle molteplici ordinanze di rimessione - è un momento essenziale di espressione della dignità umana, esso deve essere garantito a tutti senza discriminazioni derivanti dal sesso o dalle condizioni personali (quali l’orientamento sessuale), con conseguente obbligo dello Stato di intervenire in caso di impedimenti all’esercizio.

Un giurista, un sociologo del diritto, un pedagogo, il presidente dell’associazione più rappresentativa sul territorio nazionale del movimento omosessuale dialogheranno tra loro e innanzi al pubblico sulle questioni giuridiche, sociali ed etiche sollevate dall’eccezione di legittimità costituzionale solleva.

Curatore: avv. Marco Carnabuci

Relatori:

Avv. Francesco Bilotta, professore aggregato di diritto privato presso l’Università degli Studi di Udine.

Per la Mimesis edizioni ha curato i volumi: “_Le unioni tra persone dello stesso sesso_” (2008) e insieme a Bruno de Filippis “_Amore civile. Dal diritto della tradizione al diritto della ragione_” (2009). Per la stessa casa editrice dirige la Collana _LGBT -_ _Studi sull’identità di genere e l’orientamento sessuale_.

Ha collaborato alla stesura della Proposta di legge sul Patto civile di solidarietà e unioni di fatto presentata nella XIV Legislatura.

Socio fondatore dell’Associazione Avvocatura per i diritti LGBT – Rete Lenford. Ha coordinato dal punto di vista giuridico la campagna nazionale per il riconoscimento del diritto al matrimonio tra persone dello stesso sesso, denominata “Affermazione civile”.

Dott. Cirus Rinaldi, ricercatore di Sociologia giuridica, della devianza e mutamento sociale presso l’Università degli Studi di Palermo. Componente dei gruppi di ricerca sul fenomeno mafioso e sull’analisi dei fenomeni di esclusione sociale nella città di Palermo. Collabora con AGEDO. Ha partecipato in qualità di sociologo su incarico di AGEDO ai lavori della 12° commissione del Senato della Repubblica, XIV Legislatura, relativamente a "Indagine conoscitiva sui fenomeni di denatalità, gravidanza, parto e puerperio in Italia". E' autore di saggi sul tema LGBT.

Dott. Giuseppe Burgio, assegnista di Pedagogia presso l’Università degli Studi di Palermo. Si occupa di prevenzione e riduzione del bullismo omofobico. Il suo ultimo libro è “_Mezzi maschi. Gli adolescenti gay dell’Italia meridionale_” pubblicato dalla casa editrice Mimesis.

Avv. Paolo Patanè, Presidente nazionale di Arcigay. Già responsabile del settore Giuridico e del settore Salute dell’Associazione, nonché promotore della nascita di 5 nuovi comitati provinciali in Sicilia, regione storica per la nascita del movimento organizzato LGBT.