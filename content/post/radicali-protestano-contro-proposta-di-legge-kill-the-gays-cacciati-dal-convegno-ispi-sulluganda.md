---
title: 'Radicali protestano contro proposta di legge ''Kill the gays'': cacciati dal convegno ISPI sull''Uganda'
date: Tue, 04 Dec 2012 22:37:16 +0000
draft: false
tags: [Africa]
---

IL PRIMO MINISTRO UGANDESE DICE CHE NON SOSTERRÀ LA PROPOSTA.

Comunicato stampa dell’Associazione Radicale Certi Diritti e dell’Associazione Enzo Tortora - Radicali Milano

Milano, 5 dicembre 2012  

Martedì 4 dicembre l’ISPI di Milano ha organizzato un convegno intitolato “Stabilità e instabilità in Africa: il ruolo dell’Uganda” con la partecipazione del Primo Ministro ugandese, Patrick Amama Mbabazi.

In Largo Bortolo Belotti si è svolto un sit-in di protesta contro una proposta di legge, che il Parlamento ugandese dovrebbe votare entro metà dicembre, per introdurre pene più severe contro le persone LGBTI (Lesbiche, Gay, Bisessuali, Transessuali e Intersessuali) tra cui l'ergastolo e, persino, la pena di morte per chi risultasse colpevole di "omosessualità aggravata" – quando cioè uno dei partecipanti è un minore, HIV-positivo, portatore di handicap o "criminale seriale".

Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti, e Claudio Barazzetta, segretario dell’Associazione Enzo Tortora - Radicali Milano hanno partecipato al convegno per esprimere il loro dissenso nei confronti della proposta di legge. Quando il Primo Ministro ugandese ha preso la parola i segretari due segretari radicali hanno esposto uno striscione con scritto "Stop Anti-Gay Bill!" per qualche minuto, prima di essere allontanati, trattenuti dalla polizia e poi impossibilitati a partecipare alla continuazione dei lavori poiché "sgraditi". I due segretari radicali si sono allora uniti al sit-in in Largo Bortolo Belotti.

Due militanti di Arcilesbica Zami, Luisa Bordiga e Lucia Giansiracusa, sono invece rimaste nelle prime file e sono riuscite a seguire i lavori sino alla fine ponendo la questione ai relatori. Il Primo Ministro, Patrick Amama Mbabazi, dopo qualche tentennamento ha detto che il governo non sosterrà la proposta di legge.

Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti, dichiara: ≪È gravissimo che in un convegno, che dovrebbe essere di studi, non sia possibile manifestare il proprio doveroso dissenso nei confronti di una grave violazione dei diritti umani. Evidentemente il capo di Stato di un Paese che, oltre a punire con 14 anni di carcere i suoi cittadini per il solo fatto di essere omosessuali e pensare di aumentare quelle pene sino all’ergastolo o addirittura alla pena capitale, vuole vietare anche la "promozione" dei diritti delle persone LGBTI e punire tutti coloro che “finanziano o sponsorizzano l’omosessualità”, ha anche a Milano il potere di far prevalere il garbo sulla doverosa denuncia di una gravissima violazione dei diritti umani fondamentali. In ogni caso il Primo Ministro Patrick Amama Mbabazi ha potuto chiaramente vedere che anche in Italia le proteste contro la proposta di legge in discussione è forte. L’impegno preso a non sostenere la proposta di legge è importante, ma il governo non può limitarsi a una presa di distanza formale. Al contrario deve fare tutto ciò che è in suo potere per evitare che questa sciagurata proposta diventi effettivamente legge dello Stato ugandese, convincendo, per esempio, il Presidente a non firmare il provvedimento≫.

Claudio Barazzetta, segretario dell'Associazione Enzo Tortora - Radicali Milano, dichiara: "Ci si chiede come un paese come l'Uganda, che in occasione del convegno organizzato dall'ISPI si vuole presentare come uno degli attori principali per la stabilità in Africa - non essendo nemmeno membro dell'unica organizzazione sovranazionale efficace in Africa australe, la SADC Southern African Development community - possa proporre leggi come il Bill no 18 Anti Homosexuality Bill, che va esattamente nella direzione opposta a quella della volontà di creare stabilità nel continente africano. E' inverosimile e rasenta il ridicolo che, proponendosi come fautori di stabilità, e quindi di democrazia e libertà, le più alte autorità ugandesi non abbiamo permesso che venissero loro rivolte domande sulla legge in discussione. E' facile indossare l'abito dei fautori di stabilità, quando si nega, persino in terreno neutro, la possibilità di un serio e normale confronto, ponendo loro anche domande scomode. Come può un paese pretendere di essere accreditato come serio punto di riferimento, quando nel proprio ordinamento vuole introdurre una legge che mette a morte i propri cittadini in base al proprio orientamento sessuale e negando il diritto fondamentale della libertà d'espressione. I diritti umani vengono prima di ogni altra considerazione. Ci stupisce inoltre l'atteggiamento di coloro che non hanno permesso al segretario di Radicali Milano e al segretario dell'Associazione Radicale Certi Diritti, regolarmente registrati alla partecipazione del convegno, di porre domande in merito: Barazzetta e Guaiana hanno nonviolentemente esposto il loro striscione, in totale silenzio e hanno chiesto successivamente di poter, nel momento dedicato al dibattito, porre la semplice domanda "perchè volete punire, fino alla morte, esseri umani, vostri cittadini, solo per il loro orientamento sessuale?" Evidentemente per il Primo Ministro Mbabazi la stabilità non passa per la libertà d'espressione, non solo a casa propria, ma nemmeno quando è ospite di un istituto prestigioso come l'ISPI. Se l'Uganda vuole avere un ruolo decisivo per la stabilità in Africa, inizi a prendere esempio dal Sudafrica anche e sopratutto per quanto riguarda i diritti delle persone LGBTI, e faccia decadere immediatamente il Bill no.18!"

  
Per maggiori informazioni:

**[L’evento facebook con organizzatori e aderenti al sit-in >](https://www.facebook.com/events/515410075144150/516771018341389/?comment_id=516773348341156&;notif_t=event_mall_comment)**

**[La galleria fotografica >](https://www.facebook.com/media/set/?set=a.482815945090547.105970.288912161147594&;type=1)**

**[La foto più significativa >](https://www.facebook.com/photo.php?fbid=10151176045770488&set=a.10150332713275488.343646.552670487&type=1&theater)**

**[L'Associazione radicale Certi Diritti ha organizzato anche un sit-in davanti l'ambasciata ugandese a Roma il 29 novembre.](http://www.certidiritti.org/2012/11/29/29-novembre-manifestazione-davanti-ambasciata-ugandese-a-roma-contro-legge-antigay-presente-anche-delegazione-ilga-europe/)  
[IL VIDEO >](http://www.certidiritti.org/2012/11/29/29-novembre-manifestazione-davanti-ambasciata-ugandese-a-roma-contro-legge-antigay-presente-anche-delegazione-ilga-europe/)  
**