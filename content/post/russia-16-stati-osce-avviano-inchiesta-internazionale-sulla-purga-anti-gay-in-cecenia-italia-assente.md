---
title: 'Russia: 16 Stati OSCE avviano inchiesta sulla purga anti-gay in Cecenia. Italia assente'
date: Fri, 02 Nov 2018 09:46:25 +0000
draft: false
tags: [Russia]
---

![Logo OSCE e bandiere Stati](http://www.certidiritti.org/wp-content/uploads/2018/11/3957-2.jpg)
------------------------------------------------------------------------------------------------

Il lavoro degli attivisti russi del Russian LGBT Network, al quale ci onoriamo di aver contribuito, ha finalmente avuto successo.
---------------------------------------------------------------------------------------------------------------------------------

"Ieri, 16 Stati membri dell’OSCE hanno invocato il Meccanismo di Mosca, grazie al quale **potrà finalmente avviarsi un’inchiesta internazionale sugli arresti arbitrari, torture e uccisioni di persone omosessuali in Cecenia**, a prescindere dalla collaborazione della Russia. Nonostante i nostri appelli, **l’Italia è completamente assente**, mostrando un’indifferenza inaccettabile nei confronti di quello che noi attivisti riteniamo un vero e proprio crimine contro l’umanità”. Lo afferma Yuri Guaiana, Presidente dell’associazione radicale Certi Diritti.

“Il Meccanismo di Mosca è uno strumento creato dall’Organizzazione per la Sicurezza e la Cooperazione in Europa per monitorare il rispetto dei diritti umani da parte degli Stati membri. È stato invocato solo sette volte nella storia dell’OESCE e questa è la prima volta che viene invocato per **investigare la violazione dei diritti umani delle persone LGBTI**”, spiega Guaiana, che sta dando alle stampe l’opera collettanea “Il lungo inverno democratico nella Russia di Putin” dove ricostruisce anche la vicenda del _pogrom_ anti-gay in Cecenia.

“Il Meccanismo di Mosca è stato attivato dopo che la Russia ha risposto alle richieste fatte dagli Stati membri dell’OSCE, ma senza entrare nel merito delle preoccupazioni sollevate. Ora, una Commissione di 3 esperti – solo uno dei quali potrà essere scelto dalla Russia – verrà costituita dall’Ufficio OSCE per le istituzioni democratiche e i diritti umani e un’inchiesta internazionale sarà finalmente avviata. Anche se la Russia si rifiutasse di far entrare gli esperti sul suo territorio, essi potranno scrivere un rapporto intervistando i sopravvissuti rifugiatisi fuori dalla Federazione Russa e auspicabilmente **riconoscere i crimini contro l’umanità che sono stati perpetrati in Cecenia** per quello che sono”.

[**SOSTIENI LA NOSTRA CAMPAGNA CONTRO LE LEGGI SULLA COSIDDETTA "PROPAGANDA GAY IN RUSSIA"!**](http://www.certidiritti.org/donazioni/)