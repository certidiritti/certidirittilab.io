---
title: 'AMORE CIVILE: VIDEO DOSSIER SU NUOVE FORME DI CONVIVENZA E RELAZIONI AFFETTIVE'
date: Wed, 25 Jun 2008 06:11:43 +0000
draft: false
tags: [Comunicati stampa]
---

E’ stato pubblicato il video dossier sulla molteplicità delle forme di convivenza, che è stato presentato durante il convegno Amore Civile, che fa parte della Conferenza Permanente per il Diritto di famiglia.  
[  
Guarda il video](index.php?option=com_content&task=view&id=128&Itemid=55)