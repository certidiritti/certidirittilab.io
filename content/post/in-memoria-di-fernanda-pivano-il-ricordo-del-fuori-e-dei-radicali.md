---
title: 'IN MEMORIA DI FERNANDA PIVANO: IL RICORDO DEL FUORI E DEI RADICALI'
date: Wed, 19 Aug 2009 12:57:23 +0000
draft: false
tags: [Comunicati stampa]
---

**ARRIVEDERCI NANDA**  
  
Quando nel 1971 in Italia si formò il primo movimento di liberazione omosessuale, il Fuori !, Fernanda Pivano ne fu, per affetto, amicizia, intelligenza, solidarietà, sostenitrice appassionata, collaborando ai primi numeri del nostro giornale. Fu lei la prima, nella introduzione a "Jukeboxe all'idrogeno" di Allen Ginsberg, a capire e a rivelare quanto l'identità omosessuale fosse una componente di grande rilievo nella nuova cultura che arrivava dall'America.  
Le siamo tutti debitori.  
  
**Angelo Pezzana, fondatore del Fuori!,** **insieme agli amici Enzo Francone, Marco Silombria, Enzo Cucco**

**CIAO NANDA, INCONDIZIONATAMENTE RADICALE!**

Difficile, ma doveroso, spiegare ai giovani internauti d'oggi,  
apparentemente emancipati e, invece, terribilmente risucchiati nei mefitici  
gorghi di un penoso conformismo di ritorno, appiattiti, livellati, in un  
torpore che ha molto di tanatologico, chi sia stata e cosa abbia  
rappresentato per una serie di generazioni che negli anni Cinquanta ha il  
loro stigma una figura incondizionatamente radicale come quella di Fernanda  
Pivano.

E' grazie a lei, in virtù anche dell'intenso, travagliato, rapporto  
con Cesare Pavese e sicuramente dell'influenza dell'esistenzialismo di un  
grande e adombrato filosofo come Nicola Abbagnano, se la cultura italiana ha  
potuto respirare in anni cruciali un'aria diversa, più libera, liricamente  
densa di aromi primaverili, infinitamente distante dall'asfittico, bolso,  
"strapaesismo" di marcette, messali o tetri pugni chiusi tanto in voga nella  
scena letteraria e intellettuale del nostro paese.

E' stata lei a recare alle nostre orecchie la voce, anzi le voci transoceaniche di Melville, Edgar  
Lee Masters, Anderson, Hemingway, Francis Scott Fitzgerald, Faulkner e soprattutto di quella straordinaria congrega di visionari, ribelli (mai rivoluzionari), invasi sino alle midolla dal pulsare della vita e dai richiami d'assoluto, che a partire dall'anarchico e salvifico versificare dell'immaginista William Carlos Williams fu la beat generation di Kerouac, Ginsberg, Ferlinghetti, Corso, Burroughs, Snyder, Sanders, Robert (Bob) Zimmerman (Dylan). La ricordiamo a fianco del mitico Jack - On the road,  
sempre inebriato da vapori etilici o fumi d'erba, da benzedrina o codeina, in surreali conferenze stampa, o dell'Allen, poeta libertario e liberatore, vate di una coscienza allargata e lisergica, che, con gesto provocatoriamente nonviolento, a Spoleto, nel 1967, tenta di regalare un fiore all'appuntato che lo voleva in galera per oltraggio al pudore (quale?, di chi?).

E, ancora, non possiamo dimenticare l'esperienza di Pianeta fresco, avviata, sempre alla fine degli anni Sessanta, insieme ad Ettore Sottsass, inseparabile compagno di viaggi, follie e marito. Due soli numeri, editi dalla torinese libreria Hellas di Angelo Pezzana tra 1967 e 1968, subito esauriti e ormai consegnati ai ricordi degli albori della cosiddetta controcultura in Italia (dopo sono venuti Fallo! di Angelo Quattrocchi e Re Nudo di Andrea Valcarenghi ma è un'altra storia).

E' stata radicale in tutto e per tutto, candidata per e con il partito, sempre a sostegno dei diritti civili e delle battaglie di libertà e amore che hanno contrassegnato l'ultimo sessantennio della nostra storia.

Controcorrente per indole naturale, non ha avuto mai problemi a mettersi continuamente in discussione, a varcare senza retaggi confini artistici, a incontrare e raccontare "i suoi amici" cantautori, da Piero Ciampi al concittadino De André, da Vasco (da lei incensato) a Jovanotti. Due anni fa, a novant'anni, scrisse con ineguagliabile e commovente lucidità: "dove c'è poesia c'è anche Assoluto e ci sono sguardi di poeti rivolti all'eternità con o senza poesia a renderli immortali".

Ciao Nanda.

Grazie di cuore per quello che hai saputo darci.  
  
Francesco Pullia e gli amici radicali tutti.