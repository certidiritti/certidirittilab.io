---
title: 'La peste italiana e le balle del 2012'
date: Wed, 14 Mar 2012 14:28:24 +0000
draft: false
tags: [Politica]
---

"Dallo scorso week end si è aperto in Italia un dibattito politico sul matrimonio tra persone dello stesso sesso. E’ bastato che Alfano dicesse una balla colossale". Articolo di Sergio Rovasio pubblico da Il Riformista del 14 marzo 2012.

Dallo scorso week end si è aperto in Italia un dibattito politico sul matrimonio tra persone dello stesso sesso. E’ bastato che Alfano dicesse una balla colossale: “con la sinistra nozze per i gay”, per far scatenare la solita ondata di ipocrisia clerical-partitocratica mista ad un approccio “viscido-solidaristico” che ha fatto esprimere quasi tutti gli esponenti di destra e sinistra, sopra e sotto, tutti,  contro il matrimonio tra persone dello stesso sesso. Abbiamo sentito la Bindi (Presidente del Pd) rispondere ad Alfano facendo una figura persino peggiore di lui. Non poteva mancare il mulinaro bianco Giovanardi che, preoccupato di vedersi rubata la scena sul tema del dargli addosso ai gay, lunedì non ha fatto mancare la sua attesissima e fondamentale opinione sul tema: “Credo che un padre che si rivela gay al proprio figlio di otto anni possa creargli problemi”. 

Tutti contro il matrimonio tra persone dello stesso sesso. Proprio tutti. E  non poteva mancare persino il tanto atteso parere di monsignor Fisichella, rettore della Pontificia Università Lateranense, vescovo ausiliare di Roma e niente popò di meno che “cappellano” della Camera dei Deputati (ti pareva che rimanesse fuori dal Palazzo) nonché membro del Pontificio Consiglio per la Nuova Evangelizzazione, il quale dice, in una simil-dichiarazione alla Bindi,  che: “nel nostro Paese si troveranno 'altre forme' che non contraddiranno 'l'articolo della Costituzione che parla della famiglia'.  Ma si, a sti poveretti qualcosa dovremo pur dare….

E poi, manco fosse scoppiata la terza guerra mondiale l’elefante Giuliano Ferrara, preoccupato invero assai, entra nelle case delle famigliole ‘naturali’ alle 20,30 di sera, su Rai Uno, parla allarmato per tre minuti di diritti naturali, matrimonio tra uomo-donna, ecc. Preoccupatissimo, sinceramente preoccupatissimo, invero assai, del matrimonio gay. Il pericolo che avanza!

Ci fosse stato uno, anche solo uno, che abbia sentito il bisogno di far parlare quelle/quei pochi che in Italia si battono per il diritto al matrimonio tra persone dello stesso sesso: magari anche solo fare una telefonata agli avvocati e ai giuristi della campagna di Affermazione Civile promossa nel 2008 dall’Associazione Radicale Certi Diritti che, in pochi mesi di azione, sono è riuscita a far esprimere la Corte Costituzionale su questo tema e che ritengono questo il traguardo principale da raggiungere per il superamento delle diseguaglianze; oppure chiedere un parere a quei tre-quattro  parlamentari che hanno depositato in Parlamento proposte di legge sulla materia (Rita Bernardini, Marco Perduca e Donatella Poretti per i Radicali e Paola Concia per il Pd). 

Ecco cos’è la Peste Italiana che, sempre più inascoltati, i Radicali denunciano ormai da tempo. Mentre in Gran Bretagna il Primo Ministro Cameron, leader dei conservatori, e Tony Blair,  ex Primo Ministro dei Laburisti, neo-convertito al cattolicesimo, si sono espressi in questi giorni in favore del matrimonio per le coppie gay, e mentre in Spagna, Belgio, Olanda, Portogallo,  Islanda, Sud-Africa, Canada, Novergia, Argentina, Capitale del Messico e diversi Stati Usa, tale legge di uguaglianza è già diventata realtà, noi siamo qui a confortarci con la lettura dei vari Giovanardi, Ferrara, Bindi, Fisichella, Alfano, tutti U-NA-NI-ME-MEN-TE contro i matrimoni gay. 

Eh si, ecco il grande pericolo del matrimonio per le coppie gay che avanza; manco avessero incardinato la discussione in qualche Commissione della Camera o del Senato o il Governo avesse  annunciato un decreto. E’ bastato Alfano, con la sua più grande balla del 2012!   Dibattito in Tv? Confronto tra le parti? Un po’ di informazione? Niente.

La peste italiana avanza sempre di più**.**

**[Iscriviti alla newsletter di Certi Diritti >](newsletter/newsletter)**