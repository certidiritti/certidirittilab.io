---
title: 'Legge antigay in Uganda: firma la petizione >'
date: Mon, 09 May 2011 22:09:08 +0000
draft: false
tags: [Africa]
---

**Il parlamento ugandese sta per approvare l’Anti-homosexuality bill, dopo la campagna di odio lanciata dai predicatori evangelici che ha portato alla morte di tanti attivisti lgbt, tra cui David Kato Kisule, iscritto di Certi Diritti ucciso il 26 gennaio scorso. Firma la petizione di All Out.**

  
  
  
  
**[FIRMA LA PETIZIONE DI AVAAZ >](http://www.avaaz.org/en/uganda_stop_homophobia_petition/?vl)**

**[FIRMA LA PETIZIONE >](http://www.allout.org/en/petition/uganda)**

**[LA LETTERA INVIATA AL MINISTRO FRATTINI per chiedere il suo intervento >](tutte-le-notizie/1114-emergenza-uganda-frattini-intervenga-contro-legge-antigay-che-sta-per-essere-approvata.html)**

**[SOSTIENI IL FONDO DAVID KATO KISULE necessario a sostenere le spese legali per assicurare un processo equo e veritiero sulla sua morte >](campagne/in-memoria-di-david-kato/1071-fondo-in-memoria-di-david-kato-kisule.html)**