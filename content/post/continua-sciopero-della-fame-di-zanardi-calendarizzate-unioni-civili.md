---
title: 'CONTINUA SCIOPERO DELLA FAME DI ZANARDI: CALENDARIZZATE UNIONI CIVILI!'
date: Wed, 20 Jan 2010 10:00:41 +0000
draft: false
tags: [Comunicati stampa]
---

**SCIOPERO DELLA FAME PER UNIONI CIVILI: PROSEGUE LO SCIOPERO DELLA FAME DI FRANCESCO ZANARDI PER LA CALENDARIZZAZIONE DELLE PROPOSTE DI LEGGE SULLE UNIONI CIVILI. DOPO LA CAMERA ANCHE AL SENATO, OGGI, VERRA' SOLLECITATA LA CALENDARIZZAZIONE DEI DISEGNI DI LEGGE DEPOSITATI.**

Roma, 20 gennaio 2010

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti"Prosegue lo sciopero della fame di Francesco Zanardi, giunto oggi al 17° giorno.** La coppia gay di Savona aveva cominciato l'iniziativa nonviolenta il 4 gennaio scorso in Piazza Montecitorio rivolgendosi ai Presidenti del Parlamento e delle Commissioni parlamentari Giustizia di Camera e Senato per chiedere la calendarizzazione delle diverse proposte di legge depositate di diversi gruppi politici sulle unioni civili e il matrimonio gay.  
All'appello non ha risposto finora nessuno. Questa mattina la deputata del Pd, On. Paola Concia è intervenuta alla Camera dei deputati in apertura di seduta per sollecitare quanto richiesto dalla coppia gay di Savona e da decine di Associazioni che si battono per i diritti civili e umani delle persone lesbiche e gay. Due giorni fa decine di Associazioni del movimento lgbt, hanno chiesto attenzione su questo tema alle massime istituzioni del Parlamento.  
Oggi anche al Senato, in chiusura di seduta, la Senatrice radicale Donatella Poretti chiederà la calendarizzazione dei diversi Disegni di Legge depositati sulle unioni civili ricordando l'iniziativa nonviolenta in corso. Anche il Parlamento Europeo si sta mobilitando con alcuni italiani che proporranno oggi una risoluzione all'aula.  
L'Associazione Radicale Certi Diritti continuerà a sostenere la coppia gay di Savona perchè con la loro iniziativa nonviolenta si dà voce a centinaia di migliaia di persone senza diritti e discriminate che gran parte del Parlamento e del Governo continua a ignorare. Tutto questo accade in Italia mentre anche in Portogallo è stata appena approvata una legge sul matrimonio gay".