---
title: 'Parlamento Europeo approva Risoluzione su Diritti Umani, orientamento sessuale e identità di genere all''Onu'
date: Wed, 28 Sep 2011 19:35:55 +0000
draft: false
tags: [Europa]
---

Parlamento europeo approva risoluzione sui diritti umani, l'orientamento sessuale e l'identità di genere all'Onu, rigettato emendamento su matrimonio tra persone dello stesso sesso con un voto PPE-SD.

IN OSSEQUIO AL VATICANO ITALIANI DEL PPE CONFERMANO DISSENSO DA LORO GRUPPO.

Bruxelles-Roma, 28 settembre 2011

Comunicato Stampa dell'Associazione Radicale Certi Diritti:

Il Parlamento europeo ha approvato oggi una risoluzione firmata da PPE, SD, ALDE, ECR, GUE e Verdi sui diritti umani, l'orientamento sessuale e l'identità di genere nel quadro delle Nazioni Unite con 442 voti favorevoli, 104 contrari e 40 astensioni.

La risoluzione esprime la "preoccupazione per le numerose violazioni dei diritti umani e le diffuse discriminazioni connesse all'orientamento sessuale e all'identità di genere perpetrate sia nell'Unione europea che nei paesi terzi"; riconosce il lavoro dell'ONU "per garantire la piena applicazione dei principi internazionali in materia di diritti umani indipendentemente dall'orientamento sessuale o dall'identità di genere delle persone"; "accoglie con favore l'adozione, da parte del Consiglio dei diritti dell'uomo, della risoluzione A/HRC/17/19 sui diritti umani, l'orientamento sessuale e l'identità di genere"; chiede "un dibattito ad alto livello in occasione della 19a sessione del Consiglio dei diritti dell'uomo, che si svolgerà nella primavera 2012, ai fini di un dialogo costruttivo, informato e trasparente sulla questione delle leggi e pratiche discriminatorie e degli atti di violenza contro i singoli fondati sull'orientamento sessuale e l'identità di genere"; chiede agli Stati membri ed alle istituzioni UE di promuovere sistematicamente la tutela e il rispetto dei diritti umani relativi all'orientamento sessuale e all'identità di genere"; "si rammarica che nell'Unione europea i diritti di lesbiche, gay, bisessuali e transgender, ivi inclusi il diritto all'integrità fisica, alla vita privata e alla famiglia, il diritto alla libertà di opinione, di espressione e di associazione, il diritto alla non discriminazione, alla libera circolazione e il diritto di asilo, non siano ancora pienamente rispettati in ogni circostanza"; chiede alla Commissione di elaborare una tabella di marcia globale contro l'omofobia, la transfobia e le discriminazioni fondate sull'orientamento sessuale e l'identità di genere".

Alcuni emendamenti SD, ALDE, Verdi e GUE sono stati approvati, in particolare in merito alla libertà di circolazione, l'asilo politico e la depatologizzazione dell'identità di genere. Un emendamento GUE che felicitava quegli Stati membri che hanno riconosciuto il matrimonio per le persone dello stesso sesso e che chiede agli altri Stati membri di fare lo stesso é stato rigettato a causa del voto contrario del PPE e, contrariamente alle posizioni espresse in precedenza in occasione di altri rapporti, dei Socialisti (SD).

L'Associazione Radicae Certi Diritti si felicita per l'approvazione della risoluzione ma si augura pero' che il Parlamento euroLpeo voglia essere ancora più ambizioso in occasione delle prossime risoluzioni sui diritti umani nella UE, chiedendo agli Stati membri di approvare anche l'istituto del matrimonio tra persone dello stesso sesso.

Qui sotto il voto finale per appello nominale della risoluzione (sottolineati ed in neretto gli italiani che hanno votato contro o si sono astenuti):

**1.**              **B7-0523/2011 - Résolution**

28/09/2011 12:38:38.000

442

**+**

**ALDE**:          Alfano, Alvaro, Aylward, Bearder, Bilbao Barandica, Bowles, Buşoi, Chatzimarkakis, Creutzmann, Davies, De Backer, De Sarnez, Donskis, Duff, Ek, Gallagher, Gerbrandy, Goerens, Griesbeck, Haglund, Hall, Harkin, Hirsch, Hyusmenova, Ilchev, Iovine, Jensen, Kacin, Kazak, Koch-Mehrin, Kozlík, Krahmer, Lambsdorff, Lepage, Ludford, Lyon, Manders, Manner, McMillan-Scott, Meissner, Michel, Mulder, Mănescu, Newton Dunn, Ojuland, Oviir, Panayotov, Parvanova, Paulsen, Reimers, Rinaldi, Rochefort, Savisaar-Toomast, Schaake, Schmidt, Skylakakis, Thein, Theurer, Tremosa i Balcells, Uggias, Uspaskich, Vajgl, Vattimo, Verhofstadt, Vălean, Wallis, Watson, Weber Renate, Wikström, Zanoni, in 't Veld, van Baalen

**ECR**:             Ashworth, Bokros, Cabrnoch, Campbell Bannerman, Deva, Harbour, Karim, Stevenson, Swinburne, Tošenovský, Yannakoudakis, Zahradil, Zīle, Češková

**EFD**:             Salavrakos, Terho, Tzavela

**GUE/NGL**:    Bisky, Chountis, Ferreira João, Figueiredo, Hénin, Klute, Kohlíček, Le Hyaric, Liotard, Lösing, Matias, Maštálka, Meyer, Murphy, Portas, Ransdorf, Remek, Rubiks, Scholz, Søndergaard, Triantaphyllides, Vergiat, Wils, Zimmer, de Brún, de Jong

**NI**:                 Martin, Severin, Sosa Wagner, Werthmann

**PPE**:             Abad, Albertini, Antoniozzi, Arias Echeverría, Audy, Ayuso, Bach, Bagó, Bastos, Bauer, Becker, Belet, Bendtsen, Bonsignore, Boulland, Brok, Busuttil, Băsescu, Cadec, Carvalho, Casa, Cavada, Coelho, Corazza Bildt, Danjean, Dantin, Dati, Daul, Delvaux, Deutsch, Dorfmann, Díaz de Mera García Consuegra, Estaràs Ferragut, Feio, Ferber, Fernandes, Fisas Ayxela, Fjellner, Fraga Estévez, Franco, Gahler, Gallo, Gargani, Gauzès, Giannakou, Glattfelder, Grosch, Grossetête, Gräßle, Gutiérrez-Cortines, Gyürk, Gál, Gáll-Pelcz, Herranz García, Higgins, Hökmark, Ibrisagic, Itälä, Ivanova, Jahr, Jiménez-Becerril Barrio, Járóka, Kalniete, Karas, Kariņš, Kasoulides, Kelam, Kelly, Korhola, Kovatchev, Kratsa-Tsagaropoulou, Kuhn, Kukan, Kósa, Köstinger, La Via, Lamassoure, Langen, Le Brun, Le Grip, Lope Fontagné, Marinescu, Mathieu, Matula, Mazzoni, McGuinness, Melo, Morin-Chartier, Motti, Méndez de Vigo, Nedelcheva, Oomen-Ruijten, Papanikolaou, Papastamkos, Patrão Neves, Peterle, Pietikäinen, Ponga, Preda, Proust, Pöttering, Rangel, Rübig, Salatto, Sanchez-Schmid, Saïfi, Schnieber-Jastram, Schöpflin, Seeber, Sommer, Stolojan, Stoyanov Emil, Striffler, Svensson, Szájer, Sógor, Teixeira, Theocharous, Thyssen, Trzaskowski, Tsoukalas, Urutchev, Vidal-Quadras, Vlasto, Weber Manfred, Zanicchi, Zver, de Grandes Pascual, de Lange, van de Camp, Áder, Őry, Šťastný

**S&D**:            Alves, Andrés Barea, Arif, Arlacchi, Arsenis, Attard-Montalto, Ayala Sender, Badia i Cutchet, Balzani, Balčytis, Berlinguer, Berman, Berès, Blinkevičiūtė, Borsellino, Bozkurt, Brzobohatá, Bullmann, Capoulas Santos, Caronna, Castex, Cercas, Childers, Christensen, Cofferati, Correia De Campos, Cortés Lastra, Cozzolino, Daerden, Danellis, De Angelis, De Keyser, De Rossa, Domenici, Droutsas, Dušek, El Khadraoui, Estrela, Fajon, Falbr, Ferreira Elisa, Flašíková Beňová, Fleckenstein, Färm, García Pérez, Gardiazábal Rubial, Gebhardt, Geier, Geringer de Oedenberg, Gierek, Goebbels, Grelier, Groote, Gualtieri, Guillaume, Gurmai, Gutiérrez Prieto, Göncz, Haug, Herczog, Hoang Ngoc, Honeyball, Hughes, Iotova, Irigoyen Pérez, Jaakonsaari, Kadenbach, Kammerevert, Kleva, Koppa, Krehl, Kreissl-Dörfler, Lange, Leichtfried, Leinen, Liberadzki, Ludvigsson, López Aguilar, Martin David, Martínez Martínez, Masip Hidalgo, Mavronikolas, Maňka, Menéndez del Valle, Merkies, Milana, Moraes, Moreira, Neuser, Neveďalová, Obiols, Olejniczak, Padar, Paliadeli, Papadopoulou, Pargneaux, Peillon, Pirillo, Podimata, Prendergast, Prodi, Rapkay, Rapti, Regner, Repo, Rodust, Roth-Behrendt, Rouček, Sassoli, Schaldemose, Scicluna, Sehnalová, Senyszyn, Serracchiani, Simon, Simpson, Sippel, Siwiec, Skinner, Smolková, Stavrakakis, Steinruck, Swoboda, Sánchez Presedo, Tabajdi, Tarabella, Thomsen, Tirolien, Trautmann, Ulvskog, Van Brempt, Vaughan, Vigenin, Weber Henri, Weiler, Westlund, Westphal, Yáñez-Barnuevo García, Zala, Zemke

**Verts/ALE**:  Albrecht, Auken, Benarab-Attou, Bové, Brantner, Brepoels, Bélier, Bütikofer, Canfin, Cohn-Bendit, Cronberg, Delli, Durant, Eickhout, Engström, Evans, Flautre, Giegold, Grèze, Harms, Hassi, Hudghton, Häfner, Häusling, Jadot, Joly, Junqueras Vies, Keller, Kiil-Nielsen, Lambert, Lamberts, Lichtenberger, Lochbihler, Lunacek, Lövin, Rivasi, Romeva i Rueda, Rühle, Sargentini, Schlyter, Schroedter, Schulz Werner, Smith, Staes, Tavares, Taylor, Tremopoulos, Turmes, Turunen, Ždanoka

104

**-**

**ALDE**:          Takkula

**ECR**:             Bielan, Gróbarczyk, Hannan, Helmer, Kamiński, Kowal, Legutko, Migalski, Piotrowski, Sturdy, Szymański, van Dalen

**EFD**:             Belder, **Bizzotto, Borghezio, Fontana, Morganti**, Paška, **Provera, Rossi, Salvini, Scottà, Speroni**, de Villiers

**NI**:                 Brons, Claeys, Griffin, Le Pen Jean-Marie, Le Pen Marine, Mölzer, Obermayr, Stoyanov Dimitar

**PPE**:             **Allam**, Antonescu, **Baldassarre**, Balz, **Bartolozzi**, Bodu, Březina, **Cancian, Casini**, Caspary, **Comi, De Mita**, Deß, Engel, Essayah, **Fidanza**, García-Margallo y Marfil, **Gardini**, Grzyb, Handzlik, Hibner, Hohlmeier, Jeggle, Jędrzejewska, Kaczmarek, Kalinowski, Kastler, Kozłowski, Lehne, Lisek, Lulling, Mann, Marcinkiewicz, **Mastella, Matera, Mauro**, Mayer, Mikolášik, Morkūnaitė-Mikulėnienė, Niebler, Nitras, Olbrycht, **Pallone**, Pirker, Posselt, Quisthoudt-Rowohl, Reul, **Rivellini**, Roithová, **Ronzulli**, Saryusz-Wolski, Saudargas, Schnellhardt, **Scurria**, Siekierski, **Silvestris**, Skrzydlewska, Sonik, Surján, Ulmer, Voss, Weisgerber, Wieland, Winkler Iuliu, Winkler Hermann, Zalewski, Zasada, Zeller, Zwiefka, Záborská, van Nistelrooij

40

**0**

**ALDE**:          Nicolai

**ECR**:             Elles, Foster, Girling, Nicholson, Strejček

**EFD**:             Agnew, Andreasen, Bufton, Imbrasas

**NI**:                 Hartong, Kovács, Morvai, Stassen, Zijlstra

**PPE**:             Andrikienė, **Angelilli, Antinoro, Iacolino**, Jazłowiecka, Jordan Cizelj, Klaß, Koch, Kolarska-Bobińska, Landsbergis, Liese, Luhan, Mato Adrover, **Muscardini**, Mészáros, Pack, Riquet, Schwab, **Tatarella**, Tőkés, Ungureanu, Łukacijewska

**S&D**:            **Costa**, Poc, **Susta**