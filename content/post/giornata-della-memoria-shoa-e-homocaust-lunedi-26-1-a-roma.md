---
title: 'GIORNATA DELLA MEMORIA: SHOA'' E HOMOCAUST LUNEDI'' 26-1 A ROMA'
date: Thu, 15 Jan 2009 16:37:34 +0000
draft: false
tags: [Comunicati stampa]
---

ARCIGAY-CIRCOLOMARIOMIELI-Di’GAYPROJECT-GAYNET-GAYLIB-CERTIDIRITTI

Presentano

**IL**  **GIALLO**  **E**  **IL**  **ROSA**

un’idea di **Vittorio Pavoncello**  

**Shoà e Homocaust**

due genetiche per uno sterminio  

**Lunedì 26 gennaio 2009 – Dalle 11 alle 24**

QUBE - via di Portonaccio 212 – Roma

![383_copertina.jpg](http://www.certidiritti.org/wp-content/uploads/2009/01/383_copertina.jpg "383_copertina.jpg")

Ebrei ed Omosessuali per ricordare insieme la Shoà e l’Homocaust: evento non stop organizzato dall’Associazione “Ecad” in occasione del GIORNO DELLA MEMORIA 2009.

“La memoria degli altri” – questo il titolo dell’iniziativa culturale ideata da Vittorio Pavoncello -  giunge qui alla sua terza edizione, dopo gli eventi degli anni precedenti dedicati rispettivamente ad  “Ebrei e Rom” (Auditorium Ara Pacis, 2006) ed “Ebrei e Disabili” (Auditorium Parco della Musica, 2007). Nella cornice multipiano del **Qube, lunedì 26 gennaio**, **dalle 11 alle 24**, la Shoà sarà così ricordata attraverso testimonianze parlate, discusse, filmate, raffigurate e interpretate che oltre a tracciare la storia passata, cercheranno di disegnare più civili modi di comprensione, analisi e dialogo per le persone e gruppi che da sempre hanno costituito fonte e stimolo per l’identità europea.  

> > > > > **Programma**

  
  

**10.45**

**_Al di qua del bene e del male_** di **Marco Belocchi**

Spettacolo itinerante elaborato su testi di Primo Levi, Wiesel, Kertesz, Frank, Bruck e Bassani

Interpreti: Daniela Di Bitonto, Maurizio Palladino, Eleonora Pariante, Alessandro Waldergan, Paola Surace, Valter Venturelli, Valentina Maselli e Marco Belocchi. _Compagnia Genta/Rosselli_

  
  

**11.00**

Incontri-Dibattito con **Gianfranco Goretti e Marco Reglia** sulle politiche discriminatorie nei confronti degli omosessuali durante la seconda guerra mondiale. Si parlerà anche di **_Genetica e Sessualità,_** a partire dalle riflessioni di Magnus Hirschfeld, una delle figure dominanti nell'ambito della difesa dei diritti politici degli omosessuali.

  

**11.30**

Presentazione Mostre **_Homocausto_**, a cura del Circolo Mario Mieli e di Arcigay, sull'approccio del fascismo all'omosessualità e sulla persecuzione delle donne lesbiche, con particolare attenzione anche alle discriminazioni effettuate nei regimi totalitari.

L’esposizione sarà affiancata dall’omaggio pittorico **_Arte e Shoà_** di 20 artisti sul tema della Shoà e dell’Homocaust, tra i quali si segnalano Eclario Barone, Franca Bernardi, Paola Casalino, Fiorella Corsi, Giorgio Fiume, Stefano Frasca, Vardi Kahana, Menashè Kadishman, Lughia, Rita Mele, Teresa Pollidori, Eliana Prosperi, Rosella Restante, Giovanni Liberatore, Teresa Mancini, Birgitt Shola Starp.

  

**11.30**

**_Silenzi e Parole_**

Perfomance di Mimo Rorocchi con letture di Gabriella Tupone, Vittorio Hannuna.

  

**12.00**

**_La scelta di Turing_** di **Vittorio Pavoncello**

Spettacolo su Alan Turing, il noto matematico che permise di decifrare i codici crittografati dei nazisti (ENIGMA) permettendo così agli alleati di vincere la guerra e in seguito, a guerra finita, portato al suicidio perché omosessuale.

Protagonisti **Bruno Maccallini, Toni Garrani, Arianna Lazzaro** e **Cristina Aubry**

Musiche di Enzo De Rosa,  Costumi di Toni Saracino, aiuto regia Stefano Frasca.

  

**13.15 -15.00**

Proiezione dei film

**_Grune Rose_** di **Dario Picciau**, film-racconto omaggio a Richard Grüne, artista e testimone dello sterminio degli omosessuali sotto il nazismo, prodotto da Visions e Arcigay Firenze "Il Giglio Rosa“ su soggetto e sceneggiatura di Roberto Malini;

**_Paragraf 175_**, opera diretta da **Rob Epstein** e **Jeffrey Friedman** su testo di Sharon Wood, narrata nella versione originale da **Rupert Everett**: uno straordinario documento tragico e commovente che ritrae 6 sopravissuti alla persecuzione nazista contro gli omosessuali.

  

**16.00**

Incontri con **Enrico Oliari, Angelo Pezzana, Rossana Praitano, Luigi Attenasio, Pupa Garriba, Imma Battaglia, Franco Grillini, Franco Siddi, Anna Foa**, interverrà lo scrittore israeliano Yossy Levy

**18.00**

**Premio** **_Teatro e Shoà_**, a cura del CeRSE Tor Vergata

Proclamazione dei vincitori della II edizione e consegna delle targhe della Presidenza della Repubblica.

  

**18.15**

**_La scelta di Turing_** di **Vittorio Pavoncello** (replica)

  

**19.15**

**_I sogni e le pietre_**

liberamente tratto da: **“Se questo è un uomo” di Primo Levi -  Come una rana d’inverno”** conversazione con tre donne sopravvissute ad Auschwitz di **Daniela Padoan** e “ Bent” di Martin Shermann, Bertolt  Brecth - con **Giuseppe Grisafi, Francesco Magali, Raffaella Mattioli, Gloria Pomardi, Leonardo Sbragia**, ideato e diretto da:  **Marco Mattolini –Gloria Pomardi**, drammaturgia:  Marco Mattolini, coreografie: Gloria Pomardi, costumi: **Francesca Linchi**, ambientazione scenica: **Fabrizio Russo**

  

**20.45**

_Il giallo e il rosa_, composizione del **Duo** **Neoklassic** formato da **Enzo De Rosa** (pianoforte) e **Kyung Mi Lee** (violoncello)

  

**21.00**

_Nudo,_ opera  di **Sylvano Bussotti** su testo di **Aldo Braibanti,** interpretata dal soprano **Monica Benvenuti**

  

**21.30**

**_Musica dell’Uomo di domani_ \-** antologia di musiche concentrazionarie scritte dal 1933 al 1945

**Francesco Lotoro** e **l’Ensemble Musica Judaica**

pianista **Francesco Lotoro**, soprano **Anna Maria Stella Pansini**, baritono **Angelo De Leonardis**, cantore **Paolo Candido**, chitarrista **Leonardo Gallucci** , Coro Diapente di Roma diretto da **Lucio Ivaldi**

**22.30**

_Eyn, tsvey...Dreidel_

**Marco Valabrega** (violino) e **Trio Dreidel** con **Ruth Ejzen** (voce)

Brani tratti dal loro ultimo lavoro di musica klezmer

_La S.V. è invitata a_

**La memoria degli altri**  

_Con il patrocinio di_

Parlamento Europeo

Ambasciata Britannica

Ambasciata della Repubblica Federale di Germania

Ambasciata di Israele

Unione delle Comunità Ebraiche Italiane

Federazione Nazionale della Stampa Italiana

Università di Tor Vergata

Centro di Cultura Ebraica  

_Con il contributo ed il patrocinio di_

Regione Lazio

Provincia di Roma

RAI Segretariato Sociale

Comunità Ebraica di Roma

Arcigay

Circolo Mario Mieli  

Main Sponsor  

QUBE

SIXTY  

Media partner  

Radio Rock

Radio Rock Italia

Fun week

Cherubini  

  

Per informazioni:

Ufficio Stampa _IL GIALLO E IL ROSA_: Elisabetta Castiglioni

Tel/Fax  06 3225044 -  Cell 328 4112014 [elisabetta@elisabettacastiglioni.com](mailto:elisabetta@elisabettacastiglioni.com)

[www.elisabettacastiglioni.com](http://www.elisabettacastiglioni.com/)

  

QUBE: _Ufficio Stampa_  Antonietta  Donatelli [a.donatelli@inwind.it](mailto:a.donatelli@inwind.it) 339.7660942

  

ECAD   Ebraismo Culture Arti Drammatiche

Via del Portico d’Ottavia 13 – Roma  Tel. 366 454565 -  Fax 1786029584

[ecad@live.it](mailto:ecad@live.it) \- [www.ecad.name](http://www.ecad.name/)