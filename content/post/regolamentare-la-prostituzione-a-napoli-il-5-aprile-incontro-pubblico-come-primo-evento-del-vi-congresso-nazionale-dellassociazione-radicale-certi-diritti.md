---
title: 'Regolamentare la prostituzione: a Napoli il 5 aprile incontro pubblico come primo evento del VI Congresso nazionale dell''associazione radicale Certi Diritti'
date: Thu, 04 Apr 2013 21:00:51 +0000
draft: false
tags: [Lavoro sessuale]
---

A seguire incontro di benvenuti aperto a tutti e tutte al Bubble Six.

Napoli, 4 aprile 2013

IL 5 aprile 2013 alle ore 16,30 al Maschio Angioino di Napoli incontro dibattito sul tema: “Regolamentare la prostituzione. Come ridare dignità e sicurezza alle persone che scelgono di prostituirsi. Gli interventi locali possibili senza dimenticare la lotta alla criminalità ed alla tratta degli esseri umani”

Partecipano:  
Pia Covre, Comitato per i diritti civili delle prostitute  
Kemal Ordek, Attivista europeo per i diritti dei e delle sex workers  
Andrea Morniroli, Associazione Dedalus  
Gigliola Toniollo, CGIL Nuovi Diritti  
Coordina Giuseppina Tommasielli, Assessore alle Pari Opportunità della Città di Napoli

Il dibattito pubblico italiano ha letteralmente cancellato la realtà dei lavoratori e delle lavoratrici del sesso in Italia, relegandola alla cronaca nera o a quella criminale. Da Napoli l'Associazione radicale certi diritti , in apertura del suo VI Congresso nazionale, ritorna su questo tema perchè non solo i mezzi di comunicazione ma anche la classe politica si sforzi di progettare interventi che abbiano al centro la sicurezza e la dignità delle persone senza falsi moralismi o preconcette ideologie. In linea con le richieste delle Associazioni delle persone che si prostituiscono.

Il Comune di Napoli, che ha sostenuto e concesso il suo patrocinio al Congresso dell'Associazione, sarà presente con l'Assessore comunale alle pari opportunità, Giuseppina Tommasielli, a testimonianza di un nuovo impegno su questi temi.

Dopo il dibattito alle ore 20: Benvenuti a Napoli! Incontro con aperitivo aperto a tutte e a tutti. Presso Bubble Six, P.zza S.Maria La Nova 22/23. Organizzato in collaborazione con le Associazioni radicali napoletane e il Coordinamento Campania Rainbow. Presenti i rappresentanti delle associazioni LGBTI italiane.

www.certidiritti.org