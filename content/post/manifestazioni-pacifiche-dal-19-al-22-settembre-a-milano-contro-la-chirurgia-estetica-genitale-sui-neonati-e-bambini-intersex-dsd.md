---
title: 'Manifestazioni pacifiche dal 19 al 22 settembre a Milano contro la chirurgia estetica genitale sui neonati e bambini intersex/dsd.'
date: Thu, 12 Sep 2013 06:18:31 +0000
draft: false
tags: [Intersex]
---

Comunicato stampa congiunto dell'Associazione Radicale Certi Diritti e di [Intersexioni](http://www.intersexioni.it/stop-agli-interventi-di-chirurgia-cosmetica-genitale-su-neonati-e-bambini/)

12 settembre 2013

Dal 19 al 22 settembre a Milano si svolgerà il IX Convegno di endocrinologia pediatrica durante il quale verranno trattate anche questioni relative ai neonati, bambine/i e adolescenti intersex o con "differenze nello sviluppo sessuale". Come denuncia l'associazione Zwischengeschlecht per i diritti umani delle persone intersex, si tratta di un convegno in cui continuano a venire proposte come valide leoperazioni di chirurgia estetica sui genitali degli infanti e dei bambini nati con forme di intersessualità.

E questo nonostante che tali operazioni siano ormai da decenni contestate da chi le ha subite, da una parte sempre più grande della classe medica e intellettuale a livello internazionale e, proprio quest'anno, anche dal Consiglio per gli Affari Esteri dell'Unione Europea e dal Rapporto delle Nazioni Unite sulla Tortura in cui, tra l'altro, agli Stati membri viene raccomandato di evitare interventi chirurgici e terapie farmacologiche non necessarie per la salute del bambino, usate invece per "normalizzare" le caratteristiche sessuali primarie e secondarie, con effetti irreversibili e traumatici.

Per la prima volta in Italia si terranno manifestazioni pubbliche di protesta contro queste pratiche di chirurgia estetica genitale non consensuale e contro un convegno che non vede tra i partecipanti né le persone intersex/dsd direttamente interessat* né voci mediche contrarie a tali pratiche.

Come firmatari di questo comunicato, riteniamo urgente che si apra una seria e approfondita riflessione scientifica e pubblica sulle pratiche di medicalizzazione e gestione delle diverse forme intersex/dsd a fronte di una scarsissima informazione sull'argomento in Italia e dell'applicazione, in troppi ospedali, di protocolli che prevedono l'assegnazione chirurgica di sesso precoce secondo teorie che hanno origine nel secolo scorso e di cui è stata ormai ampiamente dimostrata la mancanza di validità e la fallacia medico-scientifica.

Perciò facciamo nostre le proteste pacifiche che si terranno a Milano per tutta la durata del convegno e parteciperemo alla serata informativa di mercoledì 18 settembre, invitando associazioni, gruppi e singoli ad aderire e a partecipare numeros*.

Associazione ambrosia; collettivo intersexioni, Centro di Ricerca Politesse, laboratorio smaschieramenti, associazione radicale Certi Diritti.

**Per adesioni scrivere a info@intersexioni.it o a info@certidiritti.it**

**Programma delle manifestazioni**

**Mercoledì 18 alle 19.00 si terrà una serata informativa allo Zam (via Santacroce, 19, Milano).**

Queste le date e i luoghi in cui si terranno le proteste pacifiche dal 19 al 22 Settembre

**Giovedì 19.09.2013**

#1: 08:30-13:00 Milano Congressi, Gate 2

#2: 15:00-18:00 Università (La Statale), Sede centrale

**Venerdì 20.09.2013**

#3: 07:00-09:00 Milano Congressi, Gate 2

#4: 11:00-13:00 Università Vita e Salute San Raffaele

#5: 15:00-18:00 Politecnico

**Sabato 21.09.2013**

#6: 07:00-09:00 Milano Congressi, Gate 17

#7: 11:00-15:00 Ospedale San Raffaele

**Domenica 22.09.2013**

#8: 07:00-13:30 Milano Congressi, Gate 2