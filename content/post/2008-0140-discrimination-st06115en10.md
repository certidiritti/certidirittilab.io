---
title: '2008-0140-Discrimination-ST06115.EN10'
date: Fri, 05 Feb 2010 15:38:12 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 5 February 2010

Interinstitutional File:

2008/0140 (CNS)

6115/10

LIMITE

SOC 77

JAI 109

MI 40

  

  

  

  

  

NOTE

from :

The General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

6015/10 SOC 62 JAI 103 MI 33

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached a note from the Austrian delegation with a view to the meeting of the Working Party on Social Questions on 18 February 2010.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**AT proposal**

Changes in relation to doc. 5188/10 are indicated as follows: additions are in **bold** and deletions are marked \[…\]

Article 2(7)

a)             Notwithstanding paragraph 2, in the provision of financial services, **the Member States may provide that** proportionate differences in treatment \[...\] **on the grounds of age shall not be considered discrimination for the purposes of this Directive, if age** is a determining factor in the assessment of risk **and this assessmen**t **is** based on relevant actuarial principles **and** accurate statistical data \[...\].

**b)** **In the provision of insurance, the Member States may provide on an individual basis that proportionate differences in treatment on the grounds of disability shall not be considered discrimination for the purposes of this Directive, if the state of health of the person concerned is a determining and genuine factor in the assessment of risk and this assessment is based on relevant actuarial principles, accurate statistical data and medical knowledge.**

Explanatory Statement

The above wording takes into account the Presidency proposal in Doc. 5188/10 and aims to ensure coherence with Article 5(2) of Directive 2004/113/EC.

The AT proposal makes a distinction between unequal treatment based on age in the area of financial services in general, and unequal treatment based on disability in the area of insurance services alone. It is not necessary to extend the provision concerning proportionate differences of treatment on the grounds of disability (Article 2(7)(b) to cover financial services in general, as insurance for the safeguarding of credits is in any case covered by insurance services.

It is the AT delegation's firmly held view that differences of treatment based on disability should only be allowed if the state of health of the person concerned is a determining and genuine factor in the assessment of risk.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_