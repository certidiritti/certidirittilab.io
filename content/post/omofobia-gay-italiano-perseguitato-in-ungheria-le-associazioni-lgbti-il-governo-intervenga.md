---
title: 'Omofobia. Gay italiano perseguitato in Ungheria, le associazioni LGBTI "il Governo intervenga"'
date: Thu, 21 May 2015 16:28:47 +0000
draft: false
tags: [Europa]
---

[![76b5c25073124a08a2c66163f45089ed_18](http://www.certidiritti.org/wp-content/uploads/2015/05/76b5c25073124a08a2c66163f45089ed_18-300x168.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/05/76b5c25073124a08a2c66163f45089ed_18.jpg)Andrea Giuliano è un ragazzo italiano gay che vive a Budapest e da un anno è perseguitato dai gruppi neo nazisti ungheresi. Ha ricevuto diverse minacce, intimidazioni, ha subito raid di squadracce che sono andate sotto casa sua e sul posto di lavoro. Vive, dopo aver manifestato al Pride di Budapest impugnando una bandiera che sbeffeggiava uno di questi gruppi violenti, in un incubo, ha perso il lavoro, è ospitato in case diverse, è giustamente preoccupato e spaventato. A giugno dovrà subire un processo intentato dai suoi persecutori per “diffamazione”, mentre per la sua denuncia nei confronti dei gruppi neo nazisti il processo non è stato istruito. Le associazioni lgbt italiane chiedono che il Governo italiano intervenga urgentemente nei confronti di quello ungherese, affinché l’incolumità di Andrea sia garantita. In Ungheria il premier Viktor Orbàn si è distinto per le sue leggi e campagne xenofobe e razziste, l’Ue sta svolgendo diverse indagini sulla violazione dei diritti umani in quel paese, per tutto questo noi pensiamo che il nostro Governo debba agire in modo risoluto e chiaro. Ci attendiamo risposte tempestive ed efficaci e soprattutto una concreta solidarietà del nostro Paese nei confronti di un nostro cittadino in reale pericolo.

Agedo Arcigay ArciLesbica Associazione Radicale Certi Diritti Equality Italia Famiglie Arcobaleno Mit