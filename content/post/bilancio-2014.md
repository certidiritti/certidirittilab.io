---
title: 'Bilancio 2014'
date: Wed, 23 Nov 2011 22:28:39 +0000
draft: false
tags: [Politica]
---

**Stato Patrimoniale 2014 (€)**

**ATTIVITA’**

**PASSIVITA’**

Cassa Contanti

1.893,77

Risconti passivi

100,00

Carta di credito prepagata

223,80

**RETTIFICHE PASSIVE**

** 100,00**

**CASSA**

**2.117,57**

Avanzo cumulato

  9.071,75

c/c Bancario

4.390,71

**CAPITALE SOCIALE (NETTO)**

**  9.071,75**

c/c Postale

2.972,24

**UTILE D'ESERCIZIO**

**  4.037,62**

Paypal

828,85

**BANCHE C/C**

**8.191,80**

Credito vs Partito Radicale

2.800,00

Fornitori c/anticipi

100,00

**CREDITI DIVERSI**

**2.900,00**

**TOTALE ATTIVO**

**13.209,37**

**TOTALE PASSIVO**

** 13.209,37**

                  **Conto Economico 2014 (€)**

**RICAVI**

**10.649,32**

_Contributi_

  9.431,70

_Interessi attivi_

     17,61

_Progetti ILGA_

 1.050,00

**ENTRATE**

** 10.499,31**

_Abbuoni attivi_

 0,01

**ABBUONI ATTIVI**

** 0,01**

_Sopravvenienze Attive_

 150,00

**SOPRAVVENIENZE ATTIVE**

** 150,00**

 

**COSTI**

**6611,7**

_Servizi Telematici_

 84,88

_Stampati_

 46,13

_Cancelleria_

 88,50

_Postali e spedizioni_

 69,40

_Oneri Bancari ed Altro_

    293,17

_Commissioni su carte di credito_

 104,15

_Oneri su collaborazioni_

 4,10

**SERVIZI COMUNI**

**    690,33**

_Stampa e spedizione tessere_

 58,50

_Sito Web_

 567,38

**INFORMAZIONE**

** 625,88**

_Iscrizione ILGA_

 150,00

**CONTRIBUTI EROGATI**

** 150,00**

_Iniziative varie_

 2.001,99

_Progetto Ilga_

 1.711,24

_Iniziativa “Liberi matrimoni in libera Europa”_

 768,74

_Riunioni varie_

 231,80

**INIZIATIVE VARIE**

** 4.713,77**

_Prestazioni Congressuali_

 54,58

_Spese viaggi e soggiorni_

 221,14

**VII Congresso**

** 275,72**

_Spese viaggi e soggiorni_

 123,99

**VIII Congresso**

** 123,99**

_Abbuoni passivi_

 0,01

**ABBUONI PASSIVI**

** 0,01**

_Sopravvenienze passive_

 32,00

**Sopravvenienze passive**

** 32,00**

 

**UTILE D'ESERCIZIO**

**  4.037,62**