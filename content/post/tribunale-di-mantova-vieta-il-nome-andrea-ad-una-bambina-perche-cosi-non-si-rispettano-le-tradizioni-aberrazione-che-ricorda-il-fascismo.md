---
title: 'Tribunale di Mantova vieta il nome Andrea ad una bambina perchè così "non si rispettano le tradizioni": aberrazione che ricorda il fascismo!'
date: Fri, 30 Dec 2011 11:44:36 +0000
draft: false
tags: [Politica]
---

E tutte le donne che si chiamano così sono fuorilegge?

Roma, 30 gennaio 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Il Tribunale di Mantova ha sentenziato che una bambina non può chiamarsi Andrea , perché «il nome deve identificare in maniera chiara e corretta la sessualità». A parte il fatto che il giudice dimostra di avere le idee un po' confuse usando impropriamente il termine "sessualità" al posto di "genere". Se proprio non si voleva usare il termine più corretto si poteva almeno parlare di "sesso", ma la sessualità non centra proprio nulla.

Ma soprattutto stiamo proprio superando i limiti della decenza rispetto alle competenze che lo Stato si arroga ingerendo nella vita degli individui: adesso i genitori non possono nemmeno più scegliere il nome dei propri figli. E non si tiene nemmeno conto delle tante donne che si chiamano ‘Andrea’, divenute così fuorilegge perché ‘non si rispettano le tradizioni’.  Infatti il filo logico seguito dai giudici è il seguente: in Italia Andrea «è indiscutibilmente un nome da uomo», come prova «il riferimento alla tradizione del nostro Paese». Non solo quindi i genitori devono rispettare un rigidissimo binarismo tra i sessi nella scelta del nome, ma anche la cosiddetta tradizione del nostro Paese. Imboccando questa china piuttosto sdrucciolevole non ci si potrà stupire se tra un po' qualcuno invocherà il divieto di dare ai propri figli nomi stranieri o di imporre per legge solo nomi evangelici.

Già il fascismo invocava il ritorno "alla nostra tradizione" (Il costume da Il Popolo d'Italia del 10 luglio 1938) per giustificare la pratica assurda di italianizzare moltissimi cognomi non italiani, ma almeno si aveva la decenza di chiedere ai capifamiglia il consenso per il cambio di cognome.

Siamo di fronte al'ennesima riprova che purtroppo chi invoca la tradizione in genere la strumentalizza a danno dei diritti di scelta delle persone.  
  

**ISCRIVITI ALLA NEWSLETTER >**  
**www.certidiritti.it/newsletter/newsletter**