---
title: 'CONVEGNO A FIRENZE "TUTELA DELLA PERSONA E ORIENTAMENTO SESSUALE" - 13 GIUGNO ORE 14'
date: Wed, 21 May 2008 12:03:58 +0000
draft: false
tags: [Comunicati stampa]
---

Il Centro Europeo Studi sulla Discriminazione e Avvocatura per il diritti LGBT – Rete Lenford organizzano il Convegno: [“Tutela della persona e orientamento sessuale. Strumenti, tecniche e strategie contro la discriminazione”](index.php?option=com_docman&task=doc_download&gid=12&Itemid=79)  
Bologna, 13 giugno 2008 , h. 14:00 Facoltà di Giurisprudenza – Via Zamboni 22      Patrocinato dalla Regione Emilia-Romagna e dalla Facoltà di Giurisprudenza dell’Università di Bologna e attributivo di 5 crediti formativi per avvocati e patrocinatori, il convegno, attraverso le riflessioni di costituzionalisti (Andrea Morrone e Diletta Tega), esperti di diritti umani (Robert Wintemute), giuslavoristi (Laura Calafà), magistrati (Maria Acierno), e organismi di parità (significativa la presenza dell’Ufficio nazionale antidiscriminazioni razziali e dell’Ombudsman svedese contro la discriminazione basata sull’orientamento sessuale), costituirà l’occasione per continuare a riflettere sulle coordinate teoriche della discriminazione basata sull’orientamento sessuale, le sue manifestazioni concrete, le strategie di contrasto sulla base della normativa esistente. Costituirà inoltre momento di scambio di buone pratiche e di aggiornamento sulle novità giurisprudenziali e legislative, in particolare per quanto concerne le recenti proposte di legge contro l’istigazione a discriminare e la violenza omofoba.  
  
Programma dell’iniziativa: volantino allegato  
Registrazione: gratuita per il pubblico, 40€ per gli avvocati che intendano ottenere i crediti formativi.  
Informazioni e iscrizioni: www.cesd.eu   
  
Con preghiera di assicurare la massima diffusione.