---
title: 'Martedì 20 novembre T-DoR in Piazza Montecitorio per ricordare vittime transsessuali. Certi Diritti chiede incontro al governo'
date: Mon, 19 Nov 2012 15:59:57 +0000
draft: false
tags: [Transessualità]
---

Comunicato stampa dell’Associazione radicale Certi Diritti  
Roma, 19 novembre 2012  
Martedì 20 novembre si celebra in tutto il mondo (e in diverse città italiane) il Transgender Day of Remembrance per ricordare le vittime dell’odio transfobico.  
A Roma l’evento è promosso dall’Associazione Radicale Certi Diritti che sarà presente in Piazza Montecitorio a partire dalle ore 18,30 insieme ad associazioni e personalità che si battono per la promozione e difesa dei diritti civili.  
  
Durante la commemorazione  sarà accesa una candela per ogni persona transessuale uccisa dall’odio e dal pregiudizio. L’ultima vittima è stata trovata morta a Roma sabato 17 novembre.  
  
La celebrazione si svolgerà a Montecitorio per ricordare al Parlamento, dopo 30 anni dall’approvazione della prima legge in materia di rettificazione di attribuzione di sesso, la terribile situazione che ancora oggi sono costrette a vivere le persone transessuali; una situazione di violenza e maltrattamenti di natura transfobica che pone l’Italia ai primi posti al mondo, alla quale si aggiungono gravi difficoltà nel campo del lavoro e un atteggiamento di indifferenza e non conformità agli sviluppi in Europa sulla materia. Da diversi anni i Radicali hanno depositato una proposta di legge che chiede di non obbligare la persona transessuale a un intervento chirurgico di riassegnazione sessuale per avere il cambio di nome e genere nei documenti, così come già avviene in Spagna e Gran Bretagna.  
  
Questo ha spinto l’associazione radicale Certi Diritti, il Coordinamento Trans Sylvia Rivera e la CGIL Nuovi Diritti a chiedere già nei mesi scorsi un incontro ai ministri competenti per discutere un’agenda degli interventi più urgenti per la situazione delle persone transessuali in Italia.  
  
**[Il testo della lettera inviata ai ministri >>>](notizie/comunicati-stampa/item/download/18_1e5dc4cc63eeb5ef69d067fd2d6e9c65)**

  
[trans\_richiesta\_incontro_ministri.pdf](http://www.certidiritti.org/wp-content/uploads/2012/11/trans_richiesta_incontro_ministri.pdf)