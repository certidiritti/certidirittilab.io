---
title: 'ELEZIONI EUROPEE DEL 6-7 GIUGNO'
date: Sun, 24 May 2009 13:26:38 +0000
draft: false
tags: [Senza categoria]
---

Alle prossime elezioni europee  del 6-7 giugno invitiamo gli elettori della circoscrizione Centro (Lazio, Toscana, Umbria, Marche) a votare la lista Bonino e ad esprimere la preferenza per Sergio Rovasio per l'impegno profuso, con l'Associazione Certi diritti e con la sua storica militanza radicale, per l'affermazione della laicità dello Stato e la libertà delle persone.  

  

  

**Giuliano Federico**, Direttore di [www.gay.tv](http://www.gay.tv/);

**Tania Sachs**, Capo Ufficio Stampa Vasco Rossi;  
**Helena Velena**, Transgender;

**Claudio Coccoluto**, Dj;

**Daniele Nardini,** Direttore contenuti [www.gay.it](http://www.gay.it/); 

**Poppea,** drag queen – Roma;

**Pepa**, drag queen – Roma;

**Maria Gigliola Toniollo**, Responsabile nazionale Cgil Nuovi Diritti;

**Ottavio Marzocchi,** funzionario del Gruppo Alde al PE, responsabile  Europa Certi Diritti;

**Enzo Cucco**, Direttore Fondazione Sandro Penna – Torino; esponente del movimento lgbt;

**Clara Comelli**, Presidente Associazione Radicale Certi Diritti;

**Gaia Carretta**, giornalista;

**Monica Rosellini** ,Responsabile Associazione La Strega da Bruciare (difesa dei diritti civili di chi si prostituisce e di chi ne usufruisce);

**Felix Cossolo,** Direttore di GayClubbing;

**Aldo Brancacci,** Presidente Scuola Superiore di Filosofia, Università Roma Tor Vergata;

**Lucky Amato**, Coordinatore Associazione Radicale Certi Diritti – Roma;

**Franco Grillini,** già deputato, Direttore di GayNews

**Emilo Rez**, cantautore mix-gender;

**Sharon Nizza**, Studentessa – Gerusalemme;