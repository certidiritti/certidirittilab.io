---
title: 'Ordinanza anti-prostituzione di Roma miseramente fallita, ora che verrà proporaga il Sindaco fornisca i dati tenuti nascosti'
date: Wed, 18 Jan 2012 14:09:56 +0000
draft: false
tags: [Lavoro sessuale]
---

Alemanno risponda sulle spese sostenute per la campagna fallimentre anti-prostituzione. Su 20mila operazioni svolte dalle forze dell'ordine, pagate solo qualche decina di multe. Dichiari il suo fallimento e avvii altre politiche.

Roma, 18 gennaio 2012

Comunicato Stampa di Radicali Roma e Associazione Radicale Certi Diritti

E’ notizia di oggi che la Polizia di Stato (fonte il sindacato Silp-Cgil) su un totale di 13.800 contravvenzioni fatte alle prostitute di Roma in due anni, solo quattro di queste sono state pagate. Lo scorso dicembre la Polizia Municipale (fonte sindacato Fp Cgil)  aveva diffuso la notizia che su 21.000 operazioni anti-prostituzione a Roma, solo 600 di queste erano state pagate. Ci pare di capire che la tanto strombazzata ordinanza anti-prostituzione 242 del settembre 2008 è miseramente fallita, ma forse questo non lo capisce il Sindaco e la sua Giunta del Comune di Roma che l’hanno già prorogata tre volte e magari sono in procinto di prorogarla nei prossimi giorni visto che scade a fine gennaio 2012.

Chiediamo al Sindaco di Roma di fornire alla stampa anche i dati che finora ha tenuto nascosti  - che non ha ritenuto di trasmettere nemmeno quando ha risposto all’interrogazione popolare n. presentata dai Radicali di Roma nel giugno 20101 - relativi all’applicazione della delibera anti-prostituzione. Ad esempio non ha comunicato quante persone avevano pagato le multe tanto sbandierate ma soprattutto non ha fornito i dati sui costi delle operazioni (personale, mezzi, straordinari, propaganda, ecc). E’ evidente che se fornisse questi dati dovrebbe dichiarare fallita miseramente questa operazione repressiva-proibizionista che incrementa sempre più la marginalizzazione delle prostitute e che aumenta i casi di violenza, stupri e rapine che non vengono nemmeno più denunciati per paura.