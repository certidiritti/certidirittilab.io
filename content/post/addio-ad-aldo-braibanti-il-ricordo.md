---
title: 'Addio ad Aldo Braibanti. Il ricordo.'
date: Wed, 09 Apr 2014 15:26:06 +0000
draft: false
---

**A cura di [Radio Radicale](http://www.radioradicale.it "Radio Radicale")**

E’ morto Aldo Braibanti, scrittore, sceneggiatore e drammaturgo italiano. Intellettuale “a tutto tondo”, partigiano antifascista e poeta, nella sua vita si è occupato di arte, cinema, politica, teatro e letteratura, oltre ad essere un esperto mirmecologo.

**Il caso Braibanti, il reato di plagio, il Codice Rocco, i radicali**

 Giunto a Roma nel 1962, Braibanti continua la sua ricerca e per un anno e mezzo chiede e ottiene la collaborazione dell’amico Giovanni Sanfratello, un giovane di 23 anni. Il 12 ottobre del 1964 Ippolito Sanfratello, padre di Giovanni, presenta denuncia alla Procura di Roma contro Braibanti avendo trovato in modo non chiaro la collaborazione ambigua di un Pubblico Ministero: l’accusa è di “plagio”. Giovanni dopo 15 mesi di internamento fu dimesso, con una serie di clausole che andavano dal domicilio obbligatorio in casa dei genitori al divieto di leggere libri che avessero non meno di cento anni. Ma quelli che denunciavano un fantomatico plagio, non hanno dato nessun valore alle dichiarazioni spontanee di Giovanni. Il pubblico ministero arrivò a dichiarare che: “il giovane Sanfratello era un malato, e la sua malattia aveva un nome: Aldo Braibanti, signori della Corte! Quando appare lui tutto è buio”. Dopo un processo durato 4 anni, nel 1968, Aldo Braibanti viene condannato a nove anni, che in appello diventano sei. Scontò due anni di carcere e due gli furono condonati perché partigiano della resistenza. Fu il primo e l’unico ad essere condannato per plagio, reato introdotto dal fascismo col Codice Rocco. La condanna suscitò ampia eco in tutta Italia, a favore di Braibanti si mobilitarono Alberto Moravia, Umberto Eco, Pier Paolo Pasolini, Marco Bellocchio, Adolfo Gatti, Giuseppe Chiarie e numerosi altri intellettuali e uomini di cultura. Si mobilitarono anche i radicali di **Marco Pannella** si attivò per far conoscere il caso e venne processato per **[alcuni articoli](http://www.radioradicale.it/exagora/laffare-braibanti)** sul caso per reati d’opinione. Il reato di plagio è stato dichiarato incostituzionale dalla Corte costituzionale con la sentenza n. 96 dell’8 giugno 1981.

**Intervista di Roberto Spagnoli ad Aldo Braibanti, 2001**