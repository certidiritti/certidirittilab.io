---
title: 'Governo italiano contrario alla registrazione delle civil partnership presso i consolati inglesi'
date: Wed, 07 Sep 2011 07:02:04 +0000
draft: false
tags: [Diritto di Famiglia]
---

Il Civil Partnership Act prevede che le autorità del paese nel quale ci si propone di effettuare la registrazione dell'unione civile, non obbietti a tale registrazione. L'Italia chiarisca subito.  
Interrogazione dei deputati Radicali, prima firmataria Rita Bernardini

Roma, 6 settembre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Secondo alcune segnalazioni pervenuteci da coppie miste (uno/a dei due partner cittadino britannico) che hanno chiesto ai Consolati di Gran Bretagna in Italia di poter Registrare la propria unione in base alla legge sulle unioni civili, il Civil Partnership Act 2004 e il Civil Partnership Order del 2005, che regolamenta le unioni tra persone dello stesso sesso, risulterebbe che l’Italia opponga la sua contrarietà a tale richiesta. Difatti, il Civil Partnership Act del 2004 e il Civil Partnership Order del 2005 prevede alla sezione 210  (Registration at British consulates etc), Paragrafo 2, Comma c)  che  le autorità del paese, o del territorio nel quale ci si propone di effettuare la registrazione dell'unione civile, non obbietti a tale registrazione. Se tale notizia del comportamento del Governo italiano fosse confermata, si configurerebbe una vera e propria ingerenza dell’Italia negli affari interni della Gran Bretagna con l’obiettivo di discriminare le coppie omosessuali. L’Associazione Radicale Certi Diritti preannuncia sin d’ora che investirà la questione le istituzioni europee e il Consiglio d’Europa in quanto tale comportamento viola tutte le Direttive, Raccomandazioni e gli stessi Trattati di Nizza e di Lisbona.

I deputati Radicali, prima firmataria Rita Bernardini, Presidente dell’Associazione Radicale Certi Diritti, hanno oggi presentato una interrogazione urgente al Ministro degli Esteri, al Presidente del Consiglio e al Ministro per le Pari Opportunità, di seguito il testo integrale dell’Interrogazione parlamentare:  
  
  

Al Presidente del Consiglio dei Ministri

Al Ministro degli Esteri

Al Ministro per le Pari Opportunità

Interrogazione urgente a Risposta scritta

In Gran Bretagna dal 2004 è in vigore la legge sulle unioni civili, il Civil Partnership Act 2004 e il Civil Partnership Order del 2005 che regolamenta le unioni tra persone dello stesso sesso;

la parte 5 del Civil Partnership Act del 2004 (e successive integrazioni del Civil Partnership Order del 2005), che disciplina la registrazione e la dissoluzione di unioni civili in territorio estero, indica la possibilità della Registrazione presso i Consolati britannici all’estero; in particolare la sezione 210  (Registration at British consulates etc), Paragrafo 2, Comma c)  prevede che  le autorità del paese, o del territorio nel quale ci si propone di effettuare la registrazione dell'unione civile, non obbietti a tale registrazione;

L’Associazione Radicale Certi Diritti ha ricevuto diverse segnalazioni di coppie miste, composte da persone britanniche e italiane, alle quali è stato negata la possibilità di accedere alla Registrazione presso il Consolato britannico, perché le autorità italiane, interpellate in base alla sezione 210,  Paragrafo 2, comma c) del Civil Partnership Act, si sarebbero opposte;

Per sapere:

se e quante sono le richieste finora pervenute al Ministero degli Esteri e/o al Governo  da parte delle autorità consolari della Gran Bretagna relative alla possibilità di Registrazione dell’unione tra persone dello stesso sesso;

se esistono accordi scritti tra Gran Bretagna e Italia su questo argomento e in caso di risposta affermativa quali siano;

se e per quali motivi il Governo italiano ha espresso  un parere negativo alla Registrazione di un’unione civile che avviene negli uffici Consolari e quindi in  territorio britannico;

se non ritenga il Ministro che tale forma di opposizione, evidentemente prevista per paesi dove l’omosessualità è considerata un reato ed è perseguita penalmente, rientri tra quelle forme di grave discriminazione che a parole il Governo italiano dice di voler combattere;

se non ritenga che tale decisione rientri tra quei casi di ingerenza negli affari interni di altri paesi per i quali il nostro paese dovrebbe, per lo meno, e in mancanza di una normativa equivalente nell’ordinamento italiano,  mantenere una posizione di neutralità;

se non ritenga che la decisione di opporsi sia in netto contrasto con quanto indicato dalla Corte Costituzionale con la Sentenza n. 138/2010 sulla necessità di riconoscere alla coppie gay conviventi diritti come per le coppie eterosessuali e se tale posizione non sia in netto contrasto con i Trattati Europei di Nizza e di Lisbona sulla libera circolazione dei cittadini all’interno dell’Ue e sulla lotta alle discriminazioni;

Rita Bernardini

Marco Beltrandi

Maria Antonietta Farina Coscioni

Matteo Mecacci

Maurizio Turco

Elisabetta Zamparutti