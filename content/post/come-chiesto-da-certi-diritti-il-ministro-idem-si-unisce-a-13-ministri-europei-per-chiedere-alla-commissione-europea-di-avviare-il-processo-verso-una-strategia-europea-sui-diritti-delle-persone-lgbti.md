---
title: 'Come chiesto da Certi Diritti il Ministro Idem si unisce a 13 Ministri europei per chiedere alla Commissione Europea di avviare il processo verso una strategia europea sui diritti delle persone Lgbti'
date: Thu, 16 May 2013 16:55:39 +0000
draft: false
tags: [Europa]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti.

Roma, 16 maggio 2013

L'Associazione Radicale Certi Diritti esprime grande soddisfazione per la decisione del ministro per lo Sport e per le Pari opportunità, Josefa Idem, di accogliere la nostra richiesta e unirsi ai 13 ministri europei che, in occasione della Giornata mondiale contro l'omofobia, hanno chiesto a Viviane Reding, vicepresidente della Commissione Europea, di dare inizio al processo verso una Roadmap for Equality of LGBTI People 2013-2020.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: "Il 13 maggio abbiamo scritto Idem, Bonino e Moavero Milanesi affinché l'Italia si unisse ad altri 13 paesi membri dell'Unione Europea nel sostegno una strategia europea sui diritti delle persone LGBTI, che formi la base per un approccio coerente e di lungo termine al tema istituzionalizzandone gli sviluppi e il mainstreaming delle politiche europee e nella richiesta a Viviane Reding, vicepresidente della Commissione Europea, di avviare formalmente questo percorso. Ringraziamo il ministro Idem per aver accolto la nostra richiesta. Finalmente l'Italia si unisce al fronte dei paesi membri dell'Unione Europea più attenti ai diritti umani, anche di quelli delle persone LGBTI. Speriamo ora che la vicepresidente della Commissione Europea dimostri la medesima capacità di ascolto che ha dimostrato il ministro italiano per lo Sport e per le Pari opportunità".

Per un approfondimento:

**[Il testo della lettera inviata dall'Associazione Radicale Certi Diritti ai ministri Idem, Bonino e Moavero Milanesi](documenti/download/doc_download/65-lettera-ai-ministri-bonino-idem-e-moavero-milanesi-su-road-map-eu)**

[**Il testo firmato dal ministro Josefa Idem assieme ad altri 12 paesi dell'UE**](documenti/download/doc_download/66-130508-idaho)

[**La lettera di risposta del Ministro Bonino**](documenti/download/doc_download/77-lettera-ministro-bonino)