---
title: 'ANCHE ARCIGAY FIRENZE, DOPO BOLOGNA, SOSTIENE AFFERMAZIONE CIVILE'
date: Mon, 21 Dec 2009 07:24:38 +0000
draft: false
tags: [Comunicati stampa]
---

**NOZZE GAY/ARCIGAY FIRENZE SOSTIENE AFFERMAZIONE CIVILE A FIANCO DI CERTI DIRITTI E RETE LENFORD**

Arcigay Firenze "Il Giglio Rosa" accoglie con estrema soddisfazione la notizia secondo cui la Corte d'Appello del capoluogo toscano ha rimesso alla Corte Costituzionale la decisione sul ricorso di una coppia gay fiorentina al diniego delle pubblicazioni di matrimonio civile.

Per altro, è giunta stamani la conferma che anche il Tribunale di Ferrara ha ritenuto contrario alla Costituzione il non consentire alle coppie di persone dello stesso sesso di contrarre matrimonio. "La nostra Associazione" commentano Francesco Piomboni e Matteo Pegoraro, esponenti de "Il Giglio Rosa" e prima coppia in Italia a presentare nel marzo del 2007 la richiesta di pubblicazioni di matrimonio al Comune di Firenze, avviando un procedimento giudiziale per vedersi riconosciuto il diritto alle nozze, "è a fianco dell'Associazione Radicale Certi Diritti e di Rete Lenford e sostiene con convinzione tutte le coppie che hanno deciso di mettere in gioco se stesse per una battaglia di civiltà. Ringraziamo in particolare i giudici della Corte d'Appello fiorentina per l'importante sentenza che riconosce come fondamentale il diritto, anche per due persone dello stesso sesso, a contrarre matrimonio, e soprattutto sosteniamo Massimo ed Emanuele, che testimoniano con il loro amore quest'importante risultato e hanno accolto con determinazione e coraggio l'appello che, come coppia, abbiamo lanciato assieme a Certi Diritti e Rete Lenford già dal 2007".

Per ulteriori informazioni:

Arcigay Firenze

340 8135204

[www.arcigayfirenze.it](http://www.arcigayfirenze.it/) :: [circolo@arcigayfirenze.it](mailto:circolo@arcigayfirenze.it)