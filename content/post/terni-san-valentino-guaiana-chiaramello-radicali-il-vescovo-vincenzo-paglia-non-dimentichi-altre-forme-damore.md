---
title: 'Terni, San Valentino. Guaiana/Chiaramello (Radicali): il Vescovo Vincenzo Paglia non dimentichi altre forme d''amore'
date: Mon, 13 Feb 2012 17:57:52 +0000
draft: false
tags: [Politica]
---

Martedì 14 febbraio alle 16 nella sala della Vaccara all’interno del palazzo dei Priori di Perugia, ospite del circolo Arcigay-Omphalos, il segretario di Certi Diritti Yuri Guaiana presenterà il libro 'Dal cuore delle coppie al cuore del diritto'.

In occasione dei festeggiamenti per il 14 febbraio, una sessantina di coppie si sono date appuntamento, da tutta Italia, alla basilica di San Valentino di Terni per la «festa della promessa», momento in cui riceveranno la benedizione del Vescovo Vincenzo Paglia il quale, per l’avvenimento in questione, farà un elogio dell'amore al fine di difenderlo da quello che ritiene essere il suo mortale nemico: "una concezione individualistica della vita".

**L'Associazione Radicale Certi Diritti e l’Associazione Radicale Perugia.org ricordano al Vescovo erede di San Valentino che l'amore di cui parla è solo uno dei tanti modi di declinarlo.** Modi che hanno invece come principale avversaria l'indifferenza del Parlamento repubblicano che da quasi due anni si rifiuta di seguire le indicazioni della Corte Costituzionale per la quale "l'unione omosessuale \[ha\] il diritto fondamentale di vivere liberamente una condizione di coppia, ottenendone \[...\] il riconoscimento giuridico".

Per rivendicare con forza l'eguale diritto delle coppie dello stesso sesso di scambiarsi la promessa di matrimonio civile Yuri Guaiana, il segretario dell'Associazione Radicale Certi Diritti, scenderà nella regione che ha dato i natali al Santo dell'amore insieme alle 60 coppie che hanno voluto scambiarsi la «promessa» davanti al vescovo Vincenzo Paglia.

**Martedì 14 febbraio alle 16 nella sala della Vaccara all’interno del palazzo dei Priori di Perugia, ospite del circolo Arcigay-Omphalos, presenterà il libro 'Dal cuore delle coppie al cuore del diritto'**, da lui curato, che racconta il percorso umano, politico e legale della battaglia di Affermazione Civile, campagna finalizzata al superamento delle diseguaglianze per l’accesso all’istituto del matrimonio civile delle coppie dello stesso sesso.