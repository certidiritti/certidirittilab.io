---
title: 'Radical Party'
date: Tue, 02 Oct 2012 12:25:09 +0000
draft: false
tags: [Politica]
---

Il [**Partito Radicale Nonviolento transnazionale e transpartito**](http://www.radicalparty.org) è una associazione di cittadini, parlamentari, membri di governo, di varie appartenenze nazionali, politiche e partitiche, che intende raggiungere, con i metodi della nonviolenza gandhiana, della disobbedienza civile e della democrazia, alcuni concreti obiettivi miranti alla creazione di un effettivo diritto internazionale, al rispetto dei diritti della persona e all'affermazione di democrazia e libertà ovunque nel mondo.

Dal 1995 il Partito Radicale è registrato come **Organizzazione non governativa** (Ong) con statuto consultivo di categoria generale, presso il Consiglio economico e sociale (Ecosoc) dell'**ONU**.  
Il partito non partecipa con proprie liste a elezioni nazionali, regionali o locali.

L'associazione radicale Certi Diritti è soggetto costituente del Partito Radicale Nonviolento transnazionale e transpartito dal **[dicembre 2011](http://www.certidiritti.org/2011/12/11/lassociazione-radicale-certi-diritti-diventa-soggetto-politico-costituente-del-partito-radicale-nonviolento-transnazionale-e-transpartito/)**.  
  
Gli altri soggetti costituenti sono:

**[Radicali Italiani](http://www.radicali.it/)**

[**Associazione Luca Coscioni per la libertà di ricerca scientifica**](http://www.nessunotocchicaino.it/)

[**Nessuno Tocchi Caino**](http://www.nessunotocchicaino.it/)

[**Non c'è pace senza giustizia**](http://www.npwj.org/)

[**Era - Esperanto Radikala Asocio**](http://lnx.internacialingvo.org/eo/index.php?newlang=ita)

[**Anticlericale.net**](http://www.anticlericale.net/)

**Lega Internazionale Antiproibizionista**