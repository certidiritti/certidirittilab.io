---
title: 'Ragazzo di 24 anni lasciato senza assistenza medica per curare la sifilide. Il farmaco non si trova in Italia ma in Vaticano!'
date: Wed, 18 Apr 2012 11:39:20 +0000
draft: false
tags: [Salute sessuale]
---

Esporto all'Unar e alla commissione parlamentare di inchiesta sul servizio sanitario nazionale. Annunciate due diverse interrogazioni parlamentari e alla regione Lazio.

Roma, 18 aprile 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

L’Associazione Radicale Certi Diritti invierà un esposto  all’Unar e alla Commissione parlamentare di inchiesta sull'efficacia e l'efficienza del Servizio Sanitario Nazionale  su una gravissima vicenda avvenuta a Roma i primi giorni di marzo 2012.

Un ragazzo di 24 anni. R.F., dopo essersi recato all’Ospedale Policlinico Umberto I° per un controllo medico, è risultato positivo al test della sifilide. Il medico dell’Ospedale che si occupa di malattie infettive sessualmente trasmissibili gli  prescrive una cura medica a base di penicillina per la cura della sifilide scoprendo però che lo Stato italiano da oltre un anno ha ritirato da tempo tale medicinale e che non è nemmeno mutuabile.  

Il ragazzo accompagnato da Massimo Frana dell’Associazione Radicale Certi Diritti si è visto così costretto a recarsi presso la Farmacia dello Stato della Città del Vaticano dove il farmaco è venduto in due varianti, una a base di anestetico al costo di 25 Euro a fiala per un totale di 750 Euro e l’altra, senza anestetico, al costo di 6 Euro a fiala per un totale di 180 Euro, senza alcun tipo di esenzione.

Il costo del farmaco in Italia, fino a un anno fa, era di circa 2 Euro a fiala per costo complessivo di circa 60 Euro.

Successivamente il ragazzo, accompagnato da Massimo Frana, si è recato al Pronto Soccorso dell’Ospedale San Giovanni di Roma per iniziare la cura a base di iniezioni e da lì viene mandato alla Croce Rossa Italiana dove gli rispondono che l’ambulatorio non esiste più. Dopo aver cercato di contattare la Guardia Medica tramite l’Ospedale San Giovanni la risposta data è che “per questo genere di cose occorre trovare un infermiere privato”. Il ragazzo viene quindi accompagnato all’Ospedale Policlinico di Roma dove grazie al volontario aiuto  di un’infermiera viene attivata la cura medica necessaria a base di iniezioni.   

I parlamentari Radicali e i Consiglieri Regionali Radicali hanno preannunciato il deposito in tempi brevissimi di due interrogazioni alla Camera dei deputati e al Consiglio Regionale del Lazio.