---
title: 'Gemellaggio tra Milano e San Pietroburgo: Consiglio comunale approva mozione che ne chiede la sospensione'
date: Fri, 23 Nov 2012 06:58:31 +0000
draft: false
tags: [Russia]
---

Speriamo che l’esempio meneghino sia seguito anche da Venezia, gemellata dal 2006, e da Torino, che ha sciaguratamente siglato un accordo bilaterale di collaborazione con l’ex capitale zarista solo una settimana fa. Il testo della mozione, anche in inglese.

Comunicato stampa dell'Associazione Radicale Certi Diritti

Milano, 23 novembre 2012

A distanza di otto mesi dalla presentazione, finalmente il Consiglio Comunale di Milano ha messo in discussione la mozione, presentata dal radicale Marco Cappato e firmata da tutti i capi gruppo della maggioranza, per la sospensione degli effetti del gemellaggio tra Milano e San Pietroburgo fino a quando non sarà revocata la legge contro la cosiddetta "propaganda del l'omosessualità" approvata dall'ex capitale zarista il 29 febbraio scorso.

Il Consiglio comunale ha approvato la mozione a maggioranza: il presidente Rizzo e il consigliere Calise (Movimento 5 Stelle) si sono astenuti, la Lega ha votato contro e il PDL ha abbandonato l’aula in dissenso sul metodo.

Da oggi, quindi, il Sindaco, la Giunta e tutte le partecipate non dovranno più organizzare iniziative nell’ambito del patto di gemellaggio che, dal 1967, lega Milano con San Pietroburgo. L’embargo varrà fino a quando non sarà abrogata la legge che di fatto criminalizza qualunque attività o informazione relativa alle persone LGBTI (Lesbiche, Gay, Bisessuali, Transessuali e Intersessuali) e alle relazioni tra persone dello stesso sesso, in patente violazione delle libertà di espressione e associazione, nonché degli impegni presi dalla Russia ratificando la Convenzione europea per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: ≪Con il voto di ieri, Milano dà un segno tangibile a favore dei diritti umani e delle libertà fondamentali e si dimostra all’altezza della sua tradizione antifascista, democratica e liberale. Ringrazio di cuore Moni Ovadia, Gad Lerner, Alessandro Cecchi Paone, Lella Costa, la compagnia dell’Elfo, gli altri artisti e intellettuali milanesi che hanno dato voce e volto alla campagna organizzata dall’Associazione Radicale Certi Diritti, i 1256 cittadini milanesi che l’hanno sostenuta nonché il Consiglio comunale. L’attivista di San Pietroburgo Olga Lenkova ci ha ricordato, nel video di ringraziamento inviatoci, che “i diritti umani possono solo esistere globalmente, ed è globalmente che dobbiamo lottare per loro”. Oggi Milano ha portato il suo contributo a questa lotta, aggiungendo la sua condanna a quella già espressa dal Parlamento Europeo e del Consiglio d'Europa e dimostrando di comprendere che questo non è un problema degli omosessuali ma, come ha detto Moni Ovadia, "un problema di tutti noi, un problema di riconoscimento della dignità dell'essere umano". Speriamo allora che quello che il grande artista ebreo ha definito un “odioso, ripugnate provvedimento di stampo nazifascista” venga presto rimosso e che quella che è una ferita inaccettabile alla capitale culturale russa possa così essere sanata. È proprio perché riconosciamo il valore del legame tra i cittadini delle due capitali europee che abbiamo chiesto la sospensione e non la revoca del gemellaggio: un segnale forte, ma rispettoso. Speriamo che l’esempio meneghino sia seguito anche da Venezia, gemellata dal 2006, e da Torino, che ha sciaguratamente siglato un accordo bilaterale di collaborazione con l’ex capitale zarista solo una settimana fa≫.

**[IL TESTO DELLA MOZIONE APPROVATA >](notizie/comunicati-stampa/item/download/19_9ed80eb7a23630555bd328289eab0711)**

[**IL TESTO DELLA MOZIONE IN INGLESE >**](notizie/comunicati-stampa/item/download/20_2cccff9254b03ba86875783c74671079)

**[SOSTIENI L'ASSOCIAZIONE RADICALE CERTI DIRITTI >](partecipa/iscriviti)**

  
[Testo\_Mozione\_IM-99\_gemellaggio\_S-Pietroburgo\_EMENDATA\_CC_22-11-2012.pdf](http://www.certidiritti.org/wp-content/uploads/2012/11/Testo_Mozione_IM-99_gemellaggio_S-Pietroburgo_EMENDATA_CC_22-11-2012.pdf)  
[certi\_diritti\_amended\_text\_\_nov\_27\_2012.pdf](http://www.certidiritti.org/wp-content/uploads/2012/11/certi_diritti_amended_text__nov_27_2012.pdf)