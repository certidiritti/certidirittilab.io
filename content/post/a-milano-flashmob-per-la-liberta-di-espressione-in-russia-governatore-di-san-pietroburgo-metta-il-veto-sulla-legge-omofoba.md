---
title: 'A Milano flashmob per la libertà di espressione in Russia: Governatore di San Pietroburgo metta il veto sulla legge omofoba'
date: Mon, 27 Feb 2012 21:23:58 +0000
draft: false
tags: [Russia]
---

\*\*\*Il Flashmob inizia alle 17:00 di martedì 28 febbraio in Piazzale Segesta\*\*\*

L'Associazione Radicale Certi Diritti e l'Associazione Radicale Enzo Tortora organizzano un flashmob a Milano in Piazzale Segesta (vicino al Consolato Generale della Federazione Russa) per chiedere al Governatore di San Pietroburgo di mettere il veto sulla legge omofoba che vuole limitare la libertà d’espressione nella regione e per dire agli italiani di non visitare più la città se la legge dovesse essere approvata. L’iniziativa è fatta in partnership con l'associazione AllOut.org, che ha lanciato un boicottaggio globale di San Pietroburgo. Hanno aderito anche l'Associazione AnnaViva e il Centro d'Iniziativa Gay - Arcigay Milano.

  
\*\*\*Il Flashmob inizia alle 17:00 di martedì 28 febbraio in Piazzale Segesta\*\*\*

Facebook Event Page: [https://www.facebook.com/events/313267152053799/](https://www.facebook.com/events/313267152053799/)

  
Il 29 febbraio la città di San Pietroburgo potrebbe approvare in terza e ultima lettura una legge che renderebbe illegale scrivere un libro, pubblicare un articolo o parlare in pubblico di omosessualità. In una recente audizione pubblica organizzata dai sostenitori della legge dei cosiddetti “esperti” hanno chiesto dei trattamenti sanitari obbligatori o l’isolamento degli omosessuali, mentre le persone LGBTI (Lesbiche, Gay, Bisessuali, Transessuali e Intersessuali) sono stati umiliati da sedicenti “sessuologi” che li hanno descritti come “subumani” tra gli applausi scroscianti dell’Assemblea.

Questa legge è chiaramente volta a criminalizzare qualunque attività o informazione relativa alle persone LGBTI e alle relazioni tra persone dello stesso sesso, in patente violazione delle libertà di espressione e associazione nonché degli impegni presi dalla Russia ratificando la Convenzione europea per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali.

Nonostante la condanna dell’UE ([http://www.certidiritti.it/parlamento-europeo-approva-risoluzione-di-condanna-nei-confronti-della-russia-per-le-leggi-omofobe](parlamento-europeo-approva-risoluzione-di-condanna-nei-confronti-della-russia-per-le-leggi-omofobe)) e anche dell’Italia ([http://www.certidiritti.it/ministro-degli-esteri-italiano-risponde-a-certi-diritti-e-senatore-perduca-su-gravi-fatti-omofobi-della-regione-di-san-pietroburgo-italia-impegnata-contro-omofobia](ministro-degli-esteri-italiano-risponde-a-certi-diritti-e-senatore-perduca-su-gravi-fatti-omofobi-della-regione-di-san-pietroburgo-italia-impegnata-contro-omofobia)) la legge sarà probabilmente approvata. La terza e ultima votazione sul provvedimento avverrà il 29 febbraio e aprirà una finestra di 10 giorni per il Governatore di San Pietroburgo Petrenko di porre il veto.

Alla vigilia del voto finale i militanti dell’Associazione Radicale Certi Diritti e dell’Associazione Radicale Enzo Tortora di Milano si uniranno a una crescente rete internazionale di flashmob davanti ad Ambasciate e Consolati russi coordinata dall’Associazine AllOut.org (manifestazioni si terranno a Parigi, New York, Buenos Aires, Rio, Berlino, Milano, Anversa e molte altre città in solidarietà con i gruppi di San Pietroburgo).

Indossando dei bavagli per simboleggiare l’intento censorio della legge, ci ritroveremo con una cartolina gigante da consegnare “alla Russia con amore” per mostrare le 300.000 persone in tutto il mondo che hanno protestato contro la proposta di legge. Altri cartelli saranno portati per mandare al governatore di San Pietroburgo Petrenko un messaggio forte e chiaro: “E’ in gioco la reputazione di San Pietroburgo come centro culturale e turistico. Non andarci più. Metti il veto”.

**iscriviti alla newsletter >[  
](newsletter/newsletter)[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**