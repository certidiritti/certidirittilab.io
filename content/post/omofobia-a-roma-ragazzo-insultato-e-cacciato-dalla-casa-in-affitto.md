---
title: 'OMOFOBIA A ROMA: RAGAZZO INSULTATO E CACCIATO DALLA CASA IN AFFITTO'
date: Wed, 30 Nov -0001 00:00:00 +0000
draft: false
tags: [Comunicati stampa]
---

 Roma, 13 giugno 2010

**OMOFOBIA A ROMA: ALTRO CASO CONTRO UN RAGAZZO GAY, CACCIATO DALLA CASA IN AFFITTO CON INSULTI E OFFESE. CHIEDIAMO ALLA MAGISTRATURA E AL SINDACO DI INTERVENIRE.**

**Comunicato Stampa dell’Associazione Radicale Certi Diritti:**

“Il 10 giugno scorso, Emilio Rez, un ragazzo gay di 25 anni, cantautore, mentre rientrava nel suo appartamento in affitto nella zona del Pigneto, si è accorto che le serrature della sua casa erano state cambiate. Poco dopo si è accorto che in fondo alle scale v’erano 6 sacchi neri contenenti suoi effetti personali.

Emilio Rez, che l'estate scorsa era  stato aggredito per omofobia, nei giorni scorsi era stato gravemente minacciato e insultato dai membri della famiglia proprietaria dell’appartamento, con frasi del tipo: “Sei un frocio di merda, sei un farabutto… con noi caschi male, ecc”.

E’ stata immediatamente sporta una denuncia ai Carabinieri per i reati di violazione di domicilio, appropriazione indebita e minacce. Allo stato non si conoscono le evoluzioni delle indagini, ci auguriamo che la magistratura intervenga urgentemente per difendere e tutelare una persona così offesa e maltrattata.

L'Associazione Radicale Certi Diritti si  augura  che il Sindaco di Roma intervenga per aiutare e sostenere Emilio Rez che ora, dopo 3 gioni, non può ancora rientrare a casa”.

Ufficio Stampa: 06-68979250