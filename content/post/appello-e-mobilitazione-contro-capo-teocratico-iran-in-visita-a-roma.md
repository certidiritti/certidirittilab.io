---
title: 'APPELLO E MOBILITAZIONE CONTRO CAPO TEOCRATICO IRAN IN VISITA A ROMA'
date: Sat, 31 May 2008 15:00:53 +0000
draft: false
tags: [Comunicati stampa]
---

In occasione del vertice mondiale della Fao sulla fame nel mondo, che si terrà a Roma dal 3 al 5 giugno, sarà presente ai lavori il Presidente della teocrazia fondamentalista iraniana, Ahmadinejad. Per questo si è costituito in queste ore un Comitato composto da verie Associazioni  (alcune sono indicate in fondo a questo messaggio) che hanno elaborato il testo dell'Appello 'Abbiamo fame di Libertà'. Anche l'Associazione Radicale Certi Diritti aderisce e partecipa alle varie inizaitive dedicandole alla memoria di Makwan. Il comitato ha deciso di partecipare, tra le altre iniziative, alla manifestazione promossa dal quotidiano Il Riformista, a Roma, il giorno martedì 3 giugno alle ore 20 in Piazza del Campidoglio alla quale ti invitiamo a partecipare.

La presenza del Presidente della Repubblica islamica dell’Iran, Ahmadinejad, proprio nell'ambito del vertice FAO che affronta il dramma della fame nel mondo, simboleggia la devastante contraddizione tra la violenza dei Governi totalitari e i loro proclami demagogici a favore dei popoli imponendoci inderogabilmente di denunciare la sistematica violazione dei diritti umani dei cittadini iraniani. 

Da quasi trent'anni la politica dei governi di Teheran nega i diritti delle minoranze religiose, delle donne, degli omosessuali, delle minoranze etniche, delle associazioni studentesche come di chiunque non condivida le politiche del regime. Costantemente sono negati i diritti cardine della democrazia, si impedisce la libertà d’espressione e nessuno spazio è concesso alla libertà di stampa. In Iran si susseguono esecuzioni capitali nei confronti di dissidenti politici, oppositori delle repressioni, studenti, giovani omosessuali.  

 Questa politica repressiva e la retorica dell’odio del Presidente Ahmadinejad, che proclama la volontà di distruggere lo Stato di Israele  e insiste nel negare la tragedia della Shoa, dovrebbero suggerire alla Comunità internazionale un diverso profilo nei rapporti diplomatici con l'Iran. 

Il Governo iraniano  provoca ed alimenta l’instabilità dell’intera Regione mediorientale,  sostiene ed arma  i gruppi terroristici,  e non ha ancora chiarito alla comunità internazionale gli obiettivi del suo programma nucleare.

Nel momento in cui il mondo si prepara a parlare di sicurezza e di risorse alimentari, dobbiamo avere la consapevolezza che la democrazia (civile, sociale, politica ed economica) è la soluzione alla tragedia della fame.

   **Comitato  ABBIAMO FAME  DI LIBERTA' - Partito Radicale Nonviolento, Transnazionale e Transpartito**

\- Gruppo EveryOne;

\- Associazione Radicale Certi Diritti;

\- Bene' Berith – Giovani

\- Appuntamento a Gerusalemme

\- Associazione romana Amici di Israele

\- Acmid, Associazione Marocchine residenti in Italia;

\- Associazione Luca Coscioni

\- Radicali Italiani

\- Associazione rifugiati politici – Italia

\- Associazione rifugiati politici iraniani

\- Non c'è Pace Senza Giustizia

\- Modavi, Movimento e organizzazioni di volontariato italiane <!\-\- D(\["mb","u003c/fontu003eu003c/spanu003eu003c/bu003eu003c/pu003enu003cp styleu003d"margin:0cm 0cm 0pt"u003eu003cbu003eu003cspan styleu003d"font-size:14pt"u003eu003cfont faceu003d"Times New Roman"u003e- Città solidaleu003c/fontu003eu003c/spanu003eu003c/bu003eu003c/pu003enu003cp styleu003d"margin:0cm 0cm 0pt"u003eu003cbu003eu003cspan styleu003d"font-size:14pt"u003eu003cfont faceu003d"Times New Roman"u003e- Università Cerco Lavorou003c/fontu003eu003c/spanu003eu003c/bu003eu003c/pu003enu003cdiv styleu003d"margin:0cm 0cm 0pt"u003eu003cfont faceu003d"Times New Roman"u003e u003c/fontu003eu003c/divu003enu003cdiv styleu003d"margin:0cm 0cm 0pt"u003eu003cfont faceu003d"Times New Roman"u003eu003c/fontu003e u003c/divu003enu003cdiv styleu003d"margin:0cm 0cm 0pt"u003eu003cfont faceu003d"Times New Roman"u003eu003c/fontu003e u003c/divu003enu003cdiv styleu003d"margin:0cm 0cm 0pt"u003e u003c/divu003enu003cdiv styleu003d"margin:0cm 0cm 0pt"u003eu003cfont faceu003d"Times New Roman"u003eTESTO DELLu0026#39;APPELLO u0026quot;ABBIAMO FAME DI LIBERTAu0026#39;u0026quot;u003c/fontu003eu003c/divu003enu003cp styleu003d"margin:0cm 0cm 0pt;text-align:justify"u003eu003cfont faceu003d"Times New Roman"u003e u003c/fontu003eu003c/pu003enu003cp styleu003d"margin:0cm 0cm 0pt;text-align:justify"u003eu003cfont faceu003d"Times New Roman"u003e u003c/fontu003eu003c/pu003enu003cp styleu003d"margin:0cm 0cm 0pt;text-align:justify"u003eu003cfont faceu003d"Times New Roman"u003eu003c/fontu003eu003c/pu003enu003cp styleu003d"margin:0cm 0cm 0pt;text-align:justify"u003eu003cfont faceu003d"Times New Roman"u003e u003c/fontu003eu003c/pu003enu003cp styleu003d"margin:0cm 0cm 0pt;text-align:center" alignu003d"center"u003eu003cbu003eu003cuu003eu003cspan styleu003d"text-decoration:none"u003eu003cfont faceu003d"Times New Roman"u003e u003c/fontu003eu003c/spanu003eu003c/uu003eu003c/bu003eu003cspan styleu003d"font-size:14pt"u003eu003cfont faceu003d"Times New Roman"u003eLa presenza del Presidente della Repubblica islamica dellu0026#39;Iran, Ahmadinejad, proprio nellu0026#39;ambito del vertice FAO che affronta il dramma della fame nel mondo, simboleggia la devastante contraddizione tra la violenza dei Governi u003cspan styleu003d"color:blue"u003etotalitariu003c/spanu003e e i loro proclami demagogici a favore dei popoli imponendoci inderogabilmente di u003cspan styleu003d"color:blue"u003edenunciareu003c/spanu003e la sistematica violazione dei diritti umani dei cittadini iraniani. u003c/fontu003eu003c/spanu003eu003c/pu003ennu003cp styleu003d"margin:0cm 0cm 0pt;text-align:justify"u003eu003cspan styleu003d"font-size:14pt"u003eu003cfont faceu003d"Times New Roman"u003e u003c/fontu003eu003c/spanu003eu003c/pu003enu003cp styleu003d"margin:0cm 0cm 0pt;text-align:justify"u003eu003cspan styleu003d"font-size:14pt"u003eu003cfont faceu003d"Times New Roman"u003eDa quasi trentu0026#39;anni la politica dei governi di Teheran nega i diritti delle minoranze religiose, delle donne, degli omosessuali, delle minoranze etniche, delle associazioni studentesche come di chiunque non condivida le politiche del regime. Costantemente sono negati i diritti cardine della democrazia, si impedisce la libertà du0026#39;espressione e nessuno spazio è concesso alla libertà di stampa. In Iran si susseguono esecuzioni capitali nei confronti di dissidenti politici, oppositori delle repressioni, studenti, giovani omosessuali. ",1\] ); //-->

\- Città solidale

\- Università Cerco Lavoro