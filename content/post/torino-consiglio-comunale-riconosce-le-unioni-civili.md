---
title: 'TORINO: CONSIGLIO COMUNALE RICONOSCE LE UNIONI CIVILI'
date: Wed, 30 Jun 2010 12:17:08 +0000
draft: false
tags: [affitti, aglietta, certi diritti, Comunicati stampa, conviventi, discriminazione, enzo cucco, FAMIGLIA, GAY, LAICITA', RADICALI, saraceno, torino, unioni civili, vattimo]
---

Lunedì 28 giugno il **Consiglio comunale di Torino** ha stabilito che **le unioni civili,** nelle materie e nei settori di competenza dell'amministrazione comunale, **non devono essere discriminate**

Dichiarazione di _Enzo Cucco_ dell'**Associazione Radicale Certi Diritti** e di _Jolanda Casigliani e Stefano Mossino_ dell'**Associazione Radicale Satyagraha**.

 "Tre anni fa, l'Associazione Radicale Satyagraha e l'Associazione Radicale Certi Diritti si fecero promotori di un'iniziativa politica fortemente sentita da moltissimi cittadini e cittadine di Torino di tutte le età, religioni, fedi, orientamenti politici e gusti sessuali: le nuove famiglie dovevano trovare un riconoscimento formale da parte dell'amministrazione comunale e non dovevano essere discriminate rispetto alle famiglie tradizionali.

Decidemmo, quindi, di formalizzare una proposta di deliberazione popolare per chiedere alla Città di Torino il riconoscimento di pari opportunità per le Unioni Civili. L'idea fu subito accolta dall'Associazione Radicale Adelaide Aglietta, dalla Consulta Torinese per la Laicità delle Istituzioni, dal Coordinamento Torino Pride LGBT, dalla Casa delle Donne, dal Centro Evangelico di cultura Arturo Pascal e dal Comitato torinese per la laicità della scuola, i cui rappresentanti, unitamente ai due rappresentanti delle associazioni proponenti (Associazione Radicale Certi Diritti e Associazione Radicale Satyagraha) costituirono il gruppo dei primi dieci sottoscrittori della proposta di deliberazione popolare.  
All'iniziativa aderirono oltre 31 realtà dell'associazionismo laico torinese, nonché molteplici personalità del mondo della cultura e dell'Università torinese, tra cui: Chiara Saraceno, Alfonso Di Giovine, Gianni Vattimo, Massimo Salvadori, Edoardo Tortarolo, Francesco Remotti, Carlo Augusto Viano, Franco Giampiccoli, Franco Sbarberi, Piera Egidi, Loredana Sciolla, Vincenzo Ferrone, Tullio Telmon.  
Con quest'iniziativa, le associazioni radicali torinesi riuscirono ad unire tutto l'associazionismo laico e GLBT locale, su un obiettivo politico preciso. La proposta di deliberazione venne sottoscritta da oltre 2700 cittadini e cittadine di Torino, di tutte le età, religioni, fedi, orientamenti politici e gusti sessuali. Dopo le discussioni avanti alle 10 Circoscrizioni Comunali, due passaggi in commissione ed un rinvio, lunedì, finalmente, si è giunti al voto.  
  
Gli emendamenti che hanno eliminato dal testo originario il riferimento alle pari opportunità non mutano il senso e l'importanza storica della deliberazione: sono rimasti, infatti, a chiare lettere - e da oggi costituiscono atto normativo dell'Ente Locale - sia il riconoscimento dell'Unione Civile per coloro che ottengono il rilascio del "certificato di famiglia anagrafica basato sul vincolo affettivo" inteso come reciproca assistenza morale e materiale ai sensi dell'articolo 4 del vigente regolamento anagrafico, sia il principio di non discriminazione di tale Unione, nei settori di competenza dell'amministrazione comunale e quindi: casa (assegnazione di case popolari e di contributi a sostegno di acquisti ed affitti), servizi sociali e sanitari (le unioni civili devono diventare destinatarie delle politiche rivolte alle famiglie), anziani e minori (diritto di rappresentanza e tutela dei propri conviventi di fronte ai servizi pubblici), scuola, formazione ed educazione (interventi di informazione) diritti e partecipazione (sportello informativo per i conviventi).  
  
E' un primo, importante, atto giuridico che segna il definitivo riconoscimento formale delle convivenze.  
E' una vittoria radicale, in quanto è nata dalla tenace volontà delle associazioni radicali torinesi, che sono state le promotrici dell'iniziativa, che hanno unito l'associazionismo sulla proposta, che hanno raccolto le firme e che hanno difeso la proposta di deliberazione popolare nelle 10 circoscrizioni e nelle sedute delle commissioni consiliari.  
Ma non possiamo fermarci. Alcuni sostengono che la votazione di lunedì non muta alcunché. Non è vero, in quanto l'amministrazione comunale dovrà adeguarsi al regolamento testè approvato. E seguendo la prassi politica radicale, controlleremo che tale deliberazione trovi piena e completa attuazione: in tutti gli atti emanati dalla Città di Torino le Unioni Civili non potranno più essere discriminate rispetto alla famiglia fondata sul matrimonio."  
  
Enzo Cucco  
Jolanda Casigliani  
Stefano Mossino