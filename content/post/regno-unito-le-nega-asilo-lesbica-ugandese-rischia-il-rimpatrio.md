---
title: 'Regno Unito le nega asilo, lesbica ugandese rischia il rimpatrio'
date: Thu, 16 Jun 2011 14:57:32 +0000
draft: false
tags: [Politica]
---

**Una donna ugandese che è stata marchiata con un ferro rovente nel suo paese d'origine come punizione per la sua sessualità, si trova ad affrontare il rimpatrio forzato dal Regno Unito. Articolo pubblicato dal Guardian il 31 maggio.**

La settimana scorsa il vice primo ministro, Nick Clegg, ha detto che la coalizione aveva concluso la pratica di deportare persone verso paesi dove rischiano la persecuzione a causa del loro orientamento sessuale.  
  
Ma Betty Tibikawa, 22 anni, che è detenuta nel centro per immigrati di Yarl's Wood  a Bedford, è in attesa di indicazioni sul suo rimpatrio dopo che la sua richiesta di asilo è stata respinta.  
  
Le organizzazioni dei diritti umani hanno ripetutamente documentato abusi contro gay e lesbiche in Uganda e affermano che è uno dei paesi più pericolosi al mondo per le persone gay.  
  
Tibikawa aveva appena finito il liceo ed è dovuta andare all'università di Kampala, quando è stato attaccata da tre uomini che l'hanno insultata per la sua sessualità. L'hanno inchiodata in un edificio in disuso e le hanno marchiato l'interno delle cosce con un ferro rovente. L'hanno abbandonata in stato di incoscienza, e quando finalmente è riuscita a tornare a casa è rimasta costretta a letto per due mesi. Una relazione medica indipendente ha confermato come le cicatrici siano coerenti con l'essere marchiati con un ferro rovente.  
  
"Non riesco a dormire e sto avendo incubi terribili su quello che mi potrà succedere se sarò rispedita in Uganda. La mia famiglia mi ha rinnegata perché io sono una lesbica e sono convinta che sarò uccisa se mi rimandano a casa.  
  
"Sono stata 'messa pubblicamnete al bando' in una rivista ugandese chiamata Red Pepper nel febbraio di quest'anno dicendo che me lo merito perché sono lesbica", ha detto. "Questo significa che sono in pericolo di vita al più alto rischio".  
  
Un'altra lesbica ugandese, BN, avrebbe dovuto essere rimpatriata dal Regno Unito a gennaio, ma il suo rimpatrio è stato sospeso a seguito dell'intervento dei suoi avvocati. Il suo caso sarà preso in esame in corte d'appello nel mese di luglio.  
  
David Kato, un attivista gay ugandese, è stato assassinato all'inizio di quest'anno. L'omosessualità è illegale in Uganda. Una legge anti-omosessualità che chiede misure più  punitive contro i gay avrebbe dovuto essere votata dal parlamento ugandese la scorsa settimana ma non è stato discusso. Potrebbe essere portata davanti al parlamento di nuovo più tardi nel corso dell'anno.  
  
Emma Ginn, coordinatore di Medical Justice, ha dichiarato: "Nonostante le convincenti prove mediche, la UK Border Agency rinnega la storia della sig.ra Tibikawa: gli UKBA non contesta che la sig.ra Tibikawa ha cicatrici causate da un ferro robente piatto, ma conclude che lei non ha sofferto. eventuali maltrattamenti in Uganda. Condanniamo il fatto che la loro intenzione di deportare la sig.ra Tibakawa in un paese dove essere gay è illegale e mette la tua vita a rischio ".  
  
Human Rights Watch portavoce Gauri van Gulik, ha dichiarato: "La nostra ricerca ha dimostrato che molti casi di donne come Betty non vengono prese sul serio dalla UK Border Agency Purtroppo le donne che subiscono questo tipo di violenza hanno gravi difficoltà chiedere asilo.."  
  
Un portavoce della UK Border Agency, ha dichiarato: "Il governo ha chiarito che si è impegnato a fermare la rimozione dei richiedenti asilo che hanno realmente dovuto abbandonare i paesi in particolare a causa del loro orientamento sessuale o l'identificazione di genere.  
  
"Tuttavia, quando qualcuno non si trova ad avere una vera credibilità ci aspettiamo che se ne vada volontariamente".  
  
Un uomo di 34 anni, gay  avrebbe dovuto essere rimpatriato in Uganda dal Regno Unito il 17 maggio. UKBA non ha confermato se il rimpatrio è stato effettuato.

(cf. Guardian article, ‘Ugandan woman branded by iron over sexuality faces deportation from UK’, 31 May 2011).