---
title: 'Newsletter 26 Maggio'
date: Wed, 31 Aug 2011 10:05:04 +0000
draft: false
tags: [Politica]
---

**![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)**

L’Italia è sempre più lontana dall’Europa: ascoltare quanto è stato detto alla Camera durante il dibattito generale sul ddl anti omofobia e transfobia è una ulteriore conferma, se ce ne fosse bisogno, di quanto l’omofobia e l’ignoranza siano diffuse a partire dai banchi dei parlamentari.

Certi Diritti invita tutti per una grande maratona oratoria davanti al Parlamento la prima settimana di giugno (il giorno preciso ancora non si sa) quando assisteremo, com’è probabile, alla definitiva bocciatura del provvedimento.

Abbiamo lanciato la prima applicazione lgbt(e) italiana per iPhone e scoperto che Apple censura la parola ‘omosessuale’. Abbiamo avviato un dialogo con l’azienda per risolvere questo spiacevole episodio.

Buona lettura

ASSOCIAZIONE
============

**Voto su omofobia e transfobia alla Camera del 31 maggio slitta. Sul sito tutti gli aggiornamenti SIT-IN E MARATONA ORATORIA IN PIAZZA MONTECITORIO.  
[Leggi >](tutte-le-notizie/1141.html)**

**Certi Diritti lancia prima applicazione LGBTE italiana per Iphone.  
[Leggi >](tutte-le-notizie/1137.html)**

**APPLE CENSURA PAROLA ‘OMOSESSUALI’ SU ITUNES.   
[Leggi >](tutte-le-notizie/1142-certi-diritti-apple-censura-la-parola-omosessuali-su-itunes.html)**

**'Verso l'Europride' due giorni all'Acrobax di Roma.   
[Leggi >](tutte-le-notizie/1139.html)**

**Gay Pride di Mosca ancora vietato.  
[Certi Diritti scrive al ministro Frattini >](tutte-le-notizie/1145-gay-pride-di-mosca-ancora-vietato-certi-diritti-scrive-a-frattini.html)**

**Giornata mondiale contro l'omofobia: Certi Diritti a Torino.  
[Leggi >](tutte-le-notizie/1133.html)**

**Certi Diritti: allarme omofobia, istituzioni si mobilitino.   
[Leggi >](tutte-le-notizie/1134.html)**

**Bocciata pdl anti omofo****​****bia in commissione giustizia.  
[Leggi >](tutte-le-notizie/1135.html)**

**Udienza in Corte di Appello per l'unione tra persone dello stesso.   
[Leggi >](tutte-le-notizie/1140.html)**

**Ivan Scalfarotto Come prendono il sole i gay?   
[Leggi >](http://www.ivanscalfarotto.it/2011/05/22/come-prendono-il-sole-i-gay/)**

**A che tempo che fa’ il commento sulla legge contro l’omofobia di Luciana Littizzetto, iscritta a Certi Diritti.   
[Guarda il video >](http://www.youtube.com/watch?v=kAPCdDv2k1E&feature=player_embedded)**

IN MEMORIA DI DAVID KATO KISULE
===============================

**Uganda/ Nessun dibattito in Parlamento sulla legge contro i gay.  
[Leggi >](http://www.wallstreetitalia.com/article/1131071/uganda-nessun-dibattito-in-parlamento-sulla-legge-contro-i-gay.aspx)**

**Beatrice Lamwaka. Pentole vuote**** _La marcia delle donne in Uganda_.   
[Leggi >](http://www.tellusfolio.it/index.php?prec=%2Findex.php&cmd=v&id=12827)**

**Uganda, la polizia colora di rosa chi protesta contro il governo.  
[Leggi >](http://www.news2u.it/2011/05/11/uganda-la-polizia-colora-di-rosa-che-protesta-contro-il-governo/)**

**’Europa ammonisce Governo e opposizione Ugandese.  
[Leggi >](http://www.reset-italia.net/2011/05/16/l%E2%80%99europa-ammonisce-governo-e-opposizione-ugandese/)**

RASSEGNA STAMPA
===============

**Alemanno su Europride.  
[Guarda il video >](http://www.youtube.com/watch?v=AqLWDpKF2FE&feature=youtu.be)**

**Europride. Messaggio di Alemanno, la Alicata chiede che sia respinto.  
[Leggi >](http://www.notiziegay.com/?p=75714)**

**Torino. Pride, una festa per ventimila.  
[Leggi >](http://www3.lastampa.it/torino/sezioni/cronaca/articolo/lstp/403476/) [Guarda le foto >](http://torino.repubblica.it/cronaca/2011/05/21/foto/in_migliaia_alla_sfilata_per_l_orgoglio_gay-16579695/1/)**

**Gay Pride: in migliaia sfilano in centro a Palermo – foto.   
[Leggi >](http://www.siciliainformazioni.com/giornale/cronacaregionale/125148/palermo-pride-2011.htm)**

**Omofobia, bocciato il testo. Carfagna furiosa: voterò sì.  
[Leggi >](http://www.radicali.it/rassegna-stampa/omofobia-bocciato-testo-carfagna-furiosa-voter-s)**

**Gay/ Giovanardi: Bene no a testo incostituzionale su omofobia.   
[Leggi >](http://www.lapoliticaitaliana.it/Articolo/?d=20110518&id=35619)**

**Rocco Buttiglione: "Gli omosessuali non sono un bene giuridico meritevole di tutela o da promuovere!"   
[Leggi >](http://www.queerblog.it/post/11238/rocco-buttiglione-gli-omosessuali-non-sono-un-bene-giuridico-meritevole-di-tutela-o-da-promuovere)**

**Corte Ue: unione civile omosessuale, stesso diritto alla pensione di coppia sposata.  
[Leggi >](http://www.helpconsumatori.it/news.php?id=32755)**

**Omofobia, la legge che fa paura.  
[Leggi>](http://www.lastampa.it/_web/cmstp/tmplRubriche/editoriali/gEditoriali.asp?ID_blog=25&ID_articolo=8761&ID_sezione=&sezione)**

**Pdl e Lega, ossessione gay.  
[Leggi >](http://espresso.repubblica.it/dettaglio/pdl-e-lega-ossessione-gay/2152029)**

**Intervista a Luigi De Magistris. Dico sì ai matrimoni gay.   
[Leggi >](http://www.notiziegay.com/?p=75538)**

**Da Palermo per il giudice Antonio Ingroia è ora di dire sì “alle adozioni e alle unioni civili” per gli omosessuali.  
[Leggi >](http://www.notiziegay.com/?p=75514)**

**Figli di coppie gay: lo scandalo che non c’è.  
[Leggi >](http://www.giornalettismo.com/archives/126414/figli-di-coppie-gay-lo-scandalo-che-non-ce/)**

**Ai miei clienti vip adesso offro solo peccati di gola (Storia di una trans).   
[Leggi >](http://ferdiefrancesca.forumcommunity.net/?t=45708543)**

**Pisa: detenuto con aids e tumore, rimandato in carcere dai medici dell’ospedale muore dopo due ore.  
[Leggi >](http://notizie.radicali.it/articolo/2011-05-20/editoriale/pisa-detenuto-con-aids-e-tumore-rimandato-carcere-dai-medici-dell-osp)**

**Contro l’omofobia: Brasile 1 – Italia 0.   
[Leggi >](http://www.ilfattoquotidiano.it/2011/05/21/contro-lomofobia-brasile-1-italia-0/111191/)**

**Francia. E’ imminente una proposta di legge sul matrimonio omosessuale.  
[Leggi >](http://gaynews24.com/?p=21098)**

**Gay nella cattolica Croazia: davvero facile?   
[Leggi >](http://www.notiziegay.com/?p=75431)**

**Test ‘fallometrici’. Indagine Ue contro la repubblica ceca: “Test degradanti sui richiedenti asilo”.   
[Leggi >](http://www.ilgiornale.it/esteri/indagine_ue_contro_repubblica_ceca_test_degradanti_richiedenti_asilo/europa-attualit-ue_repubblica_ceca/17-05-2011/articolo-id=523809-page=0-comments=1)**

**Università a luci rosse, Berlino capitale della prostituzione per studio.   
[Leggi >](http://www.corriere.it/esteri/11_maggio_19/dipasqua-universita-berlino_1ee05b10-8209-11e0-817d-481efd73d610.shtml)**

**In Tennessee non si potrà parlare di omosessualità negli istituti scolastici.   
[Leggi >](http://www.giornalettismo.com/archives/126353/lo-stato-dove-e-vietato-dire-gay-a-scuola/)**

**New York: pastore esorta a uccidere i gay che chiedono il matrimonio.  
[Leggi >](http://www.queerblog.it/post/11242/new-york-pastore-esorta-a-uccidere-i-gay-che-chiedono-il-matrimonio)**

STUDI, RICERCHE E MATERIALI INFORMATIVI
=======================================

**Omofobia. Il testo firmato da Paola Concia.  
[Leggi >](http://www.corriere.it/politica/11_maggio_18/testo-omofobia-concia-pd_d6d828c6-8162-11e0-ab0f-f30ae62858c8.shtml)**

**Le proposte di pregiudiziali di costituzionalità e sospensiva degli ineffabili Buttiglione, Lussana & C. che si voteranno martedì mattina 31 maggio alla Camera dei deputati prima del voto sul provvedimento contro omofobia e transfobia.   
[Leggi >](http://www.camera.it/417?idSeduta=476&resoconto=allegato_a.280200%C2%B6m=sed0476.allegato_a.n2802.sub0010#sed0476.allegato_a.n2802.sub0010)**

**Camera dei deputati, seduta del 23.5.11 . Tutela delle vittime di reati di omofobia e transfobia (pdl discussione generale).  
[Ascolta su Radio Radicale e guarda il video dal Parlamento >](http://www.radioradicale.it/new/html/vc_videocamera.php?id=328183#INT2555928)**

**Documento Politico del Roma Europride 2011.  
[Leggi >](http://www.europrideroma.com/Documento+politico.html?sezione=54&lang=it)**

**La Costituzione italiana vieta davvero il matrimonio omosessuale?  
[Leggi >](http://www.leggioggi.it/2011/05/24/la-costituzione-italiana-vieta-davvero-il-matrimonio-omosessuale/)**

**Omofobia: un italiano su quattro non vuole un vicino di casa gay.  
[Leggi >](http://www.queerblog.it/post/11222/omofobia-un-italiano-su-quattro-non-vuole-un-vicino-di-casa-gay)**

**Classifica europea dei paesi gayfriendly: l'Italia ottiene zero punti.  
[Leggi >](http://www.queerblog.it/post/11213/classifica-europea-dei-paesi-gayfriendly-litalia-ottiene-zero-punti)**

**ISTAT, calo matrimoni in Italia: meno 30mila in 2 anni.   
[Leggi >](http://mobile.italia-news.it/?idcnt=67423&lang=it)**

**Studio Usa. Se ci sono preti pedofili nella chiesa la colpa non è del celibato o dell’omosessualità.   
[Leggi >](http://gaynews24.com/?p=21127)**

**Usa/ Matrimoni gay: il 53 per cento degli americani è a favore.   
[Leggi >](http://notizie.virgilio.it/notizie/esteri/2011/05_maggio/20/usa_matrimoni_gay_il_53_per_cento_degli_americani_e_a_favore,29697956.html)**

**Aids. A rischio i progressi fatti finora.   
[Leggi >](http://www.vita.it/news/view/111528)**

LIBRI E CULTURA
===============

**Dalla Regione Toscana uno spot dedicato al turismo Gay-Friendly. ****  
[Leggi >](http://www.lanazione.it/firenze/cronaca/2011/05/17/507313-dalla_regione.shtml) ****[Guarda il Video >](http://multimedia.quotidiano.net/?tipo=media&media=15546)**

**Anna Wintour di Vogue sostiene il matrimonio gay.   
[Guarda il video >](http://www.queerblog.it/post/11220/anna-wintour-di-vogue-sostiene-il-matrimonio-gay)**

**Torino. Grandi eventi, sfilata di vanità. **Qualcuno fra le migliaia di visitatori sapeva per caso chi è Sandro Penna?**   
[Leggi >](http://archiviostorico.corriere.it/2011/maggio/20/Grandi_eventi_sfilata_vanita_co_9_110520085.shtml)**

**Macerata - Forza Nuova attacca rassegna cinema Lgbt: "Le perversioni vanno curate, non manifestate"****   
[Leggi >](http://www.queerblog.it/post/11227/macerata-forza-nuova-attacca-rassegna-cinema-lgbt-le-perversioni-vanno-curate-non-manifestate)**

**Mix [Festival cinemagaylesbico](http://www.cinemagaylesbico.com/2011/) a Milano** dal 25 al 31 maggio, Teatro Strehler, largo Greppi 1 MM2 LANZA

**_[Orgoglio e pregiudizio. Le lesbiche in Italia nel 2010](http://www.womenews.net/spip3/spip.php?article7645)_****, Alegre, € 5.00**

**Antonella Montano, Elda Andriola, _[Parlare di omosessualità a scuola](http://www.erickson.it/Libri/Pagine/Scheda-Libro.aspx?ItemId=39752)_ ,Centro Studi Erickson, € 17,50**

[www.certidiritti.it](http://www.certidiritti.it/)
==================================================