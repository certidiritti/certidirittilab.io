---
title: 'L''Associazione radicale Certi Diritti in piazza a Napoli contro le esternazioni di Silvio Berlusconi'
date: Fri, 05 Nov 2010 09:00:29 +0000
draft: false
tags: [BERLUSCONI, Comunicati stampa, napoli, OMOFOBIA, presidio]
---

 “I gay a Berlusconi: stevem scarz a munnezz!”

Presidio - Venerdì 5 novembre 2010 alle ore 16.00 a Piazza Bovio 22. Fuori dalla sede del PDL – Napoli

Martedì 2 Novembre, durante il ”Salone del Motociclo”, Silvio Berlusconi ha dichiarato che "è meglio appassionarsi alle belle donne che essere gay"! Un’infelice esternazione che è ben lungi dall’essere casuale, essendo il prodotto di una chiara strategia comunicativa volta a distogliere gli italiani dai reali problemi del Paese, che lo stesso Presidente del Consiglio non è in grado di affrontare né tanto meno risolvere....

Un'affermazione che è gravemente lesiva della dignità delle persone LGBTQE, ma soprattutto delle donne italiane che dovrebbero “appassionare” per ben altre ragioni legate al loro prezioso contributo alla storia, alla cultura ed alla civiltà di questo Paese e non in quanto oggetti da guardare e di cui disporre.

Per questi motivi saremo in piazza come attivisti di "Certi Diritti", affianco ad Arcigay e alle altre associazioni napoletane, oggi venerdì 5 novembre 2010, dalle ore 16:00 sotto la sede PdL di Piazza della Borsa a Napoli, non solo per protestare contro le esternazioni, ma chiedendo che si ritorni a parlare dei problemi reali dell'Italia e di Napoli, nonchè, richiamandoci agli artt 2 e 3 della Costituzione Italiana e della Carta di Nizza di cui lo stesso Berlusconi è firmatario in qualità di capo di Governo, pretendendo che il Presidente del Consiglio faccia scuse ufficiali a tutti gli italiani ed esigendo inoltre le immediate dimissioni dello stesso Presidente del Consiglio, in quanto rappresentativo di quella cultura machista e sessista che ripudiamo e che da sempre legittima atti di bullismo, di violenza e discriminazione ai danni delle persone LGBTQI, frequentissimi nell'Italia berlusconiana!

Per ulteriori informazioni: 335.6676600