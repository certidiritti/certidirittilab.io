---
title: 'DISCRIMINAZIONI: CENSIMENTO CON LE COPPIE GAY, INTERROGAZIONE PARLAMENTARE'
date: Fri, 05 Feb 2010 13:21:02 +0000
draft: false
tags: [Comunicati stampa]
---

**L'ISTAT EVITI POSSIBILI DISCRIMINAZIONI VERSO LE COPPIE GAY CONVIVENTI. IL PROSSIMO CENSIMENTO FOTOGRAFI LA REALTA' VERA DEL PAESE E NON QUELLA CHE PIACE SOLO AD ALCUNI.**

**Roma, 5 febbraio 2010 - Comunicato Stampa dell'Associazione Radicale Certi Diritti:** **L'Istat, in procinto di stabilire le nuove regole per l'acquisizione delle informazioni per sapere quante sono effettivamente le coppie gay conviventi, dovrà tenere conto di quanto avvenuto nello scorso censimento. Infatti pur avendo la possibilità di individuare quante sono le coppie gay conviventi in Italia, preferì, forse per far comodo a qualcuno, non inserire il dato nei risultati finali. Migliaia di persone chiedono un cambio di rotta grazie ad un appello lanciato dal portale [www.gay.it](http://www.gay.it/).  
  
I deputati radicali - Pd, primo firmatario Maurizio Turco, hanno oggi depositato un'interrogazione parlamntare urgente.   
Qui di seguito il testo integrale:  
  
**

Interrogazione urgente a risposta scritta  
  
al Presidente del Consiglio dei Ministri;  
al Ministro dell’Economia e delle Finanze;  
al Ministro per le pari opportunitá;  
  
  
Per sapere – premesso che:  
  
• i discorsi e gli atti di omofobia e transfobia in grave ascesa nel nostro Paese sono certamente prodotti dal clima sociale, culturale ideologico, intollerante, discriminatorio e illegale, che si fonda sui pregiudizi e i luoghi comuni, profittando anche della mancanza di dati e informazioni certe ed attendibili;  
  
•Secondo quanto illustrato dal Dottor Carlo D’Ippoliti, dell’Università di Roma La Sapienza, in un intervento pubblicato il 26 gennaio sul sito [vww.lavoce.info](http://vww.lavoce.info/), in occasione del passato censimento della popolazione, l’ISTAT ha gravemente mancato a tale funzione, modificando le informazioni liberamente fornite dai cittadini e le cittadine, senza alcuna trasparenza riguardo i modi e le conseguenze di tale comportamento; difatti in tale occasione  molte coppie di persone dello stesso sesso (con o senza figli) compilarono un solo modulo e si dichiararono come persone conviventi non sposate. Poi, però l'ISTAT ha considerato queste osservazioni come “dati incongruenti”, ed ha di suo arbitrio riattribuito il valore della risposta “convivente dell’intestatario” con un’altra risposta: “altra persona convivente senza legami di parentela”; con questa scelta, l’Istituto ha reso non riconoscibili le unioni affettive da quelle di altra natura, come ad esempio la convivenza del personale collaboratore domestico, o degli studenti fuori sede;  
  
•Rispondendo all’interrogazione avanzata da dodici parlamentari, nella XIV Legislatura il Sottosegretario per i rapporti con il Parlamento, dell’allora Governo Berlusconi, ha esposto una seconda possibile ragione della decisione di riattribuire le risposte fornite dai cittadini e dalle cittadine, come si legge nel resoconto stenografico della seduta alla Camera: “i dati personali idonei a rilevare la vita sessuale \[sic\] di un individuo sono considerati, ai sensi dell'articolo 4 del decreto legislativo n. 196 del 2003, dati sensibili e pertanto, ai sensi dell'articolo 20 dello stesso decreto legislativo, il trattamento dei dati sensibili da parte dei soggetti pubblici è consentito solo se conformi a disposizioni fissate dal legislatore; occorre cioè l'autorizzazione effettuata da una espressa disposizione di legge”.  
  
•Se da un lato tale indicazione può illuminare questa volta la stesura del regolamento di esecuzione del censimento, in corso di definizione, rimane alquanto controverso il fatto che, di fronte ad un vuoto normativo in materia di protezione dei dati, questi vengano modificati dall’ente che, eventualmente contra legem, li ha raccolti. Inoltre, non é privo di rilevanza che i dati raccolti contenevano informazioni sulle coppie LGBT cosí come loro avevano deciso di presentarsi, avendo sempre la possibilità di compilare due diversi questionari, o di dichiararsi “altri conviventi senza relazioni di parentela;  
  
•L’informazione sulla popolazione delle persone lesbiche e gay nel censimento é raccolta ed utilizzata da numerosi Paesi, senza che questo generi barriere di impossibilità sul piano statistico né giuridico;  
  
•Anche in Italia, l’informazione che si ricaverebbe non é scontata a priori, come ad esempio si puó vedere con un semplice esperimento. Considerando solo le coppie di persone adulte dello stesso sesso, non legate da vincoli di parentela né affinità, é possibile ottenere 151 osservazioni nel database dell’Indagine dei Bilanci delle Famiglie Italiane prodotta dalla Banca d’Italia nel 2007.  Emerge cosí che le coppie dello stesso sesso conviventi sarebbero più frequenti nel Nord e nelle Isole, e meno al Sud, e che il loro reddito medio individuale é pari a circa 17‘500 euro annui, contro i 18‘600 della media della popolazione italiana. Secondo diverse altre misure, il reddito delle coppie conviventi sembrerebbe inferiore alla media, sebbene la distribuzione dei titoli di studio non mostri differenze significative;  
  
•Queste osservazioni evidentemente non sono rappresentative dell’intera popolazione LGBT, come spiegato sopra, e oltre a includere il caso dei conviventi non legati da relazioni affettive, soffrono della grave limitazione costituita dal numero eccessivamente basso. Ad ogni modo, la loro breve analisi mostra come un’analisi più approfondita e meglio documentata potrebbe rivelare una realtà di marginalizzazione e discriminazione di segno opposto agli stereotipi dominanti.  
  
• il censimento della popolazione costituisce un’occasione unica nel decennio per lo studio anche  della popolazione omosessuale, bisessuale e transessuale, come documentato da autorevoli ricerche nazionali ed internazionali; la missione dell'Istituto nazionale di statistica è quella di servire la collettività attraverso la produzione e la comunicazione di informazioni statistiche e analisi di elevata qualità, realizzate in piena autonomia e sulla base di rigorosi principi etico-professionali e dei più avanzati standard scientifici, allo scopo di sviluppare un'approfondita conoscenza della realtà ambientale, economica e sociale dell'Italia e favorire i processi decisionali di tutti i soggetti della società;  
  
•Il portale gay italiano più importante, [www.gay.it](http://www.gay.it/), ha recentemente lanciato un appello già firmato da migliaia di cittadini affinché l’Istat accolga l’invito a modificare i criteri di raccolta ed elaborazione delle informazioni sulla popolazione lgbt;  
  
per sapere:  
  
• se non ritiene gravemente discriminatoria la mancata fornitura da parte dell’ISTAT di dati e informazioni disaggregate sul numero, le caratteristiche e le condizioni di vita della popolazione omosessuale, bisessuale e transessuale;  
• se in occasione del prossimo censimento della popolazione l’ISTAT intenda distinguere ed adeguatamente rappresentare le convivenze affettive da tutte le altre;  
• se in occasione del prossimo censimento della popolazione l’ISTAT intenda raccogliere ed elaborare dati e informazioni sulle coppie, le famiglie, e le singole persone omosessuali, bisessuali e transessuali, e fornire elaborazioni statistiche disaggregate rispetto a questa specifica fascia di popolazione;  
• se il Governo intende intervenire per garantire la completezza, veridicitá e correttezza delle informazioni raccolte nel contesto del prossimo censimento della popolazione, nel rispetto del diritto alla riservatezza dei dati personali, anche mediante autorizzazioni generali all’ISTAT per la raccolta, l’elaborazione e la fornitura, ad uso di ricerche ed elaborazioni statistiche, di informazioni riguardanti l’identitá di genere e l’orientamento sessuale.  
  
I deputati radicali - Pd:  
  
**Maurizio Turco  
Marco Beltrandi  
Rita Bernardini  
Maria Antonietta Farina Coscioni  
Matteo Mecacci  
Elisabetta Zamparutti**