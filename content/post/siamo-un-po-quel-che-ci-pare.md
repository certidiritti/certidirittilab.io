---
title: 'Siamo un po’ quel che ci pare'
date: Thu, 06 Jun 2013 07:46:24 +0000
draft: false
tags: [Politica]
---

_Replica a “Siamo tutti Gay” di Valentina Stella, Notizie Radicali 5/6/2013_

No, non serve "un cartello" appeso al collo per dire di esser gay, lesbica, bisessuale, transessuale, intersessuale, genitore gay, genitore etero, ragazza madre o ragazzo padre, sedicenne disinformato sul sesso, appassionato di sadomaso e chi più ne ha più ne metta.

E’ giusto educare all’uguaglianza piuttosto che alle differenze, ma è riduttivo e pericoloso archiviare il tutto con un “siamo tutti uguali, alla fine”. Mi ricorda il modo semplicistico con cui quest’anno il Ministero delle pari opportunità ha deciso di affrontare l’argomento omofobia con una discutibile pubblicità progresso: “Tizio ha i capelli rossi, Caio è intonato e Sempronio è gay… e non c’è nulla da dire”. “Nulla da dire” neanche per sogno, con tutto il rispetto.

Prima di entrare nel merito di quanto ho appena detto occorre aprire una parentesi sulla storia dei pride, premettendo che neanche io provo una grande simpatia per i papiri politici, le manifestazioni nazionali contro quelle locali et cetera... Le manifestazioni dell’orgoglio omosessuale, bisessuale, transessuale e intersessuale nascono il 27 giugno del 1969 (alcuni riconducono a questa data la nascita stessa dei movimenti di emancipazione gay) durante i cosiddetti moti di Stonewall. Lo Stonewall inn era un locale situato in Cristopher street a New York, vittima ogni sera delle retate della polizia locale che sistematicamente rendeva la vita impossibile a chi andava lì per incontrare persone del proprio sesso o per vestirsi con abiti del sesso opposto. Il 27 giugno 1969 migliaia di persone si ritrovarono lì a manifestare al motto di “we are everywhere”.

Oggi le celebrazioni in ricordo di quel giorno sono cambiate e hanno ceduto il passo ai tempi. Oggi non si parla più di “gay pride”, ma di “pride”. Mi piace pensare che questa evoluzione non sia l’ennesima metamorfosi di una retorica politicamente corretta sgrammaticata e ruffiana, ma una vera e propria apertura a chi è oppresso dagli atteggiamenti sessuofobici del quotidiano: dagli omosessuali che non vedono riconosciuto il diritto al matrimonio egualitario alle persone trans che si vedono negata la libertà di scegliere; passando per i giovani che negli anni 2000 non hanno ancora diritto a una corretta informazione sessuale nelle scuole, per i lavoratori del sesso che vedono umiliata la loro dignità e per chiunque in un modo o nell’altro viva diversamente dalla "media" la propria sessualità.

Non c’è “nulla da dire” sul proibizionismo sui corpi, sugli affetti e sull’informazione? “We are everywhere”, “siamo ovunque” e siamo LGBTI… E: lesbiche, gay, bisessuali, transessuali, intersessuali ed etero, come ricorda ogni tanto Marco Pannella.

Siano più che benvenute le caricature, anche se troppo spesso propinate e ripropinate come immagini di repertorio da una informazione scadente e miope che non si sforza di cercare la delegazione delle famiglie omoparentali e di spiegarne la storia, che non si avventura nel corteo per intervistare le associazioni di genitori di omosessuali, che a stento chiede aggiornamenti sulla via che prima o poi porterà al riconoscimento pieno delle libertà e delle responsabilità sessuali…

Noi dell’Associazione Radicale Certi Diritti saremo presenti per le strade in cui sfileranno i pride: senza carri, vesiti di tutto punto o mezzi nudi, truccati e non. Parteciperemo a dibattiti in cui parleremo del diritto alla sessualità delle persone disabili, della condizione degli LGBTI nei Paesi campioni della violazione dei diritti umani, dei progressi che stiamo facendo con “Affermazione civile” (il pacchetto di strategic litigations che ci ha portati alla sentenza 138 del 2010 della Corte Costituzionale).

Lo faremo da libertari e nonviolenti. Ma soprattutto da Radicali.

**Leonardo Monaco**  
**Tesoriere dell'Associazione Radicale Certi Diritti **