---
title: 'VII Congresso: no alle sanzioni penali contro l''incitamento alla discriminazione e alla violenza anche contro le persone LGBT e matrimonio egualitario'
date: Mon, 13 Jan 2014 09:39:46 +0000
draft: false
tags: [Politica]
---

Comunicato Stampa dell’Associazione Radicale Certi Diritti.

Roma, 13 gennaio 2014

Si è concluso ieri il VII Congresso dell’Associazione Radicale Certi Diritti la cui mozione generale fa di una riforma del diritto di famiglia che includa anche il**matrimonio egualitario** la **priorità della propria azione politica** rivendicando anche che all’opzione del matrimonio egualitario sia data voce in tutti i dibattiti pubblici sul tema dei diritti delle coppie omosessuali e delle famiglie omogenitoriali. 

Secondo **Yuri Guaiana**, **riconfermato segretario** dell’associazione, «la proposta di Renzi ha avuto il merito di riportare l’attenzione sul tema dei diritti delle persone LGBTI, ma le unioni civili non bastano a garantire anche ai cittadini LGBTI il fondamentale diritto di eguaglianza garantito dalla Costituzione repubblicana».

Una mozione particolare, che ha suscitato un largo dibattito, chiede di **non ricorrere al diritto penale contro l’incitamento alla discriminazione, all’ostilità e alla violenza **basate sull’orientamento sessuale e l’identità di genere così come sul genere, la disabilità, l’età, l’etnia, la nazionalità e la religione, né per fare norme-manifesto in chiave simbolica. Per l’Associazione Radicale Certi Diritti l’eguaglianza è meglio promossa da misure positive d’informazione e di formazione che accrescano la comprensione e la tolleranza piuttosto che dalla censura di punti di vista percepiti come ingiuriosi della comunità LGBTI o di ogni altra comunità. Solo in ben precise e limitate circostanze sarebbe accettabile ricorrere al massimo a provvedimenti civili e amministrativi, alla pena base della sanzione accessoria consistente in mirati lavori di pubblica utilità, o a forme di mediazione che consentano la piena partecipazione delle vittime in un ruolo attivo.

Un ordine del giorno, infine, chiede al governo italiano di fare tutto il possibile per sventare la promulgazione delle leggi che aggravano la criminalizzazione dei rapporti omosessuali in **Uganda** e in **Nigeria**, nonché di fare pressioni sulla **Repubblica Democratica del Congo** affinché il Parlamento non approvi una legge che vieti l’omosessualità.

Durante il Congresso, **Rita Bernardini**, segretaria di Radicali Italiani e già presidente dell’Associazione Radicale Certi Diritti, ha annunciato che **il 29 gennaio distribuirà la marijuana, da lei coltivata sul balcone di casa, a dei malati che potrebbero trarne molto giovamento**.

Oltre al **segretario Yuri Guaiana**, sono stati confermati anche il **tesoriere Leonardo Monaco** e il **presidente Enzo Cucco**.

  

  

  

**I DOCUMENTI CONGRESSUALI APPROVATI:**

[**MOZIONE, POSIZIONE DEL'ASSOCIAZIONE  SUL CONTRASTO ALL'OMOTRANSFOBIA E ODG APPPROVATI**](chi-siamo/mozione-congressuale)

[**BILANCIO 2013**](chi-siamo/bilancio)

[**STATUTO AGGIORNATO E REGOLAMENTO DEI GRUPPI DI INIZIATIVA LOCALE**](chi-siamo/statuto)

**L'AUDIO-VIDEO DELLA TRE GIORNI DI CONGRESSO:**

[**PRIMO GIORNO DI CONGRESSO**](http://www.radioradicale.it/scheda/400628/vii-congresso-associazione-radicale-certi-diritti-liberta-e-uguaglianza-affermazione-civile-per-i-diritti-)

[**SECONDO GIORNO DI CONGRESSO**](http://www.radioradicale.it/scheda/400629/vii-congresso-associazione-radicale-certi-diritti-liberta-e-uguaglianza-affermazione-civile-per-i-diritti-)

[**SEMINARIO SU LIBERTA' DI ESPRESSIONE, LEGGI CONTRO L'OMOTRANSFOBIA E NEGAZIONISMO IN UN'OTTICA LIBERALE**](http://www.radioradicale.it/scheda/400627/vii-congresso-associazione-radicale-certi-diritti-prima-giornata)

[**"ASSISTENZA SESSUALE: COS'E'? INTERROMPERE IL PROIBIZIONISMO SESSUOFOBICO SUI CORPI DELLE PERSONE CON DISABILITA'"**](http://www.radioradicale.it/scheda/400706/assistenza-sessuale-cose-una-proposta-nonviolenta-per-linterruzione-del-proibizionismo-sessuofobico-sui-co)