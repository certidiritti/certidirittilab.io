---
title: 'INTERVENTO DI KATO DAVID KISULE AL IV CONGRESSO DI CERTI DIRITTI'
date: Fri, 28 Jan 2011 14:18:35 +0000
draft: false
tags: [Comunicati stampa]
---

Roma, 27 novembre 2010

Cari Amici,   
innanzitutto voglio ringraziare l’associazione radicale Certi Diritti per avermi permesso di essere qui con voi quest’oggi per parlarvi sulla situazione dei diritti umani delle persone LGBTI in Uganda. 

Vorrei anche ringraziare Non c’è Pace senza Giustizia ed Elio Polizzotto che mi hanno aiutato in questi mesi a portare avanti la mia battaglia per i diritti delle persone LGBTI. Spero che questa non sia la fine ma l’inizio di una collaborazione che durerà sino a quando non vedrò realizzato il mio desiderio di una comunità LGBTI libera in Uganda. Inoltre, tengo particolarmente a ringraziare Marco Pannella e il Partito Radicale per l’impegno costante nella difesa e promozione dei diritti umani.

Sono qui oggi per parlarvi della situazione della comunità LGBTI in Uganda.

L’anno scorso i pastori evangelici degli Stati Uniti, in nome della protezione della famiglia tradizionale, hanno diffuso nel mio Paese odio e omofobia.

Ciò ha portato a una serie di conseguenze: dalle vessazioni agli arresti, dalle detenzioni alle morti, fino ad arrivare all’attuale proposta di legge contro gli omosessuali.  
La legge in vigore, secondo l’art.145 del codice penale ugandese, prevede, per chiunque commetta un reato contro natura, la reclusione a vita!

Gli atti osceni sono sanzionabili con sette anni in prigione.

Quello che vorrei trasmettervi con il mio intervento di oggi è un messaggio che riguarda anche voi italiani!

Si tratta di un problema che riguarda tutti, con ripercussioni globali. Abbiamo bisogno di affrontare insieme la questione perché l’Uganda non è uno Stato isolato, ma è parte della Comunità Internazionale.

Il disegno di legge presentato è incostituzionale e antidemocratico, perché viola il principio della non discriminazione, del diritto alla vita e al divieto di togliere arbitrariamente la vita di qualcuno.

Questa legge lede il diritto alla privacy, il diritto alla libertà e alla sicurezza della persona, diritto alla libertà di religione, di pensiero e di coscienza e il diritto alla libertà di espressione e di riunione.  
   
Il Ministro degli Affari Esteri, peggiorando paradossalmente le cose, ha promesso di eliminare la pena di morte dalla legge, sostenendo pero che alcune parti della proposta di legge dovranno essere inglobate nel sistema legislativo, senza preoccuparsi che queste clausole siano “gender neutral”.

Il ministro parla di promuovere la criminalizzazione dell'omosessualità, in altri termini ciò vorrebbe dire nessuna assistenza sanitaria, nessuna assistenza legale e un’accusa di promuovere l’omosessualità a tutti quelli che si offriranno di fornire tali servizi. Per questo motivo, organizzazioni della società civile e ONG che sostengono la causa dell'uguaglianza di genere potranno essere ad alto rischio.  
In generale questo è in contraddizione con i principi cui l’Uganda ha aderito, firmando convenzioni come ad esempio, la UDHR, ICCPR, ICESCR, CEDEW, la Carta africana, UNGASS. In alcuni di questi trattati l’Uganda è, tra l’altro, uno dei Stati promotori, come nel caso della risoluzione in materia di HIV/ AIDS.

Il giornale Rolling Stone, pubblicando foto, identità, indirizzi di luoghi di lavoro e di abitazione delle persone LGBTI, le ha rese, inoltre, più vulnerabili agli attacchi pubblici, alle minacce della polizia e così via.

Prima della pubblicazione di Rolling Stone, fondamentalisti religiosi avevano già incitato all'omofobia, mostrando materiale pornografico di uomini gay nelle chiese per incrementare l'odio, manifestando nelle strade nonostante il divieto imposto dall’ispettore generale di polizia, a Kampala, che li aveva costretti a cambiare città.

A questi incidenti non ha fatto seguito nessuna azione di governo finalizzata a punire i responsabili che cosi sono rimasti impuniti. Questo è un chiaro segnale che lo Stato ha istituzionalizzato l'omofobia e che la comunità LGBTI non ha alcuna protezione, nonostante il dovere del governo di proteggere tutti i cittadini.

Il 23 novembre 2010, presso il tribunale, alla fine dell'udienza del caso Rolling Stone, io e altri siamo stati assaliti da fondamentalisti religiosi, personale del Rolling Stone e “ex omosessuali” plagiati nel loro odio nei nostri confronti, da leader religioni.  Fortunatamente, grazie all’intervento dell’organo di monitoraggio delle ONG, siamo riusciti a lasciare indenni l’edificio. Questo ha costretto pero alcuni di noi a vivere in clandestinità nel nostro Paese per il timore di nuovi attacchi.

Chiedo, oggi, al Governo italiano di sollecitare l'ambasciatore ugandese ad ascoltare le nostre voci e di chiedere al Governo ugandese di sanzionare tutti i gruppi di persone che incitano alla discriminazione e alla violenza contro la comunità LGBTI.

Qualsiasi forma di aiuto, a noi e alla nostra sicurezza, sarà estremamente apprezzato.

Abbiamo citato in giudizio Rolling Stone all’Alta Corte Civile. L’udienza si terrà il prossimo 13 dicembre.

Abbiamo denunciato, inoltre, l’organo di vigilanza dei media, che avrà quattordici giorni per presentare la difesa. Purtroppo questo ricorso ha un costo che non riusciamo a sostenere, per cui qualsiasi tipo di aiuto da parte vostra sarà apprezzato.

Ritengo molto utile il sostegno di Non c'è Pace Senza Giustizia, che insieme ad alcune ONG locali, ha preso l'iniziativa di fare una mappatura di avvocati locali disposti a sostenere la causa. Riterrei altresì importante l’aiuto di altre organizzazioni della società civile che potrebbero dare il loro contributo per portare la giustizia e, in futuro, all’abrogazione di leggi ambigue che in nome della sodomia, portano avanti leggi per introdurre la pena di morte.

Occorre lavorare molto per sollevare il dibattito sui problemi globali come la diffusione dell'omofobia nel mondo.

Chiedo a tutti voi di aderire a un’iniziativa che stiamo preparando: una petizione per chiedere giustizia e per scoraggiare il parlamento ugandese ad approvare il disegno di legge, anche in considerazione del fatto che il Ministro dell’Etica e dell’Integrità ha dichiarato che tale testo dovrà essere approvato prima che le Camere vengano sciolte, cioè prima di maggio 2011.

Grazie a tutti per la vostra attenzione.

David Kato Kisule

Roma,  27 novembre 2010