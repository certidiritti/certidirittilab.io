---
title: 'La Camera dei Lords ha approvato in via definitiva la legge sul matrimonio egualitario. Ora basta la firma della Regina e Inghilterra e Galles avranno, dopo la Scozia, la nuova legge.'
date: Mon, 15 Jul 2013 16:26:21 +0000
draft: false
tags: [Europa]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti

Roma, 15 luglio 2013

La Camera dei Lords ha approvato in via definitiva la legge sul matrimonio egualitario. Ora basta la firma della Regina ed anche in Inghilterra e in Galles (dopo la Scozia) l'uguaglianza sarà effettiva per tutti e tutte di fronte al matrimonio. Era attesa, ma fa pur sempre piacere che nel Paese che ha costruito centinaia di anni fa la democrazia il dibattito ci sia stato, approfondito, lungo e complesso e che alla fine del percorso ha prodotto una riforma. Con questo atto passano a 10 i Paesi europei ove esiste il matrimonio egualitario, per un totale che super ail 30% dell'intera popolazione europea (Spagna, Portogallo, Francia, Olanda, Danimarca, Norvegia.....)

Evviva i conservatori inglesi, quindi, che hanno saputo guardare alla realtà della società, ed al principio di uguaglianza, non con gli occhi della reazione e del fanatismo, ma con quelli che guardano al futuro.

Esattamente la direzione a cui non guardano i nostri leader, a cominciare da quelli del Centro Destra che, accompagnati da quelli del Centro Sinistra, continuano a strologare sulla natura, senza parlare seriamente di diritti e doveri.

Speriamo che il Parlamento italiano avvii subito la discussione sui testi di legge presentati, senza escludere quelli sul matrimonio egualitario!

Enzo Cucco  
Presidente Associazione radicale Certi Diritti  
[www.certidiritti.it](http://www.certidiritti.it/)