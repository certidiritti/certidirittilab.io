---
title: 'CONFERENZA NAZIONALE DELLA FAMIGLIA: L''INVITO DELLA ROCCELLA È RIDICOLO'
date: Fri, 05 Nov 2010 12:54:42 +0000
draft: false
tags: [Comunicati stampa]
---

**L’invito della Roccella a gay e trans a partecipare alla conferenza governativa sulla famiglia è ridicolo. Grave che alla conferenza non si parli di divorzio breve, unioni civili, matrimonio gay, testamento biologico e adozioni.  
**

**  
Dichiarazione di Mario Staderini, Segretario di Radicali Italiani e  Sergio Rovasio, Segretario Associazione Radicale Certi Diritti**

  
"Stamane la Sottosegretaria Roccella, in un finto stupore, misto a finta sorpresa,  ha precisato che alla Conferenza Governativa sulla famiglia, che si aprirà lunedì 8 novembre a Milano, “tutte  le persone gay e trans che volessero venire, sono i benvenuti perché riconoscerebbero che essere figli di un uomo e di una donna è un’esperienza che unisce tutti…". Ma cosa c’entra questo con una conferenza governativa sulla famiglia?

La Sottosegretaria Roccella farebbe bene a non distrarre l’opinione pubblica dicendo cose ridicole di questa banalità. E ci mancherebbe pure che ad una conferenza governativa, promossa con fondi milionari, con spot pagati con i  soldi di tutti i cittadini,  andati sulle reti Tv pubbliche e private, con sbandieramenti di preti, arcivescovi e giornalisti di quotidiani religiosi, che addirittura avranno il ruolo di ‘rapporteur’ nei vari workshop previsti, non potessero accedere le persone gay e trans!

Ciò che la Sottosegretaria non dice è che la Conferenza governativa del mulino bianco, non affronta in nessuno dei tre giorni di lavoro i temi che in un qualsiasi paese civile, democratico e moderno, sono già divenuti leggi di  civiltà.

La vera conferenza sulle famiglie (e non quella sulla  famiglia del mulino Giovanardi), si aprirà a Milano lunedì alle ore 10,30, presso la facoltà di Scienze Politiche dell’Università di Milano, aperta a tutti i cittadini e a tutte le associazioni e realtà che vivono discriminazioni e pregiudizi grazie alla mancanza di leggi di riforma sul diritto di famiglia”.