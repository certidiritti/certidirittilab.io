---
title: 'Trans: Comune di Milano approva mozione di Certi Diritti per dare ai dipendenti trans la possibilità di usare un nome confacente al genere di elezione nei rapporti di lavoro'
date: Fri, 27 Feb 2015 08:32:58 +0000
draft: false
tags: [Transessualità]
---

[![BollataBadge](http://www.certidiritti.org/wp-content/uploads/2015/02/BollataBadge.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/02/BollataBadge.jpg)Il Consiglio comunale di Milano ha approvato nella serata del 26 febbraio la [mozione](http://www.certidiritti.org/wp-content/uploads/2015/02/Mozione-Trans.docx), scritta dall'Associazione Radicale Certi Diritti e presentata dal consigliere radicale Marco Cappato, che invita l'assessore alle politiche sociali Majorino e al l'assessore alle risorse umane Bisconti a introdurre in tutti gli uffici comunali di Milano la possibilità, per le persone in transizione, di usare il nome corrispondente al proprio genere di elezione in tutti i rapporti di lavoro e sul tesserino identificativo. 

"Il voto è un segnale di civiltà e di rispetto", ha affermato Marco Cappato, presidente del Gruppo Radicale-Federalista Europeo, continuando: "Mi auguro che la Giunta voglia dar seguito all'indicazione che arriva dal Consiglio".

"Un gesto di civiltà che riconosce dignità alle dipendenti e ai dipendenti trans del Comune di Milano", dichiara Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti. "Chiedo alla Giunta - prosegue Guaiana - di fare presto. Torino, varie università e imprese private hanno già adottato provvedimenti simili, non c'è tempo da perdere".

Comunicato stampa dell'Associazione Radicale Certi Diritti e del Gruppo Radicale - Federalista Europeo