---
title: 'Olimpiadi di Sochi: Pescante si scusi e l''Italia faccia come gli Stati Uniti'
date: Wed, 15 Jan 2014 17:37:42 +0000
draft: false
tags: [Politica]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 15 gennaio 2014

Mario Pescante, membro del Cio e presidente Coni dal 1993 al 1998 ritiene assurdo che gli Stati Uniti inviino "in Russia quattro lesbiche solo per dimostrare che i diritti dei gay sono calpestati" e parla di "terrorismo \[...\] politico in nome dei diritti".

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: "le parole di Pescante riportate dalla stampa sono cariche di omofobia e inqualificabili. Intanto gli Stati Uniti manderanno due atlete lesbiche e un atleta gay insieme ad altri atleti per rappresentare il Paese e non quattro lesbiche come dice Pescante. Se lui ritiene che gli atleti gay e lesbiche non debbano avere cittadinanza nelle delegazioni dei Pesi che partecipano alle Olimpiadi, si dovrebbe ripassare la Carta Olimpica il cui sesto principio fondamentale recita: "ogni forma di discriminazione nei confronti di un Paese o di una persona per motivi di razza, religione, politica o sesso, o altro è incompatibile con l'appartenenza al Movimento Olimpico". Non solo Pescante si dovrebbe scusare, per avere, lui sì, offeso lo spirito olimpico e i diritti umani di migliaia di cittadini Russi non solo LGBTI evocando il un terrorismo politico in nome dei diritti, ma l'Italia dovrebbe prendere esempio dagli Stati Uniti".

* * *

**ENGLISH VERSION**

**Sochi Olympics: Pescante should apologize and Italy do as the United States**

**Press release of Associazione Radicale Certi Diritti**

Mario Pescante, a senior International Olympic Committee member and former President of the Italian National Olympic Committee (1993-1998), considers it absurd that the United States will send "four lesbians to Russia just to prove that gay rights are violated" and refers to it as "political terrorism \[…\] in the name of rights." Yuri Guiana, secretary of Associazione Radicale Certi Diritti, states: "The words of Pescante reported in the press are homophobic and unspeakable. Meanwhile, the United States will send two lesbian and one gay athlete along with other athletes to represent the country and not four lesbians as Pescante stated. If he believes that gay and lesbian athletes should not have citizenship in the delegation participating in the Olympics, he should review the Olympic Charter, the sixth fundamental principle of which states that 'any form of discrimination with regard to a country or a person on grounds of race, religion, politics, gender or otherwise is incompatible with belonging to the Olympic Movement.'

Not only should Pescante apologize for having offended the Olympic spirit and the human rights of thousands of Russian citizens (not just LGBTI) by referring to it as a form of political terrorism in the name of rights, but Italy should follow the example of the United States."