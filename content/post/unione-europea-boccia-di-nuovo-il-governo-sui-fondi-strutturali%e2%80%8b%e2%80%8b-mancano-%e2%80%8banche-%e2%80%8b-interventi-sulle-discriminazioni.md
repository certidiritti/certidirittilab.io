---
title: 'Unione Europea boccia di nuovo il Governo sui Fondi Strutturali:​ mancano anche interventi sulle discriminazioni'
date: Tue, 01 Jul 2014 20:02:46 +0000
draft: false
tags: [Europa]
---

**L'Unione Europea ha rinviato di nuovo a Roma l'accordo di Partenariato con più di 200 rilievi, ottima occasione per Renzi di tappare il buco sulle discriminazioni. Chiediamo incontro con il Ministro Poletti.**

Roma, 1 luglio 2014​

L’Unione Europea ha di nuovo rispedito al mittente l’Accordo di Partenariato che il Governo italiano aveva inviato (per la terza o quarta volta) al loro esame. Questa volta gli appunti della Commissione sarebbero “solo” 200, o giù di lì. È una buona notizia per chi si occupa di discriminazioni in Italia perché in questo modo il Governo Renzi potrà modificare il testo e reinserire tra gli obiettivi individuati la _“__Lotta contro tutte le forme di discriminazione e per la promozione di pari opportunità__”._

Come è noto l’Accordo di Partenariato è essenziale per l’accesso ai fondi strutturali (in primo luogo FSE e FESR) che rappresentano l’unica fonte consistente e certa per sviluppare le politiche di inclusione sociale. Contravvenendo alle indicazioni che provenivano dall’Unione Europea (i Regolamenti parlano chiaro) nella proposta di Partenariato era stato cancellato questo punto, e il tema della lotta contro le discriminazioni diluito in tante piccole azioni che non prevedevano un'azione decisa per tutte le aree di discriminazione previste dall’Unione Europea. Guarda caso, quel documento non citava **MAI** l’orientamento sessuale e l’identità di genere tra le cause di potenziale discriminazione che invece l’UE prevede.

Preoccupazione inoltre destano le notizie che circolano sulla redazione di un PON Inclusione sociale (strumento operativo che dovrebbe seguire l’Accordo di Partenariato). In una recente riunione del tavolo tecnico le linee di indirizzo che emergono sono quelle di affidare all’UNAR **unicamente** le iniziative a favore dei ROM, Sinti e Camminanti, senza citare alcuna altra categoria di persone potenzialmente vittime di discriminazione. Se confermate queste notizie sarebbero inaccettabili, oltre che presagio di un futuro dell’UNAR “normalizzato” alle sole iniziative a favore dei ROM. Normalizzazione richiesta a gran voce dai tanti fondamentalisti che su questo terreno si sono mossi.

Le associazioni  chiedono un incontro al Ministro Poletti per avere delucidazioni sul futuro della lotta contro le discriminazioni (tutte) attraverso i fondi strutturali e in generale sull’impegno che il Ministero deve compiere per le persone a rischio per orientamento sessuale e identità di genere.

Fiorenzo Gimelli - AGEDO

Flavio Romani - Arcigay

Paola Brandolini - ArciL​esbica

Yuri Guaiana - Associazione Radicale Certi Diritti

Aurelio Mancuso - Equality Italia

Giuseppina La Delfa - Famiglie Arcobaleno

Porpora Marcasciano - MIT