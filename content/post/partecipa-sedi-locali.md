---
title: 'PARTECIPA - SEDI LOCALI'
date: Sat, 17 Sep 2011 15:40:28 +0000
draft: false
tags: [Politica]
---

**![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)**

Il più grande sostegno che puoi darci è contattare le sedi locali, e partecipare alle nostre iniziative. Se hai la passione per i diritti civili contattaci. Le cose da fare sono molte e l'impegno di ciascuno di noi può essere fondamentale e vitale per raggiungere gli importanti obbiettivi che abbiamo. 

La sede centrale è a Roma, in Via di Torre Argentina 76: info@certidiritti.it

Siamo presenti nei seguenti rerritori: 

**Piemonte e Valle d'Aosta:**

Enzo Cucco

[torino@certidiritti.it](mailto:torino@certidiritti.it)

**Lombardia:**

Carlo Cargnel

[milano@certidiritti.it](mailto:milano@certidiritti.it)

**Triveneto:**

Clara Comelli

[trieste@certidiritti.it](mailto:trieste@certidiritti.it)

**Gorizia**

Luca Cenni

[l.cenni@activeweb.it  
](mailto:l.cenni@activeweb.it)348-3237935

**Liguria:**

Alessandro Mazzone

[genova@certidiritti.it](mailto:genova@certidiritti.it)

**Emilia Romagna**

Maurizio Cecconi

[bologna@certidiritti.it](mailto:bologna@certidiritti.it)

**Marche**

Giacomo Celottini

[marche@certidiritti.it](mailto:marche@certidiritti.it)

**Lazio**

Sergio Rovasio

[roma@certidiritti.it](mailto:roma@certidiritti.it)

**Campania**

Andrea Furgiuele

[napoli@certidiritti.it](mailto:napoli@certidiritti.it)

**Puglia**

Dario Vese

[lecce@certidiritti.it](mailto:lecce@certidiritti.it)

**Calabria**

Riccardo Cristiano

[catanzaro@certidiritti.it](mailto:catanzaro@certidiritti.it)

**Sicilia**

Eduardo Melfi

[catania@certidiritti.it](mailto:catania@certidiritti.it)