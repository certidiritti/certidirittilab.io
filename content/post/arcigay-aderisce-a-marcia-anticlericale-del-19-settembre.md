---
title: 'ARCIGAY ADERISCE A MARCIA ANTICLERICALE DEL 19 SETTEMBRE'
date: Fri, 11 Sep 2009 13:23:13 +0000
draft: false
tags: [Comunicati stampa]
---

ARCIGAY ADERISCE A MARCIA ANTICLERICALE PROMOSSA DAI RADICALI IL 19 SETTEMBRE 2009.

Pubblichiamo qui di seguitoi il testo della lettera di adesione di Aurelio Mancuso, Presidente nazionale di Arcigay:

Carissimi amici Radicali,  
naturalmente Arcigay aderisce alla Marcia, purtroppo con immenso dispiacere proprio il 19 e 20 di settembre è convocato il nostro Consiglio Nazionale che discuterà di diversi temi tra cui l'organizzazione della manifestazione nazionale del 10 ottobre, il nostro contributo per avviare una grande campagna sul matrimonio civile, e la convocazione del nostro XIII° Congresso nazionale.  
Come potete comprendere saremo tutti impegnati a Bologna, però voglio che vi giunga il nostro più sincero sostegno!  
Un caro saluto  
Aurelio  Mancuso  
Presidente nazionale Arcigay