---
title: 'Risposta di Certi Diritti a Commissione europea'
date: Tue, 03 May 2011 06:28:18 +0000
draft: false
tags: [Europa]
---

Certi Diritti deposita presso la Commissione europea memoria sulla libera circolazione e mutuo riconoscimento delle unioni tra persone dello stesso sesso.

Bruxelles – Roma, 3 maggio 2011

  
  
L’associazione Radicale Certi Diritti ha depositato una memoria di risposta alla consultazione pubblica lanciata dalla Commissione europea sul Libro verde "Adempimenti amministrativi per i cittadini - promuovere la libera circolazione dei documenti pubblici e il riconoscimento degli effetti degli atti di stato civile" del 14 dicembre 2010. 

La comunicazione della Commissione europea aveva come obiettivo quello di fare una serie di proposte e sollevare domande in merito alle aspettative dei cittadini europei, delle ONG e delle istituzioni sui prossimi passi da compiere nell’ UE per abbattere gli ostacoli amministrativi opposti dagli Stati membri alla libera circolazione dei cittadini, promuovendo cosi la libera circolazione dei documenti pubblici ed il riconoscimento transnazionale degli effetti degli atti di stato civile.

Certi Diritti, nella sua risposta alla consultazione, ha denunciato il fatto che l’attuale situazione viola i diritti delle persone LGBT in quanto gli Stati membri compiono discriminazioni basate sull’orientamento sessuale nel non riconoscere i matrimoni e le partnership registrate delle coppie dello stesso sesso celebrate negli Stati membri ove questo è possibile, in violazione del mandato politico contenuto nei Trattati UE e nella Carta dei diritti fondamentali.

La Commissione ha quindi il dovere di proporre il superamento di tali violazioni e discriminazioni. Certi Diritti incoraggia quindi la Commissione europea a proporre misure di mutuo riconoscimento dei documenti ed atti (ad esempio attraverso la soppressione dell’autenticazione, della legalizzazione e della postilla, un certificato europeo di stato civile e l’utilizzo di moduli europei plurilingue) e dei loro effetti (ad esempio attraverso il riconoscimento di pieno diritto).

 Certi Diritti chiede inoltre alla Commissione europea di lanciare un Piano d’azione contro l’omofobia e per i diritti delle persone LGBT (Roadmap); di approfondire la questione dell’abuso da parte degli Stati UE delle clausole in difesa dell’ordine pubblico per non riconoscere i matrimoni e le unioni civili tra persone dello stesso sesso celebrati in quegli Stati membri che lo permettono; di rendere pubblico se ha ricevuto comunicazioni di stampo omofobico o contrario ai valori europei di tolleranza, eguaglianza e non discriminazione da parte di associazioni estremiste o integraliste cattoliche o altro.

Il testo del Libro verde della Commissione europea è disponibile al link [http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=COM:2010:0747:FIN:IT:HTML](http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=COM:2010:0747:FIN:IT:HTML)

Qui di seguito il testo integrale della risposta dell’Associazione Radicale Certi Diritti alla commissione Europea:

**Risposta alla Consultazione pubblica della Commissione europea sul**

**Libro verde "meno adempimenti amministrativi per i cittadini - promuovere la libera circolazione dei documenti pubblici e il riconoscimento degli effetti degli atti di stato civile"**

**Bruxelles, 14.12.2010, COM (2010) 747 definitivo**

All'attenzione della Commissione europea:

**INTRODUZIONE**

L'Associazione Certi Diritti ringrazia la Commissione europea per avere aperto una consultazione pubblica in merito alla questione della libera circolazione dei documenti pubblici ed il riconoscimento degli effetti degli atti di stato civile. Riteniamo che sia estremamente importante che le istituzioni europee ascoltino la voce dei cittadini e delle organizzazioni che si occupano sul campo della protezione dei loro diritti.

Come Associazione Certi Diritti, lottiamo perché i diritti delle persone LGBT siano rispettati in Italia, in Europa e nel mondo. Abbiamo in particolare fornito sostegno politico e legale alle coppie dello stesso sesso che intendevano sposarsi in Italia, ove il diritto al matrimonio per le coppie dello stesso sesso é negato, portando la questione fino alla Corte Costituzionale, che si é pronunciata riconoscendo l'importanza del riconoscimento dei diritti delle persone che intendono unirsi con persone dello stesso sesso e ha chiesto al legislatore di affrontare la questione regolamentandola in modo appropriato.

Sebbene il diritto matrimoniale sostanziale sia ancora – purtroppo! - di competenza nazionale, gli aspetti transfrontalieri sono di piena competenza europea. Sebbene l’Italia non abbia ancora adottato una legge sul matrimonio tra le persone dello stesso sesso, e neppure una sull’unione civile, e neppure una sulle convivenze, rimanendo uno degli ultimi Stati europei e del mondo occidentale a non regolamentare tali situazioni; sebbene l’Italia si allontani sempre più dall’Europa, assomigliando a uno Stato di matrice religiosa-fondamentalista, ove Stato e religione si confondono tanto da violare i diritti umani fondamentali e le regole democratiche; sebbene non si possa sperare ragionevolmente in uno sviluppo per i diritti civili nel paese, vista l’opposizione e la timidezza degli schieramenti di destra e di sinistra in tema di diritti LGBT, di unioni civili, di matrimonio; nonostante tutto cio’, l’Italia, come gli altri Stati membri dell’Unione, devono riconoscere lo stato civile delle persone che hanno contratto matrimonio con una persona dello stesso sesso o hanno adottato in un altro Stato dell’UE, senza discriminazioni basate sull’orientamento sessuale. Molti chiamerebbero questa situazione “discriminazione inversa” e la avverserebbero come impraticabile – tralasciando pero’ il fatto che tale discriminazione è causata innanzitutto dal non riconoscimento di diritti ai propri cittadini da parte delle autorità nazionali, e non certamente dal riconoscimento di diritti alle persone che esercitano i diritti europei fondamentali, ovvero il diritto alla libera circolazione, al mutuo riconoscimento, alla non discriminazione.

Anche il diritto primario e derivato dell’UE conferma tale prospettiva. Il Trattato UE afferma che _“l'Unione si fonda sui valori del rispetto della dignità umana, della libertà, della democrazia, dell'uguaglianza, dello Stato di diritto e del rispetto dei diritti umani, compresi i diritti delle persone appartenenti a minoranze. Questi valori sono comuni agli Stati membri in una società caratterizzata dal pluralismo, dalla non discriminazione, dalla tolleranza, dalla giustizia, dalla solidarietà e dalla parità tra donne e uomini”_, Articolo 2 TEU.

L’articolo 3 TEU afferma che _“l'Unione offre ai suoi cittadini uno spazio di libertà, sicurezza e giustizia senza frontiere interne, in cui sia assicurata la libera circolazione delle persone...combatte l'esclusione sociale e le discriminazioni e promuove la giustizia e la protezione sociali…”._

L’articolo 6 TEU richiama la _“Carta dei diritti fondamentali dell'Unione europea del 7 dicembre 2000, adattata il 12 dicembre 2007 a Strasburgo, che ha lo stesso valore giuridico dei trattati”,_ l’adesione della UE alla Convenzione europea per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali e afferma che “_i diritti fondamentali, garantiti dalla Convenzione europea per la salvaguardia dei diritti del­ l'uomo e delle libertà fondamentali e risultanti dalle tradizioni costituzionali comuni agli Stati membri, fanno parte del diritto dell'Unione in quanto principi generali”._

Il Trattato sul funzionamento dell’Unione europea afferma al suo articolo 10 che _“__nella definizione e nell'attuazione delle sue politiche e azioni, l'Unione mira a combattere le discriminazioni fondate sul sesso, la razza o l'origine etnica, la religione o le convinzioni personali, la disabilità, l'età o l'orientamento sessuale”_.

L’articolo 19 conferisce all’Unione poteri per prendere _“i provvedimenti opportuni per combattere le discriminazioni fondate sul sesso, la razza o l'origine etnica, la religione o le convinzioni personali, la disabilità, l'età o l'orientamento sessuale_”.

L’articolo 81.3 TEU, che definisce la competenza dell'UE in merito a misure che riguardino il diritto di famiglia con implicazioni transfrontaliere, deve essere letto in tale contesto più ampio. Le proposte della Commissione devono quindi promuovere e i diritti fondamentali, l’uguaglianza, la non discriminazione ed i diritti delle minoranze, combattendo le discriminazioni basate ad esempio sull’orientamento sessuale.

Certi Diritti ritiene quindi che la disapplicazione da parte degli Stati membri dell'UE del principio fondamentale del mutuo riconoscimento agli atti di stato civile, quali quelli di matrimonio e di filiazione, la negazione dei loro effetti e l'applicazione di clausole di ordine pubblico, per le coppie dello stesso sesso coabitanti, unite civilmente o sposate in un altro Stato membro, sia una discriminazione basata sull'orientamento sessuale, una violazione della libera circolazione dei cittadini, nonché una violazione più in generale dei diritti fondamentali delle persone, come tutelati dalla Convenzione europea sui diritti umani del Consiglio d'Europa, dai Trattati UE e dal diritto europeo.

Alla Commissione europea chiediamo iniziative, compito dovuto dai Trattati, che propongano il pieno rispetto e la piena protezione e promozione dei diritti dei cittadini europei, senza discriminazioni fondate sull'orientamento sessuale. La Commissione dovrebbe adottare in particolare una Roadmap, come richiesto dal Parlamento europeo, ovvero un piano d'azione con misure precise ed un calendario per la loro pubblicazione ed adozione, al fine di assicurare che le persone LGBT, che sono uno dei gruppi oggetto di discriminazione, possano vedere i loro diritti rispettati ed affermati.

In merito ai principi del mutuo riconoscimento e della libera circolazione, riconosciuti dal diritto primario e derivato europeo nonché riaffermato dalla giurisprudenza delle Corti, rileviamo che questi principi fondamentali dell'Unione europea sono applicati alle merci, ai capitali ed ai servizi ma paradossalmente non alle persone: questa situazione é intollerabile ed ingiustificabile. Cosa conta di più in Europa, i cittadini o le merci, i capitali, i servizi? Come giustificare gli ostacoli posti alle persone, alla loro libera circolazione ed al mutuo riconoscimento del loro stato civile, quando questi ostacoli sono inesistenti o comunque minori rispetto alle merci? E' necessario che l'Unione europea si occupi delle persone, dei cittadini europei, che sono alla base del progetto democratico europeo.

Certi Diritti sottolinea anche che numerosi Stati europei hanno un diritto di famiglia neutrale rispetto al sesso delle persone che contraggono il matrimonio, l’unione civile, la coabitazione. Gli altri Stati europei riconoscono tali unioni se contratte tra persone di sesso diverso, e non le riconoscono se contratte tra persone dello stesso sesso. Insomma la stessa legge è riconosciuta o no, applicata o no, a seconda del sesso e dell’orientamento sessuale delle persone. Non è questa una discriminazione basata sull’orientamento sessuale, lampante, chiara ed evidente? Se un tale trattamento discriminatorio in materia matrimoniale lo si applicasse – come purtroppo accaduto in passato ed oggi diventato fortunatamente inaccettabile – sulla base della razza o della religione o della nazionalità dei contraenti, non si parlerebbe di discriminazione?

Numerosi Stati, tra cui l’Italia, si avvalgono in questo delle clausole di ordine pubblico contenute nelle convenzioni internazionali in materia di mutuo riconoscimento dei matrimoni contratti all’estero. Appare singolare che il riconoscimento dell’unione tra due persone dello stesso sesso contratto sulla base della legge estera debba essere bloccato per motivi di “ordine pubblico”. Le persone omosessuali sono forse un pericolo per l’ordine pubblico, come lo sono i criminali, i terroristi, i teppisti? Il matrimonio, l’amore, l’unione di due persone sarebbe insomma causa di “disordine pubblico”? L’abuso di tale clausola varrebbe un’inchiesta della Commissione europea, essendo chiaro che tale utilizzazione è illegittima e contraria ai diritti fondamentali.

Certi Diritti chiede alla Commissione europea quindi di adottare un approccio ambizioso, che sembra avere animato la Commissaria Viviane Reding fin dall'assunzione del suo incarico, proponendo l'abolizione delle procedure della legalizzazione e delle postille e relative eccezioni, per i documenti pubblici ed in particolare di stato civile, assieme al pieno riconoscimento reciproco degli effetti degli atti di stato civile, proseguendo sulla strada del riconoscimento di pieno diritto e dell'armonizzazione delle norme di conflitto.

Certi Diritti inoltre segnala una campagna delle organizzazioni integraliste religiose cattoliche, in particolare European Dignity Watch, volte a sommergere la Commissione europea di interventi contrari alle proposte fatte nel Libro Verde per fare valere i diritti dei cittadini europei, senza discriminazioni basate sull'orientamento sessuale. Invitiamo la Commissione a riportare nel quadro della sua futura relazione sulla consultazione in merito se ha ricevuto comunicazioni di stampo omofobico o contrario ai valori europei di tolleranza, eguaglianza e non discriminazione contenuti nei Trattati europei.

**RISPOSTE ALLE DOMANDE POSTE DALLA COMMISSIONE EUROPEA**

**_Domanda 1 Ritiene che, sopprimendo formalità amministrative come la legalizzazione o la postilla, si risolvano le difficoltà incontrate dai cittadini?_**

Si. Certi Diritti condivide la posizione della Commissione europea in merito alla necessità di proporre ed arrivare urgentemente alla soppressione delle formalità amministrative per l'autenticazione dei documenti pubblici, della legalizzazione e della postilla, per tutti i documenti pubblici inclusi quelli di stato civile al fine di garantirne la libera circolazione nell'Unione, proponendo un quadro legislativo europeo uniforme e moderno. Solamente tale passo puo' assicurare una reale e piena libera circolazione e l'applicazione del principio del mutuo riconoscimento a beneficio dei cittadini. In tal modo si supererebbe inoltre la situazione di frammentazione ed incertezza giuridica attuale, i costi incorsi dai cittadini e i tempi lunghi per ottenere tali documenti, attraverso un quadro europeo unico, come richiesto dai cittadini stessi

**_Domanda 2 Occorre prevedere una cooperazione più intensa tra le autorità pubbliche degli Stati membri, in particolare per quanto riguarda gli atti di stato civile, e in quale forma elettronica?_**

Si. Tale iniziativa permetterebbe di assicurare che gli atti di stato civile siano ottenibili più velocemente ed ovunque sul territorio dell'UE. Uno studio lanciato dalla Commissione potrebbe valutare le diverse opzioni disponibili, esaminando anche sull'importanza di garantire la privacy e la protezione dei dati dei cittadini europei nella cooperazione tra amministrazioni.

**_Domanda 3 Cosa pensa dell’eventualità di registrare gli eventi di stato civile di una persona in un solo luogo, in un solo Stato membro? Quale sarebbe il luogo più indicato: il luogo di nascita, lo Stato di cui si ha la cittadinanza o lo Stato di residenza?_**

La soluzione più semplice per i cittadini europei sarebbe potere registrare gli eventi di stato civile in qualunque Stato membro presso uffici appositi, senza doversi spostare da Stato a Stato: il "certificato europeo di stato civile" proposto dalla Commissione, come pure "modelli standard" plurilingue, permetterebbero di sorpassare le barriere linguistiche permettendo effettivamente ai cittadini di registrare gli eventi di stato civile ovunque nella UE.

**_Domanda 4 Ritiene utile pubblicare l'elenco delle autorità nazionali competenti in materia di stato civile o segnalare un punto di informazione per ogni Stato membro?_**

Si. Qualunque iniziativa in merito alla migliore informazione dei cittadini é fondamentale.

**_Domanda 5 Quali soluzioni propone per evitare la traduzione o quanto meno limitarla?_**

Certi Diritti sostiene convintamente la soluzione proposta dalla Commissione di elaborare moduli standard europei plurilingui per tutti i documenti pubblici o di stato civile e un certificato europeo di stato civile.

**_Domanda 6 Quali atti di stato civile potrebbero essere oggetto di un certificato europeo di stato civile? Quali diciture dovrebbe riportare il certificato?_**

Tutti gli atti di stato civile dovrebbero essere oggetto di tale certificato europeo: la nascita, la filiazione, l'adozione, il matrimonio, il riconoscimento di paternità, la morte o l'attribuzione o il cambiamento di nome in seguito ad esempio a matrimonio, divorzio, unione registrata, riconoscimento, cambiamento di sesso o adozione. Il certificato dovrebbe riportare tutte le diciture nazionali in modo da superare le divergenze nazionali e prevedere nel caso in cui un determinato documento non sia previsto nell'ordinamento di uno Stato membro l'indicazione di un documento sostitutivo che abbia lo stesso effetto. Gli Stati membri dovranno indicare al Consiglio ed alla Commissione eventuali modifiche o nuovi documenti introdotti, in modo da aggiornare le corrispondenze tra documenti e certificato europeo.

**_Domanda 7: Ritiene che bastino le autorità nazionali per risolvere i problemi di stato civile riguardanti i cittadini in situazioni transfrontaliere? Se sì, non sarebbe opportuno che le istituzioni europee elaborassero almeno alcuni orientamenti (se del caso sotto forma di raccomandazioni dell'UE), al fine di garantire un minimo di coerenza tra gli approcci volti a trovare soluzioni pratiche ai problemi incontrati dai cittadini?_**

No, le autorità nazionali hanno dimostrato di non essere capaci da sole di risolvere i problemi di stato civile dei cittadini in situazioni transfrontaliere. Le istituzioni europee sono costantemente sollecitate in tal merito dai cittadini e dalle ONG, che richiedono l'adozione di misure europee comuni per superare gli ostacoli alla libera circolazione affermando il principio del mutuo riconoscimento. Certi Diritti condivide pienamente le affermazioni della Commissaria Reding nella plenaria del PE: un cittadino deve potere "portarsi" il suo stato civile dal paese A al paese B, per garantire pienamente i diritti fondamentali e i diritti civili dei cittadini, senza ostacoli o discriminazioni di sorta, garantendo la continuità e la permanenza della situazione di stato civile. Come afferma la Commissione, "se un cittadino decide di lasciare il proprio paese per vivere, lavorare, studiare in un altro paese, la situazione giuridica acquisita nel primo Stato membro ... non dovrebbe essere messa in discussione dalle autorità del secondo Stato membro se ciò può comportare ostacoli o difficoltà oggettive all'esercizio dei diritti del cittadino".

Certi Diritti sottolinea che oltre il mutuo riconoscimento deve comportare anche l'attribuzione di effetti civili all'atto di stato civile riconosciuto, garantendo il riconoscimento degli effetti di un atto di stato civile o di una situazione giuridica connessa allo stato civile creata in uno Stato membro diverso da quello in cui si desidera farla valere.

Certi diritti ritiene che la Commissione europea dovrebbe adottare atti vincolanti al riguardo non solamente "raccomandazioni" o dialoghi tecnici con gli Stati membri. La Commissione ha il compito di applicare le norme del Trattato sull'eguaglianza e sulle discriminazioni, sulla cittadinanza, sulla libera circolazione, applicare il principio del mutuo riconoscimento. Quando il diritto nazionale sostanziale di famiglia é in contrasto con tali principi e norme dei Trattati e della Carta dei Diritti fondamentali, compreso l'articolo 2, la Commissione deve agire per risolvere tali problemi. La soluzione proposta del riconoscimento di pieno diritto é la migliore soluzione al riguardo.

**_Domanda 8 Cosa pensa del riconoscimento di pieno diritto? A quali situazioni di stato civile potrebbe applicarsi?Per quali situazioni di stato civile rischierebbe invece di rivelarsi inappropriato?_**

Certi Diritti appoggia pienamente la proposta della Commissione sul riconoscimento di pieno diritto, che dovrebbe applicarsi a tutte le situazioni di stato civile, che assicura una serie di vantaggi quali la semplicità, la trasparenza, la certezza giuridica. Il matrimonio, come pure l'adozione, devono naturalmente essere coperti dal riconoscimento di pieno diritto, dato che é la situazione che più frequentemente pone problemi a livello europeo e non affrontarla significherebbe nullificare l'azione dell'UE in materia.

**_Domanda 9 Cosa pensa del riconoscimento basato sull'armonizzazione delle norme di conflitto di leggi? A quali situazioni di stato civile potrebbe applicarsi questa soluzione?_**

**_e Domanda 10 Cosa pensa della possibilità che i cittadini scelgano la legge applicabile? A quali situazioni di stato civile potrebbe applicarsi questa scelta?_**

**_e Domanda 11 Oltre al riconoscimento di pieno diritto e al riconoscimento basato sull'armonizzazione delle norme di conflitto, ritiene che esistano altre soluzioni agli effetti transfrontalieri delle situazioni giuridiche connesse allo stato civile?_**

Sebbene idealmente le norme di conflitto di leggi e la scelta della legge applicabile permettano di massimizzare la libertà di scelta delle persone in situazioni transfrontaliere, la soluzione del riconoscimento di pieno diritto appare più chiara e semplice anche per i cittadini. Certi Diritti rimane in attesa di un approfondimento della Commissione al riguardo al fine di determinare quale soluzione sia preferibile. Tutte le situazioni di stato civile dovrebbero essere coperte dalle norme europee, senza eccezioni. Al fine di verificare se esistano altre soluzioni oltre a quelle menzionate dalla Commissione, sarebbe utile condurre uno studio in merito alle regole che vengono applicate negli Stati federali o confederali per trarne insegnamento ed ispirazione per soluzioni europee.