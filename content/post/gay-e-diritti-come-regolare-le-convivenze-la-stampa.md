---
title: 'Gay e diritti, come regolare le convivenze - La stampa'
date: Wed, 24 Mar 2010 11:23:34 +0000
draft: false
tags: [Senza categoria]
---

[La stampa](http://www3.lastampa.it/domande-risposte/articolo/lstp/166132/)

A CURA DI RAFFAELLO MASCI

ROMA

**La Corte Costituzionale sta affrontando la questione dei matrimoni omosessuali. Potrebbero dunque entrare nella nostra legislazione?**

No. Una eventuale legge sui matrimoni tra persone dello stesso sesso, spetta al Parlamento. La Corte dovrà decidere su una questione di principio sollevata dai tribunali di Firenze, Ferrara, Venezia e Trento, dopo che ad alcuni cittadini dello stesso sesso è stato impedito di presentare le pubblicazioni di matrimonio.

**Perché questi cittadini si sono rivolti alla Consulta?**

Perché, alla luce di alcuni articoli della Costituzione, ritengono che l’accesso all’istituto del matrimonio debba essere garantito a tutti i cittadini senza alcuna discriminazione, e perché le nostre leggi non prevedono esplicitamente che i due aspiranti coniugi debbano essere di sesso diverso. Lo prevederebbero, cioè, solo in via di fatto. Queste le argomentazioni, sia pur molto semplificate.

**Ma esistono già stati che riconoscono i matrimoni omosessuali?**

Quasi tutti gli stati occidentali (eccetto Cipro, Malta, la Grecia e l’Italia) riconoscono le coppie di fatto, indipendentemente al fatto che siano omo o etero sessuali. Alcuni stati, poi - Svezia, Norvegia, Belgio, Spagna, l’Olanda e ultimamente il Portogallo - hanno esteso lo stesso istituto del matrimonio anche alle coppie gay.

**L’Italia, indipendentemente dalla sentenza in arrivo,  è tenuta a dotarsi di una legge?**

Sul matrimonio gay no. Sulle coppie di fatto, teoricamente sì. Esistono molti atti di giurisprudenza degli organismi comunitari e varie sentenze delle Corti di Strasburgo e Lussemburgo, che sollecitano gli stati dell’Unione europea a dare una risposta alle esigenze delle coppie di fatto anche omosessuali. Ma, soprattutto, esiste il Trattato di Lisbona, cioè la Costituzione europea che dà indicazioni in questo senso.

**Quindi, prima o poi, anche l’Italia dovrà adeguarsi?**

Questo non è chiaro. In quanto i provvedimenti comunitari danno indicazioni di indirizzo, ma non stabiliscono un obbligo vincolante. Senza toccare l’istituto del matrimonio, però, l’Italia potrebbe dotarsi di una norma sulle coppie di fatto sull’esempio dei Pacs francesi.

**Cosa sono i Pacs in Francia?**

La formula «Pacs» significa «patto civile di solidarietà» ed è entrata nella legislazione francese nel 1999. In sostanza una coppia può decidere di vivere insieme e di registrare la propria unione, accettando una serie di diritti e di doveri, senza ricorrere al matrimonio. Questo istituto si può applicare sia a una coppia omosessuale che eterosessuale.

**Sono stati proposti Pacs anche in Italia?**

La proposta di estendere i Pacs all’Italia non ha mai trovato una maggioranza in Parlamento. E’ stato proposto però qualcosa di analogo nella scorsa legislatura, e cioè i Di.Co (diritti dei conviventi) presentati dal governo Prodi nel 2007 più un’altra serie di proposte di legge (13 in tutto) di iniziativa parlamentare.

**E in questa legislatura ci sono analoghe istanze?**

Si, ce ne sono cinque. Una, nota come Di.Do.Re (diritti e doveri dei residenti), porta la firma, tra gli altri, dei ministri Renato Brunetta e Gianfranco Rotondi. Ce ne sono poi quattro presentate dal Pd: tre da Paola Concia e una da Mimmo Lucà.

**In cosa Di.Co o Di.Do.Re differiscono dai Pacs?**

La materia è molto complessa. In sintesi possiamo dire che i legislatori italiani, a differenza dei francesi, hanno sempre fatto in modo che una regolamentazione delle convivenze non si configurasse come una alternativa istituzionalizzata al matrimonio, quindi i vincoli dovevano essere minimi. Da noi le richieste degli interessati (associazioni gay e coppie di fatto) sono sostanzialmente tre: diritto all’eredità in caso di morte di uno dei partner e diritto all’assistenza in caso di malattia (la legge sulla privacy prevede infatti che solo il coniuge o un figlio possano parlare con i medici). La terza richiesta è la reversibilità pensionistica, che era prevista dai Di.Co ma non dai Di.Do.Re.

**Perché queste norme non sono mai passate?**

La dottrina cattolica prevede che l’unica unione affettiva possibile sia quella matrimoniale tra due persone di sesso diverso e che nessuno Stato debba fornire una possibile alternativa a questa ipotesi. Quindi i cattolici italiani presenti in Parlamento, sia a destra che a sinistra, hanno fatto muro contro qualunque normativa che regolamentasse le coppie di fatto. Inoltre l’attuale maggioranza ha detto più volte esplicitamente che, fin tanto che sarà al governo, non approverà mai una norma sulle coppie di fatto, neppure quella presentata dai suoi stessi ministri.