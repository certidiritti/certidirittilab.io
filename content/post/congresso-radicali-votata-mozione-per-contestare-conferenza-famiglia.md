---
title: 'CONGRESSO RADICALI: VOTATA MOZIONE PER CONTESTARE CONFERENZA FAMIGLIA'
date: Mon, 01 Nov 2010 13:46:17 +0000
draft: false
tags: [Comunicati stampa]
---

Chianciano Terme, 1 novembre 2010

L'Associazione Radicale Certi Diritti e Radicali Italiani contesteranno pacificamente la conferenza sulla famiglia con Silvio Berlusconi e Carlo Giovanardi che il governo ha organizzato dall'8 al 10 novembre a Milano.

E' questo l'impegno proposto ufficialmente in una mozione presentata al IX congresso, che dovrebbe approvarla nel pomeriggio. Il documento sottolinea la 'distanza abissale' delle associazioni Radicali per la famiglia dalle posizioni di palazzo Chigi, 'ispirate demagogicamente ai valori della famiglia tradizionale modello 'Mulino bianco' con una visione fondamentalista-talebano-vaticana intrisa di ipocrisia familista'. Dietro i pittoreschi attacchi alla linea del governo, il primo firmatario del documento Sergio Rovasio, segretario dell'associazione 'certi diritti', chiede una improbabile apertura alle istanze dei Radicali: testamento biologico, matrimonio civile tra omosessuali, divorzio breve, una nuova legge per l'affido condiviso, e procreazione assistita, solo per citare alcuni punti. La campagna che la mozione intende avviare e' ispirata ad un 'progetto di amore civile', gia' contenuto in una proposta di legge per la riforma del diritto di famiglia presentata alla Camera dai deputati Radicali. (ANSA).  CSS/MRS 01-NOV-10 13:05 NNNN