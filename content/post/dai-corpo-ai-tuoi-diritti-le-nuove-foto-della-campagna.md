---
title: 'Dai corpo ai tuoi diritti! Le nuove foto della campagna'
date: Sat, 25 Aug 2012 14:00:01 +0000
draft: false
tags: [Politica]
---

![LogoCD](http://www.radicalparty.org/file/Logo_Cd_2012.JPG)

è partita la campagna iscrizioni 2012 dell’associazione radicale Certi Diritti **‘Dai corpo ai tuoidiritti’: [parlamentari, artisti, ex porno divi, disabili, studenti, rifugiati e attivisti politici si sono spogliati contro la sessuofobia >](partecipa/iscriviti/itemlist/category/108)**  
La campagna, che ha avuto subito un ottimo riscontro, nasce con l'obiettivo di portare l'attenzione su temi che la classe politica, indifferente ai diritti dei cittadini e genuflessa ai diktat vaticani, continua a ignorare, ma anche per chiedere a tutti di sostenere un'associazione che non riceve finanziamenti pubblici e vive solo dei contributi di iscritti e sostenitori. **[Puoi contribuire alle nostre campagne con un semplice click](partecipa/iscriviti/itemlist/category/108)** e puoi partecipare alla campagna con la tua foto: scrivi ‘dai corpo ai tuoi diritti’ su un cartello, spogliati, **fatti la foto e inviala a [info@certidiritti.it](mailto:<a href=)" target="_blank">[info@certidiritti.it](mailto:info@certidiritti.it)**. Ogni giorno per tutta l’estate pubblicheremo nuove foto, aiutaci!            
Sabato scorso Certi Diritti ha partecipato al **[Pride di Palermo](http://www.certidiritti.org/2012/06/28/la-partecipazione-dellassociazione-radicale-certi-diritti-al-gay-pride-di-palermo-anche-in-ricordo-di-david-kato-kisule-palermo-28-giugno-2012-comunicato-stampa-dellassociazione-radicale-certi-diritti/)** e a quello di Roma.  
Nella capitale abbiamo allestito tre mezzi elettrici per rilanciare le nostre campagne sulla legalizzazione della prostituzione (**[firma l’appello](campagne-certi-diritti/itemlist/category/85-legalizzazione-prostituzione)**), sul matrimonio tra persone dello stesso sesso e sulla prevenzione delle malattie sessualmente trasmissibili con la distribuzione gratuita di migliaia di preservativi. **[Guarda il video di Radio Radicale con le interviste ai partecipanti >](http://www.radioradicale.it/scheda/355334)**

**[Dal Roma Pride: “sui diritti per gli omosessuali siamo fuori dall’Europa”](http://tv.ilfattoquotidiano.it/2012/06/23/roma-pride-2012/200223/ "Permanent link to Roma, Gay pride 2012: “Sui diritti per gli omosessuali siamo fuori dall’Europa”")**. Tra le interviste quella di **Sergio Rovasio** e di **[Dario Vese](http://www.tmnews.it/web/sezioni/video/20120624_video_13062189.shtml)** del Direttivo di Certi Diritti.           

Intanto il livello allarmante raggiunto dalle aggressioni omofobe e transfobiche nel nostro Paese ha spinto **[Ilga-Europe a scrivere a Monti, Fini e Schifani per chiedere urgenti provvedimenti. Il testo della lettera >](http://www.certidiritti.org/2012/06/26/italia-sorvegliata-speciale-ilga-europe-scrive-a-monti-fini-e-schifani-per-chiedere-urgenti-provvedimenti-contro-omofobia-il-testo-della-lettera/)**

Certi Diritti con Arcigay Fish e Enar ha scritto una lettera aperta al Presidente Monti e ad alcuni Ministri del suo Governo **[per chiedere di esercitare la propria leadership per sbloccare ladirettiva antidiscriminazione che giace presso il Consiglio dell’Unione europea >](http://www.certidiritti.org/2012/06/28/europa-non-dimentichi-la-lotta-alle-discriminazioni-lettera-di-associazioni-a-monti-per-sblocco-direttiva-commissione-europea/)**

Vice Comandante dell'Arma dei Carabinieri offende gay, polizia, Gdf, Magistrati, morti per suicidio.**[Esposto di Certi Diritti a Unar e Oscad >](http://www.certidiritti.org/2012/06/27/vice-comandante-dellarma-dei-carabinieri-offende-gay-polizia-gdf-magistrati-morti-per-suicidio-esposto-di-certi-diritti-a-unar-e-oscad/) **  
**Interventi**

"La cittadinanza va scritta": Intervista a Yuri Guaiana.** [Guarda il video >](http://www.youtube.com/watch?v=M_qyb5_6OYc&feature=share)**

Cosenza presenta Libro "Certi diritti che le coppie conviventi non sanno di avere".  
**[Guarda il video >](http://www.youtube.com/watch?v=145Wxm_75Xw)**

**[Coppie di fatto, libertà o irresponsabilità?](http://www.nuovavicenza.it/2012/06/coppie-di-fatto-liberta-o-irresponsabilita/) **Intervista a Yuri Guaiana, segretario di CertiDiritti, e** [la conseguente rettifica >](http://www.nuovavicenza.it/2012/06/guaiana-certi-diritti-100mila-bambini-in-coppie-di-fatto-non-adottati/)**

Liberi.tv - Spazio Certi Diritti: **[Informazione sessuale tra bullismo e apatia.](http://www.liberi.tv/webtv/2012/06/24/video/informazione-sessuale-tra-bullismo-e-apatia-conversazione-con)** Conversazione con la Prof.ssa Nella Coletta, insegnate e scrittrice a cura di Riccardo Cristiano.

Fuor di pagina - la rassegna stampa di Certi Diritti ora è disponibile anche in podcast su **[iTunes](http://itunes.apple.com/it/podcast/radioradicale-fuor-di-pagina/id530466683?mt=2.).[Ascolta su radio radicale >](http://www.radioradicale.it/rubrica/1004)**

[www.certidiritti.org](http://www.certidiritti.org/)