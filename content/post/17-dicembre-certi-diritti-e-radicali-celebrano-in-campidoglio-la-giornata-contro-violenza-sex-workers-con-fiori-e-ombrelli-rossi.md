---
title: '17 dicembre: Certi Diritti e Radicali celebrano in Campidoglio la Giornata contro violenza Sex Workers con fiori e ombrelli rossi'
date: Fri, 16 Dec 2011 12:16:47 +0000
draft: false
tags: [Lavoro sessuale]
---

Il 17 dicembre è la Giornata Internazionale per la fine della violenza contro le sex workers. A Roma alle ore 12 l'Associazione Radicale Certi Diritti e Radicali Roma depositeranno fiori sulla scalinata del Campidoglio e leggeranno i nomi delle prostitute uccis enegli ultimi tre anni.

Roma, 16 dicembre 2011  
   
Comunicato Stampa dell’Associazione Radicale Certi Diritti e dell’Associazione Radicali Roma  
   
Domani, sabato 17 dicembre, “Giornata Internazionale per la fine della violenza contro le Sex Workers”, che si celebra in tutto il mondo, una delegazione dell’Associazione Radicale Certi Diritti e di Radicali Roma depositerà a Roma, alle ore 12, un mazzo di fiori sulla scalinata del Campidoglio per ricordare la tragedia di centinaia di prostitute vittime delle leggi proibizoniste e della violenza e dell’odio. I partecipanti avranno gli ombrellini rossi, simbolo internazionale delle Sex Workers.  
   
Negli ultimi tre anni sono state uccise in Italia almeno trenta prostitute, domani sulla scalinata del Campidoglio verranno letti i loro nomi. E’ indefinito il numero delle violenze e delle rapine contro le persone che si prostituiscono perché tali crimini non vengono neppure più denunciati per paura.  
   
L’ordinanza anti-prostituzione del Sindaco di Roma, del settembre 2008,  è miseramente fallita, gli stessi Vigili Urbani, messi alla caccia di prostitute e clienti in questi tre anni, hanno dichiarato che su 20.281 contravvenzioni anti-lucciole (di 200 Euro l’una), di queste  sono state pagate solo 600. Il Sindaco ha risposto a un’interrogazione dei Radicali senza dare le cifre delle spese che il Comune ha dovuto affrontare per far applicare l’inutile ordinanaza anti-lucciole, che operano di giorno e di notte in tutte le strade della capitale.  
   
I Radicali hanno depositato in Parlamento una proposta di legge, a prima firma della Senatrice Donatella Poretti, che chiede di legalizzare il fenomeno della prostituzione.  
   
 L’”International Day to End Violence Against Sex Workers”  istituito nel 2003 dall’organizzazione Americana “Sex Workers Outreach Project USA”, ha l’obiettivo di ricordare gli effetti gravissimi del proibizionismo sulla prostituzione: dalla diffusione dell’Aids alle altre malattie sessualmente trasmissibili, dalla violenza e dall’odio contro le persone che si prostituiscono fino alla lotta per combattere lo stigma alimentato da leggi assurde e anti-costituzionali.