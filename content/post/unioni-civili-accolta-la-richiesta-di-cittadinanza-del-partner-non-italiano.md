---
title: 'Unioni civili: accolta la richiesta di cittadinanza del partner non italiano'
date: Wed, 28 Mar 2018 15:43:09 +0000
draft: false
tags: [Diritto di Famiglia]
---

![unioni-civili-gay-omosessuali](http://www.certidiritti.org/wp-content/uploads/2018/03/unioni-civili-gay-omosessuali-300x150.jpg)Dopo l’approvazione della legge che regolamenta le unioni civili in Italia e la possibilità per le coppie dello stesso sesso sposate o unite civilmente all’estero di trascrivere la loro unione in Italia, si è aperta la possibilità del/della partner non italiano/a di richiedere e ottenere la nostra cittadinanza.

La prima persona, di cui abbiamo notizia, che l’acquisirà in forza del matrimonio celebrato in Portogallo e trascritto come unione civile in Italia, abita a Reggio Emilia e il prossimo 30 marzo, dopo aver ottenuto il decreto di concessione, giurerà in Comune la sua fedeltà alla Repubblica italiana.

La coppia in questione, seguita dall’Associazione radicale Certi Diritti è stata una delle prime ad ottenere per il ragazzo non italiano la Carta di soggiorno per familiare di cittadino europeo grazie all’applicazione delle norme comunitarie sulla libera circolazione.

Proprio il Tribunale di Reggio Emilia, riconobbe per la prima volta in Italia ad un’altra coppia italo-uruguayana seguita da Certi diritti, il diritto alla vita familiare grazie ad un ricorso presentato dall’Avv. Giulia Perin e dispose il rilascio del primo titolo di soggiorno per il partner dello stesso sesso non italiano. Questa normativa europea permise dal 2012 fino all’approvazione della Legge sulle unioni civili, alle coppie miste di poter vivere insieme in Italia.

Ora, sempre a Reggio Emilia, la prima acquisizione di cittadinanza italiana.

Gabriella Friso, responsabile di “Affermazione civile” nel direttivo di Certi diritti dichiara: “Ricordiamo che nelle coppie miste dello stesso sesso sposate o unite civilmente all’estero, che hanno trascritto la loro unione in Italia o sono unite civilmente nel nostro Paese, il partner non italiano dopo 2 anni di vita in Italia dalla loro unione/matrimonio ,  o dopo 3, se la coppia risiede all’estero, può presentare la richiesta della cittadinanza italiana.

Questo è un altro passo importante verso l’uguaglianza ma restano ancora l’impossibilità di celebrare il matrimonio civile e il riconoscimento della doppia genitorialità, compresa la possibilità di adozione che differenziano le famiglie omoaffettive dalle altre. La nostra Associazione continuerà ad impegnarsi fino all’ottenimento dell’uguaglianza.”