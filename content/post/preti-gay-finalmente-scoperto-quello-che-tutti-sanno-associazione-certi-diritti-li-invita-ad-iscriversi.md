---
title: 'PRETI GAY: FINALMENTE SCOPERTO QUELLO CHE TUTTI SANNO, ASSOCIAZIONE CERTI DIRITTI LI INVITA AD ISCRIVERSI'
date: Fri, 23 Jul 2010 08:04:36 +0000
draft: false
tags: [certi diritti, Comunicati stampa, diritti, IPOCRISIA, preti, preti gay, SESSUALITA', vaticano]
---

**PRETI GAY: FINALMENTE UN'INCHIESTA GIORNALISTICA SERIA DOCUMENTA QUELLO CHE TUTTI SANNO! INVITIAMO I PRETI GAY A ISCRIVERSI A CERTI DIRITTI,** **DIFENDEREMO IL LORO DIRITTO ALLA SESSUALITÀ, NON QUELLO ALL'IPOCRISIA**

Dichiarazione di **Sergio Rovasio**, Segretario dell'Associazione Radicale Certi Diritti

"Finalmente un'[inchiesta giornalistica](http://blog.panorama.it/italia/2010/07/22/le-notti-brave-dei-preti-gay-una-grande-inchiesta-in-edicola-venerdi-con-panorama/ "www.panorama.it") documenta quello che tutti i frequentatori di locali gay romani sanno: tantissimi preti gay li frequentano e praticano sesso, senza nemmeno preoccuparsi molto dell'evidente ipocrisia tra ciò che predicano di giorno e ciò che fanno di notte.  
  
Certo, tutti hanno diritto a vivere la loro sessualità, ma non è accettabile che quegli stessi preti che di giorno predicano contro le persone lgbt(e), di notte pratichino esattamente il contrario.  
  
Il Vaticano - all'interno del cui territorio sappiamo per certo esistere un'area di 'battuage' frequentata da preti e cardinali gay; non riferiamo il luogo esatto, per evitare che venga piantonato dalle Guardie Svizzere in divisa, mentre quelle in borghese già ci sono - farebbe bene a non alimentare la sua ipocrisia: anziché parlare di 'scandalismo', dovrebbe prendere atto che nel suo apparato vi sono parecchi omosessuali, tutti 'velati' per paura.  
  
Alimentare l'odio contro le persone gay, lesbiche, bisessuali e transgender, come fanno le gerarchie vaticane un giorno sì e l'altro pure, vuole anche dire parlare contro una parte del proprio esercito di preti e suore, che vivono in clandestinità e nel terrore la loro condizione.  
  
Noi di Certi Diritti invitiamo le suore e i preti gay ad iscriversi alla nostra associazione: troveranno dialogo, aiuto e sostegno anziché odio e ipocrisia! Avremo così un gruppo di lavoro sui nostri temi e obiettivi anche all'interno del Vaticano."