---
title: 'CORTE COSTITUZIONALE SU MATRIMONIO GAY.'
date: Wed, 24 Mar 2010 11:24:41 +0000
draft: false
tags: [Comunicati stampa]
---

**COMUNICATO STAMPA DELL'ASSOCIAZIONE RADICALE CERTI DIRITTI, CO-PROMOTRICE CON AVVOCATURA LGBT - RETE LENFORD DELLA CAMPAGNA DI ‘AFFERMAZIONE CIVILE'.**

Roma, 24 marzo 2010

Prevista per oggi la decisione della Corte Costituzionale.

Oggi è prevista la decisione della Corte Costituzionale sui ricorsi di alcune coppie gay che avevano chiesto al loro Comune di potersi sposare ed alle quali era stato opposto il diniego. Le coppie avevano partecipato alla campagna di ‘Affermazione Civile' promossa due anni fa dall'Associazione Radicale Certi Diritti (www.certidiritti.it ) con il supporto di Avvocatura lgbt - Rete Lenford.

La campagna di Affermazione Civile prevede che le coppie lesbiche e gay che vogliono sposarsi si rechino nel loro Comune, presentino le richieste per le pubblicazioni matrimoniali e quando il Comune oppone loro il diniego viene incardinata una iniziativa legale. Alla campagna hanno aderito quasi una trentina di coppie omosessuali, centinaia di altre coppie sono in attesa della decisione della Corte.

L'Associazione Radicale Certi Diritti attende con speranza e fiducia le decisioni della Corte Costituzionale prevista per oggi, mercoledì 24 marzo. Ieri la Corte costituzionale ha escluso l'Associazione Radicale Certi Diritti dalla possibilità di potersi costituire nel giudizio di merito.

Qualunque sarà la decisione, l'Associazione Certi Diritti continuerà con forza e determinazione la sua lotta per il superamento delle disuguaglianze e contro le discriminazioni, così come avvenuto in molti paesi europei e che gran parte della classe politica italiana, con rare eccezioni, continua ad ignorare.

L'Associazione Radicale Certi Diritti aderisce al 'Comitato Si lo voglio', organismo che raggruppa le principali associazioni lgbt(e) italiane per coordinare la campagna in favore del matrimonio gay in Italia. Il Comitato Si lo voglio ha previsto per oggi una Conferenza Stampa alle ore 12, all'Hotel Nazionale.

Sergio Rovasio, Segretario Associazione Radicale Certi Diritti

Ufficio Stampa: Giacomo Cellottini Tel. 06-68979250 Cell.333-4801319