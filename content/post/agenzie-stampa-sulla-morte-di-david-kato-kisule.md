---
title: 'Agenzie Stampa sulla morte di David Kato Kisule'
date: Thu, 03 Feb 2011 12:36:10 +0000
draft: false
tags: [Comunicati stampa]
---

Uganda/Delanoe "costernato" per "barbaro omicidio" attivista gay

Parigi, 28 gen. (TMNews) - Il sindaco socialista di Parigi, Bertrand Delanoë, uno dei pochi politici francesi ad aver dichiarato la propria omosessualità, si è detto "costernato" per il "barbaro omicidio" dell'attivista ugandese per i diritti dei gay, David Kato, ucciso ieri nella sua abitazione vicino Kampala.  
Il nome, la fotografia e l'indirizzo di Kato, insieme a quelli di altri 100 gay, erano stati pubblicati in prima pagina dalla rivista ugandese Rolling Stone lo scorso ottobre, sotto il titolo 'Impiccateli'.  
"Apprendo con costernazione dell'omicidio di David Kato - scrive il primo cittadino in un comunicato - questo attivista per l'uguaglianza e il rispetto ha pagato con la sua vita la lotta che conduceva nel suo paese, l'Uganda, contro un progetto di legge iniquo, che prevede la pena di morte per gli omosessuali, per il solo motivo di essere omosessuali". Per il responsabile socialista, "questo barbaro omicidio s'iscrive in un contesto particolarmente pesante. In Uganda, come in troppi Paesi del mondo, l'omosessualità è ancora, nel 2011, repressa con durezza".  
"Davanti a tanta disumanità e intolleranza, è indispensabile che si facciano sentire gli spiriti illuminati, perché i diritti inalienabili dell'uomo possano essere riconosciuti nella loro universalità", ha aggiunto.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

BUZEK, GOVERNO CANCELLI LEGGI DISCRIMINATORIE E FACCIA INCHIESTA (ANSA) - BRUXELLES, 27 GEN - La morte di David Kato, l'attivista per i diritti dei gay in Uganda, non deve restare impunita. Il governo ugandese deve rispettare le convenzioni internazionali, cancellare la legge che definisce l'omosessualita' un crimine e deve aprire un'inchiesta sull'assassinio. Lo ha chiesto con una dichiarazione scritta il presidente del Parlamento europeo, Jerzy Buzek.  
'David Kato - - ha dichiarato - era un uomo che combatteva per il diritto delle persone a vivere in liberta' in Uganda, indipendentemente dal loro orientamento sessuale. Chiedo che i colpevoli della sua uccisione vengano portati davanti alla giustizia. Kato era un eccezionale difensore dei diritti umani'.  
'Disapprovo che l'Uganda sia un paese in cui l'omosessualita' e' ancora considerata un atto criminale - ha aggiunto Buzek - La battaglia di Kato era volta ad aumentare la tolleranza sulla sessualita', alla difesa dei diritti di lesbiche, gay, bisessuali e trans, nonche' alla protezione delle minoranze vulnerabili'.  
Buzek ha quindi ricordato che il Parlamento europeo 'ha gia' adottato due risoluzioni contro la bozza legislativa anti-omosessuale'. 'Sottolineo - ha aggiunto il presidente dell'assemblea elettiva europea che l'orientamento sessuale e' materia che ricade nel diritto individuale alla privacy nel quadro dei diritti umani universali ed in quello delle liberta' fondamentali'.  
'Ribadisco il richiamo del Parlamento europeo alle autorita' ugandesi - ha concluso Buzek - affinche' non approvino la nuova legislazione e rivedano quella esistente in modo da depenalizzare l'omosessualita' e ricordo al governo ugandese i suoi obblighi verso le leggi internazionali e verso l'accordo di Cotonou, che chiama al rispetto dei diritti umani universali'.  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

PANNELLA: DAVID KATO, ONORATO PER LA SUA VITA E PER LA BATTAGLIA IDEALE CHE LO HA PORTATO AD ESSERE ASSASSINATO, COMMEMORATO UFFICIALMENTE DAL PRESIDENTE DEL PARLAMENTO EUROPEO JERZY BUZEK. SEGNALO QUESTO E ALTRO ALLE MASSIME AUTORITÀ  DI ROMA, MAGARI DAI DUE LATI DEL TEVERE   
   
Roma, 27 gennaio 2011  
Dichiarazione di Marco PannellaLa bella, profonda, così tempestiva dichiarazione scritta del presidente del Parlamento Europeo Jerzy Buzek, per deplorare ufficialmente e protestare contro il regime ugandese per l’assassinio del compagno radicale di Certi Diritti, David Kato, è un segnale felicemente contraddittorio rispetto al desolato deserto europeo nel quale, purtroppo, tutti noi possiamo constatare di vivere.Mi permetterei di sottolineare anche alle massime autorità della nostra Repubblica che - ancorché si parli di un Radicale e quindi, secondo il nostro regime, da clandestinizzarese possibile anche in morte - una loro parola sulla morte di David Kato onorerebbe non solo la memoria di questo nostro compagno, che di per sé non ne necessita, ma la Repubblica stessa, ancorché sarebbe evidente solo se si procedesse a una Tac del regime nostrano. E visto che ci siamo, vorremmo segnalare alla loro attenzione che un altro nostro compagno, Biram Dah Abeid, consigliere generale del Partito Radicale Nonviolento, è da settimane in carcere in Mauritania, colpevole di un'azione contro la realtà schiavistica che riguarda almeno il 25 per cento di quella popolazione, e che riteniamo naturalmente in grave pericolo.  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Statement by the President Obama on the Killing of David Kato

I am deeply saddened to learn of the murder of David Kato.  In Uganda, David showed tremendous courage in speaking out against hate.  He was a powerful advocate for fairness and freedom.  The United States mourns his murder, and we recommit ourselves to David’s work.  
At home and around the world, LGBT persons continue to be subjected to unconscionable bullying, discrimination, and hate.  In the weeks preceding David Kato’s murder in Uganda, five members of the LGBT community in Honduras were also murdered.  It is essential that the Governments of Uganda and Honduras investigate these killings and hold the perpetrators accountable.  
LGBT rights are not special rights; they are human rights.  My Administration will continue to strongly support human rights and assistance work on behalf of LGBT persons abroad.  We do this because we recognize the threat faced by leaders like David Kato, and we share their commitment to advancing freedom, fairness, and equality for all.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

STATEMENT BY SECRETARY HILLARY CLINTON

Murder of Ugandan LGBT Activist David Kato  
 We are profoundly saddened by the loss of Ugandan human rights defender David Kato, who was brutally murdered in his home near Kampala yesterday.  Our thoughts and prayers are with his family, friends, and colleagues.  We urge Ugandan authorities to quickly and thoroughly investigate and prosecute those responsible for this heinous act.  
 David Kato tirelessly devoted himself to improving the lives of others.  As an advocate for the group Sexual Minorities Uganda, he worked to defend the rights of lesbian, gay, bisexual, and transgender individuals.  His efforts resulted in groundbreaking recognition for Uganda's LGBT community, including the Uganda Human Rights Commission's October 2010 statement on the unconstitutionality of Uganda's draft "anti-homosexuality bill" and the Ugandan High Court's January 3 ruling safeguarding all Ugandans' right to privacy and the preservation of human dignity.  His tragic death underscores how critical it is that both the government and the people of Uganda, along with the international community, speak out against the discrimination, harassment, and intimidation of Uganda's LGBT community, and work together to ensure that all individuals are accorded the same rights and dignity to which each and every person is entitled.  
 Everywhere I travel on behalf of our country, I make it a point to meet with young people and activists -- people like David -- who are trying to build a better, stronger future for their societies. I let them know that America stands with them, and that their ideas and commitment are indispensible to achieving the progress we all seek.  
 This crime is a reminder of the heroic generosity of the people who advocate for and defend human rights on behalf of the rest of us -- and the sacrifices they make.  And as we reflect on his life, it is also an occasion to reaffirm that human rights apply to everyone, no exceptions, and that the human rights of LGBT individuals cannot be separated from the human rights of all persons.  
 Our ambassadors and diplomats around the world will continue to advance a comprehensive human rights policy, and to stand with those who, with their courage, make the world a more just place where every person can live up to his or her God-given potential. We honor David’s legacy by continuing the important work to which he devoted his life.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

GAY/UGANDA. GRUPPO EVERYONE SU UCCISIONE ATTIVISTA DAVID KATO: “NOBILE DIFENSORE DEI DIRITTI UMANI, E’ UNA PERDITA PER LA COMUNITA’ INTERNAZIONALE”

Il co-presidente di EveryOne Matteo Pegoraro lo aveva conosciuto nel corso della Fifth Dublin Platform for Human Rights Defenders organizzata da Front Line ([www.frontlinedefenders.org](http://www.frontlinedefenders.org))

David Kato Kisule, uno dei principali attivisti per i diritti LGBT in Uganda, è stato barbaramente ucciso ieri pomeriggio, in circostanze ancora da chiarire, all’interno della sua abitazione, a Kampala. Il 16 ottobrescorso la rivista ugandese Rolling Stones aveva pubblicato in prima pagina la sua foto, assieme a quella di altri 99 attivisti omosessuali ugandesi, chiedendone l’arresto. “La scomparsa dell’amico David Kato è una perdita per la comunità internazionale e ci lascia con un vuoto nel cuore” commenta Matteo Pegoraro, co-presidente con Roberto Malini e Dario Picciau dell’organizzazione internazionale per i Diritti Umani EveryOne. “Avevo conosciuto David lo scorso febbraio, alla Fifth Dublin Platform for Human Rights Defenders organizzata a Dublino da Front Line, in collaborazione con l’Alto Commissario ONU per i Diritti Umani” spiega Pegoraro. “Eravamo 100 attivisti considerati a rischio di vita nel mondo: con me e David c’erano altre persone straordinarie che oggi non ci sono più, tra cui Bety Cariño, assassinata in Messico il 27 aprile 2010, e Georges Kanuma, leader della comunità LGBT in Burundi, scomparso per negligenze mediche sempre nell’aprile scorso. Negli occhi di David si intravedeva il dolore: un dolore radicato nel cuore per la sua gente, per coloro che ogni giorno vivevano sulla propria pelle le sevizie della discriminazione, della paura, dell’isolamento. Un dolore che era corroborato dalla voglia di giustizia, di libertà, di uguaglianza tra tutti gli esseri umani. I suoi occhi luccicavano di malinconia,” ricorda ancora l’attivista, “ma non nascondevano la forza di un uomo straordinario, pronto a dare la sua vita per tutelare i più deboli, pronto  sempre a privarsi di qualcosa per assistere i più bisognosi, pronto ad affrontare anche il più temibile degli avversari pur di difendere la propria opinione e così quella dell’altro”.  
“E’ necessario” concludono i tre co-presidenti di EveryOne, “che i difensori dei diritti umani a rischio di vita siano tutelati dalle istituzioni internazionali, e che il loro impegno e le loro azioni a difesa dei più deboli non siano ostacolate o contrastate dai Governi. La morte di David, per cui ci auguriamo sia fatta giustizia, ci insegna che è quanto mai necessario continuare a lottare per l’uguaglianza e la libertà di tutti gli esseri umani: per questo non smetteremo di chiedere a gran voce che in Uganda, come in tutti gli Stati, tra cui la nostra Italia, in cui i diritti delle minoranze vengono calpestati, si abbandonino la discriminazione e la violenza, e si avviino percorsi reali di integrazione e sensibilizzazione, partendo dal rispetto dei diritti fondamentali di ognuno”.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

(ANSA) - ROMA, 27 GEN - Arcigay condanna il 'brutale omicidio di David Kato, un militante gay ugandese ucciso a martellate', e chiede che il 'Governo italiano adotti tutte le misure necessarie affinche' il governo ugandese interrompa la campagna d'odio contro gli omosessuali promossa da estremisti religiosi'.  
'Abbiamo avuto l'onore di conoscere personalmente David - ha detto Paolo Patane', presidente Arcigay - e il suo immenso contributo per la battaglia dei diritti umani nel mondo. Kato, nel dicembre scorso, e' stato ospite dell'associazione radicale Certi diritti e aveva rilasciato una lunga intervista al mensile gay 'Pride' che spiegava in questi termini la situazione ugandese: 'L'anno scorso i pastori evangelici degli Stati Uniti, in nome della protezione della famiglia tradizionale, hanno diffuso nel mio paese odio e omofobia. Cio' ha portato a una serie di conseguenze: dalle vessazioni agli arresti, fino ad arrivare all'attuale proposta di legge contro le persone omosessuali'.  
Arcigay, proprio oggi, condanna in decine di piazze italiane per il Giorno della memoria l'odio e la barbarie che vide migliaia di omosessuali internati e uccisi dal nazi-fascismo: 'Quell'odio e' il medesimo che ancora oggi, con il nome di omofobia brutalizza omosessuali, lesbiche e transessuali in molti paesi del mondo. La lotta all'omofobia dovrebbe essere patrimonio comune di tutti i Governi che non possono chiedere alle persone omosessuali di continuare a vivere e morire da eroi'. (ANSA).

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Comunicato stampa - ArciLesbica - 28 gennaio 2011  
Apprendiamo con grande tristezza che l'attivista ugandese per i diritti LGBT David Kato è stato assassinato a Kampala.  
Nell'ottobre scorso il suo nome era comparso insieme a quelli di altri attivisti sulla prima pagina della rivista "Rolling Stone"sotto un titolo che istigava all'omicidio di gay e lesbiche.  
Vi sono luoghi al mondo dove la macchina del fango si trasforma in macchina del sangue. Luoghi dove non è necessario racimolare il coraggio per il coming out in famiglia, ma per resistere ai linciaggi.  
In Uganda l'intreccio criminale tra fondamentalismo religioso e complicità delle istituzioni, che trovano nelle persone LGBT un comodo capro espiatorio per l'odio sociale prodotto da condizioni di vita proibitive, fa di questo paese uno dei più pericolosi per la nostra vita.  
Uniamo la nostra voce a quella delle altre associazioni e della comunità internazionale affinchè il governo ugandese sia costretto ad assumersi le sue responsabilità e a far cessare le violenze contro gay e lesbiche,se necessario a costo di sanzioni.  
Il presente comunicato viene scritto nel giorno della memoria, che è anche il giorno in cui si celebrano le vittime omosessuali del nazifascismo.  
Dispiace dover affermare che l'Omocausto non è cessato, ma prosegue nel mondo, in Uganda come in Iran e ovunque venga nutrito l'odio e mantenute le discriminazioni contro gay, lesbiche, transessuali.

Ostilia Mulas  
Segreteria Nazionale ArciLesbica  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

UGANDA: UCCISO ATTIVISTA GAY; CGIL, DOLORE E SDEGNO  
(ANSA) - ROMA, 27 GEN - La Cgil 'si unisce al dolore e allo sdegno degli attivisti per i diritti umani e civili di tutto il mondo, e in particolare delle associazioni gay, lesbiche e trans, per la morte violenta di David Kato Kisule, esponente di rilievo dell'organizzazione Sexual Minorities Uganda'.  
'In una recente visita a Roma, invitato al Congresso dell'Associazione Radicale Certi Diritti, David Kato Kisule aveva testimoniato delle persecuzioni di cui sono vittime le persone trans, lesbiche e gay in Uganda, da parte di organizzazioni del fondamentalismo religioso i cui predicatori trovano terreno fertile tra la popolazione che vive nella miseria e nella disperazione', ricorda Maria Gigliola Toniollo, responsabile del settore Nuovi diritti della Cgil Nazionale.  
L'attivista per i diritti umani e civili assassinato, prosegue Toniollo, 'aveva raccontato di come in Africa e in particolare nel suo Paese, la situazione fosse sempre piu' pericolosa per gli attivisti omosessuali, tanto che durante le udienze in Tribunale egli era protetto da volontari delle Ong internazionali e difeso da diplomatici di ambasciate occidentali che lo avevano salvato da diversi tentativi di linciaggio'.  
La Cgil sottolinea come 'nonostante un clima tanto rischioso e carico di odio tuttavia David, con molto coraggio e determinazione, aveva avviato una iniziativa legale contro la rivista Rolling Stones e lo scorso 7 gennaio l'Alta Corte ugandese aveva condannato la rivista, difendendo le persone gay perseguitate. David era stato ascoltato anche dalla Sottocommissione Diritti Umani del Parlamento Europeo e, mentre molte Ong internazionali si mobilitavano in diversi paesi del mondo, il Parlamento Europeo approvava una Risoluzione di condanna nei confronti delle violenze in Uganda'. In conclusione la Cgil 'si unisce alla richiesta promossa da Smug (Sexual Minorities Uganda), Human Rights Watch, Global Rights, Global LGBT Advocacy, Npwj, e altre Ong, affinche' il Governo protegga gli attivisti in pericolo in Uganda e si avvii - conclude la nota - quanto prima un dialogo con le autorita' e le organizzazioni politiche e religiose del Paese'.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

UGANDA, GAYLIB: OMOSESSUALI VITTIME DEL FONDAMENTALISMO  ISLAMICO E CRISTIANO UNITI NELL'OMOFOBIA

L’associazione di centrodestra sgomenta per l’uccisione di David Kato Kisule,esponente del movimento gay ugandese. GayLib vicina a Certi Diritti”.

“Siamo costernati per la scomparsa dell’esponente del movimento gay ugandese David Kato  Kisule, ucciso ieri in circostanze ancora da chiarire presso la sua abitazione di Kampala: la sua morte è solo l’ultima e la più alta prova dell’odio omofobico che vige nel paese africano, dove il fondamentalismo cristiano non ha nulla da invidiare a quello islamico”. Lo affermano in una nota Gabriel Refatti e Luca Maggioni, rispettivamente responsabile esteri e vicepresidente di GayLib, i quali manifestano a Certi Diritti e al movimento gay ugandese la propria vicinanza: “Certi Diritti, l’associazione radicale gay, ci aveva fatto conoscere David grazie alla sua partecipazione al congresso dell’associazione ed in quell’occasione avevamo potuto toccare con mano l’odio che viene riservato ai gay in Uganda, tanto che la sua foto era fra le cento dei gay che il quotidiano di Kampala Rolling Stone aveva pubblicato con l’invito ad ucciderli”.  
“Siamo certi – concludono i due – che l’impegno di Kisule non si fermerà con la sua uccisione, ma continuerà nelle azioni di lotta per la libertà e la giustizia che il movimento gay ugandese e mondiale porteranno caparbiamente avanti”.

Luca Maggioni (Vicepresidente GayLib) – 347.4623142

Gabriel Refatti (Responsabile esteri GayLib) - 348.8555377

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

GRUPPO IL GUADO SU MORTE DAVID KATO  
Roma, 28 gennaio 2011  
Sono andati a prenderlo in casa dopo che una rivista che si proclama cristiana aveva pubblicato la sua foto accusandolo, in quanto omosessuale, di «promuovere un complotto che ha come finalità la corruzione di un milioni di bambini che hanno meno di dodici anni».  
L'hanno ammazzato fracassandogli la testa e la polizia di un governo condizionato dalle numerose chiese evangelicali statunitensi che hanno fatto dell'Uganda un terreno di conquista ha detto che la causa dell'aggressione è un tentativo di furto.  
L'hanno ammazzato dopo che era rientrato in Uganda da un viaggio in Europa dove aveva denunciato il clima di intolleranza e di violenza che si sta creando intorno agli omosessuali del suo paese.  
Nelle numerose interviste che ha concesso durante il suo viaggio più di una volta gli hanno chiesto perché rientra nel suo paese. La risposta è sempre stata disarmante: «Il mio compito è quello di restare nel mio paese per combattere l'omofobia».  
Una lotta per l'amore e contro l'odio.  
Una lotta per il rispetto e contro la violenza.  
Una lotta per la difesa di chi è più debole contro l'intolleranza e gli interessi delle grandi associazioni religiose statunitensi che sfogano nei paesi del terzo molto la loro incapacità di cogliere il vero messaggio che ci viene dal Vangelo.  
Una lotta di cui David Kato Kisule è un martire.  
A lui e a tutti i martiri che vivono sulla loro pelle le conseguenze dell'omofobia vorrei che dedicassimo le nostre veglie di quest'anno.  
Per lui e per tutti gli altri martiri che vivono sulla loro pelle le conseguenze dell'omofobia dobbiamo iniziare un cammino di conversione chiedendoci fin da ora cosa possiamo fare, nel concreto, per far si che episodi come la sua morte non avvengano più.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_