---
title: 'Manifesto e Mozione generale del I Congresso 2008'
date: Thu, 28 Feb 2008 13:43:25 +0000
draft: false
tags: [Politica]
---

### I Congresso dell'Associazione Radicale Certi Diritti

#### **Manifesto e Mozione** generale approvati il 1 marzo 2008

Noi sottoscritti,

  
• Riteniamo che l'amore tra due persone sia un'esperienza fondamentale e caratterizzante della vita di tutti i cittadini al di là di qualsiasi morale o religione. Per questo motivo rivendichiamo un "Amore civile", che sia posto al centro di leggi che tutelino e garantiscano le istanze e i diritti delle persone che vivono oggi una identità di genere e un orientamento sessuale soggette a discriminazioni sociali, giuridiche e politiche.

• Consideriamo la libera espressione della personalità in base al genere, all'identità, ai comportamenti e alle scelte sessuali una condizione essenziale che ciascun Paese dell'Unione Europea laica e democratica deve garantire, impedendo ogni forma di discriminazione.

• Siamo consapevoli della rilevanza che i principi di uguaglianza e pari dignità sociale, sanciti dalla Costituzione Italiana, dalla Carta dei Diritti Fondamentali dell'Unione Europea e dalle Convenzioni internazionali hanno per il libero e pieno sviluppo delle persone.

Denunciamo la gravità della situazione italiana:

• In Italia si perpetuano discriminazioni e compressioni dei diritti umani e civili fondate sull'identità di genere e sull'orientamento sessuale che compromettono il diritto ad una piena cittadinanza, da parte di chi tali discriminazioni subisce, giacché ai doveri non corrispondono le necessarie tutele da parte dell'ordinamento statuale;

• La sfera affettiva è quella dove le discriminazioni si fanno evidenti e odiose: pur in assenza di un divieto espresso, di fatto in Italia viene impedito l'accesso all'istituto matrimoniale per le coppie formate da persone dello stesso sesso, l'accesso, per le stesse, all'adozione e alla fecondazione medicalmente assistita, oltre che alcune forti limitazioni nel godimento dei diritti genitoriali;

• Non esistono istituti giuridici che riconoscano e tutelino ufficialmente forme di unione familiare diverse da quella fondata sul matrimonio;

• Nonostante i richiami delle istituzioni europee, l'Italia non ha ancora adottato una legislazione in materia di prevenzione e contrasto della discriminazione e della violenza omofobica e transfobica.

Il Congresso dell'Associazione, ritiene dunque urgente:

• promuovere forme di lotta non violenta per stimolare un dibattito all'interno del nostro Paese sulla necessità di rimuovere ogni forma di discriminazione, sostenendo tutte le iniziative politiche, sociali e culturali che rafforzino la coscienza civile e politica sui temi dei diritti civili;

• creare una rete di informazione per la divulgazione a tutti i livelli delle proposte legislative e dei processi giudiziari per il riconoscimento di nuovi diritti;

• cercare collaborazioni con le realtà che operano per il riconoscimento e l'affermazione dei diritti delle persone che vivono una identità di genere e un orientamento sessuale soggette a discriminazioni sociali, giuridiche e politiche, per consolidare e appoggiare gli obiettivi comuni, condividendo con gli stessi soggetti aspirazioni inclusive e laiche, fondate sulla parità dei diritti proprie di un'Europa che già esiste e che anche noi vogliamo.

Inoltre, impegna gli iscritti:

• a partecipare attivamente ai lavori della Conferenza Permanente per la riforma del Diritto di Famiglia con particolare attenzione alle tematiche lgbt (lesbiche, gay, bisessuali,transgender);

• a diffondere il Manifesto per l'eguaglianza dei diritti (www.matrimoniodirittogay.it) e forme di collaborazione con le associazioni lgbt;

• a collaborare strettamente con l'Avvocatura per i Diritti GLBT - "Rete Lenford", promuovendone e diffondendone l'attività, tesa a formare professionisti del settore nell'ottica di una cultura dell'azione legale in difesa e promozione dei diritti civili;

• a promuovere forme di collaborazione con l'Associazione Luca Coscioni volte alla promozione della libertà di ricerca scientifica contro i dogmi, gli integralismi e i fanatismi ideologici e religiosi;

• a promuovere a Roma una giornata dedicata all'amore civile pe ril 12 maggio prossimo;

• a promuovere una azione collettiva di affermazione dei diritti di uguaglianza.

Dà mandato, infine, agli organi di rigenti di preparare proposte di legge sui temi che formano il centro dell'iniziativa dell'Associazione, da proporre nella prossima legislatura e per uan campagna di iniziativa popolare.

Roma, il 1 marzo 2008