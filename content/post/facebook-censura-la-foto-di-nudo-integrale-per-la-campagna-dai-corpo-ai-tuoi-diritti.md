---
title: 'Facebook censura la foto di nudo integrale per la campagna ''Dai corpo ai tuoi diritti'''
date: Tue, 10 Jul 2012 11:52:13 +0000
draft: false
tags: [Politica]
---

Roma, 10 luglio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti.

  
Lo scorso 26 giugno l’Associazione Radicale Certi Diritti ha lanciato la campagna “Dai corpo ai tuoi diritti” ([http://www.flickr.com/photos/certidiritti/sets/72157630288308052/with/7443467088/](http://www.flickr.com/photos/certidiritti/sets/72157630288308052/with/7443467088/) e [http://www.facebook.com/media/set/?set=a.10151038148445973.482151.116671870972&](http://www.facebook.com/media/set/?set=a.10151038148445973.482151.116671870972&);type=3) con l'obiettivo di portare l'attenzione sui temi per i quali si batte da anni:  matrimonio tra persone dello stesso sesso e riforma del diritto di famiglia, regolamentazione della prostituzione, diritti delle persone trans e intersessuali, affettività per i detenuti, sessualità e disabilità. Temi che possono essere riassunti nella lotta alla sessuofobia e che hanno visto l’adesione anche di politici e artisti.

La campagna ha avuto un grande successo sui social network e decine di persone hanno deciso inviarci le loro immagini. Tra queste Giorgio Lorenzo Codibue ci ha inviato una foto di nudo integrale ([http://www.flickr.com/photos/certidiritti/7535237388/in/set-72157630288308052](http://www.flickr.com/photos/certidiritti/7535237388/in/set-72157630288308052)) che abbiamo deciso di pubblicare poiché crediamo nella libertà di espressione di ciascuno e un corpo nudo, senza volgarità, esprime al meglio la richiesta radicale di diritti nudi e crudi, non travestiti da elemosina concesse a dei sudditi. Di più: il corpo è la posta in gioco nell’agone politico da sempre: sui corpi si inscrivono le violenze e i soprusi e dunque bisogna partire da corpi più liberi e consapevoli per affrancarsi dai pregiudizi e dalle ideologie patriarcali e paternalistiche.

La pubblicazione è avvenuta ieri anche in seguito alla veste che ha ultimamente indossato la sessuofobia italica: in almeno tre spiagge italiane, dove da decenni si pratica il naturismo, le forze dell’ordine, in alcuni casi accompagnate da Vigilantes armati di agenzie private, impongono ai bagnanti di mettersi il costume ([http://www.certidiritti.it/notizie/comunicati-stampa/item/1522-forze-dellordine-con-supporto-guardie-private-armate-si-accaniscono-contro-chi-pratica-naturismo-governo-chiarisca](http://www.certidiritti.org/2012/07/03/forze-dellordine-con-supporto-guardie-private-armate-si-accaniscono-contro-chi-pratica-naturismo-governo-chiarisca/)).

In meno di 24 ore la foto è diventata virale su Facebook stimolando un dibattito molto interessante sui diritti, il pudore, la comunicazione e la morale. Il censore, tuttavia, ha colpito anche la realtà virtuale. Da qualche minuto al posto della foto incriminata si legge la seguente dicitura: Attachment unavailable. This attachment may have been removed or the person who shared it may not have permission to share it with you”.

Il regolamento di Facebook parla chiaro: “Non pubblicare contenuti: minatori, pornografici, con incitazioni all'odio o alla violenza, con immagini di nudo o di violenza forte o gratuita”. Essendo uno spazio privato i gestori di Facebook possono scegliere le policies che credono, anche delle policies moraliste e bigotte che per censurare la violenza ritengono necessaria la qualifica aggiuntiva “forte o gratuita”, mentre il nudo viene censurato a priori, anche nel caso della foto della campagna “Dai corpo ai tuoi diritti” dall’evidente alto valore morale e artistico.

Consigliamo ai censori nostrani e virtuali la visione dello spettacolo “Il censore” di Anthony Neilson per scoprire che i corpi nudi parlano il linguaggio dell’autenticità al quale la politica farebbe bene ritornare. Chiediamo quindi a tutti di continuare a iscriversi all’Associazione Radicale Certi Diritti e a mandarci le loro foto, anche di nudo integrale, che continueremo a pubblicare. A breve stamperemo altre cartoline con le foto che saranno piaciute di più in rete.  
  
**[SOSTIENI LE CAMPAGNE DELL'ASSOCIAZIONE >>>](partecipa/iscriviti)**