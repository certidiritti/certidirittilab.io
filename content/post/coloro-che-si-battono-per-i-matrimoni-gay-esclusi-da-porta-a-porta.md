---
title: 'COLORO CHE SI BATTONO PER I MATRIMONI GAY ESCLUSI DA PORTA A PORTA'
date: Wed, 18 Mar 2009 09:58:14 +0000
draft: false
tags: [Comunicati stampa]
---

**MATRIMONI E UNIONI GAY: L’ASSOCIAZIONE RADICALE CERTI DIRITTI E I PARLAMENTARI RADICALI ESCLUSI DALLA TRASMISSIONE 'PORTA A PORTA'. **

Roma, 18 marzo 2009  
Dichiarazione di Marco Perduca, Senatore Radicale nel gruppo del Pd e Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

"Non fa certamente notizia il fatto che nella puntata di ieri sera di Porta a Porta si sia parlato negativamente di matrimonio gay e, ovviamente, siano stati eslcuse le voci delle uniche realtà  che in Italia si battono per questo obiettivo. L’Associazione Radicale Certi Diritti da diversi mesi ha promosso in Italia una campagna di ‘Affermazione civile’ per estendere l’accesso all’istituto del matrimonio alle coppie gay, come avviene ormai in Spagna, Belgio, Olanda e in altri paesi democratici.

  
I parlamentari radicali del Pd, insieme alla deputata Paola Concia, hanno depositato a inizio Legislatura una proposta di legge in favore del matrimonio tra persone gay.

  
Ieri sera alla Terza Camera di Bruno Vespa si e' parlato di un tema preciso senza far parlare coloro che su quell’argomento promuovono campagne, iniziative e azioni nonviolente. Scriveremo alla Commissione di vigilanza, al garante per le comunicazioni e direttamente alla Rai per denunciare, ancora una volta, il silenziamento del quale sono oggetto I Radicali.