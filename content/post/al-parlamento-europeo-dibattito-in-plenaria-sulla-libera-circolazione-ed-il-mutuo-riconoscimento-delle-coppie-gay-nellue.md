---
title: 'Al Parlamento Europeo dibattito in plenaria sulla libera circolazione ed il mutuo riconoscimento delle coppie gay nell''UE'
date: Wed, 08 Sep 2010 07:33:47 +0000
draft: false
tags: [Comunicati stampa, coppie gay, libera circolazione, mutuo riconoscimento, parlamento europeo, UE, Viviane Reding]
---

**CERTI DIRITTI PLAUDE LA COMMISSARIA EUROPEA REDING PER LE FUTURE INIZIATIVE IN DIFESA DELLE COPPIE DELLO STESSO SESSO. LA FIGURACCIA DELL'ITALIA: PDL E LEGA GLI UNICI CONTRO PERCHE' LA "FAMIGLIA E' COMPOSTA SOLO DA COPPIE UOMO-DONNA".**

**L'AUDIO E VIDEO DEGLI INTERVENTI**

Bruxelles, 8 settembre 2010

**Il Parlamento europeo ha dibattuto ieri sulla base delle interrogazioni orali depositate dai gruppi comunista, liberale, verde e socialista sulla situazione delle coppie dello stesso sesso nella UE. L'Italia non ha mancato di fare la sua bella figuraccia grazie a Pdl e Lega.**

Le interrogazioni chiedevano alla Commissione europea quali iniziative avrebbe preso per cancellare le discriminazioni nella UE tra coppie dello stesso sesso e di sesso diverso, per assicurare il mutuo riconoscimento in tutti gli Stati dell'Unione delle unioni civili e dei legami matrimoniali tra persone dello stesso sesso contratti in uno Stato UE che li permettono, assicurando quindi la libera circolazione delle coppie gay. Il gruppo liberale ha anche chiesto - sulla base dell'interrogazione redatta dal responsabile delle questioni europee dell'Associazione Radicale Certi Diritti Ottavio Marzocchi - alla Commissione di elaborare una Roadmap (tabella di marcia) contro l'omofobia e per i diritti LGBT, con una serie di misure a livello europeo ed internazionale.

**Nel corso del dibattito tutti i deputati di tutti i paesi europei si sono pronunciati a favore di tali misure, tranne i deputati italiani del PdL e della Lega, che hanno difeso la famiglia "tradizionale" composta da un uomo ed una donna, al fine della riproduzione.** Vari deputati favorevoli ai diritti LGBT li hanno interrogati chiedendo loro se non ritengano necessario promuovere il rispetto dei diritti umani nella UE, l'applicazione delle norme europee e dell'eguaglianza e sulla reale volontà della popolazione italiana rispetto alla maggioranza parlamentare ed al governo.

La Commissaria Viviane Reding ha affermato che sta già sollevando con gli Stati UE, nel quadro delle discussioni sulla corretta applicazione della direttiva sulla libera circolazione, la questione dell'eguaglianza e della proibizione della discriminazione sulla base dell'orientamento sessuale negli Stati membri: una coppia dello stesso sesso unita in matrimonio o unione civile deve essere riconosciuta da tutti gli Stati membri ai fini del diritto di circolazione e di residenza. Reding ha inoltre detto che vuole promuovere il principio per cui i cittadini UE e le loro famiglie in generale possano ottenere il riconoscimento dello status acquisito nel paese A (matrimonio, civil partnership) nel paese B (per es. l'Italia), creando un riconoscimento del matrimonio o della partnership dello stesso sesso in tutti gli stati europei, sulla base del principio di eguaglianza e di non discriminazione. La Commissaria ha anche affermato che i governi non possono discriminare, che sono spesso più prudenti delle loro popolazioni e che insisterà perché il diritto europeo sia applicato, pena di dovere applicare misure più severe (come portare gli Stati membri davanti alla Corte di Giustizia). La Commissaria ha inoltre chiesto all'Agenzia per i Diritti Fondamentali di aggiornare il suo studio sull'omofobia nella UE al fine di fornire maggiori informazioni alla Commissione al riguardo per meglio lottare contro le discriminazioni.

Certi Diritti alle affermazioni della Commissaria Reding e chiede alla Commissione di includere tali proposte nel programma di lavoro della Commissione al più presto e nella Roadmap per i diritti LGBT richiesta dal gruppo liberale.

L'audio video degli interventi del dibattito al PE sono disponibili su: [http://www.europarl.europa.eu/sed/speeches.do?sessionDate=20100907](http://www.europarl.europa.eu/sed/speeches.do?sessionDate=20100907)

Le interrogazioni orali dei gruppi politici sono disponibili su:

ALDE: [http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+OQ+O-2010-0118+0+DOC+XML+V0//IT](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+OQ+O-2010-0118+0+DOC+XML+V0//IT)

Comunisti, Verdi ed altri deputati: [http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+OQ+O-2010-0081+0+DOC+XML+V0//IT](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+OQ+O-2010-0081+0+DOC+XML+V0//IT)

Socialisti: [http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+OQ+O-2010-0117+0+DOC+XML+V0//IT](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+OQ+O-2010-0117+0+DOC+XML+V0//IT)

* * *

22:48:20 > 23:28:15

Discrimination of same-sex married or in civil-partnership couples

Speaker

Function

Duration

Start

End

Language

Resources

Download

Angelilli Roberta

PT

00:01:05

22:48:20

22:49:26

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283892505911 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_22482000_22492600.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_22482000_22492600.wmv "Download the WMV flow")

de Jong Cornelis

Auteur

00:02:30

22:49:09

22:51:40

NL

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283892554304 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_22490900_22514000.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_22490900_22514000.wmv "Download the WMV flow")

Angelilli Roberta

PT

00:00:14

22:51:13

22:51:27

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283892678512 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_22511300_22512700.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_22511300_22512700.wmv "Download the WMV flow")

Cornelissen Marije

Auteur

00:02:32

22:51:19

22:53:52

NL

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283892684789 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_22511900_22535200.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_22511900_22535200.wmv "Download the WMV flow")

Angelilli Roberta

PT

00:00:15

22:53:26

22:53:42

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283892811512 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_22532600_22534200.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_22532600_22534200.wmv "Download the WMV flow")

Cashman Michael

Auteur

00:02:55

22:53:33

22:56:29

EN

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283892818547 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_22533300_22562900.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_22533300_22562900.wmv "Download the WMV flow")

Angelilli Roberta

PT

00:00:17

22:56:02

22:56:20

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283892967723 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_22560200_22562000.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_22560200_22562000.wmv "Download the WMV flow")

in 't Veld Sophia

Auteur

00:03:23

22:56:11

22:59:35

EN

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283892976440 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_22561100_22593500.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_22561100_22593500.wmv "Download the WMV flow")

Angelilli Roberta

PT

00:00:19

22:59:08

22:59:27

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283893153105 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_22590800_22592700.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_22590800_22592700.wmv "Download the WMV flow")

Reding Viviane

Commissioner

00:09:48

22:59:17

23:09:06

EN

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283893162937 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_22591700_23090600.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_22591700_23090600.wmv "Download the WMV flow")

Angelilli Roberta

PT

00:00:26

23:08:40

23:09:06

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283893725363 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_23084000_23090600.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_23084000_23090600.wmv "Download the WMV flow")

Iacolino Salvatore

au nom du groupe

00:02:43

23:08:56

23:11:39

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283893741550 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_23085600_23113900.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_23085600_23113900.wmv "Download the WMV flow")

Angelilli Roberta

PT

00:00:19

23:11:13

23:11:33

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283893878767 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_23111300_23113300.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_23111300_23113300.wmv "Download the WMV flow")

Flašíková Benová Monika

au nom du groupe

00:02:33

23:11:22

23:13:55

SK

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283893887535 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_23112200_23135500.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_23112200_23135500.wmv "Download the WMV flow")

Angelilli Roberta

PT

00:00:21

23:13:29

23:13:51

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283894014318 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_23132900_23135100.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_23132900_23135100.wmv "Download the WMV flow")

Ludford Sarah

au nom du groupe

00:03:23

23:13:41

23:17:05

EN

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283894026924 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_23134100_23170500.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_23134100_23170500.wmv "Download the WMV flow")

Angelilli Roberta

PT

00:00:42

23:16:17

23:17:00

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283894182696 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_23161700_23170000.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_23161700_23170000.wmv "Download the WMV flow")

Lunacek Ulrike

Verts/ALE

00:02:41

23:16:48

23:19:30

EN

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283894213175 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_23164800_23193000.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_23164800_23193000.wmv "Download the WMV flow")

Angelilli Roberta

PT

00:00:18

23:19:03

23:19:22

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283894348263 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_23190300_23192200.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_23190300_23192200.wmv "Download the WMV flow")

Szymanski Konrad

ECR

00:01:39

23:19:11

23:20:50

PL

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283894356061 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_23191100_23205000.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_23191100_23205000.wmv "Download the WMV flow")

Angelilli Roberta

PT

00:00:22

23:20:24

23:20:46

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283894429168 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

[![ISMA version available](http://www.europarl.europa.eu/sed/img/icon_quicktime.gif "The ISMA version is available")](http://vod.europarl.europa.eu/nasvod02/vod0409/2010/isma/VODUnit_20100907_23202400_23204600.mp4 "Download the ISMA flow") [![WMV version available](http://www.europarl.europa.eu/sed/img/icon_windowsmedia.gif "The WMV version is available")](http://vod.europarl.europa.eu/nasvod01/vod0409/2010/wm/VODUnit_20100907_23202400_23204600.wmv "Download the WMV flow")

Svensson Eva-Britt

GUE/NGL

00:02:30

23:20:36

23:23:06

SV

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283894441564 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

  

Angelilli Roberta

PT

00:00:19

23:22:39

23:22:58

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283894564005 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

  

Rossi Oreste

EFD

00:01:41

23:22:49

23:24:30

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283894574338 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

  

Angelilli Roberta

PT

00:00:19

23:24:04

23:24:23

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283894649591 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

  

ROMEVA I RUEDA Raül

Verts/ALE

00:00:35

23:24:14

23:24:49

ES

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283894659550 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

  

Angelilli Roberta

PT

00:00:19

23:24:40

23:24:59

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283894685759 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

  

Rossi Oreste

EFD

00:01:00

23:24:48

23:25:49

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283894693780 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

  

Angelilli Roberta

PT

00:00:21

23:25:30

23:25:52

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283894735917 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

  

Rivellini Crescenzio

PPE

00:02:38

23:25:42

23:28:21

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283894747795 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")

  

Angelilli Roberta

PT

00:00:19

23:27:56

23:28:15

IT

[![Intervention in video format available](http://www.europarl.europa.eu/sed/img/icon_media_video.gif "Intervention in video format")](http://www.europarl.europa.eu/sides/getVod.do?mode=unit&language=EN&vodId=1283894881058 "Intervention in video format")

![This intervention is not available in audio format](http://www.europarl.europa.eu/sed/img/icon_media_audio_inactive.gif "This intervention is not available in audio format")

![This intervention is not available in Word format](http://www.europarl.europa.eu/sed/img/icon_media_text_inactive.gif "This intervention is not available in Word format")