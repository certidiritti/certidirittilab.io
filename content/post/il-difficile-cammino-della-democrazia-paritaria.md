---
title: 'Il difficile cammino della democrazia paritaria'
date: Thu, 28 Jul 2011 14:28:48 +0000
draft: false
tags: [Politica]
---

Marilisa D’Amico, Il difficile cammino della democrazia paritaria, Giappichelli Editore, Torino 2011, pp.254, 24,00 euro. Contiene una intervista a mò di presentazione ad Emma Bonino di Cristina Molinari.

Il testo della professoressa D’Amico, docente di diritto costituzionale presso la Statale di Milano, è di grande interesse per chi vuole affrontare, con animo scevro di pregiudizi (di qualsiasi natura essi siano) il tema della parità di accesso e delle cosiddette quote.  Si tratta infatti di analisi della normativa ma anche delel politiche e delle principali esperienze europee dove il tema della parità si incrocia, e sembra scontrarsi, con l’uguaglianza e la democrazia. Il volume è particolarmente interessante perché dedica una parte non piccola ad affrontare senso e uso dei bilanci di genere che, provenienti dalal cultura di genere di scuola anglosassone, stanno entrando anche nel nostro paese come strumenti di governo a tutti gli effetti.

Da leggere assolutamente, anche perché l’autrice è una delle poche accademiche italiane che si è confrontata, autenticamente e profondamente, con  l’iniziativa giudiziaria e politica nelle cori nazionali ed europee per la difesa dell’ integrità, della responsabilità e dell’autonomia delle donne, e non solo.