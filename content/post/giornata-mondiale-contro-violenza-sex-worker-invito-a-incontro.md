---
title: 'GIORNATA MONDIALE CONTRO VIOLENZA SEX WORKER: INVITO A INCONTRO'
date: Tue, 15 Dec 2009 13:26:13 +0000
draft: false
tags: [Comunicati stampa]
---

**Giovedì 17 dicembre 2009**

**Giornata Mondiale contro la violenza sulle Sex Workers**

In occasione della giornata mondiale contro la violenza sulle Sex Workers, che si celebra ogni anno il 17 dicembre, l’Associazione Radicale Certi Diritti, il Comitato per i Diritti Civili delle Prostitute e l’Associazione La Strega da Bruciare, terranno presso la sede dei radicali, in Via di Torre Argentina, 76 – Roma (III° Piano), dalle ore 15, una Conferenza Stampa – Incontro Pubblico

sulla condizione delle prostitute in Italia e sui temi relativi alle proposte di regolamentazione della prostituzione.

All’incontro parteciperanno:

Pia Covre, Presidente Comitato diritti civili delle prostitute;

Monica Rosellini, Presidente Associazione La Strega da Bruciare;

Sergio Rovasio, Segretario Associazione Radicale Certi Diritti

Donatella Poretti, Senatrice radicale - Pd

Mario Staderini, Segretario di Radicali Italiani;

Andrea Morniroli, Cantieri Sociali

Associazione On the Road

SexyShock;

Escort Sauvage;

MIT, Movimento identità transessuale;

Andrea Maccarrone, esponente della Comunità lgbt;

Franco Grilini, presidente Gaynet;

Emanuela Costa, gruppo Le Graziose di Genova;

Leila Deianis, Associazione Transessuali Libellula;

Durante l’incontro verranno distribuite spillette con la scritta “ANCHE IO SONO UNA PUTTANA”

Perché è importante celebrare la data del 17 dicembre, Giornata mondiale contro la violenza sulle sex workers:

Troppo spesso l’omicidio o la violenza su una Sex Worker viene indagato in maniera superficiale e priva di autentica indignazione. La condanna morale, la stigmatizzazione e la mancanza di riconoscimento del lavoro sessuale ha portato ad una generalizzata criminalizzazione di chi ha scelto questa attività anche se non infrange nessuna legge.

Questo impedisce di avere il controllo sul proprio lavoro e sulla propria vita, ci mette al margine della società e ci espone a rappresaglie e violenze inaudite.Questo diventa un terreno fertile su cui crescono sfruttamento incontrollato, abuso e costrizione, orari di lavoro inaccettabili, condizioni di lavoro insalubri, ripartizione ingiusta dei guadagni e irragionevoli restrizioni della libertà di movimento

Parlare di prostituzione e Sex Work in Italia è una faccenda veramente spinosa, tutti e tutte quelle che ne parlano sembrano "non sapere nulla di ciò di cui si sta parlando". Come se fossero investiti/e da un senso di legittimità diffusa a dire la propria, per altre/i, sulle loro spalle, senza pensare di interpellarle/i. Presupponendo che questi/e ultimi/e non abbiano una voce.

E  allora è stata prodotta una spilletta "anche io sono una puttana", riprendendola da un collettivo di donne catalane.  Piaceva questo slogan perché dava visibilità alla consapevolezza delle lavoratrici del sesso, al loro orgoglio di essere tali e al loro deciso rifiuto della "vergogna" comunemente associata al mestiere. Perche' alludeva alla necessità di non nascondersi, di non fare il gioco della doppia morale. Ma, al contempo, quello che ci affascinava, era il fatto di poter portare addosso quell'"epiteto" che ancora oggi - tutte le donne sanno - corrisponde ad un'offesa. "Puttana", ben lungi da designare una professione, e' innanzitutto qualcuna che "la da' via". Il sottotesto, ci fosse bisogno di spiegarlo, e' che la cosa peggiore che una donna puo' fare, e' avere una vita sessuale di cui disporre liberamente.

E allora il cerchio si chiude, siamo da capo, e ripartiamo da qui: se fare la puttana e' un lavoro, chi lo fa deve poter emergere come tale. Se puttane siamo tutte (perche' tutte vogliamo disporre liberamente della nostra vita sessuale) bene, allora: eccoci!  SIAMO TUTT* PUTTANE