---
title: 'Polizia: Ministro Interni cancella discriminazione contro coppie stesso sesso grazie a Certi Diritti e Oscad'
date: Tue, 09 Oct 2012 10:10:49 +0000
draft: false
tags: [Diritto di Famiglia]
---

Ora si proceda con la cancellazione della Circolare Amato che rende impossibile trascrzione dei matrimoni tra persone dello stesso sesso celebrati all'estero.

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Roma, 9 ottobre 2012

Il Ministero degli Interni ha modificato la Circolare del 14 maggio scorso del Dipartimento Pubblica sicurezza (N. 333-A/9807.E.1/3368-2012 del 14 maggio 2012) avente per oggetto: “Disciplina della mobilità a domanda del personale della Polizia di Stato dei ruoli di sovrintendenti, assistenti, e agenti, che aspirano a cambiare sede di servizio”. Nella circolare infatti, era scritto che “I punteggi previsti per le esigenze del nucleo familiare si intendono estesi alle analoghe esigenze per le eventuali famiglie di fatto, intendendosi per tale quella costituita da due persone di sesso diverso che convivono, more uxorio, coabitando stabilmente insieme agli eventuali figli naturali riconosciuti o dichiarati dall’uno o da ambedue nella sede per cui si richiede il trasferimento, ovvero in sede limitrofa a quest’ultima”.

L'Associazione radicale Certi Diritti insieme al Senatore radicale Marco Perduca ha scritto all’OSCAD ed all’UNAR, ed ha incontrato i vertici dell’OSCAD il 10 luglio scorso per mettere in evidenza quanto la Circolare era contraria al principio di uguaglianza e oggettivamente discriminatoria nei confronti delle coppie dello stesso sesso, soprattutto dopo la sentenza della Corte costituzionale del 2010 e della Corte di Cassazione del 2012.

Qualche giorno fa l’OSCAD ci ha comunicato che il Ministero ha cancellato la frase indicata, quindi accogliendo la nostra richiesta.

L'Associazione radicale Certi Diritti ringrazia l’OSCAD per l’intervento effettuato, e rilancia direttamente al Ministro degli Interni la richiesta urgente di ritirare la cosiddetta circolare Amato, datata 18 ottobre 2007 con la quale il Ministero dichiarava l’impossibilità di trascrivere matrimoni tra persone dello stesso sesso contratti all’estero per motivi di “ordine pubblico”. L’argomento non è mai stato fondato, ed alla luce delle sentenze della Cote costituzionale e della Corte di Cassazione oggi è completamente destituito di ogni fondamento.