---
title: 'MAR 12-5 A TORINO INCONTRO PUBBLICO SU: PERCHE'' NO UNIONI CIVILI?'
date: Sat, 09 May 2009 14:06:35 +0000
draft: false
tags: [Comunicati stampa]
---

  
**CONTRO TUTTE LE DISCRIMINAZIONI!**

**Perché il Consiglio regionale non discute il disegno di legge contro tutte le discriminazioni?**

**Il Coordinamento Torino Pride lgbt ne discute con la Presidente Bresso, il Presidente Gariglio, l’Assessore Manica, i Presidenti Burzi, Reschigna, i vice presidenti Bossuto e Lepri.** 

**Martedì 12 maggio 2009, ore 18**

**Sala dell’Antico Macello di Po, Via Matteo Pescatore 7, Torino** 

  
 

Il 20 giugno 2006 la Giunta regionale approva il Disegno di legge denominato “**Norme di attuazione della parità di trattamento e del divieto di ogni forma di discriminazione nelle materie di competenza regionale**”. Si tratta di un disegno volto a inserire nell’ordinamento regionale il principio di non discriminazione chiaramente esplicitato non solo nella normativa europea ma fortemente radicato anche nella costituzione e nell’ordinamento italiano. Rendendo esplicito il divieto di discriminazione, e promuovendo azioni positive per rimuoverne le cause, in  6 potenziali aree ove il potenziale di discriminazione è più alto: _religione e opinioni personali, origine etnica, genere, orientamento sessuale, età, disabilità._ 

Son passati quasi tre anni, quindi, ma del ddl non se ne sa più nulla. Pare essere fermo presso l’VIII Commissione. Perché questo blocco? Si può sbloccare la situazione? 

Non poteva quindi esserci momento migliore che questo Torino Pride 2009 per discutere del tema considerato anche la concomitanza con la Giornata mondiale contro l’omofobia e la necessità che dalle dichiarazioni si passi a iniziative concrete in questo ambito. 

L’iniziativa è stata organizzata dall’Associazione Quore, dall’Associazione radicale Certi Diritti e dall’ArciGay Ottavio Mai di Torino nell’ambito delle iniziative del **Torino Pride 2009**, organizzato dal **Coordinamento Torino Pride lgbt** di cui le stesse associazioni fanno parte.   
 

Hanno confermato la loro presenza all’incontro:

**Mercedes Bresso**, Presidente Regione Piemonte; **Davide Gariglio**, Presidente Consiglio Regionale; **Giuliana Manica**, Assessore alle Pari opportunità; **Aldo Reschigna**, Presidente VIII Commissione; **Angelo Burzi**, Presidente Gruppo Forza Italia verso il Partito del Popolo della Libertà; **Juri Bossuto**, Vice Presidente Gruppo Rifondazione Comunista - Sinistra Europea; **Stefano Lepri**, Vice Presidente Gruppo Partito Democratico. 

Coordina l’incontro: **Alessandra Perera**, giornalista GRP TV. 

Per informazioni:

[www.torinopride2006.it](http://www.torinopride2006.it/)

[segreteria@torinopride.it](mailto:segreteria@torinopride.it)

011.5212033