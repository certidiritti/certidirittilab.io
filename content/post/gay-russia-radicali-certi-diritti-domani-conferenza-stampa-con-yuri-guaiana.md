---
title: 'GAY RUSSIA. RADICALI, CERTI DIRITTI: DOMANI CONFERENZA STAMPA CON YURI GUAIANA'
date: Thu, 11 May 2017 18:19:40 +0000
draft: false
tags: [Russia]
---

[![yuri-guaiana](http://www.certidiritti.org/wp-content/uploads/2016/01/Yuri-Guaiana.jpg)](http://www.certidiritti.org/staff-view/enzo-cucco/yuri-guaiana-2/)È convocata per domani alle 11.30 presso la sede di Radicali Milano in via Sebastiano del piombo 11 (Milano) una conferenza stampa con Yuri Guaiana, di ritorno da Mosca dopo essere stato arrestato dalla polizia russa nel corso di una dimostrazione contro le persecuzioni di omosessuali in Cecenia. Saranno presenti Leonardo Monaco, segretario dell'Associazione Radicale Certi Diritti e Marco Cappato dell'associazione Luca Coscioni. Nel corso della conferenza saranno annunciate le prossime iniziative radicali per il rispetto dei diritti umani in Cecenia e in tutta la federazione Russa. Roma, 11 maggio 2017