---
title: 'Professore cacciato perchè favorevole ai preservativi a scuola'
date: Mon, 28 Feb 2011 13:06:49 +0000
draft: false
tags: [Comunicati stampa]
---

Solidarietà al prof. di religione Genesio Petrucci cacciato dal Vicariato perchè favorevole al distributore di preservativi nel liceo Keplero di Roma.  
  
Roma, 28 febbraio 2011  
   
Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti

Il Professor Genesio Petrucci spiegò bene anche le ragioni per le quali era favorevole: "Vogliamo che i ragazzi al sesso ci arrivino con consapevolezza, nel rispetto di se stessi e dell'altro", insomma, parole di buon senso che andrebbero spiegate con interventi mirati in tutte le scuole d’Italia, così come già avviene nelle scuole pubbliche di tutta Europa, dove le macchinette dei preservativi sono affiancate ad opuscoli di informazione. In Italia le malattie sessualmente trasmissibili sono in aumento anche tra i minori e Roma ha, insieme a Milano, la percentuale più alta d’Italia. Occorrerebbe buon senso e responsabilità e l’avvio di campagne mirate di educazione e informazione sessuale in tutte le scuole con distribuzione gratuita di preservativi.

Esprimiamo tutta la nostra solidarietà al Prof. Genesio Petrucci per la sua forza e il suo coraggio rivolti alla difesa e alla promozione di una cultura di tolleranza e di responsabilità.  Il comportamento dei gerarchi del Vicariato di Roma è grave così come è gravissimo e folle  negare che il preservativo sia uno dei più importanti strumenti per la lotta alle malattie sessualmente trasmissibili. Chi nega questo è perché ha una visione della realtà alterata dal fondamentalismo religioso e opera contro la scienza e contro la persona.

   
Se ancora vi fosse bisogno di capire in che modo le gerarchie della chiesa cattolica promuovono il messaggio cristiano basta conoscere la vicenda resa pubblica oggi dal Prof. Genesio Petrucci, insegnante di religione, cacciato dal Vicariato di Roma, perché lo scorso anno si era espresso favorevolmente  all’installazione di una macchinetta di distribuzione di preservativi al Liceo Keplero di Roma dove insegnava.