---
title: 'PRISCILLA: DAL DESERTO ALLE PORTE DI SAN PIETRO, EVENTO A ROMA'
date: Wed, 21 Jan 2009 08:40:28 +0000
draft: false
tags: [Comunicati stampa]
---

**Priscilla: dal deserto alle porte di S.Pietro**

**Transmania e transpolitica**

**A Roma il 28 Gennaio 2009**

ore 20,00  Vernissage

Artisti :  Sandro Martini \- fotografia

Alessandra Bramante - pittura

**Esposizione dal 28-1 al 4-2**

Arte nel mondo trans e sue espressioni con documentari del movimento trans italiano

ore 22,00  video-arte di Sandro Martini: laughing Bum

video di luciano Parisi "pride 2007”

a seguire dibattito con  Roberta Franciolini M.I.T.Roma

Sonia del M.I.T. di Milano

Marcella Di Folco Nazionale

Sergio Rovasio “per certi Diritti”

Mediatore :anonimo

Una primavera “Rossa" sui diritti civili: verso il Pride 2009

**_DJ.set : ugo Sanches V.J. Angelo J._**

2° livello  Installazione a cura di Imageeventroma

"Trasmonia la dea a due lingue"