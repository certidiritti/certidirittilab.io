---
title: 'Russia: bene impegno Della Vedova a incontrarci per visti umanitari a vittime gay cecene'
date: Fri, 21 Jul 2017 10:53:48 +0000
draft: false
tags: [Russia]
---

  ![foto1_0](http://www.certidiritti.org/wp-content/uploads/2017/07/foto1_0-300x200.jpg)All’incontro sul pogrom anti-gay in Cecenia organizzato dal Padova Pride Village, il sottosegretario agli affari esteri Benedetto Della Vedova ha raccolto le richieste degli attivisti di Certi Diritti, All Out e del Russian LGBT Network per incontrarsi e discutere di visti umanitari per le vittime del suddetto pogrom. “Ottimo l’impegno del sottosegretario Della Vedova, ci metteremo subito al lavoro per organizzare tutti gli incontri necessari ad assicurare una rapida concessione dei visti umanitari” commentano Yuri Guaiana, responsabile delle questioni internazionali di Certi Diritti e senior campaign manager di All Out, presente al dibattito di ieri sera a Padova, e Leonardo Monaco, segretario di Certi Diritti.⁠