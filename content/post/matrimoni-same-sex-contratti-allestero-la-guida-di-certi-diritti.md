---
title: 'MATRIMONI SAME-SEX CONTRATTI ALL’ESTERO: LA GUIDA DI CERTI DIRITTI.'
date: Sat, 10 Sep 2016 09:24:45 +0000
draft: false
tags: [Matrimonio egualitario]
---

[![mani-1024x683](http://www.certidiritti.org/wp-content/uploads/2016/07/mani-1024x683-300x200.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/07/mani-1024x683.jpg)In seguito alla crescente confusione sulla trascrizione di unioni civili e matrimoni tra coppie dello stesso sesso in Italia già [denunciata](http://www.certidiritti.org/2016/08/26/matrimoni-same-sex-contratti-allestero-i-comuni-li-trascrivano-subito/) dall’Associazione Radicale Certi Diritti, mettiamo a disposizione di tutti e tutti un [guida](http://www.certidiritti.org/wp-content/uploads/2016/09/guida-trascrizioni-bozza-definitiva.pdf) curata da Gabriella Friso con la collaborazione di Andrea Antognoni, Esperto Anusca. “In attesa che il Governo faccia chiarezza, riteniamo fondamentale dare delle [indicazioni](http://www.certidiritti.org/wp-content/uploads/2016/09/guida-trascrizioni-bozza-definitiva.pdf) concrete a tutte e tutti coloro che si trovano in difficolta nel trascrivere i propri matrimoni o le proprie unioni civili contratte all’estero. Ribadiamo la necessità di fare chiarezza e di emanare al più presto i decreti applicativi”, dichiara Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti. _Roma, 10 settembre 2016_