---
title: 'VII Congresso Associazione Radicale Certi Diritti'
date: Mon, 09 Dec 2013 13:04:19 +0000
draft: false
tags: [Politica]
---

Milano, 10-11-12 gennaio 2014

Ex Chiesetta del Parco Trotter (via Padova)

**[L'EVENTO FACEBOOK](https://www.facebook.com/events/234295176736242/?fref=ts)**

Si avvicina il nostro VII Congresso. **Il Congresso è il massimo organo deliberativo dell'Associazione, di cui stabilisce annualmente gli obiettivi d'azione. Tutti gli iscritti all'Associazione hanno diritto di voto sui documenti congressuali e le cariche statutarie.**

VCome ogni anno l'adunanza elaborerà la strategia per l'anno venturo e dibatterà con tutti i congressisti i temi cardine dell'associazione legati al contrasto della sessuofobia e ai diritti umani e civili, con uno sguardo attento al possibile nuovo assetto organizzativo, che consenta all'associazione -all'alba del suo ottavo anno di attività- di affrontare con maggiore efficacia ed efficienza le sue battaglie storiche e quelle che si presenteranno all'orizzonte di un periodo dalla forte incertezza politica e istituzionale.

**Milano - 10, 11, 12 gennaio 2014**

**Ex Chiesetta del Parco Trotter, via Mosso 7**

**Venerdì 10 gennaio**

16:00-19:00 **La libertà d'espressione in un'ottica liberale: i casi delle proposte di legge contro omo-transfobia e negazionismo.**

Marco Gattuso, articolo 29

Andrea Bitetto, avvocato

Valerio Pocar, sociologo

Andrea Puggiotto, costituzionalista

Nicola Riva, assegnista di ricerca in filosofia del diritto presso l'Università degli Studi di Milano

Sergio Lo Giudice, Senatore PD

Andrew Smith, Article 19  

**Sabato 11 gennaio**

9:00 Inizio registrazione dei congressisti e insediamento della Presidenza.

9:30 Relazione del Segretario Yuri Guaiana.

10:30 Relazione del Tesoriere Leonardo Monaco.

11:30 **Relazione della responsabile della campagna Affermazione Civile Gabriella Friso e presentazione del "Gruppo Coppie".**

13:30-14:30 Pausa pranzo

14:30 Dibattito generale

16:00 Sessione commissioni:

**Commissione 1: Assetto organizzativo, crescita attivisti e autofinanziamento dell'associazione**

Rosario Murdica, ricercatore (ISFOL) Istituto per lo Sviluppo della Formazione Professionale dei Lavoratori

**Commissione 2: Assistenza sessuale, cos'è? Una proposta nonviolenta per l'interruzione del proibizionismo sessuofobico sui corpi delle persone con disabilità.**

Max Ulivieri, Sexability

Fabrizio Quattrini, Sessuologo

Marco Cappato, Associazone Luca Coscioni

Alessandro Frezzato, Associazione Luca Coscioni

Massimo Clara, Avvocato

**Commissione 3: Europa: opportunità e rischi per i diritti umani delle persone LGBTI nel 2014**

Gian Ludovico de Martino, presidente CIDU (Centro Interministeriale Diritti Umani)

18:30 Fine della sessione commissioni. Esposizione delle relazioni in plenaria.

19:30 Inizio dibattito generale.

21:00 Fine dei lavori della prima giornata congressuale.

**Domenica 12 gennaio**

9:00 Inizio lavori della seconda giornata congressuale. Inizio deposito delle mozioni generali e particolari, delle raccomandazioni e delle modifiche statutarie. Proseguimento del dibattito generale.

11:30 **Tavola rotonda: Obiettivo matrimonio egualitario: come raggiungerlo?**

Pietro Michelotti, Rete genitori Rainbow  
Elena Broggi, AGEDO  
Flavio Romani, Arcigay  
Aurelio Mancuso, Equality Italia

13:00 Repliche del Segretario e del Tesoriere. Conclusioni del Presidente. Candidature alle cariche statutarie e presentazione dei documenti.

14:00 Votazione dei documenti congressuali. Votazione delle cariche statutarie.

15:00 Proclamazioni. Chiusura dei lavori.

**Interverranno nel corso dei lavori:**

**Pia Covre**, Comitato per i diritti civili delle prostitute

**Marilisa D'Amico**, costituzionalista

**Rita Bernardini**, Segretaria di Radicali Italiani

**Nicoletta Paci**, Vicesindaco di Parma

**Valerio Federico**, Tesoriere di Radicali Italiani

**Sam Opio**, Executive Director Queer Youth Uganda

**Massimo Clara**, Avvocato

**Alessandro Cecchi Paone**

**COME RAGGIUNGERE LA CHIESETTA DEL PARCO TROTTER?**

La sede del congresso è in **Via Padova 69**, raggiungibile con la metro (**MM1 feramata Rovereto**) o attraverso trasporto di superficie (**bus 56 da Piazzale Loreto, angolo via Doria**)

![trotter](http://www.certidiritti.org/wp-content/uploads/2013/12/trotter.PNG)

**DOVE DORMIRE A MILANO?**

**Il VII Congresso dell'Associazione Radicale Certi Diritti è convenzionato con l'Hotel San Guido** (Via Carlo Farini 1A), che offre ai nostri congressisti due vantaggiose tariffe:

**Singola: 50€/notte**  
**Doppia: 60€/notte**

**per prenotazioni:**  
[www.hotelsanguido.com](http://www.hotelsanguido.com)  
Tel +39 02 6552261 - Fax +39 02 6572890  
E-mail: info@sanguido.191.it

_(Per altre soluzioni non convenzionate consigliamo siti di prenotazione online come booking.com e simili)_