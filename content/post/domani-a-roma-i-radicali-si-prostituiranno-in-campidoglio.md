---
title: 'DOMANI A ROMA I RADICALI SI PROSTITUIRANNO IN CAMPIDOGLIO'
date: Mon, 15 Sep 2008 13:05:22 +0000
draft: false
tags: [Comunicati stampa]
---

**Roma, 15 settembre 2008**

**DOMANI SOTTO LA SCALINATA DEL CAMPIDOGLIO PARLAMENTARI E DIRIGENTI RADICALI, INSIEME ALLE RAPPRESENTANTI DI ASSOCIAZIONI PER I DIRITTI CIVILI, SI PROSTITUIRANNO CONTRO LA DEMAGOGIA, L’IPOCRISIA, L’INUTILE PROIBIZIONISMO CRIMINOGENO. CONFERENZA STAMPA ORE …**

**Domani martedì 16 settembre, alle ore …. sotto la scalinata del Campidoglio, in occasione della presentazione dei provvedimenti del Comune di Roma contro la prostituzione, che seguono le orme del DDL governativo, le Associazioni Certi Diritti, Radicali Roma, il Detenuto Ignoto e ‘La Strega da bruciare’ (per i diritti civili delle prostitute), insieme ai parlamentari radicali Rita Bernardini, Donatella Poretti, Matteo Mecacci e Marco Perduca, svolgeranno un’azione di disobbedienza civile prostituendosi sotto il Campidoglio. A margine dell’azione, si svolgerà una Conferenza Stampa e documentata l’inutilita’ dei provvedimenti ipocriti contro la prostituzione adottati dal Governo e dal Comune di Roma. dove veranno presentate le proposte di legge depositate dai parlamentari radicali**

All’iniziativa di disobbedienza civile prenderanno parte anche dirigenti e militanti radicali, Sergio Rovasio, Segretario associazione Radicale Certi Diritti, Massimiliano Iervolino, Segretario Radicali Roma, Alessandro Gerardi e Diego Sabatinelli, Presidente e Segretario dell’Associaizone per il Divorzio breve, Irene Testa, Coordinatrice

La campagna avviata dal Governo e dalle amministrazioni locali contro le lucciole e i loro clienti per ripulire le strade del nostro “Bel Paese” è volta a ripulire esteticamente le strade dalla prostituzione lasciando assolutamente invariate le reali e serie tematiche che riguadano lo sfruttamento e la riduzione in schiavitù delle persone prostitute.