---
title: 'GAY ARRESTATI IN SENEGAL: INTERVIENE LA COMMISSIONE EUROPEA'
date: Mon, 02 Mar 2009 15:55:22 +0000
draft: false
tags: [COMMISSIONE EUROPEA, Comunicati stampa, GAY, PERSECUZIONI, SENEGAL]
---

**GAY ARRESTATI IN SENEGAL: LA COMMISSIONE EUROPEA RISPONDE A INTERROGAZIONE CAPPATO E PANNELLA SU INIZIATIVE INTRAPRESE. CONTRO GRAVI PERSECUZIONI DELLO STATO.  
Eurodeputati radicali propongono risoluzione d'urgenza al Parlamento Europeo.**

Luis Michel, commissario allo sviluppo, ha risposto a nome della Commissione europea ad una interrogazione degli eurodeputati Radicali Marco Cappato e Marco Pannella sulle persecuzioni omofobe in Senegal affermando che "la presidenza dell'UE ha intrapreso una serie di iniziative" e che “la delgazione della Commissione Europea in Senegal ha espresso in termini estremamente chiari alle autorità senegalesi la propria preoccupazione riguardo al caso di specie. La Commissione continuerà a seguire gli sviluppi con grande attenzione". La Commissione "continuerà ad adoperarsi per incoraggiare il Senegal, così come altri paesi terzi, a porre fine alle discriminazioni fondate sull'orientamento sessuale".  
Marco Cappato, responsabile per le risoluzioni d'urgenza per il gruppo liberale e democratico al PE, ha chiesto che il PE si pronunci la prossima settimana con una risoluzione in materia di persecuzioni omofobiche in Africa, dove la situazione é sempre più preoccupante in vari Stati.  
   
   
Di seguito il testo dell'interrogazione e la risposta:  
   
INTERROGAZIONE SCRITTA E-0140/09  
di Marco Cappato (ALDE) e Marco Pannella (ALDE)  
alla Commissione  
   
Oggetto:        Omofobia in Senegal  
   
Secondo quanto riportato da un articolo pubblicato il 9 gennaio 2009, a pagina 20, dal Corriere della Sera, a firma Massimo A. Alberizzi, nove omosessuali sono stati condannati in Senegal a otto anni di carcere per 'atti contro natura'.  
   
Il Senegal è tra i 38 paesi del continente africano che puniscono i rapporti omosessuali tra adulti consenzienti; la norma, che prevede una multa da 150 a 250 euro e la prigione da uno a cinque anni, non era stata mai applicata così severamente.  
   
Lo scorso febbraio 2008 una donna e 10 uomini sono stati arrestati a Dakar, dopo la pubblicazione su una rivista di una fotografia di un matrimonio gay.  
   
Più di recente, il 19 dicembre scorso, sono stati arrestati nove omosessuali, tra i quali il leader del movimento LGBT senegalese, Diadji Diouf, impegnati anche in un'organizzazione che si batte contro l'AIDS. Tali arresti sono avvenuti all'interno di un clima mediatico omofobico. Il Senegal avevaa mantenuto a lungo, sotto la Presidenza Wade, una tradizione di rispetto dei principi democratici e di difesa dei diritti umani.  
   
L'Unione europea ha promosso lo scorso dicembre la presentazione di una dichiarazione in sede ONU per la depenalizzazione dell'omosessualità, quale iniziativa centrale per il rispetto dei diritti umani degli omosessuali.  
   
Quali urgenti iniziative intende promuovere la Commissione affinché vengano scongiurate iniziative e azioni omofobiche in Senegal?  
   
Non ritiene la Commissione di doversi attivare per esprimere la preoccupazione per i fatti succitati e, tra le azioni più concrete, di dover convocare con la massima urgenza l'Ambasciatore del Senegal, anche per chiedere l'adeguamento della legislazione del paese agli standard internazionali in materia di diritti umani fondamentali?  
   
   
   
Risposta di Louis Michel  
a nome della Commissione  
(2.3.2009)  
   
La Commissione rifiuta e condanna tutte le manifestazioni di omofobia quale fenomeno che integra una violazione manifesta della dignità umana.  
   
La Commissione informa l'onorevole parlamentare che la presidenza dell'UE ha intrapreso una serie di iniziative e che la delegazione della Commissione in Senegal ha espresso in termini estremamente chiari alle autorità senegalesi la propria preoccupazione riguardo al caso di specie. La Commissione continuerà a seguire gli sviluppi con grande attenzione, essendo venuta a sapere che la decisione è stata impugnata.  
   
Mentre nell'immediato la priorità è la scarcerazione dei detenuti, nel lungo periodo la Commissione, nell'ambito degli sforzi tesi alla promozione dei diritti umani, continuerà ad adoperarsi per incoraggiare il Senegal, così come altri paesi terzi, a porre fine alle discriminazioni fondate sull'orientamento sessuale. A tale riguardo, la Commissione è pronta a sostenere iniziative volte a promuovere una maggiore tolleranza rispetto a tali tematiche.  
   
La Commissione è inoltre preoccupata per il persistere di una normativa e di atteggiamenti discriminatori nei confronti delle minoranze sessuali, che potrebbero aggravare ulteriormente l'epidemia da virus dell'immunodeficienza umana (HIV)/sindrome da immunodeficienza acquisita (AIDS) e il suo impatto, nonché rappresentare un grave ostacolo a risposte efficaci. Nell'ambito del riesame in corso del Programma europeo di azione per lottare contro l'HIV/AIDS, la malaria e la tubercolosi attraverso azioni esterne (2007-2011), la Commissione, in collaborazione con gli Stati membri e le organizzazioni della società civile, sta esaminando possibili strategie per intensificare l'azione collettiva dell'UE in settori prioritari quali l'HIV/AIDS e i diritti umani.