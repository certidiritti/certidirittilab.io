---
title: 'DIRETTIVA MINISTRO INTERNI AI PREFETTI: SEMBRA FATTA CONTRO GAYPRIDE'
date: Tue, 27 Jan 2009 15:38:13 +0000
draft: false
tags: [Comunicati stampa, direttiva, gaypride, MANIFESTAZIONE, ministero interni]
---

**DIRETTIVA DEL MINISTERO DEGLI INTERNI AI PREFETTI: IL GIRO DI VITE LIBERTICIDA SEMBRA FATTO APPOSTA CONTRO I GAY PRIDE E LE MANIFESTAZIONI DI DISSENSO CONTRO LA CHIESA CATTOLICA.**

** Roma, 27 gennaio 2009**

**_Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:_**

"Oggi è stata inviata la nuova Direttiva del Ministero degli Interni ai Prefetti che limiterà maggiormente il diritto di manifestare. Nel dispositivo si legge tra l'altro:".. si evidenzia la necessità di limitare l'accesso ad alcune aree particolarmente sensibili, specialmente quando la manifestazione coinvolga un numero di partecipanti elevato. Tali aree sensibili saranno individuate in zone a forte caratterizzazione simbolica per motivi sociali, culturali o religiosi (ad esempio cattedrali, basiliche o altri importanti luoghi di culto) o che siano caratterizzate – anche in condizioni normali – da un notevole afflusso di persone o nelle aree nelle quali siano collocati obiettivi critici".

Ci auguriamo che con questa Direttiva non sia impedito a nessuno di manifestare il proprio pensiero e di manifestare anche davanti a luoghi 'a forte caratterizzazione simbolica per motivi sociali, culturali o religiosi', luoghi da dove, sovente, partono gli anatemi e le offese contro le persone lesbiche, gay e transgender.   La Direttiva sembra fatta 'ad hoc' per colpire gli organizzatori dei gay pride".