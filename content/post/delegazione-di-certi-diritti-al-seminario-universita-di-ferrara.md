---
title: 'DELEGAZIONE DI CERTI DIRITTI AL SEMINARIO UNIVERSITA'' DI FERRARA'
date: Wed, 24 Feb 2010 15:40:50 +0000
draft: false
tags: [Comunicati stampa]
---

Venerdì 26 febbraio si svolgerà all'Università di Ferrara un seminario che tratterà la questione del matirmonio gay in vista della decisione della Corte Costituzionale fissata il 23 marzo. I lavori del seminario si svolgeranno per tutta la giornata. La Delegazione di Certi Diritti presente a Ferrara è composta da Clara Comelli, Presidente; Sergio Rovasio, Segretario e Gian Mario Felicetti, coordinatore nazionale di Affermazione Civile.

Maggiori informazioni sui lavori al seguente sito www.amicuscuriae.it

### LA «SOCIETA' NATURALE» E I SUOI "NEMICI"  
Sul paradigma eterosessuale del matrimonio