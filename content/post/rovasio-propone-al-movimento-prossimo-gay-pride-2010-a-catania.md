---
title: 'ROVASIO PROPONE AL MOVIMENTO: PROSSIMO GAY PRIDE 2010 A CATANIA'
date: Mon, 06 Jul 2009 10:44:34 +0000
draft: false
tags: [Comunicati stampa]
---

Lettera al Movimento lgbtq italiano per svolgere prossimo gay pride nazionale 2010 a Catania.

Care e cari amici del movimento lgbtq,

in occasione del Sicilia Pride, che si è svolto con successo sabato 4 luglio per le strade del centro di Catania, l'Associazione Radicale Certi Diritti ha annunciato che avrebbe proposto alle Associazioni nazionali e locali, che compongono il movimento lgbtq italiano, di lì tenere il prossimo Gay Pride nazionale 2010. 

Non sappiamo quali appuntamenti o scadenze si è dato il movimento per decidere il luogo e la data del più importante evento della nostra comunità.Vi chiedo comunque di considerare questa eventualità per il grande significato politico, sociale e simbolico che lo stesso può avere.

Un caro saluto,

  

Sergio Rovasio

Segretario Associazione Radicale Certi Diritti

[www.certidiritti.it](http://www.certidiritti.it/)