---
title: 'IL RETTORE DELL''UNIVERSITA'' DI ROMA VIETA RASSEGNA CINEMA QUEER'
date: Mon, 07 Jun 2010 20:37:32 +0000
draft: false
tags: [Comunicati stampa]
---

**LA DECISIONE DEL RETTORE DELL’UNIVERSITA’ LA SAPIENZA DI ROMA PARREBBE ISPIRATA DA UNA VISIONE CLERICO-FASCISTA DEI TEMPI ANDATI  PIUTTOSTO CHE DA VERI TIMORI DI ATTI DI VIOLENZA OMOFOBICA.**  
**_  
Comunicato Stampa dell’Associazione Radicale Certi Diritti:_**

  
Con la scusa che ‘potrebbero avvenire atti di violenza’ è stata vietata a 24 ore dall’inaugurazione di domani, martedì 8 giugno, una rassegna di cinema Queer che un gruppo di studenti aveva promosso nei prati dell’Università La Sapienza di Roma. Se pericoli ci fossero stati sarebbe stato sufficiente che le forze dell’ordine garantissero l’incolumità dei partecipanti piuttosto che proibire lo svolgimento di una rassegna che ha visto coinvolte centinaia di persone e la spesa di molti soldi che nessuno rimborserà agli organizzatori. Abbiamo il serio timore che questa decisione sia stata presa non tanto per garantire l’incolumità dei partecipanti alla rassegna ma piuttosto sia stata ispirata da una visione autoritaria di stampo clerico-fascista grazie, forse, all’intervento di qualcuno che si sarebbe troppo infastidito per una rassegna che avrebbe toccato questioni come l’omosessualità, la bisessualità e l’amore non conforme ai dettami dei vertici clericali. La decisione è gravissima. **Abbiamo comunicato agli organizzatori che siamo disponibili a sostenerli qualora decidessero di promuovere iniziative di disobbedienza civile”.**