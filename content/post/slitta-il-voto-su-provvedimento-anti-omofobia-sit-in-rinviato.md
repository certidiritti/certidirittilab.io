---
title: 'Slitta il voto su provvedimento anti omofobia. Sit-in rinviato'
date: Thu, 19 May 2011 12:09:08 +0000
draft: false
tags: [Politica]
---

  
**Alla Camera dei deputati, pur iniziando il 23 maggio la discussione del ddl contro l'omofobia, slitta il voto. Rinviata la maratona oratoria e sit-in in Piazza Montecitorio. Solidarietà a Paola Concia per le sue dimissioni da relatrice.**

Roma, 19 maggio 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

L’Associazione Radicale Certi Diritti esprime tutta la sua solidarietà e vicinanza all’on. Paola Concia che si è oggi dimessa da Relatrice del provvedimento contro l’omofobia a causa dei veti e delle prese in giro di una parte delle forze politiche clericali e fondamentaliste.

Abbiamo oggi saputo che il voto sul provvedimento anti-omofobia, la cui discussione inizierà alla Camera dei deputati, lunedì 23 maggio alle ore 10, potrebbe slittare di alcuni giorni a causa del voto (con la fiducia quasi sicuramente) sul “Decreto Omnibus” che di fatto occuperà le due giornate di voto dell’aula di martedì 24 e mercoledì 25 maggio. Il voto sulla pdl anti-omofobia, anche se molto difficile, potrebbe svolgersi giovedì 26 maggio o addirittura la settimana successiva, sempre che non si faccia la pausa pre-elettorale in vista dei ballottaggi per le amministrative.

L’Associazione Radicale Certi Diritti ha deciso quindi di rinviare alla data del voto la manifestazione di Piazza Montecitorio. In quelle stesse ore verrà promossa davanti alla Camera dei deputati, e ne daremo per tempo notizia, una maratona oratoria sit-in aperta ai cittadini alle associazioni e a quanti hanno a cuore la promozione e la difesa dei diritti civili in Italia.