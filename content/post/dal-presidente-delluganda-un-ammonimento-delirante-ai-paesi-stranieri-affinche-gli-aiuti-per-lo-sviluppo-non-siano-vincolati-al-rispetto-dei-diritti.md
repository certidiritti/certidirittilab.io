---
title: 'Dal presidente dell''Uganda un ammonimento delirante ai paesi stranieri affinchè gli aiuti per lo sviluppo non siano vincolati al rispetto dei diritti'
date: Thu, 22 Dec 2011 13:37:57 +0000
draft: false
tags: [Africa]
---

"Le preoccupazioni sui diritti gay non devono influire sugli aiuti stranieri" ha dichiarato Yoweri Museveni. Certi Diritti: sempre più paesi seguano l'esempio di Usa e Gran Bretagna.

  
Roma, 19 dicembre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Secondo fonti d’agenzia di stampa il presidente ugandese, due giorni fa, in una località vicino a Kampala, ha ammonito i paesi occidentali a non legare gli aiuti per lo sviluppo alla tutela dei diritti delle persone Lgbt(e). Il Presidente Yoweri Museveni ha detto ai delegati di un  meeting regionale a Kampala, alla presenza di molti altri capi di Stato africani, che: “Prima che si pretenda di farmi una lezione sugli omosessuali e i loro diritti, mi si parli delle ferrovie” e poi ha continuando dicendo che: “anche gli omosessuali hanno bisogno di elettricità, anche gli omosessuali hanno bisogno di strade, anche gli omosessuali hanno bisogno di ferrovie (Rapporto AFP: [http://www.news24.com/Africa/News/Uganda-President-Railways-before-gays-20111216](http://www.news24.com/Africa/News/Uganda-President-Railways-before-gays-20111216)).

Il commento di Museveni arriva a meno di due settimane dal discorso del Segretario di Stato Hillary Clinton fatto alla sede Onu di Ginevra sui diritti LGBT come diritti umani fondamentali.

Il Dipartimento di Stato ha enfatizzato che le articolate direttive di politica estera sui diritti LGBT, abbozzate la settimana scorsa dall’amministrazione Obama, sono “affermative, non punitive”. Per esempio, la Clinton ha annunciato il lancio di un fondo globale per sostenere le iniziative di gruppi LGBT in paesi dove i gay sono soggetti alla criminalizzazione e a un’ambiente ostile.

Il primo ministro britannico David Cameron ha fatto delle dichiarazioni più audaci quest’autunno, chiedendo il taglio di un tipo di aiuti bilaterali ai paesi del Commonwealth che criminalizzano l’omosessualità.

Nel frattempo, tre pastori cristiani antigay in Uganda, accusati di aver ordito una campagna contro un altro leader religioso, sono stati accusati di cospirazione questa settimana. Un’accusa che potrebbe portarli a una condanna di 5 anni di reclusione.

L’Associazione Radicale Certi Diritti si augura che siano sempre di più i paesi democratici a porre come vincolo legato agli aiuti la questione del rispetto dei diritti civili e umani, così come fatto recentemente dagli Usa e dalla Gran Bretagna alla luce dei gravissimi atti di omofobia avvenuti in Uganda e in altri paesi africani a causa del fondamentalismo religioso.