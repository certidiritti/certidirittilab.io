---
title: 'A Roma il 28 ottobre presso la sede del Partito Radicale proiezione del docufilm "Taking a chance on God" sulla vita del prete gay John McNeill'
date: Wed, 17 Oct 2012 13:02:09 +0000
draft: false
tags: [Movimento LGBTI]
---

Arriva in Italia il docufilm "Taking a chance on God", sulla vita di John McNeill, prete gay, pioniere del movimento di liberazione di omosessuali e transessuali. La tappa romana il 28 ottobre, alle ore 20.00 presso la sede del partito radicale in via di Torre Argentina 76.  

E' difficile ricordarsi dei tempi in cui la Chiesa Cattolica Romana non era ossessionata dall'omosessualità che anzi considerava un argomento "innominabile". A ricordarceli arriva in Italia, dopo la prima di New York, il docufilm “Taking a Chance on God” del regista Brendan Fay, il racconto suggestivo della lotta di John McNeill, prete cattolico americano, teologo, psicoterapeuta e gay per la "liberazione delle persone omosessuali dalla paura e dall'esclusione" nella società e nella sua chiesa.

“Taking a Chance on God” sarà proiettato in anteprima europea giovedì 25 ottobre alle 17.00 presso il Cinema Odeon di Firenze in apertura del Florence Queer Festival di Firenze (una versione short è stata presentata all’Europride di Roma nel 2011) e nei giorni successivi sarà al centro d'incontri e dibattiti aperti con il regista in varie tappe a Milano (26 Ottobre), Roma (28 ottobre), Napoli (30 ottobre), Palermo (31 ottobre), Trapani (2 novembre); Catania (4 novembre).  
  
**[L'EVENTO ROMANO SU FACEBOOK >>>](http://www.facebook.com/events/368776753207522/?ref=ts&fref=ts)**

La tappa romana, in particolare, avrà luogo il 28 ottobre, alle ore 20.00, presso la sede del Partito Radicale in via di Torre Argentina 76, e sarà organizzata da "Nuova Proposta, donne e uomini omosessuali e transessuali cristiani", insieme a (in ordine alfabetico) Adista, Agedo Roma, Arcilesbica Roma, Arcigay Roma, Associazione radicale Certi Diritti, Centro Studi e Documentazione Ferruccio Castellano, Cipax, Comunità cristiana di Base di s. Paolo, Dì Gay Project (DGP), Forum Italiano Cristiani Omosessuali, Gay Center.

“Taking a Chance on God”, attraverso suggestive immagini d’epoca e interviste ai protagonisti,ripercorre il percorso umano e spirituale di John McNeill, un piccolo prete in lotta con le paure e i dictat della gerarchia della sua chiesa. Il documentario racconta questa lotta epocale a cui la casa editrice Le Piagge ha dedicato recentemente il libro “Cercare se stessi… per trovare Dio. Omosessualità, Chiesa, Fede, Vangelo, Spirito” in cui l’anziano teologo, in una lunga intervista-testamento rilasciata al giornalista Valerio Gigante, spiega e approfondisce con grande lucidità il suo pensiero di “liberazione” su chiesa cattolica, fede e omosessualità.

L'arrivo in Italia di Taking a Chance on God è stato possibile grazie al Florence Queer Festival, organizzato dall’associazione Ireos, che fa parte della kermesse 50 giorni di cinema internazionale a Firenze della Mediateca Regionale Toscana, e all'impegno dei gruppi dei Cristiani omosessuali Italiani e alla collaborazione di numerose associazioni italiane.