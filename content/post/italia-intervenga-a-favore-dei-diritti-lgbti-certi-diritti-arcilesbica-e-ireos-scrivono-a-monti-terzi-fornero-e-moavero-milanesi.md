---
title: 'Italia intervenga a favore dei diritti lgbti: Certi Diritti, Arcilesbica e Ireos scrivono a Monti, Terzi, Fornero e Moavero Milanesi'
date: Mon, 03 Sep 2012 11:03:13 +0000
draft: false
tags: [Movimento LGBTI]
---

Con una lettera le associazioni chiedono che l'Italia intervenga al comitato dei ministri del consiglio d'europa a favore dei diritti lgbti.

Comunicato stampa dell’Associazione Radicale Certi Diritti

Roma, 3 settembre 2012

L’Associazione Radicale Certi Diritti, ArciLesbica Associazione Nazionale e IREOS – Centro Servizi Autogestito Comunità Queer hanno scritto al Presidente del Consiglio Monti, al Ministro degli Esteri Terzi, al Ministro del Lavoro e delle Politiche Sociali Fornero e al Ministro per gli Affari Europei Moavero Milanesi affinché l’Italia sostenga con convinzione che il Consiglio d’Europa dedichi risorse importanti per sostenere i diritti delle persone LGBTI (Lesbiche, Gay, Bisessuali, Transessuali e Intersessuali) quando, il prossimo 18 settembre, il Comitato dei Ministri del Consiglio d’Europa terrà un dibattito sul tema.

Il Comitato dei Ministri si era già espresso nel 2010 con la Raccomandazione CM/Rec(2010)5 del Comitato dei Ministri agli Stati membri sulle misure volte a combattere la discriminazione fondata sull’orientamento sessuale o sull’identità di genere, mentre importanti sentenze della Corte Europea dei Diritti Umani hanno elevato negli ultimi anni gli standard sui diritti umani del Consiglio d’Europa in relazione all’orientamento sessuale e all’identità di genere. Questo però non è ancora sufficiente: il rapporto 2011 del Commissario per i Diritti Umani, “Discriminazione fondata sull’orientamento sessuale e l’identità di genere” non ha lasciato dubbi sulla necessità di azioni determinate per combattere tali discriminazioni. Da allora vari sviluppi negativi in un certo numero di Stati membri hanno intensificato le preoccupazioni, specialmente in aree quali la libertà d’espressione, di riunione e di associazione. Lo scorso maggio, persino il Segretario Generale ha ritenuto necessario sostenere pubblicamente che “L’Europa non deve fare passi indietro sui diritti delle persone LGBT”.

Le associazioni LGBTI chiedono allora di “implementare serie politiche contro le discriminazioni fondate sull’orientamento sessuale e l’identità di genere, sottolineando come questa non sia una questione di “diritti speciali”, ma di godimento dei diritti umani, da parte di tutti”; un efficace monitoraggio dell’implementazione della Raccomandazione CM/Rec(2010)5 anche attraverso un appropriato finanziamento; “il rafforzamento dell’Unità LGBT del Segretariato, assicurando sufficiente personale e adeguate risorse finanziarie”; l’allocazione delle necessarie “risorse alla Commissione Europea contro il Razzismo e l’Intolleranza” e infine “di estendere a tutti gli Stati membri del Consiglio d’Europa l’iniziativa intrapresa dall’Agenzia dell’Unione Europea sui Diritti Fondamentali (FRA) volta a documentare la reale situazione delle persone LGBTI negli Stati membri dell’Unione Europea”.

La lettera si conclude con un monito: “omettere di agire significherebbe prestare il fianco alla critica di essere acquiescente rispetto alla discriminazione che la Corte Europea dei Diritti Umani ha condannato in molte occasioni”.