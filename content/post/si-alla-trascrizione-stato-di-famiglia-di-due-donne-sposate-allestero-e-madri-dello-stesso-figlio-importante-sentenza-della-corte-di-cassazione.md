---
title: 'SÌ ALLA TRASCRIZIONE STATO DI FAMIGLIA DI DUE DONNE SPOSATE ALL’ESTERO E MADRI DELLO STESSO FIGLIO. IMPORTANTE SENTENZA DELLA CORTE DI CASSAZIONE.'
date: Sat, 01 Oct 2016 07:53:18 +0000
draft: false
tags: [Diritto di Famiglia]
---

[![CASSAZIONE: SENTENZA SU STUPRO SARA' SEPPELLITA.  GIUDICI DI CAGLIARI HANNO SPAZIO PER RIMEDIARE A NOSTRO ERRORE](http://www.certidiritti.org/wp-content/uploads/2016/10/cassazione3-300x254.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/10/cassazione3.jpg)La Sentenza della Corte di Cassazione, con la quale si conferma il pronunciamento della Corte di appello, che autorizza il Comune di Torino a trascrivere nei registri anagrafici lo stato di famiglia di due donne sposate in Spagna e madri dello stesso bambino è un notevole passo avanti sulla strada della piena eguaglianza.

Questa sentenza è un esempio di come le cosiddette cause pilota siano di fondamentale importanza nella battaglia per l’uguaglianza di persone e famiglie LGBTI. Il diritto dei bambini a mantenere le relazioni familiari con i genitori, siano essi di sesso opposto o uguale, era stato uno dei principali punti su cui la legge sulle Unioni Civili non era intervenuta. Con questa pronuncia il principio del prevalente interesse del minore supera tutte le polemiche di stampo omofobo ricamate sulla presunta incapacità genitoriale delle persone omossessuali.

La sentenza riconosce come non esista alcun divieto costituzionale per le persone dello stesso sesso a generare ed allevare figli. La giurisprudenza costituzionale si mostra molto più attenta del legislatore ad un mondo in divenire e alle evidenze scientifiche senza dare peso a preconcetti ideologici.

“Una buona notizia per le coppie e le famiglie omogenitoriali italiane. Laddove non si è riusciti ad arrivare con la recente legge sulle Unioni Civili si arriverà a colpi di sentenze. Spiace constatare come per le coppie sia necessario per correre la gravosa strada giudiziaria che si sarebbe tranquillamente potuta evitare con una legge più attenta ai bisogni reali e alla concretezza. Ci complimentiamo per questa pronuncia e ci felicitiamo per le due mamme che hanno affrontato la via delle corti per l'interesse e l'amore della loro bambina.” così Yuri Guaiana, segretario dell’associazione Radicale Certi Diritti.

_Roma, 1 Ottobre 2016_