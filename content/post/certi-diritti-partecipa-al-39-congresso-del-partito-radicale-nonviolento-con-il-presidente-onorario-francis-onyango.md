---
title: 'Certi Diritti partecipa al 39° Congresso del Partito Radicale Nonviolento con il Presidente onorario Francis Onyango'
date: Fri, 09 Dec 2011 17:53:05 +0000
draft: false
tags: [Transnazionale]
---

Una delegazione dell'associazione radicale Certi Diritti partecipa ai lavori del 39° Congresso del Partito Radicale Nonviolento, a Roma dell'8 all'11 dicembre, con il Presidente onorario Francis Onyango, l'avvocato di David Kato Kisule.

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Una delegazione dell’associazione radicale Certi Diritti, guidata dal presidente onorario John Francis Onyango, dal segretario Yuri Guaiana e dal presidente Enzo Cucco, parteciperà alla seconda sessione del 39° congresso del Partito Radicale Nonviolento Transnazionale e Transpartito a Roma dall’8 all’11 dicembre.

John Francis Onyango è l’avvocato di David Kato Kisule, attivista per i diritti lgbt ucciso lo scorso gennaio in Uganda, dove una proposta di legge che tenta di imporre la pena di morte per alcuni atti omosessuali non è ancora stata ritirata dal parlamento nonostante le proteste internazionali.

Al congresso, dove insieme a Marco Pannella e Emma Bonino interverranno gli ex ministri degli esteri di Francia e Germania Bernard Kouchner e Joshka Fisher e centinaia di attivisti per i diritti umani di tutto il mondo, l’associazione radicale Certi Diritti rilancerà le sue battaglie per la promozione e la difesa dei diritti civili e umani delle persone lgbt(e).

In particolare l’associazione radicale Certi Diritti inviterà il Partito Radicale Nonviolento, Transnazionale e Transpartito a:

\- operare per fermare l'approvazione della proposta di legge che condanna l'omosessualità in Nigeria nonché il tentativo di accrescere la condanna da 5 a 15 anni di carcere per omosessualità in Camerun;

\- sostenere in sede Onu la proposta di depenalizzazione dell’omosessualità, condannata ancora in 80 paesi;

\- mobilitarsi per l’approvazione della Direttiva Europea contro le discriminazioni e la sua attuazione in Italia, inclusa la lotta contro l’omofobia, l’approvazione delle unioni civili,  l’accesso al matrimonio civile alle coppie formate da persone dello stesso sesso, la depatologizzazione della condizione delle persone transessuali;

\- continuare la lotta per assicurare che gli organismi internazionali per la difesa dei diritti fondamentali individuali rafforzino i loro poteri contro le violazioni attuate dagli Stati e dalle loro autorità.