---
title: 'Giovedì 20 settembre alle 17 la vera commemorazione della Breccia di Porta Pia'
date: Tue, 18 Sep 2012 09:53:04 +0000
draft: false
tags: [Politica]
---

Per il 142° anniversario il Partito Radicale insieme a tante altre associazioni ricorderà la breccia di Porta Pia con lo slogan 'Chiesa, paga anche tu la Spending Review'. Appuntamento sul luogo della Breccia in Corso d'Italia di fronte al monumento ai caduti.

Giovedì 20 settembre, in occasione del 142° anniversario della Breccia di Porta Pia, il Partito Radicale, Radicali Italiani, Anticlericale.net, Gruppo Lista Bonino Pannella, Federalisti Europei al Consiglio Regionale del Lazio, Associazione Radicale Certi Diritti e Associazione Luca Coscioni, insieme all’Associazione nazionale del Libero Pensiero, il Circolo Uaar di Roma, Circolo di cultura omosessuale Mario Mieli, Associazione Di ‘Gay Project, No God – Atei per la laicità degli Stati, Consulta Romana per la Laicità delle istituzioni, network “Vaticano paga tu”, Psi-Roma, Federazione dei Giovani Socialisti, AltreVie, organizzazioni e associazioni laiche, terranno la vera celebrazione della Breccia di Porta Pia.

Lo slogan della manifestazione sarà: “Chiesa, paga anche tu la Spending Review!”. La manifestazione avrà inizio alle ore 17 a Roma, sul luogo della Breccia (Corso d’Italia, monumento ai caduti).  
  
**[L'EVENTO SU FACEBOOK >>>](http://www.facebook.com/events/269841689801648/?ref=ts)**

La manifestazione si aprirà con un intervento del regista Luigi Magni. Hanno finora assicurato la loro presenza gli esponenti delle diverse organizzazioni aderenti e, tra gli altri: Maria Bonafede, Pastora della Chiesa Valdese, Mina Welby, Dado (artista di Zelig), Marco Pannella, i parlamentari e i consiglieri regionali Radicali.

La manifestazione ricorderà la Liberazione di Roma e delle opinioni pubbliche internazionali dal potere temporale dello Stato pontificio. Per la laicità e la libertà, contro ogni forma di fondamentalismo ideologico e religioso.  
Di fronte al tentativo di revisionismo storico che vorrebbe cancellare fatti e valori del Risorgimento a vantaggio di una visione politica clerical-nazionalista, nonchè alla violazione degli stessi Trattati lateranensi come documentato nel recente libro di Gianluigi Nuzzi, si rende non più differibile una nuova Liberazione, questa volta dal potere Vaticano.