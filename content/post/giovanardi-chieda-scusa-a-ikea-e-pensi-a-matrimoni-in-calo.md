---
title: 'Giovanardi chieda scusa a Ikea e pensi a matrimoni in calo'
date: Sat, 23 Apr 2011 13:07:24 +0000
draft: false
tags: [Matrimonio egualitario]
---

La Svezia esiga le scuse dal governo italiano e il sottosegretario si preoccupi del calo dei matrimoni anzi chè negare l'estistenza delle famiglie.  
  
Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti  
Roma, 23 aprile 2011

Il (molto) Sottosegretario alla famiglia Giovanardi ha dichiarato, letteralmente, che Ikea è contro la legge, anticostituzionale e manda messaggi violenti, solo perché ha promosso una pubblicità con due ragazzi che si tengono per mano. Frasi del genere potevamo sentircele dire da un qualche Ministro del governo teocratico iraniano, o da un alcolizzato di osteria, non certo da un rappresentante del Governo italiano. Ci auguriamo che la Svezia faccia una protesta ufficiale e chiede immediate scuse dal Governo italiano per questa gravissima offesa ad uno dei più importanti e frequentati centri commerciali mondiali frequentato da tutte le tipologie di famiglie. Sappiamo che a Giovanardi piace tanto il cartone animato del mulino bianco, con gli uccellini che fanno ‘cip cip’, sul praticello verde plastica, con le margherite in fiore, per descrivere il suo ideale di famiglia. Forse qualcuno dovrebbe dirgli che esistono varie tipologie di famiglie anche nel nostro paese e che dovrebbe guardare alla realtà invece di fare dichiarazioni così gravi e offensive. Magari anche alle realtà dei vari bunga bunga. Siamo ancora in attesa di conoscere per quale motivo in Italia, nonostante il suo impegno indefesso e così integralista per la difesa della famiglia del mulino bianco, aumentano i divorzi e le convivenze e diminiscono i matrimoni”.