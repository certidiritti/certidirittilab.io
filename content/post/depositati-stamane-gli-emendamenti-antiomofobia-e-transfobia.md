---
title: 'DEPOSITATI STAMANE GLI EMENDAMENTI ANTIOMOFOBIA E TRANSFOBIA'
date: Tue, 06 Oct 2009 07:23:15 +0000
draft: false
tags: [Comunicati stampa]
---

**OMOFOBIA E TRANSFOBIA: RITA BERNARDINI, DEPUTATA RADICALE PD HA DEPOSITATO STAMANE IN COMMISSIONE GIUSTIZIA OTTO EMENDAMENTI ANTI OMOFOBIA E TRANSFOBIA ELABORATI CON IL CONTRIBUTO DELL’ASSOCIAZIONE RADICALE CERTI DIRITTI.**

Comunicato Stampa dell'Associazione Radicale Certi Diritti:

**Questa mattina Rita Bernardini, deputata radicale – pd, ha depositato in Commissione Giustizia otto emendamenti migliorativi del testo unificato antiomofobia votato venerdì scorso dalla Commissione. Ci auguriamo che i membri della Commissione Giustizia, che si riunirà oggi alle ore 13 per discutere e votare gli emendamenti, prima che il provvedimento arrivi in aula lunedì della prossima settimana, sappiano cogliere l’importanza di questa occasione che ha tra i suoi obiettivi quello della lotta anche alla transfobia e organizzare meglio nella società tutte le iniziative contro questa forma di violenza.**

Di seguito i testi degli emendamenti depositati:

**Emendamento 1**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**ART. 1**

L’art. 1 è sostituito dal seguente:

“Art. 1-

1\. Dopo l’art. 1 della Legge n. 122/1993 è aggiunto il seguente:

art. 1-bis (_Discriminazione, odio o violenza per motivi connessi all’identità di genere, all’orientamento sessuale_)

Le previsioni di cui alla presente legge si applicano anche nei casi di atti di discriminazione, odio o violenza per motivi connessi all’identità di genere ed all’orientamento sessuale.

2\. All’art. 3, comma 1, Legge n. 122 del 1993, dopo la parola: “religioso”, sono aggiunte le seguenti: “o basato su orientamento sessuale o identità di genere”

**Emendamento 2**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**ART. 1**

L’art. 1 è sostituito dal seguente:

“Art. 1

All’art. 61, comma 1, del codice penale, dopo il numero 11-ter) è aggiunto il seguente:

11-quater) l’aver commesso il fatto per motivi connessi a discriminazione, odio o violenza relativi all’identità di genere e all’orientamento sessuale della persona vittima del reato”

**Emendamento 3**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**ART. 1**

All’art. 1 le parole “nei delitti non colposi contro la vita e l’incolumità individuale, contro la personalità individuale, contro la libertà personale e contro la libertà morale” sono soppresse.

**Emendamento 4**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE  
**

**ART. 1**

All’art. 1 sostituire le parole: “per finalità all’orientamento o alla discriminazione sessuale della persona offesa del reato”, con le seguenti: “per motivi connessi all’orientamento sessuale o all’identità di genere della persona vittima del reato. La circostanza aggravante si realizza quando il reato è preceduto, accompagnato o seguito da atti o parole che ledano l’onore della persona vittima del reato o di gruppi di persone di cui fa parte, a ragione del suo orientamento sessuale o identità di genere, vera o presunta”.

**Emendamento 5**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**ART. 1**

All’art. 1 sostituire le parole: “per finalità all’orientamento o alla discriminazione sessuale della persona offesa del reato”, con le seguenti: “per motivi connessi all’orientamento sessuale o all’identità di genere della persona vittima del reato”.

**Emendamento 6**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE  
**

**ART. 1**

All’articolo 1, dopo le parole “discriminazione sessuale” aggiungere le parole “o alla transessualità”.

**Emendamento 7**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**Art. 1**

Dopo l’articolo 1 è aggiunto il seguente:

“Art. 1-bis

Al’articolo 69 del codice penale è aggiunto il seguente comma: “le circostanze attenuanti, diverse da quella prevista dall’articolo 98, concorrenti con l’aggravante di cui al numero 11-quater dell’articolo 61 non possono essere ritenute equivalenti o prevalenti rispetto a questa e le diminuzioni di pena si operano sulla quantità di pena risultante dall’aumento conseguente alla predetta aggravante”.

**Emendamento 8**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**ART. 1**

Dopo l’art. 1, è aggiunto il seguente:

“Art. 1- ter .

1\. Il Governo entro novanta giorni dall’entrata in vigore della presente legge, approva, sentite le Commissioni parlamentari competenti, un Piano triennale contro le discriminazioni, con riferimento a quanto previsto dall’art. 13 del Trattato CE, anche in coordinamento con le attività previste ai sensi delle Direttive 2000/43/EC e 2000/78/EC già recepite dall’ordinamento italiano.

2\. Il Piano deve prevedere obiettivi, risorse e metodi di valutazione per il monitoraggio, la prevenzione, il contrasto e l’assistenza alle vittime di discriminazione o atti di violenza connessi all’identità di genere, all’orientamento sessuale, alla religione, all’origine etnica o geografica, all’età o alla condizione di disabilità della persona vittima del reato o della discriminazione. Il Piano deve prevedere, tra l’altro, specifiche campagne di comunicazione sociale ed iniziative di educazione \- anche sui temi connessi alla sessualità umana - nelle scuole e presso l’associazionismo giovanile.

3\. Il Piano deve inoltre prevedere la trasformazione dell’UNAR in Agenzia Nazionale contro le discriminazioni che, in autonomia dal Governo e con l’assegnazione alla stessa di adeguate risorse, diventi il soggetto che sovrintende e gestisce l’attuazione del Piano nazionale stesso ”.