---
title: 'ITALIANO MORTO A MADRID: NESSUNO IN ITALIA PARLA SCOMPARSA SUA FAMIGLIA'
date: Fri, 22 Aug 2008 11:15:16 +0000
draft: false
tags: [Comunicati stampa]
---

**L’unico italiano morto nell’incidente aereo di Madrid faceva parte di una famiglia, morta con lui. Ma in Italia non se ne deve parlare**

Roma, 22 agosto 2008 • Dichiarazione di Sergio Rovasio, Segretario Associazione radicale Certi Diritti:

La tragedia umana di Domenico Riso, unico italiano morto nell’incidente aereo di Madrid, è duplice. Nessuno in Italia, neanche i media, ad eccezione del Corriere della Sera che ha acceso un faro sulla sua vita , hanno riconosciuto che la sua morte, con quella del suo compagno e del bambino che viveva con loro a Parigi, è la fine di una famiglia. Una fine tragica, come lo è ogni famiglia che muore, che non c’è più a causa di un grave incidente.

Nessuno ha parlato di una famiglia che non c’è più, del vuoto di esistenza, di progetti, di felicità, di vita di cui non rimane più nulla. E’ tabù parlare in Italia di questo tipo di famiglie, anche quando c’è di mezzo un bambino, anche se quel bambino è morto nell’incidente. Se in Italia non è riconosciuto alcun diritto a questo tipo di famiglie perché bisognerebbe riconoscerne l’esistenza anche quando ne muore una? Viva l’Italia che vive nell’ipocrisia, dove anche se c’è una famiglia non deve esistere, né da viva né da morta, e che non merita neanche una messa.