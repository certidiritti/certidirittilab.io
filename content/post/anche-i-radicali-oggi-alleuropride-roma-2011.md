---
title: 'Anche i Radicali oggi all''Europride Roma 2011'
date: Sat, 11 Jun 2011 07:05:40 +0000
draft: false
tags: [Europa]
---

**I Radicali saranno oggi all'europride per chiedere il superamento delle diseguaglianze anche in Italia, più laicità e democrazia.  
**

**Sul carro di Certi Diritti interventi di Parlamentari, esponenti della comunità gay e  il ricordo di David Kato Kisule.  
**

**Comunicato Stampa dell’Associazione Radicale Certi Diritti**

Roma, 11 giugno 2011

Anche i Radicali saranno oggi alla Parata Europride Roma che  partirà oggi pomeriggio alle ore 15,30 da Piazza Esedra – Roma e che vedrà la partecipazione di centinaia di migliaia di persone provenienti da tutta Europa.

Ci sarà un grande carro Radicale organizzato dalle Associazioni Radicali aderenti all'Europride, insieme agli iscritti, parlamentari e militanti, con musica (trash, elettronica), interventi al microfono, distribuzione di preservativi e materiale informativo anche sull'iniziativa nonviolenta in corso sulla grave situazione delle carceri italiane.

Il carro dei Radicali ricorderà la figura di David Kato Kisule, l'esponente di Sexual Minorities Uganda ucciso a Kampala lo scorso gennaio, Enzo Francone e Marcella Di Folco.

Sul carro si alterneranno alla musica interventi di Marco Pannella, Mina Welby, dei Parlamentari Radicali Rita Bernardini, Marco Perduca, del Consigliere Regionale del Lazio Rocco Berardo.

Sarà ospite del carro dei Radicali anche anche  Kasha Jaqueline Nabagesera, Direttrice di Far Uganda, amica e compagna di David Kato Kisule, le due coppie gay milanesi che hanno ieri presentato un ricorso legale alla Corte Europea dei Diritti dell’Uomo contro l’Italia.

Tra gli ospiti ci saranno Helena Velena e la Drag Queen La Poppea.