---
title: 'IV CONGRESSO CERTI DIRITTI: INFORMAZIONI LOGISTICHE'
date: Sun, 07 Nov 2010 10:04:05 +0000
draft: false
tags: [Comunicati stampa]
---

**CITTADINI IN EUROPA, FANTASMI IN ITALIA:  
FERMIAMO LA PESTE ITALIANA!**

**IV CONGRESSO dell'ASSOCIAZIONE RADICALE CERTI DIRITTI**

sabato 27 novembre  
Hotel Palatino - Via Cavour 213 ROMA

domenica 28 novembre  
Partito Radicale - Via di Torre Argentina 76 ROMA

**Elenco strutture consigliate da Certi Diritti per pernottamento a Roma:**  
(per gli studenti e i senza reddito stiamo organizzando sistemazioni presso amici e compagni di Roma. Invitiamo coloro che vivono a Roma a darci disponibilità ad ospitare i congressisti e chi è di fuori Roma a segnalarci tale necessità all’indirizzo e-mail: [info@certidiritti.it](mailto:info@certidiritti.it))

**B&B TOURIST HOUSE**  
Via Cavour, 211 (vicino sede congresso)  
Tel. 06-47824682

**MONTI GUEST HOUSE**  
Via dei Ciancaleoni, 1 (vicino sede congresso - Fermata “Cavour” metro linea B)  
Tel. 06 4870089

**B&B ARES ROOM**  
Via Domenichino, 7 (vicino sede congresso)  
Tel. 06 4744525

**NICE APARTMENT IN ROME**  
Molto economico (stanze triple, doppie, anche senza bagno) a partire da 20 Euro per notte. Via Milazzo – Via Magenta – Via dei Mille (Vicino Stazione Termini)  
e-mail: [nicerome@hotmail.com](mailto:nicerome@hotmail.com) \- Tel. 340-1425125 (chiedere di Mr. Raman)

**HOTEL POMEZIA**  
Via Chiavari, 12 (Vicino Campo de’ Fiori)  
Tel. 06 6861371 - 6864301

**HOTEL PALATINO**  
Via Cavour, 213 (dove facciamo il congresso, un po’ caro)  
Tel. 06 4814927

**OSTELLO DI ROMA**  
Viale delle Olimpiadi, 61 (molto economico ma lontano da sede congresso)  
Tel. 06 3236267 - 06 3242613

**B&B FRIENDSHIP PLACE**  
Via Milazzo, 14 (economico ma non verificato)  
Tel. 06 87450051