---
title: 'Assistenza sessuale: presentato al Senato il DdL su Assistenza Sessuale per le persone disabili. Presenti all''appello anche l''Associazione Luca Coscioni e l''Associazione Radicale Certi Diritti.'
date: Thu, 24 Apr 2014 12:46:45 +0000
draft: false
tags: [Salute sessuale]
---

[![Sessualità e disabilità](http://www.certidiritti.org/wp-content/uploads/2014/04/Sessualità-e-disabilità-300x151.jpg)](http://www.certidiritti.org/wp-content/uploads/2014/04/Sessualità-e-disabilità.jpg)Presentato oggi alle 13:00 nella cornice della sala Caduti di Nassiryia del Senato il Disegno di Legge sull'assistenza sessuale per le pesone con disabilità. Il testo - presentato dai senatori Sergio Lo Giudice; Cirinnà; D’Adda; Guerra; Ichino; Manconi; Maran; Mastrangeli; Mattesini; Pezzopane; Ricchiuti; Spilabotte; Valentini - mira ad introdurre delle nuove figure professionali qualificate che garantiscano il benessere sessuale delle persone con disabilità.

Presenti alla conferenza stampa anche Maximiliano Ulivieri, portavoce del Comitato per una legge sull'assistenza sessuale, Giorgia Wurth, scrittrice, autrice del romanzo “L’accarezzatrice”, Fabrizio Quattrini, psicoterapista e sessuologo, Università de l'Aquila, Lorenzo Amurri, scrittore e blogger, autore di "Apnea", Leonardo Monaco, Associazione Luca Coscioni e Associazione Radicale Certi Diritti.

Il radicale Leonardo Monaco: "Il deposito di questo testo è un passo fondamentale che pone, a scanso di equivoci e manipolazioni, le prime definizioni che caratterizzano il servizio di assistenza sessuale. L'Associazione Luca Coscioni e l'Associazione Radicale Certi Diritti sosterranno il procedimento attraverso gli strumenti di lotta nonviolenta che caratterizzano da anni l'operato extraparlamentare dei queste organizzazioni Radicali. Il nostro motto è 'dal corpo dei malati al cuore della politica': oltre a dar voce agli scienziati e ai professionisti ci concentreremo sui poteziali destinatari del servizio. Ringraziamo da subito gli amici dell'Associazione Coscioni Severino Mingroni e Alessandro Frezzato, che coraggiosamente si sono resi disponibili a dar tutto il loro contributo per questa battaglia di civiltà per la libertà sessuale di tutti e contro il proibizionismo sessuofobico sui corpi delle persone con disabilità."

Comunicato stampa dell'[Associazione Luca Coscioni](http://www.associazionelucacoscioni.it/) e dell'Associazione Radicale Certi Diritti