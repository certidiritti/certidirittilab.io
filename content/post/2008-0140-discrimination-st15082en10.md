---
title: '2008-0140-Discrimination-ST15082.EN10'
date: Thu, 04 Nov 2010 13:26:30 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 4 November 2010

Interinstitutional File:

2008/0140 (CNS)

15082/10

LIMITE

SOC 676

JAI 902

MI 418

  

  

  

  

  

NOTE

from :

General Secretariat

to :

The Working Party on Social Questions

on :

4 November 2010

No. prev. doc. :

13883/10 SOC 561 JAI 759 MI 317

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

In preparation of the next meeting of the Working Party, on 4 November 2010, delegations will find attached a note from the PL delegation.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Presidency questionnaire concerning housing**

**\[Document 13883/10\]**

**PL replies**

**1.         Situation in Member States**

**Both the Racial Equality Directive (2000/43/EC) and the Gender Goods and Services Equality Directive (2004/113/EC) cover housing available to the public in their scope. Several MS have gone beyond these two grounds in their protection against discrimination.**

a)         To what extent does your national legislation against discrimination apply to housing?

The legislation in force ensures the access of persons with disabilities to housing in newly designed multifamily buildings. Nonetheless it does not apply to the existing buildings. Moreover, there is no requirement to provide an access for persons with disabilities in one-family buildings.

**b)        Do you distinguish (for this purpose) between private and public/social housing?**

No, there is no such distinction in our legislation.

c)         Do you have statistics on the number of cases of discrimination in the area of housing, and if so, what is the most prevalent ground?

Poland does not hold statistics on the number nor on the reasons of discrimination in the field of housing. So far, no cases of discrimination in this field have been notified.

**2.         Social housing**

**a)         Do you consider social housing to be a service (for the purposes of EU law)?**

No – in the meaning of art. 57 of TFUE

**b)        If not, do you consider it to be a part of the social protection system?**

We don’t consider relevant to define it for the purposes of the antidiscrimination directive .

The ongoing work on the social services of general interest (and on its definition ) could help in further reflections on that.

**3\.         Private life**

**In some cases, the principle of equality might be in conflict with the right to private life, in particular in the area of housing (e.g. renting out a room in your own apartment).**

**a)         Is this better taken into account by an exclusion of transactions in the area of private life from the scope of the Directive, or by an exclusion of activities which are not commercial or professional?**

Each person, while searching for a flat, takes into consideration individual requirements (criteria). Consequently, the persons with disabilities will look for flats easily available for them. There is no reasonable justification for making all flats on the free market available for persons with disabilities. The construction and maintenance of such flats would place an excessive financial burden on society. Only some flats should be designed according to the specific and individualized needs of persons with disabilities. Such an approach would reduce the cost implications.

Poland has one of the highest  housing shortages in the EU; for that reason, the scope of the legal intervention in the right of disposal of private dwellings for rent (partly or wholly) should possibly be restricted. The experience of the last 20 years reveals the negative impact of legal restrictions associated with excessive protection of tenants' rights - on the development of the rental market in Poland.

**b)        How is commercial/professional housing defined in your national legislation?**

Polish legislation does not contain a definition of commercial housing. The national legal acts setting out technical requirements refer to single-family and multi-family residential buildings. There are no differences based on the commercial or social character of the housing investments.

**4\.** **Disability - reasonable accommodation**

**a)      In your view, what should the obligation to provide reasonable accommodation mean for providers of housing?**

Reasonable accommodation should cover:

1\.         A wide door,

2\.         Adequate size of communication space and sanitary rooms (ensuring that it is possible to manoeuvre a wheelchair)

3\.         Passages and accessibility to buildings through ramps and lifts  (but not necessarily to all floors in the building )

**a)** **Is there legislation on the accessibility of buildings (private as well as public/social) in your country?**

Yes, the legislation in force ensures accessibility of buildings for persons with disabilities , by setting the requirements concerning the door width, the size of communication space and sanitary rooms ensuring that it is possible to manoeuvre a wheelchair, as well as the accessibility to buildings through ramps and lifts (but not necessarily to all floors in the building)

**5.         Disability - accessibility**

**a)         What should the obligation to provide for accessibility measures encompass in the area of housing?**

This obligation should encompass:

-           the ground floor in low multi-family residential buildings,

-           all floors in semi-high and high multi-family residential buildings (there is, in any case, an obligation to install a lift for common utility purposes)

**b)        Should there be a difference between old and new buildings?**

Yes, there should be. In old buildings a lift cannot always be installed, for example, for technical reasons or due to historic value/restoring reasons. Therefore the strict recommendation to align the accessibility requirements for new and old buildings would be ill-advised.

**c)         Do you support immediate applicability with respect to new buildings?**

Yes, we do. A legal requirement to ensure the accessibility of new buildings for persons with disabilities already exists in Poland. The building will not be authorised for use if the accessibility requirement is not met.

**d)        Should there be a difference between the obligations for private and public/social housing respectively?**

No, there should not.

**e)** **Are there public funds available in your country for the adaptation of housing (private as well as public/social)?**

Yes, there are.

On the basis of _the_ _Act of 8 December 2006 on financial support for social housing, sheltered housing, and houses for the homeless,_ a governmental programme of Support for Social Housing has been pursued in Poland since 2007. It aims at helping local authorities to increase the accommodation available for the poorest.

The programme is supported and complemented by local authorities and public order organizations providing protected accommodation, which can be granted to persons, who need support in a everyday life, due to difficult  living situations , age, disability or illness (_Act of 12 March 2004 on social aid)_.

  

A special Fund in BGK Bank, subsidized  from the State budget, is another source of help granted within the programme of social housing support. An investor can obtain maximum support which ranges from 30 to 50 % of his/her overall costs. The financial support can be given for the construction of housing premises, but also for the adaptation of a building for housing purposes, as well as for the renovation of existing flats/buildings.

The programme of support for social housing, implemented from the middle of the 90s, remains the other main instrument in this area. Within the programme, the societies for social housing (special groups of investors, created mainly by local communities) and housing cooperatives can apply for preferential credits covering up to 70% the total costs, for the construction of housing for rent. The activity oriented towards the integration of people with disabilities is one of the criteria facilitating the access to the credit. An applicant obtains additional points if he/she signs an agreement with the community or the organization acting on behalf of people with disabilities, in which he/she engages himself/herself to rent the flat to the person indicated by the community or such organization. The flat should be adjusted to such person’s needs and built with the credit resources.

According to art. 35a.1 of the Act of 27 August 1997 on the vocational and social rehabilitation and employment of persons with disabilities, the _poviats (Polish districts)_ are among others entrusted in co-financing, from the resources of the State Fund for Rehabilitation of Persons with Disabilities (PFRON), of the removal of architectural barriers, barriers in communication and technical barriers, in connection with individual needs of persons with disabilities.

A person with a disability, having difficulties in moving, may apply for PFRON co-financing of the removal of architectural barriers. Such disability-related alteration of the premises in which he/she lives should enable or considerably facilitate the basic everyday activities of a person with a disability or his/her contacts with community. The person with a disability applying for the co-financing has to be the owner or perpetual user of the premises in which he/she lives, or must obtain the consent for adaptation from the landlord.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_