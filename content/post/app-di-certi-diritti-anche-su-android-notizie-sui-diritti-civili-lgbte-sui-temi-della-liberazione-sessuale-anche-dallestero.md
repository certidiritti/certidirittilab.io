---
title: 'App di Certi Diritti anche su Android. Notizie sui diritti civili Lgbt(e), sui temi della liberazione sessuale, anche dall''estero'
date: Tue, 27 Mar 2012 15:15:49 +0000
draft: false
tags: [Movimento LGBTI]
---

La App italiana per i diritti civili, dopo Iphone e Ipad, sbarca anche su Android. Notizie e commenti sulle iniziative dell'associazione e su quello che accade in Italia e all'estero.

Roma, 27 marzo 2012  
   
Comunicato Stampa dell'Associazione Radicale Certi Diritti

Dopo il successo della App di Certi Diritti su AppleStore, che ha visto più di 1000 download, Certi Diritti sbarca sulla piattaforma Android, per raggiungere un numero sempre maggiore di utenti interessati ai diritti civili delle persone LGBT(E), ai temi della liberazione sessuale, alle notizie su quello che accade in  molti paesi del mondo su questi temi. La App Certi Diritti, già presente su IPhone e IPad, è stata migliorata.  
  
Certi Diritti si avvarrà della piattaforma Mobile anche per finanziare le proprie iniziative politiche e giuridiche, partecipando a specifici progetti di fundraising pensati proprio per gli utilizzatori dei nuovi samrtphone. Infatti abbiamo già ricevuto donazioni necessarie per promuovere il libro “Piccolo Uovo” in tutte le biblioteche di Milano, e continueremo a chiedere il sostegno di chi ci segue per le specifiche iniziative che portiamo avanti.  
  
Infine, utilizzeremo la piattaforma mobile anche per “raccontare” e promuovere le nostre due ultime iniziative editoriali: “Dal cuore delle coppie al cuore del diritto” che contiene gli atti dell’udienza e della sentenza 138/2010 della Corte costituzionale sui matrimoni gay e il nuovo libero appena uscito “Certi Diritti che le coppie conviventi non sanno di avere”, un manuale pratico/legale per le coppie che convivono e che si trovano senza alcun riconoscimento da parte dello stato.

Il nome della App è Certi Diritti.  
[www.certidiritti.it](http://www.certidiritti.it/)