---
title: 'Corte Europea dei Diritti dell''Uomo: I ricorsi delle coppie dello stesso sesso che lamentano l''assenza di riconoscimento giuridico in Italia hanno superato la prima fase di delibazione'
date: Sat, 28 Dec 2013 13:45:03 +0000
draft: false
tags: [Diritto di Famiglia]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 28 dicembre 2013

La cancelleria della Corte europea dei diritti dell'uomo ha reso noto che i ricorsi riuniti noti in breve come Oliari e altri c. Italia di 5 coppie dello stesso sesso - 4 seguite dagli avvocati Marilisa D'Amico, Cesare Pitea, Chiara Ragni e Massimo Clara e una seguita dall'avvocato Alexander Schuster - ha superato positivamente una prima fase del procedimento e che si è, dunque, instaurato il contraddittorio con il Governo italiano, che sarà tenuto a trasmettere alla Corte europea dei diritti dell'uomo, entro la data del 26 marzo 2014, osservazioni scritte sul merito del ricorso.

Le coppie lamentano che lo Stato Italiano nulla ha fatto - nonostante il monito della Corte Costituzionale - per dare uno strumento giuridico di riconoscimento e di garanzia alle coppie cui non è consentito il matrimonio.

La Corte europea dei diritti dell'uomo non si è ancora espressa sulla ricevibilità e sul merito del ricorso, ma ha aperto la fase del contraddittorio con il Governo italiano. Questa fase, deputata allo scambio di osservazioni scritte tra le parti, dovrebbe concludersi entro il marzo 2014 2014 e, una volta terminata, la Corte europea dei diritti dell'uomo potrà procedere a decidere relativamente al pacchetto di ricorsi.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: «ringrazio sentitamente le coppie che, di fronte all'immobilismo italiano, si sono tenacemente rivolte alla giustizia europea e gli avvocati che le stanno assistendo con grandissima professionalità. Ancora una volta l'Italia si trova a dover render conto di diritti violati o non rispettati, sarà interessante sapere cosa scriverà il Governo nelle sue osservazioni. Mi auguro cheche il silenzio discriminatorio del legislatore nazionale finalmente finisca!».