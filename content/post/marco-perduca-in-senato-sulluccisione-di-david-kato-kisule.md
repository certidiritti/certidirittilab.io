---
title: 'Marco Perduca in Senato sull''uccisione di David Kato Kisule'
date: Thu, 03 Feb 2011 09:21:04 +0000
draft: false
tags: [Comunicati stampa]
---

  
Intervento in aula del senatore radicale Marco Perduca  
27 gennaio 2011

PERDUCA (PD). Domando di parlare.

PRESIDENTE. Ne ha facoltà.

PERDUCA (PD). Signor Presidente, vorrei sottolineare anzitutto che è stato poc'anzi svolto un dibattito procedurale sul modo in cui il Governo risponde in Aula alle interrogazioni, ma nel momento in cui si fa tardi i senatori si assentano e trasformano tutto in documenti scritti.

Oggi è il Giorno della Memoria e ritengo che, se non vi fosse stato l'intervento del senatore Villari, che in effetti ha ristabilito un minimo di memoria storica per l'avvio di questa legislatura, si sarebbero ascoltate molte dichiarazioni da chi è complice di una totale mancanza di rispetto delle regole. La vicenda della Presidenza della Commissione di vigilanza Rai è sicuramente una ferita alla legalità costituzionale. Credo abbia fatto bene il senatore Villari a ricordarlo, sia pure brevemente.

La memoria che oggi abbiamo voluto celebrare in quest'Aula purtroppo non appartiene al passato: esistono ancora persecuzioni, non più in virtù di un'appartenenza etnica, ma sempre più spesso in virtù di un'appartenenza religiosa e anche di un'identità di genere od orientamento sessuale. Ieri, alle ore 13 in Uganda, è stato trovato morto un iscritto all'associazione radicale «Certi diritti», un militante dei diritti gay che si chiamava David Kato, che nel novembre scorso avevamo invitato a Roma per presentare il libro sulla sua storia: la storia - appunto - di chi si batte in un Paese dove purtroppo (non si tratta dell'unico caso nell'Africa sub-sahariana) l'omosessualità è considerata di per sé un reato penale punibile fino a 14 anni di carcere. David Kato, assieme ad altre 100 persone in Uganda, era stato incluso in un articolo della rivista locale «Rolling Stone», che metteva alla berlina per presunta omosessualità persone che avevano anche responsabilità all'interno della società ugandese.  
Tra il 44 per cento di risposte del Ministero degli affari esteri alle interrogazioni, il sottosegretario Scotti aveva prontamente risposto ad una mia interrogazione in vista della presenza di David Kato in Italia, affermando che il nostro Paese, assieme ad altri europei, è molto attento agli sviluppi in quello Stato. Spero che domani ai funerali vi sia una rappresentanza della nostra diplomazia in Uganda e che l'Italia partecipi in maniera attiva affinché le indagini possano portare alla luce i responsabili.

Concludo suggerendo la visione di un video sul sito «Current TV», dove vi è - ahinoi - un'ulteriore strumentalizzazione della religione: predicatori evangelici che dicono «Dio odia i gay» ed una serie di manifestazioni di persecuzione quotidiana nei confronti di queste persone.

Ricordiamoci che la memoria ci deve far continuare a denunciare le stragi del passato; purtroppo, però, la distrazione, la mancanza di conoscenza e quindi l'impossibilità di creare la memoria sono una concausa della continuazione di persecuzioni nel 2011. (Applausi del senatore Tonini).