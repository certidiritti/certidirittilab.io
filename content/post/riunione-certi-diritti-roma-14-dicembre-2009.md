---
title: 'Riunione Certi Diritti Roma (14 dicembre 2009)'
date: Sat, 12 Dec 2009 11:30:36 +0000
draft: false
tags: [Senza categoria]
---

Carissimi tutte e tutti,  
dopo il lutto che ha colpito la nostra associazione nei giorni scorsi, abbiamo deciso di riunire il coordinamento romano di Certi Diritti lunedì 14 dicembre alle ore 21.00 in [Via di Torre Argentina 76](http://maps.google.it/maps?f=q&source=s_q&hl=it&geocode=&q=via+di+torre+argentina+76&sll=41.889903,12.498398&sspn=0.007364,0.01929&g=Via+Tommaso+Grossi,+6,+00184+Roma,+Lazio&ie=UTF8&hq=&hnear=Via+di+Torre+Argentina,+76,+00186+Roma,+Lazio&ll=41.896895,12.477164&spn=0.003682,0.009645&t=h&z=17), terzo piano, presso la sede del Partito Radicale.  
All’ordine del giorno ci saranno le istanze da portare alla prossima riunione del coordinamento nazionale delle associazioni lesbo, trans e gay (19 dicembre), i preparativi per il congresso nazionale di Certi Diritti (30-31 gennaio) e lo stato delle iniziative che stiamo mettendo in atto per la primavera.  
Vi aspettiamo quindi numerosi alle 21.00 in [Via di Torre Argentina 76](http://maps.google.it/maps?f=q&source=s_q&hl=it&geocode=&q=via+di+torre+argentina+76&sll=41.889903,12.498398&sspn=0.007364,0.01929&g=Via+Tommaso+Grossi,+6,+00184+Roma,+Lazio&ie=UTF8&hq=&hnear=Via+di+Torre+Argentina,+76,+00186+Roma,+Lazio&ll=41.896895,12.477164&spn=0.003682,0.009645&t=h&z=17), 3° piano.

  
Certi Diritti Roma  
Luca “Lucky” Amato