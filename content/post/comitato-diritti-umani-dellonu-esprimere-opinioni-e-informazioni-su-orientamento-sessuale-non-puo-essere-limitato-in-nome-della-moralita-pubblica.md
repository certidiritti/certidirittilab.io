---
title: 'Comitato diritti umani dell''ONU: esprimere opinioni e informazioni su orientamento sessuale non può essere limitato in nome della moralità pubblica'
date: Tue, 27 Nov 2012 09:52:07 +0000
draft: false
tags: [Transnazionale]
---

Comunicato Stampa dell’Associazione Radicale Certi Diritti.

Roma, 27 novembre 2012

L’Associazione Radicale Certi Diritti plaude alla storica decisione presa, lo scorso 19 novembre, dal Comitato ONU per i Diritti Umani (OHCHR) sul [caso Irina Fedotova v. Russian Federation](http://www2.ohchr.org/english/bodies/hrc/docs/CCPR.C.106.D.1932.2010.doc): l’OHCHR riconosce che esprimere opinioni e informazioni riguardanti l’orientamento sessuale non può essere limitato in nome della moralità pubblica.

Irina Fedotova, un’attivista lesbica, tentò di organizzare, nel marzo 2009, un manifestazione per promuovere tolleranza nei confronti delle persone LGBTI, esponendo dei cartelli con scritto “l’omosessualità è normale” e “sono orgogliosa della mia omosessualità”.

Sulla base di una legge simile a quella in vigore nella regione di San Pietroburgo che vieta la cosiddetta “propaganda omosessuale” in presenza di minori Irina è stata arrestata e condannata a pagare una multa di 1.500 rubli. Irina ha quindi fatto ricorso arrivando sino al Giudice Federale il quale, tuttavia, ha stabilito che proibire informazioni “capaci di provocare danni alla salute pubblica, allo sviluppo morale e spirituale nonché di dar vita a concezioni perverse riguardanti le relazioni tra famiglie tradizionali e non tradizionali” non può essere considerata una violazione della libertà di espressione.

L’OHCHR ha invece stabilito che la Russia ha violato gli articoli 19 e 26 della Convenzione internazionale sui diritti civili e politici e quindi la libertà d’espressione e di non essere discriminata di Irina. L’OHCHR ha enfatizzato che la morale pubblica deriva da “varie tradizioni sociali, filosofiche e religiose” e non può basarsi su una singola tradizione e che l’articolo 26 proibisce anche le discriminazioni basate sull’orientamento sessuale.

L’OHCHR ha sostenuto che la Russia “non è riuscita a dimostrare” che la proibizione della propaganda omosessuale, a differenza di quella eterosessuale o sessuale in genere, tra i minori “si basi su criteri ragionevoli e oggettivi”.

Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti dichiara: “A pochi giorni dalla decisione del Comune di Milano di sospendere gli effetti del gemellaggio tra Milano e San Pietroburgo finché la legge contro la cosiddetta ‘propaganda omosessuale’ non sarà rimossa, apprendiamo con gioia questa storica decisione del Comitato ONU per i Diritti Umani. Siamo felici di aver contribuito a portare Milano dalla parte delle Nazioni Unite e dei diritti umani contro i rigurgiti liberticidi di alcuni paesi. Spero vivamente che Torino, da poco firmataria di una accordo di cooperazione con San Pietroburgo e Venezia, gemellata all’ex capitale zarista dal 2006, vogliano dare un segnale altrettanto forte contro una violazione dei diritti umani ora certificata anche dalle Nazioni Unite. Sarebbe opportuno che la Russia però non aspettasse altri segnali di condanna per revocare tutte le leggi regionali che vietano la cosiddetta ‘propaganda omosessuale’ e che violano le più importanti convenzioni internazionali sui diritti umani, civili e politici che pure la Russia ha firmato”.