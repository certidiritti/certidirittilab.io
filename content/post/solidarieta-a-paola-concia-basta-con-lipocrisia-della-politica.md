---
title: 'Solidarietà a Paola Concia. Basta con l''ipocrisia della politica'
date: Thu, 21 Apr 2011 12:51:20 +0000
draft: false
tags: [Politica]
---

La classe politica si muova per riconoscere nei fatti cittadinanza alle persone lgbt e la smetta con l'ipocrisia delle semplici dichiarazioni  
  
Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Dirtti  
  
Roma, 21 aprile 2011  
  
“La sfilata di dichiarazioni manifestatasi oggi per l’attacco subito dalla deputata Paola Concia, colpevole di tenersi per mano con la sua compagna, che merita ovviamente tutta la nostra amicizia, vicinanza e solidarietà,  dimostra che la classe politica, a parte una piccola minoranza che campa grazie alla demagogia e al populismo d’accatto, è d’accordo nel condannare gli atti di violenza e di offesa verso le persone gay e lesbiche. Ciò che però  la classe politica dovrebbe capire è che fino a quando rimane indifferente, ponendo anche veti e offese, nei confronti di proposte legislative, che consentirebbero alle persone lesbiche e omosessuali di vedersi riconosciuti diritti e cittadinanza, non fa altro che alimentare pregiudizio e odio che spesso diventa anche violenza.  
E’ forse ora che tutti siano ben consapevoli che negare la promozione di politiche sui diritti civili, altro record negativo dell’Italia di oggi, come ad esempio le proposte di legge contro le discriminazioni, sulle unioni civili e il matrimonio tra persone dello stesso sesso,  equivale a negare la dignità e il rispetto delle persone lesbiche e gay. E’ davvero inutile dire che si è contro la violenza se poi si considera una parte della cittadinanza come 'peccatrice' o ‘deviata’ o che vive con ‘disordine’ la propria condizione.  
Forse è giunto il momento di smetterla con l’ipocrisia  e muoversi tutti insieme per far diventare l’Italia, anche su questo, un po’ più civile ed europea”.