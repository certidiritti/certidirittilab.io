---
title: 'Specific motion on the persecution of people  based on their sexual orientation all over the world  and in Africa in particular'
date: Wed, 14 Dec 2011 08:05:41 +0000
draft: false
tags: [Africa]
---

**Considering that**

76 Countries all over the world criminalise relationships between individuals of the same gender, and 7 of these Countries impose death penalty (Iran, Mauritania, 12 States in the north of Nigeria, Saudi Arabia, Southern Somalia, Sudan and Yemen);

**Considering**

That the House of Representatives of Nigeria approved in its first reading the ‘bill for the prohibition of marriage between people of the same gender’, after its adoption by the Senate, and that the bill risks to be enacted in the next few days or weeks;

That the draft law was amended to such an extent that it provides for the punishment by imprisonment of up to 14 years not only for those who married an individual of the same gender, but also for the people of the same gender showing their affection in public;

That imprisonment of up to 10 years is inflicted to all those who “encourage or help them”;

That the law will apply, inter alia, to expatriates, tourists and to all individuals in Nigeria having homosexual relations;

That anyone joining, operating or attending a “gay club, association or organisation” would be liable for a sentence of 10 years’ imprisonment;

That some activities and funding provided by Italy or by the EU for the protection of human rights could be affected in case the bill is adopted;

That besides breaching the international agreements signed by Nigeria in the field of human rights, such as the right to a private and family life, the right to the freedom of expression and the right to non-discrimination, such law would definitively undermine the AIDS prevention policies in a Country in which 3.5 million people are HIV-positive (UN estimate of 2009);

**Having considered**

That the Cameroonian law-maker seems to be willing to review his laws with the aim to introduce a discrimination based on sexual orientation, by exacerbating the sanctions for sexual acts with minors only if involving people of the same gender and not individuals of different genders;

That following several cases of homophobia directly supported by the State as from the last spring, three people were sentenced to 5 years of imprisonment for their real or alleged homosexual orientation;

That also the case of Jean-Claude Roger Mbede is particularly relevant. He is a 31-year old student that, after his imprisonment on March 2nd, was detained for seven days before being accused of homosexuality and of incitement to homosexuality, therefore forced to serve a sentence of three-years’ imprisonment at the central prison of Kondengui, where detainment conditions are difficult due to a severe overcrowding, scarce levels of sanitation and shortage of food;

**Having considered**

That the Parliament of Uganda has not yet adopted the bill aimed at introducing the death penalty for homosexual people; this bill was heavily opposed by our member David Kato Kisule, brutally murdered for his struggle in favour of the rights of sexual minorities, and that this bill still risks to be approved;

**Having considered**

That in the Russian Federation bills are being examined aiming to criminalise whatever activity or information relating to LGBT people and to relations between individuals of the same gender, in violation of the freedom of expression and association;

**Having considered**

That the right of people not to be discriminated or persecuted on the basis of their sexual orientation is a fundamental right, as it is also stated by the resolution adopted by the UN Human Rights Council on 17th June 2011;

**Invites**

The Nonviolent Radical Party Transnational and Transparty to adopt the necessary initiatives at national, European and International level to denounce and fight against the persecutions and discriminations suffered by people in consideration of their sexual orientation all over the world, starting from Uganda, Nigeria, Cameroon and Russia;

To commit itself to a campaign for the decriminalisation of homosexuality at world level, starting from the approval by the UN General Assembly of a resolution on this subject.