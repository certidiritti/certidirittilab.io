---
title: 'CONSIGLIO D''EUROPA INTERVIENE IN DIFESA PERSONE TRANSESSUALI'
date: Thu, 08 Jan 2009 07:22:46 +0000
draft: false
tags: [commissario, Comunicati stampa, consiglio d'europa, diritti umani, transessuali]
---

Il Commissario dei Diritti Umani del Consiglio d'Europa, Thomas Hammarberg, lo scorso 5 gennaio, ha invitato tutti gli Stati membri a prendere tutte le misure concrete e necessarie per mettere fine alla transfobia e a tutte le discriminazioni verso le persone transessuali.

Il Commissario nella sua denuncia dice che vi sono troppe discriminazioni in Europa nei confronti delle transessuali riguardo il lavoro, l'abitazione e la sanità. Le persone transessuali, secondo il Commissario dei Diritti Umani dell'organismo europeo, parte dalla constatazione che i transessuali debbono affrontare oggi in Europa gravi problemi quotidiani frutto di  
una mancanza di sensibilita' circa la loro identita', di pregiudizi o piu'  
semplicemente di un rigetto.