---
title: 'Diritto di soggiorno del coniuge dello stesso sesso'
date: Sat, 18 Feb 2012 16:14:56 +0000
draft: false
tags: [Diritto di Famiglia]
---

Il Tribunale di Reggio Emilia riconosce come, quando sia stato celebrato il matrimonio tra persone dello stesso sesso in uno Stato che, come la Spagna, lo ammetta, coloro che l'hanno celebrato devono essere considerati coniugi anche in Italia. Di Sergio Briguglio.

Cari amici,  
alla pagina [](http://www.stranieriinitalia.it/briguglio/immigrazione-e-asilo/2012/febbraio/trib-re-coniuge-omosex.pdf)[http://www.stranieriinitalia.it/briguglio/immigrazione-e-asilo/2012/febbraio/trib-re-coniuge-omosex.pdf](http://www.stranieriinitalia.it/briguglio/immigrazione-e-asilo/2012/febbraio/trib-re-coniuge-omosex.pdf) troverete un'importantissima ordinanza del Tribunale di Reggio Emilia.

Il Tribunale accoglie il ricorso di un cittadino uruguayano contro il provvedimento dell'amministrazione italiana con sui si nega il riconoscimento del diritto di soggiorno ai sensi del D. Lgs. 30/2007.

Il cittadino uruguayano - e qui sta l'importanza del provvedimento del Tribunale - ha contratto matrimonio in Spagna con un cittadino italiano dello stesso sesso.

Il D. Lgs. 30/2007, che da' attuazione alla Direttiva 2004/38/CE, riconosce il diritto di soggiorno ai familiari (anche stranieri) dei cittadini dell'Unione europea (inclusi i cittadini italiani) purche' rientrino in determinate categorie:

1) il coniuge;

2) il partner che abbia contratto con il cittadino dell'Unione un'unione registrata sulla base della legislazione di uno Stato membro, qualora la legislazione italiana equipari l'unione registrata al matrimonio e nel rispetto delle condizioni previste dalla pertinente legislazione del nostro paese;  
   
3) i discendenti diretti di eta' inferiore a 21 anni o a carico e quelli del coniuge o partner di cui alla lettera b);  
   
4) gli ascendenti diretti a carico e quelli del coniuge o partner di cui alla lettera b).

Finora, il diritto del compagno dello stesso sesso di un cittadino comunitario o italiano aveva trovato un ostacolo insormontabile nella condizione che ho evidenziato in grassetto: non essendo equiparate al matrimonio dalla normativa italiana le unioni registrate, l'aver contratto una tale unione in altro Stato membro non consentiva al cittadino straniero di risiedere in Italia, per questo solo motivo, col compagno comunitario o italiano.

Ora, il Tribunale di Reggio Emilia riconosce come, quando sia stato celebrato il matrimonio tra persone dello stesso sesso in uno Stato che, come la Spagna, lo ammetta, coloro che l'hanno celebrato devono essere considerati coniugi anche in Italia, ai fini del diritto di libera circolazione (non non dal punto di vista del diritto di famiglia). Il diritto di soggiorno al cittadino uruguayano ricorrente viene cosi' riconosciuto perche' questi rientra nella categoria di coniuge, non in quella, meno tutelata, di partner registrato.

Notate che non e' rilevante che il matrimonio sia stato celebrato in uno Stato membro: la Commissione europea, in proposito, ha affermato come, ai fini dell'applicazione della Direttiva 2004/38/CE, debbano essere riconosciuti, in linea di principio, tutti i matrimoni contratti validamente in qualsiasi parte del mondo ([](http://www.stranieriinitalia.it/briguglio/immigrazione-e-asilo/2009/luglio/com-comm-ue-dir-38-04.pdf)[http://www.stranieriinitalia.it/briguglio/immigrazione-e-asilo/2009/luglio/com-comm-ue-dir-38-04.pdf](http://www.stranieriinitalia.it/briguglio/immigrazione-e-asilo/2009/luglio/com-comm-ue-dir-38-04.pdf)).

Ringrazio Gabriella Friso e Giulia Perin, che mi hanno segnalato l'ordinanza.

Cordiali saluti  
sergio briguglio

fonte: [](http://briguglio.blogspot.com/2012/02/diritto-di-soggiorno-del-coniuge-dello.html)[http://briguglio.blogspot.com/2012/02/diritto-di-soggiorno-del-coniuge-dello.html](http://briguglio.blogspot.com/2012/02/diritto-di-soggiorno-del-coniuge-dello.html)  
  

**iscriviti alla newsletter >[  
](newsletter/newsletter)[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**