---
title: 'Congresso nazionale dei Notai propone modifiche al Codice civile per Riforma del Diritto di famiglia'
date: Thu, 20 Oct 2011 16:52:00 +0000
draft: false
tags: [Diritto di Famiglia]
---

Successione, accordi prematrimoniali e patti di convivenza: l'associazione radicale Certi Diritti saluta positivamente le proposte di modifica del codice civile fatte dal congresso nazionale del notariato. Primo passo verso la Riforma del Diritto di Famiglia.

In attesa di legge adeguate, Certi Diritti pubblicherà a breve un manuale sulle specifiche autonomie privatistiche già previste dalla legge.

Roma, 21 ottobre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Durante il 46° congresso nazionale del Notariato, il Consiglio Nazionale del Notariato ha presentato quattro proposte di riforma del Codice civile: successione testamentaria (due proposte), accordi prematrimoniali e patti di convivenza.

Impossibile non notare che perfino i Notai stanno spingendo per aggiornare il diritto di famiglia e per rendere il matrimonio più flessibile. Ma non solo: tre proposte su quattro riguardano proprio quelle specifiche conseguenze che i governi Italiani di ogni colore si ostinano a non voler riconoscere nè alle famiglie eterosessuali non sposate, nè alle coppie omosessuali alle quali invece il matrimonio viene addirittura negato.

L’Associazione Radicale Certi Diritti saluta positivamente questa iniziativa dei Notai italiani, segno che almeno qualche istituzione di categoria italiana presta attenzione alle reali esigenze delle famiglie formulando una proposta strutturale ed in modo ufficiale.

Dispiace che il Consiglio Nazionale del Notariato abbia mancato l'opportunità di mostrare quel coraggio che questa iniziativa richiederebbe, mostrandosi invece un po' troppo preoccupato di ribadire la centralità della famiglia tradizionale in un contesto in cui non è proprio necessario ribadirlo ("Vogliamo essere chiari: riteniamo che la famiglia tradizionale sia il perno della società"). Come se ormai i divorzi express in Romania (o in altre nazioni europee) non siano una realtà.

Quando si parla di autonomie privatistiche occorre essere chiari: sono strumenti che, in mancanza di meglio, hanno senso solo nella misura in cui migliorano la qualità della vita e preparano la strada per la conquista di diritti più certi, pubblici e armonizzati con una vera e propria Riforma del Diritto di famiglia.

Per questo stiamo per pubblicare un semplice manuale che raccoglie tutti quei diritti e autonomie private che la legge, in modo più o meno estemporaneo, già prevede, in particolare per le coppie che convivono. Abbiamo ritenuto questa pubblicazione importante e fortemente strategica proprio perché, oltre che informare le coppie conviventi sulle attuali possibilità di tutela della propria famiglia, ha l’obiettivo di formarle ad una consapevolezza civica sul percorso verso la piena uguaglianza che ogni persona discriminata può intraprendere e di cui può già usufruire oggi.

Certi Diritti grazie al lavoro di associazioni ed esperti ha elaborato una proposta di riforma del diritto di famiglia depositato alla Camera e al Senato dai parlamentari radicali.