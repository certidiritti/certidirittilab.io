---
title: 'Portugues'
date: Sun, 21 Jun 2009 07:24:10 +0000
draft: false
tags: [Senza categoria]
---

“CERTI DIRITTI”
===============

ASSOCIAÇÃO RADICAL
==================

As nossas prioridades são:
--------------------------

… de nos empenharmos fortemente para difundir e ajudar a campanha de afirmação civil, para o reconhecimento do direito de matrimônio entre pessoas do mesmo sexo. Aumentar o número de casais que decidem de apoiar essa iniciativa fazendo um ato público e promover o mesmo na comunidade LGBT italiana.  
…de nos empenharmos fortemente para alcançar uma plataforma comum entre todo o movimento LGBT unido, em modo laico, para perseguir objetivos comuns, indo além das singulares diferenças, legítimas mas parciais e ideais.  
A associação dedica as suas iniciativas em memória de Makwan Moloudzadeh (o jovem de 21 anos que foi enforcado no Irã, no dia 5 de dezembro de 2007, porque foi acusado de ter tido relações homossexuais) e em nome de todos aqueles que sofreram abusos, discriminações e violência por causa da sua orientação sexual.

AFIRMAÇÃO CIVIL: uma ação concreta por um direito que ninguém pode negar aos casais homossexuais: o de se casarem.
------------------------------------------------------------------------------------------------------------------

Na Itália o casamento entre pessoas do mesmo sexo não é proibido, mas as pessoas homossexuais não podem se casar.  
Nenhuma norma veda o casamento homossexual, mas os cartórios de Registro Civil se recusam a darem entrada na documentação. Um direito negado por causa dum preconceito milenar, que não encontra justificação nas leis italianas, graças a qual se poderia tranquilamente celebrar legitimamente os casamentos homossexuais.  
Esse é o objetivo de Afirmação Civil: acabar com esse preconceito, concretizar e dar prospectiva ao amor entre pessoas homossexuais que queiram se casar.  
Uma iniciativa importante, dentro da justiça e do direito, com a qual  Certi Diritti e a rede de advocacia, Rede Lenford, seguem passo a passo, para que os casais homossexuais possam coroar os projetos de amor deles através da decisão do juíz.  
Essa iniziativa tem como inspiração as campanhas de desobediência civil e da luta não violenta que no passado, como por exemplo nos Estados Unidos contra as leis de segregação, levaram os países democráticos a um progresso social substâncial, seja no campo dos direitos civis que no das reformas institucionais, através da superação de leis injustas que limitavam as oportunidades e a felicidade das pessoas.

AMOR CIVIL: uma proposta de Reforma do Direito de Família
---------------------------------------------------------

Certi Diritti participou do trabalho da Conferência permanente pela reforma do direito de família, coordenando a elaboração relativa à definição das diferentes formas de família e união. Se trata dum projeto também para a classe politica italiana, criado durante a iniciativa “Amor Civil”,  o qual tem como objetivo introduzir alguns direitos importantes que hoje são negados, entre os quais hão uma pertinência especial aqueles que tratam das formas de família, da adoção e dos filhos.