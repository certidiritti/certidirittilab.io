---
title: 'CERTI DIRITTI PARTECIPA ALL''INCONTRO CON IL SINDACO DI ROMA ALEMANNO'
date: Sat, 12 Jun 2010 09:33:23 +0000
draft: false
tags: [Comunicati stampa]
---

La delegazione dell'Associazione radicale Certi Diritti, composta da Giovanni Bastianelli e Roberta Bonelli, ha partecipato all'incontro ufficiale, insieme alle associazioni LGBTE del territorio, con il sindaco di Roma Alemanno per trattare temi legati alla lotta contro l'omo/transfobia.

L'esito dell'incontro è stato la fissazione degli obbiettivi di creare entro l'estate un ossservatorio per tutelare le vittime di attacchi omofobi e transfobici avvenuti nella capitale. Entro il prossimo mese ci sarà una nuova riunione con l'obbiettivo di dare luogo ad un dibattito costruttivo preliminare all'istituzione dell'osservatorio.

Si invitano quindi gli utenti del sito a postare, con lo strumento dei commenti, le loro proposte per presentarle alla riunione che avrà luogo nella prima metà di luglio