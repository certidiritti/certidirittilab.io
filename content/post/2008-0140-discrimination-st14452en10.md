---
title: '2008-0140-Discrimination-ST14452.EN10'
date: Mon, 04 Oct 2010 12:13:16 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 4 October 2010

Interinstitutional File:

2008/0140 (CNS)

14452/10

LIMITE

SOC 607

JAI 802

MI 351

  

  

  

  

  

NOTE

from :

General Secretariat

to :

The Working Party on Social Questions

on :

19 October 2010

No. prev. doc. :

13883/10 SOC 561 JAI 759 MI 317

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

In preparation of the next meeting of the Working Party, which is provisionally scheduled for 19 October 2010, delegations will find attached a note from the CZ delegation.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Presidency questionnaire concerning housing**

**\[Document 13883/10\]**

**CZ replies**

**1\.** **Situation in Member States**

**Both the Racial Equality Directive (2000/43/EC) and the Gender Goods and Services Equality Directive (2004/113/EC) cover housing available to the public in their scope. Several MS have gone beyond these two grounds in their protection against discrimination.**

**a)** **To what extent does your national legislation against discrimination apply to housing?**

The antidiscrimination act also aims directly at housing. The Act ensures equal treatment and protection against discrimination “in the areas of \[…\] access to goods, services which are available to the public, including housing.”

**b)** **Do you distinguish (for this purpose) between private and public/social housing?**

No, we do not. The antidiscrimination act speaks of housing in general without distinction. According to the principle of equality, all legal conditions for ownership as well for tenancy are applied for all persons - regardless the legal status of persons owing or lending dwellings and buildings.

**c)** **Do you have statistics on the number of cases of discrimination in the area of housing, and if so, what is the most prevalent ground?**

No, we do not have this kind of statistics.

**2\.** **Social housing**

**a)** **Do you consider social housing to be a service (for the purposes of EU law)?**

Yes, we do. As the social housing – as a non-market segment - is provided to socially defined target group, this measure is implemented in the public interest. Therefore, allocation criteria are not based on a market relationship (demand and supply), but on “social” parameters.

**b)** **If not, do you consider it to be a part of the social protection system?**

**3\.** **Private life**

**In some cases, the principle of equality might be in conflict with the right to private life, in particular in the area of housing (e.g. renting out a room in your own apartment).**

**a)** **Is this better taken into account by an exclusion of transactions in the area of private life from the scope of the Directive, or by an exclusion of activities which are not commercial or professional?**

We recommend using an exception for the right to private life. First, we believe it is difficult to distinguish explicitly whether the activity is a commercial or professional one. Second, in these cases (non-commercial/ non-professional activities), there is no right (like the right for private life) that could prevail over the right to be protected against discrimination. Moreover, housing – both, ownership and tenancy - has private character. Private life, including housing, has to be based on autonomies and contractual freedom of both parties – i.e. a seller and a buyer, or a landlord and a tenant.

**b)** **How is commercial/professional housing defined in your national legislation?**

Czech legislation doesn’t define commercial/professional activities in housing.

  

**4\.** **Disability - reasonable accommodation**

**a)** **In your view, what should the obligation to provide reasonable accommodation mean for providers of housing?**

The providers of housing in existing housing stock are restricted by current technical parameters of the building / apartment. It is important to motivate the providers to improve the accessibility of the buildings. Minimal technical parameters are set in the case of new housing. The parameters should gradually become the minimal parameters of all new housing projects. From the technical point of view: Any new dwelling – regardless the investor and regardless the legal status of future tenants - has to fulfil all technical conditions stipulated by the Building Code and its implementation regulations. In practice it means, that all newly built dwellings have all technical facilities, like EW, bathroom with hot water, kitchen, etc. From the legal point of view: According to the Civil Code, a landlord may end the occupational lease only for reasons stated in this law. If the landlord needs the rental flat in his building for himself, spouse, his children, grandchildren, son or daughter-in-law, his parents or siblings, the tenant has a right to a substitute flat of a comparable quality.

**b)** **Is there legislation on the accessibility of buildings (private as well as public/social) in your country?**

Yes, it is. The basic accessibility measures (wide doorways, same level floors and rooms, walls in bathrooms ready for handles) should be mandatory parameters for housing subsidised by public funding and gradually in all newly built housing. The binding legal framework for technical aspects as well as aspects of accessibility does not distinguish between various forms of legal status neither of investors (developers) nor the owners and landlords. The special act - Building Code (No. 183/2006 Coll.) - stipulates general requirements for construction, for the utilization of the area and technical requirements for structures determined by statutory implementing instruments and furthermore the general technical requirements ensuring the use of structures by persons of advanced age, pregnant women, persons accompanying a child in a pram,

  

a child under three years old, or possibly by mentally handicapped persons or persons with limited ability of physical exercise or orientation determined by statutory implementing instruments – so called barrier-free arrangement. These technical conditions are included in a Building permit – e.g. the building office determines the conditions for the realization of the structure, and if necessary also for its use, and it decides the objections of the participants in the proceedings. The office secures the protection of public priorities by conditions and it determines especially the relation to other conditional structures and installations, observance of general requirements for construction, including the requirements for a barrier-free use of the structure, or technical standards. A planning authority is responsible for ensuring that constructions, alterations to constructions and maintenance work permitted by it correspond to the general technical requirements for constructions including the requirements for barrier-free use of constructions.

**5\.** **Disability - accessibility**

**a)** **What should the obligation to provide for accessibility measures encompass in the area of housing?**

Legislation sets the technical parameters of accessibility of buildings. There is, however, no obligation to incorporate these apartments with “ordinary” apartments. Regulation No. 398/2009 Coll., on the general technical requirements providing for barrier-free use of structures, which was prepared and published by the Ministry for Regional Development in cooperation with the organisations defending the interests of the handicapped applies to the technical conditions for common parts of residential building as well as for common facilities in these buildings. If a family house or a flat “inside” are barrier-free, it depends on an individual priorities of a given owner or household living there. The above mentioned regulation also applies to dwelling “of special purpose” intended for seniors or disabled people. These so called “adjustable flats” are prepared – from the construction point of view – to be occupied, if necessary, in a barrier-free way.

  

*   The layout of each adjustable flat must correspond to the manoeuvrability of a wheelchair, its collision-free passage through all rooms and areas of the flat, including a space for storage of the wheelchair. Taking into account the expected location of furniture, living rooms, bedrooms, antechambers and hallways of the flat must allow 360-degree rotation of the wheelchair (a circle with a 1,500-millimetre diameter). Flats for more than one person must provide sufficient space to allow the moving and manoeuvring of two wheelchairs at a time in basic habitable rooms (living room and one bedroom).

*   The entrance door to the flat, other doors and passageways in the flat must be at least 900 millimetres wide. Exceptionally, the entrance door may be at least 800 millimetres wide. Save for the entrance door, all doors in the flat must have a removable threshold. There must be sufficient space in front and behind doors for manipulating a wheelchair.

*   Staircases and inclined ramps located in front of doors must have a horizontal section at least 1,500 or 2,000 millimetres long depending on the direction in which the doors open.

*   Floor surface must feature an anti-skid design with friction coefficient of at least 0.6.

*   Loggias and balconies must be at least 1,500 (optimally 1,800) millimetres deep and accessible at the floor level of the living room with a maximum elevation difference of 20 millimetres. At least a part of the railing must be designed so as to allow a person in a wheelchair to see the immediate vicinity of the building.

  

*   Bathroom walls must allow the anchoring of supporting handles in various positions. The shower booth must be fitted with a folding seat 500 millimetres above the floor allowing front or sideways access, anti-skid tiles, drain in the floor, and a handheld showerhead controlled by a lever. The supporting handle and a soap holder must be located at a distance reachable from the seat on a wall perpendicular to the wall to which the seat is attached. There must be an empty space next to the toilet bowl at least 800 millimetres wide; the front must be at least 750 millimetres from the rear wall. The door must open in the outward direction.

*   All fixtures controlled by hand (switches, outlets, fuses, doorknobs, flushing handles) must be located at a height between 600 and 1,200 millimetres. Windows must have levers located no higher than 1,100 millimetres above the floor.

*   Accessibility of all amenities, i.e. basement stalls, bicycle and stroller storage room, workshops, laundry rooms, a civilian protection shelter, and the flat itself, must be designed to meet the needs of persons using wheelchair. If residential buildings are designed to contain garages, any modifiable flat must be assigned a parking space 3,500 millimetres wide with a barrier-free access.

**b)** **Should there be a difference between old and new buildings?**

Yes, definitely it should. The obligation to reach certain parameters is reasonable only in newly built housing. It is possible to motivate providers of existing housing to introduce changes in parameters in order to increase accessibility. It is not possible to intend to apply the same conditions for old as for new buildings. All new residential buildings have to be barrier-free in all their common parts. Shops, cinemas, bus stations, department stores, office buildings, etc. have to be barrier-free in all parts. Old buildings have to become barrier-free only after a substantial reconstruction and always when it is technically possible. In other old building, it is possible only through some non-legislative motivations.

**c)** **Do you support immediate applicability with respect to new buildings?**

Yes, we do. We do however recommend a gradual implementation. Applicability for all new dwellings is possible immediately. But at the same time, it is necessary to take into account national standards and habits. There are various systems of social help and different compensation utilities are used; therefore, requirements for barrier-free environment vary, too.

**d)** **Should there be a difference between the obligations for private and public/social housing respectively?**

No, definitely it should not. Different conditions can be set for housing projects which are subsidised by public funding and which are not. Subsidised projects can have their parameters changed more quickly. Both should, however, have their parameters changed to accessible housing in the long run. It has no sense to take into “general” account the legal status of a developer, or an owner or landlord – these aspects are changeable and so are not decisive for the “physical” usage of a given building. Therefore, all technical parameters and standards should be applicable for residential buildings regardless the form of their ownership; with the exemption of publicly subsidized new housing construction.

**e)** **Are there public funds available in your country for the adaptation of housing (private as well as public/social)?**

Yes, they are. The Ministry of Labour and Social Affairs provides social benefits to be used for adaptation of housing (this is provided directly to people with disabilities). A defining obligation is to build a certain portion of the apartments as accessible for people with disabilities in cases of newly built housing subsidised by public funding. Social assistance benefits are aimed at severely handicapped persons, which are provided in accordance with Decree by the Czech Ministry of Labour and Social Affairs no. 182/1991 Coll. (to implement the Act on Social Security and Act on Jurisdiction

  

of Czech Republic Bodies in Social Security, as amended). They assist in addressing e.g. the need of transport, adjusted housing and special compensatory aids. One-off benefits are provided by Municipal Office of a municipality with extended powers, recurrent benefits by Municipal Office of an authorized municipality. Persons entitled to social assistance benefits are:

*   Permanent residents in the Czech Republic;

*   Beneficiaries of refugee status;

*   EU/EEA citizens under directly applicable EC legislation and theirs family members;

*   EU/EEA citizens with registered residence in the Czech Republic for at least 3 months and theirs family members.

Obligatory recurrent benefit for use of a barrier-free flat is provided to persons with severe handicaps of support and motion apparatus and to totally or almost totally blind persons, in case they use a barrier-free flat, at the amount of CZK 400 per month. Such persons may be further granted the recurrent facultative benefit for use of garage up to CZK 200 per month. There is also a special programme administered by the Ministry for Regional Development, which is aimed at the “Barrier-free Municipalities” within the scope of national regional policy. Owners of old home care buildings, where flats for seniors or disabled people are situated, can obtain subsidies for necessary reconstruction with objectives to remove barriers inside buildings.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_