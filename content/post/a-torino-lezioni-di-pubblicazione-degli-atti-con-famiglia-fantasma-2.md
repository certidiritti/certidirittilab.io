---
title: 'A TORINO LEZIONI DI PUBBLICAZIONE DEGLI ATTI CON FAMIGLIA FANTASMA'
date: Sat, 12 Apr 2008 10:00:11 +0000
draft: false
tags: [Comunicati stampa]
---

Venerdì 12 Aprile, alle ore 19:30 allo Shortbus Cafè, si terrà la presentazione del libro "La Famiglia Fantasma", in cui si parlerà della Rete Lenford e dell'associazione Certi Diritti.

Sarà possibile anche ascoltare un sunto delle telefonate fatte all'Ufficiale di Stato Civile del comune di Torino in merito alla richiesta di pubblicazione degli atti tra due uomini o tra due donne. Il tutto, supportato da un vademecum che aiuta a capire i passi più importanti di questa "introduzione al matrimonio omosessuale".

Appuntamento per tutti, quindi, allo Shortbus cafè in [via G. Ferrari 5](http://maps.google.it/maps?q=via+G.+Ferrari+5+,+torino&sourceid=navclient-ff&ie=UTF-8&rlz=1B3GGGL_itIT268IT269&um=1&sa=N&tab=wl) alle 19:30. Per maggiori informazioni, clicca [qui](http://famigliafantasma.freewordpress.it/2008/04/10/famiglia-fantasma-allo-shortbus-cafe/) oppure [qui](http://www.queerway.it/dblog/articolo.asp?articolo=972).