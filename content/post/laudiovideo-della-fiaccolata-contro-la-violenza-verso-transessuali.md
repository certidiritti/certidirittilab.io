---
title: 'L''AUDIOVIDEO DELLA FIACCOLATA CONTRO LA VIOLENZA VERSO TRANSESSUALI'
date: Sat, 29 Nov 2008 09:30:14 +0000
draft: false
tags: [audio, fiaccolata, radioradicale, Senza categoria, transessuali, video]
---

Al link che segue è possibile accedere all'audio-video prodotto da Radio Radicale sulla fiaccolata contro la violenza e le discriminazioni di cui sono vittime le persone transessuali che si è tenuta a Roma venerdì 28-11-2008
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

**[http://www.radioradicale.it/scheda/267879](http://www.radioradicale.it/scheda/267879)**