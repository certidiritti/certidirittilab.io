---
title: 'Gemellaggio Milano-San Pietroburgo: Certi Diritti organizza sit-in e chiede incontro a Pisapia'
date: Mon, 15 Oct 2012 09:41:27 +0000
draft: false
tags: [Russia]
---

L'associazione radicale Certi Diritti chiede a Pisapia di incontrare il Segretario della Commissione Diritti Umani del Senato, Sen.Marco Perduca, sulla sospensione del gemellaggio tra Milano e San Pietroburgo in occasione del presidio davanti a Palazzo Marino, dalle 18.30 di giovedì 18 ottobre.  
  

Comunicato stampa dell'Associazione Radicale Certi Diritti

Milano, 15 ottobre 2012

  
A 13 giorni dal lancio della campagna per la sospensione del gemellaggio tra Milano e San Pietroburgo, Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, scrive una lettera aperta al sindaco di Milano, Giuliano Pisapia,  per chiedergli di rompere il silenzio dell'amministrazione e d'incontrare il segretario della Commissione Diritti Umani del Senato, Marco Perduca, che parteciperà a un presidio davanti a Palazzo marino giovedì 18 ottobre dalle 18:30.

La lettera spiega le ragioni che hanno portato a dar vita alla campagna per la sospensione del gemellaggio che dal 2 ottobre, giorno di lancio della campagna e del 45° anniversario del gemellaggio, a oggi è già stata apprezzata da più di 1216 utenti Facebook  (una media di più di 100 "mi piace" al giorno).  
  
**[La campagna video >>>](http://www.youtube.com/watch?v=6cgpqaGqOwM&feature=relmfu)**  
  
**[La pagina facebook >>>](http://www.facebook.com/Stopalgemellaggio)**  
  

**Segue il testo della lettera aperta:**

Erg. Sig. Sindaco,

il 29 febbraio scorso, il parlamento di San Pietroburgo ha approvato una legge che sanziona la cosiddetta “propaganda dell’omosessualità” criminalizzando di fatto qualunque attività o informazione relativa alle persone LGBTI (Lesbiche, Gay, Bisessuali, Transessuali e Intersessuali) e alle relazioni tra persone dello stesso sesso, in patente violazione delle libertà di espressione e associazione, nonché degli impegni presi dalla Russia ratificando la Convenzione europea per la salvaguardia dei diritti dell’uomo e delle libertà fondamentali. Questa legge assurda ha già colpito molti cittadini russi che hanno tentato di manifestare per i diritti delle persone LGBTI e per questo sono stati arrestati e multati.

Di fronte a questa patente violazione dei più elementari diritti umani, l’Associazione Radicale Certi Diritti ha promosso una mozione al Consiglio Comunale di Milano, firmata da tutti i capigruppo della maggioranza, per chiedere la revoca o la sospensione del patto di gemellaggio tra Milano e l’ex capitale zarista. La stessa mozione è già stata approvata all'unanimità dal Consiglio di Zona 2 e ha trovato l'appoggio di Elio De Capitani e Ferdinando Bruni che prima delle repliche del loro “Angels in America” hanno letto un testo per chiedere a Milano di dare un segno tangibile a favore dei diritti umani e delle libertà fondamentali revocando il patto di gemellaggio con San Pietroburgo.

A distanza di sei mesi, il Consiglio Comunale non ha ancora messo in discussione la mozione per motivi a noi ignoti. Abbiamo allora deciso di lanciare una campagna per chiedere al Consiglio Comunale di discutere e approvare questa mozione: varie personalità milanesi del mondo della cultura e dello spettacolo hanno registrato un video/appello in favore della sospensione del gemellaggio. Nella pagina Facebook “Milano sospendi il gemellaggio” - che dal 2 ottobre, giorno di lancio della campagna e del 45° anniversario del gemellaggio, a oggi è già stata apprezzata da più di 1216 utenti Facebook  (una media di più di 100 al giorno) - si possono vedere, tra gli altri, i video di Gad Lerner, Moni Ovadia, Alessandro Cecchi Paone, Lella Costa, la compagnia dell’Elfo. Anche la Consulta per la Laicità delle Istituzioni di Milano ha votato all’unanimità l’adesione alla campagna.

Di fronte al silenzio del Comune e all’urgenza della questione data dai sempre più frequenti episodi di violenza in Russia ai danni della comunità LGBTI e dall’approvazione, in prima lettura, di una legge analoga a quella di San Pietroburgo anche in Ucraina, prontamente condannata dall'Alto Commissariato dell'Onu per i Diritti Umani, l’Associazione Radicale Certi Diritti indice un presidio davanti a Palazzo Marino giovedì 18 ottobre dalle ore 18:30 per chiedere di mettere finalmente all’ordine del giorno del Consiglio Comunale la mozione.

Con questa nostra, vorremmo invitarla a dialogare con noi al presidio e a ricevere il senatore Marco Perduca, segretario della Commissione Diritti Umani del Senato della Repubblica, che parteciperà al presidio.

Con la più viva cordialità,  
Yuri Guaiana  
Segretario Associazione Radicale Certi Diritti