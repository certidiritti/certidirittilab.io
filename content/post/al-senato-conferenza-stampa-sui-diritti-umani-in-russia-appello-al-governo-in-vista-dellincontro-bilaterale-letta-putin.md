---
title: 'Al Senato conferenza stampa sui diritti umani in Russia. Appello al Governo in vista dell''incontro bilaterale Letta-Putin'
date: Mon, 18 Nov 2013 14:27:34 +0000
draft: false
tags: [Russia]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti

Roma 18 novembre 2013

Mercoledì 20 novembre alle ore 13.15 si terrà presso la Sala Caduti di Nassiriya del Senato, una conferenza stampa sulla situazione dei diritti umani delle persone gay, lesbiche e transessuali in Russia, a pochi giorni dall'atteso incontro bilaterale fra Letta e Putin, per lanciare un appello al Governo italiano.

Durante la conferenza stampa verrá inoltre presentata una contro iniziativa in occasione del bilaterale Italia-Russia del 26 novembre a Trieste: un convegno nazionale sulla situazione dei diritti umani in Russia che si terrá il 22 novembre sempre nel capoluogo giuliano con ospiti internazionali.

All'inizio di agosto, Associazione Radicale Certi Diritti, AGEDO, Arcigay, Famiglie Arcobaleno, Equality Italia, Arcilesbica e Rete Genitori Rainbow hanno lanciato la campagna SOS Russia a sostegno delle associazioni che combattono per i diritti umani delle persone LGBTI, ma non solo, in Russia. La situazione è infatti particolarmente drammatica: il combinato disposto della legge sugli agenti stranieri e di quella sulla "propaganda di rapporti sessuali non tradizionali" tra i minori hanno prodotto da un lato la crescita esponenziale delle aggressioni omofobe, dall'altro si è fortemente ristretta la libertà d'espressione di tutti i cittadini. Obiettivo della corsa di solidarietà italiana è quindi quello di sostenere la copertura delle spese legali per le vittime di violenza o discriminazioni basate su orientamento sessuale e identità di genere e di aiutare le associazioni russe multate a pagare le spese legali.

Alla conferenza stampa parteciperanno Yulia Matsiy, film-maker e attivista per i diritti umani russa, Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, Flavio Romani, presidente di Arcigay, Aurelio Mancuso, presidente di Equality Italia, e Natascia Esposito, del direttivo di Famiglie Arcobaleno. Porteranno inoltre un saluto i senatori Luigi Manconi, presidente della Commissione Diritti Umani del Senato, e Sergio Lo Giudice, membro della Commissione Diritti Umani.

N.B. Si ricorda che per accedere ai locali del Senato è necessario essere provvisti di giacca e cravatta, oltre che di un documento di identità.

Si accede dall'ingresso principale di Palazzo Madama che si trova a Piazza Madama 11