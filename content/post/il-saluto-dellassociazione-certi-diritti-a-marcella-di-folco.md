---
title: 'IL SALUTO DELL''ASSOCIAZIONE CERTI DIRITTI A MARCELLA DI FOLCO'
date: Thu, 09 Sep 2010 06:13:24 +0000
draft: false
tags: [Comunicati stampa]
---

COMMEMORAZIONE DI MARCELLA DI FOLCO A BOLOGNA, GIOVEDI' 9 SETTEMBRE. PRESENTE DELEGAZIONE DI CERTI DIRITTI.

Giovedi' 9 settembre 2010, una delegazione dell'Associazione Radicale Certi Diritti, composta da Sergio Rovasio, Jean Baptiste Poquelin, Giacomo Cellottini e Maurizio Cecconi, partecipera' a Bologna alla cerimonia laica per commemorare l'amica e compagna Marcella Di Folco.

Perché tutte le persone che l’hanno conosciuta e le hanno voluto bene possano portarle un ultimo saluto, giovedì 9 settembre dalle ore  9 alle ore 19 sarà allestita la camera ardente presso la Sala Renzo Imbeni ( ex Sala Bianca) del Comune di Bologna, in Palazzo d’Accursio ( piazza Maggiore 6).  
In particolare, dalle 17 alle 19 ci troveremo lì per commemorare insieme Marcella in un saluto laico, in cui ognun* potrà portare un suo contributo di ricordo e di saluto.  
   
Venerdì 10 settembre, alle ore 17, come voluto da Marcella si terrà la cerimonia religiosa, celebrata da don Giovanni Nicolini presso la parrocchia della Dozza, in via della Dozza 5/2 a Bologna.