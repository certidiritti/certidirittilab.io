---
title: 'Newsletter 14 Giugno'
date: Wed, 31 Aug 2011 10:08:43 +0000
draft: false
tags: [Politica]
---

**![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)**

La campagna di Affermazione Civile continua in Europa: venerdì scorso sono stati depositati alla Corte Europea dei Diritti dell'Uomo i ricorsi contro l’Italia di due coppie omosessuali.

Sabato a Roma si è svolta la parata dell’Europride che ha visto sfilare più di 500mila persone per i diritti lgbt(e). Certi Diritti insieme al Partito Radicale era presente con un carro dedicato alla lotta per l’uguaglianza dei diritti e contro la sessuofobia e oltre ad aver distribuito migliaia di profilattici ha ricordato l’ex tesoriere Enzo Francone, Marcella Di Folco, il compagno David Kato Kisule e tutti i gay, le lesbiche e le trans torturati e ammazzati nel mondo.

Basta con la sessuofobia, i pregiudizi e le discriminazioni!

Contribuisci a Certi Diritti e rinnova la tua iscrizione se non lo hai ancora fatto.  
**[Basta un click >](partecipa/iscriviti-e-contribuisci.html)**

Vuoi dedicare qualche ora del tuo tempo alla lotta per i diritti civili e umani? Ami la fotografia? Ti piace scrivere? Hai un’idea da proporci? Collabora con noi. Scrivi a**[tesoriere@certidiritti.it](mailto:tesoriere@certidiritti.it)**

Buona lettura

SPECIALE EUROPRIDE
==================

**Anche i Radicali all'Europride Roma 2011.  
[Leggi >](tutte-le-notizie/1160.html)**

**Sergio Rovasio: Lady Gaga leader dei diritti civili e umani.  
[Leggi >](tutte-le-notizie/1162-lady-gaga-non-solo-icona-gay-ma-anche-dei-diritti-civili-e-umani.html)**

**Enzo Cucco: siamo nell’era del Global Pride e delle social celebreties.   
[Leggi >](http://gayindependent.blogspot.com/2011/06/benvenuti-nel-global-pride.html)**

**Europride 2011: la notizia sui principali siti italiani di informazione.   
[Leggi >](http://www.queerblog.it/post/11357/europride-2011-la-notizia-sui-principali-siti-italiani-di-informazione)**

**Europride. Alessandra Mussolini: “Sacrosanto manifestare, ma no adozioni per coppie omosessuali”  
[Leggi >](http://gaynews24.com/?p=21784)**

**Europride. Il discorso tradotto in italiano di Lady GaGa.  
[Leggi >](http://gaynews24.com/?p=21774)**

**Europride. Una Lady GaGa grande, ironica, misurata ed elegante infiamma il Circo Massimo. I video.  
[Leggi e guarda >](http://www.notiziegay.com/2011/europride-una-lady-gaga-grande-ironica-misurata-ed-elegante-infiamma-il-circo-massimo-i-video/)**

**Europride 2011 - Raffaella Carrà: "L'Europride è importante e la libertà è un diritto!"  
[Leggi >](http://www.queerblog.it/post/11288/europride-2011-raffaella-carra-leuropride-e-importante-e-la-liberta-e-un-diritto)**

**Europride, Militia Christi contro la manifestazione.   
[Leggi >](http://roma.repubblica.it/cronaca/2011/06/01/news/europride_striscione_di_militia_contro_la_manifestazione-17067443/)**

**Dagli Usa, il saluto dei Democratici all’Europride 2011 di Roma.  
[Leggi >](http://gaynews24.com/?p=21417)**

**Barack Obama al popolo dei gay pride. Giugno, mese dell’orgoglio: “Sfida globale”  
[Leggi >](http://gaynews24.com/?p=21403)**

IN MEMORIA DI DAVID KATO KISULE **[contribuisci al FONDO>](campagne/in-memoria-di-david-kato/1071-fondo-in-memoria-di-david-kato-kisule.html)**
===============================================================================================================================================

**Lesbica ugandese marchiata a fuoco e seviziata: l'Inghilterra le nega l'asilo.  
[Leggi>](http://www.queerblog.it/post/11290/lesbica-marchiata-a-fuoco-e-seviziata-linghilterra-le-nega-lasilo)**

**URGENT CALL TO ACTION _EveryOne Group - Certi Diritti - Arcigay - CGIL Nuovi Diritti - No Peace Without Justice _UK asked to stop removal of Ugandan lesbian.   
[Aderisci all’inziativa (in inglese)>](http://www.everyonegroup.com/EveryOne/MainPage/Entries/2011/6/9_UK_asked_to_stop_removal_of_Ugandan_lesbian.html)**

‎**Certi Diritti: Blogger siriana scomparsa: venerdì 10 giugno sit-in a Roma.   
[Leggi >](tutte-le-notizie/1158.html)**

**Lettera dei parlamentari europei ai colleghi ugandesi.****   
[Leggi >](tutte-le-notizie/1144.html)**

**Testimonianza di Elio Polizzotto di Non c’è pace senza Giustizia su David Kato.   
[Leggi >](contributi/1157.html)**

ASSOCIAZIONE
============

**Matrimonio: due coppie gay ricorrono alla Cedu contro l'Italia.   
[Leggi >](tutte-le-notizie/1159-matrimonio-due-coppie-gay-ricorrono-alla-cedu-contro-litalia.html)**

**Incontro-seminario “Costruire (e difendere) l’Europa dei Diritti. I Diritti non sono un optional ma una parte fondamentale della strategia di sviluppo europea. E l’Italia?”.   
[Guarda e ascolta su Radio Radicale >](http://www.radioradicale.it/scheda/329373/incontro-seminario-costruire-e-difendere-l-europa-dei-diritti-i-diritti-non-sono-un-optional-ma-una-parte-)**

**Conferenza stampa "Diritti civili e umani in Italia e nel mondo. La lotta dei Radicali contro la sessuofobia e le discriminazioni, per il superamento delle diseguaglianze"  
[Guarda e ascolta su Radio Radicale >](http://www.radioradicale.it/scheda/329327/conferenza-stampa-diritti-civili-e-umani-in-italia-e-nel-mondo-la-lotta-dei-radicali-contro-la-sessuofobia)**

**"Diritti umani violati in Siria"  
[Guarda e ascolta con Ra](http://www.radioradicale.it/scheda/329483/diritti-umani-violati-in-siria)****[dio Radicale >](http://www.radioradicale.it/scheda/329483/diritti-umani-violati-in-siria)**

**5 giugno 1981-2011: Aids, dopo 30 anni le figuracce dell'Italia.   
[Leggi >](tutte-le-notizie/1155.html)**

**Pedofilia, Zanardi: “Poco più di dieci giorni fa a Matrix prevedevo il futuro”   
[Leggi >](http://www.ivg.it/2011/06/pedofilia-zanardi-poco-piu-di-dieci-giorni-fa-a-matrix-prevedevo-il-futuro/)**

**Rinnovata la patente al ragazzo di Brindisi cui era stata negata perché gay.  
[Leggi >](tutte-le-notizie/1163-rinnovata-la-patente-al-ragazzo-a-cui-era-stata-negata-perche-gay.html)**

RASSEGNA STAMPA
===============

**Peter Boom. **Al seguente link un ritratto completo sulla sua figura di artista, attore, scrittore**.   
[Leggi >](http://digilander.libero.it/pboom/)**

**''Peter Boom come io l'ho conosciuto..'' di Paolo D'Arpini****.  
[Leggi >](http://www.occhioviterbese.it/occhioviterbese/altre-rubriche/lettere-dai-lettori/8656-peter-boom-come-io-lho-conosciuto-di-paolo-darpini.html)**

**Camera/ In aula dal 13 Dl sviluppo e pregiudiziali omofobia.  
[Leggi >](http://www.tmnews.it/web/sezioni/politica/PN_20110531_00232.shtml)**

**Il prete: “L’omosessualità è una malattia”. E interrompono la messa a Milano.  
[Leggi >](http://www.giornalettismo.com/archives/128331/il-prete-lomosessualita-e-una-malattia-e-interrompono-la-messa-a-milano/)**

**Chiesa, in che mondo convivi?   
[Leggi >](http://www.ilfattoquotidiano.it/2011/06/07/dalle-alte-cattedre-i-papi-parlano-di-coppie-e/116311/)**

**Bocconi, professori e studenti testimonial contro l’omofobia.   
[Leggi >](http://milano.repubblica.it/cronaca/2011/06/07/foto/bocconi_professori_e_studenti_testimonial_contro_l_omofobia-17310646/1/)**

**Reportage de l'Espresso: ****Siamo gay, cioè normali.  
[Leggi >](http://espresso.repubblica.it/dettaglio/siamo-gay-cioe-normali/2152903)**

**Expo turismo gay al Nofrills di Bergamo.   
[Leggi >](http://www.informazione.it/c/A6CFB22B-2E0C-4911-A36C-BA0BAF265C2C/EXPO-TURISMO-GAY-AL-NOFRILLS-DI-BERGAMO)**

**Convegno diritti Glbt. L’Italia è un Paese da serie C secondo il giudizio dei parlamentari europei.  
[Leggi >](http://www.repubblica.it/politica/2011/06/10/news/diritti_glbt-17519306/)**

**Gay pride. Attacchi omofobi in Croazia. In tremila a Varsavia.   
[Leggi >](http://gaynews24.com/?p=21765)**

**Amina, la blogger lesbica siriana, non esiste.   
[Leggi >](http://www.asca.it/news-SIRIA__LA_BLOGGER_LESBICA_PRO-RIVOLTA_NON_ESISTE__E__UN_40ENNE_AMERICANO-1025517-BRK-.html)**

**I medici cattolici vogliono curare i gay. **Si ritorna a parlare di terapie riparative** .  
[Leggi >](http://www.giornalettismo.com/archives/127761/i-medici-cattolici-vogliono-curare-i-gay/)**

**Dalla Germania l'ultima follia sui gay Cure omeopatiche anti-omosessualità****.   
[Leggi >](http://www.corriere.it/esteri/11_giugno_04/germania-cure-contro-omosessualita_e6f902dc-8eb1-11e0-a8a9-c25beeea819c.shtml)**

**A Monaco sorgerà un monumento per ricordare le vittime gay dell'Olocausto****.  
[Leggi >](http://www.queerblog.it/post/11313/a-monaco-sorgera-un-monumento-per-ricordare-le-vittime-gay-dellolocausto)**

**Francia. Matrimonio tra due lesbiche, per Stato una ancora uomo****.  
[Leggi >](http://www.tmnews.it/web/sezioni/esteri/PN_20110604_00131.shtml)**

**Croazia. Il Papa:” La convivenza non può essere sostituita al matrimonio, fate figli e non abbiate paura”.****   
[Leggi >](http://www.gexplorer.net/notizie/2011/06/il-papa-contro-la-convivenza-non-puo-essere-sostituita-al-matrimonio-fate-figli-e-non-abbiate-paura/)**

**Prostitute a Praga: meglio lesbiche che con gli uomini.   
[Leggi >](http://www.giornalettismo.com/archives/128121/prostitute-a-praga-meglio-lesbiche-che-con-gli-uomini/)**

**Omofobia in Russia: i _gay_ danno solo fastidio!   
[Leggi >](http://www.gaywave.it/articolo/omofobia-in-russia-i-gay-danno-solo-fastidio/31579/)**

**Iran. Il Presidente dei diritti umani critica i matrimoni omosessuali.  
[Leggi >](http://gaynews24.com/?p=21426)**

**Due ex presidenti africani condannano l'omofobia che vige nel continente.   
[Leggi >](http://parloditeplus.blogspot.com/2011/06/due-ex-presidenti-africani-condannano.html)**

**Censimento. In Italia sotterfugi per conteggiare le coppie gay, in Nepal invece includono anche i transessuali.   
[Leggi >](http://www.notiziegay.com/2011/censimento-in-italia-sotterfugi-per-conteggiare-le-coppie-gay-in-nepal-invece-includono-anche-i-transessuali/)**

**Usa, fondamentalismo religioso. “Le lesbiche vivono nel peccato, bisogna convertirle”   
[Leggi >](http://www.giornalettismo.com/archives/127426/le-lesbiche-vivono-nel-peccato-bisogna-convertirle/)**

**L’arcivescovo di New York respinge l’accusa di omofobia per chi si oppone ai matrimoni gay.  
[Leggi >](http://gaynews24.com/?p=21409)**

**Brasile. “I video anti omofobia? Fanno diventare gay gli studenti”.  
[Leggi e guarda uno dei video >](http://www.giornalettismo.com/archives/127128/i-video-anti-omofobia-fanno-diventare-gay-gli-studenti/)**

**Illinois: entra in vigore la legge per le unioni civili.  
[Leggi >](http://www.queerblog.it/post/11304/illinois-entra-in-vigore-la-legge-per-le-unioni-civili)**

**Australia. La lobby cristiana :” Non si può far pubblicità al sesso tra gay”  
[Leggi >](http://www.giornalettismo.com/archives/128235/la-lobby-cristiana-non-si-puo-far-pubblicita-al-sesso-tra-gay/)**

STUDI, RICERCHE E MATERIALI INFORMATIVI
=======================================

**Usciamo allo scoperto! **Sintesi del primo studio completo sulla discriminazione delle persone LGBT in Italia**.   
[Leggi >](http://www.zeroviolenzadonne.it/index.php?option=com_content&view=article&id=14042:usciamo-allo-scoperto&catid=187&Itemid=0)**

**Il macigno che distrusse l'illusione dell'edonismo. **5 giugno ’81 quel giorno il centro per la prevenzione delle malattie degli Usa identificò un’epidemia di pneumocistosi polmonare in 5 pazienti gay di Los Angeles. Era l’inizio dell’epidemia dell’AIDS**.  
[Leggi >](http://www.lastampa.it/_web/cmstp/tmplRubriche/editoriali/gEditoriali.asp?ID_blog=25&ID_articolo=8817&ID_sezione=&sezione=)**

**Aids. 30 anni fa l’epidemia, in Italia 40 mila malati ‘sommersi’  
[Leggi >](http://notizie.virgilio.it/notizie/cronaca/2011/06_giugno/04/aids_30_anni_fa_l_epidemia_in_italia_40_mila_malati_sommersi,29897248.html)**

**Intrecci tra eterosessualità obbligatoria e patriarcato.   
[Leggi >](http://www.zeroviolenzadonne.it/index.php?option=com_content&view=article&id=14041:intrecci-tra-eterosessualita-obbligatoria-e-patriarcato&catid=34&Itemid=54)**

**Comunali 2011, quanto è contato davvero il "voto gay"? **Una analisi delle preferenze espresse per i candidati LGBTQ* nelle comunali in Italia**.  
[Leggi >](http://www.ilgrandecolibri.com/2011/06/comunali-2011-quanto-e-contato-davvero.html)**

**I gay? Vengono aggrediti ma non denunciano le violenze.   
[Leggi >](http://www.queerblog.it/post/11341/i-gay-vengono-aggrediti-ma-non-denunciano-le-violenze)**

**Vita di coppia e sessualità Un opuscolo per persone con HIV e i loro partner.   
[Leggi >](http://www.aids.ch/shop/produkte/infomaterial/pdf/1033_03_bez_sex.pdf)**

**Intervista al gesuita John McNeill: “L’amore gay, un’opera di Dio”   
[Leggi >](http://espresso.repubblica.it/dettaglio/lamore-gay-unopera-di-dio/2153271)**

**Polonia. Un sondaggio rivela che il 54% dei polacchi è a favore diritti per coppie stesso sesso.  
[Leggi >](http://gaynews24.com/?p=21383)**

LIBRI E CULTURA
===============

**25 Festival Mix Milano. 80 Egunean, il film vincitore sulle storia d'amore tra due donne.  
[Leggi e guarda il video >](http://it.paperblog.com/25-festival-mix-milano-80-egunean-il-film-vincitore-sulle-storia-d-amore-tra-due-donne-video-414575/)**

**Roma: compie diciotto anni Garofano Verde, rassegna di teatro omosessuale.   
[Leggi >](http://www.queerblog.it/post/11308/compie-diciotto-anni-garofano-verde-rassegna-di-teatro-omosessuale-di-roma)**

**C’è anche un travestito tra le 100 donne più sexy del mondo?** Ogni anno i lettori della rivista per uomini FHM votano per scegliere quale sia la donna più sensuale del pianeta ..**.   
[Leggi >](http://www.giornalettismo.com/archives/127901/un-travestito-tra-le-100-donne-piu-sexy-del-mondo/)**

**Vittorio Sgarbi: "Mio figlio sta bene, non è drogato né omosessuale"  
[Leggi >](http://www.queerblog.it/post/11327/vittorio-sgarbi-mio-figlio-sta-bene-non-e-drogato-ne-omosessuale)**

**"Amazing Grace" by Nhojj supports "Gay Marriage".   
[Guarda il video >](http://www.youtube.com/watch?v=aQg3XqoyV3Q)**

**Renbooks, la casa editrice che produce fumetti per omosessuali.   
[Leggi >](http://www.notiziegay.com/2011/renbooks-la-casa-editrice-che-produce-fumetti-per-omosessuali/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+blogspot%2Fnotiziegay+%28Notizie+gay%29&utm_content=Google+Reader)**

**Flavio Mazzini, _[Vivere LGBT a Roma](http://www.libreriauniversitaria.it/vivere-lgbt-roma-tutto-quello/libro/9788876155369)_ T_utto quello che c’è da sapere e i luoghi che bisogna conoscere per cogliere al meglio le opportunità offerte da una città "diversa"_, Castelvecchi, € 12,50**

_**[Diverso sarà lei - Storie di coppie gay e non](http://www.mannieditori.it/libro/diverso-sar%C3%A0-lei), **_**Manni editore, € 13,00**

COMUNICAZIONE
=============

Stanno spegnendo Internet in Siria per preparare una strage e nasconderla. Diffondete sulle vostre bacheche e su altri social network questi dati, che permettono ai siriani di collegarsi a internet via modem bypassando il blocco: Dial up access for #Syria: [+46850009990](tel:%2B46850009990)[+492317299993](tel:%2B492317299993) [+4953160941030](tel:%2B4953160941030) user:telecomix password:telecomix #syria #killswitch by @telecomix  
PASSAPAROLA. Aiutaci a far conoscere la nostra associazione.   
**[Invia ad un amico il link della pagina che racconta cos’è e cosa fa CertiDiritti](lassociazione/chi-siamo.html)**

[www.certidiritti.it](http://www.certidiritti.it/)
==================================================