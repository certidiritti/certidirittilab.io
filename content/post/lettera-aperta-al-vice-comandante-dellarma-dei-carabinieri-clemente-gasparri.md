---
title: 'Faccio parte della Guardia di Finanza e sono gay: lettera aperta al Vice Comandante dell''Arma dei Carabinieri Clemente Gasparri'
date: Tue, 03 Jul 2012 08:58:11 +0000
draft: false
tags: [Politica]
---

[Dopo le dichiarazioni di Vice Comandante dell’Arma dei Carabinieri Clemente Gasparri](http://www.certidiritti.org/2012/06/27/vice-comandante-dellarma-dei-carabinieri-offende-gay-polizia-gdf-magistrati-morti-per-suicidio-esposto-di-certi-diritti-a-unar-e-oscad/), un Appuntato Scelto della Guardia di Finanzia ha deciso di scrivere questa lettera.

  
Buongiorno, Generale.

Chi le scrive si sente direttamente chiamato in causa dalle sue esternazioni alla Scuola Ufficiali dei Carabinieri  di Roma.

Ma andiamo al dunque,

non so se sono io il “graduato” della Guardia di Finanza a cui si riferisce nel suo discorso che ha “ammesso” (come se si trattasse di una colpa) di essere gay.  Forse si o forse no, chissà. In ogni caso, caro Generale, eccomi qua, Appuntato Scelto della Guardia di Finanza Strati Marcello in servizio nel Corpo da 26 anni, attualmente a Como, al Gruppo di Ponte Chiasso, fiero di appartenere alle Fiamme Gialle. Servo il mio Paese con onestà e senso del dovere. Ah, dimenticavo, sono omosessuale.

Si, come Lei accenna, sono gay su Facebook e su Twetter, sono gay davanti ai miei amici e ai miei colleghi. Ho “ammesso” questa vergogna (perché Lei, Generale, sembra considerarla tale)  già da parecchio tempo. In caserma sanno di me da circa 12 anni e, Le sembrerà strano, ma pare che ai colleghi e soprattutto ai miei Superiori gerarchici non interessi proprio nulla del mio orientamento sessuale. E’ per questo che nell’anno del Signore 2012 mi ha fatto  impressione leggere certe affermazioni da parte del Vice Comandante di una delle più importanti Istituzione della nostra Repubblica, l’Arma dei Carabinieri.

Cosa vuol dire, come dice in un passaggio del suo discorso,  che  “ammettere di essere gay non è pertinente allo status di Carabiniere”?    Io non vado in giro con un cartello appeso al collo con su scritto “omosessuale” ne quando mi presento dico “piacere, sono l’App.Sc  Strati e sono gay”. Io cerco di essere quello che sono davanti a tutti senza dovermi più nascondere e comportandomi con naturalezza, cercando di dimostrare ai colleghi che non c’è nulla di male nell’essere gay, che la vita sessuale di ciascun militare non condiziona in alcun modo l’attività operativa.

Le sue affermazioni ci riportano indietro di decenni. Il suo “consiglio” (e noi militari sappiamo benissimo cosa significa questo termine quando proviene da un Superiore) a non palesare il proprio orientamento sessuale è un macigno che cade in testa a quei militari che magari dopo tanta  fatica e sofferenza interiore avevano deciso di uscire alla luce del sole. Di essere e di vivere finalmente la loro vera natura senza dover più fingere di essere quello che non sono. Sperando di essere giudicati  non per chi si portano a letto o per chi amano ma solo in quanto buoni militari.

Non so se la conosce, Generale, ma in Italia esiste una associazione a cui sono fiero di appartenere, Polis Aperta, che è composta da appartenenti gay e lesbiche di tutte le Forze dell’Ordine e Forze Armate, inclusa la sua, che vivono serenamente e apertamente la propria condizione di gay in un ambiente militare o militarmente organizzato. Ci conosciamo tutti e siamo sparsi per la Penisola. Provi a conoscerci, Generale, provi a parlare con un suo militare gay e vedrà che si troverà di fronte ad un Carabiniere come tutti gli altri, con gli stessi pregi e gli stessi difetti. Non impedisca ad un suo militare di amare. Nessuno dovrebbe vergognarsi di quello che è.  Io non sono fiero di essere gay, così come non sarei fiero di essere etero. Io sono fiero di essere quello che sono. Punto.

Non so se la Sua posizione sia condivisa dal Comandante Generale dell’Arma ma spero vivamente di no.

Appuntato Scelto Marcello Strati  
  
[**Vice comandante dei Carabinieri offende gay, polizia, gdf e morti per suicidio >>>**](http://www.certidiritti.org/2012/06/27/vice-comandante-dellarma-dei-carabinieri-offende-gay-polizia-gdf-magistrati-morti-per-suicidio-esposto-di-certi-diritti-a-unar-e-oscad/)