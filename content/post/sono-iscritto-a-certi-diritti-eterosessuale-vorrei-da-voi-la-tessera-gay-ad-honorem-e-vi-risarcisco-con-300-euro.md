---
title: 'Sono iscritto a Certi Diritti, eterosessuale, vorrei da voi la tessera ''gay ad honorem'' e vi ''risarcisco'' con 300 euro'
date: Mon, 02 Jul 2012 07:46:27 +0000
draft: false
tags: [Movimento LGBTI]
---

"Sono eterosessuale e in passato mi è capitato di offendere pubblicamente le persone gay  scherzando su di loro.  Sono passati diversi anni da quando questo è successo  e se ancora oggi continuo ad interrogarmi non è per un senso di colpa  ma, paradossalmente, per un senso di giustizia. " Lettera da un nostro iscritto.  
 

Cari amici dell’Associazione Radicale Certi Diritti,

sono un vostro iscritto per l’anno 2012.  Ho deciso di inviare, oltre la quota associativa, una donazione straordinaria di 300 euro,  come forma di risarcimento per un mio errore del passato. 

Sono eterosessuale e in passato mi è capitato di offendere pubblicamente le persone gay  scherzando su di loro.  Sono passati diversi anni da quando questo è successo  e se ancora oggi continuo ad interrogarmi non è per un senso di colpa  ma, paradossalmente, per un senso di giustizia. 

Ancora oggi, dopo quell’episodio mi chiedo:  “perchè mai un gay non avrebbe potuto denunciarmi?”. Perché nessun giudice mi avrebbe condannato? Tempo fa mi capitò di fare un viaggio in treno con due magistrati e sottoposi loro questo quesito.  Mi dissero che non avendo io offeso direttamente nessuno  non ero tenuto a dover risarcire nessuno.  E' successo davvero. 

Sono arrivato alla conclusione personale che  se si offende indirettamente bisogna risarcire indirettamente.  Per stabilire il risarcimento mi sono orientato sulla condanna a Galeazzi  per una offesa razzista, ma diretta, verso il portinaio della sua abitazione.  Galeazzi è stato condannato a 500 euro poi ridotti a 300 euro per il riconoscimento delle attenuanti.  Ho applicato le attenuanti anche a me stesso  perchè io non ho colpa se sono nato in una società  che non fa nulla per educare le persone al rispetto della diversità,  visto che tutti, anche io, siamo dei diversi.  Al contrario sono cresciuto in una società  che ti abitua al disprezzo fino a quando disumanizzando il diverso  si perde la percezione della malvagità  e la cattiveria viene considerata come innocua perchè normale. 

L'unico vero risarcimento è però intraprendere un percorso di amicizia  ed è questo il motivo per cui ho voluto far precedere la mia iscrizione a questa nobile nostra associazione.  Leggo con dispiacere, attraverso il vostro sito,  del continuo disprezzo che giunge da ogni direzione:  dal semplice Vigile Urbano al Generale dei Carabinieri.  So bene, purtroppo, che poche persone sono disposte nella vita  ad intraprendere un percorso umano di non-indifferenza  e provare a mettersi nei panni del prossimo, almeno una volta,  per capire "cosa si prova". 

Continuerò il mio percorso di amicizia ed eventualmente, accetterei volentieri da voi un certificato di gay "ad honorem"...  

Cordiali saluti,  
Marcello  (iscritto n. 319 all’Associazione Radicale Certi Diritti)

**[SOSTIENI ANCHE TU L'ASSOCIAZIONE CERTI DIRITTI >](partecipa/iscriviti)**