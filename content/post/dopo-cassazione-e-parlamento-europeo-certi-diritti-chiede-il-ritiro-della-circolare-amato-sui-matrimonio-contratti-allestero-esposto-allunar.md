---
title: 'Dopo Cassazione e Parlamento Europeo Certi Diritti chiede il ritiro della Circolare Amato sui matrimonio contratti all''estero. Esposto all''Unar'
date: Fri, 16 Mar 2012 10:00:21 +0000
draft: false
tags: [Diritto di Famiglia]
---

Dopo la sentenza della Cassazione sulle coppie gay, l'associazione radicale Certi Diritti chiede al governo il ritiro della circolare Amato che vieta la trascrizione dei matrimoni tra persone gay contratti all'estero utilizzando come motivazione l'ordine pubblico, che però non è applicabile al diritto di famiglia.

Roma, 16 marzo 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Dopo la storica sentenza della Corte di Cassazione di ieri che chiarisce che le coppie gay conviventi hanno gli stessi diritti delle coppie eterosessuali, l’Associazione Radicale Certi Diritti ha scritto oggi una lettera al Governo italiano, in particolare al Ministro degli Interni, per chiedere che venga  ritirata al più presto la Circolare Amato n. 55 del 2007  sui  “Matrimoni contratti all’estero tra persone dello stesso sesso” inviata a tutti i Comuni italiani, in quanto considerato un grave atto discriminatorio.

Dopo il voto del Parlamento Europeo del Rapporto  Lechner sulla "giurisdizione, la legge, il riconoscimento e l'applicazione delle decisioni e degli strumenti relativi alla successione e la creazione di un Certificato Europeo di Successione” e del Rapporto della deputata Sophie In't Veld sull'eguaglianza, che chiede alla Commissione ed agli Stati membri di "elaborare proposte per il mutuo riconoscimento delle unioni civili e delle famiglie dello stesso sesso", l’Italia non può più fare finta di niente su questi temi.  
   
Abbiamo presentato anche un esposto all’Unar chiedendo un immediato intervento in quanto la stessa Circolare Amato n. 55 vìola le disposizioni europee in  materia di libera circolazione delle persone in Europa e si fonda su una errata intepretazione del concetto di ordine pubblico interno, non applicabile al Diritto di famiglia, con la conseguenza che tale testo si configuri come un atto che genera discriminazione.

Nell’esposto all’Unar viene segnalato l’articolo 2 del D.l. n. 30 del 6 febbraio 2007, che recepisce la Direttiva europea 2004/38/CE, laddove mette in evidenza che fanno parte dei “familiari”, che possono ricongiungersi, i “coniugi”, ed è fuor di dubbio che il termine si riferisca alla figura del coniuge così come essa è configurata nel paese in cui il matrimonio tra persone dello stesso sesso è celebrato. Tale interpretazine è stata sostenuta anche dalla Corte di Cassazione (Cass. Pen. Sez. I Sent n. 1328 del 19/1/2011), e, più recentemente, dal Tribunale civile di Reggio Emilia con Ordinanza 1401/2011 depositata il 13/2/2012.

Nell’esposto l’Associazione Radicale Certi Diritti chiede quindi di prendere in esame la Circolare citata dal punto di vista del diritto antidiscriminatorio, essendoci  gli estremi per l’applicazione di quanto previsto dall’articolo 43 del Decreto Legislativo n. 286/1998.

**[SOSTIENI LE NOSTRE BATTAGLIE >](iscriviti)**