---
title: 'Assistenza sessuale, cos''è? Una proposta nonviolenta per l''interruzione del proibizionismo sessuofobico sui corpi delle persone con disabilità'
date: Tue, 14 Jan 2014 19:08:06 +0000
draft: false
tags: [Salute sessuale]
---

La registrazione audio della seconda commissione del VII congresso di Certi Diritti

 Modera: Leonardo Monaco

Intervengono: Fabrizio Quattrini (sessuologo), Massimo Clara (avvocato), Max Ulivieri (LoveAbility), Alessandro Frezzato (Ass. Luca Coscioni)