---
title: 'ANNIVERSARIO BRECCIA PORTA PIA: DOMANI PORTIAMO BANDIERA EUROPA'
date: Fri, 19 Sep 2008 12:58:55 +0000
draft: false
tags: [Comunicati stampa]
---

 **L’ASSOCIAZIONE CERTI DIRITTI ADERISCE ALLA MANIFESTAZIONE DEI PARLAMENTARI MARIA ANTONIETTA FARINA COSCIONI E MARIO PEPE. DOMANI ORE 16 A PORTA PIA. DEPOSITEREMO LA BANDIERA DELL’EUROPA SUL LUOGO DELLA BRECCIA.**

Roma, 19 settembre 2008

**Dichiarazione di Sergio Rovasio, Segretario Associazione Certi Diritti:**

 “Domani, nell’anniversario della Breccia di Porta Pia, anche l’Associazione Radicale Certi Diritti parteciperà alla manifestazione promossa dai deputati Maria Antonietta Farina Coscioni e Mario Pepe in Corso d’Italia, davanti alla colonna che ricorda il luogo della Breccia aperta nel 1870 per liberare la città di Roma dal potere temporale clericale della chiesa. All’iniziativa parteciperanno anche le Associazioni Radicali Roma e l’Uaar.

 In tale occasione Sergio Rovasio, Segretario Associazione Certi Diritti, depositerà sul luogo della breccia una bandiera dell’Europa per ricordare il grande significato storico e politico che la caduta del potere temporale sulla città di Roma ha significato per lo sviluppo della civiltà e della laicità nei paesi europei”.