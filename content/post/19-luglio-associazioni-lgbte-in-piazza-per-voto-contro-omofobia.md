---
title: '19 luglio associazioni lgbt(e) in piazza per voto contro omofobia'
date: Wed, 13 Jul 2011 17:00:00 +0000
draft: false
tags: [Movimento LGBTI]
---

**L'Associazione Radicale Certi Diritti insieme alle associazioni lgbt(e) e a parlamentari di diversi gruppi seguirà i lavori dell'aula da Piazza Montecitorio dalle ore 15 con un sit-in e una maratona oratoria per chiedere al Parlamento un segnale forte contro l'omofobia e la transfobia. Lo slogan: 'Legge incostituzionale? Omofobia parlamentare'.**

Roma, 14 luglio 2011

  
E’ previsto per martedì 19 luglio dalle ore 15 il voto sulla proposta di legge contro l'omofobia che introduce l’aggravante per i reati commessi in ragione dell’orientamento sessuale della vittima.

La legge molto probabilmente non passerà a causa delle pregiudiziali di costituzionalità presentate da deputati dell’UDC, del PDL e della Lega Nord. Deputati genuflessi alla casta vaticana e del tutto indifferenti ai diritti dei cittadini, che tentano così di aggrapparsi a inesistenti ipotesi di incostituzionalità per negare la gravità della violenza omofobica.

Per questo saremo in piazza con lo slogan 'Legge incostituzionale? Omofobia parlamentare'.

**[INVITA AL SIT-IN I TUOI AMICI SU FACEBOOK >](http://www.facebook.com/event.php?eid=229229537111401)**

  
Hanno aderito:  
Associazione radicale Certi Diritti  
Arcigay  
Agedo  
Famiglie Arcobaleno  
MIT  
Circolo Mario Mieli  
DiGayProject  
Equality  
Gay Center  
Radicali Italiani  
Forum Queer SEL  
Associazione Luca Coscioni  
CGIL- Nuovi Diritti  
Associazione 3D  
Fondazione Massimo Consoli  
I-Ken Onlus  
Yellow Sport  
Associazione Transgenere  
blog Vogliosposaretizianoferro.it  
Roma Rainbow Choir