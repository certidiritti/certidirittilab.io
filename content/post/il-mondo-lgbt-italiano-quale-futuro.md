---
title: 'IL MONDO LGBT ITALIANO: QUALE FUTURO?'
date: Tue, 22 Apr 2008 11:37:28 +0000
draft: false
tags: [Comunicati stampa]
---

Mercoledì prossimo, Arcigay Giovani di trieste organizza presso le università di Trieste una conferenza dal titolo: “Il mondo LGBT italiano: quale futuro?”  
  
CertiDiritti di trieste parteciperà: Mercoledì 23 Aprile alle ore 16:00, Edificio H2Bis – Aula 4C.

Di seguito il programma dettagliato. 

Conferenza Mercoledì 23/4 ore 16:00  
  
Edificio H2Bis – Aula 4C  
  
Il mondo LGBT italiano: quale futuro?  
  
Prof. Roberto Scarciglia  
  
(Professore ordinario di diritto pubblico comparato – Facoltà di Scienze Politiche)  
  
La legislazione sulle coppie di fatto: un’ analisi comparatistica tra la situazione italiana e spagnola  
  
Prof. Liborio Mattina  
  
(Professore ordinario di Scienza della Politica – Facoltà di Scienze Politiche)  
  
Il dibattito tra i partiti sui diritti delle unioni di fatto.  
  
   
  
Prof. Vaclav Behloransky  
  
(Professore ordinario di Sociologia Politica – Facoltà di Scienze Politiche)  
  
Società italiana, stranieri culturali e Chiesa  
  
   
  
Prof.ssa Daniela Frigo  
  
(Professore ordinario di Storia moderna – Facoltà di Scienze Politiche)  
  
Famiglia, affettività e identità individuale: la ricerca storiografica  
  
Prof.ssa Giuliana Parotto  
  
(Professore associato di filosofia politica – Facoltà di Scienze Politiche)  
  
Prof. Gabriele Pastrello  
  
(Ricercatore di economia politica – Facoltà di Scienze Politiche)