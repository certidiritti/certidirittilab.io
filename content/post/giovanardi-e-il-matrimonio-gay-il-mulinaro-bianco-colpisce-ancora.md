---
title: 'Giovanardi e il matrimonio gay: il mulinaro bianco colpisce ancora'
date: Tue, 09 Aug 2011 15:18:09 +0000
draft: false
tags: [Matrimonio egualitario]
---

E anche oggi il sottosegretario Giovanardi ha detto la sua sul matrimonio tra persone dello stesso sesso. Non avevamo ancora capito qual era la sua posizione, ora il mondo non è più col fiato sospeso. Il mulinaro bianco colpisce ancora per la sua comicità.

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti

“E anche oggi, martedì 9 agosto, il (molto) Sottosegretario Carlo Giovanardi ha detto la sua sul matrimonio tra persone dello stesso sesso. Eravamo davvero preoccupati perché ancora alle 11, 30 di oggi non aveva espresso la sua quotidiana opinione sull’argomento, per fortuna poi l’ha fatto e ora tutto ci è più chiaro e certamente riusciremo a dormire meglio. Poi anche il mondo non starà più con il fiato sospeso e chissà che forse le borse tornino al rialzo. La dichiarazione di oggi di Giovanardi, sommata a quelle di lunedì, martedì, mercoledì, giovedì, venerdì, sabato, domenica, e ancora lunedì, ecc, sullo stesso argomento, è però la più comica di tutte.

Udite, udite: il Dipartimento per le politiche della famiglia della Presidenza del Consiglio ha commissionato un sondaggio per avere un parere sull'opinione degli italiani in ordine al matrimonio fra persone dello stesso sesso con risultati pro matrimonio uomo-donna pari al 65% e una ‘forte minoranza’ che invece ritiene possibile il contrario, ovvero il 30% (forte minoranza?).

Chissà come sono stati garantiti indipendenza, pluralità e adeguata informazione rispetto alle domande fatte, quali criteri scientifici adottati, con quali conoscenze sul tema da parte degli interlocutori, che forse mai hanno sentito parlare di questo diritto, ormai riconosciuto in molti paesi democratici e civili, dove il Vaticano non c’è. Insomma, l’uscita di oggi del Sottosegretario clerical-familista-mulinaro-bianco, è di quelle più comiche mai sentite quest’anno. Ciò che invece ci fa piangere è la notizia che un Sottosegretario che è anche Senatore, ci costa, oltre al miserabile stipendio di parlamentare, altri 40.456 Euro lordi l’anno, compresa la 13° mensilità, più tutte le prebende spettanti”.