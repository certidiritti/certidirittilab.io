---
title: 'APPELLO AL QURINALE PER MARIA LUISA: MIGLIAIA LE ADESIONI'
date: Mon, 29 Jun 2009 13:24:29 +0000
draft: false
tags: [Comunicati stampa]
---

**APPELLO AL QUIRINALE: MEDAGLIA AL VALOR CIVILE A MARIA LUISA, LA RAGAZZA DI NAPOLI AGGREDITA PERCHE’ HA DIFESO UN RAGAZZO GAY. INIZIATIVA LANCIATA DA CERTI DIRITTI E IL PORTALE [WWW.GAY.IT](http://www.gay.it/)**

**MIGLIAIA LE ADESIONI IN POCHI GIORNI, TRA I FIRMATARI MAURIZIO COSTANZO, LUCIANA LITTIZZETTO E CLAUDIO COCCOLUTO DJ.**

Nei giorni scorsi l’Associazione Radicale Certi Diritti e il portale [www.gay.it](http://www.gay.it/), insieme alle associazioni lgbt di Napoli Arcigay e I-Ken Onlus, hanno promosso un Appello al Presidente della Repubblica affinchè venga conferita la Medaglia al Valor Civile a Maria Luisa Mazzarella, la ragazza che il 23 giugno scorso, a Napoli, in Piazza Bellini, era stata selvaggiamente picchiata da un gruppo di ragazzi per aver difeso un ragazzo gay da loro insultato e minacciato. A causa dell’aggressione la ragazza è stata ricoverata in ospedale con il rischio di perdere la vista ad un occhio.

Diverse migliaia di persone hanno già firmato l’appello al Presidente della Repubblica che è stato sottoscritto dai più importanti esponenti della comunità lgbt italiana. I primi firmatari sono Alessio De Giorgi, Direttore di Gay.it e Sergio Rovasio, Segretario dell’Associazione Radicale Certi Diritti. Tra gli altri hanno già firmato personalità del mondo politico e dello spettacolo: Maurizio Costanzo, Luciana Littizzetto, Marco Cappato, Pierluigi Diaco, Alessandro Cecchi Paone, Rita Bernardini, Elisabetta Zamparutti, Marco Beltrandi, Barbara Pollastrini, Vladimir Luxuria, Franco Grillini, Sergio D’Elia, Daniele Capezzone, Ivan Scalfarotto, Giovanna Melandri e molti altri.

Di seguito il testo dell’Appello che si può firmare al sito www.gay.it  
  
Al Presidente della Repubblica On. Dr. Giorgio Napolitano  
Signor Presidente,  
il 23 giugno 2009, nella centrale piazza Bellini a Napoli, una studentessa di 26 anni, Maria Luisa Mazzarella , nella circostanza di trovarsi a difendere un proprio amico omosessuale dalle offese e dalle violenze fisiche per opera di un gruppo di coetanei, è stata lei stessa oggetto di un duro atto di violenza verbale e fisica che le ha procurato lesioni su tutto il corpo e l'ha esposta al rischio di perdere addirittura un occhio.  
In un contesto sociale in cui si moltiplicano gli atti di violenza dettati dall'odio nei confronti di cittadini con un differente orientamento sessuale e che spesso si consumano nell'indifferenza generale di coloro che vi assistono, il gesto di Maria Luisa assume un innegabile valore non solo simbolico. Ci permettiamo pertanto di chiederLe di valutare la possibilità di concedere a Maria Luisa la medaglia al valor civile per aver messo a rischio la propria stessa vita in difesa di un coetaneo vittima della violenza omofoba.  
Confidiamo nella Sua sensibilità in modo che Maria Luisa possa vedersi conferita la massima onorificenza della Repubblica.