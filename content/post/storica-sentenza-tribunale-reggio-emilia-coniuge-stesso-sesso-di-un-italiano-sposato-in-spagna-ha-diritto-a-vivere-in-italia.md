---
title: 'Storica sentenza Tribunale Reggio Emilia: coniuge stesso sesso di un italiano sposato in Spagna ha diritto a vivere in Italia'
date: Tue, 14 Feb 2012 16:41:02 +0000
draft: false
tags: [Diritto di Famiglia]
---

Grande vittoria della coppia, degli avvocati e di Certi Diritti. Ora il Governo  dovrà riconoscere queste unioni tra i cittadini italiani, come richiesto dalla Corte Costituzionale con la sentenza 138/2010.

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Roma, 14 febbraio 2012

L’associazione Radicale Certi diritti ha sostenuto il ricorso presentato al Tribunale di Reggio Emilia di un ragazzo uruguayano sposato in Spagna con un cittadino italiano  a cui  era stato  negato il permesso di soggiorno dalla Questura di Reggio Emilia.

Nel ricorso, presentato dall’Avv. Giulia Perin  e concordata con il Direttivo di Certi Diritti, non si è chiesto  il riconoscimento del matrimonio spagnolo ma il diritto per i coniugi, sebbene non riconosciuti, ad avere una vita famigliare in Italia. Le nostre tesi sono state accolte dal Giudice Tanasi.

Nel ricorso si fa riferimento alla  recentissima sentenza n. 1328/2011 della Corte di Cassazione che  afferma: a) la nozione di “coniuge” prevista dall’art. 2 d.lgs. n. 30/2007 deve essere determinata alla luce dell’ordinamento straniero in cui il vincolo matrimoniale è stato contratto e che b) lo straniero che abbia contratto in Spagna un matrimonio con un cittadino dell’Unione dello stesso sesso deve essere qualificato quale “familiare”, ai fini del diritto al soggiorno in Italia

Nella sentenza inoltre, viene richiamata  la sentenza della Corte costituzionale n. 138 del 2010 che afferma: all’unione omosessuale, “intesa come stabile convivenza tra due persone dello stesso sesso”, spetta “il diritto fondamentale di vivere liberamente una condizione di coppia” e che il «diritto all’unità della famiglia che si esprime nella garanzia della convivenza del nucleo familiare (…) costituisce espressione di un diritto fondamentale della persona umana”

Altri riferimenti normativi:

Vengono richiamate le normative europee (d.lgs. n. 30/2007 sulla libera circolazione dei cittadini europei e i loro famigliari)  e i pronunciamenti della CEDU sull’argomento, inoltre, nella sentenza, si sostiene che rigettare l’istanza finisca per configurare una discriminazione fondata sull’orientamento sessuale (la cui illegittimità è stata di recente ribadita, con una risoluzione di portata storica, dal Consiglio per i diritti umani delle Nazioni Unite il 17 giugno 2011).

**Questa sentenza rappresenta un grande passo avanti per l’affermazione della non discriminazione delle coppie dello stesso sesso, ora il Governo  dovrà riconoscere queste unioni tra i cittadini italiani, come richiesto dalla Corte Costituzionale con la sentenza 138/2010.**

**[Sentenza 13 febbraio 2012!](download/doc_download/59-sentenza-13-febbraio-2012)**

 **"Ora vorremmo che le nostre nozze fossero riconosciute anche in Italia"**  
**Dopo la storica sentenza parla la coppia gay unita in matrimonio in Spagna:**  
[http://www.ilrestodelcarlino.it/reggio_emilia/cronaca/2012/02/16/668702-reggio-emilia-sposi-gay.shtml](http://www.ilrestodelcarlino.it/reggio_emilia/cronaca/2012/02/16/668702-reggio-emilia-sposi-gay.shtml)

**la coppia intervistata dal tg2:**  
[http://www.rai.tv/dl/RaiTV/programmi/media/ContentItem-ebc29754-66b9-48cb-9088-7c90edf99388-tg2.html#p=2](http://www.rai.tv/dl/RaiTV/programmi/media/ContentItem-ebc29754-66b9-48cb-9088-7c90edf99388-tg2.html#p=2)

**da Il Manifesto: Coppi gay, brillante sentenza** [http://bit.ly/wAeOBa](http://bit.ly/wAeOBa)  
  

**Sotto l'interrogazione dei deputati Pdl Agostino Foti e Ghiglia ai ministri Severino e Cancellieri**

Atto Camera

Interrogazione a risposta in Commissione 5-06243  
presentata da TOMMASO FOTI  
giovedì 23 febbraio 2012, seduta n.591

TOMMASO FOTI e GHIGLIA. -  
Al Ministro della giustizia, al Ministro dell'interno.  
 \- Per sapere - premesso che:

risulta da notizie di stampa (Il Giornale del 16 febbraio 2012) che a Reggio Emilia il giudice civile Emilia Domenica Sabrina Tanasi ha accolto il ricorso presentato da un cittadino uruguayano, cui era stato negato il permesso di soggiorno, ritenendo che vi fossero i requisiti per il ricongiungimento familiare avendo lo stesso sposato in Spagna un italiano di sesso maschile -:

non appare chiaro quale norma o interpretazione giurisprudenziale, anche di rango sovranazionale, consenta il ricongiungimento familiare fra persone coniugate dello stesso sesso;

se e quali iniziative di competenza il Governo intenda assumere nel caso in cui non sia rinvenibile un chiaro fondamento giuridico che consenta di giungere alle conclusioni cui è pervenuto il magistrato in premessa citato. (5-06243)

**  
iscriviti alla newsletter >[  
](newsletter/newsletter)[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**