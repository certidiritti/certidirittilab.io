---
title: 'GIOVANARDI CHIEDE TEST ANTIDORGA PER TUTTI TRANNE CHE PER I MINISTRI. PERCHE?  E PERCHE'' NON ANCHE I TEST PSICHIATRICI?'
date: Tue, 12 Oct 2010 11:27:46 +0000
draft: false
tags: [Comunicati stampa]
---

**GIOVANARDI CHIEDE TEST ANTIDROGA PER TUTTI. PERCHE’ TEST PER TUTTI MENO CHE PER I MINISTRI? E PERCHE’ NON I TEST PSICHIATRICI E PSICO-ATTITUDINALI PER I MEMBRI DI GOVERNO?**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

**“**Il (molto) Sottosegretario Giovanardi, che da un po’ di tempo chiede test antidroga per tutti: il venerdì per i presentatori tv, il lunedì per Belen che dovrà presentare il Festival di Sanremo, oggi che è  martedì  per i deputati e senatori, ci fa venire un sospetto. Perché chiede test obbligatori per tutti e non per i Ministri e i Sottosegretari di Governo? E poi, perché solo test antidroga e non, ad esempio, antialcool? E perché non i Test Psichiatrici e Psicoattitudinali? Magari iniziando proprio dai membri del Governo, in primis dai  Sottosegretari di Governo? Almeno così non avremmo più sospetti”.