---
title: 'Newsletter 01 Luglio'
date: Wed, 31 Aug 2011 10:15:24 +0000
draft: false
tags: [Politica]
---

**![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)**

La conferenza dei capigruppo della Camera dei Deputati ha deciso: **sull’omofobia si voterà tra l’11 e il 14 luglio**. In contemporanea col voto sit-it e maratona oratoria a Montecitorio che Certi Diritti ha organizzato insieme alle altre associazioni lgbt(e).

Approvare un  registro delle unioni civili non ha nessun senso se a questo non si accompagnano politiche che mirino ad eliminare la diseguaglianze: questa è la richiesta diCerti Diritti a Milano e ci auguriamo venga accolta dalla nuova amministrazione.

Buttiglione è ritornato, dopo essersi espresso contro l’approvazione della legge contro l’omofobia,  ci ha  regalato alcune sue opinioni confuse sull’immoralità dell’essere gay.

Dichiarazioni simili 2004 gli erano già costate il posto di commissario europeo…   
Frattini invece continua a sostenere l’illegittimità dei matrimoni tra persone dello stesso sesso ma noi non ci arrenderemo:  continua la ricerca di coppie decise ad aderire alla campagna di Affermazione civile per il Diritto al Matrimonio come principio di uguaglianza e il riconoscimento dei singoli diritti in Italia.

Buona lettura

ASSOCIAZIONE
============

E’ morto Giancarlo Scheggi, storico radicale fiorentino. **[Leggi >](tutte-le-notizie/1186-e-morto-giancarlo-scheggi-storico-militante-radicale-di-firenze.html)**

Ciao Giancarlo. Ci piace ricordarlo così: **[Guarda 1>](http://www.youtube.com/watch?v=E2NEGt7NKYs)  [Guarda 2 >](http://www.youtube.com/watch?v=1gagRHGDmIk)**

Camera: voto su omofobia tra 11 e 14 luglio. **  
[Confermati sit-in e maratona oratoria >](tutte-le-notizie/1187-camera-voto-su-omofobia-tra-11-e-14-luglio-confermato-sit-in.html)**

**Campagna  dell’integralismo ideologico contro la prostituzione in Europa promossa dell’ European women: inutile e dannosa. occorre maggiore rispetto per la dignità delle  persone che si prostituiscono. Lettere di protesta AL PE.  
[Leggi >](tutte-le-notizie/1185-offensiva-europea-contro-prostitute-occorre-maggior-rispetto.html)**

**Intervento di Sergio Rovasio sulla legge che consente i matrimoni fra omosessuali promulgata nello Stato di New York.  
[Ascolta su Radio radicale>](http://www.radioradicale.it/scheda/330867)**

**Certi Diritti scrive a Majorino: [anagrafe dei diritti come a Torino >](tutte-le-notizie/1182-certi-diritti-scrive-a-majorino-anagrafe-diritti-come-a-torino.html)**

**Nozze gay, Radicali: Gerarchie cattoliche sconfitte da democrazia.   
[Leggi >](http://www.lapoliticaitaliana.it/Articolo/?d=20110625&id=38333)**

**Consiglio d'Europa pubblica rapporto su rispetto diritti lgbt.   
[Leggi >](tutte-le-notizie/1175.html)**

**Governo: impegno su diritti lgbt in Europa e Uganda. E in Italia?   
[Leggi >](tutte-le-notizie/1176.html)**

**Carcere di Montacuto (Ancona): la situazione peggiora sempre di più.  
[Leggi >](http://www.agenziaradicale.com/index.php?option=com_content&task=view&id=12490&Itemid=53)**

**Bologna: Radicali, grave decisione su 'divorzio di stato'  
[Leggi >](http://bologna.repubblica.it/dettaglio-news/18:47/3988481)**

**I radicali: grave imporre il divorzio.   
[Leggi >](http://www.ilmessaggero.it/articolo_app.php?id=39324&sez=HOME_INITALIA&npl=&desc_sez=)**

RASSEGNA STAMPA
===============

**Diritti gay, il sabato dei pride. L'orgoglio sfila da Milano a Berlino.  
[Leggi >](http://www.repubblica.it/cronaca/2011/06/25/news/gay_pride-25giugno-18226200/?ref=HREC1-11)**

**Milano. ****Matrimoni gay, il sindaco sconfessa l’assessore.   
[Leggi >](http://www.ilgiornale.it/milano/matrimoni_gay_sindaco_sconfessa_lassessore/28-06-2011/articolo-id=531930-page=0-comments=1)**

**Milano. ****Il modello-Padova che piace tanto ai Democratici? Tre soli iscritti.   
[Leggi >](http://www.ilgiornale.it/milano/il_modello-padova_che_piace_tanto_democratici_tre_soli_iscritti/28-06-2011/articolo-id=531931-page=0-comments=1)**

**Ciro e Guido hanno detto sì: nozze gay in chiesa valdese.   
[Leggi >](http://www.ilquotidianoitaliano.it/gallerie/2011/06/news/ciro-e-guido-hanno-detto-si-nozze-gay-in-chiesa-valdese-96810.html/)**

**De Magistris balla YMCA con Luxuria: "Napoli come New York"   
[Leggi >](http://www.gay.it/channel/attualita/32026/De-Magistris-balla-YMCA-con-Luxuria-Napoli-come-New-York.html)**

**Politecnico di Torino: ****gli studenti trans possono scegliere****.  
[Leggi >](http://www.ustation.it/articoli/704-torino-gli-studenti-trans-possono-scegliere)**

**Flash Mob a favore dei gay a Treviso? Tutto annullato, il Comune lo vieta.   
[Leggi](http://www.queerblog.it/post/11469/flash-mob-a-favore-dei-gay-a-treviso-tutto-annullato-il-comune-lo-vieta)****[ >](http://www.queerblog.it/post/11469/flash-mob-a-favore-dei-gay-a-treviso-tutto-annullato-il-comune-lo-vieta)**

**Omosessualità, per Umberto Veronesi è l’amore più puro ed evoluto.   
[Leggi >](http://gaynews24.com/?p=21862)**

**Amore gay. Giovanardi contro Veronesi: “merita il premio ‘delirio per l’estate’”[Leggi >](http://gaynews24.com/?p=21870)**

**Gianni Vattimo. “Questo è il terreno su cui la Chiesa esercita il suo potere”  
[Leggi >](http://giannivattimo.blogspot.com/2011/06/questo-e-il-terreno-su-cui-la-chiesa.html)  **

**Buttiglione: “Omosessualità è disordine morale”.   
[Leggi >](http://www.uaar.it/news/2011/06/28/buttiglione-omosessualita-e-disordine-morale/)**

**Frattini: “Costituzione dice no ai matrimoni gay”   
[Leggi >](http://www.uaar.it/news/2011/06/28/frattini-costituzione-dice-no-ai-matrimoni-gay/)**

**Matrimoni gay: anche Dio era progressista.  
[Leggi >](http://www.ilfattoquotidiano.it/2011/06/27/matrimoni-gay-anche-dio-era-progressista/126437/)**

**Parigi, folla al Gay Pride chiede “stessi diritti”.  
[Leggi >](http://gaynews24.com/2011/06/26/parigi-folla-al-gay-pride-chiede-stessi-diritti/)**

**Svizzera. Sei omosessuale? Niente adozioni.  
[Leggi >](http://gaynews24.com/2011/06/25/svizzera-sei-omosessuale-niente-adozioni/)**

**Serbia. Il presidente Tadic appoggia il gay pride in vista del corteo a Belgrado.  
[Leggi >](http://gaynews24.com/?p=21847)**

**I gay perseguitati in Russia dalla cella a Facebook.  
[Leggi >](http://www.repubblica.it/esteri/2011/06/27/news/i_gay_perseguitati_in_russia_dalla_cella_a_facebook-18295272/)**

**Russia. ****Assistenti di volo gay dell'Aeroflot si organizzano in sindacato****.  
[Leggi >](http://www.tmnews.it/web/sezioni/top10/20110621_160209.shtml)**

**Bebè in provetta, l'Europa piccona la legge "Un diritto anche per le coppie non sterili".   
[Leggi >](http://www.radicali.it/rassegna-stampa/beb-provetta-leuropa-piccona-legge-un-diritto-anche-coppie-non-sterili)**

**Allenatrice della nazionale nigeriana di calcio:  “Le lesbiche? Sono una cosa sporca, Dio deve punirle”  
[Leggi >](http://www.giornalettismo.com/archives/130684/le-lesbiche-sono-una-cosa-sporca-dio-deve-punirle/)**

**Il Ghana verso la depenalizzazione dell’omosessualità.  
[Leggi >](http://www.reset-italia.net/2011/06/26/il-ghana-verso-la-depenalizzazione-dell%E2%80%99omosessualita/)**

**I vescovi Usa sulle nozze gay a New York: «Preoccupati e delusi».  
[Leggi >](http://www.ilsole24ore.com/art/notizie/2011-06-25/vescovi-nozze-york-preoccupati-185603.shtml?uuid=AaHvQ2iD)**

**Il matrimonio gay? Inevitabile, ma Obama ha ancora paura.  
[Leggi >](http://www.giornalettismo.com/archives/131085/il-matrimonio-gay-inevitabile-ma-obama-ha-ancora-paura/)**

**USA - coppia lesbica si sposa con una cerimonia indù nepalesi.  
[Leggi >](http://www.actup.org/forum/it/content/usa-coppia-lesbica-si-sposa-con-una-cerimonia-ind%C3%B9-nepalesi-4962/)**

**A Cuba i gay sfidano il governo.  
[Leggi >](http://www3.lastampa.it/esteri/sezioni/articolo/lstp/409029/)**

**Brasile, tre milioni al gay pride più grande del mondo.  
[Leggi e guarda il video >](http://tg.la7.it/cultura_e_societa/video-i433892)**

** Nozze gay in Brasile.   
[Leggi >](http://www.ticinonews.ch/articolo.aspx?id=231229&rubrica=22)**

STUDI, RICERCHE E MATERIALI INFORMATIVI
=======================================

**Deliberazione comune di Torino su “Riconoscimento delle unioni civili. Approvazione regolamento” di (2010)  
[Leggi>](http://www.luciocolavero.com/wp-content/uploads/2010/08/delibera_unioni_civili.pdf)**

**Dove e perché l’omofobia è di casa. **In molti stati del Vecchio Continente non esistono leggi contro l’omofobia.**  
[Leggi>](http://www.west-info.eu/it/dove-e-perche-lomofobia-e-di-casa-consiglio-deuropa-discriminazione-in-base-al-sesso/)**

**USA. Una ricerca dimostra la potenza della lobby omosessuale.  
[Leggi>](http://www.uccronline.it/2011/06/24/una-ricerca-dimostra-la-potenza-della-lobby-omosessuale/)**

**Quarantadue anni fa ci furono i moti di Stonewall.   
[Leggi>](http://www.queerblog.it/post/11473/quarantadue-anni-fa-ci-furono-i-moti-di-stonewall)**

**Moti di Stonewall.  
[Leggi>](http://it.wikipedia.org/wiki/Moti_di_Stonewall)**

LIBRI E CULTURA
===============

**Le monete pornografiche dell'antica Roma, per pagare il sesso sia etero che gay.  
[Leggi>](http://www.queerblog.it/post/11441/le-monete-pornografiche-dellantica-roma-per-pagare-il-sesso-sia-etero-che-gay)**

**Paolo Poli: «A me però le sfilate d’orgoglio mettono tristezza» **L'attore e regista italiano ripercorre episodi e aneddoti del mondo artistico e intellettuale, quando l'omosessualità era ancora un tabù.   
**[Leggi>](http://www.corriere.it/cultura/11_giugno_27/cazzullo-paolo-pini-omosessualita-tabu_bc3f44ae-a0b6-11e0-b2f7-bc745ffd716f.shtml)**

**Il più ricco premio letterario d'America nasce grazie ad autore gay.   
[Leggi>](http://www.queerblog.it/post/11435/il-piu-ricco-premio-letterario-damerica-nasce-grazie-ad-autore-gay)**

**Patrick Branwell, il fratello gay delle sorelle Brontë.  
[Leggi>](http://www.queerblog.it/post/11476/patrick-branwell-il-fratello-gay-delle-sorelle-bronte)**

**Se anche l'Unione Europea realizza spot con uomini hot e nudi...   
[Leggi>](http://www.queerblog.it/post/11432/se-anche-lunione-europea-realizza-spot-con-uomini-hot-e-nudi)**

**Da rileggere:**  
** Da “[Il fenomeno transessuale](http://www.libreriacoletti.it/libro/fenomeno-transessuale.aspx?p=662038)”di Harry Benjamin, Astrolabio 1968,**

**Appendice C:“Il transessualismo:aspetti mitologici, storici ed etnologici”di Richard Green.   
[Leggi>](http://www.crisalide-azionetrans.it/Ilfenomenotransessuale_app_c.html)**

**David Leavitt, _[Eguali amori](http://www.deastore.com/libro/eguali-amori-david-leavitt-mondadori/9788804609148.html)_ , Mondadori,1992, € 9.50 (ristampa)**

** Edmund White, _[E la bella stanza è vuota](http://www.lafeltrinelli.it/products/9788806130329/E_la_bella_stanza_e_vuota/Edmund_White.html)_, Einaudi, 1997,  € 13,43**

[www.certidiritti.it](http://www.certidiritti.it/)
==================================================