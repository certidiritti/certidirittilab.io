---
title: 'SORPRESA PER SONDAGGIO ON LINE CORSERA: 58,6% VOTA IN FAVORE NOZZE GAY'
date: Sat, 08 Nov 2008 11:22:49 +0000
draft: false
tags: [Comunicati stampa]
---

**I LETTORI DEL CORRIERE DELLA SERA PIU’ AVANTI DEGLI ELETTORI DELLA CALIFORNIA: SORPRESA PER IL RISULTATO DEL SONDAGGIO ON LINE, IL 58,6% E’ CONTRARIO AL RISULTATO DEL REFERENDUM SUL MATRIMONIO GAY.**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti**

“Vi sono notizie che meriterebbero l’attenzione dei media ma che in alcuni casi passano sotto silenzio, è il caso di un sondaggio on line del Corriere della Sera pubblicato dal 5 novembre e che chiede: “Referendum in più Stati, bocciate le nozze gay. Siete d’accordo?”. **A oggi, su quasi 7.000 votanti, il 58,6% ha risposto No mentre il 41,3% ha risposto di Si. **

**Certo, se non fossimo in Italia la notizia non ci sarebbe ma la novità sta proprio nel fatto che senza una adeguata informazione, senza mobilitazioni di piazza, come accade invece a San Francisco per la lotta in favore del matrimonio gay, la maggioranza dei lettori on line del Corriere della Sera non si dice d’accordo con l’esito referendario contro le nozze gay che, tradotto, significa che è favorevole al matrimonio tra persone dello stesso sesso.**

**Siamo felici di questo esito, dimostra che la nostra battaglia di Affermazione Civile, che vede coinvolte decine di coppie gay in iniziative legali per il riconoscimento del matrimonio gay, ha il consenso per lo meno della maggioranza dei lettori del Corriere della Sera”.**

**Il sondaggio con i risultati si trova al seguente link:**

**http://www.corriere.it/appsSondaggi/pages/corriere/d_3657.jsp**