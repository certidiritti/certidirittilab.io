---
title: 'Unioni civili. Certi Diritti: in Commissione Giustizia passa testo Cirinnà. Si lavori per garantire un testo decente.'
date: Thu, 26 Mar 2015 15:40:10 +0000
draft: false
tags: [Diritto di Famiglia]
---

[![francesco-nitto-palma-pdl](http://www.certidiritti.org/wp-content/uploads/2015/03/francesco-nitto-palma-pdl-300x199.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/03/francesco-nitto-palma-pdl.jpg)"Con 14 voti favorevoli, 8 contrari e un astenuto è stato licenziato il testo base Cirinnà sulle unioni civili. Di sicuro non è il matrimonio per tutti né una riforma esaustiva del diritto di famiglia, che proprio nel 2015 compie i suoi 40 anni e quindi necessità di essere aggiornata, ma rappresenta comunque un primo passo avanti".Così Yuri Guaiana, Segretario dell'Associaizone Radicale Certi Diritti, commenta il voto di oggi pomeriggio della in Commissione giustizia del Senato. "A differenza della proposta di Forza Italia, il testo unificato della relatrice Monica Cirinnà riconosce molti diritti riconosciuti dal matrimonio, compresa la pensione di reversibilità, e mantiene il riconoscimento del secondo genitore, pur escludendo le adozioni. Resta in piedi, per ora, anche l'istituto della 'convivenza di fatto' per etero ed omosessuali", aggiunge Guaiana. "Il lavoro è ancora lungo e le forze parlamentari devono garantire un testo che non sia oggetto di compromessi con l'ala fondamentalista e clericale del Parlamento e che anzi superi alcuni aspetti critici. Noi dell'Associaizone Radicale Certi Diritti siamo a completa disposizione per confronti e suggerimenti nel corso della fase di emendamenti".

_Comunicato stampa dell'Associazione Radicale Certi Diritti_