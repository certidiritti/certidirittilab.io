---
title: 'A Roma corso di educazione alle differenze affettive e sessuali'
date: Thu, 03 Mar 2011 16:29:55 +0000
draft: false
tags: [bullismo, Comunicati stampa, corso, GAY, lazio, OMOFOBIA, quaranta, roma, scuola]
---

Il progetto, finanziato dalla provincia di Roma e dell'Unione Europea, è stato ideato da Pasquale Quarante e supportato da Agedo, Arcigay, Arcilesbica, Certi Diritti, Circolo Mario Mieli, Famiglie Arcobaleno, MIT e Rete Lenford. Il bando scade il 14 marzo.

Il Cirps Consortium (consorzio universitario dell’Università Sapienza di Roma) e il Dipartimento di ricerche filosofiche dell’Università di Roma Tor Vergata propongono un corso di formazione professionale intitolato Corso di educazione alle differenze affettive e sessuali.

Il corso intende informare e **sensibilizzare, in particolar modo i giovani, sulle identità di genere e sugli orientamenti sessuali**, anche alla luce delle più recenti acquisizioni scientifiche. Saranno analizzate le rappresentazioni del vissuto delle persone omosessuali e transessuali veicolate dai mezzi di comunicazione di massa.

Durante il corso, ampio spazio verrà dedicato alla riflessione sulle cosiddette best practices (buone prassi). Un’azione positiva in tal senso sarà l’istituzione e l’avviamento di un **osservatorio permanente sulla comunicazione e l’informazione veicolata dai mass media sull’omosessualità** (OMO, Osservatorio Media e Omosessualità). Un’altra azione positiva sarà il Premio giornalistico **Penna Arcobaleno** che verrà istituito dall’osservatorio e che sarà conferito ai professionisti dell’informazione-comunicazione che avranno trattato il tema dell’orientamento sessuale e dell’identità di genere con maggiore competenza e professionalità.

Il progetto (Corso di formazione, osservatorio e premio giornalistico) finanziato dalla Provincia di Roma e dall’Unione Europea è stato ideato dal giornalista **Pasquale Quaranta** con il coordinamento tecnico di Valeria Troya, esperta in progettazione europea (Cirps Consortium) e di René Buonocore, mediatore linguistico-culturale (Cirps Consortium).

«La proposta formativa – spiega Pasquale Quaranta – consta di 60 ore di lezione in aula più 3 mesi di tirocinio e si articola in tre moduli: sociale, salute e benessere, sicurezza. Il corso fornisce strumenti d’intervento per allievi motivati a muoversi nell’ambito dell’informazione e comunicazione non stereotipata ma consapevole delle identità di genere e degli orientamenti sessuali. Gli argomenti trattati vanno dal bullismo omofobico tra i banchi di scuola agli studi gay e lesbici in ambito accademico; dal minority stress al coming out in famiglia, fino alla genitorialità omosessuale; dai pronunciamenti della giurisprudenza alle leggi vigenti; dai femminielli nella cultura napoletana al transessualismo; dalla prostituzione maschile alle politiche di riduzione del danno; dalle infezioni a trasmissione sessuale al mobbing sul luogo di lavoro».

Il Corso di educazione alle differenze affettive e sessuali è supportato, tra gli altri, da **Agedo** (associazione genitori di omosessuali), **Arcigay**, **Arcilesbica**, **MIT** (movimento identità transessuale), **Famiglie Arcobaleno** (associazione genitori omosessuali), **Rete Lenford** (avvocatura per i diritti glbt), **Certi diritti** (associazione radicale), **Circolo Mario Mieli di Roma**.

«La possibilità – conclude Pasquale Quaranta – di avere un confronto tra giovani e ricercatori, studiosi e autori, attivisti e militanti, esponenti della cultura, dell’informazione-comunicazione, permetterà di fornire indicazioni utili ai corsisti che riceveranno un’educazione al rispetto delle differenze affettive e sessuali».

**Il bando del corso resterà aperto fino al 14 marzo 2011**.

La sede, le date e l’orario delle selezioni saranno indicate il 16 marzo 2011. **Le lezioni si terranno a partire dal 28 marzo 2011 presso la sede del Cirps Consortium** (Palazzo Doria Pamphilj), in Piazza della Costituente a Valmontone (RM). La partecipazione al corso è gratuita ed è prevista un’indennità di frequenza per i disoccupati pari a 3 euro per ogni ora di corso effettivamente frequentata, previa presenza ad almeno il 70% del monte ore del corso. Al termine del corso, gli allievi che avranno superato le prove d’esame, conseguiranno un attestato di frequenza valido agli effetti della Legge Regionale n. 23 del 25 febbraio 1993.  
**Per ulteriori informazioni rivolgersi allo 06 959938216**.