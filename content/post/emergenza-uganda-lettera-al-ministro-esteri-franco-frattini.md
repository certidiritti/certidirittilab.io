---
title: 'EMERGENZA UGANDA: LETTERA AL MINISTRO ESTERI FRANCO FRATTINI'
date: Sun, 08 May 2011 11:54:20 +0000
draft: false
tags: [Africa]
---

EMERGENZA UGANDA CONTRO GAY: LE ASSOCIAIZONI ARCIGAY, CERTI DIRITTI, NESSUNO TOCCHI CAINO, LUCA COSCIONI, GRUPPO EVERYONE  E PARLAMENTARI RADICALI SCRIVONO Al MINISTRO FRATTINI. INTERVENGA CONTRO BILL ANTIGAY CHE STA PER ESSERE APPROVATO.

Roma, 8 maggio 2011  
L’ Associazione Radicale Certi Diritti, Arcigay, Nessuno Tocchi Caino, Gruppo EveryOne e Associazione Luca Coscioni, hanno scritto oggi una  lettera urgentissima al Ministro degli Esteri Franco Frattini, e, per conoscenza, al Presidente della Repubblica on. Giorgio Napolitano, per chiedergli di intervenire con la massima urgenza su quanto sta avvenendo in Uganda dove il Parlamento sta per approvare il provvedimento contro le persone omosessuali in violazione della legalità internazionale. 

La lettera è stata anche firmata dai parlamentari Radicali Rita Bernardini, Donatella Poretti, Marco Perduca, Marco Beltrandi, Matteo Mecacci ed Elisabetta Zamparutti.

  
Il parlamento ugandese, che verrà sciolto il prossimo 12 maggio, sta per approvare il Bill Anti gay del 2009 per motivi puramente propagandistici mettendo a rischio la vita di molti esponenti della comunità lgbt ugandase. Nella lettera, dove viene ricostruita la vicenda dell’approvazione in tempi stranamente molto rapidi del provvedimento contro gli omosessuali, si chiede al Ministro di attivarsi, così come  hanno già fatto le rappresentanze diplomatiche di alcuni paesi occidentali, per eventualmente dare ospitalità agli esponenti della comunità gay ugandese che nei prossimi giorni dovranno scappare dal paese qualora il provvedimento venisse approvato.   
   
**Di seguito il testo integrale della lettera inviata oggi al Ministro degli Esteri Franco Frattini e, p.c. al Presidente della Repubblica on. Giorgio Napolitano:**  
   
Roma, 8 maggio 2011

  
         Signor Ministro degli Esteri,

nel corso dei lavori della 3rd Regional Changing Faces, Changing Spaces Conference appena conclusasi a Nairobi, Kenya, che raggruppa esponenti di Ong, Associzioni e movimenti che si battono per la difesa dei diritti civili e umani nei paesi del Sud-Est africano, la vasta delegazione degli attivisti del movimento lgbt ugandese ha lanciato un grave allarme sulla imminente approvazione, da parte del Parlamento, dell’Anti-homosexuality bill depositato dal deputato ugandese David Bahati nel 2009.  
            La fretta sarebbe dovuta a ragioni puramente propagandistiche-elettorali dato che il prossimo 12 maggio il Parlamento verrà sciolto e i sostenitori della proposta di legge si sono attivati per far approvare il provvedimento in questi giorni.  
            Secondo informazioni verificate, la commissione parlamentare incaricata di esaminare il provvedimento, ha convocato nei giorni scorsi per alcune audizioni il predicatore Ssempa, il principale leader del fondamentalismo religioso evangelico ed altri esponenti evangelici che hanno fomentato nel paese, in questi mesi, l’odio contro le persone lesbiche e gay.  Il Presidente della stessa commissione parlamentare si è rifiutato di convocare la coalizione e le organizzazioni che sono contro il ddl degli integralisti evangelici.  
             La commissione parlamentare inviera' nelle prossime ore le raccomandazioni al Parlamento senza aver ascoltato il parere dei rappresentanti delle Ong che si battono per la difesa dei diritti civili e umani. Secondo alcune informazioni l’intenzione dei promotori è di inviare il testo che prevederebbe la pena di morte per le persone omosessuali e non più il solo ergastolo come precedente comunicato, in violazione della legalità internazionale. Il Parlamento voterà la prossima settimana il provvedimento.  
   
Le principali Ong per i diritti umani che operano in Uganda si stanno attivando in queste ore per aiutare e proteggere gli attivisti lgbt, già perseguitati dai media e da parte dell’opinione pubblica che verrebbero, qualora il provvedimento venisse approvato, immediatamente arrestati.  
Anche alcune diplomazie occidentali, in testa gli Stati Uniti, hanno minacciato che in caso di approvazione del testo daranno vita ad un embargo economico nei confronti dell’Uganda. Di questo è stato anche informato il Presidente ugandese Museveni nel tentativo di farlo desistere dalla firma del provvedimento che così, anche se approvato, non entrerebbe in vigore.  
            Lo scorso ottobre il Governo italiano, tramite il Sottosegretario agli Esteri Enzo Scotti, in risposta ad una interrogazione dei Senatori Radicali Marco Perduca, Donatella Poretti ed Emma Bonino, si era impegnato a seguire con attenzione gli sviluppi riguardo la persecuzione degli omosessuali in Uganda.  
Le chiediamo Signor Ministro di intervenire attraverso tutti i canali diplomatici affinchè venga scongiurata l’ eventualità dell’approvazione del Bill contro le persone omosessuali, frutto di propaganda e odio fondamentalista. Le chiediamo anche di allertare la nostra sede diplomatica a Kampala qualora esponenti della comunità lgbt ugandese chiedessero protezione nelle prossime ore.  
Grazie per l’attenzione,  
   
Sergio Rovasio, Segretario Associazione Radicale Certi Diritti; Paolo Patanè, Presidente Arcigay; Sergio D’Elia, Presidente di Nessuno Tocchi Caino; Marco Cappato, Segretario Associazione Luca Coscioni, Roberto Malini, per il Gruppo EveryOne. I parlamentari Radicali:  
Donatella Poretti, Marco Perduca, Rita Bernardini, Marco Beltrandi, Matteo Mecacci, Elisabetta Zamparutti