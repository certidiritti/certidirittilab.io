---
title: 'Mozione generale del III Congresso 2010'
date: Sun, 28 Feb 2010 13:24:32 +0000
draft: false
tags: [Politica]
---

**III Congresso dell'Associazione Radicale Certi Diritti**

**Mozione generale**

approvata all'unanimità il 31 gennaio 2010

L'anno 2009 è stato caratterizzato da una forte iniziativa politica per il raggiungimento della piena parità per le persone lgbt nel nostro Paese, segno tangibile di una vitalità delle diverse componenti del movimento e delle singole persone, che solo alcune incomprensibili contrapposizioni ideologiche e personalistiche rendono meno forte ed incisivo.

A dispetto dei molti profeti di sventura, che dal 2008 non fanno che lamentare l'estrema difficoltà del cambiamento, senza indicare strategie alternative al declino annunciato, l'anno passato ha dimostrato che:

1) l'iniziativa [**Affermazione civile**](affermazione-civile.html) e la campagna per il diritto al matrimonio, promosse insieme alla [**Rete Lenford,**](http://www.retelenford.it/) hanno prodotto ben 4 ricorsi coi quali alcuni tribunali italiani interrogano la Corte costituzionale sull'esigibilità del diritto al matrimonio ed al riconoscimento giuridico delle forme di convivenza tra persone dello stesso sesso, stante l'assenza di norme che lo impediscano in modo esplicito.

È inoltre sotto gli occhi di tutti quanto Affermazione civile sia stata feconda di ulteriori iniziative politiche sul tema, a volte nate spontaneamente da singole coppie che con la loro personale iniziativa hanno superato le timidezze del movimento (di una parte di esso) per riaffermare l'urgenza delle riforme richieste.

In particolare, si ricordano l'annuncio del **Sindaco di Torino** di voler celebrare simbolicamente, ma pubblicamente, l'unione di una coppia di donne; la coraggiosa iniziativa di **Francesco e Manuel**, e la campagna "**Delbono sposaci**". A tale straordinaria testimonianza di vitalità e voglia di combattere l'Associazione radicale Ceti Diritti ha cercato di corrispondere con la propria capacità di iniziativa e organizzazione, sia nei casi in cui era tra i promotori delle iniziative che negli altri in cui ne ha valutato l'importanza e, di conseguenza, la necessità di sostegno;

2) la pubblicazione del **Progetto di Riforma del diritto di famiglia** denominato [**Amore civile**](http://www.amorecivile.it/) consente a tutti coloro che hanno a cuore la promozione di riforme autenticamente liberali e modernizzatrici del nostro ordinamento, di avere un punto di riferimento autorevole e importante per la costruzione di alleanze e di iniziative politiche e sociali;

l'aver incalzato le Istituzioni pubbliche, in primo luogo il Ministero per le pari opportunità, ha prodotto il primo serio tentativo parlamentare di affrontare legislativamente il tema dell'**omofobia** e della **transfobia**, dando ragione a chi, come noi, ha sempre sostenuto la necessità di mantenere sempre aperto il canale del confronto e della proposta con i rappresentanti istituzionali, senza rinunciare alla protesta laddove è stata necessaria.

Il Congresso dà atto e ringrazia i deputati e le deputate che hanno condotto le iniziative parlamentari ed assicura loro tutto il sostegno e il supporto affinchè il 2010 sia l'anno della legge italiana contro l'omofobia e la transfobia;

3) dal 1 dicembre 2009 fanno parte integrante dei Trattati costitutivi dell'Unione Europea, diventando vincolanti per tutti i paesi membri, sia la [**Carta europea dei diritti fondamentali**](http://www.europarl.europa.eu/charter/default_it.htm), sia il nuovo [**Trattato della Comunità**](http://europa.eu/lisbon_treaty/full_text/index_it.htm), così come modificato dal Trattato di Lisbona.

L'art. 21 della prima e il 19 del secondo, insieme alle Direttive comunitarie ed alle risoluzioni del Parlamento europeo, sono strumenti importanti per le iniziative di difesa dei diritti delle persone lgbt soprattutto in Italia.

Tanto più importanti se si considerano le reiterate **pressioni vaticane** sulle forze politiche e sulle istituzioni europee e internazionali dove, alleandosi con i rappresentanti dei peggiori fondamentalismi del mondo, tenta di bloccare (spesso riuscendoci) ogni ragionevole passo vanti sulla strada del pieno riconoscimento del diritto all'integrità, alla dignità ed alla vita di relazione delle persone omosessuali e transessuali.

  
Il Congresso, inoltre, non può non prendere atto del non raggiungimento dell'**obiettivo di 1000 iscritti** che il II Congresso aveva indicato per il 2009, anche se le iscrizioni all'Associazione sono aumentate rispetto al 2008. **L'aumento degli iscritti e delle iscritte all'Associazione è requisito essenziale per crescere,** ma soprattutto per dotarsi di una organizzazione all'altezza delle necessità politiche che abbiamo di fronte.

Il Congresso, infine, ringrazia gli organi statutari, in particolare il **segretario nazionale**, per l'impegno e la dedizione profuse nella realizzazione delle iniziative, ed alla luce delle rilevantissime scadenze del prossimo anno si augura di poter nuovamente contare sul loro impegno.

Tutto ciò premesso, il **III Congresso dell'Associazione radicale Certi Diritti**, consapevole che i cambiamenti per i quali stiamo lottando non saranno ottenuti né a breve né facilmente, **impegna gli organi statutari** a:

1) operare affinché il percorso per definire **obiettivi e strumenti comuni a tutto il movimento lgbt italiano**, sia velocizzato e rafforzato. Solo l'**unità laica di tutte le realtà**, garantita attraverso poche ma trasparenti regole di condivisione democratica degli oneri e degli onori che questo percorso comporterà, sono la garanzia che la richiesta di diritti espressa dalla comunità lgbt con forza abbia un interprete all'altezza della difficoltà del compito;

2) procedere, nello spirito indicato al punto 1 ed alla luce del prossimo pronunciamento della Corte Costituzionale, alla creazione del [**Comitato nazionale per il riconoscimento del diritto al matrimonio tra persone dello stesso sesso "Sì, lo voglio"**](http://www.affermazionecivile.it/) per costruire uno strumento partecipato ed efficace utile nella costruzione di una grande campagna nazionale su questo tema che veda coinvolte tutte le associazioni e le singole persone che vogliano passare dalle parole ai fatti. Il Comitato deve diventare al più presto il motore di tutte quelle iniziative che sono necessarie per il raggiungimento dell'obiettivo del riconoscimento del matrimonio, compreso il sostegno e la promozione di "**Affermazione civile**", con l'obiettivo di creare consenso sulla proposta, nella comunità lgbt, nella società e nelle istituzioni italiane;

3) valutare la possibilità, insieme ai rappresentanti dei gruppi di **persone transessuali**, di organizzazione **iniziative di disobbedienza civile e/o di affermazione civile**, per la riforma della legge 164/82 e il raggiungimento degli obiettivi di dignità, garanzia e protezione che il movimento da tempo chiede con forza;

4) avviare una **campagna straordinaria di iscrizione e sostegno all'Associazione,** con l'obiettivo di rafforzare la sua presenza sul territorio e potenziare la sua attività;

Il Congresso supporta gli organi statutari nella loro decisione di sostenere da subito i Comitati organizzatori dell'**Europride 2011** e del **Congresso di ILGA Europe 2011**: si tratta di due appuntamenti importanti e utili anche per la realtà italiana, ai quali l'Associazione può e deve dare il proprio contributo.

Il Congresso si rivolge ai **milioni di italiani e italiane senza volto**, a tutti e tutte coloro che nel silenzio vivono la propria vita omosessuale e transessuale e guardano con indifferenza o disaffezione all'attivismo politico sui temi lgbt. Si rivolge a loro per dire con forza che **cambiare è possibile**, che possiamo non solo sperare in un Paese più giusto e meno violento nei nostri confronti, ma essere certi che, grazie al contributo di ciascuno, così come mirabilmente dimostrato dalle decine di coppie di "Affermazione civile", le cose possono cambiare, in meglio.

Il Congresso fa appello a tutte e tutti i cittadini che hanno a cuore i diritti delle persone, il superamento delle diseguaglianze e la laicità dello stato, a sostenere le lotte delle persone lesbiche, gay, bisessuali, trans gender, transessuali e intersessuali, che sono anche **obiettivi di lotta e di speranza per milioni di persone eterosessuali**.

Il Congresso, infine, auspica che per le prossime **elezioni regionali** i candidati radicali della Lista Bonino-Pannella e tutti i candidati che condividono gli obiettivi dell'Associazione abbiano il sostegno e l'appoggio necessari per essere eletti nei consigli regionali.

Firenze, il 31 gennaio 2010