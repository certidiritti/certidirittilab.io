---
title: 'Caso lesbica ugandese, Certi Diritti: si mobiliti anche  l''Italia'
date: Thu, 16 Jun 2011 14:52:05 +0000
draft: false
tags: [Africa]
---

**L'associazione radicale Certi Diritti si felicita per le pressioni europee ed internazionali sul caso della lesbica ugandese Betty Tibikawa e chiede alle autorità italiane di attivarsi.**

Certi Diritti si felicita per la mobilitazione internazionale sul caso di Betty Tibikawa, la cittadina ugandese alla quale é stato negato l’asilo politico in Gran Bretagna e che rischia ora di essere espulsa e rinviata nel suo paese di origine. Betty ha subito persecuzioni in Uganda, ha ferite sul corpo inflitte anche con un ferro da stiro, e rischia di subirne nuovamente se rinviata nel suo paese. L’Uganda é uno dei paesi dove le persone LGBT sono più a rischio:  poco tempo fa é stato ucciso il militante per i diritti LGBT David Kato, l’omosessualità é punita con 7 anni di prigione, mentre una legge “anti-omosessuali” potrebbe essere riesaminata dal parlamento a breve.

Sollecitato da Certi Diritti e da EveryOne, l’intergruppo del Parlamento europeo per i diritti LGBT ha preparato una lettera ache sarà inviata a giorni al Ministro inglese per l’immigrazione Damien Green, per sollevare il caso di Betty e ricordare gli impegni che il governo inglese aveva preso pubblicamente di “interrompere la deportazione dei richiedenti asilo che hanno dovuto abbandonare certi paesi a causa del loro orientamento sessuale… e che li mette a rischio di imprigionamento, tortura o esecuzione”.  L’intergruppo chiede quindi di riesaminare il caso di Betty e di non espellerla verso l’Uganda. L’europarlamentare liberale Sarah Ludford ha già scritto una lettera ieri su questo caso.

Certi Diritti si felicita per tali iniziative e chiede alle autorità italiane di intervenire presso il governo inglese per chiedere che Betty non venga espulsa verso l’Uganda.