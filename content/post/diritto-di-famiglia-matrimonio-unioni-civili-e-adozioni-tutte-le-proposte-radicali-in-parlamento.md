---
title: 'Diritto di famiglia, matrimonio, unioni civili e adozioni: tutte le proposte radicali in Parlamento'
date: Sun, 18 Mar 2012 20:56:43 +0000
draft: false
tags: [Diritto di Famiglia]
---

I Parlamentari Radicali hanno depositato in questa legislatura 10 proposte e disegni di legge su riforma del diritto di famiglia, matrimonio tra persone dello stesso sesso, unioni civili, Pacs, e legge di riforma sulle adozioni.

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Nel corso dell’attuale XVI Legislatura i parlamentari Radicali, eletti nel Pd, hanno depositato 10 Proposte e Disegni di Legge su Unioni Civili, Matrimonio tra persone dello stesso sesso, Riforma del Diritto di famiglia e legge di riforma sulle adozioni.

Le proposte di Legge sono elencate qui di seguito con il nome del primo firmatario e co-firmate dai Parlamentari Radicali. Alla Camera Rita Bernardini, Marco Beltrandi, Maria Antonietta Farina Coscioni, Matteo Mecacci, Maurizio Turco, Elisabetta Zamparutti. Al Senato  Emma Bonino, Donatella Poretti e Marco Perduca.

**Proposte di Legge presentate alla Camera dei deputati:**

1)      Modifiche al codice civile e all'articolo 3 della legge 1o dicembre 1970, n. 898, in materia di diritto a contrarre matrimonio e di eguaglianza giuridica tra i coniugi (**[1064](http://www.camera.it/126?pdl=1064&ns=2)**) prima firmataria on. Rita Bernardini (presentata il 15 maggio 2008);

2)      Modifiche al codice civile e altre disposizioni in materia di unione civile (**[1065](http://www.camera.it/126?pdl=1065&ns=2)**) prima firmataria on. Rita Bernardini (presentata il 15 maggio 2008);

3)      Modifiche al codice civile in materia di testamento biologico, di disciplina del diritto di famiglia e della fecondazione assistita, al codice penale in materia di omicidio del consenziente e di atti di violenza o di persecuzione psicologica all'interno della famiglia, nonché al codice di procedura civile in materia di disciplina della domanda di divorzio (**[3607](http://www.camera.it/126?pdl=3607&ns=2)**) prima frimataria On. Rita Bernardini (presentata il 6 luglio 2010)

**Disegni di Legge presentati al Senato:**

1)      Modifiche al codice civile in materia di diritto a contrarre matrimonio. **[S. 594](http://www.senato.it/loc/link.asp?tipodoc=sddliter&leg=16&id=31220)** prima firmataria Sen. Donatella Poretti;

2)     Riforma del diritto di famiglia. **[S. 2263](http://www.senato.it/loc/link.asp?tipodoc=sddliter&leg=16&id=35628)**  prima firmataria Sen. Donatella Poretti

3)     Modifiche al codice civile e altre disposizioni in materia di unione civile. **[S. 603](http://www.senato.it/loc/link.asp?tipodoc=sddliter&leg=16&id=31302)** primo firmatario Sen. Marco Perduca;

4)     Disciplina del patto civile di solidarietà. **[S. 2129](http://www.senato.it/loc/link.asp?tipodoc=sddliter&leg=16&id=35285)** primo firmatario Sen. Marco Perduca;

5)     Disciplina dell'unione civile. **[S. 2130](http://www.senato.it/loc/link.asp?tipodoc=sddliter&leg=16&id=35286)** primo firmatario Sen. Marco Perduca;

6)     Modifiche al codice civile in materia di eguaglianza nell'accesso al matrimonio, **[S. 2131](http://www.senato.it/loc/link.asp?tipodoc=sddliter&leg=16&id=35287)**,  primo firmatario Sen. Marco Perduca;

7)     Introduzione del titolo VII-bis al libro primo del codice civile, in materia di assunzione della responsabilità genitoriale **[S. 2132](http://www.senato.it/loc/link.asp?tipodoc=sddliter&leg=16&id=35288)**,  primo firmatario Sen. Marco Perduca.

  
**[COME SOSTENERE LA NOSTRA ASSOCIAZIONE >](iscriviti)**

[**RICEVI LA NEWSLETTER >**](newsletter/newsletter)