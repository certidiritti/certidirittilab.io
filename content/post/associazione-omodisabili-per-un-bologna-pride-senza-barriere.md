---
title: 'ASSOCIAZIONE OMODISABILI PER UN BOLOGNA PRIDE SENZA BARRIERE'
date: Sat, 21 Jun 2008 14:01:31 +0000
draft: false
tags: [Comunicati stampa]
---

Il Bologna Pride punta ad essere Senza Barriere grazie all'impegno di un gruppo di volontar* sensibil* ai temi della disabilità e dell’omosessualità.  
Il loro progetto si occupa sia dell’aspetto informativo che di quello dei servizi ed ha l’obiettivo di favorire l’accessibilità a tutt* coloro che vorranno partecipare al Pride, compresa quella ‘minoranza nella minoranza’, spesso invisibile, rappresentata dalle persone gay, lesbiche, e transessuali con disabilità. Il nostro progetto si impegna a garantire in particolare (materiali e info di dettaglio sono disponibili qui: [www.bolognapride.it/2008/06/04/un-pride-accessibile-a-tutti/](http://www.bolognapride.it/2008/06/04/un-pride-accessibile-a-tutti/) ):  
  
a) accessibilità da parte alle persone non vedenti e ipovedenti dei materiali stampati ed elettronici prodotti per la comunicazione del Pride;  
  
b) informazioni sul grado di accessibilità architettonica dei luoghi dove si terranno i numerosi eventi del Pride;  
  
c) informazioni sulla mobilità accessibile a Bologna (arrivare e muoversi in città, con mezzi propri o pubblici);  
d) informazioni sulle strutture alberghiere idonee ad accogliere persone con disabilità;  
e) disponibilità di un gruppo di volontari in grado di offrire accompagnamento e sostegno durante la parata di sabato 28 giugno;  
f) interpretariato LIS durante il comizio sul palco alla fine della parata del 28 giugno.  
  
Inoltre, Arcigay nazionale organizza un interessante laboratorio teatrale gratuito. “M’ama o non m’ama?” mira ad esplorare le emozioni, gli immaginari, i vissuti le relazioni e rielaborarle attraverso il linguaggio teatrale e il confronto con gli altri progetto teatrale sulle affettività ‘diverse’.  
Appuntamento a Bologna dal 4 al 6 luglio 2008.  
Volantino promozionale (con contatti per le pre-iscrizioni): [fronte](http://www.lelleri.it/lab_fronte.pdf) e [retro](http://www.lelleri.it/lab_retro.pdf).  
  
Per ulteriori informazioni o per rendersi disponibili come volontar* i nostri contatti sono: **omodisabili@libero.it** | 348 5167091.  
  
Per eventuali approfondimenti, sono a disposizione:  
a) [la lista di discussione on-line](http://it.groups.yahoo.com/group/17giugno/) per scambiare informazioni, suggerimenti, esperienze e materiali sul rapporto tra omosessualità e disabilità;  
b) [il report di ricerca sociale “ABILI DI CUORE.](http://www.lelleri.it/report/abilidicuore.pdf) **Omo-disabilità: quale rapporto tra omosessualità e disabilità?”;**