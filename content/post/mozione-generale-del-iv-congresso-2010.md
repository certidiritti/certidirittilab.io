---
title: 'Mozione generale del IV Congresso 2010'
date: Mon, 29 Nov 2010 19:15:09 +0000
draft: false
tags: [Politica]
---

##### Roma, 27 e 28 novembre 2010

Il 2010 è stato un anno importante per la storia della piena uguaglianza delle persone lgbt in Italia: la sentenza della corte costituzionale sul diritto al matrimonio civile per le persone dello stesso sesso rappresenta un deciso passo avanti su questa strada perché riconosce valore costituzionale alle unioni tra persone dello stesso sesso, richiama fermamente il Parlamento a legiferare sulla materia ed apre nuovi ambiti di intervento per il riconoscimento dei singoli diritti.

Questo risultato è il frutto della campagna di Affermazione Civile che l’Associazione radicale certi Diritti ha condotto con straordinario impegno e determinazione insieme a Rete Lenford, ma soprattutto grazie alle decine di coppie di uomini e donne che hanno partecipato, con le loro vite e la loro storia, all’iniziativa.

Questo risultato è anche la prova che la scelta di affrontare la questione dell’uguaglianza delle persone omosessuali in Italia attraverso il metodo nonviolento radicale dell’impegno diretto e del ricorso alla giustizia per il riconoscimento di diritti e legalità, è stata non solo efficace ma necessaria in un Paese come il nostro dove legalità e diritti sono quotidianamente calpestati.

La sentenza della Corte costituzionale e l’impegno di alcune decine di uomini e donne omosessuali sono fonte di speranza e di impegno rinnovato perché indicano una strada feconda di nuove iniziative e di obiettivi da raggiungere. Questo risultato e questa speranza sono anche una indicazione per l’Associazione affinché rifletta sulla propria attività e sul contesto entro il quale l’Associazione opera.

E’ chiaro infatti che ogni passo avanti nel percorso della piena uguaglianza di fronte alla legge senza distinzione di genere e di orientamento sessuale, dei cittadini e delle cittadine di questo paese, si accompagna, anzi si fonda sulla necessità di affrontare la sessuofobia, quindi il tema della libertà sessuale degli individui, nel suo complesso. Anche coinvolgendo la “maggioranza eterosessuale” soggetta, come tutti, ai divieti dei fondamentalismi religiosi e ideologici. Questa considerazione impone all’Associazione di procedere nella sua attività mettendo al centro del suo impegno il tema più complessivo della lotta contro ogni forma di sessuofobia e per la difesa dei diritti civili e umani delle persone senza distinzione di genere e orientamento sessuale.

Il Congresso ringrazia per la loro straordinaria testimonianza David Kato Kasule, Nikolai Alexeiev, Santos Felix e Monnine Griffith, che hanno ricordato che la difesa dei diritti delle persone omosessuali non può non essere un impegno transnazionale, a partire dalla drammatica situazione in cui vivono le persone omosessuali nei paesi ove vige la pena di morte o il reato di omosessualità. Il loro intervento richiede una risposta forte dell’Associazione, nelle singole situazioni indicate ed un impegno di alleanza e coordinamento con le associazioni che in Europa (dentro l’ILGA) lottano per la piena uguaglianza.

Il Congresso, inoltre, è consapevole che gli importanti obiettivi raggiunti e quelli che abbiamo di fronte, necessitano di una organizzazione più forte, in termini di risorse umane e risorse economiche, organizzazione che dobbiamo costruire a partire dall’impegno straordinaria di ciascun iscritto.

**Tutto ciò premesso, il IV Congresso dell’Associazione radicale certi Diritti,**

-        conferma la centralità dell’obiettivo del riconoscimento del diritto al matrimonio civile per le persone dello stesso sesso in Italia, e di tutte le iniziative che, sotto la definizione di “Affermazione civile” individuano e organizzano azioni legali e politiche per il riconoscimento di specifici diritti. Il Congresso da mandato agli organi dell’associazione di approfondire le specifiche iniziative di Affermazione civile e di verificare eventuali alleanze con altre associazioni, non solo lgbt, italiane ed europee;

-        conferma l’impegno sul progetto “Amore civile” e, ritenendo che sia necessario non solo sostenerlo ma rilanciarlo nel dibattito pubblico come nell’iniziativa politica e sociale concreta, impegna gli organi a fare dell’Associazione uno dei soggetti del rilancio della Conferenza per la riforma del diritto di famiglia;

-        ritiene che sia necessario ed urgente che l’Associazione ampli il suo ambito di intervento al tema della lotta di ogni forma di sessuofobia e per la difesa dei diritti e della libertà sessuale delle persone, coinvolgendo la maggioranza eterosessuale anche attraverso l’organizzazione di una iniziativa che raccolga le associazioni e le persone che su questi obiettivi possono essere coinvolti;

-        ritiene che la difesa dei diritti sessuali della persona, nella tradizione radicale della difesa dei diritti civili e umani in tutto il mondo, e sulla scia dell’impegno che nel passato il Fuori! ha profuso su questi temi, debba diventare centrale nell’attività dell’Associazione, a partire da specifiche iniziative nei confronti delle istituzioni internazionali ed europee, ed impegna gli organi in tal senso. Queste iniziative trovano sede naturale e fondamentale per il loro sviluppo nel Partito radicale Nonviolento Transnazionale Transpartito, partito al quale l’Associazione ha chiesto di diventare soggetto costituente e con il quale, anche prima di questo riconoscimento, intende collaborare sulle iniziative per la difesa dei diritti civili e umani delle persone senza distinzione di genere e orientamento sessuale;

-        fa propri i documenti elaborati dalle organizzazioni internazionali ed europee delle persone transgenere e da mandato agli organi dell’Associazione di individuare puntuali obiettivi ed iniziative per il raggiungimento degli stessi, da organizzare in Italia ed in Europa;

-        ritiene che il tema della difesa dei diritti delle lavoratrici e dei lavoratori del sesso debba fare un deciso passo avanti verso la piena legalizzazione, contro ogni forma di ipocrisia e sessuofobia, dando mandato agli organi  dell’Associazione di operare in tal senso, insieme al Comitato per la difesa dei diritti delle prostitute e ogni altra associazione che condivida questo obiettivo;

-        ritiene che l’obiettivo di moltiplicare gli scritti e le risorse a disposizione debba diventare impegno costante di ciascuno degli iscritti, ai quali si rivolge direttamente  e personalmenteperché si lavori, fin da subito a moltiplicare le adesioni ed i contributi  all’Associazione.

-        Il Congresso, infine, ringrazia gli eurodeputati, i deputati italiani, a partire da quelli iscritti a Radicali Italiani,  gli organi di Radicali italiani e del PRTTN ed i tanti avvocati e docenti universitari, per il loro fondamentale sostegno per le iniziative dell’associazione, e chiede loro di continuare la battaglia comune moltiplicando l’impegno e le alleanze possibili.

Approvata all’unanimità

Roma, 28 novembre 2010