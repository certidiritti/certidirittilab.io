---
title: 'Certi Diritti per Giornata Internazionale contro omofobia: incontro GISHR, presentazione rapporto Istat con Presidente della Camera e consegna 7mila firme al comune di Roma'
date: Wed, 16 May 2012 09:16:26 +0000
draft: false
tags: [Politica]
---

Roma, 16 maggio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Domani, 17 maggio, giornata internazionale contro l’omofobia, alle ore 10, l’Associazione Radicale Certi Diritti incontrerà alla Camera dei deputati la Global Initiative for Sexuality and Human Rights (Gishr) dell'Organizzazione Internazionale Heartland Alliance for Human Needs & Human Rights, che opera in diversi Paesi per la promozione e la difesa dei diritti umani.

L’incontro vedra' la partecipazione di alcuni parlamentari ed esponenti del Partito Radicale Nonviolento, del Segretario dell’Associazione Radicale Certi Diritti, Yuri Guaiana, della responsabile Cgil Nuovi Diritti, Maria Gigliola Toniollo, insieme  al coordinatore di Gishr, Stefano Fabeni e rappresentanti di Associazioni Lgbt di diversi Paesi tra cui Nigeria, Burundi, Kenia, Libano, Malesia, Argentina, Trinidad & Tobago, Stati Uniti.

Alle ore 12 presso la Sala del Mappamondo della Camera dei deputati si terrà la presentazione del Rapporto Istat su: ‘La popolazione omosessuale nella società italiana” con la presenza di molte Associazioni ed esponenti Lgbt e il Presidente della Camera Gianfranco Fini.

Alla stessa ora al Comune di Roma, Radicali Roma e Certi Diritti, insieme a decine di associazioni romane che si battono per la difesa e la promozione dei diritti civili, consegneranno agli uffici del Campidoglio oltre 7.000 firme per una proposta di Delibera di Iniziativa Popolare, raccolte negli ultimi tre mesi, che ha l’obiettivo di riconoscere le Unioni Civili anche nella Capitale.