---
title: 'CERTI DIRITTI PARTECIPA A INIZIATIVA NONVIOLENTA DI FRANCESCO E MANUEL'
date: Sat, 02 Jan 2010 14:37:13 +0000
draft: false
tags: [Comunicati stampa]
---

MATRIMONIO GAY: CERTI DIRITTI ADERISCE E PARTECIPA ALLA CONFERENZA STAMPA – MANIFESTAZIONE DI FRANCESCO E MANUEL, IN SCIOPERO DELLA FAME PER IL RICONOSCIMENTO DELLA LORO UNIONE PROMOSSA A ROMA IL 4 GENNAIO IN P.ZA MONTECITORIO.

Roma, 2 gennaio 2010

L’Associazione Radicale Certi Diritti aderisce e partecipa all’iniziativa nonviolenta promossa da Francesco Zanardi e Manuel Incorvaia del 4 gennaio 2010 in Piazza Montecitorio dalle ore 15. Per la prima volta una coppia gay inizia uno sciopero della fame di dialogo affinchè il Parlamento calendarizzi le proposte di legge sulle unioni civili e il matrimonio gay.

**L’Associazione Radicale Certi Diritti aderisce e partecipa all’iniziativa nonviolenta promossa da Francesco Zanardi e Manuel Incorvaia del 4 gennaio 2010 in Piazza Montecitorio dalle ore 15. Per la prima volta una coppia gay inizia uno sciopero della fame di dialogo affinchè il Parlamento calendarizzi le proposte di legge sulle unioni civili e il matrimonio gay.**

Anche esponenti di Certi Diritti inizieranno il 4 gennaio uno sciopero della fame di sostegno e aiuto alla coppia gay di Savona.

Alla conferenza Stampa – manifestazione di Piazza Montecitorio hanno finora aderito e partecipano:

\- Don Franco barbero, esponente delle comunità cristiane di base;

\- Sergio Rovasio, segretario associazione Radicale Certi Diritti;

\- Donatella Poretti, Senatrice radicale - Pd;

\- Anna Paola Concia, deputata Pd;

\- Vladimir Luxuria, già parlamentare;

\- Franco Grillini già parlamentare, Direttore di Gaynet;

\- Alessandro Cecchi Paone, giornalista;

\- I rappresentanti regionali del movimento Gay Italiani;

\- Il movimento spontaneo We Have a Dream;

\- I rappresentanti delle associazioni nazionali che partecipano al progetto e al riconoscimento dei diritti della comunità LGBT italiana.

**Inizieranno lo sciopero della fame: Francesco, Manuel, Rita, Luigia, Gian Mario, Raffaella, Assunta, Antonio, Rita, Sebastiano, Giacomo, Tony, Carla, Luisella, Christian, Elisabetta, Franco, Deborah, Alix, Giovanni, Dany, Marica, Stefano, Davide, Rosanna,Davide, Valentina, Milena, Renzo, Lucio, Andrea, Paola, Luisa. I rappreentanti del movimento spontaneo Gay Italiani, Europa Intervieni e We Have a Dream.**

**Per info: [f.zanardi@gayitaliani.eu](mailto:f.zanardi@gayitaliani.eu)**

**Altre notizie: [www.certidiritti.it](http://www.certidiritti.it/)**

**La conferenza sarà trasmessa in diretta Live Stream 2424 [http://www.glbt-tv.it/?p=396](http://www.glbt-tv.it/?p=396) e su Radio Radicale.**