---
title: 'La Ministra Idem fa un passo avanti, ma siamo sicuri che stia andando verso l''uguaglianza?'
date: Fri, 10 May 2013 11:00:04 +0000
draft: false
tags: [Politica]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti

Roma, 10 maggio 2013

In un'intervista apparsa sui quotidiani stamane il Ministro alle Pari Opportunità molto opportunamente (senza gioco di parole...) dice di essere contraria alle coppie di serie B, ed annuncia a presto una proposta del Governo in materia di riconoscimento delle cosiddette unioni civili. La dichiarazione è senz'altro positiva, e la salutiamo come un ulteriore passo avanti, soprattutto perché sottolinea in modo esplicito che non si tratta di problemi di forma, ma di sostanza, ovvero del complesso di diritti (e di doveri, Signora Ministro non li dimentichi) che il provvedimento riconoscerà alle unioni civili. Peccato che dalle sue stesse parole ci siano ancora dubbi su quanto questa proposta andrà nella direzione da lei auspicata:

1) il riconoscimento delle unioni civili varrà sia per le coppie eterosessuali che per le coppie omosessuali? La questione non è affatto secondaria, stante le ufficiali e pubbliche dichiarazioni pre elettorali sia di Bersani sia di Berlusconi che parlavano unicamente di un provvedimento a favore delle coppie dello stesso sesso;

2) come fa a considerarsi ugualitaria una proposta che non affronta e risolve il tema della protezione dei diritti dei figli delle coppie di persone dello stesso sesso?

3) Ancora, come si fa a considerarsi ugualitaria una proposta che non affronta e risolve il tema dell'accesso di tutti i cittadini all'istituto del matrimonio civile rimuovendo quell'odiosa discriminazione che impedisce di scegliere con chi sposarsi, minando il diritto al matrimonio, come ha riconosciuto anche la Corte Europea dei Diritti dell'Uomo nel caso Goodwin c. Regno Unito, 17 luglio 2002?

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: "Noi siamo per una riforma complessiva del diritto di famiglia che riconosca il diritto di eguaglianza rivendicato da cittadine e cittadini omosessuali, nonché l'esigenza di cittadine e cittadini di qualunque orientamento sessuale di uno o più istituti maggiormente flessibili. Consideriamo quindi la volontà espressa dal Ministro un primo piccolo passo avanti, ma attendiamo di vedere la proposta del Governo per chiarire i dubbi espressi sopra e ricordiamo la nostra proposta di collaborazione, gratuita e qualificata: non si fidi dei soliti noti, signora Ministro, ascolti anche le associazioni su questi temi".