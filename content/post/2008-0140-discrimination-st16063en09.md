---
title: '2008-0140-Discrimination-ST16063.EN09'
date: Wed, 02 Dec 2009 13:37:00 +0000
draft: false
tags: [Senza categoria]
---

  

  

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 2 December 2009

Interinstitutional File:

2008/0140 (CNS)

16063/09

LIMITE

SOC 706

JAI 836

MI 434

  

  

  

  

  

OUTCOME OF PROCEEDINGS

from :

The Working Party on Social Questions

on :

9 & 13 November 2009

No. prev. doc. :

15896/09 SOC 686 JAI 829 MI 425

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

**I.         INTRODUCTION**

At its meetings on 9 and 13 November 2009, the Working Party on Social Questions continued its examination of the above proposal. The discussion focused on a set of Presidency drafting suggestions[\[1\]](#_ftn1). Written contributions from NL[\[2\]](#_ftn2) and AT[\[3\]](#_ftn3) were also circulated prior to the meeting.

**II.        MAIN ITEMS DISCUSSED**

**Article 2 (Concept of discrimination)**

**\- Article 2(1)**

HU and LV wondered whether creating a new category of discrimination ("discrimination in the form of denial of reasonable accommodation") in Article 2(1) was necessary. LV took a similar view.

BE, CZ, DK, DE, IE, ES, FR, LV, NL and SK pointed out that the denial of reasonable accommodation should only apply to discrimination on the grounds of disability. SK and RO preferred the wording contained in Article 2(5) of the previous Presidency text (doc. 14009/09). NL suggested keeping "discrimination in the form of denial of reasonable accommodation" in Article 2(1), but as a separate sentence that mentioned only the disability ground and included a reference to proportionality ("…unless this would impose a disproportionate burden"). Acknowledging that the provision should cover disability only, and provisionally welcoming the solution suggested by NL, Cion also pointed out that proportionality was already mentioned in Article 4.

**\- Article 2(6)**

DK recalled its position whereby age limits in national legislation, adopted by national parliaments, should not be covered by the Directive (see doc. 12893/09). BG and FR supported the DK position.

NL recalled its written suggestion (see doc. 13651/09). CZ supported the NL wording. IE also supported the concerns raised by NL.

IE and UK reiterated their view whereby minors (persons under 18) should be excluded from the Directive. UK supported by LV also suggested exploring the possibility of a more nuanced approach, with separate provision on legitimate differences of treatment on the grounds of age in different sectors. Cion explained that the current wording already established a strong presumption of legitimacy in respect of the Member States' legislation.

  

He pointed out that the Member States' right to maintain legitimate age limits in areas of national competence was also assured in the Article 3(2).

**\- Article 2(7)**

AT maintained a reservation and recalled its written suggestion (doc. 15896/09).

Giving their first reactions, BE, BG, EE and FI supported the written suggestion by AT. FR, IT and UK expressed doubts. Cion expressed its reservations by pointing out that discrimination grounds such as sexual orientation and religion or belief were, in practice, not relevant under Article 2(7). Cion endorsed the view put forward by AT that using age or disability as risk-assessment criteria should only be allowed where these factors represented genuine risk factors.

IE and DE made the point that it was premature to examine the question of insurance in the context of Article 2(7), pending an expected ruling by the ECJ and of a Commission study on insurance and discrimination, and in the light of abiding uncertainties with respect to the exact meaning of the insurance provisions in Directive 2004/114/EC. Cion undertook to keep delegations informed of its study, while distinguishing the study from the present discussions; but acknowledged that the Court's forthcoming ruling in Case C-236/09 Test-Achats  could be relevant to the discussions on the proposed Directive.

BE reiterated its call for greater transparency on the part of insurance companies, and called for a reference to "objective justification" to be added in Article 2(7). BE also called for banking, mortgages and insurance to be mentioned separately, as they were in the UNCRPD. Responding, Cion explained that banking and insurance were interlinked; for example, mortgages had to be insured and were granted on different terms depending on the age of the applicant.

**Article 3 (Scope)**

MT recalled its earlier written contribution regarding the provisions in respect of family law in Article 3(2)(a) (doc. 14876/09).

NL tabled a written contribution, including a series of questions (doc. 15430/09).

Acknowledging the request to clarify the scope, for example, with respect to education and health services, Cion explained that the scope of the proposed Directive was _restricted_ and much narrower than the scope of the UNCRPD. He acknowledged the challenge of applying the Community concept of "services", developed in a cross-border context, in an anti-discrimination context.

With respect to the examples given by NL regarding obligations to provide accessibility in the context of "public space" (doc. 15430, p. 4), Cion gave the provisional answer that none of the cases listed fell under the scope of the draft Directive, as public space as such was not covered by the proposal. He explained, moreover, that taxes as such were not covered by the remit of the draft Directive.

**Article 4 (Accessibility for persons with disabilities)**

MT, NL and UK favoured clarifying the scope before discussing the details of the disability provisions, including the implementation calendar (Article 15. See also Article 3 above).  LU also saw a need to clarify the links between Article 3 and Article 15(2).

**\- Cost implications**

Certain delegations (DE, DK, MT, IT, NL, PL) reiterated their concerns in respect of the cost implications of the provisions.

**\- Physical environment**

IE and FI preferred "the built environment". Cion acknowledged that the concept of the "built environment" might be a clearer term, but explained that the concept of  the "physical environment" was used in the UNCRPD. See also note from NL (doc. 15430/09).

UK questioned the choice of the word "include" in Article 4(2), as it suggested an open list. Cion pointed out that the introduction of sign language, for example, was not, necessarily, a matter of removing a barrier.

FI asked for the reference to "ICT and Systems" to be clarified. To avoid any conflict with the exemption Article 4b(2), UK suggested placing this example in the recitals.

BE, ES and PT suggested deleting "unless this would impose a disproportionate burden". BE reiterated its misgivings in respect of any exemption for new buildings (see also Article 15(2)). Cion favoured maintaining the words in the text, in the interest of clarity.

DE, FR, NL and UK saw a need to clarify the scope, particularly in respect of _housing_. UK entered a reservation in respect of all the provisions concerning housing. UK explained that, in the United Kingdom, rented housing was not covered by the general accessibility provisions, as the needs of successive tenants could not be anticipated; landlords therefore had an obligation to provide accessibility to tenants with disabilities on a case-by-case basis. NL affirmed the importance of the points raised by UK.

Responding to IE, Cion explained that the requirements laid down in the Directive might be met by existing legislation, but if they were not, the new standard set by the Directive would prevail.

**Article 4a (Reasonable accommodation for persons with disabilities)**

Expressing doubt in respect of the wording "where needed in a particular case", UK pointed out that "reasonable accommodation" also had an _anticipatory_ dimension. Cion explained that anticipatory issues were covered by the concept of accessibility.

DK and FI preferred moving the reference to "individual measures" from Recital 20a to Article 4a (see doc. 12893/09). Cion confirmed that meeting the requirement to provide reasonable accommodation by means of the provision of special services could also be permitted under the current wording, depending on the circumstances (e.g., instead of providing a menu in Braille to a blind customer in a restaurant, reading the menu to him or her).

FR entered a reservation on the definition of "reasonable accommodation" in Article 4a(2) and urged the need to clarify it. DE also called for clarification, especially in respect of the link between Articles 4 and 4a. DE also saw a discrepancy between Article 4a(2) and Recital 20a, as the former contained no mention of a "disproportionate burden".

**Article 4b (Common provisions regarding accessibility and reasonable accommodation)**

FR suggested that the purpose or type of building be taken into account when assessing whether a burden was disproportionate in Article 4b(1).

FR suggested "or national rules in the areas of Member States' competence" in Article 4b(4). CZ also questioned the deletion of "national rules" and "public transport". UK asked for clarification in respect of the impact on areas where there were applicable national rules but no EC rules (e.g. black cabs in London).

IE and FI suggested clarifying the rules alluded to in Article 4b(4) in a recital. FI also suggested adding a reference to EC legislation regarding "passenger rights" in Article 4b(4).

**Article 15**

Delegations broadly welcomed the increasingly nuanced approach to the implementation calendar.

**\- "New buildings, facilities and infrastructure" (Article 15(2))**

FR supported the transitional period of 5 years for new buildings, facilities and infrastructure; CZ, ES, IT, AT also saw the new tentative date as a step in the right direction. Cion also acknowledged the need to provide for some form of transitional period for new buildings, facilities and infrastructure.

BG suggested 10 years for new buildings, facilities and infrastructure.

FI preferred the previous Presidency text (doc. 14009/09); should that previous wording be used, FI could support 5 years for new buildings, facilities and infrastructure.

DE supported by LT felt that 5 years was too short and expressed a preference for progressive implementation, with no implementation dates.

PL also preferred leaving the implementation deadlines to the Member States' discretion.

BE entered a reservation on Article 15(2) in respect of new buildings.

**\- "Existing buildings, facilities and infrastructure" (Article 15(2))**

BG, LT, MT and FI expressed the view that _a minimum_ of 20 years were needed to implement the Directive in respect of existing buildings, facilities and infrastructure. CZ expressed the view that 30 years might be better than 20.

AT and PT felt that 20 years was too long.

**\- ES suggestion concerning the implementation calendar**

ES suggested a nuanced implementation calendar, whereby the Directive could apply as follows:

_\- immediately_ to publicly funded buildings and infrastructure as well as existing buildings, facilities and infrastructure undergoing significant renovation;

\- _two years_ after adoption to buildings and infrastructure as well as existing buildings, facilities and infrastructure undergoing significant renovation, funded exclusively by private parties; and

\- _fifteen years_ after adoption to existing buildings.

Delegations undertook to study the ES suggestions, some (CZ, IT, AT, PT) expressing tentative support.

**Reporting requirements (Articles 15 and 16)**

FR and LT supported aligning the reporting requirements with those established by the UNCRPD, LT calling for a template to be produced by the Commission. Cion explained that such guidance could be issued once the requirements to emerge in the context of the UNCRPD had become clear.

\* \* \*

All delegations maintained scrutiny reservations on all the new suggestions and general scrutiny reservations on the proposal. CZ, DK, FR, MT and UK have maintained parliamentary scrutiny reservations, CY and PL maintaining linguistic scrutiny reservations. The Commission has meanwhile affirmed its original proposal at this stage and has maintained a scrutiny reservation on any changes thereto.

DE reiterated its doubts regarding the need for the proposal, as well as in respect of major outstanding issues including the legal basis, the question of subsidiarity, the cost implications of the provisions, and their impact on SMEs.

  

Delegations' **general positions** are summarised in doc. 14008/09[\[4\]](#_ftn4).

Further details of delegations' positions as expressed at the meetings of the Working Party on 9 and 13 November 2009 as well as during previous discussions are set out in the footnotes to Addendum 1 to this document.

**III.      CONCLUSION**

While delegations welcomed the progress achieved, the discussions also revealed a need for extensive further work on the numerous outstanding issues.

A Presidency report[\[5\]](#_ftn5) outlining the progress achieved in the discussions by the Working Party was presented to the EPSCO Council on 30 November 2009.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) Doc. 15320/09. The Chair reminded delegations that the Presidency drafting suggestions on this proposal were distributed as public documents in the interest of transparency.

[\[2\]](#_ftnref2) Doc. 15430/09.

[\[3\]](#_ftnref3) Doc. 15896/09.

[\[4\]](#_ftnref4) See pp. 2-3.

[\[5\]](#_ftnref5) See doc. 15575/09.