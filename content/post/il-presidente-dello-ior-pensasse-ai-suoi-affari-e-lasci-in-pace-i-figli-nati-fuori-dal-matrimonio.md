---
title: 'IL PRESIDENTE DELLO IOR PENSASSE AI SUOI AFFARI E LASCI IN PACE I FIGLI NATI FUORI DAL MATRIMONIO'
date: Tue, 31 Aug 2010 14:30:59 +0000
draft: false
tags: [Comunicati stampa]
---

Leggo sul Corriere della Sera di sabato 28 agosto un intervento fatto da Ettore Gotti Tedeschi, presidente dello Ior (banca vaticana), in occasione del meeting di Rimini organizzato da CL.

Per il signor Gotti Tedeschi, la via per uscire dalla crisi è "riprendere a fare figli". E fin qui possiamo anche condividere, sempre che lo Stato metta finalmente a disposizione delle famiglie, tutte, gli strumenti per potere crescere più serenamente questi figli : asili nido, aiuti economici per lo studio, gratuità totale delle cure, aiuti sostanziali alle mamme singole, e in generale ai nuclei monogenitoriali o svantaggiati, ecc.... Non vorrei dilungarmi su questi aspetti che altri trattano molto meglio e più approfonditamente di me.

Ma l'intervento prosegue con queste parole: " fare figli, ma in una famiglia vera, fatta di papà e mamma, fondata sul matrimonio. Non lo dico da moralista, ma da economista".

Vorrei rispondere all'economista e non al moralista con una serie di domande e osservazioni.

Lei sa che oggi, in Italia, più del 20 per cento delle nascite avviene fuori del matrimonio ?

Lei sa che, nel 2020, in Italia, la metà dei figli nasceranno fuori dal matrimonio (1)?

Il 2020 è domani, non fra tre secoli !

Lei sa che, in Italia, oggi ci sono ALMENO 100 000 bambini e ragazzi che crescono con uno o anche tutti i due genitori omosessuali ? fuori dal matrimonio dunque (anche se qualcuno di questi omosessuali si sposerebbe volentieri se potesse) ?

A questi milioni di bambini di oggi e di domani, Lei che cosa dice ? Che non parteciperanno alla vita economica del nostro paese ? Magari saranno dispensati dal pagare le tasse ? Vivranno di sussidi dello Stato ? come parassiti ? Oppure pensa di portarli al "confine", fuori dallo Stato ?

O parteciperanno invece come ogni cittadino sano e adulto alla ripresa e al mantenimento delle condizioni socio economiche della nazione, insieme a tutti gli altri ? quelli con un papà e una mamma, quelli con due padri o con una mamma sola, quelli nati da genitori sposati in Comune o in Chiesa, separati o divorziati, singoli o in unione stabile o no con chi gli pare, quelli cresciuti da 4 genitori (due mamme e due papà) ??

TUTTI e TUTTE questi figli saranno cittadini italiani e parteciperanno alla costruzione della nostra economia. Che Le piaccia o no.

E meno male. Anche per la nostra economia.

Le sue parole sono discriminatorie e offensive per milioni di cittadini che già oggi pagano le tasse e i contributi, lavorano e producono, mettono su imprese o sono dipendenti pubblici o privati.

Offendono tutti quegli adulti che scelgono di fare figli al di fuori del matrimonio e tutti quegli adulti che non possono scegliere per mancanza di legge. Offendono tutti i figli nati, e quelli che nasceranno, al di fuori del matrimonio o che hanno una configurazione familiare diversa da quella da lei proposta a modello unico.

Signor Gotti Tedeschi, Le voglio rammentare soltanto, parafrasando la grande Gertude Stein, che

Un figlio è un figlio è un figlio. Un cittadino è un cittadino è un cittadino.

Tutto il resto non è economia, è soltanto moralismo.

**Giuseppina La Delfa**

**[presidente@famigliearcobaleno.org](mailto:presidente@famigliearcobaleno.org)**

**associazione genitori omosessuali**

**www.famigliearcobaleno.org**

334 3798912

(1) statistica realizzata da Alessandro Rosina, docente di Demografia  
all'università Cattolica di Milano. Nei prossimi dieci anni un bambino su

due nascerà da genitori non sposati. Oggi il numero di figli naturali, per

distinguerli da quelli legittimi (nati all'interno del matrimonio) supera il