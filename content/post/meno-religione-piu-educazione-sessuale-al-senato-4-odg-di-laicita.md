---
title: 'MENO RELIGIONE, PIU'' EDUCAZIONE SESSUALE: AL SENATO 4 ODG DI LAICITA'''
date: Tue, 21 Oct 2008 14:24:47 +0000
draft: false
tags: [Comunicati stampa]
---

**Università/Istruzione, Poretti: presentati 4 ODG sul Disegno di Legge n. 1108. Più educazione civica e meno religione. educazione sessuale obbligatoria.**

Roma, 21 ottobre 2008

• Intervento della Senatrice Donatella Poretti, parlamentare Radicale-Partito Democratico

Con il senatore Marco Perduca, abbiamo presentato quattro ordini del giorno al disegno di legge n.1108, recante misure urgenti in materia di istruzione e università, la cui discussione avrà inizio questo pomeriggio in Senato.

Con uno si intende impegnare il Governo a predisporre le misure necessarie affinché l'informazione sessuale sia obbligatoria nei programmi delle scuole medie inferiori e superiori, con particolare riferimento ai metodi contraccettivi ed al riconoscimento dei diversi orientamenti sessuali.

Un altro, considerati gli effetti positivi avuti sull'organizzazione familiare e sull'occupazione di molte donne, in linea con quanto indicato ai Paesi Ue dagli accordi di Lisbona, chiede di assicurare alle scuole pubbliche organici e risorse sufficienti a coprire la crescente domanda di orari didattici a tempo pieno e a tempo prolungato.

Un terzo riguarda la necessità di rivedere complessivamente il sistema dei crediti scolastici e formativi in modo da evitare che la frequenza o meno all'insegnamento della religione cattolica non costituisca fattore di discriminazione tra gli studenti ai fini della determinazione del credito scolastico per l'ammissione agli esami di Stato, ma sia messa a loro disposizione un'offerta di attività scolastiche alternative all'interno delle istituzioni stesse.

Da ultimo, considerate le discriminazioni che di fatto avvengono rispetto alla nomina e al trattamento economico degli insegnanti di religione, nei confronti degli altri insegnanti delle altre materie e nei confronti di chi non ha rapporti con le curie diocesane, si chiede di impegnare il Governo a ripensare all'ora di religione, potenziando invece l'insegnamento dell'educazione civica.

Qui gli Ordini del giorno: http://blog.donatellaporetti.it/?p=309