---
title: 'MOSCA: VIOLENZE E ARRESTI CONTRO GAY PRIDE. INTERVENGA BERLUSCONI'
date: Sat, 16 May 2009 09:34:34 +0000
draft: false
tags: [Comunicati stampa]
---

Gay pride. Cappato e Rovasio: Putin e Medvedev disprezzano libertà di associazione. Berlusconi a Mosca non resti zitto sui diritti civili
-----------------------------------------------------------------------------------------------------------------------------------------

Roma, 16 maggio 2009

_**• Dichiarazione di Marco Cappato, Capolista della Lista Bonino – Pannella alle Elezioni europee, e Sergio Rovasio, candidato della Lista Bonino-Pannella per la Circoscrizione Italia centrale**_

L'arresto degli organizzatori e dei partecipanti al Gay Pride di Mosca dimostra il disprezzo della Russia di Putin e Medvedev per il diritto alla libertà di associazione e la legalità internazionale. Nonostante le sentenze della Corte europea dei diritti dell'uomo - che ha affermato il dovere degli Stati membri del Consiglio d’Europa di autorizzare i “Pride” e di proteggere i partecipanti - per l'ennesima volta il sindaco di Mosca ha negato l'autorizzazione per la manifestazione e la polizia ha arrestato brutalmente i difensori dei diritti umani, come era avvenuto nel 2007 quando come Radicali avevamo partecipato alla manifestazione, eravamo stati arrestati e coinvolti in processi farsa.

L'Unione europea e l'Italia devono esprimere ufficialmente il proprio disappunto alle autorità russe, e chiedere il rilascio degli arrestati. Chiediamo al Presidente Berlusconi di cogliere l’occasione della sua presenza a Mosca per spendere una parola, per una volta, sui diritti umani, civili e politici dei cittadini Russi"