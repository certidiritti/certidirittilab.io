---
title: 'PER CHI AMA LO STATO DI DIRITTO: SEGNATE LA DATA DEL 23 MARZO 2010'
date: Tue, 02 Feb 2010 15:00:03 +0000
draft: false
tags: [Comunicati stampa]
---

**L'Associazione Radicale Certi Diritti consiglia, a chi ama l'idea di vivere in uno Stato di Diritto, di segnare la data del 23 marzo 2010 sul proprio calendario.**

COMUNICATO STAMPA

Roma, 2 febbraio 2010

Ad una manciata di ore dall'equinozio di primavera in Italia, assisteremo  alla  sentenza della Corte Costituzionale che si pronuncerà sulla legittimità del rifiuto delle pubblicazioni matrimoniali ricevuto da due coppie di persone dello stesso sesso che avevano ricevuto dal loro Comune il diniego alla richiesta delle pubblicazioni matrimoniali.

 I comuni in questione sono quello di Venezia e quello di Trento , i cui tribunali,di primo grado il primo e della Corte d'appello il secondo, che , chiamati in giudizio dagli avvocati di Rete Lenford - Avvocatura per i diritti Lgbt, hanno deciso di rimettere, con un'ordinanza di remissione, la decisione di costituzionalità del divieto a contrarre matrimonio tra due persone dello stesso sesso.

L'Associazione Radicale Certi Dirtti attende con grande speranza e fiducia  questa decisione che è frutto della campagna di Affermazione Civile, un semplice percorso di richiesta dei propri diritti in sede giudiziaria, concertata dalla stessa Associazione e da Rete Lenford, a partire dai primi mesi del 2008.

Considerando  infatti l'immobilismo del Parlamento italiano a trovare una soluzione legislativa  al vuoto di tutele per le coppie italiane di persone dello stesso sesso, Certi Diritti ha avviato, fin dalla sua fondazione, nel marzo del 2008, una campagna di ricerca di  coppie formate da due donne o due uomini disposte a far valere i propri diritti nel loro Tribunale.

L'associazione Radicale Certi Diritti si prepara a questa data storica per il nostro Paese con ottimismo e convinta che, come stabilito dalla Carta dei diritti fondamentali dell'Unione Europea resa vincolante dal Trattato di Lisbona, il diritto di sposarsi è un diritto fondamentale ed è manifestazione della dignità umana.

E' impensabile che l'Italia , rimasta ormai in Europa fanalino di coda riguardo la tutela delle coppie formate da persone dello stesso sesso, non si sia ancora data  una legge che tuteli una coppia di uomini gay  o di donne lesbiche , nonostante la realtà registri oramai  migliaia di coppie di persone dello stesso sesso molto spesso anche in presenza di figli.

L'associazione radicale Certi Diritti ritiene che , in questi pochi giorni che ci separano da una data storica per i diritti delle persone omosessuali, l'intera comunità associativa di difesa e promozione dei diritti lgbt debba ritrovare forza, vigore e un senso di solidarietà che riesca a fare breccia nella società civile tutta per far comprendere che i diritti di alcuni sono patrimonio culturale per i diritti di tutti.

Sulla home page del  sito dell'associazione, www.certidirtti.it, è stato creato un "conto alla rovescia" dei giorni , i minuti ed i secondi , che mancano alla sentenza della Corte Costituzionale.

E' impegno dell'associazione, utlizzare il tempo rimanente per lavorare affinché la Consulta si esprima, finalmente ,  in favore delle coppie formate da persone dello stesso sesso.