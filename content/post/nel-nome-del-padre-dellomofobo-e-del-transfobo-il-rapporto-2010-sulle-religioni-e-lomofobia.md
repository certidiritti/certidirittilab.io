---
title: 'Nel nome del padre, dell’omofobo e del transfobo. Il Rapporto 2010 sulle religioni e l’omofobia'
date: Mon, 03 Jan 2011 09:07:40 +0000
draft: false
tags: [Comunicati stampa]
---

![](http://temi.repubblica.it/UserFiles/micromega-online/Image/rapport_2010_0.jpg)_da [gionata.org](http://www.gionata.org/omofobia/storie-e-riflessioni/.-nel-nome-del-padre-dellomofobo-e-del-transfobo.html)_

Mentre il 2010 volge al termine, salutiamo il nuovo anno con la pubblicazione, per la prima volta in italiano, del “Rapporto 2010 sulle religioni e l'omofobia” che fa parte del “[Decimo Rapporto sull'omofobia 2010](http://www.sos-homophobie.org/sites/default/files/rapport_annuel_2010.pdf)”, circa 180 pagine, curato dall'associazione francese “[Sos Homophobie](http://www.sos-homophobie.org/)”.

Nelle 12 pagine dedicate alle “Religioni e l'omofobia” e intitolate “Nel nome del Padre, dell'omofobo e del transfobo” il Rapporto 2010 traccia un quadro complesso, con molte ombre e qualche luce, perché a tutt'oggi le religioni sono ancora dei mezzi di potente legittimazione dell'omofobia e della transfobia agli occhi di numerosi credenti.  
Le persone omosessuali credenti invece, dal canto loro, spesso sono lacerate tra “il loro credo, il peso della tradizione religiosa e la loro identità sessuale’ e ‘fanno fatica a trovare un equilibrio”.

Il Rapporto 2010 affronta questi temi attraverso un rapido excursus sulle principali religioni occidentali: Cristianesimo (suddiviso in cattolici, anglicani, evangelici), Islam e Ebraismo.  
Sei agili sezioni intitolate: il cattolicesimo tra compassione e ipocrisia; l'omofobia non è omogenea all'interno del cristianesimo; Islam: tutto lo spettro dell'omofobia e della trans fobia; L'ebraismo: tu non amerai affatto; Ebraismo. Il punto di vista di Franck Giaoui e al termine una piccola rassegna stampa.

E’ stato un lungo anno, pieno comunque di tante sorprese positive, non ultima l'approvazione in Italia, da parte del Sinodo della Chiesa Valdese, della benedizione matrimoniale per le coppie omosessuali.  
Perché, come ricorda questo Rapporto 2010, “le religioni non sono entità monolitiche e la loro interpretazione varia secondo la società, secondo il Paese, e anche secondo l'atteggiamento e l'indirizzo delle diverse autorità religiose, così anche omosessuali e trans cominciano ad essere accolti/e da alcune istituzioni religiose”.

Questo vuol essere il nostro augurio per un buon anno per tutti gli uomini di buona volontà.

**Nel nome del padre, dell'omofobo e del transfobo  
Il Rapporto 2010 sulle religioni e l'omofobia**

Ancora una volta SOS homophobie ha ricevuto soltanto un limitato numero di testimonianze che denunciano l'omofobia e la transfobia che originano dalla sfera delle religioni.  
I discorsi religiosi rimangono tuttavia dei potenti vettori che troppo spesso legittimano l'omofobia e la transfobia agli occhi dei credenti.

Sette testimonianze ricevute dall'associazione richiamano indirettamente questa realtà.

Lacerati tra il loro credo, il peso della tradizione religiosa e la loro identità sessuale, un certo numero di omosessuali e di trans in questo modo fanno fatica a trovare un equilibrio.

Ma le religioni non sono entità monolitiche e la loro interpretazione varia secondo la società, secondo il Paese, e anche secondo l'atteggiamento e l'indirizzo delle diverse autorità religiose, così anche omosessuali e trans cominciano ad essere accolti/e da alcune istituzioni religiose.

**IL CATTOLICESIMO TRA COMPASSIONE E IPOCRISIA  
**  
Dopo la condanna della teoria del genere (1) da parte del Papa Benedetto XVI in occasione del suo discorso alla Curia nel 2008, per tutto il 2009 il Vaticano non ha smesso di opporsi all'uguaglianza di diritti delle minoranze sessuali in nome dei valori della "famiglia tradizionale" e del matrimonio eterosessuale.

La Chiesa cattolica continua a mettere in guardia gli Stati sui pretesi pericoli per la società insiti nell'autorizzare il matrimonio tra persone dello stesso sesso.

L'anno 2009 è anche stato caratterizzato dalla revoca, avvenuta in ottobre, di Padre Santoro da parte dell'Arcivescovo di Firenze, per aver celebrato il matrimonio religioso di una transessuale e del suo compagno, che erano già sposati civilmente da venticinque anni.

Con la stessa pervicacia il Vaticano continua a condannare i progetti di legge che prevedono che le coppie dello stesso sesso possano accedere all'adozione.

A questo proposito il progetto di legge uruguayano, così come la decisione del tribunale amministrativo di Besançon di concedere il nulla osta all'adozione ad Emmanuelle B., sono state nuove occasioni per la Chiesa di Roma di riaffermare il suo rifiuto dell'adozione da parte di omosessuali "per il bene del bambino".

Nella continuità delle argomentazioni tenute nel 1986 dal cardinale Ratzinger (futuro papa), che fanno riferimento all'omosessualità come ad "una minaccia per la vita e il benessere di un certo numero di individui", il Vaticano ha rifiutato di firmare la dichiarazione presentata il 13 dicembre 2008 all'ONU che richiedeva la depenalizzazione universale dell'omosessualità (2).

La controproposta promossa dal Vaticano e da numerosi Stati islamici afferma che i diritti umani universali non includono "l'interessarsi ai diritti di specifiche persone", escludendo di fatto le minoranze sessuali, ma sostiene "i principi di non-discriminazione e di uguaglianza".

Fatto questo che non impedisce al Papa e alla Chiesa di opporsi nel febbraio 2010 a un progetto di legge sull'uguaglianza che proteggesse gli omosessuali e i trans dalla discriminazione nelle assunzioni al lavoro.

La Santa Sede rimane così in profondo contrasto con le realtà pastorali di accoglienza così come con il vivere quotidiano e il benessere dei credenti omosessuali e trans.

La reazione del Vaticano alla dichiarazione del cardinale messicano Javier Lozano Barragan nel dicembre 2008 costituisce un esempio del paradosso e dell'ipocrisia della Chiesa di Roma.

Il cardinale Barragan affermava: "Forse essi \[le persone LGBT\] non sono colpevoli ma, agendo contro la dignità del corpo, non entreranno nel Regno dei cieli \[...\]. L'omosessualità è dunque un peccato \[...\].

Ma questo non giustifica nessuna forma di discriminazione. Soltanto Dio ha il diritto di giudicare. Noi, sulla Terra, non possiamo condannare, e in quanto persone, abbiamo tutti gli stessi diritti. Sono comunque persone e dunque devono essere rispettate".

Il portavoce della Santa Sede ha allora ricordato il Catechismo della Chiesa cattolica che definisce "disordinati" gli atti omosessuali, ma prende atto del fatto che "un numero non indifferente di uomini e di donne presentano profonde tendenze omosessuali", e che costoro "devono essere accolti con rispetto, compassione e delicatezza".

La Chiesa cattolica romana dunque accoglie "nella compassione" (patire insieme...) gli omosessuali o i trans, "peccatori per loro stessa natura". Ma se essa sembra accettare le persone, per contro condanna gli atti, i comportamenti, e dunque i diritti degli omosessuali e dei trans.

Questa inaccettabile posizione è una negazione del diritto delle minoranze sessuali ad essere se stesse.  
Costituisce quindi una fertile base per l'espressione di un'omofobia e di una transfobia da parte delle correnti fondamentaliste.

Così nell'ottobre 2009, riuniti in Vaticano nel quadro di un sinodo sull'Africa, alcuni vescovi hanno proposto leggi contro il matrimonio omosessuale.

Gli estremisti religiosi cristiani cercano anche i fondamenti della loro legittimità, sull'esempio della Fraternità San Pio X di Stoccarda, che nel luglio 2009 aveva paragonato l'omosessualità al nazismo.

Gli estremisti ultranazionalisti e religiosi approfittano dell'ambiguità della parola delle istituzioni religiose per far annullare il Gay Pride di Mosca o quello di Belgrado.

**L'OMOFOBIA NON È OMOGENEA ALL'INTERNO DEL CRISTIANESIMO  
**  
Il cristianesimo, composto da una pluralità di Chiese, oggi si deve confrontare con le discussioni sul riconoscimento dei diritti delle minoranze sessuali.

Resa fragile da un persistente disaccordo riguardo all'omosessualità e all'ordinazione di donne vescovo, la Chiesa anglicana con la sua corrente più liberale costituisce uno dei rami del cristianesimo più consapevoli delle realtà della nostra società.

Secondo Inclusive Church, un'associazione che promuove la diversità all'interno della comunità anglicana, le persone LGBT rappresentano nel 2009 circa il 20% del clero della Chiesa anglicana.

Il fondatore di Inclusive Church oggi vuole portare le autorità religiose a riconoscere questa situazione e metter fine all'ipocrisia: "Guardate tutte queste persone, esse svolgono un ruolo nella vita della Chiesa da dieci, venti, trenta e a volte addirittura da quarant'anni". Dobbiamo aprire gli occhi e smetterla di pretendere che tutto questo non esista".

Tuttavia la Chiesa di Inghilterra non ha mai fatto niente per i diritti degli omosessuali e dei trans. Inoltre la benedizione dell'unione di due reverendi gay fatta nel giugno 2009 dal pastore Martin Duedley ha provocato l'ira della frangia conservatrice degli anglicani.

La Chiesa anglicana americana è anche pioniera per l'integrazione di tutti i suoi fedeli. Il vescovo del Maine affermava nel luglio 2009: "Abbiamo dei fedeli gay e lesbiche, e dei membri del clero gays e lesbiche, noi cerchiamo di onorare la diversità della fede e della teologia nella nostra Chiesa".

In questa occasione la Chiesa episcopale degli Stati Uniti ha dato il suo accordo alla benedizione delle unioni omosessuali negli Stati che le riconoscono legalmente. Alcuni giorni più tardi, essa autorizzava ufficialmente l'ordinazione di pastori, uomini e donne, omosessuali.

Ma bisogna aspettare il dicembre 2009 affinchè la prima lesbica venga nominata vescovo di questa Chiesa e messa a capo della diocesi di Los Angeles.

Dopo la Chiesa episcopale Americana, sono i protestanti luterani ad accettare, nell'agosto 2009, i pastori omosessuali celibi o in coppia.

La Chiesa luterana ha anche consacrato nel novembre 2009 la prima vescova pastora lesbica a capo del vescovato di Stoccolma. La Chiesa di Svezia d'altra parte aveva approvato ufficialmente il matrimonio di omosessuali nelle sue chiese.

**ISLAM: TUTTO LO SPETTRO DELL'OMOFOBIA E DELLA TRANS FOBIA  
**  
L'Islam, benché non sia il solo a fare discorsi di odio verso le minoranze sessuali, resta nel 2009 una delle confessioni maggiormente omofobe e transfobe.

Delle dieci testimonianze riguardanti questa confessione ricevute quest'anno da SOS homophobie, sei mettono sotto accusa delle affermazioni e delle azioni omofobe e transfobe provenienti da individui di religione musulmana.

Nel mondo musulmano d'oggi, il fatto di svelare la propria identità sessuale può sempre condurre a morte.  
L'Islam considera la famiglia tradizionale come la chiave del mantenimento di una società "morale".

Numerosi musulmani LGBT devono affrontare l'isolamento e l'ostracismo da parte della loro famiglia e della loro comunità.  
Così Ahmet Yildiz, omosessuale turco ventiseienne, nel luglio 2008 veniva assassinato a colpi di fucile ad Istanbul in un agguato che la famiglia, in nome dell'onore e dei valori famigliari dell'Islam, aveva vigliaccamente organizzato contro di lui.

In casi estremi, le minoranze sessuali possono dover affrontare sentenze legali di carcerazione, di flagellazione, e anche di morte.

Così l'Iran, che figura tra i 77 Paesi che vietano penalmente l'omosessualità e la transessualità ancora nel 2009, ha condannato a morte nel mese di ottobre 2009 il sedicenne Nemat Safavi riconosciuto colpevole da un tribunale di aver praticato "atti sessuali che non sono ammessi".

Ed inoltre, secondo un rapporto dal titolo: Vogliono sterminarci: assassinio, tortura, orientamento sessuale e generi in Irak, pubblicato nell'agosto 2009 da Human Rights Watch, le azioni di tortura e le sommarie esecuzioni riguardanti uomini sospettati di omosessualità si vanno intensificando in nome dell'applicazione della legge islamica.

L'Arabia Saudita contribuisce attivamente alla sanguinosa repressione dell'omosessualità, come ha dimostrato la decapitazione con la sciabola di due Sauditi il 4 agosto 2009. Alcuni Paesi musulmani, come il Senegal, stigmatizzano le minoranze sessuali esumando le spoglie delle persone LGBT dai cimiteri musulmani (maggio 2009).

Altri Paesi islamici, anche se non puniscono con la morte gli omosessuali e i trans, si sono opposti, insieme al Vaticano, alla depenalizzazione universale dell'omosessualità all'ONU, nel dicembre 2008.

Ali Abdussalam Traki, diplomatico libico all'ONU, d'altra parte si era espresso così: "Si tratta di un argomento molto delicato, molto sensibile. In quanto musulmano, io non sono favorevole. L'omosessualità non è accettata nella maggioranza dei Paesi del mondo. E ci sono Paesi che l'autorizzano, pensando che sia una forma di democrazia... Penso che non sia così".

I Paesi europei contano un'importante popolazione musulmana, dalla quale le persone LGBT possono essere respinte in nome della religione.  
I comportamenti omofobi e transfobi che si basano sull'interpretazione di testi e sulla parola del clero musulmano continuano ad essere numerosi in Francia.

Ad esempio il club di football Bébel Créteil il cui allenatore nell'ottobre 2009 ha rifiutato che la sua squadra giocasse contro il club Paris Foot Gay per via delle convinzioni religiose dei calciatori musulmani.

Se non si può far altro che condannare ogni discorso di odio e il respingimento nella clandestinità delle minoranze sessuali da parte dei musulmani francesi, in compenso possiamo salutare il coraggio di uomini e di donne di cultura musulmana che testimoniano la loro aspirazione a vivere pienamente la loro omosessualità (Un omosessuale nella città di Brahim Nait-Balk; Homo-ghetto: Gays e lesbiche nelle città: i clandestini della Repubblica di Franck Chaumont).

Lo sforzo di reinterpretare il Corano e la legge islamica in favore dell'integrazione di tutti i fedeli deve costituire una priorità.

Siti Musdah Mulia, professore di pensiero islamico all'Istituto di Scienze indonesiane, ha del resto dichiarato nel novembre 2009 che "Gli omosessuali e l'omosessualità sono naturali e creati da Dio, e dunque compatibili con l'Islam".

**L'EBRAISMO: TU NON AMERAI AFFATTO  
**  
La religione ebraica non rimane inattiva. Si veda l'esempio di Israel Gutman, storico dell'Istituto Yad Vachem di Gerusalemme, che nel maggio 2008 condannava la costruzione a Berlino di un monumento alla memoria della deportazione degli omosessuali durante la seconda guerra mondiale.

All'interno dei gruppi più estremisti i soggetti omosessuali e trans rimangono ancora tabù e oggetto di disprezzo per le autorità religiose, come denuncia il primo film di Haim Tabakman, 'Tu non amerai affatto', uscito nel 2009.

E sono ancora gli ebrei ortodossi che, nel maggio 2009, tramite il ministro israeliano dell'Interno, Eli Yishai, membro del partito ultraortodosso Shass, e del Grande Rabbinato di Israele, hanno richiesto lo spostamento del Gay Pride di Tel Aviv "lontano dalla popolazione credente e dai minori".

Tuttavia un rabbino ortodosso, Ron Yosef, ha rivelato pubblicamente la sua omosessualità e nel febbraio 2008 ha creato l'associazione HOD (acronimo ebraico che indica gli "omosessuali religiosi") e www.hod.org.il, il primo sito Internet indipendente concepito per gli omosessuali ebrei ortodossi.

Questo sito mira a sensibilizzare la comunità religiosa, a promuovere un dialogo in uno spirito più tollerante e a portare un sostegno psicologico al pubblico religioso omosessuale.

Per fare ciò, il fondatore di HOD non ha esitato a rivolgersi direttamente ai capi, agli educatori e ai rabbini della comunità ebraica ortodossa per difendere il diritto a partecipare alla vita quotidiana ebraica senza essere messi al bando dalla comunità.

L'assenza di argomentazioni religiose che condannano fermamente l'omofobia e la transfobia continua a rafforzare il senso di impunità degli estremisti delle tre grandi religioni monoteiste, ma continua anche ad escludere, di fatto, gli omosessuali e i trans rifiutando loro di poter vivere serenamente il loro orientamento o la loro identità sessuale e la loro fede.

Benchè nelle società più liberali comincino a sorgere i dibattiti e i confronti, è ancora difficile per un credente omosessuale o un trans trovare un posto nelle comunità religiose. Ed è proprio negli Stati religiosi, nei quali i diritti degli individui fanno riferimento all'interpretazione rigida della "legge divina", che l'omofobia e la transfobia rimangono più radicate.

In questo contesto, la difesa del principio di laicità sembra essere una protezione per lottare contro la diffusione dei valori omofobi e transfobi veicolati dalle tre religioni del Libro.

Nel 2010 e nel 2011 la giornata contro l'omofobia (Idaho) ha per tema le religioni. Questa è l'occasione, per SOS homophobie e per l'insieme delle associazioni LGBT, di denunciare i discorsi e le azioni omofobe e transfobe che originano dalle diverse confessioni.

Il mondo associativo LGBT e SOS homophobie invitano così i dignitari religiosi, ma anche i credenti, ad interrogarsi insieme sulla riconciliazione tra la fede e l'orientamento sessuale, la fede e la transidentità, allo scopo di instaurare il dialogo, di dare maggior incisività alla lotta contro le discriminazioni fatte alle persone LGBT e di riconoscere tutti i credenti, ciascuno nelle proprie diversità.

**Il punto di vista di Franck Giaoui  
**  
Il Beit Haverim ("Casa degli Amici", in ebraico), è nato nel 1977 dal desiderio di un gruppo di amici preoccupati di conciliare la loro cultura ebraica e la loro omosessualità. Costituita come Associazione dal 1982, essa si indirizza ancora ai gays e alle lesbiche ebrei della Francia, ma le sue attività oggi tendono ad estendersi oltre questa sola doppia identità.

Sulla difficoltà di conciliare un'identità ebraica ed un orientamento omosessuale, Franck Giaoui risponde senza mezzi termini che si tratta soprattutto di "uno shock culturale" prima ancora di essere "uno shock religioso in rapporto a testi che proibiscono l'omosessualità": "\[Questa conciliazione\] non è difficile per quanto riguarda il singolo individuo. Ma quando la si considera in rapporto ad un quadro familiare, a tradizioni culturali talvolta molto pesanti, nel caso di famiglie tradizionali, allora sì che è complesso, e questo vale sia per i musulmani che per i cristiani."

Una ventina d'anni fa, se un giovane ebreo decideva di assumere la propria omosessualità, la viveva di nascosto, talvolta conduceva una doppia vita. "Era eccezionale che lo dicesse alla sua famiglia poichè essa (...) non parlava di omosessualità; e qualora essa fosse stata scoperta, si sarebbe spesso determinata una rottura famigliare."

Ma oggi, la comunità ebraica si è evoluta, proprio come la società francese. "Per il 78% dei simpatizzanti e dei membri del Beit Haverim nel 2009, ebraismo ed omosessualità sono conciliabili tra loro.  
La maggior parte dei giovani ebrei ora assume la propria omosessualità, certo, talvolta contro il parere della famiglia, ma non è più un dramma, o in ogni caso lo è molto meno."

Allo stesso modo, anche se i testi sono rimasti identici, la loro interpretazione si è evoluta: "Ieri (il 17 febbraio 2009, ndr)una sinagoga concistoriale organizzava un dibattito sul punto di vista della Torah (3) riguardo all'omosessualità. Il rabbino teneva un discorso che soltanto cinque o dieci anni fa sarebbe stato impensabile, ed ora questi dibattiti sono frequenti!"

Quando viene interrogato sui fondamenti di un divieto dell'omosessualità presenti nei testi, Franck Giaoui ricorda che " il Beit Haverim non è incline ad esprimersi sulla religione poichè non è un'associazione di culto, ma di cultura".  
E se accetta di risponderci, è semplicemente "in quanto responsabile associativo che da anni ascolta persone specializzate su quest'argomento", senza coinvolgere la responsabilità dell'associazione.

In primo luogo, "ciò che effettivamente è citato ne Pentateuco è una frase che dice . Ma l'interpretazione di testi "dipende dai diversi movimenti religiosi dell'ebraismo, che è ben lontano dall'essere unico."

Dall'interpretazione più ristretta ad esegesi più liberali, il senso dato al testo varia sensibilmente: divieto dell'atto di sodomia ("non essendo esplicitamente citata l'omosessualità femminile, (...) questo divieto riguarda soltanto l'omosessualità maschile"), oppure di ogni atto di potere sessuale di un uomo su di un altro... "Nell'interpretazione più liberale dei tasti, risulta vietato soltanto un atto sessuale che si assimila ad un avvilimento dell'altra persona, e questo indipendentemente dall'omosessualità."

In secondo luogo "la religione ebraica rivelata da un testo è soggetta ad un' enorme quantità di interpretazioni e di traduzioni, ci possono essere diverse accezioni di ciò che viene chiamato omosessualità e divieti."

E così il termine ebraico "toevah", che definisce l'omosessualità ma anche molti altri comportamenti, viene tradotto con "abominio" ma per alcuni sarebbe piuttosto sinonimo di "allontanamento": "Non sarebbe veramente un divieto, ma un allontanamento dalla fede o, per semplificare, un allontanamento da Dio."

E' interessante precisare che anche nelle correnti più rigide (ortodossi e ultraortodossi) è proprio l'atto che viene respinto, e non la persona. "Non si nasconde nella religione ebraica che una tendenza omosessuale può capitare a chiunque.  
Ma ci sono testi che descrivono come è possibile correggere questa tendenza o questo orientamento sessuale.  
In essi viene affermato che si deve esercitare il proprio libero arbitrio per canalizzare correttamente il proprio orientamento verso cose che non sono proibite. In altre parole per astenersi dal passare all'atto concreto..."

"Di contro nei movimenti liberali, per esempio negli Stati Uniti, ci sono sinagoghe che sono completamente 'omofile' nelle quali delle lesbiche e dei gays sono rabbini."

Anche degli eterosessuali frequentano queste sinagoghe, poichè vi trovano "al di là della religione, (...) una trasmissione di cultura più aperta che in certe sinagoghe o scuole religiose molto ortodosse".

Oltre alla sua partecipazione ai principali raduni LGBT in Francia, il Beit Haverim offre numerose attività. Franck Giaoui precisa che "per quanto riguarda la dimensione ebraica, \[l'associazione\] non propone alcun ufficio religioso dato che non ci sono officianti.

Invece dal lato pratico, organizza in modo piuttosto conviviale o semplicemente come una festa, delle serate cercando di farle coincidere con le feste religiose: a Pourim (4), a Pessa'h (5), a Roch Hachana (6)...

Talvolta queste feste vengono combinate con serate a tema, come i travestimenti in occasione di Pourim". Allo stesso modo si riappropria e gestisce i tradizionali pranzi del shabbat (7) organizzando dei pasti preparati a turno dai suoi aderenti "per ritrovare un'atmosfera tradizionale" nella casa del Beit.

"L'apertura della Casa del Beit ha avuto grande importanza poichè ci ha permesso, dopo tre anni, di avere numerose e varie attività in un luogo fisso e centrale di Parigi \[5, rue Fénelon, nel X arrondissement\]."

Il Beit Haverim organizza e partecipa anche a conferenze e scambi "con le diverse componenti della comunità ebraica (...) su vari argomenti di cultura, di società o di religione."

Ma le sua attività non si limitano a questo e mirano ad un'apertura sulla società, su attività socializzanti che non riguardano soltanto la cultura ebraica: corso di ebraico, scuola di canto, cineclub, forum "genitorialità", laboratori di sviluppo personale, serate conviviali... "

Queste attività sono riservate agli aderenti ma tutti, ebrei o non ebrei, omo o etero, possono venirci una volta a fare una prova prima di aderirvi."

Sta di fatto che Franck Giaoui porta uno sguardo nuovo e critico sugli attuali movimenti associativi, uno sguardo che non cessa di interrogare la società e le sue evoluzioni.

Partendo dalla constatazione che oggi i giovani si incontrano e comunicano prima di tutto tramite le nuove tecnologie e i social networks di Internet, e da quella di un militantismo ancora necessario ma talvolta obsoleto nella sua forma (visti i progressi dei diritti LGBT negli ultimi vent'anni), comprende perchè "certi giovani (...) dicono di non comprendere l'utilità, per essi, di un'associazione esclusivamente militante".

Secondo lui l'avvenire delle associazioni LGBT passa attraverso un adattamento: "La società sta già progredendo: il 64% dei Francesi sono favorevoli a concedere il diritto al matrimonio alle coppie omosessuali e il 57% a concedere loro il diritto di adottare.

E' evidente che al momento giusto il legislatore saprà mettersi d'accordo con la società, vale a dire con i suoi elettori! Tra una decina d'anni ci saranno di nuovo altre sfide. Dunque quello che si deve fare è preparare le generazioni di oggi e di domani perchè il nostro militantismo non sia un militantismo comunitarista.

Io penso che una della grandi evoluzioni, che in ogni caso il Beit Haverim da qualche anno ha iniziato a percorrere, è l'apertura verso una lotta per l'universalità dei diritti. Non si chiedono diritti specifici per gli omosessuali, le lesbiche e i trans: si chiede che tutti abbiano gli stessi diritti.

Non si chiede una lotta specifica contro gli antisemiti: si chiedono delle leggi che combattano contro l'insieme delle stigmatizzazioni e delle discriminazioni razziste. (...)  
E' bene che ognuno abbia una maggior apertura e non si limiti a difendere soltanto ciò che lo riguarda direttamente."

Ciò nonostante rimane ancora molto da fare in materia della doppia identità "ebraica-omosessuale". Far evolvere le diverse interpretazioni dei testi "verso un giusto equilibrio"; perchè "una coppia omo che desideri praticare la religione ebraica possa manifestarsi pubblicamente e i due possano venire fianco a fianco alla sinagoga.  
Ed inoltre è anche più pratico, dato che in una sinagoga gli uomini sono insieme, le donne sono insieme, dunque è un vantaggio rispetto agli etero!"

"Si deve prendere ciò che c'è di buono in ognuno ed estenderlo all'insieme degli spiriti. E perchè non allargare questo approccio, oltre i confini della comunità ebraica, all'insieme della comunità nazionale in Francia?"

Questo articolo è preso da un'intervista con Franck Giaoui. Il testo integrale è disponibile sul sito www.sos-homophobie.org

**RASSEGNA STAMPA  
**  
_Gli omosessuali ‘non entreranno mai nel regno dei cieli’  
_Articolo tratto da Le Figaro (Francia) del 3 dicembre 2009

Gli omosessuali e i transessuali "non entreranno mai nel regno dei cieli" ha affermato ieri il cardinale Javier Lozano Barragan, emerito 'ministro della Salute' del Vaticano, aggiungendo che "sono comunque persone e dunque bisogna rispettarle".  
Il portavoce di Benedetto XVI, padre Lombardi, ha reagito invitando ad "evitare qualsiasi forma di discriminazione ingiusta".

_Indonesia: l'Islam si traveste  
_Articolo di Arnaud Vaulerin tratto da Libération (Francia) del 5-6settembre 2009

Da un anno i transessuali del Paese hanno il loro proprio luogo di culto  
In precedenza si trovavano costretti a pregare sottoponendosi alla derisione, se non addirittura alle minacce.

Travestiti da uomo o da donna, scivolavano nelle sale di preghiera delle moschee temendo di essere smascherati. Maryani ha subito questo tipo di umiliazioni, prima di aprire, nel luglio 2008, la prima scuola coranica per waria: i transessuali secondo la denominazione indonesiana.

Questo luogo unico nel più grande paese musulmano al mondo, che accoglie omosessuali e travestiti si trova in una stradina di Notoyudan, tranquillo quartiere della cosmopolita capitale culturale del Paese, Yogyakarta. Non ci sono banchi, nè sale di lettura, e nemmeno la moschea in quella scuola.

Questo centro coranico non è simile a nessun altro. Trofei di concorsi di trucco si mescolano ad immagini kitsch della Mecca, foto di spettacoli circondano le riproduzioni delle sure.

Parrucchiera. In una sala dai muri gialli e arancio, Maryani riceve in casa sua, senza proselitismo. Nata uomo e cattolica, nel 1960 è diventata donna all'età di 15 anni. In seguito si è convertita all'Islam.

"Dio ha voluto così ed io ho rispettato la sua volontà. Ma sono rimasto fisicamente come Egli mi ha fatto." Questa ex prostituta e cantante si è trasformata in una pia parrucchiera che trucca e pettina bellerini, artisti e futuri sposi.

E' in questo modo che finanzia le attività del centro che accoglie regolarmente una trentina di transessuali, di gays e di lesbiche. "Accettiamo volentieri anche gli eterosessuali, dice Maryani divertita, ma sono soprattutto i 'waria' a venire qui. Anch'essi hanno il diritto di essere credenti.

Qui non hanno più bisogno di portare il mukena \[il velo per le donne, ndr\] o il sarong \[per gli uomini. ndr\].  
Vengono per studiare e soprattutto per imparare i rituali, recitare il Corano e mangiare insieme, senza tensioni e senza minacce". E senza segregazione.

Tutti si ritrovano sul pavimento bianco della sala comune. Là vengono tenuti dei corsi gratuiti ad opera di una ventina di ustadz, degli insegnanti musulmani, che si alternano tra loro. Vengono dalla grande vicina scuola del predicatore Hamrolie Harun.

Da molti anni quest'uomo sta a fianco degli omosessuali e si fa vedere accanto a loro. Ha incoraggiato la creazione di Senin-Kamis, il nome ufficiale della scuola, che significa semplicemente lunedì-giovedì, i giorni in cui è aperta.

Rispetto. Certo, il Consiglio degli ulema si è irritato per gli spettacoli dei travestiti destinati a scaldare la folla dei meetings durante l'ultima campagna delle legislative del 9 aprile.

Ma questo significa dimenticare la vivace tradizione dei waria in Indonesia e la realtà della diversità culturale in questo Paese mosaico di 235 milioni di abitanti nel quale convivono musulmani, cristiani, induisti e buddisti.

A Yogyakarta, il clero non ha trovato nulla da ridire sul lavoro dedicato di Mareyani. "Il capo della moschea è venuto: E' un segno di rispetto, no?". Un vicino si affaccia con la testa attraverso la porta e saluta. Più tardi una donna porta delle verdure. Il successo di Senin-Kamis è tutto merito di Maryani. Sposata e poi divorziata, ha adottato una bambina che ha 8 anni, ed oggi si definisce "serena e in pace".

Vive la sua fede con un candore disarmante. Come una devota matriarca, Maryani vigila con abnegazione sul suo centro. Colpita dal terremoto del 27 maggio 2006, che ha fatto più di 5.000 morti a Yogyakarta, ha iniziato a raccogliere denaro tra i transessuali.

L'idea della scuola le è venuta successivamente. "Era mio destino aprire questo posto. Prima di morire voglio fare del bene e aiutare gli altri waria".  
Ha convinto Tini a raggiungerla. Questa collaboratrice di 41 anni con discrezione assiste Maryani da molti mesi.

"Finalmente vivo senza essere minacciata, dice. Non era più possibile indossare il sarong per non sconvolgere la gente". Un planning, appuntato al muro, mostra dettagiatamente le attività: non c'è nemmeno uno spazio libero.

NOTE

(1) "L'uomo vuole rendersi autonomo e disporre autonomamente di ciò che lo riguarda, ma agendo in questo modo vive contro la verità, vive contro il suo creatore \[...\] Essa (la Chiesa) non ha solamente il dovere di difendere la terra, l'acqua e l'aria, ma anche di proteggere l'uomo dalla sua stessa distruzione."

(2) La dichiarazione in favore della depenalizzazione riafferma il principio di non-discriminazione e denuncia in particolare "il ricorso alla pena di morte, le esecuzioni extragiudiziarie, sommarie o arbitrarie, la pratica della tortura, dei trattamenti crudeli o disumani" inflitti agli omosessuali e ai trans.

(3) La Torah, "legge" in ebraico, definisce i primi cinque libri della Bibbia, chiamati anche "I Cinque Libri di Mosè" o "Pentateuco".

(4) Pourim è la festa che celebra le vicende, riportate nel libro di Ester, che hanno permesso agli ebrei di Persia di sfuggire, nel IV secolo a.C. ad un massacro.

(5) Pessa'h è la Pasqua ebraica.

(6) Roch Hachana è la celebrazione del nuovo anno del calendario ebraico.

(7) Il shabbat è il giorno di riposo assegnato al settimo giorno della settimana ebraica.

Testo originale tratto da [Rapport sur l’homophobie 2010](http://www.sos-homophobie.org/sites/default/files/rapport_annuel_2010.pdf) (file pdf)

_(31 dicembre 2010)_