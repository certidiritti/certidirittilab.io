---
title: 'UN OMOSESSUALE NORMALE'
date: Sun, 30 Jan 2011 09:44:54 +0000
draft: false
tags: [Politica]
---

_[![pezzana.cover_s](http://www.certidiritti.org/wp-content/uploads/2011/01/pezzana.cover_s.gif)](http://www.certidiritti.org/wp-content/uploads/2011/01/pezzana.cover.jpg)Angelo Pezzana_

UN OMOSESSUALE NORMALE

Diario di una ricerca d'identità attraverso il ricordo, la storia, il costume, le vite

COLLANA: **Eretica speciale**  
GENERE:  
pp. 288  

PREZZO: **12,00** euro  
(**20%** di sconto sul prezzo di copertina: **15,00** euro)  
ISBN: **978-88-6222-153-5**  
  

![](http://www.stampalternativa.it/img/libri_righette.gif)

Il libro è una sorta di diario nel quale l’autore racconta i momenti più significativi nella sua vita di omosessuale: ricordi d’infanzia, la prima percezione del sesso, i viaggi, le persone incontrate, illustri e sconosciute, la scoperta del mondo gay e la ricerca delle parole che non esistevano ancora. Poi la politica, l’omofobia, la cronaca nera, le leggi, le molte brevi biografie, per finire con il sesso e l’amore. La forma del racconto breve evidenzia quanto il libro sia ironico, divertente, polemico.