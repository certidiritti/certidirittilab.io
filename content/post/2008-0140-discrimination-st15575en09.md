---
title: '2008-0140-Discrimination-ST15575.EN09'
date: Tue, 17 Nov 2009 13:29:20 +0000
draft: false
tags: [Senza categoria]
---

  

  

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 17 November 2009

Interinstitutional File:

2008/0140 (CNS)

15575/09

SOC 670

JAI 805

MI 413

  

  

  

  

  

REPORT

from :

The Presidency

to :

Permanent Representatives Committee (Part I) / Council (EPSCO)

No. prev. doc. :

15320/09 SOC 645 JAI 762 MI 407

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

-    Progress Report

**I.       INTRODUCTION**

On 2 July 2008, the Commission adopted a proposal for a Council Directive aiming to extend the protection against discrimination on the grounds of religion or belief, disability, age or sexual orientation to areas outside employment. Complementing existing EC legislation[\[1\]](#_ftn1) in this area, the proposed Directive would prohibit discrimination on the above-mentioned grounds in the following areas: social protection, including social security and healthcare; social advantages; education; and access to goods and services, including housing.

  

At the time, a large majority of delegations welcomed the proposal in principle, many endorsing the fact that it aims to complete the existing legal framework by addressing all four grounds of discrimination through a horizontal approach.

Most delegations have affirmed the importance of promoting equal treatment as a shared social value within the EU. In particular, several delegations have underlined the significance of the proposal in the context of the UN Convention on the Rights of Persons with Disabilities (UNCRPD)[\[2\]](#_ftn2). However, some delegations would have preferred more ambitious provisions in regard to disability.

While emphasising the importance of the fight against discrimination, certain delegations have put forward the view that more experience with the implementation of existing Community law is needed before further legislation is adopted at the Community level. These delegations have questioned the timeliness of and the need for the Commission’s proposal, which they see as infringing on national competence for certain issues and as conflicting with the principles of subsidiarity and proportionality.

Certain other delegations have also requested clarifications and expressed concerns relating, in particular, to the lack of legal certainty, the division of competences, and the practical, financial and legal impact of the proposal.

For the time being, all delegations have maintained general scrutiny reservations on the proposal. CZ, DK, FR, MT and UK have maintained parliamentary scrutiny reservations, CY and PL maintaining linguistic scrutiny reservations. The Commission has meanwhile affirmed its original proposal at this stage and has maintained a scrutiny reservation on any changes thereto.

  

The European Parliament adopted its Opinion under the Consultation Procedure on 2 April 2009[\[3\]](#_ftn3).

**II.      THE COUNCIL'S WORK UNDER THE SWEDISH PRESIDENCY**

The EPSCO Council having been informed of the progress achieved on the provisions concerning disability under the Czech Presidency on 8 June 2009[\[4\]](#_ftn4), the Working Party on Social Questions has continued its examination of the entire proposal under the Swedish Presidency on the basis of a series of Presidency drafting suggestions concerning, in particular, _the scope_ of the Directive, _the division of competences_ between the European Community and the Member States, _the disability provisions_, and _the implementation calendar_[\[5\]](#_ftn5). These drafting suggestions aimed, _inter alia_:

-        to clarify _the scope_ by adjusting Article 3, and by providing examples and explanations in the recitals;

-        to clarify the question of the respective _competences_ of the European Community and the Member States by stating that the division of competences is not changed by the Directive (Article 3(2)), and by explicitly listing certain matters to which the Directive does not apply (Article 3(2));

  

-        to fine-tune the _disability provisions_, including by aligning them more closely with the UNCRPD and by introducing a clear distinction between the general requirement to improve "accessibility" and the more specific requirement to provide "reasonable accommodation" to ensure "access" in particular cases;

-        to introduce clearer provisions regarding _legitimate differences of treatment_, including on the grounds of age and disability; and

-        to adjust _the tentative implementation calendar_ by providing for separate implementation deadlines for ensuring accessibility in respect of new buildings, facilities and infrastructure, as well as existing buildings, facilities and infrastructure undergoing significant renovation (5 years), and for all other existing buildings, facilities and infrastructure (20 years).

Delegations have broadly welcomed the Presidency's suggestions as steps in the right direction. However, the discussions have also shown that extensive further work is still needed on many aspects of the proposal.

**III.    OUTSTANDING ISSUES**

**1.           Division of Competences, Scope and Subsidiarity (Article 3)**

Further discussion is needed on _the scope_, including with a view to establishing the intended reach of _the disability provisions_ (see below) and to demarcating the _division of competences_ between the Member States and the European Community even more precisely than hitherto[\[6\]](#_ftn6). In particular, more work is needed to elucidate the delicate distinction between _access_ to fields such as education, healthcare and social protection, and _the organisation_ of such fields, the latter being an area of national competence.

**2.           The Disability Provisions (Articles 4, 4a and 4b)**

Further discussion will be needed on the following issues:

-        _the scope_ of the provisions and _the concrete obligations_ to be created, including with respect to the physical environment (or "the built environment"); new and existing buildings, facilities and infrastructure; different types of building and housing; transport; and the design and manufacture of goods;

-        the method for assessing what constitutes _a disproportionate burden_, and the notion of _a denial of reasonable accommodation_.

-        _the financial, administrative and practical implications_ of the provisions and their scope, particularly regarding _SMEs and the self-employed_; and

-        the exact interrelationship between the draft Directive and _more detailed sectoral standards or specifications_ on the accessibility of particular goods and services, including public transport.

**3.           The Implementation Calendar**

Delegations have broadly welcomed the increasingly nuanced approach to the implementation calendar, but have also called for clarification concerning the obligations to be created by the Directive before dates are agreed.

**4.** **Legal Certainty in the Directive as a Whole**

In underlining the importance of legal certainty, delegations have expressed the wish to avoid further cases having to be brought before the European Court of Justice (ECJ). They have consequently stressed the need for the clearest possible wording throughout, including in the _definitions_ of key terms, and have underlined the importance of ensuring _consistency with existing legislation_.

**5.       Other Issues**

A large number of other questions will also require further discussion, including the following:

-        the _legal basis_, including _the cross-border dimension_ that underlies the Community competences in the fields listed in the scope;

-        the concepts of _harassment_, _discrimination by association_ and _discrimination based on assumptions_;

-        legitimate differences of treatment _on the grounds of age_ (especially in respect of minors), and _on the grounds of disability as well as age_ in the provision of _financial services_ (including banking and insurance);

-        the need to find the right balance between _protection against discrimination_ and _rights in the private sphere_ (including freedom of speech and religion_)_; and

-   the issue of _gender mainstreaming_.

  

Further information and details of delegations' positions may be found in docs. 14008/09, 14979/09 + COR 1 and 16063/09[\[7\]](#_ftn7).

**IV.    CONCLUSION**

While significant progress has been made under the Swedish Presidency in the attempt to clarify _the scope_, _the division of competences_, _the disability provisions_, and _the implementation calendar_, there is a clear need for extensive further work on the proposal. The Committee is invited to take note of this Progress Report and to forward it to the EPSCO Council on 30 November 2009.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) In particular, Council Directives 2000/43/EC, 2000/78/EC and 2004/113/EC.

[\[2\]](#_ftnref2) See docs. 12892/2/08 REV 2, 12892/08 ADD 1 REV 1, 8321/09, 9277/09 and 12786/09.

[\[3\]](#_ftnref3) See doc. A6-0149/2009. Kathalijne Maria Buitenweg (Group of the Greens / European Free Alliance) served as Rapporteur.

[\[4\]](#_ftnref4) See doc. 10073/1/09 REV 1.

[\[5\]](#_ftnref5) Docs. 11774/09, 12792/09, 13238/09, 14009/09 and 15320/09. The Working Party has discussed the proposal at six meetings: 17 July, 7 and 22 September, 22 October, and 9 and 13 November 2009.

[\[6\]](#_ftnref6) See also the Opinion of the Council Legal Service (doc. 14896/08).

[\[7\]](#_ftnref7) To be distributed in due course.