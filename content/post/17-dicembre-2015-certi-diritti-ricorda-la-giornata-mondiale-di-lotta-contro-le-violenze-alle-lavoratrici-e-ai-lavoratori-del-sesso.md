---
title: '17 DICEMBRE 2015: CERTI DIRITTI RICORDA LA GIORNATA MONDIALE DI LOTTA CONTRO LE VIOLENZE ALLE LAVORATRICI E AI LAVORATORI DEL SESSO.'
date: Wed, 16 Dec 2015 14:59:48 +0000
draft: false
tags: [Lavoro sessuale]
---

[![sex work](http://www.certidiritti.org/wp-content/uploads/2015/05/sex-work-300x114.png)](http://www.certidiritti.org/wp-content/uploads/2015/05/sex-work.png)Il 17 dicembre è la giornata mondiale di lotta contro le violenze fatte alle lavoratrici e ai lavoratori del sesso. Aggressioni, repressione, sfruttamento, espulsioni, violenze, omicidi: se queste violenze non riguardano solo lavoratrici e lavoratori sessuali, esse sono favorite da un quadro giuridico che li penalizza specificamente.

La messa fuori legge dell'adescamento e del prossenetismo favoriscono lo sfruttamento di lavoratrici e lavoratori del sesso, che devono anche lottare contro i fermi e le sanzioni imposte da assurde ordinanze di sindaci particolarmente zelanti, per non parlare delle proposte sempre più insistenti di criminalizzare i clienti. Oltre a favorire le violenze, questa situazione favorisce la precarizzazione del lavoro sessuale e mina la salute delle lavoratrici e dei lavoratori sessuali.

"La vulnerabilità alla violenza e allo sfruttamento non è combattuta da misure punitive o poliziesche, ma dalla piena decriminalizzazione del lavoro sessuale sul modello neozelandese che permetta anche una piena e libera organizzazione degli interessi delle lavoratrici e dei lavoratori sessuali. È tempo che il legislatore se ne faccia carico. Lavoratrici e lavoratori del sesso sono cittadini e lavoratori come tutti gli altri e devono essere trattati allo stesso modo. Il lavoro sessuale è lavoro e dev'essere regolamentato dal codice civile, non da quello penale", dichiara Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti.