---
title: 'Certi Diritti sottoscrive lettera a Berlusconi su Fondo Aids'
date: Sat, 09 Jul 2011 19:00:00 +0000
draft: false
tags: [Lavoro sessuale]
---

**Il Governo italiano non ha ancora versato al Fondo Globale per la Lotta contro l’AIDS, la Tubercolosi e la Malaria i contributi promessi per il 2009 e il 2010 e non ha assunto alcun impegno finanziario per il triennio 2011-2013. Lettera aperta della società civile italiana.**

  
In vista della VI Conferenza IAS su Patogenesi, Trattamento e Prevenzione dell’HIV – la più grande riunione scientifica mondiale sull’HIV/AIDS - che si svolgerà a Roma dal 17 al 20 luglio 2011, il Forum della società civile italiana sull’HIV/AIDS, [che si incontrerà a Roma il 12 luglio](http://www.forumhivaids.it/?page_id=17), ha redatto una lettera aperta al Presidente del Consiglio Berlusconi affinché rinnovi lo storico impegno dell’Italia nei confronti del Fondo Globale.

L'associazione radicale Certi Diritti sottoscrive la lettera aperta della società civile italiana e internazionale al Presidente Berlusconi

**L’ITALIA FINANZI IL FONDO GLOBALE CONTRO LE PANDEMIE**  
**AIDS, TUBERCOLOSI E MALARIA NON ASPETTANO!**

  
Roma, luglio 2011

Signor Presidente del Consiglio dei Ministri Silvio Berlusconi,

in vista della più grande riunione scientifica mondiale sull’HIV/AIDS, la VI Conferenza IAS su Patogenesi, Trattamento e Prevenzione dell'HIV che si aprirà il 17 luglio p.v. a Roma, Le scriviamo per sollecitare il Governo italiano a rinnovare l’impegno storico nei confronti del Fondo Globale per la Lotta contro l'AIDS, la Tubercolosi e la Malaria (GFATM).

Dal 2002, il Fondo è diventato il principale finanziatore di programmi di lotta contro l'AIDS, la tubercolosi e la malaria, salvando la vita a 6,5 milioni di persone e ha contribuito a migliorare la salute materna e infantile anche attraverso la promozione dell’accesso ai servizi per la salute sessuale e riproduttiva e l’HIV/AIDS.

L'Italia ha svolto un ruolo di primo piano nel lancio del Fondo Globale in occasione del Vertice G8 del 2001 a Genova, ma il suo impegno è  venuto meno negli ultimi due anni, in quanto il Governo italiano non ha ancora erogato i contributi promessi per il 2009 e il 2010, non ha assunto alcun impegno finanziario per il triennio 2011-2013 e non è più titolare di un seggio unico al Consiglio di Amministrazione del Fondo.

Siamo consapevoli delle difficoltà che l'Italia sta attraversando a causa della crisi economica globale, sappiamo, tuttavia, che i tagli al Fondo e più in generale, all’Aiuto Pubblico allo Sviluppo, ricadranno in particolare su donne, ragazze e bambini che non hanno accesso alla prevenzione, all’assistenza sanitaria, ai servizi sociali e i cui diritti umani sono pertanto quotidianamente violati.

Altri paesi europei che hanno piani di risanamento economico non differenti da quello italiano hanno ugualmente saputo rispettare i propri impegni.

Il Vertice G8 di Deauville del maggio scorso ha riconfermato il proprio sostegno al Fondo Globale e ne ha accolto l’impegno a realizzare una riforma per migliorare la supervisione, l’accountability e l’efficacia nell’uso delle risorse, che consentirà ai donatori di onorare i propri impegni finanziari. E ancora, il mese scorso, le Nazioni Unite hanno adottato una Dichiarazione che riconosce indispensabile un adeguato finanziamento del Fondo Globale per raggiungere l'accesso universale a prevenzione, terapia e sostegno, per il contrasto dell’HIV/AIDS.

L’Italia non può, Signor Presidente, smentire un impegno così importante che, se mantenuto, congiuntamente a un adeguato rilancio della cooperazione allo sviluppo, contribuirebbe anche allo sviluppo sociale ed economico del Sud del mondo.

Pertanto, La esortiamo ad assicurare al più presto che l’Italia:

•    comunichi un piano di rientro per l’esborso al Fondo Globale dei contributi per il 2009 e il 2010 - pari a 260 milioni di euro - e dei 30 milioni di dollari addizionali che Lei ha promesso al Vertice G8 del 2009 a L'Aquila

•    rinnovi l’impegno finanziario a favore del Fondo Globale per il triennio 2011-2013.  
Non assicurare le risorse necessarie al Fondo Globale ne pregiudicherebbe la capacità di raggiungere gli obiettivi strategici e avrebbe ripercussioni anche in termini di credibilità internazionale per il nostro Paese.

Confidando in un Suo riscontro, ci è gradita l’occasione per porgerLe i migliori saluti.

Actionaid - Italia  
ANLAIDS Onlus – Associazione Nazionale per la Lotta contro l’AIDS – Italia  
Arcigay – Italia  
Circolo di Cultura Omosessuale Mario Mieli - Italia  
CNCA - Coordinamento Nazionale Comunità di Accoglienza  – Italia  
Comitato per i Diritti Civili delle Prostitute Onlus – Italia  
Forum della società civile italiana sull’HIV/AIDS - Italia  
Forum Droghe – Italia  
Gruppo Abele Onlus – Italia  
I Ragazzi della Panchina Onlus - Italia  
LILA Nazionale Onlus – Lega Italiana per la Lotta contro l’AIDS – Italia  
MIT – Movimento Identità Transessuale – Italia  
Nadir Onlus - Italia  
NPS Italia Onlus – Network Persone Sieropositive  - Italia  
Osservatorio Italiano sull’Azione Globale contro l’AIDS – Italia  
Villa Maraini – Italia