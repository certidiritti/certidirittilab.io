---
title: 'UNIONI CIVILI. RADICALI: SOLIDARIETÀ A FAMIGLIA SERGIO LO GIUDICE PER RIPETUTI ATTACCHI'
date: Fri, 12 Feb 2016 13:52:01 +0000
draft: false
tags: [Politica]
---

[![download](http://www.certidiritti.org/wp-content/uploads/2016/02/download.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/02/download.jpg)"Ci stringiamo al nostro amico Sergio Lo Giudice e alla sua famiglia per i ripetuti attacchi di cui sono oggetto, provocazioni basse e meschine che ieri con il siparietto del vicepresidente Gasparri hanno decisamente toccato il fondo. Ogni apprezzamento nei  confronti della loro vita privata e delle loro scelte è una ferita inferta alle migliaia di famiglie omogenitoriali che vivono nel nostro paese. Ringraziamo Sergio, suo marito e le altre famiglie arcobaleno visibili che con grande sacrificio portano avanti una battaglia di tutti." Lo dicono in una nota congiunta Filomena Gallo e Marco Cappato, segretario e tesoriere dell'Associazione Luca Coscioni, Yuri Guaiana e Riccardo Magi, segretari rispettivamente dell'Associazione Radicale Certi Diritti e di Radicali Italiani. "Invitiamo l'ufficio di presidenza di palazzo Madama a far rispettare in Aula l’art. 3 della nostra Costituzione che parla del diritto alla dignità di tutte le persone, senza distinzione alcuna".