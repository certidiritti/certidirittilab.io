---
title: 'Ricordando Davide, oggi alle ore 19.30 fiaccolata a Roma'
date: Thu, 22 Nov 2012 10:10:45 +0000
draft: false
tags: [Movimento LGBTI]
---

Martedì Davide, un quindicenne romano, si è tolto la vita per omofobia. Il Circolo di Cultura Omosessuale Mario Mieli, Queerlab, l’Associazione Radicale Certi Diritti  e Luiss Arcobaleno danno appuntamento stasera alle 19.30 per una fiaccolata che partirà  da via di San Giovanni in Laterano (Coming Out) per arrivare al Liceo Cavour in Via delle Carine, 1.

    Il nome è di fantasia ma purtroppo tutto il resto è un’orrenda realtà. Davide, un quindicenne romano, si è tolto la vita per omofobia. Vessato dai compagni da più di un anno, rimproverato da un’insegnante, non ce l’ha fatta più e si è impiccato con una sciarpa, davanti al fratello minore.  
  
    L’ennesima bruttissima pagina che racconta una condizione, quella di gay e lesbiche e trans ancora in età scolare, che sono preda dell’ignoranza, insultati ed emarginati.  
    “Il Circolo di Cultura Omosessuale Mario Mieli registra  questo episodio di bullismo ignorante che colpisce chi si trova in un età delicatissima di affermazione della propria identità e si trova spesso a farlo in un contesto sociale e familiare ostile o indifferente.  
    Siamo da anni impegnati a fronteggiare l’ignoranza che colpisce in particolare, sin dall’età più giovane gay, lesbiche e transgender – afferma Andrea Maccarrone, Presidente dell’Associazione – ma non è facile entrare nelle scuole per fare informazione e per contrastare il bullismo, non solo quello di stampo omofobo e transofobo.  
  
    Finché le Istituzioni non prenderanno veramente sul serio questo problema collaborando con le Associazioni che se ne occupano, non riusciremo a scardinare alla radice un problema che è prima di tutto culturale. Crediamo inoltre che alle aggressioni, alle vessazioni, agli insulti nei confronti di gay lesbiche e trans uno stato democratico deve  rispondere con leggi adeguate e pene veramente commisurate al delitto. È in tal senso che reclamiamo da tempo l’estensione della legge Mancino ai reati di stampo omofobico. Anche il ruolo dei media risulta importantissimo: l’atteggiamento di chi, come nell’articolo odierno su Repubblica.it, minimizza a semplici bravate insulti e ingiurie, finisce per risultare colpevolmente complice.”  
  
Per ricordare Davide, esprimere vicinanza a chi lo ha amato e rispettato con la sua diversità e il suo invincibile desiderio di autenticità, per non lasciare che il suo gesto passi ancora una volta inosservato e senza conseguenze, Il Circolo di Cultura Omosessuale Mario Mieli, Queerlab, l’Associazione Radicale Certi Diritti  e Luiss Arcobaleno  danno appuntamento  stasera  alle 19.30 per una fiaccolata che partirà  da via di San Giovanni in Laterano (Coming Out) per arrivare al Liceo Cavour in Via delle Carine, 1.  L’invito per tutte e tutti è di venire senza bandiere e indossando un capo rosa,  il colore tanto amato da Davide.