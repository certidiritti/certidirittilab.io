---
title: 'Gemellaggio Milano - San Pietroburgo: pubblichiamo la corrispondenza tra le due amministrazioni e la risposta del comune di Milano a un''interrogazione del gruppo radicale- federalista europeo'
date: Tue, 05 Mar 2013 10:39:05 +0000
draft: false
tags: [Russia]
---

Comunicato Stampa dell’Associazione Radicale Certi Diritti.

Milano, 4 marzo 2013

Lo scorso dicembre, l’Associazione Radicale Certi Diritti è venuta a conoscenza di una serie di articoli, diffusi dalla stampa russa, secondo i quali la città di Milano avrebbe mandato comunicazione formale a quella di San Pietroburgo affermando che “la città di Milano non risponde alle raccomandazioni del Consiglio Comunale e continuerà a lavorare con la sua città gemella di San Pietroburgo” e che la legge contro la cosiddetta propaganda omosessuale non impedirebbe la collaborazione sulla base del gemellaggio.

Grazie al consigliere radicale Marco Cappato abbiamo immediatamente fatto un’interrogazione per chiedere al Sindaco:  
Se corrisponda al vero quanto riportato dalla stampa russa;  
Di conoscere il contenuto della corrispondenza intercorsa tra l’amministrazione di Milano e quella della città di San Pietroburgo sulla questione;

Di sapere come s’intenda rispettare Mozione IM-99: Gemellaggio San Pietroburgo del 22 novembre 2012.  
Nella risposta del direttore del Settore Relazioni Internazionali, dott. Santaniello, si legge che “le notizie apparse sulla stampa locale di San Pietroburgo non sono corrette” e che “i media hanno dato comunicazioni parziali e di parte alle nostre comunicazioni”. Il dott. Santaniello conclude la sua risposta affermando che “L’Amministrazione comunale non ha intrapreso iniziative dirette , né attribuito patrocini o deciso contributi nell’ambito delle attività del Gemellaggio”.

Nella corrispondenza intercorsa tra le due amministrazioni si evince come effettivamente Santaniello non abbia detto che “la città di Milano non risponde alle raccomandazioni del Consiglio Comunale e continuerà a lavorare con la sua città gemella di San Pietroburgo”. Santaniello si limita a precisare che la mozione non revoca il gemellaggio. Essa infatti impegna la Giunta e le partecipate a non promuovere né svolgere attività nell’ambito dello stesso, sospendendo di fatto gli effetti dello stesso sino alla revoca della legge contro la cosiddetta “propaganda omosessuale”.

Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti, dichiara: “siamo contenti dell’ufficiale smentita di quanto affermato dalla stampa russa. Siamo invece assai insoddisfatti poiché il Direttore del settore Relazioni Internazionali del Comune di Milano non ha ritenuto di prendere una posizione netta sul significato della mozione e dei suoi effetti nelle comunicazioni intercorse con la città di San Pietroburgo. Sui diritti umani non si può glissare opportunisticamente, la mozione parla molto chiaro e noi continueremo a vigilare sul suo rispetto da parte della Giunta e delle partecipate”.

**[LA RISPOSTA ALL'INTERROGAZIONE SUL GEMELLAGGIO >](http://www.milanoradicale.it/2013/02/28/risposta-interrogazione-sul-gemellaggio-milano-san-pietroburgo/)**

  
Alcuni articoli pubblicati sulla stampa russa:  
[Милан останется побратимом Петербурга, несмотря на антигейский закон](http://vchera.com/news/30219/)

[Милан останется побратимом Петербурга](http://fedpress.ru/news/society/news_society/1355991492-milan-ostanetsya-pobratimom-peterburga)

[Милан останется городом-побратимом с Петербургом](http://bookman.spb.ru/piter/milan-ostanetsya-gorodom-pobratimom-s-peterburgom.html)

[Питерский закон о гей-пропаганде не повлияет на отношения с Миланом](http://www.rg.ru/2012/12/20/reg-szfo/milan-anons.html)

[Сотрудничеству Петербурга и Милана ничего не угрожает](http://www.iskusstvo.tv/News/2012/12/21/sotrudnichestvu-peterburga-i-milana-nichego-ne-ugrozhaet)