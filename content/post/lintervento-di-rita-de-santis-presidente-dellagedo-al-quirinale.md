---
title: 'L''INTERVENTO DI RITA DE SANTIS, PRESIDENTE DELL''AGEDO, AL QUIRINALE'
date: Mon, 17 May 2010 10:59:44 +0000
draft: false
tags: [Comunicati stampa]
---

INTERVENTO DI RITA DE SANTIS, PRESIDENTE DELL’AGEDO, DURANTE L’INCONTRO AL QURINALE DEL PRESIDENTE DELLA REPUBBLICA CON LE ASSOCIAZIONI LGBT IN OCCASIONE DELLA GIORNATA INTERNAZIONALE CONTROL’OMOFOBIA.

Signor Presidente,

è con sincera commozione che oggi, davanti alla sua Illustre Persona, mi accingo a leggere il mio intervento, in rappresentanza delle associazioni LGBT che con molta sensibilità e con grande spirito democratico  lei ha deciso di ricevere in una giornata così piena di significati per tutti noi.

Mi chiamo Rita De Santis e sono la presidente Nazionale di AGedo associazione che riunisce genitori, parenti, amici di omosessuali e transessuali, ed assicura la sua presenza su tutto il territorio nazionale svolgendo un ruolo di accoglienza per i genitori, di sensibilizzazione nelle scuole e di attenzione  sui diritti non ancora assegnati ai nostri figli.

Mi è stato dato l'onere e l’onore di rappresentare tutte le associazioni da Lei invitate all’incontro in quanto in queste associazioni sono i luoghi dove i nostri figli trovano lo spazio per le loro esigenze e le loro idee, associazioni  con le quali lavoriamo in rete per costruire un futuro scevro da discriminazioni.

Le porto in questa testimonianza il saluto di noi genitori parenti e amici di persone omosessuali, bisessuali e transessuali, sia di quelli che il coraggio rende visibili, sia di quelli che purtroppo per varie circostanze visibili non sono o non possono esserlo..

Mi creda non è facile vivere da invisibili, da clandestini, fantasmi ai quali viene negato innanzitutto il primo fondamentale diritto dato agli esseri umani che è quello del riconoscimento della propria identità di genere per le persone trasgender ed intersessuate, e il riconoscimento della propria affettività e del proprio amore per i gay e le lesbiche.

Purtroppo i moltissimi episodi di omo-transfobia che ormai quasi quotidianamente si perpetuano nel nostro paese, ci addolorano e ci fanno riflettere sul fatto che molto spesso l’ignoranza, il pregiudizio trionfano sul quel presupposto fondamentale della democrazia che è la ricchezza della differenza, eletta a cardine di una società laica ma attenta anche ai valori di chi crede.

Certo, oggi noi, davanti a Lei ci sentiamo fortunati di avere un così alto interlocutore e questa sua sensibilità ci fa percepire lontano storicamente e definitivamente tramontati i roghi medioevali e la oscura stagione dei triangoli rosa.

Ma noi tutti vorremmo che in questa società, per molti versi così avanzata, non ci fossero più i 78 paesi dove essere gay e trans vuol dire galera e i 7 dove per l’omosessualità e la transessualità c’è ancora la pena di morte e non vorremmo che questi fratelli dei nostri figli spesso costretti ad emigrare per sfuggire a condizioni estreme di violenza si vengano poi a trovare in un paese come il nostro, dove nonostante tutte le importanti libertà conquistate,  alberga ancora tanto razzismo, tanta omofobia e transfobia, dovendo così subire discriminazioni multiple.

Essere qui oggi non ci differenzia molto da tutti i genitori del pianeta, essere qui è una testimonianza di quell’amore che noi dobbiamo e che sentiamo per i nostri figli e che nella mente di ognuno di noi si tramuta nella speranza del raggiungimento di un loro completo benessere.

Questo purtroppo però non è possibile poiché l’Italia non essendosi allineata all’articolo 13 del paragrafo 2 del trattato di Lisbona è rimasta l’unico paese della comunità europea dove ci sono ancora **cittadini di serie A e cittadini di serie B.**

**Di fronte a doveri tutti uguali,   per i diritti palesemente diseguali.**

A questo proposito bisognerebbe che il nostro parlamento prendesse in considerazione, anche sulla scorta del parere della corte costituzionale che citando l’articolo 2 della costituzione,ha rinviato a quella sede il riconoscimento delle unioni omosessuali, di pronunciarsi, perché la mancanza di leggi

comporta, a nostro modesto avviso, che proprio i cittadini non tutelati da univoci diritti diventano facilmente bersaglio di scherno e di umiliazione quando poi non si arriva nei casi estremi alla violenza e alla morte.

Noi siamo qui oggi in nome di quell’amore che anche il sommo poeta Dante descrive e percepisce come il motore del mondo, di quell’amore che vorremmo fosse riconosciuto ai nostri figli nella formazione delle loro famiglie e nell’esercizio di quella genitorialità che non va confusa con la mera possibilità di generare.

Siamo qui perché i nostri nipoti ( sono almeno 100.000 in Italia i figli e le figlie delle persone omosessuali e transessuali) non abbiano più a soffrire l’allontanamento dai lori genitori nelle cause di separazione, perché possano contare sempre sull’appoggio degli educatori nelle scuole, perché possano testimoniare sereni la verità della loro famiglia.

Siamo qui per chiedere che i nostri figli e figlie trans non vengano confuse con fabbriche di vizio, o peggio di perversioni e devianze.

Vorremo togliere loro quell’imbarazzo nel trovarsi chiuse in un maschile e femminile che spesso non li rappresenta nel più profondo del loro essere attraverso una nuova legge che togliendoli da quel baratro di confusione che li costringe a vivere tutta la vita in un limbo giuridico e li lascia spesso  in una imbarazzante situazione di abuso sia nella vita privata che nel lavoro.

Vorremmo oggi diffondere nel nostro paese e in tutto il mondo l’orgoglio che dal profondo del cuore sentiamo per tutti questi nostri figli perché è attraverso loro che abbiamo compreso la mirevole complessità della natura e del creato.

Noi genitori di questa immensa popolazione ( si stima che il 10 per cento degli abitanti della terra sia LGBT) rinnovandoLe i saluti e ringraziamenti speriamo che questo storico incontro sia l’inizio di una stagione migliore, speriamo di non dover più registrare che la visibilità diventi violenza, che la pienezza affettiva della vita non sia riservata solo ai figli di serie A; speriamo che in un domani illuminato dall’arcobaleno del buon senso e dell’uguaglianza nessuno debba venire più giudicato per il proprio orientamento sessuale o per l’identità di genere.

Solo se riusciremo attraverso le istituzioni ,la cultura, la società, la scuola a far comprendere il grande miracolo dell’amore che non si coniuga in un’unica declinazione potremmo trasformare in un futuro non lontanissimo **la giornata mondiale contro l’omofobia** in una normale tranquilla ma sfavillante **giornata d’amore che  purtroppo non potrà mai realizzarsi senza il riconoscimento dei diritti fondamentali di tutti gli esseri umani.**

Mi permetta di chiudere Presidente non  senza prima averla ringraziata a nome di tutti ,con una piccola poesia tratta dall’archivio AGEDO del coming-aut. di cui porta il titolo

**Coming-aut**

**Vorrei poggiare la testa sulla tua spalla**

**mamma**

**senza sentire l’imbarazzo**

**della tua domanda.**

**Chi sono?**

**Non so sono tuo figlio**

**o forse una figlia**

**e per questo**

**vorrei che tu mi amassi**

**e nel tuo immenso**

**cuore  di conchiglia**

**vorrei che tu**

**potessi trasformare**

**il mio granello di**

**sabbia**

**in una splendida perla**

**e averla**

**incastonata nel tuo**

**occhio, la**

**sotto le ciglia.**

**dove prima era il pianto**

**ora il sorriso.**

**Dammi mamma la mano**

**Attraversiamo insieme**

**I dubbi e l’incertezza**

**La differenza sai è**

**solo una ricchezza.**

**RD**