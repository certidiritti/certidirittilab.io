---
title: 'DOPO VENEZIA E TRENTO ANCHE FIRENZE  ALLA CONSULTA PER MATRIMONIO GAY'
date: Wed, 16 Dec 2009 08:13:47 +0000
draft: false
tags: [Comunicati stampa]
---

**RICORSO PER NO ALLE NOZZE GAY: LA CORTE D’APPELLO DI FIRENZE RITIENE FONDATO  RICORSO DELLA COPPIA E RIMETTE ALLA CORTE COSTITUZIONALE LA DECISIONE.  
DOPO VENEZIA E TRENTO ANCHE FIRENZE RINVIA ALLA CORTE COSTITUZIONALE: SUCCESSO DELLA CAMPAGNA DI AFFERMAZIONE CIVILE.**

Roma, 16 dicembre 2009  
_**Comunicato Stampa dell’Associazione Radicale Certi Diritti:**_

“La Corte d’Appello di Firenze ha rimesso alla Corte Costituzionale la decisione sul merito del ricorso presentato da una coppia gay che aveva ricevuto un diniego, da parte del Comune di Firenze, alle pubblicazioni per il matrimonio.

Questa decisione dimostra che la Corte d’Appello di Firenze, come quelle di Venezia e di Trento, ritiene fondate le ragioni del ricorso, presentato quando il Comune si era opposto alle pubblicazioni per il loro matrimonio Questa decisione dimostra, se ancora ve ne fosse bisogno, che la campagna di Affermazione Civile, lanciata due anni fa dall’Associazione Radicale Cerit Diritti e da Avvocatura Lgbt – Rete Lenford, finalizzata  al riconoscimento del diritto al matrimonio civile per le coppie gay, è una strada molto importante per il superamento delle disuguaglianze e delle discriminazioni verso la persone omosessuali.

La classe politica si ostina a non voler riconoscere e regolamentare il matrimonio e  le unioni civili per questo abbiamo avviato la strada legale, così come accadde negli Stati Uniti contro la segregazione razziale.

Un altro passo avanti è stato compiuto per l’uguaglianza e i diritti delle persone lesbiche e gay.