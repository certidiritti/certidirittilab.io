---
title: 'Obama conferma impegno per i diritti Lgbt e si impegna per aiuto adolescenti gay vittime del bullsimo'
date: Sun, 02 Oct 2011 03:43:34 +0000
draft: false
tags: [Americhe]
---

L'intervento del Presidente degli Stati Uniti alla Conferenza annuale della Human Right Campaing sia da esempio alla nostra classe politica.

Comunicato Stampa dell'Associazione Radicale Certi Diritti

L'intervento del Presidente degli Stati Uniti alla Conferenza annuale della Human Right Campaing, una delle più importanti organizzazioni americane che si battono per la promozione e la difesa dei diritti Lgbt, in corso a Washington,  segna un passo importante nel cammino del superamento delle diseguaglianze negli Usa e nel resto del mondo. Il Presidente Usa ha testualmente detto che 'ogni americano, gay, bisessuale, e transgender, merita di essere uguale davanti alla legge e di poter accedere gli stessi diritti di tutti gli altri cittadini' ricordando di aver fatto abolire l'ipocrita e odioso principio del 'don't ask, don't tell'. che di fatto impediva ai militari di vivere dichiaratamente la propria omosessualità. Tra i temi del suo intervento è stato anche affrontato quello dei suicidi tra gli adolescenti gay, vittime del bullismo:

Barack Obama ha detto che "nessun ragazzo gay dovrà in futuro sentirsi solo e abbandonato a sè stesso". 

Noi dell'Associazione Radicale Certi Diritti ci auguriamo che la classe politica italiana impari qualcosa dal Presidente Usa e abbia il coraggio di tenere alta la bandiera dei diritti civili e umani, senza timori, paure, tentennamenti ipocriti e clericali. Rivendicare il superamento delle diseguaglianze e  lottare per  l'accesso di tutti i cittadini ai diritti, è uno dei pilastri  di ogni società democratica moderna e civile.

Al suo arrivo al centro congressi di Washington, un'enorme platea di almeno 3.000 persone gli ha tributato una fragorosa standing ovation. Il Presidente dell'organizzazione, Jo Solmonese, ha invitato tutti a sostenere Barack Obama, definendolo come 'il presidente che ha fatto piu' di tutti nella storia d'America per migliorare la vita della comunita' gay'.  
Dopo la battuta su Lady Gaga, paragonata a una sorta di capo di Stato, Obama ha ribadito che 'ogni americano, gay, bisessuale e transgender, merita di essere uguale davanti alla legge e godere degli stessi diritti di tutti gli altri cittadini'.  
Quindi, tra l'entusiasmo generale, ha ricordato che grazie ai suoi sforzi e' stato abrogato l'odioso principio del 'Don't ask, don't tell', che costringeva i militari gay a vivere nell'ombra, senza poter dichiarare apertamente la propria identita' sessuale.  
Infine ha parlato di un fenomeno che sta colpendo molto l'opinione pubblica americana, quello dei suicidi tra gli adolescenti gay, vittime di bullismo. 'Nessun ragazzo gay - ha esclamato Obama - dovra' in futuro sentirsi solo e abbandonato a se' stesso'. (ANSA).