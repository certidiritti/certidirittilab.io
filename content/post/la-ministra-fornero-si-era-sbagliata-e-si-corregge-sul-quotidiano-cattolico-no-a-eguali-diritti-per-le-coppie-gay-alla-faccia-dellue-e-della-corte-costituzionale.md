---
title: 'La ministra Fornero si era sbagliata e si corregge sul quotidiano cattolico: no a eguali diritti per le coppie gay, alla faccia dell''UE e della Corte costituzionale'
date: Mon, 21 May 2012 22:56:29 +0000
draft: false
tags: [Politica]
---

Sarà per questo che non vuole incontrare le associazioni lgbt? Dopo Bersani anche il ministro dimostra di non conoscere le sentenze della Corte costituzionale e della Corte di Cassazione.

Roma, 21 maggio 2012

comunicato stampa dell'associazione radicale Certi Diritti

Su Avvenire di ieri il Ministro Fornero ha precisato il suo pensiero in merito a matrimonio e unioni, sostenendo che:

"Non ho quindi auspicato che le unioni di fatto, sia etero sia omosessuali, siano equiparate alla famiglia fondata sul matrimonio tra un uomo e una donna, ma semplicemente invitato ad aprire gli occhi sulle diverse realtà che stanno emergendo e a non dimenticare, e meno che mai a discriminare, i diritti dei singoli individui che vi si riconoscono e che chiedono con forza un riconoscimento. Senza queste risposte, si acuirebbe un altro fattore di instabilità sociale, oltre a quelli di carattere economico resi più acuti dalla crisi in atto."

Il tutto condito da considerazioni sulla sua vita personale del tutto fuori luogo.

Incomprensibili, incoerenti e davvero strabiche come dichiarazioni, sia rispetto a quanto accade nel resto dell'Europa, sia rispetto agli sviluppi della stessa giurisprudenza costituzionale italiana che ben due anni fa, con una sentenza che evidentemente pochi hanno letto con attenzione, scioglie del tutto ogni tipo di riserva sule unioni tra persone dello stesso sesso ed il valore costituzionale che le stesse unioni hanno.

Il Ministro Fornero, e tutti coloro che la pensano come lei, devono spiegare agli italiani in base a quale cultura, principio o idea le persone omosessuali non possono accedere a una forma riconosciuta di matrimonio o di unione, NON sulla base dei loro diritti individuali (ci mancherebbe pure...) ma sulla base del valore che l'unione in sè ha per la società e non solo per gli individui che la compongono. E per quale motivo le unioni tra persone eterosessuali NON matrimoniali non debbano avere piena uguaglianza: ci dica, Signora Ministro, in uno stato laico chi è che decide il valore delle unioni tra persone? Il Vaticano o le persone stesse? E in base a quale criterio quelle celebrate con matrimonio sono "migliori" di quelle che non vogliono, o non possono, celebrarsi?