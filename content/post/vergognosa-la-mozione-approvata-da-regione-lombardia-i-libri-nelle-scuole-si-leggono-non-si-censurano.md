---
title: 'Vergognosa la mozione approvata da Regione Lombardia: i libri nelle scuole si leggono, non si censurano'
date: Wed, 07 Oct 2015 16:48:23 +0000
draft: false
tags: [Politica]
---

[![logo_reg_lombardia](http://www.certidiritti.org/wp-content/uploads/2015/10/logo_reg_lombardia.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/10/logo_reg_lombardia.jpg)Mentre il Papa auspica "una sempre più proficua attività al servizio delle giovani generazioni" alla casa editrice Lo Stampatello, la Regione Lombardia rispolvera metodi novecenteschi approvando una mozione che chiede che "vengano ritirati dalle scuole i libri e il materiale informativo che promuovono la teoria del gender". "La teoria del gender non esiste, mentre esiste ed è molto pericoloso il bullismo omo-transfobico che rende troppo spesso la vita impossibile a tante studentesse e tanti studenti delle scuole lombarde. È vergognoso che la Regione lombardia si proponga di mettere dei libri all'indice e tenti, di fatto, di impedire che le scuole educhino al rispetto delle differenze e all'inclusione" Così Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti che conclude: "i libri nelle scuole si leggono, non si censurano!"