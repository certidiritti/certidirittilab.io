---
title: 'PETIZIONE PER ABBASSARE IL COSTO DEI PRESERVATIVI'
date: Fri, 12 Mar 2010 14:59:13 +0000
draft: false
tags: [Comunicati stampa]
---

Lanciata la petizione per abbassare il costo dei preservativi. Il Comitato di Roma promosso da Sergio Rovasio, candidato tra i capilista della Lista Bonino-Pannella alla Regione Lazio.

> Fai clic qui per [sottoscrivere alla petizione](http://www.sergiorovasio.it/sottoscrivi-la-petizione-di-sergio-rovasio-sullabbassamento-del-costo-dei-profilattici.html)

Al Ministro per le Pari Opportunità;  
Al Ministro per la Salute;  
Al Ministro della Gioventù

Noi sottoscritti chiediamo che il costo dei preservativi venga abbassato tramite apposite contrattazioni con le case produttrici e/o con convenzioni per la distribuzione gratuita in scuole e locali notturni, accompagnate da specifiche campagne di informazione, prevenzione e aiuto per combattere le malattie sessualmente trasmissibili.