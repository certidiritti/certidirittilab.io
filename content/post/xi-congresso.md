---
title: 'XI Congresso dell''Associazione Radicale Certi Diritti'
date: Fri, 13 Oct 2017 12:12:21 +0000
draft: false
---

![Grafica XI Congresso, copertina (1)](http://www.certidiritti.org/wp-content/uploads/2017/10/Grafica-XI-Congresso-copertina-1.png) **E' convocato per il 17, 18 e 19 novembre 2017 l'XI Congresso dell'Associazione Radicale Certi Diritti**.

Come ogni anno l'assemblea sarà l'occasione per tirare le somme sull'anno trascorso, discutere insieme delle nostre battaglie e assolvere gli adempimenti statutari.

**Tutti possono partecipare al Congresso**, che quest'anno si terrà a Roma in [Via Angelo Bargoni, 40](https://www.google.it/maps/place/Sede+Radicali+ROMA/@41.8800485,12.4685765,15z/data=!4m5!3m4!1s0x0:0x3e9eb78e61c9eb12!8m2!3d41.8800485!4d12.4685765), ma ti ricordiamo che **per godere dei diritti di elettorato attivo e passivo devi essere in regola con la quota di iscrizione 2017**. Qualora volessi iscriverti puoi farlo anche adesso [qui ](http://www.certidiritti.org/iscriviti/)

Qui troverai man mano tutte le informazioni per la tre giorni congressuale e a preannunciare la tua partecipazione attraverso l'[evento Facebook](https://www.facebook.com/events/1487564717964345).

Ti ricordiamo che **tutto il nostro lavoro è volontario** e che vive solo delle vostre donazioni. A Roma, con non pochi sforzi economici e logistici, avremo ospiti dall'Italia e dall'estero che animeranno il nostro dibattito generale: **[dona ora](http://www.certidiritti.org/donazioni/) per convocare assieme a noi questa undicesima assemblea!**

**La libertà sessuale non è un’opinione - Ordine dei lavori**

**Venerdì**

17.00 Conferenza precongessuale su diritto di famiglia

   \- Melita **Cavallo**, già Presidente del Tribunale dei minori di Roma

   \- Filomena **Gallo**, segretario Associazione Luca Coscioni

   \- Marilena **Grassadonia**, presidente Famiglie Arcobaleno

   \- Angelo **Schillaci**, gruppo legale di Famiglie Arcobaleno

Modera Enzo Cucco

**Sabato**

9.30 Relazioni Tesoreria e Segreteria

11.00 Intersex, lo stato delle iniziative in Italia e in Europa

   - Kristian **Randjelovic**, XY Spectrum (Serbia)

   - Sabina **Zagari**, OII-Italia

   - Claudio **Uberti**, direttivo dell’Associazione Radicale Certi Diritti

   - Alessandro **Comeni**, Presidente onorario di Certi Diritti e co-fondatore di OII-Italia

12.00 Riforma Minniti-Orlando e le sue ripercussioni sui richiedenti asilo LGBTI e sulle categorie più vulnerabili

   - Avv. Giulia **Perin**

   - Giulia **Bodo**, Arcigay Rainbow Valsesia - Vercelli, coordinatrice del Progetto AfriArcigay

   - Oma **Balep**, rifugiato politico

   - Gabriella **Friso**, direttivo dell’Associazione Radicale Certi Diritti

13.00 Pausa pranzo

14.00 Transnazionale 

Ukraina

   \- Mauro **Voerzio**, editor di StopFake, le fake news sui temi LGBTI e i canali di diffusione in Italia della propaganda putiniana.    \- Massimiliano **Di Pasquale**, giornalista freelance, inquadramento storico politico sull'Ucraina    \- Olena **Shevchenko**, Insight NGO, Il movimento LGBTI in Ucraina a partire da Euromaidan.

Azerbaijan

   \- Javid **Nabiyev**, Nefes LGBT Azerbaijan Alliance

15.30 Prostituzione: una proposta per la XVIII legislatura 

   \- Giorgia **Serughetti**

   \- Attivista sex worker da confermare

   \- Pia **Covre**, presidente del Comitato per i Diritti Civili delle Prostitute

16.30 Prep, uno strumento in più

   \- Dott. Alessandro **Soria**, medico Infettivologo dell’Unità Operativa di Malattie Infettive dell'Ospedale San Gerardo di Monza    \- Dott. Massimo **Cernuschi**, medico Infettivologo del San Raffaele e presidente di ASA Milano onlus

17.30 Transessualità nell’età evolutiva

   \- Andrea Tiziano Di Francesco, presidente di Beyond differences

18.00 Dibattito Generale e votazione bilancio 2016-2017

21.00 Chiusura della prima giornata di lavori

**Domenica**

9.30 Dibattito generale (seguito)

12.30 Votazione cariche e documenti

14.30 Chiusura dei lavori

**Interverranno nel corso dei lavori:**

Emma Bonino, già ministra degli Esteri; Marco Cappato, tesoriere Associazione Luca Coscioni; Marco Gentili, copresidente Associazione Luca Coscioni; Riccardo Magi, segretario di Radicali Italiani; Marco Perduca, Associazione Luca Coscioni; Daniele Sandri Boriani, Cammini di Speranza; Sebastiano Secci, Circolo di Cultura Omosessuale Mario Mieli; Antonella Soldo, presidente di Radicali Italiani; Mina Welby, Associazione Luca Coscioni.