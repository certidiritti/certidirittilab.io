---
title: 'Milano: il Gender Manager non basta, Comune guardi tutte le diversità.'
date: Tue, 07 Nov 2017 15:14:25 +0000
draft: false
tags: [Politica]
---

![logo-milano-900x600](http://www.certidiritti.org/wp-content/uploads/2017/11/logo-milano-900x600-300x200.jpg)"La proposta della Consigliera Diana De Marchi di nominare una Gender Manager per Milano è apprezzabile, ma forse timida: noi Radicali siamo convinti che non si debba restringere l'attenzione alle sole donne, ma allargare l'orizzonte a politiche capaci di valorizzare tutte le diversità, siano esse di genere, di origine etnica, di religione o convinzioni personali, di abilità fisiche, di età, di orientamento sessuale o identità di genere. Per questo motivo rilanciamo con la proposta di un Diversity Manager per Milano per promuovere il rispetto e l’integrazione a 360 gradi." Lo dicono Barbara Bonvicini, segretario dell'Associazione Enzo Tortora - Radicali Milano e Claudio Uberti, membro del direttivo dell'Associazione Radicale Certi Diritti.