---
title: 'Buone notizie per il matrimonio egualitario: negli USA la corte suprema ha applicato il principio di non discriminazione. Italia sempre più isolata e arretrata anche nel dibattito della sua classe dirigente'
date: Wed, 26 Jun 2013 12:59:23 +0000
draft: false
tags: [Americhe]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti

Roma, 26 giugno 2013

  
Oggi la Corte suprema degli Stati Uniti, dopo mesi di meticolosa istruttoria e decine di sedute e audizioni, ha accolto la tesi di coloro che pensano che il matrimonio sia un istituto che non può essere precluso alle persone omosessuali. Violerebbero quindi il 5 emendamento le leggi che vietano l'estensione del matrimonio civile alle persone dello stesso sesso.  
Uno dopo l'altro, qundi, i massimi organi giurisdizionali dei paesi occidentali stanno facendo a pezzi al retorica di chi vuole il matrimonio patromonio esclusivo delle coppie eterosessuali.

Secondo l'Associazione radicale Certi Diritti una bella lezione per i conservatori, di destra e di sinistra, che continuano a negare l'evidenza, e con essa diritti e doveri delle coppie formate da persone dello stesso sesso.