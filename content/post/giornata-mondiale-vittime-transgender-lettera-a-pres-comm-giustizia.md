---
title: 'GIORNATA MONDIALE VITTIME TRANSGENDER: LETTERA A PRES. COMM GIUSTIZIA'
date: Wed, 19 Nov 2008 12:28:57 +0000
draft: false
tags: [Comunicati stampa]
---

**GIORNATA VITTIME TRANSGENDER: LETTERA DI BERNARDINI E CONCIA ALLA PRESIDENTE DELLA COMMISSIONE GIUSTIZIA DELLA CAMERA PER LA MESSA ALL'ODG DELLA LEGGE SUL CAMBIO NOME, COME IN SPAGNA E GRAN BRETAGNA.**

Roma, 19 novembre 2008

In occasione della giornata mondiale dedicata alla memoria delle vittime transgender e transessuali che si celebra domani, giovedì 20 novembre, in tutto il mondo, Rita Bernardini, deputata radicale - Pd e Paola Concia, deputata Pd, hanno inviato una lettera a Giulia Buongiorno, Presidente della Commissione Giustizia della Camera dei deputati, nella quale chiedono che venga messa all'odg dei lavori la proposta di legge n.1066. La proposta di legge prevede la possibilità che le persone transgender e transessuali possano cambiare il loro nome senza che obbligatoriamente venga fatto l'intervento chirurgico di cambiamento del sesso. Leggi simili sono state approvate in Spagna e Gran Bretagna e rispondono in modo adeguato ad uno dei molteplici problemi che le persone transgender e transessuali vivono in condizioni molto difficili.

Nella lettera viene ricordato come tra i gravi problemi che vivono le persone transgender e transessuali vi siano anche l'omofobia e la transfobia di cui sono spesso vittime in Italia, come documentato dal rapporto 2008 preparato dall'Ufficio diritti umani e lotta alla violenza dell'Arcigay, inviato insieme alla lettera, alla Presidente della Commissione Giustizia.