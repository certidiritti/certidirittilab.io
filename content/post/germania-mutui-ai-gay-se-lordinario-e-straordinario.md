---
title: 'GERMANIA: MUTUI AI GAY, SE L''ORDINARIO E'' STRAORDINARIO'
date: Thu, 05 Mar 2009 09:28:41 +0000
draft: false
tags: [Comunicati stampa]
---

*   _**EpolisRoma, a pagina 7**_   

Notizia di ieri: una banca tedesca, la Bhw Bausparkasse, anche nelle sue sedi in Italia concede senza problemi mutui per la casa a coppie composte da persone dello stesso sesso.

E, soprattutto, accetta nel calcolo di considerare la somma dei redditi. Chiede solo che la casa sia cointestata e che le persone convivano. A pensarci mi sarebbe sembrato estremamente strano, nel periodo in cui cercavo una banca sufficientemente avventurosa da concedermi un mutuo, se quell’istituto si fosse interessata dei miei gusti in fatto di bellezza muliebre. Se mi avessero chiesto se preferissi le bionde o le brune, gli occhi azzurri o quelli castani. Le banche si sono invece interessate sulla situazione finanziaria mia e della persona assieme alla quale chiedevo il mutuo. E anche al fatto che avremmo sommato i rispettivi redditi per un ragionevole tempo, e avessimo una forte motivazione a ripagare il debito (cioè che la casa fosse cointestata). Ecco direi che nel caso delle coppie gay che cercano casa la logica dovrebbe essere la stessa. Quindi lode alla banca tedesca, questa è una bellissima notizia. La cosa triste è che sia una notizia. Cioè che non sia totalmente ovvio che due persone, quali che siano, possano avere un mutuo. Se son disposte a impegnarsi insieme nell’impresa titanica dell’acquisto di una casa e del viverci insieme. E quindi, a tutti gli effetti, sono una famiglia.

 Fonte: www.epolisroma.it