---
title: 'Rimuovere le discriminazioni contro le famiglie non matrimoniali si può. Cominciamo dai congedi parentali'
date: Wed, 11 Apr 2012 10:18:02 +0000
draft: false
tags: [Diritto di Famiglia]
---

Lettera aperta dell’Associazione radicale Certi Diritti ai Ministri Riccardi e Fornero.

Roma, 11 aprile 2012

Il ministro Riccardi ha annunciato alcune proposte di emendamento al disegno di legge del Governo sul tema dei congedi parentali. Tenendo conto della disponibilità a modificare in parte il provvedimento chiediamo ai Ministri Fornero e Riccardi di cominciare a dare un segnale di concretezza rimuovendo le discriminazioni   adeguando le norme sul congedo parentale estendendole  alle famiglie non matrimoniali.

  
L’intervento che noi auspichiamo segue la logica della Legge regionale dell’Emilia Romagna del 2009 e della Deliberazione del Consiglio comunale di Torino n. 84 del 28 giugno 2010 che riconoscono alle famiglie anagrafiche unite da vincolo affettivo lo status di famiglia (che peraltro dal punto di vista anagrafico già hanno) ed estendono questa possibilità ad entrambi i genitori facenti parte del nucleo familiare.

Se la nostra proposta fosse accolta si otterrebbe l’uguaglianza di trattamento in questo specifico settore tra coppie matrimoniali e non, in linea con l’affermazione nella sentenza 138/10 della Corte Costituzionale e di quelle recenti 4184/12 della Corte di Cassazione e della sentenza di Reggio Emilia del 13.2.12 evitando che le coppie si debbano di nuovo rivolgere alla magistratura per poter affermare il diritto all’uguaglianza di trattamento.