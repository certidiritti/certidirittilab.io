---
title: '2008-0140-Discrimination-ST06628.EN10'
date: Wed, 17 Feb 2010 15:53:39 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 17 February 2010

Interinstitutional File:

2008/0140 (CNS)

6628/10

LIMITE

SOC 132

JAI 150

MI 57

  

  

  

  

  

NOTE

from :

The General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

6092/10 SOC 75 JAI 108 MI 39

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached a note from the Irish delegation with a view to the meeting of the Working Party on Social Questions on 18 February 2010.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**Note from the Irish delegation**

**Subject: Art. 3(1)(a). Scope**

In Ireland's view it would be more appropriate to deal with the principle of equal treatment on the four specified grounds in relation to access to social protection including social security in a separate directive, rather than as part of a broader measure applying to access to goods and services in both the public and private sectors.

Social welfare schemes (unlike, for example, the provision of goods and services on a professional or commercial basis) inherently involve differences in treatment between classes of persons. It may be necessary to make much more detailed provision to deal with the application of the principle of equal treatment on the grounds covered to social protection, including social security. This is so particularly in light of the fact that two of the grounds covered by the proposed directive (disability and age) are two of the principal contingencies covered by the social security system.

A separate directive relating to equal treatment in relation to social protection including social security would permit the more complex issues involved in combating discrimination in that sphere to be more fully addressed.  It would also be in line with EU measures in relation to equal treatment on grounds of sex, where such equal treatment in matters of social security is dealt with discretely by Directive 79/7/EEC.

**Subject: Finnish observations (Recital 17b)**

Notwithstanding Ireland's comments above, we wish to note that we are in agreement with the Finnish view that reference to Regulation 883/2004/EC and the last sentence of Recital 17b should be deleted from the text.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_