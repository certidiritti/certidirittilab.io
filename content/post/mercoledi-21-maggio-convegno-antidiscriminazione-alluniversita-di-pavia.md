---
title: 'MERCOLEDI'' 21 MAGGIO - CONVEGNO ANTIDISCRIMINAZIONE ALL''UNIVERSITA'' DI PAVIA'
date: Sun, 18 May 2008 19:12:27 +0000
draft: false
tags: [Affermazione Civile, certi diritti, Comunicati stampa, Francesco Bilotta, PUbblicazione degli atti, Università di Pavia]
---

**“Discriminati per legge? La tutela antidiscriminatoria in riferimento all’orientamento sessuale nel rapporto di lavoro”**. Se ne discute il 21 maggio prossimo, dalle 21, presso l’Aula Magna dell’Università degli Studi di Pavia per iniziativa del [gruppo Coming-aut](http://www.coming-aut.it) , un gruppo di giovani militanti LGBT pavesi. Introdurrà il quadro giuridico il dott. **Giuseppe Eduardo Polizzi**.  
  
Interverranno **l’on. Franco Grillini**, della costituente socialista e direttore di gaynews.it, le avvocatesse **Alessandra Rossari** e **Stefania Santilli** e **l’avvocato Francesco Bilotta**, socio fondatore della Rete Lenford, la rete italiana di avvocati e giuristi gay friendly, che riferirà sulla “via giudiziaria”, possibile alternativa alla via parlamentare per il riconoscimento dei diritti civili delle persone gay, lesbiche e transessuali.  
  
Bilotta informerà dell'iniziativa in corso, di Lenford, in collaborazione con l'associazione radicale Certi diritti, di [richiesta di coppie gay della pubblicazione degli atti di matrimonio in numerosi comuni italiani.](index.php?option=com_content&task=view&id=41&Itemid=72)  
  
Nel corso dell’incontro una donna, lesbica, madre e lavoratrice, testimonierà come, concretamente, l'orientamento sessuale, può essere d’ostacolo al riconoscimento di elementari diritti civili e ad una esistenza serena.  
  
_Autore: Giuseppe Polizzi  
Fonte: gaynews  
_