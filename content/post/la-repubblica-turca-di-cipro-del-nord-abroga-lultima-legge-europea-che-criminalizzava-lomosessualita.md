---
title: 'La Repubblica turca di Cipro del Nord abroga l''ultima legge europea che criminalizzava l''omosessualità'
date: Tue, 28 Jan 2014 13:43:11 +0000
draft: false
tags: [Europa]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 27 gennaio 2014

Oggi il Parlamento della Repubblica Turca di Cipro del Nord ha abrogato la norma del codice penale che puniva con 5 anni di carcere i rapporti omosessuali tra adulti consenzienti. La Repubblica Turca di Cipro del Nord era l'unico paese europeo che ancora criminalizzava l'omosessualità.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: "Nella giornata che commemora i crimini nazi-fascisti, anche contro gli omosessuali, non poteva arrivare notizia migliore per l'Europa tutta. Dopo 69 anni da quel 27 gennaio 1945 quando si aprirono i cancelli di Auschwitz a disvelare uno dei più atroci orrori del Novecento, l'Europa può finalmente vantarsi di essere un continente che non criminalizza più i suoi cittadini per il loro orientamento sessuale. Già nel 1981 la Corte Europea dei Diritti Umani aveva sentenziato, nello storico caso Dudgeon vs UK, che le leggi che vietavano i rapporti omosessuali tra adulti consenzienti dovevano essere abolite perché in contrasto con la Convenzione Europea sui Diritti Umani. Ci sono voluti altri 33 anni per liberare completamente il continente da queste leggi ingiuste e discriminatorie, ma oggi, proprio in occasione della giornata della memoria ce l'abbiamo fatta".