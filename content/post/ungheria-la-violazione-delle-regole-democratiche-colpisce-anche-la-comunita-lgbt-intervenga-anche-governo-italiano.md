---
title: 'Ungheria: la violazione delle regole democratiche colpisce anche la comunità Lgbt. Intervenga anche Governo italiano'
date: Sat, 21 Jan 2012 09:33:55 +0000
draft: false
tags: [Europa]
---

Con le riforme liberticide e antidemocratiche del governo di Viktor Orban a rischio i diritti fondamentali e delle persone lgbt. Appello alle autorità italiane con richiesta di intervento presso le autorità ungheresi e dell'Unione Europea.

Roma, 20 gennaio 2011  
   
Comunicato Stampa dell'Associazione Radicale Certi Diritti  
   
L'Associazione Radicale Certi Diritti esprime preoccupazione per la situazione dei diritti delle persone LGBT in Ungheria e per la progressiva erosione della democrazia, dei diritti umani, dello stato di diritto e dell'eguaglianza - ovvero i valori fondamentali che sono alla base del Trattato (art.2) dell'Unione europea.  
   
Il governo di Viktor Orban in Ungheria ha approvato nel giro di poco più di un anno una serie di riforme liberticide ed antidemocratiche, imponendo grazie alla maggioranza parlamentare dei 2/3, ottenuta con il 53 % dei voti, la riforma della Costituzione in senso populista, nazionalista e conservatore; una legge di controllo governativo dei media pubblici e privati, della banca centrale, del sistema giudiziario, dell'autorità garante sulla privacy; una legge discriminatoria sulle religioni; la criminalizzazione delle persone senza casa e la definizione di famiglia discriminatoria come "basata sul matrimonio di un uomo con una donna", escludendo cosi non solo le coppie dello stesso sesso, ma anche le famiglie monoparentali dalla Carta costituzionale.  
   
La Commissione europea ha dato alle autorità ungheresi un mese per cambiare alcune di queste leggi (giustizia, autorità privacy e banca centrale), ma le ONG, le organizzazioni internazionali (ONU, Parlamento europeo, Consiglio d'Europa, OSCE) hanno espresso gravi preoccupazione anche sulle altre riforme introdotte dal governo Orban. In particolare il gruppo liberale al parlamento europeo ha chiesto l'attivazione della procedura di monitoraggio della situazione in Ungheria prevista dall'articolo 7.1 del Trattato dell'Unione Europea, che permette al Consiglio di mettere sotto osservazione uno degli Stati membri quando vìola i suoi stessi parametri democratici.  
   
L'Associazione Radicale Certi Diritti ha rivolto oggi un appello alle autorità italiane perché esprimano la loro preoccupazione alle autorità ungheresi e al Consiglio dell'Unione Europea, richiamando Viktor Orban a rispettare i principi europei di eguaglianza, diritti fondamentali, democrazia e stato di diritto, come previsto dal Trattato UE agli articoli 2 e 7.

Nei giorni scorsi l'Associazione Radicale Certi Diritti aveva promosso una manifestazione davanti alla sede consolare ungherese di Milano.