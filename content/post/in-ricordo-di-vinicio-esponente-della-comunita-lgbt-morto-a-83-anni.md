---
title: 'IN RICORDO DI VINICIO, ESPONENTE DELLA COMUNITA'' LGBT MORTO A 83 ANNI'
date: Tue, 29 Dec 2009 08:04:48 +0000
draft: false
tags: [Comunicati stampa]
---

Ciao Vinicio!

Salutiamo Vinicio Diamanti, artista, compagno di lotte e di sogni ancora oggi irrealizzati. Si è spento ieri,  mercoledì 23 dicembre 2009, all'età di 83 anni, presso la Casa di Cura S. Antonio. Le esequie, si terranno domani a Roma. Lo ricordiamo con profondo affetto e stima.  Da sempre ha vissuto nel quartiere Prati di Roma dove era nato da genirori entrambi romani.

A 18 anni, come più volte ci ricordava, venne riformato al militare, "poichè non abbastanza uomo". A venti anni iniziò a studiare canto e a frequentare la facoltà di Medicina che presto abbandonò. In quegli anni conobbe Mario, il suo grande amore.  Una storia sentimentale che durerà 15 anni e si concluderà con la morte dell'amante.

Vinicio, come raccontò in un'intervista rilasciata ad Andrea Pini ("Pride - Mensile gay italiano" novembre 2007) si scopre da subito una persona molto effeminata. Iniziò così a fare teatro rendendo questa sua caratteristica un punto di forza dei suoi personaggi. Conosce e apprezza altri omosessuali che interpretano ruoli da travestito (la Cordero, George O'Brian, Dominot, Giò Staiano) ed affina, nel corso degli anni e grazie a tanta "gavetta", la sua professione di attore.

Inizia dall'avanspettacolo, negli anni '50 facendo ruoli en travestì: canta, balla, fa la soubrette, all'Ambra Jovinelli, al Principe di via Cola Di Rienzo, all'Oriente, all'Espero.

Negli anni '60 è il momento del suo debutto al cinema accanto ad artisti come Enrico Maria Salerno, Giancarlo Cobelli, Franco Enriquez, Manuela Kusterman, Giancarlo Nanni, Memè Perlini, e Pippo Di Marca (con il quale reciterà tutto Genet). Nemerose le tournè che lo vedranno sui palchi di tutta Italia.

Negli anni '70 sperimenta il teatro d'avanguardia e con Memè Perlini al teatro La Piramide appare nudo in scena in Eliogabalo ("Quanto freddo che ho patito!"). Lavora anche con la Moriconi e con Gassman.

Sebbene abbia partecipato a diversi film. Da ricordare "Il Vizietto" di Edouard Molinaro, del 1978, con Ugo Tognazzi e Michel Serrault e "Delitto al Blue Gay del 1984 di Bruno Corbucci, con Tomas Milian, Bombolo ed Emilia Di Nardo. Vinicio ha sempre preferito il teatro al cinema. Fellini lo avrebbe voluto per La Dolce Vita, ma Diamanti è costretto a rifiutare: troppo allettanti i mesi da passare in tournèe rispetto ai pochi giorni di presenza su un set. Con Vinicio ci lascia un pezzo di storia "en travestì" del teatro e del cinema italiano ma ci lascia anche un pezzo di movimento GLBTQ. Sempre in prima fila, infatti, fino agli ultimi Gay Pride. Ciao Vinicio!

Mauro Cioffari (GayRoma.it)