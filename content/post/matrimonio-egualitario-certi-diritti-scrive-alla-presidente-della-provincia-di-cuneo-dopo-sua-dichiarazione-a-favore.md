---
title: 'Matrimonio egualitario, Certi Diritti scrive alla presidente della provincia di Cuneo dopo sua dichiarazione a favore'
date: Thu, 15 Nov 2012 18:14:24 +0000
draft: false
tags: [Matrimonio egualitario]
---

“Con l'approvazione delle nozze gay la Francia da' una lezione di civiltà finalmente all'altezza della sua migliore storia fatta di lumieres e di droits de l'homme” aveva detto la presidente leghista Gianna Gancia.

Alla Presidente  
della Provincia di Cuneo  
Dott.ssa Gianna Gancia

CUNEO

  
Roma, 12 novembre 2012

  
Gentile Presidente,

nella giornata di ieri è circolata su facebook un lancio ANSA che riportava questa sua dichiarazione, virgolettata:

“Con l'approvazione delle nozze gay la Francia da' una lezione di civiltà finalmente all'altezza della sua migliore storia fatta di lumieres e di droits de l'homme”. E ancora: “Una conquista civile che cade nello stesso giorno in cui i cittadini del Maine e del Maryland si esprimono nella medesima direzione attraverso un referendum: le lumieres illuminano entrambe le sponde dell'Atlantico. Ben vengano i diritti, va da sé insieme ai doveri”.

Abbiamo cercato conferma di questa sua dichiarazione sul web ma non siamo stati fortunati. Lei ce la può confermare? Una dichiarazione di tale natura, infatti, ci sembra di grande rilievo, per molti motivi che sarebbe lungo elencare, e forse anche inutile.  
Come forse sa la nostra Associazione è da sempre favorevole ad applicare il principio di eguaglianza fino in fondo, anche per le coppie dello stesso sesso. E pensiamo che il nostro Paese debba aprire gli occhi alla realtà che viviamo e riconoscere che le forme familiari si sono moltiplicate ed hanno tutte bisogno di riconoscimento e protezione.

In attesa di ricevere conferma della sua dichiarazione le inviamo le migliori congratulazioni, dichiarandole la nostra disponibilità ad incontrarla (pubblicamente o privatamente) per eventualmente affrontare i tanti modi in cui la Provincia di Cuneo potrebbe favorire quelle “conquiste civili” di cui il nostro Paese ha bisogno.

Con i migliori saluti,  
Yuri Guaiana  
Segretario  
Associazione radicale Certi Diritti