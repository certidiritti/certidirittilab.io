---
title: 'Comune di Roma taglia del 75% i fondi ai malati di Aids'
date: Thu, 24 Mar 2011 13:16:00 +0000
draft: false
tags: [Salute sessuale]
---

Il comune di Roma taglia del 75% i fondi per l’assistenza ai malati di Aids, ma aumenta il numero di assessori e consiglieri comunali. Non c'è limite alla vergogna della casta (s)partitocratica.  
  
Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti  
  
Mentre il Comune di Roma nel suo bilancio di previsione 2011 taglia del 75% i fondi destinati all’assistenza dei malati di Hiv, da 2 milioni di Euro a 500.000 Euro, il Governo, con un blitz, fa approvare dal Consiglio dei Ministri una norma che prevede un aumento del numero degli Assessori e dei Consiglieri Comunali di Roma. Insomma, l’ennesima vergogna spartitocratica con un ulteriore aumento dei costi della casta della città di Roma che già oggi, tra Giunta, Consiglio e consulenze mette in bilancio 82 milioni di Euro, il doppio rispetto alla città di Milano.  
  
Lo stesso provvedimento era stato inserito, e poi tolto, per la contrarietà di alcuni Ministri, nel Decreto Milleproroghe e ora re-inserito nel decreto per il finanziamento del Fondo per lo Spettacolo. Davvero uno spettacolo pietoso, della peggiore spartizione partitocratica.  
  
Ciò che impressiona è la totale indifferenza di parte della classe politica ai bisogni e alle necessità delle persone più deboli. La città di Roma ha circa 500 malati di Aids che necessitano di cure, quasi tutte domiciliari. Con i tagli previsti i malati saranno letteralmente abbandonati a loro stessi. Inoltre, è bene ricordarlo, la città di Roma ha il numero più alto di contagi di Hiv -circa 600 all’anno- e di altre malattie sessualmente trasmissibili, senza che vi sia nessuna campagna di informazione, prevenzione e/o promozione dell’uso del preservativo nelle scuole e nei luoghi di socialità.