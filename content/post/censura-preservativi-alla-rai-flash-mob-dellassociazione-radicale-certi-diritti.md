---
title: 'Censura preservativi alla RAI: a Milano flash mob dell''associazione radicale Certi Diritti'
date: Fri, 02 Dec 2011 09:09:49 +0000
draft: false
tags: [Salute sessuale]
---

L’Associazione Radicale Certi Diritti che oggi inizierà il suo congresso a Milano ([http://www.certidiritti.it/v-congresso-dellassociazione-radicale-certi-diritti-a-milano](v-congresso-dellassociazione-radicale-certi-diritti-a-milano)), promuove insieme a Radicali Milano per le 14:30 un’azione dimostrativa (Flash Mob preservativo contro l’Aids) davanti alla sede Rai di Milano in corso Sempione 27.

Ci appelliamo a tutti i cittadini e alle associazioni LGBT affinché siano presenti per protestare contro questa censura talebana.