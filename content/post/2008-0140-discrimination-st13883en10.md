---
title: '2008-0140-Discrimination-ST13883.EN10'
date: Sun, 21 Nov 2010 13:05:36 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 21 September 2010

Interinstitutional File:

2008/0140 (CNS)

13883/10

LIMITE

SOC 561

JAI 759

MI 317

  

  

  

  

  

NOTE

from :

The Presidency

to :

The Working Party on Social Questions

on :

19 October 2010

No. prev. doc. :

12107/10 SOC 456 JAI 623 MI 242

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

In preparation of the next meeting of the Working Party, which is provisionally scheduled for 19 October 2010, delegations will find attached a Presidency questionnaire that focuses on the provisions concerning housing.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**Questions for SQWP debate on housing**

**1\.** **Situation in Member States**

Both the Racial Equality Directive (2000/43/EC) and the Gender Goods and Services Equality Directive (2004/113/EC) cover housing available to the public in their scope. Several MS have gone beyond these two grounds in their protection against discrimination.

a) To what extent does your national legislation against discrimination apply to housing? b) Do you distinguish (for this purpose) between private and public/social housing?

c) Do you have statistics on the number of cases of discrimination in the area of housing, and if so, what is the most prevalent ground?

**2\.** **Social housing**

a) Do you consider social housing to be a service (for the purposes of EU law)?

b) If not, do you consider it to be a part of the social protection system?

**3\.** **Private life**

In some cases, the principle of equality might be in conflict with the right to private life, in particular in the area of housing (e.g. renting out a room in your own apartment).

a) Is this better taken into account by an exclusion of transactions in the area of private life from the scope of the Directive, or by an exclusion of activities which are not commercial or professional?

b) How is commercial/professional housing defined in your national legislation?

**4\.** **Disability - reasonable accommodation**

a) In your view, what should the obligation to provide reasonable accommodation mean for providers of housing?

b) Is there legislation on the accessibility of buildings (private as well as public/social) in your country?

**5\.** **Disability - accessibility**

a) What should the obligation to provide for accessibility measures encompass in the area of housing?

b) Should there be a difference between old and new buildings?

c) Do you support immediate applicability with respect to new buildings?

d) Should there be a difference between the obligations for private and public/social housing respectively?

e)  Are there public funds available in your country for the adaptation of housing (private as well as public/social)?

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_