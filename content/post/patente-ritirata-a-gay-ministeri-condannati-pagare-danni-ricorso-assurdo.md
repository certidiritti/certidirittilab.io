---
title: 'PATENTE RITIRATA A GAY: MINISTERI CONDANNATI PAGARE DANNI, RICORSO ASSURDO'
date: Sun, 12 Oct 2008 09:30:12 +0000
draft: false
tags: [Comunicati stampa]
---

**PATENTE RITIRATA A CITTADINO PERCHE' GAY: IL RICORSO DEI DUE MINSITERI CONDANNATI A PAGARE 100.000 EURO PER DANNI DIMOSTRA LA CULTURA TALEBANA E RETROGRADA IMPERANTE, ALTRO CHE STATO MODERNO E CIVILE.**

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

“La decisione dei Ministeri della Difesa e dei Trasporti di ricorrere tramite l’Avvocatura dello Stato contro la sentenza di primo grado, che li aveva condannati a risarcire i danni a un cittadino al quale era stata ritirata la patente perché gay, dimostra che il nostro è un paese retrogrado, ispirato ad una concezione politica talebana. Difatti, anziché riconoscere il grave errore causato, si tenta di legittimare un’azione che non ha eguali in Europa, che mette l’Italia allo stesso livello dei paesi teocratici e omofobi. Così non si tutelano i cittadini e gli interessi di uno Stato democratico e civile”.