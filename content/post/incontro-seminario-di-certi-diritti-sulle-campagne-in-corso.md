---
title: 'Incontro-seminario di Certi Diritti sulle campagne in corso'
date: Thu, 14 Jul 2011 18:00:00 +0000
draft: false
tags: [Politica]
---

**Da vanerdì 15 a domenica 17 luglio, Certi Diritti terrà un seminario con i membri del direttivo ed esperti legali per organizzare le prossime campagne contro la sessuofobia e per il superamento delle diseguaglianze.**

Roma, 15 luglio 2011

Da venerdì 15 a domenica 17 luglio 2011 l’Associazione Radicale Certi Diritti terrà a Dorio (Lecco) un Incontro-Seminario con il Segretario, il Tesoriere e i  membri del Direttivo alla presenza di esperti legali. I temi che verranno affontati riguarderanno la vita dell’Associazione,  la campagna di Affermazione Civile della via giudiziaria, che vede coinvolte in Italia e all’estero diverse decine di coppie gay conviventi per farsi riconoscere i propri diritti. Verranno anche discussi i ricorsi presentati (e da presentare) in Italia,  alla Corte Europea dei Diritti dell’Uomo e alla Corte di Giustizia europea.

Tra gli altri, verranno analizzati i temi che vedono impegnata l’Associazione sui diversi fronti della lotta alla sessuofobia, dal superamento delle diseguaglianze per le persone lesbiche, gay e bisessuali all’impegno per il superamento delle discriminazioni di cui sono vittime le persone transesuali. Altri temi riguarderanno la Riforma del Diritto di Famiglia, proposta depositata nei mesi scorsi in Parlamento dai Parlamentari Radicali e le possibili iniziative comuni da promuovere con altre realtà politiche e associative nel paese.

Verranno anche discussi i temi legati alla campagna per la legalizzazione della prostituzione e per il superamento di leggi proibizioniste inutili, costose e criminogene sempre più diffuse  in Italia.