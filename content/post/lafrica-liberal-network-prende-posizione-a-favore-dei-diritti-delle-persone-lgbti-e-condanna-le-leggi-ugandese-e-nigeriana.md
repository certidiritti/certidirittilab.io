---
title: 'L''Africa Liberal Network prende posizione a favore dei diritti delle persone LGBTI e condanna le leggi ugandese e nigeriana'
date: Tue, 28 Jan 2014 13:53:32 +0000
draft: false
tags: [Africa]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 28 gennaio 2014

L'Africa Liberal Network, il coordinamento di 35 partiti liberali operanti in 24 paesi africani, ha preso una posizione storica a favore dei diritti delle persone LGBTI e contro le leggi omofone approvate in Uganda e Nigeria.

Nella dichiarazione rilasciata ieri si legge che «la discriminazione propagata dalle proposte di legge nigeriana e ugandese sono non-africane ed eticamente indifendibili». L'ALN difende «la liberà e il diritto di ciascuno di vivere la vita che preferiscono senza timore di persecuzioni. Questo diritto si estende a tutti, a prescindere dal genere, l'etnia o l'orientamento sessuale e a prescindere dalla volontà della maggioranza».

Yuri Guaiana segretario dell'Associazione Radicale Certi Diritti, dichiara: «questa presa di posizione dei partiti liberali africani, per nulla scontata, è di grandissima importanza anche per la forza delle parole scelte. Ringrazio Olivier Kamitatu Etsu, presidente di ALN, per il coraggio dimostrato nel fare questa dichiarazione in un continente dove la maggioranza degli elettori, probabilmente, non è d'accordo e criticherà fortemente questa presa di posizione. Il fatto che in Africa ci siano questi anticorpi autoctoni all'omofobia dilagante dà speranza e ci impone di sostenerli e rafforzarli in ogni modo, ne va della salute di un intero continente e, letteralmente purtroppo, della vita di milioni di persone LGBTI».

**[IL DOCUMENTO DI _AFRICA LIBERAL NETWORK_](notizie/comunicati-stampa/item/download/43_a26c4fef07bcbfbc719f49aaff783675)**

  
[ALN\_Statement\_on\_LGBTI\_rights\_in\_Africa.pdf](http://www.certidiritti.org/wp-content/uploads/2014/01/ALN_Statement_on_LGBTI_rights_in_Africa.pdf)