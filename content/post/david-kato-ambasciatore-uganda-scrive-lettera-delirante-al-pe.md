---
title: 'David Kato: ambasciatore Uganda scrive lettera delirante al PE'
date: Thu, 21 Apr 2011 14:17:30 +0000
draft: false
tags: [Africa]
---

L'attivista ugandese si sarebbe reso corresponsabile della sua morte. Le associazioni lgbte lanciano una raccolta fondi per sostenere un processo equo e veritiero.  
  
Comunicato Stampa di Non c’è Pace Senza Giustizia, Arcigay e Associazione Radicale Certi Diritti  
  
Bruxelles – Roma, 18 aprile 2011  
  
Sull’omicidio di David Kato Kisule, avvenuto nella sua abitazione, vicino a Kampala, il 26 gennaio scorso, l’Ambasciatore dell’Uganda presso la Commissione Europea, ha scritto una assurda lettera al Presidente del Parlamento Europeo che di fatto esprime giudizi gravi, frutto più di pregiudizio e ignoranza anziché inchieste e indagini processuali.  
  
Nella lettera al Presidente Jerzy Buzek, l’Ambasciatore ugandese Stephen T. K. Katenta-Apuli dice di scrivere  “per correggere l’impressione che può aver avuto Lei assieme a  vari membri del Parlamento Europeo riguardo al fatto che l'omicidio di David Kato sia stato il risultato della sua campagna a favore dei diritti dei gay e delle lesbiche in Uganda. Ciò non può essere più lontano dalla verità”.   
  
Nella lettera l’Ambasciatore descrive una situazione imbarazzante nella quale David Kato si sarebbe venuto a trovare per non aver pagato le prestazioni sessuali di un detenuto che lui stesso era riuscito a far uscire dal carcere poco prima ed è per questo che si è reso lui stesso corresponabile della sua morte.  
L’Ambasciatore, non contento di aver espresso alcuni giudizi sulla differenza tra prostitute e  
prostituti, aggiunge poi che il Governo dell’Uganda ha arrestato il colpevole e che sarà processato e potrebbe anche essere condannato a morte al patibolo.  
  
Non c'è Pace Senza Giustizia, Arcigay e  Certi Diritti esprimono preoccupazione e condanna per il comportamento che le autorità ugandesi hanno sin qui manifestato sulla vicenda. Gli stessi amici e compagni di Sexual Minorities Uganda, e il loro avvocato, che sta coordinando le iniziative in vista del processo,  hanno ripetutamente richiesto di fare luce su questa drammatica vicenda che ha ancora molti lati oscuri e sui cui le autorità ugandesi non hanno fatto adeguate indagini. **[E’ anche per questo che è stato lanciata in Italia una campagna con l'obiettivo di  raccogliere  fondi per sostenere le spese in vista del processo che a breve si terrà a Kampala.](campagne/in-memoria-di-david-kato/1071-fondo-in-memoria-di-david-kato-kisule.html)**  
  
Il giudizio – sentenza espresso dall’Ambasciatore ugandese  Katenta nella lettera, spiega bene il fatto che le autorità ugandesi non intendono fare alcuno sforzo per arrivare alla verità e cercano di addossare la responsabilità ad una persona che certamente non era sola quando David fu ucciso, ipotizzando per lui addirittura la pena di morte;  è bene ricordare che il giorno dell’omocidio di David Kato Kisule furono proprio i vicini di casa di David a chiamare la polizia perché videro un furgone parcheggiato fuori dalla sua abitazione con uno strano movimento di persone che li mise in allarme.