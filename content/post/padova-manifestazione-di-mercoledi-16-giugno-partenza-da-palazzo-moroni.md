---
title: 'PADOVA: MANIFESTAZIONE DI MERCOLEDÌ 16 GIUGNO PARTENZA DA PALAZZO MORONI'
date: Mon, 14 Jun 2010 15:56:59 +0000
draft: false
tags: [Comunicati stampa]
---

**PADOVA: DUE RAGAZZI AGGREDITI PERCHE' “GAY E COMUNISTI”. CERTI DIRITTI ADERISCE ALLA MANIFESTAZIONE CONTRO L’OMOFOBIA PREVISTA PER MERCOLEDI' 16 GIUGNO.**

Una coppia di giovani, Matteo ed Enrico, è stata aggredita lo scorso mercoledì notte a Padova davanti ad un locale dopo essere stata ricoperta di insulti, dovuti non solo alle loro inclinazioni sessuali e affettive ma anche al loro abbigliamento e alle loro presunte idee politiche.

Una coppia di giovani, Matteo ed Enrico, è stata aggredita lo scorso mercoledì notte a Padova davanti ad un locale dopo essere stata ricoperta di insulti, dovuti non solo alle loro inclinazioni sessuali e affettive ma anche al loro abbigliamento e alle loro presunte idee politiche. L'aggressore dei due ragazzi, un 31enne in preda all'alcool – un ex militante dell'estrema destra padovana noto alla polizia per precedenti nell'ambito del tifo violento - è stato denunciato dalla Digos.

Si aggiunge così, peraltro in una città solitamente aperta ed accogliente come Padova, un altro triste tassello ad un'escalation di violenze ed aggressioni che vedono come vittime ragazzi omosessuali, e che sembra ormai inarrestabile.

L'Associazione Radicale Certi Diritti aderisce alla manifestazione di solidarietà ("in movimento contro pregiudizi, discriminazioni e violenza") indetta per mercoledì 16 giugno 2010 alle ore 19, e che si svolgerà nel capoluogo lungo le strade del centro, partendo da Palazzo Moroni (sede del Comune).