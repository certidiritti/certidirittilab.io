---
title: 'DELEGAZIONE CERTI DIRITTI IN VISITA ALLE TRANSESSUALI CARCERE DI REBIBBIA'
date: Sat, 20 Mar 2010 08:58:12 +0000
draft: false
tags: [Comunicati stampa]
---

**CARCERE DI REBIBBIA: DOMENICA 21 MARZO DELEGAZIONE RADICALE IN VISITA AL CARCERE. PREVISTA LA VISITA ANCHE AL REPARTO RISERVATO ALLE PERSONE TRANSESSUALI.**

Domenica 21 marzo, alle ore 11,30, una delegazione radicale, composta da Rita Bernardini, deputata radicale – Pd, Sergio Rovasio, Segretario Associazione Radicale Certi Diritti e candidato tra i capilista della Lista Bonino-Pannella alla Regione Lazio, Giacomo Cellottini, membro del Direttivo dell’Associazione Radicale Certi Diritti, farà una visita ispettiva nel carcere di Rebibbia.

La delegazione radicale visiterà, tra gli altri, il reparto riservato alle persone transessuali.