---
title: 'NEGARE P.ZA SAN GIOVANNI PER GAYPRIDE DI ROMA E'' GESTO CLERICO-FASCISTA'
date: Mon, 04 May 2009 14:25:15 +0000
draft: false
tags: [Comunicati stampa]
---

NEGARE PIAZZA SAN GIOVANNI PER IL GAY PRIDE E’ GESTO AUTORITARIO GRAVISSIMO. SI VUOLE RIPORTARE LA CITTA’ DI ROMA A PRIMA DEL 1870. SOLIDARIETA’ AL MARIO MIELI, SOSTERREMO OGNI OPPOSIZIONE A TALE DECISIONE.

Comunicato Stampa dell' Associazione Radicale Certi Diritti:

“La gravissima decisione di non concedere Piazza San Giovanni per il Gay Pride di Roma del prossimo 13 giugno, richiesta dal Circolo Mario Mieli di Roma, a nome di oltre 30 associazioni lgbt italiane, è un fatto di gravità inaudita. Si tenta di riportare la città di Roma a prima del XX Settembre 1870 dimenticando che Roma è la capitale di uno Stato che dovrebbe essere laico e democratico.

Esprimiamo al Circolo Mario Mieli tutta la nostra solidarietà e vicinanza per questo gesto politico che rasenta la peggior cultura politica clerico-fascista. Questa sera il coordinamento di Roma dell’Associazione Radicale Certi Diritti si riunirà per decidere tutte le azioni di sostegno contro tale decisione. Già lo scorso anno fu organizzato un sit-in di fronte alla basilica di San Giovanni perché fu negata la piazza per una cerimonia clericale che si svolse da un’altra parte dell’area, alle 21 di sera, molto oltre la fine del gay pride. Sosterremo in ogni sede le iniziative che verranno prese contro tale grave decisione”.