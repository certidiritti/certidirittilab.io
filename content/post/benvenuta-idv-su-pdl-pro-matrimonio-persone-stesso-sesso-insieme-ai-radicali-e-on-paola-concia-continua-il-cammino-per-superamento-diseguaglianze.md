---
title: 'Benvenuta Idv su pdl pro matrimonio persone stesso sesso. Insieme ai Radicali e On. Paola Concia continua il cammino per superamento diseguaglianze'
date: Fri, 06 Jul 2012 12:09:57 +0000
draft: false
tags: [Matrimonio egualitario]
---

La campagna di Affermazione Civile continua per coppie conviventi e sposate all'estero.

Roma, 6 luglio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Diamo il benvenuto a Idv per aver oggi depositato una Proposta di Legge per estendere il matrimonio civile alle coppie dello stesso sesso che si aggiunge a quelle dei Radicali e dell’On. Paola Concia del Pd. Sappiamo bene che l’Italia, seppur molto lentamente, sta andando nella stessa direzione di altri paesi europei dove il riconoscimento del matrimonio civile tra persone dello stesso sesso è già divenuto un traguardo:  Paesi Bassi, Belgio, Spagna, Norvegia, Svezia, Portogallo, Islanda e Danimarca.

I Radicali sin dalla scorsa Legislatura hanno depositato diverse proposte e disegni di legge, tra gli altri quello sulla Riforma del Diritto di Famiglia che include anche il matrimonio tra persone dello stesso sesso. La proposta riguarda i diversi temi che necessitano di una vera e propria riforma generale sempre più urgente: dal divorzio breve alla regolamentazione delle coppie di fatto, dalle norme sulla procreazione assistita alla lotta alla violenza sulle donne in famiglia.

Nonostante il matrimonio tra persone dello stesso sesso non sia mai oggetto di adeguata informazione, in Italia  quasi il 50% degli italiani è favorevole. Se vi fosse una vera e adeguata informazione, anziché denigrazione e offesa da parte dei fondamentalisti religiosi e di alcuni politici clerical-partitocratici, si raggiungerebbe la stragrande maggioranza di persone favorevoli come sta avvenendo negli Usa dopo che il Presidente Obama si è  impegnato per questa causa.  
  
Intanto l’Associazione Radicale Certi Diritti continua la sua campagna di Affermazione Civile che ha l’obiettivo di sostenere legalmente le iniziative legali contro l’Italia per il mancato rispetto delle Direttive e Risoluzioni europee sul tema del riconoscimento dei diritti delle coppie gay conviventi e/o sposate all’estero.  
  
**[Al seguente link tutte le proposte dei Parlamentari Radicali riguardanti le unioni di fatto, matrimonio tra persone dello stesso sesso e Riforma del Diritto di Famiglia >>](http://www.certidiritti.org/2012/03/18/diritto-di-famiglia-matrimonio-unioni-civili-e-adozioni-tutte-le-proposte-radicali-in-parlamento/)**