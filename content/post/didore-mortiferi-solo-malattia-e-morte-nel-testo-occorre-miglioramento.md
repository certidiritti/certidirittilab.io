---
title: 'DIDORE'' MORTIFERI, SOLO MALATTIA E MORTE NEL TESTO, OCCORRE MIGLIORAMENTO'
date: Wed, 15 Apr 2009 10:48:31 +0000
draft: false
tags: [Comunicati stampa]
---

UNIONI CIVILI:  DIDORE’ NECESSITANO DI MIGLIORAMENTI, LA REGOLAMENTAZIOINE  SI APPLICA SOLO SU CASI DI MALATTIA E MORTE. LE COPPIE SONO ANCHE ALTRO.  
   
Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:  “Sulla possibilità che i Didorè possano essere approvati entro l’anno vi sono due notizie, una buona e un’altra pessima. La buona notizia è che anche esponenti della Pdl si rendono conto che esistono in Italia quasi un milione di coppie conviventi e che è sempre più urgente regolamentare alcuni aspetti della loro vita; la pessima notizia è che i Didorè sembrano più che altro una elemosina centellinata per regolamentare esclusivamente situazioni drammatiche come la malattia o la morte del partner. La vita di coppia è anche altro e sorprende che nel testo firmato da oltre 50 parlamentari non vengano regolamentati anche altri aspetti della vita. C’è da augurarsi che i parlamentari migliorino questa proposta di legge per rispondere in modo più adeguato alle richieste e ai solleciti che giungono da una parte della società e sempre più dall’Europa. Sempre che il Vaticano sia d’accordo però”.