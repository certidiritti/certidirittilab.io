---
title: 'ROMA, 28 APRILE 2010 - TAVOLA ROTONDA: "IL MOVIMENTO GLBTQI E LA SOCIETÀ"'
date: Tue, 27 Apr 2010 09:52:26 +0000
draft: false
tags: [Comunicati stampa]
---

Carissimi tutti,

noi, donne e uomini di "Nuova Proposta, donne e uomini omosessuali cristiani" abbiamo il piacere di invitare tutti voi, in rappresentanza delle varie associazioni glbtqi, alla tavola rotonda che abbiamo organizzato all'interno del programma 2009/2010, centrato sulla fecondità della persona omosessuale.

L'abbiamo intesa come un momento di confronto per capire come e se è posibile essere maggiormente fecondi, all'interno del movimento, per promuovere con maggior efficacia le istanze e le richieste di diritti per la comunità glbtqi, e favorire l'informazione.

Capirete come, per esigenze di tempo, non stato possibile estendere (benché sarebbe stato estremamente interessante) la lista dei relatori a tutte le associazioni, ma ciò non toglie che sia auspicabile una presenza variegata con possibilità di intervenire dalla platea con domande e stimoli, sempre con l'obiettivo di avere un confronto sereno e costruttivo.

Vista la rilevanza dell'appuntamento, vi preghiamo di aiutarci a diffondere la notizia e di partecipare tutti!

Grazie!

Andrea Rubera (per Nuova Proposta)

![locandina_tavola_rotonda_280410-def_ld1](http://www.certidiritti.org/wp-content/uploads/2010/04/locandina_tavola_rotonda_280410-def_ld1.jpg)

_Il movimento glbtqi e la società : come acquistare efficacia nel supportare le istanze delle persone omosessuali, transessuali e intersessuali presso la società e i media?  
  
  
La difficoltà dell'affermazione della "questione omosessuale e transessuale" è generata solo dal pregiudizio e dall'omofobia, difficili da scalfire, oppure ci sono anche delle dinamiche interne al movimento che possono essere smussate per invertire questa situazione di stallo?_

_  
Il linguaggio e la comunicazione utilizzati all'interno del movimento glbtqi sono forse autoreferenziati, comprensibili e metabolizzabili solamente dalle persone  
glbtqi stesse? E' possibile un lavoro che porti a comunicare con la società parlando un linguaggio che sia facilmente metabolizzabile, senza snaturare la propria identità?_

_  
E' arrivato il momento, come movimento glbtqi nel suo complesso, per serrare le fila, fare massa critica e trovae un minimo comun denominatore su cui lavorare in maniera convergente?_

**  
A queste e ad altre domande tenteremo di dare una risposta nel corso della Tavola Rotonda, organizzata da noi di "Nuova Proposta, donne e uomini omosessuali cristiani", perché in quanto cristiani crediamo fermamente nel dialogo, nel confronto e nel costruire insieme, nel potenziale di fecondità del movimento glbtqi, nella contaminazione della società, nella formazione e informazione.**

[Allegato in jpeg](http://www.certidiritti.org/wp-content/uploads/2010/04/locandina_tavola_rotonda_280410-def_ld.jpg)