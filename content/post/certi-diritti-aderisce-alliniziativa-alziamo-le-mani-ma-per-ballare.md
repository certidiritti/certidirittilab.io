---
title: 'Certi Diritti aderisce all''iniziativa ''Alziamo le mani ma per ballare'''
date: Fri, 23 Mar 2012 13:09:13 +0000
draft: false
tags: [Movimento LGBTI]
---

Appuntamento domenica 25 marzo 2012 alle ore 15.30, nel piazzale davanti alla discoteca Just In di Germignaga-Luino (VA), dove sabato 17 marzo sono avvenuti i pestaggi contro sette ragazzi tra i quali Marco Coppola, presidente di Arcigay Verbania.

Una manifestazione per dire no alla violenza. Nasce così 'Alziamo le mani per ballare' promosso da un gruppo di giovani di Luino, dove sabato 17 marzo nella discoteca Just In sette ragazzi, tra cui Marco Coppola, presidente di Arcigay Verbania, sono stati aggrediti.  
Già in altre occasioni, nello stesso locale, si sono verificati episodi di violenza da parte dei buttafuori ai danni di diversi ragazzi, alcuni dei quali hanno sporto denuncia dopo aver riportato gravi lesioni (ematomi sul collo, frattura del naso, perforazione del timpano).

Dopo i fatti accaduti sabato 17 marzo, nonostante le dichiarazioni del gestore della discoteca, la parlamentare del PD Paola Concia ha presentato una interrogazione a Elsa Fornero e Anna Maria Cancellieri. Per l'ex ministro Mara Carfagna "non ci deve essere nessuna tolleranza". Molti giornali fra i quali Corriere della Sera, Il Giornale hanno pubblicato articoli sulla vicenda e la notizia è stata diffusa anche da diversi telegiornali. La trasmissione televisiva Matrix ne ha parlato nella puntata di giovedì 22 marzo.

Un gruppo di giovani Luinesi ha deciso di organizzare una manifestazione contro la violenza nei luoghi di divertimento e di divulgare la notizia attraverso una pagina fan su Facebook “ALZIAMO LE MANI MA PER BALLARE”.

  
**Ecco il manifesto:**

Chiediamo al sindaco di Luino Andrea Pellicini e al sindaco di Germignaga Enrico Prato, al presidente della provincia di Varese Dario Galli e a quello della regione Roberto Formigoni, di intervenire su fatti gravi che coinvolgono i ragazzi e le loro famiglie. Vogliamo che cessino gli insulti e le violenze che hanno coinvolto in questi anni non solo ragazzi gay ma molte altre persone. È da troppo tempo che questi episodi colpiscono minorenni e non. Le chiamate ai carabinieri si moltiplicano. La polizia non è in grado di garantire la tutela dei giovani e i ragazzi davanti ai carabinieri non sono dovutamente ascoltati.  
Ci appelliamo a tutte le forze politiche, alle associazioni giovanili, alle comunità religiose, perché cessino questi gesti. Organizziamo una manifestazione per portare alla luce tanti episodi simili rimasti nascosti.  
Ci auguriamo di potere avere al nostro fianco anche tutte le famiglie perché questa manifestazione sia pacifica e che sia chiaro con quali principi vogliamo agire. No alla violenza!!!!  
I nostri genitori devono essere certi e assicurati che, quando usciamo alla sera, non andiamo a finire in ospedale conciati da buttafuori violenti e senza alcuna vera ragione. Chiediamo sostegno alla stampa, alle televisioni e a tutti i ragazzi che come noi “le mani le vogliono alzare solo per ballare” in discoteca, in allegria e senza rischi di  
pestaggi.

[http://www.facebook.com/pages/alziamo-le-mani-ma-per-ballare/260033197417366](http://www.facebook.com/pages/alziamo-le-mani-ma-per-ballare/260033197417366)  
[http://www3.varesenews.it/varese/articolo.php?id=229134](http://www3.varesenews.it/varese/articolo.php?id=229134)