---
title: 'La Duma approva in prima lettura una legge anti gay. Certi Diritti scrive al Ministro degli Esteri'
date: Sat, 26 Jan 2013 09:58:57 +0000
draft: false
tags: [Russia]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti

Roma, 26 gennaio 2012

Ieri, 25 gennaio 2013, la Duma ha approvato in prima lettura, con 388 voti favorevoli su un totale di 450 deputati, la legge contro la cosiddetta 'propaganda omosessuale'.

Se adottata anche in seconda e terza lettura, nonché firmata dal Presidente, verrà estesa a tutta la Federazione russa la sciagurata legge, già in vigore a San Pietroburgo e in altre otto regioni, che non colpisce soltanto le persone Lgbti, ma tutti coloro che usano la parola omosessuale in un loro scritto, in un loro spettacolo o in una loro canzone, a prescindere dal loro orientamento sessuale.

L'Associazione Radicale Certi Diritti ha scritto al Ministro degli Esteri Terzi per chiedere di fare tutto quanto è in suo potere per evitare la definitiva approvazione di questa legge già condannata dal Parlamento Europeo e dalle Nazioni Unite.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: "Con questo gesto la Russia si sta ponendo al di fuori della civiltà europea, violando apertamente la Convenzione Europea per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali, che pure ha sottoscritto. Solo due giorni fa Håkon Haugli, rapporteur dell'Assemblea Parlamentare del Consiglio d'Europa (PACE) per i diritti delle persone LGBT, aveva definito la proposta di legge in discussione alla Duma "un tentativo di imporre una restrizione alle libertà fondamentali". A essere in gioco sono i valori europei fondamentali: i diritti umani, la democrazia e lo Stato di diritto".