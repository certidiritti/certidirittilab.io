---
title: 'DEPENALIZZAZIONE OMOSESSUALITA'' ALL''ONU: VATICANO RESTA SOLO CON DITTATORI'
date: Wed, 18 Mar 2009 15:12:01 +0000
draft: false
tags: [Comunicati stampa]
---

**DEPENALIZZAZIONE UNIVERSALE DELL'OMOSESSUALITA': UE E USA CON LA PROPOSTA FRANCESE. IL VATICANO RESTA SOLO CON GLI STATI ISLAMICI E TOTALITARI. DICHIARAZIONE DI MARCO CAPPATO.**  
  
Bruxelles, 18 Marzo 2009  

Rispondendo a un'interrogazione da me presentata insieme ai colleghi Liberali, la Commissaria Benita Ferrero-Waldner ha ribadito che "la Commissione ritiene inaccettabile che le persone possano essere oggetto di discriminazione sulla base dell’orientamento sessuale. Per questo motivo, la Commissione non può che sostenere l’iniziativa della Presidenza, appoggiata da tutti gli Stati membri dell’Unione, sulla depenalizzazione dell’orientamento sessuale.  
La Commissione è quindi pronta ad affrontare tale questione con i paesi terzi, Santa Sede inclusa, in occasione dei contatti sulla promozione e tutela dei diritti dell’uomo".  
  
Dichiarazione di Marco CAPPATO, deputato europeo Radicale:

«La notizia che Obama ha deciso di cancellare il veto di Bush sulla risoluzione francese all'ONU per la depenalizzazione dell'omosessualità riporta gli Stati Uniti a fianco delle democrazie occidentali e dell'Unione europea per un impegno contro le discriminazione sulla base dell’orientamento sessuale. Spero che gli USA vorranno affrontare la questione anche nell'ambito dei rapporti con i paesi terzi - come ha annunciato la Commissione europea. Ormai ad osteggiare  attivamente l'iniziativa francese - sostenuta da 66 Paesi - restano solo gli Stati islamici, i regimi totalitari e il Vaticano, uniti in una "santa alleanza"».  
  
Testo dell'interrogazione alla Commissione  
di Marco Cappato, Sophie In't Veld, Andrew Duff, Olle Schmidt, a nome del gruppo ALDE  
  
Iniziativa francese all'ONU sulla depenalizzazione dell'omosessualità  
  
La Francia ha lanciato un'iniziativa in sede ONU sulla depenalizzazione universale dell'omosessualità, raccogliendo l'adesione di più di 50 Stati tra cui tutti gli Stati Membri dell'UE, volta al deposito di una dichiarazione comune  all'Assemblea Generale. La dichiarazione afferma che le violazioni, persecuzioni, torture, trattamenti inumani, crudeli e degradanti, arresti e detenzioni arbitrarie, esecuzioni e discriminazioni compiute sulla base dell'orientamento sessuale e dell'identità di genere sono una violazione dei diritti umani e chiedendo misure legislative o amministrative per assicurare che orientamento sessuale e identità di genere non siano alla base di sanzioni criminali, in particolare esecuzioni, arresti e detenzioni. Rispetto a tale dichiarazione, il Vaticano ha espresso la sua opposizione attraverso l'osservatore permanente della Santa Sede presso le Nazioni Unite, monsignor Celestino Migliore, il quale ha affermato che "con una dichiarazione di valore politico, sottoscritta da un gruppo di paesi, si chiede agli Stati ed ai meccanismi internazionali di attuazione e controllo dei diritti umani di aggiungere nuove categorie protette dalla discriminazione, senza tener conto che, se adottate, esse creeranno nuove e implacabili discriminazioni...Per esempio gli Stati che non riconoscono l'unione tra persone dello stesso sesso come "matrimonio" verranno messi alla gogna e fatti oggetto di pressioni".  
Quali iniziative la Commissione europea ha intrapreso o intende intraprendere al fine di assicurare che la dichiarazione francese ottenga il più ampio sostegno da parte degli Stati all'ONU? Con quali Stati extra-europei ha preso contatto al riguardo? Cosa intende fare la Commissione per scongiurare l'azione diplomatica del Vaticano e dei 91 Stati totalitari o integralisti che nel mondo prevedono sanzioni, torture, pene e persino l'esecuzione capitale (in 10 paesi islamici) contro le persone omosessuali? Non ritiene la Commissione che sia urgente sollevare tale questione nell'ambito delle relazioni internazionali e del cosiddetto dialogo interculturale - interreligioso che l'UE mantiene con il Vaticano e con i "rappresentanti" della religione cattolica ed adottare le misure a tal riguardo come accadrebbe con Stati terzi?  
  
  
E-6723/08 IT  
Risposta di Benita Ferrero-Waldner  
a nome della Commissione  
  
  
La Commissione ritiene inaccettabile che le persone possano essere oggetto di discriminazione sulla base dell’orientamento sessuale. Per questo motivo, la Commissione non può che sostenere l’iniziativa della Presidenza, appoggiata da tutti gli Stati membri dell’Unione, sulla depenalizzazione dell’orientamento sessuale.  
  
La Commissione è quindi pronta ad affrontare tale questione con i paesi terzi, Santa Sede inclusa, in occasione dei contatti sulla promozione e tutela dei diritti dell’uomo.  
  
assistant to Marco Cappato MEP  
  
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-  
Parlement européen  
ASP 09 G 346  
60, rue Wiertz  
B-1047 Bruxelles

Tel : +32 (0)2 28 45288  
Fax : +32 (0)2 28 49288

  
[marco.cappato@europarl.europa.eu](mailto:marco.cappato@europarl.europa.eu)  
[www.marcocappato.it](http://www.marcocappato.it)  
[www.radicalparty.org](http://www.radicalparty.org)