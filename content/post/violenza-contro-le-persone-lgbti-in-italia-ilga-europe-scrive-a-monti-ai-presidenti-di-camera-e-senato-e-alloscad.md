---
title: 'Violenza contro le persone LGBTI in Italia: ILGA Europe scrive a Monti, ai presidenti di Camera e Senato e all''OSCAD'
date: Mon, 25 Jun 2012 07:54:14 +0000
draft: false
tags: [Politica]
---

To the President of the Council of Ministers of Italy, Pr. **Mario MONTI**

To the President of the Italian House of Representatives, On. **Gianfranco FINI**

To the President of the Italian Senate, On. **Renato SCHIFANI**

To the President of the Observatory for security against discriminatory acts (OSCAD),  
Mr **Francesco Cirillo**

  
Violence against lesbian, gay, bisexual trans and intersex (LGBTI) people

In the last years, the number of homophobic and transphobic incidents reported in Italy  
has been growing to reach an alarming level. ILGA-Europe, the European Region of the  
International Lesbian, Gay, Bisexual, Trans and Intersex Association believes that this  
phenomenon provides full evidence of an unacceptable context of violence against  
lesbian, gay, bisexual and trans people.

In the last days, various dramatic incidents were documented around the Roma Pride  
2012\. This culminated in a violent aggression against one of the organisers of the event,  
Guido Allegrezza, who was severely beaten up by five young men on 13 June. These  
attacks are the last examples of a more general situation that has been documented by  
our member organisations in Italy, including Arcigay, Arcilesbica and Certi Diritti. A  
report by Arcigay, with the cases of violence and homophobia and transphobia reported  
in the press in 2011, is attached to this letter.

Wherever such violence happens, ILGA-Europe stresses the importance of a strong and  
comprehensive reaction by all the relevant public authorities, in order to tackle  
intolerance and prevalent forms of hate crime. All European governments, including the  
Italian government, have subscribed to such principles, in political commitments such as  
the OSCE’s Ministerial Council Decision N° 9/09 of December 20091, or the  
Recommendation CM/Rec(2010)5 of the Committee of Ministers of the Council of  
Europe to member states on measures to combat discrimination on grounds of sexual  
orientation or gender identity2.

The recent events show how necessary it is for Italy to promptly and fully implement  
such commitments. One key measure is the adoption of legislative measures on  
appropriate criminal penalties for violence, threats of violence, incitement to violence and  
related harassment based on sexual orientation or gender identity. In the Italian context,  
it is time to amend the Mancini law of 25 June 1993, to enlarge its scope to hate crimes

[http://www.osce.org/cio/40695](http://www.osce.org/cio/40695)  
[https://wcd.coe.int/ViewDoc.jsp?id=1606669](https://wcd.coe.int/ViewDoc.jsp?id=1606669)

perpetrated with a homophobic or a transphobic motivation. Currently, the law’s  
provisions only address violence based on ethnicity, nationality and religion. ILGA-  
Europe is aware that various bills were proposed in the last years, and that their  
rejection by the Parliament sent a very negative message to the public.

In addition, OSCE and Council of Europe commitments go beyond the adoption of  
appropriate criminal legislation. It is indeed equally important to make sure that the  
perpetration of such violence is vigorously investigated, and that, where appropriate  
evidence is found, those responsible are prosecuted, tried and duly punished. Data on  
hate crime shall be collected, maintained and made public in a reliable way. To achieve  
these objectives, it is essential to build confidence between police forces, prosecution  
services and victims as well as the NGOs representing the potentially victimised  
communities. The development of professional training and capacity building activities  
for law-enforcement officers is an important action in that respect.

More generally, we would like to emphasize that the rights to be free from discrimination  
and to be protected from violence, including violence motivated by hatred, are among  
the rights recognised by a number of international and European human rights  
instruments, legally binding for the States. Homophobia and/or transphobia may already  
be seen as aggravating factors in criminal behaviour in 16 member states of the  
European Union.

The European Court of Human Rights has started developing case law in that respect.  
Its decisions Nachova and others v. Bulgaria (2005), Secic v. Croatia (2007) and  
Milanovic v. Serbia (2010) made clear that States have an obligation to investigate on  
the racist or religious motivation of a crime. More recently, in the context of hate speech,  
the Court stressed, that “discrimination based on sexual orientation was as serious as  
discrimination based on race, origin or colour (Vejdeland and others v. Sweden, 2012).

On the basis of these legal and political context elements, ILGA-Europe believes that the  
Italian Government and the Italian Parliament have a duty to give a remedy to a worrying  
national situation, by seriously implementing the commitments they have made in  
European fora.

We look forward to hearing from you on the initiatives that the Italian Government and  
Parliament can form to improve the national standards on tackling homophobic and  
transphobic violence.

Yours sincerely,

Gabi Calleja  
Martin K.I. Christensen  
Co-Chairs of the Executive Board