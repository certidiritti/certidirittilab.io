---
title: 'SOS RUSSIA'
date: Sat, 20 Jul 2013 19:49:49 +0000
draft: false
tags: [Russia]
---

Sono passate poche settimane dalla **promulgazione della legge che vieta la cosiddetta "propaganda omosessuale" su tutto il territorio russo**...

Altre norme che riguardano direttamente le ONG contribuiscono ulteriormente a **rendere impossibile l'operato delle organizzazioni che si occupano di tutelare e promuovere i diritti umani**: ingenti sanzioni pecuniarie sono comminate a tutte le associazioni che ricevono finanziamenti stranieri e che si rifiutano di iscriversi al registro che raccoglie tutte queste organizzazioni che "tradiscono" i valori tradizionali della patria.

**Le associazioni che difendono i diritti umani delle persone gay, lesbiche, bisessuali, intersessuali e transessuali in Russia si oppongono a questo trattamento infamante** e sono pronti a farsi carico delle loro responsabilità andando incontro ad un esborso di circa 20.000€ che potrebbe voler determinare la loro **chiusura**.

Hanno bisogno del nostro aiuto per evitare che ciò accada ed è per questo che abbiamo deciso di dare il via ad una **corsa di solidarietà** nei confronti dei nostri amici ed amiche russe e di tutti coloro che difendono i diritti umani delle persone gay, lesbiche, bisessuali, transessuali e intersessuali in quel Paese.

Abbiamo **tre modi** per aiutarli:

Puoi contribuire versando anche solo 2€ attraverso Paypal cliccando qui:

  

![](https://www.paypalobjects.com/it_IT/i/scr/pixel.gif)

Oppure puoi effettuare un bonifico sul Conto Corrente **IT 34 E 08327 03221 000000003165**  
intestato a: **Associazione Radicale Certi Diritti  
**causale: **SOS Russia**

**Infine, se ti iscrivi entro la fine di agosto a Certi Diritti con la quota da 50€, devolveremo 20€ della tua quota associativa al fondo di emergenza per la Russia >>> [http://www.certidiritti.it/partecipa/iscriviti](partecipa/iscriviti)**

**SOS RUSSIA è promossa da:**

_Associazione Radicale Certi Diritti – Agedo - Arcigay - Famiglie Arcobaleno - Equality Italia - Arcilesbica - Rete Genitori Rainbow__![logoCD](http://www.certidiritti.org/wp-content/uploads/2013/12/logoCD.jpg)__![logo-agedo](http://www.certidiritti.org/wp-content/uploads/2013/07/logo-agedo.jpg)____![Arcigay Logo](http://www.certidiritti.org/wp-content/uploads/2013/07/Arcigay_Logo.png)_![arcilesbica](http://www.certidiritti.org/wp-content/uploads/2013/07/arcilesbica.jpg)__![equality](http://www.certidiritti.org/wp-content/uploads/2013/07/equality.jpg)___![526-3-duemamme](http://www.certidiritti.org/wp-content/uploads/2013/07/526-3-duemamme.jpg)_

_![Rete-genitori-rainbow](http://www.certidiritti.org/wp-content/uploads/2013/07/Rete-genitori-rainbow.jpg)_

Tutte le associazioni possono aderire inviando una mail a: info@certidiritti.it e pubblicando su web il banner dell'iniziativa.

I fondi sono versati sul conto dell'Associazione radicale Certi Diritti ed interamente utilizzati per aiutare le associazioni russe che difendono i diritti umani delle persone lgbti.