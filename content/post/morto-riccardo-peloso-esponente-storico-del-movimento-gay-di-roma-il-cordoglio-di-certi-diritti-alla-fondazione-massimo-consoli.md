---
title: 'Morto Riccardo Peloso, esponente storico del movimento gay di Roma. Il Cordoglio di Certi Diritti alla Fondazione Massimo Consoli'
date: Wed, 22 Feb 2012 11:52:55 +0000
draft: false
tags: [Movimento LGBTI]
---

\* nella foto Riccardo Peloso (al centro) e Massimo Consoli (a destra).

Comunicato Stampa dell'Associazione Radicale CertiDiritti  
   
Roma, 22 febbbraio 2012  
   
L'Associazione Radicale Certi Diritti esprime il suo cordoglio e la sua più sentita vicinanza alla Fondazione Massimo Consoli per la scomparsa dell'amico e compagno di lotte Riccardo Peloso.  Riccardo, esponente storico del mondo gay della capitale,  tra i fondatori della Fondazione Massimo Consoli, aveva partecipato a molte delle iniziative dell'Associazione Radicale Certi Diritti.

Nel 2008 era stato candidato nelle liste dei Radicali nel I e nel III Municipio di Roma.  
I suoi racconti e le vicende narrate, sulla sua vita, in ogni occasione di incontro, sono sempre stati spunto di riflessione per l'importanza storica e il contesto politico in cui sono avvenute. Gli incontri con Pasolini, i suoi racconti sui pomeriggi passati al cimitero degli inglesi con Dario Bellezza, gli happening organizzati a Capocotta con Allen Ginsberg nei primi anni '70, i racconti dei primi locali gay di Roma  negli anni '60 e '70, hanno sempre affascinato le persone che hanno avuto la fortuna e il privilegio di ascoltarlo, di essergli amico.  Riccardo, persona cara e sincera, mancherà a tutti noi.  
  

**iscriviti alla newsletter >[  
](newsletter/newsletter)[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**