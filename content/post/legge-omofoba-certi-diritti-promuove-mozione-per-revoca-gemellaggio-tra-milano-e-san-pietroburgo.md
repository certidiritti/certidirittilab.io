---
title: 'Legge omofoba: Certi Diritti promuove mozione per revoca gemellaggio tra Milano e San Pietroburgo'
date: Fri, 02 Mar 2012 10:59:10 +0000
draft: false
tags: [Russia]
---

Il gruppo radicale-federalista europeo ha presentato la mozione in Consiglio Comunale e in tutte le zone. I capigruppo di maggioranza al comune sottoscrivono.

Comunicato stampa dell’associazione radicale Certi Diritti

2 marzo 2012

Il 29 febbraio scorso la città di San Pietroburgo ha approvato in terza e ultima lettura una legge che rende illegale scrivere un libro, pubblicare un articolo o parlare in pubblico di omosessualità.

Subito dopo l’approvazione di questa legge - chiaramente volta a criminalizzare qualunque attività o informazione relativa alle persone LGBTI (Lesbiche, Gay, Bisessuali, Transessuali e Intersessuali) e alle relazioni tra persone dello stesso sesso, in patente violazione delle libertà di espressione e associazione nonché degli impegni presi dalla Russia ratificando la Convenzione europea per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali - **l’Associazione Radicale Certi Diritti ha chiesto al Comune di Milano di revocare immediatamente il gemellaggio tra la città di Milano e quella di San Pietroburgo.**

**Il Gruppo Radicale - Federalista Europeo ha quindi presentato una mozione in Consiglio Comunale e in tutte le zone** per chiedere la denuncia del patto di Gemellaggio ratificato tra le due città il 2 ottobre 1967. In Consiglio Comunale, la mozione a prima firma Cappato è stata subito sottoscritta da Carmela Rozza, capogruppo del PD, Patrizia Quartieri, capogruppo di SEL, Anna Scavuzzo, capogruppo della lista civica per Pisapia e Anita Sonego, capogruppo della Federazione della Sinistra.

**iscriviti alla newsletter >[  
](newsletter/newsletter)[](newsletter/newsletter)[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**