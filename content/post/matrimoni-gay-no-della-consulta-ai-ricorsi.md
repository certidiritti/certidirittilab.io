---
title: 'Matrimoni gay, no della Consulta ai ricorsi'
date: Wed, 14 Apr 2010 13:52:55 +0000
draft: false
tags: [Senza categoria]
---

su _**[La Repubblica](http://www.repubblica.it/cronaca/2010/04/14/news/consulta_matrimoni_gay-3344318/)**_ del 14 aprile 2010

La Corte costituzionale ha respinto come inammissibili e infondate le istanze arrivate da Venezia e Trento. I giudici hanno fatto riferimento alla "discrezionalità del legislatore"

OAS_RICH('Left');

![Matrimoni gay, no della Consulta ai ricorsi ](http://www.repubblica.it/images/2010/04/14/140054186-0636c8ad-660e-4cef-a42d-1feb47808321.jpg "Matrimoni gay, no della Consulta ai ricorsi ") 

Antonella D'Annibale (sinistra) e Debora Galbiati Ventrella al loro matrimonio simbolico il 27 febbraio a Torino

**ROMA** \- La Corte costituzionale ha respinto come inammissibili e infondati i ricorsi sui matrimoni tra persone dello stesso sesso. Nelle motivazioni la Consulta fa riferimento alla discrezionalità del legislatore: i giudici fanno intendere che non è loro competenza regolamentare la questione e affermano che la trattazione della materia spetta soltanto al Parlamento. Resta da vedere - ma questo si comprenderà solo dalla lettura delle motivazioni della sentenza che sarà scritta dal giudice Alessandro Criscuolo - se la Corte coglierà l'occasione o meno per sollecitare il legislatore a provvedere.  
  
A sollevare il caso davanti alla Consulta erano stati il tribunale di Venezia e la Corte d'appello di Trento nell'ambito di distinte cause intraprese da tre coppie omosessuali contro il rifiuto loro opposto dall'ufficiale di Stato civile dei comuni di residenza di fare le pubblicazioni matrimoniali da loro richieste. La Corte aveva iniziato ad esaminare il caso nell'udienza pubblica del 23 marzo scorso, ma aveva rinviato la decisione a dopo le festività pasquali.  
  
I ricorsi ipotizzavano in particolare l'ingiustificata compromissione degli articoli 2 (diritti inviolabili dell'uomo), 3 (uguaglianza dei cittadini), 29 (diritti della famiglia come società naturale fondata sul matrimonio) e 117 primo comma (ordinamento comunitario e obblighi internazionali) della Costituzione. I ricorrenti, in sostanza, affermavano la non esistenza nell'ordinamento di un espresso divieto al matrimonio tra persone dello stesso sesso e lamentavano l'ingiustificata compromissione di un diritto fondamentale (quello di contrarre matrimonio) oltre che la lesione di una serie di diritti sanciti a livello comunitario. Per non parlare poi - veniva fatto notare - della disparità di trattamento tra omosessuali e transessuali, visto che a questi ultimi, dopo il cambiamento di sesso, è consentito il matrimonio tra persone del loro sesso originario.

  
Durante l'udienza del 23 marzo i legali delle coppie omosessuali avevano sollecitato la Corte a dare una "risposta coraggiosa" che, anticipando l'intervento del legislatore, consentisse il via libera ai matrimoni omosessuali. Dal canto suo, l'avvocato dello Stato Gabriella Palmieri, per conto della presidenza del Consiglio, aveva ribadito che il matrimonio si basa sulla differenza tra sessi e aveva rivendicato il primato del legislatore a decidere su una materia tanto delicata.  
  
Il ricorso alla Consulta era di fatto scaturito dalla campagna 'Sì lo voglio' promossa dall'associazione radicale Certi diritti e da diverse associazioni per i diritti della comunità lgbt (lesbiche, gay, bisessuali e trans). Negli ultimi due anni sono state una trentina le coppie omosessuali che in Italia si sono presentate al proprio Comune per ottenere la pubblicazione di matrimonio. Di fronte al rifiuto, tutte hanno fatto ricorso al tribunale, con l'obiettivo dichiarato di spingere il giudice a chiedere una pronuncia della Corte costituzionale sulla legittimità delle norme che impediscono le nozze tra persone dello stesso sesso. I magistrati del Tribunale di Venezia e della Corte d'appello di Trento sono stati i primi a farlo.