---
title: 'Mozione generale del V Congresso 2011'
date: Wed, 14 Dec 2011 07:43:20 +0000
draft: false
tags: [Politica]
---

Il 2011 ha visto manifestarsi pienamente la dimensione paradossale della situazione italiana in materia di uguaglianza delle persone omosessuali, transessuali e intersessuali: la grande visibilità, anche istituzionale, che l’Europride e la Conferenza di ILGA Europe hanno prodotto non è riuscita nemmeno a scalfire la colpevole inattività di un Parlamento che continua a far finta che gli italiani e le italiane siano come il Vaticano li vorrebbe.

Un Parlamento che oltre ad essere cieco e sordo alla vita di milioni di persone, è anche muto di fronte al forte richiamo della corte costituzionale che, con la sentenza 138/2010, ha messo in evidenza la necessità di regolamentare le unioni tra persone dello stesso sesso come formazioni sociali a pieno titolo garantite dalla Carta.

Questa situazione può produrre, letteralmente, disperazione, se non si conquista la consapevolezza di quanto il paradosso italiano affondi le sue radici in una più generale patologia del sistema democratico-istituzionale, che al tempo stesso è causa e conseguenza della non volontà della classe dirigente (e non solo politica) di questo paese di affrancarsi dall’egemonia vaticana. Una patologia che la cultura radicale ha pienamente descritto come vera e propria peste, ormai diffusa oltre i confini nazionali e capace di intaccare anche le grandi riforme europee e l’attività delle sue istituzioni in materia di libertà individuali.

L’associazione rinnova la sua gratitudine ed il suo impegno per onorare la scelta di speranza e di futuro rappresentata dalle coppie che hanno scelto di essere parte della campagna di “Affermazione civile”, così come l’impegno generoso di pochi e preziosi avvocati e giuristi che con animo militante alimentano e sostengono la nostra azione politica.

L’Associazione ritiene quindi che la strada intrapresa di:

*   assumere come prioritaria la lotta contro ogni forma di sessuofobia, per la piena responsabilità e libertà individuale in materia di autodeterminazione della propria vita;
*   allargare le alleanze sui diritti, anche e soprattutto oltre il mondo delle associazioni lgbt;
*   tradurre la dimensione transazionale della difesa delle libertà sessuali della persona in concrete azioni ed iniziative;

sia da perseguire con forza, dando mandato agli organi associativi di:

1.  sostenere con ogni sforzo politico ed organizzativo la seconda parte della campagna di Affermazione civile e la riforma del diritto di famiglia depositata in Parlamento dai deputati radicali*;
2.  essere promotori e attivi sostenitori di nuove alleanze per i diritti e contro ogni forma di discriminazione;
3.  dare concretezza alla scelta transnazionale operata con al richiesta di diventare soggetto costituente del partito Radicale Transnazionale e Transpartito.

  
\* Pdl n. 3607 che prevede 1) Testamento biologico. 2) Parentela. 3) Rapporto tra matrimonio civile e matrimonio religioso. 4) Matrimonio tra persone dello stesso sesso. 5) Cognome della moglie e dei figli. 6) Rapporti tra separazione e divorzio. 7) Mediazione familiare. 8) Assegnazione della casa familiare. 9) Modifiche dell’affido condiviso. 10) Solidarietà post matrimoniale. 11) Le unioni libere. 12) Le intese di solidarietà. 13) Le comunità intenzionali. 14) Quadro complessivo degli istituti familiari e parafamiliari. 15) La filiazione. 16) Procreazione assistita. 17) Adozione. 18) Responsabilità genitoriale. 19) Amministrazione di sostegno. 20) Modifiche a norme penali.

Milano, 4 dicembre 2011