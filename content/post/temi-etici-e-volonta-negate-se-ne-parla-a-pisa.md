---
title: 'TEMI ETICI E VOLONTÀ NEGATE, SE NE PARLA A PISA'
date: Wed, 03 Mar 2010 07:45:00 +0000
draft: false
tags: [Comunicati stampa]
---

**Omofobia, matrimoni gay, testamento biologico, fecondazione assistita, RU486 e molti altri saranno i temi affrontati nel Barcamp organizzato dall'associazione Luca Coscioni di Pisa e dall'associazione radicale Certi Diritti.**

Matrimonio gay, pillola del giorno dopo, RU486, omofobia. **I cosiddetti temi "sensibili" saranno al centro del primo Barcamp** \- una "non conferenza" come la chiamano gli oganizzatori - organizzato dalla Cellula Coscioni di Pisa e dall'associazione radicale Certi Diritti **giovedì 4 marzo dalle 18.00 alle 24.00 presso il Circolo Arci Isola Verde.**

**L'incontro pubblico ha come titolo "LA RISCOSSA DELLE VOLONTÀ NEGATE"** e sarà caratterizzato da brevi interventi di massimo 15 minuti ciascuno, ognuno su un tema diverso. A seguire le domande del pubblico. Il riconoscimento del matrimonio gay, le norme della Regione Toscana contro l'omofobia, il registro pisano delle unioni civili e il riconoscimento delle coppie di fatto saranno i temi al centro della prima parte del Barcamp. Dopo la cena, per la quale è però necessario prenotarsi, saranno affrontati altri temi: la Legge 40 sulla fecondazione assistita, il testamento biologico, l'uso della canapa per la terapia del dolore, la pillola del giorno dopo e, infine, la RU486.

Fra i relatori interverranno anche **Alessandra Petreri**, vicepresidente della Provincia di Pisa, **Marco Bani** e **Luigi Branchitta**, consiglieri comunali Pd al Comune di Pisa, **Daniele Nardini**, direttore dei contenuti del portale Gay.it, **Sergio Bartolommei**, presidente Consulta di Bioetica di Pisa, **Filomena Gallo**, avvocato e vicesegretario dell'associazione Luca Coscioni per la libertà di ricerca scientifica. La moderazione è affidata al coordinatore della cellula Coscioni di Pisa Enrico Stampacchia.

«**Al centro di ogni relazione - dice Stampacchia - non daremo solo le informazioni sulla situazione normativa** di ogni tema trattato, ma faremo anche una ricognizione su tutte quelle iniziative che potranno modificare e hanno in parte già modificato l'attuale quadro legislativo. Basti pensare - conclude il coordinatore della Cellula Coscioni di Pisa - alla **sentenza della Corte Costituzionale del 23 marzo** **sui matrimoni gay**, o alle sentenze che hanno modificato l'esito del referendum sulla fecondazione assistita».

Per informazioni è sufficiente chiamare il numero 329.070.48.25 o visitare il sito web www.lucacoscioni.it/cellulapisa

> **_Cosa_**: "La riscossa delle volontà negate, il primo BarCamp sui diritti individuali: dalle iniziative politiche al riconoscimento giuridico"
> 
> **_Quando_**: Giovedì 4 marzo ore 18.00-24.00
> 
> **_Dove_**: presso il "Circolo Arci Isola Verde", Via Frascani, Pisa
> 
> Per registrarsi all'evento fare clic su [questo link](tutti-gli-eventi/details/13-la-riscossa-delle-volonta-negate.html "LA RISCOSSA DELLE VOLONTÀ NEGATE")