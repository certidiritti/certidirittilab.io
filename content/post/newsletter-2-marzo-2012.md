---
title: 'Newsletter 2 Marzo 2012'
date: Sun, 25 Mar 2012 09:42:31 +0000
draft: false
tags: [Politica]
---

![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)

**San Pietroburgo approva legge omofoba**  
Dopo il [flash mob](a-milano-flashmob-per-la-liberta-di-espressione-in-russia-governatore-di-san-pietroburgo-metta-il-veto-sulla-legge-omofoba) a Milano e l’[appello](legge-regionale-contro-la-propaganda-omosessuale-approvata-a-san-pietroburgo) al Ministro degli Esteri italiano, al Consiglio D'Europa e all'Unione Europea contro la legge che multerà chi si dichiara gay e diffonde testi sull'omosessualità, l’associazione radicale [Certi Diritti promuove una mozione per revocare il gemellaggio tra M ilano e San Pietroburgo](legge-omofoba-certi-diritti-promuove-mozione-per-revoca-gemellaggio-tra-milano-e-san-pietroburgo). La mozione presentata dal consigliere Marco Cappato è stata sottoscritta da quasi tutti i capigruppo in consiglio comunale.

**Unioni civili, ma il nostro obiettivo resta il matrimonio!  
[Regioni, comuni e diritti delle unioni civili](regioni-comuni-e-diritti-delle-unioni-civili): **una  riflessione per definire meglio aspettative e iniziative. Di Enzo Cucco,Presidente Associazione radicale Certi Diritti.

Su Agenzia Radicale:** [un registro anche per Milano](http://www.agenziaradicale.com/index.php?option=com_content&task=view&id=13592&Itemid=51)**

Da Vicenza: famiglia anagrafica **[“si tratta di applicare una legge già esistente”](http://www.ilgiornaledivicenza.it/stories/Cronaca/337225_ma_si_tratta_di_applicare_una_legge_gi_esistente/)**

**Oggi a Roma incontro ****[I diritti lgbt(e) in Italia dopo la sentenza 138/10](i-diritti-lgbte-in-italia-dopo-la-sentenza-138-10) ore 18 presso la sede del Partito Radicale, via  di Torre Argentina 76,  Roma**

**Piccolo Uovo**  
Successo dell’iniziativa di Certi Diritti ‘Adotta anche tu piccolo uovo’: i soldi raccolti ci permetteranno di regalare a tutte le biblioteche di Milano non solo ‘Piccolo uovo’ ma anche il libro ‘Piccola Storia di una famiglia’.**  
[Marco Tullio Altan: polemica sulla coppia di pinguini gay del ‘Piccolo Uovo’ >](http://libriblog.com/in-evidenza/marco-tullio-altan-e-polemica-sulla-coppia-di-pinguini-gay-del-piccolo-uovo/) **

**Divorzio breve  
**La Commissione Giustizia della Camera ha approvato **[la proposta di legge sul cosiddetto «divorzio breve»](http://www.leggioggi.it/2012/02/24/tutto-pronto-per-il-divorzio-breve/)**. Se il Senato confermerà l’approvazione sarà un primo passo positivo, ma resta comunque lontano l’obiettivo della riforma del Diritto di famiglia come Certi Diritti e i radicali si auspicano. **[Qui il testo della proposta di legge >](http://www.leggioggi.it/wp-content/uploads/2012/02/ddl-divorzio-breve1.pdf)**

**[Doppio cognome ai figli: un timido passo avanti >](http://www.lastampa.it/_web/cmstp/tmplRubriche/editoriali/gEditoriali.asp?ID_blog=25&ID_articolo=9815)**

**Notizie dal mondo**

**[Adozioni gay: il Parlamento portoghese dice no >](http://www.blitzquotidiano.it/politica-europea/adozioni-gay-parlamento-portogallo-no-1132747/)**

**[In India vogliono che la Corte Suprema renda illegali i rapporti gay >](http://www.queerblog.it/post/14491/in-india-vogliono-che-la-corte-suprema-renda-illegali-i-rapporti-gay)**

**[Tre donne arrestate per omosessualità in Camerun >](http://www.queerblog.it/post/14537/tre-donne-arrestate-per-omosessualita-in-camerun)**

**[Kenya: violenze contro i gay a un convegno sull'Hiv >](http://www.queerblog.it/post/14501/kenia-violenze-contro-i-gay-a-un-convegn0-sullhiv)**

  
**Sostenere le iniziative dell’associazione radicale Certi Diritti costa meno di un caffè a settimana. [Diventa protagonista delle battaglie contro la sessuofobia e per i diritti civili >](iscriviti)**  
**  
[Seguici su twitter >](http://twitter.com/#!/certidiritti)**