---
title: 'Discussione su unioni civili a Milano: Certi Diritti raccoglierà le firme davanti Palazzo Marino'
date: Mon, 23 Jul 2012 08:55:50 +0000
draft: false
tags: [Diritto di Famiglia]
---

Oggi prima della discussione in Consiglio Comunale sul registro delle unioni civili, l'Associazione Radicale Certi Diritti raccoglierà le firme per la proposta di delibera d'iniziativa popolare sulle famiglie di fatto e sugli altri 4 testi (www.milanoradicalmentenuova.it). Poi diretta twitter dall'aula con hastag #registromilano.

Comunicato stampa dell'Associazione Radicale Certi Diritti

Milano, 23 luglio 2012

In occasione della discussione in Consiglio Comunale sul registro delle unioni civili, l'Associazione Radicale Certi Diritti raccoglierà le firme davanti per la proposta di delibera d'iniziativa popolare sulle famiglie di fatto e sugli altri 4 testi (**www.milanoradicalmentenuova.it**) davanti a Palazzo Marino dalle 16:00 alle 17:30. La pressione della raccolta firme ha già prodotto il risultato di portare in discussione il registro prima della fine della stessa, oggi vogliamo ricordare che già più di 4000 cittadini hanno sottoscritto un testo simile a quello in discussione e che quindi, nel caso in cui non dovesse essere approvato o uscisse stravolto e snaturato, un nuovo testo verrà presentato appena conclusa la raccolta.

**Dalle 17:30 ci sposteremo in aula consigliare da dove faremo una diretta Twitter del dibattito sull'account @certidiritti. L'hashtag sarà: #registromilano.**