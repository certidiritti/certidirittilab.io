---
title: 'Perché siamo favorevoli al matrimonio tra cattolici'
date: Tue, 17 May 2011 09:34:11 +0000
draft: false
tags: [Matrimonio egualitario]
---

Dal libro di Franco Buffoni "Alfabeto laico in salsa gay piccante". Circolò in Spagna prima di Zapatero questo documento. Continueremo a diffonderlo finché l’Italia non si desterà.

Siamo completamente favorevoli al matrimonio tra cattolici. Ci pare un errore e un’ingiustizia cercare di impedirlo. Il cattolicesimo non è una malattia. I cattolici, nonostante a molti non piacciano o possano sembrare strani, sono persone normali e devono godere degli stessi diritti della maggioranza, come se fossero, ad esempio, informatici od omosessuali.  
   
Siamo coscienti che molti comportamenti e aspetti del carattere delle persone cattoliche, come la loro abitudine a demonizzare il sesso, possono sembrarci strani. Sappiamo che a volte potrebbero emergere questioni di sanità pubblica, a causa del loro pericoloso e deliberato rifiuto all’uso dei profilattici. Sappiamo anche che molti dei loro costumi, come l’esibizione pubblica di immagini di torturati, possono dare fastidio a tanti. Però tutto ciò risponde più a un’immagine mediatica che alla realtà e non è un buon motivo per impedire loro il diritto al matrimonio.  
   
Alcuni potrebbero argomentare che un matrimonio tra cattolici non è un vero matrimonio, perché per loro si tratta di un rito e di un precetto religioso assunto davanti al loro dio, anziché di un contratto tra due persone. Inoltre, dato che i figli nati fuori dal matrimonio sono pesantemente condannati dalla Chiesa cattolica, qualcuno potrebbe ritenere che - permettendo ai cattolici di sposarsi - si incrementerà il numero dei matrimoni “riparatori” o volti alla semplice ricerca del sesso (proibito dalla loro religione fuori dal matrimonio), andando così ad aumentare i casi di violenza familiare e le famiglie problematiche. Bisogna però ricordare che questo non riguarda solo le famiglie cattoliche e che, siccome non possiamo metterci nella testa degli altri, non possiamo giudicare le loro motivazioni.  
   
Inoltre, dire che non si dovrebbe chiamarlo matrimonio, ma in un’altra maniera, non è che la forma, invero un po’ meschina, di sviare il problema su questioni lessicali del tutto fuori luogo. Anche se cattolici, un matrimonio è un matrimonio e una famiglia è una famiglia!  
   
E con questa allusione alla famiglia, passiamo all’altro tema incandescente, che speriamo non sia troppo radicale: siamo anche favorevoli a che i cattolici adottino bambini. Qualcuno si potrà scandalizzare. È probabile che si risponda con un’affermazione del tipo: “Cattolici che adottano bambini?!? I bambini potrebbero diventare a loro volta cattolici!”. A fronte di queste critiche, possiamo rispondere che è ben vero che i bambini figli di cattolici hanno molte probabilità di diventare a loro volta cattolici (a differenza dei figli degli omosessuali o degli informatici), ma abbiamo già detto che i cattolici sono gente come tutti gli altri. Nonostante le opinioni di qualcuno e alcuni indizi, non ci sono tuttavia prove che dimostrino che i genitori cattolici siano meno preparati di altri a educare figli, né che l’ambiente religiosamente orientato di una casa cattolica abbia un’influenza negativa sul bambino.  
   
Infine i tribunali per i minori esprimono pareri sulle singole situazioni, ed è precisamente loro compito determinare l’idoneità dei possibili genitori adottivi.  
   
In definitiva, nonostante l’opinione contraria di alcuni, crediamo che bisognerebbe permettere ai cattolici di sposarsi e di adottare dei bambini. Esattamente come agli informatici e agli omosessuali.  
   
(Circolò in Spagna prima di Zapatero questo documento. Continueremo a diffonderlo finché l’Italia non si desterà).