---
title: 'OMOFOBIA E TRANSFOBIA: RIPRESENTEREMO IN AULA EMENDAMENTI MIGLIORATIVI RESPINTI IN COMM. GIUSTIZIA'
date: Tue, 06 Oct 2009 11:41:24 +0000
draft: false
tags: [Comunicati stampa]
---

**OMOFOBIA E TRANSFOBIA: LA COMMISSIONE GIUSTIZIA HA RESPINTO GLI EMENDAMENTI MIGLIORATIVI.**

**LA MAGGIORANZA E IL GOVERNO CONTINUANO A IGNORARE IL GRAVE FENOMENO DELLA VIOLENZA CONTRO LE PERSONE TRANSESSUALI.**

**RIPRESENTEREMO GLI STESSI EMENDAMENTI IN AULA.**

Roma, 6 ottobre 2009

Dichiarazione dell’Associazione Radicale Certi Diritti.

Oggi pomeriggio la Commissione Giustizia della Camera dei deputati ha votato contro tutti gli emendamenti proposti dalla deputata radicale Rita Bernardini, elaborati insieme all’Associazione Radicale Certi Diritti, con il contributo di associazioni ed esponenti della comunità lgbt. Gli emendamenti migliorativi, tra gli altri, avevano lo scopo di inserire anche la lotta alla violenza contro le persone transessuale. La decisione di non voler considerare la transfobia è grave, ipocrita e non tiene conto di quanto le Associazioni lgbt italiane richiedono da molti anni alla classe politica.

Gli stessi emendamenti verranno riproposti la prossima settimana in aula. Lunedì 12 ottobre difatti inzierà la discussione generale sul provvedimento e il voto finale della Camera dei deputati è previsto martedì 13 ottobre.

Giovedì 8 ottobre all’incontro con il Ministro Carfagna delle Associazioni lgbt italiane chiederemo di ripensare la decisione manifestata dal Governo oggi in Commissione Giustizia di netta contrarietà a ogni dialogo e apertura.

**Occorre che tutte le associazioni lgbt si mobilitino nelle prossime ore affinché i parlamentari vengano sensibilizzati sul tema.**

**La manifestazione nazionale contro l’omofobia di sabato 10 ottobre dovrà diventare necessariamente una grandissima mobilitazione contro l’indifferenza, l’ipocrisia, la violenza che con questa ottusità della classe politica non si vuole combattere.**