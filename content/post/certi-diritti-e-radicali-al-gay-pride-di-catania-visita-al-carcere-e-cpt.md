---
title: 'CERTI DIRITTI E RADICALI AL GAY PRIDE DI CATANIA: VISITA AL CARCERE E CPT'
date: Thu, 03 Jul 2008 17:20:18 +0000
draft: false
tags: [Comunicati stampa]
---

DELEGAZIONE RADICALE E ASSOCIAZIONE CERTI DIRITTI E A CATANIA SABATO 5 E DOMENICA 6 LUGLIO: VISITA AL CARCERE, AL CPT E AL CENTRO DI PRIMA ACCOGLIENZA. GAY PRIDE CITTADINO CON RITA BERNARDINI, DEPUTATA RADICALE – PD, SERGIO ROVASIO, SEGRETARIO DI CERTI DIRITTI E GIANMARCO CICCARELLI, SEGRETARIO ASS. RADICALI CATANIA.

  
Di seguito il Programma della due giorni

**_Sabato 5 luglio:_**Ore 11 Visita al Carcere di Catania di Rita Bernardini, deputata radicale Pd**;** Sergio Rovasio, Segretario Associazione Radicale Certi Diritti; Gian Marco Ciccarelli, Segretario ass. Radicali Catania.ore 12.30 **Conferenza Stampa** davanti al Carcere di Catania (Piazza Lanza, 11)su: "Pacchetto Sicurezza, emergenza carceri e centri di identificazione ed espulsione (ex Cpt); diritti civili, gaypride di Catania; ore 17.30 partecipazione al Gay Pride di Catania; a seguire interventi dal palco di Rita Bernardini in rappresentanza dei radicali ed Eduardo Melfi in rappresentanza dell'Associazione Radicale Certi Diritti. **_Domenica 6 luglio:_**ore 11 Visita al Centro di Identificazione ed espulsione di Pian del Lago – Caltanissetta;ore 12.30 Davanti al Centro, incontro con la Stampa sulla situazione del Cie (ex Cpt);ore 16 Visita al Centro di Prima Accoglienza  di Cassibile (Siracusa) principali strutture di detenzione dei migranti sbarcati in Sicilia