---
title: 'Associazione Radicale Certi Diritti incontra Ministro degli Interni su Circolare Amato. A breve la risposta'
date: Fri, 15 Jun 2012 08:54:20 +0000
draft: false
tags: [Politica]
---

L’associazione è da anni impegnata nella richiesta di ritiro della circolare Amato del 2007 che vieta ai comuni le trascrizioni dei matrimoni tra persone dello stesso sesso celebrati all'estero.

Roma, 15 giugno 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti  
   
A margine del Convegno "Immigrazione: una sfida e una necessità" organizzato ieri dal Partito Radicale, Sergio Rovasio, membro della Segreteria dell'Associazione Radicale Certi Diritti, ha incontrato il Ministro degli Interni Anna Maria Cancellieri sulla questione della richiesta di ritiro della Circolare "Amato" del 2007 che vieta ai Comuni la trascrizione dei matrimoni tra persone dello stesso sesso celebrati all'estero per motivi di "ordine pubblico". Il Ministro ha affermato che è in corso un esame approfondito della questione e che presto ci farà sapere quali decisioni saranno prese.

L'Associazione radicale Certi Diritti ringrazia il Ministro degli Interni per l'interessamento che ha voluto manifestare, e si augura che presto possa essere ritirata quella circolare e ripristinare così la libera circolazione in Europa, quindi in Italia, di persone e famiglie, così come l'Unione Europea prevede e ci richiede con Direttive e diverse Risoluzioni votate anche dal Parlamento Europeo.