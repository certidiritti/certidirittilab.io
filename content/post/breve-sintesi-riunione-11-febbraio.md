---
title: 'BREVE SINTESI RIUNIONE 11 FEBBRAIO'
date: Sat, 13 Feb 2010 08:00:45 +0000
draft: false
tags: [Senza categoria]
---

Carissimi,  vi aggiorniamo brevemente sulla riunione di ieri.

  
1) In attesa di indicazioni più precise da parte degli organizzatori, tendenzialmente possiamo aspettarci che verrà organizzata a Roma una manifestazione nazionale domenica 21 Marzo, giorno di primavera: segnatevi la data e partecipiamo!

  
2) La festa di autofinanziamento del 6 Marzo non è più confermata.

  
3) La manifestazione del 10 Aprile è potenzialmente confermata. Ovviamente, essendoci la sentenza della corte due settimane prima, non potrà essere più come l'avevamo pensata. E' stato deciso di mantenere un profilo "light" per l'eventuale giornata, un po' come è stato per il 17 Maggio: senza palco e altre strutture. Procederemo subito per i permessi. Poi, solo dopo la sentenza del 23 Marzo, ci sintonizzeremo sul da farsi.

  
4) Le coppie milanesi hanno deciso di mettere a disposizione di tutte le coppie di Affermazione Civile quanto avevano già preparato in vista del 10 Aprile: video, logo "Isi" delle coppie, l'idea dei doppi fiocchi bianchi, una lettera da spedire ai VIP...

  
5) Proporremo al Comitato "Sì lo voglio" un volantino da distribuire in città per informare le persone sulla data del 23 Marzo  
Inoltre:  a) Marco ci invierà una mail per farci sapere della giornata del 14 Febbraio a Magenta. E' la prima di una serie di "uscite" in alcune città lombarde, organizzata da tutte le associazioni LGBT Milanesi (Coordinamento Arcobaleno) b) Luca, ci farà sapere come procede la raccolta firme necessaria a formalizzare le candidature dei radicali italiani per le regionali lombarde  
  
Ciao! Gian mario, Luca, Francesco e Yuri