---
title: 'Una ventenne fa sesso con minori consenzienti. E'' violenza?'
date: Sat, 25 Aug 2012 14:02:47 +0000
draft: false
tags: [Politica]
---

![LogoCD](http://www.radicalparty.org/file/Logo_Cd_2012.JPG)

questa settimana alcuni quotidiani hanno riportato la notizia di una **ventenne arrestata per aver fatto sesso con ragazzi minori consenzienti**. Ecco un esempio dei titoli utilizzati:  
La ventenne che «violenta» i minorenni (Corriere.it)      
Ventenne salernitana «pedofila» adescava ragazzini nel parco (Il Mattino)      
**Per il giornalista ma soprattutto per il pm una ventenne che ha rapporti sessuali con ragazzi che la contattano compie una violenza sessuale. **Siamo convinti che su un tema come questo, in un paese malato di sessuofobia e moralismo, occorra aprire un dibattito. Lo facciamo insieme al Segretario dell’associazione Yuri Guaiana nella rassegna stampa settimanale che puoi**[ascoltare online](http://www.radioradicale.it/scheda/356793/fuor-di-pagina-la-rassegna-stampa-di-certi-diritti)** oppure** [scaricare come podcast](http://itunes.apple.com/it/podcast/radioradicale-fuor-di-pagina/id530466683).**  
**Dal 21 luglio la potrai ascoltare su Radio Radicale ogni venerdì alle 24,30.**

Intanto la campagna ‘Dai corpo ai tuoi diritti’, lanciata per raccogliere iscrizioni ma anche per portarel’attenzione sui temi della sessuofobia, continua con successo.               
**[La censura da parte di Facebook della foto di nudo integrale](http://www.certidiritti.org/2012/07/10/facebook-censura-la-foto-di-nudo-integrale-per-la-campagna-dai-corpo-ai-tuoi-diritti/)** ha aperto un interessante dibattito anche grazie al **[post di Vittorio Zambardino su Libernazione.it](http://libernazione.it/nudo-si-tolga-e-non-si-stampi/)**. Continuano ad arrivare foto ed iscrizioni, cosa per noi fondamentale perché non riceviamo finanziamenti pubblici e tutto quello che facciamo è solo grazie al** [contributo di iscritti e sostenitori](partecipa/iscriviti/itemlist/category/108)**.

Grazie alle foto la nostra pagina su Facebook ha superato i 6mila fan, 94mila utenti hanno visualizzato i nostri post e 2.400 li hanno condivisi o commentati.  
**Ma i vostri amici conoscono Certi Diritti e la campagna 'Dai corpo ai tuoi diritti'?   
**Perché non ci aiuti a farglielo sapere? Invia loro un messaggio con **[il link all'album](http://www.flickr.com/photos/certidiritti/sets/72157630288308052/with/7443467088/)** e chiedi se vogliono partecipare inviando la propria foto a [info@certidiritti.it](mailto:<a href=)" target="_blank">[info@certidiritti.it](mailto:info@certidiritti.it) o contribuire all’associazione.

Perché sostenerci? Perché **Certi Diritti è nata con un obiettivo: passare dalle parole ai fatti.** Dalla **[campagna di Affermazione Civile](campagne-certi-diritti/itemlist/category/86-affermazione-civile)** che continua con iniziative legali per il riconoscimento del matrimonio tra persone dello stesso sesso alle firme affinché i comuni non discriminino le famiglie non matrimoniali, dallo straordinario lavoro dei parlamentari radicali, che hanno depositato oltre 10 proposte di legge su questi temi, al **[manifesto/appello per i dirittidelle e dei sex worker che ti invitiamo a firmare](campagne-certi-diritti/itemlist/category/85-legalizzazione-prostituzion%20e).**  
   
- Certi Diritti insieme ad Arcigay e altre associazioni lancia un appello a Governo e Partiti:  
**[Non cancellate l’UNAR, l’ente contro tutte le discriminazioni](http://www.certidiritti.org/2012/07/12/appello-a-governo-e-partiti-non-cancellate-lunar-lente-contro-tutte-le-discriminazioni/)** finito sotto la scure della spending review.  
\- A Gaeta continuano le ronde di vigilantes armati. Dopo l'interrogazione parlamentare dei Radicali,**[un’altra ne viene presentata alla regione Lazio.](http://www.certidiritti.org/2012/07/12/spiaggia-dellarenauta-di-gaeta-continuano-intimidazionidiversi-gruppi-chiedono-chiarimenti-alla-regione-lazio/)**

\- Continua la raccolta firme a Milano. Il Pd: libertà di coscienza ai cattolici su unioni civili. **["E' inaudito che sulla questione diritti civili si utilizzi il termine 'libertà di coscienza'.](http://www.gay.tv/news/attualita/milano-unioni-civili-pd/)** 

\- denuncia della Lila: Ministero della Difesa chiede test Hiv per suonare nella banda o per accedere alle scuole dell'Esercito.** [Esposto all'Unar e all'Oscad >](http://www.certidiritti.org/2012/07/06/denuncia-della-lila-ministero-della-difesa-chiede-test-hiv-per-suonare-nella-banda-o-per-accedere-alle-scuole-dellesercito-esposto-allunar-e-alloscad/)**

**- [il Prefetto Cirillo e una delegazione dell'Oscad incontra i rappresentanti della nostra associazione.](http://www.certidiritti.org/2012/07/11/incontro-con-loscad-dellassociazione-radicale-certi-diritti-riguardo-le-disposizioni-discriminatorie-di-ministeri-interni-e-difesa/) **E’ stata confermata la richiesta di modifica della circolare discriminatoria delMinistero degli Interni verso le persone LGBT e del Ministero della Difesa su persone con hiv.

\- Parlamentari Radicali depositano **[interrogazione urgente per ritiro Circolare Amato](http://www.certidiritti.org/2012/07/11/parlamentari-radicali-depositano-interrogazione-urgente-per-ritiro-circolare-amato-che-alimenta-discriminazioni/)** che vieta la trascrizione dei matrimoni gay contratti all’estero perché contrari all’ordine pubblico.

**- [Idv deposita pdl per estendere il matrimonio civile alle coppie dello stesso sesso](http://www.certidiritti.org/2012/07/06/benvenuta-idv-su-pdl-pro-matrimonio-persone-stesso-sesso-insieme-ai-radicali-e-on-paola-concia-continua-il-cammino-per-superamento-diseguaglianze/)** chesi aggiunge a quelle dei Radicali e dell’On. Paola Concia del Pd.

**- [Giovanardi e i gay: da gennaio 31 dichiarazioni sui gay. Certi Diritti cerca di capire il perchè con un sondaggio](http://www.certidiritti.org/2012/07/07/giovanardi-e-i-gay-da-gennaio-31-dichiarazioni-sui-gay-certi-diritti-cerca-di-capire-il-perche-con-un-sondaggio/)** Esprimi la tua scelta e leggi i primi risultati sul nostro sito.

**- [Intervista di Radio Radicale a Sergio Rovasio sul manuale dei Carabinieri che definisce i gay “degenerati](http://www.radioradicale.it/scheda/356680/intervista-a-sergio-rovasio-su-manuale-dei-carabinieri-che-definisce-i-gay-degenerati)” **errore poi … **[corretto dall’Arma](http://www.tmnews.it/web/sezioni/top10/20120711_150617.shtml)**

- L’Associazione Radicale Certi Diritti aderisce alla Campagna** “[Vorrei ma non posso – It’s  Wedding Time](http://www.vorreimanonposso.org/tag/certi-diritti/)”**.

**- [delegazione dell'Associazione Radicale Certi Diritti partecipa al gay pride di Budapest boicottato da sindaco e dal governo Orban >](http://www.certidiritti.org/2012/07/07/delegazione-dellassociazione-radicale-certi-diritti-al-gay-pride-di-budapest-sindaco-e-governo-orban-lo-boicottano/)**

**\- da Liberi.tv:  [le attività dell'Associazione Radicale Certi Diritti, intervista al Segretario Yuri Guaiana](http://www.liberi.tv/webtv/2012/07/06/video/attivit%C3%A0-dellassociazione-radicale-certi-diritti-intervista-al)** intervista di Riccardo Cristiano.

  
**[www.certidiritti.it/partecipa/iscriviti](partecipa/iscriviti) **