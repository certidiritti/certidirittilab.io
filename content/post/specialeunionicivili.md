---
title: 'FuorDiPagina: speciale Unioni Civili'
date: Mon, 26 Jan 2015 21:10:02 +0000
draft: false
---

![FDP speciale UC](http://www.certidiritti.org/wp-content/uploads/2015/01/FDP-speciale-UC.png)

**Ogni martedì alle 23.30 su**

![radio-radicale-logo-142675](http://www.certidiritti.org/wp-content/uploads/2015/01/radio-radicale-logo-142675.jpg) **Fuor di Pagina**, la rassegna stampa del martedì sulle libertà sessuali a cura dell’Associazione Radicale Certi Diritti, **darà spazio per qualche tempo a un ciclo di approfondimenti sull’iter delle unioni civili in Commissione giustizia al Senato**. Ogni settimana spazio a parlamentari e giuristi per approfondire il dibattito attorno al ‘ddl Cirinnà’.  

Martedì 20 gennaio 2015, ospite in collegamento **Sen. Enrico Cappelletti** (M5s)

 

Martedì 27 gennaio 2015, ospite in collegamento **Sen. Sergio Lo Giudice** (PD)

Martedì 10 febbraio2015, ospite in collegamento **Bruno De Filippis** (Esperto di Diritto di Famiglia)

Martedì 17 febbraio2015, in collegamento **Yuri Guaiana **(Associazione Radicale Certi Diritti), **Giusepppina La Delfa** (Famiglie Arcobaleno) e **Flavio Romani** (Arcigay)

 Martedì 24 febbraio2015, in collegamento **Franco Grillini **(Gaynet), **Paola Brandolini** (ArciLesbica) e **Aurelio Mancuso** (Equality Italia)