---
title: 'UNIONI CIVILI. RADICALI: OGGI PRIMO PASSO, CONTINUA BATTAGLIA PER EGUAGLIANZA'
date: Wed, 11 May 2016 14:20:10 +0000
draft: false
tags: [Diritto di Famiglia]
---

[![ed610085_smush_parl_3](http://www.certidiritti.org/wp-content/uploads/2014/06/ed610085_smush_parl_3-300x110.png)](http://www.certidiritti.org/wp-content/uploads/2014/06/ed610085_smush_parl_3.png)“Il voto di oggi segna solo un primo passo verso la conquista in Italia di diritti che in tanti altri paesi sono già storia: le unioni civili che usciranno dalla Camera dei deputati, infatti, sono lontane dall’essere la soluzione definitiva ai problemi delle coppie omosessuali e dei loro figli, esclusi dal provvedimento nel corso della bagarre al Senato. Non sono l’azione di riforma complessiva di un diritto di famiglia che da decenni non riflette più le esigenze di una società in mutamento. Oggi, quindi, siamo felici per le coppie che da troppo tempo aspettavano questo momento, già da domani saremo di nuovo al lavoro, anche con tutti gli strumenti giuridici a nostra disposizione, per una riforma della legge in senso egualitario. Continua la battaglia radicale per assicurare davvero pari diritti a tutti i cittadini”.

Lo dicono Riccardo Magi, segretario di Radicali Italiani, e Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti, commentando la fase finale dell’iter parlamentare delle unioni civili.

Roma, 11 maggio 2016