---
title: 'COPPIA GAY DI SAVONA CHIEDE AI POLITICI IL RICONOSCIMENTO DELLA LORO UNIONE'
date: Thu, 12 Nov 2009 07:28:09 +0000
draft: false
tags: [Comunicati stampa]
---

**Prosegue la battaglia di Francesco Zanardi e Manuel Incorvaia** **per il  
riconoscimento giuridico della loro unione.**  
Dopo l'invio al presidente della Repubblica Napolitano e la pubblicazione su Facebook che ha registrato una valanga di consensi, la coppia omosessuale di Savona ha inviato una lettera (tradotta in francesce e inglese) ad oltre 700 eurodeputati.  
A sostegno della lotta di civiltà di Manuel e Francesco, importanti associazioni nazionali e internazionali.

Oggetto: Questa lettera è stata spedita oggi a 737 tra eurodeputati italiani ed europei.

Lettera APERTA agli  eurodeputati italiani e a tutti i deputati europei

Signor deputato,

Le scrivo per porgere alla Sua attenzione un caso significativo, per il  
quale la nostra Nazione è già stata più volte richiamata, uno dei tanti che offusca da anni il  
prestigio della Repubblica Italiana agli occhi degli altri Paesi dell'Europa unita.

Io, cittadino italiano, omosessuale, vorrei potermi sentire davvero  
fiero di questo mio Stato, ma stando così le cose non riesco a sentirmi tale poiché non vedo  
riconosciuti alcuni dei miei fondamentali diritti costituzionali come quello di potermi unire in matrimonio con la persona che amo e di poterla tutelare, diritto tra l'altro giustamente concesso e riconosciuto praticamente tutto il resto dell'Europa.  
Il mio nome è  Francesco Zanardi, ho 39 anni, sono di Savona. Il mio  
compagno è Manuel Incorvaia, 22 anni, anche lui di Savona.  
Non scriviamo per chiedere un riconoscimento legale dovuto ad un capriccio  
personale o ad un'altra futile scusa, ma perché ne abbiamo realmente  
bisogno, e come noi tantissime altre coppie dello stesso sesso.

Una sera, durante un viaggio in Grecia io, Francesco, sono stato quasi ridotto in fin di vita e Manuel., il mio amato compagno, ha rischiato di perdere, non solo me, la persona che ama, ma  gran parte del rapporto concreto costruito in anni di convivenza.  
Ma non solo: Manuel perdendo me, suo unico bene e punto di riferimento, avrebbe perso il senso stesso della sua vita.

Infatti , se fossi deceduto in seguito ad un’aggressione omofobica subita,  
Manuel si sarebbe trovato in mezzo alla strada e la casa dove abbiamo vissuto assieme  
per tanto tempo, non avrebbe potuto più essere sua, poiché sono io il  
titolare di tutti i contratti ad essa relativi e lui, giuridicamente, “non  
esiste”.  
Noi non chiediamo niente di più, rispetto agli altri, vorremmo solo il riconoscimento legale del diritto al matrimonio, peraltro già sancito dalla carta Costituzionale e applicabile ad ogni cittadino italiano.

Vorremmo infatti farLe notare, anche se Lei li conoscerà benissimo, quanto  
contenuto in alcuni articoli della Costituzione Italiana e della carta dei diritti:

  
Art. 21 sella Carta dei diritti fondamentali dell'unione europea vieta ogni  
forma di discriminazione: COMPRESA QUELLA BASATA SULL'ORIENTAMENTO SESSUALE.

  
La risoluzione dell'8 febbraio 1994  del Parlamento Europeo raccomanda di far cessare la proibizione a contrarre matrimonio alle coppie dello stesso sesso e invita a garantire loro tutti i diritti e  
benefici dell'istituto matrimoniale e i correlati obblighi e doveri. Da allora ci sono stati ripetuti solleciti e raccomandazioni in merito: l'Italia è stata anche richiamata, ma non ha fatto NULLA.

  
L'Italia ha sottoscritto il trattato di Lisbona, che prevede tutela per le minoranze discriminate, anche per gay lesbiche e trans. Ovviamente anche su questo l'Italia è inadempiente.

Articolo 3 della Costituzione.

Tutti i cittadini hanno pari dignità sociale e sono eguali davanti alla legge, senza distinzione di sesso, di razza, di lingua, di religione, di opinioni politiche, di condizioni personali e sociali.

È compito della Repubblica rimuovere gli ostacoli di ordine economico e sociale, che, limitando di fatto la libertà e l'eguaglianza dei cittadini, impediscono il pieno sviluppo della persona umana e l'effettiva partecipazione di tutti i lavoratori all'organizzazione politica, economica e sociale del Paese.

Articolo 7 della Costituzione.

Lo Stato e la Chiesa cattolica sono, ciascuno nel proprio ordine, indipendenti e sovrani.  
I loro rapporti sono regolati dai Patti Lateranensi. Le modificazioni dei Patti, accettate dalle due parti, non richiedono procedimento di revisione costituzionale.

Quindi mi ritrovo a chiedereLe, egregio deputato, per quale motivo certe importanti leggi italiane o dell'Europa unita, non vengano applicate.

Non servirebbero referendum o votazioni di massa, in quanto tali leggi sono all'interno della Costituzione, accettata e voluta dall'Assemblea Costituente. È un po' come la bibbia dei laici: che piaccia o meno, nella Costituzione è scritto che la famiglia esiste già, indipendentemente dal  
sesso dei due individui primi, è anche scritto (articolo 3) che la Chiesa o chi per essa deve restare fuori dalla sfera politica.

Alla luce di questi fatti la domanda è: perché tanto egoismo e cecità verso l'amore, visto che riconoscere la nostra unione civile non comporterebbe alcunché se non l’applicazione di sacrosanti diritti?  
 Le ovvie ragioni parlano direttamente al cuore: solo chi ha visto in faccia la morte,  chi è arrivato al tragico punto di desiderare di non svegliarsi il giorno dopo, ritenendo inutile e meschina la propria esistenza, solo costoro possono capire appieno certe cose.  
Solo chi ha sofferto può conoscere la sofferenza.  
Il nostro rapporto  è unico e speciale e poiché nella nostra storia ci sono anche implicazioni serie di salute, pensiamo sia giusto offrirci reciprocamente la possibilità di vedere riconosciuti diritti pari a tutti gli altri cittadini di questo Paese.

La nostra vita non è facile, non è stata facile e probabilmente non lo sarà.

Noi siamo tutto ciò che abbiamo. Vorremmo poterci aiutare e soccorrere, in salute e malattia, ricchezza e povertà come tutte le coppie del mondo che si amano.

A nome mio, di Manuel e dei molti cittadini omosessuali e non che sostengono la nostra causa,  porgo i più sinceri e cordiali saluti.

 Francesco Zanardi e Manuel Incorvaia Savona 10/11/2009