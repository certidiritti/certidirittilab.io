---
title: 'certi diritti alla manifestazione delle donne, con le prostitute'
date: Sun, 13 Feb 2011 13:39:20 +0000
draft: false
tags: [Comunicati stampa, pia covre, Prostituzione, rovasio, sex worker, unita]
---

Domenica, 13 febbraio 2011  
Comunicato Stampa dell’Associazione Radicale Certi Diritti

Anche se alla manifestazione di oggi l’Associazione Radicale Certi Diritti non ha aderito, una delegazione composta, tra gli altri, dal Segretario Sergio Rovasio e dal Tesoriere Giacomo Cellottini, si recherà in Piazza del Popolo alle ore 14 al fianco delle prostitute che manifesteranno con l’ombrello rosso in Piazza del Popolo.

Siamo per **la legalizzazione dell’attività delle sex workers contro le ipocrisie e le falsità degli pseudo-moralisti e perbenisti di ogni genere** che di giorno promuovono leggi proibizioniste e criminogene con le quali poi campano di notte.  
Le ragioni della presenza della delegazione di Certi Diritti sono anche raccolte in una parte del Comunicato Stampa diffuso dal Comitato per i diritti civili delle prostitute: “Ci saremo perchè non accettiamo che il nostro Paese sia trascinato nel fango da una classe politica che ci ha ridotti ad una democrazia degenerata. Perchè non accettiamo di essere usate, infangate e strumentalizzate per la restaurazione di una morale sessuale stantia che soffoca le aspirazioni e le libertà di ogni donna. 

Perchè rifiutiamo la divisione patriarcale tra Donne per bene e Donne per male. Noi donne tutte lavoriamo nei tanti servizi informali di cura, siamo il pilastro portante delle famiglie, ma senza reddito. Siamo impegnate a vita nei lavori che attengono alla sfera  riproduttiva,  e anche in questo dobbiamo dipendere dagli uomini perchè il nostro lavoro non viene pagato e se è salariato non vale mai come quello maschile.

La condizione delle lavoratrici del sesso o delle prostituite è drammaticamente peggiorata a causa dell'ipocrisia e dell'ingiustizia delle politiche sulla sicurezza e sull'immigrazione”.