---
title: 'Europride 2011: i diritti civili e umani in Italia e nel mondo'
date: Mon, 06 Jun 2011 20:33:57 +0000
draft: false
tags: [Europa]
---

La lotta dei Radicali contro la sessuofobia e le discriminazioni, per il superamento delle  
diseguaglianze. Con EMMA BONINO, MARCO PANNELLA, PAOLO PATANE' E KASHA NABAGESERA dall'Uganda

**giovedì 9 giugno alle ore 15,15 presso la sede del Partito Radicale in via di Torre Argentina, 76 a Roma conferenza Stampa con:**

Emma BONINO, Vice Presidente del Senato

Marco PANNELLA, leader dei Radicali

Kasha Jacqueline NABAGESERA, Director Far Uganda  
(collega di David Kato Kisule, ucciso in Uganda lo scorso gennaio)

Paolo PATANE', Presidente nazionale di Arcigay

Rita DE SANTIS, Presidente Agedo

Sergio Stanzani, Presidente del Partito Radicale Nonviolento

Mario Staderini, Segretario di Radicali Italiani

Enzo CUCCO, Direttore Fondazione Fuori!

Sergio ROVASIO, Segretario Associazione Radicale Certi Diritti