---
title: 'APPELLO ESPONENTI COMUNITA'' LGBT PER APPROVARE PDL ANTI OMO E TRANSFOBIA'
date: Mon, 12 Oct 2009 11:46:40 +0000
draft: false
tags: [Senza categoria]
---

**APPELLO AL PARLAMENTO**

**APPROVATE IL PROVVEDIMENTO CONTRO L’OMOFOBIA E LA TRANSFOBIA**

_Le attuali sottoscrizioni all’appello rappresentano una lista parziale perché sono in continuo aggiornamento_

_Egregi Onorevolioggi inizia la discussione nell’Aula della Camera dei Deputati sulle norme aggravanti per i delitti ai danni delle persone omosessuali._

_

Oggi inizia la discussione nell’Aula della Camera dei Deputati sulle norme aggravanti per i delitti ai danni delle persone omosessuali.

La relatrice della legge è Anna Paola Concia, unica parlamentare dichiaratamente omosessuale, che in questo anno ha tentato tutte le strade per ottenere un risultato rispetto al tema.

La ringraziamo, così come ringraziamo tutte e tutti i parlamentari che hanno proposto in Commissione Giustizia e che riproporranno in aula emendamenti che intendono migliorare il testo base.

Vi chiediamo di prestare attenzione a quello che sta accadendo nel Paese: negli ultimi anni gli omicidi, le aggressioni, le violenze nei confronti delle persone gay, lesbiche, trans sono aumentati in misura preoccupante. Per queste ragioni avevamo proposto, l’immediata e più corretta estensione della legge Mancino che prevede sanzioni e aggravanti rispetto ai delitti d’odio.

Nel Parlamento questa strada non riscuote il consenso della maggioranza dei/delle deputate, per cui avete ricercato una mediazione, che si esplicita con il provvedimento che oggi avvia il suo iter. È un provvedimento che non ci soddisfa, che non contiene per esempio le aggravanti per i delitti contro l’onore (insulti, minacce) né rispetto ai danni procurati al patrimonio (beni mobili e immobili). Soprattutto è stato eliminato un riferimento esplicito e chiaro rispetto alle persone trans.

Su questo ultimo punto anche il ministro Mara Carfagna si è espressa decisamente a favore dell’immediata reintroduzione. Le persone trans, sono le più esposte e colpite da ogni tipo di angherie, l’Italia ha il triste primato di essere lo Stato, nel mondo occidentale, dove sono assassinate il più alto numero di persone trans.

Facciamo appello al senso di giustizia: date un segno tangibile che anche a noi è riconosciuta la possibilità di ottenere un primo piccolo passo verso la piena cittadinanza.

Vi chiediamo di reintrodurre la norma che tutela le persone trans e di votare approvare così un provvedimento, che in questo momento storico assume i connotati di una risposta politica e assunzione di responsabilità da parte del Parlamento italiano.

  

Aurelio Mancuso presidente nazionale Arcigay

**_Francesca Polo presidente nazionale ArciLesbica_**

**_Giuseppina La Delfa presidente nazionale Famiglie Arcobaleno_**

**_Rita De Santis presidente nazionale Agedo – Genitori di figli omosessuali e trans_**

**_Fabianna Tozzi presidente nazionale Associazione Transgenere_**

**_Franco Grillini presidente GayNet_**

**_Enrico Oliari presidente nazionale GayLib_**

**_Imma Battaglia presidente Di’ Gay Project_**

**_Sergio Rovasio segretario nazionale Associazione Radicale Certi Diritti_**

**_Vladimir Luxuria, artista_**

**_Fabrizio Marrazzo presidente Arcigay Roma_**

**_Nicola Cicchiti presidente nazionale Polis Aperta_**

**_Gabriele Murgia segretario Coordinamento Torino Pride LGBT_**

**_Carlo Santacroce presidente Associazione 3D Bologna_**

**_Stefano Aresi presidente Circolo di Cultura Omosessuale Milk Milano_**

**_Martina Castellana direttivo nazionale GayLib_**

**_Carlo Cremona presidente I-ken Napoli_**

**_Fabrizio Ungaro presidente Ireos Firenze_**

**_Filippo Riniolo presidente nazionale Chroma_**

**_Francesco Piomboni presidente Arcigay Firenze_**

**_Daniele Brosolo presidente Arcigay Udine_**

**_Fabio Astrobello presidente Arcigay Reggio Emilia_**

**_Giorgio Dell’Amico presidente Arcigay Modena_**

**_Flavio Romani presidente Arcigay Ferrara_**

**_Alessandro Tosarelli presidente Arcigay Rimini_**

**_Lorenzo Starnini, a nome di Arcigay Perugia_**

**_Emanuele Sigismondi presidente Arcigay Cuneo_**

**_Stefano Co’ presidente Arcigay Trentino_**

**_Ivan Belloni, a nome di Arcigay Pesaro-Urbino_**

**_Calogero Cavataio presidente Arcigay Livorno_**

**_Mauro Cioffari   responsabile sito informativo [GayRoma.it](http://gayroma.it/)_**

**_Guido Allegrezza_**

**_Stefano Bucaioni_**

**_Marco Tagliatela_**

**_Andrea Benedino consigliere nazionale Arcigay_**

**_Rosario Murdica consigliere nazionale Arcigay_**

**_Marco Reglia responsabile nazionale Arcigay Memoria_**

**_Fabio Saccà responsabile nazionale Rete Giovani Arcigay_**

**_Simone Barbieri Tavolo lgbte Roma e Lazio_**

**_Queer.SeL - Sinistra e Libertà per la cultura delle differenze_**

**_Associazione Nuova Proposta donne e uomini omosessuali cristiani di Roma_**

**_Associazione Liberamentenoi di Roma_**

**_Regina Satariano vicepresidente Trans Genere_**

**_I Responsabili, psicologi, giuristi, endocrinologi, psichiatri, mediatori culturali, collaboratori, utenti -Consultorio TransGenere Torre del Lago_**

**_

**_PER SOTTOSCRIVERE L'APPELLO: inviare una mail a[presidente@arcigay.it](mailto:presidente@arcigay.it)_**

  


_**_

_  

  
  

Matteo Ricci

_Ufficio Stampa_

_Arcigay_ _Associazione Lesbica e Gay Italiana_

_tel_  +39.**348.6839779**

_mail_ **[ufficiostampa@arcigay.it](mailto:ufficiostampa@arcigay.it)**

_web_ **[www.arcigay.it](http://www.arcigay.it/)**











_