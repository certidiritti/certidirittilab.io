---
title: 'Dirigenti di Arcigay aggrediti: solidarietà di Certi Diritti'
date: Mon, 11 Apr 2011 15:58:06 +0000
draft: false
tags: [Movimento LGBTI]
---

  
I Radicali, prima firmataria la presidente di Certi Diritti Rita Bernardini, hanno depositato un'interrogazione urgente al governo su quanto accaduto.  
  
Roma, 11 aprile 2011  
  
Comunicato Stampa dell’Associazione Radicale Certi Diritti La sera tra venerdì 8 e sabato 9 aprile, due dirigenti di Arcigay sono stati vilmente aggrediti e malmenati a Napoli, mentre si trovavano insieme ad altri esponenti dell’Associazione per un corso di formazione. L’Associazione Radicale Certi Diritti esprime la sua solidarietà e vicinanza ad Antonello Sannino, presidente di Arcigay Salerno e Federico Esposito, segretario provinciale di Arcigay Pistoia, aggrediti perché omosessuali. E’ evidente che il clima di pregiudizio e di odio continua ad essere alimentato a Napoli e nel resto d’Italia senza che si intervenga adeguatamente con interventi di informazione ed educazione presso le scuole, i centri di socialità e di lavoro. La stessa legge contro l’omofobia dell’On. Paola Concia è ferma ormai da mesi per la mancanza di volontà  politica per approvarla.