---
title: 'ALL''UNIVERSITA'' STATALE DI MILANO E'' ARRIVATO LO SPORTELLO CONTRO LE DISCRIMINAZIONI E L''OMOFOBIA'
date: Wed, 07 Apr 2010 23:59:17 +0000
draft: false
tags: [Senza categoria]
---

Qualche settimana fa aveva provocato scalpore [la vicenda di uno studente aggredito verbalmente e minacciato da un addetto alla manutenzione dell’edificio](http://www.queerblog.it/post/7462/studente-omosessuale-aggredito-alla-statale-di-milano-siete-la-feccia-dellumanita), perchè impegnato ad affiggere cartelli per un cineforum a tematica Lgbt: “_Attaccane un altro e ti ammazzo di botte. Per voi malati qui non c’è posto_“.

Da subito la nascita di polemiche e la promessa da parte dell’istituto di assicurare rispetto per gli studenti da parte di aziende esterne, assunte proprio dall’Università. E invece da oggi è anche inoltre possibile recarsi personalmente a denunciare eventuali discriminazioni o angherie subite. E’ stato infatti creato uno sportello informativo e di supporto, in merito ad eventuali fatti spiacevoli all’interno dell’ateneo. Inolte sarà possibile essere sostenuti e trovare l’appoggio da specialisti del settore, impegnato a fornire supporto morale e psicologico.

Per chi è preoccupato di ripercussioni o troppo timido e spaventato, è accessibile anche il servizio online: con una semplice mail sarà possibile raccontare la propria esperienza. Lontani da eventuali occhi indiscreti. Una bella ed intelligenza iniziativa, non trovate?