---
title: 'RADICALI: LA CHIESA NON SI PERMETTA DI INGERIRE NELLE VICENDE DELLO STATO ITALIANO.'
date: Fri, 12 Feb 2016 10:40:46 +0000
draft: false
tags: [Politica]
---

[![vaticano_piovra](http://www.certidiritti.org/wp-content/uploads/2015/04/vaticano_piovra.png)](http://www.certidiritti.org/wp-content/uploads/2015/04/vaticano_piovra.png)Apprendiamo con sconcerto che il cardinale Angelo Bagnasco, presidente della CEI, ha chiesto il voto segreto sulle unioni civili.

"Forse il presidente della CEI è troppo impegnato ad amministrare i lauti proventi dell’8 per mille e delle esenzioni IMU e si è dimenticato che è dal 20 settembre del 1870 (146 anni!) che Roma non è più governata dalla Chiesa e che il Senato della Repubblica Italiana è regolato dal suo presidente e non da quello della CEI", dichiara Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti.

"Bagnasco dovrebbe rileggersi le parole dette da Paolo VI all’angelus del 20 settembre 1970 quando, ricordò le parole di Cristo: date a Cesare ciò che è di Cesare e date a Dio ciò che è di Dio, e invitò a saper «saggiamente distinguere le due sfere dell’ordine umano, la sfera temporale e civile da quella spirituale e religiosa»", continua Guaiana.

"Da radicali preferiamo però richiamare il motto di Camillo Benso Conte di Cavour: «libera Chiesa, in libero Stato». La CEI la smetta di ingerire nelle vicende dello Stato italiano, quell’ultimo avanzo di feudalesimo politico che era lo Stato Pontificio, non tornerà più. Bagnasco se ne faccia una ragione!", conclude Guaiana