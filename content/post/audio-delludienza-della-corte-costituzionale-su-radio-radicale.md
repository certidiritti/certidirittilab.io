---
title: 'AUDIO DELL''UDIENZA DELLA CORTE COSTITUZIONALE SU RADIO RADICALE'
date: Wed, 24 Mar 2010 07:51:41 +0000
draft: false
tags: [Comunicati stampa]
---

AL SEGUENTE LINK DI RADIO RADICALE (CHE RINGRAZIAMO) E' POSSIBILE ASCOLTARE L'UDIENZA DELLA CORTE COSTITUZIONALE SUL MATRIMONIO GAY:

[http://www.radioradicale.it/scheda/300046/corte-costituzionale-udienza-pubblica-sulla-vicenda-dei-ricorsi-delle-coppie-gay-che-chiedono-il-matrimoni](http://www.radioradicale.it/scheda/300046/corte-costituzionale-udienza-pubblica-sulla-vicenda-dei-ricorsi-delle-coppie-gay-che-chiedono-il-matrimoni)