---
title: 'Ius soli: Certi Diritti al fianco dei digiunatori'
date: Tue, 10 Oct 2017 10:26:26 +0000
draft: false
tags: [Politica]
---

![ius-soli-590235.660x368](http://www.certidiritti.org/wp-content/uploads/2017/10/ius-soli-590235.660x368-300x167.jpg) L'associazione Radicale Certi Diritti e' al fianco delle centinaia di persone che stanno digiunando a staffetta per un'approvazione rapida e sicura da parte del Senato della legge sulla cittadinanza. L'iniziativa nonviolenta è stata indetta dal senatore Luigi Manconi e da Radicali Italiani a sostegno dei 900 insegnanti che il 3 ottobre hanno annunciato uno sciopero della fame in solidarietà a quei bambini stranieri nati in Italia che aspettano ancora uno scatto di civiltà da parte del del nostro Parlamento" Lo dicono annunciando la loro partecipazione al digiuno Leonardo Monaco, Dario Belmonte e Yuri Guaiana dell'Associazione Radicale Certi Diritti. I tre sciopereranno rispettivamente l'11 ottobre, il 12 e oggi. "Ben conosciamo l'energia e l'impegno che richiedono le lunghe battaglie per le libertà civili. Non ci tireremo indietro per questa ed altre occasioni, in nome dell'universalità dei diritti umani e civili", concludono.