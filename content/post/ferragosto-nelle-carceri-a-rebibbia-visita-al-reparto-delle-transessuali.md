---
title: 'FERRAGOSTO NELLE CARCERI: A REBIBBIA VISITA AL REPARTO DELLE TRANSESSUALI'
date: Wed, 12 Aug 2009 06:41:04 +0000
draft: false
tags: [Comunicati stampa]
---

FERRAGOSTO NELLE CARCERI:  LA DELEGAZIONE RADICALE CHE SI RECHERA’ A REBIBBIA VISITERA’ ANCHE IL REPARTO DELLE PERSONE TRANSESSUALI.

“In occasione dell’iniziativa ‘Ferragosto nelle carceri’, promossa in tutta Italia grazie all’iniziativa della deputata radicale Rita Bernardini e  Radicali Italiani, venerdì 14 agosto una delegazione radicale visiterà anche il Carcere romano di Rebibbia.

La delegazione radicale, guidata dal deputato Matteo Mecacci, accompagnato da Sergio Rovasio, Segretario dell’Associazione Radicale Certi Diritti e Michele De Lucia, Tesoriere di Radicali Italiani, farà visita anche nel reparto delle transessuali dove sono detenute alcune decine di persone”.

Maggiori informazioni sull’iniziativa al sito [www.radicali.it](http://www.radicali.it)