---
title: 'Le differenze tra figli permangono. Venga rinviata decisione e subito tavolo tecnico'
date: Wed, 10 Jul 2013 08:46:13 +0000
draft: false
tags: [Diritto di Famiglia]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti e dell'Associazione Luca Coscioni

Roma,  10 luglio 2013

Previsto nel Consiglio dei Ministri di venerdì il decreto di attuazione della legge 219, predisposto dalla medesima Commissione che ha curato i testi della legge, sembra essere in dirittura di arrivo, ampiamente prima della prevista scadenza del 31/12/2013, senza che vi sia stato alcun confronto o dibattito sul contenuto di essi. Il momento di discussione era (ed è) invece quanto mai necessario, considerata l'importanza della Riforma che sta per completarsi nel suo percorso legislativo e l'esistenza di punti critici o comunque controversi. Tra essi si segnalano:

*   la persistenza di un sistema codicistico il quale, invece di abolire la differenza tra figli legittimi e naturali, la sostituisce con la distinzione tra figli nati nel matrimonio e fuori di esso, lasciando in vita una serie di norme destinate a dimostrare chi fosse "legittimo" e chi no ed ora sopravvissute per dimostrare, pur non esistendone più la necessità, il fatto che la nascita sia avvenuta dentro il matrimonio;
*   la persistenza di un sistema farraginoso e differenziato per la formazione dell'atto di nascita, anziché una semplice disciplina unitaria;
*   l'assoluta inadeguatezza e carenza della parte processuale della riforma;
*   il fatto che il decreto attuativo si schieri per la posizione interpretativa minoritaria, secondo la quale la legge 219 non ha abolito o ridotto la distinzione tra adozione legittimante ed adozione in casi particolari;
*   la discutibilissima decisione, non rientrante nei confini della delega, di spostare il contenuto dei provvedimenti relativi alla separazione personale dei coniugi in altro capo del codice, spezzando l'unitarietà della materia separativa;
*   la persistenza di discriminazioni tra figli nati e non nel matrimonio, in termini di legittimazione ad agire per la modifica dello stato (art. 244 ed art. 263 c.c.);
*   l'assoluta non realizzazione della delega in termini di definizione della responsabilità genitoriale.

La delega prevedeva che il delegato delineasse "la nozione di responsabilità genitoriale, quale aspetto dell'esercizio della potestà genitoriale". La delega attribuiva un compito molto ampio e sostanziale e ciò suscitava dubbi in ordine all'utilizzabilità di tale strumento per introdurre importanti innovazioni. Il problema risulta superato per il modo in cui, nel testo in esame, si è data attuazione alla delega. Nessuna nozione di responsabilità genitoriale e nessuna indicazione sui suoi contenuti e sulle differenze dalla potestà sono state infatti poste in essere e, di fatto, il delegato si è limitato a sostituire, in tutti i punti ove ricorreva, la parola potestà con responsabilità. Non sembra questo il modo di attuare una riforma. Più in generale, si rileva che il sistema risente del fatto che, a monte, devono essere risolti altri problemi, come quello della regolamentazione giuridica delle coppia c.d. di fatto. Ma questo è un rilievo che riguarda l'intera legge 219 e non, in modo specifico, il decreto delegato. In ragione di quanto si è detto, si chiede un rinvio della decisione e l'attivazione di un tavolo tecnico, di cui facciano parte tutte le componenti politiche e sociali, per la modifica dei punti del decreto che, all'esito di un'ampia discussione, risultino inadeguati.