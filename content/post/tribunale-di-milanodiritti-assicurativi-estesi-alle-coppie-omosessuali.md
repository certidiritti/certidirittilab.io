---
title: 'TRIBUNALE DI MILANO,DIRITTI ASSICURATIVI ESTESI ALLE COPPIE OMOSESSUALI'
date: Thu, 28 Jan 2010 07:02:08 +0000
draft: false
tags: [Senza categoria]
---

I giudici milanesi estendono alle coppie omosessuali i diritti assicurativi previsti a favore delle coppie eterosessuali.  
  
La questione trae origine dalla querela presentata dal dipendente di una banca, al quale è stato negato il diritto di dare copertura assicurativa al proprio compagno. L'atteggiamento restio della Cassa mutua nazionale delle Bcc è derivato dal fatto che il convivente fosse dello stesso sesso del richiedente, ragion per cui l'ente assicurativo ha preferito restituire il contributo già versato, piuttosto che concedere la copertura assicurativa.  
  
Il tribunale meneghino, con sentenza n. 3113 pronunciata lo scorso 15 dicembre, ha censurato tale scelta, affermando la necessità di interpretare estensivamente l'espressione "more uxorio", presupposto della suddetta copertura assicurativa, secondo quanto previsto dal decreto legislativo 216 del 2003.  
  
In particolare, ha affermato che la "convivenza more uxorio" va intesa nel senso di "unione" non formalizzata, non solo come "matrimonio" non formalizzato, dunque senza alcuna discriminazione in base al sesso.  
  
Il fondamento giuridico della pronuncia è costituito dall'art.3 della Costituzione, enunciazione del principio di uguaglianza formale e sostanziale, che vieta qualunque discriminazione su base sessuale; gli articoli 8 e 14 della Convenzione europea dei diritti dell'uomo; l'articolo 21 della Carta dei diritti fondamentali dell'Unione Europea.  
Inoltre i giudici hanno richiamato anche le innumerevoli risoluzioni del Parlamento Europeo, con le quali quest'ultimo ha invitato gli Stati membri ad aprire le porte al riconoscimento dei diritti delle coppie di fatto e ad abbattere le barriere discriminatorie.

(Articolo redatto dal Sole 24ore)

Marco e Erminio hanno gentilmente risposto a qualche domanda che ponevamo loro circa questo straordinario episodio:  
  
>1-Da quanto tempo avete deciso di stare insieme come coppia?è stato  
difficile dichiarare il vostro amore al mondo che vi appartiene?

Conviviamo stabilmente dal 2007. Ci siamo conosciuti ed innamorati in modo improvviso ed inaspettato e questo ha provocato una serie di problemi perché ha coinvolto persone molto vicine ed importanti; il fatto di essere una coppia omoaffettiva ha costituito soltanto ciò che potremmo definire "un'aggravante". E' stato difficile ma per noi necessario e naturale.  
  
  
>2-Pensate che la cultura popolare,così discriminatoria verso gli  
omosessuali,qui in Italia non sia al passo con i maggiori paesi europei  
anche a causa dell'atteggiamento degli stessi nei confronti della società?

"In Italia gli omosessuali non otterranno diritti fino a quando non avranno il coraggio e la serenità di dirlo alla loro MAMMA". Con questa battuta vogliamo dire che le cause della discriminazione sono molteplici e spesso concatenate, si fondano sulla paura ed è prima di tutto nella famiglia che si vive il problema della diversità e il rischio del rifiuto. La situazione si aggrava a causa di una cultura popolare soggiogata e manipolata dal potere politico,religioso ed economico: le persone sono bombardate da informazioni superficiali e scandalistiche che ancora associano l'omosessualità al peccato, alla devianza,alla malattia ed alla pedofilia. In un ambiente sociale e famigliare, se non apertamente ostile, incapace di fornire modelli di ruolo, un omosessuale cresce e vive provando una grande sensazione di solitudine e di inadeguatezza. "In pratica bisogna costruirsi una corazza che impedisce la socializzazione piena con gli altri" \[ I. Scalfarotto \] Per noi l'unica soluzione  auspicabile è trovare la forza di liberarsi da tutte le paure, togliersi la corazza e guardare al mondo con occhi nuovi e una riconquistata serenità e forza per affrontare le persone che ci circondano, scardinare i loro pregiudizi e le loro paure e scoprire come talvolta sia semplice farsi accettare. Non è sempre un percorso semplice ma dà tante soddisfazioni.

  
  
\> 3-il fatto di esservi esposti in una causa pubblica a difesa della vostra  
coppia vi ha creato dei problemi,quali le conseguenze anche nell'ambito  
lavorativo?

  
La sentenza è molto recente e per il momento non abbiamo avuto ripercussioni negative nella nostra vita lavorativa e privata; tutti coloro che ci conoscono, anche in ambito lavorativo, hanno accolto con gioia la notizia. Abbiamo affrontato la causa con molta serenità perché crediamo lecita la nostra richiesta. Probabilmente la Cassa Mutua ricorrerà in appello.  Se così fosse dovremmo riarmarci di pazienza, attendere un secondo giudizio, e contemporaneamente prendere atto di un inspiegabile ( per non dire incivile ) accanimento nei nostri confronti visto che è palese la volontà discriminatoria.

  
  
\> 4-Essere i primi ad aver ottenuto una sentenza positiva dalla cassazione  
riferita alle tutele del more uxorio è stata una grande conquista,è secondo voi un passo che porterà conseguenze positive nel campo delle unioni  
omosessuali?

  
La sentenza, che purtroppo è solo di primo grado, rappresenta sicuramente un importante segno di evoluzione della società che vuole essere civile. Solo pochi anni fa sarebbe sembrato assurdo semplicemente pensare di intentare una causa per il riconoscimento di una prestazione mutualistica. Ma, nonostante il clima ostile e le pesanti dichiarazioni di alcuni personaggi pubblici, qualcosa è cambiato nel sentire della gente comune e noi abbiamo trovato quel minimo di garanzie e serenità per esporci. La sentenza, con quelle motivazioni così precise, apre alcuni spiragli sul riconoscimento di una realtà che sempre più cittadini vivono quotidianamente ma che i nostri rappresentanti politici ancora si ostinano a non voler prendere in considerazione e regolarizzare. Per noi è stata una bella scarica di fiducia che ci auspichiamo non venga mortificata dai prossimi eventi. Ci piace sentirci come un bambino che ha compiuto il suo primo passo. sicuramente il bambino cadrà molte volte prima di trovare la stabilità per correre, ma oramai ha iniziato quel cammino inarrestabile che lo porterà alla sicurezza dello stare diritto di fronte al mondo.  Come quel bambino abbiamo visto le persone intorno a noi gioire per questo piccolo riconoscimento. Non sappiamo dove ci porteranno i prossimi passi ma almeno per la prima volta qualcosa si è smosso e la speranza si è accesa.

\> 5-Avvertite di essere tutelati dalle varie associazioni presenti sul  
territorio che dovrebbero essere adibite a difesa della cultura omosessuale?

In Italia esistono troppi gruppi e associazioni che si occupano tutte di tutto. Questo frazionamento porta ad una dispersione che rende inefficaci i loro buoni propositi e la loro azione.

\> 6-Secondo voi esiste fratellanza e unione tra gli omosessuali in difesa dei  
loro diritti?

In Italia non esiste identità sociale ed i particolarismi, la varietà di opinioni e di campanilismi, di cui il nostro paese è permeato, anziché essere un motivo di ricchezza rendono difficili ogni forma di coesione e lotta per gli stessi principi. Il mondo omosessuale Italiano ha gli stessi difetti del popolo a cui appartiene e sapere cosa vogliono i gay italiani è un vero mistero, infatti non esiste coscienza di gruppo e non si riesce a costruire, come in altri paesi un potere di Lobby.   
  
  
\> 7-Vorreste sposarvi?Regalate al matrimonio tra omosessuali lo stesso valore  
che ha il matrimonio per gli eterosessuali?Pensate che una coppia  
omosessuale possa crescere un bambino facendolo sentire al pari dei figli  
delle coppie etero?

Se sposarsi significa acquisire gli stessi diritti e doveri che in questo momento sono concessi solo alle coppie eterosessuali, allora la nostra risposta è un deciso "si, lo voglio" Contemporaneamente crediamo dovrebbero esistere delle leggi che regolano e tutelano anche altre possibili forme di convivenza. Per quanto riguarda l'educazione dei figli in una coppia omosessuale, riteniamo che nella situazione italiana attuale le problematiche e le difficoltà sarebbero analoghe a quelle affrontate dai figli delle prime coppie di genitori separati.

  
> 8-Milano è una citta favorevole alla vita omosessuale?

Come tutte le grandi città, e se hai una buona disponibilità economica, Milano permette di scegliere ambienti più aperti e colti con maggiori possibilità di vivere liberi e rispettati. Questo non significa che nella quotidianità Milano è una città favorevole ad una vita omosessuale, al limite è più corretto dire che a Milano gli omosessuali sono più tollerati. Inoltre nella grande città è più facile essere anonimi in mezzo alla gente e non farsi conoscere neanche dal vicino di casa. Ma a Milano una coppia omosessuale ha gli stessi NON diritti di una qualsiasi coppia omosessuale italiana.