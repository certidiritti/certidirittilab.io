---
title: 'PRESERVATIVI: PARLAMENTO EUROPEO DIVENTA PAPAMENTO? RESPINTO VOTO CENSURA'
date: Fri, 08 May 2009 07:36:27 +0000
draft: false
tags: [Comunicati stampa]
---

**PAPA/PARLAMENTO EUROPEO: GRAVE IL RIGETTO DELL’EMENDAMENTO DI CENSURA DELLE DICHIARAZIONI DEL PAPA SUI PRESERVATIVI.** NO ALLA TRASFORMAZIONE DEL PARLAMENTO EUROPEO IN PAPAMENTO EUROPEO, VOLUTA DAL PPE E PARTI DEL PSE. SODDISFAZIONE PER APPROVAZIONE EMENDAMENTI LGBT.  
Bruxelles-Roma, 8 maggio 2009

Dichiarazione di Sergio Rovasio, Segretario di Certi Diritti e candidato nella Circ.ne Centro della Lista Bonino/Pannella e di Ottavio Marzocchi,  responsabile per le questioni europee di Certi Diritti:

Il Parlamento Europeo ha approvato ieri alcuni emendamenti in  difesa delle persone LGBT depositati da Marco Cappato e Sophie In’t Veld relativi al rapporto sui diritti umani nel mondo, ma ha anche rigettato, dopo un tentativo fallito del PPE di farlo dichiarare irricevibile, un emendamento degli stessi deputati europei sulle recenti dichiarazioni del Papa sui preservativi.  
   
E’ grave che il Parlamento europeo, sempre in prima fila nella difesa dei diritti umani, delle libertà fondamentali, delle minoranze e della laicità dell’Europa, decida di non condannare le dichiarazioni anti-scientifiche del Papa sui preservativi, peraltro manipolate ripetutamente dalle gerarchie ecclesiastiche nelle trascrizioni ufficiali  sui siti del Vaticano. Tali affermazioni sono gravissime perché indeboliscono la lotta all’AIDS nel mondo e alimentando l’ignoranza e la falsa informazione.  
   
E’ ironico che proprio il giorno prima si fosse dibattuto al Parlamento Europeo di Durban 2, la conferenza internazionale boicottata in difesa della libertà di espressione e di critica delle religioni, per poi il giorno successivo bocciare un emendamento di  critica delle affermazioni del Papa.  
   
Grazie ai deputati del PPE, dei numerosi astenuti nel PSE e della componente Democratica del gruppo ALDE (che comprendono gli eurodeputati della ex-Margherita), il Parlamento Europeo é diventato per un giorno il “Papamento europeo”.  
   
   
Qui di seguito si può verificare il risultato del voto fatto mediante appello nominale (pagine 48 e 49): [http://www.europarl.europa.eu/sce/data/votes_results/doc//AN.20090507.pdf](http://www.europarl.europa.eu/sce/data/votes_results/doc//AN.20090507.pdf)