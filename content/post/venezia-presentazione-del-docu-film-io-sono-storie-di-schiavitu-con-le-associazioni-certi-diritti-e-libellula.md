---
title: 'Venezia: presentazione del docu-film ''Io sono - storie di schiavitù'' con le associazioni Certi Diritti e Libellula'
date: Fri, 09 Sep 2011 07:02:31 +0000
draft: false
tags: [Politica]
---

Sabato 10 settembre sarà proiettato il documentario di Barbara Cupisti sulla tratta della prostituzione e sullo sfruttamento dei migranti.

Roma, 9 settembre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

L’Associazione Radicale Certi Diritti, con il Segretario Sergio Rovasio e l’Associazione Libellula di Roma, parteciperanno domani, sabato 10 settembre, alla presentazione del film-documentario ‘Io sono – Storie di schiavitù’, di Barbara Cupisti. Il film-documentario verrà presentato a Venezia in occasione della 68° Mostra del Cinema come evento della sezione Controcampo italiano, alle ore 11,30, presso la Sala Darsena al Lido di Venezia.

I protagonisti del film-documentario sono uomini, donne, transessuali, minori, che, si trovano spesso vittime di violenze, sopraffazioni, ricatti e sfruttamenti di ogni genere da parte di organizzazioni criminali e che si vedono negate assistenza e aiuto anche dalle nostre istituzioni.

Il film racconta delle difficoltà in cui si trovano i migranti che giungono in Italia, spinti dalla fame, dalla miseria, dalla tortura, dalla guerra o dalla semplice ricerca di un futuro migliore, spesso negato da organizzazioni criminali senza scrupoli e pronte a sfruttare queste persone nell'illegalità. Secondo il rapporto 2009 del Copasir (Comitato parlamentare per la sicurezza della Repubblica) la tratta degli esseri umani è infatti la terza fonte di reddito delle organizzazioni criminali.

L'Associazione Radicale Certi Diritti e l'Associazione Libellula ringraziano Barbara Cupisti per questo suo impegno artistico e civile di grande spessore e che pone l’attenzione anche su una realtà sconosciuta, quella delle persone transessuali vittime del traffico di esseri umani: una realtà, quella del transessualismo, come ha giustamente sottolineato Barbara Cupisti, "che nel  nostro paese e' stata oggetto di attenzione solo per scandali mediatici che non si sono minimamente soffermati sulla realta' drammatica vissuta da persone che hanno un'identita' fragilissima, e  che sono esposte a violenze e discriminazioni mai raccontate prima."

Il lavoro di Barbara Cupisti già stato premiato alla Mostra del Cinema di Venezia con il film-documentario ‘Madri’, vincitore del David di Donatello nel 2008 e con "Vietato Sognare" vincitore di numerosi premi tra cui quelli di Amnety Italia per "Cinema e Diritti Umani" nel 2009.