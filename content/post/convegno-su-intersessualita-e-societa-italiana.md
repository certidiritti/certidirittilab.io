---
title: 'Convegno su ” Intersessualità e Società Italiana”'
date: Tue, 21 Sep 2010 07:40:13 +0000
draft: false
tags: [Comunicati stampa]
---

[![](http://www.ireos.org/wp-content/uploads/intersex1.gif "intersex")](http://www.ireos.org/wp-content/uploads/intersex1.gif)**24 settembre 2010**

Consiglio Regionale della Toscana

_Sala degli Affreschi_

via Cavour 4, Firenze

#### Programma

**09.00 – 09.30** Registrazione dei partecipanti

**09.30 – 10.00** Inizio dei lavori e saluti delle Istituzioni

Introduce e coordina _Fabrizio Ungaro_, Presidente di Ireos – Centro Servizi Autogestiti Comunità Queer

**10.00** _Michela Balocchi_, dott.ssa di ricerca in Sociologia e Sociologia Politica, Università di Firenze  
“Aspetti sociologici della medicalizzazione dell’intersessualità. La sperimentazione dell’accoglienza a Ireos”

**10.25** _Beatrice Busi_, dott.ssa di ricerca in Filosofia della scienza, Università La Sapienza di Roma  
“Dall’ermafroditismo ai disordini dello sviluppo sessuale: note sulla negoziazione tra movimento intersessuale e comunità scientifica e l’evoluzione dei protocolli medici ”

**10.50** _Francesca Torricelli_, direttrice SOD Dipartimento di Diagnostica Genetica, Azienda Ospedaliera Universitaria Careggi, Firenze  
“Genetica della intersessualità”

**11.15** Coffee break

**11.30** _Maria Grazia Campus_, Commissione di Bioetica della Regione Toscana  
“Il normale e il patologico”

**11.50** _Lorenzo Bernini_, ricercatore in Filosofia politica, Università di Verona  
“Maschio e femmina Dio li creò!? L’intersessualità come sabotaggio del binarismo sessuale”

**12.10** _Renato Busarello_, Laboratorio SmaschieRamenti, Bologna  
“Intersessualità e nuove politiche di genere: per un movimento LGBTIQ”

**12.30 Interventi dal pubblico e dibattito**

**Interverranno:  
**_Enzo Brogi_ (Consigliere Regionale PD-Riformisti toscani),  
_Mauro Romanelli_ (Consigliere Regionale Federazione della Sinistra e Verdi),  
_Regina Satariano_ (Responsabile Consultorio Transgenere),  
_Monica Sgherri_ (Capogruppo Regionale Federazione della Sinistra e Verdi),  
_Fabianna Tozzi_ (Presidente Associazione Transgenere).

**Scarica tramite il seguente link il volantino completo**

[volantino convegno 24092010](http://www.ireos.org/wp-content/uploads/volconv24092010.pdf)