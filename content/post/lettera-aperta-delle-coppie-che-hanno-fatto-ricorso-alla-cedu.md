---
title: 'Lettera aperta delle coppie che hanno fatto ricorso alla CEDU'
date: Mon, 08 Feb 2016 16:27:17 +0000
draft: false
tags: [Affermazione Civile]
---

[![registro-firma-bologna-merola-nozze-gay-estero-15-settembre](http://www.certidiritti.org/wp-content/uploads/2014/09/registro-firma-bologna-merola-nozze-gay-estero-15-settembre-300x150.jpg)](http://www.certidiritti.org/wp-content/uploads/2014/09/registro-firma-bologna-merola-nozze-gay-estero-15-settembre.jpg)Due delle coppie che hanno reso possibile la sentenza della Corte Europea dei Diritti dell'Uomo con la quale si riconosce che le coppie dello stesso sesso non vedono riconosciuto il loro diritto umano fondamentale alla vita famigliare dallo Stato italiano intervengono nel dibattito sulle unioni civili con una lettera aperta che pubblichiamo volentieri.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Al signor Presidente della Repubblica

Sergio Mattarella

c/o Palazzo del Quirinale

00187 – Roma

Al Sen. Pietro Grasso, Presidente del Senato della Repubblica

Alla dott.ssa Laura Boldrini, Presidente della Camera dei Deputati

Milano, 8 Febbraio 2016

Vi scriviamo in questi giorni in cui le Camere discutono e votano la proposta di legge Cirinnà sulle Unioni Civili. Siamo quattro cittadini italiani che, in passato, per denunciare l’inescusabile mancanza di rispetto che le Istituzioni ci riservano in quanto omosessuali, abbiamo avuto l'ardire di reclamare davanti ai giudici un trattamento paritario, per poi vedere l’Italia intera tristemente condannata di fronte alla Corte Europea dei Diritti dell’Uomo.

Siamo una goccia nel mare dei cittadini – italiani, Europei ed extraeuropei in Italia – che chiedono da decenni il riconoscimento della piena dignità di donne e di uomini, che chiedono di vedere riconosciute le proprie vite e le proprie famiglie, e la parità di trattamento dei diritti e dei doveri reciproci e nei confronti della comunità. Vale la pena avere presente che non pochi di questi cittadini sono già morti, senza alcun conforto di Patria.

Nel 2016 finalmente le Istituzioni italiane hanno iniziato ad ascoltarci concretamente e questo ci riempie di gioia e di speranza, che le difficoltà con cui dobbiamo fare i conti quotidianamente possano finalmente aver fine.

Vi scriviamo con il sogno di poter percorrere, in queste righe, un ideale cammino insieme verso una Italia migliore e più giusta. Molto migliore. Al di là delle apparenze politiche e mediatiche, questi giorni di discussione della proposta di legge Cirinnà sono giorni di profonda unità nazionale, come raramente – in passato – ne abbiamo vissuti. Sotto questa legge si riconoscono le più svariate tipologie di persone: le persone apertamente omosessuali, i nostri figli, i nostri genitori, i nostri parenti, gli amici, i colleghi, i nostri datori di lavoro, grandissima parte del popolo europeo. E soprattutto tutti i giovani che meritano di potersi costruire il migliore futuro possibile, lo stesso futuro che l’Italia oggi sta scegliendo di darsi, o di negarsi: in dipendenza dalla scelta che farete.

Perciò crediamo sia questo il momento migliore per dare voce alle discriminazioni che, come omosessuali, noi e milioni di altre famiglie affrontiamo quotidianamente. Le difficoltà che molti bambini affrontano insieme ai loro genitori nel loro vivere quotidiano (dalle più semplici, come quella dell'essere portati a scuola, alle più dolorose, che comportano l'assistenza in ospedale o la separazione dei genitori). Vorremmo testimoniare come sono le nostre vite. Ogni cittadino onesto partecipa alla vita del nostro Paese con le sue possibilità e cercando, nel suo piccolo, di farlo progredire; e come ogni cittadino, anche noi affrontiamo quello che la vita ci propone avendo affianco la persona con cui scegliamo liberamente di vivere: la persona che amiamo. Questo è famiglia. Ed è anche e proprio come famiglia che noi contribuiamo alla nostra società pure non avendone di ritorno nessun riconoscimento e nessuna tutela.

Il testo di questa legge non ci soddisfa appieno, in quanto noi sosteniamo il diritto egualitario, ivi compreso il matrimonio con rito civile, e perciò la proposta di legge Cirinnà – così come formulata originariamente – ci sembra il compromesso minimo e inderogabile per riallinearci con i Paesi che garantiscono a tutti i loro cittadini una parità di trattamento.

L’Italia sta finalmente scegliendo da che parte stare, è tristemente l’ultima in Europa e nel mondo Occidentale a farlo e sa che il tempo per scegliere sta terminando. Non si può più non scegliere. E voi siete chiamati a farlo per noi. In questi giorni le nostre vite – tutta la speranza custodita fino ad oggi, tutti i nostri sogni per il domani – sono nelle vostre mani. Questa è l’espressione più pura della sacralità della democrazia, che vi chiediamo di vivere con totale abnegazione.

Poi, qualunque scelta voi farete, le nostre vite torneranno nelle nostre mani. E continueremo ad ardire, inarrestabili come abbiamo sempre fatto per rendere pieno onore alla nostra dignità, finché godremo insieme a tutti voi della piena uguaglianza che ogni cittadino merita responsabilmente di fronte alla legge.

Se necessario, porteremo nuovamente davanti alla Corte Europea dei Diritti dell’Uomo lo spirito con cui Aldo Moro e Giorgio La Pira contribuirono a definire l’articolo 29 della nostra Costituzione: il concetto originario e autentico di società naturale, al quale anche noi famiglie omosessuali apparteniamo a pieno titolo. Ed insieme, eterosessuali e omosessuali, porteremo all’attenzione della Corte Costituzionale ogni discriminazione che non sarà stata cancellata, affinché venga sottoposta al vaglio della ragionevolezza.

Perciò non siamo qui a fare leva sui buoni sentimenti o su degli ideali astratti. Nemmeno su opinioni o convinzioni personali, o sulla vostra indiscutibile libertà di coscienza: in realtà è propriamente sul piano della ragionevolezza che sarete giudicati voi e la vostra legge. E quindi, è sempre sul piano della ragionevolezza che vi invitiamo ad esprimere posizioni oneste, libere e responsabili, quali depositari del futuro di questa nazione.

Questi sono i giorni in cui noi, insieme a tantissimi italiani, vogliamo credere in voi. Essere all’altezza di così grande fiducia sarà il più grande vantaggio e privilegio di cui possiate mai godere.

I più sentiti e rispettosi saluti.

Gian Mario Felicetti Riccardo Perelli Cippo Roberto Zacheo Riccardo Zappa _(in ordine alfabetico)_