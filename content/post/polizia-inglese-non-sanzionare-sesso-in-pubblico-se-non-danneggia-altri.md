---
title: 'POLIZIA INGLESE: NON SANZIONARE SESSO IN PUBBLICO SE NON DANNEGGIA ALTRI'
date: Fri, 17 Oct 2008 14:18:38 +0000
draft: false
tags: [Comunicati stampa]
---

**NON SANZIONARE SESSO IN PUBBLICO: DALLA GRAN BRETAGNA UN’ALTRA LEZIONE DI TOLLERANZA E ATTENZIONE AI NUOVI FENOMENI SOCIALI. L’ITALIA INVECE RIMANE AI LIVELLI DELLA SHARIA O, MEGLIO DIRE, DEL VATICANO.** **MARONI INVII SUBITO ESPERTI IN INGHILTERRA**.

Roma, 17 ottobre 2008 

_**Dichiarazione di Sergio Rovasio, segretario dell’Associazione radicale Certi Diritti, e di Mario Staderini, della Giunta nazionale di Radicali Italiani**_

Ancora una volta l'Inghilterra ci insegna come gestire in maniera moderna ordine pubblico e libertà sessuale. Il paese dove ai Gay Pride sfilano poliziotte lesbiche in cerca di nuove reclute o dove sfilano i militari gay con i loro Generali, l'Associazione dei Capi della polizia promuove nuova tolleranza e un nuovo approccio al fenomeno dei rapporti sessuali in pubblico. Difatti, il Vice capo della polizia, in un rapporto boccia le politiche inutilmente repressive e propone tolleranza per gli adulti consenzienti che fanno sesso senza danneggiare o importunare il prossimo. Come affermano nel loro rapporto "la polizia non deve essere un arbitro della morale, ma rispondere in maniera misurata e professionale alle lamentele della cittadinanza".

Occorrerebbe che il Ministro Maroni inviasse subito squadre di esperti delle nostre Forze dell'ordine presso i loro colleghi inglesi per studiare gli effetti benefici di queste politiche. In Italia centinaia di coppie gay ed eterosessuali sono denunciate ogni anno per atti osceni in luogo pubblico per comportamenti che non ledono nessuno. È quanto accaduto, ad esempio, per i due ragazzi rinviati a giudizio per un bacio appassionato a tarda notte, appartati nei pressi del Colosseo e che rischiano la reclusione dai tre mesi ai tre anni, una pena superiore persino a quella prevista negli Emirati Arabi, dove però è reato persino il sesso fuori dal matrimonio.

In uno Stato liberale, dove vige lo stato di diritto non dovrebbe esserci spazio per reati senza vittima. Purtroppo dobbiamo constatare che anche su questo l'Italia è più vicina ai paesi teocratici - avendo il Vaticano in casa evidentemente - piuttosto che a quelli di cultura liberale. Speriamo che il nostro Governo 'liberale' se ne accorga presto