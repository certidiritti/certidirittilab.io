---
title: 'Anche Casini ritiene necessarie le tutele per le coppie conviventi, compreso il diritto ereditario. Passo avanti ma il nostro obiettivo rimane il matrimonio'
date: Wed, 08 Aug 2012 10:25:22 +0000
draft: false
tags: [Diritto di Famiglia]
---

Roma, 8 agosto 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Le dichiarazioni del leader dell’Udc sulla necessità di riconoscere alle coppie gay ed eterosessuali tutele civili è un grande passo avanti che dimostra di come questo paese, anche se molto lentamente, si avvia sempre più nella direzione del riconoscimento di diritti e garanzie fino ad oggi negati. Anche se dice di “conoscere tanti gay che non pensano al matrimonio e che ritengono questa impostazione una forzatura ideologica” va riconosciuto al leader dell’Udc di aver finalmente aperto una breccia in un'area politica che si era sempre caratterizzata alle obbedienze clerical-vaticane. Difatti gli stesso ha parlato di tutele anche sul diritto ereditario e altre garanzie.

La direzione è certamente quella giusta e finalmente si va incontro a quanto sollecitato dalla Corte Costituzionale con la sentenza 138/2010 e a  quanto chiedono, ormai da molti anni, l’Unione Europea e il Consiglio d’Europa all’Italia con Direttive, Raccomandazioni e proposte sostenute anche dai gruppi conservatori europei.

Riguardo il suo sentirsi agli antipodi sul riconoscimento del matrimonio per le coppie gay gli possiamo rispondere che forse anche lui prima o poi comprenderà le ragioni di centinaia di migliaia di cittadini che chiedono di poter accedere a tale istituto considerato ormai il vero traguardo dell’ugugaglianza, facendo magari tesoro di quelle stesse ragioni che vedono il premier britannico David Cameron o il Primo Ministro neo-zelandese, conservatori, tra i principali sostenitori del matrimionio civile tra persone dello stesso sesso.