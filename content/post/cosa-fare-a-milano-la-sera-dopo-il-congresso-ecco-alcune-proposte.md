---
title: 'COSA FARE A MILANO LA SERA DOPO IL CONGRESSO? ECCO ALCUNE PROPOSTE:'
date: Tue, 22 Nov 2011 20:31:10 +0000
draft: false
tags: [Politica]
---

Prima di passare all'ampia scelta di locali che Milano offre, vi segnaliamo la programmazione per il 2, 3 e 4 dicembre del **Teatro Elfo-Puccini** ([www.elfo.org](http://www.elfo.org/)) che, grazie alla sua produzione, gioca un ruolo determinante nel superare pregiudizi e valorizzare ogni forma di unione affettiva, a prescindere dall'orientamento sessuale.

SALA FASSBINDER  
**FREDDO**  
22 NOVEMBRE - 4 DICEMBRE 2011  
ADVANCE BOOKING intero a €20 a chi acquista entro il 7 novembre

SALA SHAKESPEARE  
**CABARET YIDDISH**  
29 NOVEMBRE - 4 DICEMBRE 2011  
ADVANCE BOOKING intero a €20 a chi acquista entro il 14 novembre

SALA BAUSCH  
**ELETTRA, BIOGRAFIA DI UNA PERSONA COMUNE**  
Nuove Storie  
29 NOVEMBRE - 4 DICEMBRE 2011

  
**Quanto ai locali ecco l'ampia offerta meneghina:**

**BAR**

NEXT GROOVE CAFÉ  
V. Sammartini, 23 20125 Milano (MI)

RICCI  
Piazza della Repubblica, 27 (ang. via Pisani) 20124 Milano (MI)

VERGINE CAMILLA C/O PATCHOULI CAFÈ -MERCOLEDI-  
Corso Lodi 51 20139 Milano (MI)

THE OTHER SIDE  
Via Montecengio, 3 20138 Milano (MI)

LELEPHANT  
Via Melzo, 22 20129 Milano (MI)

MONO  
VIA LECCO 6 20100 MILANO (MI)

LE MASCHERE PUB  
Via Maiocchi, 12 20129 Milano (MI)

I BEERBANTI  
Via Solari, 52 20144 Milano (MI)

RHABAR  
Via Alzaia Naviglio Grande, 150 20144 Milano (MI)

  
**BAR & PUB**

COMPANY CLUB  
V. Benadir, 14 20132 Milano (MI)

LOVE  
via Sammartini, 23 20125 Milano (MI)

KING MILANO  
VIA DERNA 19 21032 MILANO (MI)

  
**DISCOTECHE**

MAMA OFF BY MAMAMIA (IL VENERDÌ)  
viale Umbria, 42 20135 Milano (MI)

AFTERLINE DISCOPUB  
Via Sammartini 25 20125 Milano (MI)  
[026692130](tel:026692130)  
TROVA NELLA MAPPA

TUTTI I VENERDÌ: THAT'S ALL C/O BLACK HOLE  
viale Umbria ang. via Cena 20135 MILANO (MI)

G-LOUNGE  
Via Larga, 8 20122 Milano (MI)

DELIRIUM NIGHT (OGNI SABATO SERA)  
VIA Giacosa 4 Cinisello Balsamo (MI)

G-NIGHT LITE (C/O BLEVEL - IL VENERDÌ)  
via alserio 3 20100 milano (MI)

TEADANCE MILANO (DOMENICA)  
Viale Padova 21 20100 MILANO (MI)

BARBARELLA CLUB C/O SHOCKING CLUB  
Bastioni di porta nuova 12 20100 Milano (MI)

MACELLERIAHOUSECLUB  
via Plezzo 16 Milano (MI)

ONE WAY CLUB  
Via F. Cavallotti, 204 20099 Sesto S. Giovanni (MI)

GLITTER LA DISCOTECA (SABATO)  
C/O HD Via Filippo Tajani 11 20134 Milano (MI)  
TROVA NELLA MAPPA

JOIN THE GAP @ BORGO DEL TEMPO PERSO (DOMENICA)  
Via F. Massimo 36 20139 Milano (MI)

PLASTIC  
Viale Umbria, 120 201353 Milano (MI)

PIZZA CONNECTION C/O LE Q CLUB  
Via Cosenz, 61 20158 Milano (MI)

[POURHOMME@MAGAZZINIGENERALI](mailto:%3Ca%20href=)" target="_blank">[POURHOMME@MAGAZZINIGENERALI> (OGNI SABATO)  
via Piertasanta 16 20100 Milano (MI)](mailto:POURHOMME@MAGAZZINIGENERALI%3C/a)

[

19.02.10 - RUVIDO-MAN AT PLAY - CAN YOU BEAR IT?  
Piazza Carlo Erba 20129 milano (MI)

RUVIDOMILANO   
Via Mecenate 76/24 20183 Milano (MI)

](mailto:POURHOMME@MAGAZZINIGENERALI%3C/a)