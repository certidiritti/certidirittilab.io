---
title: 'LE MINISTRE CARFAGNA E GELIMNI ESCLUDONO OMOFOBIA DA ACCORDO ANTIVIOLENZA'
date: Thu, 09 Jul 2009 06:05:03 +0000
draft: false
tags: [Comunicati stampa]
---

DEPUTATI RADICALI HANNO OGGI DEPOSITATO UN'INTERROGAZIONE URGENTE A RISPOSTA SCRITTA SUL PROTOCOLLO SOTTOSCRITTO DALLE MINISTRE CARFAGNA E GELMINI SU SETTIMANA CONTRO VIOLENZA CHE ESCLUDE GLI ATTI VIOLENTI PER CAUSA DI ORIENTAMENTO SESSUALE  E DISABILITA', CONTRO DIRETTIVE EUROPEE.

  

Roma, 8 luglio 2009

Interrogazione a risposta scritta

Al Ministro per le Pari opportunità

Al Ministro dell'istruzione

  

  

Per sapere - premesso che:  
  
Il Ministro per le Pari Opportunità e il Ministro dell'istruzione hanno sottoscritto un protocollo di intesa per la collaborazione di iniziative comuni nell'ambito della "Settimana contro la violenza" che si svolgerà dal 12 al 18 ottobre prossimi.  
  
detta collaborazione si esplicita in un ambito di attività di prevenzione e contrasto dei fenomeni di violenza e discriminazione, per i quali vengono esplicitate esclusivamente quelle relative a "intolleranza religiosa, di razza e di genere", mentre non si citano nemmeno le altre potenziali cause di intolleranza, discriminazione e quindi di violenza, che sulla base dell'articolo 13 del Trattato CE sono anche quelle basate su età, disabilità e orientamento sessuale. Le sei potenziali aree di discriminazione sono a loro volta riprese nelle Direttive 43 e 78 del 2000, recepite con apposite leggi dello stato anche dall'Italia.  
  
Considerato che le cronache, segnatamente quelle relative alla vita scolastica ed extrascolastica dei giovani nel nostro paese, segnalano da tempo numerossimi casi di bullismo, violenza e discriminazione legati in particolare a disabilità e orientamento sessuale, oltre che a tutte le altre tipologie di intolleranza;  
  
Per sapere:  
  
per quale motivo i Ministri non hanno ritenuto di estendere a tutte le potenziali forme di intolleranza e discriminazione previste dal Trattato dell'Unione le iniziative della Settimana contro la violenza;  
  
se ritengono di sviluppare un programma informativo ed educativo ad hoc rispetto alle tre potenziali forme di discriminazione e intolleranza che non sono state comprese nel protocollo di intesa.

  

Elisabetta Zamparutti

Marco Beltrandi

Rita Bernardini

Maria Antonietta Farina Coscioni

Matteo Mecacci

Maurizio Turco