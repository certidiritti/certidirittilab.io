---
title: 'Perché sono nato gay in Africa?'
date: Sat, 02 Apr 2011 23:22:30 +0000
draft: false
tags: [Africa]
---

**Perché sono nato gay in Africa?**

**Articolo pubblicato da Guardian del 27.3.11**

**Traduzione di Daniela Domenici**

**Testo in inglese:**

[http://www.guardian.co.uk/world/2011/mar/27/uganda-gay-lesbian-immigration-asylum](http://www.guardian.co.uk/world/2011/mar/27/uganda-gay-lesbian-immigration-asylum)

_Elizabeth Day ascolta l’esperienza di due rifugiati gay dall’Uganda dove l’omofobia violenta è la politica statale._

Da bambino in Uganda John Bosco ricorda di aver ascoltato il racconto di una vecchia donna che se un uomo si addormenta al sole e quest’ultimo passa sopra di lui, lo attraversa, si sveglierà da donna. “ Avevo l’abitudine di farlo quando ero bambino” dice John ora trent’anni dopo. Siede a un tavolo in un affollato caffè sulla strada che viene dalla stazione di Southampton, le sue dita giocano con il manico di un bicchiere di cioccolata calda. “trascorrevo tutto il giorno sdraiato sotto il sole. Sin dall’infanzia volevo essere una ragazza. Volevo le bambole. A scuola giocavo a _netball_. Volevo vestirmi come una ragazza … raccoglievo erbe nel mio cestino che si sapeva facessero crescere i seni. Ho tentato ogni cosa ma non ha funzionato”.

Mi racconta che non c’era un solo momento in cui non si rendesse conto di essere gay; che la consapevolezza di ciò è sempre stata là inespressa finché non ha trovato le parole giuste. Appena è cresciuto John ha iniziato a essere attratto dagli uomini. Alla radio ascoltava storie di coppie gay che venivano picchiate e uccise dalla polizia. Dice che se avesse potuto cambiare se stesso l’avrebbe fatto perché desiderava così disperatamente essere considerato “normale” per stare bene,

Quando è andato all’università a studiare per una laurea in _business administration_ i suoi parenti e i suoi vicini a Kampala gli chiedevano se avesse mai avuto una fidanzata “Usavo un sacco di scuse, non sono pronto oppure ho una fidanzata che non vive nella mia stessa zona” dice “Era difficile perché non puoi essere aperto (sulla tua sessualità). Non  puoi socializzare come qualunque altra persona. Per la maggior parte del tempo devi mantenere le distanze. Ti senti come se non fossi te stesso. Rende le cose molto difficili”.

Questa è la realtà dell’essere gay nella moderna Uganda, un posto dove l’omosessualità è criminalizzata con il codice penale, punibile con la prigione a vita. Secondo le organizzazioni per i diritti umani circa 500.000 omosessuali vivono nella nazione, in modo nascosto la loro sessualità per paura di una violenta punizione sia della polizia che delle comunità di appartenenza. La legislazione anti-gay è un retaggio del colonialismo britannico progettato per punire quello a cui le autorità imperiali pensavano come “sesso innaturale” – e che è stato in seguito rinforzato onda dopo onda dai missionari cattolici.

Sebbene la maggior parte dell’eredità sia stata smantellata quando l’Uganda si è modernizzata l’omofobia è diffusa come sempre. Una legge anti-omosessualità, che deve essere discussa dal parlamento prima di giugno, chiede la pena di morte per “omosessualità aggravata” – per esempio per le persone gay con HIV che praticano sesso o persone gay che fanno sesso con minorenni.- Nota popolarmente come la “legge che uccide i gay” renderebbe un crimine anche il non riferire che qualcuno che tu conosci pratichi l’omosessualità mettendo così i genitori, i parenti e gli amici a rischio.

“Una delle cose che gli Ugandesi dicono è che essere gay è cultura europea, che è non-africana” spiega John, 31 anni “C’è l’idea che gli Europei e gli Americani  stiano corrompendo persone per essere gay dando loro denaro per esserlo”.

Lo scorso ottobre il tabloid ugandese anti gay “Rolling Stone” ha pubblicato una lista di 100 omosessuali top della nazione sotto il titolo “Impiccateli”. In gennaio il famoso attivista per i diritti dei gay David Kato è stato ucciso picchiato a morte nella sua casa da un assassino con un  martello. I gay, le lesbiche e le persone transgender in Uganda fronteggiano vessazioni, vandalismo, condanne a morte e violenza su base quotidiana. Possono essere licenziati se fanno outing, costretti a contrarre matrimonio eterosessuale e imprigionati dalle autorità senza accusa o senza avere una difesa legale. In alcuni dei casi peggiori possono essere sottoposti allo “stupro correttivo”.

Non è solo l’Uganda – per anni il mondo sviluppato ha chiuso un occhio sulla questione – la persecuzione sanzionata degli omosessuali, secondo l’Osservatorio sui Diritti Umani, esiste in 38 su 53 nazioni africane. Ora un lungo film documentario con nuove caratteristiche cerca di ristabilire l’equilibrio. “Getting Out”, diretto dalla regista Alexandra Chapman insieme con Christian Aid, narra la storia dei rifugiati gay che sono costretti a subire discriminazioni nelle proprie nazioni.

“E’ importante per le persone dell’Occidente capire che l’omofobia legalizzata e sanzionata dallo Stato è una realtà in molte parti dell’Africa” dice il dott. Chris Dolan, direttore del progetto di Legge per i Rifugiati all’Università Makerere a Kampala che è stato fondamentale nella creazione del film. Dolan che si batte per la protezione dei diritti delle tutte le minoranze assediate in questo corno d’Africa, dice che il clima politico in Uganda “permette un ampio raggio di abusi e violazioni che diminuiscono seriamente la qualità della vita di tutte le persone lesbiche, gay, bisessuali, trans gender e intersex la maggior parte delle quali cercano evitare “il controllo pubblico” e pone anche tali persone in un pericolo serio ed estremo.

Per John il pericolo è presto diventato troppo grande per essere ignorato. Al “freshers’ ball” della sua università ha incontrato e si è innamorato di un uomo chiamato Aziz. Erano discreti facendo attenzione a non essere visti agire troppo intimamente in pubblico. In questo modo – non essendo mai del tutto onesti, vivendo in penombra, sempre guardandosi dietro le spalle – la loro relazione è continuata dopo la laurea quando John ha trovato un lavoro ben remunerato in banca. Quando John ha portato Aziz per la prima volta a casa a visitare la sua famiglia è stato presentato come “il mio miglior amico”. E’ diventato come un altro figlio per mia madre. Questa è stata la nostra vita fino al 2001”.

Poi ogni cosa è cambiata. Un gruppo di amici gay di John sono stati arrestati in una retata della polizia. Sono stati picchiati e costretti a dire i nomi di altre persone gay che conoscevano. John si è reso conto che doveva andar via. “Dovevo scomparire” dice “Avevo un po’ di denaro messo da parte così ho pagato un’agenzia privata per farmi avere un visto un passaporto … non ho detto a nessuno che stavo partendo, neanche alla mia famiglia. Non sapevo dove stavo andando. Ma poi, fortunatamente, quella persona mi ha dato un visto per la Gran Bretagna”.

John Bosco non sapeva allora che i suoi problemi stavano soltanto cominciando.

Florence Kizza sorride tanto. Ha un viso carino e affilato con occhi inclinati e denti dritti e bianchi. Quando parla lo fa con una voce chiara e uniforme, il suo leggero accento ugandese pone le parole in un ritmo irregolare. La incontriamo in un caffè di Richmond nel Surrey vicino a dove lavora in una banca. Sebbene la storia che lei mi narra sia orrenda Florence non mostra emozioni mentre la racconta oltre a un leggero stringere gli occhi, uno sguardo da un lato, una breve pausa nella sua narrazione. Spiega che crollare e piangere sarebbe cedere a qualcosa a cui lei deve resistere. Perché Florence è una donna che si definisce una sopravvissuta.

Florence ha 32 anni, è ugandese ed è lesbica. E’ cresciuta a Najjanankumbi sul versante meridionale di Kampala, figlia di un ricco uomo d’affari che ha mandato Florence e sua sorella a una prestigiosa scuola superiore per ragazze.

“Mi sono resa conto (della mia sessualità) a scuola ma sono quelle cose di cui non si parla” dice “E’ qualcosa che non esprimi mai a voce alta. Sono stata cresciuta da cattolica. Ogni giorno questi sacerdoti pregavano che una persona gay dovrebbe essere lapidata a morte, che dovrebbero morire. Se tu ascolti questo, puoi mai aprirti?”

Quando Florence aveva 16 anni entrambi i suoi genitori morirono di Aids a un anno di distanza l’uno dall’altro. Florence fu tolta dalla scuola e allevata dai parenti. Più grande diventava, più aumentava la certezza di essere lesbica. Sola e sempre più isolata lei desiderava ardentemente compagnia. E allora, facendo la spesa un giorno al mercato, ha incontrato una donna di nome Susan originaria della parte occidentale del paese. “Parlava una lingua diversa” dice Florence“ ma ci siamo subito trovate legate. Siamo andate a prendere un caffè abbiamo parlato e poi ci siamo incontrate altre cinque volte”. Gradualmente le due donne sono diventate più legate ma, come John Bosco, stavano attente a come si comportavano insieme in pubblico. Florence ha continuato a vivere sola. Ancora, il fatto che era una donna in età matrimoniale senza un marito provocava sospetti nella comunità locale.

Nel dicembre 2000 i vicini irruppero in casa sua e la trovarono a letto con Susan, I paesani strapparono gli abiti delle due donne denudandole, le fecero sfilare lungo le strade e poi le picchiarono di fronte a una folla ululante. “Dire che fu doloroso non rende l’idea” dice Florence ora. “Puoi venire picchiata ma essere umiliata in giro davanti a Dio sa quanta gente – perdi la tua dignità. Ho provato la sensazione di desiderare di morire in quel momento”.

Bandita dal suo villaggio, Florence è stata costretta a cercare un altro luogo dove stare. Ha trascorso le notti a casa di Susan, svegliandosi presto la mattina per scivolare fuori di nascosto con l’oscurità. Ma nonostante abbia cercato di essere attenta non è bastato. Nel marzo del 2001 Florence è stata arrestata e, dopo tre giorni, picchiata e violentata da tre poliziotti in caserma. L’assalto è stato così feroce che, dieci anni dopo, Florence ne porta ancora le ferite. E’ freddo quando ci incontriamo e Florence indossa un maglione con la cerniera alzata e le maniche lunghe ma, perfino col tempo più mite, non ama mostrare le strisce martoriate che serpeggiano lungo la pelle di tutte e due le braccia.

“Guardando indietro penso che gli ufficiali di polizia mi abbiano trovato molto provocante” dice Florence e chiude e metà gli occhi come se tentasse di apparire distante, tenebrosa “C’è stato un momento in cui uno di loro mi ha colpito il secondo giorno e io lo guardavo e non piangevo. Lo guardavo molto, molto calma. Gli dicevo: hai finito? Non mi fai male, e ridevo” mi guarda e incontra il mio sguardo “e lui ha smesso”.

Il terzo giorno Florence è scappata quando uno dei poliziotti si è ubriacato ed è stata in grado di rubare le chiavi della sua cella. E’ corsa fuori nelle strade e ha preso un taxi verso la casa di un amico. Sapeva che doveva uscire dalla nazione prima che la polizia la rintracciasse. Lei e Susan attraversarono il confine con il Congo. Là hanno pagato un “trafficante umano” per portarle in Gran Bretagna “Ha detto: Ok, dovete pagarmi un prezzo molto alto. Ha chiesto 20.000 sterline. Ho dovuto dare uno dei pezzi di terra di mio padre come garanzia”.

All’ultimo minuto il trafficante ha detto che era troppo pericoloso per me e Susan viaggiare nello stesso momento. Ha detto: “Una sola persona, scegliete”. Ho detto a Susan di andare lei perché io ero malata, non avevo l’energia. Ma loro hanno detto che dovevo andare io perché la mia salute era brutta e stavo peggiorando”.

Nel settembre 2001 Florence è volata in Gran Bretagna ed è stata portata dal trafficante a un B&B a Wembley nel nord di Londra. Le ha dato una banconota da 50 sterline e l’ha lasciata lì. All’età di 22 anni Florence era da sola in una città straniera enorme con poco denaro e nessuna prospettiva. Per giorni ha girato per le strade insicura su cosa fare o a chi rivolgersi. Dopo le sue esperienze in Uganda guardava ognuno senza fiducia e con sospetto. Ha dovuto elemosinare soldi per mangiare.

Un uomo di un gruppo di una chiesa locale un giorno la portò all’Ufficio Centrale per farsi dare lo status di rifugiata ma Florence era profondamente intimidita dalla procedura dell’intervista. “fondamentalmente non mi fidavo delle autorità a causa delle brutte esperienze che avevo avuto in Uganda” dice Florence “le interviste erano degradanti. Mi chiedevano di parlare della mia vita personale, di spiegare come facevo sesso. Il modo in cui mi guardavano … pensavo … Gesù Cristo … sono così disgustosa? Onestamente ero così arrabbiata. Non avevano alcuna idea”.

Già sofferente per la patologia da stress post trauma e per la depressione i sintomi di Florence peggiorarono man mano che viaggiava attraverso il sistema legale della Gran Bretagna. La sua iniziale richiesta di rimanere fu rifiutata con le motivazioni che sarebbe stata al sicuro se fosse ritornata in Uganda, ricollocata in un’area diversa della nazione e se avesse agito “con discrezione”.

Secondo Erin Power, il manager del gruppo dell’_Immigration_ _Group Lesbian and Gay_ in Gran Bretagna, una larga parte del problema per i rifugiati ugandesi gay è il non desiderio di parlare apertamente di una identità sessuale che hanno mantenuta segreta per tutta la loro vita, spesso perfino a se stessi. “Se fanno quello che ci si aspetta che facciano e si avvicinano all’Home Office appena arrivano in Gran Bretagna, vengono intervistati da un’autorità in uniforme a cui devono dire di essere gay” dice Power “ma quella è la stessa persona che, nella loro nazione, li perseguiterebbe per aver parlato apertamente”.

“Abbiamo richiedenti che non hanno mai detto prima di essere gay. L’idea che loro possano identificare se stessi è problematica perché loro hanno spesso tenuto segreto questa cosa per tutta la vita … alcuni richiedenti non hanno mai fatto sesso ma noi sosteniamo che essere del mondo LGBT non significa con chi fai sesso, è chi sei e qual è la tua identità. Abbiamo combattuto per far sì che il nostro Home Office non si focalizzasse sul sesso. Fino a oggi si devono provare due cose: una, che sei gay o lesbica e due, che la tua nazione è pericolosa”.

Come provi che sei gay? Power ride “Tutti ci chiedono sempre questo”.

John Bosco stava affrontando problemi simili in una diversa parte della città. Essendo arrivato con 600 sterline, John ha trovato una stanza in affitto a Manor Park nel nord di Londra. “Ho pensato che se mi fosse riuscito arrivare in una nazione di lingua inglese, sarei stato OK. Non appena sono arrivato ho cercato un lavoro perché avevo le qualifiche giuste. Non conoscevo il sistema dell’asilo politico per niente”.

Quando ha tentato di cercare un lavoro gli è stato detto che aveva bisogno di un “national insurance number”. “Non sapevo cosa fosse” dice John. Finalmente un giorno un gruppo di giamaicani che ha incontrato per strada lo ha diretto verso gli uffici britannici della Boarder Agency a Croydon ma invece di quella che immaginava sarebbe stata un’intervista diretta, John dice che è stato fatto spogliare nudo, gli sono state chieste le impronte digitali, caricato su un camion e portato al centro di detenzione per l’immigrazione di Oakington nel Cambridgeshire.

Qui lui ha parlato alle autorità attraverso un interprete che era, però, di un’altra parte dell’Uganda e non parlava la stessa lingua tribale, così le affermazioni di John sono state rifiutate come inconsistenti. John, terrorizzato su come avrebbe potuto reagire l’autorità della Gran Bretagna, non disse loro che era gay e che questa era la vera ragione per cui era fuggito dall’Uganda. “Mi chiesero se volevo un avvocato” dice ora “lui dice no scuotendo la testa. “Non sapevo cosa significasse questa parola”.

Non riuscire a farsi capire o a fornire una storia consistente per spiegare il suo status di rifugiato finì per costare caro a John. Da Oakington è stato portato a Haslar, un centro per la rimozione dell’immigrazione gestito dal servizio della prigione a Portsmouth. Per le prime settimane non ha avuto cambio di vestiario e doveva lavare il suo unico paio di mutande ogni giorno. Quando una volontaria locale lo ha visitato per chiedergli se aveva bisogno di qualche aiuto, John le ha finalmente confessato tutto.

“Quando mi ha chiesto: “Perché sei fuggito?” io ho risposto “A causa della mia sessualità”. Lei ha detto “E’ Ok, non è un problema”. Mi sono dovuto sedere a queste parole. “Si sdraia nella sua sedia al caffè, incrociando le braccia sul petto con un’espressione di shock negli occhi. “Stavo tremando. Non avevo mai avuto nessuno che mi avesse parlato in quel modo. Lei era la prima persona  a cui avevo mai parlato della mia sessualità ed è stata carina con me”. Non ce la fa più, china la testa e si asciuga rapidamente gli occhi pieni di pianto.

Dopo Quattro mesi a Haslar a John viene dato il permesso di stare in Gran Bretagna ma l’Home Office si è appellato contro questa decisione. Per i seguenti sei anni, dal 2002 al 2008, la vita di John è diventata un ciclo stancante di battaglie legali. Ha ottenuto un posto lavorando a un “charity” di salute mentale di Southampton e ha speso 21.000 sterline del suo denaro in avvocati. Nel 2008 durante una visita di routine alla stazione di polizia (i termini del suo permesso per rimanere nella nazione richiedevano che andasse alla polizia una volta al mese) è stato messo a braccia dentro un camion, portato all’aeroporto e messo su un volo di ritorno in Uganda.

“Stavo pensando, perché non mi uccidete. Non ho amici, nessun parente, niente. Quanto dovrà durare questa cosa? Non ho intenzione di cambiare me stesso per essere accettato”.

Come per Florence Kizza il giudice del suo caso aveva deciso che John non avrebbe fronteggiato alcun pericolo immediate se fosse ritornato in Uganda, cambiato il suo comportamento e si fosse trasferito in una diversa parte della nazione per vivere “con discrezione”. Questo nonostante il fatto che la fotografia di John fosse stata stampata sulla prima pagina di un quotidiano in Uganda solo poche settimane prima che fosse stato deportato. Vivere con discrezione era proprio l’ultima cosa che avrebbe potuto fare.

Dopo pochi giorni che era rientrato a Kampala, John è stato arrestato. La polizia lo ha gettato in una cella con varie atre persone e lo ha sottoposto a regolari bastonature. “Le bastonature non sono qualcosa a cui puoi dire che ti abitui” dice ora “E’ qualcosa che ti aspetti”.

Ha corrotto la polizia per farsi rilasciare con il poco denaro che gli era rimasto ed è andato a nascondersi per sei mesi. Alla fine i suoi avvocati gli hanno fatto ottenere lo status di rifugiato per cinque anni ed è stato fatto tornare in Gran Bretagna. Ma il permesso finisce nel 2014 e John ancora vive in uno stato di ansiosa incertezza, isolato dalla sua famiglia, dai suoi amici e dal suo ex compagno Aziz,  non è riuscito a rintracciare nessuno di loro.

“Ho ancora brutti sogni: persone che mi prendono in giro, di essere picchiato” dice “Talvolta dormo e penso cosa succederà dopo il 2014? Tutto quello che voglio è libertà dove posso essere chi sono”.

A Florence è stato dato lo status di rifugiata lo scorso anno. Da quando ha lasciato l’Uganda ha completato la sua laurea in “business management” alla Kingston University. Per un periodo ha lavorato per un supermarket: ora ha un impiego negli uffici di una banca in una bella strada di Twickenham. Non ha mai più avuto notizie della sua compagnia Susan. “Abbiamo tentato in tutti i modi di trovarla” dice con la voce intrisa di emozione “Penso che mi sto abituando a questo”.

Nel luglio 2010 la Corte Suprema della Gran Bretagna ha categoricamente denunciato il “ragionamento a discrezione”, che è stato centrale nel respingere le richieste di rifugiato sia di Florence che di John, sostenendo che la decisione ha fallito nel riconoscere i diritti umani degli omosessuali e ha contravvenuto alla convenzione per i rifugiati delle nazioni Unite. L’Home Office ha quindi prodotto una serie di linee guida, consultandosi con gruppi di asilo, su come asserire la validità di tali richieste e a tutti i “senior case-workers” è stato fatto frequentare una sessione di stage di un giorno su questi argomenti. “Quel processo è terminato alla fine di febbraio” dice Erin Power “così non sappiamo quale sarà il risultato. Ovviamente speriamo che ci sarà qualche miglioramento perché alcune delle interviste erano onestamente abbastanza orribili”.

Di nuovo nel caffè di Southampton la cioccolata calda di John è diventata fredda. Dice che gli manca la sua famiglia “sempre” e che non ha molta vita sociale, sentendosi troppo nero per essere completamente accettato dalla comunità gay prevalentemente in questa parte del mondo e troppo gay per essere completamente accettato dalle persone “dritte, giuste” che incontra. Trascorre la maggior parte delle serate e finesettimana in una stanza in affitto guardando telenovele in TV. “richiamare i ricordi mi stressa troppo” dice alla fine della nostra conversazione “ ma la ragione per cui lo faccio è perché se non lo facessi le persone non capiranno cosa sta succedendo, specialmente le persone in Uganda che non hanno voce. L’unico modo perché capiscano è per me quello di parlarvi di ciò”.

Mentre spinge la sedia sotto il tavolo dice che è afflitto da due domande. “Mi chiedo tutto il tempo perché sono nato gay? E se sono nato gay, perché sono nato in Africa?”

Se ne va lasciando che la porta del caffè si chiuda delicatamente in silenzio dietro di lui e girandosi per darmi un saluto con la mano e un sorriso attraverso la finestra mentre se ne va. Forse non ci sarà mai una risposta. Ma per ora, almeno, John Bosco è libero di porre le domande ad alta voce.

_A questo articolo si aggiunge un messaggio di Moses Nsubuga - Information Officer - Refugee Law Project_

Cari amici e colleghi

Nella seduta di oggi a Londra (martedì 29 marzo) per il lancio di “Getting Out”, un nuovo documentario sulla ricerca di asilo LGBT prodotto dal Refugee Law Project (Progetto di Legge per i Rifugiati) in collaborazione con la Coalizione per una Società Civile sui Diritti Umani e la Legge Costituzionale in Uganda. Potreste essere interessati a leggere un articolo importante dedicato al film e alle interviste con molte delle persone coinvolte nella sua creazione. Il brano è apparso nel “Sunday Observer” britannico di ieri, 27 marzo 2011, e può essere trovato al seguente link:  
[http://www.guardian.co.uk/world/2011/mar/27/uganda-gay-lesbian-immigration-asylum#start-of-comments](#start-of-comments)  
vi prego di notare che l’articolo contiene due errori importanti:

1.  1)    Il documentario è stato prodotto dal Law Project Refugee in collaborazione con la UCSCHRCL e NON da Christian Aid.
2.  2)    2) il regista principale è stato Daniel Neumann con l’assistenza di Alexandra Chapman e Chris Dolan.

Copie del documentario sono ora disponibili da parte del Refugee Law Project su richiesta. E’ uno strumento utile per chiunque opera su argomenti come asilo politico e immigrazione.

Per richiederne una copia per favore scrivere una email a Angella Nabwowe a [a.nabwowe@refugeelawproject.org](mailto:a.nabwowe@refugeelawproject.org)  
  
Moses Nsubuga - Information Officer - Refugee Law Project