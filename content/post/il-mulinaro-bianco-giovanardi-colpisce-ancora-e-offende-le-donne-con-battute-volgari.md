---
title: 'Il mulinaro bianco Giovanardi colpisce ancora e offende le donne con battute volgari'
date: Sun, 12 Feb 2012 15:10:37 +0000
draft: false
tags: [Politica]
---

Questa volta se la prende con le donne che si baciano in pubblico e le equipara a chi fa pipì in strada. Prima o poi sarà espulso anche lui da qualche organo, speriamo politico.

Roma, 12 febbraio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Il mulinaro bianco Carlo Giovanardi, esponente di punta del pdl clerical-fondamentalista, ha colpito ancora. Oggi se la prende con le donne che si baciano in pubblico e le equipara “a chi fa la pipì in strada”. Non contento dell’intervento di alta e nobile politica ha poi affermato a Radio 24 che: "Ci sono anche faccende delicate, ci sono infatti problemi di batteri, eccetera eccetera, che richiedono una grande attenzione nel momento in cui si fanno certe pratiche”. Non ha saputo trattenersi quando poi si è messo a dare lezioni di anatomia applicata all’erotismo-sessuale, difatti ha aggiunto che  “ci sono organi costruiti per ricevere e organi costruiti per espellere”.

Al neo anatomo-fisiologo, divenuto ora grande esperto di come si devono svolgere i rapporti sessuali facciamo sapere che prima o poi la società lo espellerà, così' come sempre accaduto con persone come lui che, con l’ironia e l’idiozia, per farsi un po’ di pubblicità d’accatto, perché null’altro ha da dire, offende milioni di donne e di persone con battute patetiche.

Si vergogni di quello che dice per sopravvivere,  si ritiri prima di essere lui espulso da qualche organo, speriamo politico.