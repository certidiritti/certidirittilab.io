---
title: 'Newsletter 08 Luglio'
date: Wed, 31 Aug 2011 10:16:32 +0000
draft: false
tags: [Politica]
---

**![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)**

Per ogni passo in avanti, l’Italia rischia di farne due indietro.

La Camera dei Deputati, all’alba del 2011, ha riconosciuto, con l’approvazione di un Disegno di legge che modifica il Codice Civile, l’uguaglianza tra figli nati nel matrimonio e quelli naturali.

Ma sempre la Camera martedì prossimo potrebbe approvare una legge (contro) il testamento biologico che nega il diritto di rifiutare trattamenti come l’alimentazione e l’idratazione forzata, una legge che Stefano Rodotà ha definito “ideologica, violenta, bugiarda, sgrammaticata, incostituzionale”.   

Intanto il voto sulla legge contro l’omofobia slitta ancora, ma Certi Diritti insieme alle altre associazioni è pronta a scendere in piazza per chiederne l’approvazione.

Dopo l’approvazione del matrimonio tra persone dello stesso sesso nello stato di New York, anche in Italia sembra avviarsi un dibattito sul tema. Daria Bignardi e Alessandro Capriccioli dai loro blog esortano la sinistra a fare la sua parte e sono diverse le coppie italiane che hanno annunciato il proprio matrimonio all’estero nei prossimi due mesi. La nostra campagna di Affermazione Civili intanto prosegue. Sono già pronti altri ricorsi oltre ai [due presentati alla Corte Europea dei Diritti dell’Uomo](tutte-le-notizie/1159-matrimonio-due-coppie-gay-ricorrono-alla-cedu-contro-litalia.html) nel mese di giugno.

Buona lettura

ASSOCIAZIONE
============

**Il pride, il cardinale e la promessa di Pisapia.   
[Leggi >](tutte-le-notizie/1189.html)**

**Interrogazione Ue su campagna europea contro prostituzione.   
[Leggi >](tutte-le-notizie/1188.html)**

RASSEGNA STAMPA
===============

**Piemonte/Verdi chiedono dimissioni Cota: aveva vinto Bresso.  
[Leggi >](http://www.tmnews.it/web/sezioni/politica/PN_20110630_00057.shtml)**

**Il sogno di Matteo e Matteo "Vogliamo sposarci a New York".  
[Leggi >](http://bologna.repubblica.it/cronaca/2011/06/28/news/sposarci_a_new_york_il_sogno_vicino-18324643/)**

**Christus Rex: 'Matrimoni' e bambini agli omosessuali, vergogne internazionali!  
[Leggi >](http://www.gaynews.it/articoli/Primo-piano/87011/Christus-Rex--Matrimoni--e-bambini-agli-omosessuali-vergogne-internazionali.html)**

**Primo passo verso riconoscimento coppie di fatto? Niente più figli di serie B. Stessi diritti per figli nati fuori da matrimonio.  
[Leggi >](http://ricerca.repubblica.it/repubblica/archivio/repubblica/2011/07/01/niente-piu-figli-di-serie-bimbi-nati.html)**

**Versace: sì a legalizzare le coppie gay.  
[Leggi >](http://www.quotidianamente.net/cronaca/fatti-dallitalia/versace-si-a-legalizzare-le-coppie-gay-2282.html)**

**Matrimoni gay, il sindaco di Castelnovo Sotto riapre il dibattito.  
[Leggi >](http://www.reggionline.com/it/2011/07/05/matrimoni-gay-il-sindaco-di-castelnovo-sotto-riapre-il-dibattito-5159)**

**Basta nascondersi dietro quel "non siamo pronti" ****S****u Vanity Fair un articolo di Daria Bignardi sul matrimonio tra persone dello stesso sesso.  
[Leggi >](rassegna-stampa/1190.html)**

**Dal 17 al 20 luglio. La conferenza internazionale sull’Aids si tiene in Italia, l’unico paese che ha tagliato i fondi per la ricerca.  
[Leggi >](http://rassegnastampa.unipi.it/rassegna/archivio/2011/07/04SIA1224.PDF)**

**Pillola dei “5 giorni dopo”, mozione del Pdl per fermarla.   
[Leggi >](http://www.ilsecoloxix.it/p/magazine/2011/06/28/AOuB5Og-pillola_mozione_fermarla.shtml)**

**Gay Village: aggressioni e minacce all'interno del parco.  
[Leggi >](http://eur.romatoday.it/gay-village-2011-minacce-aggressioni.html)**

**La movida non è per tutti: ennesima discriminazione sui trans.  
[Leggi >](http://www.liniziativa.net/news/?id_news=813&La+movida+non+%C3%A8+per+tutti:+ennesima+discriminazione+sui+trans&path=13)**

**“Sei gay? La Chiesa cattolica non ti fa il funerale”   
[Leggi >](http://www.giornalettismo.com/archives/131827/sei-gay-la-chiesa-cattolica-non-ti-fa-il-funerale/)**

**Tv. Un sito di incontri per preti ‘omosensibili’ mostrato in tv da un’inchiesta di ‘Tabloid’.   
[Leggi >](http://www.notiziegay.com/?p=249)**

**Avvenire spiega qual’è il ‘puro amore’.  
[Leggi >](http://www.notiziegay.com/?p=62)**

**Giuliano Ferrara attacca le famiglie arcobaleno. “L’antinatalismo mascherato da libertà” .  
[Leggi >](http://www.notiziegay.com/?p=209)**

**Voglia di matrimonio.   
[Leggi >](http://www.carta.org/2011/07/voglia-di-matrimonio/)**

**Angelino Alfano: le famiglie sono solo quelle formate da uomo e donna con figli.  
[Leggi >](http://www.queerblog.it/post/11510/angelino-alfano-le-famiglie-sono-solo-quelle-formate-da-uomo-e-donna-con-figli)**

**Ebrei, musulmani, gay, russi, zingari: l'odio corre in rete.   
[Leggi >](http://ipsnotizie.it/nota.php?idnews=1777)**

**L’Europarlamento parla di diritti delle minoranze sessuali nel mondo.   
[Leggi >](http://www.notiziegay.com/?p=291)**

**Ungheria: Ue boccia la nuova Costituzione; Orban, rispettateci.  
[Leggi >](http://www.bluewin.ch/it/index.php/564,431458/Ungheria__Ue_boccia_la_nuova_Costituzione;_Orban,_rispettateci/it/news/estero/sda/)**

**Svizzera. Le unioni registrate non bastano, “perché i gay non possono essere buoni genitori?   
[Leggi >](http://www.ticinolibero.ch/?p=71979)**

**Ginevra. Poco più di mille persone alla parata del Gay Pride.  
[Leggi >](http://gaynews24.com/2011/07/02/ginevra-poco-piu-di-mille-persone-alla-parata-del-gay-pride/)**

**I gay perseguitati in Russia si fotografano in cella e pubblicano su Facebook.   
[Guarda le foto >](http://www.repubblica.it/esteri/2011/06/27/foto/i_gay_in_cella_le_foto_su_facebook-18295198/1/?ref=HREC1-5)**

**Bbc: lesbiche sudafricane, stupri correttivi e omicidi.  
[Leggi >](http://it.peacereporter.net/articolo/29218/Bbc%3A+lesbiche+sudafricane%2C+stupri+correttivi+e+omicidi)**

**USA. Il Rhode Island approva le unioni civili.   
[Leggi >](http://www.ilpost.it/2011/06/30/il-rhode-island-approva-le-unioni-civili/)**

**Barack Obama: sul matrimonio gay decidono i singoli Stati dell’unione.  
[Leggi >](http://gaynews24.com/2011/06/30/barack-obama-sul-matrimonio-gay-decidono-i-singoli-stati-dellunione/)**

**Cuba, omosessuali scendono in piazza: basta repressioni!  
[Leggi >](http://www.net1news.org/cuba-omosessuali-scendono-in-piazza-basta-repressioni.html)**

**Brasile. Visite speciali a detenuti gay.   
[Leggi >](http://gaynews24.com/2011/07/05/brasile-visite-speciali-a-detenuti-gay/)**

**Perù. In centinaia al Gay pride di Lima.  
[Leggi >](http://gaynews24.com/2011/07/04/peru-in-centinaia-al-gay-pride-di-lima/)**

**Thailandia. ****Le trans al voto derise e offese****.   
[Leggi >](http://www.giornalettismo.com/archives/131866/trans-thailandia/)**

**Il Premier di Samoa loda trans: miracoli di Dio.  
[Leggi >](http://gaynews24.com/2011/07/05/il-premier-di-samoa-loda-trans-miracoli-di-dio/)**

**India. Bufera sul ministro della sanità che ha dichiarato: l’omosessualità è “una malattia”   
[Leggi >](http://gaynews24.com/2011/07/05/india-bufera-sul-ministro-della-sanita-che-ha-dichiarato-lomosessualita-e-una-malattia/)**

STUDI, RICERCHE E MATERIALI INFORMATIVI
=======================================

**A Message of Hope from the United States Senate. **Messaggio contro l'omofobia del Senato americano (inglese) **  
[Guarda il video>](http://www.youtube.com/watch?v=5GjS2XrD2wc&feature=youtu.be)**

**Lo psicologo Marchesini: «non possiamo scegliere il nostro genere.   
[Leggi>](http://www.uccronline.it/2011/06/27/lo-psicologo-marchesini-%C2%ABnon-possiamo-scegliere-il-nostro-genere-sessuale%C2%BB/)**

**L’altra faccia del Gay Marocchino in Italia.   
[Leggi>](http://www.gaymarocco.com/articoli/233-laltra-faccia-del-gay-marocchino-in-italia.html)**

**Islam e gay, integralisti VS liberali.  
[Leggi>](http://www.ilgrandecolibri.com/2011/06/islam-e-gay-integralisti-vs-liberali.html#more)**

**Francesi pro-adozioni per coppie gay.   
[Leggi>](http://www.west-info.eu/it/francesi-pro-adozioni-per-coppie-gay/)**

**Croazia: per il 47% degli studenti, l'omosessualità è una malattia e no alla manifestazioni gay per il 60%.  
[Leggi>](http://www.queerblog.it/post/11512/croazia-per-il-47-degli-studenti-lomosessualita-e-una-malattia-e-no-alla-manifestazioni-gay-per-il-60)**

LIBRI E CULTURA
===============

**Il bacio che piace a Saffo e fa inorridire Repubblica.  
[Leggi>](http://www.ilgiornale.it/cultura/doppia_immorale/02-07-2011/articolo-id=532707-page=0-comments=1)**

**Nella Stoccolma liberal l’asilo che ha abolito i generi.  
[Leggi>](http://www.zeroviolenzadonne.it/rassegna/pdfs/6a7fd99bbf14809e560f1a9251946f1f.pdf)**

**La pubblicità che strizza l'occhio agli attraversamenti di genere.   
[Leggi>](http://www.queerblog.it/post/11500/la-pubblicita-che-strizza-locchio-agli-attraversamenti-di-genere)**

**Uruguay: Eduardo Galeano (e altri vip) a favore del matrimonio gay.   
[Leggi e guarda i video>](http://www.queerblog.it/post/11490/uruguay-eduardo-galeano-e-altri-vip-a-favore-del-matrimonio-gay)**

**Il nudo maschile del pittore Henry Scott Tuke.  
[Leggi>](http://www.queerblog.it/post/11493/il-nudo-maschile-di-henry-scott-tuke)**

**Lil B: è uscito l'album "I'm gay"   
[Leggi>](http://www.queerblog.it/post/11504/lil-b-e-uscito-lalbum-im-gay-e-la-cover-omaggia-marvin-gaye)**

**Michael Cunningham: "Sui diritti gay Obama ha deluso"****   
[Leggi>](http://www.gqitalia.it/show/lifestyle/2011/7/michael-cunningham-al-limite-della-notte-sui-diritti-gay-obama-ha-deluso-omofobia-governo-devono-fare-piu)**

**La bisessualità come orientamento****.  
[Leggi>](http://www.queerblog.it/post/11516/la-bisessualita-come-orientamento)**

**Ora basta santificazioni, Pasolini resta un lupo.  
[Leggi>](http://www.ilgiornale.it/cultura/basta_santificazioni_pasolini_e_lupo/03-07-2011/articolo-id=532868-page=0-comments=1)**

**Elettra Groppo, [Due non è il doppio di uno](http://www.deastore.com/libro/due-non-e-il-doppio-elettra-groppo-elmi-s-world/9788897192008.html), Elmi’s World,** **€ 18.40**

**Ricky Martin. [Me](http://www.arcanaedizioni.com/index.php?page=shop.product_details&category_id=5&flypage=flypage_arcana_2.tpl&product_id=162&vmcchk=1&option=com_virtuemart&Itemid=53), Arcana editore, € 18,00**

**Lombardi – Rotta, [Nudo di donna con Presidente](http://www.lafeltrinelli.it/products/9788897426011/Nudo_di_donna_con_Presidente/Rotta_Stefano.html),  Fuorionda,  € 16,00**

[www.certidiritti.it](http://www.certidiritti.it/)
==================================================