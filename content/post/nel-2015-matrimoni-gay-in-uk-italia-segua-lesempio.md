---
title: 'Nel 2015 matrimoni gay in Uk, Italia segua l''esempio'
date: Sun, 18 Sep 2011 21:02:07 +0000
draft: false
tags: [Europa]
---

Annunciata per il 2015 la legge per i matrimoni tra persone dello stesso sesso in Gran Bretagna. Intanto prosegue in Italia la campagna di Affermazione Civile che prevede ricorsi alla Corte Europea dei Diritti dell'Uomo delle coppie gay italiane e miste costrette a sposarsi all'estero.

  
Roma, 18 settembre 2011  
   
Comunicato Stampa dell'Associazione Radicale Certi Diritti  
   
Mentre in Italia assisitiamo a deliranti prese di posizione, da destra, sinistra, sopra e sotto, sulle coppie gay che non avrebbero diritto ad accedere all'istituto del matrimonio, ci giunge notizia dalla Gran Bretagna che il Governo nel 2015 legalizzerà i matrimoni tra persone dello stesso sesso. Lo hanno reso noti i Liberal-democratici annunciando anche il sostegno del Primo Ministro David Cameron e del suo Vice Nick Clegg.  
  
Noi, nell'attesa della prossima dichiarazione del (molto) Sottosegretario Carlo Giovanardi, che questo week end non ci ha ancora fatto sapere qual'è la sua presa di posizione sul tema,  continuiamo con la campagna di Affermazione Civile che prevede ricorsi alla Corte Europea dei Diritti dell'Uomo delle coppie gay italiane e miste costrette a sposarsi all'estero.