---
title: 'A ROMA FALLIMENTO DELLE POLITICHE PROIBIZIONISTE SULLA PROSTITUZIONE'
date: Sat, 17 Jul 2010 11:05:20 +0000
draft: false
tags: [Senza categoria]
---

**COMUNE DI ROMA: ALEMANNO AMMETTA IL FALLIMENTO DEL PROIBIZIONISMO SULLA PROSTITUZIONE, ANZICHE’ SOLLECITARE IL GOVERNO SU UNA MATERIA POLITICAMENTE "INNOMINABILE".**

**Dichiarazione di** **Sergio Rovasio****, Segretario Associazione Radicale Certi Diritti e** **Riccardo Magi****, Segretario Radicali Roma:**

“Il completo fallimento delle politiche proibizioniste sulla prostituzione, applicate come ‘prove tecniche’ nel Comune di Roma con la delibera del Sindaco Alemanno dal settembre 2008, dimostra la necessità e l'urgenza di un'inversione di marcia e di nuove iniziative coraggiose sul piano politico e sociale.  La richiesta di Alemanno di un intervento del Governo, per giustificarsi di tale fallimento, dimostra di quanta ipocrisia la nostra classe politica sia capace, anche su un tema umanamente così complesso come la prostituzione.

Nei giorni scorsi abbiamo tenuto a Roma, presso la sede dei Radicali, un convegno che ha analizzato le fallimentari politiche proibizioniste sulla prostituzione anche a Roma, con interventi di studiosi, operatori sociali, prostitute, avvocati e politici che su questo tema non sono mai stati ascoltati, specie da quanti hanno la presunzione di risolvere il problema con arresti, multe e altre forme repressive assurde, applicate in violazione dei diritti civili e umani delle persone.

Secondo il Sindaco Alemanno - che guarda caso auspica un intervento del Governo proprio in una materia sulla quale ha fatto calare un silenzio imbarazzato, per questioni politicamente "innominabili" (di cui però tutti sanno tutto) - le multe elevate nella città di Roma, sulla base dell’ordinanza antiprostituzione, nel solo 2009 sono state 14.000; **ma di queste sono state pagate solo il 20**%. Gli introiti corrisponderebbero a un importo di 600.000 euro, ma nessuno dice a quanto ammontano i costi sostenuti per questa politica inutile e demagogica,  ad esempio per l'utilizzo straordinario della Polizia Municipale, con il brillante risultato che, a distanza di un anno e mezzo, **oggi tutte le strade consolari di Roma sono nella stessa, situazione precedente, se non peggiore**.

Nei prossimi giorni presenteremo alcune interrogazioni popolari su questo tema, per conoscere  i dati  dello sperpero di denaro pubblico, nella speranza che questo fallimento ‘romano’ (ma anche di tanti altri comuni italiani) non venga trasformato nell'ennesima 'grande operazione governativa' - in realtà ipocrita e fallimentare ”.