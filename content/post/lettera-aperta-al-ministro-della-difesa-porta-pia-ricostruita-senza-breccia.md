---
title: 'LETTERA APERTA AL MINISTRO DELLA DIFESA: PORTA PIA RICOSTRUITA SENZA BRECCIA'
date: Sat, 08 Nov 2008 10:21:31 +0000
draft: false
tags: [breccia, Comunicati stampa, difesa, lettera, ministro, PORTA PIA]
---

**FESTA DELLE FORZE ARMATE: LA RICOSTRUZIONE DELLA BRECCIA DI PORTA PIA FATTA AL CIRCO MASSIMO E' SENZA LA BRECCIA, SENZA BERSAGLIERI E SENZA NEMMENO L'ELENCO DEI 49 CADUTI IL XX SETTEMBRE 1870. PERCHE' SIGNOR MINISTRO? FORSE E' POSSIBILE RIMEDIARE.**

_**Sergio Rovasio, Segretario dell'Associazione Radicale Certi Diritti ha scritto stamane una Lettera a aperta al Ministro della Difesa:**_

Signor Ministro della Difesa,

stamane mi sono recato in visita alla spettacolare mostra allestita dai militari a Roma al Circo Massimo in occasione dei festeggiamenti per l'anniversario della Vittoria che si festeggia il 4 novembre di ogni anno. All'ingresso dell'esposizione del Circo Massimo è stata ricostruita una enorme scenografia che rappresenta Porta Pia. Il problema, Signor Ministro, è che nella bella ricostruzione di Porta Pia non v'è traccia nè della Breccia, nè dei nomi dei Bersaglieri caduti il 20 Settembre 1870, nè un Bersagliere in carne ed ossa cui chiedere informazioni. L'unica iscrizione rappresentata è una commemorazione latina che guarda caso parla di Papa Pio IV, il Papa che la fece costruire. 

Non vorremmo, Signor Ministro, che dopo quanto avvenuto il 20 settembre scorso a Porta Pia, la memoria di quei 49 Bersaglieri venga definitivamente cancellata. Come ricorderà, a Porta Pia, lo scorso 20 settembre, il rappresentante del Comune di Roma, alla cerimonia commemorativa dell'evento che si fa ogni anno, anzichè ricordare i nomi dei Bersaglieri caduti, lesse i nomi dei 19 Zuavi pontifici. I nomi dei Bersaglieri caduti li lessero il pomeriggio i radicali che organizzarono la vera cerimonia commemorativa, per onorare con il dovuto rispetto la storica data.

Le chiedo, Signor Ministro, di considerare la possibilità che vengano apposti i nomi dei 49 Bersaglieri sulla Porta Pia ricostruita al Circo Massimo per poter onorare al meglio i nostri soldati caduti che permisero a Roma di diventare la Capitala d'Italia.

Cordial saluti,

Sergio Rovasio

Segretario Associazione Radicale Certi Diritti