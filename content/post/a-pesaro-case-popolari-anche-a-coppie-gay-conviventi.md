---
title: 'A Pesaro case popolari anche a coppie gay conviventi'
date: Thu, 01 Sep 2011 21:17:44 +0000
draft: false
tags: [Diritto di Famiglia]
---

Pesaro-Roma, 1 settembre 2011

Dichiarazione di Matteo Mainardi, Presidente Radicali Marche e Sergio Rovasio, Segretario associazione Radicale Certi Diritti

A seguito delle denunce fatte dai Radicali sui Regolamenti riguardanti l'accesso alle Case Popolari, il Comune di Pesaro ha recepito le misure anti-discriminatorie da noi proposte.

Nell' ”Avviso pubblico per la formazione della graduatoria degli aspiranti all'assegnazione di un Alloggio di Edilizia Residenziale Pubblica sovvenzionata nel Comune di Pesaro” infatti si menziona ora la convivenza che “è attestata dalla certificazione anagrafica, che dimostra la sussistenza di tale fatto da almeno due anni antecedenti la scadenza del presente Avviso”.

Ciò significa che d'ora in avanti tutte le coppie, anche quelle omosessuali, potranno fare richiesta di alloggio popolare dato che la legge prevede che si possano costituire famiglie anagrafiche in ragione dell’esistenza di vincoli affettivi, senza ulteriori specificazioni.

Una grande vittoria Radicale, ma prima di tutto una grande vittoria dei cittadini del Comune di Pesaro.