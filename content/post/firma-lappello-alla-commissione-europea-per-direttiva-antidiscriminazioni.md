---
title: 'FIRMA L''APPELLO ALLA COMMISSIONE EUROPEA PER  DIRETTIVA ANTIDISCRIMINAZIONI '
date: Thu, 15 May 2008 15:40:51 +0000
draft: false
tags: [Comunicati stampa]
---

17 MAGGIO 2008 GIORNATA MONDIALE CONTRO L'OMOFOBIA.

IL PARTITO RADICALE NONVIOLENTO E L'ASSOCIAZIONE RADICALE CERTI DIRITTI, [LANCIANO UN APPELLO ALLA COMMISSIONE EUROPEA PER UNA DIRETTIVA ANTI-DISCRIMINAZIONE ESTESA.](http://www.radicalparty.org/lgbt_discrimination/form.php)  

**  
Dichiarazione di Ottavio Marzocchi, responsabile questioni europee dell'Associazione radicale Certi Diritti:  
**

" Per la lotta a tutte le forme di violenza omofobica e transfobica, occorre quanto prima che la Commissione Europea si muova per la direttiva anti-discriminazione che dovrà allargare la protezione per le persone omosessuali anche nell'ambito dei beni e i servizi, la casa, l'educazione, la protezione sociale che tutti i paesi dell'Unione Europea dovranno rispettare.  

Il Partito Radicale Nonviolento, transnazionale e transpartito, insieme all'Associazione radicale Certi Diritti, e le Associazioni lgbt che in Italia vorranno co-promuovere questa mobilitazione, lanciano una campagna nazionale per firmare [una petizione rivolta alla Commissione Europea](http://www.radicalparty.org/lgbt_discrimination/form.php) al seguente link [http://www.radicalparty.org/lgbt_discrimination/form.php](http://www.radicalparty.org/lgbt_discrimination/form.php)

Chiediamo anche alla Commissione Europea di assicurare finalmente la libera circolazione delle coppie dello stesso sesso all'interno dei paesi dell'Unione Europea attraverso il mutuo riconoscimento delle unioni contratte all'estero, e che l'asilo politico sia concesso a coloro che fuggono da persecuzioni e rischio di morte a causa del proprio orientamento sessuale.

Nel mondo, é necessario arrivare all'abolizione della pena di morte, alla criminalizzazione ed alle persecuzioni basate sull'orientamento sessuale, come richiesto dal Parlamento europeo come previsto dal Rapporto Cappato sui diritti umani nel mondo approvato lo scorso 8 maggio. Questi sono fatti concreti che possono dare risposte adeguate alle varie forme di omofobia ancora diffusissime nel mondo".