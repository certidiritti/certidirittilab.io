---
title: 'AMORE CIVILE – CERTI DIRITTI IN RADIO'
date: Thu, 30 Dec 2010 12:28:52 +0000
draft: false
tags: [Comunicati stampa]
---

**LA NOTTE DI NATALE E’ NATA A RADIO RADICALE LA RUBRICA ‘AMORE CIVILE – CERTI DIRITTI IN RADIO’.  LA NOTTE DI CAPODANNO ANDRA’ IN ONDA LA SECONDA PUNTATA SU: LIBRO ‘OPUS GAY’, MALATTIE SESSUALMENTE TRASMISSIBILI, LAICITA'  E TESTAMENTO BIOLOGICO.**

Roma, 30 dicembre 2010

La notte di natale è nata a Radio Radicale la rubrica ‘Amore Civile – Certi Diritti in Radio’ trasmissione curata da Sergio Rovasio, Segretario dell’ Associazione Radicale Certi Diritti.

La seconda puntata andrà in onda la notte di Capodanno 2011, dalla mezzanotte alle due. Tra i temi trattati:

**Discussione in studio con Sergio Rovasio e Alessandro Litta Modignani sul libro ‘Opus Gay’ ( Newton Compton editori), insieme all’autrice  Ilaria Donatio .  Un percorso tra le storie intense e contradditorie di religiosi e fedeli gay e lesbiche.**

**Malattie Sessualmente Trasmissibili: collegamento con Giampaolo Liuzzo della IST Onlus di Milano e Tullio Prestileo, Presidente Analaids Sicilia e Direttore scientifico dell’Istituto nazionale per la medicina delle migrazioni e per la povertà.**

**Altri temi della puntata: laicità, fecondazione assistita, testamento biologico in collegamento con Maurizio Cecconi, Portavoce Rete Laica di Bologna,  l’Avv. Filomena Gallo, Vice Segretario Associazione Luca Coscioni e Maria Laura Cattinari, Presidente di Libera Uscita.**

La trasmissione radiofonica, settimanale, tratterà i temi relativi al diritto di famiglia, delle politiche proibizioniste in tema di prostituzione, della censura ideologica e fondamentalista in tema di informazione sessuale, della prevenzione delle malattie sessualmente trasmissibili, del pregiudizio e della violenza contro le persone transessuali e transgender, della promozione e difesa dei diritti civili e umani delle persone, anche riguardo il loro orientamento sessuale.

Per idee, suggerimenti, proposte, contattare la redazione di ‘Amore Civile – Certi Diritti in Radio’ al seguente indirizzo e-mail: [info@certidiritti.it](mailto:info@certidiritti.it)