---
title: 'CONGRESSO CERTI DIRITTI 2009: RELAZIONE DI CLARA COMELLI, PRESIDENTE DI CERTI DIRITTI'
date: Tue, 24 Mar 2009 07:37:54 +0000
draft: false
tags: [Senza categoria]
---

![Clara Comelli](components/com_joomgallery/img_thumbnails/congresso_annuale_dellassociazione_radicale_certi_diritti_bologna_2009_1/congresso_2009_13_20090317_1972172648.jpg)Grazie di cuore a tutti quanti per aver accettato il nostro invito e soprattutto grazie per lo sforzo organizzativo di molti di voi che ci permette di essere qui oggi, ad un anno di distanza dalla nascita della nostra associazione, per confrontarci e fare il punto sulle attività svolte in quest’anno.  
Siamo qui anche per rinnovare il nostro impegno a costruire sempre più ampie iniziative per sostenere e rivendicare i diritti civili per le persone omosessuali e più in generale per tutti coloro che subiscono discriminazioni relativamente alla propria identità di genere, ai propri comportamenti e scelte e per il proprio orientamento sessuale .  
Le iniziative che proporremo hanno , come è nostra abitudine, un preciso carattere di concretezza.  
Fin dall’inizio infatti abbiamo deciso di rifiutare le azioni simboliche per dare spazio appunto a quelle concrete a dimostrazione che i diritti non sono simbolici ma sono delle prerogative che si possono misurare nella realtà. Potrei dire con uno slogan che l’associazione radicale certi diritti non vive di propaganda ma di fatti concreti.  
In questa mia relazione , prima di portare il discorso su di un ambito più ristretto, vorrei cercare di analizzare in maniera necessariamente generica, quello che è il contesto più ampio in cui la nostra associazione opera.  
Purtroppo il livello di democrazia in Italia oggi è peggiorato rispetto ad un anno fa. O meglio alcuni nodi sono venuti al pettine.  
Abbiamo assistito infatti in questo lasso di tempo ad una situazione molto drammatica nel nostro Paese.  
In maniera eclatante e pubblica sono stati messi in campo dai politici italiani tutti i mezzi, anche quelli antidemocratici, per frenare un processo di evoluzione sociale e per impedire la democratica libertà degli individui ad autoderminarsi.  
E’ ovvio che mi sto riferendo al ben noto caso Englaro e nel proseguire con questo mio ragionamento capirete perché io abbia voluto citarlo.  
Ho detto che più che essere peggiorate le cose, si sono acutizzate delle situazioni. Che il nostro non fosse un Paese da essere preso a modello in fatto di democrazia e diritti civili ce ne eravamo già accorti confrontandoci con altri Paesi europei.  
Ma quello che è successo nello specifico è stata la reazione dura della politica ad una volontà di un singolo cittadino, il signor Englaro, di voler vedere riconosciuti i propri diritti.  
Cosa poteva fare e cosa ha fatto lo stesso ? Si è rivolto necessariamente alla giustizia.  
E siccome la Costituzione prevede la tutela all’autoderminazione, la giustizia gli ha dato ragione.  
Ecco che solo in questo momento ci siamo accorti di vivere in un Paese dove i politici sono disposti a tutto pur di non riconoscere determinati principi che avrebbero dato la stura ad una modernizzazione generale.  
E’ risaputo che la politica italiana oltre ad essere in grave pericolo di democrazia è anche condizionata dalle gerarchie vaticane. L’una e l’altra cosa messe assieme hanno determinato e continuano a determinare un freno all’evoluzione civile dell’Italia.  
Da questa poco rassicurante situazione generale , arrivo ora ai temi che più riguardano questo congresso.  
La promozione e l’affermazione dei diritti civili relativamente ai temi che ho citato all’inizio.  
Con lo stesso ragionamento di Englaro e seguendo un modus operandi comune a tutte le democrazie, anche Certi Diritti ha deciso di rivolgersi alla magistratura per avere delle decisioni che stabiliscano che la richiesta delle coppie dello stesso sesso di contrarre matrimonio non è contraria ai principi sanciti dalla Costituzione.  
Abbiamo messo in piedi così quella che è la oramai conosciuta iniziativa di affermazione civile.  
E l’apporto più importante è stato dato dalle coppie che hanno deciso di aderire a questa iniziativa.  
Senza di loro, senza persone che richiedono i propri diritti, non avremmo potuto fare nulla.  
E’ per questo che un grazie sentito va soprattutto a loro. Che hanno deciso, così come ha fatto Englaro, di sottrarsi alla ipocrisia diffusa del “tanto le cose si possono sistemare in altri modi” ed hanno deciso di intraprendere la via maestra della legalità e della trasparenza.  
E’ ovvio che fondamentale è anche la nostra collaborazione con la Rete Lenford che rende possibile da un punto di vista tecnico questa iniziativa ed anche alle avvocate e agli avvocati di Rete Lenford va il nostro riconoscimento e ringraziamento.  
I radicali infine per la loro storia non avrebbero potuto sottrarsi a questo stimolo ed anzi hanno deciso con questa associazione , che li rappresenta come appendice, di ribadire in maniera sempre più forte e convinta la loro posizione di assoluta apertura ai diritti civili e al ripristino della democrazia.  
Si è creata insomma, una positiva alchimia , per la quale tre componenti , tutte e tre essenziali, hanno messo ognuna per la propria parte, a disposizione energia e professionalità per dare un forte scossone alla pigra e conservatrice politica nostrana.  
A questo punto all’Associazione Radicale Certi Diritti non rimane che continuare su questa strada. I processi sono in corso e le coppie che hanno aderito sono sufficientemente dislocate sul territorio italiano per essere rappresentative di diversi tribunali. Anche se purtroppo, è questa è una lacuna che andrà colmata con l’aiuto e la collaborazione di tutti, la zona del sud Italia è completamente scoperta.  
Quindi ci aspettiamo che altre coppie aderiscano e si rendano disponibili anche e soprattutto da Roma in giù.  
Ci aspettiamo anche che il grande movimento lgbt italiano capisca l’importanza di questa azione. Che le collaborazioni che noi abbiamo sempre cercato con le altre associazioni diano finalmente i loro frutti.  
Che, chi ha assistito incredulo a tutti questi anni di discussioni politiche in cui la non volontà di dare piena cittadinanza alle persone omosessuali ha sempre prevalso sulla realtà dei fatti, cominci a ricredersi sulla capacità politica di rappresentare i propri cittadini e che si trovi al nostro fianco per dare un vero e concreto segnale di rifiuto delle discriminazioni.  
Non possiamo che augurarci che , anche grazie alle decisioni dei giudici, questo Paese ritrovi la strada smarrita della democrazia e che finalmente l’Italia possa dirsi degna di far parte dell’Europa.  
La nostra Associazione  ha voluto anche darsi una dimensione europea iscrivendosi di recente all’ILGA , l’International Lesbian and Gay Association, e grazie al nostro responsabile delle questioni ed iniziative europee Ottavio Marzocchi, siamo sempre a conoscenza delle iniziative prese in seno al Parlamento Europeo e con lo stesso ci interfacciamo riguardo alle varie questioni.  
E sempre con lo sguardo rivolto all’Unione Europea, perché necessariamente il caso Italia prima o dopo sarà oggetto di sanzioni e provvedimenti da parte della stessa per non dare corso alle indicazioni di non discriminazione delle persone lgbt, abbiamo in cantiere un ‘iniziativa di riconoscimento in Italia dei matrimoni o delle unioni civili stipulati nei paesi comunitari.  
Sempre concretamente quindi faremo in modo che veramente l’Italia possa dirsi ospitale con i cittadini europei e possa così dare pieno sviluppo a quel concetto di libertà di spostamento e di piena cittadinanza ormai caratteristica fondante del nostro continente.  
Anche su altri piani di discussione la nostra associazione ha operato in quest’anno di vita.Ci siamo occupati infatti anche di prostituzione e persone transessuali.  
Il denominatore comune delle nostre azioni è stato sempre l’essere trasversali alle forze politiche ed alle ideologie per puntare dritti al cuore del problema e cioè il rispetto dei principi costituzionali.  
Il nostro apporto alla comunità lgbt è di schietta collaborazione con le caratteristiche proprie che ci connotano.  
Altre associazioni hanno deciso per politiche diverse. Noi riteniamo che le prime non escludano le seconde e viceversa ma anzi siamo per il rafforzamento delle battaglie.  
Quando una classe politica, supportata su questi temi dalle gerarchie vaticane , si dimostra così pervicacemente restia a riconoscere la varietà dei bisogni e dei desideri della popolazione e di conseguenza nega implicitamente il diritto ad esistere ad alcuni , la presa di posizione dev’essere forte e chiara.  
Io credo che non siano più questi i tempi delle mediazioni, fatte troppo in passato e senza risultato alcuno. Qualcosa non ha funzionato.  
Ora giriamo pagina e usiamo la forza che usano i nostri detrattori, coloro che non vogliono farci emancipare, che vogliono relegarci ad un buio silenzio, colpendoci dritti nella nostra capacità di avere cittadinanza e cioè nel nostro possedere dei diritti.  
L’omofobia forse si placherà quando due donne o due uomini potranno sposarsi.  
  
Ho volutamente lasciato in fondo un grazie particolare che rivolgo ai miei amici del direttivo che mi hanno aiutata innanzitutto a crescere umanamente e che si sono sempre dimostrati degli ottimi ed attenti interlocutori. E’ grazie a loro se il mio approcio alle nostre attività si è trasformato in una volontà costruttiva di lavorare in un team affiatato ed intelligente.