---
title: 'Riconosciuta doppia paternità da gestazione per altri. E'' la prima volta.'
date: Tue, 28 Feb 2017 14:55:08 +0000
draft: false
tags: [Gestazione per altri]
---

![1452617825_vanessa2-600x335](http://www.certidiritti.org/wp-content/uploads/2016/09/1452617825_vanessa2-600x335-300x167.jpg)"Apprendiamo dal portale Articolo29.it della storica sentenza che ha portato al riconoscimento del certificato di nascita di un bambino nato grazie alle tecniche di maternità surrogata. Come per le precedenti sentenze della Corte di Cassazione, la Corte d'Appello di Trento ha basato questa decisione sul supremo interesse del minore, concetto chiaro ed elementare che il Parlamento sembra voler continuare a ignorare seguitando a non dare risposte ai genitori dei figli-fantasma del nostro Diritto di Famiglia." Così Leonardo Monaco, segretario dell'Associazione Radicale Certi Diritti "Congratulazioni ai due papà e grazie per aver messo a disposizione della causa la vostra storia. La palla torna adesso al Parlamento: confidiamo in un sussulto delle Camere prima che sia un altro Giudice ad infliggere una nuova umiliazione al Legislatore", conclude il radicale Leonardo Monaco.