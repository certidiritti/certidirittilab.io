---
title: 'A CONGRESSO CERTI DIRITTI PREMIO A PRESIDENTE REPUBBLICA E MINISTRO ESTERI'
date: Tue, 10 Mar 2009 08:23:42 +0000
draft: false
tags: [Comunicati stampa]
---

**IL 14 MARZO A BOLOGNA, DURANTE I LAVORI DEL CONGRESSO DELL’ASSOCIAZIONE CERTI DIRITTI, CONSEGNA DEL PREMIO BOAROSA AL MINISTRO DEGLI ESTERI FRANCO FRATTINI E AL PRESIDENTE DELLA REPUBBLICA GIORGIO NAPOLITANO PER IL LORO IMPEGNO CONTRO LE DISCRIMINAZIONI.**

Il Premio BoaRosa è assegnato a chi si è distinto in modo speciale per aver sostenuto i cittadini che chiedono il riconoscimento e la tutela dei diritti delle persone che subiscono discriminazioni sociali, giuridiche e politiche a causa della loro identità di genere, del loro orientamento sessuale e del modo con cui esprimono la loro affettività. Per il 2008 è stato conferito alla nazione italiana che, attraverso il Ministro del Esteri, ha sostenuto con convinzione l'iniziativa francese presso l'ONU per la depenalizzazione universale dell'omosessualità e ha contribuito all'affermazione di principi fondamentali e irrinunciabili, promuovendo la concreta attuazione della Dichiarazione Universale dei Diritti dell'Uomo.

**Guido Allegrezza, referente del gruppo BoaRosa per la consegna del premio ha dichiarato:** "Il coraggio e la determinazione che hanno caratterizzato nelle sedi internazionali l'azione del nostro Ministro degli Esteri ci onorano come cittadini e ci rendono particolarmente orgogliosi di essere italiani". Prosegue Guido Allegrezza: "Con il deciso sostegno alla proposta francese, l'Italia ha riaffermato la sua laicità, dimostrando di essere a pieno titolo ambasciatore di diritti civili e promotrice di una civiltà plurale, aperta e capace di valorizzare la ricchezza dell'alterità".

**Sergio Rovasio, Segretario dell'Associazione Radicale Certi Diritti, ha dichiarato:**

"L'Associazione Radicale Certi Diritti è felice ed onorata di poter ospitare nel corso dei suoi lavori congressuali un momento di così alto valore civile. Riteniamo molto significativo che un gruppo di cittadini, che si impegnano personalmente per il riconoscimento e la tutela di diritti civili, abbia voluto sottolineare l'importanza sul piano politico internazionale della scelta fatta dal Governo italiano di appoggiare la proposta di depenalizzazione universale dell'omosessualità. Ciò è particolarmente rilevante soprattutto se si considerano le posizioni del Vaticano, immediatamente seguite all'annuncio della proposta da parte della Francia. Posizioni dalle quali il nostro Governo non è stato influenzato, contrariamente a quanto si potesse pensare”.

Il Gruppo BoaRosa nasce dall'aggregazione di alcuni cittadini e blogger che decidono di creare una formazione fluida e priva di un'organizzazione tradizionale, capace di costituirsi come coscienza critica all’interno del movimento lesbico, gay, bisessuale e transgender capitolino (e non solo) e di restituirgli la freschezza, la vocazione sociale, gli anticorpi e la vertigine intellettuale che un tempo gli appartenevano.

Il Premio BoaRosa è una targa smaltata e decorata a tecnica mista, pregevole opera del Maestro romano Andrea Di Stefano, specializzato nella pittura e decorazione a terzo fuoco su porcellana.