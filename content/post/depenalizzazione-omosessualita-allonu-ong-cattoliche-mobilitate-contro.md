---
title: 'DEPENALIZZAZIONE OMOSESSUALITA'' ALL''ONU. ONG CATTOLICHE MOBILITATE CONTRO'
date: Fri, 05 Dec 2008 10:15:09 +0000
draft: false
tags: [Comunicati stampa]
---

**DEPENALIZZAZIONE OMOSESSUALITA' ONU: OLTRE 50 PAESI SOSTENGONO LA PROPOSTA FRANCESE DI DEPENALIZZAZIONE DELL'OMOSESSUALITA' ALL'ONU. ONG CATTOLICHE OBBEDIENTI AL VATICANO, SCATENATE CONTRO. AL PARLAMENTO ITALIANO ED EUROPEO DEPOSITATE INTERROGAZIONI PARLAMENTARI.**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti e Ottavio Marzocchi, responsabile questioni europee di Certi Diritti:**

A oggi sono più di 50 gli Stati che appoggiano il documento francese da proporre all'Onu per chiedere la depenalizzazione dell'omosessualità, tra questi vi sono tutti i paesi dell'Unione europea.

Mercoledì 10 dicembre prossimo in Commissione Esteri il Governo italiano dovrà rispondere ad una interrogazione urgente del deputato radicale del Pd Mecacci che chiede l'impegno della diplomazia italiana verso questi paesi affinché firmino il documento francese. I deputati europei Marco Cappato, Giusto Catania e Sophie In't Veld hanno depositato una interrogazione a Consiglio e Commissione per chiedere conto al Vaticano delle sue affermazioni.

Le organizzazioni cattoliche, obbedienti ai voleri del Vaticano, tra queste 'C-fam' con l'iniziativa 'Friday fax', stanno già organizzando l'opposizione. Le diplomazie dei paesi firmatari stanno promuovendo incontri con i paesi che ancora non hanno ancora firmato il documento.

Alcuni paesi hanno detto di non voler sostenere la proposta, tra questi la Turchia e il Congo. Da alcuni paesi si è invece in attesa di una risposta, tra questi Sud Corea, Cosa Rica, Cuba, Monaco, Suriname e Canada.

Domani, sabato 6 dicembre, in Piazza Pio IX, al confine tra lo Stato italiano e quello del Vaticano, dalle ore 17, si svolgerà una fiaccolata sit-in sulla grave posizione decisa dal Vaticano che, non firmando il documento francese, di fatto, sostiene gli oltre 80 paesi del mondo che perseguitano gli omosessuali, in 9 di questi è prevista la pena di morte. Alla manifestazione, promossa dalle associazioni Certi Diritti, Arcigay e Arcilesbica, hanno aderito Radicali Italiani e, tra le altre, le associazioni lgbt, Mario Mieli, DjGayProject, GayLib, Libellula, Rosa Arcobaleno, oltre alle Associazioni Luca Coscioni e Nessuno Tocchi Caino insieme a personalità politiche, della società civile e del movimento lgbt".

Per info: [certidiritti@radicali.it](mailto:certidiritti@radicali.it)