---
title: 'Matrimonio gay in Italia, Rovasio: fiducia nella decisione della Corte Costituzionale. Vogliamo un''Italia più civile e pienamente europea'
date: Wed, 24 Mar 2010 10:54:54 +0000
draft: false
tags: [Comunicati stampa]
---

_**Dichiarazione di Sergio Rovasio, candidato lista Bonino-Pannella nel Lazio e Segretario Associazione radicale 'Certi Diritti'**_

Martedì 23 marzo la Corte Costituzionale è chiamata a decidere sui ricorsi di tre coppie gay che avevano chiesto al loro Comune di potersi sposare ed alle quali era stato opposto il diniego. Le coppie avevano partecipato alla campagna di 'Affermazione Civile' promossa due anni fa dall'Associazione Radicale Certi Diritti (www.certidiritti.it ) con il supporto di Avvocatura lgbt -- Rete Lenford. Alla campagna di Affermazione Civile hanno aderito 24 coppie gay e lesbiche, ma sono migliaia le coppie in attesa della decisione della Corte. L'Associazione Radicale Certi Diritti attende con fiducia la decisione della Corte Costituzionale . Qualunque sarà, l'Associazione Certi Diritti continuerà con forza e determinazione la sua lotta per il superamento delle disuguaglianze e contro le discriminazioni, che gran parte della classe politica italiana continua ad ignorare, con rare eccezioni. Così facendo vogliamo dare un contributo fondamentale per fare dell'Italia un paese più civile e veramente europeo.  
  
Sergio Rovasio, candidato lista Bonino-Pannella nel Lazio e segretario Associazione Radicale Certi Diritti