---
title: 'ROMA, GIOVEDI'' 24/9: FIACCOLATA CONTRO LA VIOLENZA, CON LE ISTITUZIONI'
date: Tue, 22 Sep 2009 06:19:09 +0000
draft: false
tags: [Comunicati stampa]
---

_“Roma è da sempre città di pace, solidarietà e accoglienza, capitale  del dialogo interreligioso, luogo di incontro fra i popoli e  le culture. Questa grande vocazione universale, che nasce dalla storia stessa di Roma, deve oggi trovare la forza di  confrontarsi con la concretezza delle sfide del tempo che stiamo vivendo.  
_

_Oggi più che mai, siamo chiamati a testimoniare, nella parola e nell’esempio, la possibilità di rifiutare ogni forma di intolleranza, rigettare ogni discriminazione che sottrae libertà e diritti alla persona umana, di aprirci all’altro e di costruire nuovi spazi di dialogo e di civiltà, nel rispetto della legalità e della sicurezza di tutti. C’è molto che possiamo fare insieme cominciando col sostenere l’iter parlamentare delle norme che prevedono un aggravamento delle pene per tutti i reati che hanno il loro movente nell’omofobia. Molto da dire e da affermare nel nostro impegno quotidiano. Per questo abbiamo pensato fosse giusto inviare un segnale corale  da parte di tutte le istituzioni, delle forze civiche, sociali e religiose che hanno a cuore il futuro di Roma: una fiaccolata cittadina contro  ogni forma di discriminazione, di intolleranza e di razzismo. Una manifestazione che possa rappresentare un  incontro tra tutte le  forze vive della nostra città e un punto di partenza per indicare la strada da seguire nel nostro futuro, per dare più forza e respiro a quella Roma che amiamo e in cui troviamo le radici del bene comune”._

Con questo comunicato le nostre  istituzioni locali (Regione Lazio, Provincia e Comune di Roma) promuovono congiuntamente la fiaccolata che giovedì 24 settembre, a partire dalle ore 19.00, vedrà sfilare migliaia di cittadini da Piazza SS. Apostoli, fino al Campidoglio. Alla sfilata parteciperanno, insieme ai presidenti Marrazzo e Zingaretti, al sindaco Alemanno e a molti rappresentanti della società civile, le associazioni lesbiche, gay e transessuali, chiamate a testimoniare i tremendi episodi di violenza omo e transfobica di questi giorni.

Anche l’associazione radicale Certi Diritti aderisce alla manifestazione e parteciperà con le sue bandiere alla fiaccolata, insieme a Radicali Roma e al Comitato Ernesto Nathan, pur nella convinzione che il semplice aggravamento delle pene per i reati di omo e transfobia non basti. Non può essere infatti sufficiente inasprire le condanne in un paese come questo, dove non vi è certezza della pena, dove comunque dimostrare l'aggravante omofobica sarebbe difficilissimo, anche alla luce di quanto avvenuto di recente, e dove difficilmente si arriva, in caso di querela, al “coming out” della parte lesa.

Sarebbe molto più importante, accanto alle iniziative legali, promuovere campagne serie di informazione rivolte ai cittadini, ai giovani soprattutto, fin dalle scuole di primo livello, con iniziative mirate, come si sta già facendo nel caso dello stalking.

Soltanto così potremmo educare i cittadini di oggi e di domani al vero rispetto delle differenze e delle diversità tutte, e non a quella pelosa tolleranza che spesso è in realtà vera intolleranza.

Ricordando a tutti che il negare e l’opporsi al riconoscimento di diritti civili e spazi di libertà per le persone e le coppie del mondo lesbico, gay, bisex, trans ed intersessuale È DI PER SE UN ATTO DI OMOFOBIA E TRANSFOBIA, invitiamo le istituzioni a riflettere e ad adeguarsi a quegli standard minimi di diritti civili ormai raggiunti dalla pressoché unanimità dei paesi europei.

Luca (LUCKY) Amato, _Certi Diritti Roma_

Demetrio Bacaro, _Radicali Roma_

Massimiliano Iervolino, _Comitato Ernesto Nathan_