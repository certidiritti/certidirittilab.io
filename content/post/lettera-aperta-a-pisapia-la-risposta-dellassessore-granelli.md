---
title: 'LETTERA APERTA A PISAPIA: LA RISPOSTA DELL''ASSESSORE GRANELLI'
date: Sat, 16 Jun 2012 15:20:38 +0000
draft: false
tags: [Politica]
---

Marco Granelli, Assessore alla Sicurezza e Coesione sociale del Comune di Milano, risponde alla [lettera aperta di Leonardo Monaco](http://www.certidiritti.org/2012/06/15/se-il-vigile-urbano-ti-chiama-ricchione/). Presto un incontro tra i due.

Egr. Sig. Leonardo Monaco  
ho letto oggi la sua lettera inviata al Sindaco di Milano Giuliano Pisapia e ai suoi assessori, dove descrive un episodio che le è accaduto giovedì scorso al rientro del concerto di Madonna. Quanto da lei raccontato è deplorevole e da condannare con fermezza. Per questo le esprimo il mio forte dispiacere e la mia solidarietà personale e come assessore alla Sicurezza e Coesione sociale, Polizia Locale, Protezione Civile e Volontariato del Comune di Milano. E' mio desiderio incontrarla di persona per esprimerle i miei sentimenti e per poter apprendere, se lei è d'accordo, maggiori particolari su quanto accaduto affinchè io possa verificare l'accaduto e fare quanto nella mia responsabilità per isolare e condannare l'episodio deplorevole che oltre ad offendere la sua persona offende i Vigili di Milano che sicuramente non condividono e condannano l'episodio da lei narrato.  
La ringrazio per avermi informato, mi scuso ancora e mi auguro di poterla incontrare.  
Marco Granelli