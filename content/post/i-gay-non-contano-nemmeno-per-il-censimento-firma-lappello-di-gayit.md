---
title: 'I GAY NON CONTANO NEMMENO PER IL CENSIMENTO... FIRMA L''APPELLO DI GAY.IT'
date: Wed, 03 Feb 2010 09:44:50 +0000
draft: false
tags: [Comunicati stampa]
---

Di seguito pubblichiamo l'intervento di Carlo D'Ippoliti sul rischio di esclusione delle coppie gay dal prossimo censimento italiano. Il portale gay.it e gay.tv hanno lanciato un appello, già firmato da migliaia di cittadini, che vi invitiamo a sottoscrivere....

I GAY CHE NON CONTANO di [Carlo D'Ippoliti](http://www.lavoce.info/lavocepuntoinfo/autori/pagina804.html) 26.01.2010 da www.lavoce.info

[http://www.lavoce.info/articoli/pagina1001518.html](http://www.lavoce.info/articoli/pagina1001518.html)

### Per firmare l'appello...

[http://www.gay.it/contaci/](http://www.gay.it/contaci/)