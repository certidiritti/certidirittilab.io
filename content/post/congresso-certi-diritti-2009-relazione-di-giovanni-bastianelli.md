---
title: 'CONGRESSO CERTI DIRITTI 2009: RELAZIONE DI GIOVANNI BASTIANELLI'
date: Tue, 24 Mar 2009 07:59:35 +0000
draft: false
tags: [Senza categoria]
---

### ![Giovanni Bastianelli](components/com_joomgallery/img_thumbnails/congresso_annuale_dellassociazione_radicale_certi_diritti_bologna_2009_1/congresso_2009_68_20090318_1817887962.jpg)Gruppi Studenteschi Certi Diritti

  
Come è stato più volte rilevato in occasione degli incontri del Direttivo e nel corso di conversazioni inerenti questo tema, un elemento di notevole criticità che si dovrà affrontare a partire dal 2009 è quello delle iniziative per l'incremento del numero degli iscritti all'associazione, non già con l'intento di rimpinguare le casse della tesoreria o di perpetuare una logica di "tesseramento", quanto piuttosto con il proposito di dare massima diffusione alle nostre iniziative, crearne e svilupparne altre, ma soprattutto far convergere l'attenzione di molti sulle tematiche che a noi stanno a cuore.  
  
Con queste brevi premesse, si è ritenuto che potesse rispondere a questa linea di azione una specifica attività rivolta al mondo delle università, nelle quali è in corso una sorta di "rinascita" delle coscienze ed un ritorno all'impegno politico e sociale, come testimoniano i recenti fenomeni di contestazione legati alle riforme e ai tagli promossi dall'attuale governo. Al di là di questa probabile ricettività, si ritiene che questo tipo di linea di azione abbia una notevole importanza di carattere strategico, essendo gli studenti universitari di oggi i professionisti di domani; si tratta di una sorta di "semina" della società del futuro, con l'idea che chi si è formato una cultura ed un'esperienza in tema di diritti delle persone glbte potrà in futuro non solo, nel migliore dei casi, essere un agente attivo di informazione e di influenza della società, ma anche un agente di costruzione del consenso con il solo atteggiamento positivo verso il cambiamento.  

### Concept

L'idea di fondo è semplice: costituire gruppi di studenti che portano le iniziative e il dibattito attorno ai temi cari a Certi Diritti all'interno delle facoltà universitarie. I gruppi, una volta costituiti ed operanti, agiranno promuovendo ed organizzando, anche in collaborazione con altre realtà universitarie - comprese le istituzioni accademiche, incontri culturali, seminari, dibattiti coinvolgendo sia gli universitari, sia i soci dell'associazione.  
Di preferenza, i dibattiti e gli incontri si svolgeranno con docenti interni delle università ed esterni nonché da personalità legate alle tematiche trattate da Certi Diritti.  
Parallelamente, i gruppi studenteschi si occuperanno di fornire ascolto individuale e serviranno da punto di riferimento agli studenti che hanno una qualche forma di disagio o che cercano un ascolto per risolvere i loro conflitti con le famiglie in caso di comming-out non accettato dai genitori. I gruppi studenteschi di Certi Diritti si porrà quindi anche come punto di riferimento per la tutela e la segnalazione all'università di eventuali comportamenti aggressivi o discriminatori affinché nasca una forma di dialogo costruttivo fra le istituzioni accademiche e gli studenti.  

### Costruire i gruppi studenteschi

La costruzione dei gruppi studenteschi di Certi Diritti, dovrebbe avvenire per "aggregazione spontanea". Sarà però necessario almeno per il primo periodo di attività, effettuare iniziative di promozione presso le facoltà, attraverso soci che appartengano al mondo dell'università, sia docenti, sia studenti.  
Si ritine pertanto di chiedere al Direttivo di attivare una comunicazione diretta a tutti gli iscritti, chiedendo di raccogliere iscrizioni e partecipazioni all'interno delle università, e di spingere poi per la costruzione dei gruppi.  
Ulteriori attività promozionali orientate sulla stessa linea si potranno poi avviare anche in funzione di un censimento degli iscritti e la valutazione delle opportunità che possono nascere rispetto alle loro caratteristiche, eventualmente anche con l'appoggio di altre organizzazioni studentesche o glitto.

### Ampliamento telematico

Per dare anche maggiore risalto ai gruppi studenteschi, ma anche agli iscritti all'associazione Certi Diritti, verrà creata una community nel sito di Certi Diritti, dove ogni utente avrà a disposizione una pagina personale dove potrà inserire le proprie iniziative inerenti l'associazione e che potrà servire quindi da archivio storico. Questa novità è pensata per fare in modo che anche chi non può essere presente fisicamente alle nostre iniziative possa avere una voce in capitolo e possa anche dare luogo attraverso l'uso della rete a delle ulteriori iniziative e lasciarne memoria nella sua pagina personale del sito.  

### Un primo obiettivo per il 2009

Avviando in coincidenza con il Congresso l'attività su questo progetto si può fissare l'obiettivo di massima di creare almeno 2 gruppi entro la fine del 2009 (è plausibile che essi possano nascere a Roma, Milano, Torino, Parma, Lecce, Catania o Trieste, città nelle quali più forte è la presenza dell'Associazione) e che essi possano organizzare almeno 2 iniziative di dibattito o di confronto all'interno delle sedi universitarie.