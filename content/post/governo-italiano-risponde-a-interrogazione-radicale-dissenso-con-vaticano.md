---
title: 'GOVERNO ITALIANO RISPONDE A INTERROGAZIONE RADICALE, DISSENSO CON VATICANO'
date: Wed, 10 Dec 2008 14:02:02 +0000
draft: false
tags: [Comunicati stampa]
---

**DEPENALIZZAZIONE OMOSESSUALITA’ ALL’ONU: BENE IL GOVERNO ITALIANO, URGONO PERO’ AZIONI CONCRETE E URGENTI. LA POSIZIONE ITALIANA IN NETTO DISSENSO DAL VATICANO.**

**Dichiarazione di Matteo Mecacci, deputato radicale del Pd e Sergio Rovasio, Segretario Associazione Radicale Certi Diritti.**

 

Il Governo italiano ha oggi risposto all’ interrogazione parlamentare del deputato radicale del Pd Matteo Mecacci, nella quale si chiedeva quali interventi ha messo in campo l’Italia per sostenere la proposta francese e dell’Ue di depenalizzazione dell’omosessualità all’Onu.

Il Governo ha dato una risposta positiva, in totale dissenso da quanto espresso dal Vaticano sull’argomento nei giorni scorsi. Il nostro Ministero degli Esteri ha ufficialmente dichiarato che i paesi europei firmatari della proposta francese “intendono sensibilizzare la comunità internazionale sulle violenze subite dalle persone omosessuali, bisessuali e transessuali” e che “in questo spirito la Presidenza francese chiederà a nome della UE, e degli altri Stati che aderiranno all’iniziativa, la depenalizzazione dell’orientamento sessuale. Considerare l’omosessualità alla stregua di un reato – in alcuni casi addirittura punibile con la pena di morte – rappresenta, infatti, una chiara violazione dei diritti fondamentali così come riconosciuti dalle convenzioni delle Nazioni Unite, a cominciare dall’art. 26 del Patto Internazionale relativo ai diritti civili e politici”. Il Ministro degli Esteri, nella risposta dichiara altresì: “L’italia, in linea con il proprio tradizionale impegno a favore dei diritti umani e contro la pena di morte, si è unita agli altri Paesi europei per sostenere e promuovere questa iniziativa sin dal suo nascere. Nello stesso spirito, continueremo ad appoggiare gli sforzi dei Paesi dell’alleanza transregionale per raccogliere il maggior numero di adesioni attorno alla Dichiarazione e, più in generale, a sostenre l’UE nel suo impegno contro ogni forma di discriminazione”.

**In merito alle dichiarazioni ufficiali del Governo italiano, il deputato radicale del Pd Matteo Mecacci e Sergio Rovasio, Segretario dell’Assoocazione Radicale Certi Diritti hanno dichiarato:**

“Il Governo italiano, sulla proposta francese e dell’Ue di depenalizzazione dell’omosessualità all’Onu (che non mira a stravolgere il diritto di famiglia nei singoli Stati), ha preso una posizione chiara e netta, seppur tardiva rispetto alle gravissime dichiarazioni di Monsignor Celestino Migliore, rappresentante del Vaticano all’Onu, che sono esattamente all’opposto di quelle del Governo italiano.

Non possiamo che rallegrarcene. Ci auguriamo però che l’Italia si attivi in queste ore in tutte le sedi diplomatiche, con l’urgenza necessaria, affinché aumentino il numero di Paesi co-firmatari della proposta francese prima del deposito della dichiarazione. Occorre anche che il Governo italiano, proprio per quanto ribadito nella risposta all’interrogazione, sul rispetto dei diritti umani e riguardo la lotta alla pena di morte nel mondo, si attivi con fatti concreti verso quei paesi che per legge perseguitano, anche con la pena di morte, le persone omosessuali”.

Il testo integrale dell'interrogazione e della risposta del Governo italiano:

_Ministero degli Affari Esteri_

 Gabinetto del Ministro

**Interrogazione a risposta in Commissione 5 - 00671 dell'On. Mecacci (PD), sulle iniziative relative alla depenalizzazione universale dell'omosessualità.**

• La tematica dell'orientamento sessuale **viene affrontata in numerosi strumenti e dichiarazioni dell'Unione Europea.**

La **Carta** **dei diritti fondamentali dell'Unione Europea e lo stesso Trattato di Lisbona** impegnano la Comunità a combattere le discriminazioni fondate sul sesso, la razza o l'origine etnica, la religione o le convinzioni personali, la disabilità, l'età o l'orientamento sessuale.

• Il tema della non discriminazione ricorre inoltre in molte dichiarazioni e prese di posizione politiche da parte delle istituzione europee. Basti pensare alla risoluzione adottata nel 2007 dal Consiglio **sui seguiti dell’ “Anno europeo delle pari opportunità per tutti" in cui si ribadisce l'impegno degli Stati membri nel contrasto di tutte le forme di discriminazione,** inclusa quella basata sull' orientamento sessuale.

• Coerentemente con questi assunti, **la UE ha deciso di promuovere, assieme ad un gruppo di Paesi provenienti da tutti i continenti, un 'iniziativa per chiedere la depenalizzazione universale dell'orientamento sessuale,** nell'anno in cui ricorre il 60mo anniversario della Dichiarazione Universale dei Diritti dell'Uomo.

• **L'iniziativa consisterà in una dichiarazione che sarà pronunciata dalla Presidenza francese nelle prossime settimane alla 63ma sessione dell'Assemblea Generale.**

• Tale dichiarazione s'iscrive sul solco di precedenti iniziative sulla medesima questione, come **la dichiarazione presentata nel 2006 dalla Norvegia al Consiglio dei Diritti Umani delle Nazioni Unite, firmata da 54 Stati, tra i quali l'Italia e gli altri Paesi dell'UE.**

• E soprattutto, **la Dichiarazione** **si innesta nella strategia ad ampio respiro che l'Unione Europea porta avanti per promuovere la tutela dei diritti Umani nel mondo.**

• Con questa iniziativa trans-nazionale, la UE e gli altri Paesi "like minded" non intendono **promuovere l'adozione di nuove norme internazionali né, tantomeno, entrare nel merito di tematiche strettamente nazionali come il diritto di famiglia e la definizione dell'istituto matrimoniale.** Essi intendono, piuttosto, **sensibilizzare la Comunità internazionale sulle violenze subite dalle persone omosessuali, bisessuali e transessuali,**

• In questo spirito la Presidenza francese chiederà a nome della UE e degli altri Stati che aderiranno alla iniziativa **la depenalizzazione dell'orientamento sessuale. Considerare l'omosessualità alla stregua di un reato - in alcuni casi addirittura punibile con la pena di morte - rappresenta, infatti, una chiara violazione dei diritti fondamentali così come riconosciuti dalle convenzioni delle Nazioni Unite, a cominciare** dall'articolo 26 del **Patto Internazionale relativo ai diritti civili e politici.** E' questa la conclusione cui sono giunti non solo gli Stati membri della UE ma anche gli organi delle Nazioni Unite incaricati di vigilare l'attuazione dei trattati, come il Comitato sui diritti economici, sociali e culturali e il Comitato sui Diritti del Fanciullo.

• **L'Italia, in linea con il proprio tradizionale impegno a favore dei diritti umani e contro la pena di morte, si è unita agli altri Paesi europei per sostenere e promuovere questa iniziativa sin dal suo nascere.** Nello stesso spirito, continueremo ad appoggiare gli sforzi dei Paesi dell'alleanza transregionale per raccogliere il maggior numero di adesioni attorno alla Dichiarazione e, più in generale, a sostenere l'UE nel suo impegno contro ogni forma di discriminazione.

_Quello che segue è il testo dell’Interrogazione dell’on. Matteo Mecacci:_

**Interrogazione a risposta orale in Commissione al Ministro degli Esteri**

\- In 80 paesi del mondo l'omosessualità è considerata un reato; in 9 di questi è prevista la pena di morte, mentre in numerosi altri Stati le persone omosessuali sono oggetto di torture, persecuzioni, violenze e discriminazioni in ragione del loro orientamento sessuale;

\- la Francia, per iniziativa della Segretaria di Stato per gli affari esteri ed i diritti umani Rama Yade, ha lanciato una iniziativa diplomatica internazionale al fine di presentare all'Assemblea generale delle Nazioni Unite, il prossimo dicembre a New York, una dichiarazione sulla depenalizzazione universale dell'omosessualità;

\- tale iniziativa ha raccolto ad oggi il sostegno di più di 50 Stati - inclusi tutti gli Stati dell'Unione europea - di tutti i continenti;

\- la persecuzione degli omosessuali è in palese contrasto con i principi stabiliti dalla dichiarazione universale dei Diritti dell'Uomo, dalla Convenzione europea sui diritti umani e le libertà fondamentali, dalla Carta dei diritti fondamentali dell'Unione Europea ed è contro la cultura giuridica e scientifica contemporanea;

\- il Parlamento europeo e l'Assemblea Generale degli Stati americani hanno adottato risoluzioni chiedendo iniziative internazionali, anche a livello ONU, per assicurare il rispetto dei diritti umani delle persone omosessuali e la fine delle esecuzioni, persecuzioni, violenze e discriminazioni basate sull'orientamento sessuale;

\- sono sempre più numerosi i casi di persone extracomunitarie omosessuali, provenienti da paesi dove l'omosessualità è considerata reato, che chiedono lo status di rifugiato politico all'Italia ed all'Unione europea;

\- l'Organizzazione Mondiale della Sanità definisce l'omosessualità una variante naturale del comportamento umano e l'ha depennata sin dal 1990 dall'elenco delle malattie mentali;

Per sapere:

\- quale supporto politico e diplomatico sta dando l'Italia all'iniziativa del Governo francese riguardo la richiesta di depenalizzazione universale dell'omosessualità da presentare all'Onu e, in particolare, al fine di raccogliere l'adesione di altri Stati con i quali l'Italia ha relazioni diplomatiche intense, quali la Turchia, l'Albania, San Marino, la Georgia, gli Stati del Mediterraneo, o gli Stati Uniti, il Canada, l'Australia, l'Africa del Sud;

\- quanti sono i casi di riconoscimento dello status di rifugiato politico che l'Italia ha concesso negli ultimi cinque anni alle persone fuggite dal loro paese di origine a causa delle persecuzioni di cui sono state vittime per il proprio orientamento sessuale;

\- se non ritenga urgente adoperarsi affinché in campo internazionale, con l'attivazione delle nostre sedi diplomatiche, vi siano interventi nei confronti di quei paesi che considerano l'omosessualità un reato e in particolare nei confronti di quei paesi che prevedono per gli omosessuali la pena di morte.

On. Matteo Mecacci