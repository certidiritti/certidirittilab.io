---
title: 'LA REGIONE LAZIO CONSIDERA I SITI CON LA PAROLA GAY SITI PORNOGRAFICI'
date: Wed, 17 Nov 2010 11:09:27 +0000
draft: false
tags: [Comunicati stampa]
---

**LA REGIONE LAZIO CONSIDERA I SITI CON LA PAROLA GAY SITI PORNOGRAFICI. GRAVISSIMA AZIONE CENSORIA CHE DISCRIMINA I SITI DI INFORMAZIONE GAY. ALLA PISANA E’ ARRIVATO IL ‘GOEBBELS DE’ NOANTRI”?**

_Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti e Capo Segreteria Lista Bonino Pannella, Federalisti Europei alla Regione Lazio:_

“Quando dai pc della Regione Lazio si naviga su internet, e si richiedono indirizzi che hanno la parola “gay”, scatta un’azione censoria che impedisce di aprire la pagina ed esce  la frase: “ Accesso non consentito. Motivazione: Le attuali policy aziendali non consentono l'accesso al sito richiesto. La pagina che si sta tentando di visualizzare è categorizzata come "Pornography".

Dopo aver chiuso (e riaperto) nelle ultime 48 ore, l’accesso a decine di siti di informazione e social-network, ora è la volta dei siti che danno informazioni sulla comunità lgbt italiana.

L’esempio più eclatante è dato dalla richiesta di accesso a siti di informazione che pubblicano spesso anche iniziative dei consiglieri regionali radicali del Lazio, ad esempio sulle unioni civili e per la lotta alle discriminazioni. Tra questi vi sono i siti internet [www.gay.it](http://www.gay.it/) oppure  [www.gaynews.it](http://www.gaynews.it/) e ancora [www.gay.tv](http://www.gay.tv/).

Occorrerebbe capire per quale motivo è stata attivata questa procedura che ha alcuni aspetti gravi: da una parte si considera la stessa parola gay come una parola pornografica e dall’altra si decide in modo autoritario, alla “Goebbels de noantri” di limitare l’accesso perché magari il contenuto non piace a chi sta nei piani alti.

Ci auguriamo che tutto ciò sia dovuto ad un errore e non ad una politica censoria, sperando che venga al più presto ristabilito il libero accesso a tutti i siti senza alcun tipo di discriminazione.