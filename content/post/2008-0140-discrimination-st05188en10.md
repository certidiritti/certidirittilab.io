---
title: '2008-0140-Discrimination-ST05188.EN10'
date: Wed, 13 Jan 2010 12:11:58 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 13 January 2010

Interinstitutional File:

2008/0140 (CNS)

5188/10

LIMITE

SOC 13

JAI 16

MI 8

  

  

  

  

  

NOTE

from :

The Presidency

to :

The Working Party on Social Questions

on :

22 January 2010

No. prev. doc. :

16063/09 SOC 706 JAI 836 MI 434

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached a set of Presidency drafting suggestions concerning Article 1-3 of the above proposal, as well as the Recitals.

Changes in relation to the previous version (doc. 16063/09 ADD 1) are indicated as follows: new text is in **bold**, and deletions are marked "**\[…\]"**.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

Proposal for a

COUNCIL DIRECTIVE

on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

THE COUNCIL OF THE EUROPEAN UNION,

Having regard to the Treaty **on the Functioning of** the European **Union**, and in particular Article **19**(1) thereof,

Having regard to the proposal from the Commission[\[1\]](#_ftn1),

Having regard to the **consent** of the European Parliament[\[2\]](#_ftn2),

Whereas:

(1)          In accordance with Article **2** of the Treaty on European Union, the European Union is founded on the **values of respect for human dignity, freedom, democracy, equality, the rule of law and respect for human rights, including the rights of persons belonging to minorities, values** which are common to all **the** Member States. **In accordance with Article 6 of the Treaty on European Union, the European Union recognises the rights, freedoms and principles set out in the Charter of Fundamental Rights of the European Union, and fundamental rights, as guaranteed by the European Convention on the Protection of Human Rights and Fundamental Freedoms and as they result from the constitutional traditions common to the Member States, shall constitute general principles of the Union's law.**

  

(2)          The right to equality before the law and protection against discrimination for all persons constitutes a universal right recognised by the Universal Declaration of Human Rights, the United Nations Convention on the Elimination of all forms of Discrimination Against Women, the International Convention on the Elimination of all forms of Racial Discrimination, the United Nations Covenants on Civil and Political Rights and on Economic, Social and Cultural Rights, the UN Convention on the Rights of Persons with Disabilities, the European Convention for the Protection of Human Rights and Fundamental Freedoms and the European Social Charter, to which \[all\] Member States are signatories. In particular, the UN Convention on the Rights of Persons with Disabilities includes the denial of reasonable accommodation in its definition of discrimination.

(3)          This Directive respects the fundamental rights and observes the fundamental principles recognised in particular by the Charter of Fundamental Rights of the European Union. Article 10 of the Charter recognises the right to freedom of thought, conscience and religion; Article 21 prohibits discrimination, including on grounds of religion or belief, disability, age or sexual orientation; and Article 26 acknowledges the right of persons with disabilities to benefit from measures designed to ensure their independence.

(4)          The European Years of Persons with Disabilities in 2003, of Equal Opportunities for All in 2007, and of Intercultural Dialogue in 2008 have highlighted the persistence of discrimination but also the benefits of diversity.

(5)          The European Council, in Brussels on 14 December 2007, invited Member States to strengthen efforts to prevent and combat discrimination inside and outside the labour market[\[3\]](#_ftn3).

(6)          The European Parliament has called for the extension of the protection of discrimination in European Union law[\[4\]](#_ftn4).

  

(7)          The European Commission has affirmed in its Communication ‘Renewed social agenda: Opportunities, access and solidarity in 21st century Europe’[\[5\]](#_ftn5) that, in societies where each individual is regarded as being of equal worth, no artificial barriers or discrimination of any kind should hold people back in exploiting these opportunities. Discrimination based on religion or belief, disability, age or sexual orientation may undermine the achievement of the objectives of the EC Treaty, in particular the attainment of a high level of employment and of social protection, the raising of the standard of living, and quality of life, economic and social cohesion and solidarity. It may also undermine the objective of abolishing of obstacles to the free movement of persons, goods and services between Member States.

(8)          **Existing European Union legislation includes** three legal instruments[\[6\]](#_ftn6) **adopted** on the basis of Article 13(1) of the EC Treaty**, which aim** to prevent and combat discrimination on grounds of sex, racial and ethnic origin, religion or belief, disability, age and sexual orientation. These instruments have demonstrated the value of legislation in the fight against discrimination_._ In particular, Directive 2000/78/EC establishes a general framework for equal treatment in employment and occupation on the grounds of religion or belief, disability, age and sexual orientation. However, variations remain between Member States on the degree and the form of protection from discrimination on these grounds beyond the areas of employment.

(9)     Therefore, legislation should prohibit discrimination based on religion or belief, disability, age or sexual orientation in a range of areas outside the labour market, including social protection, education and access to and supply of goods and services, including housing. Services should be taken to be those within the meaning of Article **57** of the Treaty **on the Functioning of the European Union**.

(10)   Directive 2000/78/EC prohibits discrimination in access to vocational training; it is necessary to complete this protection by extending the prohibition of discrimination to education which is not considered vocational training.

(11)   Deleted.

  

(12)   Discrimination is understood to include direct and indirect discrimination, harassment, instructions to discriminate and denial of reasonable accommodation **to people with disabilities. Moreover, direct discrimination includes discrimination** **based on assumptions about a person's religion or belief, disability, age or sexual orientation.**

(12a) (new) In accordance with the judgment of the Court of Justice in Case C-303/06[\[7\]](#_ftn7), it is appropriate to provide explicitly for protection from discrimination by association on all grounds covered by this Directive. Such discrimination occurs, _inter alia_, when a person is treated less favourably, or harassed, because, in the view of the discriminator, he or she is associated with persons of a particular religion or belief, disability, age or sexual orientation, for instance through his or her family, friendships, employment or occupation. **\[…\]**

(12b) (new) Harassment is contrary to the principle of equal treatment, since victims of harassment cannot enjoy access to social protection, education and goods and services on an equal basis with others. Harassment can take different forms, including unwanted verbal, physical, or other non-verbal conduct. Such conduct may be deemed harassment in the meaning of this Directive when it is either repeated or otherwise so serious in nature that it has the purpose or effect of violating the dignity of a person and of creating an intimidating, hostile, degrading, humiliating or offensive environment. In this context, the mere expression of a personal opinion or the display of religious symbols or messages are presumed as not constituting harassment[\[8\]](#_ftn8).

(**12c) (new) When defining the concept of harassment, the Member States may establish the necessary measures to** **guarantee protection of the right to freedom of religion, or to freedom of expression. Such measures may include legislative provisions limiting the protection against harassment where this is necessary for ensuring the respect of the fundamental rights and freedoms of** **others.**

  

(13)   In implementing the principle of equal treatment irrespective of religion or belief, disability, age or sexual orientation, the **European Union** should, in accordance with Article **8** of the **Treaty on the Functioning of the European Union**, aim to eliminate inequalities, and to promote equality between men and women, especially since women are often the victims of multiple discrimination.

(14)   The appreciation of the facts from which it may be presumed that there has been direct or indirect discrimination should remain a matter for the national judicial or other competent bodies in accordance with rules of national law or practice. Such rules may provide, in particular, for indirect discrimination to be established by any means including on the basis of statistical evidence.

(14a) Differences in treatment in connection with age may be permitted under certain circumstances if they are objectively justified by a legitimate aim and the means of achieving that aim are appropriate and necessary. **\[…\]**

**(14b) (new) People under the age of 18 must be protected against discrimination on the grounds of age. This protection is without prejudice to differences of treatment provided under national rules aiming to protect the best interests of the child, especially rules concerning the organisation of education at all levels, and access to certain goods and services.**

(15)   Actuarial and risk factors related to disability and to age are used in the provision of insurance, banking and other financial services. These should not be regarded as constituting discrimination where service providers have shown, by relevant actuarial principles, accurate statistical data or medical knowledge, that  such factors are determining factors for the assessment of risk.

(16)     Deleted.

(17)   While prohibiting discrimination, it is important to respect other fundamental rights and freedoms, including the protection of private and family life and transactions carried out in that context, the freedom of religion, the freedom of association, the freedom of expression and the freedom of the press.

  

(17a) **(****new) This Directive covers the application of the principle of equal treatment in the fields of social protection, education and access to goods and services within the limits of the competences of the European Union.**

**The Member States are responsible for the organisation and content of systems of social protection, health care and education, as well as for the definition of who is entitled to receive social protection benefits, medical treatment and education.**

(17b) (new)  This Directive covers access to social protection, which includes social security, social assistance, and health care, thereby providing comprehensive protection against discrimination in this field. Consequently, the Directive applies with regard to access to rights and benefits which are derived from general or special social security,  social assistance and healthcare schemes, which are provided either directly by the State, or by private parties in so far as the provision of those benefits by the latter is funded by the State. In this context, the Directive applies with regard to benefits  in cash, benefits in kind and services, irrespective of whether the schemes involved are contributory or non-contributory. The abovementioned schemes include, for example, access to the branches of social security defined by Regulation 883/2004/EC on the coordination of social security systems[\[9\]](#_ftn9), as well as schemes providing for benefits or services granted for reasons related to the lack of financial resources or risk of social exclusion.

(17c) Deleted.

(17d) (new) All individuals enjoy the freedom to contract, including the freedom to choose a contractual partner for a transaction. This Directive should not apply to economic transactions undertaken by individuals for whom these transactions do not constitute a professional or commercial activity.

**In particular, this Directive does not apply to transactions related to housing which are performed by natural persons, when the activities in question do not constitute a professional or commercial activity.**

  

In this context, the concept of professional or commercial activity may be defined in accordance with the national laws and practice of the Member States.

(17e) (new) This Directive does not alter the division of competences between the European **Union** and the Member States in the areas of education and social protection, including social security, social assistance and health care. It is also without prejudice to the essential role and wide discretion of the Member States in providing, commissioning and organising services of general economic interest.

(17f) (new) The exclusive competence of Member States with regard to the organisation of their social protection systems includes decisions on the setting up, financing and management of such systems and related institutions as well as on the substance and delivery of benefits and health services and the conditions of eligibility. In particular Member States retain the possibility to reserve certain benefits or services to certain age groups or persons with disabilities. Moreover, this Directive is without prejudice to the powers of the Member States to organise their social protection systems in such a way as to guarantee their sustainability.

(17g) (new) The exclusive competence of Member States with regard to **\[…\] the organisation of their educational systems and the content of teaching and of educational activities**, including the provision of special needs education,  includes the setting up and management of educational institutions, the development of curricula and other educational activities and the definition of examination processes. In particular Member States retain the possibility to set age limits for certain education activities. However, there may be no discrimination in the access to educational activities, including the admission to and participation in classes or programmes and the evaluation of students' performance.

(17h) (new) This Directive does not apply to matters covered by family law including marital status and adoption, and laws on reproductive rights. It is also without prejudice to the secular nature of the State, state institutions or bodies, or education.

(18)     Deleted.

  

(19a) Persons with disabilities include those who have long‑term physical, mental, intellectual or sensory impairments which, in interaction with various barriers, may hinder their full and effective participation in society on an equal basis with others.

(19b) (new) Measures to ensure accessibility for persons with disabilities on an equal basis with others, to the areas covered by this Directive play an important part in ensuring full equality in practice. Such measures should include the identification and elimination of obstacles and barriers to accessibility. They should not impose a disproportionate burden.

(19c) (new)

(19d) (new) Improvement of accessibility can be provided by a variety of means, including application of the "universal design" principle. “Universal design” means the design of products, environments, programmes and services to be usable by all people, to the greatest possible extent, without the need for adaptation or specialised design. “Universal design” should not exclude assistive devices for particular groups of persons with disabilities where this is needed.

(20)   Legal requirements[\[10\]](#_ftn10) and standards on accessibility have been established at European level in some areas while Article 16 of Council Regulation 1083/2006 of 11 July 2006 on the European Regional Development Fund, the European Social Fund and the Cohesion Fund and repealing Regulation (EC) No 1260/1999[\[11\]](#_ftn11) requires that accessibility for disabled persons is one of the criteria to be observed in defining operations co-financed by the Funds. The Council has also emphasised the need for measures to secure the accessibility of cultural infrastructure and cultural activities for people with disabilities[\[12\]](#_ftn12).

  

(20a) (new) In addition to general measures to ensure accessibility, individual measures to provide reasonable accommodation play an important part in ensuring full equality in practice for persons with disabilities to the areas covered by this Directive. Reasonable accommodation means necessary and appropriate modifications and adjustments not imposing a disproportionate burden, where needed in a particular case, in order to ensure to persons with disabilities access on an equal basis with others.

(20b)  (new) In assessing whether measures to ensure accessibility or reasonable accommodation would impose a disproportionate burden, account should be taken of a number of factors including the size, resources and nature of the organisation or enterprise, as well as the costs and possible benefits of such measures. A disproportionate burden would arise, for example, where significant structural changes would be required in order to provide access to movable or immovable property which is protected under national rules on account of their historical, cultural, artistic or architectural value.

(20c) (new) The principle of accessibility is established in the United Nations Convention on the Rights of Persons with Disabilities. The principles of reasonable accommodation and disproportionate burden are established in Directive 2000/78/EC[\[13\]](#_ftn13) and the United Nations Convention on the Rights of Persons with Disabilities.

(21)   The prohibition of discrimination should be without prejudice to the maintenance or adoption by Member States of measures intended to prevent or compensate for disadvantages suffered by a group of persons of a particular religion or belief, disability, age or sexual orientation. Such measures may permit organisations of persons of a particular religion or belief, disability, age or sexual orientation where their main object is the promotion of the special needs of those persons.

  

(22)   This Directive lays down minimum requirements, thus giving the Member States the option of introducing or maintaining more favourable provisions. The implementation of this Directive should not serve to justify any regression in relation to the situation which already prevails in each Member State.

(23)   Persons who have been subject to discrimination based on religion or belief, disability, age or sexual orientation should have adequate means of legal protection. To provide a more effective level of protection, associations, organisations and other legal entities should be empowered to engage in proceedings, including on behalf of or in support of any victim, without prejudice to national rules of procedure concerning representation and defence before the courts.

(24)   The rules on the burden of proof must be adapted when there is a prima facie case of discrimination and, for the principle of equal treatment to be applied effectively, the burden of proof must shift back to the respondent when evidence of such discrimination is brought. However, it is not for the respondent to prove that the plaintiff adheres to a particular religion or belief, has a particular disability, is of a particular age or has a particular sexual orientation.

(25)   The effective implementation of the principle of equal treatment requires adequate judicial protection against victimisation.

(26)   In its resolution on the Follow-up of the European Year of Equal Opportunities for All (2007), the Council called for the full association of civil society, including organisations representing people at risk of discrimination, the social partners and stakeholders in the design of policies and programmes aimed at preventing discrimination and promoting equality and equal opportunities, both at European and national levels.

(27)   Experience in applying Directives 2000/43/EC[\[14\]](#_ftn14) and 2004/113/EC[\[15\]](#_ftn15) show that protection from discrimination on the grounds covered by this Directive would be strengthened by the existence of a body or bodies in each Member State, with competence to analyse the problems involved, to study possible solutions and to provide concrete assistance for the victims.

(28)   Deleted.

(29)   Member States should provide for effective, proportionate and dissuasive sanctions in case of breaches of the obligations under this Directive.

(30)   In accordance with the principles of subsidiarity and proportionality as set out in Article 5 of the Treaty **on the Functioning of the European Union**, the objective of this Directive, namely ensuring a common level of protection against discrimination in all the Member States, cannot be sufficiently achieved by the Member States and can therefore, by reason of the scale and impact of the proposed action, be better achieved by the **European Union**. This Directive does not go beyond what is necessary in order to achieve those objectives.

(31)   In accordance with paragraph 34 of the interinstitutional agreement on better law-making, Member States are encouraged to draw up, for themselves and in the interest of the **European Union**, their own tables, which will, as far as possible, illustrate the correlation between the Directive and the transposition measures and to make them public.

Article 1  
Purpose

This Directive lays down a framework for combating discrimination on the grounds of religion or belief, disability, age, or sexual orientation, with a view to putting into effect in the Member States the principle of equal treatment within the scope of Article 3.

Article 2  
Concept of discrimination

1.       For the purposes of this Directive, the “principle of equal treatment” shall mean that  there shall be no direct or indirect discrimination**, \[...\]** on any of the grounds referred to in Article 1.

  

2.       For the purposes of paragraph 1, the following definitions apply:

a)       direct discrimination shall be taken to occur where one person is treated less favourably than another is, has been or would be treated in a comparable situation, on any of the grounds referred to in Article 1;

(b)     indirect discrimination shall be taken to occur where an apparently neutral provision, criterion or practice would put persons of a particular religion or belief, a particular disability, a particular age, or a particular sexual orientation at a particular disadvantage compared with other persons, unless that provision, criterion or practice is objectively justified by a legitimate aim and the means of achieving that aim are appropriate and necessary;

(c)  Denial of reasonable accommodation in a particular case, as provided for by Article 4a of the present Directive as regards persons with disabilities, shall be taken to be discrimination,

3\.       Harassment shall be deemed to be a form of discrimination within the meaning of paragraph 1, when unwanted conduct related to any of the grounds referred to in Article 1 takes place with the purpose or effect of violating the dignity of a person and of creating an intimidating, hostile, degrading, humiliating or offensive environment. In this context, the concept of harassment may be defined in accordance with the national laws and practice of the Member States.

3a.     Discrimination includes direct discrimination or harassment due to a person's association with persons of a certain religion or belief, persons with disabilities, persons of a given age or of a certain sexual orientation; **\[...\]**

4.       An instruction to discriminate against persons on any of the grounds referred to in Article 1 shall be deemed to be discrimination within the meaning of paragraph 1.

5.

**6\.** Notwithstanding paragraph 2, differences of treatment on grounds of age shall not constitute discrimination, if they are objectively justified by a legitimate aim, and if the means of achieving that aim are appropriate and necessary. **\[…\]**

**In this context,** differences of treatment **\[…\] under national regulations** fixing a specific age for access to social protection, including social security, social assistance and healthcare; education; and certain goods or services which are available to the public**,** **\[…\] or providing for** more favourable conditions of access for persons of a given age, in order to promote their economic, cultural or social integration, **are presumed to be non-discriminatory.**

6a.     (new) Notwithstanding paragraph 2, differences of treatment of persons with a disability shall not constitute discrimination, if they are aimed at protecting their health and safety and if the means of achieving that aim are appropriate and necessary.

7.       Notwithstanding paragraph 2, in the provision of financial services, proportionate differences in treatment where, for the service in question, the use of age or disability is a determining factor in the assessment of risk based on relevant actuarial principles, accurate statistical data or medical knowledge shall not be considered discrimination for the purposes of this Directive.

8.       This Directive shall be without prejudice to measures laid down in national law which, in a democratic society, are necessary for public security, for the maintenance of public order and the prevention of criminal offences, for the protection of health and the protection of the rights and freedoms of others.

Article 3  
Scope

1.       Within the limits of the powers conferred upon the **European Union**, the prohibition of discrimination shall apply to all persons, as regards both the public and private sectors, including public bodies, in relation to **\[…\]:**

  

(a)          Social protection, including social security, social assistance and healthcare;

(b)

(c)          Education;

(d)     and the supply of, goods and other services which are available to the public, including housing.

Subparagraph (d) shall apply to natural persons only insofar as they are performing a professional or commercial activity defined  in accordance with national laws and practice.

2.       This Directive does not alter the division of competences between the European **Union** and the Member States. In particular it does not apply to:

(a)     matters covered by family law, including marital status and adoption, **and the benefits dependent thereon, as well as** laws on reproductive rights**;**

(b)     the organisation of  Member States' social protection systems, including decisions on the setting up, financing and management of such systems and related institutions as well as on the substance and delivery of benefits and services and the conditions of eligibility;

(c)     the powers of Member States to determine the type of health services provided and the conditions of eligibility; **\[…\]**

(d)     **\[…\]** **the organisation of the Member States' educational systems, the content of teaching and of** educational activities, including the provision of special needs education**; and**

(e)     **(new)** **the** **organisation** **of the Member States’ housing services, including the management or allocation of such services and the powers of the Member States to determine the conditions of eligibility for social housing.**

  

3.       **\[…\]** **the application of the principle of equal treatment in the area of education does not preclude differences of treatment based on religion in the context of the admission policies of ethos-based educational institutions.**

**These differences of treatment must be proportionate and necessary for the protection of the rights and freedoms of others**, **and shall not justify discrimination on any other ground.**

3a.     This Directive is without prejudice to national measures authorising or prohibiting the wearing of religious symbols.

4.       This Directive is without prejudice to national legislation ensuring the secular nature of the State, State institutions or bodies, or education, or concerning the status and activities of churches and other organisations based on religion or belief.

5.       This Directive does not cover differences of treatment based on nationality and is without prejudice to provisions and conditions relating to the entry into and residence of third-country nationals and stateless persons in the territory of Member States, and to any treatment which arises from the legal status of the third-country nationals and stateless persons concerned.

**\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_**

  

* * *

[\[1\]](#_ftnref1) OJ C , , p. .

[\[2\]](#_ftnref2) OJ C , , p. .

[\[3\]](#_ftnref3) Presidency conclusions of the Brussels European Council of 14 December 2007, point 50.

[\[4\]](#_ftnref4) Resolution of 20 May 2008 P6_TA-PROV(2008)0212.

[\[5\]](#_ftnref5) COM (2008) 412

[\[6\]](#_ftnref6) Directive 2000/43/EC, Directive 2000/78/EC and Directive 2004/113/EC

[\[7\]](#_ftnref7) Case C-303/06, Coleman v. Attridge, judgment of 17 July 2008, nyr.

[\[8\]](#_ftnref8) Wording taken from Recital 17 (see doc. 12792/09, p. 3).

[\[9\]](#_ftnref9) OJ L 166, 30.4.2004, p. 1.

[\[10\]](#_ftnref10) Regulation (EC) No. 1107/2006 and Regulation (EC) No 1371/2007

[\[11\]](#_ftnref11) OJ L 210, 31.7.2006, p.25. Regulation as last amended by Regulation (EC) No 1989/2006 (OJ L 411, 30.12.2006, p.6).

[\[12\]](#_ftnref12) OJ C 134, 7.6.2003, p.7

[\[13\]](#_ftnref13) OJ L 303, 2.12.2000, p. 16.

[\[14\]](#_ftnref14) OJ L 180, 19.7.2000, p. 22.

[\[15\]](#_ftnref15) OJ L 373, 21.12.2004, p. 37.