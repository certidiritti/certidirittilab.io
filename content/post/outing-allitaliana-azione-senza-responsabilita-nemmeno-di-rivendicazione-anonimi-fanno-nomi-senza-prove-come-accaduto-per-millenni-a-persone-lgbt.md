---
title: 'Outing all''italiana azione senza responsabilità, nemmeno di rivendicazione... anonimi fanno nomi senza prove, come accaduto per millenni a persone lgbt'
date: Fri, 23 Sep 2011 18:33:30 +0000
draft: false
tags: [Politica]
---

L'outing all'italiana diventa azione senza responsabilità, nemmeno azione di rivendicazione, dove accustori anonimi fanno i nomi di persone che chiederebbero poi scusa al papa. Lo strumento è contradditorio e non aiuterebbe nessuno.

Comunicato Stampa dell'Associazione Radicale Certi Diritti

Dopo tanto tam-tam è stato diffuso un elenco di personalità politiche, presunte gay, e omofobe, su un sito anonimo, depositato all'estero.  Le reazioni a questa iniziativa sono la più varie, sia all'interno della comunità LGBT che in tutta la società.

L'outing è uno strumento di per sè contraddittorio, utilizzato solo in alcune delle nazioni che sono all'avanguardia nella lotta dei diritti civili di gay e lesbiche, e sempre con risultati ambigui.

Siamo certi che se l'Italia fosse stata una nazione capace di trattare con dignità le persone omosessuali (in politica come nelle religioni, nelle istituzioni laiche così come nei media) si poteva risparmiare questo episodio che sicuramente non sarà ricordato come la punta di diamante della conquista dei diritti civili in Italia per le persone gay e lesbiche. Quali sarebbero gli effetti? Per alcune/i della comunità lgbt si soddisferebbe un bisogno di vendetta che nulla porterebbe né agli accusatori (rigorosamente anonimi) né tantomeno alle vittime (cambierebbero?).

In particolare colpisce l'assoluta mancanza di responsabilità (e addirittura di identità) da parte di chi accusa. Cosa che inoltre fa pensare che le accuse non potranno essere corredate delle necessarie prove, oltre che del contraddittorio che ci si aspetta in simili contesti. Messa così, tutta questa operazione potrà apparire esclusivamente pretestuosa, come già fu per il famoso caso Boffo.

Nessuno potrà poi dimenticare che le persone lesbiche, gay, bisessuali e transgender sono state esse stesse, per millenni, vittime di delazioni e comportamenti vili che avevano l’obiettivo di denigrare e mettere all’indice la loro esistenza. E’ mai possibile che una riflessione su questo i responsabili anonimi non riescano a farla? Almeno una riflessione.

In Italia, poi, ci sono altri aberranti precedenti che lasciano ipotizzare che operazioni di outing saranno inclini al fallimento. In alcuni casi si è  avuto come esito il rafforzamento della vergogna e dello stigma di un comportamento che invece meriterebbe di emergere con trasparenza, alla luce del sole. All'outing non è mai seguito, almeno qui da noi, un sereno esame di coscienza nè la valutazione coraggiosa e limpida dei propri comportamenti sessuali. Al contrario abbiamo assistito addirittura a pubbliche scuse davanti alla persona del papa, e chissà poi perché? Come se del proprio orientamento sessuale si debba dare di conto ad una autorità religiosa. Ma, appunto, siamo in Italia.