---
title: 'Coppie gay. Viale: indietro non si torna, il matrimonio è una opzione in agenda'
date: Mon, 23 Jul 2012 12:50:30 +0000
draft: false
tags: [Diritto di Famiglia]
---

"Lo dico alla Bindi, e lo dico a Casini, sulle coppie gay indietro non si torna. La loro presunta apertura sul riconoscimento giuridico delle coppie dello stesso sesso con vincoli affettivi, in qualunque essi intendano, non è un punto di arrivo, ma un punto di partenza."

Questa è il commento Silvio Viale, presidente di Radicali Italiani, al dibattito che si è aperto sulle aperture di Bindi e Casini sulle coppie gay.

Silvio Viale ha aggiunto:  
"Posso anche ammettere che quelle della Bindi e di Casini possano essere delle aperture, rispetto alle anacronistiche chiusure di un passato recente, sebbene condite da argomentazioni strampalate, ma la questione del matrimonio gay rimane in agenda. Io non chiedo al presidente del PD e al leader del terzo polo di convertirsi, ma solo di fare uno sforzo in più per riconoscere che i diritti umani dell'individuo sono imprescindibili dai rapporti affettivi. Noi non vogliamo limitare il valore religioso del loro matrimonio religioso, ma è inevitabile che la parità di diritti debba andare verso una parità giuridica e individuale di vincoli famigliari simili e uguali. Come per tutti i diritti civili, Bindi e Casini, devono essere consapevoli che indietro non si torna e cominciare a pensare che la società non può fermarsi ad aspettare loro. Devono cominciare ad accettare di essere minoranza, come lo sono sull'aborto e sul divorzio. Posso anche ammettere che quando i Costituenti scrissero famiglia intendessero quella tra un uomo e una donna, ma furono così lungimiranti da non scriverlo, perchè il principio fosse davvero universale. Anche quella tra due persone dello stesso sesso è  "una società naturale fondata sul matrimonio". Su questo a sinistra c'è una maggiore e crescente consapevolezza. Di Pietro e Grillo, fosse anche solo per un dispetto al PD, sembrano personalmente schierati. E' l'ora che anche nel centrodestra cessino ignavia e ipocrisia verso le coppie dello stesso sesso con vincoli affettivi affinché non si continui a negare diritti a cittadini cinsiderati di serie B."