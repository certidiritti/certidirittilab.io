---
title: 'Parlamento europeo vota risoluzione per depatologiazzazione transessualismo. Grande passo avanti di civiltà'
date: Thu, 13 Oct 2011 21:57:13 +0000
draft: false
tags: [Transessualità]
---

Roma – Bruxelles, 13 ottobre 2011

Dichiarazione di Maria Gigliola Toniollo, Responsabile Cgil Nuovi Diritti e Sergio Rovasio, Segretario Associazione Radicale Certi Diritti

Il 28 settembre scorso il Parlamento Europeo ha votato una Risoluzione a sostegno dell'esclusione del transessualismo dalla lista delle malattie mentali nella futura versione della ICD - International Classification of Diseases" dell'Organizzazione Mondiale della Sanita'.

La risoluzione sostiene la battaglia che le associazioni delle persone transessuali stanno conducendo in tutto il mondo per ottenere nell'ICD 11 una riclassificazione della cosiddetta "disforia di genere", che preveda il ricorso ai farmaci, alle cure mediche e chirurgiche per le persone transessuali, ove ne abbiano la necessità, senza lo stigma negativo dell'inquadramento medico-psichiatrico.

L'Associazione Radicale Certi Diritti si felicita con il Parlamento Europeo, e ringrazia  la parlamentare olandese Emine Bozkur, del gruppo socialista, presentatrice del testo, per l'impegno civile dimostrato verso una situazione umana complessa e gravemente colpita dal pregiudizio e rinnova il proprio impegno in favore della liberta' e dell'autodeterminazione delle persone.