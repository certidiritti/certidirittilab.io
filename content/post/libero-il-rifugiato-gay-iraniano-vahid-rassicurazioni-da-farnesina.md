---
title: 'LIBERO IL RIFUGIATO GAY IRANIANO VAHID, RASSICURAZIONI DA FARNESINA'
date: Fri, 26 Jun 2009 15:15:48 +0000
draft: false
tags: [Comunicati stampa]
---

LIBERO IL RIFUGIATO GAY IRANIANO VAHID. HA RIABBRACCIATO IL SUO COMPAGNO A ROMA. RASSICURAZIONI DA FINI E FARNESINA SULLA PROTEZIONE UMANITARIA

GRUPPO EVERYONE: “UN CASO CHE SI CONCLUDE POSITIVAMENTE GRAZIE ALL’INTERCESSIONE DEL PRESIDENTE DELLA CAMERA CON LA FARNESINA E IL MINISTERO DEGLI INTERNI”

Vahid Kiani Motlagh, il gay iraniano 32enne che era stato fermato in Francia lo scorso 25 maggio e rinchiuso nel centro di detenzione all’aeroporto di Saint-Exupery a Lione, si trova ora di nuovo a Roma, finalmente libero. “Vahid, come era stato anticipato ieri dal nostro Gruppo,” dichiarano Roberto Malini, Matteo Pegoraro e Dario Picciau, co-presidenti del Gruppo EveryOne “è partito da Lione alle 8,40 ed è arrivato all’aeroporto romano di Fiumicino alle 10,15, dove è stato poi trattenuto, fino a poco fa, per le procedure di trattazione alla polizia di frontiera in aeroporto, quindi in Questura di Roma. Provato, ma felice, Vahid è ora libero, a fianco del suo compagno." "Ieri avevamo inviato un appello urgente al Presidente della Camera dei Deputati, Gianfranco Fini, per sollecitare una sua azione di sensibilizzazione con la Farnesina e il ministro Frattini, e già in serata avevamo ricevuto le prime rassicurazioni. Questa mattina" continuano i leader di EveryOne "abbiamo avuto la conferma dal presidente Fini della sua intercessione con la Farnesina e il ministro Frattini affinché venisse accordata immediata protezione umanitaria al ragazzo. La Farnesina ha fatto sapere di stare seguendo con attenzione l'intero caso, nell'interesse di preservare al massimo i diritti fondamentali di Vahid, escludendo da subito una sua futura deportazione in Iran".

"E’ stata una campagna faticosa," contiua il Gruppo EveryONe "ma siamo riusciti a seguire passo dopo passo Vahid attraverso un'azione di diplomazia internazionale con le autorità francesi, l'Alto Commissario ONU per i Rifugiati, il Parlamento Europeo - grazie all'intercessione di Ottavio Marzocchi dell'Associazione Radicale Certi Diritti - e le istituzioni italiane. Ringraziamo Gianfranco Fini e il consigliere diplomatico Alessandro Cortese, che non solo si sono dimostrati disponibili e interessati alla positiva risoluzione del caso, ma hanno permesso che già dall'arrivo di Vahid in Italia venisse attivata una procedura che ne escludesse a priori la deportazione nel Paese d'origine".

EveryOne ringrazia infine tutte le associazioni che si sono attivate per supportare l'azione per la vita di Vahid, in particolare Certi Diritti e la Fondazione Massimo Consoli, che hanno promosso iniziative a sostegno della nostra campagna; Arcigay Roma, che d'ora in poi seguirà Vahid passo dopo passo nelle istanze di protezione umanitaria; Arcilesbica Roma, GayNet, Azione Trans e una rete internazionale di organizzazioni per i Diritti Umani. Un grazie infine agli on. Concia e Della Vedova; agli eurodeputati Lambert, Romeva, Cashman, In´t Veld, Lunacek, Gröner; a Dirk De Meirleir, direttore di ILGA-Europe; al PRI e ai giovani ebrei d'Italia, che hanno sollecitato il Governo a interessarsi del caso e attivare canali diplomatici per fornire protezione umanitaria immediata a Vahid.

Per ulteriori informazioni:  
Gruppo EveryOne  
+39 334 8429527 :: +39 331 3585406  
www.everyonegroup.com :: info@everyonegroup.com