---
title: 'IDEE RADICALI SU BOFFO E LA PRIVACY'
date: Sun, 30 Aug 2009 16:16:40 +0000
draft: false
tags: [Comunicati stampa]
---

**Il Riformista, 30 agosto 2009**

_Rubrica delle lettere_

Con una ipocrisia degna di peggior causa **Vittorio Feltri** sostiene che il fatto per cui il Direttore di Avvenire, **Dino Boffo**, sia da giudicarsi immorale è la condanna per molestie e non per le sue relazioni omosessuali, come se lo svelamento dell’omosessualità del Direttore dell’organo di stampa di proprietà della Conferenza Episcopale Italiana non fosse molto di più che sapido pettegolezzo, e non si inserisca nella feroce lotta per l’ egemonia sul capo del Governo tra la Lega e il Vaticano.

Quello che invece è molto chiaro è che il Direttore de Il Giornale, applicando la ben nota legge del taglione, dà maggior vigore proprio a quel metodo che con il suo giornalismo al vetriolo intende censurare, ovvero delegittimare il proprio avversario usando l’arma del giudizio morale, in particolare del giudizio sulla sua moralità sessuale.

L’insopportabile **ambiguità** dell’iniziativa di Vittorio Feltri, che per difendere il diritto alla privacy del Presidente del Consiglio sputtana quella del Direttore de L’Avvenire, è il motivo per cui ci sembra che si debba, con forza, difendere la libertà di Boffo, e chiunque altro, ad avere relazioni sessuali con adulti consenzienti senza che questo diventi oggetto di valutazione morale da parte di chicchessia.

E quando parliamo di difendere la libertà alla **privacy** di Dino Boffo ci riferiamo anche all’ipotesi che egli possa essere, a un tempo, omosessuale e contrario ai diritti delle coppie tra persone dello stesso sesso. Non v’è nessuna logica accettabile dietro l’argomento per cui un omosessuale “velato” non possa attaccare pubblicamente iniziative politiche sull’omosessualità. La nostra personale incomprensione della distonia tra comportamenti privati e posizioni pubbliche non può arrivare a salutare come benvenuti **outing ‘forzati’ e violenti** come quello fatto da Feltri.

Eppure se si rimane dentro la logica che il doppio caso Berlusconi Boffo rappresenta, tutto è legittimo, comprese le prossime neanche tanto velate minacce di Feltri in merito ad altri “svelamenti” di doppie moralità.

In verità tutti gli attori di questa vicenda sanno benissimo che l’unico modo per uscire da questo circolo vizioso di ricatti incrociati è semplice, e passa attraverso la serena rivendicazione di Boffo, Berlusconi e compagnia, non solo del diritto alla privacy, ma del diritto ad avere relazioni sessuali e sentimentali senza che questo diventi oggetto di valutazione morale. Non sappiamo se questo accadrà mai, per il momento registriamo solo la negazione di qualsiasi cosa da parte di Berlusconi e il rumoroso imbarazzo di Boffo, che si trincera dietro la solita teoria del complotto e della aggressione.

**Noi, pazienti, continueremo a difendere il diritto alla privacy delle loro Signorie**. Ci facessero, però, la cortesia di guardare con meno sprezzante superficialità alle richieste di eguaglianza che arrivano da buona parte di quella minoranza di qualche milione di italiani che si preoccupa più dei propri diritti che della moralità altrui. Alla fine sarebbe un bene per tutti, anche per loro.

  
**Enzo Cucco**, direttore della [Fondazione Sandro Penna](http://www.fondazionesandropenna.it/)

**Sergio Rovasio**, segretario [Associazione Radicale Certi Diritti](http://www.certidiritti.it/)