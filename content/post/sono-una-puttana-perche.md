---
title: 'Sono una puttana perché ...'
date: Sun, 22 Apr 2012 15:27:59 +0000
draft: false
tags: [Lavoro sessuale]
---

Testo letto da Tenera Valse in chiusura della conferenza nazionale sulla legalizzazione della prostituzione tenutasi il 21 aprile al palazzo della Provincia di Roma e organizzata dall'Associazione Radicale Certi Diritti, dal Comitato per i diritti civili delle prostitute e da CGIL-Nuovi Diritti.

Sono una puttana perché mi piace

Sono una puttana perché ho amato senza riserve

Sono una puttana perché ho disobbedito senza riserve

Sono una puttana perché non confondo più sesso, sex work e amore

Sono una puttana perché come puttana posso ridere senza motivo e anche a sproposito

Dico sono una puttana e non faccio la puttana perché quando lo faccio lo sono anche intimamente

Sono una puttana perché il lavoro che avevo prima non sapevo più farlo

Sono una puttana per essere la figlia che i miei non avrebbero voluto mai e non vogliono adesso

Sono una puttana perché non sono il figlio maschio che i miei avrebbero voluto

Sono una puttana per capire chi mi vuole veramente bene e mi accetta anche se sono una puttana

Sono una puttana perché non sopporto i tacchi a spillo e voglio indossarli il meno possibile per camminare a piedi scalzi

Sono una puttana perché non sopporto il perbenismo

Sono una puttana perché il mio lavoro mi ha impoverito

Sono una puttana perché gli uomini che rispondono agli annunci erotici te la chiedono gratis senza dire né buongiorno né buonasera e dando per scontato che sei una puttana gratuita

Sono una puttana perché dai diamanti non nasce niente dal letame nascono i fiori

Sono una puttana perché gli uomini vogliono fare con me cose che non hanno il coraggio di chiedere alle mogli e alle fidanzate (incluso il mio compagno)

Sono una puttana perché mio marito confonde la donna per bene che ha sposato con la propria madre e qualche volta anche con sua figlia

Sono una puttana perché gli uomini per bene vogliono sposare una donna per bene ma per fare sesso preferiscono una puttana

Sono una puttana perché il sesso ognuno lo fa come vuole anche a pagamento se ritiene opportuno

Sono una puttana perché qualcuno mi ha detto ti vesti come una puttana, ti trucchi come una puttana, fai sesso come una puttana

Sono una puttana per sapere cosa provano in strada le donne sfruttate, mie sorelle

Sono una puttana perché il lavoro a 4 euro l’ora è schiavitù per uomini e donne e io voglio vivere

Sono una puttana perché ho bisogno di leggere e avere tanti libri

Sono una puttana perché il lavoro intellettuale e il lavoro artistico sono sottopagati sfruttati e offesi

Sono una puttana perché l’Italia ha rinunciato ai suoi artisti

Sono una puttana perché l’Italia ha affamato i suoi artisti

Sono una puttana per poter restare in Italia e fare arte

Sono una puttana perché i miei amici artisti sono quasi tutti andati via, attori, registi, produttori più o meno indipendenti, musicisti, ballerini, mimi, pittori, performer, artisti visivi, designer, architetti e folli innamorati della vita

Sono una puttana perché le mie amichette studentesse universitarie per pagarsi una camera tugurio in affitto a Roma devono fare le puttane (i maschi per lo stesso motivo spacciano o battono anche loro)

Sono una puttana perché amo la mia vagina e non la disprezzo

Sono una puttana perché certe volte quando ho fatto l’amore mi sono sentita stuprata

Sono una puttana perché la famiglia in questo momento storico è il luogo più violento che c’è

Sono una puttana perché sono stata una bambina assillata

Sono una puttana per tutte le donne che sono morte a causa del loro essere donne, madri, lavoratrici, figlie incomprese, mogli sole, puttane sole, amanti nascoste, fidanzate uccise perché un uomo insicuro le perseguitava

Sono una puttana perché il sesso a pagamento è un lavoro e ha dignità

Sono una puttana perché la puttana è genere sessuale diverso e un piacere diverso per tutti coloro che lo praticano

Sono una puttana perché il sesso è sacro e sacro a questo mondo vuol dire monetizzato. Che tutti sappiano cos’ é il loro senso del sacro

Sono una puttana perché ogni gesto è puro incluso il più riprovevole se lo fai con l’istinto di un animale e non di un uomo

Sono una puttana perché i miei alunni consideravano Pasolini un frocio marchettaro

Sono una puttana perché sono femmina e femminista

Sono una puttana perché per una donna fare sesso libero in molti posti è ancora un reato

Sono una puttana, una strega una mistica, una madre, una santa perché posso contenere il mondo, tenendomi in equilibrio

Sono una puttana perché il mio pensiero potrebbe dare fastidio

Sono una puttana perché hanno uccisa Ipazia dandole della puttana

Sono una puttana perché non è solo il mestiere delle donne ma da che mondo e mondo è il lavoro degli uomini

Sono una puttana perché ho abolito il senso di colpa dei genitali

Sono una puttana perché il mio cuore è puro e non ha doppio fondo

Sono una puttana perché rigetto il potere

Sono una puttana perché voglio riprendermi il desiderio di essere donna come dico io e non come dice un uomo

Sono una puttana perché questo è un mestiere inventato da un uomo per dividere le donne in belle e brutte buone e cattive e io me lo riprendo

Sono una puttana perché voglio stabilire il prezzo che gli uomini mi hanno dato e mi hanno tolto

Sono una puttana perché questo è un lavoro nobile e antico rovinato dalla politica come dice Pia Covre

Sono una puttana perché tutte le donne per bene e per male sono sorelle

Sono una puttana perché una moglie per bene può essere più in vendita di una puttana

Sono una puttana perché non voglio la protezione di un uomo

Sono una puttana perché questa parola si usa a sproposito

Sono una puttana perché mio padre aveva paura che diventassi una puttana

Sono una puttana perché voglio che noi donne riprendiamo a vederci e a ridere delle nostre imperfezioni fisiche e della nostra vecchiaia

Sono una puttana contro una società che trasforma le nostre bambine, le nostre figlie in tutto il mondo in puttane

Sono una puttana perché di dieci uomini politici che ho conosciuto a nessuno darò mai il mio voto solo perché è mio cliente

Sono una puttana perché meglio freddare i bollenti spirti con una puttana che fare violenza alle proprie donne

Sono una puttana perché gli uomini non sanno gestire come uomini la propria sessualità

Sono una puttana perché la civiltà di un paese si misura da come tratta le proprie donne

Sono una puttana perché amo la democrazia e odio la dittatura della maggioranza

Sono una puttana perché da un uomo di potere non ho mai accettato mai niente tranne il mio cache e da mangiare. Da mio padre

Sono una puttana perché finalmente mi amo e mi accudisco come merito

Sono una puttana perché ho bisogno di una stanza tutta per me

Sono una puttana perché voglio capire che c’è di tanto strano in questo mestiere

Sono una puttana perché le donne devono stare tutte insieme puttane e donne per bene e mettere insieme le loro vite

Sono una puttana perché ancora oggi una donna non prende valore di per sé ma ancora come secoli addietro per il matrimonio che ha fatto al massimo per gli studi che ha fatto o i figli che ha avuto

Sono una puttana perché voglio sapere perché le donne hanno paura di essere definite puttane

Sono una puttana per sapere perché questa società ingiuria le puttane, le occulta, le biasima, le disprezza ma dietro le porte delle puttane c’è sempre la fila

Sono una puttana colta perché la gente dice che se fai la puttana è perché non hai altre possibilità

Sono una puttana perché vorrei che tutte le donne dicessero anch’io sono una puttana come te e non mi vergogno di dirlo perché anch’io ...

Sono una puttana

Sono Tenera Valse

*foto di Eleonora Calvelli per Vanity Fair

**[FIRMA IL MANIFESTO/APPELLO PER  
LA LEGALIZZAZIONE DELLA PROSTITUZIONE >](legalizzazione-prostituzione)**