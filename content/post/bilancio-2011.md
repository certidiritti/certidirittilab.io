---
title: 'BILANCIO 2011'
date: Fri, 09 Dec 2011 07:54:53 +0000
draft: false
tags: [Politica]
---

**Situazione finanziaria al 29 novembre 2011**

**AVANZO DI BILANCIO 2010**

**€ 1.765,00**

**RICAVI 2011**

ISCRIZIONI e CONTRIBUTI

€ 17.906,00

INTERESSI BANCARI ATTIVI

€ 16,00

**Totale**

**€ 17.922,00**

**COSTI 2011**

**SPESE GENERALI**

_Spese Bancarie_

€ 183,38

_Sito web_

€ 151,00

_Acquisto computer_

€ 582,99

_Spedizioni e cancelleria_

€ 103,44

_Telefono_

€ 75,00

_Spedizione e stampa tessere_

€ 494,80

**Subtotale**

**€ 1.590,61**

**CONGRESSI E RIUNIONI**

_Congresso Milano_

Affitto sala

€ 665,00

Pubblicità

€ 78,05

Accantonamento costi presunti

€ 654,00

Totale

**€ 1.397,05**

_Riunioni varie_

**€ 434,00**

**Subtotale**

**€ 1.831,05**

**CONTRIBUTI EROGATI**

_Contributo Smug Uganda_

€ 575,00

_Contributo Convegno Intersex Firenze_

€ 200,00

_Iscrizione Ilga Europe e Ilga Europe 2011_

€ 250,00

_Iscrizione Campania Rainbow_

€ 50,00

_Iscrizione Tv Radicale_

€ 100,00

**Subtotale**

**€ 1.175,00**

**INIZIATIVE POLITICHE**

_Contributo ricorso Affermazione civile_

€ 500,00

_Gadget, preservativi e volantini_

€ 1.684,60

_EuroPride Roma_

€ 4.162,50

_Libro 'Dal cuore delle coppie al cuore del diritto'_

€ 2.371,60

_Serata contro la Sessuofobia_

€ 600,00

_Varie_

€ 4.006,64

**Subtotale**

**€ 13.325,34**

**TOTALE COSTI**

**€ 17.922,00**

**AVANZO DI BILANCIO 2011**

**€ 0,00**

**TOTALE ATTIVO**

**€ 1.765,00**