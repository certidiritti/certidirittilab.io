---
title: 'IN RICORDO DI ENZO FRANCONE. di Sergio Rovasio'
date: Mon, 30 Nov 2009 12:32:23 +0000
draft: false
tags: [Comunicati stampa]
---

**In ricordo di Enzo Francone**

**di Sergio Rovasio**

Ho conosciuto Enzo tanti anni fa. Credo fosse il 1978 o il 1979. Su alcuni muri della zona Lingotto di Torino, dove sono nato e cresciuto, vidi un giorno dei manifesti dove si vedevano due persone nude che in un disegno stilizzato correvano libere con delle catene spezzate. Era il simbolo della liberazione sessuale, uno dei cavalli di battaglia, quello certo più ‘scandaloso’, dei radicali. Il disegno era di Silombria che in quel periodo collaborava con i radicali e il Fuori!, organizzazioni che a Torino, in quegli anni, erano una cosa sola.

Incuriosito da quei manifesti andai in Via Garibaldi 13 a Torino, nella loro sede. Ero un po’ agitato, l’idea di vedere o incontrare delle persone così ‘strane’ mi metteva curiosità e inquietudine... Nel cortile del palazzo storico, sul lato destro, v’erano alcuni vecchietti che giocavano a carte sotto un cartello che diceva: ‘pensionati radicali’; sul retro di un altro portone di vetro si vedevano delle persone che parlavano intorno a un microfono che pendeva dal soffitto, era una radio libera appena nata, si chiamava Radio Radicale, non era ancora diventata network nazionale. Era la radio libera più ascoltata in città, dava voce ad una varia umanità: tossicodipendenti, gay, travestiti, pensionati, prostitute, casalinghe disperate, mogli tradite, uomini soli e così via. Sul lato sinistro del cortile invece c’era la sede ‘ufficiale’ dei radicali, che era un gran casino. C’erano carte, tavoli, persone che scrivevano cartelli e mi colpii molto un telefono a gettoni; fu messo lì, mi spiegò poi Enzo, perché così almeno sulle spese telefoniche si risparmiava. Tutto questo gran teatro di strada, di vera umanità, era ‘gestito’ da Enzo Francone che era il Segretario Regionale del Partito Radicale.

Enzo era anche il Segretario del Fuori!, il primo movimento gay italiano nato nel 1970, che inaugurò le sue azioni nel 1972, quando un gruppo di ‘pazzi’, guidati da Angelo Pezzana, si incatenò davanti al Casinò di Sanremo dove si svolgeva un convegno di Psichiatri che definiva l’omosessualità un ‘flagello sociale’. Insieme ad Angelo ed Enzo, nel gruppo che manifestava, e che fu portato in commissariato, c’era, tra gli altri, Mario Mieli.

Ciò che si vedeva nel cortile di Via Garibaldi 13, ben rappresentava lo spirito libertario di quegli anni, ciò di cui Enzo era ‘diretto responsabile’ e ciò dava l’idea della personalità che lo ha poi sempre caratterizzato. Fu anche l’autore del ‘manuale di autodifesa del travestito’ che veniva distribuito di notte nei luoghi di prostituzione contro alcuni soprusi delle forze dell’ordine.

Da allora Enzo non è cambiato in nulla, è sempre stato impegnato con determinazione e simpatia nelle iniziative che lo hanno visto protagonista in favore della difesa della libertà, dei diritti delle persone lgbt a Torino, in Italia e all’estero. E’ stato tra i fondatori dell’Iga, poi divenuto Ilga, (International Lesbian and Gay Association) e della Fondazione Sandro Penna di Torino. Era uno dei principali animatori del Coordinamento Torino Pride. Esempio di capacità organizzativa e coabitativa tra associazioni che lavorano per gli obiettivi comuni. La sua famiglia era capitanata da Enzo Cucco e poi tutti gli altri compagni e amici che gli sono stati vicini fino all’ultimo.

Tra gli aspetti più creativi che lo hanno visto promotore di iniziative innovative per il movimento lgbt non si può non ricordare il primo gay pride italiano che si svolse a Torino nel 1978, la prima partita di calcio tra gay ed etero; la prima volta di un locale gay, il Fire, gestito da un’associazione, che per anni è stato il punto di riferimento della comunità lgbt di Torino; il primo giornale gay, il Fuori Notizie, e molto, molto altro.

Certamente le azioni più clamorose (e rischiose) di cui Enzo si rese protagonista furono quelle in cui si fece arrestare: a Teheran nel 1979 contro le persecuzioni verso le persone gay del neo-regime “rivoluzionario” komeinista e a Mosca nel 1980, sulla Piazza Rossa, in occasione dell’inaugurazione delle olimpiadi, per protestare contro l’articolo 121 del codice penale sovietico e contro la detenzione di due persone gay ingiustamente arrestate, di cui nessuno più parlava. Subì interrogatori e violenze fisiche, venne espulso. Mostrò alla stampa i segni delle violenze al suo arrivo in Italia.

Lo scorso marzo L’Associazione Radicale Certi Diritti lo elesse Tesoriere con grande entusiasmo e si mise subito al lavoro. Forte della sua esperienza si attivò immediatamente per aiutare gli organizzatori del Genova Pride. Durante la marcia non smise mai di ballare sul carro di Certi Diritti l’inno dei gay ‘Ymca’, e noi eravamo pure un po’ preoccupati. La malattia lo ha colpito all’improvviso e gli ha lasciato poche settimane di vita. Quando in questi giorni gli telefonavo per tenerlo aggiornato delle nostre iniziative era felice. L’ultima telefonata giovedì scorso è stata quella dell’addio, me lo fece intendere lui, non lo avevo mai sentito così.

Caro Enzo, sei stato davvero qualcosa di speciale per tutti noi. Grazie.

Sergio Rovasio

Segretario Associazione Radicale Certi Diritti

[www.certidiritti.it](http://www.certidiritti.it/)