---
title: 'Europa non dimentichi la lotta alle discriminazioni: lettera di Associazioni a Monti per sblocco Direttiva Commissione Europea'
date: Thu, 28 Jun 2012 11:04:54 +0000
draft: false
tags: [Europa]
---

Roma, 28 giugno 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Con una lettera aperta indirizzata al Presidente Monti e ad alcuni Ministri del suo Governo l’Associazione Radicale Certi Diritti, Arcigay, FISH (Federazione italiana per il superamenro dell’handicap) ed ENAR (european network against racism) chiedono al Governo italiano di esercitare la propria leadership per sbloccare la nuova direttiva antidiscriminazione che giace presso il Consiglio dell’Unione europea.

Tutta Europa guarda al prossimo Consiglio europeo per le importanti decisioni che dovrà assumere in materia di economia.

Noi vorremmo ricevere buone notizie anche in merito al progetto di Direttiva europea contro tutte le forme di discriminazione: già approvata dalla Commissione e dal Parlamento, giace sul tavolo del Consiglio dell’Unione Europea (l’altra istituzione intergovernativa che insieme al Consiglio europeo indirizza il governo dell’Unione) da più di due anni bloccata dai veti incrociati di alcuni Paesi, tra cui la Germania e l’Italia.

Si tratta di una Direttiva della massima importanza, perché estende a tutte le categorie di potenziale discriminazione la protezione già prevista per il settore lavoro (con la Direttiva n. 78 del 2000) e per motivi di origine etnica e contro il razzismo (con la Direttiva n. 43 del 2000), completando il quadro normativo europeo delineato nella Carta europea dei diritti fondamentali e nel Trattato per il funzionamento dell’Unione europea.

Si tratta quindi di estendere il principio di non discriminazione a tutti gli ambiti della vita delle persone a rischio discriminazione per motivi connessi alla propria età, disabilità, orientamento sessuale, religione. Contro questa direttiva le lobby del fondamentalismo religioso e della miopia europea contro l’inclusione sociale si sono alleate, e per il momento hanno avuto la meglio.

**In allegato il testo integrale della Lettera.**

  
[lettera\_governo\_italiano\_direttiva\_orizzontale.doc](http://www.certidiritti.org/wp-content/uploads/2012/06/lettera_governo_italiano_direttiva_orizzontale.doc)