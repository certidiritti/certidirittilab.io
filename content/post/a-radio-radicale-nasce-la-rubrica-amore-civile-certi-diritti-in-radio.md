---
title: 'A RADIO RADICALE NASCE LA RUBRICA ‘AMORE CIVILE – CERTI DIRITTI IN RADIO’'
date: Fri, 24 Dec 2010 09:16:32 +0000
draft: false
tags: [Comunicati stampa]
---

**A RADIO RADICALE NASCE LA RUBRICA ‘AMORE CIVILE – CERTI DIRITTI IN RADIO’.** **LA PRIMA PUNTATA LA NOTTE TRA IL 24 E IL 25 DICEMBRE 2010.**

Roma, 23 dicembre 2010

La notte tra giovedì 24 e venerdì 25 dicembre, dalla mezzanotte alle due, andrà in onda su Radio Radicale, la prima puntata dell’appuntamento settimanale radiofonico ‘**Amore civile – Certi Diritti in Radio**’.

Tra i temi trattatati: proposte di iniziativa politica decisi dal IV Congresso dell’Associazione Radicale Certi Diritti dello scorso novembre, matrimonio tra persone dello stesso, la campagna di Affermazione Civile,  progetto di Riforma del Diritto di Famiglia.

Sono inoltre previsti collegamenti telefonici con Pia Covre, Presidente del Comitato per i Diritti Civili delle Prostitute, Rita Bernardini, deputata radicale eletta nelle liste del Pd e neo-Presidente dell’Associazione Radicale Certi Diritti e Maria Gigliola Toniollo, Cgil Nuovi Diritti e rappresentante di Certi Diritti nel Comitato nazionale di Radicali Italiani.

La trasmissione radiofonica, settimanale, tratterà i temi della Riforma del diritto di famiglia (divorzio breve, unioni civili, matrimonio tra persone dello stesso sesso, adozioni e affido, parità tra figli nati fuori e dentro i matrimonio, lotta alla violenza dentro le mura domestiche, in particolare contro le donne, ecc), delle politiche proibizioniste in tema di prostituzione, della censura ideologica e fondamentalista in tema di informazione sessuale, della prevenzione delle malattie sessualmente trasmissibili, del pregiudizio e della violenza contro le persone transessuali e transgender, della promozione e difesa dei diritti civili e umani delle persone, anche riguardo il loro orientamento sessuale.

Per idee, suggerimenti, proposte, contattare la redazione di ‘Amore Civile – Certi Diritti in Radio’ al seguente indirizzo e-mail: [info@certidiritti.it](mailto:info@certidiritti.it)