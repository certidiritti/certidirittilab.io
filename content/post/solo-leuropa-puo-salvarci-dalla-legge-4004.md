---
title: 'SOLO L''EUROPA PUÒ SALVARCI DALLA LEGGE 40/04'
date: Thu, 22 May 2008 06:29:41 +0000
draft: false
tags: [Amica Cicogna, Chiara Lalli, Comunicati stampa, Diritto alla salute, Legge 40]
---

Il 23 maggio 2008, ore 11.30, conferenza stampa alla [sala della Stampa Estera, via dell’Umiltà 83/C](http://maps.google.it/maps?q=via+dell'umilt%C3%A0+83+c+,+Roma&ie=UTF8&ll=41.900456,12.482808&spn=0.007155,0.020084&z=16) organizzata da L’Associazione Amica Cicogna onlus con l’Associazione Luca Coscioni per la libertà di ricerca scientifica e le Associazioni Cerco un bimbo, Unbambino.it, L’altra cicogna onlus, Madre Provetta onlus La legge 40/2004 viola il diritto alla salute, il diritto alla salute riproduttiva e all’equità di trattamenti sanitari che dovrebbero essere garantiti a tutti i cittadini europei (indipendentemente dallo Stato membro di residenza o in cui si richiede la prestazione).  
  
Le violazioni di tali diritti emergono con evidenza dalle relazioni sugli effetti della legge 40/04 presentate al Parlamento: le conseguenze dell’applicazione di una norma dello Stato Italia sono certificate e documentate in modo ufficiale.  
  
Risulta palese la violazione di molte norme del diritto comunitario derivante: dall’esclusione di molti cittadini all’accesso delle tecniche al divieto di fecondazione eterologa; dall’impossibilità di revocare il consenso dopo la produzione degli embrioni (e non fino al momento dell’impianto come sanciscono le leggi di altri Paesi); dal limite di produzione di 3 embrioni; dal divieto di accesso per tutti coloro che desiderano una diagnosi di preimpianto non invasiva per garantire la salute della donna e del nascituro.  
  
Durante la conferenza verranno forniti i dettagli sulle iniziative già avviate e sui requisiti necessari per avviarne.  
Interventi  
Coordina: Frances Mary Kennedy, giornalista  
Chiara Lalli, bioeticista  
Filomena Gallo, avvocato, Presidente di Amica Cicogna  
I Presidenti e i Segretari delle Associazioni: Marco Cappato, Federica Casadei, Monica  
Soldano, Laura Pisano, Patrizia Battistini, alcuni esperti.  
A seguire gli interventi di alcuni cittadini italiani discriminati dalla legge 40/04.  
Sono stati invitati i Parlamentari Italiani e i Parlamentari del Parlamento Europeo.  
Per informazioni  
Simona Nazzaro (333 2041828) Chiara Lalli (333 4680028) e Filomena Gallo (333 4567091)