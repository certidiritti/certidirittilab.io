---
title: 'SINTESI SULLA RIUNIONE DEL 30 MARZO 2010'
date: Thu, 01 Apr 2010 09:11:54 +0000
draft: false
tags: [Senza categoria]
---

Carissim*,

vi aggiorniamo sulla riunione di ieri sera come sempre ricca di aggiornamenti e prossimi eventi.

### **1)SENTENZA CORTE COSTITUZIONALE**

a-  Abbiamo fatto un breve resoconto di quanto avvenuto a Roma tra il 20 e il 23 Marzo

-lo voglio                                     i.    la fondazione "politica"del comitato "Si"

-Sera del 21                                ii.   Manifestazione in Piazza S. Apostoli

-Interventi degli avvocati            iii.   La sentenza della corte dal vivo,gli stati d'animo delle tante persone accorse.

                                                   iv.   La definizione di una linea di condotta della comunicazione per i vari scenari

                                                          possibili.

b-C'è stato tra l'altro un intervento in diretta su Radio Popolare,proprio durante la nostra riunione,certi diritti Milano ha viaggiato nell'etere,"live"in tutta Italia...

### **2)LE COLLABORAZIONI DI CERTI DIRITTI IN ATTO SUL TERRITORIO DI MILANO**

a-Continua il lavoro di YURI e AVI con la consulta laica di Milano.

b-MARCO e  VALERIO ci hanno aggiornato sul loro lavoro con il coordinamento arcobaleno delle associazioni LGBT milanesi.In vista ci sono due incontri dove siamo tutti invitati a partecipare:

i. Venerdì 9 Aprile,la sera,festa di autofinanzianamento del coordinamento per le prossime uscite.

ii. Domenica 18 Aprile,"L'amore spiazza"fa tappa a Bergamo,con le stesse modalità già testate con successo a Magenta:presenza LGBT in una piazza del centro,con varie micro attività che permettono una interazione in diretta con le persone.

iii. Al più presto il coordinamento deciderà anche in merito ad un Pride locale a Milano.

c-EMANUELE(che cura la sezione "Milano" del sito Certi diritti)si sta preparando a partecipare al progetto IO di Arcigay(immigrazione e omosessualità)anche grazie alle idee di GABRIELLA Frisio esperta nel settore e membro dell'associazione "Renzo e Lucio"di Lecco.

d-Inoltre sempre YURI ha molto collaborato con il CIG e altre realtà di Milano per varie iniziative comuni che ci sono state durante i giorni dell'udienza presso la Corte Costituzionale.