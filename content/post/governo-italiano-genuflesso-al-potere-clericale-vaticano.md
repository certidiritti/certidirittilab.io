---
title: 'GOVERNO ITALIANO GENUFLESSO AL POTERE CLERICALE VATICANO'
date: Fri, 06 Feb 2009 17:26:03 +0000
draft: false
tags: [clericale, Comunicati stampa, englaro, governo]
---

******

DECRETO ENGLARO: IL GOVERNO GENUFLESSO AL POTERE CLERICALE VATICANO, NESSUN RISPETTO PER LA DIGNITA' UMANA, IMPORRE LE PROPRIE VISIONI E' DA CULTURA CLERICO-FASCISTA. GRAVE STRAPPO ALLA LEGALITA' COSTITUZIONALE.

******

 **Roma, 6 febbraio 2009**

**Comunicato Stampa del Direttivo dell'Associaizone Radicale Certi Diritti:**

"Quanto fatto oggi dal Governo Berlusconi è la conferma che sul piano dei diritti civili ci troviamo in un'era politica di grave involuzione, di affermazione della peggiore cultura politica clerico-fascista. Voler imporre a tutti gli italiani una visione dogmatica, integralista e fondamentalista della vita con leggi ad hoc, principalmente per asservire il potere delle gerarchie vaticane, è in netto contrasto con le leggi di civiltà e tutela della persona che ormai tutte le democrazie europee promuovono. Il nostro paese si allontana dall'Europa e si avvicina sempre più alle peggiori dittature teocratiche.

Il Decreto Legge approvato dal Governo rappresenta un grave strappo alla legalità costituzionale. Se i nostri diritti e la loro tutela, assicurata dalla magistratura, sono esposti all'arbitrio della maggioranza parlamentare, ciò significa che non esiste più il patto costituzionale che ha portato il nostro Paese fuori dall'epoca fascista. Quando si invoca il ricorso al popolo si decreta la fine dello stato di diritto.

L'Associazione Radicale Certi Diritti esprime il suo più grande plauso al Presidente della Repubblica per il coraggioso gesto di non firmare il Decreto clerical-fondamenalista".