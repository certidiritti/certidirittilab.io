---
title: 'Il Papa parla di Aids, scontro sul preservativo'
date: Mon, 30 Nov 2009 15:27:05 +0000
draft: false
tags: [Senza categoria]
---

Il Papa parla di Aids, scontro sul preservativo

• da **Il Secolo XIX** del 30 novembre 2009

In vista della Giornata mondiale contro l’Aids, il papa ieri all’Angelus ha invitato «a moltiplicare e coordinare gli sforzi» per giungere «a fermare e a debellare questa malattia». Di preservativo, stavolta, non ha parlato, ma tanto è bastato per ricondurre alla memoria le polemiche del marzo scorso. «La diffusione dell’ Aids non si può superare con la distribuzione dei preservativi che, anzi, aumentano i problemi», disse Benedetto XVI sul volo che lo stava portando in Africa, parole che suscitarono accese reazioni da parte di governi e istituzioni. Il parlamento belga giunse ad approvare una risoluzione che definiva «inaccettabili» le parole di Ratzinger. Ieri l’associazione radicale "Certi diritti" è tornata a condannare la posizione della Chiesa, auspicando in una nota «che venga quantificato il danno prodotto da proclami antiscientifici, ispirati da un credo religioso, che alimentano la diffusione dei virus dell’Aids». La Lila, la Lega italiana per la lotta contro l’Aids, ha lanciato la sua campagna di prevenzione il cui slogan sarà "Yes, we condom".