---
title: 'RIUNIONE CERTI DIRITTI ROMA'
date: Mon, 02 Nov 2009 09:01:46 +0000
draft: false
tags: [Senza categoria]
---

Carissimi tutte e tutti,

martedì 3 novembre ci sarà una riunione del coordinamento romano di Certi Diritti.

All’ordine del giorno la discussione sulle iniziative che prossimamenteabbiamo intenzione di mettere in atto per coinvolgere, informare e sensibilizzare i giovani sulle tematiche del mondo gay, bisex e lesbico, anche alla luce della recente recrudescenza degli attacchi omofobi ai danni della nostra comunità e l’assoluta latitanza della  
classe politica su questi temi.

Vi aspettiamo quindi numerosi alle 21.30 in Via Tommaso Grossi 6, zona Colosseo.

Certi Diritti Roma  
Luca “Lucky” Amato

  
[Visualizzazione ingrandita della mappa](http://maps.google.it/maps?f=q&source=embed&hl=it&geocode=&q=Via+Tommaso+Grossi+6&sll=41.889903,12.498398&sspn=0.014408,0.038581&ie=UTF8&hq=&hnear=Via+Tommaso+Grossi,+6,+00184+Roma,+Lazio&t=h&z=16&ll=41.889903,12.498398)