---
title: 'Domani a Roma cerimonia davanti ambasciata ugandese per David Kato'
date: Thu, 27 Jan 2011 16:51:10 +0000
draft: false
tags: [Comunicati stampa]
---

**I FUNERALI DI DAVID KATO KISULE  SI TERRANNO  IN UGANDA A NAMATABA, VENERDI’ 28 GENNAIO, ALLE ORE 14.  
A ROMA, ALLA STESSA ORA, CERIMONIA DAVANTI AMBASCIATA DELL’UGANDA.  
A MILANO DOMANI, APPUNTAMENTO ALLE ORE 17,30 IN PIAZZA DUOMO, VOLANTINAGGIO E FIACCOLATA FINO AL CONSOLATO UGANDESE.**

**A BRUXELLES SI SVOLGERA’ QUESTA SERA A PARTIRE DALLE ORE 18 FIACCOLATA DAVANTI ALLA SEDE DEL MINISTERO DEGLI ESTERI.**

**Comunicato Stampa dell'Associazione Radicale Certi Diritti:**

Roma, 27 gennaio 2011

**Domani, venerdì 28 gennaio, si svolgeranno  in Uganda, a Namataba - Mukono District i funerali di David Kato Kisule, rappresentante del movimento omosessuale ugandese, iscritto all’Associazione Certi Diriti.**

Questa sera a Bruxelles, veglia per David Kato promossa dall'Ong radicale Non c'è Pace Senza Giustizia, davanti alla sede del Ministero degli Esteri, parteciperanno le organizzazioni lgbt e rappresentatni dell'Intergruppo lgbt al Parlamento Europeo.

  
**Alla stessa ora a Roma, davanti alla sede dell’Ambasciata dell’Uganda, in Viale Giulio Cesare, 71, l’Associazione Radicale Certi Diritti, insieme agli amici e compagni radicali e di altre Associazioni e Ong, ricorderanno la figura di David Kato, ucciso dall’odio e dal fanatismo politico e religioso.**

**A Milano, venerdì 28 gennaio, alle ore 17,30 Certi Diritti Milano, insieme ad altre Associazioni, si daranno appuntamento in Piazza Duomo. A seguire si svolgerà una fiaccolata fino alla sede Consolare ugandese.**

David Kato è stato brutalmente assassinato ieri pomeriggio nel suo appartamento a Namataba, Mukono District, vicino a Kampala. La polizia sta ancora facendo delle indagini per capire quante persone sono coinvolte nel brutale pestaggio. David è morto mentre veniva trasportato in ospedale.

foto di Yuri Guaiana