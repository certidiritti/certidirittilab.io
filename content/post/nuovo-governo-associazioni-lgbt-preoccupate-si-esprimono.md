---
title: 'Nuovo governo : associazioni LGBT preoccupate si esprimono'
date: Thu, 20 Feb 2014 13:51:36 +0000
draft: false
tags: [Movimento LGBTI]
---

Comunicato stampa congiunto

Roma, 20 febbraio 2014

Siamo molto molto preoccupati per la discussione in atto sulle nomine dei futuri ministri, in particolare per ciò che riguarda le deleghe alle Pari opportunità e i Diritti, per noi di cruciale importanza.

Abbiamo ben chiaro che le deleghe alle Pari Opportunità e Diritti, di solito poco attraenti per i candidati ministri, sono oggi un piatto alettante poiché, grazie a chi si è succeduto negli ultimi tempi a quella responsabilità , è partito l'impegno concreto di lottare attivamente, e su tanti fronti, contro l'omofobia e la transfobia ; la Strategia Nazionale di Lotta contro l'omofobia, messa a punta dall'UNAR con la collaborazione attiva delle Associazioni LGBT italiane, ne è un importante strumento.

Sappiamo che ci sono forze politiche dentro la maggioranza che appoggerà il nuovo governo che vogliono con determinazione contrastare le azioni positive a sostegno delle persone omosessuali e transessuali, ostacolando qualsiasi progetto che miri a restituire loro visibilità e dignità sociali.

Chiediamo dunque con forza al Presidente del Consiglio incaricato, di non lasciarsi influenzare dalle posizioni arretrate, pericolose, anti europeiste, che intendono mettere in discussione che siamo una Repubblica laica, fondata sull'uguaglianza dei suoi cittadini e sulla non discriminazione. Rammentiamo a Matteo Renzi gli impegni assunti durante le Primarie rispetto alla necessità di legiferare a favore dei nuclei familiari delle persone omosessuali e transessuali .

Confidiamo che il benessere e il rispetto dell'insieme dei cittadini italiani verrà considerato come prioritario dal nuovo governo e che nulla verrà fatto per stoppare o fare arretrare un evidente progresso istituzionale in atto per migliorare non solo la condizione di vita delle persone LGBTQ ma anche creare e rafforzare l'idea di una società inclusiva, rispettosa e aperta.

Fiorenzo Gimelli presidente nazionale Agedo

Flavio Romani presidente nazionale Arcigay

Paola Brandolini presidente nazionale ArciLesbica

Enzo Cucco presidente Certi Diritti

Aurelio Mancuso presidente nazionale Equality Italia

Giuseppina La Delfa presidente nazionale Famiglie Arcobaleno

Porpora Marcasciano presidente nazionale MIT