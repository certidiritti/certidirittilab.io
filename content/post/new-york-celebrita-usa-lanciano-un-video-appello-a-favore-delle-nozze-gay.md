---
title: 'New York: celebrità USA lanciano un video-appello a favore delle nozze gay'
date: Wed, 15 Sep 2010 14:24:57 +0000
draft: false
tags: [diritti civili, MATRIMONIO, Senza categoria, USA, video]
---

  

Riproponiamo un video, segnalato da [queerblog.it](http://www.queerblog.it/post/9002/new-york-celebrita-in-video-che-lanciano-un-appello-a-favore-delle-nozze-gay), in cui personaggi noti statunitensi – tra cui Kyra Sedgwick, il reverendo Al Sharpton e l’attrice Julianne Moore – hanno preso posizione a favore del matrimonio tra persone dello stesso sesso.