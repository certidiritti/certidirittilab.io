---
title: 'Gay iracheno rischia la vita se rimpatriato. Mobilitazione di Ong e Associazioni per salvarlo'
date: Fri, 30 Dec 2011 12:07:22 +0000
draft: false
tags: [Medio oriente]
---

Azad, giovane gay iracheno con il rimpatrio rischia la vita. Certi Diritti partecipa alla campagna per salvarlo.

Roma, 29 dicembre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Decine di Associazioni e Ong sono mobilitate in tutta Europa per salvare la vita ad Azad Hassan Rasol, giovane gay iracheno al quale le autorità norvegesi hanno respinto la richiesta di asilo e si trova ora in grave pericolo.

Anche l’Associazione Radicale Certi Diritti, insieme ad  Amnesty International France, Iraqi LGBT, la Fondazione Massimo Consoli, il Sindacato Europeo dei Lavoratori, GaiaItalia.com, Cinemagay.it, Alba Montori, Gaynews.it, Gayroma, la Rete Evengelica Fede e Omosessualità, l'Associazione Sharazade, aderisce alla campagna promossa dal Gruppo EveryOne per salvargli la vita e ha inviato un appello urgente all’Ambasciatore norvegese in Italia.

Azad,  si è visto respingere la richiesta di asilo dall'Alta Corte di Oslo ed è in attesa di deportazione in patria. Vive in Norvegia con il proprio compagno, dopo aver abbandonato l'Iraq, paese che le principali organizzazioni per i diritti umani considerano uno dei luoghi in cui gay e lesbiche subiscono le più terribili forme di persecuzione, che in molti casi giungono fino alla loro esecuzione sommaria.

E’ evidente che la sentenza della Corte è stata emessa in violazione della Convenzione di Ginevra e di tutte le carte internazionali sulla tutela dei Diritti Umani.

L’appello di Certi Diritti è rivolto, in particolare, al Re di Norvegia Harald V, al Governo norvegese, al Comitato del Premio Nobel per la pace di Oslo, gli Alti Commissari Onu per i Rifugiati e i Diritti Umani e il Commissario Ue per i Diritti Umani. 

Nella lettera inviata viene ricordato che "Se l'atteggiamento ostile al ragazzo e lesivo del suo diritto alla libertà e alla vita dovesse essere mantenuto dalle autorità norvegesi, chiediamo che si mettano in atto procedure umanitarie affinché venga accolto in un altro paese dell'Unione europea, in deroga umanitaria agli accordi di Dublino”.