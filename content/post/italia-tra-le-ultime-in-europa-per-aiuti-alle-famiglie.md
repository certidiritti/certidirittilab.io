---
title: 'Italia tra le ultime in Europa per aiuti alle famiglie'
date: Sun, 06 Feb 2011 13:24:45 +0000
draft: false
tags: [BERLUSCONI, certi diritti, Comunicati stampa, diritto di famiglia, FAMIGLIA, family day, figli, ocse, pil, RADICALI, rovasio]
---

Lo confermano i dati presentati al network europeo 'Città per la famiglia'. Quelli del Family Day stanno zitti. Dopo la propaganda il silenzio.

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti

Roma, 6 gennaio 2011

"Secondo il network europeo "Città per la famiglia" riunito a Parma in questi giorni, i dati del Family database dell'Ocse, diffusi durante i lavori, documentano in modo chiaro che nell'Ue la spesa pubblica pro famiglia incide in media con una percentuale del 2,2 sul Pil. L'Italia invece spende solo l'1,43% per le famiglie con figli ed è relegata al ventunesimo posto in una classifica a 27.

Gli integralisti religiosi, i grandi intellettuali politici, che oggi sono al Governo,  teorici della famiglia del mulino bianco, ci hanno assalito mediaticamente dalla mattina  alla sera, per mesi e anni, sulle loro fantasie ideologiche con manifestazioni di preti, cardinali, parrocchie con slogan sul 'valore universale della famiglia', 'sugli aiuti che il tal Governo avrebbe garantito e assicurato',  sulla famiglia a senso unico, ecc.

Lo scorso novembre il Governo ha fatto persino una Conferenza governativa di tre giorni a Milano, con spese di spot tv, e propaganda ideologica di ogni genere dai costi economici spaventosi. Tutto per arrivare a questi risultati.

Vorremmo sentire da lor signori, preti, politici, cardinali, integralisti di ogni dove, qualche accenno su questi pessimi risultati. E di fronte al vostro silenzio non ci resta che rispondere con la nostra proposta di Riforma del Diritto di Famiglia, depositata in Parlamento, per rendere il nostro paese più moderno".