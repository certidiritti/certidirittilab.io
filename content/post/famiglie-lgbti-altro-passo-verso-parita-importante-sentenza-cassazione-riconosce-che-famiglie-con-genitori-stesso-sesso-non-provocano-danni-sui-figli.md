---
title: 'Famiglie lgbti: altro passo verso parità. Importante sentenza Cassazione riconosce che famiglie con genitori stesso sesso non provocano danni sui figli'
date: Fri, 11 Jan 2013 17:18:51 +0000
draft: false
tags: [Diritto di Famiglia]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti

Roma, 11 gennaio 2012

La prima sezione civile della Corte di Cassazione ha emanato una sentenza importante (la numero 601) che rappresenta un altro passo avanti nella difficile strada verso la piena uguaglianza delle famiglie e delle persone lgbt nel nostro paese.  
Raccogliendo quello che da anni studi indipendenti e autorevoli, ricercatori e istituzioni, hanno documentato - e quello che noi abbiamo sempre sostenuto vivendo con i nostri figli e figlie - la Cassazione ha dichiarato che una coppia formata da persone dello stesso sesso non produce danno sui propri flgli, e questo non può essere motivo per l'allontanamento dei figli stessi da uno dei coniugi in caso di separazione.  
Questa posizione non è nuova da parte dei giudizi italiani, ma è molto importante che vi sia stato un riconoscimento da parte della Corte di Cassazione. Ora vedremo come i Tribunali applicheranno questa sentenza. Ma soprattutto chiediamo ai candidati al prossimo parlamento di prendere nota di questa sentenza, e di agire di conseguenza, facendo sì che Camera e Senato la smettano di non guardare alla realtà dei fatti e comincino invece a operare solo (!) sulla base della stessa.

[**qui il testo della sentenza >**](notizie/comunicati-stampa/item/download/23_572e7edd66bfb261ceccd4200aab7de5)

  
[GENITORE\_GAY\_2013.pdf](http://www.certidiritti.org/wp-content/uploads/2013/01/GENITORE_GAY_2013.pdf)