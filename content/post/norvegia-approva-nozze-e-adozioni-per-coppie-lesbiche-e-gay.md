---
title: 'NORVEGIA APPROVA NOZZE E ADOZIONI PER COPPIE LESBICHE E GAY'
date: Thu, 12 Jun 2008 09:20:59 +0000
draft: false
tags: [Comunicati stampa]
---

Il Parlamento norvegese ha approvato l'11 giugno, a larga maggioranza (84 voti a favore e 41 contrari), la legge che estende l'istituto del matrimonio alle coppie lesbiche e gay e che consente di adottare bambini. La legge prevede anche, per le lesbiche, di ricorrere all'inseminazione artificiale.