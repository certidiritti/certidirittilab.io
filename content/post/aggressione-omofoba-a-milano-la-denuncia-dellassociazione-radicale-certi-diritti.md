---
title: 'Aggressione omofoba a Milano: la denuncia dell''associazione radicale Certi Diritti'
date: Fri, 29 Jun 2012 17:36:32 +0000
draft: false
tags: [Politica]
---

Ieri sera due ragazzi di 23 anni sono stati insultati, minacciati con un coltello e aggrediti con pugni e schiaffi da due coetanei.

Firmare la proposta di delibera di iniziativa popolare per la prevenzione, il contrasto e l'assistenza alle vittime di discriminazione, non solo omofoba, e per le pari opportunità per tutti è il gesto più concreto per combattere l'omofobia e spronare il Consiglio comunale ad agire.

Comunicato stampa dell'Associazione Radicale Certi Diritti

Milano, 29 giugno 2012

Ieri sera due ragazzi di 23 anni sono stati insultati, minacciati con un coltello e aggrediti con pugni e schiaffi da due coetanei sul tram 14 presso la fermata di Lanza, adiacente al teatro Strehler, dove si stava concludendo la XXVI edizione del Festival Mix di cinema gaylesbico.

L'aggressione di ieri notte è particolarmente grave poiché è avvenuta su un mezzo ATM e sottolinea l'importanza che la Giunta attui politiche concrete per combattere l'omofobia nella città con il maggior numero di residenti LGBTI d'Italia. L'Associazione Radicale Certi Diritti, insieme ai Radicali, all'Arcigay di Milano, a Equality e ad altre associazioni sta raccogliendo le firme per una proposta di delibera di iniziativa popolare per la prevenzione, il contrasto e l'assistenza alle vittime di discriminazione, non solo omofoba, e per le pari opportunità per tutti ([http://milanoradicalmentenuova.it/discriminazioni/](http://milanoradicalmentenuova.it/discriminazioni/)). Tra l'altro si chiede di istituire, senza oneri aggiuntivi per il Comune, un ufficio che abbia come compito quello di promuovere e sviluppare specifiche iniziative volte alla rimozione delle cause di discriminazione ed alla prevenzione di atti di violenza, maltrattamenti e discriminazione diretta ed indiretta.

Firmare questa proposta è il gesto più concreto per combattere l'omofobia e spronare il Consiglio comunale ad agire. Per questo l'Associazione Radicale Certi Diritti invita tutti i cittadini ad andare a firmare presso gli uffici comunali di via Larga 12, in tutte le 9 zone o ai banchetti pubblicizzati sul sito www.milanoradicalmentenuova.it.