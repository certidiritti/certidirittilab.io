---
title: 'RETE LENFORD, AVVOCATI IN DIFESA DEI GLBT'
date: Wed, 11 Mar 2009 08:06:27 +0000
draft: false
tags: [Senza categoria]
---

![Rete Lenford](http://www.retelenford.it/sites/ad.sitetest.it/files/logo.jpg)In tutta Italia sono ormai 44 gli avvocati aderenti   
mercoledì 11 marzo 2009 , di [Il Manifesto](http://www.ilmanifesto.it/)

di m.rav.  
È da più di un anno che esiste. Per rispondere al bisogno di informazione e diffusione della cultura e del rispetto dei diritti delle persone omosessuali. Si chiama Rete Lenford - la presidente è Saveria Ricci del foro di Firenze - e si propone di mettere in contatto gli avvocati che si occupano della tutela giudiziaria di persone gay, lesbiche, bisessuali e transessuali. Lenford Harvey era un avvocato giamaicano che si batteva per i diritti delle persone sieropositive: nel 2005 alcuni ladri entrarono nella sua casa, da una foto che lo ritraeva con il suo compagno capirono che era omosessuale, lo trascinarono fuori e lo ammazzarono. Per la Rete (l'avvocatura per i diritti Lgbt) rappresenta un esempio per chi ha deciso di assolvere, anche nel campo della tutela dei diritti delle persone omosessuali, la funzione sociale di sostegno e tutela alle persone discriminate. In tutta Italia sono ormai 44 gli avvocati aderenti e sono copromotori della campagna di «Affermazione civile», lanciata dall'associazione Certi Diritti per ottenere la pubblicazione del matrimonio civile per le coppie dello stesso sesso. Quest'ultima organizzazione (a carattere tematico), nata nell'ambiente radicale, si ritroverà a Bologna il 14 marzo per il secondo congresso nazionale che oltre a fare il punto sulla campagna promuoverà una nuova iniziativa volta al riconoscimento in Italia dei matrimoni o delle unioni civili stipulati nei paesi comunitari.