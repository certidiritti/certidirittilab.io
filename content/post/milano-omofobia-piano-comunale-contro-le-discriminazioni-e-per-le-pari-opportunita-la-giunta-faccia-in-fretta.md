---
title: 'Milano, Omofobia. Piano comunale contro le discriminazioni e per le pari opportunità: la Giunta faccia in fretta.'
date: Thu, 14 Nov 2013 07:59:25 +0000
draft: false
tags: [Politica]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti e del Comitato MilanoRadicalmenteNuova

Milano 14 novembre 2013

Il 4 novembre scorso si è registrato l'ennesimo atto di discriminazione a Milano. Questa volta è toccato alla comunità LGBTI ad essere presa di mira: alcuni passanti hanno infatti gettato bottiglie piene di urina contro dei ragazzi che stavano bevendo un drink fuori da un noto locale gay.

Il 16 maggio scorso il Consiglio Comunale ha approvato la delibera di iniziativa popolare in materia di prevenzione, contrasto e assistenza alle vittime di discriminazione e per la promozione di pari opportunità per tutti che impegna la Giunta a presentare al Consiglio Comunale un Piano comunale contro le discriminazioni e per le pari opportunità per tutti entro sei mesi. Tra due giorni scadrà il termine per la presentazione del Piano e la Giunta rischia di essere inadempiente.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: «il gesto omofobo del 4 novembre scorso è solo l'ultimo di una serie troppo lunga di atti discriminatori non solo ai danni delle persone LGBTI, ma anche di disabili, anziani e migranti. La Giunta non può permettersi ritardi su un tema così importante e delicato. È urgente che presenti il Piano contro le discriminazioni e per le pari opportunità per tutti e che il Consiglio lo approvi in modo da permettere al Comune di incidere concretamente con iniziative di mainstreaming, di formazione e di altro genere coordinate da una specifica struttura individuata. La delibera inoltre indicava tra gli indirizzi prioritari anche quello di consultare le associazioni attive nell'ambito della prevenzione, contrasto e assistenza delle persone vittime di discriminazione sia nella fase di progettazione del piano sia in quella di verifica in itinere dello stesso. Spiace constatare che a 2 giorni dalla scadenza prevista per la presentazione del Piano le associazioni non siano ancora state coinvolte».