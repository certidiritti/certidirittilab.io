---
title: 'TERZO CONGRESSO DI CERTI DIRITTI  A FIRENZE DAI VALDESI 30-31 GENNAIO 2010'
date: Tue, 08 Dec 2009 15:43:55 +0000
draft: false
tags: [Comunicati stampa]
---

Oggetto**: Terzo Congresso nazionale dell’Associazione Radicale Certi Diritti – Firenze, 30-31 gennaio 2009 dai Valdesi.**

Care e cari amici,

sabato 30 e domenica 31 gennaio, terremo a Firenze il III° Congresso dell’Associazione Radicale Certi Diritti.   Sarà un Congresso  molto importante e per questo ci teniamo a fartelo sapere con un buon anticipo. Così da poterti segnare le date.

I lavori si svolgeranno presso la Foresteria Valdese di Firenze, Via dè Serragli, 49 dove si potrà anche dormire in ambienti molto confortevoli e ad ottimi prezzi.

Nei prossimi giorni avremo il programma definitivo dei lavori e lo invieremo insieme ad altre info logistiche e più approfondite considerazioni sulla vita dell’Associazione Radicale Certi Diritti. Comunque maggiori informazioni le potrei anche trovare nel nostro sito ufficiale

[www.certidiritti.it](http://www.certidiritti.it/)

Grazie, a presto,

**Clara Comelli****, Presidente**

**Sergio Rovasio, Segretario**