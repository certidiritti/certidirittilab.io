---
title: 'LITUANIA: LIBERTA'' PER LA COMUNITA'' LGBT. MANIFESTAZIONE A BRUXELLES'
date: Wed, 24 Jun 2009 11:26:06 +0000
draft: false
tags: [Comunicati stampa]
---

CERTI DIRITTI HA PARTECIPATO OGGI A MANIFESTAZIONE A BRUXELLES PER I DIRITTI LGBT IN LITUANIA

Bruxelles, 24 giugno 2009

Certi Diritti ha partecipato oggi alla manifestazione tenutasi a Bruxelles per chiedere il rispetto dei diritti delle persone LGBT in Lituania e per il diritto alla libertà d'espressione.

Il Parlamento lituano ha infatti approvato un progetto di legge che vieta di parlare dell'omosessualità nelle scuole ed in qualunque media che possa essere accessibile ai bambini. La legge "sulla protezione dei minori contro l'effetto negativo dell'informazione pubblica" paragona la presentazione dell'omosessualità sotto una luce positiva alla rappresentazione di violenza fisica o psicologica, all'esposizione di corpi morti o mutilati crudelmente, alle informazioni che possano creare paura o orrore o che incoraggi l'auto-mutilazione o il suicidio. L'intergruppo LGBT al PE ha già scritto ripetutamente alle autorità europee e della Lituania, in particolare appellandosi alla Presidenza della Repubblica Lituana affinché non firmi la legge - che peraltro rischia di essere presentata anche al Parlamento polacco.