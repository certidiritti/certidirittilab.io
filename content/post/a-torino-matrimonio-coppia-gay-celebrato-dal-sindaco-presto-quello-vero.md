---
title: 'A TORINO MATRIMONIO COPPIA GAY CELEBRATO DAL SINDACO: PRESTO QUELLO VERO'
date: Sat, 27 Feb 2010 14:23:39 +0000
draft: false
tags: [Comunicati stampa]
---

**SABATO 27-2 A TORINO IL SINDACO SPOSA 'SIMBOLICAMENTE' UNA COPPIA DI LESBICHE. CERTI DIRITTI E I PARLAMENTARI RADICALI PLAUDONO A QUESTA INIZATIVA E SI AUGURANO CHE PRESTO LA CERIMONIA SI POSSA SVOLGERE IN COMUNE ANCHE GRAZIE ALLA CAMPAGNA DI AFFERMAZIONE CIVILE.**

**Dichiarazione dei parlamentari radicali eletti nel Pd Rita Bernardini, Dontatella Poretti, Marco Perduca e di Sergio Rovasio, Segretario Associazione RAdicale Certi Diritti:**

**

  

Domani, sabato 27 febbraio, a Torino, il Sindaco 'sposerà' simbolicamente un coppia di persone lesbiche che hanno partecipato alla campagna di 'Affermazione Civile' promossa due anni fa in tutta Italia dall'Associazione Radicale Certi Diritti e da Avvocatura lgbt Rete Lenford. Grazie a questa campagna la Corte Costituzionale sarà chiamata a pronunciarsi il prossimo 23 marzo sui ricorsi di alcune delle coppie omosessuaali che hanno chiesto al loro Comune le pubblicazioni per il matrimonio. Anche in Parlamento giacciono proposte e disegni di legge dei parlamentari radicali che da sempre sono impegnati sul fronte del riconoscimento dei diritti civili per le persone lesbiche e gay.

Ringraziamo il Sindaco di Torino per questa inizativa che va nella direzione del superamento delle diseguaglianze e per l'affermazione di leggi di civilità come l'Europa ci chiede".

  


**