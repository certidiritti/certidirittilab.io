---
title: 'PENA DI MORTE: APPROVATA ALL’ONU NUOVA RISOLUZIONE'
date: Wed, 22 Dec 2010 12:18:23 +0000
draft: false
tags: [Comunicati stampa]
---

**PENA DI MORTE: APPROVATA ALL’ONU NUOVA RISOLUZIONE. CRESCE IL FRONTE PRO MORATORIA. OGGI ALLA CAMERA VOTO SULLA MOZIONE ZAMPARUTTI CONTRO L’ESPORTAZIONE DI PENTOTAL NEGLI STATI UNITI.**

Roma, 22 dicembre 2010

**COMUNICATO STAMPA DELL'ASSOCIAZIONE RADICALE** **NESSUNO TOCCHI CAINO**

L’Assemblea Generale delle Nazioni Unite ha approvato ieri sera una nuova Risoluzione a favore di una moratoria universale della pena di morte. E’ la terza dopo la storica risoluzione approvata nel dicembre del 2007 e quella del 2008.

Hanno votato **a favore** in **109** Paesi, **contro 41** mentre **35** si sono **astenuti** (altri **7** Paesi erano **assenti** al momento del voto). Si è registrato **un deciso passo avanti rispetto al 2007** quando in Assemblea plenaria i voti a favore furono 104, i contrari 54 e le astensioni 29 (più 5 assenti al momento del voto). Un ulteriore passo avanti è stato compiuto anche rispetto al secondo voto sulla Risoluzione pro moratoria avvenuto nel dicembre 2008, quando si espressero 106 a favore, 46 contro e si astennero 34 Paesi (altri 6 erano assenti al momento del voto).

**Il dato politico più significativo** del voto al Palazzo di Vetro è il **voto favorevole di 6 Paesi** che nel 2008 avevano votato contro (**Kiribati**, **Maldive** e **Mongolia**) o si erano astenuti (**Bhutan**, **Guatemala** e **Togo**) e il **voto di astensione di 4 Paesi** (**Comore, Nigeria, Isole Salomone** e **Tailandia**) che nel 2008 avevano votato contro. Sono aumentati anche i cosponsor della Risoluzione, in totale 90, tre dei quali lo hanno fatto per la prima volta: **Cambogia**, **Russia** e **Madagascar**.

L’unica **novità nel testo** della nuova risoluzione riguarda la richiesta – in parte contenuta nel testo del 2007 – rivolta agli Stati membri di ‘**rendere disponibili le informazioni rilevanti circa l’uso della pena di morte** al fine di favorire **un dibattito nazionale informato e trasparente**’.

**“Il nuovo voto al Palazzo di Vetro a favore della moratoria registra l’evoluzione positiva in atto da oltre dieci anni nel mondo verso la fine dello Stato-Caino e il superamento del fasullo e arcaico principio dell’occhio per occhio,” ha dichiarato Sergio D’Elia, Segretario di Nessuno tocchi Caino.**

Intanto è previsto oggi alla Camera dei Deputati il voto sulla **mozione presentata dalla Deputata Radicale Elisabetta Zamparutti** \- sottoscritta da esponenti di tutti i gruppi politici - che **impegna il governo a controllare che il Pentotal prodotto in Italia dalla casa farmaceutica Hospira di Liscate non finisca nei penitenziari americani per la pratica dell'iniezione letale**.