---
title: 'CERTI DIRITTI SI PREPARA PER IL GENOVA PRIDE'
date: Sun, 21 Jun 2009 09:03:57 +0000
draft: false
tags: [Comunicati stampa]
---

Certi Diritti si prepara al Pride di Genova, e per questo vi ripropone le immagini di Bologna 2008, con una piccola aggiunta. Mentre vedrete le decine di coppie che erano venute e promettersi un amore civile sul nostro carro, ascolterete, in sottofondo, una telefonata con l'ufficio matrimoni del comune di Genova...

Un mix di pregiudizi burocratici, ma anche gentilezza e comprensione da parte di chi, cittadino come noi, comprende le nostre istanze. Un motivo in più per partecipare all'iniziativa di affermazione civile.

  

**A:** Ufficio matrimoni,  buongiorno

**Andrea Conte:** Buongiorno, mi chiamo Andrea Conte e volevo delle informazioni per il matrimonio civile

**A:** Cittadini Italiani entrambi?

**Andrea Conte:** Si

**A:** Solo matrimonio civile?

**Andrea Conte:** Sì

**A:** Ok, uno dei due viene qua da noi, in corso  Torino 11 al  secondo piano, la stanza 209 e manifesta la volontà di contrarre matrimonio.

**Andrea Conte:** Io ho intenzione di venire con il mio uomo per chiedere la pubblicazione degli atti

**A:** No, aspetti un secondo... No... eh no, la legislazione italiana non lo prevede ancora

**Andrea Conte:** Ma mi sa dire in quale articolo, perché...

**A:** Guardi le passo la funzionaria dell'ufficio

**B:** Pronto? Pronto?

**Andrea Conte:** Pronto buongiorno. mi chiamo Andrea Conte e sto chiedendo informazioni per la pubblicazione degli atti perché vorrei presentarmi con il mio uomo e ovviamente ...

**B:** Certo, però in italia non è possibile

**Andrea Conte:** OK

**B:** Le dico, è una domanda che mi lascia un attimino perché... sinceramente, è ovvio... lei deve capire che è la prima volta che ci chiedono una cosa del genere

**Andrea Conte:** La prima volta?

**B:** Nel senso che non è mai capitato.

**Andrea Conte:** Ah!

**B:** Per cui non so dirle espressamente la norma.

**Andrea Conte:** Mi stupisce di essere stato il primo a farle una domanda del genere

**B:** Sì, sinceramente sì. Mi posso informare eventualmente su... magari se vuole richiamare,

**Andrea Conte:** Volentieri

**B:** Non c'è nessun problema, ecco.

**Andrea Conte:** L'articolo 93 sulla pubblicazione non dice che devono presentarsi un uomo con una donna

**B:** Eh, però a Genova le posso garantire che è la prima domanda che... in questo senso che ci viene rivolta. L'evoluzione dei tempi è chiaro che c'è, però non abbiamo ancora ben la situazione sottomano, ecco.

**Andrea Conte:** Io le sarei veramente grato se lei mi facesse questa cortesia. Quand'è che potrei richiamarla?

**B:** Magari mi mandi una mail.

**Andrea Conte:** Volentieri

**B:** Se mi manda una mail poi io le rispondo.

**Andrea Conte:** Sì, mi dice la mail per cortesia?

**B:** Sì, allora: [matrmioni@comune.genova.it](mailto:matrmioni@comune.genova.it)

**Andrea Conte:** Va bene. E... all'attenzione di?

**B:** Io sono Isola. Se vuole sapere il cognome sono Isola.