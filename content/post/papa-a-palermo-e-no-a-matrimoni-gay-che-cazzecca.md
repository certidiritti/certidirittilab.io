---
title: 'PAPA A PALERMO E NO A MATRIMONI GAY: CHE C''AZZECCA?'
date: Sun, 03 Oct 2010 11:21:31 +0000
draft: false
tags: [Comunicati stampa, Matrimoni gay, palermo, PAPA]
---

**PAPA A PALERMO CON MANIFESTI CONTRO MATRIMONIO GAY: CHE C’AZZECCA? FORSE  NON SANNO CHE IL MATRIMONIO GAY IN ITALIA NON C’E’, MEGLIO SE SCRIVEVANO ‘NO ALLE BESTEMMIE’.**

Roma, 3 ottobre 2010

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

“I grandi eroi dell’Anno Domini 2010 della Giovane Italia, movimento giovanile del Pdl, che in occasione della visita del Papa a Palermo hanno affisso la città di manifesti  con la scritta ‘no ai matrimoni gay’ forse non sanno che il matrimonio gay in Italia è ancora impedito a differenza del Portogallo, Spagna, Belgio, Olanda e altri paesi. Diciamo che questi grandi eroi dei nostri giorni, così acuti, impegnati, intelligenti e molto, ma molto coerenti, avrebbero forse dimostrato più vicinanza al Papa se avessero affisso manifesti con su scritto ‘no alle bestemmie’. Ma questa è l’Italia più fedele al Papa di oggi, bestemmiatrice per bocca del suo Premier e impegnata a lottare per dire no ad una legge che non esiste”.