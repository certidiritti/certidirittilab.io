---
title: 'A TORINO QUALCOSA SI E'' MOSSO GRAZIE AD AFFERMAZIONE CIVILE'
date: Fri, 11 Dec 2009 07:22:10 +0000
draft: false
tags: [Comunicati stampa]
---

seminare e raccogliere

Ho letto con calma commenti e riflessioni sull'affermazione civile di Antonella e Deborah e sulle dichiarazioni di Chiamparino, e son molto contento. Si, molto, perché ritengo che Antonella e Deborah abbiamo colto fino in fondo lo spirito di affermazione civile che certi diritti e rete lenford hanno lanciato ormai un paio di anni fa, e che con la loro determinazione e il loro coraggio hanno reso feconda di risultati politici.

Per chi omosessuale non è il coraggio rischia di non essere evidente. Ed anche coloro che guardano con sufficienza alle iniziative per il riconoscimento del diritto delle persone omosessuali ad accedere all'istituto matrimoniale, in questi giorni hanno potuto riflettere sulle battute, sulle superficialità, sull'omofobia sopita dei tanti che in autobus o nei locali hanno commentato la notizia. E non parlo delle dichiarazioni di coloro che sono contrari alle dichiarazioni di Chiamparino, che parlano da sole...

Le dichiarazioni di Chiamparino non devono essere sopravvalutate, ovviamente, ma da convinto riformista quale sono ritengo che "another break in the wall" è stato realizzato.

Affermazione civile è proprio questo: uomini e donne che con le proprie vite e la propria faccia chiedono che i diritti siano tali (siano "certi" ) per tutti e tutte, senza distinzione. Ed anche se in molti dei commenti anche di persone a noi vicine, questo elemento della vicenda è rimasto nella penna, credo che non sia superfluo, né autocelebrativo, sottolineare che affermazione civile sta dando i suoi frutti, prima con i due rinvii alla corte costituzionale e poi con le storie come quella di Antonella e Deborah. Piccoli passi, certo, ma che nel vuoto della politica di questi tempi, hanno una eco forte di speranza.

Si semina in pochi, si raccoglie in molti: è nella natura delle cose, anche di quelle politiche. Ma se aumentasse il numero di coloro che seminano, il numero cioè di uomini, donne e associazioni che sostengono certi diritti e rete lenford in questa iniziativa, i frutti che tutti e tutte potranno raccogliere non potranno che aumentare.

Enzo Cucco