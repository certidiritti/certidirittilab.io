---
title: 'Sul giornale le foto di cento gay "Condannateli all''impiccagione"'
date: Mon, 18 Oct 2010 04:59:27 +0000
draft: false
tags: [Senza categoria]
---

![Sul giornale le foto di cento gay "Condannateli all'impiccagione"](http://www.repubblica.it/images/2010/10/14/185856996-2b39a762-9995-4509-99c3-4f492617bf95.jpg "Sul giornale le foto di cento gay "Condannateli all'impiccagione"")

### "Rolling Stone" addita alla pubblica delazione nomi e indirizzi di un folto gruppo di leader omosessuali e di attivisti per i diritti civili. La protesta di alcune associazioni radicali europee: "La Ue intervenga". Il governo di Kampala chiude la testata

**ROMA** \- Cento leader gay con foto e indirizzi additati al pubblico ludibrio e con l'invito a denunciarli per una possibile condanna a morte. Accade in Uganda, dove il giornale ["Rolling Stone" 1](http://www.boxturtlebulletin.com/2010/10/04/27002) nel suo numero di Ottobre pubblica un articolo intitolato "Divulgate 100 Foto Dei Leader Gay Ugandesi" sollecitando "l'impiccagione degli  
omosessuali". Le immagini di presunti omosessuali, lesbiche e attivisti di diritti umani sono correlate da nomi, posizione professionale, descrizione della vita privata, indirizzo di casa e del posto di lavorone dell'abitazione sia privata che lavorativa. Il governo [ugandese è già intervenuto e ha fatto chiudere la testata.  
2](http://www.boxturtlebulletin.com/2010/10/14/27302)  
La denuncia viene da due associazioni radicali: "Non c'è pace senza giustizia" e "Certi diritti" che parlano di gesto "Incompatibile con il principio dello Stato di diritto. La libertà di stampa non può essere tradotta nella mancanza di limiti". In particolare in un Paese in cui una proposta di legge propone di punire l'omosessualità con la pena capitale  
  
La questione sta sollevando proteste dentro e fuori il Paese africano già duramente colpito dalle stragi dello scorso decennio. I gruppi radicali di cui sopra chiedono all'opinione pubblica mondiale e alle organizzazioni internazionali di prendere posizione contro "l'infame campagna orchestrata" da Rolling Stone e fanno pressione affinché Rolling Stone porga immediatamente scuse e che si impegni a non proseguire con la pubblicazione di questi articoli. Appello anche al Parlamento Europeo perché "intervenga contro questa vergognosa e criminale campagna mediatica". Chiamato in causa, il governo di Kampala è intervenuto facendo chiudere la testata e scatenando la protesta dei giornalisti in nome di una malintesa "libertà di stampa".

(14 ottobre 2010)