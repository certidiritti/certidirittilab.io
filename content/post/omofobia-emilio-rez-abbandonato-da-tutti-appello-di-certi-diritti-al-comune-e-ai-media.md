---
title: 'OMOFOBIA, EMILIO REZ ABBANDONATO DA TUTTI. APPELLO DI CERTI DIRITTI AL COMUNE E AI MEDIA'
date: Wed, 06 Oct 2010 13:20:26 +0000
draft: false
tags: [Comunicati stampa]
---

La vicenda del ragazzo gay cacciato dalla casa in affitto non è considerata urgente dal tribunalee. L'udienza davanti al giudice rinviata ad aprile 2011.

  
Roma, 6 ottobre 2010

Comunicato Stampa dell’Associazione Radicale Certi Diritti.

Lo scorso 10 giugno, Emilio Rez, un ragazzo gay di 25 anni, cantautore, mentre rientrava nel suo appartamento in affitto nella zona del Pigneto a Roma, si era accorto che la serratura  della sua casa era stata cambiata. Poco dopo si accorse che in fondo alle scale v’erano sei sacchi neri contenenti suoi effetti personali.  
  
Emilio Rez, che nei giorni precedenti questo atto era stato gravemente minacciato e insultato dai membri della famiglia proprietaria dell’appartamento con frasi del tipo: “Sei un frocio di merda, sei un farabutto… con noi caschi male” e altro, aveva immediatamente presentato denuncia ai Carabinieri per i reati di violazione di domicilio, appropriazione indebita e minacce.  
  
Ieri in Tribunale, alla presenza dell’Avvocato Massimo Vita, che difende Emilio,  c’è stata la prima udienza alla quale i proprietari dell’appartamento non si sono nemmeno presentati, inoltre il giudice ha rinviato l’udienza al 28 aprile 2011. Intanto Emilio Rez continua a rimanere senza casa e senza i suoi effetti personali, aiutato da amici e totalmente abbandonato dal Comune che aveva detto di voler intervenire sulla vicenda.  
  
La nostra Associazione chiede al Comune e ai media di intervenire per garantire tutela e assistenza a coloro che hanno il coraggio di denunciare soprusi e violenze, in questo caso di tipo omofobico. Francamente, sapere che Emilio Rez potrà forse sperare in un intervento del Giudice alla fine dell’aprile 2011, per un atto di prepotenza subito nel giugno 2010, rende bene l’idea in quale situazione di rischio e pericolo vivono le persone lgbt in questo paese.  
  
Certi Diritti  
Ufficio stampa Tel. 06-65937036