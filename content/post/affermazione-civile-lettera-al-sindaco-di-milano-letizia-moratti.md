---
title: 'AFFERMAZIONE CIVILE: LETTERA AL SINDACO DI MILANO LETIZIA MORATTI'
date: Fri, 06 Nov 2009 07:48:18 +0000
draft: false
tags: [Comunicati stampa]
---

di **Piero Bellotto** su [www.tomblog.it](http://www.tomblog.it/?p=2725)

Gentile Letizia Moratti, Sindaco di Milano,

oggi, 3 novembre 2009, io e il mio fidanzato (e altre due coppie, e un legale della Rete Lenford) ci siamo recati in comune presso il vostro ufficio Pubblicazioni Matrimoniali, in via Larga 12, al fine di richiedere, per l’appunto, tale pubblicazione \[continua\]

Prima di scendere nel dettaglio di ciò che è avvenuto, ci terrei a ricordarle ciò che enuncia l’articolo 3 della Costituzione Italiana che dichiara testualmente:  
**“Tutti i cittadini hanno pari dignità sociale e sono eguali davanti alla legge, senza distinzione di sesso, di razza, di lingua, di religione, di opinioni politiche, di condizioni personali e sociali. È compito della Repubblica rimuovere gli ostacoli di ordine economico e sociale, che, limitando di fatto la libertà e l’eguaglianza dei cittadini, impediscono il pieno sviluppo della persona umana e l’effettiva partecipazione di tutti i lavoratori all’organizzazione politica, economica e sociale del Paese.”**

Brevemente: una deliziosa signorina ha accolto la novella coppia, con un gran sorriso chiedendo “chi di voi due si sposa e con chi?” e alla risposta “noi due con noi stessi” il sorriso si è raggelato in un ghigno satanico, ma pur sempre estremamente gentile.  
Dopo brevissima consultazione col capo ufficio, ci ha informato che non avremmo nemmeno potuto far richiesta di pubblicazione, in quanto sprovvisti dei requisiti necessari.

Ad ogni modo la sua collega (che esclamava nel frattempo: che bello! Buona fortuna!) ci ha suggerito di far domanda in carta libera all’ufficio Protocollo.  
Usciti dall’ufficio nel quale ci sono stati negati -in quanto rei d’omosessualità- i nostri elementari diritti di cittadino italiano che vota e paga le tasse, ci siamo confrontati con le altre coppie alle quali era stata detta la medesima cosa.  
_Nota a margine: durante questa breve consultazione tra coppie ed avvocato, un dipendente comunale si è premurato di farci sapere (ripetutamente) che la suoneria del suo telefonino era “Faccetta Nera”, nota canzoncina dei bei tempi perduti del fascio._

Così il variopinto gruppetto si è spostato ai piani bassi, all’ufficio protocollo e, dopo non poco penare, è riuscito a depositare finalmente questa domanda di richiesta per poter accedere al servizio di Pubblicazioni Matrimoniali.

Letizia, vorrei porre la sua attenzione su un esempio banale ma molto significativo: se a lei, in quanto donna, venisse negato il Diritto Costituzionale di accedere al voto, come si sentirebbe?

Ecco, esattamente nello stesso modo mi sento io stamane, violato nella mia dignità umana, personalmente offeso dai suoi dipendenti e soprattutto arrabbiato perché:  
1) non solo mi viene negato il diritto di sposarmi, ma l’atteggiamento dei suoi dipendenti, viola l’articolo 3 della costituzione, rendendo **ANTICOSTITUZIONALE L’OPERATO DELL’ANAGRAFE DI MILANO**  
2) oltre al diritto di sposarmi mi viene negato anche il mio diritto di dignità, sminuendo la mia persona, nonostante –come già sottolineato- io sia Cittadino Italiano incensurato, paghi le tasse ed eserciti il diritto/dovere di voto  
3) naturalmente a tutto ciò si sommano tutti gli altri diritti correlati ai quali non posso accedere: si va dalla licenza lavorativa matrimoniale per arrivare al diritto di successione, alla responsabilità sanitaria… la lista è lunga, caro Sindaco

La prego quindi di dar seguito, nei tempi previsti dalla legge, alla richiesta di pubblicazioni depositata dalla nostra coppia e dalle altre coppie facenti parte dell’associazione [“Certi Diritti”](../../undefined/).  
La prego inoltre di ricordare ai suoi dipendenti che l’intimidazione è un reato punibile per legge.

Cordialmente

Un cittadino che paga le tasse.