---
title: 'ANNIVERSARIO BRECCIA DI PORTA PIA: INVITO A MARCIA ANTICLERICLAE 19-9'
date: Fri, 11 Sep 2009 12:07:33 +0000
draft: false
tags: [Comunicati stampa]
---

Di seguito il testo della lettera di invito per la Marcia Anticlerciale del 19 settembre 2009 in occasione dell'anniversario della Breccia di Porta Pia del XX Settembre 1870.

Cara/caro,  
sabato 19 settembre 2009, le Associazioni radicali Anticlericale.net e Certi Diritti, con l’adesione e la partecipazione di Radicali Italiani, Associazione Luca Coscioni, Nessuno Tocchi Caino, Associazione per il Divorzio Breve, Radicali Roma, Comitato Ernesto Nathan, Agorà Digitale, hanno promosso la Marcia Anticlericale per commemorare la data del XX Settembre 1870, storica data della Breccia di Porta Pia.  
           

La Marcia Anticlericale di sabato 19 settembre si svolgerà come segue:  
-         ore 14 appuntamento a Porta Pia;  
-         ore 14.30 interventi dal palco dei promotori della Marcia, di parlamentari e dirigenti radicali, esponenti e rappresentanti politici che parteciperanno alla Marcia;  
-         ore 16 partenza da Porta Pia > Via XX Settembre > Quirinale – Palazzo della Consulta > Via IV Novembre > Via del Plebiscito > Ghetto ebraico > P.za Cairoli > P.za Giuditta Tavani Arquati > Campo de’ Fiori > Corso Vittorio > Piazza Pio XII (confine italiano con il Vaticano).  
   
Anticlericale.net da sabato mattina alle ore 10.30, terrà presso la sede di Via di Torre Argentina, 76 – Roma il suo V°  Congresso fino alle ore 13.30 per poi riprendere i lavori domenica 20 settembre dalle ore 10 fino al primo pomeriggio.  
L’Associazione Radicale Certi Diritti terrà domenica, dalle ore 10.30 alle ore 18 circa, la riunione del suo Direttivo (vicino alla sede dei radicali) aperta a tutti coloro che vorranno partecipare.

Maurizio Turco  
Deputato radicale, Presidente di Anticlericale.netClara Comelli  
Presidente dell’Associazione Radicale Certi DirittiCarlo Pontesilli  
Segretario Anticlericale.netSergio Rovasio  
Segretario dell'Associazione Radicale Certi DirittiMichele De Lucia  
Tesoriere di Anticlericale.net e di Radicali Italiani

                                  
P.S. Di seguito elenchiamo il significato storico delle tappe della Marcia Anticlericale di sabato 19 settembre 2009:  
   
Porta Pia  
alle ore 10,10 del XX Settembre 1870 il primo bersagliere irrompe attraverso la breccia di Porta Pia.  
Via XX Settembre;  
In molte città Italiane le vie dedicate alla data del XX settembre partono quasi sempre dal duomo della città; ciò serviva a rimarcare la vittoria della Stato laico sulla chiesa.  
Quirinale  e Palazzo della Corte Costituzionale  
prima del 1870 residenza dei papi (anche se già dal ritorno dall’esilio di Gaeta dopo i moti del 1848, il Papa aveva scelto il Vaticano come luogo di residenza); il Palazzo della Consulta ha quella sede dal 1955. Fino al 1870 era la sede del Tribunale della sacra consulta e del corpo delle guardie nobili;  
Via XXIV Maggio – Via IV Novembre – P.za Venezia  
Via del Plebiscito, via dedicata al Plebiscito del 2 ottobre 1870.  
« Vogliamo la nostra unione al Regno d'Italia, sotto il governo del re Vittorio Emanuele II e dei suoi successori » . Il plebiscito si svolse il 2 ottobre 1870. I risultati videro la schiacciante vittoria dei sì, 40.785, a fronte dei no che furono solo 46. Il risultato complessivo nella provincia di Roma fu di 77.520 "sì" contro 857 "no". In tutto il territorio annesso i risultati furono 133.681 "sì" contro 1.507 "no".  
Ghetto ebraico, luogo di segregazione, fino al 1870.  
Un muro circondava la zona e cinque cancelli su di esso erano aperti soltanto durante il giorno per permettere agli ebrei di uscire per esercitare i due soli mestieri a loro consentiti: vendita ambulante di stracci e prestiti ad usura, sempre indossando, però lo speciale contrassegno di colore giallo a dimostrazione dell'appartenenza al popolo ebraico.  
I cancelli erano sorvegliati da una sentinella (la cui remunerazione era a carico degli ebrei); nessun ebreo poteva allontanarsi dal ghetto di notte se non voleva essere gravemente punito. Il ghetto fu demolito nel 1870 con l'unificazione d'Italia e la perdita del potere temporale dei Papi.  
Piazza Benedetto Cairoli  
Benedetto Cairoli è stato un politico italiano. Fu garibaldino, rifugiato politico e cospiratore anti-austriaco, deputato al Parlamento, Presidente del Consiglio dei Ministri italiano nei periodi 24 marzo 1878 - 19 dicembre 1878 e 14 luglio 1879 - 29 maggio 1881  
Il prestigio del Cairoli fu grande, anche in quanto rifletteva i meriti dei quattro fratelli, tutti caduti nelle guerre risorgimentali: il padre morto in esilio, Ernesto morto tra i Cacciatori delle Alpi, Luigi morto a Cosenza, di tifo, durante la Spedizione dei Mille, Enrico morto allo Scontro di villa Glori il 23 ottobre del 1867, Giovanni morto per le ferite riportare a villa Glori.  
Trastevere Piazza Giuditta Tavani Arquati e Via della Lungaretta 97.  
La mattina del 25 ottobre 1867, giorno in cui Garibaldi prendeva Monterotondo nel corso della terza spedizione per liberare Roma, una quarantina di patrioti, di cui 25 romani, si riunirono in via della Lungaretta 97, nel rione romano di Trastevere, nella sede del lanificio di Giulio Ajani, per decidere sul da farsi. Il gruppo preparò una sommossa per far insorgere il Roma contro il governo di Pio IX. Deteneva delle cartucce e un arsenale di fucili. Alla riunione partecipò anche la Arquati, con il marito e uno dei tre figli della coppia, Antonio. Verso le 12 e mezzo, una pattuglia di zuavi giunta da via del Moro attaccò la sede del lanificio. I congiurati cercarono di resistere al fuoco. In poco tempo, però, le truppe pontificie ebbero la meglio e riuscirono a farsi strada all'interno dell'edificio. Alcuni congiurati riuscirono a fuggire, mentre altri furono catturati. Sotto il fuoco rimasero uccise 9 persone, tra cui Giuditta Tavani Arquati, incinta del quarto figlio, il marito e il loro giovane figlio.  
   
Campo dè Fiori (passando da Piazza Benedetto Cairoli)  
Luogo simbolo degli anticlericali. Vi fu arso vivo Giordano Bruno il 17 febbraio 1600, condannato dall’Inquisizione perché considerato eretico.   Sin dal 1876 un comitato studentesco aveva iniziato la sottoscrizione per un monumento in onore del filosofo nolano, martire del libero pensiero, raccogliendo col tempo adesioni prestigiose, tra cui quelle di Giosuè Carducci, Ernest Renan, Ferdinand Gregorovius, Victor Hugo, Michail Bakunin, George Ibsen.  
Il pontefice minacciò di abbandonare Roma per rifugiarsi nella cattolica Austria, qualora la statua fosse stata scoperta al pubblico. Finalmente il 9 giungo 1889 venne inaugurato a Campo de' Fiori il monumento dedicato a Giordano Bruno. Alla base del monumento si legge un'iscrizione del filosofo Giovanni Bovio, oratore ufficiale della cerimonia di inaugurazione: "A Bruno, il secolo da lui divinato qui dove il rogo arse".  
Corso Vittorio  
Ponte Sant’Angelo – Castel Sant’Angelo  
Luogo di prigionia e di esecuzioni capitali per opera del potere pontificio  
Piazza Pio XII  
Area di confine tra lo Stato italiano e il Vaticano