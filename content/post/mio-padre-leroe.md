---
title: 'Mio padre, l''eroe'
date: Sat, 30 Apr 2011 09:56:27 +0000
draft: false
tags: [Medio oriente]
---

Pensieri sulla vita e l'universo di una lesbica siriana. Articolo pubblicato su damascusgaygirl.blogspot.com. Traduzione di Alba Montori.  
  
Abbiamo ricevuto una visita dei servizi di sicurezza: era tarda notte, nelle ore piccole. Tutti erano profondamente addormentati. Mi sono svegliata quando ho sentito il rumore e subito ho intuito cosa stava succedendo.  
  
Abu Ali, il nostro portiere, era stato svegliato da qualcuno che suonava alla porta, e stava barcollando lì a guardare fuori, pensava che tutti fossero dentro, ma non sarebbe stata la prima volta che qualche membro della famiglia oppure qualcun altro fosse rimasto fuori troppo per un caffè in ritardo ... invece, ha visto due ragazzi giovani, di circa venticinque anni, vestiti in giacca di pelle nera, muscolosi, che stavano fumando. Egli ha capito subito chi fossero e ha suonato l'allarme per svegliare tutti. E' rimasto sorpreso quando hanno dichiarato che erano qui per ... me ...  
  
Intanto io appena mi sono svegliata ho avuto una idea piuttosto precisa di quello che era successo esattamente . Così, mi sono infilata nei miei vestiti più in fretta che potevo - quelli che avevo preparato per un momento simile; biancheria intima di cotone semplice e t-shirt (senza reggiseno o qualcosa di simile), jeans, pullover largo sopra ... ho messo gli occhiali e mi son precipitata giù nel cortile.  
  
Mio padre era già là fuori, lui non si era preoccupato di vestirsi del tutto e indossava solo una camicia da notte. Era già lì discutendo animatamente con loro. Quando sono comparsa, uno di loro ha fatto un cenno: "E 'lei"  
  
"Io?"  
"Sì ... abbiamo bisogno di discutere alcune cose con te." Sorriso oleoso che non si estende agli occhi.  
"Cose come?"  
Snocciola un lungo elenco di cose che ho postato in inglese e in arabo.  
"Potreste esservene perse un paio." dico io, i miei nervi sono quasi sempre la parte peggiore di me, ma anche se non sono completamente sveglia, mi obbligo a non sentire la voglia di fuggire. Se corro, so che spareranno. Posso vedere le armi, il rigonfiamento delle pistole e dei coltelli probabilmente sotto la giacca.  
"Ne abbiamo già abbastanza", dice quello "cospirazione contro lo Stato, invito alla rivolta armata, collaborazione con elementi stranieri".  
""Uh huh, quali?"  
"La trama salafita", l'altro dice, il suo accento indica che viene direttamente da un villaggio della Ansariya Jebel. "Fare trame settarie".  
"Davvero? " mio padre interrompe. "Mia figlia è una salafita?" Comincia a ridere. "Ma guardala: non vedi che è ridicolo.. Lei non si copre nemmeno più ... e se avete davvero letto neanche la metà di quel che ha scritto, sai quanto sia ridicolo !  
Quando è stata l'ultima volta che hai sentito un wahhabita, o anche qualcuno della fratellanza dire che indossare l'hijab è una scelta della donna soltanto? "  
Fa una pausa, quelli non dicono niente.  
  
"Non credo proprio, "va avanti ". Quando è stata l'ultima volta che hai visto uno di quelli scrivere che la religione non dovrebbe  essere la religione di stato?"  
Ancora niente.  
  
"Quando è stata l'ultima volta che li avete visti andare dicendo che ai gay dovrebbe essere consentito il diritto di sposarsi, un uomo con un uomo o una donna con una donna?"  
Niente  
"E visto che non rispondete, voi vedete bene", lui dice, "che non avete alcuna ragione di prendere mia figlia."  
  
Quelli stanno zitti. Poi uno sussurra qualcosa all'altro, sorride  
"Uh huh," dice quello, "così tua figlia ti dice tutto, eh?"  
"Certo", dice mio padre  
"Ti ha detto che le piace andare a letto con le donne?" e sogghigna, veleno puro, sentendosi di aver messo a segno un bel colpo. "Che lei è una di quelle lesbiche che si scopa le bambine?" (le parole arabe che usa lui sono molto più crude ...ma tanto per dare l'idea)  
Mio padre mi guarda. Annuisco, ci capiamo  
"Lei è mia figlia", dice, e posso vedere la rabbia crescente nei suoi occhi, "e lei è quello che è e se la vuoi, devi prendere pure me."  
"Stupido cittadino stronzo" dice lo stesso ragazzo "Tutti voi finocchi ricchi siete gli stessi. Non c'è da meravigliarsi se questa finisce per scopare le ragazze e gli ebrei" (di nuovo, in arabo è molto più grezza,,,)  
Fa un passo avvicinandosi a me e mette la sua mano sul mio petto  
"Forse se tu stessi con un vero uomo", mi sogghigna "la piantavi con queste sciocchezze e bugie, forse dovremmo fartelo vedere ora e lasciare che la tua checca di paparino guardi così capisce come sono gli uomini veri."  
  
Sto quasi tremando di rabbia. Mio padre muove leggermente la testa per dirmi di tacere.  
  
"Che cosa sei?" esclama "Lo sciacallo ha dormito con la scimmia per concepirti? Quali sono i vostri nomi?"  
Loro glieli dicono. Lui annuisce  
  
"Tuo padre", dice a quello che ha minacciato di stupro me "lo sa quello che fai? Lui era un ufficiale, vero? e ha servito in ..." ( cita esattamente e poi si gira verso l'altro) "e tua madre? non era la figlia di ...?"  
Rimangono entrambi con gli occhi sgranati sì, proprio così  
"Che cosa potranno pensare, se sentissero di come vi comportate? E mia figlia? Lasciate che vi dica questo di lei; che ha fatto molte cose che, se fossi stato in lei, io non avrei fatto. Ma non ha mai smesso di essere mia figlia una volta e se io ci sarò non si darà mai il caso che vi permetta di farle del male. Voi non la porterete via da qui.  E, se ci provate, sappiate che le anime dei suoi antenati vi stanno guardando. Sapete qual è il nostro cognome? Davvero? Allora sapete dove ci trovavamo quando Maometto, la pace sia su di lui, è andato a Medina, sapete chi è stato a liberare Al Quds, sapete anche, forse, che il mio, di paparino, ha combattuto per salvare questo paese dagli stranieri e chi era, sapete chi sono stati i miei zii e i miei fratelli ... e se questo non vi fa vergognare a sufficienza, conoscete i miei cugini... e ci fermiamo qui...  
  
"Lasciatela stare e dite al resto della vostra banda di lasciarla stare,  e vi dirò qualcosa ora perché penso che forse siate troppo stupidi per capirlo da soli,  sì siete alawiyeen,.. .. non negatelo, lo so io e pure voi. Noi siamo sunniti. Voi lo sapete bene. E nei vostri uffici e nei vostri villaggi vi dicono sempre che tutti voi dovete stare spalla a spalla adesso, perché stiamo arrivando per voi il più presto possibile e vi serviremo come hanno servito i nostri nella terra dei due fiumi. Così voi siete spaventati. Lo sarei anch'io.  
  
"Allora venite qui a prendere Amina. Lasciate che vi dica qualcosa anche se non è lei che dovete temere,.. Lei dovreste ricolmarla di  elogi, lei e le persone come lei Sono quelli dicendo Alawi, sunniti, Arabi, Kurdi. , duruzi, cristiano, ognuno è lo stesso e sarà pari nella nuova Siria, sono quelli che, se la rivoluzione viene, sarà la salvezza di tua madre e le tue sorelle Sono loro lotta contro il wahhabita più seriamente, idioti.. sono, però, che serve loro da dire 'ogni sunnita salafita è, ogni manifestante è salafita, ognuno di loro è un nemico', perché quando fai così lo fai così.  
  
Anche se lei non è quello che si deve temere,.. dovreste essere colmi di lodi per lei e per persone come lei Qui c'è chi viene chiamato Alawi, sunniti, Arabi, Kurdi. , duruzi, cristiano, ognuno è lo stesso e sarà pari nella nuova Siria, sono quelli che, se la rivoluzione viene, sarà la salvezza di vostra madre e delle vostre sorelle. Sono quelli che lottano contro i wahhabita più seriamente. Idioti, a che serve andar dicendo che ogni sunnita è un salafita , che ogni manifestante è salafita, che ognuno di loro è un nemico', perché  fai così quando lo fai tanto per fare..  
"Vostro Padre e vostra Madre non vivranno per sempre, non potranno tenervi sulla retta via sempre e lo sapete entrambi. Quindi, se volete far bene per voi stessi in futuro, smettetela e non portatevi via Amina.  
Potrete tornare a casa vostra e dire ai vostri che le persone come lei sono i migliori amici che gli Alawi possano avere e non devono venire a disturbarla.  
"E adesso, voi due vi scusate entrambi per averla svegliata e averla messa in mezzo così. Mi avete capito?"  
  
E il tempo si ferma quando lui smette di parlare. Adesso o lo sbattono giù e lo picchiano, stuprano me, e ci portano via ... o ...  
  
Il primo annuisce, poi il secondo  
"Torna a dormire", dice, "ci dispiace di avervi fatto agitare".  
E se ne vanno!  
  
Non appena il cancello è chiuso,,, ho sentito applausi, tutti in casa erano svegli e stavano guardando dai balconi e dalle porte e dalle finestre in tutto il cortile ... e tutti applaudivano ...  
  
IL MIO PAPA' li aveva appena sconfitti ! Non con le armi ma con le parole ... e ci avevano lasciati stare lì ...  
L'ho abbracciato e baciato, oh  gli devo la mia vita adesso, letteralmente.  
E tutti sono venuti giù e ci siamo abbracciati e baciati, ogni membro della famiglia, i servi e tutti ... abbiamo vinto ... questa volta ...  
  
Mio padre è un eroe, l'ho sempre saputo bene ... ma ora ne sono sicura ...  
  
E nella notte abbiamo festeggiato questa piccola vittoria, che forse può non risuccedere, ma forse ...  
Ha fatto qualche telefonata nel mezzo della notte, ha svegliato alcune persone (e ne ha trovato alcune che non dormivano) che sono nel regime e ha detto loro di tener d'occhio i sicari ' Essi possono subire conseguenze per il loro operato se non smetteranno ( per questo che non volevano dare i loro nomi), e probabilmente non faranno nuove spedizioni ... .  
  
Forse diranno agli altri, di lasciare in pace la "liberale" ... o forse no. Solo il tempo potrà dirlo  
  
Così, quando mio padre dice che non se ne andrà fino a quando non viene la democrazia oppure è morto, non ho altra scelta se non quella di rimanere qui. Non perché me lo impone, ma perché non lo fa  
  
Noi abbiamo mandato via tutti, tutti quelli che vogliono possono andare a Beirut. Io non posso andare via, voglio rimanere, qualsiasi cosa succede.