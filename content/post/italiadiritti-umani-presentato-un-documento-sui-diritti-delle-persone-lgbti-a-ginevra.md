---
title: 'Italia/Diritti Umani: Presentato un documento sui diritti delle persone LGBTI a Ginevra'
date: Mon, 06 Oct 2014 20:07:26 +0000
draft: false
tags: [Transnazionale]
---

[![UN-UPR-Geneva](http://www.certidiritti.org/wp-content/uploads/2014/04/UN-UPR-Geneva-300x200.jpg)](http://www.certidiritti.org/wp-content/uploads/2014/04/UN-UPR-Geneva.jpg)Nell'ambito del processo per la Revisione Periodica Universale sullo stato dei diritti umani nel nostro Paese, si tiene oggi a Ginevra la pre-sessione ​dedicata all'Italia. Una coalizione di associazioni LGBTI - tra cui il Centro Risorse LGBTI, l'Associazione Radicale Certi Diritti, Famiglie Arcobaleno e Intersexioni - presenterà oggi tramite ILGA un [documento](http://www.certidiritti.org/wp-content/uploads/2014/10/Statement-Italy-UPR-Pre-Sessions.pdf) di analisi dei diritti umani delle persone LGBTI dalla scorsa  Revisione Periodica Universale che si era tenuta nel 2009.

Le associazioni propongono alle varie delegazioni che esamineranno il documento del governo italiano anche una serie di raccomandazioni  che rappresenteranno la griglia di riferimento per il governo italiano in tema di diritti umani.

Tra le raccomandazioni, sette riguardano la protezione dalla violenza e dalle discriminazioni (tra queste le richiesta di una sistematica formazione dei pubblici ufficiali e il regolare finanziamento di un equality body indipendente che lavori su una agenda per l'uguaglianza multi-ground), una riguarda l'adozione di una legge che riconosca i diritti e doveri delle coppie dello stesso sesso, una chiede l'inserimento  nel Codice etico del CONI della proibizione delle discriminazioni basate su orientamento sessuale, identità ed espressione di genere, una richiede la possibilità, per le persone trans, di modificare i propri documenti identitari con procedure rapide ed efficienti, e tre riguardano il diritto all'integrità fisica delle persone intersessuali.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti e coordinatore del lavoro sulla Revisione Periodica Universale, dichiara: "quello di oggi è un passaggio molto importante che permette alle ONG di prendere la parola davanti alle delegazioni che giudicheranno il documento governativo. Abbiamo lavorato anche con i altri partners europei per influenzare i governi che il 27 ottobre valuteranno l'Italia. Se questi governi faranno raccomandazioni puntuali come quelle che proponiamo noi e l'Italia le accoglierà, nei prossimi cinque anni avremo una speranza in più di vedere i diritti umani maggior mente rispettai anche nel nostro Paese".

**Per leggere il documento presentato in inglese, clicca [qui](http://www.certidiritti.org/wp-content/uploads/2014/10/Statement-Italy-UPR-Pre-Sessions.pdf).**