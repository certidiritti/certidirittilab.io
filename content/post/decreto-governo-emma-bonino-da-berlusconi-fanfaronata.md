---
title: 'DECRETO GOVERNO, EMMA BONINO: DA BERLUSCONI FANFARONATA'
date: Fri, 06 Feb 2009 17:21:43 +0000
draft: false
tags: [Comunicati stampa]
---

Roma, 6 feb. -Emma Bonino, esponente storica dei radicali e Vice Presidente del Senato ha oggi dichiarato: "Il presidente del Consiglio Silvio Berlusconi, pur consapevole che il decreto non verra' firmato dal presidente della Repubblica, ha voluto ingraziarsi il Vaticano a costo di violare brutalmente le regole base di uno stato di diritto. Con questa fanfaronata, indegna di un presidente del Consiglio, Berlusconi ha anche confermato quello che era oramai ovvio da tempo, cioe' la volonta' di governo e maggioranza di adottare una legge non sul ma contro il testamento biologico.

Ssoprattutto a coloro che fingono di conoscere il testo del ddl all'esame del Senato, che all'articolo 5 comma 6 si dichiara a chiare lettere che alimentazione e

idratazione 'non possono formare oggetto' di testamento biologico. Quindi, il cittadino, pur in stato di piena capacita' di intendere e di volere e in situazione di compiuta informazione medico-clinica, non potra' decidere della propria sorte. **G****ia' ai cittadini adulti di questo paese non e' consentito organizzare i propri legami e la propria vita in maniera responsabile e consapevole, ora viene loro tolta anche la liberta' di decidere di morire in** **maniera dignitosa.** Mi auguro che il presidente della Repubblica, dopo aver espresso il suo parere negativo sul decreto di oggi, non firmera' neppure la legge che il Parlamento si appresta a votare perche' palesemente inconstituzionale".