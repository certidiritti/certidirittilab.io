---
title: 'Dalla visita del papa a Milano al Pride di Bologna: sostieni Certi Diritti'
date: Sat, 25 Aug 2012 13:51:47 +0000
draft: false
tags: [Politica]
---

![LogoCD](http://www.radicalparty.org/file/Logo_Cd_2012.JPG)

Giugno è il mese dell’orgoglio lgbt. 

In tutto il mondo, con il Pride, viene celebrato il **28 giugno 1969**, giorno in cui per la prima volta gay lesbiche e trans decisero di reagire alla repressione della polizia. Nella notte di venerdì 27 giugno un’irruzione delle forze dell’ordine nello "Stonewall Inn", un bar gay nel Greenwich Village di New York, scatenò quelli che oggi vengono ricordati come i **moti di Stonewall**.

In Italia come ogni anno sono molte le città che ospiteranno il Pride.   
L’associazione radicale Certi Diritti ha partecipato a quello di Salerno e sarà anche a Torino, Roma e**Bologna, dove il 9 giugno si svolgerà il Pride nazionale**.   
Il movimento lgbte ha scelto di essere in piazza nonostante il terremoto che ha colpito l’Emilia, ma il corteo non avrà carri addobbati né impianti amplificati per la diffusione della musica e [**tutte le iniziative del Bologna Pride saranno a sostegno dei territori colpiti dal sisma**](http://www.bolognapride.it/2012/05/30/le-iniziative-del-bologna-pride-a-sostegno-dei-territori-colpiti-dal-sisma/).

**Al Mosca Pride invece anche quest’anno si sono verificati arresti e violenze.   
L’associazione radicale Certi Diritti ha scritto una [lettera al Segretario Generale delConsiglio d'Europa](ancora-arresti-al-gay-pride-di-mosca-lettera-al-segretario-generale-del-consiglio-deuropa-intervenite).**

A Milano il Consiglio di Zona 2 ha approvato la [**mozione presentata dal vicepresidente della Zona Yuri Guaiana, del Gruppo Radicale-Federalista Europeo, per la revoca del patto di gemellaggio tra Milano e San Pietroburgo**](radicali-il-consiglio-di-zona-2-approva-allunanimita-la-mozione-radicale-per-la-revoca-del-gemellaggio-con-san-pietroburgo) in ragione della legge approvata, il 29 febbraio scorso, dal parlamento di San Pietroburgo che sanziona la cosiddetta "propaganda dell'omosessualità", criminalizzando di fatto qualunque attività o informazione relativa alle persone LGBTI. La stessa mozione è stata presentata anche al Consiglio Comunale d al consigliere radicale Marco Cappato ed è stata firmata da tutti i capigruppo della maggioranza.

L’associazione radicale Certi Diritti ha anche promosso, **insieme ad Amnesty International, due incontri (a Milano il 7 giugno e a Roma il 13) con militanti lgbt albanesi, turchi e russi **per fare il punto dei diritti lgbt in ai confini dell’Unione europea.

Intanto il 25 maggio a Milano, presso il Teatro dell'Elfo, abbiamo ricordato il nostro compagno [**David Kato Kisule**](http://www.dazebaonews.it/italia/societa/everyone-group-diritti-umani/item/10806-milano-teatro-dellelfo-la-condizione-dei-gay-nel-mondo-omaggio-a-david-kato) e promosso un incontro sulla depenalizzazione dell’omosessualità nel mondo con Elio Polizzotto (direttore del Centro per la Difesa e Promozione dei Diritti Umani), Roberto Malini (co-presidente del Gruppo EveryOne) ed Elio De Capitani, direttore artistico e presidente del Teatro dell'Elfo.

Il Papa è a Milano ma la città dei diritti e della laicità delle istituzioni non si ferma. Anzi, si mobilita per parlare delle riforme laiche ed europee che ancora aspettano di essere introdotte. Continua laraccolta firme per le 5 proposte di iniziativa popolare. [**Scopri come e quando firmare >**](http://milanoradicalmentenuova.it/)

**Fuor di pagina - la rassegna stampa di Certi Diritti. [Ascolta su radio radicale >](http://www.radioradicale.it/rubrica/1004)[Podcasting >](http://www.radioradicale.it/podcasting.php?e=1004)**

**Liberi.Tv- Spazio Certi Diritti: [Oltre il pregiudizio, oltre il tempo. L'Amore è Oltre. Intervista a Vincenzo Perrellis, Scrittore e Docente di lingue e cultura albanese.](http://www.liberi.tv/webtv/2012/05/28/video/oltre-pregiudizio-oltre-tempo-lamore-%C3%A8-oltre-intervista)**Intervista a cura di** Riccardo Cristiano.**

**5x1000**,** 8x 1000** E’ tempo di dichiarazione dei redditi ed è tempo di scelte, obbligatorie sull’8 per mille e facoltative sul 5 per mille. [**Le nostre proposte >**](5-per-mille-8-per-mille-consigli-per-chi-non-vuole-farsi-male-da-solo) 

**Cogliamo l’occasione della newsletter per ricordarti che senza il tuo aiutol’associazione radicale Certi Diritti non può promuovere nessuna iniziativa.  
Tutto il nostro lavoro è volontario e solo con i contributi dei sostenitori possiamo continuare le nostre battaglie.**

[**Se non puoi o non vuoi iscriverti puoi comunque sostenere l’associazione. Per noi ogni piccolo aiuto è fondamentale. Scopri come fare. Grazie.**](iscriviti)

[**www.certidiritti.org**](http://http//www.certidiritti.it/iscriviti)