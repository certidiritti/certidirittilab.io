---
title: 'DOCUMENTO SULLA SICUREZZA REDATTO DALL''ORDINE NAZIONALE DEGLI ASSISTENTI SOCIALI'
date: Thu, 10 Jul 2008 15:48:40 +0000
draft: false
tags: [Comunicati stampa, Ordine Assistenti Sociali, sicurezza]
---

**[Il Consigliio Nazionale dell'ordine degli Assistenti Sociali](http://www.cnoas.it/index.php)** ha redatto un [documento pubblico](index.php?option=com_docman&task=doc_download&gid=29&Itemid=79), rivolto all'attenzione delle istituzioni italiane, per esprimere "preoccupazione per gli scenari che sembrano aprirsi con gli attuali orientamenti sul tema della sicurezza, così come emersi dal dibattito politico e tradottisi recentemente in alcuni provvedimenti normativi."  
[Cliccando qui è possibile leggere il documento.](index.php?option=com_docman&task=doc_download&gid=29&Itemid=79)