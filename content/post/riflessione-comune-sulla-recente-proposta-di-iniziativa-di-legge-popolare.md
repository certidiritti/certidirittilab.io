---
title: 'Riflessione comune sulla recente proposta di iniziativa di legge popolare'
date: Wed, 13 Jun 2012 13:23:59 +0000
draft: false
tags: [Politica]
---

In questi giorni è stata annunciata ed è partita la raccolta delle firme a sostegno di una proposta di legge di iniziativa popolare in materia di riconoscimento di unioni civili, patti di solidarietà e unioni di fatto.

Pur essendo stupiti  che le nostre associazioni (attive da molti anni su questi temi) non siano state coinvolte, almeno nella fase di confronto che precede le scelte, riteniamo che sia non solo un diritto, ma un dovere, farsi promotori di proposte concrete per superare lo scandaloso silenzio di un  Parlamento che continua ad essere cieco e sordo di fronte alla realtà delle famiglie composte da coppie di persone dello stesso sesso.

Uno degli obiettivi della proposta di legge di iniziativa popolare è quello di rianimare il dibattito in Italia su questi temi: per questo riteniamo utile esprimere il nostro punto di vista che ci vede concordi sui seguenti punti:

1.  
L’avvio della campagna di raccolta firme è stato accompagnato dall’insistente ripetizione dell’argomento che la sentenza n. 138/2010 della Corte costituzionale escluderebbe la possibilità di ottenere il matrimonio civile tra persone dello stesso sesso in Italia. Questo argomento è completamente inesatto, non solo alla luce della sentenza citata ma anche della più recente sentenza di Cassazione, ed è sostenuta da un numero molto ampio di giuristi e docenti universitari. Non esiste, quindi, un ostacolo costituzionale alla possibilità del Parlamento di legiferare sulla materia, estendendo il matrimoni civile alle coppie costituite da persone dello stesso sesso.

2.  
Per le associazioni che sottoscrivono il presente documento, i principi di uguaglianza e di pari dignità sono inderogabili. Nel progetto di legge citato questo principio pare essere accolto con una evidente contraddizione laddove sceglie   di chiamare “unione civile” quello che di fatto è un matrimonio. A cosa serve un cambio di nome? Riteniamo che la questione non sia solo nominalistica ,diversamente il nostro Paese avrebbe già una legge per l’estensione dei  diritti matrimoniali e per il riconoscimento di altre forme di convivenza.

3.  
Le nostre associazioni ritengono che sia importante lavorare per una evoluzione del diritto di famiglia che si ponga realmente la questione del riconoscimento di differenti forme di convivenza, a partire dal principio di autodeterminazione che anche nella vita di relazione deve essere affermato. Ma il riconoscimento di queste forme non può in alcun modo svolgere una funzione “compensativa” rispetto all'estensione del matrimonio civile come affermazione di un'eguaglianza sostanziale e non solo formale ,per le coppie dello stesso sesso

4.  
Precisato questo ,riteniamo che ogni contributo possa essere importante,ma che la nostra prassi politica debba essere rappresentativa dei diritti nella loro integrità e non di una mediazione degli stessi : il testo infatti suggerisce che il nostro paese, a differenza delle più moderne democrazie,  non è pronto per accogliere il matrimonio civile per le coppie dello stesso sesso. Non possiamo dunque sostenere una proposta "sostitutiva" che finirebbe solo per formalizzare la disuguaglianza. Chiediamo invece ai promotori della legge  di riformulare il testo e di indicare come primo traguardo l'estensione del matrimonio civile e non le "unioni civili" per le coppie omosessuali. Potremo in questo modo dedicare tempo e impegno al successo della proposta con tutti i mezzi di cui disponiamo.

AGEDO  
ARCIGAY  
ASSOCIAZIONE RADICALE CERTI DIRITTI  
FAMIGLIE ARCOBALENO  

IL PRESENTE DOCUMENTO E' APERTO ALLA SOTTOSCRIZIONE DI TUTTE LE ASSOCIAZIONI E SINGOLE PERSONE CHE VI SI RICONOSCANO. SEGNALARE A [segretario@certidiritti.it](mailto:segretario@certidiritti.it), [ufficiostampa@arcigay.it](mailto:ufficiostampa@arcigay.it), [presidente@famigliearcobaleno.org](mailto:presidente@famigliearcobaleno.org), [agedobrescia@gmail.com](mailto:agedobrescia@gmail.com)