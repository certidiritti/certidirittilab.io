---
title: 'Corte Costituzionale nuovamente chiamata a decidere su matrimonio per coppie gay'
date: Wed, 01 Dec 2010 12:02:14 +0000
draft: false
tags: [certi diritti, Comunicati stampa, consulta, CORTE COSTITUZIONALE, ferrara, matrimonio gay]
---

**LA CORTE COSTITUZIONALE CHIAMATA NUOVAMENTE A PRONUNCIARSI SUL DIVIETO PER LE COPPIE OMOSESSUALI DI SPOSARSI.  
Comunicato Stampa dell'Associazione Radicale Certi Diritti e della Costituzionalista Marilisa D'Amico, coordinatrice dello staff legale della campagna di Affermazione Civile:**

Roma, 1 dicembre 2010 

La Corte costituzionale si riunisce oggi in Camera di Consiglio per decidere sulla questione di legittimità costituzionale sollevata dal Tribunale di Ferrara, che ha ancora una volta prospettato alla Corte la necessità di aprire il matrimonio a persone dello stesso sesso, perché l’esclusione da questo diritto determina una violazione degli artt. 2, 3 e 29 della Costituzione, degli artt. 12 e 16 della Dichiarazione universale dei diritti dell'uomo, dell’art. 12 della Convenzione per la salvaguardia dei diritti dell’uomo e delle libertà fondamentali, nonché degli artt. 7 e 9 della Carta dei diritti fondamentali dell’Unione europea.Le parti del processo instaurato innanzi al Tribunale di Ferrara non si sono però costituite nel giudizio costituzionale e la Corte deciderà in camera di consiglio.

È molto probabile, quindi, che, come peraltro già accaduto con l’ordinanza n. 276 del 2010, e non essendo stati prospettati profili diversi o ulteriori, la Corte si pronunci nel senso della manifesta inammissibilità e infondatezza delle questioni, richiamandosi alla sentenza n. 138 del 2010.