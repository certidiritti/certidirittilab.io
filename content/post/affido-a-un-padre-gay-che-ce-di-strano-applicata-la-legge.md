---
title: 'AFFIDO A UN PADRE GAY: CHE C''E'' DI STRANO? APPLICATA LA LEGGE!'
date: Wed, 23 Jul 2008 15:32:33 +0000
draft: false
tags: [Comunicati stampa]
---

DICHIARAZIONE DI SERGIO ROVASIO, SEGRETARIO DELL'ASSOCIAZIOEN RADICALE CERTI DIRITTI.

Non possiamo non notare qualcosa di strano nelle recenti, entusiastiche dichiarazioni pubbliche in merito alla cosiddetta "rilevanza storica" della sentenza del Tribunale di Bologna che acconsente all'affidamento congiunto di una bambina nata dal matrimonio tra una donna eterosessuale e un uomo omosessuale.

Strano perche' il Tribunale non ha fatto altro che applicare la Costituzione e la Legge, i cui principi di non discriminazione ed uguaglianza sono trasparenti e non equivoci, anche nei confronti delle persone omosessuali.

Strano perche' far passare per una "conquista storica" la semplice applicazione della Legge (peraltro gia' sicuramente applicata molte altre volte nel nostro Paese) rischia di proiettare gli altri obiettivi di parita' in un futuro talmente lontano che diventa persino difficile immaginarlo.

L'Associazione radicale Cetti Diritti proprio in nome di questa fiducia nella Legge, nella Costituzione, e nella buona volontà di chi amministra la Giustizia nel nostro Paese crede che, dopo le prove poco edificanti offerte dall'ultimo Parlamento, sia proprio la fiducia nei diritti e nel valore dei principi costituzionali che ci governano a darci una possibilita' per il riconoscimento di altri diritti di parita' oggi negati.

Gia' oggi piu' di 25 coppie omosessuali sono andati di fronte agli uffici anagrafici dei Comuni per chiedere la pubblicazione della richiesta di matrimonio, ed avviando cause contro i dinieghi che le stese amministrazioni offrono.

E' necessario passare dalla protesta per i diritti negati all'affermazione dei diritti stessi, con iniziative non virtuali ma concrete.