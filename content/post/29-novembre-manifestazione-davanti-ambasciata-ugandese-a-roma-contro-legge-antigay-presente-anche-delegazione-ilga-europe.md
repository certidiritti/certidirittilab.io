---
title: '29 Novembre manifestazione davanti ambasciata ugandese a Roma contro legge antigay. Presente anche delegazione ILGA-Europe'
date: Thu, 29 Nov 2012 07:27:56 +0000
draft: false
tags: [Africa]
---

Comunicato stampa dell'Associazione radicale Certi Diritti

Roma, 28 novembre 2012

Giovedì 29 novembre, alle ore 12,30, si svolgerà a Roma, davanti all’Ambasciata dell’ Uganda (Viale Giulio Cesare, 71) una manifestazione sit-in per denunciare  la nuova legge appena approvata contro l’omosessualità.

**[IL VIDEO DEL SIT-IN >>>](http://www.radioradicale.it/scheda/367019/legge-antigay-in-uganda-manifestazione-dei-radicali-davanti-lambasciata-a-roma)**

Manifestazioni anche in altre città italiane e del mondo.L’iniziativa è promossa dall’Associazione Radicale Certi Diritti, insieme al Partito Radicale Nonviolento transnazionale e transpartito, Nessuno Tocchi Caino e Radicali Roma. Alla manifestazione  parteciperà, tra gli altri, una delegazione di Ilga-Europe, l'International Lesbian and Gay Association (in questi giorni a Roma per una riunione di lavoro) con Amnesty International, il Circolo Mario Mieli, Sel-Roma, Gruppo Lista Bonino Pannella, Federalisti Europei alla Regione Lazio, Gay Center, Associazione DiGayProject, Rete Near Antidiscriminazioni, Cgil Nuovi Diritti ed esponenti della comunità Lgbt di Roma.

Nei giorni scorsi una Commissione del Parlamento ugandese ha approvato il disegno di legge, l’ “anti-homosexuality bill”, che prevede pene molto severe, fino all’ergastolo, per le persone omosessuali e per coloro che difendono i diritti civili della comunità Lgbt. La portavoce del Parlamento ugandese ha definito questa legge un "regalo di Natale" per i suoi sostenitori. Il testo della nuova legge appena approvata al momento non prevede la pena di morte, anche se rimane alto il rischio di una sua approvazione in tempi brevi. Nella sua forma originale, il testo della legge prevedeva che i condannati per "omosessualità aggravata" – quando cioè uno dei partecipanti è un minore, HIV-positivo, portatore di handicap o "criminale seriale" – andavano incontro alla pena di morte.

Il disegno di legge originale vietava anche la "promozione" dei diritti dei gay e prevedeva la punizione di tutti coloro che "finanziano o sponsorizzano l’omosessualità" o "incoraggiano l'omosessualità".Dopo l'omicidio di David Kato Kisule, noto attivista ugandese iscritto all'associazione radicale Certi Diritti,  avvenuto due anni fa, riteniamo sia sempre più urgente agire per bloccare il bill anti-gay e aderire così alla mobilitazione internazionale contro l’Uganda.David Kato Kisule, nel dicembre 2010, poche settimane prima di essere brutalmente assassinato in Uganda, era stato a Roma, al Congresso dell’Associazione Radicale Certi Diritti  e a Bruxelles al Parlamento Europeo, per denunciare le persecuzioni di cui sono vittime le persone gay nel suo paese. Diverse Ong e militanti gli offrirono aiuto e protezione in Europa ma lui decise di tornare nel suo paese per combattere contro l’omofobia sociale e di Stato molto diffusa nel paese anche a causa delle chiese fondamentaliste evangeliche.