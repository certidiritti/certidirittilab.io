---
title: 'Governo inglese lancia consultazione pubblica su nozze gay nonostante parere contrario chiesa. Classe politica italiana impari'
date: Thu, 15 Mar 2012 23:58:07 +0000
draft: false
tags: [Matrimonio egualitario]
---

Roma, 15 marzo 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Oggi il Governo inglese ha lanciato la consultazione pubblica sui matrimoni tra persone dello stesso sesso, chiedendo all’opinione pubblica se la nuova legge deve essere introdotta in Inghilterra e Galles. Ad opporsi all’iniziativa del Governo inglese ci sono in prima fila  i cattolici che stanno  organizzando la partecipazione alla consultazione con l’organismo “Coalition for marriage” promosso dall’Arcivescovo cattolico Peter Smith, con il sostegno dell’ex arcivescovo di Canterbury, Lord George Carey e dal vescovo di Sherwsbury, Mark Davies.

Il Ministro per le Pari Opportunità del Governo britannico, Lynne Featherstone, nei giorni scorsi aveva detto all’Indipendent che “Il governo sta promuovendo una società giusta in cui le persone si rispettino. Credo che se una coppia si ama e vuole impegnarsi per una vita in comune, dovrebbe avere la possibilità di celebrare un matrimonio civile, a prescindere che si tratti di una coppia di omosessuali o di eterosessuali. Non vogliamo cambiare il matrimonio religioso, o chiedere ai gruppi religiosi di andare contro le proprie tradizioni”.

Intanto oggi sul Times il Ministro degli Interni britannico, Teresa May, esprime il suo sostegno alla proposta governativa che consente alle persone omosessuali di accedere all’istituto del matrimonio. Il Ministro degli Interni nel suo intervento sostiene, tra l’altro, che: “I matrimoni civili rafforzeranno la società e consentiranno alle coppie gay di amarsi e rispettarsi come tutte le altre coppie. Il matrimonio deve essere per tutti e lo Stato non deve discriminare le persone omosessuali impedendo loro di sposarsi. Andremo avanti su questa strada nonostante l’opposizione della chiesa anglicana e cattolica. Lo Stato non può impedire alla gente di sposarsi.”.

L’Associazione Radicale Certi Diritti invita la classe politica italiana a imparare cos’è la democrazia e a smetterla di genuflettersi ai voleri del Vaticano.

[Una volta a settimana tutti gli aggiornamenti sulle nostre battaglie per i diritti civili e la la libertà sessuale. Iscriviti alla newsletter >](newsletter/newsletter)