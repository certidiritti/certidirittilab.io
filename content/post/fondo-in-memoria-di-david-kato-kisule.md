---
title: 'Fondo in memoria di David Kato Kisule'
date: Tue, 22 Mar 2011 08:10:00 +0000
draft: false
tags: [Politica]
---

Il 26 gennaio del 2011, l’attivista ugandese e difensore dei diritti umani, David Kato Kisule, esponente della comunità LGBTI, è stato trovato assassinato nella sua residenza a Kyetume, nel distretto di Mukono, Uganda. David ha trascorso la propria vita da militante in difesa dei diritti umani, combattendo per l'uguaglianza di uomini e donne, perseguitati a causa del loro orientamento sessuale fino al giorno del suo assassinio.

La comunità LGBTI ugandese e la comunità internazionale hanno condannato l'uccisione di David Kato Kisule e si sono rivolte al governo ugandese per chiedere indagini imparziali, rigorose ed efficaci, in modo che ci siano i presupposti per un giusto processo in grado di far emergere la verità su quanto accaduto, su chi ha voluto e commesso l’assassinio di David.

La comunità LGBTI non si farà intimidire; il suo impegno proseguirà e la morte di David sar onorata solamente quando sarà vinta la lotta per l’uguaglianza e la giustizia di uomini e donne.

**Arcigay**  
**Associazione radicale Certi Diritti**  
**Non c'è Pace Senza Giustizia**  
**con il consenso di Sexual Minorities Uganda (SMUG)**

   ![](https://www.paypalobjects.com/WEBSCR-640-20110401-1/it_IT/i/scr/pixel.gif)

Se volete, potete inoltre contribuire al fondo  
Uganda Legal Fund  
con bonifico bancario sul c/c n 12691/24:  
IBAN: IT52 V030 6905 0310 0000 1269 124  
ABI: 03069 - CAB:05031  
Cariplo Banca Intesa BCI  
Intestato al Comitato Non c’e Pace Senza Giustizia  
Piazza Colonna Agenzia 1 (Roma) Italia

  
  
**Ti invitano ad onorare la memoria di David Kato Kisule**  
**Partecipando alla raccolta fondi per un processo equo e veritiero**

In Uganda le autorità governative e i sostenitori del fondamentalismo religioso, questi sostenuti anche da componenti della destra americana, continuano ad istigare all’odio contro coloro che appartengono alla comunità LGBTI.

Durante la cerimonia funebre di David, il pastore anglicano con parole violente ha condannato l’attivista ucciso e con lui l’intera comunità LGBTI incitando ancora una volta alla violenza. I media, a loro volta, continuano ad alimentare questi sentimenti contribuendo ad accrescere la violenza, la discriminazione e i crimini contro la comunità LGBTI.

In questo scenario il Governo ugandese non ha accantonato la proposta di legge che propone la pena di morte per gli omosessuali “recidivi”.  
  
**Noi ci impegniamo a non lasciare soli i nostri compagni ugandesi e a contribuire a questa battaglia di civiltà.**

**Come fare una donazione?**  
con bonifico bancario sul conto corrente 12691/24 intestato a  
**Comitato Non c’è Pace Pace Senza Giustizia**  
Via di Torre Argentina, 76 00186 – Roma (Italy)  
  
Cariplo - Banca Intesa BCI  
Piazza Colonna AG 1 Roma  
IBAN IT52 V030 6905 0310 0000 1269 124  
ABI 03069 CAB 05031

* * *

On the 26th of January 2011, Ugandan LGBT activist and human rights defender, David Kato Kisule,was found murdered at his residence in Kyetume, Mukono District.

**DAVID SPENT HIS LIFE ADVOCATING FOR EQUALITY AND DIGNITY FOR ALL, FOR HIS SISTERS AND BROTHERS PERSECUTED BECAUSE OF THEIR SEXUAL ORIENTATION OR GENDER IDENTITY, CONSTANTLY UNTIL THE DAY BEFORE HIS MURDER.**

The Ugandan LGBTI and the international community stand together to condemn the killing of David Kato Kisule and call the Government of Uganda to a rigorously, effective and impartial investigation into the murder, and a fair and accountable trial which will bring those responsible for his killing, include those who may have encourage or request his murder, to justice. Any kind and or extremely form of intimidation will not stop the LGBTI cause across Uganda, and  
the death of David will be only honored when the struggle for justice and equality will be won.

**ARCIGAY**  
**The Radical Association CERTI DIRITTI**  
**NO PEACE WITHOUT JUSTICE**  
**Endorsed by Sexual Minorities Uganda (SMUG)**

**INVITE YOU TO HONOR DAVID KATO KISULE’S MEMORY**

**YOU CAN STAND UP AND SPEAK OUT for a fair and accountable David murder trial**

   ![](https://www.paypalobjects.com/WEBSCR-640-20110401-1/it_IT/i/scr/pixel.gif)

You can also contribute to  
Uganda Legal Fund  
through bank transfer to:  
Comitato Non c’e Pace Senza Giustizia  
Account no. 12691/24  
IBAN: IT52 V030 6905 0310 0000 1269 124  
ABI: 03069 - CAB:05031  
Cariplo Banca Intesa BCI  
Piazza Colonna AG 1 Rome (Italy)  
  
There is a strong and long history of hate speech in Uganda against LGBT people by government authorities and religious fundamentalists, both local and international, particularly, the American Right Wing. Even during David’s funeral, the presiding religious minister chose to condemn David in his coffin and the larger LGBTI movement, further inciting people to violence.

The role of the media has continued to fuel the sentiments, which came in the wake of the recurring hate speech. This has further incited a range of violations, which include and not limited to hate crimes, discrimination and rejection of LGBT people in Uganda.

In this contest, the Ugandan Government has not given up on the Anti-Homosexuality Bill, which punishes “aggravated homosexuality” with the death penalty.

**THE UGANDAN LGBTI COMMUNITY IS NOT ALONE, AND WE WILL CONTINUE TO WORK AND SUPPORT THEIR STRUGGLES.**