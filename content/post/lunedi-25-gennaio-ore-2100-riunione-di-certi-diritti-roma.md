---
title: 'LUNEDI'' 25 GENNAIO, ORE 21.00 - RIUNIONE DI CERTI DIRITTI ROMA'
date: Wed, 20 Jan 2010 19:02:54 +0000
draft: false
tags: [Senza categoria]
---

Carissime e carissimi,

siamo alla vigilia di un'importante data che vede l'Associazione Radicale Certi Diritti convocare il suo 3° Congresso Nazionale a Firenze, nei giorni 30 e 31 gennaio p.v.  
Una tappa fondamentale del nostro cammino, che noi di Certi Diritti Roma vogliamo affrontare preparati nel migliore dei modi.

  
Siamo anche reduci dal succedersi di una serie di avvenimenti accaduti nella nostra città nell'ultimo mese e mezzo che hanno visto il 19 dicembre tutto il mondo gay, lesbico e trans riunirsi per decidere in maniera più o meno unitaria il prossimo Pride di Napoli 2010, la riunione del 7 gennaio di tutte le realtà transgender in risposta alle continue violenze di cui alcune di loro sono state vittime in questi giorni, (laddove Certi Diritti ha svolto un ruolo di catalizzatore per il superamento delle incomprensioni fra le diverse sensibilità), ma anche le burrascose elezioni per la presidenza del Circolo Mario Mieli (reiterate ben due volte), e il Congresso provinciale di Arcigay Roma (con tutto lo strascico di polemiche che ne è seguito).  
C'è poi ancora in atto la protesta di Francesco e Manuel, che chiedono il diritto a regolarizzare la loro unione e che dal giorno del primo sit-in di protesta del 4 gennaio in Parlamento portano avanti lo sciopero della fame (Francesco è ormai al 16° giorno, mentre Manuel ha dovuto smettere per problemi di salute), cui sono seguite le innumerevoli manifestazioni di sostegno che alla fine e dopo tanto tempo hanno visto l'accordo di tutte le associazioni.  
C'è poi una grande novità che noi di Certi Diritti stiamo lanciando in questi giorni, ovvero "Si, lo voglio" COMITATO PER IL RICONOSCIMENTO DEL MATRIMONIO TRA PERSONE DELLO STESSO SESSO, un comitato di cui si spera facciano parte il maggior numero di soggetti del movimento gay, lesbico e trans italiano a sostegno della campagna Affermazione Civile, che già incredibili risultati sta ottenendo.  
Per tutti questi motivi e per discutere delle iniziative future che Certi Diritti Roma ha intenzione di mettere in atto sul territorio, convochiamo un'assemblea del gruppo per lunedì 25 gennaio alle ore 21.00 presso la sede del Partito Radicale in Via di Torre Argentina 76 a Roma, dove aspettiamo tutti, soci e simpatizzanti, che vogliano dire la loro e darci una mano in questo percorso di crescita o anche semplicemente venire a vedere chi siamo e cosa facciamo.  
Un saluto.

Luca "Lucky" Amato  
Certi Diritti Roma

> [Per confermare la propria presenza all'evento potete registtrarvi cliccando qui](tutti-gli-eventi/details/5-riunione-di-certi-diritti-roma.html)