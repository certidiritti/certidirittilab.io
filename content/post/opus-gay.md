---
title: 'Opus Gay'
date: Wed, 02 Feb 2011 11:37:37 +0000
draft: false
tags: [Politica]
---

**LA CHIESA CATTOLICA E L’OMOSESSUALITÀ**

Da anni la Chiesa cattolica assume una posizione di pubblica condanna nei confronti degli omosessuali. Ma, al tempo stesso, sostiene che gay e lesbiche non debbano essere discriminati.

Le gerarchie ecclesiastiche sembrano usare in materia un doppio registro: l’omosessualità è insostenibile a livello ufficiale ma si può tollerare sul piano privato, se non ostentata pubblicamente. Un esempio significativo di come la “Chiesa dei no” si ponga di fronte al proprio popolo, spesso in contraddizione con la stessa Parola contenuta nei Vangeli. Ma che ruolo hanno oggi i gay nella comunità dei credenti?

Questo libro-inchiesta vuole portare alla luce tale divario, denunciando l’ipocrisia che si annida nelle dichiarazioni degli organi clericali ufficiali, ma dando anche voce ai tanti omosessuali che di questa Chiesa, nonostante tutto, vorrebbero far parte. Ilaria Donatio – giornalista che da anni si occupa di diritti civili – ripercorre le storie difficili e contraddittorie di religiosi e fedeli gay e lesbiche, per arrivare ad affrontare il tema della morale sessuale nel cattolicesimo. L’autrice, attraverso un’analisi delle posizioni assunte dal magistero della Chiesa e l’interpretazione delle Sacre Scritture, ricostruisce il panorama attuale dell’acceso dibattito su fede e omosessualità.

**DALL’INTRODUZIONE:**

Quando ho iniziato a scrivere questo libro sul rapporto tra chiesa cattolica e omosessualità, mi sono sentita in difficoltà. Come giornalista, infatti, avevo già affrontato in passato la questione dei diritti civili e provato a indagare le ragioni della loro costante violazione. Questa volta, però, avrei dovuto interrogarmi e scegliere le parole giuste per raccontare due realtà che, in modo diverso, hanno sempre fatto parte della mia vita. Ma non c’erano parole giuste per descriverle. Solo espressioni limitate che, purtroppo, rischiavano di tradirle.

Da una parte la chiesa: ho studiato in una scuola cattolica per otto anni e sono stata, letteralmente, accompagnata nella mia formazione da un prete. Tutti i sabati, con i miei amici, leggevamo la parola e cercavamo di comprendere come farla entrare nelle nostre vite.

Dall’altra parte, le persone omosessuali. È davvero strano parlare di loro come se fossero una categoria a sé: ogni volta mi sembra di ridurli a un’etichetta. E di violarne la dignità di persone _a tutto tondo._

Nella realtà  funziona esattamente al contrario: incontri qualcuno, inizi a volergli bene e lui ti confida, tra le altre cose, anche le sue preferenze sessuali. Tu ne prendi atto, e vai avanti. Nella vita di tutti i giorni, semplicemente, fai esperienza dell’altro, senza incasellarlo o definirlo come si è costretti a fare nei libri. O nelle discussioni dottrinali.

Così è  successo anche a me. E quando ho incontrato un mio caro, vecchio amico _gay_, mi sono lasciata guidare da lui nel suo mondo, mettendoci anche un po’ del mio.

Questa prima relazione ha messo alla prova quello in cui credevo, che professavo con molta sicurezza e altrettanta serenità. Non c’è stato alcun conflitto. Ma una sintesi di vita: i miei principi si sono addolciti e la verità delle singole persone si è fatta avanti. Con la conseguenza, inevitabile, che entrambi – valori e vissuto – si sono, in parte, relativizzati: una vera liberazione. Una conquista. Che credo abbia ispirato anche queste pagine: ogni capitolo è stato, in fondo, provocato proprio da un incontro. A volte, è una testimonianza, altre un’intervista, altre ancora uno spunto giornalistico, ma qui non ci sono tesi da dimostrare. Solo alcuni fatti che ho provato a raccontare, e la proposta di altrettanti percorsi di lettura.

Se dovessi, alla fine, suggerire ai lettori due chiavi con cui “aprire” il libro, queste sarebbero certamente la responsabilità e la libertà.

Credo, come Lévinas, che la responsabilità nasca (e possa nascere) solo dalla relazione con l’altro e non da un principio. «l’estraneo che non ho né concepito, né partorito, l’ho già in braccio»1, scriveva il filosofo francese. È l’etica della differenza. Che, se solo diventasse un _bene comune_ – proprio come l’acqua che beviamo, l’aria che respiriamo e la terra che calpestiamo – avrebbe una portata rivoluzionaria sulle vite di tutti. E capovolgerebbe il nostro punto di vista di persone integrate, riconosciute, privilegiate, tutelate. Eterosessuali, anche.

La libertà è  l’altra faccia della responsabilità. Non è affatto vero che la mia libertà inizia dove finisce quella altrui. Credo, al contrario, che cominci esattamente nel punto in cui parte la libertà dell’altro. Ed è capace di realizzarsi solo nel rapporto con lui.

Ecco, fino a quando esisteranno persone, gay, lesbiche, transessuali, che non saranno libere di vivere dignitosamente, di tutelare i propri diritti, di vedere riconosciute le proprie scelte, fino a quel momento, nessuno sarà realmente libero. E ne siamo responsabili tutti.

• **CO 79**

• **Brossura**

• **336 pagine circa**

• € **12,90**

• **ISBN 978-88-541-2276-5**

**UFFICIO STAMPA NEWTON COMPTON**

Fiammetta Biancatelli – cell. 347.2154309 – [fiammettabiancatelli@newtoncompton.com](mailto:fiammettabiancatelli@newtoncompton.com)

*   Vania Ribeca – cell. 333.2554215 – [vania@newtoncompton.com](mailto:vania@newtoncompton.com)
    

Anna Voltaggio – cell. 333.4891759 – [annavoltaggio@newtoncompton.com](mailto:annavoltaggio@newtoncompton.com)

Via Panama 22 – 00198 Roma –  Tel. 06/65002553 – fax 06/65002892 [www.newtoncompton.com](http://www.newtoncompton.com/)