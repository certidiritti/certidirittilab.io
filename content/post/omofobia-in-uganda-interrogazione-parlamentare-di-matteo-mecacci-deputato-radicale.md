---
title: 'OMOFOBIA IN UGANDA: INTERROGAZIONE PARLAMENTARE DI MATTEO MECACCI, DEPUTATO RADICALE'
date: Tue, 16 Nov 2010 11:18:33 +0000
draft: false
tags: [Comunicati stampa, interrogazione parlamentare, Matteo Mecacci, OMOFOBIA, Uganda]
---

Matteo Mecacci, deputato radicale, eletto nel PD, iscritto all'Associazione radicale Certi Diritti, ha presentato alla Camera un'interrogazione parlamentare sulla campagna omofobica in corso in Uganda e sui tentativi di far approvare nel Paese la legge contro l'omosessualità denominata “Anti Homosexuality Bill 2009”

**Interrogazione a risposta scritta·****al Ministero degli Esteri,·****presentata da·****MATTEO MECACCI, deputato radicale (PD)**

Premesso che:

il 2 ottobre scorso, il giornale ugandese, _Rolling Stone_ ha pubblicato un articolo intitolato _“Divulgate 100 Foto dei Leader Gay Ugandesi”_;

nell’articolo, prima parte di una serie di 4 edizioni, si mostrano 100 foto di persone gay o lesbiche, che sono correlate da: nomi, posizione professionale, descrizione della vita privata, ubicazione dell’abitazione sia privata che lavorativa;

in tale articolo, inoltre, si rendono note le seguenti affermazioni:

\- si sollecita “l’impiccagione degli omosessuali” in Uganda, chiedendo al Governo ugandese di intervenire pesantemente nei loro confronti.

\- si sostiene che gay e lesbiche sarebbero volti a “reclutare” entro il 2012 un milione di bambini sotto i dodici anni, in quanto ritenuti facilmente indirizzabili all’omosessualità ed alla bisessualità.

\- un anonimo dirigente della Chiesa ugandese asserisce che: _“Se il governo non farà un passo coraggioso per l’impiccagione di decine e decine di omosessuali, il vizio continuerà a mangiare la fibra morale e la cultura della nostra grande nazione”_.

\- alcuni leader politici e della Chiesa ugandesi, per anni hanno sostenuto che ci sia in atto, una cospirazione internazionale delle Nazioni Unite e di alcuni governi occidentali, per convertire i giovani ugandesi all’omosessualità, con miliardi di dollari che sono incanalati segretamente verso associazioni gay ugandesi.

lo stesso articolo fa menzione della metodologia usata per l’indagine ossia l’uso di vari siti web sia nazionali che internazionali, di Facebook per rintracciare, collezionare per poi pubblicare, foto, identità e dati, nonché infiltrazioni effettuate anche con l’uso di telecamere nascoste sia in luoghi di ritrovo pubblici che in abitazioni private;

fonti locali riferiscono che una coalizione formatasi a seguito della pubblicazione del suddetto articolo, si e’ recata presso la sede del giornale per consegnare personalmente una lettera al redattore, in cui si chiede un atto pubblico di scuse e di non proseguire nella pubblicazione di ulteriori foto e dati personali come già annunciato nell’articolo del 2 ottobre scorso;

la lettera ha ricevuto risposta negativa da parte del direttore, che ha invece confermato la linea del giornale, ossia, la pubblicazione di nuove foto e dati personali nei prossimi quattro numeri;

come riportato da Apcom il 21 ottobre scorso, l’attivista ugandese per i diritti umani _Julian_ _Onziema_ ha denunciato che: _“Almeno quattro omosessuali sono stati aggrediti e molti altri sono stati costretti a nascondersi dopo la pubblicazione sulla prima pagina della rivista ugandese ‘Rolling Stone’ di nome, indirizzo e fotografia di 100 gay, con la scritta ‘Impiccateli’”_;

il 10 novembre scorso Amnesty International ha, inoltre denunciato che: _“il Presidente dell’ONG Minoranze Sessuali Uganda, Frank Mugisha, così come molte delle persone additate dal giornale ugandese Rolling Stone, hanno ricevuto minacce di morte e insulti, per telefono, in strada e dai vicini”_;

Considerato, che:

il 25 settembre 2009, il deputato _David Bahati_ ha presentato al Parlamento ugandese una proposta di legge contro l'omosessualità denominata “Anti Homosexuality Bill 2009”;

con tale proposta di legge, si prevede in particolare quanto segue:

\- per coloro che sono ritenuti colpevoli di atti omosessuali la pena detentiva è portata da 14 anni all’ergastolo.

\- la pena di morte è prevista per coloro che commettono atti omosessuali con minori o se malati di HIV.

\- pene severe sono stabilite, anche nei confronti di coloro che sono a conoscenza di persone omosessuali e non le denunciano, per esempio, ogni genitore che non denuncia alle autorità competenti la figlia lesbica o il figlio gay rischia multe salate e sino a 3 anni di carcere; lo stesso destino per gli insegnanti, mentre il padrone di casa che osasse affittare proprietà a sospetti omosessuali rischia sino a 7 anni di prigione.

\- si minaccia anche di punire o rovinare la reputazione di chiunque sia a favore della legalizzazione dell’omosessualità e di chi lavora per la protezione dei diritti delle minoranze sessuali, e nella categoria sono compresi medici e attivisti della società civile.

il progetto in questione prevede l'annullamento, da parte dell'Uganda, di tutti gli impegni internazionali o regionali assunti dal paese che siano considerati in contrasto con le disposizioni di siffatto progetto;

la legge è già stata condannata dal Commissario europeo De Gucht, dai Governi britannico, francese e svedese, come pure dal Presidente degli Stati Uniti Obama e dal Presidente e dal Vicepresidente della Commissione per gli affari esteri della Camera dei rappresentanti degli Stati Uniti;

il progetto di legge è stato anche denunciato da organizzazioni non governative di tutto il mondo e nella stessa Uganda come un grande ostacolo alla lotta contro l'HIV e l'AIDS nella comunità omosessuale;

in Africa l'omosessualità è legale soltanto in 13 paesi ed è un reato punibile in 38 paesi, tra i quali la Mauritania, il Sudan e la Nigeria settentrionale che prevedono per l'omosessualità addirittura la pena di morte, e l'approvazione di una legge siffatta in Uganda potrebbe avere effetti a cascata su altri paesi africani, nei quali le persone sono o potrebbero essere perseguitate in ragione del loro orientamento sessuale;

la Risoluzione del Parlamento europeo del 17 dicembre 2009 sulla Proposta di legge contro l'omosessualità in Uganda sottolinea che “_l'orientamento sessuale è una questione che rientra nella sfera del diritto individuale alla vita privata, garantito dalla legislazione internazionale in materia di diritti umani, secondo cui l'uguaglianza e la non discriminazione dovrebbero essere promosse e la libertà di espressione garantita; condanna pertanto la proposta di legge del 2009 contro l'omosessualità”_;

la Risoluzione, inoltre, chiede: _“alle Autorità ugandesi di non approvare la proposta di legge e di rivedere la legislazione nazionale allo scopo di depenalizzare l'omosessualità”_;

Chiede di sapere:

\- se il Governo sia a conoscenza dei fatti sopra menzionati.

\- se alla luce di quanto riportato, il Governo italiano abbia intenzione di adottare, sia in sede bilaterale sia in quella internazionale, iniziative volte a spingere il Governo ugandese - vincolato dall’Accordo di Cotonou del 23 giugno 2000 al rispetto dei diritti umani universali - a contrastare i reati di omofobia che mettono a rischio la vita dei cittadini ugandesi gay o lesbiche.

\- se il Governo italiano non intenda adoperarsi, sia in sede bilaterale sia internazionale, per chiedere alle Autorità ugandesi di non approvare la proposta di legge che mira a discriminare le persone omosessuali.