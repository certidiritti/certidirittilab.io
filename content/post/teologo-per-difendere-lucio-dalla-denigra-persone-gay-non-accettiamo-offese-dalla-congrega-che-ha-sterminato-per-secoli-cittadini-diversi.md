---
title: 'Teologo per difendere Lucio Dalla denigra persone gay: non accettiamo offese dalla congrega che ha sterminato per secoli cittadini ''diversi'''
date: Thu, 08 Mar 2012 10:57:25 +0000
draft: false
tags: [Politica]
---

Non ci interessa il dibattito sull'omosessualità di Lucio Dalla, ma non accettiamo le offese e le denigrazioni da parte di uomini di quella chiesa che in passsato condannava al rogo gli eretici e le persone gay.

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Roma, 7 marzo 2012

Abbiamo deciso di non entrare finora nel dibattito, a volte molto ipocrita, a volte più serio, sulla vita personale di Lucio Dalla e sulla sua scelta di non rendere pubblica la sua omosessualità, di cui peraltro quasi tutti sapevano.  
Sia però ben chiaro che non accettiamo offese e denigrazioni da uomini di chiesa che, in nome della fede presunta o reale di Lucio Dalla, della sua ultima, penultima o prima confessione, del suo credo o non credo religioso, si permettano di scrivere, come ha fatto tal padre Giorgio Maria Carbone, domenicano, docente di bioetica e teologia morale alla Facoltà di Teologia dell’Emilia Romagna, che ‘visto che gli italiani devono essere ‘educati’ ad accettare le nozze gay, serve una vittima della presunta ipocrisia italiana, allora Dalla è ridotto a omosessuale’.  
   
A proposito di ipocrisie ci viene da dire: “da quale pulpito e congrega vien la predica”. Se c’è qualcuno che campa di ipocrisie e privilegi di ogni dove è proprio la chiesa cattolica,  con le sue giustificazioni religiose che utilizza per assolvere anche i peccatori più accaniti,  compresi quelli obbligati a vivere la loro condizione di nascosto.

Le persone omosessuali sono persone come tutte le altre, non certo ‘ridotte’ come vorrebbe l’emerito teologo. Il caso Dalla dimostra semmai di come sul piano civile non vi siano tutele per il partner che non avrà nemmeno diritto ad uno straccio di eredità. Il grande emerito teologo si guarda bene dal dire queste verità.  
   
Presto o tardi il matrimonio per le persone dello stesso sesso arriverà anche in Italia, nonostante il Vaticano;  per noi non c’è bisogno di fare vittime come hanno fatto invece i Domenicani con la Santa Inquisizione nel corso dei secoli in nome del loro fondamentalismo religioso che hanno causato odio, violenza, morte, stermini, spesso di persone innocenti, che avevano la colpa di essere considerate  'diverse'.

**iscriviti alla newsletter >[  
](newsletter/newsletter)[](newsletter/newsletter)[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**