---
title: 'TRIBUNALE DI MILANO PER LA PRIMA VOLTA SU UN CASO DI SEPARAZIONE OMOGENITORIALE'
date: Fri, 12 Feb 2010 00:32:34 +0000
draft: false
tags: [Senza categoria]
---

Tribunale per i Minori di Milano su un caso di separazione omogenitoriale: la co-mamma è inequivocabile figura genitoriale, ma non potrà vedere i suoi figli perchè "al momento stanno bene".  
IL DIRITTO DEL PIU' FORTE

Milano. Il Tribunale per i Minorenni è stato recentemente chiamato ad esprimersi, per la prima volta, su un caso di separazione omogenitoriale.  
Una coppia di donne, dopo aver avuto due bambini con l'aiuto di una coppia di amici, si divide. La madre biologica decide, come la legge sembra permetterle, di interrompere i rapporti tra l'altra e i figli. Ma la co-mamma si rivolge al Tribunale per i Minori: anche se lei non può aprire un procedimento, decide di farlo il Pubblico Ministero in vece sua, riscontrando "come emerge con chiarezza dagli elementi raccolti \[...\] la comune scelta di condividere la nascita e la crescita dei bambini \[...\] secondo uno schema tipicamente famigliare".  
Dopo un periodo di consulenze tecniche con i minori e gli adulti coinvolti, il Tribunale non può che constatare l'inequivocabile ruolo genitoriale svolto da entrambe le donne. La determinazione dei ruoli genitoriali è individuata a tal punto che i periti del Tribunale riscontrano l'emersione della Sindrome da alienazione genitoriale (o PAS, dall'acronimo di Parental Alienation Syndrome, detta anche sindrome di Gardner), sindrome tipica delle separazioni problematiche, causata dalla pressione del genitore affidatario nel voler distruggere, agli occhi dei figli, l'immagine dell'altro.  
Il Tribunale però, visto che al momento la situazione dei minori, escludendo la problematica legata alla separazione, è positiva, decide di non dover procedere e di archiviare il caso.  
Pur constatando il coraggio del Tribunale per i Minori che, in assenza di qualunque norma a legittimare le richieste del genitore di fatto, decide di proseguire per accertare il reale vissuto affettivo dei bambini dopo la separazione, rimane l'ingiustizia subita da due minori che non vedono difeso il rapporto con una delle loro due figure genitoriali, laddove in una situazione tradizionale quel rapporto sarebbe stato difeso per legge.  
Ci domandiamo ora: se i bambini coinvolti nelle separazioni sono protetti dalle leggi perché questi bambini, pur soffrendo della stessa situazione, vengono lasciati in balia dei fatti? Perché se solo si possono "definire nella norma allo stato della valutazione effettuata" non vengono riconosciute loro le tutele che per altri sarebbero ovvie?  
Non ci resta che constatare che un diritto negato a un adulto è un diritto negato al minore a lui legato e che l'interesse del bambino purtroppo raramente riesce ad essere superiore alle debolezze della legge.  
Adesso che un Tribunale italiano ha toccato con mano come due genitori dello stesso sesso funzionino, nel bene e nel male, come due genitori tout court, aspettiamo che il legislatore inizi a porsi il problema della tutela di questi minori.  
Per quanto riguarda questo caso in particolare auspichiamo invece che il Tribunale per i Minorenni, nel monitorare la situazione come è scritto che farà, arrivi a restituire ai bambini, con i tempi e i modi che riterrà, quella inequivocabile figura genitoriale che, come constatato dai suoi periti, è stata così ingiustamente sottratta loro.  
  
  
Il decreto in sintesi  
Con ricorso depositato presso il Tribunale per i Minorenni la co-mamma  
  
"premesso di avere avuto una relazione sentimentale con X iniziata nel corso del 1993, di aver convissuto con la stessa dal 1994 al dicembre 2003 nonostante diverse difficoltà di coppia e alcuni brevi allontanamenti;  
di essere giunte, insieme e ciascuna attraverso un proprio percorso, alla determinazione di avere dei figli;  
che da questo loro desiderio erano nati Z e W - i quali portavano, come secondo nome, anche i loro rispettivi nomi di battesimo;  
che si era sempre adoperata per la cura dei bambini creando con loro un forte legame affettivo;  
che la separazione della coppia ma l'inequivocabile legame tra Y e i piccoli aveva portato alla comune decisione di regolamentare le modalità di incontro con Z e W e di quantificare il contributo di Y per il mantenimento dei bambini, collocati presso la madre;  
che con il tempo la madre dei minori aveva posto difficoltà nel regolare svolgersi delle visite di Y ai bambini sino a giungere prima a incontri "protetti" e poi alla definitiva chiusura di ogni relazione con gli stessi;  
che la scelta della madre - sulle cui capacità genitoriali e sulla cui adeguatezza non venivano sollevati dubbi - stavano creando disagio e difficoltà relazionali ai bambini"aveva chiesto "l'affidamento congiunto dei minori con collocamento presso la madre biologica".

  
Il TM aveva dichiarato il "difetto di legittimazione attiva di Y rispetto alle domande formulate" ed aveva disposto la trasmissione degli atti al PM perché valutasse la richiesta di un procedimento ex artt 330 e segg cc  
In particolare il Tribunale aveva osservato che la ricorrente non era titolare del diritto potestativo di ottenere una decisione nel merito in quanto "è pacifico che la titolarità della potestà spetti soltanto ai 'genitori', presupponendo un rapporto di filiazione, biologica o legale (si vedano le ipotesi di adozione), tra i soggetti..."  
  
Il PM ha quindi chiesto procedersi: "sentite la madre biologica dei minori e la signora Y" (si noti come in più riprese si usi nel decreto il termine "madre biologica" per designare una delle due parti, e non ci si limiti all'uso di madre, proprio perché lo stesso Tribunale vede in questa famiglia la presenza di una seconda figura genitoriale di sesso femminile), "il TM ha disposto CTU al fine di verificare lo stato psicofisico dei minori, la natura e la qualità della relazione con la madre e con la Y".  
  
E' interessante notare come all'obiezione della difesa della madre biologica che vedeva "un ingiustificato accanimento \[...\] ad esclusiva tutela dell'adulto Y" il Tribunale per i Minori risponda di "operare nell'esclusivo interesse dei minori \[...\] al fine di verificare lo stato psicologico dei minori e la natura delle relazioni dagli stessi intrattenute con figure non parentali (\[...\] perché la figura della Y è stata 'vissuta' dai minori sin dalla loro nascita nell'ambito di una relazione di coppia che ha visto le due donne porsi, inequivocabilmente, come figure genitoriali in assenza però di qualsiasi possibilità di riconoscimento legale) ma affettivamente legate a Z e W.  
Ed è proprio questo inequivocabile legame affettivo - verificato anche dai CTU - che ha portato il TM a consentire la partecipazione al procedimento della signora Y, ben consapevole del fatto che, da un punto di vista strettamente legale, non vi è oggi alcuna possibilità di riconoscimento di una legitimatio ad causam di un soggetto non legato da vincolo alcuno al minore \[...\] ma altrettanto consapevole della pacifica e innegabile condivisione da parte di Y e dei minori \[...\] di importanti momenti di crescita.  
Condivisione che, come emerge con chiarezza dagli elementi raccolti, nasce dalla pacifica e prolungata convivenza tra le due donne, dalla comune scelta di condividere la nascita e la crescita dei bambini (i quali portano, infatti, i nomi di entrambe, Y e X) secondo uno schema tipicamente familiare (tanto che alla cessazione della convivenza la coppia ha sottoscritto accordi "di separazione" con riguardo al diritto di visita dei minori e al contributo per il loro mantenimento)".  
  
I CTU concludono (il sottolineato è del Tribunale per i Minori):  
"questi bambini non portano il senso di un conflitto attraverso il sintomo, sono bambini che funzionano, pertanto si può derivare che gli aspetti di accadimento e di fornitura di strumenti relazionali e cognitivi siano stati dati in congrua dose  
La questione della separazione delle due figure genitoriali, X e Y, invece, ha indubbiamente provocato un graduale, ma massiccio sovvertimento dell'omeostasi precedente. Segni di questa vicenda sono clinicamente rilevabili \[...\] In altre parole si può ipotizzare l'emersione di quella che comunemente viene chiamata Sindrome di Gardner \[...\]  
Il loro problema, se così lo possiamo definire, è inquadrabile all'interno della conflittualità separativa che ha colpito le due figure, che, da sempre \[...\] sono state figure di riferimento genitoriale."  
  
Continua il TM:  
"In conclusione il procedimento deve essere archiviato \[...\] non essendo emerso che l'assenza di rapporti (tra Y e i due bambini) sia causa di quel grave pregiudizio che solo giustificherebbe l'intervento del TM ai sensi degli artt 330 e segg cc.  
Ritiene peraltro il TM debba essere dato incarico ai ss (Servizi Sociali) competenti di monitorare la situazione dei minori \[...\]"  
  
FAMIGLIE ARCOBALENO  
www.famigliearcobaleno.org  
  
Ufficio stampa: Maria Silvia Fiengo 348-3833239  
stampa@famigliearcobaleno.org  
msfiengo@tiscali.org