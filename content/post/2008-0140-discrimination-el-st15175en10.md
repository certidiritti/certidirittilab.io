---
title: '2008-0140-Discrimination-EL-ST15175.EN10'
date: Wed, 24 Nov 2010 11:33:45 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 24 November 2010

Interinstitutional File:

2008/0140 (CNS)

15175/10

LIMITE

SOC 682

JAI 866

MI 389

  

  

  

  

  

NOTE

from :

General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

13883/10 SOC 561 JAI 759 MI 317

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached a note from the EL delegation.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Presidency questionnaire concerning housing**

**\[Document 13883/10\]**

**EL replies**

**1\.** **Situation in Member States**

_Both the Racial Equality Directive (2000/43/EC) and the Gender Goods and Services Equality Directive (2004/113/EC) cover housing available to the public in their scope. Several MS have gone beyond these two grounds in their protection against discrimination._

i.                   To what extent does your national legislation against discrimination apply to housing?

_The two relevant laws , namely Law 3304/2005, that has transposed  the Racial Equality Directive (2000/43/EC), and Law 3769/2009, that has transposed the Gender Goods and Services Equality Directive (2004/113/EC),  apply to the public and the private sector of the economy. The abovementioned legal framework covers housing available to the public for the grounds of “ethnic_ _or racial_ _origin” and sex._ _There is no distinction between private and public/social housing._

ii.     Do you distinguish (for this purpose) between private and public/social housing?

_There is no distinction between private and public/social housing_.

**c)** **Do you have statistics on the number of cases of discrimination in the area of housing, and if so, what is the most prevalent ground?**

_No statistics are available._

**2\.** **Social housing**

i.     Do you consider social housing to be a service (for the purposes of EU law)?

_The_  _Labour_  _Housing_  _Organization_ _(__ΟΕΚ__)_ _is_  _a_  _body_  _exclusively_ _funded_  _be_  _employees__’_ _and_  _employers__’_ _contributions_ _(__no_  _statutory_  _funding__),_ _on_  _a_  _reciprocity__-__basis__._ _Therefore__,_ _eligible_  _for_  _OEK__’__s_  _services_  _are_  _only_  _employees_  _as_  _well_  _as_  _pensioners_  _who_  _pay_  _or_  _have_  _paid_  _contributions_  _to_  _OEK__._  _In other words, OEK does not offer_ _universal_  _coverage__._ _However__,_ _being_  _the_  _only_  _public_  _body_  _that_  _provides_  _decent_  _housing_  _to_  _the_  _less_  _privileged_  _employees_  _and_  _pensioners__,_ _OEK_  _can_  _be_  _considered_  _the_  _only_  _social_  _housing_  _agency_  _in_  _Greece._

_ΟΕΚ_  _beneficiaries_  _are__:_ _a__)_ _all_ _blue and white-collar_ _employees_  _working_  _in_ _the private sector_ _who_  _are_  _insured_  _at_  _a_  _main_  _social_  _security_  _organization_  _and_  _pay_  _contributions_  _to_  _ΟΕΚ__,_ _b__)_ _employees working in Public Enterprises and the wider public sector who pay contributions to OEK, and c__)_ _pensioners of the above categories._

_Prerequisite_  _for_  _housing_  _eligibility_  _is_  _that_  _beneficiaries_  _do_  _not_  _already_  _own_  _a_  _house_  _or_  _have_  _enough_  _assets to buy a house on their own. Additionally, they must have completed a certain number of working days; the number of days required differs depending on their family status._

_In principle “social housing” could be considered as a service._

b)         If not, do you consider it to be a part of the social protection system?

**3\.** **Private life**

In some cases, the principle of equality might be in conflict with the right to private life, in particular in the area of housing (e.g. renting out a room in your own apartment).

i.                   Is this better taken into account by an exclusion of transactions in the area of private life from the scope of the Directive, or by an exclusion of activities which are not commercial or professional?

_Non-commercial or non-professional activities should be excluded. Thus_ _private_ _actions_ _and,_ _consequently,_ _private_ _life_ _will_ _not_ _be_ _undermined._ _In any case, in order to ensure that the right to equality will not pose a challenge to the right to private life, the relevant wording in the last paragraph of Article 3(1) of the original Proposal (doc. 11531/08) should be retained._

b)        How is commercial/professional housing defined in your national legislation?

_The term commercial/ professional housing in a broad sense is mostly used in the taxation legislation__. It refers to the space, building or part of a building in which a professional activity is carried out. In the Hellenic_ _urban_  _development_  _legislation__,_ _which_  _also contains_ _accessibility provisions__,_ _the_  _term commercial/professional housing is not used but buildings are distinguished on a usage basis (e.g. residence, tourist facilities, administration, education, industrial facilities)._

**4\.** **Disability - reasonable accommodation**

i.                   In your view, what should the obligation to provide reasonable accommodation mean for providers of housing?

_The obligation for reasonable accommodation in the case of existing housing that is not yet accessible to persons with disabilities should be seen in relation to the disproportionate burden that the adjustment would impose. Specifically, the difficulty of implementing a simple solution (in technical and economic terms) for ensuring accessibility (horizontal or vertical) for persons in wheelchairs should be noted. Additionally, large-scale and costly measures are not advisable due to the natural ageing of the raw materials of buildings as well as to the  limited lifespan of some buildings._

_Another parameter is the very rough natural terrain in Greece that results in the development of urban settlements on steeply sloping ground. This parameter often makes the construction of pavements and other communal areas, as well as the construction of building entrances with gentle inclines offering access for persons in wheelchairs, extremely difficult. Simple adjustments that facilitate the access of people with other types of disability, such as blind people, for example the construction of handrails in stairways, are acceptable._

_In general, this issue should be seen in a flexible way and the obligation for necessary adjustments should be regulated by the national legislation in accordance with the specific characteristics of each Member State._

**b)** **Is there legislation on the accessibility of buildings (private as well as public/social) in your country?**

_Yes there is. More specifically, the current legal framework provides the obligation of ensuring horizontal (ramp with a suitable gradient and corridors that allow the passing of a wheel chair) and vertical accessibility to all  buildings  up to three floors that must have elevators of sufficient size for a wheelchair (article 28, L.2831/2000. This provision concerns all buildings constructed after the aforementioned law came into force, without distinction between public and private buildings._

_It_ _should be noted_ _that buildings in areas with steep inclines, or small plots are excluded from these provisions due to the objective difficulty of constructing elevators in sufficient size to facilitate wheelchair access._

_Also in some categories of new buildings (public buildings or those of general public interest, schools, offices ) there are provisions for horizontal and vertical accessibility of disabled people as well as the provision for accessible sanitary areas for them._

_Furthermore, according to the Ministerial Dec. 52487/ 16-11-2001, specific regulations apply for existing  public buildings that are under renovation or in cases that people with disabilities work there, in order to facilitate access for these persons._

**5\.** **Disability - accessibility**

i.        What should the obligation to provide for accessibility measures encompass in the area of housing?

_Please_  _refer_  _to_  _question_ _3,_ _par__._ _B._ _The_ _abovementioned_ _legal_ _framework_ _sets_ _the_ _minimum_ _accessibility_ _requirements._

**ii.           Should there be a difference between old and new buildings?**

_There should be a difference; stricter requirements should be imposed in cases of new buildings, while less stringent measures should be applied to old existing buildings, priority being given to buildings that are used more often by the public._

**Do you support immediate applicability with respect to new buildings?**

_A differentiation on usage should be implemented._ _Indigenous_  _and_  _objective_  _difficulties_  _as_  _regards_  _accessibility_  _for_  _wheelchair users_  _also_  _exist. This parameter is recognized in the existing legal framework, given Greece’s natural terrain which has steep inclines as well as relatively small building plots._

_In_  _any_  _case__,_ _it_  _is_  _a_  _fact_  _that_  _the_  _majority_  _of_  _the_  _existing_  _buildings_  _have_  _been_  _built_  _prior_  _to_  _the_  _coming_  _into_  _force_  _of_  _the_  _current_  _legal_  _framework_  _on_  _accessibility_  _and_  _therefore_  _no_  _provisions_  _have_  _been_  _made_ _for_  _people_  _with_  _disabilities__._ _Therefore__,_ _a_  _major_  _issue_  _emerges_  _as_  _regards_  _the_  _necessary_  _resources_  _and_  _the_  _time_  _needed_  _for_  _the_  _adjustments_  _to_  _the_  _needs_  _of_  _people_  _on_  _wheelchairs._  _T__he_  _adjustments_  _needed_  _for_  _other_  _categories_  _of_  _people_  _with_  _disabilities_  _are_  _considered_  _more_  _simple_  _in_  _terms_  _of_  _both_  _resources_  _and_  _time__._

_According to the official estimates, more time (10 to 12 years for example) should be given for modifications to existing buildings, and an exclusion clause should be provided for some building categories (e.g. depending on the public usage or the ground inclination)._

**i.** Should there be a difference between the obligations for private and public/social housing respectively?

_No_

e)  Are there public funds available in your country for the adaptation of housing (private as well as public/social)?

_No_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_