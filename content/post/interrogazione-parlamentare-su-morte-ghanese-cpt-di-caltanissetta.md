---
title: 'INTERROGAZIONE PARLAMENTARE SU MORTE GHANESE CPT DI CALTANISSETTA'
date: Tue, 08 Jul 2008 17:51:25 +0000
draft: false
tags: [Comunicati stampa]
---

Dopo la visita ispettiva di Rita Bernardini, deputata radicale - Pd e Sergio Rovasio, Segretario Associazione Radicale Certi Diritti, è stata depositata oggi un'interrogazione parlamentare sulla morte del giovane avvenuta nella notte tra il 29 e il 30 giugno scorso.

[http://www.radicali.it/view.php?id=125802](http://www.radicali.it/view.php?id=125802)