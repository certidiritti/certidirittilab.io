---
title: 'Patente negata a gay: interrogazione anche al Parlamento europeo'
date: Thu, 12 May 2011 08:25:07 +0000
draft: false
tags: [Europa]
---

**Allertati da Certi Diritti, gli eurodeputati del gruppo ALDE Marietje Schaake, Sophie In't Veld (entrambe del partito D66) e Gianni Vattimo hanno depositato oggi una interrogazione sul caso di Cristian Friscina al PE rivolta alla Commissione europea.**

  
  
Il testo dell'interrogazione:  
  
Omofobia in Italia  
  
L'associazione italiana Certi Diritti ha denunciato l'11 maggio il fatto che, dopo un caso simile accaduto a Catania e che ha portato a sentenze di condanna dei Ministeri responsabili, un ulteriore caso di rifiuto della concessione di una patente di guida ha avuto luogo a Brindisi, Italia, sulla base dell'orientamento sessuale. Quando Cristian Friscina ha chiesto il rinnovo della sua patente emessa nel 1999, la sua richiesta é stata rigettata sulla base di documenti nel suo fascicolo che parlavano di "patologie che possono pregiudicare la sicurezza nella guida", a causa del fatto che aveva dichiarato di essere omosessuale in occasione del suo servizio di leva, e nonostante avesse passato positivamente tutti i controlli psicofisici. Cristian é ora privato della sua patente di guida, con grave pregiudizio della sua possibilità di muoversi, recarsi al lavoro, etc.  
Le istitutioni europee celebreranno il 17 maggio la giornata mondiale contro l'omofobia - ma purtroppo le discriminazioni e aggressioni omofobiche continuano ad avere luogo sul territorio della UE.  
E' la Commissione a conoscenza dei fatti summenzionati? chiederà la Commissione informazioni alle autorità italiane? quali azioni intraprenderà la Commissione per assicurare che tali atti di discriminazione basata sull'orientamento sessuale non abbiano più luogo, né in Italia, né in altri Stati membri? quando lincenzierà la Commissione la Roadmap contro l'omofobia e per i diritti delle persone LGBT, comprendente misure legislative e non-legislative, al fine di assicurare il rispetto, la protezione e la promozione dell'eguaglianza e dei diritti umani delle persone LGBT?  
  
  
Homophobia in Italy  
  
  
The Italian association Certi Diritti has denounced on 11 May 2011 the fact that, after a similar case that happened in Catania and resulted in a judgment condemning the responsible Ministries, another case of denial in the granting of a driving licence has taken place in Brindisi, Italy, on the basis of sexual orientation. When Cristian Friscina asked for the renewal of his 1999 driving licence, his request was rejected as documents in his file say that he has "pathologies that could prejudice the driving security" due to the fact that he had declared to be homosexual when being in the army, although he passed all psychophysical checks. Cristian is now deprived of his driving licence, with great prejudice to his possibilities to move around, go to work, etc.  
  
EU institutions will celebrate the International Day Against Homophobia on the 17th of May - still homophobic discriminations and aggressions keep on taking place on the EU soil.  
  
Is the Commission aware of these facts? Will it ask for information from the Italian authorities on the above mentioned facts? Which actions will it take to ensure that similar events of discrimination based on sexual orientation do not take place anymore, neither in Italy, nor in other Member States? When will the Commission issue its Roadmap against Homophobia and for LGBT rights with specific legislative and non-legislative measures to ensure the respect, protection and promotion of the equality and of the human rights of LGBT persons?