---
title: 'CORTE EUROPEA: GARANTIRE A PARTNER PENSIONE. QUANDO IN ITALIA?'
date: Fri, 04 Apr 2008 01:27:41 +0000
draft: false
tags: [Comunicati stampa, diritti, europa, partner, pensione]
---

CORTE EUROPEA: LA PENSIONE INTEGRATIVA ANCHE AL PARTNER CONVIVENTE SUPERSTITE. L’EUROPA GARANTISCE SEMPRE PIU’ DIRITTI. L’ITALIA SI ADEGUI, ORMAI E’ QUASI ULTIMA.

Comunicato Stampa di Sergio Rovasio, Segretario dell’Associazione Radicale Certi Diritti:

“La Corte di giustizia delle Comunità europee ha stabilito, in una storica sentenza adottata ieri, 1 aprile, che gli artt. 1 e 2 della Direttiva 2000/78/CE, sulla parità di trattamento nell’impiego e nell’occupazione, precludono norme nazionali secondo le quali, alla morte del partner registrato dello stesso sesso, il partner sopravvissuto non è ammesso a godere della pensione di reversibilità (Corte di Giustizia, Maruko c. Versorgungsanstalt der deutschen Bühnen. Causa C‑267/06).

Con questa sentenza si supera una grave disparità di trattamento tra coppie conviventi e coppie sposate. Viene garantito al partner regolarmente unito in unione civile la stessa pensione integrativa prevista per il coniuge superstite legato da matrimonio, ancora una volta un organismo europeo precisa i parametri di uguaglianza per le persone unite da relazioni di convivenza previsti dalle legislazioni nazionali.

L’Italia come al solito è fanalino di coda: non solo qui ci scordiamo l’applicazione di questa storica sentenza, purtroppo siamo ancora senza una legge che regolamenti le unioni civili. Con i Dico e i Cus si era cominciato a balbettare qualcosa, forse è il caso che si cominci a fare qualcosa di concreto, come sta avvenendo in tutte le democrazie occidentali. Rischiamo di rimanere troppo indietro e restare allineati ai paesi teocratici fondamentalisti”.

L’Associazione Radicale Certi Diritti valuterà bene come investire le due Corti europee, in campi diversi, di casi che mettano a nudo la miseria e l’ingiustizia della situazione italiana”.