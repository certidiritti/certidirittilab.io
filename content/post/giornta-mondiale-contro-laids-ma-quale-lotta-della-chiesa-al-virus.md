---
title: 'GIORNTA MONDIALE CONTRO L''AIDS: MA QUALE LOTTA DELLA CHIESA AL VIRUS?'
date: Sun, 29 Nov 2009 12:12:52 +0000
draft: false
tags: [Comunicati stampa]
---

**GIORNATA MONDIALE CONTRO L’AIDS: OCCORREREBBE VALUTARE QUALI SONO LE RESPONSABILITA’ DELLA CHIESA NELLA DIFFUSIONE DEL VIRUS ANZICHE’ DIFFONDERE FRASI DI RITO E INFARCITE DI IPOCRISIA.**

**L’AIDS SI COMBATTE CON L’INFORMAZIONE E IL PRESERVATIVO.**

_**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti**_

“Quello che ha detto oggi il Papa in occasione della giornata mondiale contro l’Aids ha dell’incredibile. Non comprendiamo come sia possibile sostenere che ‘la chiesa non cessa di prodigarsi per combattere l’Aids’ quando lo stesso Papa, in occasione del suo viaggio in Africa nel marzo 2009, ebbe a dire che “l’Aids non si può superare con la distribuzione dei preservativi che al contrario aumentano il problema”. Tesi antiscientifica, ispirata al credo religioso e che semmai avrà di già aggravato una situazione di emergenza della malattia che ha ucciso in Africa dai primi anni ’80 ad oggi più di 25 milioni di persone, principalmente nella zona sub-sahariana. Sono almeno 22,5 milioni le persone che vivono contagiate dal virus HIV in quella zona.

Quasi nessuno in Italia avrà il coraggio di sottolineare questa grave contraddizione vaticana che si riverbera sulla nostra classe politica con conseguenze disastrose nella società. La totale mancanza di informazione ed educazione sessuale, così come la mancata pronuncia della parola ‘preservativo’ nelle tv e nelle scuole italiane, dimostra la miseria e la studipidità che ci circonda e che permette al virus dell’Aids di diffondersi. E’ forse bene ricordare che in Italia ci sono tra le 150 e le 180.000 persone sieropositive, 22.000 persone con Aids conclamato e che fino ad oggi sono morte per il virus quasi 35.000 persone. Ovviamente non è in corso nessuna campagna nazionale sul preservativo, l’unico strumento che oltre all’informazione combatte quasi al 100% il virus.

Ci auguriamo che prima o poi venga “quantificato” il danno prodotto da proclami e tesi antiscientifiche, ispirate da un credo religioso, che alimentano la diffusione del virus dell’Aids in Africa e nel mondo. Altro che lotta all’Aids.

E’ bene ricordare tutto questo in occasione della giornata mondiale della lotta contro l’Aids, giusto per non essere complici dell’ipocrisia che sembra essere diventata la vera bibbia nel nostro paese”.