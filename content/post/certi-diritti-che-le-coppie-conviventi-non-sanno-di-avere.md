---
title: 'CERTI DIRITTI CHE LE COPPIE CONVIVENTI NON SANNO DI AVERE. Edizioni Stampa Alternativa'
date: Tue, 20 Mar 2012 05:44:24 +0000
draft: false
tags: [Diritto di Famiglia]
---

CERTI DIRITTI CHE LE COPPIE CONVIVENTI NON SANNO DI AVERE

Per vivere consapevolmente in coppia o in attesa di giuste nozze

Autori: Bruno de Filippis, Gian Mario Felicetti  
Gabriella Friso, Filomena Gallo  
Editore: Stampa Alternativa  
Collna: Eretica  
pp. 112  
prezzo: 12 euro

Le coppie conviventi, non unite nel “sacro” vincolo del matrimonio, vengono definite “coppie di fatto”.

“Di fatto” e non “di diritto”, solo perché legislatori inadempienti non le hanno mai regolate, diversamente da quanto avviene in qualsiasi altra parte del mondo  
civile.

Concepito come un manuale di sopravvivenza, il libro indica gli strumenti legalmente utilizzabili per realizzare, almeno in parte, i diritti di coppia: in che modo tutelarsi per restare insieme nel caso la vita conduca uno dei due in ospedale o in carcere, per conservare la casa, ottenere risarcimenti o congedi, stipulare convenzioni e assicurazioni, garantire che i figli non subiscano danni e discriminazioni.

Per le coppie composte da persone dello stesso sesso, sono suggeriti soluzioni e rimedi che consentano importanti diritti.

Il libro sarà disponibile in tutte le librerie a partire dal 21 marzo 2012.

TUTTI I PROVENTI E I DIRITTI D'AUTORE DEL LIBRO SARANNO DESTINATI ALL'ATTIVITA' DELL'ASSOCIAZIONE RADICALE CERTI DIRITTI.

**[ACQUISTA IL LIBRO ONLINE >](http://www.stampalternativa.it/libri/978-88-6222-281-5/bruno-de-filippis-/certi-diritti-che-le-coppie.html)**