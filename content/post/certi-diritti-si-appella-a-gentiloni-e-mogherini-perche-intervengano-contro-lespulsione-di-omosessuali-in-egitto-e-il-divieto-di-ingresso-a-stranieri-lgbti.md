---
title: 'Certi Diritti si appella a Gentiloni e Mogherini perché intervengano contro l''espulsione di omosessuali in Egitto e il divieto di ingresso a stranieri LGBTI'
date: Fri, 17 Apr 2015 07:28:13 +0000
draft: false
tags: [Africa]
---

[![Flag_of_Egypt.svg](http://www.certidiritti.org/wp-content/uploads/2015/04/Flag_of_Egypt.svg_.png)](http://www.certidiritti.org/wp-content/uploads/2015/04/Flag_of_Egypt.svg_.png)"La notizia che l'Egitto vieterà l'ingresso nel Paese ai gay e riconoscerà alla polizia l''autorità di espellere gli stranieri omosessuali è sconcertante è denota una ulteriore preoccupante involuzione autoritaria del Paese". Questa la reazione di Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, che continua: "Già la Repubblica di Trinidad e Tobago contiene un divieto all'ingresso di gay e lesbiche nella legge sull'immigrazione che tuttavia non sembra essere implementato, visto che Elton John ha potuto tenere il suo concerto nel 2007. L'espulsione di un cittadino libico accusato dalla polizia di essere omosessuale, fa presagire ben altra determinazione da parte del governo egiziano". Guaiana conclude: "La decisione egiziana rappresenta una chiara violazione dell'articolo 13 della Dichiarazione universale dei diritti umani ("ogni individuo ha diritto alla libertà di movimento e di residenza entro i confini di ogni Stato") votata a suo tempo anche dall'Egitto. Ci appelliamo al ministro degli Esteri italiano, Paolo Gentiloni, e all'Alto rappresentante dell'Unione per gli affari esteri e la politica di sicurezza, Federica Mogherini, affinché intervengano immediatamente sul governo egiziano".

Comunicato stampo dell'Associazione Radicale Certi Diritti