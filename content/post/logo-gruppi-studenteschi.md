---
title: 'Logo Gruppi Studenteschi'
date: Fri, 27 Mar 2009 07:13:34 +0000
draft: false
tags: [Senza categoria]
---

Per merito del nostro grafico Matteo di Grande, i Gruppi Studenteschi dell'associazione Certi Diritti hanno finalmente un logo per essere riconosciuti.

Un ringraziamento particolare a Matteo per le sue grandi capacità comunicative ed espressive che ci ha dimostrato attraverso un simbolo pieno di significati.