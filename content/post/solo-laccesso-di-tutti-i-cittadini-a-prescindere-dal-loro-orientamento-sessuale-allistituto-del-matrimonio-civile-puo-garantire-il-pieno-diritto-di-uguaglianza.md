---
title: 'Solo l''accesso di tutti i cittadini, a prescindere dal loro orientamento sessuale, all''istituto del matrimonio civile può garantire il pieno diritto di uguaglianza'
date: Wed, 16 May 2012 09:09:38 +0000
draft: false
tags: [Matrimonio egualitario]
---

Roma, 15 maggio 2012

comunicato stampa Associazione radicale Certi Diritti

E' triste misurare la differenze che corrono tra il segretario del PD e Barak Obama. A parte il nome, i democratici italiani e quelli statunitensi non hanno proprio nulla in comune. Bersani non solo non ha ascoltato il Presidente USA che ha chiaramente compreso come "la negazione dell'eguaglianza nel matrimonio significa che \[gli omosessuali\] non sono ancora considerati davvero cittadini a pieno titolo", ma non ha ascoltato nemmeno le parole della Corte Costituzionale che, nelle motivazioni alla sentenza 138/2010 ha escluso chiaramente che il riconoscimento giuridico delle coppie dello stesso sesso possa avvenire "soltanto" attraverso un'equiparazione di tali unioni al matrimonio, riconoscendone questa possibilità come costituzionale.

Solo l'accesso di tutti i cittadini, a prescindere dal loro orientamento sessuale, all'istituto del matrimonio civile può garantire il pieno diritto di uguaglianza che è il vero nocciolo della questione.

Dopodiché occorre prevedere altre forme di regolamentazione degli affetti che acquistano il loro senso proprio nella diversità dal matrimonio civile e in una maggiore flessibilità. Ecco perché l'Associazione radicale certi diritti propone una riforma complessiva del diritto di famiglia che comprenda il matrimonio omosessuale, un riconoscimento delle unioni libere e persino di intese di solidarietà e di comunità intenzionali.

Visto che in Commissione Giustizia alla Camera la discussione è iniziata solo sulle Unioni civili, noi chiediamo ancora una volta che si discuta di una riforma più complessiva del diritto di famiglia o che comunque si inseriscano nella discussione almeno le proposte di legge riguardanti il matrimonio tra persone dello stesso sesso.