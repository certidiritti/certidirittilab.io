---
title: 'PROSTITUZIONE: GOVERNO ALFIERE IPOCRISIA E NON SOLUZIONE'
date: Fri, 12 Sep 2008 14:16:52 +0000
draft: false
tags: [Comunicati stampa]
---

Poretti: Prostituzione e nuova legge. Il Governo alfiere dell'ipocrisia e delle non-soluzioni

Roma, 11 settembre 2008

• Intervento della senatrice Donatella Poretti, parlamentare Radicale-Partito Democratico

Fa effetto sentire il ministro della Pari Opportunita', Mara Carfagna, che, nel presentare il ddl approvato dal Governo per contrastare la prostituzione, dice "Mi fa orrore, non comprendo chi vende il proprio corpo...". Fa effetto perche' proviene da una persona che e' stata nel mondo dello spettacolo (e non mi sembra abbia fatto opera di pentimento), dove il proprio corpo viene venduto come base dell'offerta. Non si tratta certamente di una vendita per rapporti sessuali, ma che questa vendita poi possa ispirare sessualita'... negarlo e' bendarsi gli occhi, ipocrisia. La medesima che ammanta tutto il disegno di legge, dove l'operazione base e' nascondere la spazzatura sotto il tappeto.

Le prostitute vengono lasciate a se stesse, colpendo solo il mercato che si manifesta agli occhi dei benpensanti di passaggio. Benpensanti che come il presidente della commissione giustizia del Senato, Filippo Berselli, non negano di essere stati svezzati al sesso dal rapporto meretricio (chissa' se per strada o in una casa chiusa...).

E' l'Italia dei quaquaraqua che si ritrova in questo disegno di legge, con le solite vittime, cioe' i piu' deboli, quelli che non devono esistere per la legge e se esistono per l'umaninta', e' bene che vivano nella penombra, senza diritti da affermare e rivendicare, senza protezione che non l'istinto di sopravvivenza, istinti contro il quale il potere, se violi la legge e sei ancora piu' debole, cioe' extracomunitaria, ti rispedisce nell'inferno da cui sei scappata invece che aiutarti a inserirti civilmente nella nostra societa'.

Povera ministra Carfagna, che ho avuto occasione di difendere e apprezzare in altre situazioni (1), e' proprio finita nelle mani dei papponi dei tempi delle case chiuse.

Per la prostituzione, invece, occorre intervenire prevedendo regole che integrino nel tessuto sociale e lavorativo chi sceglie di prostituirsi per mestiere. Cosi' l'ho gia' previsto in un disegno di legge presentato col sen. Marco Perduca (2), rifacendomi a quanto accade da anni in altri Paesi europei, con risultati concreti contro quelle criminalità efferate che nel nostro Paese godono (e continueranno a godere, secondo il Ddl del Governo) di impunità di fatto. La mancanza di regolamentazione giuridica e fiscale delle prostitute, equivale a nessuna tutela previdenziale e lavorativa, anche per chi esercitera' questo mestiere in una casa di propria proprieta'. Mentre non si capisce perché affittare una casa a chi si prostituisce per mestiere, proprio perche' il disegno di legge del Governo incentiva la prostituzione al chiuso, debba comportare il rischio di favoreggiamento. L'ipocrisia fa da padrona in tutti gli ambiti di questo ddl.

Comunque, ci rivedremo in Parlamento, dove il mio impegno legalizzatorio e umanitario e' garantito.

(1) [http://blog.donatellaporetti.it/?s=carfagna](http://blog.donatellaporetti.it/?s=carfagna)

(2) [http://blog.donatellaporetti.it/?p=86](http://blog.donatellaporetti.it/?p=86)