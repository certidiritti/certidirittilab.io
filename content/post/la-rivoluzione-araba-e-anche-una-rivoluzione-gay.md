---
title: '"La rivoluzione araba è anche una rivoluzione gay"'
date: Tue, 15 Mar 2011 14:53:22 +0000
draft: false
tags: [Transnazionale]
---

A sostenerlo è Abdellah Taïa, giovane scrittore marocchino che in Italia ha appena pubblicato il romanzo 'Uscirò da questo mondo e dal tuo amore' (Isbn Edizioni, 124 pp), intervistato da Anna Momigliano per Panorama.it

Macché deriva islamista. **La rivolta che impazza nelle piazze arabe è (anche) una rivoluzione della minoranza omosessuale**, una comunità finora isolata, disprezzata e spesso attivamente perseguitata nel mondo arabo, e che ora, con questi sussulti di democrazia, forse avrà occasione di dire la sua. A sostenerlo è **Abdellah Taïa**, giovane scrittore marocchino che in Italia ha appena pubblicato il romanzo _'Uscirò da questo mondo e dal tuo amore'_ (Isbn Edizioni, 124 pp). Panorama.it lo ha intervistato per parlare di libertà, scrittura, democrazia e sentimenti. Abdellah, nel tuo libro tu descrivi vividamente la violenza che hai subìto da bambino. L’idea che mi sono fatta dai primi capitoli è che tu sia cresciuto in un ambiente in cui l’omosessualità è disprezzata… eppure lo stupro omosessuale è quasi accettato, o se non altro comune. E’ così? Come spiegheresti questa dicotomia?  
  
La situazione descritta nel primo capitolo è molto più complessa di così. Si tratta del tema del volo in particolare, in cui l’eroe (il mio “io”, me) è molto attaccato e ancora insiste a reprimere, negare. Quando il silenzio diventa insopportabile, si lascia morire, cadere. Egli vive, piange, cade, la solitudine è terribile, in un mondo dove continua a rovesciare i codici, deve ridare un senso alla sua individualità, la sua omosessualità.  
  
Un gruppo di giovani uomini tenta di violentarlo. Saprà resistere come meglio può. Ma, e qui è dove le cose si complicano, si innamora del leader di questo gruppo, Chouaib…  
  
Come tutto ciò che ho scritto finora, questo libro descrive l’omosessualità in modo problematico. Ma non do lezioni, mostro situazioni che derivano dalla mia esperienza, do a vedere una parte di me, ma attraverso artifici letterari.  
  
Come descriveresti la situazione degli omosessuali nel mondo arabo?  
  
Molte cose stanno cambiando nel mondo arabo. Gli arabi si stanno ribellando, finalmente. Escono in strada, mandano via i dittatori. Gli arabi stanno cercando di inventare una personalità, finalmente libera, liberata. Sono soggetti politicizzati che non hanno più paura, che vogliono ridefinirsi partendo da zero.  
  
La rivoluzione araba, spero con tutto il cuore, consentirà la liberazione di tutti, eterosessuali ed omosessuali. Usciremo dalla schizofrenia, da questi codici che ci impediscono di essere noi stessi.  
  
I dittatori arabi hanno finora impedito alle persone di ottenere la dignità, una vera tolleranza… L’omosessualità esiste nel mondo arabo e gli omosessuali arabi sono molto coinvolti nella Rivoluzione, naturalmente. E ‘ovvio.  
  
Ora tu vivi in Francia, credi che l’omosessualità sia più accettata in Europa?  
  
In apparenza, l’omosessualità è accettata in Occidente. La gente si dice tollerante, qui accettano gli omosessuali senza alcun problema. Ma quando si tratta di cambiare le leggi che discriminano gli omosessuali, le resistenze cominciano a saltare fuori… E’ vero che vi è una certa libertà di vivere l’omosessualità, non lo nego. Ma gli atteggiamenti non sono cambiati completamente su questo tema. L’omosessuale continua anche qui a essere visto come qualcuno di strano, indefinito. O una macchietta fashionista  
  
Come vivi il tuo status di intellettuale arabo in Occidente? Incontri spesso pregiudizi?  
  
L’immagine degli arabi sta cambiando in tutto il mondo. L’Occidente non è mai del tutto uscito dalla sua immagine “orientalista” degli arabi. Oggi il mondo sta finalmente scoprendo gli arabi, tutti gli arabi. Gli arabi sono la Rivoluzione. Stanno reinventando il concetto, l’idea di rivoluzione. E’ l’Occidente che deve ora seguire l’esempio.  
  
Non sei il primo scrittore arabo a parlare esplicitamente di omosessualità. Penso al successo di Palazzo Yacoubian. Pensi che le cose stiano lentamente cambiando?  
  
Palazzo Yacoubian è un romanzo molto bello e coraggioso. E Alaa Aswani uno scrittore impegnato che ammiro molto. Per me rappresenta l’intellettuale arabo che non snobba la gente, che va al cuore delle persone, che parla della realtà, che denuncia, che supporta la differenza, tutte le differenze.  
  
Spero che avremo nel mondo arabo sempre più scrittori come lui, rafforzati nel loro impegno. Quanto a me, non ho mai pianificato di svelare un giorno la mia omosessualità.  
  
Io scrivo. E per scrivere significa raccontare il mondo indipendentemente delle reazioni altrui, dalle incomprensioni e dagli insulti. Scrivere per è la nudità, la libertà totale.  
  
—  
Anna Momigliano è una scrittrice e giornalista milanese. Ha scritto reportage da Israele, Libano e altri Paesi mediorientali. Per Marsilio ha pubblicato Karma Kosher

www.panorama.it