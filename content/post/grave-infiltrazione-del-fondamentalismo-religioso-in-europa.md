---
title: 'Grave infiltrazione del fondamentalismo religioso in Europa'
date: Wed, 06 Apr 2011 10:56:09 +0000
draft: false
tags: [Europa]
---

Il Parlamento europeo chiarisca il ruolo di Carlo Casini, presidente del Movimento per la vita e della Commissione Affari Costituzionali, che sarebbe co-autore della nuova costituzione liberticida dell'Ungheria.

Bruxelles – Roma, 6 aprile 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Il parlamento ungherese sta discutendo un progetto di Costituzione proposto dalla coalizione di governo composta dai partiti FIDESZ (conservatore) e KDNP (partito cristiano-democratico) che prevede un **esplicito divieto dei matrimoni tra persone dello stesso sesso e dell'aborto, cancella l'orientamento sessuale dalla lista delle discriminazioni proibite, prevede la possibilità di attribuire un voto aggiuntivo per le famiglie con bambini (in violazione del principio sul quale si basa la democrazia contemporanea: "una persona, un voto"), limita i poteri della Corte Costituzionale** ed agevola l'occupazione da parte delle forze di governo attuali in tutte le principali istituzioni del paese.

La  denuncia è stata diffusa da diverse Ong che si occupano di diritti umani, dalle associazioni LGBT, dalle opposizioni, ed é arrivata al Parlamento Europeo, dove i gruppi Alde, Socialdemocratico, Verde e Gue (Sinistra unitaria) hanno depositato diverse interrogazioni alle quali il Presidente della Commissione europea Barroso ha risposto affermando che la Commissione seguirà gli sviluppi sul progetto della nuova Costituzione ungherese per verificarne la conformità con il Diritto ed i valori europei.  
**Il rischio è che in Ungheria si affermi lo smantellamento sistematico di ogni contrappeso democratico, istituzionale e costituzionale, e l'imposizione di una controriforma di stampo autoritario, vaticano-talebano, conservatore e populista.**  
Nel corso di un recente dibattito al Parlamento europeo é emerso che in tale opera di stesura della nuova Costituzione ungherese, i co-autori della proposta sarebbero stati aiutati da un eurodeputato italiano, Carlo Casini, che é anche Presidente della Commissione Affari Costituzionali del Parlamento europeo. 

![On-Casini](http://www.certidiritti.org/wp-content/uploads/2011/04/On-Casini.jpg)Carlo Casini, nel corso di un incontro al quale ha partecipato come "esperto di diritto internazionale, diritti umani e questioni bioetiche", si é  complimentato sostenendo che inserire "i riferimenti alle radici millenarie e cristiane della nazione ungherese nel preambolo della Costituzione è importante perché puo' contribuire all'identità nazionale ungherese”. Casini ha inoltre accolto con favore l'idea che “la Costituzione debba proclamare la necessità della protezione della vita umana fin dal momento della concezione”. 

L’Associazione Radicale Certi Diritti  invita il Parlamento europeo, ed in particolare i membri della commissione Affari Costituzionali, a chiedere all'on. Casini chiarimenti in merito al suo ruolo istituzionale nella vicenda e se il suo intervento non sia in contrasto con quanto sancito dai Trattati internazionali e dal diritto europeo.