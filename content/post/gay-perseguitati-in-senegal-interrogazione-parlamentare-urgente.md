---
title: 'GAY PERSEGUITATI IN SENEGAL: INTERROGAZIONE PARLAMENTARE URGENTE'
date: Fri, 09 Jan 2009 12:26:39 +0000
draft: false
tags: [Comunicati stampa]
---

PERSECUZIONI OMOSESSUALI IN SENEGAL: INTERROGAZIONE PARLAMENTARE URGENTE DEI DEPUTATI RADICALI DEL PD. CHIESTA LA CONVOCAZIONE DELL'AMBASCIATORE E INIZIATIVE CON L'UNIONE EUROPEA.       
Roma, 9 gennaio 2009 In merito alle gravi notizie sugli omosessuali perseguitati in Senegal, uno dei pochi paesi africani che rispetta solitamente i diritti umani pur prevedendo nel suo Codice penale la persecuzione dell'omosessualità, i deputati radicali del Pd Matteo Mecacci, Rita Bernardini, Marco Beltrandi Maria Antonietta Farina Coscioni, Maurizio Turco ed Elisabetta Zamparutti, hanno oggi depositato un'interrogazione parlamentare urgente. Richiesta la convocazione dell'Ambasciatore del Senegal e un intervento presso l'Unione Europea. Di seguito il testo dell'interrogazione:  
   
   
INTERROGAZIONE A RISPOSTA ORALE URGENTE   
Al Ministro degli Esteri  
   
Considerato che:  
   
Secondo quanto riportato da un articolo pubblicato il 9 gennaio 2009, a pagina 20, del Corriere della Sera a firma Massimo A. Alberizzi, in Senegal sono stati condannati a otto anni di carcere nove omosessuali per 'atti contro natura';  
\- Il Senegal è tra i 38 paesi del continente africano che  puniscono i rapporti omosessuali tra adulti consenzienti; la norma, che prevede una multa da 150 a 250 euro e la prigione da uno a cinque anni, non era stata mai severamente applicata;  
\- lo scorso febbraio 2008 una donna e 10 uomini sono stati arrestati a Dakar, dopo la pubblicazione su una rivista di fotografia di foto di un matrimonio gay;  
\- più di recente, il 19 dicembre scorso, nove persone omosessuali, tra questi il leader del movimento Lgbt senegalese, Diadji Diouf, impegnati anche in un'organizzazione che si batte contro l'aids, sono state arrestate;  
\- tali arresti sono avvenuti all'interno di un clima mediatico omofono;  
\- il Senegal ha mantenuto a lungo, sotto la Presidenza Wade una tradizione di rispetto dei principi democratici e di difesa dei diritti umani;  
\- L'Italia, di concerto con l'Unione Europea ha promosso la presentazione di una dichiarazione in sede ONU per la depenalizzazione dell'Omofobia lo scorso dicembre, quale iniziativa centrale per il rispetto dei diritti umani delle persone omosessuali  
   
Per sapere:  
   
\- quali urgente iniziative intende promuovere il nostro Governo a tutti i livelli, incluso quello europeo, affinché vengano scongiurate iniziative ed azioni omofobe in Senegal;  
\- se non ritenga il Ministro di dover attivare la propria diplomazia per esprimere la preoccupazione del nostro Governo per i fatti succitati e, tra le azioni più concrete, convocare con la massima urgenza l'Ambasciatore del Senegal, anche per chiedere l'adeguamento della legislazione del paese agli standard internazionali in materia di diritti umani fondamentali;