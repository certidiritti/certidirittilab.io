---
title: 'Uganda: Non abbassare la guardia!'
date: Sat, 18 Jan 2014 09:26:45 +0000
draft: false
tags: [Africa]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti

Roma, 17 gennaio 2013

La notizia che il presidente Yoweri Museveni si è rifiutato di firmare la legge che punisce gli omosessuali recidivi con il carcere a vita e prevede pene anche per chi parla dell'omosessualità senza condannarla o non denuncia le persone lgbti alle autorità è sicuramente positiva, ma non basta a risolvere il problema.

[Come ha detto Samuel Opio, Executive Director di Queer Youth Uganda, al VII Congresso dell'Associazione Radicale Certi Diritti](http://www.certidiritti.org/2014/01/14/sam-opio-al-vii-congresso-dellassociazione-radicale-certi-diritti/), «Secondo l'articolo 91(3), il Presidente della Repubblica dell'Uganda ha 30 giorni di tempo per firmare una legge già votata dal Parlamento: in caso contrario, egli ha la facoltà di inviarla di nuovo al Parlamento, richiedendo la modifica totale o parziale della legge stessa. Il Presidente della Repubblica dispone altresì della facoltà di notificare per iscritto al Presidente del Parlamento il proprio diniego».

Quest'ultimo è il caso in questione poiché Yoweri Museveni ha inviato una lettera al Parlamento in cui afferma che l'omosessualità può essere causata sia "da una cattiva educazione" sia dal bisogno di fare soldi, e che la cura migliore consiste nel miglioramento dell'economia nazionale. Il

Parlamento quindi adesso può modificare la proposta di legge e ripresentarla al Presidente. Se il Presidente si rifiutasse ancora di firmare la proposta di legge, quest'ultima, se avesse ottenuto il voto favorevole di almeno i due terzi di tutti i membri del Parlamento, diventerebbe automaticamente legge entro 30 giorni.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, afferma: «la comunità internazionale e il governo italiano devono continuare a fare pressioni, non solo sul governo, ma anche sui parlamentari, affinché non si verifichi lo scenario prefigurato dalla costituzione ugandese. Facciamo inoltre nostre le richieste che Samuel Opio, Executive Director di Queer Youth Uganda, ha fatto al VII Congresso dell'Associazione Radicale Certi Diritti tenutosi la settimana scorsa a Milano:

Chiediamo al Governo italiano di negare l'accesso in Italia a tutti i leader politici Ugandesi finché perdurerà questa situazione;

Chiediamo al Governo italiano di riconoscere il diritto di asilo ai richiedenti GLBTI Ugandesi e a tal proposito, chiediamo di inviare all'ambasciata italiana in Uganda una comunicazione formale, che autorizzai il rilascio dei visti a ogni richiedente GLBTI Ugandese che si senta in pericolo».