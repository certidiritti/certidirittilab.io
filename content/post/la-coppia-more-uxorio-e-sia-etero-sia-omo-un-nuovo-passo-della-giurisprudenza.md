---
title: 'La coppia more uxorio è sia etero sia omo: un nuovo passo della giurisprudenza'
date: Sun, 08 Apr 2012 12:12:32 +0000
draft: false
tags: [Diritto di Famiglia]
---

La Cassa Mutua di una banca garantisce l’assistenza sanitaria, previo versamento del relativo premio, (anche) al convivente more uxorio. Commento di Massimo Clara.

Secondo il Tribunale di Milano, la convivenza more uxorio non si caratterizza per la diversità di sesso dei conviventi, anzi sarebbe discriminatorio ed arbitrario considerare rilevante questo elemento. E’ di questi giorni la notizia che la Corte d’ Appello ha confermato questo giudizio.

Il fatto: la Cassa Mutua di una banca garantisce l’ assistenza sanitaria, previo versamento del relativo premio, (anche) al convivente more uxorio. Ma quando il signor Tizio presenta la domanda gli viene risposto di no, perché il suo convivente beneficiario Caio è del suo medesimo sesso.

Il signor Tizio non ci sta, fa causa, vince in Tribunale ed oggi in Appello, un’ altra buona notizia ricca di utili riflessioni.

La prima: anche se non si è ancora ottenuto (e si dovrà ottenere) il matrimonio, molti diritti per le coppie del medesimo sesso già esistono, succede che vengano negati perché l’omofobia si trova dappertutto, ma la via giudiziaria anche se lunga e faticosa dà giusti risultati.

La seconda: se è vero che l’omofobia si trova dappertutto, è anche vero che la cultura antidiscriminatoria fa ogni giorno passi avanti, e nei tribunali noi vediamo affermarsi una cultura civile che progressivamente permea le aule di giustizia perché permea larga parte della società.

La terza: è nelle parole del Tribunale. L’ art. 3 della Costituzione protegge l’individuo da qualunque discriminazione legata all’ orientamento sessuale. Ed altrettanto fa la normativa europea (e questi sono principi generali che si applicano sempre, non solo per l’assistenza sanitaria).

La quarta: è la più profonda, e la più importante. Il concetto di more uxorio che le nostre sentenze riconoscono dà valore alla scelta ed alla responsabilità delle persone, al loro sentimento reciproco, alla loro volontà di essere coppia, mentre si allontanano sempre più gli argomenti “naturalistici”, in realtà repressivi, forgiati per riconoscere ed imporre una sola visione di famiglia, una sola sessualità lecita.

Sia chiaro: non è finita, però siamo sulla strada giusta

Massimo Clara