---
title: 'Fermiamo la legge omofoba di San Pietroburgo: firma l''appello di Amnesty'
date: Tue, 06 Mar 2012 21:54:43 +0000
draft: false
tags: [Russia]
---

Il disegno di legge che limita il diritto alla libertà di espressione delle persone lgbtqi ha superato l'udienza finale lo scorso 29 febbraio. Per divenire legge dovrà essere firmato dal governatore di San Pietroburgo entro le prossime due settimane. Firma l'appello.  
   
La proposta di legge che prevede l'imposizione di multe per azioni pubbliche finalizzate alla propaganda della sodomia, del lesbismo, della bisessualità e transessualità fra i minori, introdotta nel novembre del 2011, ha infine superato la terza lettura il 29 febbraio.  
  
Se il governatore di San Pietroburgo non metterà il veto, la legge avrà un impatto negativo sulla libertà di espressione e riunione delle persone Lgbti e impedirà ai giovani Lgbti di avere accesso o condividere informazioni fondamentali per la loro salute e il loro benessere, fra cui informazioni sui gruppi sociali, le reti di supporto e la salute sessuale e riproduttiva. Il disegno di legge, inoltre, limiterà duramente a San Pietroburgo le attività delle organizzazioni Lgbti.  
   
Questo disegno di legge riconosce giuridicamente la discriminazione contro le persone Lgbti, che è già diffusa in Russia, perpetra il giudizio per cui le persone Lgbti non meritano la stessa protezione dei diritti umani dei loro amici, familiari e colleghi eterosessuali e contribuisce a creare un clima di ostilità e violenza nei confronti delle persone Lgbti.

**FIRMA L'APPELLO DI AMNESTY AL GOVERNATORE DI SAN PIETROBURGO >[](http://www.amnesty.it/Liberta-espressione-a-rischio-in-Russia) [http://www.amnesty.it/Liberta-espressione-a-rischio-in-Russia](http://www.amnesty.it/Liberta-espressione-a-rischio-in-Russia)**