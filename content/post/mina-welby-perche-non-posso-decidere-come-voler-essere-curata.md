---
title: 'Mina Welby: perché non posso decidere come voler essere curata?'
date: Thu, 17 Mar 2011 13:03:50 +0000
draft: false
tags: [bioetica, biotestamento, Comunicati stampa, Coscioni, costituzione, eluana, englaro, libertà, mina welby, parlamento, Testamento Biologico]
---

Intervento di Mina Welby sul testamento biologico. Sulla materia è all’esame del Parlamento un disegno di legge della maggioranza che è già stato esaminato e approvato dal Senato e, adesso, purtroppo peggiorato, deve essere discusso e votato alla Camera dei Deputati.

Il testamento biologico, o meglio le disposizioni anticipate sui trattamenti sanitari, consiste semplicemente nelle dichiarazioni che io faccio oggi per un even­tuale momento futuro in cui non sarò più in grado di poter esprimere al medico che mi deve curare la mia volontà su quello che può o non può fare sul mio corpo. Anche fare o non fare queste dichiarazioni è, ovviamente, lasciato alla libera scelta di ognuno.

In pratica si tratta di un prolungamento del consenso o dissenso informato.

Mi spiego: se vado dal medico non accetto un trattamento sen­za conoscerne gli effetti; il medico è obbligato a informarmi sulla diagnosi, le terapie, le controindicazioni. Normalmente è la persona interessata ad accettare o rifiutare delle terapie.

Come si sa su questa materia è all’esame del Parlamento un disegno di legge della maggioranza che è già stato esaminato e approvato dal Senato e, adesso, purtroppo peggiorato, deve essere discusso e votato alla Camera dei Deputati.

È utile citare il primo articolo del disegno di legge: “La presente legge, tenendo conto dei principi di cui agli articoli 2, 3, 13 e 32 della Costituzione: a) riconosce e tutela la vita umana,…. b) riconosce e garantisce la dignità di ogni persona….. e) riconosce che nessun trattamento sanitario può essere attivato…. f) garantisce che …..il medico debba astenersi da trattamenti straordinari non proporzionati,…..”

Ricordo che, nell’art. 2 la nostra Costituzione riconosce e garantisce i diritti inviolabili dell’uomo; nell’art. 3 si afferma la pari dignità sociale e l’eguaglianza dei cittadini davanti alla legge; nell’art. 13 è garantita l’inviolabilità della libertà personale; nell’art. 32 si garantisce la tutela della salute come fondamentale diritto dell’individuo, ma anche la necessità di una legge per obbligare a un determinato trattamento sanitario con il limite **assoluto** del rispetto della dignità umana.

Mi soffermo sull’art. 3 della Costituzione (“Tutti i cittadini hanno pari dignità sociale e sono eguali davanti alla legge, senza distinzione,…., di condizioni personali e sociali”).  
  
In verità il disegno di legge non rispetta questo principio. Infatti, quando il medico ritiene giusto intervenire su di te con una qualsiasi terapia che lui ritenga giusta, le tue disposizioni scritte e firmate hanno meno valore che se tu fossi cosciente e potresti rifiutare, e non c’è opposizione del fiduciario o opinione di collegio medico che tenga: il medico deve solamente annotare sulla cartella clinica il motivo per cui è intervenuto su di te.  
  
Un esempio concreto: se un malato di SLA che **ha rifiutato la tracheotomia per iscritto**, si risveglia tracheostomizzato per una vita (che lui avverte come tortura continua), con chi se la deve prendere?

Ecco il punto fondamentale: anche una persona che perde le sue capacità fisiche o mentali conserva il suo pieno valore e la sua dignità di essere umano e deve essere rispettato nella sua volontà.  
  
Assecondare un malato nella sua scelta e accompagnarlo con umanità non significa per un medico essere esecutore burocratico di un ordine, ma può essere di sicuro traghettatore di un navigante al suo porto. Perché spesso morire può essere l’unica speranza di star bene per una persona. 

Questo disegno di legge è stato redatto appositamente come rivincita sulla sentenza della Cassazione che aveva avuto come conseguenza la possibilità di morire per Eluana Englaro, dopo la decisione della Corte d’Appello di Milano a favore del distacco del sondino naso gastrico.

Il Governo insieme ai suoi seguaci videro in questa sentenza una usurpazione di potere, una invasione di campo. Perciò in questo progetto di legge all’art. 3, comma 5 si scomoda perfino la Convenzione delle Nazioni Unite sui diritti delle persone con disabilità, (New York, 13 dicembre 2006), secondo la quale **”alimentazione ed idratazione, nelle diverse forme in cui la scienza e la tecnica possono fornirle al paziente, devono essere mantenute fino al termine della vita, ad eccezione del caso in cui le medesime risultino non più efficaci nel fornire al paziente i fattori nutrizionali necessari alle funzioni fisiologiche essenziali del corpo. Esse non possono formare oggetto di dichiarazione anticipata di trattamento.”**

Si parla di scienza e tecnica che forniscono la nutrizione artificiale: può essere un sondino naso gastrico o una peg che si posiziona con un atto chirurgico, ma può essere anche una sacca che viene collegata con una flebo a una vena centrale. La nutrizione e idratazione artificiale vengono spacciate per sostegno vitale e non si dice che sono trattamenti

Anche la ventilazione forzata è sostegno vitale ed anche trattamento sanitario; e anche la dialisi lo è. In rianimazione qualsiasi tipo di trattamento è sostegno vitale.

Fino a quando sono capace di comunicare con il medico, io posso rifiutare la NIA (Nutrizione e Idratazione Artificiale), nel momento che perdo coscienza questo non vale più, il sondino di Stato mi è assicurato. Gli articoli 3 e 32 della nostra Costituzione sono collegati per quello che riguarda sia la volontà attuale che quella anticipata perché è l’articolo 3 che conferma la mia pari dignità umana sia che io mi trovi in stato di coscienza che in stato di incoscienza. Questa futura legge calpesta un mio diritto sacrosanto e la Costituzione stessa. E per questo la NIA è considerata sostegno vitale, non trattamento sanitario, checché ne dicano tutte le società internazionali sul carattere sanitario della nutrizione artificiale.

Lo scopo della legge proposta dovrebbe essere, secondo qualcuno fra i favorevoli alla promulgazione, impedire altre sentenze simili a quelle emesse per Eluana Englaro. Io temo, invece, che provocherà migliaia di denunce e sentenze.

Il testo è pieno di contraddizioni e sottolinea varie volte che si vuole proibire l’eutanasia. Per questo si citano due volte gli articoli del codice penale 575, 579 e 580. Citando questi articoli, secondo me, si vogliono classificare come eutanasia, le desistenze terapeutiche che abitualmente vengono praticate, sia su richiesta dei pazienti, sia su richiesta dei loro parenti, sia per decisione dei medici, per troncare altre sofferenze. L’eutanasia nel nostro paese non è un crimine. Lo si vuole istituire?

Mi sto domandando il motivo di questa invasività nel nostro vivere. **Perché non posso decidere io come voler essere curata anche quando non sarò più capace di intendere e di volere?** Ma i parlamentari, i nostri legislatori non hanno avuto il dubbio sulla costituzionalità di questa proposta di legge? Spero che le radio, le televisioni, i giornali si attivino per dare le giuste informazioni ai cittadini in modo che possano giudicare. 

Chiedo ai parlamentari di non fare una legge per affermare una visione etica, ma costituzionale, fruibile da tutti coloro che ne sentono il bisogno.

Serve una legge semplice. Non si imponga la propria morale, la propria coscienza a noi cittadini. Per i nostri errori rispondiamo noi.

**Se tu pensi che io sbaglio e pensi che io stia meglio con la tua correzione, non mi rendi felice e tu non sei giusto. Rimani al posto tuo e lasciami felice!**

Mina Welby