---
title: 'AGENZIA DIRITTI FONDAMENTALI UE: SI A MATRIMONIO GAY'
date: Tue, 01 Jul 2008 11:43:18 +0000
draft: false
tags: [Agenzia per i diritti fondamentali, certi diritti, Comunicati stampa, Matrimoni omosessuali, Ottavio Mazzocchi, unione europea]
---

L'Agenzia per i diritti fondamentali del''UE ha pubblicato ieri [il rapporto sull'omofobia ,richiesto dal PE nel 2007.  
](http://fra.europa.eu/fra/material/pub/comparativestudy/FRA_hdgso_part1_en.pdf)_"Certi diritti invita il governo italiano a seguire le raccomandazioni dell'Agenzia per i diritti fondamentali dell'Unione europea ed a riconoscere le unioni ed i matrimoni tra le persone dello stesso sesso"_ ha dichiarato Ottavio Marzocchi, responsabile per le questioni europee dell'associazione e membro dell'Intergruppo LGBT al PE.  
  
"L'Agenzia, nel suo rapporto sull'omofobia pubblicato ieri e richiesto dal PE nel 2007, ha giustamente stigmatizzato l'assenza di regolamentazione delle relazioni tra le persone dello stesso sesso in Italia ed in una – oramai - minoranza di Stati membri UE ed ha chiesto a questi di riconoscere le unioni contratte in altri Stati membri.  
  
L'Agenzia ha affermato, dando ragione alle posizioni espresse in numerose interrogazioni da numerosi deputati europei quali Marco Cappato (Radicali) e Sophie In't Veld (D66), che l'assenza di riconoscimento costituisce un ostacolo proibito dai Trattati europei alla libertà di circolazione dei cittadini comunitari, nonché una discriminazione basata sull'orientamento sessuale in violazione del principio di eguaglianza.  
  
Il rapporto richiama il caso di due cittadini italiani che non hanno ottenuto il riconoscimento del matrimonio contratto in Olanda, ovvero degli amici Antonio Garullo e Mario Ottocento, che da anni stanno conducendo una battaglia legale per affermare i loro diritti, che sono quelli della comunità LGBT italiana, europea ed internazionale.  
  
Certi Diritti ringrazia l'Agenzia per avere ricordato all'UE, all'Italia ed agli Stati membri che I diritti LGBT sono - né più né meno - diritti fondamentali, in particolare alla vigilia della decisione della Commissione europea in merito alla direttiva orizzontale anti-iscriminazioni prevista per il 2 luglio, nonché di fronte ad una situazione italiana di progressiva putrefazione  e violazione di diritti e libertà, da quelle dei Rom a quelle delle persone LGBT fino a quelle di tutti i cittadini."  
  
[Il testo della relazione é disponibile in inglese.](http://fra.europa.eu/fra/material/pub/comparativestudy/FRA_hdgso_part1_en.pdf)