---
title: 'Quando i diritti negati in Italia si affermano grazie all’Europa'
date: Fri, 21 Sep 2012 08:45:18 +0000
draft: false
tags: [Europa]
---

Basta con le discriminazioni delle coppie dello stesso sesso. Contributo di Gabriella Friso* e Yuri Guaiana** pubblicato dall'associazione www.articolo3.org

Nel febbraio scorso il Tribunale di Reggio Emilia ha accolto il ricorso di un cittadino uruguaiano che, sposato in Spagna con un italiano, si è visto negare dalla Questura il permesso di soggiorno, richiesto in applicazione delle norme sulla libera circolazione in Europa, perché il loro matrimonio non era considerato valido in Italia.  
  
Nel ricorso, presentato dall’Avv. Giulia Perin e concordato con il Direttivo dell’associazione radicale Certi Diritti, non si è chiesto il riconoscimento del matrimonio spagnolo ma il diritto per i coniugi ad avere una vita famigliare in Italia. Sono state quindi richiamate le norme europee (d.lgs. n. 30/2007 sulla libera circolazione dei cittadini europei e i loro famigliari), i pronunciamenti della CEDU (Corte Europea dei Diritti Umani) sull’argomento, gli altri strumenti internazionali recepiti e ribaditi nella Carta dei diritti fondamentali dell'unione europea. Rigettare l’istanza avrebbe configurato una discriminazione fondata sull’orientamento sessuale (la cui illegittimità è stata ribadita, con una risoluzione di portata storica, dal Consiglio per i diritti umani delle Nazioni Unite il 17 giugno 2011). Inoltre l’art. 9 della Carta europea dei diritti fondamentali, in vigore dal 1 dicembre 2009, in quanto recepita dal Trattato europeo di Lisbona, ha individuato in capo ad ogni persona “il diritto di sposarsi e di costituire una famiglia”, utilizzando un’espressione diversa da quella contenuta nell’art. 12 della CEDU per cui non richiede più come requisito necessario la diversità di sesso dei soggetti del rapporto.  
  
Le stesse linee guida emanate dalla Commissione europea per una migliore trasposizione della direttiva n. 2004/38 (COM 2009 - 313) sottolineano che “ai fini dell’applicazione della direttiva devono essere riconosciuti, in linea di principio, tutti i matrimoni contratti validamente in qualsiasi parte del mondo”, mentre vengono espressamente menzionate le sole eccezioni dei matrimoni forzati e dei matrimoni poligami.  
  
Queste indicazioni sono state ribadite recentemente anche dalla Corte di Cassazione (n. 4184 del 13.3.12) a dimostrazione che, sebbene queste norme non abbiano efficacia diretta nel nostro ordinamento nazionale e il loro contenuto non abbia alcun effetto cogente quanto all’approvazione dell’istituto matrimoniale, “il giudice nazionale, in dipendenza di quanto disposto dall’attuale articolo 117 della Costituzione, così come novellato nel 2001, ha però l’obbligo di conformare la propria attività interpretativa ai vincoli derivanti dall’ordinamento comunitario e dagli obblighi internazionali, e tra essi dai diritti fondamentali così come interpretati dalla CEDU” (cosiddetta interpretazione convenzionalmente conforme).  
   
Anche le indicazione contenute in due importanti sentenze italiane hanno confermato questo indirizzo.  
La prima è la n. 138/2010 della Corte Costituzionale in cui si afferma che: all’unione omosessuale, “intesa come stabile convivenza tra due persone dello stesso sesso”, spetta “il diritto fondamentale di vivere liberamente una condizione di coppia” e che il “diritto all’unità della famiglia che si esprime nella garanzia della convivenza del nucleo familiare (…) costituisce espressione di un diritto fondamentale della persona umana”.  
   
La seconda della Corte di Cassazione n.1328/2011 in cui si asserisce che:  
a) la nozione di “coniuge” prevista dall’art. 2 d.lgs. n. 30/2007 deve essere determinata alla luce dell’ordinamento straniero in cui il vincolo matrimoniale è stato contratto e che  
b) lo straniero che abbia contratto in Spagna un matrimonio con un cittadino dell’Unione dello stesso sesso deve essere qualificato quale “familiare”, ai fini del diritto al soggiorno in Italia.  
  
L'esito del ricorso di Reggio Emilia è davvero molto importante, in quanto non solo l'Avvocatura dello Stato, letto il provvedimento del tribunale ha ritenuto di non impugnarlo, e la questura di Reggio Emilia ha rilasciato al richiedente la carta di soggiorno per i famigliari di cittadini EU di durata quinquennale, ma, soprattutto, perché anche la questura di Milano in due casi del tutto analoghi ha riconosciuto il diritto di soggiorno al coniuge dello stesso sesso di due cittadini italiani, consolidando in questo modo il provvedimento emiliano.  
  
Si tratta dunque di un esempio evidente di come la normativa europea sempre più entri a far parte del nostro ordinamento per quanto riguarda la tutela dei diritti fondamentali del cittadino.  
A riprova di questo, anche la Corte d'Appello del Tribunale di Milano ha rigettato il ricorso presentato dalla Cassa mutua di una banca di Credito Cooperativo che non intendeva concedere a una coppia dello stesso sesso i privilegi previsti ai conviventi more uxorio, e ha stabilito che in questa nozione rientrano sia le coppie conviventi eterosessuali che omosessuali (Corte d'Appello di Milano, 31 agosto 2012, n. 7176 Data pubblicazione). Tutti questi successi denotano che la giurisprudenza, dopo l'intervento della Consulta, continua a muoversi e spesso va nella direzione giusta…  
  
Ora è il momento che il Governo italiano ne prenda atto e finalmente rimuova tutte le norme discriminatorie nei confronti delle coppie formate da persone dello stesso sesso, a partire dalla Circolare n. 55 del 2007 “Matrimoni contratti all’estero tra persone dello stesso sesso. Estratti plurilingue di atti dello stato civile”, detta più semplicemente “Circolare Amato”, che vieta a queste famiglie la trascrizione dei matrimoni celebrati all’estero, fino ad arrivare alla completa uguaglianza dei cittadini con l’approvazione del matrimonio civile egualitario anche nel nostro Paese.  
  
Proprio per raggiungere questo obbiettivo l’Associazione radicale Certi Diritti ha dato vita alla Campagna di Affermazione civile che sta incardinando delle cause pilota, come quella di Reggio Emilia, che facciano emergere le discriminazioni evidenti tra le coppie matrimoniali eterosessuali e quelle dello stesso sesso che si vedono impedita la possibilità di accedere a questo istituto. Purtroppo quando la politica è sorda, l’unica possibilità che resta è quella di rivolgersi alla Magistratura.  
   
*Direttivo dell’Associazione radicale Certi diritti  
\*\* Segretario nazionale dell’Associazione radicale Certi diritti