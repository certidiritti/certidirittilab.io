---
title: 'Partito Radicale e Certi Diritti partecipano a Bruxelles alla manifestazione per la democrazia, lo Stato di Diritto e i diritti fondamentali in Ungheria'
date: Sat, 16 Mar 2013 09:14:43 +0000
draft: false
tags: [Europa]
---

comunicato stampa dell'associazione radicale Certi Diritti

Bruxelles, 15 marzo 2013

Si é tenuta oggi una manifestazione a Bruxelles per la difesa della democrazia, lo stato di diritto ed i diritti fondamentali in Ungheria, con la partecipazione di militanti ungheresi, europei e della comunità internazionale, ONG (Amnesty International, ILGA-Europe) e l'eurodeputata austriaca Ulrike Lunaceck del gruppo verde. Per il Partito Radicale Nonviolento Transnazionale e Transpartito e l'associazione radicale Certi Diritti era presente Ottavio Marzocchi (nelle foto).

La situazione in Ungheria precipita di giorno in giorno, a causa del golpe bianco attuato dal primo ministro Viktor Orban, dal suo governo e partito FIDESZ: dopo avere imbavagliato la stampa attraverso una nuova legge sui media, le autorità ungheresi hanno modificato la Costituzione attraverso una serie di emendamenti, il quarto dei quali é stato approvato lunedi scorso.

Tale emendamento permette, tra laltro, la limitazione dei poteri della Corte Costituzionale, la possibilità di spostare i processi in modo arbitrario, la criminalizzazione delle persone senza casa (homeless), l'introduzione di una definizione restrittiva di famiglia che colpisce le famiglie monoparentali ed omosessuali, la discriminazione tra religioni.

La Corte Costituzionale ungherese aveva già bocciato tali misure, che anche il Consiglio d'Europa aveva criticato, e per tale motivo le autorità ungheresi hanno deciso di elevarle a norma costituzionale per blindarle e renderle praticamente irreversibili. La Commissione europea e il Consiglio d'Europa, nonché alcuni Stati membri e gli USA, hanno espresso grave preoccupazione, mentre al PE il Presidente del gruppo liberale Guy Verhofstadt e del gruppo Verde Cohn-Bendit hanno chiesto alla Commissione di avviare la procedura di monitoraggio e sanzioni contro l'Ungheria. Il PE ha inoltre deciso di tenere un dibattito durante la sessione di Aprile, quando sarà pubblicato il rapporto Tavares sulla situazione della democrazia in Ungheria.