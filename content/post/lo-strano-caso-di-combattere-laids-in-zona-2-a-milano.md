---
title: 'Lo strano caso di combattere l''aids in zona 2 a Milano'
date: Fri, 04 Nov 2011 17:41:51 +0000
draft: false
tags: [Salute sessuale]
---

comunicato stampa di Yuri Guaiana,  
Consigliere di Zona 2 del Gruppo Radicale - Federalista europeo a Milano

ll 2 novembre si è tenuta una riunione congiunta della commissione commercio e di quella sanità per esaminare la proposta che il Gruppo radicale - Federalista europeo ha immaginato insieme a Federfarma e a “Milano contro l’AIDS”: il primo dicembre, giornata mondiale della lotta contro l’AIDS, tutte le 41 farmacie di zona 2 avrebbero potuto vendere i preservativi con almeno il 20% di sconto distribuendo anche volantini, scritti da “Milano contro l’AIDS”, per informare sulle malattie a trasmissione sessuale e la loro prevenzione con accluso un preservativo in omaggio. L’iniziativa sarebbe stata promossa in vari modi: con una conferenza stampa e con locandine in tutte le farmacie della zona.

Si trattava di decidere se dare il patrocinio della zona e un finanziamento, erogato solo dalla commissione commercio, di circa 500 € + IVA per contribuire alla stampa dei volantini.

Il dibattito è durato più di due ore dando modo di ascoltare le posizioni più astruse provenienti da destra come da sinistra. Oltre ai consiglieri sono intervenuti Giampiero Toselli di Federfarma e Lella Cosmaro di “Milano contro l’AIDS” per illustrare l’iniziativa nonché il dott. Battistella e Rosaria Iardino di NPS invitati dal PD. Si è voluto sminuire il valore del 1 dicembre citando strumentalmente una ricerca sullo stigma appena conclusasi e mettendo altrettanto strumentalmente in opposizione le iniziative informative con quelle formative e più sistemiche, che peraltro esorbitano dalle competenze e dalle disponibilità della commissione commercio, come se le une escludessero le altre.

Alla fine il PD ha proposto di concedere solo il patrocinio senza alcun finanziamento e ha votato di conseguenza la sua proposta insieme all’opposizione, con il risultato “straordinario” che le commissioni hanno deliberato di non finanziare l’iniziativa e di dare solo il patrocinio, rendendo così la stessa iniziativa impraticabile.

Il Gruppo radicale - Federalista europeo trova questo atteggiamento molto grave, anche perché la commissione commercio ha in cassa 1500€ che, grazie a questo voto, andranno interamente inutilizzati. L’iniziativa proposta rappresentava certamente una goccia nel mare del disinteresse della Regione Lombardia per la prevenzione delle malattie a trasmissione sessuale, ma poco è meglio di niente e una goccia dopo l’altra potrebbero pure colmare il vuoto pneumatico in cui si trova la città.