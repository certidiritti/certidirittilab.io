---
title: 'PUTTANA O FINOCCHIO? CITTADINO! PARTECIPA AL GAY PRIDE DI GENOVA!'
date: Wed, 24 Jun 2009 06:48:20 +0000
draft: false
tags: [Comunicati stampa]
---

Care e cari amici,

sabato 27 giugno 2009, si terrà a Genova il Gay Pride nazionale, manifestazione alla quale parteciperanno centinaia di migliaia di persone provenienti da tutta Italia. L’ Associazione Radicale Certi Diritti, Radicali Italiani e l’Associazione Luca Coscioni, hanno aderito e partecipano ai diversi eventi in programma. Uno dei portavoci del Genova Pride, da mesi impegnato nella realizzazione di questo evento insieme a molte altre Associazioni lgbt, è Alberto Villa, riferimento di Certi Diritti per Genova e la Liguria.

In occasione del Gay Pride abbiamo previsto un nostro carro con musica, del DJ Claudio Coccoluto, balli, interventi sulla campagna di Affermazione Civile e molto altro. Parteciperanno molti iscritti e sostenitori di Certi Diritti provenienti da tutta Italia, invitiamo anche te a sfilare con noi. Il Gay Pride di Genova partirà alle ore 16 da Piazza Principe (alle 11.30 terremo una riunione del Direttivo di Certi Diritti lì vicino).

Questo evento sarà per Certi Diritti una grande occasione di visibilità per far conoscere il nostro impegno per la laicità, la responsabilità e la libertà delle persone. Faremo conoscere ai partecipanti al Gay Pride la nostra campagna di Affermazione Civile, promossa insieme agli avvocati di Rete Lenford ed altri esponenti del movimento lgbt italiano.

Marceremo con orgoglio e determinazione nella consapevolezza che le lotte per il superamento delle diseguaglianze e delle discriminazioni si affermano con l’impegno di tutti, contro inutili e ameni steccati ideologici e/o associativi. Oggi più che mai, in Italia, dobbiamo avere come riferimenti di lotta nonviolenta quelli di Rosa Parks, Martin Luther King e Milk, eroi del nostro tempo, rigorosamente impegnati per la libertà e i diritti civili e umani delle persone.

Nel 1976 i Radicali e il Fuori diffusero un manifesto con la scritta: “Ti chiamano finocchio, lesbica e puttana. Per i Radicali sei un cittadino”. Oggi, a distanza di 33 anni è stato diffuso un manifesto dell’Arci con i volti dei deputati del Pd Anna Paola Concia e di Jean Leonard Toudì con la scritta: “Ci chiami ‘sporco negro’ e ‘lesbica schifosa’ ma ti offendi se ti chiamano ‘italiano mafioso’”. Ciò conferma, se ve ne fosse bisogno, che a distanza di 33 anni abbiamo ancora molto da fare. Insieme.

A presto, forse a Genova. Ciao.

Sergio Rovasio, Segretario Associazione Radicale Certi Diritti