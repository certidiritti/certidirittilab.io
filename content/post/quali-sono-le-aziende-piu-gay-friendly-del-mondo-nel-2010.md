---
title: 'QUALI SONO LE AZIENDE PIÙ GAY FRIENDLY DEL MONDO NEL 2010?'
date: Fri, 11 Jun 2010 07:49:44 +0000
draft: false
tags: [Comunicati stampa]
---

L’International Gay and Lesbian Chamber of Commerce rende noti i nomi delle aziende più gay friendly del mondo: IBM, Google, BT Group, Morgan Stanley e Cisco Systems.

**Amsterdam, Paesi Bassi, 10 giugno 2010** – L'International Gay and Lesbian Chamber of Commerce (IGLCC – Camera di Commercio Gay e Lesbica Internazionale) pubblica i risultati della seconda edizione dell'International Business Equality Index. Questo indice misura le performance di società multinazionali in relazione alle tematiche della diversità e dell'inclusione (diversity & inclusion) concentrandosi specificamente sulle comunità di lesbiche, gay, bisessuali e transgender (LGBT) nei paesi in cui operano. Funge da indicatore della leadership in materia di diversità assunta dalle compagnie multinazionali nelle comunità locali in cui sono presenti.

Quest’anno le 5 aziende più LGBT friendly del mondo sono IBM, Google, BT Group, Morgan Stanley e Cisco Systems. Di queste, tre si erano classificate tra le prime 5 nel 2009 – mentre sia Google che Morgan Stanley partecipano per la prima volta all’ Equality Index.

Secondo Pascal Lépine, presidente fondatore e segretario generale dell’IGLCC, quest’anno le iscrizioni all’Equality Index sono più che raddoppiate rispetto al debutto nel 2009. Venticinque compagnie internazionali, con cifre di vendita complessive pari a oltre 1 bilione di dollari e con un organico totale di oltre 2,2 milioni di persone in 220 paesi, hanno partecipato all’indagine completa.

«A nome dei dipendenti di IBM e delle nostre comunità LGBT di tutto il mondo, IBM è estremamente fiera di aver ricevuto questo riconoscimento da parte dell'International Gay and Lesbian Chamber of Commerce (IGLCC)» ha affermato Frank Kern, senior vice president, Global Business Services. «Il patrimonio di IBM in materia di diversità dura da quasi 100 anni, e noi c'impegniamo oggi, come in ogni momento della nostra storia, a sviluppare costantemente le nostre politiche globali innovative sul posto di lavoro.»

La partecipazione entusiasta di così tante aziende all’Equality Index 2010 è indice del ruolo sempre più importante che le società multinazionali stanno svolgendo per quanto riguarda l’introduzione e la difesa della diversità ovunque esse siano presenti. «Le attività internazionali hanno un ruolo di importanza incommensurabile per lo sviluppo della tolleranza e della diversità in tutto il mondo», ha affermato Pascal Lépine. «Non solo i programmi di diversità, come quelli misurati dall’Equality Index, sono utili per le comunità ma sono anche importanti per l’andamento degli affari e contribuiscono a garantire la produttività dei dipendenti.»

David Pollard, presidente dell’International Business Equality Index Committee, ha affermato: «È molto incoraggiante il fatto che 22 delle 25 aziende che hanno partecipato allo studio di questo anno offrano dei programmi di diversità e inclusione in tutti i paesi in cui operano; ancora di più colpisce il fatto che lo stesso numero di aziende includa le tematiche LGBT nel proprio programma globale sulla diversità. Si tratta di un risultato molto entusiasmante, che mostra come il mondo del lavoro possa contribuire globalmente al benessere della comunità.»

Anche quest’anno l'ILGA (International Lesbian, Gay, Bisexual, Trans and Intersex Association) e la sua antenna europea ILGA Europe hanno svolto un ruolo centrale nello sviluppo dell'Equality Index. «La crescita registrata dall’Index e l’aumento della sua copertura a livello internazionale ci riempiono di soddisfazione», hanno sottolineato Gloria Careaga e Renato Sabbadini, co-segretari generali di ILGA. «Considerando che un numero troppo alto di persone appartenenti alla comunità LGBTI subisce ancora oggi in tutto il mondo molestie e altre forme di discriminazione sul posto di lavoro, è essenziale svolgere un ruolo guida esemplare; ed ecco perché premiare le politiche e i processi aziendali di inclusione LGBTI più meritevoli con l’Equality Index 2010 è il modo migliore di indicare alle altre aziende la via da seguire.»

La realizzazione dell’Equality Index è stata resa possibile grazie al lavoro di un comitato internazionale composto da gay, lesbiche e transgender che vivono e lavorano in nove paesi diversi in Europa e nell'America del Nord.

«Purtroppo nessuna azienda italiana ha partecipato quest'anno all'Equality Index», aggiunge Angelo Caltagirone, presidente di egma, l'associazione europea di manager gay e lesbiche, e membro del comitato organizzativo dell'Index. «Ma non va dimenticato che praticamente tutte le aziende partecipanti hanno sedi in Italia, presso le quali lavorano oltre 100'000 persone.»

Per scaricare una copia del rapporto sull'Equality Index: [www.iglcc.org/index2010](http://www.iglcc.org/index2010)

_**L'IGLCC in sintesi  
L’International Gay and Lesbian Chamber of Commere (IGLCC – Camera di Commercio Gay e Lesbica Internazionale) è stata fondata nel 2006 ad Amburgo in Germania e ha sede a Montréal in Canada. L'IGLCC è la rete business LGBT leader in campo internazionale e opera in 15 paesi tramite 18 camere di commercio e organizzazioni professionali. L'IGLCC rappresenta gli interessi di oltre 55 milioni di dipendenti, titolari di aziende e consumatori in tutto il mondo appartenenti alla comunità lesbica, gay, bisessuale e transgender (LGBT).**_

Per ulteriori informazioni o per richieste da parte dei media:

America del Nord  
Pascal Lépine  
Segretario generale IGLCC  
+1 514-808-9339

Europa  
David Pollard  
Presidente dell'International Business Equality Index Committee  
+31 6 340 566 04

Italia  
Angelo Caltagirone  
Vice Chairman  
+41 79 322 01 05