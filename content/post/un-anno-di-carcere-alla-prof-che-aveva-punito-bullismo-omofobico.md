---
title: 'Un anno di carcere alla prof. che aveva punito bullismo omofobico'
date: Thu, 24 Feb 2011 14:15:10 +0000
draft: false
tags: [appello, bullismo, CARCERE, certi diritti, Comunicati stampa, OMOFOBIA, palermo, RADICALI, suicidio]
---

Certi Diritti esprime solidarietà alla professoressa condannata ad un anno di carcere, in appello, per aver tentato di fermare il bullismo omofobico nella sua classe.

Roma, 24 febbraio 2011

Una recente sentenza della Corte d’Appello di Palermo ha condannato ad un anno di carcere per abuso dei mezzi di correzione una professoressa che nel gennaio 2006 aveva tentato di interrompere una serie di atti di bullismo omofobico ai danni di un suo alunno, facendo scrivere alla lavagna cento volte al responsabile “sono un deficiente” e aprendo una discussione in classe.  
   
Aggredita verbalmente e denunciata dal padre dell’alunno punito, la professoressa era stata assolta in primo grado il 27 giugno 2007.  Il giudice, escludendo la sussistenza di danni morali a carico del punito, aveva pienamente riconosciuto la validità dell’intervento della professoressa: se non fosse intervenuta, la professoressa “avrebbe finito per accreditare (…) l’idea che condotte vessatorie a danno dei più deboli sarebbero state comunque accettate”.

Dalla nuova sentenza, che rovescia la precedente e infligge alla professoressa la pena gravissima di un anno di carcere, esce rafforzata  la posizione del padre del ragazzo, incentrata sul rifiuto di condannare i comportamenti vessatori, ben documentati, del figlio.  
Ma soprattutto, su un piano più generale, il messaggio che la sentenza attuale finisce col mandare è una conferma dell’impunità dei bulli, una forte dissuasione degli insegnanti dall’assumere in pieno i loro doveri educativi, e una riduzione all’irrilevanza del diritto di tutti alla sicurezza e alla dignità contro ogni discriminazione.

L’Associazione Radicale Certi Diritti esprime piena solidarietà alla professoressa punita, e grave allarme per questo nuovo contributo alla diffusione della cultura dell’omofobia, di cui sono il risultato (per limitarsi al mondo della scuola) mille episodi, tra i quali la persecuzione (denunciata e non interrotta da nessuno) che portò al suicidio il giovane Matteo.  Una vicenda orribile (ricordata anche nella sentenza di primo grado della professoressa palermitana) della quale, alla fine, sono rimaste nell’ombra tutte le responsabilità.