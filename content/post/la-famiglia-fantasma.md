---
title: 'La Famiglia Fantasma'
date: Fri, 23 Apr 2010 19:07:47 +0000
draft: false
tags: [Senza categoria]
---

**La Famiglia Fantasma: DiCo, PACS e matrimoni omosessuali, la politica italiana in crisi**

di Gian Mario Felicetti

_Milioni di Italiani sono_ **_fantasmi:_**_  
senza storia  
senza legge    
senza futuro_

La Repubblica Italiana si ostina a negare il riconoscimento istituzionale a milioni di coppie, obbligate a non sposarsi, come ai tempi delle leggi razziali.

**RISCATTARE LA PROPRIA STORIA** – Questa condizione di subalternità è dannosa per tutta la società civile perché le famiglie e gli affetti omosessuali sono fondamentali per l’identità culturale di ogni popolo. L’Italia omofoba è un albero sofferente dalle radici recise.

[**_![GAY_quadrati_verticale](http://www.certidiritti.org/wp-content/uploads/2009/11/GAY_quadrati_verticale.png)_**](http://bioetiche.blogspot.com/2008/01/intervista-vittorio-lingiardi-su.html)

**OTTENERE IL RICONOSCIMENTO DELLA LEGGE** \- Per essere riconosciuti dalla legge, bisogna conoscere la legge e avere fiducia nelle istituzioni. Così diventa facile confutare i più radicati luoghi comuni: la Costituzione Italiana definisce il matrimonio come unione tra coniugi e non vieta in nessun modo il matrimonio tra persone dello stesso sesso.

**CONQUISTARE IL PROPRIO FUTURO** – Il libro aiuta a costruirsi un proprio lessico, necessario per aprirsi con serenità e onestà intellettuale al dibattito sul diritto alla paternità e maternità da parte delle persone omosessuali: una lecita dimensione affettiva, totalmente nuova ed epocale per gay e lesbiche di tutto il mondo.

_Il libro si può acquistare online e può essere ordinato in tutte le librerie._