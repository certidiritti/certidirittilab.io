---
title: 'Diritti umani. Revisione Periodica Universale con più raccomandazioni di sempre all''Italia, è campanello d''allarme'
date: Wed, 10 Dec 2014 11:11:57 +0000
draft: false
tags: [Transnazionale]
---

[![UN-UPR-Geneva](http://www.certidiritti.org/wp-content/uploads/2014/12/UN-UPR-Geneva-300x200.jpg)](http://www.certidiritti.org/wp-content/uploads/2014/12/UN-UPR-Geneva.jpg)"187 raccomandazioni quando nel 2009, nel corso dell'ultimo processo di Revisione Periodica Universale, erano 'solo' 92. E' un campanello d'allarme inquietante quello che risuona da Ginevra" Queste le parole di Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, che ha scritto una [lettera aperta](http://www.huffingtonpost.it/yuri-guaiana/lettera-napolitano-renzi-mancorni-giornata-mondiale-diritti-umani_b_6289830.html) al Presidente Napolitano, al Presidente del Consiglio Renzi e al Presidente della Commissione Diritti Umani al Senato Luigi Manconi. Tra le richieste, quella di audire in Senato le organizzazioni che si occupano di promozione e tutela dei diritti umani in Italia. "Ben 20 stati hanno richiesto al nostro Paese che venga posto in essere e adeguatamente finanziato un istituto per i Diritti Umani, per la prima volta ci sono state 5 raccomandazioni sul riconoscimento dei diritti delle persone LGBTI. Servono immediatamente delle risposte concrete".

Comunicato Stampa dell’Associazione Radicale Certi Diritti