---
title: 'Il Parlamento Europeo approva il Rapporto Lunacek e chiede a grande maggioranza una strategia europea per i diritti umani delle persone LGBT'
date: Tue, 04 Feb 2014 13:47:14 +0000
draft: false
tags: [Europa]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 4 febbraio 2014

Il Parlamento Europeo ha oggi approvato una raccomandazione che chiede alla Commissione Europea, agli Stati membri e alle agenzie UE di «lavorare congiuntamente» per una strategia europea pluriennale «volta a proteggere i diritti fondamentali delle persone LGBTI».

Il documento, noto come Rapporto Lunacek, dal nome della relatrice Ulrike Lunacek che è anche Co-Presidente dell'Intergruppo LGBTI al Parlamento Europeo, è stato approvato con 394 voti a favore e 176 contrari. I voti a favore sono arrivati dai 5 maggiori gruppi politici: Partito Popolare Europeo, Alleanza Progressista dei Socialisti e dei Democratici, Alleanza dei Democratici e dei Liberali per l'Europa, Verdi, Sinistra Unitaria Europea - Sinistra Verde Nordica. Anche alcuni Conservatori e Riformisti Europei hanno sostenuto il Rapporto.

Per il Parlamento Europeo la strategia LGBTI dovrà combattere le discriminazioni basate sull'orientamento sessuale e l'identità di genere nel campo del lavoro, dell'educazione, della salute, dell'accesso a beni e servizi, delle libertà di movimento ed espressione, per quanto riguarda i crimini d'odio, l'asilo e le relazioni internazionali.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: «il voto di oggi è particolarmente importante poiché gruppi organizzati di estremisti religiosi e ultra-conservatori avevano orchestrato una campagna di sabotaggio del Rapporto Lunacek, ma sono stati sconfitti da un voto che dimostra come la maggioranza degli europarlamentari aderiscano ai principi fondamentali dell'Unione Europea di uguaglianza, non discriminazione e dignità per tutti. L'ampia maggioranza con cui il Rapporto è stato approvato manda un forte messaggio alla Commissione Europea e agli Stati membri secondo il quale la discriminazione delle persone LGBTI non è accettabile e va combattuta. L'Unione Europea deve fissare degli obiettivi precisi per combattere le discriminazioni basate su orientamento sessuale e identità di genere, raggiungerli ed istituire un efficiente meccanismo di monitoraggio per misurare i progressi in questo campo».

* * *

**394 A FAVORE**

ALDE: Alvaro, Aylward, Bearder, Bennahmias, Bennion, Bilbao Barandica, Bowles, Creutzmann, Crowley, Davies, De Backer,

Donskis, Duff, Gallagher, Gerbrandy, Goerens, Goulard, Griesbeck, Hall, Harkin, Hellvig, Hirsch, Hyusmenova, Jensen,

Johansson, Jäätteenmäki, Kacin, Kazak, Klinz, Koch-Mehrin, Krahmer, Lambsdorff, Lepage, Ludford, Lyon, Løkkegaard,

McMillan-Scott, Meissner, Michel, Mulder, Newton Dunn, Neyts-Uyttebroeck, Nicolai, Oviir, Panayotov, Parvanova,

Paulsen, Reimers, Ries, Rinaldi, Rochefort, Rohde, Savisaar-Toomast, Schaake, Schmidt, Silaghi, Skylakakis, Takkula,

Taylor Rebecca, Thein, Theurer, Torvalds, Tremosa i Balcells, Uggias, Vattimo, Verhofstadt, Vălean, Weber Renate,

Werthmann, Wikström, Zanoni, de Sarnez, in 't Veld, van Baalen

ECR: Bokros, Češková

EFD: Binev, Terho, Tzavela, Vanhecke

GUE/NGL: Anderson, Chountis, Ernst, Ferreira João, Flasarová, Hadjigeorgiou, Händel, Hénin, Klute, Kohlíček, Le Hyaric, Liotard,

Lösing, Maštálka, Meyer, Michels, Murphy, Mélenchon, Omarjee, Ransdorf, Rubiks, Scholz, Sousa, Søndergaard,

Triantaphyllides, Vergiat, Wils, Zimmer, Zuber, de Jong

NI: Bonanini, Childers, Ehrenhauser, Severin, Sosa Wagner

PPE: Bach, Bastos, Bauer, Belet, Bendtsen, Böge, Carvalho, Cavada, Coelho, Corazza Bildt, Dehaene, Delvaux, Dorfmann,

Díaz de Mera García Consuegra, Ehler, Engel, Estaràs Ferragut, Fernandes, Fisas Ayxela, Fjellner, Florenz, Gabriel,

Garriga Polledo, Grosch, Gräßle, Herranz García, Higgins, Hökmark, Ibrisagic, Jiménez-Becerril Barrio, Jordan, Járóka,

Kaczmarek, Kalniete, KariĦš, Kelly, Korhola, Koumoutsakos, Kratsa-Tsagaropoulou, Landsbergis, Lope Fontagné,

Luhan, Lulling, Matera, Mazej Kukovič, McGuinness, Metsola, Millán Mon, Neynsky, Oomen-Ruijten, Ortiz Vilella,

Patriciello, Pitsillides, Poupakis, Ronzulli, Ross, Sarvamaa, Sedó i Alabart, Striffler, Svensson, Szájer, Teixeira,

Theocharous, Wortmann-Kool, Zalba Bidegain, Zanicchi, de Lange, van Nistelrooij, van de Camp

S&D: Abela Baldacchino, Alves, Andrieu, Andrés Barea, Arlacchi, Arsenis, Attard-Montalto, Ayala Sender, Badia i Cutchet,

Baldini, Barracciu, Berman, Berès, Borzan, Bozkurt, Brzobohatá, Bullmann, Capoulas Santos, Caronna, Cashman,

Castex, Cercas, Christensen, Cofferati, Correia de Campos, Cortés Lastra, Costa, Costello, Cottigny, Cuschieri,

Daerden, Danellis, De Angelis, De Castro, De Keyser, Domenici, Désir, El Khadraoui, Ertug, Estrela, Fajon, Falbr,

Flašíková Beňová, Frigo, Färm, García Pérez, García-Hierro Caraballo, Gardiazábal Rubial, Gebhardt, Geier, Geringer

de Oedenberg, Gierek, Glante, Goebbels, Gomes, Groote, Gualtieri, Guerrero Salom, Guillaume, Gurmai, Gutiérrez

Prieto, Göncz, Haug, Hedh, Herczog, Hoang Ngoc, Honeyball, Howitt, Hughes, Irigoyen Pérez, Jaakonsaari,

Kadenbach, Kalfin, Kammerevert, Kirilov, Koppa, Krehl, Kreissl-Dörfler, Lange, Leichtfried, Leinen, Liberadzki,

Ludvigsson, Lyubcheva, López Aguilar, Martin David, Martínez Martínez, Masip Hidalgo, Maňka, McAvan, McCarthy,

Menéndez del Valle, Merkies, Milana, Mizzi, Moraes, Moreira, Muñiz De Urquiza, Mynář, Neuser, Neveďalová, Nilsson,

Obiols, Olejniczak, Padar, Paleckis, Panzeri, Pargneaux, Petrović Jakovina, Picula, Pirillo, Pittella, Poc, Podimata,

Prendergast, Prodi, Rapkay, Rapti, Regner, Repo, Riera Madurell, Rodust, Roth-Behrendt, Rouček, Schaldemose,

Sehnalová, Senyszyn, Simon, Simpson, Sippel, Siwiec, Skinner, Sophocleous, Stavrakakis, Steinruck, Stihler, Swoboda,

Sánchez Presedo, Tarabella, Thomas, Thomsen, Tirolien, Toia, Trautmann, Turunen, Ulvskog, Valjalo, Van Brempt,

Vaughan, Vergnaud, Weber Henri, Weidenholzer, Weiler, Westlund, Westphal, Willmott, Yáñez-Barnuevo García,

Zemke

Verts/ALE: Albrecht, Alfonsi, Andersdotter, Auken, Benarab-Attou, Besset, Bicep, Bové, Breyer, Bélier, Bütikofer, Chrysogelos,

Cochet, Cohn-Bendit, Cornelissen, Cramer, Cronberg, Delli, Demesmaeker, Durant, Eickhout, Engström, Evans,

Flautre, Giegold, Grèze, Harms, Hassi, Hudghton, Häfner, Häusling, Irazabalbeitia Fernández, Jadot, Joly, Keller, Kiil-

Nielsen, Lambert, Lamberts, Lichtenberger, Lunacek, Lövin, Rivasi, Romeva i Rueda, Rühle, Sargentini, Schlyter,

Schroedter, Schulz Werner, Staes, Tarand, Trüpel, Turmes

**176 – CONTRARI**

ECR: Bielan, Czarnecki, Eppink, Gróbarczyk, Kowal, Legutko, Migalski, Piotrowski, Poręba, Strejček, Szymański, Tomaševski,

Wojciechowski, Zasada, Zīle

EFD: Allam, Belder, Bizzotto, Cymański, Fontana, Imbrasas, Kurski, Morganti, Paksas, Paška, Provera, Salvini, Scottà,

Speroni, Włosowicz, Ziobro, de Villiers

NI: Borghezio, Brons, Claeys, Dodds, Gollnisch, Griffin, Le Pen Marine, Morvai, Mölzer, Obermayr, Stadler, Stoyanov,

Vadim Tudor, van der Stoep

PPE: Angelilli, Antonescu, Auconie, Audy, Baldassarre, Bartolozzi, Becker, Berlato, Berra, Bertot, Bodu, Borys, Boulland,

Bratkowski, Brok, Bušić, Bánki, Březina, Cadec, Cancian, Casini, Caspary, Dantin, Dati, Daul, Deß, Feio, Ferber,

Fidanza, Franco, Gallo, Gardini, Gargani, Gauzès, Grossetête, Grzyb, Gutiérrez-Cortines, Handzlik, Hibner, Hohlmeier,

Hortefeux, Jahr, Jeggle, Juvin, Kalinowski, Karas, Kastler, Kelam, Klaß, Koch, Köstinger, La Via, Lamassoure, Langen,

Le Grip, Liese, Maletić, Mann, Marcinkiewicz, Marinescu, Mastella, Mathieu Houillon, Matula, Mayer, Mayor Oreja,

Mazzoni, Melo, Mikolášik, Morin-Chartier, Morkūnait÷-Mikul÷nien÷, Mészáros, Niculescu, Niebler, Olbrycht, Pallone,

Patrão Neves, Peterle, Pieper, Pirker, Plenković, Posselt, Preda, Proust, Quisthoudt-Rowohl, Reul, Riquet, Roatta,

Roithová, Rossi, Rübig, Salafranca Sánchez-Neyra, Sanchez-Schmid, Saudargas, Saïfi, Schnellhardt, Schnieber-

Jastram, Schwab, Scurria, Seeber, Siekierski, Silvestris, Skrzydlewska, Sommer, Sonik, Stauner, Stier, Stolojan, Surján,

Sógor, Thyssen, Trematerra, Tıkés, Ulmer, Vaidere, Verheyen, Vidal-Quadras, Vlasto, Voss, Wałęsa, Weber Manfred,

Wieland, Winkler Iuliu, Winkler Hermann, Zaleski, Zalewski, Zeller, Záborská, Łukacijewska, Šadurskis, Šťastný

**72 ABSTENTIONS**

ALDE: Godmanis

ECR: Andreasen, Ashworth, Atkins, Bradbourn, Cabrnoch, Callanan, Campbell Bannerman, Deva, Elles, Fajmon, Ford,

Foster, Fox, Girling, Hannan, Harbour, Kamall, Karim, Kirkhope, Kožušník, McClarkin, McIntyre, Nicholson, Ouzký,

Rosbach, Stevenson, Swinburne, Tannock, Van Orden, Yannakoudakis

EFD: (The Earl of) Dartmouth, Agnew, Clark, Farage, Helmer, Messerschmidt, Salavrakos

GUE/NGL: Angourakis, Toussas

NI: Bloom, Colman, Hartong, Martin Hans-Peter, Nattrass, Stassen, Zijlstra

PPE: Antinoro, Bagó, Balz, Borissov, Băsescu, Danjean, De Mita, Fraga Estévez, Giannakou, Glattfelder, Gál, Iacolino, Kuhn,

Kukan, Kósa, Lehne, Malinov, Mitchell, Pack, Panayotova, Pöttering, Schöpflin, Urutchev, de Grandes Pascual, İry

**\*\*\***

**TESTO COME APPROVATO DAL PE:**

<Amend><Date>{29/01/2014}29.1.2014</Date><ANo>A7-0009</ANo>/<NumAm>2</NumAm>

**Emendamento  **<NumAm>**2**</NumAm>

<RepeatBlock-By><Members>**Roberta Metsola**</Members>

<AuNomDe>{PPE}a nome del gruppo PPE</AuNomDe>

<Members>**Michael Cashman**</Members>

<AuNomDe>{S&D}a nome del gruppo S&D</AuNomDe>

<Members>**Sophia in 't Veld**</Members>

<AuNomDe>{ALDE}a nome del gruppo ALDE</AuNomDe>

<Members>**Ulrike Lunacek**</Members>

<AuNomDe>{VERT}a nome del gruppo Verts/ALE</AuNomDe>

<Members>**Cornelis de Jong**</Members>

<AuNomDe>{GUE}a nome del gruppo GUE/NGL</AuNomDe>

</RepeatBlock-By>

<TitreType>**Relazione**</TitreType>**                                                                            A7-0009/2014**

<Rapporteur>**Ulrike Lunacek**</Rapporteur>

<Titre>Tabella di marcia dell'UE contro l'omofobia e la discriminazione legata all'orientamento sessuale e all'identità di genere</Titre>

<DocRef>2013/2183(INI)</DocRef>

<DocAmend>**Proposta di risoluzione (articolo 157, paragrafo 4, del regolamento) volta a sostituire la proposta di risoluzione non legislativa A7-0009/2014**</DocAmend>

<Article>**Risoluzione del Parlamento europeo sulla tabella di marcia dell'UE contro l'omofobia e la discriminazione legata all'orientamento sessuale e all'identità di genere**</Article>

_Il Parlamento europeo_,

–        visto l'articolo 2 del trattato sull'Unione europea,

–        visti gli articoli 8 e 10 del trattato sul funzionamento dell'Unione europea,

–        vista la Carta dei diritti fondamentali dell'Unione europea, in particolare l'articolo 21,

–        vista la Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali,

–        vista la raccomandazione CM/Rec(2010)5 del Comitato dei ministri del Consiglio d'Europa agli Stati membri, del 31 marzo 2010, sulle misure per combattere la discriminazione basata sull'orientamento sessuale o l'identità di genere,

–        vista la comunicazione della Commissione intitolata "Strategia per un'attuazione effettiva della Carta dei diritti fondamentali dell'Unione europea" (COM(2010)0573),

–        visti la relazione 2012 della Commissione sull'applicazione della Carta dei diritti fondamentali dell'Unione europea (COM(2013)0271) e i relativi documenti di lavoro,

–        viste la proposta di direttiva del Consiglio recante applicazione del principio di parità di trattamento fra le persone indipendentemente dalla religione o le convinzioni personali, la disabilità, l'età o l'orientamento sessuale (COM(2008)0426) e la sua posizione del 2 aprile 2009 in merito a tale proposta\[1\],

–        visti gli orientamenti per la promozione e la tutela dell'esercizio di tutti i diritti umani da parte di lesbiche, gay, bisessuali, transgender e intersessuali (LGBTI), adottati dal Consiglio dell'Unione europea nella sua riunione del 24 giugno 2013,

–        vista la relazione dell'Agenzia dell'Unione europea per i diritti fondamentali del novembre 2010 in materia di omofobia, transfobia e discriminazioni basate sull'orientamento sessuale e l'identità di genere,

–        visti i risultati del sondaggio relativo a lesbiche, gay, bisessuali e transgender nell'Unione europea, condotto dall'Agenzia per i diritti fondamentali (FRA) e pubblicato il 17 maggio 2013,

–        visto il parere della FRA del 1° ottobre 2013 sulla situazione dell'uguaglianza nell'Unione europea a 10 anni dall'attuazione iniziale delle direttive in materia di uguaglianza,

–        vista la sua risoluzione del 24 maggio 2012 sulla lotta all'omofobia in Europa\[2\],

–        vista la sua risoluzione del 12 dicembre 2012 sulla situazione dei diritti fondamentali nell'Unione europea (2010-2011)\[3\],

–        vista la sua risoluzione del 14 marzo 2013 sul rafforzamento della lotta contro il razzismo, la xenofobia e i reati generati dall'odio\[4\],

–        visto l'articolo 157 del suo regolamento,

A.      considerando che l'Unione europea si fonda sui valori del rispetto della dignità umana, della libertà, della democrazia, dell'uguaglianza, dello Stato di diritto e del rispetto dei diritti umani, compresi i diritti delle persone appartenenti a minoranze;

B.      considerando che nella definizione e nell'attuazione delle sue politiche e azioni, l'Unione europea mira a combattere la discriminazione fondata sul sesso, la razza o l'origine etnica, la religione o le convinzioni personali, la disabilità, l'età o l'orientamento sessuale;

C.      considerando che a giugno 2013 il Consiglio dell'Unione europea ha adottato fermi orientamenti per la promozione e la tutela dell'esercizio di tutti i diritti umani da parte di lesbiche, gay, bisessuali, transgender e intersessuali (LGBTI) al di fuori dell'Unione europea, e che dovrebbe garantire una tutela efficace di tali diritti all'interno dell'UE;

D.      considerando che l'Unione europea già coordina la sua azione attraverso politiche globali in materia di uguaglianza e non discriminazione tramite la "Strategia quadro per la non discriminazione e le pari opportunità per tutti", in materia di uguaglianza di genere tramite la "Strategia per la parità tra donne e uomini 2010-2015", in materia di disabilità tramite la "Strategia europea sulla disabilità 2010-2020" e in materia di uguaglianza per i rom tramite il "Quadro dell'UE per le strategie nazionali di integrazione dei Rom fino al 2020";

E.       considerando che, nella sua "Strategia per un'attuazione effettiva della Carta dei diritti fondamentali dell'Unione europea", la Commissione ha riconosciuto la necessità di sviluppare politiche specifiche fondate sui trattati, riguardanti diritti fondamentali particolari;

F.       considerando che, nel sondaggio condotto nel 2013 sulle persone LGBT nell'UE, l'Agenzia dell'Unione europea per i diritti fondamentali ha rilevato che, in tutta l'UE nell'anno precedente al sondaggio, un intervistato LGBT su due si sentiva discriminato o molestato a causa dell'orientamento sessuale, uno su tre ha subito discriminazioni nell'accesso a beni o servizi, uno su quattro è stato aggredito fisicamente e uno su cinque è stato discriminato in materia di occupazione o impiego;

G.      considerando che la FRA ha raccomandato all'UE e agli Stati membri di elaborare piani di azione che promuovano il rispetto delle persone LGBT e la tutela dei loro diritti fondamentali;

H.      considerando che a maggio 2013 undici ministri per le Pari opportunità\[5\] hanno invitato la Commissione a definire una politica globale dell'UE per l'uguaglianza delle persone LGBT e dieci Stati membri\[6\] hanno già adottato o stanno esaminando politiche analoghe a livello nazionale e regionale;

I.        considerando che il Parlamento europeo ha chiesto in dieci occasioni l'istituzione di uno strumento programmatico globale dell'Unione europea che assicuri l'uguaglianza sulla base dell'orientamento sessuale e dell'identità di genere;

**_Considerazioni generali_**

1.       condanna con forza qualsiasi forma di discriminazione legata all'orientamento sessuale e all'identità di genere e deplora vivamente che i diritti fondamentali di lesbiche, gay, bisessuali, transgender e intersessuali (LGBTI) non siano ancora sempre rispettati appieno nell'Unione europea;

2.       ritiene che nell'Unione europea manchi attualmente una politica globale per la tutela dei diritti fondamentali delle persone LGBTI;

3.       riconosce che la responsabilità di tutelare i diritti fondamentali spetta congiuntamente alla Commissione europea e agli Stati membri; invita la Commissione ad avvalersi delle proprie competenze nella massima misura possibile, anche agevolando lo scambio di buone prassi tra gli Stati membri; invita gli Stati membri a rispettare gli obblighi previsti dal diritto dell'Unione europea e dalla raccomandazione del Consiglio d'Europa sulle misure per combattere la discriminazione basata sull'orientamento sessuale o l'identità di genere;

**_Contenuto della tabella di marcia_**

4.       invita la Commissione europea, gli Stati membri e le agenzie competenti a collaborare alla definizione di una politica globale pluriennale per la tutela dei diritti fondamentali delle persone LGBTI, vale a dire una tabella di marcia, una strategia o un piano di azione che includano i temi e gli obiettivi indicati di seguito;

**A.           Azioni orizzontali per l'attuazione della tabella di marcia**

i)          la Commissione dovrebbe adoperarsi per garantire il rispetto dei diritti esistenti in tutte le sue attività e in tutti i settori in cui è competente integrando le questioni collegate ai diritti fondamentali delle persone LGBTI in tutte le attività pertinenti, ad esempio in sede di elaborazione delle politiche e proposte future o di monitoraggio dell'attuazione del diritto dell'Unione europea;

ii)         la Commissione dovrebbe agevolare, coordinare e monitorare lo scambio di buone prassi tra gli Stati membri attraverso il metodo di coordinamento aperto;

iii)        le agenzie competenti dell'Unione europea, tra cui l'Agenzia dell'Unione europea per i diritti fondamentali (FRA), l'Istituto europeo per l'uguaglianza di genere (EIGE), la Fondazione europea per il miglioramento delle condizioni di vita e di lavoro (Eurofound), l'Accademia europea di polizia (CEPOL), l'Unità europea di cooperazione giudiziaria (Eurojust), la Rete giudiziaria europea (RGE) e l'Ufficio europeo di sostegno per l'asilo (EASO), dovrebbero integrare le questioni correlate all'orientamento sessuale e all'identità di genere nelle proprie attività e continuare a fornire alla Commissione e agli Stati membri una consulenza basata su elementi concreti in materia di diritti fondamentali delle persone LGBTI;

iv)        la Commissione e gli Stati membri dovrebbero essere incoraggiati a raccogliere periodicamente dati pertinenti e comparabili sulla situazione delle persone LGBTI nell'UE, di concerto con le agenzie competenti e con Eurostat, agendo nel pieno rispetto delle norme dell'Unione in materia di protezione dei dati;

v)         la Commissione e gli Stati membri dovrebbero incoraggiare, di concerto con le agenzie competenti, la formazione e il potenziamento delle capacità degli organismi nazionali competenti in materia di uguaglianza, delle istituzioni nazionali per i diritti umani e di altre organizzazioni incaricate della promozione e della tutela dei diritti fondamentali delle persone LGBTI;

vi)        la Commissione e gli Stati membri dovrebbero adoperarsi, di concerto con le agenzie competenti, per sensibilizzare i cittadini in merito ai diritti delle persone LGBTI;

**B.           Disposizioni generali in materia di non discriminazione**

i)          gli Stati membri dovrebbero consolidare l'attuale quadro giuridico dell'UE lavorando all'adozione della proposta di direttiva recante applicazione del principio di parità di trattamento fra le persone indipendentemente dalla religione o le convinzioni personali, la disabilità, l'età o l'orientamento sessuale, provvedendo tra l'altro a chiarire il campo di applicazione delle relative disposizioni e le spese connesse;

ii)         la Commissione, gli Stati membri e le agenzie competenti dovrebbero prestare particolare attenzione alla situazione delle lesbiche, che sono vittime di discriminazioni e violenze multiple (fondate sia sul sesso sia sull'orientamento sessuale), e provvedere di conseguenza all'elaborazione e all'attuazione di opportune politiche antidiscriminazione;

**C.           Non discriminazione nel settore dell'occupazione**

i)          nel monitorare l'attuazione della direttiva 2000/78/CE che stabilisce un quadro generale per la parità di trattamento in materia di occupazione e di condizioni di lavoro, la Commissione dovrebbe rivolgere un'attenzione particolare alla tematica dell'orientamento sessuale; nel monitorare l'attuazione della direttiva 2006/54/CE riguardante l'attuazione del principio delle pari opportunità e della parità di trattamento fra uomini e donne in materia di occupazione e impiego, la Commissione dovrebbe rivolgere un'attenzione particolare alla tematica dell'identità di genere;

ii)         la Commissione dovrebbe definire, di concerto con le agenzie competenti, orientamenti atti a specificare che, nella direttiva 2006/54/CE riguardante l'attuazione del principio delle pari opportunità e della parità di trattamento fra uomini e donne in materia di occupazione e impiego, il termine "sesso" contempla anche i transgender e gli intersessuali;

iii)        gli organismi competenti in materia di uguaglianza dovrebbero essere incoraggiati a informare le persone LGBTI, nonché i sindacati e le organizzazioni di datori di lavoro, in merito ai diritti di tali persone;

**D.           Non discriminazione nel settore dell'istruzione**

i)          la Commissione dovrebbe promuovere l'uguaglianza e la lotta alle discriminazioni basate sull'orientamento sessuale e sull'identità di genere in tutti i suoi programmi dedicati all'istruzione e ai giovani;

ii)         la Commissione dovrebbe agevolare, attraverso il metodo non vincolante del coordinamento aperto, la condivisione di buone prassi fra gli Stati membri nell'ambito dell'istruzione formale, anche a livello di materiali didattici e di politiche di lotta al bullismo e alla discriminazione;

iii)        la Commissione dovrebbe agevolare, attraverso il metodo non vincolante del coordinamento aperto, la condivisione di buone prassi fra gli Stati membri in tutti i rispettivi settori dedicati all'istruzione e alla gioventù, compresi i servizi di assistenza ai giovani e i servizi sociali;

**E.            Non discriminazione nel settore della sanità**

i)          la Commissione dovrebbe inserire le problematiche sanitarie delle persone LGBTI nell'ambito delle pertinenti politiche sanitarie strategiche di più ampio respiro, comprese quelle concernenti l'accesso all'assistenza sanitaria, l'uguaglianza sotto il profilo sanitario e la voce globale dell'UE nelle questioni correlate alla salute;

ii)         la Commissione dovrebbe continuare a collaborare con l'Organizzazione mondiale della sanità per depennare i disturbi dell'identità di genere dall'elenco dei disturbi mentali e comportamentali e per garantire una riclassificazione non patologizzante in sede di negoziati relativi all'undicesima versione della classificazione internazionale delle malattie (ICD-11);

iii)        la Commissione dovrebbe sostenere gli Stati membri nella formazione dei professionisti del settore sanitario;

iv)        la Commissione e gli Stati membri dovrebbero condurre ricerche sulle questioni sanitarie specifiche alle persone LGBTI;

v)         gli Stati membri dovrebbero tenere conto delle persone LGBTI nei programmi e nelle politiche nazionali in materia di sanità, garantendo che i programmi di formazione, le politiche sanitarie e le indagini sulla salute prendano in considerazione le questioni sanitarie specifiche di tali soggetti;

vi)        gli Stati membri dovrebbero introdurre procedure di riconoscimento giuridico del genere, oppure rivedere quelle esistenti, onde garantire il pieno rispetto del diritto dei transgender alla dignità e all'integrità fisica;

**F.            Non discriminazione in relazione a beni e servizi**

i)          nel monitorare l'attuazione della direttiva 2004/113/CE che attua il principio della parità di trattamento tra uomini e donne per quanto riguarda l'accesso a beni e servizi e la loro fornitura, la Commissione dovrebbe rivolgere un'attenzione particolare all'accesso a beni e servizi da parte dei transgender;

**G.           Azioni specifiche a favore di transgender e intersessuali**

i)          la Commissione dovrebbe fare in modo che l'identità di genere sia inclusa tra i motivi di discriminazione vietati da ogni futura legislazione sull'uguaglianza, rifusioni comprese;

ii)         la Commissione dovrebbe integrare le questioni specifiche legate a transgender e intersessuali in tutte le pertinenti politiche dell'UE, replicando l'approccio adottato nella Strategia per la parità tra donne e uomini;

iii)        gli Stati membri dovrebbero garantire che gli organismi competenti in materia di uguaglianza siano informati in merito ai diritti dei transgender e degli intersessuali e alle questioni specifiche che li riguardano, assicurando altresì che ricevano una formazione al riguardo;

iv)        la Commissione, gli Stati membri e le agenzie competenti dovrebbero ovviare all'attuale carenza di informazioni, ricerche e normative pertinenti in relazione ai diritti umani degli intersessuali;

**H.      Cittadinanza, famiglie e libera circolazione**

i)          la Commissione dovrebbe elaborare orientamenti per garantire l'attuazione della direttiva 2004/38/CE relativa al diritto dei cittadini dell'Unione e dei loro familiari di circolare e soggiornare liberamente nel territorio degli Stati membri e della direttiva 2003/86/CE relativa al diritto al ricongiungimento familiare, nell'ottica di assicurare il rispetto di tutte le forme di famiglia riconosciute a livello giuridico dalle leggi nazionali degli Stati membri;

ii)         la Commissione dovrebbe presentare proposte finalizzate al riconoscimento reciproco degli effetti di tutti gli atti di stato civile nell'Unione europea al fine di ridurre gli ostacoli discriminatori di natura giuridica e amministrativa per i cittadini e le relative famiglie che esercitano il proprio diritto di libera circolazione;

iii)        la Commissione e gli Stati membri dovrebbero valutare se le attuali restrizioni in materia di cambiamento dello stato civile e modifica dei documenti d'identità applicabili ai transgender pregiudichino la capacità di queste persone di godere del proprio diritto di libera circolazione;

iv)        gli Stati membri che hanno adottato normative in materia di convivenza, unioni registrate o matrimoni di coppie dello stesso sesso dovrebbero riconoscere le disposizioni affini adottate da altri Stati membri;

**I. Libertà di riunione e di espressione**

i)          gli Stati membri dovrebbero adoperarsi affinché siano garantiti il diritto alla libertà di espressione e il diritto alla libertà di riunione, in particolare per quanto riguarda le marce dell'orgoglio e le manifestazioni analoghe, assicurando che tali eventi si svolgano in maniera legale e che i partecipanti beneficino di una protezione efficace;

ii)         gli Stati membri dovrebbero astenersi dall'adottare leggi che limitino la libertà di espressione in relazione all'orientamento sessuale e all'identità di genere e riesaminare quelle già in vigore;

iii)        la Commissione e il Consiglio dell'Unione europea dovrebbero considerare gli Stati membri che adottano leggi per limitare la libertà di espressione in relazione all'orientamento sessuale e all'identità di genere responsabili di una violazione dei valori su cui si fonda l'Unione europea, e reagire di conseguenza;

**J.            Discorsi di incitamento all'odio e reati generati dall'odio**

i)          nell'attuare la direttiva 2012/29/UE in materia di diritti, assistenza e protezione delle vittime di reato, la Commissione dovrebbe monitorare e fornire assistenza agli Stati membri relativamente ai problemi specifici legati all'orientamento sessuale, all'identità di genere e all'espressione di genere, in particolare qualora i reati siano motivati da pregiudizi o discriminazioni che potrebbero essere correlati alle caratteristiche personali delle vittime;

ii)         la Commissione dovrebbe proporre una rifusione della decisione quadro del Consiglio sulla lotta contro talune forme ed espressioni di razzismo e xenofobia mediante il diritto penale includendo altre forme di reati generati dall'odio e di incitamento all'odio, ivi comprese quelle fondate sull'orientamento sessuale e sull'identità di genere;

iii)        la Commissione dovrebbe agevolare, di concerto con le agenzie competenti, lo scambio di buone prassi tra gli Stati membri per quanto riguarda la formazione e l'istruzione delle forze di polizia, della magistratura inquirente, dei giudici e degli operatori dei servizi di assistenza alle vittime;

iv)        l'Agenzia per i diritti fondamentali dovrebbe assistere gli Stati membri nel migliorare la raccolta di dati comparabili riguardo ai reati d'odio di matrice omofobica e transfobica;

v)         gli Stati membri dovrebbero registrare i reati generati dall'odio commessi contro persone LGBT e indagare al riguardo, nonché adottare legislazioni penali che vietino l'istigazione all'odio sulla base dell'orientamento sessuale e dell'identità di genere;

**K.           Asilo**

i)          di concerto con l'Ufficio europeo di sostegno per l'asilo (EASO) e le agenzie competenti e nel quadro dell'attuale legislazione e giurisprudenza dell'UE, la Commissione dovrebbe includere le questioni specifiche legate all'orientamento sessuale e all'identità di genere nell'attuazione e nel monitoraggio della legislazione in materia di asilo, comprese la direttiva 2013/32/UE recante procedure comuni ai fini del riconoscimento e della revoca dello status di protezione internazionale e la direttiva 2011/95/UE recante norme sull'attribuzione, a cittadini di paesi terzi o apolidi, della qualifica di beneficiario di protezione internazionale;

ii)         la Commissione e gli Stati membri dovrebbero garantire, di concerto con le agenzie competenti, che i professionisti che operano nel settore dell'asilo, compresi intervistatori e interpreti, ricevano una formazione adeguata – inclusa quella esistente – per trattare le questioni specificatamente correlate alle persone LGBTI;

iii)        la Commissione e gli Stati membri dovrebbero garantire, di concerto con l'EASO e in cooperazione con il Servizio europeo per l'azione esterna (SEAE), che la situazione giuridica e sociale delle persone LGBTI nei paesi di origine sia documentata sistematicamente e che tali informazioni siano messe a disposizione dei responsabili delle decisioni in materia d'asilo nel quadro delle informazioni sui paesi d'origine;

**L.            Allargamento e azione esterna**

i)          la Commissione dovrebbe proseguire l'attuale monitoraggio delle questioni collegate all'orientamento sessuale e all'identità di genere nei paesi in via di adesione;

ii)         la Commissione, il Servizio europeo per l'azione esterna, il rappresentante speciale dell'UE per i diritti umani e gli Stati membri dovrebbero applicare sistematicamente gli orientamenti del Consiglio per promuovere e tutelare l'esercizio di tutti i diritti umani da parte delle persone LGBTI e mantenere una posizione unitaria nella risposta alle violazioni di tali diritti;

iii)        la Commissione e il Servizio europeo per l'azione esterna dovrebbero mettere a disposizione dell'Ufficio europeo di sostegno per l'asilo e degli Stati membri le informazioni ottenute dalle delegazioni dell'UE in merito alla situazione delle persone LGBTI nei paesi terzi;

5.    sottolinea che una siffatta politica globale deve rispettare le competenze dell'Unione europea, delle sue agenzie e degli Stati membri;

6.    rammenta la necessità di rispettare la libertà di espressione e manifestazione di convinzioni od opinioni riconducibili al pluralismo delle idee, purché non istighino all'odio, alla violenza o alla discriminazione;

o

o        o

7.       incarica il suo Presidente di trasmettere la presente risoluzione al Consiglio dell'Unione europea, alla Commissione europea, al Servizio europeo per l'azione esterna, ai governi e ai parlamenti degli Stati membri, a tutte le agenzie citate e al Consiglio d'Europa.

Or. <Original>{EN}en</Original>

</Amend>

* * *

\[1\] GU C 241 E dell'8.10.2009, pag. 68.

\[2\] Testi approvati, P7_TA(2012)0222.

\[3\] Testi approvati, P7_TA(2012)0500.

\[4\] Testi approvati, P7_TA(2013)0090.

\[5\]  Austria, Belgio, Croazia, Danimarca, Finlandia, Francia, Italia, Lussemburgo, Malta, Paesi Bassi, Svezia.

\[6\]  Belgio, Croazia, Francia, Germania, Italia, Malta, Paesi Bassi, Portogallo, Spagna, Regno Unito.