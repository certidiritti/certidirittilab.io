---
title: 'Matrimonio gay in Francia: no Corte Costituzionale francese'
date: Fri, 28 Jan 2011 09:05:13 +0000
draft: false
tags: [Affermazione Civile, certi diritti, Comunicati stampa, CORTE COSTITUZIONALE, francia, GAY, lisbona, matrimonio gay, nizza, RADICALI, ricorsi, rovasio, trattato, unioni]
---

La Corte costituzionale francese: divieto matrimonio tra persone dello stesso sesso conforme a costituzione. Ma la campagna di Affermazione civile va avanti, in Italia e in Francia.

Roma, 28 gennaio 2011

**Comunicato Stampa dell’Associazione Radicale Certi Diritti:**

“Anche la Corte Costituzionale francese, come quella italiana lo scorso aprile, ha oggi stabilito che il divieto del matrimonio tra persone dello stesso sesso è conforme alla Costituzione e che è compito del legislatore la responsabilità di decidere su modifiche legislative.

La lotta per il superamento delle diseguaglianze continua  - in Italia con la campagna di Affermazione Civile dell’Associazione Radicale Certi Diritti - almeno fino a quando in tutti i paesi dell’Unione europea sarà riconosciuto il matrimonio tra persone dello stesso sesso. La classe politica dovrà prima o poi dare ascolto a chi si batte per il superamento delle diseguaglianze.

Anche in Francia, come in Italia, sono in corso campagne legali di coppie lesbiche e gay che prevedono ricorsi alla Corte Europea dei Diritti dell’Uomo e alla Corte di Giustizia Europea  per vedersi riconosciuto il matrimonio contratto in paesi come il Portogallo, Spagna, Belgio e Olanda. I Trattati di Nizza e di Lisbona sono molto chiari e le contraddizioni delle legislazioni nazionali prima o poi, anche con la via legale, dovranno essere superati dalle classi politiche.