---
title: 'Morte Wicky Hassan: il cordoglio dell''Associazione Radicale Certi Diritti'
date: Fri, 16 Dec 2011 13:56:06 +0000
draft: false
tags: [Movimento LGBTI]
---

Il fondatore del marchio Energie e Miss Sixty lo scorso 23 novembre aveva partecipato ad un evento di Certi Diritti e di Nuova proposta con Angelo Pezzana.

Roma, 16 dicembre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

L’Associazione Radicale Certi Diritti esprime il suo più profondo cordoglio e la sua vicinanza alla famiglia di Wicky Hassan, in particolare ai suoi figli, al suo compagno, alla Comunità ebraica di Roma e a tutti coloro che ne hanno apprezzato l’impegno militante per la promozione e la difesa dei diritti delle persone lgbt(e).

Wicky Hassan, fondatore del marchio Energie e Miss Sixty, deceduto oggi a causa di un tumore, seguiva con molta attenzione le iniziative politiche dell’Associazione Radicale Certi Diritti e non mancava di far giungere i suoi suggerimenti, le sue critiche e i suoi continui stimoli per  migliorarne l’azione da lui molto apprezzata. Lo scorso 23 novembre era stato nella sede dei Radicali, in Via di Torre Argentina, a Roma, per partecipare ad una iniziativa promossa da Certi Diritti insieme  a Nuova Proposta  con la proiezione del documentario storico sul “Fuori!” e per la presentazione del libro di Angelo Pezzana.