---
title: "1° DICEMBRE 2010 XXIII GIORNATA MONDIALE CONTRO AIDS"
date: 2010/11/30 12:24:35 +0000
draft: false
tags: [Comunicati stampa]
---

**10% DEI CONTAGI DA HIV A ROMA. INDAGINE IN 12 SCUOLE ROMANE: IL 96% NON HA MAI FATTO UN TEST. IL 49% NON USA PRESERVATIVO. IL SINDACO: PREVENZIONE CON I VALORI MORALI. DOMANI OMAGGIO DI  PRESERVATIVI AL SINDACO DI ROMA CON LA SCRITTA: ‘VALORI MORALI’**

**Comunicato Stampa delle Associazioni Radicali Certi Diritti e Radicali Roma:**

Domani, 1 dicembre si celebra la XXIII giornata mondiale contro l’Aids. Assisteremo alla liturgia dell’ipocrisia, delle parole che nulla produrranno sul piano dell’ informazione, prevenzione, delle iniziative concrete e sempre più urgenti che alimenteranno ancora di più ignoranza e mala informazione, innanzitutto a Roma la città dove avvengono in assoluto più congagi.

Secondo le ultime informazioni dell’Istituto Superiore di Sanità in Italia ci sono ogni anno oltre 3.000 nuovi contagi da HIV, il 10% di questi nella sola città di Roma. Le persone sieropositive in Italia sono circa 170.000. 1 persona su 4 non sa di essere infetta.   I casi di decessi per Aids in Italia sono poco meno di 200

L’età media dei nuovi contagiati, etero e gay è di 40 anni, il 25% di loro non sa di esserlo.

Secondo un’indagine Anlaids, appena pubblicata, su 1000 studenti tra i 16 e 19 anni, di 12 istituti romani, 1 ragazzo su 17 dice di aver contratto una malattia a trasmissione sessuale; il 96% degli studenti interpellati non ha mai fatto un test e  solo l’1% lo fa con cadenza regolare.  Nonostante il 90% sappia che non usare il preservativo, e avere più partner, sono fattori di rischio, la metà non lo usa. Secondo l’indagine, pochi conoscono il virus Hiv e l’Aids, quasi nessuno, degli studenti, sa che i rapporti orali veicolano malattie sessuali.

Il 54% degli studenti non ritiene giusta la presenza di distributori di condom nelle scuole perché ‘diseducativo’ e perché ‘incita a fare sesso’, oltre che essere ‘imbarazzante’. Ciò che sorprende è la reazione del Sindaco di Roma che, tra le altre cose, ha detto che “dobbiamo muoverci sul versante dell’informazione, dell’educazione sessuale e della prevenzione legata anche ai valori morali, perché sono i giovani a chiedercelo” (sic!).

Insomma, la metà dei giovani non usa il preservativo e visto che un’altra metà non vuole le macchinette condom nelle scuole, evidentemente per motivi causati dall’ignoranza e dalla colpevolizzazione peccaminosa, si propone di fare prevenzione con i valori morali. E allora perchè non togliere i professori di matematica se alla maggiroanza degli studenti non piacciono? Questo è il modo di affrontare l’emergenza Hiv nella città con il più alto numero di contagi insieme a Milano.

**Domani, mercoledì 1 dicembre, alle ore 12, in Campidoglio, delegazione dei radicali, guidata da Sergio Rovasio, Segretario Associazione Radicale Certi Diritti e Riccardo Magi, Segretario Radicali Roma, regalerà confezione di preservativi al Sindaco con il marchio ‘Valori Morali’**.