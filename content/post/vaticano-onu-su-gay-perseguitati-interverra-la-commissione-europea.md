---
title: 'VATICANO-ONU SU GAY  PERSEGUITATI: INTERVERRA'' LA COMMISSIONE EUROPEA'
date: Tue, 03 Feb 2009 12:56:07 +0000
draft: false
tags: [Comunicati stampa]
---

VATICANO, DEPENALIZZAZIONE OMOSESSUALITA' ALL'ONU: LA COMMISSIONE EUROPEA  AFFRONTERA' LA QUESTIONE CON I PAESI TERZI, SANTA SEDE INCLUSA.  
   
Roma, 3 febbraio 2009 In risposta ad una interrogazione dei deputati europei Marco Cappato, deputato radicale (Gruppo Alde, Giusto Catania (Gue/Ngl) e Sophia In 't Veld (Alde), alla Commissione sulla posizione assunta dal Vaticano in sede Onu contro la depenalizzazione dell'omosessualità, Stato alleato delle dittature e teocrazie che considerano reato gli atti omosessuali, la Commissione europea tramite la sua Commissaria Benita Ferrero-Waldner  ha così risposto:  
"La Commissione ritiene inaccettabile che le persone possano essere oggetto di discriminazione sulla base dell'orientamento sessuale. Per questo motivo, la Commissione non può che sostenere l'iniziativa della Presidenza, appoggiata da tutti gli Stati membri dell'Unione, sulla depenalizzazione dell'orientamento sessuale.   La Commissione è quindi pronta ad affrontare tale questione con i paesi terzi, Santa Sede inclusa, in occasione dei contatti sulla promozione e tutela dei diritti dell'uomo".

Su questa vicenda  Marco Cappato, deputato europeo e Sergio Rovasio, Segretario dell'Associaizone Radicale Certi Diritti hanno dichiarato:  
   
"Il fondamentalismo religioso e ideologico, alleato all'Onu con il Vaticano contro la depenalizzazione dell'omosessualità, ha subito oggi un forte colpo. La Commissione Europea, infatti, nell'ambito delle sue iniziative in difesa dei Diritti Umani preannuncia di attivarsi verso quei paesi che perseguitano per legge, o per anatemi, gli omosessuali. Ci auguriamo che finalmente venga alla luce l'ipocrisia delle gerarchie vaticane che da una parte dice di non voler discriminare le persone omosessuali e dall'altra si allea all'Onu con i paesi che gli omosessuali li arrestano, torturano e uccidono per legge".