---
title: 'L''Associazione Radicale Certi Diritti porta "Piccolo Uovo" in regalo a tutte le 24 biblioteche rionali di Milano'
date: Thu, 22 Mar 2012 11:50:18 +0000
draft: false
tags: [Politica]
---

Oggi l'Associazione Radicale Certi Diritti ha cominciato a portare un pacchetto regalo contenente quattro libri, tra cui "Piccolo Uovo" e "Piccola storia di una famiglia" a tutte le 24 biblioteche rionali di Milano.

22 marzo 2012

Dopo le minacce di censure e di roghi sulla pubblica piazza del libricino per bambini, intitolato "Piccolo Uovo", che mostra tra le varie forme di famiglia possibili anche quella con i genitori dello stesso sesso, **l'Associazione Radicale Certi Diritti ha lanciato una sottoscrizione pubblica online** ([https://www.facebook.com/adottapiccolouovo](https://www.facebook.com/adottapiccolouovo)) per comprare 24 copie di piccolo uovo da donare a tutte le biblioteche rionali di Milano.

L'iniziativa ha raggiunto un immediato successo raggiungendo in poco tempo 337 "mi piace" e permettendo di raccogliere **387,90 € donati da 22 cittadini, compreso il contributo di 120€ raccolto dall'associazione studentesca BESt** – Bocconi Equal Students.

Grazie al contributo raccolto e a uno sconto fatto dalla casa editrice "Lo Stampatello" è stato possibile donare alle biblioteche anche **"Piccola storia di una famiglia"**, che la Lega Nord voleva togliere dalle 5 biblioteche rionali che già lo possedevano, oltre a "Piccolo Uovo". A questi due titoli, l'Associazione Radicale Certi Diritti ha aggiunto anche **"Dal cuore delle coppie al cuore del diritto. L'udienza alla Corte Costituzionale per il diritto al matrimonio tra persone dello stesso sesso"**, a cura di Yuri Guaiana e **"Amore civile. Dal diritto della tradizione al diritto della ragione"**, a cura di Bruno de Filippis e Francesco Bilotta.

Stamattina Yuri Guaiana, Nicolò Calabro e Lorenzo Lipparini hanno iniziato il tour delle donazioni in bicicletta a partire dalla biblioteca Sicilia in via Sacco 14 che ha accolto con entusiasmo i regali. Il tour è proseguito nelle biblioteche di zona 2, 5 e 9 (vedi il video: [http://www.youtube.com/watch?v=OoWRw2rQAxE](http://www.youtube.com/watch?v=OoWRw2rQAxE)). Nei prossimi giorni i consiglieri di zona radicali porteranno alle biblioteche delle restanti zone gli stessi pacchetti completando così l'operazione "Adotta Piccolo Uovo".

[CONTRIBUISCI ALLE INIZIATIVE DI CERTI DIRITTI >](iscriviti)