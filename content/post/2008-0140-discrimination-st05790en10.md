---
title: '2008-0140-Discrimination-ST05790.EN10'
date: Fri, 05 Feb 2010 15:31:05 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 5 February 2010

Interinstitutional File:

2008/0140 (CNS)

5790/10

LIMITE

SOC 49

JAI 84

MI 31

  

  

  

  

  

OUTCOME OF PROCEEDINGS

from :

The Working Party on Social Questions

on :

22 January 2010

No. prev. doc. :

5188/10 SOC 13 JAI 16 MI 8

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

**I.         INTRODUCTION**

At its meeting on 22 January 2010, the Working Party on Social Questions continued its examination of the above proposal. The discussion focused on a set of Presidency drafting suggestions[\[1\]](#_ftn1). A written contribution from UK[\[2\]](#_ftn2) was also circulated prior to the meeting.

The Chair explained that the Presidency gave high priority to this proposal and would seek to facilitate a political agreement at the June EPSCO Council, if possible.

  

The Working Party agreed that the Spanish Presidency's drafting suggestions relating to this proposal would be distributed as public documents in the interest of transparency. The Chair also informed the Working Party that the Presidency and the Commission had been invited to a technical meeting with the European Parliament's Committee on Civil Liberties, Justice and Home Affairs[\[3\]](#_ftn3).

The Chair recalled, moreover, that following the entry into force of the Lisbon Treaty, the proposal now came under Article 19 of the Treaty on the Functioning of the European Union (unanimity in the Council, followed by the _consent_ of the European Parliament). Delegations (CZ, MT) requested guidance from the Council Legal Service (CLS) concerning the new procedure.

DE entered a general reservation on the proposal, reiterating its concerns[\[4\]](#_ftn4), including in respect of the legal basis and subsidiarity, the need for a thorough impact assessment and cost-benefit analysis, the burden that the proposed measures would impose on businesses (especially SMEs), the lack of legal certainty, and the ongoing infringement proceedings stemming from existing anti-discrimination legislation.

LT recalled its doubts regarding the need for the proposal, citing concerns over the lack of legal certainty and the need for a thorough impact assessment, and expressing the view that the proposal conflicted with the principles subsidiarity and proportionality.

MT explained that its final position would depend on solutions being found to its general concerns, including with regard to the legal, financial and practical implications of the proposal and the need to clarify the scope and the extent of the obligations.

EL maintained its reservation in respect of the inclusion of education within the scope.

NL stated that it supported the proposal provided that solutions could be found to its concerns, particularly in respect of the financial implications and the need for legal certainty.

  

BE, BG, FR, HU, AT, PT, RO, SK, SE, UK expressed their support for the ongoing work on the draft Directive, FR explaining that its main outstanding concerns related to age discrimination and the proposal's potential implications for secular education in France.

All delegations have maintained scrutiny reservations on all the new suggestions and general scrutiny reservations on the proposal. CZ, DK, FR, MT and UK have maintained parliamentary scrutiny reservations, CY and PL maintaining linguistic scrutiny reservations. The Commission has meanwhile affirmed its original proposal at this stage and has maintained a scrutiny reservation on any changes thereto. **Delegations' general positions are summed up in doc. 14008/09; for further details, see doc. 16063/09 + ADD 1.**

**II.        MAIN ITEMS DISCUSSED**

**1.           Purpose (Article 1)**

DE entered a reservation and expressed the view that adopting legislation at the EU level on all the discrimination grounds mentioned in Article 1 was not appropriate.

**2.           Principle of equal treatment (Article 2(1))**

Several delegations (FR, FI, SE, UK) called for clarification to the effect that _denial of reasonable accommodation_ constituted discrimination. FR, HU and AT suggested making Article 2(1)(c) into a separate paragraph, to conclude "…shall be **deemed** to be discrimination". Taking a similar position, RO called for Article 2(5) to be restored to the text (see doc. 14009/09).

NL reiterated its view that reasonable accommodation need not be provided if it imposed a disproportionate burden and asked whether the cross-reference to the Article 4a was enough to establish this proviso.

Cion agreed that clarification was needed in the light of the concerns raised.

  

**3.           Harassment (Article 2(3) and Recitals 12b and 12c)**

UK reiterated its view that _religion or belief_ and _sexual orientation_ should be left out of the definition of harassment. MT also had difficulty with the concept of harassment in connection with these discrimination grounds. UK could also accept the deletion of "harassment" from the text. DK repeated its earlier suggestion for such a deletion.

SK entered a scrutiny reservation on the definition of harassment (Article 2(3)).

FR called for the last sentence of Recital 12b to be deleted.

RO found Recital 12c to be redundant, as harassment could be defined nationally (see Article 2(3)).

FR, LT, MT and FI found Recital 12c difficult to interpret in practice, and FR expressed the view that it was up to the European Court of Justice (ECJ) to establish the balance between conflicting rights such as freedom of expression and freedom of religion; FR also suggested adding "freedom of information" in Recital 17. EE also saw a need for clarification and suggested that practical examples be included.

FR entered a reservation on Recital 12c. BE and FI entered scrutiny reservations.

EL reiterated its call for education to be removed from the scope of the Directive.

**4.           Harassment due to association (Article 2(3a) and Recital 12)**

PL suggested replacing "people with disabilities" with "persons with disabilities" in Recital 12.

  

CZ suggested rewording the second sentence of Recital 12 as follows: "Moreover, **\[…\]** discrimination **\[…\]** based on assumptions about a person's religion or belief, disability, age or sexual orientation **shall be deemed to be direct discrimination**." Cion suggested: "Discrimination includes direct discrimination or harassment based on assumptions about a person's religion or belief, disability, age or sexual orientation."

LV, FI and SE asked for clarification as to why _harassment due to association_ was not included in Recital 12. The Presidency concurred that it should be included.

DE, supported by EL, DK, FR and MT, was unable to accept the extension of the ban on discrimination to cover discrimination _based on assumptions_ in Recital 12. IT also expressed doubts. BG entered a scrutiny reservation.

LU preferred to see _discrimination by association_ included in Article 2(3a).

Cion explained that it was logical to include _discrimination based on assumptions_ in a recital, as it was comprised in the concept of discrimination, but not yet the subject of ECJ Case Law. By contrast, _discrimination by association_ belonged in the articles, as there was already jurisprudence on this issue.

**5.       Legitimate differences of treatment on the grounds of age (Article 2(6) and Recital 14b)**

UK felt that "In this context" was a misleading phrase (Article 2(6)), as there ought to be no requirement to _justify_ the exceptions listed in the second sub-paragraph of Article 2(6).

  

BG, DK, MT and UK raised the question as to whether "national regulations" (Article 2(6)) covered non-legislative rules such as executive orders.  Cion observed that this problem was solved by the wording used in doc. 16063/09 ADD 1 ("the Member States may lay down in the national law **aims** which can be considered to be legitimate **and means** which can be considered appropriate and necessary")[\[5\]](#_ftn5). It warned against the danger, in practical terms, of leaving age out of the Directive altogether.

DK suggested replacing "are presumed to be non-discriminatory" with "**shall be deemed** to be non-discriminatory" in Article 2(6). UK suggested "**do not constitute**". IT, MT and NL supported these suggestions.

HU preferred Recital 14a in doc. 16063/09 ADD 1 to Recital 14b.

Unaware of evidence of unjustified _discrimination against children_, IE expressed the view that the regulatory burden arising from legislation against such discrimination would not be justified. UK and NL joined their voices to this view, UK recalling its suggestion that minors be excluded from the age discrimination provisions. Cion was unable to accept any outright exclusion of minors.

The Presidency explained that discrimination against minors was a real problem, for example, if a hotel refused children as guests.

FR and NL asked for the link between Article 2(6), second sub-paragraph, and the _positive discrimination_ provisions in Article 5 to be clarified.

FR and MT maintained scrutiny reservations on Article 2(6).

  

IT entered a reservation on Recital 14b and pointed out that the wording replicated the provisions in the first sub-paragraph of Article 2(6). LU and MT also called for clarification, LU stressing the Member States' role in deciding how to handle differences of treatment on the ground of age in respect of minors. EL reiterated its reservation in respect of the inclusion of education within the scope. Cion pointed out that the second sentence of Recital 14b could be deleted, as the organisation of education was, in any case, excluded. Cion also saw a need for legal clarification in respect of "the best interest of the child", which, in any case, constituted "a legitimate aim" (see Article 2(6)).

**6.           Legitimate differences of treatment of persons with disabilities (Article 2(6a))**

CZ, LU, AT and UK suggested deleting Article 2(6a); as an alternative, UK suggested limiting this provision to indirect discrimination only. NL asked for clarification in respect of the need to take into account the health and safety _of society as a whole_.

**7.           Financial services (Article 2(7) and Recital 15)**

UK requested clarification as to whether the practices listed in its written contribution[\[6\]](#_ftn6) would be allowed to continue under the terms of the proposed Directive.  IE recalled its earlier written contribution[\[7\]](#_ftn7) addressing the same issue. Cion gave a preliminary response and undertook to study the UK note further.

BE and AT reiterated their view that differences of treatment should only be allowed on the basis of a person's state of health, not disability. BE and AT offered to submit a written suggestion. AT maintained a scrutiny reservation on Article 2(7) and Recital 15.

SK considered it legitimate to use _age_ as a determining factor when it was a real determining factor in the assessment of risk. This delegation also expressed the view that medical knowledge (without any additional statistical data) was enough to justify differences of treatment for, for example, persons over the age of 90.

  

FI suggested changing "service providers **have shown**" to "service providers **can show**" in Recital 15.

**8.           Scope Article 3(1) and Related Recitals**

**a) Deletion of "access to"**

The Presidency explained that the term "access to"[\[8\]](#_ftn8) had been deleted in Article 3(1), the thinking being that the scope should be defined in explicit detail.

CZ and UK preferred to keep "access to" in the text. BE, LU, MT, RO and UK requested legal clarification.

FR, NL, AT and RO took a flexible position on the deletion of "access to", underlining the importance of delineating the scope as clearly as possible, NL advocating the use of examples.

IT and RO pointed out the need for consistent wording, e.g. in Recitals 17 and 17b, where "access to" still appeared.

AT saw a need to include a reference to "access to" in Article 3(1)(d).

EL, FI and Cion supported the Presidency's approach, Cion pointing out that the term "access to" appeared neither in the Treaties, nor in existing anti-discrimination legislation, the expression "access to" being, not an objective, but an instrument. It recalled that such an expression had been introduced by the French presidency in order to distinguish between national and EU competences, but meanwhile that distinction had been developed extensively in Article 3(2).

**b) Scope (Article 3(1) and Recitals 17a, 17d and 17h)**

AT called for "social advantages" to be reinserted into the text (Article 3(1)(b)).

DE did not consider the scope to be clearly defined, and raised the question as to whether public works and infrastructure were included. Moreover, DE was unable to accept the inclusion of education and housing within the scope, and saw a risk of legal uncertainty in respect of the distinction to be drawn between a professional or commercial activity and transactions between individuals. MT and AT also wished to see the last-mentioned issue clarified.

Stressing the need to respect the principles of subsidiarity and proportionality, DE was unable to accept Recitals 17a and 17d.

PL saw a need to ensure consistency between Recitals 17a and 17h (family law).

**9.       Matters excluded from the scope (Article 3(2)(a)-(c) and (d) and Recital 17d)**

BE and FR raised the question as to whether Article 3(2)(a) was consistent with ECJ Case Law, FR also pointing out that "benefits" were also covered in Article 3(2)(b).

NL, RO and Cion asked for the meaning of "benefits dependent on family law" (Article 3(2)(a)) to be clarified. The Presidency pointed to Directive 2000/78/EC as a precedent. MT reaffirmed its request that reference be made to family law "**and public policy related thereto**" (Article 3(2)(a))[\[9\]](#_ftn9). Cion entered a reservation on the inclusion of the words "benefits dependent on family law".

Calling for clarification of the scope, AT asked whether survivor's pensions, for instance, were included.

  

UK reiterated its misgivings in respect of the provisions concerning education and housing.

MT also took the view that "social housing" fell under national competence. CZ and EE felt that social housing was part of social protection (Article 3(2)(e)).

NL saw a need to clarify Article 3(2) and Recitals 17a and 17b.

MT asked whether benefits funded by private parties were included in Recital 17b. Recalling its written contribution[\[10\]](#_ftn10), FI underlined the need to cover social security benefits _not_ covered by the State. FI also suggested deleting the final sentence of Recital 17b.

RO asked for the term "comprehensive protection against discrimination" in Recital 17b to be clarified.

FI was unsure as to whether Recital 17d was needed. RO took the view that the reference to "housing" in the second sub-paragraph of Recital 17d was redundant, as housing was already covered by the first sub-paragraph. UK suggested replacing "In particular" with "For example" at the beginning of Recital 17d.

FR entered a scrutiny reservation on Article 3(2).

**10.         Education (Article 3(2)(d), Article 3(3) and Recital 17g)**

IE and EL wished to see education removed from the scope of the Directive. EL was unable to accept the last sentence of Recital 17g, as the Member States had exclusive competence for organisation of their educational systems. MT and UK also expressed misgivings in respect of the provisions concerning education.

  

IT and Cion preferred the version of Article 3(3) as contained in doc. 16063/09 ADD 1, Cion pointing out that the earlier wording left the issue of the admission policies of faith-based schools to the Member States' discretion.

Responding to BG and HU, the Chair suggested replacing "ethos-based" with "faith-based".

FI asked for the meaning of the phrase "rights and freedoms of others" to be clarified.

FR entered a reservation on Article 3(3).

**11.         Other issues**

DK, EE, NL and RO suggested streamlining and condensing the recitals (e.g. Recitals 12b and 12c and Recitals 17a and 17b).

**III.      CONCLUSION**

The Presidency took note of the issues raised during the course of the discussion.

The Chair concluded that the CLS would be invited to comment on the legal issues raised during the course of the meeting. She invited the delegations to submit suggestions in writing.

The Working Party will continue its work on the proposal, with a view to addressing the numerous outstanding issues. The next meeting is provisionally scheduled for 18 February 2010.

\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) Doc. 5188/10.

[\[2\]](#_ftnref2) Doc. 5568/10.

[\[3\]](#_ftnref3) The new EP Rapporteur is Raúl Romeva I Rueda (ES, Group of the Greens / European Free Alliance).

[\[4\]](#_ftnref4) See doc. DS 823/08.

[\[5\]](#_ftnref5) See doc. 16063/09 ADD 1.

[\[6\]](#_ftnref6) See doc. 5568/10.

[\[7\]](#_ftnref7) See doc. 13046/09.

[\[8\]](#_ftnref8) See Opinion of the Council Legal Service in doc. 14896/08, p. 29.

[\[9\]](#_ftnref9) See doc. 14786/09.

[\[10\]](#_ftnref10) See doc. 13496/09.