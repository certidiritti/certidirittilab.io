---
title: '4 giovani gay rischiano la morte in Iran. Firma la petizione per salvarli >'
date: Mon, 14 May 2012 10:14:34 +0000
draft: false
tags: [Medio oriente]
---

La Corte Suprema della Repubblica Islamica dell'Iran ha confermato la condanna a morte di quattro giovani gay.

La Corte Suprema della Repubblica Islamica dell'Iran ha confermato la condanna a morte di quattro giovani gay: Vahid Akbari, Sahadat Arefi, Javid Akbari e Hushmand Akbari. La sharia identifica gli omosessuali come "Nemici di Allah" e prevede che essi siano assassinati.

Il Gruppo EveryOne chiede all'Alto Commissario delle Nazioni Unite sui Diritti Umani, al Commissario dell'Unione europea per i Diritti Umani, all'Organizzazione per la Cooperazione Islamica, alla Commissione Islamica per i Diritti Umani e all'intera società civile di sostenere il nostro appello per la difesa della vita dei quattro giovani omosessuali e di tutti coloro che soffrono ogni sorta di persecuzione.

Fonte: www.repubblica.it

**[FIRMA LA PETIZIONE >>>](http://www.gopetition.com/petitions/urgent-stop-the-hanging-of-four-men-for-sodomy-in-i.html)**