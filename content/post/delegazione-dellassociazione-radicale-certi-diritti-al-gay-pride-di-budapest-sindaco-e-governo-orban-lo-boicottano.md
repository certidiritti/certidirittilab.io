---
title: 'Delegazione dell''Associazione Radicale Certi Diritti al gay pride di Budapest, sindaco e governo Orban lo boicottano'
date: Sat, 07 Jul 2012 19:08:08 +0000
draft: false
tags: [Europa]
---

Roma, 7 luglio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

L’Associazione Radicale Certi Diritti partecipa oggi al Gay Pride di Budapest in Ungheria, con una delegazione composta da Ottavio Marzocchi, responsabile delle questioni europee dell’associazione, funzionario del gruppo liberale al Parlamento Europeo, e Ximo Nogueroles, membro dell’Associaizone.  Partecipano al Gay Pride anche l’ Eurodeputata Sophie In’t Veld, olandese liberale, nel quadro del programma ALDE 4 Equality”, ed Ulrike Lunacek, eurodeputata Verde austriaca.

La situazione dei diritti LGBTI in Ungheria peggiora sempre di più da quando il governo di Viktor Orban ha imposto un’agenda politica conservatrice che ha portato all’adozione di una legge sulla protezione della famiglia” che esclude di proposito le persone lesbiche e gay. La Corte Costituzionale ne ha solo in parte cancellato gli effetti, ma il governo intende reintrodurre il contenuto bocciato in una legge costituzionale omnibus sulla quale la Corte Costituzionale non potrà intervenire.

Il gay pride di Budapest era stato inizialmente proibito - come già accaduto negli anni precedenti - e poi autorizzato grazie alle sentenze giudiziarie. Il Sindaco, Istvan Tarlos, ha risposto nei giorni scorsi ad una lettera di un gruppo di Deputati Europei che esprimevano preoccupazione in merito alla sicurezza del gay pride dicendo che “nel corso dei 20 anni scorsi ho accumulato una moltitudine di gravi problemi a Budapest, non credo che sia una misura di intelletto dedicarmi alla questione che mi sottoponete (la sicurezza del gay pride)”.  La polizia si è impegnata, secondo gli organizzatori, a proteggere i partecipanti dalle bande di neo-nazisti, fondamentalisti cattolici e i nazionalisti che attaccano ogni anno la manifestazione. Ciò che più sconcerta è che la polizia ha chiesto ai manifestanti di vestirsi in modo appropriato per non dare scandalo e non essere riconosciuti.

La delegazione si augura che il Gay Pride si svolga serenamente e si appella alle autorità  ungheresi perché appoggino la lotta per i diritti LGBTI e alla lotta all’omofobia, garantendo la sicurezza e la protezione dei partecipanti alla manifestazione.