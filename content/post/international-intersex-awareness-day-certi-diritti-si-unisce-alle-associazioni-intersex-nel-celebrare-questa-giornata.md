---
title: 'International Intersex Awareness Day: Certi Diritti si unisce alle associazioni intersex nel celebrare questa giornata'
date: Fri, 26 Oct 2012 07:32:36 +0000
draft: false
tags: [Intersex]
---

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Roma, 26 ottobre 2012

L’Associazione Radicale Certi Diritti coglie occasione della giornata internazionale della consapevolezza intersessuale per cercare di accrescere la consapevolezza e la comprensione intorno alle tematiche legate all’intersessualità, chiedere al Ministro della Salute di rivedere le pratiche alle quali le persone intersessuali sono sottoposte e chiedere al Governo italiano di prendere le necessarie misure per garantire il pieno rispetto dei diritti umani delle persone intersessuali, come richiesto anche dal Rapporto 2012 della Commissione Europea sulla “Discriminazione basata sul sesso, l’identità di genere e l’espressione di genere”.

Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti, dichiara: “La base delle discriminazioni sofferte dalle persone intersessuali sta nel fatto che la nostra società è intrappolata in un binarismo sessuale che nega l’esistenza delle persone intersessuali. All’interno di questa cornice culturale e giuridica i corpi intersessuali sono ‘normalizzati’ attraverso interventi medici e di chirurgia plastica volti a riallinearli ai sessi riconosciuti. I bambini intersessuali sono sottoposti di routine a questi intrusivi e assolutamente non necessari interventi di ‘normalizzazione’, senza che venga espresso un preventivo consenso personale e informato, esponendoli conseguenze negative di lungo periodo sul loro benessere fisico, psicologico e sessuale. È ora che si cominci a parlare seriamente di tutto questo e che si ponga al più presto fine a questa patente violazione del diritto all’integrità corporea di ciascun individuo”.