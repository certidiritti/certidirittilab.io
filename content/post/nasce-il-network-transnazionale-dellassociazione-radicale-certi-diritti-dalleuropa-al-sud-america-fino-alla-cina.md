---
title: 'Nasce il network transnazionale dell''associazione radicale Certi Diritti, dall''Europa al Sud-America fino alla Cina'
date: Sun, 08 Jan 2012 23:23:12 +0000
draft: false
tags: [Transnazionale]
---

Dall'Africa al Sud-America, dall'Europa alla Cina: un coordinamento di militanti e sostenitori dell'associazione per la promozione e la difesa dei diritti civili

Roma, 7 gennaio 2011

Comunicato  Stampa dell’Associazione Radicale Certi Diritti

L’Associazione Radicale Certi Diritti dall’8 dicembre scorso è diventata uno dei soggetti politici costituenti del Partito Radicale Nonviolento, transnazionale e transpartito. In questa settimane uno dei primi obiettivi è stato quello di realizzare un network  transnazionale composto da esperti, militanti, attivisti e sostenitori che da diversi paesi del mondo potranno fornire informazioni, idee e promuovere azioni politiche sui temi legati alla liberazione sessuale delle persone, per la promozione e la difesa dei diritti civili, anche in tema lgbti(e),  contro tutti i tipi di tabù e persecuzioni ispirate dal fondamentalismo ideologico e religioso.

La rete inter-transnazionale nata in questi giorni vedrà la collaborazione di:

Francis Onyango, Presidente onorario di Certi diritti, Avvocato, Kampala ;

Stefano Fabeni Direttore della Global Initiative for Sexuality and Human Rights Heartland Alliance for Human Needs & Human Rights di Washington ;

Davide Miraglia, militante e attivista di Berlino;

Wezeshatz, Associazione LGBTI della Tanzania;

Maria Gigliola Toniollo, Cgil Nuovi Diritti;

Jack Rock, cittadino canadese che vive in Tanzania;

Ottavio Marzocchi e Joaquim Nogueroles, funzionari dell’Unione Europa – Bruxelles;

Sharon Nizza, studentessa – Gerusalemme;

Roni Guetta, già Tesoriere di Certi Diritti – Londra;

Ugo Millu, Avvocato – Barcellona;

Vittorio Scardamaglia e Sara Garbagnoli – Parigi;

Eugert Mattia Aliaj, membro dell'Aleanca Kunder Diskriminimit Lgbt - Albania; 

Da Pechino due persone di cui non possiamo diffondere il nome.

L'Associazione Radicale Certi Diritti è anche membro di Ilga-Europe e collabora con diverse organizzazioni internazionali sul tema dei diritti delle Sex-Workers.  
  

Tutto il nostro lavoro è volontario e solo dal canale dell’autofinanziamento possiamo avere le risorse economiche e l'aiuto per promuovere ulteriori iniziative.**  
Iscriviti o contribuisci per il 2012  
[http://www.certidiritti.it/iscriviti](iscriviti)**