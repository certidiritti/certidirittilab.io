---
title: 'Proposta di legge n. 2939 del 1/7/2002'
date: Mon, 21 Jun 2010 05:58:32 +0000
draft: false
tags: [Senza categoria]
---

CAMERA DEI DEPUTATI - Proposta di legge d'iniziativa dei deputati:

TITTI DE SIMONE, DEIANA, AMICI, BELLILLO, BULGARELLI, CHIAROMONTE, CIALENTE, ALFONSO GIANNI, GRIGNAFFINI, GRILLINI, LEONI, SANTINO ADAMO LODDO, MANTOVANI, MASCIA, PISAPIA, RIVOLTA, RUSSO SPENA, RUZZANTE, SODA, TRUPIA, VALPIANA, VENDOLA, ZANELLA

**Norme in materia di adeguamento del nome all'identità psico-fisica della persona**

_**Art. 1**_

1.  La persona che vuole cambiare il nome in quanto, in ragione della propria identità di genere e della difformità tra aspetto esteriore e nome anagrafico, sente di non appartenere al sesso indicato nel suo atto di nascita presenta apposita domanda al sindaco del luogo di residenza.
    
2.  Può presentare la domanda di cambiamento del nome il cittadino italiano che ha compiuto i diciotto anni di età e che ha intrapreso il percorso di adeguamento dell'identità fisica all'identità psichica.
    
3.  La domanda deve indicare il nome o i nomi che la persona richiedente ha scelto.
    

**_Art. 2_**

1.  Il sindaco di cui all'articolo 1, comma 1, decide sulla domanda di cambiamento del nome entro un mese dalla presentazione della stessa, sentiti i pareri della persona richiedente e del medico specialista o dello psicologo che la ha in cura, il quale, sulla base della formazione e dell'esperienza professionali acquisite, sia esperto nelle problematiche relative al transessualismo ed alle identità di genere.
    
2.  Entro il termine previsto dal comma 1, il provvedimento del sindaco è notificato, tramite messo comunale, al giudice tutelare nella cui circoscrizione rientra il comune interessato. Il mancato accoglimento della domanda è motivato per iscritto.
    
3.  Il giudice tutelare, entro i cinque giorni successivi dal ricevimento della notificazione di cui al comma 2, assunte le informazioni ed operato il controllo di conformità circa i requisiti legali dell'istanza, provvede, con decreto motivato, a convalidare o a non convalidare il provvedimento del sindaco. Il decreto è notificato, nei cinque giorni successivi, al sindaco ed alla stessa persona richiedente.
    
4.  Contro il provvedimento deliberato dal giudice tutelare la persona richiedente può proporre ricorso al tribunale competente per territorio, entro un mese dal ricevimento della notificazione. La decisione del ricorso è assunta in camera di consiglio.
    

**_Art. 3_**

1.  A tutela della riservatezza della persona richiedente, è esclusa l'affissione del decreto del giudice tutelare emesso ai sensi dell'articolo 2, comma 3, nell'albo pretorio del comune presso il quale è stata presentata la domanda di cambiamento del nome.
    

**_Art. 4_**

1.  Il decreto che dispone il cambiamento del nome è annotato negli atti dello stato civile della persona richiedente.
    
2.  L'ufficiale dello stato civile del luogo di residenza, se la nascita o il matrimonio è avvenuto in altro comune, dà prontamente avviso del cambiamento all'ufficiale dello stato civile del luogo di nascita o di matrimonio, che provvede ad analoga annotazione nell'atto di nascita o di matrimonio della persona richiedente.
    

**_Art. 5_**

1.  Ferme restando le disposizioni della legge 31 dicembre 1996, n. 675, e successive modificazioni, l'uso personale o da parte di terzi del nome precedente è perseguibile ai sensi degli articoli 494, 495 e 496 del codice penale, fatta salva la sussistenza di ulteriori fattispecie delittuose.
    

**_Art. 6_**

1.  Al procedimento per il cambiamento del nome previsto dalla presente legge, si applicano le disposizioni stabilite dall'articolo 93 del regolamento di cui al decreto del Presidente della Repubblica 3 novembre 2000, n. 396.
    

[su](http://www.t-girl.it/1-dis/1.6-leg/1.6.1-legge_164-82.htm#top)