---
title: 'Parlamentari Radicali depositano interrogazione urgente per ritiro Circolare Amato che alimenta discriminazioni'
date: Wed, 11 Jul 2012 12:23:09 +0000
draft: false
tags: [Politica]
---

Roma, 11 luglio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

I Parlamentari Radicali Marco Perduca al Senato e Rita Bernardini alla Camera dei deputati, hanno depositato una interrogazione urgente al Ministro dell’Interno e al Ministro per i Rapporti con il Parlamento chiedendo chiarimenti su quanto denunciato dall’Associazione Radicale Certi Diritti riguardo gli effetti prodotti dalla Circolare Amato del 2007 n. 55 che vieta ai Comuni la trascrizione dei matrimoni tra persone dello stesso sesso contratti all’estero anche da cittadini italiani.

L’Associazione Radicale Certi Diritti aveva inviato nei mesi scorsi una lettera firmata anche da Arcigay, Famiglie Arcobaleno, Arcilesbica, Agedo, Rita Bernardini deputata Radicale e Paola Concia, deputata del Pd, ai membri del Governo, in particolare al Ministro dell’ Interno, chiedendo il ritiro della Circolare Amato che, per ragioni di ‘ordine pubblico’, materia questa non applicabile al diritto di famiglia come ribadito ripetutamente dalla Corte di Cassazione, di fatto è una vera e propria discriminazione di Stato nei confronti delle persone lesbiche e gay.

Nell’interrogazione parlamentare sono stati richiamate le norme europee e italiane che vietano all’Italia questa forma di discriminazione: dalla Direttiva europea 2004/38/CE (articoli 3,9,10 e 33) alle  Risoluzioni e Mozioni approvate in questi anni dal Parlamento Europeo, le sentenza della Corte Costituzionale 138/2010, della Corte di Cassazione 1328/2011, l’ordinanza del Tribunale civile di Reggio Emilia 1401/2011 e l’articolo 43 del Decreto Legislativo n. 286/1998 (testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero).

Nell’interrogazione i Parlamentari Radicali chiedono ai due Ministri di chiarire quali siano le loro valutazioni e iniziative sugli effetti prodotti  dalla Circolare Amato che sono in  palese e netto contrasto  con le diverse norme europee e italiane e se non ritengano di doverla al più presto ritirare.

**Qui di seguito il testo integrale dell’interrogazione presentata da Marco Perduca al Senato e da Rita Bernardini alla Camera dei deputati:**

**Al Ministro dell’Interno**

**Al Ministro per i Rapporti con il Parlamento**

**Interrogazione**** urgente a risposta scritta**

**Per sapere – premesso che:**

Nell’Ottobre 2007 l’allora Ministro dell'Interno Giuliano Amato ha diffuso la Circolare n. 55 avente per  oggetto «Matrimoni contratti all’estero tra persone dello stesso sesso. Estratti plurilingue di atti dello stato civile» (Protocollo n. 15100/397/0009861); la Circolare è stata inviata in tutti i Comuni italiani dando precise disposizioni sulla non trascrizione dei matrimoni effettuati all'estero dalle coppie dello stesso sesso;

la Circolare ministeriale è in netto contrasto con il Trattato di Nizza sulla libera circolazione, con il Trattato di Lisbona sulla lotta ad ogni forma di discriminazione e la Direttiva europea 2004/38/CE, in particolare gli articoli 3, 9, 10 e 33;

l’articolo 2 del D.l. n. 30 del 6 febbraio 2007, che recepisce la Direttiva europea 2004/38/CE, cita in modo chiaro il fatto che i “familiari” possono ricongiungersi e vengono definiti come tali anche i “coniugi”, ed è fuor di dubbio che il termine si riferisca alla figura del coniuge così come essa è configurata nel paese in cui il matrimonio tra persone dello stesso sesso è celebrato e che in base alla Direttiva stessa deve essere riconosciuto come tale; tale interpretazione è stata sostenuta anche dalla Corte di Cassazione (Cass. Pen. Sez. I Sent n. 1328 del 19/1/2011), e, più recentemente, dal Tribunale civile di Reggio Emilia con Ordinanza 1401/2011 depositata il 13/2/2012;

la CircolareAmato del 2007 dal punto di vista del diritto antidiscriminatorio vìola l’articolo 43 del Decreto Legislativo n. 286/1998 (testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero);

diverse sentenze della Corte di Cassazione, in particolare la più recente 4184/2012, hanno esplicitamente escluso che i matrimoni tra persone dello stesso sesso possano dirsi contrari all'ordine pubblico, tale indicazione si ricava anche dalla dalla sentenza 138 della Corte Costituzionale, peraltro la CircolareAmato 2007/55 motiva per ragioni di ordine pubblico l''indicazione riguardo la non trascrivibilità dei matrimoni contratti all'estero.

il Parlamento Europeo ha recentemente approvato il Rapporto  Lechner sulla "giurisdizione, la legge, il riconoscimento e l'applicazione delle decisioni e degli strumenti relativi alla successione e la creazione di un Certificato Europeo di Successione” e il Rapporto della deputata Sophie In't Veld sull'eguaglianza, che chiede alla Commissione ed agli Stati membri di "elaborare proposte per il mutuo riconoscimento delle unioni civili e delle famiglie dello stesso sesso";

lo scorso 14 aprile l’Associazione Radicale Certi Diritti, che da anni si batte per il ritiro della CircolareAmato, ha mandato una lettera ai membri del Governo, co-firmata oltre che dalla sottoscritta anche da Anna Paola Concia, deputata del Pd, Yuri Guaiana, Segretario Associazione Radicale Certi Diritti, Paolo Patanè, presidente nazionale Arcigay, Paola Brandolini, presidente Arcilesbica, Luca Possenti, vice presidente Famiglie Arcobaleno e Rita De Santis, presidente AGEDO, nella quale veniva chiesto ai destinatari di attivarsi per ritirare la CircolareAmato 2007/n.55;

in risposta a quella lettera il Ministro per i Rapporti con il Parlamento con lettera del 15 maggio 2012 (prot. 62-2012) scrisse che aveva ritenuto “di inoltrare la nota di trasmissione al Ministro dell’Interno, competente per valutare la richiesta in merito alla CircolareAmato n.55 del 18 ottobre 2007…”;

recentemente il Ministro degli Interni ha fatto sapere ai rappresentanti della Associazione Radicale Certi Diritti, che la sollecitavano a dare una risposta riguardo la richiesta di ritiro della CircolareAmato, che della questione ha investito il Ministro per i Rapporti con il Parlamento;

**Per sapere:**

quali siano le valutazioni dei Ministri interrogati riguardo le questioni poste in premessa con particolare riguardo agli effetti prodotti dalla CircolareAmato 2007 n. 55 rispetto alle leggi citate, alla Direttiva europea citata, i documenti votati dal Parlamento Europeo, così come per  le diverse sentenze del Tribunale di Reggio Emilia e della Corte di Cassazione;

se il Ministro dell’Interno non ritenga urgente ritirare la CircolareAmato2007 n. 55 viste le evidenti diverse forme di discriminazione che determina;

se effettivamente la questione la sta seguendo il Ministro per i Rapporti con il Parlamento o se la competenza è del Ministro dell'Interno;

se non ritengano che tale CircolareAmato sia in netto contrasto con la lotta alle discriminazioni nei confronti delle persone lesbiche e gay e in contrasto con le norme che prevedono la trascrizione dei matrimoni contratti all'estero da cittadini italiani nelle anagrafi dei Comuni.

Sen. Marco Perduca

On. Rita Bernardini