---
title: 'Appello di giuristi e docenti universitari: il Parlamento è libero di decidere sul matrimonio egualitario. Nessun ostacolo da Costituzione e Corte costituzionale'
date: Thu, 20 Sep 2012 08:42:43 +0000
draft: false
tags: [Matrimonio egualitario]
---

Comunicato stampa dell’Associazione Radicale Certi Diritti

Roma, 20 settembre 2012

Da tempo circolano interpretazioni infondate delle sentenze della Corte costituzionale e della stessa Carta, volte a sostenere l’ipotesi che in Italia ci sia un blocco a livello costituzionale nell’estendere l’istituto matrimoniale anche alle coppie formate da persone dello stesso sesso.

Questa interpretazione è infondata: né la Costituzione né le sentenze dell’Alta Corte contengono limitazioni al pieno accesso all’istituto del matrimonio civile da parte delle coppie formate da persone dello stesso sesso. Al contrario la sentenza n. 183 del 2010 è molto esplicita nel sollecitare il Parlamento italiano a legiferare sulla materia.

Questo è il contenuto dell’appello promosso dall’Associazione Radicale Certi Diritti e redatto dalla professoressa Marilisa D’Amico, dell’Università di Milano. L’appello è già stato sottoscritto da 18 docenti universitari.

**[IL TESTO DELL'APPELLO e L'ELENCO DEI FIRMATARI >>>](http://www.certidiritti.org/2012/09/20/matrimonio-per-le-coppie-dello-stesso-sesso-il-parlamento-e-libero-di-decidere/)**  
  
**I giuristi e i docenti universitari che vogliono aderire all'appello**  
**possono farlo scrivendo a info@certidiritti.it**