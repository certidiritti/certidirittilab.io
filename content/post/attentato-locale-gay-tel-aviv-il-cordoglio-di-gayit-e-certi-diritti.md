---
title: 'ATTENTATO LOCALE GAY TEL AVIV: IL CORDOGLIO DI GAY.IT E CERTI DIRITTI'
date: Sun, 02 Aug 2009 11:40:11 +0000
draft: false
tags: [Comunicati stampa]
---

**ATTACCO LOCALE GAY DI TEL AVIV: LETTERA DI CORDOGLIO DI ALESSIO DE GIORGI, DIRETTORE DI GAY.IT E SERGIO ROVASIO, SEGRETARIO DI CERTI DIRITTI AGLI ESPONENTI DELLA COMUNITA’ GAY DI TEL AVIV INCONTRATI LO SCORSO 12 GIUGNO .**

Appresa la notizia della sparatoria nel locale della principale associazione gay di Tel Aviv, Agudah, Alessio de Giorgi, Direttore di Gay.it e Sergio Rovasio, Segretario dell’Associazione Radicale Certi Diritti, hanno scritto una lettera di cordoglio al Presidente di Agudah, Mike Amel, la principale associazione lgbt israeliana, al fondatore della casa gay della città Ethan Pinkas e al deputato gay della Knesset, Nitzan Horowitz, incontrati lo scorso 24 giugno durante il gay pride israeliano.

Roma, 2 agosto 2009

**“Cari Mike, Ethan e Nitzan,**

**

quanto accaduto ieri notte nella sede di Agudah a Tel Aviv ci ha sconvolti per la brutalità dell’azione, avvenuta contro persone deboli e indifese che lì si erano ritrovate per stare in un luogo sicuro e protetto. Vi esprimiamo la nostra forte vicinanza per questa grande tragedia certamente alimentata dall’odio e dall’omofobia.

Questa violenza inaudita rischia di rimettere in discussione la sicurezza e la serenità che con forza e determinazione siete riusciti a raggiungere in questi anni nella città di Tel Aviv, tra le più avanzate riguardo il riconoscimento dei diritti per le persone lgbt israeliane e palestinesi.

Lo scorso 12 giugno, durante gli eventi organizzati per il gay pride cittadino, al quale abbiamo partecipato con grande entusiasmo, siamo rimasti impressionati dai vostri successi, purtroppo lontani anni luce da quelli italiani. Questa tragedia causerà ancora più timori e paure da parte della società verso la comunità lgbt, paure sempre più alimentate anche dall’odio del fondamentalismo religioso e ideologico che insieme dobbiamo combattere, ovunque.

Vi abbracciamo con affetto e vi preghiamo di esprimere il nostro più sentito cordoglio ai familiari delle vittime,

Alessio De Giorgi, Direttore di Gay.it  
Sergio Rovasio, Segretario Associazione Radicale Certi Diritti

**