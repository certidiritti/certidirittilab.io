---
title: 'TRANS E GAY NON ANDRANNO IN PARADISO: INVECE IL CARDINALE BARRAGAN SI!'
date: Wed, 02 Dec 2009 13:58:56 +0000
draft: false
tags: [Comunicati stampa]
---

**I GAY NON ANDRANNO IN PARADISO: EFFETTIVAMENTE OGGI C’ERA PROPRIO BISOGNO DELLE PAROLE DEL CARDINALE BARRAGAN. NON BASTAVANO LE VIOLENZE, I SOPRUSI CONTRO GAY E TRANS. C’ERA PROPRIO BISOGNO DEL SUO MESSAGGIO DI DIALOGO….**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

“Il Cardinale Barragan ha lanciato i suoi fulmini contro transessuali e gay, che non andranno in paradiso. Effettivamente, dopo le botte, le violenze, gli assalti verso le persone gay c’era proprio bisogno dei suoi strali, delle sue condanne, dei suoi fulmini. I suoi messaggi di dialogo, di attenzione verso il prossimo, verso le persone deboli invece a lui lo faranno andare in paradiso. Beato lui, che invidia…noi fanfaroni, lui santo, subito eh, per carità. E poi quella sua frase infarcita di ipocrisia: 'L'omossessualita' e' un peccato ma questo non giustifica alcuna forma di discriminazione...'. Meno male".

**Qui di seguito il brano di San Paolo che fa tanto agitare il Cardinale Barragan:**

“26Per questo Dio li ha abbandonati a passioni infami; le loro donne hanno cambiato i rapporti naturali in rapporti contro natura. 27Egualmente anche gli uomini, lasciando il rapporto naturale con la donna, si sono accesi di passione gli uni per gli altri, commettendo atti ignominiosi uomini con uomini, ricevendo così in se stessi la punizione che s’addiceva al loro traviamento. 28E poiché hanno disprezzato la conoscenza di Dio, Dio li ha abbandonati in balìa d’una intelligenza depravata, sicché commettono ciò che è indegno, 29colmi come sono di ogni sorta di ingiustizia, di malvagità, di cupidigia, di malizia; pieni d’invidia, di omicidio, di rivalità, di frodi, di malignità; diffamatori,30maldicenti, nemici di Dio, oltraggiosi, superbi, fanfaroni, ingegnosi nel male, ribelli ai genitori, 31insensati, sleali, senza cuore, senza misericordia. 32E pur conoscendo il giudizio di Dio, che cioè gli autori di tali cose meritano la morte, non solo continuano a farle, ma anche approvano chi le fa”.