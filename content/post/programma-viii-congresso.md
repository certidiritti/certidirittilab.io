---
title: 'Programma Provvisorio VIII Congresso'
date: Thu, 04 Sep 2014 17:34:17 +0000
draft: false
---

**Venerdì 21 novembre 2014**

**ore 16:00 Grand Hotel Lamezia**

sessione speciale: _**"Esperienze concrete di promozione dei diritti umani delle persone lgbti in Italia nell’amministrazione pubblica e nelle aziende private”**_

Umberto **Costamagna**, Presidente _Call &Call_ Fabio **Galluccio**, responsabile _diversity management Telecom Italia_ Bruno **Talarico**, segretario generale della FP (Funzione pubblica) della Cgil Catanzaro-Lamezia Gianni **Speranza**, Sindaco della città di Lamezia Terme Gianluca **Cuda**, Sindaco del Comune di Pianopoli modera: Marco **Marchese**, direttivo _Associazione Radicale Certi Diritti_

\*\*\*

**Sabato 22 novembre 2014**

**Ore 9.00** **Inizio lavori congressuali**

Approvazione della Presidenza, dell’Ordine dei lavori e del Regolamento

Intervento di Ottavia **Voza**, attivista per i diritti delle persone trans

Presentazione delle Relazioni:

**Relazione del Segretario** **Relazione del Tesoriere** **Relazione su affermazione civile**

Alessandro **Comeni** \- attivista intersex

Saluto del Sindaco di Lamezia Terme Interventi delle Associazioni locali e nazionali

**Dibattito generale**

**Ore 13.30 – 14.30 pausa lavori**

**Dibattito generale** nel corso del quale interverranno

Sergio **Lo Giudice**, Senatore Pd (_I diritti delle persone LGBTI nel Parlamento Italiano_) Rita **Bernardini**, Segretaria di _Radicali Italiani_ Marco **Pannella**, Filomena **Gallo**, Segretaria _Ass. Luca Coscioni_ (_Legge 40 e la fecondazione eterologa e gestazione per altri_) Massimo **Modesti** (_Le seconde generazioni LGBT_)

18:00 **Presentazione del documentario sulla storia del movimento LGBTI calabrese**

\*\*\*

**Domenica 23 novembre 2014**

**Ore 9.00** lavori congressuali come da ordine dei lavori

**Ore 10.00** Volker **Beck**, membro verde del _Bundestag_ per Cologna (video) **Votazione documenti congressuali e degli organi associativi** **A seguire votazione responsabile gruppo locale Calabrese**

Hanno annunciato la loro presenza:

Giuseppina **Ladelfa**, _Famiglie Arcobaleno_ Riccardo **Magi**, Consigliere comunale di Roma Capitale e Presidente di _Radicali Italiani_ Aurelio **Mancuso**, _Equality Italia_