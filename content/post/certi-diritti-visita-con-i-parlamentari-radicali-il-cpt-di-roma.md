---
title: 'CERTI DIRITTI VISITA CON I PARLAMENTARI RADICALI IL CPT DI ROMA'
date: Sun, 01 Jun 2008 13:39:01 +0000
draft: false
tags: [Comunicati stampa]
---

Lunedì 2 giugno, alle ore 11.30, una delegazione di parlamentari, composta da Rita Bernardini, deputata radicale – Pd, Segretaria di Radicali Italiani, Elisabetta Zamparutti, deputata radicale, Tesoriera di Radicali Italiani ed Elettra Deiana, dirigente di Rifondazione Comunista si recheranno al Centro di Permanenza Temporanea di Ponte Galeria a Roma per verificare le condizioni dei cittadini stranieri trattenuti.

La delegazione radicale sarà accompagnata dai radicali Valentina Ascione, Alessandro Gerardi, Avvocato e Sergio Rovasio, Segretario Associazione Radicale Certi Diritti.

La delegazione radicale sarà accompagnata dai radicali Valentina Ascione, Alessandro Gerardi, Avvocato e Sergio Rovasio, Segretario Associazione Radicale Certi Diritti.**A conclusione della visita, alle ore 13 circa, si svolgerà davanti all’ingresso del CPT (Via Portuense, 1680 – Km. 10.400 – Roma) una Conferenza Stampa.**

Per info: Valentina Ascione 339 52 64 184
=========================================