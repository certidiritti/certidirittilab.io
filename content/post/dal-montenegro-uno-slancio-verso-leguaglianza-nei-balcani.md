---
title: 'Dal Montenegro uno slancio verso l''eguaglianza nei Balcani'
date: Thu, 22 Mar 2012 23:38:00 +0000
draft: false
tags: [Europa]
---

Conclusa la conferenza europea 'Insieme contro le discriminazioni'. Dai Balcani avviato un importante percorso politico in vista della storica conferenza del Consiglio d'Europa promossa dal Regno Unito.

Roma, 23 marzo 2012

comunicato stampa dell'associazione radicale Certi Diritti

A Budva, Montenegro, si è svolta lunedì 19 marzo 2012 una storica conferenza che segna una tappa importante nel percorso verso una sempre maggiore eguaglianza e tutela dei diritti nei Balcani.

Il primo ministro della nazione ospitante, Igor Lukšić, ha invitato rappresentanti degli Stati europei e in particolare dei Balcani ed esperti internazionali per parlare di diritti, orientamento sessuale e identità di genere alla conferenza Insieme contro la discriminazione. Obiettivo e filo rosso dell'evento è stato favorire un percorso soprattutto politico di contrasto alla discriminazione contro lesbiche, gay e persone trans nella regione. In diversi Stati, infatti, si ripetono attacchi e violenze contro la comunità LGBT, soprattutto in occasione di manifestazioni pubbliche da loro promosse quali i pride.

Oltre a ministri del governo montenegrino hanno preso la parola rappresentanti governativi di Bosnia, Serbia, Macedonia, Kosova e Croazia. In particolare la vicepremier di quest'ultimo Stato, nonché ministro per il welfare e la gioventù Milanka Opačić ha sottolineato l'impegno nel contrasto alla discriminazione proprio per tutelare i giovani. Inoltre, ha annunciato che il governo croato sta lavorando ad una proposta di legge per introdurre un'unione civile per persone dello stesso genere.

Hanno partecipato ai lavori anche rappresentanti dei governi di Austria, Germania, Regno unito e Stati uniti. Assente l'Italia, la cui esperienza è stata tuttavia rapidamente ricordata nell'intervento di Alexander Schuster. Questi ha sottolineato che le riforme legislative non sono in sé sufficienti e che occorre contrastare anche l'omofobia istituzionalizzata, ad esempio attraverso campagne che visivamente associno lo Stato alla lotta contro l'esclusione e la discriminazione di persone LGBT.

La conferenza Insieme contro la discriminazione è stata fortemente voluta quale passo propedeutico verso altra storica conferenza, ovvero quella di più giorni che si terrà la prossima settimana al Consiglio d'Europa. Voluta dalla presidenza britannica per fare il punto sulla Raccomandazione CM/Rec(2010)5 sulle misure per combattere la discriminazione per motivi di orientamento sessuale o identità di genere, mira anche a promuoverne l'implementazione da parte degli Stati.

Tutti gli interventi alla conferenza di Budva sono disponibili online sul sito del Governo al seguente indirizzo:  
[http://www.gov.me/en/Together\_against\_Discrimination/video/](http://www.gov.me/en/Together_against_Discrimination/video/)