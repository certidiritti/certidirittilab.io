---
title: 'Parte su Radio Radicale la trasmissione di approfondimento sull''iter delle Unioni Civili a cura di Certi Diritti'
date: Tue, 20 Jan 2015 16:32:37 +0000
draft: false
tags: [Diritto di Famiglia]
---

[![francesco-nitto-palma-pdl](http://www.certidiritti.org/wp-content/uploads/2015/01/francesco-nitto-palma-pdl-300x199.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/01/francesco-nitto-palma-pdl.jpg)"A partire da oggi alle 23.30 sulle frequenze di Radio Radicale, Fuor di Pagina, la rassegna stampa del martedì sulle libertà sessuali a cura dell'Associazione Radicale Certi Diritti, darà spazio per qualche tempo a un ciclo di approfondimenti sull'iter delle unioni civili in Commissione giustizia al Senato. Ogni settimana spazio a parlamentari e giuristi per approfondire il dibattito attorno al 'ddl Cirinnà'" Così Leonardo Monaco, tesoriere dell'Associazione Radicale Certi Diritti e conduttore della trasmissione. "'Conoscere per deliberare' è la bandiera di Radio Radicale, e noi cercheremo di far arrivare nelle case degli italiani gli elementi necessari per valutare l'operato del Legislatore, chiamato ad esprimersi dopo 40 anni di inerzia totale sul versante del Diritto di Famiglia". Aggiunge Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti. Oggi, 20 gennaio, sarà il senatore del Movimento 5 Stelle ad inaugurare la serie di speciali di approfondimento.

Comunicato stampa dell'Associazione Radicale Certi Diritti