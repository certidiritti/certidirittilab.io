---
title: 'La mancanza del mutuo riconoscimento delle unioni omosessuali in Europa: Un ostacolo alla libertà di movimento'
date: Wed, 23 Nov 2011 09:14:54 +0000
draft: false
tags: [Diritto di Famiglia]
---

Conclusa a Strasburgo la Conferenza sul mancato riconoscimento, tra Stati, delle unioni tra persone stesso sesso. Ecco Il documento finale >

Roma, 23 novembre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Si sono conclusi a Strasburgo i lavori della Conferenza internazionale intitolata "La mancanza del riconoscimento reciproco di unioni civili e matrimonio omosessuale tra gli stati membri dell'Unione Europea e del Consiglio d'Europa: un ostacolo alla libertà di movimento delle persone?". Ai lavori ha partecipato una delegazione dell'Associazione Radicale Certi Diritti.

La Conferenza si è svolta in due giorni ed è stata organizzata dalla Ong Fèdération Nationale de l'Autre Circle, dal Consiglio d’Europa, con il Patrocinio del Segretario Generale.  Ai lavori hanno partecipato esperti e rappresentanti di Ong e organismi governativi di molti paesi europei. I temi trattati sono stati: il riconoscimento delle unioni omosessuali in Europa, il ruolo della Corte Europea dei Diritti dell'Uomo, nonchè focus su alcune situazioni nazionali sull’Italia, la Spagna, l'Ungheria, l'Irlanda, l'Olanda, la Finlandia, la Francia e la Polonia.  
  

**Qui di seguito pubblichiamo il documento finale di Strasburgo, votato all’unanimità dai partecipanti alla Conferenza:**

**La mancanza del mutuo riconoscimento delle unioni omosessuali in Europa: Un ostacolo alla libertà di movimento**

Strasburgo, Francia, 18 e 19 novembre 2011

Dichiarazione di Strasburgo

Noi, partecipanti della Conferenza con una particolare competenza sulle questioni LGBT, riuniti a Strasburgo il 18 e 19 novembre 2011 sotto gli auspici del Segretariato Generale del Consiglio d’Europa, con il sostegno della Città di Strasburgo e su iniziativa dell’ONG francese l’Autre Circle, entro i limiti della nostra riflessione sull’uguaglianza dei diritti per la popolazione LGBT dell’Europa,

Ringraziando il Senatore-Sindaco di Strasburgo, Roland Ries, e il Direttore dei Diritti Umani e dell’Antidiscriminazione del Consiglio d’Europa, Ralf René Weingaerther, per la loro calda ospitalità, il loro benvenuto e il loro contributo al lavoro della conferenza,

1\. Dichiarazione di principi fondamentali

                    1.1. Ricordiamo che il dovere degli Stati membri del Consiglio d’Europa è di non discriminare la vita famigliare di tutte le coppie, anche quando le coppie si sono spostate, si stanno spostando o vogliono spostarsi da un paese all’altro.

                    1.2. Consideriamo che una pietra miliare della legislazione del Consiglio d’Europa è il diritto al rispetto per la vita famigliare (articolo 8 della Convenzione Europea dei Diritti Umani).

                    1.3. Sottolineiamo che la Corte Europea dei Diritti Umani ha ripetutamente sostenuto che non solo le coppie di sesso diverso hanno una vita famigliare, ma anche le coppie dello stesso sesso.

                    1.4. Riconosciamo che l’articolo 14 della Convenzione Europea dei Diritti Umani obbliga i 47 paesi membri del Consiglio d’Europa ad assicurare il godimento di questo diritto senza alcuna discriminazione.

                    1.5. Ricordiamo che la Corte Europea dei Diritti Umani ha ripetutamente affermato che ragioni particolarmente serie sono richieste per giustificare una discriminazione basata sull’orientamento sessuale.

                    1.6. Consideriamo che una pietra angolare del diritto comunitario è la libertà di movimento, come specificato dalla direttiva 2004/38/EC sui diritti dei cittadini dell’Unione e dei loro famigliari di spostarsi e risiedere liberamente entro il territorio degli Stati membri.

                    1.7. Enfatizziamo che l’articolo 2 della direttiva include la nozione di “famigliari”: il/la coniuge del cittadino dell’Unione, i suoi figli minori di 21 anni, il/la coniuge dei figli minore di 21 anni, alcuni parenti dipendenti, e (nei paesi dove le unioni civili sono previste) il partner unito civilmente e i suoi figli minori di 21 anni.

                    1.8. Riconosciamo che l’articolo 3 della direttiva aggiunge che gli Stati membri dovrebbero anche “facilitare l’ingresso e la residenza” de, tra gli altri, “i partner con cui il cittadino dell’Unione abbia una relazione duratura, debitamente attestata”.

                    1.9. Indichiamo che deriva dall’articolo 21 della Carta Europea dei Diritti Fondamentali che i 27 Stati membri dovrebbero implementare questa Direttiva senza discriminazioni sulla base dell’orientamento sessuale.

 1.10. Sottolineiamo che sia la Corte di giustizia dell’Unione Europea e la Corte Europea dei Diritti Umani hanno ripetutamente sostenuto che le discriminazioni tra coppie omo e eterosessuali sono una forma di discriminazione basata sull’orientamento sessuale.

1.11. Consideriamo che il non riconoscimento di matrimoni o unioni civili stranieri principalmente a causa dell’orientamento sessuale della coppia, manca di rispetto alla loro vita famigliare e costituisce un importante ostacolo alla loro libertà di movimento.

1.12. Riaffermiamo la nostra visione di un’Europa nella quale i diritti delle persone LGBT siano equiparati a quelli degli eterosessuali, in particolare nell’area del matrimonio e delle unioni civili e nel mutuo riconoscimento di quei matrimoni e unioni civili attraverso le frontiere nazionali.

1.13. Riaffermiamo la nostra solidarietà con le persone LGBT i cui diritti sono stati negati dai governi nazionali o che si sono trovati di fronte a ostacoli amministrativi quando hanno tentato di godere di quei diritti. In particolare esprimiamo la nostra solidarietà con le persone transgender che subiscono discriminazioni e hanno problemi nell’ottenere il riconoscimento del loro genere acquisito o soffrono a causa del suo non riconoscimento.

1.14. Diamo il nostro sostegno agli sforzi fatti dal Consiglio d’Europa, specialmente la Raccomandazione (2010)5 della Commissione dei Ministri del Consiglio d’Europa agli stati membri sulle misure per combattere le discriminazioni basate sull’orientamento sessuale e l’identità di genere, adottata il 31 marzo 2010 e le raccomandazioni del Commissario per i Diritti Umani, Thomas Hammarberg, nel suo rapporto del 2011 sulle Discriminazioni basate sull’orientamento sessuale e l’identità di genere in Europa (si veda pagina 13 della raccomandazione 5.5 sul riconoscimento e i diritti delle coppie dello stesso sesso).

1.15. Diamo il nostro supporto agli sforzi dell’Unone Europea per migliorare l’attualmente imperfetta trasposizione nelle legislazioni nazionali della Direttiva 2004/38/EC.

1.16. Sottolineiamo l’importanza di adottare delle regolamentazioni sui diritti di proprietà delle coppie sposate o unite civilmente senza riguardo all’orientamento sessuale e della recente consultazione sul mutuo riconoscimento dei documenti di stato civile.

2\. Richiesta d’azione.

2.1. Chiediamo ai politici europei e nazionali di porre immediatamente fine alle discriminanti barriere legali e regolamentari a cui sono esposte le coppie gay e lesbiche nella loro mobilità internazionale e di assicurare che le coppie dello stesso sesso vedano garantite eguaglianza di diritti con le coppie omosessuali.

2.2. Intendiamo incoraggiare l’uso di cause pilota per identificare e chiudere le falle presenti nella legislazione e creare una giurisprudenza volta a facilitare cambiamenti delle leggi o delle politiche che violano diritti costituzionali o umani, e assicurare che le leggi siano propriamente interpretate e applicate.

2.3. Parteciperemo in forum per la formazione di politiche e strategie per assicurare il mainstreaming di tematiche e prospettive LGBT nella realizzazione della libertà di movimento e dei diritti umani, sia a livello europeo sia a livello nazionale.

2.4. Incoraggeremo organizzazioni pubbliche e private a implementare politiche di diversity management che assicurino l’inclusione dei loro dipendenti LGBT e rispettino il loro diritto alla vita famigliare, includendo quei casi nei quali sia interesse sia dei dipendenti sia delle organizzazioni coinvolte.

2.5. Identificheremo le prassi migliori e incoraggeremo e accompagneremo tali organizzazioni nell’implementazione delle loro politiche LGBT di diversity management; forniremo materiale e programmi di formazione a questo fine.