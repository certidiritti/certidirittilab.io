---
title: 'Gravissimo atto omofobo di Stato in Uganda: interrotta riunione di attivisti gay. Tentativo di arresto di Kasha, che è riuscita a fuggire. Le daremo aiuto'
date: Wed, 15 Feb 2012 17:00:31 +0000
draft: false
tags: [Africa]
---

In Uganda il ministro dell'etica e dell'integrità interrompe riunione di attivisti gay. Tentativo di arresto di Kasha Jaqueline Nabagasera, attivista che aveva partecipato all'Europride di Roma.

Roma, 15 febbraio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

\* nella foto Kasha all'Europride di Roma del 12 giugno 2011. Foto di Marco Marchese.

Ieri, a Entebbe, in Uganda, all’Imperial Resort Beach Hotel, il Ministro dell’Etica e dell’Integrità dell’Uganda, Simon Lokodo, accompagnato da alcuni poliziottti, ha interrotto una riunione di attivisti Lgbt dando l’ordine di arrestare l’attivista Kasha Jaqueline Nabagasera  che è  riuscita a scappare. Kasha  lo scorso giugno aveva partecipato all’Europride di Roma ed era intervenuta  a diversi incontri organizzati da Agedo e Certi Diritti.  Nei mesi scorsi le era stato consegnato il premio 2011  Martin Ennals for Human Rights Defenders.

Il Ministro ugandese e la polizia hanno fatto disperdere gli attivisti riuniti per un workshop in corso da alcuni giorni, organizzato dalla Ong ‘Freeedom and Roam Uganda’, Associazione che si batte per il riconoscimento delle unioni civili, dichiarando che  ha fatto chiudere la conferenza perché è illegale, ha poi urlato agli attivisti  che “Non accettiamo l’omosessualità in Uganda, quindi andatevene a casa”. Alla riunione partecipavano 30 attivisti e si doveva conlcudere nella serata di ieri.

L’Avvocato ugandese Francis Onyango, Presidente onorario dell’Associazione Radicale Certi Diritti, ci ha subito informati di questo gravissimo atto omofobo di Stato. Ci siamo resi disponibili ad accogliere a Roma Kasha qualora decidesse di lasciare il paese.

Abbiamo chiesto all’Intergruppo Lgbt al Parlamento Europeo di attivarsi con urgenza presso tutte le sedi istituzionali europee per intervenire nei confronti dell’Uganda contro questo ennesimo atto di barbarie.  
  

**iscriviti alla newsletter >[  
](newsletter/newsletter)[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**