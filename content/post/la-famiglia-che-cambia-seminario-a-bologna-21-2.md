---
title: 'LA FAMIGLIA CHE CAMBIA, SEMINARIO A BOLOGNA 21-2'
date: Wed, 11 Feb 2009 07:57:27 +0000
draft: false
tags: [Comunicati stampa]
---

Seminario formativo in diritto di famiglia e tutela antidiscriminatoria  
  
Promosso e Organizzato da: Comitato Provinciale Arcigay “Il Cassero”  
con la collaborazione di: Rete Lenford – Avvocatura per i Diritti Lgbt,  
ufficio Politiche delle Differenze – Comune di Bologna, Famiglie Arcobaleno, Gruppo Consiliare PD Bologna, 3D Bologna  
  
Evento formativo accreditato dal Consiglio dell’Ordine degli Avvocati di Bologna  
Evento formativo patrocinato dal Comune di Bologna  
**  
sabato 21 febbraio 2009 ore 9.30  
c/o Palazzo d’Accursio (Sala Bianca), piazza Maggiore 6, Bologna**  
  
Si terrà sabato 21 febbraio dalle ore 9.30 alle ore 17.30 a Bologna presso Palazzo d’Accursio (Sala Bianca) in piazza Maggiore 6, il corso formativo La Famiglia che cambia promosso dal Comitato Provinciale Arcigay Il Cassero, patrocinato dal Comune di Bologna e accreditato dall’Ordine degli Avvocati di Bologna (nr. 6 crediti).  
L’evento è organizzato con la collaborazione di Rete Lenford - Avvocatura per i Diritti Lgbt, dell’ufficio Politiche delle Differenze del Comune di Bologna, del gruppo consiliare di Bologna del PD, di Famiglie Arcobaleno e di 3D.  
L’ingresso è gratuito.  
  
Il seminario (rivolto principalmente agli Avvocati, ma aperto a tutti) affronterà il tema della pluralità delle relazioni familiari esistenti al di fuori del modello matrimoniale consegnatoci dalla tradizione e delle varie soluzioni giuridiche adottate in molti paesi europei ed extraeuropei per regolare il fenomeno delle convivenze ed approntare nuovi schemi giuridici per quanti non possono o non vogliono sposarsi.  
Il corso offrirà, inoltre, una panoramica comparata sulla tipologia e le dimensioni del fenomeno discriminatorio in ambito familiare e l’analisi della normativa internazionale e nazionale inerente le discriminazioni basate sull’orientamento sessuale.  
  
**Relazioni**  
  
**• Dott.ssa Maria Acierno**, Magistrato in servizio presso l'Ufficio del Massimario e del Ruolo della Corte di Cassazione: Le fonti internazionali e la giurisprudenza della CEDU e della Corte di Cassazione  
**• Avv. Francesco Bilotta**, Ricercatore di diritto privato presso l’Università di Udine, avvocato del Foro di Trieste: Evoluzione giurisprudenziale in materia di famiglia di fatto e prospettive di tutela delle unioni tra persone dello stesso sesso de iure condito  
**• Dott. Matteo Bonini Baraldi**, dottore di ricerca presso l’Università di Bologna- PhD EU Research Adviser -Law, Political Sciences & Economics: Le unioni registrate all’estero nel diritto internazionale privato  
**• Dott. Marco Gattuso**, Giudice presso il Tribunale Civile di Reggio Emilia: La famiglia gay e la Costituzione  
**• Dott. Helmut Graupner**, avvocato in Vienna "European Commission on Sexual Orientation Law (ECSOL)": The Case Tadao Maruko: Equal right to pension benefits? Legal implications of the Maruko judgment  
**• Avv. Antonio Rotelli**, avvocato del foro di Taranto: I registri comunali delle unioni civili e le anagrafi  
  
  
Intervento: Avv. Maria (Milli) Virgilio, Assessora del Comune di Bologna - Assessorato Scuola,  
Formazione e Politiche delle differenze.  
  
Moderatori: **Avv. Anna Maria Tonioni**, avvocato del foro di Bologna  
**Dott. Michele Giarratano**, responsabile settore Giuridico Cassero  
  
  
**Altre informazioni  
  
L’ingresso è gratuito e aperto al pubblico.**  
Il costo per l'accreditamento formativo (per i soli avvocati) è di 60 euro.  
Per gli Avvocati, soci di Arcigay, che si iscriveranno al convegno direttamente presso la sede del Cassero (via don Minzoni, 18) e per i soci Rete Lenford il costo è di 50 euro (entro il 13 febbraio).  
Per informazioni: giuridico@cassero.it  
  
  
  
  
  
  
  
  
  
_Informazioni tecniche sul Convegno  
Segreteria Organizzativa dell'Evento: Maria La Paglia  
tel. 051.5280391 (lun. e giov. 15.30-18.30, mart. e merc. 10-13)  
fax. 051 6495015 mail: giuridico@cassero.it_