---
title: 'Quello che le regioni possono fare per i diritti delle persone lgbt, e non solo'
date: Tue, 05 Feb 2013 23:47:24 +0000
draft: false
tags: [Politica]
---

Documento di proposte dell'associazione radicale Certi Diritti per tutti i candidati e le candidate ed anche per i consiglieri e le amministrazioni in carica. Lo Stato inattivo non sia la scusa per le regioni.

Comunicato stampa dell'associazione radicale Certi Diritti

Roma, 6 febbraio 2013

Non è vero che in Italia dobbiamo aspettare che il Parlamento legiferi per realizzare quella parità di trattamento che la costituzione chiede per tutti e tutte, senza limitazioni. Sia per i diritti individuali che per quelli familiari le Regioni, che hanno potere legislativo e programmatorio in ampi settori della pubblica amministrazione, possono intervenire con provvedimenti che mettano in pratica il dettato costituzionale e le norme comunitarie in materia di rimozione delle cause che producono disparità di trattamento e discriminazione.

E soprattutto non è utile (non deve essere così) che gli interventi a favore delle persone lgbt siano realizzati dimenticandosi delle altre potenziali cause di discriminazione che le nostre leggi hanno individuato: potremmo citare l'articolo 3 della Costituzione, che tutti dovrebbero conoscere. MA ancora più precise sono la Carta europea dei diritti fondamentali e il Trattato per il Funzionamento dell'Unione Europea (vere e proprie leggi anche per i nostro Paese) che indicano quali siano gli ambiti prioritari di intervento per la rimozione delle cause di discriminazione: genere, orientamento sessuale, disabilità, età, nazionalità e origine etnica, religione e credenze personali.

Famiglia, politiche sociali, sanità, casa, sono i principali ambiti nei quali le Regioni possono intervenire, o con leggi o con provvedimenti amministrativi, in attesa che il Parlamento intervenga con proprie leggi. In tutti questi ambiti le Regioni possono, intervenendo a favore di una di queste categorie di persone, intervenire a favore di tutte.

Lo hanno già fatto, parzialmente, la Liguria, la Toscana e l'Emilia Romagna. Possono farlo, come loro e magari andando ulteriormente avanti sulla stessa strada, tutte le Regioni italiane.

L'associazione radicale certi diritti ha sintetizzato le principali iniziative che le Regioni possono attivare per colmare il gap di uguaglianza per le persone gay lesbiche e transessuali in Italia. Lo ha fatto riassumendo in un documento le specifiche iniziative che possono essere adottate.

E dichiara fin d'ora che sarà a disposizione di chiunque voglia lavorare in questa direzione.

**[SCARICA IL DOCUMENTO IN PDF >](notizie/comunicati-stampa/item/download/26_38fb3b909136fbe8e4a90329d5776263)[  
](notizie/comunicati-stampa/item/download/25_005e2ad815bc39b8614e2545032f0bd9)**

**[Sostieni l'associazione >](partecipa/iscriviti)**

**[  
](notizie/comunicati-stampa/item/download/25_005e2ad815bc39b8614e2545032f0bd9)Candidati alle elezioni regionali che hanno sottoscritto il documento:  
  
LAZIO  
**Giuseppe **Rossodivita** \- Amnistia Giustizia Libertà  
Rocco **Berardo** \- Amnistia Giustizia Libertà  
Sergio **Rovasio** \- Amnistia Giustizia Libertà  
Riccardo **Magi** \- Amnistia Giustizia Libertà  
Mario **Staderini** \- Amnistia Giustizia Libertà  
Massimiliano **Iervolino** \- Amnistia Giustizia Libertà  
Paola **Di Folco** \- Amnistia Giustizia Libertà  
Luca **Comellini** \- Amnistia Giustizia Libertà

Luca **Martinelli** \- Fare per Fermare il Declino  

**LOMBARDIA**  
Marco **Mori** \- Partito Democratico  
  
Luca **Trentini** \- Sinistra Ecologia e Libertà  
  

Otto **Bitjoka**, Patto Civico con Ambrosoli

  
[certidiritti\_richieste\_regioni\_2013\_pdf_2.pdf](http://www.certidiritti.org/wp-content/uploads/2013/02/certidiritti_richieste_regioni_2013_pdf_2.pdf)