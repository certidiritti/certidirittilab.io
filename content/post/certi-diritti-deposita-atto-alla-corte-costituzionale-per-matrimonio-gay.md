---
title: 'CERTI DIRITTI DEPOSITA ATTO ALLA CORTE COSTITUZIONALE PER MATRIMONIO GAY'
date: Fri, 06 Nov 2009 15:36:18 +0000
draft: false
tags: [Comunicati stampa]
---

Nei giorni scorsi l'Associazione Radicale Certi Diritti ha depositato, grazie alla disponibilità della Costituzionalista  Prof. Avv. Marilisa D'Amico,  un atto di intervento relativo al rinvio alla Corte Costituzionale, da parte del Tribunale di Trento, del ricorso presentato da alcune coppie gay riguardo il diniego opposto loro dal Comune relativamente alla richiesta delle pubblicazioni per il loro matrimonio.

Questa azione ha lo scopo di sostenere davanti alla Corte Costituzionale le ragioni poiltiche e sociali del matrimonio tra persone dello stesso sesso, così come avviene in diversi paesi europei.

Alla Corte costituzionale si pone la questione della illegittimità costituzionale delle norme del codice civile che sembrerebbero escludere il matrimonio per le coppie omosessuali, in relazione agli artt. 2 (che tutela i diritti fondamentali di tutti gli individui) e 29 (che tutela la famiglia, senza specificare che il matrimonio é fra un uomo e una donna) della Costituzione.

La decisione della Corte  è attesa nei prossimi mesi.