---
title: 'Pride di Barcellona ospita comunità lgbt di Tel Aviv'
date: Sun, 26 Jun 2011 12:55:26 +0000
draft: false
tags: [Movimento LGBTI]
---

**Barcellona, La nuova capitale gay del Mediterraneo ospita quest'anno la Comunita’ Lgbt israeliana che lo scorso anno fu ingiustamente esclusa dagli organizzatori dell'Europride di Madrid.**

**Dichiarazione di Sergio Rovasio, Segretario dell’ Associazione Radicale Certi Diritti e Ugo Millul, rappresentante di Certi Diritti a Barcellona**

Barcellona - Roma, 26 giugno 2011

Dal 18 al 28 giugno 2011 si svolge il Gaypride di Barcellona che vedrà coinvolte decine di migliaia di persone in una delle città più gayfriendly d’Europa.

E’ passato un anno da quando gli organizzatori dell'Europride del 2010 di Madrid decisero di escludere la partecipazione delle associazioni israeliane per i diritti delle persone LGBTE perché avevano la colpa di essere anche finanziate dal comune di Tel Aviv che, secondo la piattaforma organizzatrice dell´evento, non aveva denunciato il comportamento dell'esercito israeliano in relazione ai fatti della _Freedom Flotilla._

La città di Tel Aviv è quindi oggi protagonista  del Gay Pride Barcellona 2011 in quanto città invitata perché considerata "l'oasi gayfriendly del Medio Oriente."

Tel Aviv sará presente attraverso la partecipazione alla sfilata di carri tematici e con l'esibizione di artisti israeliani della scena LGBTE: dalla DJ Yoav Arnon al gruppo musicale Knob – con il patrocinio del Gay Pride da parte di alcune associazioni israeliane.

Ringraziamo gli organizzatori del Gay Pride di Barcellona del 2011 per la scelta di promuovere Tel Aviv, riparando cosí all'ingiusta esclusione delle associazoni israeliane dall'Europride 2010.

Va anche ricordato che Associazioni israeliane come Agudah e la Open House di Gerusalemme svolgono uno straordinario lavoro di aiuto, protezione e sostegno anche alle e ai  palestinesi.

Barcellona, che aspira ad esser riconosciuta come capitale gay del Mediterraneo, riconosce in Tel Aviv un importante punto di riferimento e l'opposta sponda per la protezione e per la promozione dei diritti LGBTE, nel bacino del Mediterraneo.