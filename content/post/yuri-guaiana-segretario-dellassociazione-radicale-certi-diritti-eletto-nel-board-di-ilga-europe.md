---
title: 'Yuri Guaiana, Segretario dell''Associazione Radicale Certi Diritti, eletto nel board di ILGA Europe'
date: Sat, 26 Oct 2013 11:29:09 +0000
draft: false
tags: [Movimento LGBTI]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti

Zagabria, 26 ottobre 2013

Con 80 voti su 190, Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti è stato eletto nel direttivo di ILGA-Europe, l'organizzazione non governativa europea di lesbiche, gay, bisessuali, transessuali e intersessuali che riunisce 408 associazioni di 45 paesi europei. ILGA-Europe è la branca europea di ILGA, l'associazione mondiale fondata nel 1978 lavora per i diritti umani delle persone Lgbti a livello europeo.

La candidatura di Yuri Guaiana è stata sostenuta da Associazione Radicale Certi Diritti, Arcigay, Arcigay "Il Cassero" - Bologna, Centro d'iniziativa Gay - Comitato provinciale Arcigay di Milano, Arcigay Arcilesbica Omphalos, Famiglie Arcobaleno, Commission LGBT d'EELV, France, Cavaria vzw, Belgium, FELGBT Federación Estatal de Lesbianas, Gays, Transexuales y BisexualesLAMBDA, Fondazione Fuori!, DìGay Project - DGP Italia, Cavaria vzw, Belgium

Yuri Guaiana, dichiara: "ringrazio di cuore tutte le associazioni italiane ed europee che mi hanno sostenuto e i delegati di vari paesi che mi hanno votato. Ritengo questa una grande opportunità non solo per me, ma soprattutto per l'intero movimento Lgbti italiano che potrà portare le sue varie voci al cuore della più importante organizzazione Lgbti europea in maniera più continuativa e strutturata, ma che avrà anche la possibilità di essere più coinvolto nelle tante importanti iniziative che ILGA-Europe promuove regolarmente".