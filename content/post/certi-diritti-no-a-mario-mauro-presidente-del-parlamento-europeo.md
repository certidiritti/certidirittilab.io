---
title: 'CERTI DIRITTI: "NO A MARIO MAURO PRESIDENTE DEL PARLAMENTO EUROPEO"'
date: Wed, 17 Jun 2009 18:32:04 +0000
draft: false
tags: [Comunicati stampa]
---

_**Dichiarazione di Sergio Rovasio, Segretario di Certi Diritti:**_

"Nonostante le promesse ed i proclami di Berlusconi, di Frattini, di Alfano, non sembra che l'Italia riuscirà a piazzare il candidato del PDL Mario Mauro alla Presidenza del Parlamento europeo. Come riportano i giornali internazionali, a Mario Mauro il suo stesso gruppo politico, il PPE, preferisce il candidato polacco, protestante, ex-Primo Ministro Buzek per il prestigioso posto. Le stesse fonti riferiscono che Mauro é visto come "troppo pio". In realtà é visto come troppo vicino alle posizioni della destra cattolica fondamentalista. Anche se il PPE decidesse di candidare Mauro, il PSE o il gruppo ALDE dovrebbero poi votare per lui, cosa poco probabile.  
  
Mario Mauro, rispetto agli altri deputati italiani del centro-destra, che spiccano per le loro assenze nel corso dei lavori del PE, sarà pure persona corretta e lavoratrice ma le sue posizioni politiche sono considerate troppo integraliste dallo stesso gruppo del PPE.

  

Mario Mauro si é pronunciato con opinioni controverse e in modo molto chiaro su alcuni temi: dalla bandiera europea, da lui definiia "simbolo cristiano della Vergine Maria", al continuo richiamo all'Europa "nata cristiana e per la difesa dell'identità e delle radici cristiane altrimenti l'Europa non é", dalla "dittatura laica totalitaria" che si afferma in Europa, al "supermercato dei diritti voluto dalla comunità internazionale sui diritti umani" fino alla proposta di "un concordato tra UE e chiesa cattolica sul modello italiano" alla risoluzione sull'omofobia approvata dal PE definita come un "manifesto inneggiante alla distruzione dei valori". Inoltre le sue  idee sulla famiglia "deformate e devianti" e quelle sulla ricerca sulle cellule staminali con richiamo a Mendele danno bene l'idea a quali idee poco liberali e tolleranti egli si ispiri. La sua vicinanza a CL ed ai Legionari di Cristo lo dovrebbe vedere impegnato su altri pulpiti piuttosto che quelli del Pe.

  
Ci auguriamo vivamente che non si sia costretti a vivere un dejà vu, ovvero un Buttiglione 2, sfiduciato dal Pe come Commissario per le sue idee troppo integraliste.

Il dossier su Mario Mauro é disponibile al sito di Certi Diritti [a questo linK per la versione pdf](index.php?option=com_docman&task=doc_download&gid=38&Itemid=79) e a [questo per la versione internet](index.php?option=com_content&view=article&id=391)