---
title: 'CROCIFISSO OBBLIGATORIO: VENERDI'' 22-1 RADICALI  AL CSM  IN DIFESA GIUDICE TOSTI'
date: Thu, 21 Jan 2010 13:55:06 +0000
draft: false
tags: [Comunicati stampa]
---

**PROCEDIMENTO DISCIPLINARE DELCSM CONTRO IL MAGISTRATO LUIGI TOSTI, COLPEVOLE DI NON VOLERE I CROCIFISSI NELLE AULE DEL TRIBUNALE. VENERDI' 22 GENNAIO I RADICALI DAVANTI AL CSM.**

Venerdì 22 gennaio, dalle 9,30 Radicali Italiani e le Associazioni Radicali Certi Diritti, Anticlericale.net, Luca Coscioni, saranno davanti alla sede del Csm con dirigenti e militanti radicali con lo slogan: ‘Si alla liberà di religione, no alle imposizioni religiose'.

Venerdì infatti si svolgerà al Consiglio Superiore della Magistratura la prima udienza disciplinare che vede imputato il Giudice Luigi Tosti, reo di non volere nell'aula del Tribunale il crocefisso, o, in alternativa, di avere affiancati al crocifisso anche gli altri simboli religiosi.

Il giudice Tosti ha subìto due condanne penali ad un anno di reclusione poi annullate dalla Corte di Cassazione. Da quattro anni è stato sospeso dalle sue funzioni e delllo stipendio.