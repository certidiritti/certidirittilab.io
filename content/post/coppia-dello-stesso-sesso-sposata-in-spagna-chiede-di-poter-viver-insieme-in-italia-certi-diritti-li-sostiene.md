---
title: 'Coppia dello stesso sesso sposata in Spagna chiede di poter viver insieme in Italia. Certi Diritti li sostiene'
date: Wed, 19 Oct 2011 11:19:34 +0000
draft: false
tags: [Diritto di Famiglia]
---

Nel ricorso, tra le motivazioni addotte,  si fa riferimento alla Sentenza della Corte Costituzionale 138/10 che afferma: a) all’unione omosessuale, “intesa come stabile convivenza tra due persone dello stesso sesso”, spetta “il diritto fondamentale di vivere liberamente una condizione di coppia”.

Roma, 18 ottobre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Due ragazzi, uno uruguaiano e l’altro italiano, che si sono sposati  lo scorso anno in Spagna, dove il matrimonio tra persone dello stesso sesso è riconosciuto, hanno chiesto alle autorità italiane di poter vivere in Italia. A seguito del diniego, l’Associazione Radicale Certi Diritti ha promosso un ricorso con gli avvocati della coppia affinché venga riconosciuto questo diritto, peraltro garantito dalla Corte Costituzionale che indica una precisa strada in materia di tutela delle coppie formate da persone dello stesso sesso.

In ambito Ue è evidente che l’aver negato questo diritto viola il Trattato di Nizza sulla libera circolazione e il Trattato di Lisbona sulla lotta alle discriminazioni.

Nel ricorso viene chiesto il rilascio di una carta di soggiorno per familiare di cittadino comunitario non avente la cittadinanza di uno Stato membro dell’Unione europea, così come  la Spagna l’ha rilasciato al ragazzo uruguaiano.  Nel ricorso, tra le motivazioni addotte,  si fa riferimento alla Sentenza della Corte Costituzionale 138/10 che afferma: a) all’unione omosessuale, “intesa come stabile convivenza tra due persone dello stesso sesso”, spetta “il diritto fondamentale di vivere liberamente una condizione di coppia”. Inoltre non si può creare una differenza di trattamento rispetto alla condizione delle coppie eterosessuali pena la violazione degli art. 2 e 3 della Costituzione.

L’Associazione Radicale Certi Diritti si augura che la sentenza, imminente, sia favorevole alla richiesta della coppia. L’intervento dei Giudici dovrà essere coerente con quanto affermato innumerevoli volte dalla Corte Costituzionale e dalla Cassazione proprio per la mancanza di ogni tutela normativa per le coppie e famiglie non sposate (siano esse eterosessuali o omosessuali).  
  
iscriviti alla newsletter

**www.certidiritti.it/newsletter/newsletter**