---
title: 'MATRIMONIO OMOSESSUALE:SARà UN SINDACO LEGHISTA A CELEBRARE IL PRIMO?'
date: Sat, 13 Feb 2010 07:51:02 +0000
draft: false
tags: [Senza categoria]
---

Dichiarazione dei Candidati alle elezioni regionali in Lombardia nella Lista Bonino-Pannella  
Francesco Poirè, Segretario dell'_Associazione Enzo Tortora - Radicali Milano_  
Gian Mario Felicetti, Responsabile dell'_Associazione Radicale Certi Diritti_ per la campagna di Affermazione Civile  
Luca Piva, Tesoriere dell'_Associazione Radicale Certi Diritti_

"Apprendiamo con sorpresa dai giornali che il Sindaco leghista di Viggiù, Sandy Cane, ha rifiutato "a malincuore" di unire civilmente in matrimonio due donne che hanno chiesto la pubblicazione degli atti nel suo Comune.

Vorremmo ricordare al sindaco di Viggiù - con cui cercheremo di entrare in contatto anche personalmente - che la Legge italiana non impedisce ad una coppia omosessuale di accedere al matrimonio ma che, semplicemente, si è affermata una consuetudine che, per ora, non l'ha permesso.

Questo è quanto, da oltre due anni, l'Associazione Radicale Certi Diritti ([www.certidiritti.it](http://www.certidiritti.it/)) e l'avvocatura LGBT - Rete Lenford ([www.retelenford.it](http://www.retelenford.it/)), stanno cercando di far comprendere al Paese, attraverso l'iniziativa di Affermazione civile ([www.affermazionecivile.it](http://www.affermazionecivile.it/)), che ha portato la questione davanti alla Corte Costituzionale.  
  
Siamo in attesa della sentenza della Consulta, prevista per il 23 marzo prossimo: ci auguriamo che porrà fine alla violazione del diritto dei cittadini omosessuali a contrarre matrimonio con una persona del proprio stesso sesso.  
  
Come candidati della Lista Bonino-Pannella, ci candidiamo in Lombardia anche per portare dentro alle istituzioni lombarde la voce delle persone gay, bisessuali e transessuali."