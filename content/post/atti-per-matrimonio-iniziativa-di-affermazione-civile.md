---
title: 'ATTI PER MATRIMONIO INIZIATIVA DI AFFERMAZIONE CIVILE'
date: Tue, 15 Apr 2008 17:58:39 +0000
draft: false
tags: [Affermazione Civile, certi diritti, PUbblicazione degli atti, Senza categoria]
---

**_AFFERMAZIONE CIVILE 2008-2010_****  
_Un’azione concreta per un diritto che nessuno può negare alle coppie omosessuali: sposarsi._**

Un diritto che è negato sulla base di un pregiudizio millenario e che non trova nessuna giustificazione e nessuna copertura nell'ordinametno italiano, nel quale c'è tutto lo spazio che occorre affinché i matrimoni omosessuali possano essere celebrati ed avere pieno riconoscimento.

Con **AFFERMAZIONE CIVILE**, vogliamo ottenere proprio questo: spazzare il pregiudizio e dare concretezza e prospettiva all'amore delle persone omosessuali che si vogliono sposare.  

Come per ogni battaglia saranno necessari **impegno e determinazione** perché è importante che le coppie omosessuali ci mettano la faccia e siano disposti a testimoniare in tribunale per difendere il loro amore e la loro voglia di formare una famiglia.  

E noi di CERTI DIRITTI saremo lì con loro, a seguire passo per passo le coppie omosessuali che vorrano coronare il loro progetto d'amore: dalla richiesta di **pubblicazione degli atti**, passando per tutti gli altri atti amministrativi necessari ad arrivare ad una sentenza che si spera possa essere favorevole.

Le coppie che desiderano unirsi alle altre decine di coppie che già hanno avviato la loro **AFFERMAZIONE CIVILE**, potranno contare sull'**assistenza gratuita** dei legali della **[Rete Lenford](http://www.retelenford.org/)**, che sostiene questa iniziativa grazie ai suoi avvocati presenti su tutto il territorio.

Se anche tu ci credi, aiutaci a sviluppare **AFFERMAZIONE CIVILE**:

*   se fai parte di una coppia omosessaule che si vuole sposare [contattaci per partecipare all'iniziativa](scrivici.html?view=category);
*   se fra le tue amicizie ci sono coppie di gay e lesbiche, fagli conoscere **AFFERMAZIONE CIVILE** e invitali a mettersi in contatto con noi.
*   dacci una mano a diffondere **AFFERMAZIONE CIVILE** tra tutte le persone che conosci attraverso i tuoi contatti, il tuo blog, il tuo sito internet, facebook e gli altri social network
*   partecipa alla discussione nel nostro [forum](component/fireboard/) e facci sapere che cosa ne pensi.

**Affermazione Civile** si avvale dell'attività volontaria di:

*   [Francesco Bilotta](mailto:questionilegali@certidiritti.it) che ne segue gli aspetti giuridico-legali e assicura il patrocinio in Tribunale, in collaborazione con i colleghi della Rete Lenford. Ci piace ricordare che Francesco Bilotta, oltre ad essere avvocato e docente di Istituzioni di Diritto Privato all'Università di Trieste è anche curatore del volume "[Le unioni tra persone dello stesso sesso. Profili di diritto civile, comunitario e comparato](component/content/article/195.html)".

*   [Matteo Pegoraro](mailto:sedilocali@certidiritti.it) e Carlo D'Ippoliti che curano la comunicazione e la promozione, oltre al rapporto con le coppie interessate all'iniziativa

*   [Gian Mario Felicetti](mailto:allegrezza@gmail.com) (3299045945), che cura il coordinamento nazionale dell'attività.

Se vuoi aiutarci nell'attività locale di promozione di Affermazione Civile, mettiti in contatto con [Gian Mario](mailto:affermazionecivile@certidiritti.it), [Carlo](mailto:roma@certidiritti.it?subject=Affermazione%20Civile) o [Matteo](mailto:sedilocali@certidiritti.it?subject=Affermazione%20Civile).

[![cosa_sono_le_pubblicazioni_degli_atti.png](http://www.certidiritti.org/wp-content/uploads/2008/04/cosa_sono_le_pubblicazioni_degli_atti.png "cosa_sono_le_pubblicazioni_degli_atti.png")](campagne/affermazione-civile/pubblicazione-degli-atti.html)

[![domande frequenti](http://www.certidiritti.org/wp-content/uploads/2008/04/faq.png "domande frequenti")](campagne/affermazione-civile/faq.html) [![i_fondamenti_giuridici.png](http://www.certidiritti.org/wp-content/uploads/2008/04/i_fondamenti_giuridici.png "i_fondamenti_giuridici.png")](campagne/affermazione-civile/fondamenti-giuridici.html)

  

  

  

  

  

[![cosa_sono_le_pubblicazioni_degli_atti.png](http://www.certidiritti.org/wp-content/uploads/2008/04/cosa_sono_le_pubblicazioni_degli_atti.png "cosa_sono_le_pubblicazioni_degli_atti.png")](campagne/affermazione-civile/pubblicazione-degli-atti.html)

Se vuoi capire meglio [cosa sono le Pubblicazioni degli Atti, clicca qui.](campagne/affermazione-civile/fondamenti-giuridici.html) In questo modo capirai che partecipare all'iniziativa non determina **nessuna conseguenza legale** e **nessun vincolo prematrimoniale.**

[![domande frequenti](http://www.certidiritti.org/wp-content/uploads/2008/04/faq.png "domande frequenti")](campagne/affermazione-civile/faq.html) Qui invece trovi [le risposte alle domande più frequenti](campagne/affermazione-civile/faq.html) che ci vengono rivolte. Ad ogni domanda troverai una risposta breve e semplice, e una più dettagliata, che fornisce spiegazioni in termini di legge.[![i_fondamenti_giuridici.png](http://www.certidiritti.org/wp-content/uploads/2008/04/i_fondamenti_giuridici.png "i_fondamenti_giuridici.png")](campagne/affermazione-civile/pubblicazione-degli-atti.html) Questo, invece, è un articolo che descrive il [fondamento giuridico della nostra iniziativa.](campagne/affermazione-civile/pubblicazione-degli-atti.html)

Per altre informazioni scrivi a [info@certidiritti.it](mailto:info@certidiritti.it?subject=info%20affermazione%20civile)