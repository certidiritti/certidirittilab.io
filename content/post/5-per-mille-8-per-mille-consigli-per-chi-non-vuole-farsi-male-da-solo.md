---
title: '5 per mille - 8 per mille: consigli per chi non vuole farsi male da solo'
date: Fri, 18 May 2012 07:37:40 +0000
draft: false
tags: [Politica]
---

E’ tempo di dichiarazione dei redditi ed è tempo di scelte, obbligatorie sull’8 per mille e facoltative sul 5 per mille.

  
Prima di scegliere conviene ripassare qualche informazione sul meccanismo, e sul pericolo che i nostri soldi finanzino associazioni, ma soprattutto istituzioni religiose che, come la chiesa cattolica, hanno fatto della battaglia contro i diritti lgbt un fiore all’occhiello.

L’8 per mille viene prelevato dalla dichiarazione dei redditi di ciascuno di noi ed assegnato, sulla base della nostra scelta, o allo stato o a una delle  ad una delle chiese che hanno sottoscritto l’intesa. Ma se la scelta non viene fatta, la nostra quota verrà comunque assegnata tra le chiese firmatarie, secondo una regola proporzionale che premia chi già prende di più. Quindi, mi raccomando, se non volete finanziare la chiesa cattolica, dovete scegliere di destinare i fondi allo stato o alla chiesa valdese, che come sapete è da tempo molto attiva nella difesa dei diritti lgbt in Italia.

Il 5 per mille viene prelevato dalla dichiarazione dei redditi solo se noi scegliamo di assegnarlo ad una organizzazione senza scopo di lucro che ne ha diritto. In questo caso, quindi, la libertà è assoluta, ed è una ottima occasione per premiare chi davvero, e da molto tempo, lavora per l’uguaglianza reale delle persone omosessuali, lesbiche e transessuali, in Italia e nel mondo.  
Il metodo è semplice, basta indicare nell’apposita casella il codice fiscale dell’associazione prescelta. Noi ti consigliamo:

**Associazione Luca Coscioni** ([http://www.associazionelucacoscioni.it/](http://www.associazionelucacoscioni.it/) )  
CF: 97283890586

**Associazione Nessuno Tocchi Caino** ([http://www.nessunotocchicaino.it/](http://www.nessunotocchicaino.it/))  
CF: 96267720587

**Associazione Non c’è pace senza giustizia** ([http://www.npwj.org/it](http://www.npwj.org/it) )  
CF: 97107730588

**Lambda, gruppo di volontariato per i diritti delle persone lgbti** (di Torino)  
CF: 97666510017