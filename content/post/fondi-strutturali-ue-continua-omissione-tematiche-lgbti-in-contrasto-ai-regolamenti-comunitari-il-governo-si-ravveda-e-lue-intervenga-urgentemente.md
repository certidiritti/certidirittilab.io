---
title: 'Fondi Strutturali UE: Continua omissione tematiche LGBTI in contrasto ai regolamenti Comunitari. Il Governo si ravveda e l''UE intervenga urgentemente.'
date: Fri, 17 Oct 2014 14:55:35 +0000
draft: false
tags: [Europa]
---

[![3f7e7640f41058f77960cdcaf9bbb55b143f92fb5cfb2b6909222ea5](http://www.certidiritti.org/wp-content/uploads/2014/10/3f7e7640f41058f77960cdcaf9bbb55b143f92fb5cfb2b6909222ea5-300x199.jpg)](http://www.certidiritti.org/wp-content/uploads/2014/10/3f7e7640f41058f77960cdcaf9bbb55b143f92fb5cfb2b6909222ea5.jpg)Leggiamo preoccupati che nella bozza di PON Inclusione sociale (lo strumento operativo con il quale il Governo utilizza il FSE in questa materia ) le iniziative per la prevenzione, contrasto e assistenza delle vittime di discriminazione basate su orientamento sessuale e identità di genere sono state completamente omesse. Definirsi preoccupati è poco di fronte al grave pericolo che nella prossima programmazione (ovvero gli unici fondi reali a disposizione dello Stato e delle Regioni per intervenire nella lotta contro le discriminazioni e a favore dell’inclusione sociale) sia di fatto reso impossibile l’accesso a queste risorse, anche minime, per il reinserimento di persone gay lesbiche e transessuali a rischio di esclusione dal mondo del lavoro e della socialità in generale. Tutto ciò accade contro l’evidenza dei fatti, le indagini ISTAT e le richieste esplicite dell’Unione europea. L’assenza di questo tema segue l'assenza, da noi ampiamente denunciata nel passato, di queste tematiche anche dall’Accordo di Partenariato tra Stato e UE sull’utilizzo di tutti i fondi strutturali. Il meccanismo è lo stesso: citare orientamento sessuale e identità di genere nell’ambito delle iniziative intraprese, sostanzialmente per rispettare il vincolo della condizionalità, ma non prevedere nulla di specifico nell’ambito delle iniziative concrete e dei finanziamenti. Questo meccanismo, lo ricordiamo, non potrà cancellare le precise indicazioni contenute nei Regolamenti comunitari, ma renderà di fatto molto difficile l’accesso a queste risorse per interventi in queste aree, sia da parte del Governo che da parte delle Regioni. Non possiamo non evidenziare, infine, lo scarso ruolo dell’UNAR entro lo stesso PON - Inclusione sociale, ed in tutta la programmazione futura dei fondi strutturali, che getta ancora più dubbi sulla reale capacità di incidere e operare di questo Ufficio su temi, come quelli dell’inclusione sociale, per i quali  è stato creato. Su tutte queste questioni il Ministro Poletti non ha mai non solo risposto alla richiesta di incontro che le associazioni italiane lgbti hanno avanzato, ma nemmeno fornito chiarimenti e risposte. Torniamo a sollecitare al Ministro e al Sottosegretaria Biondelli un incontro, ma soprattutto di procedere alle opportune integrazioni all’Accordo di Partenariato. In questo contesto ci sembrerebbe auspicabile anche l'intervento della neo-consigliera alle Pari Opportunità on. Giovanna Martelli. Chiediamo infine con forza un intervento dell’Unione Europea in sede di valutazione del PON Inclusione sociale.  

Fiorenzo Gimelli - AGEDO

Marco Canale - ANDOS

Flavio Romani - Arcigay

Paola Brandolini - ArciLesbica

Yuri Guaiana - Associazione Radicale Certi Diritti

Aurelio Mancuso - Equality Italia

Giuseppina La Delfa - Famiglie Arcobaleno