---
title: 'Gravissima denuncia della Lila: censurata la parola ''preservativo'' nella traduzione dell''opuscolo dell''Oms'
date: Thu, 26 Apr 2012 11:14:39 +0000
draft: false
tags: [Salute sessuale]
---

Il ministero della sanità censura la parola 'preservativo' dall'opuscolo dell'0MS per la prevenzione delle malattie sessualmente trasmissibili. Interrogazione parlamentare dei Radicali.

Comunicato Stampa dell’Associazione Radicale Certi Diritti  
Roma, 26 aprile 2012

La Lega Italiana per la Lotta all’Aids ha denunciato un fatto gravissimo:  censurata la parola ‘preservativo’ nell’opuscolo fatto tradurre in italiano dal Ministero della Sanità. L’opuscolo era la riproduzione di quello dell’Organizzazione Mondiale della Sanità dove la parola condom era presente.  E’  un fatto gravissimo che dimostra di come in questo paese, per fare un favore al Vaticano, nonostante l’aumento delle malattie sessualmente trasmissibili, si affronta il problema della prevenzione. Dopo aver cancellato sul piano nazionale e regionale le campagne di prevenzione e informazione ora siamo passati alla censura della parola preservativo, unico strumento di prevenzione sicura delle malattie durante i rapporti sessuali.  
   
I deputati Radicali eletti nel Pd hanno oggi depositato una interrogazione urgente a risposta scritta.

Di seguito il testo integrale dell'Interrogazione urgente a risposta scritta:

  
Al Ministro della Salute

Per sapere – premesso che:

Secondo quanto denunciato dalla Lila, Lega italiana per la lotta all’Aids, nella traduzione dell'opuscolo per i tifosi redatto dall'Organizzazione Mondiale della Sanità per i tifosi che tra poco più di un mese si riverseranno in Polonia e Ucraina per i Campionati europei di calcio 2012, è stato censurato il condom come mezzo di prevenzione della trasmissione dell’Hiv;

L’opuscolo dell’Oms dice testualmente che "Per evitare il rischio di malattie sessualmente trasmissibili, assicurarsi di usare correttamente il preservativo - femminile o maschile";

Lo scorso 1° dicembre, Giornata mondiale di lotta contro l'Aids, prima in conferenza stampa e poi nel mezzo delle proteste per la circolare Rai che chiedeva ai redattori di omettere il termine "preservativo", il ministro della Salute Renato Balduzzi  aveva rassicurato sul fatto che “il condom è previsto nei nostri programmi di prevenzione”; la stessa rassicurazione era stata reiterata in Commissione Nazionale Aids, dove siede anche la Lega Italiana per la Lotta contro l'Aids.

Nell’opuscolo tradotto in italiano ci si riferisce ad un generico "rapporti protetti" e si trasforma così un'informazione chiara e pragmatica in una sorta di precetto morale; secondo la Lila gli unici rapporti protetti sono quelli che prevedono l'uso del preservativo (il preservativo maschile e anche quello femminile), l'unico mezzo meccanico che garantisce, se usato correttamente, la protezione dall'Hiv e dalle altre malattie sessualmente trasmissibili.

Per sapere:

\- Per quale motivo nella traduzione in italiano dell’opuscolo dell’OMS è scomparsa la parola preservativo;

\- Quanti sono i casi di trasmissione dell’infezione dell’Hiv negli ultimi anni in Italia;

-  Se non ritenga che censurare la parola ‘preservativo’ dai documenti realizzati con l’obiettivo di prevenire la trasmissione delle malattie sessualmente trasmissibili sia inutile e dannoso;  
   
Ai seguenti link le due versioni dell'opuscolo (in allegato il confronto), quella in inglese pubblicata nel sito dell'OMS e quella italiana pubblicata nel sito del ministero della Salute:  
 

[http://www.euro.who.int/\_\_data/assets/pdf\_file/0004/158998/Health\_Travel\_Advice\_2012\_UEFA_v7.pdf](http://www.euro.who.int/__data/assets/pdf_file/0004/158998/Health_Travel_Advice_2012_UEFA_v7.pdf)

[http://www.salute.gov.it/imgs/C\_17\_newsAree\_1992\_listaFile\_itemName\_0_file.pdf](http://www.salute.gov.it/imgs/C_17_newsAree_1992_listaFile_itemName_0_file.pdf)