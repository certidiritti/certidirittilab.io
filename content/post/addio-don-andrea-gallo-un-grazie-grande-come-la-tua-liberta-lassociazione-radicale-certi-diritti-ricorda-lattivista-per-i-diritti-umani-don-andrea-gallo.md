---
title: 'Addio Don Andrea Gallo. Un grazie grande come la tua libertà. L''Associazione radicale Certi Diritti ricorda l''attivista per i diritti umani Don Andrea Gallo'
date: Fri, 24 May 2013 11:45:57 +0000
draft: false
tags: [Politica]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti.

Roma, 24 maggio 2013

Generoso e furioso, sacerdote libertario e attivista per i diritti umani di tutti e di tutte.  Ecco come ricorderemo Don Andrea Gallo con cui molte volte abbiamo incrociato i nostri passi e che abbiamo sentito al nostro fianco, in prima fila, per l'uguaglianza e la dignità delle persone omosessuali, transessuali e delle persone che si prostituiscono.

Gli dobbiamo un grazie, grande come la libertà di coscienza che hai testimoniato.