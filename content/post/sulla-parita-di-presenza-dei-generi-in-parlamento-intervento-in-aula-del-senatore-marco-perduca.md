---
title: 'Sulla parità di presenza dei generi in Parlamento: intervento in aula del senatore Marco Perduca'
date: Wed, 18 Jul 2012 13:57:50 +0000
draft: false
tags: [Politica]
---

"Allora, dal momento che state facendo un discorso presuntamente modernista, secondo il quale volete veder rappresentati all'interno delle aule parlamentari due generi, al 50 e 50, cerco di ricordarvi che siamo nel 2012 e che, al di fuori di queste aule, sono in corso dibattiti che riguardano non soltanto l'identità di genere e l'orientamento sessuale - che qui non ci interessano - ma anche i diritti da garantire a queste persone, in quanto individui portatori di diritti."

PERDUCA (PD). Domando di parlare.

PRESIDENTE. Ne ha facoltà.

PERDUCA (PD). Signor Presidente, la senatrice Poretti ed io stiamo facendo lo sciopero della fame da ieri sera per denunciare la mala amministrazione della giustizia e, come decine di migliaia di persone soprattutto negli istituti penitenziari, avevamo scelto il silenzio per affrontare i prossimi quattro giorni. Quando però si parla della Costituzione e si ascoltano alcuni ragionamenti, con rispetto parlando, è difficile mantenere il silenzio. Quindi, ci troviamo costretti ad intervenire, non sempre, ma sicuramente su alcune questioni fondamentali.

È stato già fatto notare che alla Camera dei deputati tale problema non è stato sollevato con altrettanta decisione e forza. I presentatori dell'emendamento intendono affrontare una questione di pari rappresentanza, ma ciò vorrebbe dire che in ambo le Camere detta rappresentanza è «garantita per legge».

Vi è però un problema aggiuntivo, che avevamo cercato di sollevare durante il dibattito sul finanziamento pubblico dei partiti, ossia che la scienza ed anche la sociologia ci dicono che non è così semplice definire l'identità di genere e che magari i generi - a questo punto - possono non essere più due, ma eventualmente anche tre. (Brusìo). Allora, dal momento che state facendo un discorso presuntamente modernista, secondo il quale volete veder rappresentati all'interno delle aule parlamentari due generi, al 50 e 50, cerco di ricordarvi che siamo nel 2012 e che, al di fuori di queste aule, sono in corso dibattiti che riguardano non soltanto l'identità di genere e l'orientamento sessuale - che qui non ci interessano - ma anche i diritti da garantire a queste persone, in quanto individui portatori di diritti.

Cosa implicherebbe tale riformulazione, che andrebbe sicuramente a migliorare il testo originario dell'emendamento? Anche se non ho ancora capito come si possa garantire tale pari rappresentanza di genere all'interno delle Camere, comunque, la suddetta riformulazione comporterebbe che, nel momento in cui si venisse nominati o eletti, si dovrebbe sottoscrivere una dichiarazione di appartenenza ad un determinato genere, con il divieto di cambiarlo? (Applausi dal Gruppo PdL).

Scusate, ma quest'argomento, più di tutti gli altri che sono stati utilizzati, è quello per cui - pur essendo i radicali da sempre molto attenti all'accesso di chiunque, indipendentemente da genere, religione e razza, a qualsiasi ufficio pubblico - la senatrice Poretti ed io ci asterremo, ma non nel senso che non parteciperemo alla votazione, bensì pigiando il bottone bianco, qualora si dovesse arrivare al voto elettronico. (Applausi della senatrice Poretti e del senatore Valentino).

\[iframe width="420" height="315" src="http://www.youtube.com/embed/4qqY20Tw32o" frameborder="0" allowfullscreen\]