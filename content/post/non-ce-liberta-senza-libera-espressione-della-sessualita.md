---
title: 'Non c''è libertà senza libera espressione della sessualità'
date: Tue, 15 Feb 2011 11:21:22 +0000
draft: false
tags: [Politica]
---

Emma Bonino, vicepresidente del Senato, rompe il silenzio delle istituzioni italiane sull’assassinio dell’attivista gay ugandese David Kato Kisule. L’intervista.

di Pasquale Quaranta, www.p40.it, 15 febbraio 2011

Meno di un mese fa l’attivista gay ugandese **David Kato Kisule** è stato brutalmente ucciso per il suo impegno nell’associazione Sexual Minorities Uganda. Era uno dei più importanti esponenti del movimento in difesa dei diritti delle persone omosessuali in Africa. Il 27 novembre 2010 avevamo raccolto il suo SOS durante un suo soggiorno a Roma, ospite dell’associazione radicale **Certi diritti**. E proprio questa associazione insieme alla Compagnia del Teatro dell’Elfo, ha promosso il 12 febbraio una commemorazione-ricordo al Teatro Valle di Roma.

Mentre venivano lette le parole di cordoglio del presidente degli Stati Uniti, Barack Obama, del segretario di Stato Hillary Clinton, del presidente del Parlamento europeo Jerzy Buzek e di altri ancora, era impossibile non pensare all’assenza di dichiarazioni da parte delle istituzioni italiane. A rompere questo silenzio al Teatro Valle c’è stato l’intervento di Emma Bonino, vicepresidente del Senato, che a partire dalla morte di David Kato ha parlato di omofobia, diritti e libertà.

_«Quando mi sono stabilita in Egitto nel 2001_ – ha raccontato Bonino – _era il periodo in cui erano stati arrestati e poi messi in galera e poi fatti sparire (non si riusciva nemmeno a sapere dove diavolo fossero) gli omosessuali che si ritrovavano in uno di quei barconi sul Nilo. Cominciavo a prendere contatti per capire dove fossero finiti. La stampa usava tutti gli stereotipi di questo mondo: “Non è un problema di libertà individuale, erano islamisti infiltrati che complottavano…”, insomma le solite cose»._

L’esponente radicale ha affrontato anche il tema dei pregiudizi che ha ritrovato tra persone che si occupano di diritti. _«Quello che mi colpì fu che gli amici egiziani con cui avevo contatti, coloro che si occupavano di stato di diritto, di democrazia, snobbavano un po’ il tema dei diritti delle persone omosessuali. Affermavano, strumentalmente, che non era ancora il momento per occuparsene. Non lo dicevano chiaramente ma era un messaggio implicito»._

Occuparsi di omosessuali, insomma, non era una cosa seria.  
**_«Un atteggiamento che ritrovo anche qui da noi. Democrazia sì, libertà sì, sesso un po’ meno. Se poi parliamo di gay e lesbiche, allora men che meno perché i tempi non sono ancora maturi; se poi sono transessuali, allora possiamo aspettare per generazioni!»_**.

L’ex ministro per il Commercio internazionale e per le politiche europee è intervenuta anche sulle tensioni politiche in Tunisia, Egitto e Algeria in riferimento a un principio di libertà universale.  
_«Comunque andrà a finire –_ ha continuato Bonino _– si farà piazza pulita dello stereotipo secondo il quale la libertà è un valore occidentale. La libertà, cioè il diritto di vivere come si vuole nei limiti della libertà degli altri, è un bisogno universale. Che sia l’Uganda, che sia il barcone dell’Egitto, il principio travalica qualunque pseudocultura, frontiera o barriera nazionale. Non esiste libertà che non passi anche attraverso la libera espressione della sessualità»._

Parlando infine del ruolo delle religioni in quanto strumenti di potente legittimazione dell’omofobia, ha dichiarato: «_**Quasi tutte le religioni sono omofobe. C’è un legame perverso che unisce le religioni ed è la condanna delle relazioni omosessuali**, una condanna che definirei tetragona, salda e irremovibile. Un esempio? Le gerarchie cattoliche e l’Islam: divisi su tutto ma uniti nella negazione della libertà di autodeterminazione del corpo e della sessualità individuale di ciascuno»._

Prima che Emma Bonino lasci il teatro, riusciamo a porle ancora qualche domanda.

**Da vicepresidente del Senato, perché questo silenzio assordante delle istituzioni sulla morte di David Kato?**  
Tutta la politica italiana è incentrata su altro. Credo che i miei colleghi trovino un po’ esotico occuparsi di un omicidio avvenuto in Africa. La centralità in Italia è così circoscritta a ciò che accade nel Paese. Penso persino che qualcuno abbia nel retropensiero: “Ma magari se l’è andata a cercare…”. Questi sono pregiudizi così difficili da superare…

**Perché in Italia non riusciamo a far approvare una legge contro l’omofobia?**  
Perché esiste una cultura politica convinta che essere eterosessuali sia la cosa “migliore”. E che i diritti delle persone omosessuali siano comunque di serie B. Secondo questo pensiero, tutte le persone dovrebbero adeguarsi a questa eterosessualità. Secondo me l’esigenza e la priorità di una legge contro l’omofobia non la sente nessuno.

**Qual è l’ostacolo più grande per la piena equiparazione dei diritti, quindi anche per il matrimonio tra persone dello stesso sesso e la genitorialità omosessuale?**  
Ci sono dei ruoli che vengono ipocritamente attribuiti all’uomo e alla donna a patto che siano entrambi eterosessuali. Credo semplicemente che l’ostacolo più grande sia quello di riconoscere che tutti i cittadini sono uguali di fronte alla legge. Nessuno escluso.

***Emma Bonino, vicepresidente del Senato, è iscritta a Certi Diritti.**