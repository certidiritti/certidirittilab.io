---
title: 'DIRITTI PROSTITUTE: SU RR L''AUDIOVIDEO DELLA MANIFESTAZIONE IN CAMPIDOGLIO'
date: Wed, 17 Sep 2008 05:43:50 +0000
draft: false
tags: [Senza categoria]
---

AL SEGUENTE LINK L'AUDIO-VIDEO DI RADIO RADICALE DELLA MANIFESTAZIONE IN DIFESA DEI DIRITTI DELLE PROSTITUTE:

L'Associazione Certi Diritti ringrazia la redazione di Radio Radicale per questo servizio pubblico:

[http://www.radioradicale.it/scheda/262350](http://www.radioradicale.it/scheda/262350)