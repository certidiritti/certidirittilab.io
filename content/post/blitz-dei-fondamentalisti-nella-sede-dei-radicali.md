---
title: 'BLITZ DEI FONDAMENTALISTI NELLA SEDE DEI RADICALI'
date: Thu, 12 Nov 2009 12:46:05 +0000
draft: false
tags: [Comunicati stampa]
---

BLITZ SEDE RADICALI: GRAVE ATTO DI INTOLLERANZA DEL FONDAMENTALISMO. OCCORRE CHE SI AFFERMI LA CIVILTA’ DELLA TOLLERANZA CONTRO QUESTE AZIONI DI SQUADRISMO.  RADICALI SI BATTONO ANCHE PER LIBERTA’ DI RELIGIONE.

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

“Il grave blitz vigliacco e anche un po’ patetico, avvenuto stamane ad opera di ignoti nella sede centrale dei radicali a Roma, dimostra della grave situazione di intolleranza ormai sempre più diffusa a Roma e nel paese. Offendere con accuse assurde e ridicole i radicali, inchiodando delle croci nel portone di ingresso e gettando nelle scale volantini con accuse ridicole, denota una grave ignoranza ispirata da idee fondamentaliste. Ci auguriamo che qualcuno spieghi loro che i radicali si battono per la libertà delle persone, non solo in Italia ma anche in molti paesi del mondo. Compresi quei paesi dove i regimi totalitari impediscono la professione della propria religione. Varrebbe la pena di ricordare, per tutti, i Montagnards vietnamiti perseguitati solo perché cristiani cui è impedito di radunarsi nei loro luoghi di culto. Da anni i Montagnards sono iscritti al Partito Radicale nonviolento, Transnazionale e Transpartito per la lotta che da sempre promuovono contro i regimi totalitari che negano anche la libertà di religione”.

ALLE ORE 10.47 DI QUESTA MATTINA PRESSO LA SEDE DEI RADICALI DI ROMA, IN  VIA DI TORRE ARGENTINA, 76 E’ AVVENUTO UN BLITZ FIRMATO DA UN SITO [WWW.LOTTASTUDENTESCA.NET](http://www.lottastudentesca.net/).

“Questa mattina presso la sede centrale dei Radicali è avvenuto un blitz di persone che dopo aver fatto scoppiare qualcosa hanno buttato per le scale alcuni volantini offensivi nei confronti dei radicali, con accuse varie.

Sul portone di ingresso, al terzo piano, sono stati ‘inchiodati’ due  pezzi di legno a forma di crocifisso e un altro simile poco distante sul muro laterale al portone di ingresso.

E’ stata immediatamente informata la Polizia di quanto avvenuto”.

Ufficio Stampa: Sergio Rovasio 337-798942