---
title: 'CERTI DIRITTI PARTECIPA AL FERRAGOSTO NELLE CARCERI ITALIANE'
date: Thu, 14 Aug 2008 11:10:40 +0000
draft: false
tags: [Senza categoria]
---

A FERRAGOSTO MOBILITAZIONE DI RADICALI ITALIANI NELLE CARCERI ITALIANE: DRAMMATICA LA SITUAZIONE DEL SOVRAFFOLLAMENTO CHE COINVOLGE OLTRE 50.000 DETENUTI

**Radicali Italiani ha promosso per domani, venerdì 15 agosto, in tutta Italia, una giornata di mobilitazione all'interno delle carceri italiane che vedrà coinvolti dirigenti, parlamentari e militanti radicali, insieme a deputati e consiglieri regionali del Pd e del Pdl . All'uscita da ogni carcere è prevista una Conferenza Stampa locale dove verranno date le informazioni sulle condizioni di detenzione dei carcerati.**

**A Roma, domani 15 agosto, alle ore 11.30, davanti al Carcere di Regina Coeli, Antonella Casu, Segretaria di Radicali Italiani, Rita Bernardini, deputata radicale – Pd e Sergio Rovasio, Segretario Associazione radicale Certi Diritti, incontreranno la stampa subito dopo la visita nel carcere.**

**Tra le carceri che verranno visitate domani, 15 agosto, segnaliamo:**

**Torino –** Mariangela Cotto, Consigliere regionale Pdl, Bruno Mellano, Presidente di Radicali Italiani e Iolanda Casigliani, Segretaria Associazione radicale Satyagraha;

**Alessandria** \- Marco Cappato, deputato europeo radicale e Gian Piero Buscaglia militante radicale;

**Cuneo** \- Enrico Costa, deputato Pdl , Michele De Lucia, Tesoriere di Radicali Italiani e Bruno Mellano Presidente di Radicali italiani;

**Cremona** \- Maurizio Turco, deputato radicale – Pd, Sergio Ravelli, Vice Presidente Comitato nazionale di Radicali Italiani e Ermanno De Rosa Presidente associazione radicale Piero Welby;

**Bolzano** \- Luisa Gnecchi deputata Pd, Donatella Travisan e Elena Dondio, militanti radicali;

**Bologna** \- Marco Beltrandi deputato radicale – Pd, Fabrizio Gambarini e Zeno Godetti, militanti radicali;

**Firenze** – Donatella Poretti, Senatore radicale – Pd, Antonio Bacchi e Claudia Sterzi, militanti radicali;

**Perugia** – Maria Antonietta Farina Coscioni, deputato radicale – Pd, Walter Vecellio, Pierfrancesco Pellegrino e Andrea Maori, militanti radicali;

**Roma Regina Coeli**, Rita Bernardini, deputato radicale – Pd, Antonella Casu, Segretaria di Radicali Italiani e Sergio Rovasio, Segretario Ass. Certi Diritti;

**Viterbo**, Maria Antonietta Farina Coscioni, deputato radicale Pd, Walter Vecellio e Prof. Osvaldo Ercoli, militanti radicali;

**Lecce** – Elisabetta Zamparutti, deputato radicale – Pd, Sergio D'Elia, Segretario Associazione Nessuno Tocchi Caino;

**Catania Bicocca** – Giuseppe Berretta deputato Pd e Gianmarco Ciccarelli, militante radicale;

**Sassari** – Guido Melis, deputato Pd, Irene Testa e Tiziana Marranci militanti radicali;

**Napoli** – Guglielmo Vaccaro, deputato Pd, Andrea Furgiuele, militante radicale;

**Palermo Pagliarelli** – Giuseppe Apprendi, deputato regionale Pd, Donatella Corleo, militante radicale;

**San Gimignano** – Gianni Cuperlo e Susanna Cenni, deputati Pd e Giulia Simi militante radicale;

**Milano San Vittore** – Giuseppe Benigni, consigliere regionale PD, Luigi Manconi, ex parlamentare e Alessandro Litta Modignani, militante radicale.