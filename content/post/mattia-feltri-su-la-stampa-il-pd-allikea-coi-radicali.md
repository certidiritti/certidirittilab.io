---
title: 'Mattia Feltri su La Stampa: il PD all''Ikea coi Radicali'
date: Fri, 06 May 2011 06:40:45 +0000
draft: false
tags: [Politica]
---

Su La Stampa di oggi, venerdì 6 maggio, pagina 11, rubrica 'Paesi e Buoi' di Mattia Feltri.

"Sempre suggestive le analisi politiche di Antonio Di Pietro, uno che preferisce la solidità della metafora contadina alla ipocrisia delle sottigliezze professorali.  
Ieri, intervistato sul nostro giornale, il leader dell'Italia dei Valori ha così tratteggiato le geografia sentimentale del centrosinistra: il Partito Democratico deve decidere se restare a casa dalla moglie con il mattarello che è l'IDV oppure sfarfalleggiare a giro con quella grandissima escort che è il Terzo Polo.

Proponiamo una terza ipotesi: all'Ikea coi Radicali".