---
title: 'Giovanardi non contento della brutta figura su Ikea...'
date: Mon, 25 Apr 2011 21:12:30 +0000
draft: false
tags: [Politica]
---

Il sottosegratrio continua nel gioco ambiguo delle offese e delle denigrazioni, ora contro le lobby  potenti!  
  
Roma, 24 aprile 2011  
Dichiarazione di SErgio Rovasio, SEgretario Associazione Radicale Certi Diritti  
  
Il (molto) Sottosegretario Carlo Giovanardi, che ancora non risponde a domande ben precise sul fatto che in Italia aumentano i divorzi, diminuiscono i matrimoni, e grazie al Governo cala in  pochi anni il fondo per gli interventi per le famiglie, che dal 2008 al 2013 passa addirittura da 346 a 31 milioni, continua ad esprimere giudizi  su presunte 'lobby potenti' che opererebbero per azzerare  la famiglia. Ormai preso da deliri persecutori, e da visioni catastrofiste, che gli fanno dichiarare che è in corso una campagna per 'azzerare' la 'famiglia prevista dalla Costituzione'  sostiene che occorre 'rispetto per ogni forma di orientamento sessuale e di solidarità per tutti coloro che sono oggetto di violenza fisica e verbale'. Insomma, prima dice una ovvietà del tutto pleonastica sulla violenza e poi attacca coloro che si battono per l'estensione dei diritti a chi non ne ha accusandoli di essere potenti lobby. Consigliamo a Giovanardi riposo e serenità in un qualche monastero, almeno  per tutta la durata della Legislatura, eviterà così di fare figuracce, come quella sull'Ikea, e di mettere il nostro paese ancora di più nel ridicolo di quanto già lo è, grazie ai vari Bunga Bunga dei suoi amici di Governo.