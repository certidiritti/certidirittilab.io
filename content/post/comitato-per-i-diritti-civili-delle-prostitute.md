---
title: 'COMITATO PER I DIRITTI CIVILI DELLE PROSTITUTE'
date: Fri, 05 Mar 2010 09:37:18 +0000
draft: false
tags: [Comunicati stampa]
---

Grande soddisfazione per la sentenza della Cassazione che ha condannato un cliente che si era rifiutato di pagare per il servizio sessuale ricevuto.

Non vi è dubbio infatti che pretendere di fare sesso senza pagare la prestazione equivale ad un atto di violenza sessuale.

Siamo convinte che quanto hanno deciso i giudici della Corte D'appello di Genova prima e confermato la Suprema Corte ora metta in evidenza che il contratto stipulato verbalmente fra una lavoratrice sessuale e il cliente va rispettato perché valido a tutti gli effetti.

Riteniamo che usare il reato di stupro sia adeguato in quanto non si tratta di "merce" non pagata come potrebbe essere in caso di furto al supermarket. Bensì di un servizio alla persona non retribuito e quindi "forzato", pertanto una violenza.

A nostro avviso la sentenza segna un passo avanti nel riconoscimento di una attività che se esercitata liberamente dovrebbe essere riconosciuta come un lavoro.

Anche il fatto che le lavoratrici si ribellino a questi soprusi e violenze facendo denuncia è molto positivo e dimostra che si aspettano di ottenere giustizia.

Non è raro infatti che avvengano episodi simili, ma in passato è sempre stato difficile convincere le donne a denunciarli alle forze dell'ordine, la nostra associazione sta costatando che sempre più spesso le colleghe ci chiedono di sporgere denuncia. Un atteggiamento che dimostra come sia aumentata la sensibilità verso i propri diritti e la volontà di vederli rispettati.

Pia Covre