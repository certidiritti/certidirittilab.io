---
title: 'Sos da Kampala'
date: Thu, 30 Dec 2010 14:02:23 +0000
draft: false
tags: [Comunicati stampa]
---

Intervista a Kato David Kisule, attivista gay ugandese costretto a vivere in clandestinità dopo che un giornale ha pubblicato in copertina la sua foto e il suo indirizzo per esporlo al pubblico ludibrio. Lui però non si arrende: “Resto in Uganda per combattere l’omofobia”.

di [Pasquale Quaranta](http://www.p40.it/autore/Pasquale+Quaranta/),

[Pride](http://www.p40.it/testata/Pride), gennaio 2011, p. 12

Il 2 ottobre 2010, il giornale ugandese _Rolling Stone_ ha pubblicato un articolo intitolato “Divulgate 100 foto dei leader gay ugandesi” in cui si leggevano i loro nomi, gli indirizzi di casa e una descrizione della loro vita privata. Le informazioni sono state ottenute attraverso vari siti web tra cui anche _Facebook_ e attraverso l’uso di telecamere nascoste sia in luoghi di ritrovo pubblici che in abitazioni private. Il _tabloid_ sostiene che gay e lesbiche vogliono “reclutare” entro il 2012, in una sorta di complotto internazionale, un milione di bambini sotto i 12 anni ritenuti facilmente convertibili all’omosessualità e alla bisessualità.

A denunciare l’accaduto numerose associazioni umanitarie internazionali e naturalmente il movimento glbt ugandese. Uno dei principali bersagli di questa incredibile campagna di dio è **Kato David Kisule** (nella foto), attivista gay ugandese il cui volto è stato pubblicato addirittura sulla copertina di _Rolling Stone._ L’associazione radicale _Certi Diritti_ lo ha invitato a Roma in occasione del suo ultimo congresso, ed è lì che l’abbiamo incontrato.

**Cosa sta succedendo in Uganda?**  
L’anno scorso i pastori evangelici degli Stati Uniti, in nome della protezione della famiglia tradizionale, hanno diffuso nel mio paese odio e omofobia. Ciò ha portato a una serie di conseguenze: dalle vessazioni agli arresti, fino ad arrivare all’attuale proposta di legge contro le persone omosessuali.

**Di cosa si tratta?**  
La legge in vigore prevede già la reclusione per chiunque commetta un reato “contro natura”. La proposta in discussione punta a inasprire ulteriormente le pene. Si era parlato addirittura di introdurre la pena di morte in alcuni casi, ma su questo punto il governo ha poi fatto marcia indietro. Tuttavia rimane in campo un grave peggioramento della situazione. Il governo vuole promuovere la criminalizzazione dell’omosessualità a tutti i livelli, fino a punire chi non denuncia le persone glbt o le aiuta in qualsiasi modo. Che l’omofobia più violenta sia già un fenomeno istituzionalizzato è del resto dimostrato dal fatto che le autorità non fanno assolutamente nulla per proteggere la nostra comunità contro chi la attacca.

**Che conseguenze ha avuto la pubblicazione delle foto sul giornale?**  
L’uscita del primo articolo ha causato l’aggressione di otto persone i cui nomi e foto erano stati pubblicati, mentre una donna è stata costretta ad abbandonare la propria casa dopo che i vicini l’avevano colpita con una raffica di pietre.

**Avete intentato una causa contro il giornale?**  
Sì, e la corte suprema dell’Uganda ha emesso il primo novembre 2010 un ordine provvisorio che intima a _Rolling Stone_ di cessare qualsiasi pubblicazione che identifichi con nomi, foto o altri dati rilevanti, persone riconosciute come gay e lesbiche. Abbiamo consegnato una lettera al giornale in cui abbiamo chiesto di non proseguire nella pubblicazione di ulteriori foto e dati personali come preannunciato, ma abbiamo ricevuto una risposta negativa da parte del direttore che ha confermato l’intenzione di pubblicare nuove foto e dati personali. Come in effetti poi è accaduto.

**Il 23 novembre 2010 avete avuto un’ulteriore udienza presso la corte. Com’è andata?**  
Alla fine dell’udienza del caso _Rolling Stone,_ io e altri siamo stati assaliti da fondamentalisti religiosi, personale del giornale ed “ex omosessuali” plagiati nel loro odio nei nostri confronti da _leader_ religiosi. Fortunatamente, grazie all’intervento di alcuni rappresentanti di associazioni per i diritti umani siamo riusciti a lasciare indenni l’edificio. Questo ha costretto alcuni di noi a vivere in clandestinità per il timore di nuovi attacchi.

**Anche tu vivi in clandestinità?**  
Sono costretto a vivere in luoghi diversi dove non c’è la televisione e dove non ci sono i giornali perché potrebbero riconoscermi. Anche le associazioni lgbt sono costrette a cambiare spesso sede perché potremmo avere dei problemi se ci trovano. Ma ho deciso di vivere in Uganda e di restarci per lottare contro questa omofobia in prima linea e per proteggere gay e lesbiche in Uganda.

Prima di salutarci chiedo a David se ha un compagno. “Non aspettano altro che incastrarmi… È molto difficile che io possa avere una relazione soddisfacente”. Lo invito a mostrarmi una copia del giornale _Rolling Stone_ per fotografarlo. Il suo volto è ora più sereno, ma negli occhi scorgo l’inquietudine per quanto ha raccontato. Gli chiedo cosa possiamo fare, noi italiani, per aiutarlo. “Chiedo a tutti voi di firmare una petizione per chiedere giustizia e scoraggiare il parlamento ugandese ad approvare la criminale proposta di legge contro le persone omosessuali, anche in considerazione del fatto che il ministro dell’etica e dell’integrità ha dichiarato che tale testo dovrà essere approvato prima che le Camere vengano sciolte, cioè prima di maggio 2011”. Per scrivere a David (in inglese): _[davidkisule@gmail.com](mailto:davidkisule@gmail.com)_