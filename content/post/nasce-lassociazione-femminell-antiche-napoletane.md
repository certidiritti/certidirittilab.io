---
title: 'NASCE L''ASSOCIAZIONE FEMMINELL ANTICHE NAPOLETANE'
date: Thu, 16 Apr 2009 16:17:42 +0000
draft: false
tags: [Comunicati stampa]
---

Si terrà sabato 18 aprile a Torre Annunziata l’incontro dal titolo **_O femmenell. Nel passato, nel presente: una realtà culturale_**.

Nel corso del pomeriggio sarà presentato lo statuto della costituenda **A.F.A.N. - Associazione Femmenell Antiche Napoletane** dal coordinatore dell’associazione **Luigi Di Cristo** insieme alla **Dott.ssa Cinzia Russo** (Presidente A.M.A.C.I.). All'iniziativa interverrà anche **Sergio Rovasio**, Segretario dell'Associazione Radicale Certi Diritti.

> _“La presentazione dello statuto A.F.A.N_ \- afferma Di Cristo - _è motivato dal progetto di costituzione di un centro di documentazione, raccolta e conservazione della storia del ‘femminiello napoletano’, dimenticato, ignorato, tenuto nascosto, nella sua unicità territoriale”_
> 
> Alcune delle foto che fanno parte del centro di documentazione saranno proiettate nel corso dell’incontro. Sarà inoltre proiettato il documentario _“Cerasella: ovvero l’estinzione della femminella”_ alla presenza del regista **Massimo Andrei** e dell’attore **Ciro Cascina**.
> 
> > _“La ‘comunità’ del femminiello_ \- conclude il coordinatore dell’A.F.A.N. - _ha continuato ad esistere, rinnovandosi e ponendosi spesso al centro delle più importanti manifestazioni culturali e religiose. Studiarne la storia è, studiare la storia sociale, la storia della filosofia, della religione, delle tradizioni popolari, della letteratura, dell’arte… in una parola, la storia della cultura locale”_
> 
> La serata si concluderà con pizziche e tamorre.
> 
> L’incontro, patrocinato dal Comune di Torre Annunziata, si svolgerà **dalle 18.30 presso la sede dell’A.M.A.C.I.** in via Mauro Morrone 15.