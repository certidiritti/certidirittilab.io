---
title: 'Certi Diritti: dove sta la novità nelle dichiarazioni di Bersani? I diritti delle unioni civili li aveva già ampiamente citati, ma anche in Italia è l''ora del matrimonio egualitario'
date: Thu, 07 Mar 2013 15:26:58 +0000
draft: false
tags: [Matrimonio egualitario]
---

Comunicato stampa dell'Associazione radicale Certi Diritti.

Roma, 7 marzo 2013

Ancora prima delle elezioni avevamo prefigurato un Parlamento con ampie maggioranze sui diritti lgbt ed anche sul matrimonio egualitario. Subito dopo le elezioni lo abbiamo ripetuto e, numeri alla mano, ne abbiamo anche avuto la conferma aritmetica visto che il grande risultato grillino ha, fino a prova contraria, dimostrato che quello che si poteva immaginare prima del 24 febbraio è accaduto. Ora Bersani inserisce tra gli otto punti sui quali chiede a Grillo di poter governare, tra cui, testuale : "Norme sulle unioni civili di coppie omosessuali secondo i principi della legge tedesca". Niente di più e niente di meno di quello che c'era nel programma di coalizione di Italia Giusta. Quindi davvero sono incomprensibili questi cori a favore della posizione di Bersani che, semplicemente, è coerente con le premesse. Ma le associazioni lgbt non erano tutte d'accordo nel dire che non bastava?

La novità sarebbe quindi, nell'aver inserito questo punto tra gli otto del programma dell'ipotetico Governo a tempo.   Questa, se non è l'inserimento di bandiera per un governo impraticabile, è l'unica vera novità. Ma rimane insufficiente e strategicamente errato. Non è un buon inizio, è un inizio sbagliato: perchè notoriamente nella trattativa che seguirà si dovrà giocare sul togliere e non sul mettere. O forse Bersani pensa che si entra in Parlamento con una proposta di riconoscimento di unioni civili e si esce con il matrimonio egualitario?

Attendiamo fiduciosi di conoscere le risposte degli altri partner di questa ipotesi di Governo e soprattutto di vedere i testi concreti delle proposte di legge.