---
title: 'BOFFO - INTERROGAZIONE RADICALE: CHIARIRE  SE POLIZIA SCHEDA OMOSESSUALI'
date: Mon, 31 Aug 2009 04:46:28 +0000
draft: false
tags: [Comunicati stampa]
---

**Interrogazione a risposta scritta ai Ministri della Giustizia e degli Interni dei Senatori Perduca e Poretti**

Premesso che   
  
\- in data 28 agosto 2009 sulla prima pagina del quotidiano Il Giornale è apparso un articolo a firma di Vittorio Feltri, direttore del quotidiano, ove si descrivono fatti di cui sarebbe stato protagonista Dino Boffo, direttore de L’Avvenire;  
  
\- a pagina 3 del giornale si riportano, virgolettate, quindi attribuite alla fonte, le seguenti parole: “… il Boffo, noto omosessuale già attenzionato dalla Polizia di Stato per questo genere di frequentazioni …”. Il virgolettato è da riferirsi, secondo Vittorio Feltri, alla nota informativa che accompagna il rinvio a giudizio, quindi  a documento prodotto dall’autorità giudiziaria del Tribunale di Terni;  
  
\- sui quotidiani del 30 agosto è riportata la notizia che il Ministro Maroni avrebbe verificato l’esistenza di informazioni sulla vita sessuale del direttore de l’Avvenire presso la Polizia di Stato e che lo lo stesso avrebbe telefonato personalmente a Dino Boffo per rassicurarlo sulla inesistenza di tali informazioni;  
  
si interrogano i Ministri della Giustizia e degli Interni per chiedere di:  
  
\- verificare l’attendibilità della citazione e l’originale della stessa;  
  
\- verificare cosa intendesse chi ha scritto la Nota informativa parlando di "già attenzionato alla Polizia di Stato per questo genere di frequentazioni";  
  
\- verificare in quale modo e per quale motivo la Polizia di Stato sarebbe stata a conoscenza di questo particolare della vita privata del dottor Boffo;  
  
\- verificare se presso la Polizia di Stato, l’Arma dei Carabinieri o altre Forze di Polizia o presso l’Autorità giudiziaria vengano, sotto qualsiasi forma e per qualsiasi ragione, conservate informazioni in merito all’orientamento e all’identità sessuale di persone sottoposte a indagine e non;  
  
\- intervenire presso le forze di polizia per assicurarsi che la legge sia sempre rispettata, sia nel caso di  personalità note come Dino Boffo che nel caso di cittadini e cittadine italiane che note non sono.

**Dichiarazione di Marco Perduca, Senatore radicale eletto nelle liste PD, e Enzo Cucco dell'Associazione radicale Certi Diritti**  
  
"Tra le pieghe della sanguinosa lotta di potere fra una parte delle forze di Governo e il Vaticano, su chi esercita la maggiore influenza sul Presidente del Consiglio, la cui ultima puntata è stata scritta da Vittorio Feltri sulle colonne de il Giornale, c’è una questione, piccola all’apparenza, ma di grande rilievo per la vita di tutti gli italiani e le italiane, e non solo delle persone omosessuali.  
  
Feltri riporta tra virgolette una frase tratta dalla Nota informativa allegata alla richiesta di rinvio a giudizio di Dono Boffo del GIP del Tribunale di Terni, nella quale si dice che “il Boffo, noto omosessuale già attenzionato dalla Polizia di Stato per questo genere di frequentazioni … “  
  
La domanda sorge spontanea: la Polizia di Stato scheda gli omosessuali? Tiene memoria delle abitudini di vita, dell’orientamento sessuale, e di ogni altra informazione cosiddetta personale? Che significa esattamente “attenzionato”?  
  
La domanda è rivolta sia al Ministro della Giustizia che al Ministro degli Interni, ma interessa anche, e direttamente, il Garante della privacy che nel passato, con esemplare solerzia, era intervenuto praticamente in tempo reale per stigmatizzare quegli organi di stampa che utilizzavano le registrazioni telefoniche.  
  
Speriamo che i Ministri interpellati e il garante per la protezione dei dati personali offrano presto esaurienti risposte alle domande della interrogazione parlamentare.