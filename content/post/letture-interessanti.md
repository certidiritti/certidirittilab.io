---
title: 'Letture interessanti'
date: Sun, 08 Nov 2009 14:35:40 +0000
draft: false
tags: [Senza categoria]
---

**Le unioni tra persone dello stesso sesso Profili di diritto civile, comunitario e comparato a cura di Francesco Bilotta**

![cover-unioni2.jpg](http://www.certidiritti.org/wp-content/uploads/2008/10/cover-unioni2.jpg "cover-unioni2.jpg")

Le famiglie formate da persone dello stesso sesso pongono al diritto numerose questioni. Per la prima volta in maniera organica esse vengono affrontate sia dal punto di vista del diritto italiano, sia dal punto di vista di sistemi giuridici stranieri. Di particolare interesse sono le proposte di interpretazione delle norme interne alla luce dei principi del diritto comunitario. Obiettivo del volume è indagare la percorribilità di una strada giudiziaria per rivendicare i diritti delle coppie omosessuali.

Scritti di: Vittoria Barsotti, Chiara Bertone, Francesco Bilotta, Stefano Bolognini, Matteo Bonini Baraldi, Francesco Dal Canto, Giovanni Dall'Orto, Bruno de Filippis, Chiara Lalli, Morris Montalti, Lara Olivetti, Carmela Simona Pastore, Antonio Rotelli, Alexander Schuster.

Francesco Bilotta è ricercatore di diritto privato nell'Università di Udine e avvocato in Trieste. Autore di numerosi saggi in materia di diritti delle persone, diritti dei consumatori, responsabilità civile, questioni legate al mondo LGBT. Ha collaborato alla stesura della proposta di legge sul Patto civile di solidarità e unioni di fatto presentata nella XIV Legislatura. é socio fondatore dell'Associazione Avvocatura per i diritti LGB

* * *

**[**_![immagine_citizen_gay](http://www.certidiritti.org/wp-content/uploads/2009/11/immagine_citizen_gay.gif)_**](http://bioetiche.blogspot.com/2008/01/intervista-vittorio-lingiardi-su.html)**

Un libro da non perdere per chi si batte per i diritti delle persone LGBT(E): "Citizen gay. Famiglie, diritti negati e salute mentale" di Vittorio Lingiardi. [**Intervista a Vittorio Lingiardi su** _**Citizen gay**_](http://bioetiche.blogspot.com/2008/01/intervista-vittorio-lingiardi-su.html)

Vittorio Lingiardi è psichiatra, psicoanalista, docente della Facoltà di Psicologia 1 della "Sapienza". Ha  pubblicato un libro dal titolo [**Citizen Gay. Famiglie, diritti negati e salute mentale**](http://www.saggiatore.it/index.php?page=boo.detail&id=bk070148) (Il Saggiatore, Milano). Già nel titolo emergono alcuni temi centrali della sua riflessione... [**Leggi tutta l'intevista**](http://bioetiche.blogspot.com/2008/01/intervista-vittorio-lingiardi-su.html)

[http://bioetiche.blogspot.com/2008/01/intervista-vittorio-lingiardi-su.html](http://bioetiche.blogspot.com/2008/01/intervista-vittorio-lingiardi-su.html)

* * *

![libro_amore_civile](http://www.certidiritti.org/wp-content/uploads/2009/11/libro_amore_civile.jpg)**Amore Civile, dal Diritto della tradizione  
al Diritto della ragione**

a cura di Bruno de Filippis e Francesco Bilotta  
[MIMESIS](http://it-it.facebook.com/note_redirect.php?note_id=201141901336&h=bc3ac5de0d1abb0ba4058f086fb37b8a&url=http%3A%2F%2Fwww.mimesisedizioni.it%2Farchives%2F001130.html "http://www.mimesisedizioni.it/archives/001130.html") collana Sx  
Quaderni Loris Fortuna.  
Questo volume raccoglie il lavoro svolto in quasi due anni da studiosi, esperti e rappresentanti di associazioni, impegnati in un tavolo di lavoro permanente, ove le diverse competenze (diritto civile, penale, comparato ed internazionale, di medicina e genetica, di filosofia, psicologia e sociolgia) potessero aver modo di lavorare insieme per redigere un Progetto di Riforma globale del Diritto di Famiglia. Tra gli obiettivi vi è quello di proporre alla classe politica una riforma complessiva raccolta nel libro Amore Civile . Idea ispiratrice del lavoro è il concetto di laicità dello Stato.  
Interventi, tra gli altri, di Emma Bonino, Bruno De Filippis, Francesco Bilotta, Marco Pannella, Marco Cappato, Sergio Rovasio, Diego Galli, Guido Allegrezza, Diego Sabatinelli...  
Dal 7 novembre 2009 Amore Civile è disponibile in tutte le LIBRERIE, e ON-LINE nei principali siti di vendita di libri, anche in offerta on line direttamente alla [casa editrice](http://it-it.facebook.com/note_redirect.php?note_id=201141901336&h=bc3ac5de0d1abb0ba4058f086fb37b8a&url=http%3A%2F%2Fwww.mimesisedizioni.it%2Farchives%2F001130.html "http://www.mimesisedizioni.it/archives/001130.html").

* * *

**La Famiglia Fantasma: DiCo, PACS e matrimoni omosessuali, la politica italiana in crisi**

di Gian Mario Felicetti

_Milioni di Italiani sono_ **_fantasmi:_**_  
senza storia  
senza legge    
senza futuro_

La Repubblica Italiana si ostina a negare il riconoscimento istituzionale a milioni di coppie, obbligate a non sposarsi, come ai tempi delle leggi razziali.

**RISCATTARE LA PROPRIA STORIA** – Questa condizione di subalternità è dannosa per tutta la società civile perché le famiglie e gli affetti omosessuali sono fondamentali per l’identità culturale di ogni popolo. L’Italia omofoba è un albero sofferente dalle radici recise.

[**_![GAY_quadrati_verticale](http://www.certidiritti.org/wp-content/uploads/2009/11/GAY_quadrati_verticale.png)_**](http://bioetiche.blogspot.com/2008/01/intervista-vittorio-lingiardi-su.html)

**OTTENERE IL RICONOSCIMENTO DELLA LEGGE** \- Per essere riconosciuti dalla legge, bisogna conoscere la legge e avere fiducia nelle istituzioni. Così diventa facile confutare i più radicati luoghi comuni: la Costituzione Italiana definisce il matrimonio come unione tra coniugi e non vieta in nessun modo il matrimonio tra persone dello stesso sesso.

**CONQUISTARE IL PROPRIO FUTURO** – Il libro aiuta a costruirsi un proprio lessico, necessario per aprirsi con serenità e onestà intellettuale al dibattito sul diritto alla paternità e maternità da parte delle persone omosessuali: una lecita dimensione affettiva, totalmente nuova ed epocale per gay e lesbiche di tutto il mondo.

_Il libro si può acquistare online e può essere ordinato in tutte le librerie._

_

* * *

_  

[![](http://www.beitcasaeditrice.it/images/copertine%20x%20recnsioni/Cover-Felice-cons.jpg)](http://www.beitcasaeditrice.it/images/CoverFelice.jpg)

**La breve vita dell’ebrea Felice Schragenheim**  
_Erica Fischer_  
208 pp. 225 x 275 mm. Numerose illustrazioni nel testo, euro 32,00.  
Traduzione di Daniela Zuffellato.  
ISBN: 978-88-95324-08-1

Felice Schragenheim (Berlino 1922 – Bergen-Belsen 1945), giovane scrittrice e giornalista ebrea, affronta la vita e l’amore con un coraggio e con una lievità che ancora oggi ci sorprendono: si sente protagonista della commedia dell’esistenza e non si lascia intimorire né dalla dittatura nazista né dal pregiudizio benpensante. Sarà proprio questa sua sincerità che la porterà ad affrontare in prima persona, senza risparmiarsi, la tragedia del suo tempo. Questo volume raccoglie, nelle immagini e nelle poesie di Felice e del suo mondo, una testimonianza indimenticabile sulla sua vita e sulla sua vicenda, che ha ispirato il romanzo di Erica Fischer _Aimée & Jaguar_.

* * *

![abbabusiness](http://www.certidiritti.org/wp-content/uploads/2009/11/abbabusiness.jpg)Generazione _abba_. Nelle imprese d'eccellenza - in Europa e nel mondo intero – sta uscendo allo scoperto la comunità GLBT – acronimo americano che definisce da vent'anni gay, lesbiche, transessuali e transgender.

Al bando la clandestinità di un tempo, questi lavoratori _abba_ fanno sempre più spesso coming out in azienda, dimostrando oggi apertamente, nella quotidianità del lavoro, un atteggiamento positivo e collaborativo e un pensare differente. Diventano così alleati di chi persegue il cambiamento, vero mantra per uscire dal pantano di una crisi tanto reale quanto percepita.

Così – in barba all'arretratezza di pensiero della società e spesso delle politiche sociali - si scopre nei fatti un nuovo profilo delle organizzazioni, tolleranti verso le differenze di genere, intese come arricchimento e creazione di valore. Per la propria cultura di impresa e - soprattutto – per il business.

_Mamma mia!_ Ecco il lato b delle imprese, che si rivela nei migliori ambienti di lavoro dove le persone sentono di poter essere autentiche, raccontandosi e mettendosi in gioco.

_Abbabusiness_ è un tratto identitario delle persone GLBT, che hanno molto spesso anticipato mode, tendenze, estetiche. Ma è anche il requisito di tutta quella parte della popolazione aziendale “etero” protagonista di una visione interculturale, interessata ad un approccio positivo verso tutto ciò che è multietnico, interreligioso e – ovviamente – multigenere.

In questo saggio gli autori - due esperti di management e cultura di impresa – inquadrano il fenomeno e spiegano perché le organizzazioni più competitive scelgono di accogliere e di rispettare i collaboratori di tutti gli orientamenti sessuali: un approccio interessato alle diversità genera valore per l’azienda che ha l’urgenza di cambiare ed è sintomo di una cultura fertile e aperta.

Abbabusiness è una fotografia dell'impresa moderna. Ed è anche un invito che i due autori rivolgono con forza a ciascun collaboratore: “Fai del tuo business un _abbabusiness_. Esci allo scoperto e colora la tua azienda”.

_Scopri l'abbabusiness anche online, su_ [www.abbabusiness.it](http://www.abbabusiness.it/)

Il testo è stato scritto da Giampaolo Colletti e Andrea Notarnicola.

**_Giampaolo Colletti_** _è un trentenne con la passione per le nuove tecnologie applicate alla cultura di impresa. Si occupa di community digitali e comunicazione interna integrata._

_Dall'età di ventidue anni lavora in aziende d'eccellenza. E' co-Founder dell'Osservatorio sull'Enteriprise Generated Content dell'Università Bocconi, network che coinvolge le maggiori imprese italiane che studiano i nuovi trend della comunicazione digitale aziendale. Ha scritto e condotto format tv per Rai Educational e per alcuni circuiti privati nazionali. Giornalista pubblicista, ha scritto per .Com, Italia Oggi e Sole24Ore nell'inserto sul mondo del lavoro Job24. Attualmente scrive per Nòva24 e collabora con Wired. E' Founder della prima wikipedia sulle nuove forme di micro web tv Altratv.tv. Pescarese di nascita e milanese di adozione, da due anni vive in un ridente paesello nel cuore della Romagna, a pochi chilometri dal mare._

**_Andrea Notarnicola_**_, partner di Newton Management Innovation - Gruppo 24 ORE, è consulente di direzione di primarie imprese globali caratterizzate da intense diversità culturali, etniche, religiose, di genere e orientamento sessuale. Formatore e coordinatore dei format di MASTER 24, si occupa di innovazione nei linguaggi d’impresa attraverso il teatro e i linguaggi tv e di edutainment (education+entertainment). Ha scritto il primo volume sulla corporate tv in Italia, uno dei primi volumi sul coaching e un manuale per la formazione dei leader del cambiamento. E’ stato uno dei promotori del Manifesto dello Humanistic Management. E’ editorialista de L’IMPRESA – Rivista italiana di management e membro del collettivo Jack O. Selz, autore di “Zzzoot – Fulminati in azienda”. E’ stato autore e conduttore di programmi educational della RAI e coordinatore dell’area sviluppo manageriale del MIB School of Management._

_Entrambi gli autori hanno scoperto l'abbabusiness. E ve lo vogliono raccontare._

* * *

![image002](http://www.certidiritti.org/wp-content/uploads/2009/11/image002.jpg)**La fa****miglia de-genere**

**un libro di Matteo Bonini Baraldi**

**su matrimonio, omosessualità e Costituzione**

Mimesis Edizioni, 2010

Collana LGBT, a cura di Francesco Bilotta

172 pagine, € 15,00

ISBN 978-88-5750-072-0

**Dalla quarta di copertina**. Nel corso del 2009 varie coppie di persone dello stesso sesso hanno deciso di richiedere al proprio Comune di residenza le pubblicazioni matrimoniali. In seguito al rigetto della richiesta, i protagonisti di questa vicenda hanno perseguito le vie legali per affermare il loro diritto a contrarre matrimonio civile. Piuttosto che avallare definizioni essenzialiste del concetto di famiglia, il volume analizza il quadro giuridico italiano e straniero in un'ottica innovativa, chiedendosi se l'esclusione dal matrimonio possa essere giustificata da un interesse pubblico preminente. Ne emerge come, lungi dal costituire una questione rilevante solo per una scomoda minoranza, in termini di rispetto delle identità, la questione del matrimonio tra persone dello stesso sesso è problema che riguarda tutti, cittadini e non cittadini, uomini e donne, eterosessuali e omosessuali, perché interroga il concetto di persona e di libertà, la definizione dei ruoli di genere in una famiglia che cambia, il rapporto fra norma e corpo, il significato del principio di eguaglianza in una moderna democrazia.

Pubblicate i vostri commenti sul blog: http://lafamigliadegenere.wordpress.com/

Per i vostri ordini: http://www.mimesisedizioni.it/archives/001313.html#001313

Per contatti: famigliadegenere@gmail.com

* * *