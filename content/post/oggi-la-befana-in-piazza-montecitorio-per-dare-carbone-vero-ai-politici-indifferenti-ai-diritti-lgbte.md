---
title: 'Oggi la Befana in Piazza Montecitorio per dare carbone (vero) ai politici indifferenti ai diritti Lgbt(e)'
date: Thu, 05 Jan 2012 11:48:28 +0000
draft: false
tags: [Politica]
---

Iniziativa promossa da Certi Diritti insieme ad altre associazioni. Di seguito la lettera che verrà letta dalla befana.

Roma, 5 gennaio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti e Radicali Italiani

Questa mattina, 5 gennaio,  alle ore 11,30, la Befana, accompagnata da una delegazione dell’Associazione Radicale Certi Diritti e di Radicali Italiani, sarà a Roma, in Piazza Montecitorio, per consegnare un carico di carbone (vero) ai parlamentari italiani. Mentre nel resto d’Europa vengono ovunque approvate leggi di Riforma sul Diritto di Famiglia, in Italia si continua a far finta di niente e a sbandierare demagogicamente ‘la famiglia del Mulino Bianco’ tanto cara ai politici clerical-fondamentalisti.  
In Parlamento giacciono decine di proposte e disegni di legge, anche dei parlamentari Radicali, senza che nessuno di questi venga calendarizzato.

La Befana, oltre a consegnare il carbone ai parlamentari  leggerà il testo di una lettera-appello inviata lo scorso novembre al Presidente del Consiglio Mario Monti, nella quale diverse Associazioni, che si battono per la promozione dei diritti civili in Italia, chiedono che il Governo consideri prioritari anche i temi della Riforma del Diritto di Famiglia; tra gli altri, quelli riguardanti i temi del riconoscimento delle unioni civili,  la separazione, la mediazione familiare, il divorzio breve, il matrimonio tra persone dello stesso sesso, le norme sulla procreazione e sulla genitorialità responsabile, le norme in materia di filiazione legittima e naturale, nuove norme contro la violenza ed i maltrattamenti in famiglia.

Qui di seguito il testo della lettera che verrà letto oggi dalla Befana:

Signor Presidente del Consiglio,

il Governo al quale lei sta lavorando avrà un compito immenso, e urgente, per il nostro Paese: riportare l’Italia agli standard sociali, culturali e normativi, non solo economici, comuni ai paesi dell’Unione Europea.

Come Lei ben saprà la Strategia 2020 adottata nel giugno 2010 dal Consiglio Europeo  ha fissato nell’interdipendenza tra politiche di sviluppo e politiche per l’inclusione sociale uno dei cardini delle strategie di sviluppo europeo, individuando tra gli interventi prioritari della Piattaforma contro l’emarginazione la lotta contro le discriminazioni. Operare contro ogni forma di discriminazione, quindi, non è solo obiettivo di giustizia e di uguaglianza, ma condizione per lo sviluppo.

Tradurre questi principi in interventi normativi e politiche specifiche è un compito arduo per chi si accinge ad operare in un Paese, come il nostro, ove il velo dell’ideologia nasconde una realtà  in trasformazione al pari con il resto dell’Europa. In particolare riteniamo che negli ultimi anni il pregiudizio ideologico di una parte della nostra classe dirigente si sia accanito contro le famiglie, in particolare contro la responsabilità e la libertà su cui si sono costruite nuove forme di famiglia in Italia al pari che nel resto dell’Europa.

Le segnaliamo quindi, un progetto integrato e completo di Riforma del Diritto di famiglia che comprende specifici progetti di intervento su:

 -      la regolamentazione delle coppie di fatto;

 -      la separazione, la mediazione familiare;

 -      il divorzio breve;

 -      il matrimonio tra persone dello stesso sesso;

 -      le norme sulla procreazione e sulla genitorialità responsabile;

 -      la norme in materia di filiazione legittima e naturale;

 -      nuove norme contro la violenza ed i maltrattamenti in famiglia

Noi siamo convinti che l’Italia ha bisogno urgentemente di un intervento normativo, e crediamo che il Suo Governo possa e debba contribuire a colmare il divario tra noi e il resto dell’Europa su queste materie.

Le segnaliamo il testo di una Proposta di Legge che raccoglie l’insieme del progetto di Riforma del Diritto di Famiglia ( n. 3067, prima firmataria l’On. Rita Bernardini, "Modifiche al codice civile in materia di testamento biologico, di disciplina del diritto di famiglia e della fecondazione assistita, al codice penale in materia di omicidio del consenziente e di atti di violenza o di persecuzione psicologica all'interno della famiglia, nonché al codice di procedura civile in materia di disciplina della domanda di divorzio").

Siamo certi che La sua sensibilità e La sua attenzione ai temi dei diritti civili saranno considerati prioritari per il suo Governo.

Cordiali saluti,

Associazione Radicale Certi Diritti

Associazione Luca Coscioni

 Associazione Divorzio Breve

Equality Italia

Arcigay

Radicali Italiani

Famiglie Arcobaleno

Associazione GayLib

Agedo (Associazione dei genitori di persone omosessuali)

GayNet (Associazione giornalisti gay)

Fondazione Massimo Consoli

Associazione Nuova Proposta, donne e uomini omosessuali cristiani

Gaynews.it (quotidiano di informazione sull'omosessualità)