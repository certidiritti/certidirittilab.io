---
title: 'Omofobia, Scalfarotto, Cancellieri, unioni civili, Madonna e i ricchioni'
date: Sat, 25 Aug 2012 13:57:36 +0000
draft: false
tags: [Politica]
---

![LogoCD](http://www.radicalparty.org/file/Logo_Cd_2012.JPG)

In due giorni in Italia: violenta aggressione all’attivista lgbt Guido Allegrezza e a una coppia di lesbiche a Velletri, tentato stupro a un ragazzo, botte a una transessuale serba ad Ardea, insulti a un gruppo di ragazzi da parte dei vigili urbani.  
**La solidarietà non basta più, la politica faccia la sua parte: necessarie iniziative concrete e campagna di informazione**.

[**Guido Allegrezza aggredito >**](http://www.certidiritti.org/2012/06/15/violenta-aggressione-a-guido-allegrezza-fatto-gravissimo-occorre-agire-subito-con-campagne-concrete/)

[**Se il vigile ti chiama ‘ricchione’. Ecco cosa è successo a Milano dopo il concerto di Milano >**](http://www.certidiritti.org/2012/06/15/se-il-vigile-urbano-ti-chiama-ricchione/)

Elezioni e matrimonio tra persone stesso sesso: finalmente se ne discute all'interno dei partiti, speriamo con azioni conseguenti. **La posizione di dialogo di Ivan Scalfarotto va sostenuta perchè chiara e concreta**. Si tenga conto di quanto l'Unione Europea chiede ai paesi membri.

Dopo il [**Pride nazionale di Bologna**](http://www.certidiritti.org/2012/06/09/certi-diritti-al-pride-nazionale-di-bologna-con-radicali-italiani-nessuno-tocchi-caino-e-associazione-luca-coscioni/), l’associazione radicale Certi Diritti questo week end partecipa al [**Torino Pride**](http://www.torinopride.it/) e al [**Pride Park di Roma**](http://www.wherevent.com/detail/Certi-Diritti-CERTI-DIRITTI-AL-PRIDE-PARK-DI-ROMA), in attesa della grande parata del 23 giugno per le strade della capitale. 

In questi giorni è partita la raccolta delle firme a sostegno di una proposta di legge di iniziativa popolare in materia di riconoscimento di unioni civili, patti di solidarietà e unioni di fatto. Certi Dirittie tante altre associazioni non aderiscono all’iniziativa.  
[**Qui ti spieghiamo perché >>>**](http://www.certidiritti.org/2012/06/13/riflessione-comune-sulla-recente-proposta-di-iniziativa-di-legge-popolare/)

[**L’associazione radicale Certi Diritti ha incontrato la ministra Cancellieri**](http://www.certidiritti.org/2012/06/15/associazione-radicale-certi-diritti-incontra-ministro-degli-interni-su-circolare-amato-a-breve-la-risposta/) sul ritiro della circolare Amato che vieta la trascrizione dei matrimoni gay contratti all’estero.

**[L'impegno contro l'omofobia e transfobia in Europa](http://www.radioradicale.it/scheda/354501/limpegno-contro-lomofobia-e-transfobia-in-europa): **ascolta la conferenza organizzata in Senato da Amnesty Italia e Associazione Radicale Certi Diritti. Tra gli interventi anche quello commovente del padre di Daniel, giovane ragazzo gay massacrato in Cile lo scorso marzo.

[No all'omofobia, sì ai diritti umani delle persone lgbti in Italia:](http://www.certidiritti.org/2012/06/09/no-allomofobia-si-ai-diritti-umani-delle-persone-lgbti-in-italia/) Certi Diritti sottoscrive l’appello di**Amnesty International Italia**. [Firma anche tu >](http://www.certidiritti.org/2012/06/09/no-allomofobia-si-ai-diritti-umani-delle-persone-lgbti-in-italia/)

[**Alessandro Baracchini, il coming out del giornalista Rai in diretta >**](http://www.corriere.it/cronache/12_giugno_14/coming-out-giornalista-rai_79e7f2b6-b64c-11e1-a717-30326103327c.shtml)

**Liberi.tv Spazio Certi Diritti: [Genitori come gli altri](http://www.liberi.tv/webtv/2012/06/08/video/genitori-come-gli-altri-conversazione-con-cecilia-davos-vice) \- Conversazione con Cecilia D'Avos Vice Presidente Rete Genitori Rainbow**

**Fuor di pagina - la rassegna stampa di Certi Diritti ora è disponibile anche in podcast su [iTunes](http://itunes.apple.com/it/podcast/radioradicale-fuor-di-pagina/id530466683?mt=2.). [Ascolta su radio radicale>](http://www.radioradicale.it/rubrica/1004)**

**[Firma il manifesto per la legalizzazione della prostituzione >](component/k2/itemlist/category/85)**

**[Dai la possibilità all’associazione radicale Certi Diritti di continuare le battaglie per idiritti civili e contro la sessuofobia. Scopri come fare >](partecipa/iscriviti)**

[**www.certidiritti.org**](http://http./www.certidiritti.org)