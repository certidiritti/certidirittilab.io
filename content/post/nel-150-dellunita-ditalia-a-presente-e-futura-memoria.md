---
title: 'Nel 150° dell''Unità d''Italia, a presente e futura memoria...'
date: Tue, 15 Mar 2011 13:41:47 +0000
draft: false
tags: [150, 20 settembre, anticlericale, bruno, cavour, chiesa, Comunicati stampa, ITALIA, LAICITA', RADICALI, risorgimento, STORIA, uaar, unita]
---

a cura di Sergio Rovasio  
  
Per ricordare il 150° anniversario dell’Unità d’Italia in questi giorni assisteremo a cerimoniose valangate di ipocrisia, riti e liturgie con echi di retorica e frasi fatte, mischiate a falsità e alterazioni della realtà che metteranno in secondo piano il degrado e le difficoltà in cui si trova l’Italia del 2011. Verranno nascoste e si negheranno tutte le malefatte e ruberie del sistema partitocratico che negli ultimi decenni, con la commistione di privilegi di cui beneficia il Vaticano e le gerarchie della chiesa, alimentano la negazione di libertà, diritti e progresso al nostro paese.  Tutto ciò è molto bene documentato dal libro giallo 'La peste italiana' curato dai radicali e scaricabile dal sito www.radicali.it  
  
Riteniamo utile ricordare qui, a presente e futura memoria, **l’intervento che fece Camillo Benso Conte di Cavour nel marzo del 1861, nel Parlamento Subalpino, in occasione di un dibattito parlamentare sulla elezione di alcuni preti avvenuta approfittando delle canoniche, delle sacrestie e delle prediche domenicali**. Le elezioni in alcuni collegi furono rifatte e i clericali vennero sconfitti.  
  
_"Ma quando il clero, riconquistata ed assicurata la libertà, vuol combattere per riacquistare gli antichi privilegi, per far tornare indietro la società, per impedire il normale e regolare sviluppo della civiltà moderna, allora è da deplorare il suo intervento nelle lotte politiche (...). Io ho troppa fede nel principio del progresso e della libertà per temere che possa essere posto a cimento in una lotta condotta con armi puramente legali. Se la libertà ha potuto fare dei progressi immensi quando aveva a lottare contro il clero e le classi privilegiate, e la libertà era in certo modo inerme, come mai potrei temere che ora dessa potesse correre vero pericolo se avesse a combattere i suoi avversari ad armi uguali? (…) Ma se io non temo le lotte politiche, quando siano combattute con armi legali, non posso dire altrettanto, ove il clero potesse impunemente valersi delle armi spirituali di cui è investito per ben altri uffizi che per trionfare questo o quell’altro politico candidato. Oh! Allora veramente la lotta non sarebbe più uguale; ed ove si lasciasse in questo terreno pigliare piede e assoldarsi l’uso di queste armi spirituali, la società correrebbe i più gravi pericoli, la lotta da legale correrebbe rischio di trasformarsi in lotta materiale"._  
  
Conte Camillo Benso di Cavour  
  
dal libro: “Il Conte di Cavour” (ricordi biografici)  di Giuseppe Massari – Torino 1872 ed. A.Barion