---
title: 'Vandalismo/La Croce: Certi Diritti condanna ancora'
date: Tue, 20 Jan 2015 09:55:51 +0000
draft: false
tags: [Politica]
---

[![10410088_10152972141455428_4568956473616424897_n](http://www.certidiritti.org/wp-content/uploads/2015/01/10410088_10152972141455428_4568956473616424897_n-300x300.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/01/10410088_10152972141455428_4568956473616424897_n.jpg)"Dalle agenzie di stampa apprendiamo di un altro atto vandalico di cui è stato vittima il giornale La Croce, questa volta. All'odio e all'intolleranza non si risponde mai con il vandalismo, ma con l'amore è il dialogo non violento per informare e convincere. Questo l'insegnamento di Martin Luther King di cui tra l'altro ieri si celebrava l'anniversario della nascita. Condanniamo, senza se e senza ma, chiunque sia stato ed esprimiamo solidarietà alla redazione di La Croce”. Così Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti.

Comunicato stampa dell'Associazione Radicale Certi Diritti