---
title: 'Aids: in Etiopia contagi in diminuzione grazie al preservativo'
date: Sat, 12 Mar 2011 12:16:38 +0000
draft: false
tags: [AFRICA, aids, certi diritti, Comunicati stampa, condom, PAPA, PRESERVATIVO, RADICALI]
---

I fondamentalisti che diffondo false informazioni sulla sua inefficacia smettano di alimentare l'ignoranza.  
  
Roma, 11 marzo 2011  
  
Comunicato Stampa dell’Associazione Radicale Certi Diritti

Secondo quanto reso pubblico da 'African Voice' l’Etiopia ha avviato una straordinaria campagna per abbattere la diffusione del virus dell'Aids **quadruplicando la distribuzione di preservativi e prolungando le cure gratuite per i malati**.  Un programma non facile, ma possibile: basti pensare che secondo i dati del ministero della Salute nel 2001, quando si e' dato il via al primo piano anti-Aids, **la popolazione affetta dal virus era circa il 25 per cento, soprattutto giovani. Oggi la media e' calata sotto il 3 per cento** (si tratta di circa 1,2 milioni di etiopi), fermandosi a un 2,4 nei centri urbani (dove il livello di formazione e istruzione e' piu' alto) e raggiungendo un 7,7 nelle zone rurali (dove ignoranza e credenze popolari frenano bruscamente cure e prevenzione).

L'obiettivo del governo ora e' intensificare la strada intrapresa arrivando a abbattere la soglia dei malati all'1 per cento. Per riuscirci il governo dell'Etiopia ha previsto un aumento della distribuzione dei farmaci, in modo da estendere il trattamento dall'attuale 60 per cento della popolazione, all'85, coinvolgendo cosi' altri 400mila malati. Poi, per far in modo che il trattamento sia realmente efficace e' prevista la creazione di ulteriori 3mila centri specializzati nella cura dell'Aids da spalmare sull'intero territorio. **Un ulteriore fronte su cui agire e' quello della distribuzione gratuita dei preservativi**: si intende passare dagli attuali 97 milioni a 400 milioni all'anno. Oltre alle cure, il governo ritiene indispensabile agire soprattutto sul fronte della prevenzione.  
  
In questo senso la prima cosa da fare e' potenziare l'informazione tra i cittadini. Secondo una recente ricerca una considerevole percentuale di giovani tra i 12 e i 24 anni d'eta' non ha mai sentito parlare di preservativi, mentre un terzo non usa il condom perche' lo ritiene 'immorale', e il 48 per cento ritiene che sia inutile nel matrimonio. **Il governo intende poi affrontare anche lo spinoso caso della prevenzione tra omosessuali: vuole promuovere apertamente l'uso del preservativo anche tra questa fetta di popolazione**. Si tratta di una svolta se si pensa che in Etiopia l'omosessualita' e' considerata un crimine sanzionabile con una pena che va dai 3 ai 12 mesi di carcere.