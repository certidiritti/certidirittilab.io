---
title: 'Sochi 2014 / Pescante: scuse maldestre. Il codice di comportamento sportivo vieti le discriminazioni basate su orientamento sessuale, identità ed espressione di genere'
date: Fri, 17 Jan 2014 13:07:35 +0000
draft: false
tags: [Politica]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 17 gennaio 2014

Dopo la polemica scatenata dalle sue parole di fronte al Consiglio Nazionale del Coni, Mario Pescante, membro italiano del Cio, ammette: «Sui gay ho usato termini impropri», ma ribadisce il no «all'uso politico delle Olimpiadi».

Al di là delle scuse maldestre di Pescante, le sue parole hanno gettato un'ombra sul CONI che è necessario spazzare via. Chiediamo che allo sport italiano venga restituita la dignità che si merita a partire dalla sua legge fondamentale: il Codice di comportamento sportivo. L'articolo 6 del testo deliberato dal Consiglio Nazionale nella riunione del 30 ottobre 2012, sancisce il principio di non discriminazione («I tesserati, gli affiliati e gli altri soggetti dell'ordinamento sportivo devono astenersi da qualsiasi comportamento discriminatorio in relazione alla razza, all'origine etnica o territoriale, al sesso, all'età, alla religione, alle opinioni politiche e filosofiche») senza esplicitare il divieto di discriminazione basato su orientamento sessuale, identità ed espressione di genere.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, afferma: «Il modo migliore per lavare l'offesa ai valori dello sport, per parafrasare le parole del titolo del Corriere della Sera di oggi, è proprio quello di modificare il Codice di comportamento sportivo inserendo anche il divieto di discriminazione basato su orientamento sessuale, identità ed espressione di genere. Esplicitare questi terreni di possibile discriminazione non solo sarebbe importante visti gli umori diffusi nella burocrazia dello sport italico, ma acquisirebbe un significato simbolico fondamentale di fronte agli episodi di omo-transfobia che sono ancora troppo diffusi negli spogliatoi, nelle squadre, sui campi e sulle tribune di palazzetto e stadi italiani. Una presa di posizione di questo tipo farebbe bene ai tanti atleti LGBTI e non, alla società tutta, ma soprattutto ai giovani che si rivolgono al mondo dello sport con tanta passione, entusiasmo e aspettative».