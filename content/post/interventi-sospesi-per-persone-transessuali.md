---
title: 'INTERVENTI SOSPESI PER PERSONE TRANSESSUALI'
date: Thu, 02 Dec 2010 15:38:22 +0000
draft: false
tags: [Comunicati stampa]
---

**INTERVENTI SOSPESI PER PERSONE TRANSESSUALI: GRAVISSIMO QUANTO ACCADE ALL’OSPEDALE SAN CAMILLO DI ROMA. CONSIGLIERI RADICALI PRESENTANTO INTERROGAZIONE URGENTE CO-FIRMATA DA RAPPRESENTANTI DI OTTO DIVERSI GRUPPI POLITICI.**

 **_Consiglio Regionale del Lazi, Gruppo Consiliare Lista Bonino Pannella – Federalisti Europei_**

Giovedì 2 dicembre 2010

_**Dichiarazione dei consiglieri regionali della Lista Bonino Pannella, Giuseppe Rossodivita e Rocco Berardo**_

Da circa un anno l’Ospedale San Camillo di Roma, che ha uno dei più importanti centri di eccellenza riguardo gli interventi chirurgici per le persone transessuali, ha sospeso la sua attività. I consiglieri regionali radicali Giuseppe Rossodivita e Rocco Berardo, della Lista Bonino Pannella, hanno depositato una interrogazione urgente, co-firmata dai Consiglieri di altri Gruppi politici, e precisamente da Angelo Bonelli (Verdi), Luciano Romanzi (Psi), Luigi Nieri (Sel), Ivano Peduzzi (Fds), Anna Maria Tedeschi (Idv) Giuseppe Celli (Lista Civica per Bonino) e Enzo Foschi (Pd).

E’ gravissimo che un centro di aiuto e sostegno alle persone transessuali, uno dei migliori d’Italia, abbia sospeso da circa un anno gli interventi chirurgici. Occorre che la Regione intervenga con la massima urgenza, così come chiedono decine di Associazioni nel territorio, per evitare che molte persone transessuali vengano ancor più emarginate e per garantire loro aiuto contro il pregiudizio e la violenza.

**Di seguito il testo integrale dell’interrogazione depositata:**

ALLA PRESIDENTE DELLA GIUNTA REGIONALE DEL LAZIO

Interrogazione urgente a risposta scritta

Oggetto: sospensione assistenza sanitaria del Centro Saifip dell'Ospedale San Camillo- Forlanini di Roma

Per sapere – premesso che:

Il Servizio di Adeguamento tra Identità Fisica ed Identità Psichica – SAIFIP dell’Azienda ospedaliera San Camillo – Forlanini di Roma si trova in una delicata e complessa situazione che determina gravissimi disagi per le persone transessuali e le loro famiglie;

Il Prof. A. Felici, Primario della S.C. di Chirurgia Plastica e Ricostruttiva per circa vent’anni, è andato in pensione nel gennaio 2010. Sembra che da tale data non siano stati più effettuati gli interventi di vaginoplatica, intervento necessario nel percorso di adeguamento medico-psicologico-legale di una persona MtoF, e che, pertanto, la lista d’attesa per gli interventi chirurgici è aumentata in modo considerevole; attualmente, bisogna aspettare per il primo intervento di vaginoplastica tra i 24 ei 30 mesi;

Viene così a mancare, per le persone con disturbi dell’identità di genere, sottoposte ancora oggi a ogni tipo di discriminazione e di violenze fisiche e morali, uno dei più importanti centri di specializzazione, assistenza e aiuto del Lazio e tra i migliori d’Italia;

Il SAIFIP è stato istituito nel 1992, proprio per assicurare alle persone con “Disturbo dell’Identità di Genere” (cod. 302.85 – DSM IV-TR 2000) quel complesso di prestazioni medico-chirurgiche e psicologico-diagnostiche che consentano loro un adeguamento tra la propria identità fisica e quella psichica, al fine di migliorare la loro qualità di vita e ha da sempre fornito le prestazioni suddette grazie da un’equipe qualificata e prestigiosa che poche strutture in  Italia sono riuscite a realizzare;

Per sapere:

\- se non ritenga la Presidente di dover intervenire con urgenza sulla struttura ospedaliera affinchè venga scongiurato il rischio che le persone transessuali vengano private  di qualsivoglia forma di assistenza (psicologica, medico-chirurgica, endocrinologica), loro garantita costituzionalmente, quale imprescindibile diritto alla salute;

\- se tale decisione non determini il fatto che a causa della  mancanza di tale assistenza medico psicologica nella Regione Lazio diventi inevitabile lo spostamento presso altre regioni o stati esteri, con conseguente incremento della  spesa di rimborso a carico della Regione Lazio;

\- se non venga così vanificato tutto il lavoro di formazione e specializzazione sul tema dell’identità di genere e dei suoi disturbi realizzata in questi anni da parte dell’equipe interdisciplinare del SAIFIP, medici, psicologi e personale ausiliario e del comparto del reparto;

\- se sia a conoscenza del fatto che il SAIFIP rappresenta in Italia il centro specialistico con il più alto numero di utenti e l’unico che prevede un servizio specifico rivolto ai bambini e adolescenti

con problematiche relative all’identità di genere;

\- se non ritenga necessario  garantire la continuità del progetto, mediante l’incremento delle attività e prestazioni già presenti e la ripresa immediata e regolare nel tempo degli interventi chirurgici nel percorso di adeguamento MtoF e se non ritenga che tale continuità risulti  assolutamente indispensabile, soprattutto per il trattamento di quelle persone che sono già inserite nel protocollo di intervento, sia medico che psicologico, la cui interruzione potrebbe avere per loro effetti devastanti,

\- se è a conoscenza del fatto che molte associazioni e singoli professionisti, che si battono per la difesa dei diritti civili e umani delle persone transessuali, hanno firmato un appello esprimendo la loro più viva preoccupazione  per le sorti dell’utenza e delle loro famiglie e che  ritengono che il SAIFIP, unico servizio pubblico nel Lazio e tra i pochi in Italia nel settore, debba riprendere al

più presto e diventare più stabile ed ancorato al territorio, proprio per poter rispondere ad una

domanda di assistenza sanitaria, sempre più crescente e complessa.

Giuseppe Rossodivita

Rocco Berardo

Angelo Bonelli

Luciano Romanzi

Luigi Nieri

Ivano Peduzzi

Anna Maria Tedeschi

Giuseppe Celli

Enzo Foschi