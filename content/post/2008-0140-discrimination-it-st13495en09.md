---
title: '2008-0140-Discrimination-IT-ST13495.EN09'
date: Mon, 21 Sep 2009 10:42:53 +0000
draft: false
tags: [Senza categoria]
---

  

  

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 21 September 2009

Interinstitutional File:

2008/0140 (CNS)

13495/09

LIMITE

SOC 529

JAI 604

MI 345

  

  

  

  

  

NOTE

from :

General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

13049/09 SOC 504 JAI 564 MI 326

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached a note from the Italian delegation with a view to the meeting of the Social Questions Working Party on 22 September 2009.

Changes are indicated in relation to doc. 13238/09.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Italian suggestions for amendments**

*   **Suggestion for amendment of Article 2(3):**

Harassment shall be deemed to be a form of discrimination within the meaning of paragraph 1, when unwanted conduct related to any of the grounds referred to in Article 1 takes place with the purpose or effect of violating the dignity of a person and of creating an intimidating, hostile, degrading, humiliating  or offensive environment. In this context, the concept of harassment may be defined in accordance with the national laws and practice of the Member States. **The mere expression of a personal opinion or the display of religious symbols or messages are presumed as not constituting harassment.**

*   **Suggestion for amendment of Article 2(3a) and Recital 12a:**

Discrimination includes direct discrimination or harassment due to a person's association with persons of a certain religion or belief, persons with disabilities, persons of a  given age or of a certain sexual orientation**; or based on assumptions about a person's religion or belief, disability, age or sexual orientation**. **A certain conduct may be deemed discrimination by association in the meaning of this Directive when it is so serious in nature that it has the purpose or effect of violating the dignity of a person and of creating an intimidating, hostile, degrading, humiliating or offensive environment.**

(12a)    (new)  In accordance with the judgment of the Court of Justice in Case C-303/06, it is appropriate to provide explicitly for protection from discrimination by association on all grounds covered by this Directive. Such discrimination occurs, _inter alia_, when a person is treated less favourably, or harassed, because, in the view of the discriminator, he or she is associated with persons of a particular religion or belief, disability, age or sexual orientation, for instance through his or her family, friendships, employment or occupation**. Moreover, discrimination within the meaning of this Directive also includes direct discrimination or harassment based on assumptions about a person's religion or belief, disability, age or sexual orientation.**

**A certain conduct may be deemed discrimination by association in the meaning of this Directive when it is so serious in nature that it has the purpose or effect of violating the dignity of a person and of creating** **an intimidating, hostile, degrading, humiliating or** **offensive environment.**

*   **Suggestion for amendment of Article 2(7) and Recital 15:**

Notwithstanding paragraph 2, in the provision of financial services, proportionate differences in treatment where, for the service in question, the use of age or disability is a determining factor in the assessment of risk based on relevant actuarial principles, accurate statistical data or  medical knowledge **scientifically proved medical knowledge and evidence** shall not be considered discrimination for the purposes of this Directive.

(15) Actuarial and risk factors related to disability and to age are used in the provision of insurance, banking and other financial services. These should not be regarded as constituting discrimination where service providers have shown, by relevant actuarial principles, accurate statistical data or medical knowledge **scientifically proved medical knowledge and evidence**, that  such factors are determining factors for the assessment of risk.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_