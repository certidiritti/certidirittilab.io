---
title: '2008-0140-Discrimination-FR-ST14852.EN10'
date: Tue, 12 Oct 2010 10:41:09 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 12 October 2010

Interinstitutional File:

2008/0140 (CNS)

14852/10

LIMITE

SOC 645

JAI 835

MI 372

  

  

  

  

  

NOTE

from :

General Secretariat

to :

The Working Party on Social Questions

on :

19 October 2010

No. prev. doc. :

13883/10 SOC 561 JAI 759 MI 317

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

In preparation of the next meeting of the Working Party, which is provisionally scheduled for 19 October 2010, delegations will find attached a note from the FR delegation.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**CONTRIBUTION DE LA DELEGATION FRANCAISE DU 6 OCTOBRE 2010 EN REPONSE AU QUESTIONNNAIRE  DE LA PRESIDENCE SUR LE LOGEMENT  
(doc 13883/10 du 21 septembre 2010)**

**(_EN VUE_ _DU DEBAT AU GROUPE DES QUESTIONS SOCIALES_**_)_

**1\.** **Situation in Member States**

Both the Racial Equality Directive (2000/43/EC) and the Gender Goods and Services Equality Directive (2004/113/EC) cover housing available to the public in their scope. Several MS have gone beyond these two grounds in their protection against discrimination.

a)       To what extent does your national legislation against discrimination apply to housing? _(Etendue de la législation nationale contre les discriminations en matière de logement)_

**_REPONSE :_**

**La loi 2008-496 du 27 mai 2008 portant diverses dispositions d’adaptation au droit communautaire dans le domaine de la lutte contre les discriminations dispose d’un large champ d’application qui couvre le logement, son article 2 énonçant le principe général selon lequel : « toute discrimination directe ou indirecte fondée sur l'appartenance ou la non-appartenance, vraie ou supposée, à une ethnie ou une race est interdite en matière de protection sociale, de santé, d'avantages sociaux, d'éducation, d'accès aux biens et services ou de fourniture de biens et services ».**

**Cependant l’affirmation du principe de non discrimination dans les locations de logements figurait déjà à l’article 1er de la loi n°89-462 du 6 juillet 1989, modifié par la loi du 17 janvier 2002 qui dispose qu’ : « aucune personne ne peut se voir refuser la location d’un logement en raison de son origine, son patronyme, son apparence physique, son sexe, sa situation de famille, son état de santé son handicap, ses mœurs, son orientation sexuelle, ses opinions politiques, ses activités syndicales ou son appartenance ou non appartenance vraie ou supposée à une ethnie, une nation, une race ou une religion déterminée »**

  

b)      Do you distinguish (for this purpose) between private and public/social housing_?_

**_REPONSE_**

**Le principe de non discrimination est applicable aussi bien dans le secteur public que privé, la loi n°2008-496 affirmant dans son article 5 que « I. - Les articles 1er à 4 et 7 à 10 s'appliquent à toutes les personnes publiques ou privées, y compris celles exerçant une activité professionnelle indépendante ».**

c)       Do you have statistics on the number of cases of discrimination in the area of housing, and if so, what is the most prevalent ground?

**Les données judiciaires sur le sujet sont inexistantes. Le nombre des discriminations effectives n’est donc pas connu.**

**2\.** **Social housing**

a)       Do you consider social housing to be a service (for the purposes of EU law)?

**_REPONSE_**

**Non, le logement social ne peut être considéré comme un service économique au sens de l’article 57 TFUE. Il s’agit en effet d’un service dont il faut reconnaître le caractère particulier de service d’intérêt économique général (SIEG).**

b)      If not, do you consider it to be a part of the social protection system?

**_REPONSE_**

**Pour la France, le logement social relève de la politique nationale de protection sociale et de solidarité. Cette politique puise son fondement dans la reconnaissance du droit au logement et au respect de la dignité humaine. Il constitue une réponse au déficit structurel d’offre de logement, notamment dans les grandes agglomérations et pour un public qui ne dispose pas des ressources suffisantes pour accéder aux produits fournis par le marché. Le logement social recouvre la mise à disposition de logements à loyers modérés d’une part, et différents régimes d’aides financières, d’autre part.**

**A cet égard, l’article L301-1 du Code de la construction et de l’habitation dispose que _:_**

**_« I. - La politique d'aide au logement a pour objet de favoriser la satisfaction des besoins de logements, de promouvoir la décence du logement, la qualité de l'habitat, l'habitat durable et l'accessibilité aux personnes handicapées, d'améliorer l'habitat existant et de prendre en charge une partie des dépenses de logement en tenant compte de la situation de famille et des ressources des occupants. Elle doit tendre à favoriser une offre de logements qui, par son importance, son insertion urbaine, sa diversité de statut d'occupation et de répartition spatiale, soit de nature à assurer la liberté de choix pour toute personne de son mode d'habitation._**

**_II. - Toute personne ou famille éprouvant des difficultés particulières, en raison notamment de l'inadaptation de ses ressources ou de ses conditions d'existence, a droit à une aide de la collectivité pour accéder à un logement décent et s’y maintenir._**

**En ce qui concerne les aides financières au logement, il existe en France trois catégories d’allocation (non cumulables entre elles), versées sous condition de ressources :**

**\-        _L’aide personnalisée au logement (APL, Art L. 351-1 du Code de la construction et de l’habitation)_ : Cette aide est financée par le Fonds national d’aide au logement (FNAL), via une dotation de l’Etat et une contribution de la branche famille de la sécurité sociale. Elle est versée par les organismes payeurs des prestations familiales. Elle s’adresse aux familles ou aux personnes seules, soit au titre de la location, soit au tire de l’accession à la propriété, dès lors que le logement a été conventionné ou a bénéficié de prêts aidés par l’Etat (pour le locatif, il s’agit essentiellement du parc HLM). Bénéficiaires : 2 613 000 ménages en 2008.**

**-        L’allocation de logement social (ALS, Article L. 831-1 du code de la sécurité sociale) financée par le FNAL, via une dotation de l’Etat et une cotisation à la charge des employeurs. Elle est versée par les organismes payeurs des prestations familiales. Elle s’adresse aux personnes ou couples n’ayant pas de personne à charge. Bénéficiaires : 2 331 000 ménages en 2008.**

**\-        L’allocation de logement familiale (ALF, Article L. 542-1 du code de la sécurité sociale) : cette allocation est intégralement financée par la branche famille de la sécurité sociale et versée par les organismes payeurs des prestations familiales. Elle s’adresse essentiellement aux personnes et aux couples ayant au moins un enfant ou une personne à charge. Bénéficiaires : 1 346 000 ménages en 2008.**

**Parmi ces trois allocations, seule l’ALF est une prestation de sécurité sociale relevant du règlement 883/2004/CE de coordination des régimes de sécurité sociale. Elle fait en effet partie des prestations familiales du code de la sécurité sociale, dans la mesure où son octroi est conditionné à l’existence d’une charge de famille. Pour leur part et au regard des règles de libre circulation du Traité, l’APL et l’ALS constituent des avantages sociaux au sens du règlement 1612/68/CEE.**

**En ce qui concerne les règles de non discrimination du Traité en raison de la race ou de l’origine ethnique, ces trois prestations relèvent de la Directive  2000/43/CE : l’ALF au titre de l’art 3 e) protection sociale, y compris la sécurité sociale et les soins de santé, l’APL et l’ALS au titre de l’art 3 f) avantages sociaux.**

**3\.** **Private life**

In some cases, the principle of equality might be in conflict with the right to private life, in particular in the area of housing (e.g. renting out a room in your own apartment).

**_REPONSE_**

**Le principe de non discrimination est un principe de portée générale qui ne peut faire l’objet de dérogation et dont le respect est par ailleurs assuré par l’établissement de sanctions pénales.**

a)       Is this better taken into account by an exclusion of transactions in the area of private life from the scope of the Directive, or by an exclusion of activities which are not commercial or professional?

**_REPONSE_**

**Aucune des alternatives proposées n’est satisfaisante du point de vue du droit français qui n’admet pas de dérogation au principe de non discrimination quel que soit le domaine d’application (contrat de droit privé, ou contrat commercial ou professionnel).**

b)      How is commercial/professional housing defined in your national legislation?

**_REPONSE_**

**Le bail commercial :**

**Le statut des baux commerciaux résulte d'un décret du 30 septembre 1953. On parle de statut car plusieurs dispositions sont d'ordre public et ne peuvent être contredites par les conventions des parties. Les dispositions légales fixent les conditions pour bénéficier des avantages de ce statut.**

**Le caractère impératif du statut ne concerne que quelques règles, que les articles L. 145-15, L. 145-16 et L. 145-45 du code de commerce délimitent. En dehors d'elles, la rédaction des clauses procède de la liberté contractuelle.**

**Le bail commercial doit être conclu pour une durée minimale de neuf ans. C'est l'article L. 145-4 du code de commerce qui l'impose. Le locataire et le propriétaire peuvent prévoir une durée supérieure, mais il leur est interdit de stipuler une durée inférieure à neuf ans. Toutefois, une exception est admise pour les baux dits de courte durée (2ans).**

**Le statut protecteur des baux commerciaux ne s'applique qu'au propriétaire d'un fonds commercial, industriel ou artisanal. Ce fonds doit en outre être exploité. Les deux principaux critères exigés pour l’identification d’un fonds de commerce sont la clientèle et une autonomie de gestion.**

**Le bail professionnel :**

**Le bail professionnel est partiellement réglementé par la loi n° 86-1290 du 23 décembre 1986.**

**La plupart des règles qui lui sont applicables restent néanmoins déterminées par les articles 1713 et suivants du code civil.**

**C’est un contrat par lequel un propriétaire (le bailleur) loue à un tiers (le preneur) un bien immobilier affecté à l’exercice exclusif d’une activité :**

**\-        non commerciale (professions libérales ou assimilées, association …)**

**\-        ou artisanale lorsqu'il n'existe pas de fonds artisanal.**

**Le bail professionnel doit être écrit et sa durée être au moins égale à 6 ans. Les baux de plus de 12 ans doivent être établis par un notaire. Le locataire peut à tout moment notifier au bailleur, par lettre recommandée avec AR ou par acte d'huissier, son intention de quitter les locaux en respectant un délai de préavis de 6 mois. Le bailleur pour sa part est tenu pour toute la durée du bail mais peut en refuser le renouvellement par notification (lettre recommandée avec AR, ou exploit d'huissier) adresser au locataire au moins 6 mois avant la**

**fin du bail.**

**Le bail mixte (habitation/professionnel)**

**Le bail mixte est un contrat par lequel un propriétaire (le bailleur) loue à un tiers (le preneur) un bien immobilier dont une partie est destinée à l'habitation du locataire et l'autre partie utilisée pour l'exercice de sa profession. Ce type de bail est régi par la loi n° 89-462 du 6 juillet 1989.**

**Le bail mixte doit obligatoirement être établi par écrit et comporter certaines mentions obligatoires.**

**Seul le preneur peut obtenir l'annulation du bail qui ne respecterait pas ces dispositions.**

**La durée minimale du bail est de :**

-                 **3 ans lorsque le propriétaire est une personne physique,**

**\- 6 ans lorsque le propriétaire est une personne morale.**

**Toutefois la durée du bail peut être inférieure à 3 ans sans être inférieure à un an, lorsque le bailleur est une personne physique, une indivision ou une société civile constituée entre parents, et qu'un** **événement précis justifie qu'il ait à reprendre le local pour des raisons professionnelles ou familiales. Le locataire peut résilier le bail à tout moment. Il doit avertir le bailleur 3 mois avant, ou un mois avant, pour raisons professionnelles (transfert d'activité, cessation d'activité...). Le congé doit être signifié au bailleur par lettre recommandée avec accusé de réception ou par acte d'huissier.**

**Le bail est résilié de plein droit si une clause de résolution le prévoit expressément. Cette dernière joue un mois après un commandement demeuré infructueuse.**

**Comme dans le cas d'un bail professionnel, il importe de vérifier qu'une activité professionnelle est possible dans les locaux loués tant au niveau administratif qu'à celui de la copropriété.**

**Dans certaines agglomérations, il est important de vérifier auprès de la mairie (ou préfecture pour Paris), que le local est bien affecté administrativement à l'exploitation d'une activité professionnelle.**

**Il convient également de vérifier l’affectation du bail par rapport au règlement de copropriété. Dans l’hypothèse, où le règlement de copropriété interdirait l’exercice de l’activité professionnelle il est possible de demander une autorisation d’exercer cette activité à l’assemblée générale des copropriétaires.**

**Ce rappel de la réglementation française est fait à titre informatif dans la mesure où en matière de logement, seul le bail mixte est concerné, les baux professionnels et commerciaux étant exclusifs de tout logement.**

**_Bien évidemment, il convient de rappeler que quelle que soit la définition des baux, celle-ci est sans incidence sur le principe selon lequel il ne peut être procédé à des discriminations à l’encontre du bailleur et du preneur_****. _Cette règle vaut autant pour les bailleurs privés que pour les professionnels de la location ou de la vente_.**

**4\.** **Disability - reasonable accommodation**

a)             In your view, what should the obligation to provide reasonable accommodation mean for providers of housing?

**_REPONSE_**

**Pour les bailleurs, l’obligation de rendre intégralement un bâtiment d’habitation accessible est imposée lorsque la valeur des travaux entrepris dans le bâtiment correspond à plus de 80% de sa valeur hors terrain.**

**En droit français, la réglementation en matière d’accessibilité du cadre bâti met l’accent sur le principe d’une obligation de mise en accessibilité des locaux, avec certains cas de dérogations prévus de manière encadrée. Il n’est pas fait référence à la notion d’aménagement raisonnable, hormis dans le champ de l’emploi, où la loi prévoit ainsi qu’ « afin de garantir le respect du principe d’égalité de traitement à l’égard des travailleurs handicapés, l’employeur prend, en fonction des besoins dans une situation concrète, les mesures appropriées pour permettre aux travailleurs d’accéder à un emploi ou de conserver un emploi correspondant à leur qualification, de l’exercer ou d’y progresser ou pour qu’une formation adaptée à leurs besoins leur soit dispensée.  Ces mesures sont prises sous réserve que les charges consecutives à leur mise en œuvre ne soient pas disproportionnées, compte tenu de l’aide prévue à l’article L 5213-10 qui peuvent compenser en tout ou partie les dépenses supportées à ce titre par l’employeur » (article L 5213-6 du code du travail, disposition introduite en transposition de la directive « emploi »). En parallèle de cette obligation pour l’employeur de prendre des mesures appropriées afin de répondre à des situations particulières, il existe également des obligations légales imposées aux entreprises en matière d’accessibilité de leurs locaux.**

**L’aménagement raisonnable peut ainsi être envisagé comme une mesure particulière prise en fonction des besoins d’une personne, sans que cette mesure n’entraine de charge disproportionnée.**

**En France, dans le champ du logement, certains dispositifs permettent d’accompagner le traitement des situations particulières afin que les besoins des personnes handicapées soient pris en compte. Plusieurs dispositifs financiers peuvent ainsi être mobilisés au bénéfice des propriétaires occupants et bailleurs, ou des locataires, afin de permettre l’adaptation et la mise en accessibilité de leurs logements. Il s’agit par exemple des aides versées par l’ANAH (Agence nationale pour l’amélioration de l’habitat) aux propriétaires occupants ou bailleurs ainsi que dans certains cas aux locataires, des dispositifs de crédits d’impôts accordés concernant l’installation ou le remplacement de certains équipements et enfin des aides pouvant être versées au titre de la prestation de compensation du handicap (3ème élément) à la personne handicapée elle-même pour l’adaptation et la mise en accessibilité de son logement.**

  

b)      Is there legislation on the accessibility of buildings (private as well as public/social) in your country**?**

**_REPONSE_**

**La loi n°2005-102 du 11 février 2005 pour l’égalité des droits et des chances rend obligatoire l'accès des personnes handicapées aux locaux d'habitation neufs, privés ou publics, quel que soit leur handicap.**

**Oui, elle s’applique principalement aux bâtiments collectifs neufs (secteur privé comme secteur social) et aux maisons individuelles groupées. Pour l’existant, l’obligation de mettre en accessibilité intégralement le bâtiment n’est obligatoire que lorsque le coût des travaux dépasse 80% de la valeur du bâtiment (cf question 4a)**

**5\.** **Disability - accessibility** _(invalidité- accessibilité)_

a)       What should the obligation to provide for accessibility measures encompass in the area of housing?

**_REPONSE_**

**Pour les logements collectifs nouvellement crées (secteur social comme privé), il existe une obligation d’ascenseur lorsque l’immeuble fait plus de 3 étages. Obligation qu’une personne en fauteuil roulant puisse avoir accès aux logements en rez-de-chaussée et à ceux desservis par ascenseur au moins dans une unité de vie du logement (composée de la cuisine, du salon, d’une chambre, d’un cabinet d’aisance et d’une salle d’eau).**

b)      Should there be a difference between old and new buildings?

**_REPONSE_**

**Oui, cf question 4**

c)       Do you support immediate applicability with respect to new buildings?

**_REPONSE_**

**Un délai d’application est toujours souhaitable pour permettre aux Etats membres de prendre en compte une nouvelle directive.**

  

d)      Should there be a difference between the obligations for private and public/social housing respectively?

**_REPONSE_**

**Oui, les maisons individuelles construites pour un usage propre sont exclues de notre réglementation. De plus les logements sociaux répondant à un besoin différent et ayant un financement spécifique, il peut être souhaitable de ne pas les traiter de la même manière.**

e)  Are there public funds available in your country for the adaptation of housing (private as well as public/social)?

**_REPONSE_**

**Oui, il existe un crédit d’impôt pour l’acquisition de matériel spécifique pour aménager un logement pour une personne handicapée ou vieillissante.** **L'Agence nationale de l'habitat** (**l’ANAH) subventionne les travaux de mise en accessibilité des logements ainsi que le** **maintien à domicile des personnes qui, quels que soient leur âge et leur situation, rencontrent des difficultés dans ce domaine du fait de leur état de santé ou de leurs incapacités motrices ou physiques** **; les organismes HLM (Habitation à loyers modérés)** **peuvent déduire les dépenses engagées pour l’accessibilité et l’adaptation de leurs logements en faveur de personnes en situation de handicap sur le montant de la taxe foncière sur les propriétés bâties qu’ils acquittent.**

**L'aide de l'Agence peut être accordée sous certaines conditions aux propriétaires occupants, aux propriétaires bailleurs et, exceptionnellement, aux locataires qui réalisent des travaux d'adaptation ou d'accessibilité. L'Anah peut également subventionner les syndicats de copropriétaires pour les travaux d'accessibilité des immeubles.**

**La prestation de compensation (PCH), créée par la loi du 11 février 2005, permet de prendre en compte les aménagements concourant à l'adaptation et à l'accessibilité du logement. Ils peuvent concerner les pièces ordinaires du logement : la chambre, le séjour, la cuisine, les toilettes et la salle d'eau. Toutefois, la prestation de compensation peut aussi prendre en compte des aménagements concourant à l'adaptation et à l'accessibilité d'une autre pièce du logement permettant à la personne handicapée d'exercer une activité professionnelle ou de loisir et des pièces nécessaires pour que la personne handicapée assure l'éducation et la surveillance de ses enfants.**

**Les aménagements pris en compte sont destinés à maintenir ou à améliorer l'autonomie de la personne handicapée et doivent lui permettre de circuler, d'utiliser les équipements indispensables à la vie courante, de se repérer et de communiquer, sans difficultés et en toute sécurité. Ils visent également à faciliter l'intervention des aidants qui accompagnent la personne handicapée à domicile pour la réalisation des actes essentiels de l'existence.**

**L’article 200 quater A du code général des impôts a porté création d’un crédit d’impôt à destination des personnes les plus fragiles. Il concerne les dépenses d’installation ou de remplacement d’équipements spécialement conçus pour les personnes âgées ou handicapées.**

**Cette mesure, initialement prévue jusqu’au 31 décembre 2010, fait l’objet d’un article dans le projet de loi de finances pour 2011 visant à l’aménager et à la proroger (jusqu’au 31 décembre 2013).**

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_