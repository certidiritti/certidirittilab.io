---
title: 'Stamane fermati i radicali a Porta Pia, fermato Staderini a Rovasio impedita consegna libro a Bertone'
date: Mon, 20 Sep 2010 11:21:15 +0000
draft: false
tags: [Breccia di Porta Pia, chiesa, Comunicati stampa, PORTA PIA, RADICALI, rovasio, staderini, STORIA, XX settembre]
---

XX SETTEMBRE: Radicali, denunciata la grave opera di revisione storica. Il segretario Staderini identificato preventivamente, e poi fermati i radicali. A Rovasio viene impedito di consegnare a Bertone il libro di Ernesto Rossi "Il Sillabo e dopo".

Roma, 20 settembre 2010

Questa mattina Mario Staderini, Segretario di Radicali Italiani, Sergio Rovasio, Segretario dell’Associazione radicale certi diritti, Diego Galli, responsabile di Radioradicale.it, Riccardo Magi, Segretario di Radicali Roma si sono recati alla celebrazione ufficiale del XX settembre, dove erano presenti i rappresentanti di molte istituzioni italiane e un centinaio di cittadini.

I Radicali hanno assistito in silenzio sino al completamento del programma ufficiale,  all’interno del quale l’unico intervento che i cittadini presenti hanno potuto ascoltare è stata la “preghiera” del Segretario di Stato Vaticano, il quale ha ricordato Pio IX, i bersaglieri e gli zuavi pontifici caduti in occasione della Breccia, e ha parlato di riconciliazione.

Mentre assisteva alle celebrazioni, senza che nulla fosse accaduto, il Segretario di Radicali Italiani  Staderini e Pier Paolo Segneri, militante radicale, sono stati fermati e identificati dagli agenti della Digos che non hanno fornito alcuna spiegazione a riguardo.

Solo dopo, al termine del programma previsto,  rivolto al Segretario di Stato Vaticano, Cardinale Bertone, che lo stava ascoltando, Sergio Rovasio ha mostrato, per fargliene dono, il libro di Ernesto Rossi, “Il Sillabo e dopo” , che gli è stato immediatamente sequestrato dalle forze dell’ordine, mentre gli altri Radicali scandivano la frase “Vaticano e Partitocrazia, serve una nuova Porta Pia”. Gli agenti presenti hanno circondato i Radicali, dopo averli fermati,  li hanno identificati .

**_Dichiarazione di Mario Staderini, segretario di Radicali Italiani e di Sergio Rovasio, segretario dell'Associazione Radicale Certi Diritti_**

“In nome dell’Unità d’Italia si è completata oggi una operazione di revisione storica volta a cancellare il vero significato del XX settembre, evento che ha segnato il mondo contemporaneo aprendo alla libertà di coscienza e di opinione.

Non occorreva nessuna riconciliazione tra Risorgimento e cattolicesimo, perché sono stati proprio i cattolici liberali come Rosmini e Manzoni a lottare per la separazione tra potere spirituale e potere temporale.

Anziché il rappresentante di uno Stato che fa dell’assolutismo e dell’intolleranza una bandiera, sarebbe stato più utile sentire la voce della comunità ebraica, che con Porta Pia celebrò la fine del Ghetto cui lo Stato pontificio l’aveva ridotta.

Il potere temporale di oggi si chiama Vaticano, con i suoi sterminati interessi finanziari, economici e immobiliari, a cui si appoggia la partitocrazia italiana per continuare ad evitare di rendere conto al popolo italiano dei sessant’anni in cui ha rubato tutto, compresa democrazia e Stato di diritto.

Fa paura un regime che teme che il dissenso possa essere conosciuto, tanto da arrivare a controlli editoriali, identificazioni preventive e a mettere all’Indice il libro dell’antifascista Ernesto Rossi. “