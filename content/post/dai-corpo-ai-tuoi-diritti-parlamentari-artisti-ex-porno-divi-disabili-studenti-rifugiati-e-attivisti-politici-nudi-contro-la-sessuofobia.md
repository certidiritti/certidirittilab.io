---
title: '''Dai corpo ai tuoi diritti'': parlamentari, artisti, ex porno divi, disabili, studenti, rifugiati e attivisti politici nudi contro la sessuofobia'
date: Tue, 26 Jun 2012 11:52:33 +0000
draft: false
tags: [Politica]
---

Parte la campagna iscrizioni 2012 dell'associazione radicale Certi Diritti che da anni si batte per i diritti civili e la libertà sessuale.

Roma, 26 giugno 2012

Ci hanno messo la faccia, anzi il corpo. Si sono spogliati e si sono fatti fotografare, mostrando un cartello con la slogan: 'Dai corpo ai tuoi diritti'. Sono parlamentari, artisti, ex porno divi, disabili, studenti, rifugiati e attivisti politici che hanno deciso di sostenere le campagne dell'Associazione Radicale Certi Diritti per i diritti civili e la libertà sessuale. Perchè la sessuofobia colpisce tutti: uomini e donne, etero, gay e trans, giovani e anziani, prostitute, detenuti e disabili.

Una settimana fa, solo nella Capitale, sono stati cinque gli episodi di violenza contro le persone lgbt in un paese che ha il triste primato per omocidi di persone trans. Eppure non esistono in Italia campagne di sensibilizzazione contro l'omofobia e la transfobia, né la necessaria informazione sessuale da un governo e un servizio pubblico televisivo che censurano la parola 'preservativo', mentre aumentano soprattutto tra i giovani i casi di hiv, sifilide e altre malattie sessualmente trasmissibili.

  
In Italia il 63% dei cittadini è favorevole alle unioni civili e il 44% al matrimonio gay (dati Istat), ma ancora non c'è una legge che riconosca i diritti delle coppie conviventi, per non parlare di adozione ai single e omogenitorialità. L'82,2% degli italiani vuole il divorzio breve (Eurispes), ma l'ultima riforma del diritto di famiglia risale al 1975 e sulla prostituzione persiste un proibizionismo criminogeno che non produce altro che inutili e dannose ordinanze comunali.

Nel nostro Paese manca anche una legge che permetta alle persone trans di cambiare nome e genere sui propri documenti senza operazione di riassegnazione sessuale e le condizioni di vita in carcere sono per loro ancora più drammatiche per la mancanza di un'adeguata assistenza sanitaria. In Italia inoltre è vietato parlare di affettività per i detenuti e i termini 'sessualità' e 'disabilità' sono addirittura inaccostabili.  
   
L'associazione radicale Certi Diritti ha così deciso di lanciare la campagna fotografica 'Dai corpo ai tuoi diritti' con l'obiettivo di portare l'attenzione su questi e altri temi che la classe politica, indifferente ai diritti dei cittadini e genuflessa ai diktat vaticani, continua a ignorare ma anche per chiedere di sostenere un'associazione che non riceve finanziamente pubblici e vive solo dell'autofinanziamento degli iscritti.

Nelle prime 16 foto si sono spogliati per l'obiettivo della fotografa Patrizia Todisco,  tra gli altri, i parlamentari radicali Marco Perduca e Maria Antonietta Farina Coscioni, l'attore e musicista Fabiano Lioi affetto da osteogenesi imperfetta, St.RoboT del teknoelectroproject La Roboterie, il consigliere comunale Marco Cappato e la co-presidente dell'associazione Luca Coscioni Mina Welby, il segretario di Nessuno Tocchi Caino Sergio D'Elia e l'ex porno divo Francesco Malcom che ha recitato in 'Penocchio' e in decine di altri di film. La campagna è visibile da oggi sul sito www.certidiritti.org e su tutti i social network. Chiunque può partecipare inviando la propria foto a [info@certidiritti.it.](mailto:info@certidiritti.it.)

**[TUTTE LE FOTO DELLA CAMPAGNA >](partecipa/iscriviti)**

**[Vota su facebook la tua foto preferita >](http://www.facebook.com/media/set/?set=a.10151038148445973.482151.116671870972&type=1)  
**