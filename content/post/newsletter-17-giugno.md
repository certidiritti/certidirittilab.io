---
title: 'Newsletter 17 Giugno'
date: Wed, 31 Aug 2011 10:11:53 +0000
draft: false
tags: [Politica]
---

**![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)**

Continua tra rinvii e pregiudiziali di incostituzionalità, l’iter del ddl contro l’omofobia. Come possiamo pensare a un Italia che garantisca parità di diritti se la quasi totalità dei Parlamentari non approvano neppure due miseri articoli, più che altro simbolici, contro le violenze che ormai giornalmente investono le persone LGBT? La votazione è slittata a luglio e noi Certi Diritti insieme alle altre associazioni sarà in piazza per una maratona oratoria.  
Intanto si è arrivati a  concepire il “divorzio di Stato” pur di non riconoscere che esistono diverse famiglie oltre a quella tradizionale. Sotto il comunicato di Certi Diritti sulla vicenda di Alessandra Bernaroli.

La Cnn sta cercando di far luce sulle terapie “riparatorie” che anni fa hanno portato, tra l’altro,  al suicidio di un ragazzo affidato alle cure del dottor Rekers. Alle stesse teorie fanno riferimento i gruppi omofobi dell’integralismo cattolico in Italia …

**Certi Diritti è un’associazione completamente autofinanziata e ha bisogno del tuo sostegno per continuare le sue campagne [www.certidiritti.it/iscriviti-e-contribuisci](partecipa/iscriviti-e-contribuisci.html)**

Buona lettura

ASSOCIAZIONE
============

**Imposto divorzio a coppia perché partner cambia sesso.   
[Leggi >](tutte-le-notizie/1165.html)**

**Negato l’asilo a lesbica ugandese che ora rischia il rimpatrio: mobilitazione internazionale. Certi Diritti: si mobiliti anche l’Italia.  
[Leggi >](tutte-le-notizie/1166.html)**

**Voto su pdl anti-omofobia rinviato a luglio. Confermata maratona oratoria.[Leggi >](tutte-le-notizie/1168-voto-della-camera-su-pdl-anti-omofobia-rinviato-a-luglio.html)**

**Legge Omofobia: indecenti le pregiudiziali, vengano ritirate.  
[Leggi >](tutte-le-notizie/1164.html)**

**Lady Gaga non solo icona gay ma anche dei diritti civili e umani.  
[Leggi >](tutte-le-notizie/1162.html)**

**Lunedì 13 giugno Certi Diritti ha incontrato una delegazione dell’Osce.  
[Leggi >](tutte-le-notizie/1161.html)**

**EuroPride 2011 di Roma.  
[Guarda il video di Radio Radicale >](http://www-2.radioradicale.it/scheda/329516/europride-2011-di-roma)**

RASSEGNA STAMPA
===============

**Come ti smonto la legge sull’omofobia.  
[Di Matteo Winkler >](http://abominevolediritto.wordpress.com/2011/06/16/come-ti-smonto-la-legge-sullomofobia/)**

**Gay Pride: l'Ambasciata USA presso la Santa Sede era presente.  
[Leggi >](http://www.queerblog.it/post/11392/gay-pride-lambasciata-usa-presso-la-santa-sede-era-presente)**

**Roma Europride 2011: ancora polemiche da Carlo Giovanardi.  
[Leggi >](http://www.gaywave.it/articolo/roma-europride-2011-ancora-polemiche-da-carlo-giovanardi/31761/)**

**Milano, dalla Giunta Pisapia arriva il via libera al Gay Pride.  
[Leggi >](http://www.ilquotidianoitaliano.it/gallerie/2011/06/news/milano-dalla-giunta-pisapia-arriva-il-via-libera-al-gay-pride-93059.html/)**

**Onda gay in Versilia, meta preferita dal turismo omosex.  
[Leggi >](http://www.ilreporter.it/index.php?option=com_content&view=article&id=17361:onda-gay-in-versilia-mete-preferita-dal-turismo-omosex&catid=76:cronaca-e-attualita&Itemid=125)**

**Diritti fuoriLegge 40 - La procreazione medicalmente assistita in Italia dopo la sentenza della Corte Costituzionale. Conquiste e nuove speranze.   
[Leggi >](http://www.lucacoscioni.it/comunicato/diritti-fuorilegge-40-la-procreazione-medicalmente-assistita-italia-dopo-la-sentenza-dell)**

**'Amina' e l’ennesima bufala della Rete.  
[Leggi >](http://www.agenziaradicale.com/index.php?option=com_content&task=view&id=12452&Itemid=54)**

**Francia. Il governo ribadisce il suo no ai matrimoni gay.   
[Leggi >](http://gaynews24.com/?p=21628)**

**Giovani socialisti francesi si mobilitano per il matrimonio gay e lesbico.   
[Leggi >](http://www.queerblog.it/post/11382/giovani-socialisti-francesi-si-mobilitano-per-il-matrimonio-gay-e-lesbico)**

**Orgoglio LGTB: a Bucarest 200 partecipanti e diversi ambasciatori.   
[Leggi >](http://gayitaly.it/gay-chat/2011/06/orgoglio-lgtb-a-bucarest-200-partecipanti-e-diversi-ambasciatori/)**

**Slovacchia. E’ Stanislav Fořt il primo deputato gay del paese.  
[Leggi >](http://www.lavoce.sk/25465/2011/06/06/notizie-principali/e-stanislav-fort-il-primo-deputato-gay-del-paese/)**

**Cnn: Kirk, curato a 5 anni per effeminatezza e morto suicida.  
[Leggi >](http://www.queerblog.it/post/11363/cnn-kirk-curato-a-5-anni-per-effeminatezza-e-morto-suicida)**

**Kenya, chiesta la condanna a morte per i gay.   
[Leggi >](http://it.peacereporter.net/articolo/28952/Kenya,+chiesta+la+condanna+a+morte+per+i+gay)**

**Sudafrica. ****Quattro coltellate perché è _gay_****.   
[Leggi >](http://www.tg3.rai.it/dl/tg3/articoli/ContentItem-b780a988-9b2a-49de-8047-fd3933a65581.html)**

**L’Onu sull’Aids, la Siria e la giustizia internazionale.  
[Leggi >](http://notizie.radicali.it/articolo/2011-06-14/intervento/l-onu-sull-aids-la-siria-e-la-giustizia-internazionale)**

**California. Un giudice _gay_ può emettere sentenze sui matrimoni _gay_?  
[Leggi >](http://www.ilpost.it/2011/06/15/un-giudice-gay-puo-emettere-sentenze-sui-matrimoni-gay/)**

**New York City, presto i matrimoni tra gay.   
[Leggi >](http://www.ilsecoloxix.it/p/mondo/2011/06/14/AOzxyPc-matrimoni_york_presto.shtml)**

**Il vescovo Luis Gonzaga Bergonzini parla di "dittatura gay", un problema per i figli.  
[Leggi >](http://www.queerblog.it/post/11381/il-vescovo-luis-gonzaga-bergonzini-parla-di-dittatura-gay-un-problema-per-i-figli)**

STUDI, RICERCHE E MATERIALI INFORMATIVI
=======================================

**Gli impegni dell'Italia nella lotta globale all'AIDS al meeting delle Nazioni Unite di New York: tante belle parole ma zero fatti.  
[Leggi >](http://www.lila.it/doc/documentazione/2011_06_13Comstampa_Statement_ITA_HLM_OMU.pdf)**

**Usa. Sempre più adozioni gay, raddoppiate in un decennio.  
[Leggi >](http://america24.com/news/sempre-pi-adozioni-gay-raddoppiate-in-un-decennio)**

**La libertà di scelta è scelta di libertà.  
[Guarda il video di radio Radicale >](http://www.radioradicale.it/scheda/329742/la-liberta-di-scelta-e-scelta-di-liberta)**

LIBRI E CULTURA
===============

**Spot contro l'omofobia. Ecco possibili danni e conseguenze.  
[Guarda il video >](http://www.gaywave.it/articolo/video-gay-ecco-cosa-succede-alle-vittime-dell-omofobia/31765/)**

**Amnesty International Spot (Gay Rights).  
[Guarda il video >](http://www.youtube.com/watch?v=ZkAO0fYv3nM)**

**L’omofobia e la moratoria sulle enormità.  
[Leggi >](http://gilioli.blogautore.espresso.repubblica.it/2011/06/11/la-moratoria-sulle-enormita/)**

**Antonella Montano, Elda Andriola,  _[Parlare di omosessualità a scuola](http://www.deastore.com/libro/parlare-di-omosessualita-a-scuola-antonella-montano-elda-andriola-centro-studi-erickson/9788861378278.html)_, Centro Studi Erickson, € 17.50**

** Fernando Liggio, _[Il fondamento biofisiologico dell'omosessualità](http://www.deastore.com/libro/il-fondamento-biofisiologico-dell-omosessualita-fernando-liggio-aracne/9788854838956.html),_ Aracne, € 9.00**

**Da rileggere:**  
**Bonini Baraldi Matteo, **** _[La famiglia de-genere. Matrimonio, omosessualità e Costituzione](http://www.ibs.it/code/9788857500720/bonini-baraldi-matteo/famiglia-de-genere-matrimonio-omosessualit-agrave.html?shop=3916)_, Mimesis, ****€ 15,00**

[www.certidiritti.it](http://www.certidiritti.it/)
==================================================