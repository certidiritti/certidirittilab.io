---
title: 'Associazione radicale Certi Diritti scrive a Pisapia: la Costituzione non vieta il matrimonio egualitario'
date: Mon, 24 Sep 2012 10:05:35 +0000
draft: false
tags: [Matrimonio egualitario]
---

Lettera aperta del Segretario dell'associazione Yuri Guaiana al sindaco di Milano che continua a sostenere la necessità di una modifica della Costituzione per approvare il matrimonio tra persone dello stesso sesso.

Comunicato stampa dell'Associazione Radicale Certi Diritti

Milano, 24 settembre 2012

Caro Sindaco,

Nell’intervista che ha rilasciato oggi su “Pubblico”, alla domanda se è favorevole ai matrimoni omosessuali risponde testualmente: “Sì, ma serve una modifica costituzionale. La carta non parla di maschio e femmina ma perché non si prevedeva il caso gay”. Dispiace che anche Lei alimenti questa interpretazione della nostra Carta costituzionale che per numerosi e autorevoli costituzionalisti è del tutto infondata. Di più, la sentenza della Corte Costituzionale n. 138 del 2010 è molto esplicita nel sollecitare il Parlamento italiano a legiferare sulla materia. In un passaggio delle motivazioni, l’Alta Corte recita: “si deve escludere \[che il riconoscimento delle unioni omosessuali\] possa essere realizzato soltanto attraverso una equiparazione delle unioni omosessuali al matrimonio”. Questa infatti era la domanda rivolta alla Corte, ma “soltanto”, caro Sindaco, significa che tale equiparazione possa avvenire anche, ma non solo, attraverso un’equiparazione al matrimonio, se l’italiano non è un’opinione.

Pochi giorni fa, in una data evocativa come il 20 settembre, l’Associazione Radicale Certi Diritti ha rivolto **al Parlamento un appello scritto dalla prof.ssa D’Amico e sottoscritto da vari giuristi e docenti universitari**, alcuni dei quali milanesi, nel quale si sottolinea che **“– secondo l’opinione pur restrittiva della Corte – il legislatore, nell’esercizio della sua discrezionalità, non è vincolato da un divieto della Carta costituzionale, che non prevede e non impone che il matrimonio sia riservato alle coppie eterosessuali”.**

Ci vediamo costretti a rivolgere anche a lei questo appello, invitandola a un confronto pubblico sul tema.

Cordialità,  
Dott. Yuri Guaiana  
Segretario Ass. radicale  
Certi Diritti  
  
**[IL TESTO DELL'APPELLO E TUTTI I FIRMATARI >>>>>](http://www.certidiritti.org/2012/09/20/matrimonio-per-le-coppie-dello-stesso-sesso-il-parlamento-e-libero-di-decidere/)**