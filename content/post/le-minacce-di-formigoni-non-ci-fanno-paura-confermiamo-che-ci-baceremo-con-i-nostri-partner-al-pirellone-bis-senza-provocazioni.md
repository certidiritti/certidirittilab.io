---
title: 'Le minacce di Formigoni non ci fanno paura: confermiamo che ci baceremo con i nostri partner al Pirellone Bis, senza provocazioni'
date: Tue, 14 Feb 2012 13:04:33 +0000
draft: false
tags: [Diritto di Famiglia]
---

L'Associazione radicale Certi Diritti accetta l'invito del presidente della Lombardia al Pirellone Bis. Coppie etero e gay si baceranno e si terranno per mano. Formigoni cosa vuole? Solo le famiglie del Mulino Bianco.  
   
Roma, 14 febbraio 2012  
 

Comunicato Stampa dell’Associazione Radicale Certi Diritti

La strana minaccia del Presidente della Regione Lombardia non ci fermerà. L’Associazione Radicale Certi Diritti, accogliendo l’invito di Ivan Scalfarotto e del Consigliere Giuseppe Civati,  conferma che suoi esponenti innamorati, legati da vincoli affettivi, gay o eterosessuali che siano, parteciperanno, insieme ad altre Associazioni, all’invito rivolto da Formigoni alle coppie in occasione dell’apertura del Pirellone bis per la serata di San Valentino.

Il Presidente della Regione Lombardia  minaccia di far intervenire la questura qualora vi siano provocazioni. Ebbene, noi ci saremo e senza provocazioni, semplicemente con i nostri partner, con amore, affetto e speranza di un mondo migliore e meno clericale. Sarà l'occasione per mostrare a tutta Milano che l'amore non conosce aggettivazioni e ha tutto la medesima dignità. Chissà che non se ne accorgano anche negli altri piani del Palazzo della Regione e inizino ad attuare politiche di sostegno a tutte le forme famigliari, per le pari opportunità per tutti e per combattere ogni forma di discriminazione.