---
title: 'L''Associazione Radicale Certi Diritti su Tv Radicale'
date: Mon, 25 Jul 2011 13:39:36 +0000
draft: false
tags: [Politica]
---

Su [www.tvradicale.it](http://www.tvradicale.it/) la prima puntata della rubrica "Spazio Certi Diritti" di Riccardo Cristiano che ospita Giacomo Cellottini, tesoriere dell'[Associazione Radicale Certi Diritti](http://www.certidiritti.it/ "Associazione Radicale “Certi Diritti” ").

[http://www.tvradicale.it/](http://www.tvradicale.it/)