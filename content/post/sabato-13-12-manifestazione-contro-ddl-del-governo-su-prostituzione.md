---
title: 'SABATO 13-12 MANIFESTAZIONE CONTRO DDL DEL GOVERNO SU PROSTITUZIONE'
date: Fri, 12 Dec 2008 13:44:50 +0000
draft: false
tags: [Comunicati stampa, MANIFESTAZIONE, Prostituzione]
---

**Roma, 12 dicembre 2008**

**PROSTITUZIONE: SABATO 13 DICEMBRE MANIFESTAZIONE NAZIONALE CON RADICALI ITALIANI E CERTI DIRITTI A ROMA, IN PIAZZA FARNESE, CONTRO IL PROVVEDIMENTO DEL GOVERNO, PROIBIZIONISTA E IPOCRITA.**

**In occasione della mobilitazione delle "Sex Worker" in tutta Europa, contro il provvedimento del Governo italiano contro la prostituzione, proibizionista e ipocrita, domani, sabato 13 dicembre si terrà a Roma, in Piazza Farnese, a partire dalle ore 15, una manifestazione nazionale contro il DDL 'Carfagna'.  
   
La manifestazione é co-promossa da Radicali Italiani e l'Associazione Radicale Certi Diritti insieme al Comitato per i diritti civili delle prostitute, l'Associazione La Strega da bruciare e molte altre Associazioni.  
Il provvedimento governativo ha iniziato il suo iter parlamentare lo scorso 6 novembre al Senato nelle  Commissioni Affari Costituzionali e Giustizia.   
La Senatrice radicale del Pd, Donatella Poretti, che partecipera' alla manifestazione di Piazza Farnese, ha chiesto in Commissione siano audite, prima di ogni decisione,  decine di organizzazioni che si occupano di volontariato e che sono impegnate sul fronte dei diritti civili delle prostitute.  
   
Sergio Rovasio, Segretario dell'Associazione Radicale Certi Diritti ha dichiarato:  
   
"In tutta Europa, contro il provvedimento del Governo italiano, repressivo e propagandistico, ci sarà una straordinaria mobilitazione che vedrà protagoniste le 'Sex Worker' in molte città e capitali. Dalla presentazione del Disegno di Legge Carfagna sulla prostituzione e con le ordinanze di tanti Sindaci in tutta Italia si è creato un pericoloso clima di intolleranza verso tutte le persone che si prostituiscono. Insieme al ddl si sono avviate campagne politico-mediatiche per alimentare l'allarme sociale e la paura dei cittadini.  Sulle persone socialmente «deboli» (della cui sicurezza non ci si preoccupa), si vuole oggi indirizzare l'insicurezza e la paura della gente facendole diventare il capro espiatorio su cui sfogare le frustrazioni di un Paese che si sta impoverendo in tutti i sensi. Se le risorse utilizzate per questa inutile repressione fossero destinate al dialogo, all'aiuto e al sostegno delle persone piu' deboli e in difficolta', non si alimenterebbe l'odio, la discriminazione e il pregiudzio".   
 **