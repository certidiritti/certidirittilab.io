---
title: 'Sabato 21 aprile a Roma Conferenza nazionale sulla legalizzazione della prostituzione con giuristi, avvocati, prostitute/i e politici'
date: Fri, 20 Apr 2012 20:54:58 +0000
draft: false
tags: [Lavoro sessuale]
---

Roma, 20 aprile 2012

Sabato 21 aprile a Roma, presso la Sala Di Liegro della Provincia di Roma (Via IV Novembre 119/A) si svolgerà (ore 9,30 – 19) la Conferenza nazionale sulla “Legalizzazione della Prostituzione”. L’incontro, che prevede interventi di esperti legali e personalità impegnate sul tema del riconoscimento dei diritti civili delle/dei Sex Workers, sarà suddiviso in diverse sessioni:

\- Diritti e rispetto delle libertà personali;

\- Prostituzione, una storia infinita;

\- Costi economici, aspetti legali e fiscali nella situazione attuale e in una prospettiva di cambiamento;

\- Immigrazione, traffico di essere umani, impatto delle politiche migratorie;

\- Diritti Umani, scenari delle prostituzioni e politiche pubbliche;

\- Le politiche locali a confronto;

\- La parola alle e ai Sex-Workers;

\- Verso il cambiamento: iniziative per la legalizzazione e proposte di legge;

Sono previsti gli interventi, tra gli altri, di:  
Pia Covre, Presidente del Comitato per i Diritti Civili delle prostitute; Matteo Mecacci, Presidente della Commissione Diritti Umani Osce, Deputato Radicale – Pd; Porpora Marcasciano, Presidente del Mit; Prof. Paola Degani, Dipartimento di Scienze politiche, giuridiche e studi internazionali dell’Università di Padova; Lasse Braun, Scrittore – Regista; Maria Gigliola Toniollo, Cgil Nuovi Diritti; Lilli Chiaromonte, Federconsumatori; Gianni Meuti, Sindacato Polizia Silp – Cgil Nazionale; Avv. Alessandro Gerardi, Radicali Roma, Avv. Francesca Re, Phd Candidate Università di Roma Tor Vergata; Claudio Donadel, Coordinatore dell’Unità operativa per la protezione sociale e umanitaria del Comune di Venezia; Enzo Cucco, Presidente Associazione Radicale Certi Diritti; Sandro Simionato, Vice Sindaco Comune di Venezia; Riccardo Magi, Segretario Radicali Roma; Giulia Crivellini, rappresentate Comitato per la delibera di iniziativa popolare al Comune di Milano sulla legalizzazione; Sergio Rovasio, Ass.ne Radicale Certi Diritti; Antonia Monopoli, Peer Educator Ala Milano Onlus; Enrico Salvatori, Comitato Radicali Italiani; Antonio Nigrelli, Piccole Trasgressioni; Maya Checchi, Cybercore; Rita Bernardini, deputato Radicale Pd; Vittoria Franco, Pd; Livia Turco Pd.  
Concluderanno i lavori:

Carla Corso, Comitato Diritti Civili delle prostitute e Marco Pannella, leader dei Radicali.  
 

Durante i lavori è previsto un dibattito e la  Proiezione di: "Ni Coupables ni Victimes"  Videobox a cura di [Betty@sexworkeurope Creazione](mailto:Betty@sexworkeurope%20Creazione) by Sexyshock in collaborazione con Icrse - International Committee on the Rights of Sex Workers in Europe www.sexworkeurope.org  www.ecn.ong/sexyshock        Special guests Scarlot Hariot & Wonder Bra

  
Al seguente link il programma dei lavori:

[http://www.certidiritti.it/conferenza-nazionale-sulla-legalizzazione-della-prostituzione](conferenza-nazionale-sulla-legalizzazione-della-prostituzione)