---
title: 'PD/INNOCENZI: “FESTA PER VOTARE, IN DISCOTECA RACCOLTA FIRME SU EUTANASIA”'
date: Mon, 17 Nov 2008 14:49:19 +0000
draft: false
tags: [Comunicati stampa]
---

Raccolta firme per l’eutanasia, per l’abolizione della ricetta della pillola del giorno dopo e intrattenimento delle drag queen: questo il programma per la festa conclusiva della campagna elettorale di Giulia Innocenzi e di tutti i candidati ai Giovani Democratici che la sostengono. La festa si terrà il 20 novembre alla discoteca romana Loft, in v. di Libetta (Ostiense), dalle ore 22. “Con la mia candidatura ho cercato di avvicinare i giovani alla politica. L’idea di raccogliere le firme e di proporre disegni di legge in discoteca è una nuova forma di coinvolgimento dei ragazzi, in continuità con tutte le forme di comunicazione – dal blog, Facebook, Youtube – che ho utilizzato in questi giorni”. Presenti alcuni parlamentari radicali e diretta con Radio Radicale da mezzanotte in poi.

![retro_od_copia.jpg](http://www.certidiritti.org/wp-content/uploads/2008/11/retro_od_copia.jpg "retro_od_copia.jpg")