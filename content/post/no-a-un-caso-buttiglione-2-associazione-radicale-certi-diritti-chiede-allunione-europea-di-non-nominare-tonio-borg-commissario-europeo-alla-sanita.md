---
title: 'No a un caso Buttiglione 2: Associazione radicale Certi Diritti chiede all''Unione Europea di non nominare Tonio Borg Commissario europeo alla Sanità'
date: Mon, 12 Nov 2012 10:52:45 +0000
draft: false
tags: [Europa]
---

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Roma, 12 novembre 2012

L’Associazione Radicale Certi Diritti si appella al Presidente della Commissione europea José Manuel Barroso, alla commissione sanità ed ambiente del PE ed al governo maltese per rivedere la nomina di Tonio Borg a Commissario europeo.

A seguito delle dimissioni improvvise del Commissario europeo maltese alla salute John Dalli, volute dal Presidente della Commissione europea José Manuel Barroso a seguito di una inchiesta dell’OLAF, il governo dell’isola ha proposto Tonio Borg, ex Ministro agli affari esteri, come Commissario europeo alla sanità. La Commissione ambiente e sanità del PE esaminerà le sue credenziali martedì prossimo.

Tonio Borg è conosciuto per le sue posizioni estremamente conservatrici in materia di divorzio, aborto (Borg è un militante dell’organizzazione anti-abortista “Gift of Life” ed ha tentato di fare inserire il divieto di aborto e la difesa dei diritti dei bambini non nati nella Costituzione maltese, appoggiando una Carta dei diritti, la protezione, la dignità e lo sviluppo dei bambini non nati e dei doveri delle altre persone al riguardo), diritti LGBTI (si è opposto alle unioni civili per le coppie dello stesso sesso), immigrazione (trattamento dei richiedenti asilo ed espulsione di 220 eritrei), ma anche per il suo nazionalismo anti-europeo.

I media europei hanno inoltre pubblicato articoli su un suo presunto coinvolgimento come Ministro degli Esteri nella concessione della residenza a Malta a Rakhat Aliyev, ricchissimo genero del dittatore kazako Nazarbayev ed accusato in seguito dal regime kazako di tortura e coinvolgimento nell’uccisione di due banchieri, nonché di traffico di armi e di droga, corruzione, riciclaggio, furto.

Una serie di NGOs - ILGA-Europe, la Federazione Umanista Europea, Catholics for Choice, organizzazioni per i diritti dei migranti e contro il razzismo - hanno espresso forti preoccupazioni per il fatto che Borg sarà incaricato del portafoglio sensibile della salute, che copre la salute sessuale e riproduttiva e l’AIDS. Inoltre qualunque Commissario europeo partecipa alle decisioni collettive della Commissione, avendo così un potere di intervento in questioni di diritti delle donne e delle persone LGBTI.

Anche nel campo conservatore si sono sollevate preoccupazioni, espresse dal Presidente della Commissione Affari esteri del PE Elmar Brok, che ha chiesto chiarimenti in merito alla questione Aliyev. L’eurodeputato liberale Holger Krahmer, che aveva partecipato al Gay Pride a Malta, è intervenuto esprimendo la sua contrarietà alla nomina di Borg, come pure i deputati liberali nella commissione giustizia ed affari interni, in particolare Cecilia Wikström, che ha definito Borg “un dinosauro”.

L’Associazione Radicale Certi Diritti chiede alle istituzioni europee ed al governo maltese di rivedere la nomina di Borg alla Commissione europea. Sono troppe le ombre che si sono accumulate sulla sua candidatura e ragioni di opportunità politica dovrebbero condurre alla nomina di altri candidati, per evitare un caso Buttiglione 2 o un caso Dalli 2. Perché non nominare invece un eurodeputato?