---
title: 'Certi Diritti: "Inaccettabili le posizioni dell''Ambasciatore eritreo"'
date: Sun, 12 May 2013 08:22:05 +0000
draft: false
tags: [Africa]
---

Comunicato dell'Associazione Radicale Certi Diritti.

Roma, 12 maggio 2013

Apprendiamo che la Farnesina ha convocato l'ambasciatore eritreo per il quale qualsiasi straniero presente sul territorio eritreo ha l'obbligo di rispettare gli usi e i costumi locali e, a maggior ragione, le disposizioni di legge che vietano i rapporti omosessuali.

A tal proposito, Paolo Mannina precisa di non aver violato nessuna legge eritrea e di non aver commesso nessun reato sanzionato dalla legge di quel Paese. Egli, infatti, non è stato accusato di nulla e la sua espulsione non può quindi essere motivata da una violazione della legge eritrea. Mannina sottolinea inoltre che l'interruzione del suo rapporto di lavoro non è stata volontaria poiché egli è stato costretto a rientrare in Italia. Prima della sua partenza ha dovuto sottoscrivere una rinuncia al contratto ma ha fatto allegare al modulo predisposto, una nota in cui spiegava immediatamente al Ministero per gli Affari Esteri che la sua decisione non era libera ma obbligata per non mettere in pericolo la sua incolumità, come peraltro lo stesso ambasciatore italiano gli aveva caldamente consigliato.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, afferma: "Ringraziamo la Farnesina per aver convocato l'Ambasciatore eritreo e auspichiamo che il Ministero degli Esteri insista per avere una motivazione formale del contegno eritreo, poiché espellere un cittadino dipendente dello Stato italiano senza una motivazione ufficiale è gravemente lesivo della dignità non solo del cittadino colpito dal provvedimento, ma dello Stato Italiano stesso. Paolo Mannina è stato vittima di una sorta di espulsione preventiva non motivata da alcuna violazione di legge. Chi decide gli organici delle scuole italiane all'estero? Il governo italiano o quello eritreo con le sue leggi contrarie ai più elementari diritti umani? Auspichiamo che lo Stato italiano sappia difendere i cittadini italiani e i suoi dipendenti ovunque essi si trovino e precisiamo che qualora Paolo Mannina non ottenesse un nuovo incarico potrebbe anche configurarsi la fattispecie di discriminazione indiretta, vietata dalla Direttiva europea 2000/78/CE recepita nell'ordinamento giuridico italiano dal decreto n. 216/2003".