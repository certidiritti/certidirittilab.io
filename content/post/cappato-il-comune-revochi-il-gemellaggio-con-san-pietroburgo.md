---
title: 'Cappato: il comune revochi il gemellaggio con San Pietroburgo'
date: Tue, 02 Oct 2012 12:07:55 +0000
draft: false
tags: [Russia]
---

Dichiarazione di Marco Cappato, Presidente del Gruppo Radicale - federalista europeo al Comune di Milano

Milano, 2 ottobre 2012

Il 29 febbraio scorso, il Parlamento di San Pietroburgo ha approvato una legge che sanziona la cosiddetta “propaganda dell’omosessualità” criminalizzando di fatto qualunque attività o informazione relativa alle persone lesbiche, gay, bisessuali e transessuali e alle relazioni tra persone dello stesso sesso. Nonostante questo, è ancora attivo il gemellaggio tra le città di Milano e San Pietroburgo, che tuttora si traduce in iniziative di collaborazione tra le due città come l'attuale festival del cinema russo in corso a Milano.

Insieme ai capigruppo della maggioranza abbiamo depositato una mozione, predisposta dall'Associazione Radicale Certi Diritti, già approvata in zona 2, per chiedere la revoca del gemellaggio. A tale richiesta si sono uniti, nel giorno del 45° anniversario della firma del gemellaggio, personalità come Moni Ovadia, Lella Costa e della Compagnia dell'Elfo, nei prossimi giorni pubblicheremo i video di Federica Fracassi, Danilo De Biasio, ex direttore di Radio Popolare, Ferruccio Capelli, direttore della Casa della cultura, lo storico Aldo Sabino Giannuli, Serena Sinigaglia del Teatro Ringhiera e tanti altri. Mi auguro che la mozione sia messa all'ordine del giorno del Consiglio e approvata al più presto.

**[I VIDEO DELLA CAMPAGNA >>](http://www.youtube.com/watch?v=6cgpqaGqOwM)**

**[LA CAMPAGNA SU FACEBOOK >>](http://www.facebook.com/Stopalgemellaggio)**