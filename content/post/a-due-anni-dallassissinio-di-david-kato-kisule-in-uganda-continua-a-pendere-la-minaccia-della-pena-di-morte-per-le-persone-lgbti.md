---
title: 'A due anni dall''assissinio di David Kato Kisule in uganda continua a pendere la minaccia della pena di morte per le persone lgbti'
date: Fri, 25 Jan 2013 12:40:40 +0000
draft: false
tags: [Africa]
---

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Roma, 25 gennaio 2013  

Domani, 26 gennaio 2013, ricorre il secondo anniversario dell’assassinio di David Kato Kisule, attivista per i diritti delle persone LGBTI in Uganda e iscritto all’Associazione Radicale Certi Diritti.

Il 4 ottobre 2010 il nome di Kato fu il primo di una lista di 100 nomi che il tabloid ugandese Rolling Stone pubblicò chiedendone l’esecuzione in quanto omossessuali; Kato, con Kasha Jacqueline e Onziema Patience, altri due attivisti ugandesi, denunciarono la rivista per far sì che interrompesse la pubblicazione delle foto e dei nomi di persone omosessuali o presunte tali. Il 3 gennaio 2011 l'Alta Corte di Giustizia si espresse con una sentenza secondo la quale la pubblicazione delle liste della rivista Rolling Stone e la relativa incitazione alla violenza della stessa hanno rappresentato una minaccia alla libertà e ai diritti umani fondamentali di tutte le persone coinvolte, attaccando il loro diritto alla dignità umana e violando il diritto costituzionale alla privacy.

Il 27-28 novembre 2010 partecipò al IV Congresso dell'Associazione Radicale Certi Diritti alla quale si era iscritto.

Il 26 gennaio 2011 attorno alle 14:00 Kato fu aggredito nella sua casa a Bakusa da almeno un uomo che lo ha colpito due volte alla testa con un martello; da settimane David, ma anche molti suoi colleghi ed amici, avevano riferito di una continua escalation di minacce e molestie a seguito della vittoria in tribunale.

Il 2 febbraio 2011 la polizia annunciò l'arresto di Nsubuga Enock reo confesso dell'omicidio, che avrebbe ucciso David perché quest'ultimo non voleva pagarlo per dei favori sessuali; versione confermata dalla polizia ugandese che si è sempre rifiutata di prendere in considerazione possibili altri moventi e di compiere indagini più appropriate, suscitando non molti dubbi anche a livello internazionali.

Tra una decina di giorni, il 4 febbraio, il Parlamento riaprirà e la proposta di legge che prevede, tra l’altro, la pena di morte per le persone LGBTI potrebbe essere votata e approvata, vanificando la lotta di David. Nonostante le dichiarazioni del Primo Ministro ugandese a Milano di non sostenere la proposta di legge, il Ministero delle Finanze ha già dato il nulla osta sull’approvazione della proposta di legge, la cui implementazione non creerebbe problemi economici.

Nel frattempo continuano arresti arbitrari di attivisti LGBTI e raid della polizia negli uffici delle associazioni, rendendo ancora più drammatica la situazione e difficile l’attività per i diritti umani in loco.

Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti, afferma: “I fatti sembrano contraddire le dichiarazioni del Primo Ministro e del Presidente, indicando un sostanziale appoggio del governo alla polizia. Crediamo che il miglior modo per ricordare David, sia mantenere alta l’attenzione sull’Uganda e aiutare il più possibile gli attivisti ugandesi nella loro lotta”.