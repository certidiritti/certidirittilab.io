---
title: 'La partecipazione dell''associazione radicale Certi Diritti al Gay Pride di Palermo, anche in ricordo di David Kato Kisule'
date: Thu, 28 Jun 2012 11:08:54 +0000
draft: false
tags: [Africa]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti

Palermo, 28 giugno 2012

Anche quest’anno una delegazione di Certi Diritti ha partecipato al gay pride di Palermo. Erano  
presenti militanti dalla Sicilia, da Roma, Milano e Bruxelles. I palermitani hanno accolto con affetto e curiosità la manifestazione che ha attraversato il centro storico della città. Erano presenti molti carri e tanti giovani e giovanissimi.  
Completamente insensato si è dimostrato l’allarme lanciato da Frommer's, una delle guide turistiche più vendute degli Usa, che ha sconsigliato ai turisti gay di andare in Sicilia, considerata pericolosa.  
  
Per fortuna ci hanno pensato i palermitani a smentire questa falsità. Forse più che in altre città  
italiane, il gay pride a Palermo si trasforma in una grande festa popolare. Moltissimi giovani gay o etero vi partecipano. Molti si affacciano semplicemente alle finestre o scendono per strada. In particolar modo attraversando piazza Marina e la Vucciria, zone ancora popolari, si nota come vi sia molto interesse da parte della gente. Anche chi la pensa in modo differente, non sembra però scandalizzato. Anzi mostra molta curiosità. Per non parlare delle tante persone che fermavano le Drag Queen’s più spettacolari per farsi fotografare con loro. Colpiva anche la presenza di molti ragazzi del liceo che partecipavano al corteo. Insomma l’atmosfera era semplicemente solare e allegra.  
  
I militanti dell’associazione Radicale Certi Diritti hanno distribuito preservativi e i nuovi volantini con la campagna “Dai corpo ai tuoi diritti” per l’iscrizione all’associazione. Inoltre, hanno portato in corteo lo striscione dell’associazione e le bandiere del partito radicale.  
Da segnalare anche il gay village, che è stato ospitato per una settima nella spettacolare villa  
Pantelleria, splendido esempio di casa di campagna della nobiltà palermitana. La villa che fa parte dei beni che lo stato ha confiscato alla mafia, pur essendo ormai inglobata dalla città, continua a emanare un fascino che solo certe architetture siciliane sanno dare. Al gay village è stato anche presentato Kuchus of Uganda, di Mathilda Piehl. Documentario sull’omosessualità in Uganda.  
  
L’iniziativa è stata organizzata dall’Associazione radicale Certi Diritti e dell’Associazione radicale David Kato-Radicali Palermo. Era presente Elio Polizzotto-segretario del Centro per la di fesa e la promozione dei Diritti Umani.  
Sempre a Palermo, la sera precedente al pride, è stata presentata la biografia non autorizzata di Roberto Formigoni scritta da Lorenzo Lipparini membro del comitato nazionale di Radicali italiani.  
  
Il weekend palermitano è stato ricco ed intenso. La festa finale si è tenuta al Rise Up, discoteca dove si svolgono le serate Lgbt. Il Gay Pride di Palermo si è dimostrato come tutti gli anni una splendida esperienza, sia per la capacità dei palermitani di organizzare una vera festa popolare, sia per la bellezza della città e per il suo mare caraibico che permettono di conciliare l’attivismo politico con un salto nel mare azzurro e con un bicchiere di sangue di Sicilia alla Vucciria.