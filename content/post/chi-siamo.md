---
title: 'Chi siamo'
date: Thu, 27 Mar 2008 19:24:05 +0000
draft: false
tags: [Senza categoria]
---

  
L'associazione radicale Certi Diritti nasce il primo marzo 2008, nella sede di Roma del Parlamento Europeo, come centro di iniziativa politica nonviolenta, giuridica e di studio per la promozione e la tutela dei diritti civili, per la responsabilità e la libertà sessuale delle persone. **[Tutta la sua attività è svolta da volontari e le sue risorse economiche sono quelle degli iscritti e dei contribuenti.](partecipa/iscriviti)**

L'associazione nasce sull'onda dell'indignazione per la palese incapacità del Parlamento della XV^ Legislatura di affrontare e risolvere i tanti problemi irrisolti in materia di libertà e responsabilità sessuale delle persone ed effettiva uguaglianza tra persone di diverso orientamento sessuale. Dedica inoltre la sua attività a **[Makwan](we-love-makwan)**, omosessuale 18enne impiccato dal regime iraniano a causa del suo orientamento sessuale e simbolo dei tanti uomini e donne che ancora subiscono la pena di morte, torture, carcere e discriminazioni in molti paesi del mondo.

**L'associazione nasce dall'esperienza radicale, facendo proprio il metodo nonviolento** che ha caratterizzato la storia delle iniziative libertarie e delle riforme sociali che hanno accompagnato lo sviluppo italiano, tra cui il divorzio, la regolamentazione dell'aborto, l'educazione sessuale, l'obiezione di coscienza, la riforma del diritto di famiglia, la legge per la rettifica dello stato anagrafico delle persone transessuali e si ispira alle battaglie del Fuori, il primo movimento omosessuale italiano, federato sin dai primi anni ’70 al Partito Radicale.   

L'associazione partecipa ai lavori dell'**Intergruppo on Lgbt Rights** del Parlamento Europeo, fa parte ed è attiva in ILGA (International Lesbian and Gay Association) e collabora con l'Ong americana **Global Rights**, **Amnesty International** e **AllOut.org**. Ha un network per lo scambio di informazioni con corrispondenti in tutti i continenti. Dal dicembre 2011 l'Associazione Radicale Certi Diritti è **[soggetto costituente del Partito Radicale Nonviolento Transnazionale Transpartito](http://www.radicalparty.org/)**, lavora a stretto contatto con alcuni eletti e funzionari del Parlamento Europeo e collabora con le associazioni della galassia radicale su iniziative specifiche.

I principali ambiti nei quali l'associazione ha svolto le sue attività sono:  
  

**Riforma del diritto di famiglia**  
L'iniziativa principale in questo ambito è stata quella denominata **["Affermazione civile"](campagne-certi-diritti/itemlist/category/86-affermazione-civile)**, campagna nazionale di iniziativa giuridica, realizzata insieme a ‘Rete Lenford – Avvocatura per i diritti LGBT’,  volta al riconoscimento del matrimonio civile per le coppie dello stesso sesso. La campagna nasce da oltre 30 coppie che hanno chiesto la pubblicazione degli atti di matrimonio negli uffici del proprio comune e una volta ottenuto il rifiuto, in forma di documento ufficiale, impugnarlo in tribunale. Quattro sono le cause rinviate direttamente alla Corte Costituzionale, che il 14 aprile 2010 emette la sentenza **[138/10](tutte-le-notizie/685-le-motivazioni-della-sentenza-della-corte-costituzionale)**[.](tutte-le-notizie/685-le-motivazioni-della-sentenza-della-corte-costituzionale.) La sentenza non riconosce nell’immediato il diritto al matrimonio civile, ma riconosce in modo esplicito che le unioni tra persone dello stesso sesso hanno pari dignità e diritti delle coppie unite in  matrimonio sollecitando il Parlamento a varare una disciplina di carattere generale in materia.  
  
Il silenzio della politica di fronte ai diritti dei cittadini e al monito della Corte costituzionale convince Certi Diritti a lanciare **"Affermazione Civile 2.0"**, che vede l’associazione impegnata su più fronti:  
•    riconoscimento dei singoli diritti in Italia, perché la Corte costituzionale ha chiaramente invitato ogni cittadino discriminato dall'ordinamento italiano a chiedere giustizia in ogni specifico campo, dalla pensione ai diritti del lavoro, dalle tasse pagate alla pensione, dalla retribuzione al TFR, dalla possibilità di avere figli alla successione ereditaria;  
•    riconoscimento del matrimonio per le coppie dello stesso sesso, con due ricorsi presentati nel maggio 2011 alla Corte Europea dei Diritti grazie alla costituzionalista Marilisa D’Amico;  
•    trascrizione in Italia di matrimoni/unioni civili avvenute all'estero;  
•    ricongiungimento familiare per le coppie in cui uno dei due partner non è europeo, perché nonostante l’unione contratta all’estero e nonostante il cittadino non europeo goda già di un  permesso di soggiorno europeo di lungo periodo rilasciato da altri stati europei, in Italia non viene concesso il ricongiungimento familiare, né viene riconosciuto il permesso di soggiorno di lungo periodo rilasciato da altri stati europei (e in teoria validi su tutto il territorio europeo).  
  
L'associazione è inoltre intervenuta su alcuni casi di violazione diretta delle norme europee ed italiane, che son chiaramente discriminatorie, in particolare al Ministro degli Interni, per chiedere che venga  ritirata al più presto la **Circolare Amato n. 55 del 2007  sui  "Matrimoni contratti all’estero tra persone dello stesso sesso"** inviata a tutti i Comuni italiani, in quanto considerato un grave atto discriminatorio e per chiedere che venga modificata una recente circolare in materia di trasferimenti del personale di polizia che discrimina in modo esplicito le coppie formate da persone dello steso sesso.

La campagna di Affermazione Civile si inserisce all'interno di un progetto più ampio per una riforma del diritto di famiglia. Elaborata con altre associazioni, esperti di diritto ed esponenti politici di diversi schieramenti, la proposta è stata pubblicata nel volume **"Amore Civile. Dal diritto della tradizione al diritto della ragione"** a cura di Francesco Bilotta e Bruno De Filippis e nel luglio 2010 è stata depositata, come Proposta di Legge alla Camera dei deputati dagli eletti radicali.

**  
Informazione sessuale, preservativo, soccorso civile**  
Tra gli obiettivi dell’Associazione vi sono anche la promozione di iniziative per campagne di informazione sui temi della sessualità nelle scuole di ogni ordine e grado, per la prevenzione di malattie sessualmente trasmissibili, per la diffusione e l’uso del preservativo e campagne per l’accesso alla pillola del giorno dopo.

  
**Legalizzare la prostituzione**  
Il 21 aprile 2012 con il Comitato per i diritti civili delle prostitute e la Cgil-Nuovi Diritti, l’Associazione organizza a Roma una conferenza nazionale sulla legalizzazione della prostituzione durante la quale viene lanciato un **[manifesto/appello per la decriminalizzazione e la regolamentazione della prostituzione](campagne-certi-diritti/itemlist/category/85-legalizzazione-prostituzione)**. E sono allo studio iniziative di disobbedienza ed affermazione civile per contrastare discriminazioni, soprusi e violenze per le persone che si prostituiscono e i loro clienti

  
**Diritti delle persone transessuali**  
A trent'anni esatti dall'approvazione della **legge 164/82** (primo firmatario della proposta fu Franco De Cataldo) che regola il cambio di nome e di genere per le persone transessuali, l’associazione ha avviato una collaborazione con le associazioni di persone transessuali per definire una agenda comune per portare a compimento le riforme che con la Legge 164 sono state avviate.

  
**Iniziative su temi transnazionali**  
L'Associazione ha ospitato nel suo Congresso del 2010 il militante omosessuale ugandese **David Kato Kisule** che, da iscritto, lavorò per coinvolgerci in una serie di iniziative specifiche per la gravissima situazione in cui vivono le persone omosessuali in Uganda e nella fascia subsahariana dell’Africa. David poco tempo dopo è stato ucciso a seguito di una impressionante campagna di odio alimentato da organizzazioni religiose.  
Anche in conseguenza di questo fatto gravissimo, abbiamo deciso di cercare di intervenire in modo più concreto sul tema della tutela della libertà sessuale nel mondo. Cercando di individuare un obiettivo che fosse adeguato alle nostre possibilità e che potesse interagire al meglio con il **PRNTT**.  
Abbiamo quindi, dato vita a un **network internazionale di esperti e attivisti per i diritti umani** in vari paesi, tra cui Israele, Albania, Stati Uniti, Canada, Cina, Argentina, Belgio, Germania, Tanzania e Uganda.  
L'impegno sull'Uganda continua anche grazie all’avvocato **John Francis Onyango**, (ex difensore di David) da dicembre presidente onorario dell’Associazione.  
Grazie alla collaborazione dei deputati e senatori radicali il Ministro degli esteri italiano risponde in più occasioni agli appelli dell’associazione per le leggi omofobe in Nigeria e Camerun e per i gravi fatti omofobi in Russia.      
Il Consiglio di Zona 2 di Milano approva la mozione presentata dal vicepresidente della Zona Yuri Guaiana, del Gruppo Radicale-Federalista Europeo, per la **revoca del patto di gemellaggio tra Milano e San Pietroburgo**. La stessa mozione è stata presentata anche al Consiglio Comunale dal consigliere radicale Marco Cappato ed è stata firmata da tutti i capigruppo della maggioranza.  
  

  
**Renditi anche tu protagonista delle nostre battaglie di libertà e di uguaglianza.** Abbiamo gruppi di volontari in diverse città italiane. L'elenco completo lo trovi nel nostro sito.  
  
**[iscriviti](partecipa/iscriviti)**  
  
Nella tradizione radicale, per noi "prendere la tessera" non ha valenza identitaria, etnica o di appartenenza: molto più civicamente, significa dire: **"quest'anno io condivido i vostri obiettivi e li faccio miei. L'anno prossimo vedremo."**

Tutto **il nostro lavoro è volontario** e solo dal canale dell’autofinanziamento possiamo avere le risorse economiche e l'aiuto per promuovere ulteriori iniziative. Abbiamo gruppi di volontari in diverse città italiane.