---
title: 'La diversità non è un valore. E'' l''uguaglianza il valore della diversità'
date: Thu, 02 Dec 2010 13:36:35 +0000
draft: false
tags: [Politica]
---

![diversità e uguaglianza](http://www.certidiritti.org/wp-content/uploads/2010/12/diversità-e-uguaglianza-e1540644632285.png)
-------------------------------------------------------------------------------------------------------------------------------

Diversità e differenza sono due opposti.
----------------------------------------

**Di Pier Paolo Segneri** \- Si discute molto, da anni, come pure in questi ultimi giorni, del significato e dell’importanza della “diversità” che, però, non va mai confusa con la “differenza”. Sono due parole tra loro in contrapposizione. La discussione sulla diversità è interessante perché è un dibattito che riguarda tutti e che investe l’attualità presente e futura del pensiero liberale e libertario. L’uguaglianza è la lotta all’ingiustizia. **Non si può comprendere la diversità, perciò, senza prima definire l’uguaglianza.** Perché i due concetti sono tra loro uniti e indissolubili.

L’articolo 3 della nostra Carta costituzionale recita: “_Tutti i cittadini hanno pari dignità sociale e sono eguali davanti alla legge, senza distinzione di sesso, di razza, di lingua, di religione, di opinioni politiche, di condizioni personali e sociali_”. Insomma, **tutti gli uomini e le donne nascono tra loro uguali. Sono uguali.** Al di là della famiglia di appartenenza, al di là dell’etnia, della religione, degli orientamenti sessuali e del ceto sociale. Siamo tutti uguali davanti alla Legge e di fronte al Mistero. **L’uguaglianza implica, quindi, pari dignità e pari opportunità.** Senza distinzioni dovute alla ricchezza, all’opinione politica, alla nazionalità o al censo. L’uguaglianza degli uomini e delle donne, insomma, riguarda ciascuno di noi. E non guarda al colore della pelle, degli occhi o dei capelli. La razza umana è una e una soltanto. Il genere umano è uno solo. Senza eccezioni. Senza deroghe. Senza pregiudizi.

Nella _Dichiarazione Universale dei Diritti dell’Uomo_ si legge: “_Tutti gli uomini nascono liberi ed uguali in dignità e diritti. Essi sono dotati di ragione e di coscienza e devono agire gli uni verso gli altri in spirito di fratellanza_”. Ma **gli uguali sono diversi**, per definizione. La diversità di cultura, di carattere, di gusti, di attitudini e di ingegno sono l’espressione evidente della nostra uguaglianza. Sono il segno che **l’uguaglianza vive nella diversità**. E che dobbiamo trovare non dei valori condivisi, come si sente ripetere un po’ da ogni parte, ma trovare la convivenza anche di valori diversi: valori che ci permettano di convivere nonostante la diversità, di vivere insieme nonostante e grazie alle nostre diversità.

![XII CONGRESSO di Certi Diritti. "Non possiamo aspettare i tempi del potere". A Milano dal 23 al 25 novembre.](http://www.certidiritti.org/wp-content/uploads/2018/10/MIKES-LEATHErBoots.png)

**[LEGGI IL PROGRAMMA DEL CONGRESSO](https://goo.gl/forms/24drtxR5XBgnQvqx2)**
------------------------------------------------------------------------------

Siamo uguali perché siamo diversi. Abbiamo diversità e non differenze. Nessuna differenza è accettabile perché le differenze producono disuguaglianza. La diversità, invece, si nutre vicendevolmente e reciprocamente della diversità degli altri. E così, mentre **le differenze dividono e creano disuguaglianze, la diversità unisce ed arricchisce la collettività**. Il principio di uguaglianza, perciò, tutela i diversi e combatte le differenze. L’uguaglianza è sinonimo di coesione e di diversità, cioè l’esatto opposto di una visione omologante del tessuto sociale e degli uomini.

Gli omologhi sono coloro che non hanno diversità, ma vivono appiattiti e schiacciati nella loro identità e identicità. Sempre identica a se stessa. Piena di differenze, ma omologa. **Essere diversi nell’uguaglianza rende forti e omogenei.** Mentre la mancanza di diversità rende deboli e omologhi. L’omologazione crea la massa e “fare massa” non è un concetto positivo, nemmeno in fisica. C’è una vecchia battuta di Woody Allen che dice: “_Non vorrei mai appartenere ad un club che accettasse tra i suoi membri uno come me_”. E’ l’elogio della diversità e, al medesimo tempo, è anche un elogio al principio di uguaglianza. Perché se l’essere diverso conduce all’esclusione, allora si crea una differenza, cioè un’ingiustizia.

L’uguaglianza è inclusiva e include dentro di sé le varie diversità, le disuguaglianze sono escludenti e, infatti, escludono i diversi. Vasco Rossi canta: “_Mi ricordo che mi si escludeva per ragioni che oggi fanno solo ridere_”. **Il principio di uguaglianza non ammette differenze.** L’uguaglianza è la lotta alle ingiustizie e alle differenze. E la lotta alle ingiustizie non accetta i veti sulle persone o sui popoli, non permette esclusioni arbitrarie ed è incompatibile con qualsivoglia oscurantismo, fanatismo o proibizionismo. L’uguaglianza è un diritto. Anzi, è il Diritto. E chi non rispetta i diritti degli altri perde automaticamente i propri.

La lotta all’ingiustizia è la lotta per il rispetto della persona, per il rispetto dell’uguaglianza delle persone anche nella loro diversità, per il riconoscimento dei diritti e, allo stesso tempo, dei doveri di ciascuno. Ma **la diversità non è un valore, è l’uguaglianza il valore della diversità**.