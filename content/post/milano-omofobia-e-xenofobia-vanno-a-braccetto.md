---
title: 'MILANO, OMOFOBIA E XENOFOBIA VANNO A BRACCETTO.'
date: Tue, 01 Dec 2009 16:32:42 +0000
draft: false
tags: [Senza categoria]
---

**I RADICALI: CITTA' ORMAI INCIVILE, SERVONO POLITICHE DI INTEGRAZIONE E RISPETTO**

**_Dichiarazione di Francesco Poirè, segretario dell'Associazione Enzo Tortora - Radicali Milano e membro dell'Associazione Radicale Certi Diritti:_**

Mentre al Comune di Milano si fa a gara a chi la spara più grossa su moschee e immigrazione, in giro per la città aumentano i casi di immigrati irregolari, vittime di malattie e violenza, che rinunciano alle cure nel timore di essere espulsi, e le aggressioni intolleranti nei confronti di omosessuali, picchiati nell'indifferenza generale.

Alla nostra Associazione si sono già rivolte una donna extracomunitaria irregolare dopo aver subito uno stupro di gruppo e, oggi, un italiano che ha soccorso un cittadino irregolare con probabile TBC e che sta cercando di convincerlo a farsi curare. Abbiamo indirizzato queste persone in luoghi dove l'assistenza sanitaria, psicologica e legale è garantita a chiunque. Alla donna abbiamo consigliato il centro Soccorso Violenza Sessuale della Mangiagalli, che dal 1996 svolge il suo servizio di aiuto alle donne vittime di questo abominevole reato, al secondo abbiamo consigliato il NAGA, che dal 1987 promuove e tutela i diritti di tutti i cittadini stranieri nonché dei nomadi, senza discriminazioni.

A queste segnalazioni si aggiungono, sul versante dell'intolleranza, i casi di almeno quattro omosessuali picchiati per strada negli ultimi due mesi. L'ultimo, reso noto oggi dalla stampa, è ancora più grave: un uomo di 47 anni seguito all'interno di un McDonald's e picchiato da tre ragazzi italiani sotto gli occhi degli avventori, che hanno assistito immobili al pestaggio.  
Forse se a Milano si parlasse meno di referendum sulla moschea, sgomberi di rom e senzatetto e si facessero più politiche di integrazione e rispetto delle minoranze si potrebbe vivere in una città civile, cosa che ormai Milano non è più.