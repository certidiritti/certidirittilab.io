---
title: 'CORTE EUROPEA DIRITTI DELL''UOMO: COPPIA GAY È FAMIGLIA'
date: Fri, 25 Jun 2010 06:13:31 +0000
draft: false
tags: [CEDU, Comunicati stampa, Corte europea dei diritti dell'uomo, famiglie gay, MATRIMONIO, matrimonio dello stesso sesso, unioni gay]
---

**La Corte Europea dei Diritti dell'Uomo** **riconosce le famiglie gay e parifica le coppie di fatto omosessuali ed eterosessuali.**

Comunicato Stampa dell’Associazione Radicale Certi Diritti e due analisi di studiosi del Diritto.

Ieri, la Corte europea dei diritti dell'uomo ha pronunciato la sentenza nel caso di Schalk e Kopf c. Austria.  La Corte ha riconosciuto "la rapida evoluzione di atteggiamenti sociali nei confronti delle coppie dello stesso sesso in molti Stati membri e un numero considerevole di Stati  hanno legiferato per il riconoscimento legale".

La Corte, nella sentenza, afferma che "una coppia convivente dello stesso sesso che vive in un partenariato stabile, rientra nel concetto di 'vita di famiglia', così come per il rapporto di una coppia di sesso diverso nella stessa situazione", avvalorando la tesi del Prof. Robert Wintemute intervenuto presso la Corte per conto di ILGA-Europe.

Ciò rappresenta un cambiamento rilevante per la Corte: è la prima volta infatti che la Corte europea dei Diritti Umani si riferisce alle unioni tra persone dello stesso sesso come famiglie richiamandosi all'articolo 8 (diritto al rispetto della vita privata e familiare) della Convenzione Europea dei Diritti dell'Uomo.

La Corte ha anche fatto un importante riferimento alla Carta dei diritti fondamentali dell'Unione Europea e ha sottolineato che l'articolo 9, relativo al diritto di sposarsi, non fa riferimento a uomini e donne. La Corte ha poi detto che "il diritto al matrimonio sancito dall'articolo 12 \[della convenzione\] non deve essere in alcun modo considerato limitatamente al matrimonio tra due persone di sesso opposto".

L’Associazione Radicale Certi Diritti considera  molto importanti le decisioni della Corte Europea, poiché rafforzano le iniziative giurisdizionali che ben presto verranno avviate – in collaborazione con Rete Lenford – Avvocatura LGBT e con il “Comitato Sì lo voglio”  per continuare le iniziative di Affermazione Civile che ad Aprile hanno portato alla sentenza della Corte Costituzionale 138/2010.

La sentenza di Strasburgo  ha però anche sostenuto che l'Austria non ha violato l'articolo 12 (diritto al matrimonio) non permettendo a una coppia dello stesso sesso di sposarsi.   Tre dei sette giudici erano del parere che vi sia stata una violazione dell'articolo 14 (divieto di discriminazione) in combinato disposto con l'articolo 8 (diritto al rispetto della vita privata e familiare) e che l'Austria aveva l'obbligo di introdurre un partnership legge immatricolati prima del 1 gennaio 2010.

Così come la Corte costituzionale italiana, la Corte Europea dei diritti dell’uomo si è mostrata timida di fronte al tema del diritto al matrimonio, lasciando libero campo all’iniziativa legislativa nazionale e – così facendo – aprendo la strada ad una serie di problemi e contraddizioni in tema di libera circolazione e di uguaglianza dei cittadini di fronte alla legge”.

Resta agli stati la libertà di scelta sulle unioni gay  
Il Sole 24 Ore - ‎10 ore fa‎  
Il riconoscimento del diritto al matrimonio per le coppie omosessuali non passa per Strasburgo. La Corte europea, infatti, con la sentenza del 24 giugno ...Corte europea dei diritti umani e matrimoni omosessuali: un'analisi

  
[http://www.ilsole24ore.com/art/norme-e-tributi/2010-06-30/resta-stati-liberta-scelta-080715.shtml?uuid=AYj6ha3B](http://www.ilsole24ore.com/art/norme-e-tributi/2010-06-30/resta-stati-liberta-scelta-080715.shtml?uuid=AYj6ha3B)

Libertiamo.it - ‎29/giu/2010‎  
\- È difficile cogliere le evoluzioni sociali e culturali attraverso la legislazione. Il “consenso” generale su una determinata questione che può emergere da ...

[http://www.libertiamo.it/2010/06/29/corte-europea-dei-diritti-umani-e-matrimoni-omosessuali-unanalisi/](http://www.libertiamo.it/2010/06/29/corte-europea-dei-diritti-umani-e-matrimoni-omosessuali-unanalisi/)