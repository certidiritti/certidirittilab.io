---
title: 'MORTE TRANSESSUALE CIE DI MILANO: UNA "SENZA NOME" CHE NON MERITA RISPOSTA'
date: Sat, 26 Dec 2009 09:21:00 +0000
draft: false
tags: [Comunicati stampa]
---

**Dichiarazione di Sergio Rovasio, Segretario dell’Associazione radicale Certi Diritti**

26 dicembre 2009

Una persona per ora senza  nome, che aveva la sola colpa di essere una transessuale brasiliana, rinchiusa da tre giorni nel Centro di identificazione ed espulsione di Milano, è morta suicida ieri, 25 dicembre dell’Anno Domini 2009.

La notizia, ovviamente, nella serenità e nella gioia del Natale, è una di quelle che dà tanto fastidio. I parlamentari radicali, sicuramente, presenteranno su questa drammatica vicenda un’interrogazione parlamentare, che probabilmente si aggiungerà alle tante altre presentate e rimaste senza risposta. Come inascoltata, evidentemente, è rimasta ogni sua richiesta d’aiuto.