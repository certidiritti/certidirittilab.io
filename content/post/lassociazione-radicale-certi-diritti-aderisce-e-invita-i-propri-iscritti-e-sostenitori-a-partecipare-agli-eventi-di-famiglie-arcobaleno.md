---
title: 'L''Associazione Radicale Certi Diritti aderisce e invita i propri iscritti e sostenitori a partecipare agli eventi di Famiglie Arcobaleno'
date: Sat, 08 May 2010 14:41:11 +0000
draft: false
tags: [Comunicati stampa]
---

**Festa delle Famiglie 2010**

**Un'iniziativa di visibilità e di condivisione festosa**

L'idea della "Festa delle famiglie", la Festa di TUTTE le Famiglie, è nata nella primavera del 2009, mentre preparavamo il Genova Pride. Abbiamo avuto voglia di pensare a una festa in cui tutti si sentissero inclusi, in cui fossero presenti TUTTI I TIPI DI FAMIGLIA, coscienti del fatto che la famiglia è là dove è presente l'amore.

[Scarica il comunicato](http://www.certidiritti.org/wp-content/uploads/2010/05/Comunicato_Festa_Famiglie_2010.pdf)

L'anno scorso è stato un banco di prova, ci siamo trovati a Milano, Roma e Avellino, in parchi pubblici, a festeggiare insieme ai nostri parenti, ai nostri amici e a tutti coloro con noi solidali. Questa prima esperienza ci ha dato voglia di ampliare e approfondire il concetto.

La Festa delle Famiglie vuole essere un momento di aggregazione tra tutte le persone che si riconoscono in un'idea di società inclusiva e in un concetto "plurale" di realtà familiari.

L'iniziativa parte dall'associazione Famiglie Arcobaleno, che riunisce genitori e aspiranti genitori omosessuali, ma vuole coinvolgere tutta la rete delle associazioni LGBTQ, le associazioni familiari, e tutte le persone che, almeno per un giorno, vogliono confrontarsi e avvicinarsi alla comunità delle persone lesbiche, gay, trans, bisessuali.

Domenica 23 maggio, la "Festa delle famiglie" sarà un evento di gioia e di vita, che ci auguriamo possa coinvolgere tutte le realtà associative del territorio.

La proposta è un grande avvenimento all'aperto, in un parco cittadino, dove fare un picnic collettivo invitando giocolieri, maghi, artisti che divertono i nostri bimbi e attirino le famiglie "tradizionali" presenti in quel luogo.

Perché noi pensiamo che è solo insieme agli altri che possiamo abbattere il muro dell'odio e del pregiudizio, e se siamo obbligati al silenzio sulla stampa, ci riprendiamo la parola in un altro modo, obbligando gli altri a vederci, a parlarci, a prendere atto delle nostre vite, dei nostri corpi e delle nostre voci.

Perciò non dovranno mancare i nostri simboli e un banchetto dove ognuno possa attingere informazioni.

La festa sarà annuale, nell'ottica della visibilità, della vitalità e della condivisione; si svolgerà in contemporanea in più città italiane, grandi e piccole, e sarà nello stesso tempo un momento di grande gioia di vivere, ma anche l'espressione delle nostre rivendicazioni.

Festa delle Famiglie edizione 2010

L'iniziativa ha carattere nazionale, in collaborazione con le diverse realtà locali e nazionali dell'associazionismo prevalentemente, ma non solo, LGBTQI: la Festa delle famiglie ha come obiettivo la visibilità della realtà delle famiglie omogenitoriali presso la società civile, vuole essere un'occasione di incontro per noi e i nostri figli, un momento informativo e divulgativo, di gioia e condivisione. Ma si propone anche come momento di aggregazione fra la nostra e le altre associazioni presenti sul territorio, un invito rivolto dalla comunità LGBTQI alle famiglie "tradizionali" a condividere una giornata di festa.

Questo anno, in particolare, la Festa delle famiglie vuole essere l'annuncio ufficiale dell'apertura del Pride Nazionale. Tutti a Napoli il 26 giugno sarà il sottofondo e l'invito rivolto alla comunità.

L'avvenimento sarà pubblicizzato sulla stampa locale e nazionale e invitiamo tutti alla massima divulgazione e promozione dell'evento.

Il ritrovo sarà reso visibile con striscioni e palloncini, di Famiglie Arcobaleno e delle associazioni LGBTQI che vorranno aderire e sostenere l'iniziativa.

Sarà presente un banchetto per la distribuzione di materiale informativo e divulgativo: brochure, volantini, adesivi e altri gadget disponibili.

Durante la giornata verranno proposte attività di animazione e spettacolo a cura di artisti di strada, musicisti, ecc., finalizzate a intrattenere i bambini presenti e ad attirare l'attenzione dei passanti.

Le città dove festeggeremo sono:

\- Ferrara

Piazza XXIV Maggio, dalle ore 15:00

\- Firenze

da definire

\- Mestre

Parco San Giuliano (da definire)

\- Milano

Largo Marinai d'Italia, dalle 11:00 alle 18:00

lato via Anfossi - vicino allo spazio giochi dei bimbi

Giocoleria e intrattenimento per i bambini

\- Napoli

Riviera di Chiaia (animazione e gazebo), dalle ore 10:00 alle 14:00

Picnic sulla spiaggia antistante dalle 14:00 alle 16:00

Corteo da via Caracciolo a Castel dell'Ovo

(per la promozione del Pride del 26 giugno) dalle 16:00 alle 19:00

\- Roma

Villa Ada o villa Borghese (da definire) dalle 11:00 alle 18:00

\- Torino

da definire

Le associazioni LGBTQI locali e nazionali sono invitate ad aderire e partecipare all'evento. Sono invitate ad inviare una mail di adesione all'evento all'indirizzo [aderire@famigliearcobaleno.org](mailto:aderire@famigliearcobaleno.org) allegando il logo dell'associazione.

All'URL [http://www.famigliearcobaleno.org/FestaFamiglie2010.htm](http://www.famigliearcobaleno.org/FestaFamiglie2010.htm) sono pubblicati tutti i dettagli per ogni città, le locandine da stampare, il logo e i banner per pubblicizzare l'evento.

Famiglie Arcobaleno

Associazione Genitori Omosessuali

www.famigliearcobaleno.org