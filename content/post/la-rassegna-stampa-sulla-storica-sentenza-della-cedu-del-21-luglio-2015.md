---
title: 'La rassegna stampa sulla storica sentenza della CEDU del 21 luglio 2015'
date: Wed, 22 Jul 2015 11:05:40 +0000
draft: false
tags: [Diritto di Famiglia]
---

[![Judges-of-the-European-co-007](http://www.certidiritti.org/wp-content/uploads/2015/07/Judges-of-the-European-co-007.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/07/Judges-of-the-European-co-007.jpg) Le interviste e gli articoli di giornale sulla [**storica sentenza della Corte EDU**](http://www.certidiritti.org/2015/07/21/cedu-certi-diritti-leuropa-accoglie-le-nostre-tesi-e-condanna-litalia-per-violazione-del-diritto-al-rispetto-della-vita-privata-e-familiare-delle-coppie-dello-stesso-sesso/ "Leggi di più") sui diritti delle coppie e delle famiglie dello stesso sesso. **Vuoi aiutarci a portare avanti le nostre battaglie e a raggiungere nuove vittorie come questa? Il tuo sostegno è fondamentale, dacci una mano con la tua [iscrizione](http://www.certidiritti.org/iscriviti/) o con un [contributo libero](http://www.certidiritti.org/donazioni/)!**

**La conferenza stampa con Yuri Guaiana, Marco Cappato, le coppie ricorrenti e i loro legali** **L'intervista a Radio Radicale del Segretario dell'Associazione Radicale Certi Diritti Yuri Guaiana**

 

**"Tutta la città ne parla" , Radio3, tra gli intervenuti Giammario Felicietti (uno dei ricorrenti di Certi Diritti) e Alexander Shuster (uno dei legali dei ricorrenti)**

[![play-i](http://www.certidiritti.org/wp-content/uploads/2015/07/play-i-150x150.png)](http://www.radio3.rai.it/dl/portaleRadio/media/ContentItem-13ddcb57-9e8d-40a3-820a-8277aa133d29.html#)

 

**Siti web**

[Diritti civili, la battaglia radicale premiata in Europa](http://news.google.com/news/url?sr=1&sa=t&ct2=it%2F0_0_s_0_0_t&usg=AFQjCNEE7vqkLNGviQjtcjdZ9QO5-LZydw&did=3a52436f81a5b6ad&sig2=51dE1AIZP2fqxPaUt2XuIg&cid=52779501203815&ei=FKKvVbixIYL31Ab61YmQCw&rt=STORY&vm=STANDARD&url=http%3A%2F%2Filmanifesto.info%2Fdiritti-civili-la-battaglia-radicale-premiata-in-europa%2F)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

il manifesto

L'Associazione radicale “**Certi Diritti**”, nata per eliminare queste odiose violazioni, dal 2008 ha lanciato la campagna “Affermazione Civile” che, dopo aver portato alla sentenza della Corte Costituzionale 138/2010 con la quale si è affermata «la **...**

[Coppie stesso sesso: Corte EDU condanna Italia Pieno **...**](http://www.tellusfolio.it/index.php?prec=%2Findex.php&cmd=v&id=18886)
------------------------------------------------------------------------------------------------------------------------------------

Tellus Folio

Per conto dell'associazione radicale **Certi Diritti** abbiamo depositato un amicus curiae nel procedimento che oggi ha determinato una nuova sentenza di condanna nei confronti dell'Italia (cit. par. 144 e seguenti). Abbiamo evidenziato quanto l'Italia **...**

[Unioni civili: relazione pronta, in aula il 6 agosto](http://www.gay.it/news/Unioni-civili-relazione-voto-senato)

Gay.it (Comunicati Stampa) (Blog)

"È importante notare - sottolinea Yuri Guaiana, segretario di **Certi Diritti**, ... diritto alla vita famigliare e gli stessi diritti e doveri delle coppie sposate".

[Condannati dalla Corte dei Diritti dell'Uomo: "Riconoscete coppie gay"](http://www.gay.it/news/Condanna-Corte-dei-Diritti-dell-Uomo-coppie-gay)
================================================================================================================================================

Gay.it (Comunicati Stampa) (Blog)