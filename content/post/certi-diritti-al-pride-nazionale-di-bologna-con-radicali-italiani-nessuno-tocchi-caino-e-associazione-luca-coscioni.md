---
title: 'Certi Diritti al Pride nazionale di Bologna con Radicali Italiani, Nessuno Tocchi Caino e Associazione Luca Coscioni'
date: Sat, 09 Jun 2012 08:24:30 +0000
draft: false
tags: [Politica]
---

L'Associazione radicale Certi Diritti a Bologna insieme a dirigenti e militanti che si battono per i diritti civili e umani. Saranno distribuiti migliaia di cartoline e preservativi per rimarcare l’impegno dell’Associazione sui temi della liberazione sessuale che il fondamentalismo clerical-partitocratico continua negare ai cittadini italiani.

Roma - Bologna, 9 giugno 2012

Comunicato Stampa dell'Associazione Radicale Certi Diritti

L’Associazione Radicale Certi Diritti partecipa oggi al Gay Pride, la grande manifestazione nazionale per i diritti civili e umani che si svolgerà  a Bologna. Saranno presenti insieme  al Segretario Yuri Guaiana e il Tesoriere Giacomo Cellottini, insieme alle Associazioni Radicali di Bologna, dirigenti e militanti Radicali provenienti da tutta Italia.

Da quando è nata l’Associazione Radicale Certi Diritti si batte per il superamento delle diseguaglianze e per il riconoscimento di diritti civili finora negati alle persone lesbiche, gay, bisessuali e transessuali. Durante la manifestazione verranno distribuiti volantini e preservativi con il logo www.certidiritti.org  per rimarcare l’impegno dell’Associazione sui temi della liberazione sessuale che il fondamentalismo clerical-partitocratico continua negare ai cittadini italiani.

Tra le richieste dell’Associazione: il ritiro della Circolare Amato del 2007 che discrimina le coppie omosessuali sposate all’estero e ne impedisce il loro riconoscimento in Italia, sblocco del veto da parte dell’Italia della Direttiva antidiscriminazione della Commissione Europea, sostegno alla proposta Onu di depenalizzazione dell’omosessualità in tutto il mondo, campagne di informazione sui temi della sessualità  nelle scuole di ogni ordine e grado, maggiori tutele legislative per le persone transessuali, legalizzazione della prostituzione. anche maschile, regolamentazione delle unioni civili e del matrimonio tra persone dello stesso sesso, riforma del diritto di famiglia,  sostegno e promozione della campagna di Affermazione Civile (iniziative legali contro l’Italia che nega diritti alle coppie conviventi), sostegno alle Risoluzioni del Parlamento Europeo e del Consiglio d’Europa contro ogni forma di discriminazione, anche nei paesi dell’Est e per l’affermazione dei diritti civili in tutti i paesi membri, iniziative transnazionali per la difesa e la promozione dei diritti civili in Africa e nei paesi totalitari e teocratici, maggiore laicità contro le ingerenze clerical-talebane negli affari interni dell’Italia.

Al Gay Pride nazionale di Bologna hanno aderito e parteciperanno iscritti e  sostenitori di Radicali Italiani, Nessuno Tocchi Caino e dell’Associazione Luca Coscioni.