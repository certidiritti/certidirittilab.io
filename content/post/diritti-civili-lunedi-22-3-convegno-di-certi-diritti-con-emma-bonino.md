---
title: 'DIRITTI CIVILI: LUNEDI'' 22-3 CONVEGNO DI CERTI DIRITTI CON EMMA BONINO'
date: Sat, 20 Mar 2010 09:33:47 +0000
draft: false
tags: [Comunicati stampa]
---

**DIRITTI CIVILI: DALLA COSTITUZIONE AI TRATTATI EUROPEI. LUNEDI 22 MARZO A ROMA CONVEGNO DI GIURISTI CON EMMA BONINO E SERGIO ROVASIO.**

**Lunedì 22 marzo, dalle ore 16, si svolgerà a Roma, presso la sede dei radicali, Via di Torre Argentina, 76 un convegno di giuristi sul tema ‘Diritti Civili: dalla Costituzione ai Trattati europei. Le politiche di Riforma per la Regione’.**

**DIRITTI CIVILI: DALLA COSTITUZIONE AI TRATTATI EUROPEI LE POLITICHE DI RIFORMA PER LA REGIONE**

**Conferenza promossa dall’Associazione Radicale Certi Diritti**

**Lunedì 22 marzo, ore 16, Via di Torre Argentina, 76 - Roma**

**con**

**Emma Bonino**, candidata Presidente della Regione Lazio

**Sergio Rovasio**, Segretario Associazione Radicale Certi Diritti e Candidato tra i capilista della Lista Bonino-Pannella alla Regione Lazio

**Dall’Europa dei diritti alle Regioni delle Opportunità**: Enzo Cucco, Direttore Fondazione Sandro Penna di Torino e candidato della Lista Bonino-Pannella alla Regione Piemonte;

**I diritti delle famiglie non matrimoniali, normativa costituzionale ed europea**, Bruno de Filippis, Giurista;

**Le politiche di promozione dei Diritti Civili per le Regioni**, Avvocato Giuseppe Rossodivita, candidato tra i capilista della Lista Bonino-Pannella alla Regione Lazio;

**I diritti contesi:** Marilisa D’Amico, Professore Ordinario di Diritto Costituzionale Università di Milano;

**La tutela sovranazionale delle coppie gay**: Mario Patrono, Professore Ordinario di Diritto Costituzionale Università La Sapienza di Roma;

**La campagna di Affermazione Civile:** Avvocato Antonio Rotelli, Presidente di Avvocatura Lgbt, Rete Lenford;

**Diritti civili e Laicità delle Istituzioni**: Avvocato Francesco Bilotta, Università di Udine;

**A seguire confronto tra gli esponenti della Comunità lgbt(e) e associazioni per la difesa e la promozione dei diritti civili.**