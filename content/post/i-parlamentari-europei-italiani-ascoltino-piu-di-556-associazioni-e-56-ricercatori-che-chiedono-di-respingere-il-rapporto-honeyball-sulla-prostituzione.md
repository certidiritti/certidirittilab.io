---
title: 'I Parlamentari Europei Italiani ascoltino più di 556 associazioni e 56 ricercatori che chiedono di respingere il Rapporto Honeyball sulla prostituzione'
date: Fri, 21 Feb 2014 10:19:09 +0000
draft: false
tags: [Lavoro sessuale]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti e del Comitato per i Diritti Civili delle Prostitute.

Roma, 21 febbraio 2014

L'Associazione Radicale Certi Diritti e il Comitato per i Diritti Civili delle Prostitute sono tra le più di [554 associazioni europee](http://www.sexworkeurope.org/node/488) rappresentanti la società civile che, con 56 accademici e ricercatori, chiedono al Parlamento Europeo di respingere il rapporto di Mary Honeyball, Parlamentare Europea londinese, che sarà discusso in una sessione a Strasburgo il prossimo 25 febbraio e che promuove la criminalizazione dei clienti delle lavoratrici e dei lavoratori del sesso.

L'impressionante numero di 554 NGO e associazioni così come 56 fra accademici e ricercatori di scienze sociali, scienze politiche e di salute pubblica hanno firmato lettere indirizzate ai Parlamentari Europei chiedendo di respingere un rapporto presentato dalla parlamentare Mary Honeyball che chiede agli Stati membri di avallare la criminalizazione dei clienti dei lavoratori del sesso.

La lettera dalle NGO, presentata dal Comitato Internazionale per i Diritti dei sex workers in Europa ICRSE, una rete che rappresenta 59 organizzazioni in Europa e Asia Centrale e di cui fa parte anche il Comitato per i Diritti Civili delle Prostitute. Per ICRSE il rapporto confonde la tra lavoro sessuale e tratta, non si preoccupa della salute dei sex workers e non è basato su prove scientifiche.

Luca Stevenson, coordinatore di ICRSE commenta: «Il modello svedese di criminalizazione dei clienti non solo è inneficace per ridurre la prostituzione e la tratta, ma è anche pericoloso per le/i sex workers. Infatti aumenta lo stigma che è la maggiore causa di violenza contro di noi. È una politica fallimentare denunciata da tutte le organizzazioni di sex workers e da molte organizzazioni di donne, LGBT e di migranti, così come da molti organismi delle Nazioni Unite».

Tra i firmatari vi sono organizazioni per i diritti dei lavoratori del sesso ma anche molti gruppi per i diritti delle donne come l'International Planned Federation, una rete di 40 membri in Europa e il National Council of German Women's Organization, che rappresenta 50 organizzazioni femminili in Germania. In Italia è stato sottoscritto, oltre che dall'Associazione Radicale Certi Diritti e dal Comitato per i Diritti Civili delle Prostitute, anche da Equality Italia, da Arcigay e da altre ancora.

Pia Covre e Yuri Guaiana, rispettivamente presidente del Comitato per i Diritti Civili delle Prostitute e segretario dell'Associazione Radicale Certi Diritti, commentano: «Noi pensiamo che la sistematica criminalizzazione degli acquirenti di sesso non porterà i risultati che i sostenitori di questa idea sperano. Piuttosto, l'esperienza in Svezia ha dimostrato che la prostituzione non scompare con l'introduzione della criminalizzazione dei compratori, le attività semplicemente si nascondono nel sommerso. Questa non può essere la soluzione».

Marija Tosheva, Advocacy officer di SWAN, la Rete di Advocacy dell'Est Europa e Centro Asia spiega: " Il rapporto non riesce a rappresentare le differenti realtà del lavoro sessuale nei contesti europei. Rinforza gli stereotipi che tutte le donne provenienti dall'Est Europa siano trafficate in Europa occidentale, mettendo a tutte l'etichetta di vittime, escludendole dal dibattito e dai processi decisionali. Alcune sex workers migrano per cercare migliori opportunità di lavoro, alcune diventano vulnerabili alla violenza e allo sfruttamento, ma etichettare tutte le sex workers come vittime di violenza e criminalizzare ogni aspetto del lavoro sessuale vuol dire distogliere lo sguardo dalla realtà per guardare a soluzioni moralistiche e repressive."

Un gran numero di organizzazioni che si occupano di HIV, incluso European Aids Treatment group, Aids Action Group e la LILA in Italia hanno sottoscritto la lettera . Mary Honeyball cita a malapena l'HIV nel suo rapporto, apparentemente ignorando che i sex workers sono un gruppo chiave sul fronte della lotta all'HIV. Il rapporto cita la definizione sulla salute sessuale dell'Organizazione Mondiale della Sanità ma curiosamente ignora che l'OMS si pone contro il "modello svedese" perché esso ha un impatto negativo sulla vita e la salute dei sex workers in quanto limita l'accesso all'uso del condom e ad altre misure per prevenire l'HIV.

Un ulteriore documento redatto e firmato da più di 40 accademici e ricercatori consiste in un contro rapporto presentato ai Parlamentari Europei che analizza la mancanza e il travisamento di prove nel rapporto di Honeyball. La lettera che accompagna il contro rapporto dice: "un voto favorevole a questo rapporto potrebbe avere gravi conseguenze su una popolazione già marginalizzata" e continua : «Il rapporto di Honeyball non riesce ad affrontare i problemi e i danni che possono crearsi nel lavoro sessuale, invece produce dati parziali, inesatti e smentiti. Noi crediamo che le politiche debbono basarsi su dati attendibili e speriamo che voterete contro la mozione per criminalizzare i clienti dei lavoratori del sesso».

Il contro-rapporto ha notato che, tra gli altri errori sorprendenti, Mary Honeyball ha completamente frainteso una relazione congiunta commissionata dal Comune di Amsterdam e dal Ministero della Giustizia olandese, confondendo, in maniera imbarazzante, i dati sui coffee shops con quelli sui postriboli.

Le 554 NGO e i 56 ricercatori firmatari delle lettere sollecitano i Parlamentari Europei a rigettare, il 27 febbraio, il rapporto di Mary Honeyballs.

**Per maggiori informazioni:**

http://www.sexworkeurope.org/news/general-news/more-540-ngos-and-45-researchers-demand-members-european-parliament-reject-ms