---
title: 'APPROVATO DAL PE IL RAPPORTO DIRITTI UMANI DI MARCO CAPPATO'
date: Fri, 09 May 2008 12:13:23 +0000
draft: false
tags: [Comunicati stampa]
---

**Approvato a larghissima maggioranza il rapporto del Deputato Europeo Radicale Marco Cappato sul rispetto dei Diritti Umani. Tra i punti centrali i temi Lgbt.  
****  
Il testo integrale in inglese (link fondo testo).  
**  
Il Parlamento Europeo ha ieri approvato a Bruxelles il rapporto di Marco Cappato sul rispetto dei diritti umani nel mondo con 533 voti favorevoli, 63 contrari e 41 astensioni. Tra i punti centrali del rapporto, che commenta i risultati delle politiche europee nell'ambito dei diritti umani nel 2007, anche diversi passaggi relativi alle tematiche LGBT, tra cui l'articolo 82, che si occupa delle discriminazioni sessuali in russia, e gli articoli 106 e 141 sui diritti LGBT.**  
  
Di seguito i paragrafi approvati:  
  
**… 82\. Deplora che l'Unione europea abbia ottenuto finora scarsi risultati nel favorire cambiamenti politici in Russia, in particolare per quanto riguarda questioni delicate quali la situazione in Cecenia e nelle altre Repubbliche del Caucaso, l'impunità e l'indipendenza della magistratura, il trattamento dei difensori dei diritti umani e dei prigionieri politici, tra cui Mikhail Khodorkovsky, l'indipendenza di mezzi di informazione e la libertà di espressione, il trattamento delle minoranze etniche e religiose, il rispetto dello Stato di diritto e la tutela dei diritti umani nelle forze armate, le discriminazioni fondate sull'orientamento sessuale nonché altre questioni;   
  
…106. Reitera il suo appello affinché tutte le discussioni con i paesi terzi, gli strumenti, i documenti e le relazioni, comprese le relazioni annuali, in materia di diritti umani e democrazia affrontino in modo esplicito i temi relativi alla discriminazione, tra cui le questioni riguardanti le minoranze etniche, nazionali e linguistiche, la libertà religiosa, comprese l’intolleranza nei confronti di qualunque religione e le pratiche discriminatorie ai danni delle minoranze religiose, le discriminazioni di casta, la tutela e la promozione dei diritti delle popolazioni indigene, i diritti umani delle donne, i diritti dei minori, i diritti delle popolazioni indigene, i disabili, comprese le persone affette da un handicap intellettivo, e le persone di qualsiasi orientamento sessuale, associando pienamente le loro organizzazioni, sia nell'Unione europea che, se del caso, nei paesi terzi;  
  
…141. Chiede alla Commissione e al Consiglio di prendere iniziative dell'Unione europea a livello internazionale volte a combattere le persecuzioni e le discriminazioni basate sull'orientamento sessuale e l'identità di genere, ad esempio promuovendo una risoluzione sulla questione a livello delle Nazioni Unite e appoggiando le ONG e gli attori che promuovono l'uguaglianza e la non discriminazione; condanna il fatto che molti paesi abbiano criminalizzato il comportamento omosessuale, che l'Iran, l'Arabia Saudita, lo Yemen, il Sudan, la Mauritania, gli Emirati Arabi Uniti e parti della Nigeria impongano la pena di morte per atti omosessuali, che 77 paesi abbiano leggi che consentono alle autorità statali di perseguire ed eventualmente irrogare pene detentive per atti omosessuali e che numerosi paesi, quali il Pakistan, il Bangladesh, l'Uganda, il Kenya, la Tanzania, lo Zambia, il Malawi, il Niger, il Burkina Faso, la Malaysia e l'India (paese nel quale le disposizioni del codice penale in materia sono attualmente oggetto di revisione giurisdizionale) abbiano leggi che prevedono l'irrogazione di pene detentive che vanno da 10 anni all'ergastolo; appoggia pienamente i principi di Yogyakarta sull'applicazione del diritto internazionale in materia di diritti umani in relazione all'orientamento sessuale e all'identità di genere; chiede con insistenza agli Stati membri di concedere l'asilo alle persone che rischiano di subire persecuzioni nei loro paesi di origine a causa del loro orientamento sessuale o della loro identità di genere.   
  
Al seguente link il testo integrale (in inglese) del Rapporto Cappato:  
**  
[http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//NONSGML+REPORT+A6-2008-0153+0+DOC+PDF+V0//EN&language=EN](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//NONSGML+REPORT+A6-2008-0153+0+DOC+PDF+V0//EN&language=EN)****  
Per info e ufficio stampa:  
****Ottavio Marzocchi [ottavio.marzocchi@europarl.europa.eu](mailto:ottavio.marzocchi@europarl.europa.eu)  
****Sergio Rovasio [rovasio_s@camera.it](mailto:rovasio_s@camera.it)**