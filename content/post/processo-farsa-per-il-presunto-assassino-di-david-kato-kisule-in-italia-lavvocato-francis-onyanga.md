---
title: 'Processo farsa per il presunto assassino di David Kato Kisule. In Italia l''Avvocato Francis Onyanga'
date: Sat, 12 Nov 2011 15:49:08 +0000
draft: false
tags: [Africa]
---

Il presunto assassino di David Kato Kisule condannato a 30 anni di prigione. Per i legali processo farsa, senza garanzie.

Francis Onyango, avvocato di David Kato Kisule, sarà in Italia per partecipare al Congresso di Certi Diritti (Milano 3-4 dicembre) e del Partito Radicale Nonviolento (Roma 8-11 dicembre).

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Dall’Uganda giungono notizie pessime che confermano la continua violazione dei diritti per le persone Lgbt e, più in generale, dei diritti umani.

Nei giorni scorsi è giunta la notizia di una condanna a 30 anni al ventiduenne Sidney Nsubuga Enoch, il presunto assassino di David Kato Kisule, senza che il collegio di avvocati guidati da Francis Onyango e i legali di SMUG (Sexual Minorities Uganda) fossero informati del processo. Secondo l’Avvocato la condanna è stata decisa senza che fossero sentiti i testimoni e senza che le parti fossero informate del processo in corso.

Il Direttore di Smug, Frank Mughisha,  in questi giorni si trova a Washington dove il 10 novembre gli è stato conferito il premio della Fondazione Robert F. Kennedy Center for Justice and Human Rights; nella motivazione si legge, tra l'altro, che “il suo coraggioso attivismo è nel cuore del movimento per i diritti umani”. Il premio prevede anche una partnership e, nel concreto, darà per alcuni anni il sostegno della Fondazione a Smug che a sua volta lavora con decine di associazioni in Uganda. L’organizzazione Smug assiste inoltre alcuni detenuti procurando loro un rappresentante legale, e offre formazione sul tema dell’Hiv/Aids.

Francis Onyango, l’Avvocato di Davide Kato Kisule, si era iscrittolo lo scorso febbraio al Partito Radicale Nonviolento, transnazionale e transpartito.  Ha confermato la sua partecipazione, il prossimo 3-4 dicembre a Milano, al V Congresso dell’Associazione Radicale Certi Diritti e, dall’8 all11 dicembre alla II Sessione del 39° Congresso del Partito Radicale Nonviolento che si terrà a Roma.