---
title: 'LUNEDI'' 23-3, COMMEMORAZIONE MILIONI VITTIME AIDS IN P.ZA S.PIETRO'
date: Thu, 19 Mar 2009 16:19:18 +0000
draft: false
tags: [Comunicati stampa]
---

AL TERMINE DELLA VISITA DEL PAPA IN AFRICA,  LUNEDI’ 23 MARZO, RADICALI E LE PIU' IMPORTANTI ASSOCIAZIONI LGBT,  COMMEMORERANNO  LE VITTIME DELL’AIDS IN PIAZZA PIO XII (AL CONFINE TRA ITALIA E VATICANO).

Lunedì 23 marzo, alle ore 17, al termine della visita del Papa in Africa, al suo ritorno in Vaticano, si svolgerà in Piazza Pio XII (al confine tra Italia e Vaticano), la commemorazione delle vittime dell’Aids che, in Africa, è la prima causa di morte fra le persone di ogni età. La cerimonia di commemorazione, con i simboli del lutto e i lumini accesi, si svolgerà nel ricordo degli oltre 30 milioni di esseri umani morti di Aids dal 1982 ad oggi, tra questi milioni di gay in tutto il mondo (in basso la tabella del Report on the global Aids epidemic 2007). Finora il principale strumento di prevenzione riconosciuto dalla scienza è il preservativo.  
  
La manifestazione commemorativa è promossa da Radicali Italiani insieme alle Associazioni Radicali Certi Diritti, Radicali Roma, Associazione Luca Coscioni, Anticlericale.net; le Associazioni lgbt, Arcigay Roma, DiGayProject, GayLib, Circolo Mario Mieli di Roma, Rosa Arcobaleno, Libellula, Coordinamento Trans Silvya Rivera, Uaar circolo di Roma.  
  
Tra gli altri, hanno di già preannunciato la loro partecipazione dirigenti e militanti radicali, Antonella Casu, Segretaria di RI; Paola Concia, deputata del Pd; Imma Battaglia, Presdiente di DJGayProject;  Marco Perduca senatore radicale nel Pd; Maurizio Turco, deputato radicale del Pd; Franco Grillini, Direttore di Gaynews; Fabrizio Marrazzo, Presidente Arcigay Roma; Sergio Rovasio, Segretario Certi Diritti;  Rossana Praitano, Presidente Mario Mieli; Luca Liguoro, Presidente Rosa Arcobaleno, Daniele Priori GayLib Roma.  
  
“Il problema dell’ Aids non si può superare con la distribuzione di preservativi che, al contrarioi, aumentano i problemi…” Conferenza Stampa del Papa,  16 marzo 2009 Aereo Italia-Camerun.  
Negli Stati Uniti, l’AIDS è la seconda causa di morte fra i giovani Nell’Africa Subsahariana, l’AIDS è la prima causa di morte fra le persone di ogni età.

Numero di persone che vivevano con l’HIV nel 2007

• Totale:

33.2 milioni

(30.6–36.1 milioni)

• Adulti

30.8 milioni

(28.2–33.6 milioni)

• Donne

15.4 milioni

(13.9–16.6 milioni)

• Bambini sotto i 15 anni

2.5 milioni

(2.2–2.6 milioni)

Nuove infezioni da HIV nel 2007

• Totale

2.5 milioni

(1.8–4.1 milioni)

• Adulti

2.1 milioni

(1.4–3.6 milioni)

• Bambini sotto i 15 anni

420 000

(350 000–540 000)

Numero di morti per AIDS nel 2007

• Totale

2.1 milioni

(1.9–2.4 milion)

• Adulti

1.7 milioni

(1.6–2.1 million)

• Bambini sotto i 15 anni

330 000

(310 000–380 000)

_(1) Report on the global AIDS epidemic 2007_