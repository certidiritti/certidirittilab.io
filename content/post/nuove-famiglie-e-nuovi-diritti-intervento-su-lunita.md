---
title: 'NUOVE FAMIGLIE E NUOVI DIRITTI:  INTERVENTO SU L''UNITA'''
date: Thu, 15 Oct 2009 06:49:55 +0000
draft: false
tags: [Comunicati stampa]
---

di Sergio Rovasio  
  
da [L’Unità](http://www.facebook.com/note_redirect.php?note_id=184352646336&h=0fc7bce61ea0556ac1f49326c2183519&url=http%3A%2F%2Fwww.unita.it%2F "http://www.unita.it/") del 15 ottobre 2009, pag. 19

In Olanda una coppia gay può sposarsi, in Francia non esiste distinzione tra figlio legittimo e figlio naturale, in Belgio è appena stata approvata una riforma sulle adozioni che prevede la possibilità anche alle persone single o gay di poterlo fare, in Spagna è stata approvata da poco una legge moderna e innovativa contro il grave fenomeno della violenza contro le donne nell’ambito familiare.

Quasi tutti i Paesi europei riescono ad approvare leggi di riforma che si adeguano alle grandi trasformazioni sociali dei tempi moderni, in particolare quelli che riguardano l’ambito della famiglia. Del resto in questi Paesi esiste un pragmatismo che meglio si combina con il motto "la politica è al servizio del cittadino" molto lontana dai contorcimenti del sistema partitocratrico italiano che guarda più al Vaticano e a certi interessi piuttosto che adeguare le sue azioni alle richieste di parte della società.  
Un gruppo di esperti e di rappresentanti di associazioni, tra le altre l’Associazione Luca Coscioni, Certi Diritti, Divorzio Breve, Figli Negati, Conacreis, hanno lavorato per quasi due anni a un progetto di riforma che tocca i principali capitoli del diritto di famiglia. Il lavoro è stato coordinato dal giurista Bruno de Filippis, con un importante contributo dell’Avvocato Francesco Bilotta e si è ispirato all’idea slogan: «Amore Civile, dal diritto della tradizione al diritto della ragione» nato da un convegno promosso nel 2007 dall’Associazione Luca Coscioni e dai radicali.  
I capitoli, con le proposte di riforma e di regolamentazione legislativa, riguardano il matrimonio gay, le unioni civili, le norme sulla procreazione assistita, il divorzio breve, la violenza sulle donne, la filiazione legittima e naturale e altri temi molto importanti della vita quotidiana delle persone. Si è cercato di cucire insieme idee e proposte con l’obiettivo di immaginare una nuova legislazione adatta ai tempi e ad una società laica e moderna.  
Si tenta così di dare corso ad un’idea che lo stesso Marco Pannella precisa essere rivolta alla classe politica «che ha il dovere di non ignorare questo lavoro, ma di fornire risposta a tutti gli interrogativi che esso pone».  
L’idea che più ha ispirato questo lavoro è il concetto di laicità dello Stato così ben espresso dal giurista Bruno de Filippis: «Ogni concezione ideologica o religiosa merita il massimo rispetto, ma altrettanto rispetto è dovuto alla libertà dei cittadini. I principi che animano religioni o ideologie possono influenzare la vita delle persone ma non devono essere imposti per legge a coloro i quali non li condividono». Il progetto, presentato in un convegno alla Camera dei deputati la scorsa settimana alla presenza di parlamentari e giornalisti, ha l’ambizione di diventare a breve una Proposta di Legge da proporre alla firma di tutti i parlamentari della Repubblica.