---
title: 'CONGRESSO DELL''ASSOCIAZIONE RADICALE CERTI DIRITTI'
date: Sat, 07 Feb 2009 19:48:10 +0000
draft: false
tags: [Comunicati stampa]
---

**Oggetto: Sabato 14 marzo 2009 Congresso dell'Associazione Radicale Certi Diritti a Bologna presso l'Hotel Europa.**

Sabato 14 marzo 2009 terremo a Bologna il nostro 2° Congresso nazionale. La nostra Associazione, nata nel marzo 2008 a Roma, con questo appuntamento intende fare il punto della situazione sulle sue attività, la sua organizzazione e le iniziative promosse in questi mesi per il raggiungimento degli obiettivi statutari e della Mozione votata a Roma.

I temi di cui ci occupiamo sono molti e ogni altro tuo contributo è il benvenuto, di qualsiasi tipo esso sia, anche di idee e suggerimenti.

Siamo nel pieno di una grande crescita che con il prossimo Congresso vorremmo meglio organizzare e rafforzare anche nell'ambito delle attività di Radicali Italiani cui la nostra Associazione è statutariamente collegata.

Il Congresso lo terremo  presso l'Hotel Europa – Via Boldrini Cesare 11 (a 5 minuti dalla Stazione Centrale di Bologna), i lavori inizieranno alle ore 9.30 e proseguiranno per tutta la giornata di sabato 14 marzo. Domenica 15 marzo abbiamo previsto nello stesso luogo una riunione del nuovo Direttivo.

Abbiamo previsto per la mattinata due momenti dedicati ai saluti esterni e ad una Conferenza con alcune personalità politiche nazionali che possano intervenire sui temi dei diritti civili. Maggiori dettagli li troverai nel sito [www.certidiritti.it](../)

Porta amici e conoscenti, facci sapere se parteciperai al seguente indirizzo: [segretario@certidiritti.it](mailto:segretario@certidiritti.it). Grazie per l'apporto che ci vorrai dare.

Un caro saluto,

**Clara Comelli, Presidente -- Sergio Rovasio, Segretario -- Roni Guetta, Tesoriera**

P.S. Sono aperte le iscrizioni per il 2009, unico canale di autofinanziamento dell'Associazione Radicale Certi Diritti, trovi le modalità per farlo al seguente link [ISCRIZIONI 2009](index.php?option=com_content&view=article&id=16&Itemid=88)  
  
Non hai idea quanto sia importante ogni contributo!!! Davvero.