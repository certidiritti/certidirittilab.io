---
title: 'CERTI DIRITTI ADERISCE E PARTECIPA AL SICILIA PRIDE 2010: SABATO 19 GIUGNO'
date: Thu, 17 Jun 2010 11:53:21 +0000
draft: false
tags: [Comunicati stampa]
---

L’Associazione Radicale Certi Diritti ha aderito e parteciperà al Sicilia Pride 2010, che **sabato 19 giugno 2010** si sfilerà per la prima volta per le strade di **Palermo**.

Alla manifestazione di Palermo parteciperanno il Segretario Sergio Rovasio e rappresentanti di Certi Diritti da Catania, Milano e Roma.

Saranno inoltre presenti i radicali di Palermo e di altre città della Sicilia, da sempre impegnati sui diritti civili e umani delle persone lgbt(e).

Maggiori info sul Sicilia Pride: **[http://siciliapride.org/](http://siciliapride.org/)**