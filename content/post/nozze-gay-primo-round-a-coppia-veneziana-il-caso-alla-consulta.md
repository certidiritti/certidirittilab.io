---
title: 'NOZZE GAY, PRIMO ROUND A COPPIA VENEZIANA: IL CASO ALLA CONSULTA'
date: Tue, 21 Apr 2009 08:39:24 +0000
draft: false
tags: [Senza categoria]
---

_Il Comune non aveva concesso la pubblicazione dell’avviso di matrimonio. Il Tribunale: decida la Consulta_

[![](http://www.ilgazzettino.it/MsgrNews/HIGH/20090421_gay.jpg)](http://www.gazzettino.it/articolo.php?id=55300&sez=NORDEST)

  

di **Gianpaolo Bonzio**

VENEZIA (21 aprile) - Sarà direttamente la Corte costituzionale a decidere sulla richiesta di matrimonio tra una coppia di uomini. Il caso è stato portato alla luce dai Radicali dell’associazione "Certi diritti" che da tempo si battono per il riconoscimento delle unioni civili tra coppie dello stesso sesso.  
Si tratta di una svolta di un certo peso, visto che in precedenza molti Comuni bocciavano senza tanti problemi queste richieste di unioni. E invece in questo caso la vicenda ha seguito un percorso diverso.

  
«Davanti alla bocciatura della richiesta di pubblicazione dell’unione tra due persone dello stesso sesso - spiega il segretario nazionale dell’associazione, Sergio Rovasio - i nostri legali hanno assistito la coppia promuovendo un ricorso al Tribunale, visto poi che in altre città europee non ci sono problemi. A questo punto il Tribunale di Venezia, per la prima volta, ha deciso di inviare tutta la documentazione direttamente alla Corte costituzionale».  
  
Per l’associazione "Certi diritti", che su questo argomento ha avviato una battaglia di "affermazione civile", la svolta potrebbe aprire nuovi scenari in vista del riconoscimento effettivo del matrimonio civile tra persone dello stesso sesso. Per quanto riguarda la coppia che ha formulato l’istanza di matrimonio va detto che si tratta di persone della provincia di Venezia che lavorano a Milano.  
  
Gli avvocati della rete Lenford ricordano che su questa battaglia sono impegnati diversi legali visto che il problema interessa numerose coppie. In questi mesi, inoltre, i professionisti hanno seguito diverse cause relative ai problemi che gli omosessuali hanno avuto in famiglia, nei posti di lavoro e anche a scuola. E secondo l’associazione "Certi diritti" nel nostro Paese è la prima volta che viene garantita una tutela a tutto campo.  
  
«Anche alla luce di questa decisione del Tribunale lagunare - aggiunge Rovasio - ci auguriamo che si arrivi presto ad una regolamentazione che sancisca, una volta per tutte, l’assoluta regolarità del matrimonio gay o di altre forme che diano garanzie alle unioni tra le coppie dello stesso sesso».  
L’avvocato difensore della coppia si dice ottimista e attende con molta fiducia il pronunciamento della Corte costituzionale.  
  
«Crediamo fermamente che escludere le coppie dello stesso sesso dalle tutele che discendono dal matrimonio, sia contrario alla nostra Costituzione - afferma Francesco Billotta, il legale della coppia - ma anche agli impegni che l'Italia ha assunto entrando nell'Unione europea. Qui, è bene precisarlo, non stiamo infatti parlando di coppia di fatto, ma di persone che si vogliono sposare, esattamente come qualsiasi coppia che intende essere riconosciuta quale portatrice di diritti e di doveri nei confronti dello Stato. Per i miei assistiti è una soddisfazione vedere che li si considera come una vera e propria famiglia».

http://www.gazzettino.it/articolo.php?id=55300&sez=NORDEST