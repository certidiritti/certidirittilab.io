---
title: 'Giovanardi parla 5 giorni a settimana di gay: roba da Freud'
date: Tue, 03 May 2011 07:28:19 +0000
draft: false
tags: [Politica]
---

Il sottosegretario Giovanardi e i baci gay: non risponde su aumento divorzi, calo matrimoni e tagli alla famiglia ma cinque giorni a settimana parla di gay. Gli altri due giorni può dedicarli a risponderci anzichè "buttarla in caciara"?  
  
Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti  
Roma, 3 maggio 2011

Il (molto) sottosegretario alla famiglia Carlo Giovanardi che un giorno si e l’altro pure fa dichiarazioni sui temi dell’omosessualità e mai risponde alle domande che gli abbiamo fatto sul perché in Italia aumentano le coppie (anche gay) conviventi, aumentano i divorzi e diminuiscono i matrimoni, anche ieri, lunedì, ha fatto una dichiarazione sui “gay che mai si devono baciare in pubblico”.  Insomma, per lui l’omosessualità è una fissazione, parrebbe roba da Freud.  
Il martedì dice che esistono ‘lobby potenti che operano per azzerare la famiglia’, il mercoledì se la prende con Ikea perché fa una pubblicità con due ragazzi che si tengono per mano, il mercoledì in risposta a Eataly che ha fatto suo il manifesto di Ikea, se la prende con chi discrimina la famiglia composta da uomo e donna, ecc.  Noi ci acconteteremmo di sapere, anche il sabato o la domenica, se non gli crea troppa fatica, per quale motivo il Governo di cui fa parte, nel quinquennio 2008-2013 ha tagliato - da 346 milioni a 31 milioni - i fondi destinati alle famiglie a lui tanto care.   E’ molto probabile che oggi, martedì, o domani, mercoledì, farà dichiarazioni sui gay che vanno in bicicletta o sui gay che non devono fare rapine in banca.  La smetta di ‘buttarla in caciara sui gay’ e ci dia una risposta. O stia zitto per sempre, per evitare figuracce come quelle che continua a fare.