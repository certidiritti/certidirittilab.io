---
title: 'Gruppo Alde denuncia al PE nuovo codice civile rumeno per discriminazione contro persone lgbt'
date: Fri, 14 Oct 2011 19:14:41 +0000
draft: false
tags: [Europa]
---

L'interrogazione dei liberal-democratici denuncia il fatto che l'articolo 277 sia inutile e controproducente, perché cementa una discriminazione basata sull'orientamento sessuale all'interno del codice civile e rischia inoltre di di incitare l'omofobia.

   
Bruxelles - Roma, 14 ottobre 2011  
   
Comunicato Stampa dell'Associazione Radicale Certi Diritti  
   
Il gruppo ALDE (Liberal-Democratico) al PE ha depositato al Parlamento europeo una interrogazione orale sul nuovo codice civile rumeno entrato in vigore il primo ottobre, che dedica l'articolo 277 alla "proibizione o equivalenza delle forme di coabitazione con il matrimonio".  
   
Tale articolo afferma al punto 1 che "il matrimonio tra persone dello stesso sesso é proibito"; al punto 2 che i "matrimoni conclusi o contratti all'estero, sia da cittadini rumeni che stranieri, non sono riconosciuti in Romania"; al punto 3 che "le unioni civili tra persone dello stesso o di sesso opposto, concluse o contratte all'estero, sia da cittadini rumeni che stranieri, non sono riconosciute in Romania".  
   
L'interrogazione del gruppo ALDE denuncia il fatto che tale articolo sia inutile e controproducente, perché cementa una discriminazione basata sull'orientamento sessuale all'interno del codice civile e rischia inoltre di di incitare l'omofobia; contrasta con l'obiettivo della Commissione europea di assicurare che i cittadini europei possano portare con sé il loro status civile quando si spostano da uno stato all'altro; crea una situazione di incertezza giuridica e di contraddizione con le norme europee sulla libera circolazione, nonché con i valori europei di eguaglianza e non discriminazione. Il gruppo ALDE chiede infine alla Commissione di aprire una procedura d'infrazione contro la Romania per violazione delle direttive sulla libera circolazione e per discriminazione basata sull'orientamento sessuale.  
   
L'interrogazione per essere dibattuta in plenaria deve ottenere il sostegno degli altri gruppi politici al PE.  
   
Certi Diritti si felicita per l'iniziativa presa dall'on. Renate Weber (liberale rumena), che con la collaborazione del funzionario del gruppo ALDE per la commissione libertà pubbliche nonché responsabile per le questioni europee Ottavio Marzocchi, ha redatto l'interrogazione, invita la Romania a cancellare l'articolo 277 dal Codice Civile e chiede alla Commissione europea di attivarsi nel caso in cui questo non venga fatto.