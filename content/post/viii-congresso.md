---
title: 'VIII Congresso'
date: Wed, 30 Jul 2014 13:30:30 +0000
draft: false
---

[![headercongresso8](http://www.certidiritti.org/wp-content/uploads/2014/07/headercongresso8.png)](http://www.certidiritti.org/wp-content/uploads/2014/07/headercongresso8.png)**LAMEZIA TERME,** **21-22-23 NOVEMBRE 2014**

Grand Hotel Lamezia, Piazza Lamezia – Sant’Eufemia di Lamezia Terme (Cz)

[![LAVORI](http://www.certidiritti.org/wp-content/uploads/2014/07/LAVORI.png)](http://www.certidiritti.org/wp-content/uploads/2014/07/LAVORI.png)

**Sessione speciale di venerdì 21 novembre** 

**I Giornata** 

**II Giornata** 

  [![DOCUMENTI](http://www.certidiritti.org/wp-content/uploads/2014/07/DOCUMENTI.png)](http://www.certidiritti.org/wp-content/uploads/2014/07/DOCUMENTI.png)

 [Mozione Generale Approvata](http://www.certidiritti.org/2011/11/23/mozione-generale-approvata-dal-viii-congresso/ "mozione generale")

[Mozione Particolare sui diritti delle persone intersex](http://www.certidiritti.org/2011/11/23/mozione-particolare-sui-diritti-delle-persone-intersex-approvata-dal-viii-congresso/ "mozione particolare")

[Mozione Particolare sulla gestazione per altri](http://www.certidiritti.org/2011/11/23/mozione-particolare-sulla-gestazione-per-altri-approvata-dal-viii-congresso/ "mozione particolare")

[Bilancio](http://www.certidiritti.org/2011/11/23/bilancio-2014/ "bilancio 2014")

[Organi e incarichi statutari](http://www.certidiritti.org/chi-siamo/organi-e-incarichi/)

**RELAZIONI/INTERVENTI DEGLI OSPITI DELL'VIII CONGRESSO**

[Relazione di Massimo Modesti “Seconde generazioni LGBT”](http://www.certidiritti.org/wp-content/uploads/2014/07/Relazione-Certi-Diritti-G2-LGBT.pdf)

[Intervento di Volker Beck, membro verde del Bundestag per Cologna](http://www.certidiritti.org/wp-content/uploads/2014/07/Intervento-di-Volker-Beck-membro-verde-del-Bundestag-per-Cologna.pdf)