---
title: 'Germania dice sì a Matrimonio Egualitario. Da noi libertà civili alla finestra.'
date: Fri, 30 Jun 2017 10:15:12 +0000
draft: false
tags: [Europa]
---

![Alemania3-1024x642](http://www.certidiritti.org/wp-content/uploads/2017/06/Alemania3-1024x642-300x188.jpg)"Nel mese dell'orgoglio e agli sgoccioli della legislatura popolari e socialisti tedeschi approvano il matrimonio egualitario, noi in Italia ci accontentiamo del dibattito sulle coalizioni elettorali, e le libertà civili stanno alla finestra. Diventando il quattordicesimo Paese europeo a superare la discriminazione matrimoniale, l'Italia perde quel 'modello tedesco' al quale si ispirò per la legge sulle unioni civili, segno che in tutta Europa i diritti sono in movimento e che solo il confronto laico consente il superamento di storici traguardi" Così Leonardo Monaco, segretario dell'Associazione Radicale Certi Diritti. "Certi Diritti continuerà a mettere in pratica la strategia delle cause pilota per scardinare quel Diritto di famiglia che a gran voce l'Italia vuole vedere riformato; per il bene della democrazia e dei diritti dei cittadini speriamo pur sempre che la sveglia suoni in Parlamento prima della prossima umiliazione da parte delle corti", conclude l'esponente radicale.