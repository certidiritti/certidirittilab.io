---
title: 'RINVIO DECISIONE CORTE A DOPO FESTE DI PASQUA CI FA BEN SPERARE. IL COLLEGIO DI DIFESA HA SVOLTO UN LAVORO STRAORDINARIO.'
date: Wed, 24 Mar 2010 11:46:12 +0000
draft: false
tags: [Comunicati stampa]
---

**MATRIMONIO GAY: IL RINVIO DELLA DECISIONE DELLA CONSULTA SUI RICORSI DI ALCUNE COPPIE GAY CI FA BEN SPERARE.. LA LOTTA PE RIL SUPERAMENTO DELLE DISEGUAGLIANZE CONTINUA.**

**Dichiarazione di Sergio Rovasio Segretario Associazione Radicale Certi Diritti**

**“La decisione della Corte costituzionale di rinviare a dopo le festività pasquali la decisione sul merito dei ricorsi di alcune coppie gay, ci fa ben sperare. La richiesta di maggior tempo per una  decisione della Corte potrebbe significare che quanto illustrato dal nostro  Collegio di difesa, composto dai Professori Marilisa D’Amico, Enzo Zeno Zencovich, Vittorio Angiolini e Alexander Schulster e dallo straordinario team di  Avvocati Massimo Clara, Francesco Bilotta, Alessandro Giadrossi, Antonio Rotelli, Saveria Ricci, Mario Di Carlo ed Ileana Alesso, merita attenzione e approfondimento rispetto ai principi volti al superamento delle disuguaglianze come indicato dalla Costituzione italiana.**

Attendiamo con fiducia e speranza la decisione della Corte Costituzionale e ci auguriamo che il cammino intrapreso con la campagna di Affermazione Civile, incardinato con Avvocatura lgbt Rete Lenford, raggiunga quanto prima l’obiettivo dell’accesso all’istituto del matrimonio gay per le coppie dello stesso sesso”.