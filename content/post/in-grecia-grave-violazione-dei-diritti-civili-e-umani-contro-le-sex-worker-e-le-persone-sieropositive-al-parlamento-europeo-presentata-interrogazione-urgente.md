---
title: 'In Grecia grave violazione dei diritti civili e umani contro le sex worker e le persone sieropositive. Al Parlamento Europeo presentata interrogazione urgente'
date: Thu, 10 May 2012 11:17:16 +0000
draft: false
tags: [Europa]
---

Rastrellamenti e imposizione del test hiv con pubblicazione, di foto, indirizzi e generalità delle persone con hiv.

Comunicato Stampa dell’Associazione Radicale Certi Diritti e del Comitato per i Diritti Civili delle Prostitute

Roma, 10 maggio 2012

Nei giorni scorsi in Grecia si sono verificati episodi gravissimi di violazione dei diritti civili e umani delle persone prostitute. La polizia ha fatto dei veri e propri rastrellamenti nelle strade e nei luoghi dove si svolge l’attività delle sex-workers imponendo il test dell’Hiv. Delle persone trovate sieropositive al test sono state diffuse informazioni riservate, in violazione di tutte le norme nazionali e internazionali anche sanitarie sulla privacy. In alcuni casi sono stati rese pubbliche anche le abitazioni delle Sex-Workers con Hiv nel disprezzo delle più elementari regole civili e umanitarie.

La prostituzione non è regolamentata e i tagli alla sanità nei fatti fanno aumentare i casi di Hiv tra la popolazione, clienti compresi, ai quali non viene chiesto a nessuno di fare il test dell’Hiv.  

Il comportamento delle autorità greche è gravissimo e fatto nel disprezzo di tutte le norme sul rispetto dei Diritti Umani, della Carta dei Diritti fondamentali dell’Unione Europea e della Carta europea dei Diritti del Malato.

I membri del Parlamento Europeo Gianni Vattimo, Renate Weber, Sonia Alfano, Sophie In't Veld , Leonidas Donskis, Sarah Ludford, Alexander Alvaro, Nadja Hirsch, Nathalie Griesbeck hanno firmato una interrogazione urgente alla Commissione Europea e le Associazioni in difesa dei diritti delle Sex-Workers si sono attivate in una campagna di sensibilizzazione di centinaia di deputati europei contro questa vergognosa azione delle autorità greche. Ci auguriamo che la Commissione Libertà Pubbliche del Parlamento Europeo si attivi al più presto su quanto sta accadendo in Grecia.