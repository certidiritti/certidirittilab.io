---
title: 'Ciao Sergio'
date: Sat, 19 Oct 2013 14:39:36 +0000
draft: false
tags: [Politica]
---

Elio De Capitani ricorda Sergio Stanzani

19 ottobre 2013

Ciao Sergio,

"la dimostrazione che gli anni sono passati per me... è che non mi fa più nessun piacere sentirmi "leccare'...(qui ridevi) mi conferma ancora di più che appunto (e ridevi) ... e una di quelle cose che (ridevi ancora) ... sono lì che ... sono gli ultimi convenevoli ( e ridevi ancora di più)..."

Se penso a te Sergio, penso a quella risata ... "eh eh eh" ... arrochita, ma leggera, lo sguardo stanco, appassionato e l'ironia che ti ha accompagnato per tutta la vita.

Niente convenevoli ora che sei morto, Stanzani? Ci provo, ma non garantisco.

"Libertà, diritto, democrazia. E' la santissima trinità mia, va bene, quella che credo sia del partito radicale. Libertà, diritto, democrazia. La libertà è un presupposto, la democrazia è l'obiettivo, che cos'è che collega... come si fa a ottenere... è attraverso la legge, quindi attraverso il diritto. E una cosa molto semplice... però è sacrosanta. (...)" E qui - era il 2010 - hai chiuso con un gesto tutto tuo, condito da un "va bè", la mano che diceva "ma lasciamo perdere" oppure "l'ho detto tante di quelle volte, mi devo ancora ripetere?"...

Io vengo da un'altra tradizione e da un'altra santissima trinità - libertà eguaglianza fraternità - ho sempre fatto i conti però - grazie anche a persone molte limpide come te, Sergio - con la santissima e lucidissima trinità tua e del partito radicale.

Riflettere su come contemperare la tua e la mia trinità ha portato noi del Teatro dell'Elfo a scrivere nel nostro statuto:

"La libertà offende il lavoro e lo separa dalla cultura quando è fatta soprattutto di privilegi. Ma la libertà non è fatta soprattutto di privilegi, bensì è fatta di doveri. E nel momento stesso in cui ognuno di noi cerca di far prevalere i doveri della libertà sui privilegi, in quel momento la libertà ricongiunge il lavoro e la cultura e mette in moto una forza che è l'unica in grado di servire efficacemente la giustizia. Si può allora formulare molto semplicemente la regola della nostra azione, il segreto della nostra resistenza: tutto ciò che umilia il lavoro umilia l'intelligenza, e viceversa. La lotta rivoluzionaria, lo sforzo secolare di liberazione si definisce innanzitutto come duplice e incessante rifiuto dell'umiliazione."

Così Albert Camus nel 1936. Così ancora oggi, per noi. L'arte ci permette di essere liberi e di non essere liberi solo per noi stessi. Il patto tra lavoro e cultura è il principio guida del nostro essere.

Ma tu, Sergio, ci dicevi le stesse cose, con parole che colpivano nella loro schietta e lucida semplicità:

"La libertà è una condizione che evidentemente se non la amministri, se non la gestisci, se non la senti parte anche tua, in quello che ti può e ti deve limitare, allora non è ... è prepotere, è ... niente ... niente non è..."

"Mi vengono gli accidenti" dicevi in quella tua intervista, appassionata ma anche dolorosa, del dicembre 2010 "io mi rifiuto di sentirmi di essere una delle poche persone ragionevoli nate in questo paese, va bene, e quindi mi sembra proprio una cosa assurda ... questo silenzio ..."

Senti, ogni volta ci si rimane di sasso davanti al silenzio o peggio, all'autoindulgenza degli italiani. Ma gli spiriti come il tuo ci hanno sempre spinto a non chiuderci a riccio ma a cercare sempre e ancora di condurre grandi e piccole battaglie. E spesso assieme ai radicali, come quella di Certi Diritti, minuscola ma splendida e tenace associazione radicale a cui dò il mio piccolo contributo. Ci siamo incontrati proprio lì, a un congresso di Certi Diritti, ed eri già troppo vecchio per non essere te stesso fino in fondo: non sono riuscito a dirti nulla di tutto questo, proprio per non "leccare", per non essere mandato al diavolo con grazia bolognese.

Ti dico grazie adesso. Non è mai troppo tardi per un grazie, se la gratitudine la si porta dentro.

Un abbraccio ai compagni che ti renderanno omaggio a Torre Argentina, anche per me, che non potrò esserci, almeno fisicamente.

Elio