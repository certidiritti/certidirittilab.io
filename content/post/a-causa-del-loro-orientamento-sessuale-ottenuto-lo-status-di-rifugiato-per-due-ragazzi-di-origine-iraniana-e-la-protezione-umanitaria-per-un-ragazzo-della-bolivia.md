---
title: 'A causa del loro orientamento sessuale ottenuto lo status di rifugiato per due ragazzi di origine iraniana e la protezione umanitaria per un ragazzo della Bolivia'
date: Wed, 24 Apr 2013 19:59:14 +0000
draft: false
tags: [Politica]
---

Comunicato congiunto 

24 aprile 2013

Questo è avvenuto grazie al contributo del Progetto Immigrazioni e Omosessualità che supporta da anni ragazzi/e immigrati/e che ricercano in Italia quella protezione che a causa del loro diverso orientamento sessuale non è riconosciuta nel paese di origine.

Sabato 4 maggio alle ore 9:00 presso Arci Corvetto in via Oglio 21 a Milano partirà il corso di formazione "Arcobaleni Migranti" organizzato dal Progetto IO insieme alle associazioni "Todo Cambia" e "Università Migrante" . Si prega di diffondere la notizia presso tutti i vostri canali.

Per maggiori informazioni:

http://www.unimigrante.net/arcobaleni-migranti/

Il Progetto IO - Immigrazioni e Omosessualità è una rete di volontari e di associazioni come lo Sportello Trans A.L.A Milano Onlus, CIG-Arcigay, Arcilesbica Zami, Linea Lesbica Amica e Certi Diritti che opera grazie anche al supporto offerto dallo studio Santilli&Corace di Milano.

Oltre all'assistenza dei richiedenti asilo il progetto IO continua a sostenere e ribadire la necessità e l'importanza di promuovere eventi di sensibilizzazione/informazione rivolti alla cittadinanza, con l'obiettivo primario di innalzare il livello di conoscenza su immigrazione, omosessualità e transessualità, svincolando questi temi dall'attenzione sfrenata verso tutto ciò che è cronaca nera, rosa, nero-rosa e cercando di focalizzare l'attenzione sugli aspetti culturali di cui sono portatrici le persone migranti omosessuali e transessuali e sulle discriminazioni e l'emarginazione sociale che loro spesso subiscono.