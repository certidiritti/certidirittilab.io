---
title: 'IL  14-6 CERTI DIRITTI CON FAMIGLIA FANTASMA AL COMUNE DI ELMAS -CA'
date: Wed, 11 Jun 2008 17:02:32 +0000
draft: false
tags: [Comunicati stampa]
---

[![](http://www.queerway.it/public/WindowsLiveWriter/a72b9eebedc8_96B5/famiglia-fantasma_thumb.jpg)](http://famigliafantasma.freewordpress.it/info/) Sabato 14 Giugno, presso la sala Consiliare del comune di Elmas, si terrà la presentazione del libro "La Famiglia Fantasma" e si parlerà di Certi Diritti e dell'iniziativa di Affermazione Civile. **Interverranno:**  
Saluti del Sindaco (Valter Piscedda), dell'Assessore alla Cultura (Solange Pes) e dell'Assessore alle politiche giovanili (alessandra Pili).  
Intervento di Diego Lasio (docente Psicologia Sociale - Università di Cagliari)  
Intervento di Francesco Mastinu (Segretario del Consiglio Ordine Assistenti Sociali - Regione Sardegna).  
Intervento dell’Autore: Gian Mario Felicetti  
  
**Modera:**  
Cinzia Isu - Responsabile settore Servizi alla persona - Comune di Elmas.  
  
![Comune di Elmas - Stemma](http://www.araldicacivica.it/vecchio%20sito/Sardegna/Comuni%20ca/elmas.gif)**La biblioteca del Comune di Elmas** ha portato avanti un’iniziativa degna di menzione: infatti, grazie a un particolare interesse e sensibilità di alcuni amministratori referenti e del personale del settore dei servizi alla persona, ha deciso di acquistare e aggiungere nel proprio catalogo delle opere in prestito alcuni libri che trattassero della tematica LGTB.  
  
L’idea guida che ha generato tale intento si fonda sia su due aspetti distinti ma correlati:  
• Sopperire alla difficoltà di riuscire a reperire nel circuito editoriale e in quello della vendita della Sardegna dei testi afferenti a tale corrente letteraria, per la quale gli appassionati son quasi sempre obbligati ad acquistarne via web i titoli,  
• Il particolare interesse della cittadinanza alle tematiche e ai problemi, ad ampio spettro, riguardante i cittadini omosessuali.  
  
È stato pertanto deciso di offrire agli interessati la possibilità di poter avere accesso a libri a tematica LGTB e quindi di poter scoprire o approfondire la conoscenza di tale ambito.  
Attualmente sono stati acquistati, catalogati e resi disponibili all’utenza una cinquantina di titoli, che spaziano dalla saggistica alle vere e proprie opere letterarie e romanzi, la novità sta nel fatto che, oltre ai noti testi a livello mondiale di Leavitt, Genet e White, presenti comunque in gran parte dei servizi pubblici bibliotecari e in qualche libreria, si è cercato di estendere la scelta con i libri che trattano di tale tematica ma che trovano una diffusione ristretta nella collettività sarda, avvalendosi pertanto dell’acquisto anche di libri delle case editrici specializzate in materia come Playground, Del Cardo, Il Dito e la Luna, Edizioni Libreria Croce, etc, che magari hanno pubblicato in questi anni opere molto interessanti ma non conosciuti a livello locale e regionale. Questa nuova opportunità pertanto dovrebbe costituire un’opportunità sia per gli utenti LGTB che anche eterossessuali che hanno interesse, in ambito letterario, a documentarsi sulla “Letteratura a Tematica Omosessuale”.  
  
Quest’iniziativa, che nel suo piccolo rimane significativa, è di buon auspicio affinché possano crearsi i presupposti per ulteriori passi e aperture della realtà locale sarda ai problemi e alle istanze della comunità gay, che ancora oggi si sente emarginata e marginale nella vita e nell’accettazione ma rivendica comunque lo status di “cittadinanza”, fruitrice pertanto di diritti e servizi. Simboleggia quindi un primo passo su un progetto e percorso che coinvolgerà, tramite la presentazione di alcuni servizi, diversi settori dell’area dei servizi alla persona.  
La biblioteca è aperta a tutti, anche ai non residenti nel Comune, per ulteriori informazioni basta consultare il sito internet www.comune.elmas.ca.it alla voce Biblioteca Comunale. Gli orari di apertura sono attualmente così strutturati:  
  
BIBLIOTECA COMUNALE DI ELMAS P.zza San Sebastiano - Tel. 070 2135095  
ORARIO “Invernale” – Dal 1° Ottobre alla 1^ settimana di Giugno  
Mattino: Lun – Mer – Ven – Sab dalle 9 alle 12  
Sera : dal Lun al Ven dalle 15.30 alle 18.30  
  
ORARIO “Estivo” – Dalla 2^ settimana di Giugno al 30 Settembre  
Mattino: dal Lun al Ven dalle 9 alle 12  
Sera : Lun – mer – Gio dalle 16.30 alle 19.30