---
title: 'ECCO PERCHE’ LA “REGOLAMENTAZIONE” DELLA PROSTITUZIONE E’ L’UNICA PROSPETTIVA POSSIBILE'
date: Tue, 03 Aug 2010 08:58:30 +0000
draft: false
tags: [Comunicati stampa]
---

Di Gaspare Serra

UNA DOVEROSA PREMESSA:  
  
Propongo alla vostra attenzione un saggio dedicato al tema della prostituzione in Italia, che (numeri e normative alla mano) cerca di analizzare i mille risvolti del problema.

  
  
Premessa “imprescindibile” per una corretta valutazione delle considerazioni svolte è la necessità di distinguere tra:  
\- una prostituzione “volontaria”  
\- ed una prostituzione “coatta”  
(e, di conseguenze, di approntare “politiche diverse” per “problematiche diverse”!).  
  
L’analisi svolta nell’articolo arriva ad una conclusione:  
a- nell’ambito della prostituzione volontaria, il “proibizionismo” non ha pagato negli anni! L’unica alternativa possibile è una “parziale regolamentazione” (o legalizzazione) dell’attività di prostituzione;  
b- nell’ambito della prostituzione coatta, invece, occorre rafforzare i mezzi e gli strumenti con cui perseguire lo sfruttamento della prostituzione.  
  
In sintesi, tre sono i punti focali del dossier in oggetto:  
1- le “schiave” della prostituzione (le donne coartate, costrette a vendersi) andrebbero aiutate ad uscire da tale racket (a tal fine, si propone ad esempio sia l’estensione a queste del programma di aiuto e protezione attualmente previsto per i pentiti di mafia sia un severo inasprimento delle pene per gli sfruttatori)  
2- chi, invece, sceglie di prostituirsi per “bisogno” o “emarginazione sociale”, andrebbe aiutato con appositi programmi di assistenza economica e reinserimento sociale (ad esempio, offrendo loro assistenza nel cercare un lavoro)  
3- le excort (coloro che scelgono “liberamente” e “volontariamente” di prostituirsi: in primis per arricchirsi facilmente o far carriera), invece, “esistono” (negarlo sarebbe una facile “ipocrisia”, se non altro per ciò che rilevano tutti gli studi di settore -del Censis, Parsec, Caritas, gruppo Abele, Osservatorio sulla Prostituzione del Min. dell’Interno…). A loro (e solo a loro) andrebbe allora riconosciuta la libertà di prostituirsi:  
\- in privato (a tutela dell’ordine pubblico e del buon costume)  
\- e legalmente (riconoscendo un sistema di diritti e doveri).  
Spetterebbe alla Società, alla Politica, alla Scuola ed alle Famiglie, a tal punto, saper trasmettere ai giovani i giusti e corretti modelli di comportamento per condannare “moralmente” (non certo penalmente) tali facili costumi …  
  
Premesso che il sottoscritto, nonostante la delicatezza e complessità del tema trattato, si augura di non aver offeso la “sensibilità” di nessun lettore, auspico in particolare che tutti coloro che vogliano commentare tale articolo:  
a- precedano al commento (ad ogni libera e personale considerazione) una lettura “attenta” del dossier (per evitare facili “fraintendimenti” nell’interpretazione del pensiero dello scrivente …)  
b- e utilizzino toni e linguaggi moderati e rispettosi delle posizioni di ognuno.  
  
Grazie …  
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-  
  
  
I NUMERI DEL “MERCATO DEL SESSO” IN ITALIA:  
  
La prostituzione è forse l’unica “attività lucrosa” che non ha mai attraversato periodi di crisi nella storia dell’umanità … (non a caso la si riconosce come il “mestiere più vecchio del mondo”!).  
  
Secondo i dati resi pubblici nel corso dell’ultimo Convegno della Caritas (svoltosi nel 2008, in occasione del 50° anniversario della legge Merlin n.75 del 1958):  
\- in Italia operano non meno di “70 mila prostitute” (non solo donne, anche uomini e transex)  
\- di queste, circa il 50% sono straniere (provenienti da ben 60 paesi diversi: nigeriane, albanesi, polacche e bielorusse soprattutto) ed il 20% minorenni  
\- le donne che si prostituiscono in strada sono circa 30.000: le restanti esercitano la “professione” in casa o in locali privati  
\- solo il 20% (secondo altri dati addirittura il 10%) di chi si prostituisce è vittima del racket (generalmente straniera, si tratta di donne condotte in Italia con il miraggio di un lavoro dignitoso per poi, sequestrati i documenti, essere costrette a prostituirsi attraverso violenze e minacce, rivolte anche a parenti, genitori o figli rimasti in patria)  
\- sono “9 milioni” i clienti (di cui ben l’80% richiede rapporti “non protetti”)  
\- per un giro d’affari che si aggira attorno ai 90 milioni di euro al mese (oltre un miliardo l’anno!)  
(per conoscere più dettagliatamente i dati del “gruppo Abele”: [http://www.gruppoabele.org/Index.aspx?idmenu=3378](http://www.gruppoabele.org/Index.aspx?idmenu=3378) ).  
  
Questi numeri, pur nella loro “approssimatività” (trattandosi di una attività occulta), sono sufficienti per comprendere le dimensioni del fenomeno in oggetto.  
  
  
LO “SFRUTTAMENTO” DELLA PROSTITUZIONE:  
  
Parlando della prostituzione “coatta” (forzata), dietro questo giro d’affari non si nasconde più, prevalentemente, il singolo sfruttatore (il “lenone” o “magnaccia”) o la mafia locale: a gestire la prostituzione sono oramai le mafie internazionali (alle quali le nostre mafie hanno lasciato spazio dopo aver preferito il più redditizio commercio della droga).  
Si stima che ogni mese una prostituta renda al proprio sfruttatore dai 5 ai 7mila euro!  
  
La prostituzione coatta straniera è gestita, per lo più, dagli stessi trafficanti internazionali che organizzano la “tratta” di essere umani:  
\- i più crudeli sembrano essere i nigeriani (pronti a ricorrere a brutalità, ricatti e ritorsioni verso i familiari delle vittime)  
\- gli albanesi si sono allontanati dalle pratiche più violente (che spesso comportavano la fuga delle prostitute, col conseguente rischio di essere scoperti), preferendo ora lasciar loro più libertà e guadagni  
\- le cinesi, ultime arrivate, vengono fatte prostituire solo nelle case private (diventando di fatto “invisibili”) e sono destinate per lo più a clienti connazionali.  
  
Con l’ingresso recente di molti paesi dell’Est Europa nella UE (ad esempio, la Romania) risulta calato, invece, lo sfruttamento della prostituzioni delle ragazze dell’est: potendo queste raggiungere liberamente l’Italia, non hanno più la necessità di appoggiarsi alle associazioni criminali per soggiornare clandestinamente nel nostro Paese.  
  
  
LA “LEGGE MERLIN” SULLA PROSTITUZIONE  
  
In Italia il fenomeno della prostituzione è disciplinato dalla “legge Merlin” n.75 del 1958, che 51 anni fa abolì (rese illegali) le “case di tolleranza” (o “case chiuse”), vietando la prostituzione “indoor” (il testo della legge è consultabile su: [http://www.caritas.it/Documents/25/2188.pdf](http://www.caritas.it/Documents/25/2188.pdf) ).  
  
La prostituzione viene così definita come un’attività che prevede “atti sessuali prestati dietro pagamento” (non necessariamente in denaro ma anche in natura: l’offerta di un luogo dove abitare, di qualcosa da mangiare, di sostanze stupefacenti …).  
La legge Merlin:  
1- rende legale la prostituzione (salvo, ovviamente, quella minorile)  
2- ne vieta, però, l’esercizio in “forma organizzata” o “al chiuso” (è tollerata, dunque, la prostituzione “outdoor”: per tale ragione gran parte delle prostitute esercitano per strada, pur col rischio di essere multate per il reato di adescamento)  
3- punisce, invece, l’adescamento, il favoreggiamento e lo sfruttamento della prostituzione (che costituiscono reato).  
  
  
LA LEGISLAZIONE SULLA PROSTITUZIONE NEL RESTO D’EUROPA:  
  
La regolamentazione normativa della prostituzione non è uniforme nei diversi paesi europei …  
  
In OLANDA la prostituzione è legale dal 1815: è sufficiente aver raggiunto la maggiore età per poterla esercitare.  
Si tratta di una professione come ogni altra: vengono pagate le tasse sui proventi dell’attività e le prostitute sono considerate lavoratrici a tutti gli effetti (con conseguenti diritti e doveri).  
Chi si prostituisce non è soggetto a controlli sanitari (per non violarne la privacy).  
Di recente (dall’ottobre 2000) sono diventati legali anche le case chiuse. Sono anche disponibili undici zone “speciali”, dove le professioniste del sesso lavorano all’aperto (al di fuori da queste zone, però, chi si prostituisce rischia l’arresto!).  
  
In BELGIO la prostituzione è legale fin dal 1948. Non sono perseguibili le prostitute né i clienti, salvo se tale attività turba l’ordine pubblico.  
Le prostitute debbono essere in regola con il fisco (pagano le tasse) proprio come delle lavoratici autonome e possono godere anche di assistenza sociale.  
Le case chiuse sono proibite così come lo sfruttamento, il favoreggiamento e l’adescamento: la prostituzione, però, è “tollerata” in club, bar e vetrine sulla strada.  
  
In AUSTRIA la prostituzione è consentita nelle case chiuse ed è obbligatoria una registrazione di esercizio. All’aperto è tollerata solo in alcune aree urbane ed extra-urbane.  
  
In GERMANIA la prostituzione è regolamentata da una recente legge che la legalizza equiparandola ad una normale attività, consentendo l’apertura di case d’appuntamento. Sono circa 400.000 le “lavoratrici del sesso” che godono di tutte le garanzie assicurative in materia di malattia, disoccupazione e pensione.  
Le prostitute non sono soggette a controlli sanitari per non violarne la privacy.  
Il favoreggiamento non è punibile (sempre che non vi sia sfruttamento).  
  
In GRECIA chi esercita la prostituzione ha l’obbligo di iscriversi in appositi registi e deve sottoporsi periodicamente a visite mediche che autorizzano a svolgere tale attività.  
  
In INGHILTERRA la prostituzione non è reato ma lo sono il favoreggiamento, lo sfruttamento, la pubblicità e l’adescamento in luoghi pubblici.  
Il lavoro si svolge prevalentemente in locali e abitazioni private, ma anche in strada.  
  
In SPAGNA le case chiuse sono illegali dal 1956 ma, di fatto, si sono trasformate in “club”.  
Dal 1995 la legge non vieta la prostituzione ma punisce chi ricatta e sfrutta le prostitute.  
  
In SVIZZERA la prostituzione è legale. Nel Canton Ticino viene anche esercitata all’interno di bar-alberghi.  
Una recente legge (però rimasta inapplicata) ha anche introdotto la patente per affittacamere e la registrazione delle prostitute.  
  
In SLOVENIA la nuova legislazione del 2003 ha depenalizzato l’attività di prostituzione (nel vecchio regime considerata reato) e aumentato le pene per gli sfruttatori: non si proibisce, dunque, la prostituzione (né “indoor” né “outdoor”) mentre lo sfruttamento viene severamente punito!  
  
In FRANCIA le “case di tolleranza” sono state chiuse nel 1946 con la legge Marthe Richard (proprio come avvenuto in Italia in seguito alla c.d. legge Merlin).  
Mentre è proibita la prostituzione “indoor” è concessa la prostituzione “outdoor” (all’aperto). E’ punito, però, sia l’adescamento dei clienti (violando la tranquillità e l’ordine pubblico) che il favoreggiamento.  
  
In IRLANDA la prostituzione è reato.  
Non esistono case chiuse e sono previste ammende ed arresto per le prostitute ed i clienti.  
  
In SVEZIA la prostituzione può essere esercitata ma sono previste ammende e pene sia per i clienti (secondo una legge in vigore dal gennaio 1999, i clienti colti in flagrante rischiano da sei mesi a un anno di carcere) che per chi sfrutta e per chi affitta locali destinati alla prostituzione.  
Non sono punibili le prostitute: è però sanzionato l’adescamento (sia se compiuto da chi vuole vendere la prestazione sessuale, dunque, sia se compiuto da chi la vuole comprare -il cliente-).  
  
  
LE NOVITA’ PROPOSTE NEL “D.D.L. CARFAGNA”  
  
Uno dei primi provvedimenti adottati dal Governo Berlusconi è stato la presentazione di un disegno di legge (a firma del Ministro per le Pari Opportunità, Mara Carfagna) per perseguire la prostituzione di strada.  
  
La necessità di una nuova regolamentazione in materia è facilmente spiegabile:  
1- la prostituzione risulta disciplinata da una legge risalente al 1958 (quando le lucciole autorizzate a lavorare nelle case chiuse erano solo 4.000 ed i “bordelli” autorizzati 714)  
2- e la normativa vigente ha permesso numerose “storture del diritto” (sindaci intraprendenti o giudici solerti hanno facilmente “abusato” di interpretazioni forzate, rispettivamente, del Codice della Strada e del Codice Penale al solo fine di punire più efficacemente i clienti).  
  
Il provvedimento in esame (il cui testo è consultabile su: [http://www.senato.it/japp/bgt/showdoc/frame.jsp?tipodoc=Ddlpres&leg=16&id=313179](http://www.senato.it/japp/bgt/showdoc/frame.jsp?tipodoc=Ddlpres&leg=16&id=313179) ) mira ad introdurre le seguenti novità:  
1) INTRODUZIONE DEL “REATO DI PROSTITUZIONE IN LUOGO PUBBLICO O APERTO AL PUBBLICO”.  
Viene vietato prostituirsi in strade, parchi, aperta campagna o in luoghi aperti al pubblico (come locali pubblici o posti accessibili al pubblico) in quanto ciò desterebbe “allarme sociale”. Si prevede l’arresto da 5 a 15 giorni, con ammenda da 200 a 3mila euro.  
2) PERSECUZIONE DEI CLIENTI.  
I clienti delle prostitute “outdoor” diverranno penalmente perseguibili, rischiando le stesse pene previste per le “professioniste” (da 5 a 15 giorni di arresto ed una ammenda da 200 fino a 3mila euro).  
3) INASPRIMENTO DELLE PENE NEI CASI DI PROSTITUZIONE MINORILE.  
4) SEMPLIFICAZIONE DELLE PROCEDURE PER IL RIMPATRIO DEI MINORI.  
Sono previste procedure semplificate ed accelerate per il rimpatrio dei minori extracomunitari non accompagnati che si prostituiscono.  
5) INASPRIMENTO DELLE PENE PER L’ASSOCIAZIONE PER DELINQUERE FINALIZZATA ALLO SFRUTTAMENTO DELLA PROSTITUZIONE.  
Si prevede la reclusione da 4 a 8 anni per i promotori e organizzatori dell’associazione e da 2 a 6 anni per i partecipanti.  
  
Nonostante tale d.d.l. fosse stato annunciato come un “provvedimento urgente” per contrastare la prostituzione e “far pulizia” nelle strade (orribile espressione giornalisticamente!), occorre constare come, per il momento, tale d.d.l. sia stato decisamente accantonato … Forse lo scandalo “excort” che ha coinvolto il Presidente del Consiglio in persona ha reso “inopportuno” affrontare il tema in un pubblico dibattito parlamentare? (!)  
  
  
ANALISI CRITICA DEL D.D.L. CARFAGNA:  
  
  
I- VIETARE LA PROSTITUZIONE IN STRADA SPOSTERA’ SEMPLICEMENTE LE PROSTITUTE IN CASA!  
  
Il d.d.l Carfagna non vieta la prostituzione “tout court” ma solo quella “outdoor”: in pratica, costringerà le donne che “vendono” il proprio corpo a farlo al chiuso!  
  
Secondo il Ministro Carfagna “è giusto proibire la prostituzione non per essere moralisti ma perché fino ad oggi l’Italia è stata terra di conquista per il racket”. Non si può non essere d’accordo sul tale questione di principio …  
Ma siamo convinti che spingere le prostitute ad esercitare “clandestinamente” la loro professione tra le mura domestiche sia un modo più efficace per combattere questo racket?  
Siamo certi che le donne che continueranno a prostituirsi “indoor” (al chiuso degli appartamenti) saranno più libere e tutelate?  
Siamo sicuri che il modo migliore per affrontare il problema prostituzione sia “criminalizzare” le donne che si prostituiscono (in luogo dei loro aguzzini!)?  
  
Sempre il Ministro per le Pari Opportunità ha dichiarato che la prostituzione è “un fenomeno vergognoso che spesso è connesso alla riduzione in schiavitù, all’uso e all’abuso dei minori, che a volte sfocia anche in fenomeni di violenza come lo stupro, tutti fenomeni strettamente collegati alla prostituzione in strada”. Una denuncia certamente fondata …  
Ma siamo sicuri che il provvedimento proposto rappresenti -come dichiarato dal Ministro- “uno schiaffo durissimo per togliere linfa al mercato della prostituzione”?  
Non è probabile, al contrario, che il mercato della prostituzione più che essere smantellato sarà semplicemente costretto a “riorganizzarsi al chiuso”?!  
  
Il problema della prostituzione:  
\- non può essere affrontato solo come una questione di tutela dell’ “ordine pubblico” e del “buon costume”  
\- ma, al contrario, impone anche la tutela della dignità, salute e sicurezza di chi volontariamente si prostituisce!  
Spostare la prostituzione (ed il racket annesso) all’interno di appartamenti chiusi (all’oscuro per forze dell’ordine ed operatori sociali) renderà ancor più difficile contrastarla!  
Come se non bastasse, il d.d.l. governativo non ha nemmeno tentato di contenere tali conseguenze prevedendo aggravanti per il reato di sfruttamento della prostituzione esercitata al chiuso.  
  
  
II- ESPELLERE I MINORI CHE SI PROSTITUISCONO METTERA’ A REPENTAGLIO LA LORO SICUREZZA!  
  
Il d.d.l. Carfagna prevede il “ricongiungimento familiare” nel paese di origine (in pratica, l’espulsione!) per le prostitute extracomunitarie minorenni senza permesso di soggiorno (ossia clandestine).  
In questo modo, però, si finisce soltanto per colpire “due volte” la prostituta vittima del racket della prostituzione:  
\- prima “obbligata” con l’inganno e con la forza al meretricio  
\- poi espulsa in maniera “coatta” nel suo paese di origine (esponendola sia alle vendette delle organizzazioni che l’avevano schiavizzata sia al rischio di essere rimandata sulla strada in un altro paese europeo!).  
  
(per una analisi critica più approfondita del d.d.l. Carfagna: [http://www.caritasitaliana.it/materiali/Italia/prostituzione/DocumentoProstituzioneTratta.pdf](http://www.caritasitaliana.it/materiali/Italia/prostituzione/DocumentoProstituzioneTratta.pdf) )  
  
  
PROPOSTE PER UN “DIVERSO APPROCCIO” AL FENOMENO DELLA PROSTITUZIONE:  
  
  
PRIMO:  
PERCHE’ NON CONSENTIRE LA PROSTITUZIONE “LIBERA E VOLONTARIA”?  
  
Occorre sfatare una “comune ipocrisia”: non tutte le donne che si prostituiscono sono “schiave” costrette a farlo!  
Accanto alla prostituzione “coatta” (che costituirebbe non più del 20% di tale mercato, secondo i dati forniti da organizzazioni come il Censis ed il Parsec) esiste anche una prostituzione “volontaria”; la “non prevalenza” della costrizione nella prostituzione, del resto, è stata affermata anche dall’Osservatorio sulla Prostituzione del Ministero dell’Interno (composto da molte tra le più reputate organizzazioni di assistenza).  
A prostituirsi sono non soltanto soggetti deboli, soggiogati, sfruttati o “costretti” a vendersi ma anche persone che scelgono liberamente “la strada” (o le “suite” d’alto borgo!) come comoda scorciatoia per realizzare “soldi facili”, per emanciparsi economicamente!  
  
Personalmente giudico la prostituzione come un’attività “immorale” e umiliante la “dignità umana” (sia di chi la esercita sia di chi ne beneficia!).  
Il legislatore, però, non è chiamato ad esprimere “giudizi morali” nell’esercizio della sua funzione di legislazione: dovrebbe assumere un approccio il più possibile “laico”, rispettoso delle diversità di opinioni e delle minoranze.  
Ciò comporta il dovere di riconoscere e rispettare pienamente la “libertà di espressione sessuale” di ogni persona, finanche se quest’ultima scelga di prostituirsi!  
Prostituirsi sarà pure un “peccato” o una condotta immorale per la generalità dei cittadini: questo, però, non basta a trasformare tale comportamento in un reato!  
“Vendere” il proprio corpo rientra -piaccia o non piaccia- tra quelle libertà personali garantite dalla Costituzione (art. 13) e meritevoli “sempre” di tutela nei limiti in cui non incidano sulla “pari libertà” degli altri!  
  
Perché, allora, non tollerare (e regolamentare) la “libera e consapevole” scelta di un soggetto maggiorenne (uomo o donna che sia) di concedere prestazioni sessuali dietro controprestazione?  
Una “parziale legalizzazione” della attività di meretricio perseguirebbe un duplice obiettivo:  
1- far emergere la prostituzione “volontaria” (sull’esempio di quanto avvenuto in altri paesi europei, dove questa ha trovato forme legali di svolgimento, minimizzando i costi che ricadono sulla società e sulle persone che svolgono l’attività)  
2- e perseguire più efficacemente la prostituzione “coatta”, ossia il favoreggiamento e lo sfruttamento della prostituzione (concentrando gli sforzi dell’apparato repressivo dello Stato sul contrasto agli sfruttatori).  
  
Un d.d.l alternativo a quello del Governo per la “regolamentazione della prostituzione volontaria” è già presente in Parlamento, a firma dei sen. Donatella Poretti e Marco Perduca (il testo è consultabile su: [http://www.senato.it/japp/bgt/showdoc/frame.jsp?tipodoc=Ddlpres&leg=16&id=303190](http://www.senato.it/japp/bgt/showdoc/frame.jsp?tipodoc=Ddlpres&leg=16&id=303190) ).  
Tale progetto prevede:  
1- la abrogazione della legge Merlin  
2- il riconoscimento dell’esercizio della prostituzione come una attività lavorativa attraverso cui si offrono servizi sessuali regolarmente remunerati  
3- l’assoggettamento dei profitti derivanti dalla vendita di prestazioni sessuali al prelievo fiscale  
4- e l’introduzione di una serie di misure a cui le persone che esercitano tale attività devono sottostare (controlli sanitari, norme igieniche, regole di sicurezza dei locali in cui viene esercitata tale attività …)  
Questo disegno di legge può rappresentare una ragionevole base di partenza per una riforma della materia, sia pur crediamo che occorrerebbe migliorarlo:  
1- con l’introduzione di un espresso divieto di esercitare la prostituzione in strada (o in luogo pubblico o aperto al pubblico), a tutela dell’ordine pubblico e del buon costume  
2- con la previsione di una regolamentazione delle “case di tolleranza”  
3- con l’inasprimento delle pene attualmente previste per lo sfruttamento della prostituzione  
4- e con l’introduzione del vincolo per lo Stato di destinare tutti i proventi del prelievo fiscale sulle prostitute a progetti di “educazione alla sessualità” e di “reinserimento sociale” per le donne che decidono di uscire da tale mercato.  
  
  
SECONDO:  
PERCHE’ NON CONSENTIRE LA PROSTITUZIONE ALL’INTERNO DI “CASE DI TOLLERANZA”?  
  
Perché non regolamentare la prostituzione “indoor”?  
Le case chiuse -pur se luoghi “abominevoli”- di fatto già esistono ed, inoltre, costituiscono un luogo certamente più sicuro e civile della strada per prostituirsi!  
  
Perché, inoltre, non favorire la nascita di “cooperative di donne”, col doppio risultato:  
1- di eliminare la prostituzione dalle strade (rispettando la sensibilità delle famiglie, stanche di assistere alla “contrattazione del sesso” nei marciapiedi sotto casa!)  
2- e di tutelare la salute, dignità e libertà delle “professioniste del sesso” (la nascita di “libere case autogestite” renderebbe le donne più forti e capaci di resistere alle pressioni della criminalità)?  
  
Riconoscere la prostituzione come una “professione legale” consentirebbe, inoltre:  
1- sia di “tassare” tale attività, imponendo delle autocertificazioni di reddito presunto (controllato periodicamente dagli enti statali competenti in base al tenore di vita effettivo)  
2- sia di “regolamentare” la prostituzione, introducendo un sistema di diritti e doveri (ad esempio, richiedendo il rispetto di condizioni igienico-sanitarie sicure).  
  
  
TERZO:  
PERCHE’ NON PERSEGUIRE PIU’ SEVERAMENTE IL “FAVOREGGIAMENTO” E LO “SFRUTTAMENTO” DELLA PROSTITUZIONE?  
  
Le “schiave del sesso” non sono la maggioranza delle donne che in Italia si prostituiscono: rappresentano, comunque, una minoranza rilevante (si stima intorno al 20%).  
Attualmente lo “sfruttamento” della prostituzione è disciplinato:  
\- dall’art. 3 della legge Merlin (n. 75 del 1958), che prevede una pena della reclusione da 2 a 6 anni  
\- e dall’art. 600 bis c.p., che prevede un aggravamento di pena (da 6 a 12 anni di reclusione) nel caso della prostituzione minorile.  
  
Perché non equiparare le pene previste per lo “sfruttamento della prostituzione” a quelle previste per il reato di “sequestro di persona” (che, ex art. 630 c.p., prevede la reclusione da 25 a 30 anni)?  
Ciò consentirebbe di punire in modo “esemplare” chiunque:  
\- abusi di una donna  
\- la usi a fini di lucro  
\- e/o ne limiti le libertà (in primis, la libertà sessuale).  
  
  
QUARTO:  
PERCHE’ NON VIETARE OGNI FORMA DI PROSTITUZIONE SULLE STRADE?  
  
La prostituzione -come ogni libertà- va incontro a dei limiti: in nessun caso dovrebbero ammettersi forme di prostituzione lesive:  
\- dell’ “ordine pubblico”  
\- o del “buon costume”.  
I marciapiedi delle nostre città, invece, di notte sono pieni di donne (per lo più straniere: nigeriane, cinesi, russe, ucraine, slovene, albanesi …) disposte, senza pudori, a mettere in vetrina il proprio corpo per adescare i clienti. Uno spettacolo indegno e irrispettoso della libertà della gente che vive in questi quartieri!  
  
Perché, allora, non vietare la prostituzione di strada (o in luogo pubblico o aperto al pubblico) “in qualsiasi forma” esercitata?  
Ciò sarebbe quello cui punta il d.d.l. Carfagna. Resta fermo, però, il limite “originario” di tale progetto di legge: non avere il coraggio di affrontare globalmente il problema introducendo una regolamentazione della prostituzione “indoor”.  
  
  
QUINTO:  
PERCHE’ NON REALIZZARE PROGETTI DI “REINSERIMENTO SOCIALE” PER LE DONNE CHE SI PROSTITUISCONO?  
  
Perché non estendere alle donne “vittime” della prostituzione (e che decidono di collaborare per liberarsi da tale racket) il sistema di aiuto e protezione già previsto per i mafiosi collaboratori di giustizia?  
Nel caso della prostituzione “coatta”, sarebbe un aiuto concreto l’offerta da parte dello Stato, in cambio della denuncia dei propri aguzzini, della garanzia di ottenere:  
1- un permesso di soggiorno (nel caso di donne extracomunitarie)  
2- un programma di protezione (ove occorrente)  
3- ed un piano di assistenza economica.  
  
Per contrastare la prostituzione “volontaria”, invece, servirebbero anzitutto politiche di sostegno economico: garantire, in pratica, che “per nessuna persona” la strada della prostituzione divenga l’unica opportunità per vivere o per mantenere i propri figli!  
Solo così si potrebbe ridurre il fenomeno della prostituzione ai limiti del “fisiologico” …  
  
  
SESTO:  
PERCHE’ NON INTRODURRE L’INSEGNAMENTO DELLA “EDUCAZIONE ALLA SESSUALITA’” NELLE SCUOLE?  
  
Il fenomeno della prostituzione volontaria, infine, è un problema legato:  
\- oltre che ad aspetti economici  
\- anche (o soprattutto) a “devianze culturali” (sia dal lato dell’offerta che della domanda!).  
Non si potrebbe altrimenti spiegare il proliferare di ragazze (spesso studentesse universitarie) disposte a prostituirsi in cambio di facile denaro: non solo (o non tanto) per mantenersi negli studi quanto per potersi permettere un guardaroba griffato o la frequentazione dei locali notturni più alla moda!  
  
Concedersi a pagamento perché altri facciano del proprio corpo quello che si vuole è una scelta mostruosa!  
L’atto sessuale dovrebbe essere compiuto solo per Amore: svilirlo con l’offerta di denaro rende l’uomo più simile alle bestie ed il rapporto in sé un rapporto senz’anima, senza emozioni, intriso della più grezza sessualità!  
E’ difficile, però, condannare questo “delirio” quando i messaggi che tv e mass-media (a volte perfino la politica!) trasmettono giornalmente sono sempre più tesi:  
\- alla mercificazione del valore della donna  
\- alla proposizione di modelli culturali aberranti  
\- ed alla istigazione ad una visione “morbosa” del sesso!  
  
Perché non promuovere, allora, una campagna culturale ed educativa che aiuti (specie i più giovani) a prendere consapevolezza dell’ “orrore” della prostituzione?  
Sarebbe auspicabile, anzitutto, una riformulazione dei piani di studio scolastici che preveda l’introduzione dell’insegnamento della “Educazione alla sessualità” in tutte le scuole pubbliche (di ogni ordine e grado).  
L’educazione è l’unica arma vincente in grado contrapporsi alla “diseducazione” morbosa e strisciante dei giovani e ragazzi. Come è possibile, difatti, educare i giovani ad un rapporto “non traumatico” (o esagitato) col sesso se:  
\- in famiglia e nelle scuole, questo aspetto integrante della vita di ogni individuo continua ad essere un “tabù”  
\- e la “pornografia virtuale”, il più delle volte, risulta essere l’unica vera lezione sessuale alla portata di tutti?!