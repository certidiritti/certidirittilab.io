---
title: 'OGGI A ROMA MANIFESTAZIONE CONTRO TRANSFOBIA CO-PROMOSSA DA CERTI DIRITTI, LE ADESIONI PERVENUTE'
date: Fri, 28 Nov 2008 14:26:43 +0000
draft: false
tags: [ADESIONI, Comunicati stampa, MANIFESTAZIONE, transfobia]
---

Fiaccolata contro la violenza e la discriminazione verso le persone transessuali.  
   
Oggi, venerdì 28 novembre, alle ore 17.30, a Roma, sulla scalinata del Campidoglio, si terrà una fiaccolata contro la violenza e le discriminazioni di cui sono vittime le persone transessuali. Verrà letto un messaggio di Vladimir Luxuria.  
 

Le persone transessuali sono vittime di derisione e persecuzioni di ogni genere, alimentate dal pregiudizio e dall'ignoranza. Lunedì notte a Roma è stata assassinata Roberta, una transessuale brasiliana. Nel 2008, contro le persone trasngender e transessuali, ci sono stati quattro omicidi e diversi atti di inaudita violenza in varie parti del paese, in particolare a Roma, che spesso non vengono neanche denunciati; le aggressioni sono quasi sempre ispirate da un atteggiamento di pregiudizio e violenza che ha come cornice l'omofobia, molto diffusa nel nostro paese, alimentata anche dai provvedimenti governativi e del Comune di Roma contro la prostituzione; le persone transgender e transessuali vivono una condizione soggettiva difficile che necessiterebbe di maggiore aiuto e assistenza da parte delle strutture pubbliche, in particolare in ambito sanitario e sociale.  
   
Alla manifestazione co-promossa dalle Associazioni Libellula, Coordinamento Silvia Ravera, Cgil Nuovi Diritti, La Strega da bruciare e dai Radicali Certi Diritti, parteciperanno, tra gli altri:- Rita Bernardini, deputata radicale del Pd,  Paola Concia, deputata del Pd; Marco Perduca, Senatore radicale del Pd; Antonella Casu, Segretaria di Radicali Italiani; Michele De Lucia, Tesoriere di Radicali Italiani;  Gigliola Toniollo, Cgil Nuovi Diritti; Leila Deianis, Ass. Libellula; Massimiliano Iervolino, Segretario Radicali Roma; Sergio Rovasio, Segretario Associazione Radicale  Certi Diritti.  
   
Tra le personalità hanno preannunciato, tra gli altri, la loro partecipazione: Franco Grillini, Presidente onorario di Arcigay, Rossana Praitano, Presidente del Circolo Mario Mieli, Aurelio Mancuso, Presidente nazionale di Arcigay, Imma Battaglia, Presidente  
Digayproject, Daniele Gosti e Luca Liguoro, Segretario e Presidente di Rosa Arcobaleno; Daniele Priori, Vice-Presidente di GAyLib, l'Associazione AzioneTrans.  
   
LE ADESIONI PERVENUTE

ASSSOCIAZIONI ADERENTI

Arcigay nazionale

Associazione DjGayProject

Associazione Luca Coscioni

Associazione Radicale Certi Diritti

Associazione Rosa Arcobaleno

Associazione Strega Da Bruciare Roma

Cantieri Sociali Napoli

Coordinamento delle Associazioni Trans Sylvia Rivera

Comitato Diritti civili delle prostitute

Circolo De Cultura Omosessuale Mario Mieli

Coordinamento Torino Pride

Coop.Dedalu Napoli

Donne Brasiliane In Italia

Facciamo Breccia

Famiglie Arcobaleno

Gaylib

Mit Bologna

Mit Roma

No.Di I Nostri Diritti

Radicali Italiani

Radicali Roma

Redazione Di Babilonia

Personalità Aderenti:

Andrea Mele Circolo Mieli

Andrea Morniroli – Cooperativa Dedalus Napoli

Angela Azzarro Giornalista

Annachiara Gourgeus Roma

Antonella Casu Segreteria Radicali Italiani

Aurelio Mancuso Presidente Arcigay Nazionale

Carla Corso Attivista

Carla Valeri Associazione Di Volontariato Magliana’80

Carlo Guarino – Arcigay Roma

Carmine Amoroso - Regista

Daniele Nalbone Giornalista

Daniele Pace Pastorino Attore

Danieli Nalbone – Libera Roma

Deborah Di Cave – Assistente sociale, attivista

Edoardo Del Vecchio

Emanuele Decembini – giornalista

Enzo Cucco – esponente movimento lgbt

Fabianna Tozzi Daneri – Presidente Associazione Transgenere

Fabrizio Marrazzo Presidente Arcigay Roma

Francesca Merli Psicologa

Franco Grillini, Presidente Onorario Di Arcigay

Gianluca Bottoni Regista

Giorgio Rainelli – Presidente Refo

Giovanni Anversa Giornalista Rai

Giuseppina La Delfa Famiglia Arcobaleno

Hervè Ducroux – cineasta

Imma Battaglia Attivista

Leila Daianis Attrice E Attivista

Letizia Tommasini – Pastore Valdese

Liberaroma

Loris Antonelli, Associazione di promozione sociale Diversamente

Luca Liguoro, Presidente Associazione Rosa Arcobaleno

Marcella Di Folco M.I.T Bologna

Marcia Leite Mediatrice Culturale

Marco Perduca, Senatore Radicale - Pd

Marea Cooperativa Sociale

Maria Gigliola Toniollo C.G.I.L. Nazionale - Nuovi Diritti

Mario Cirrito Giornalista

Michele De Lucia, Tesoriere Di Radicali Italiani

Mina Welby, esponente ass. Luca Coscioni

Mirella Izzo – Azione Trans

Mirella Sartori – Italia laica

Monica Rosellini Associazione Strega Da Bruciare Roma

Paola Concia, Deputata Pd

Pepa, drag queen

Pia Covre Attivista

Pietro Pipi, Segretario Associazione radicale di Gorizia

Pilar Saravia Antropologa - Nodi

Poppea, drag queen

Porpora Marcasciano M.I.T. Bologna

Priori Daniele, Vice presidente di Gaylib

Rita Bernardini, deputata radicale del Pd

Rita De Santis – Agedo

Roberta D’Agostini – Consigliere provinciale Roma

Roberta Franciolini – Mit Roma

Rosa Mendes – Presidente Cooperativa Marea, donne brasiliane

Salvatore Marra Cgil Lazio – Nuovi Diritti

Salvatore Bonadonna, esponente di Prc

Sara Bakacs – Psicologa

Selene De Rosa - Attrice

Toni Ventrila - Avvocato

Elena Bigini - Facciamo Breccia

Savero Aversa -Prc

Sinistra Europea

Sergio Rovasio Segretario Ass. Radicale Certi Diritti

Stefano Bolognini – giornalista saggista

Stefano Gentile - Ricercatore Universitario

Tito Flagella - Avvocato

Valery Taccarelli - Mit Bologna

Vladimir Luxuria -

  
Per adesioni: [presidenza@libellula2001.it](mailto:presidenza@libellula2001.it)  
Per info: Sergio Rovasio 06-67609021