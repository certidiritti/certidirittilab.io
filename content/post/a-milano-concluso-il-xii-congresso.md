---
title: 'A Milano concluso il XII Congresso'
date: Mon, 26 Nov 2018 14:41:01 +0000
draft: false
tags: [Politica]
---

Alla fine della tre giorni milanese, confermati come segretario Leonardo Monaco e presidente Yuri Guaiana. Eletto Claudio Uberti tesoriere al posto dell'uscente Dario Belmonte.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

[![46939125_10156514044220973_8436198080180649984_n](http://www.certidiritti.org/wp-content/uploads/2018/11/46939125_10156514044220973_8436198080180649984_n-300x225.jpg)](http://www.certidiritti.org/wp-content/uploads/2018/11/46939125_10156514044220973_8436198080180649984_n.jpg)Al centro dei lavori congressuali gli impegni storici dell'Associazione fondata nel 2008 con lo scopo di difendere e di promuovere, attraverso il metodo radicale della nonviolenza, la libertà e la responsabilità di tutti: dall'impegno rinnovato sul fronte della riforma del **diritto di famiglia** con il filone di ricorsi di "Affermazione Civile", fino agli sforzi per la difesa dei **diritti umani delle persone LGBTI nel mondo**. Mounir Baatour, presidente di Shams, associazione che lotta per la depenalizzazione dell'omosessualità in Tunisia, ha prospettato iniziative congiunte per aumentare la **pressione internazionale sull'abolizione dell'articolo 230 del codice penale tunisino**.

Particolare attenzione ha suscitato il **confronto tra comunità medico-scientifica e attivismo Intersex**: un primo fondamentale passo di dialogo tra i due mondi, verso un lavoro sinergico di riforma della normativa vigente per il rispetto dell'integrità fisica e del diritto all'identità delle persone Intersex.

Tra gli impegni per il 2019 dell'Associazione il sostegno alla **campagna internazionale per il rientro della popolazione mondiale**, presentata alla plenaria dal consigliere regionale lombardo di Più Europa Michele Usuelli, volta alla diffusione della conoscenza e della responsabilità in materia di salute sessuale e riproduttiva. Nella mozione generale approvata non poteva mancare la storica battaglia per la **decriminalizzazione della prostituzione** attraverso la sensibilizzazione istituzionale e le iniziative giudiziarie. Prosegue, insieme all'Associazione Luca Coscioni il lavoro di studio e di promozione di soluzioni normative per una **regolamentazione etica della gestazione per altri** che tuteli i diritti di tutte le parti coinvolte in questa pratica.

Leonardo **Monaco**, segretario dell'Associazione: "Il lavoro di Certi Diritti riparte forte di un dibattito stimolante e ricco. Riparte dall'elemento distintivo dell'Associazione, quello della nonviolenza, in una fase politica di forte crisi del dialogo che richiede azioni urgenti nei confronti di un Potere del non fare che trascurando la complessità della società, nega la soluzione più semplice e intuitiva: l'**affermazione della libertà e della responsabilità dell'individuo**."

Yuri **Guaiana**, presidente: "In un periodo in cui **la politica delle identità si sta diffondendo** – mettendo i diversi gruppi identitari in opposizione tra loro con il rischio di far risorgere razzismo, sessismo e omofobia –  noi di Certi Diritti andiamo avanti sulla strada tracciata da Marco Pannella, Rosa Parks, Martin Luther King e tutto il movimento per i diritti civili : la strada dei diritti umani, dell'individualità e di un'umanità condivisa che conduce a una società dove non tutti, ma ciascun individuo può accedere a tutti i diritti, le libertà e le opportunità offerte dal contesto statuale e sociale che ciascuno di noi contribuisce a costruire"

Così Claudio **Uberti**, neo eletto tesoriere di Certi Diritti: "Come emerso nel corso di un ricco e profondamente umano dibattito che ha animato il congresso appena concluso, Certi Diritti è riuscita, ancora una volta, a essere **uno spazio capace di accogliere**, far crescere e difendere le libertà dell'individuo. La strada verso una società capace di garantire il pieno rispetto dei diritti umani è ancora lunga: continueremo a lavorare con questo obiettivo in Italia, in Europa e nel mondo; **iscrivendovi o facendo una donazione in favore dell'associazione radicale Certi Diritti** vi unirete alla nostra marcia."

* * *

### **Audio-video del Congresso a Cura di radio Radicale:**

[Prima giornata](https://www.radioradicale.it/scheda/558329/xii-congresso-di-certi-diritti-non-possiamo-aspettare-i-tempi-del-potere-prima) - [Seconda giornata](https://www.radioradicale.it/scheda/558330/xii-congresso-di-certi-diritti-non-possiamo-aspettare-i-tempi-del-potere-seconda) - [Terza giornata](https://www.radioradicale.it/scheda/558334/xii-congresso-di-certi-diritti-non-possiamo-aspettare-i-tempi-del-potere-terza-e)

### **Documenti congressuali:**

[Mozione Generale](http://www.certidiritti.org/mozione-generale-approvata-dal-xii-congresso/) - [Mozione particolare sulle elezioni europee](http://www.certidiritti.org/mozione-particolare-sulle-elezioni-europee-del-2019-approvata-dal-xii-congresso/)

Roma, 26 novembre 2018