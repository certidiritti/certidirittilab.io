---
title: 'SONDAGGIO EUROPEO LGBT LANCIATO DALLA FRA'
date: Wed, 28 Mar 2012 03:45:36 +0000
draft: false
tags: [Europa]
---

A parte qualche notizia occasionale relativa alla discriminazione delle persone lesbiche, gay, bisessuali e trans (LGBT), esistono pochissimi dati comparabili raccolti in tutta l’UE sulle esperienze quotidiane delle persone LGBT in merito alla discriminazione.

In risposta a tale situazione, l’Agenzia per i Diritti Fondamentali (FRA) dell’Unione Europea ha lanciato il primo sondaggio on-line in assoluto a livello UE per avere un quadro dettagliato della vita delle persone lesbiche, gay, bisessuali e trans (maggiorenni) che si cercherà di stabilire sulla base delle loro esperienze. Il sondaggio riguarda i 27 Stati membri dell’Unione Europea (compresa Italia) e la Croazia.

In quanto primo sondaggio di questo tipo a livello UE, i risultati sosterranno lo sviluppo delle politiche relative alla parità di trattamento per le persone LGBT nell’Unione Europea e dovrebbero determinarne il programma per gli anni a venire.

In base ai risultati del sondaggio, i responsabili delle decisioni prese a livello nazionale ed europeo, nonché le organizzazioni non governative saranno in grado di orientare meglio le loro strategie ed attività di difesa e sostegno delle comunità LGBT per vivere ed esprimersi liberamente in un ambiente non discriminatorio.

Il sondaggio è completamente anonimo (nessun dato sui partecipanti e le loro sessioni sono accessibili in alcun modo). Il sondaggio è a cura di Gallup, società di consulenza e di sondaggi professionali.

Al fine di dare peso ai risultati, il Sondaggio europeo LGBT conta sulla partecipazione di un ampio e vario gruppo di persone lesbiche, gay, bisessuali e trans in ogni paese. Pertanto, per la riuscita di questo sondaggio è estremamente importante che si raggiunga un ampio gruppo target, grazie all’aiuto dei partecipanti che possono inviare il sondaggio per email, condividerlo attraverso i social network o semplicemente invitare gli amici LGBT a parteciparvi.