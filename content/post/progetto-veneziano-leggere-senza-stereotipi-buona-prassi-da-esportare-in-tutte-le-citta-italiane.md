---
title: 'Progetto veneziano "Leggere senza stereotipi": buona prassi da esportare in tutte le città italiane'
date: Sat, 08 Feb 2014 13:51:46 +0000
draft: false
tags: [Politica]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti.

Roma, 8 marzo 2014

Il Comune di Venezia ha varato ieri il progetto «Leggere senza stereotipi» per il quale nelle biblioteche di di 36 asili nido e 18 scuole materne verranno distribuite 49 favole per bambini che raccontano storie come tante altre, ma senza cadere negli stereotipi più classici. C'è una storia di due pinguini maschi che allevano un cucciolo, quella di un cane che vuol essere una ballerina, la storia di un bambino di 6 anni affetto dalla sindrome di down, amico di una bimba epilettica, quella di una famiglia allargata con il nuovo compagno della mamma, la storia di una coppia divorziata con il bimbo che si trova a vivere con un genitore solo e quelle di famiglie di migranti e di bimbi adottati.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: «Ringrazio il Comune di Venezia - e in particolare Camilla Seibezzi, delegata del sindaco del Pd Giorgio Orsoni alle politiche contro le discriminazioni - per questa meritoria iniziativa che dovrebbe essere presa a modello da tutte le città italiane. Nel 2010 un progetto simile era già stato proposto a Torino e nel 2012 l'Associazione Radicale Certi Diritti ha lanciato una sottoscrizione pubblica per donare il libro Piccolo Uovo a tutte le biblioteche rionali di Milano, ma questa iniziativa ha il merito di valorizzare tutte le diversità e non solo quelle basate su orientamento sessuale e identità di genere adottando un approccio orizzontale e di mainstreaming sostenuto anche a livello europeo. Le critiche a questo progetto appaiono pretestuose e ideologiche perché non tengono in nessun conto che la realtà che, già oggi, bimbi e bimbe si trovano a vivere nelle scuole italiane è assai diversificata sotto il profilo culturale, etnico, religioso, nonché per quanto riguarda le famiglie di provenienza che sono le più varie e comprendono anche quelle omogenitoriali. Invito nuovamente tutti gli amministratori locali a prendere esempio da Venezia nel promuovere la pari dignità di tutte le famiglie e di tutte le persone».