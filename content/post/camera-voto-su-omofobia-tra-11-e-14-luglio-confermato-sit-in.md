---
title: 'Camera: voto su omofobia tra 11 e 14 luglio. Confermato sit-in'
date: Thu, 30 Jun 2011 21:40:13 +0000
draft: false
tags: [Politica]
---

**In contemporanea Certi Diritti organizza sit-in e maratona oratoria con le altre associazioni lgbt(e)**

**Comunicato Stampa Associazione Radicale Certi Diritti**

**Roma, 29 giugno 2011**

La Conferenza dei Capigruppo della Camera dei deputati riunita oggi,  ha deciso che il voto su Pdl anti-omofobia, la cui discussione si è svolta addirittura lo scorso maggio, si terrà nella settimna parlamentare 11-14 luglio. I giorni utili per il voto sono quelli di martedì 12 e mercoledì 13 luglio.

Confermiamo la nostra intenzione di tenere in Piazza Montecitorio un sit-in - Maratona oratoria, insieme a tutte le Associazioni, realtà, singoli, politici e personalità che hanno a cuore la lotta all'omofobia, nelle stesse ore in cui verranno anche presentate e discusse le eccezioni di costituzionalità e di sospensiva, presentate da Udc e Lega che potrebbero, se approvate dall'aula, 'cancellare' lo stesso provvedimento dal voto, come già accaduto oltre un anno e mezzo fa.

Per info e adesioni Sit-in / Maratona oratoria: