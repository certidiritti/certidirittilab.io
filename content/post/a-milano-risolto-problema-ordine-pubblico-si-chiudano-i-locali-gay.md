---
title: 'A MILANO RISOLTO PROBLEMA ORDINE PUBBLICO: SI CHIUDANO I LOCALI GAY'
date: Mon, 04 Aug 2008 12:14:56 +0000
draft: false
tags: [Comunicati stampa]
---

**CHIUSURA ANTICIPATA DEL LOCALE DI MILANO AFTER LINE: LA GRAVE DECISIONE DEL COMUNE DI MILANO SEMBRA ISPIRATA PIU' ALLA DISCRIMINAZIONE VERSO LE PERSONE LGBT CHE A MOTIVI DI ORDINE PUBBLICO. INTERROGAZIONE DEI DEPUTATI RADICALI**

**_Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti_**

**"Per risolvere il problema del degrado, dell'ordine pubblico e magari pure quello tanto fastidioso, per la classe politica, dell'omosessualità, il Comune di Milano ha intimato al locale Disco-Club After Line di Milano di chiudere alle ore 22 anziché alle ore 2. Ci pare francamente fuori luogo addurre ragioni di ordine pubblico o di degrado urbano se un locale, luogo storico e di riferimento per la comunità gay, viene fatto chiudere quattro ore prima rispetto al suo solito orario di apertura. Riteniamo che l'accaduto riguardi una delle tante forme di omofobia che da un po' di tempo a questa parte vanno tanto di moda anche tra le istituzioni. Ci auguriamo che l'interrogazione parlamentare dei deputati radicali chiarisca fino in fondo questa vicenda di per sé troppo poco chiara. Se così non fosse vorremmo sapere quanti sono i locali chiusi perché frequentati da persone con chiare tendenze eterosessuali?".**

**_Di seguito il testo dell'interrogazione parlamentare depositata oggi dai deputati radicali Elisabetta Zamaparutti, Rita Bernardini, Marco Beltrandi, Matteo Mecacci:_**

**Interrogazione a risposta scritta al Ministro degli Interni e al Ministro per le pari opportunità:**

**Per sapere – premesso che:**

**\- Il 14 diembre 2007 il Comune di Milano emette un'ordinanza per chiusura anticipata dei locali di Milano After Line e Next Grove alle ore 22.00. Il locale After Line di Via Sammartini, è stato per anni un punto di riferimento importante per la comunità gay italiana, sicuramente il primo e più conosciuto disco bar gay italiano, aperto normalmente fino alle due di notte.**

**\- La decisione della chiusura anticipata farebbe seguito ad una relazione della Questura di Milano dove, tra l'altro, si legge: "la zona della Stazione Centrale soffre di problemi per il comportamento tenuto da avventori di esercizi commerciali e pubblici che costituiscono luoghi di ritrovi di soggetti dalla chiara tendenza omosessuale, dediti al consumo di stupefacenti, all'abuso di sostanze alcoliche, e soliti a gesti di intemperanza, oscenità e disturbo ai residenti della zona..".**

**\- Sulla decisione del Comune di Milano, il titolare del locale After Line ha opposto un ricorso al Tar che ha annullato la decisione amministrativa.**

**\- Il Comune di Milano presenta appello al Consiglio di Stato che il 10 giugno 2008 lo accoglie. Nell'ordinanza del Consiglio di Stato si legge: "...Il Collegio ha ritenuto la restrizione d'orario in quanto 'adottata in conformità dell'ordinanza sindacale sugli orari, soggetta a regole severe in funzione di tutela dell'ordine e della sicurezza pubblica...giustificata in relazione alla natura di licenza di pubblica sicurezza dell'autorizzazione'. Secondo il Comune di Milano 'il provvedimento di riduzione dell'orario di esercizio del locale citato in oggetto è volta a porre rimedio a una oggettiva situazione di grave pericolo per l'ordine pubblico e la sicurezza dei cittadini, che allo stato permane. La finalità perseguita della disposizione non è dunque quella di sanzionare la condotta del gestore...bensì quella di impedire attraverso la sua anticipata chiusura, il protrarsi di una situazione di pericolosità sociale...".**

**Per sapere:**

**\- Se non ritenga il Ministro degli Interni che la relazione della Questura di Milano, probabile causa dell'azione amministrativa del Comune di Milano, contenga una esplicita forma di discriminazione verso le persone omosessuali;**

**\- se la chiusura anticipata del locale After Line sia stata decisa per colpire uno dei ritrovi della comunità gay indipendentemente dal degrado urbano della zona;**

**\- se il degrado o "il grave pericolo per l'ordine pubblico" di alcuni quartieri della città di Milano, o di altre grandi metropoli, non vada combattuto con altri strumenti: ad esempio maggior vigilanza, maggiori strutture e servizi piuttosto che la limitazione dell'apertura di locali presenti in tali zone;**

**\- quanti altri locali, frequentati da persone con "chiare tendenze eterosessuali", hanno avuto nella città di Milano limitazione di orario di apertura adducendo ragioni di 'ordine pubblico'.**