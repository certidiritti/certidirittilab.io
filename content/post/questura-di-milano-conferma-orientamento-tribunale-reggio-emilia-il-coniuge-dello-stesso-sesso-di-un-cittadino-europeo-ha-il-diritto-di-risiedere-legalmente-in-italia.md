---
title: 'Questura di Milano conferma l''orientamento del tribunale di Reggio Emilia: il coniuge dello stesso sesso di un cittadino europeo ha il diritto di risiedere legalmente in Italia'
date: Fri, 31 Aug 2012 07:49:38 +0000
draft: false
tags: [Diritto di Famiglia]
---

Comunicato stampa dell'associazione radicale Certi Diritti

Roma, 31 agosto 2012

Il 30 agosto 2012 la Questura di Milano ha rilasciato, nello stesso giorno in cui è stato richiesto, il permesso di soggiorno al coniuge serbo dello stesso sesso di un cittadino italo-canadese in base alle norme sulla libera circolazione dei cittadini europei e i loro famigliari.  
   
Adrian e Djiordje si sono sposati in Canada nel 2009 ma da qualche tempo risiedono in Italia. Seguiti dall’Associazione Radicale Certi Diritti hanno presentato la richiesta di permesso di soggiorno alla Questura di Milano, che già lo aveva concesso nel maggio scorso ad una coppia seguita da Rete Lenford, ottenendone l’immediata consegna.

L’orientamento della Questura è così stato confermato: il coniuge dello stesso sesso, a prescindere dal non riconoscimento del matrimonio contratto all’estero, ha comunque il diritto di risiedere regolarmente in Italia perché considerato famigliare di cittadino comunitario in base alle norme sulla libera circolazione in Europa, recepite in Italia con il d.lgs. n. 30/2007, e ai pronunciamenti della Corte europea dei diritti dell'uomo sull’argomento. Ricordiamo, infatti, che le linee guida emanate dalla Commissione europea per una migliore trasposizione della direttiva sulla libera circolazione, n. 2004/38 (COM 2009 - 313), sottolineano che “ai fini dell’applicazione della direttiva devono essere riconosciuti, in linea di principio, tutti i matrimoni contratti validamente in qualsiasi parte del mondo”, mentre vengono espressamente menzionate le sole eccezioni dei matrimoni forzati e dei matrimoni poligami.

Inoltre l’art. 9 della Carta europea dei diritti fondamentali, in vigore dal 1 dicembre 2009 in quanto recepita dal Trattato europeo di Lisbona, individua in capo a ogni persona “il diritto di sposarsi e di costituire una famiglia”, senza più richiedere il requisito della la diversità di sesso, come invece faceva l’art. 12 della Convenzione europea dei Diritti dell'Uomo.  

Yuri Guaiana, segretario dell’Associazione Radicale Certi diritti dichiara: “La decisione della Questura di Milano rinforza la sentenza del Tribunale di Reggio Emilia che nel febbraio scorso, per la prima volta in Italia, aveva decretato, in accoglimento del ricorso dell’Associazione Radicale Certi Diritti, il rilascio del permesso di soggiorno per un ragazzo uruguayano regolarmente sposato in Spagna con un italiano. La Questura di Rimini, che dal 17 maggio 2012 sta bloccando il rilascio del permesso di soggiorno ad un cittadino cubano sposato in Spagna da un italiano, dovrebbe prendere esempio. Sarebbe anche ora che questa contraddizione per la quale si rilasciano permessi di soggiorno per motivi famigliari a coniugi dello stesso sesso, senza che il loro matrimonio contratto all’estero venga riconosciuto in Italia si risolva con il ritiro della Circolare Amato del 2007 n. 55 che vieta ai Comuni la trascrizione dei matrimoni tra persone dello stesso sesso contratti all’estero anche da cittadini italiani per ragioni di ‘ordine pubblico’, materia peraltro non applicabile al diritto di famiglia come ribadito ripetutamente dalla Corte di Cassazione. E’ tempo che il governo permetta ai coniugi dello stesso sesso di vedere pienamente riconosciuto il proprio stato civile in Italia”.  
  

**[Sostieni la nostra associazione >>>](partecipa/iscriviti)**

**[Tutti nudi per la campagna iscrizioni 2012 >>>](http://www.certidiritti.org/2012/06/26/dai-corpo-ai-tuoi-diritti-parlamentari-artisti-ex-porno-divi-disabili-studenti-rifugiati-e-attivisti-politici-nudi-contro-la-sessuofobia/)**

**[Iscriviti alla newsletter >>>](partecipa/newsletter)**

**[La nostra rassegna stampa settimanale su Radio Radicale >>>](http://www.radioradicale.it/skas?evento=1004&fuzzy=0)**

  
[Carta\_Soggiorno\_Djordje.jpg](http://www.certidiritti.org/wp-content/uploads/2012/08/Carta_Soggiorno_Djordje.jpg)