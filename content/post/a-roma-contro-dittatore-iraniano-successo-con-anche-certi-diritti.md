---
title: 'A ROMA CONTRO DITTATORE IRANIANO: SUCCESSO CON ANCHE CERTI DIRITTI'
date: Wed, 04 Jun 2008 14:26:02 +0000
draft: false
tags: [Comunicati stampa]
---

L’Associazione Radicale Certi Diritti ha partecipato, insieme ad altre 25 organizzazioni, alle manifestazioni organizzate ieri. martedì 3 giugno, a Roma in Piazza di Spagna e in Piazza del Campidoglio, contro la visita in Italia, alla Fao, del Presidente iraniano Ahmadinejad La manifestazione di Piazza del Campidoglio, che ha visto al partecipazione di alcune migliaia di persone e di leader politici e di Governo di tutti gli schieramenti politici, è stata presentata da Sergio Rovasio, Segretario di Certi Diritti. Oltre a ricordare le vittime dell’omofobia dello Stato iraniano e la violazione dei più elementari diritti umani e civili delle donne e degli oppositori del regime, il Segretario di Certi Diritti è intervenuto per ricordare la figura di Makwan Moloudzadeh, il ragazzo di 18 anni impiccato in Iran dalle autorità, nel dicembre 2007, perché gay. L’Associaizone Certi Diritti, dalla sua fondazione, dedica le sue campagne alla memoria di Makwan.