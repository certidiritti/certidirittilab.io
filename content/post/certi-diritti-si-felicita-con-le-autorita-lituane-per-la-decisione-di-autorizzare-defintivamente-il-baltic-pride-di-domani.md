---
title: 'CERTI DIRITTI SI FELICITA CON LE AUTORITA'' LITUANE PER LA DECISIONE DI AUTORIZZARE DEFINTIVAMENTE IL BALTIC PRIDE DI DOMANI.'
date: Fri, 07 May 2010 12:45:24 +0000
draft: false
tags: [baltic pride, certi diritti, Comunicati stampa, gay pride, Pride, vilnius]
---

Bruxelles, 7 maggio 2010

  
La corte di appello lituana di vilnius ha deciso questo pomeriggio di autorizzare in via definitiva il baltic pride previsto per domani, riconfermando in tal modo l'autorizzazione sospesa ieri su ricorso del procuratore generale e di un consigliere locale.

Certi diritti si felicita della decisione della corte di appello, che in tal modo assicura la protezione del diritto fondamentale d'espressione, assemblea e manifestazione, senza discriminazioni. Certi diritti sfilerà domani nel corso del baltic gay pride con cartelloni inneggianti al matrimonio gay in Lituania, in Italia e in Europa e per chiedere la revisione della legge omofobica sulla "protezione dei minori dalle informazioni nocive", quali la promozione del matrimonio gay.