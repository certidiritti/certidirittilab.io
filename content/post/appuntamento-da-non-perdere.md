---
title: 'APPUNTAMENTO DA NON PERDERE'
date: Wed, 30 Nov -0001 00:00:00 +0000
draft: false
tags: [Senza categoria]
---

SPETTACOLO DI E CON DARIO FO
============================

[Homepage](http://www.certidiritti.it/) \> [Comunicati stampa](comunicati-stampa.html) \> SPETTACOLO DI E CON DARIO FO

S. AMBROGIO E L'ORRENDO SFASCIO DI MILANO  
Le buone e moderne idee sulla città di un uomo del IV secolo  
  
MILANO - LUNEDÌ 25 GENNAIO 2010 - ore 20.30  
CAMERA DEL LAVORO - C.SO DI PORTA VITTORIA 43  
  
spettacolo di e con DARIO FO  
  
Saluto ai presenti di Onorio Rosati (Segretario Generale Camera del Lavoro-CGIL Milano).  
Brevi testimonianze di: Giuseppe Boatti (Politecnico di Milano), Basilio Rizzo  
(Consigliere Comunale - Lista UnitiConDarioFo), Cini Boeri (Comitato per Sant'Ambrogio),  
Rolando Mastrodonato (Comitato Vivi e progetta un'altra Milano), Daria Cattolica  
(Comitato Val Bavona).  
  
Si invitano a partecipare i Comitati, le Associazioni e tutti i cittadini impegnati a  
contrastare la dilagante speculazione edilizia e ad impedire lo scempio urbanistico, che  
potrebbe essere ulteriormente aggravato dalla approvazione del Piano di Governo del  
Territorio (PGT).  
  
L'intero ricavato dello spettacolo, grazie alla disponibilità di Dario Fo, andrà a  
sostegno del Comitato Val Bavona, occupato da tempo nella lotta per la tutela del  
territorio e dell'ambiente in zona Lorenteggio.  
Dopo lo spettacolo, Dario Fo firmerà in originale le stampe del disegno da Lui  
appositamente realizzato per la serata; le copie autografate saranno messe a disposizione  
del pubblico ad ulteriore sostegno dell'iniziativa.  
  
Ingresso con offerta libera fino a esaurimento posti. E' possibile sottoscrivere e  
ritirare il biglietto di entrata presso il Punto Rosso da lunedì 4 gennaio 2010 - orario  
continuato 9.30-18.30.  
  
Organizzano  
Camera del Lavoro Metropolitana-Cgil Milano, Comitato Val Bavona, Associazione Culturale  
Punto Rosso  
  
info:  
Associazione Culturale Punto Rosso  
Via G. Pepe 14 (angolo Via Carmagnola - MM2 Garibaldi)  
tel. 02-874324 e 02-875045  
info@puntorosso.it  
www.puntorosso.it