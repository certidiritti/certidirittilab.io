---
title: 'Le unioni tra persone dello stesso sesso'
date: Fri, 23 Apr 2010 18:59:27 +0000
draft: false
tags: [Senza categoria]
---

**Profili di diritto civile, comunitario e comparato**

a cura di **Francesco Bilotta**

_[Mimesis Edizioni](http://www.mimesisedizioni.it/)_, 2010

Le famiglie formate da persone dello stesso sesso pongono al diritto numerose questioni. Per la prima volta in maniera organica esse vengono affrontate sia dal punto di vista del diritto italiano, sia dal punto di vista di sistemi giuridici stranieri. Di particolare interesse sono le proposte di interpretazione delle norme interne alla luce dei principi del diritto comunitario. Obiettivo del volume è indagare la percorribilità di una strada giudiziaria per rivendicare i diritti delle coppie omosessuali.

Scritti di: Vittoria Barsotti, Chiara Bertone, Francesco Bilotta, Stefano Bolognini, Matteo Bonini Baraldi, Francesco Dal Canto, Giovanni Dall'Orto, Bruno de Filippis, Chiara Lalli, Morris Montalti, Lara Olivetti, Carmela Simona Pastore, Antonio Rotelli, Alexander Schuster.

Francesco Bilotta è ricercatore di diritto privato nell'Università di Udine e avvocato in Trieste. Autore di numerosi saggi in materia di diritti delle persone, diritti dei consumatori, responsabilità civile, questioni legate al mondo LGBT. Ha collaborato alla stesura della proposta di legge sul Patto civile di solidarità e unioni di fatto presentata nella XIV Legislatura. é socio fondatore dell'Associazione Avvocatura per i diritti LGB