---
title: 'Mozioni particolari e raccomandazioni 2011'
date: Wed, 14 Dec 2011 07:21:46 +0000
draft: false
tags: [Politica]
---

### Raccomandazione

Il V congresso dell’associazione radicale Certi Diritti invita la Segreteria a studiare la concretizzazione di una ricerca che dia realizzazione alle problematiche relative alle persone omosessuali, in senso generale, dalla terza età in poi, notoriamente sole e bisognose di sostegno per poi istituire una fondazione.

### Mozione particolare

In Italia per secoli l’omosessualità è stata oggetto di discriminazione e persecuzioni e, nel nostro Paese, ancora vi è chi parla, per essa, di “peccato” o “malattia”, che deve essere “curata” e può “guarire”.

La secolare grave forma di prevaricazione ed ignoranza ha subito un’accelerazione durante il periodo fascista, poiché il relativo regime ha posto la sua ideologia, le sue squadracce e la sua propaganda al servizio di esse e ha contribuito a sedimentare una sub cultura con un forte contenuto di violenza fisica e morale nei confronti dell’omosessualità.

Nell’attuale fase storica, nella quale la ripresa di atteggiamenti medievali da parte di movimenti religiosi ha suscitato anche in alcuni Paesi occidentali la tentazione di trovare rifugio nella difesa di valori ritenuti “tradizionali” e nella quale la spinta verso il progresso si è fortemente attenuata ed anzi sono prevalse tendenze opposte, tutte le proposte di riforma e modernizzazione nel diritto di famiglia hanno subito un arresto ed in questo quadro hanno trovato ostacoli insormontabili anche le proposte presentate nell’ambito della tematica omosessuale.

Attualmente, neppure le proposte di legge tese a contrastare fenomeni di inaccettabile violenza, con la previsione di una specifica aggravante per le aggressioni motivate di omofobia, hanno trovato accoglimenti da parte delle forze maggioritarie in Parlamento, nonostante il forte valore “morale” che esse avrebbero potuto avere, come indicazione dei principi di rispetto della persona che esse sottolineavano.

In questo quadro, tuttavia, rimangono punti di riferimento inequivocabili l’art. 3 della Costituzione, nella parte in cui vieta ogni discriminazione basata sul genere o sulle condizioni personali dei cittadini e ne impone l’uguaglianza sostanziale di fronte alla legge, e l’art. 2, nella parte in cui riconosce e garantisce i diritti inviolabili dell’uomo, sia come singolo sia nelle formazioni sociali ove si svolge la sua personalità.

Poste queste premesse, si osserva, in relazione al tema del matrimonio tra persone dello stesso sesso, che il codice civile vigente, pur prevedendo nel dettaglio una serie di requisiti ed impedimenti alle nozze, non afferma, in alcuna sua parte, che la diversità di sesso ne sia requisito. Tanto si verifica perché esse deriva dal codice canonico nel quale un’unione di questo tipo non veniva vietata poiché neppure ipotizzabile. Neppure alcun divieto di carattere formale si desume dalla Costituzione, poiché la stessa parla di matrimonio ma non lo definisce, consentendone la determinazione alla legislazione dei tempi.

In assenza di ostacoli di legge, la questione può essere rimessa pienamente alla coscienza sociale e morale della collettività.

I sottoscrittori del presente documento sono convinti che ogni istituto di diritto civile che riguardi la realizzazione di fondamentali esigenze dell’individuo debba essere accessibile in egual modo da parte di tutti.  
Sono altresì convinti che essenza della democrazia siano il rispetto e il pluralismo e, quindi, la rinuncia ad imporre sé stessi o il proprio gruppo territoriale culturale o ideologico come modello unico o “superiore”.

Pertanto essi chiedono che l’istituto del matrimonio in primo luogo e forme equivalenti come subordinata, siano resi accessibili alle persone dello stesso sesso, per tutela re la dignità del loro essere e delle loro scelte ad assicurare il rispetto di diritti fondamentali della persona.

Rivolgono tale richiesta anche per attenuare lo “spread” che, nell’0ambito del diritto di famiglia continua ad allargarsi rispetto alle legislazioni degli altri Paesi europei.