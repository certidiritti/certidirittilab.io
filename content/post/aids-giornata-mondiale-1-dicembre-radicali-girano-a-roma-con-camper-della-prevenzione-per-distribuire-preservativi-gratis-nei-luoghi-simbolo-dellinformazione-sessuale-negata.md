---
title: 'AIDS - Giornata mondiale:  1 dicembre Radicali girano a Roma con ''camper della prevenzione'' per distribuire preservativi gratis nei luoghi simbolo dell''informazione sessuale negata'
date: Thu, 29 Nov 2012 08:02:26 +0000
draft: false
tags: [Salute sessuale]
---

Il camper girerà tra scuole, seminari religiosi, strade della prostituzione maschile e femminile, locali gay.  
Malattie sessualmente trasmissibili in aumento: le istituzioni facciano di più per la prevenzione con specifiche campagne di informazione.

Comunicato Stampa dell’Associazione Radicale Certi Diritti e di Radicali Roma.

Giudichiamo positivamente progetti come la campagna di informazione del Comune di Roma "Hiv? Sintonizzati" rivolta agli studenti romani o il nuovo sistema di assistenza a domicilio per i cittadini malati per quanto in via sperimentale, ma crediamo che si possa e si debba fare di più sul fronte della prevenzione, anche perché la Città di Roma insieme a Milano ha il più alto tasso di malattie sessualmente trasmissibili, compreso l’Hiv.  
Il contagio da Hiv attraverso il rapporto sessuale rimane il principale veicolo di infezione e mentre da anni la malattia è uscita dalle "categorie a rischio" è riguarda tutte gli strati sociali e tutte le età, resta drammaticamente basso l'abitudine all'uso del preservativo nei rapporti sessuali.

Le istituzioni devono quindi con tutti i mezzi diffondere non solo l'informazione ma direttamente lo strumento sanitario migliore per la tutela della propria salute contro l'Aids.  
Per questo invitiamo l'amministrazione comunale a programmare la distribuzione di preservativi gratuita o la vendita a prezzi fortemente scontati nelle farmacie comunali e nelle strade, così come avviene in molte città europee. Iniziative del genere, già promosse in passato e ripetute oggi anche in diverse città italiane,  sono peraltro facilmente realizzabili attraverso accordi con le società produttrici.

Per tutto questo sabato 1° dicembre, in occasione della Giornata mondiale contro l'Aids, gireremo con il “Camper della prevenzione” nella città di Roma, nei luoghi simbolo dell’informazione sessuale negata. In mattinata distribuiremo preservativi agli studenti davanti ad alcuni licei di Roma, all’ora di pranzo saremo davanti al Pontificio collegio americano del nord (quello del Gianicolo), nel pomeriggio nei luoghi della prostituzione femminile e transessuale (via Salaria, via Palmiro Togliatti, via della Pisana), in serata alla Gay street, e in tarda serata nei luoghi della prostituzione maschile della Stazione Termini e di Valle Giulia.