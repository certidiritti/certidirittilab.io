---
title: 'Unioni civili. Con decreti finisce limbo, ma troppi diritti ancora alla finestra'
date: Sat, 14 Jan 2017 14:42:26 +0000
draft: false
tags: [Diritto di Famiglia]
---

![15894269_1207128599340341_3096930807806370367_n](http://www.certidiritti.org/wp-content/uploads/2017/01/15894269_1207128599340341_3096930807806370367_n-225x300.jpg)"Il Consiglio dei Ministri, con l'approvazione dei tre decreti, ha dato piena effettività alla legge sulle unioni civili e ha portato definitivamente a compimento il primo piccolo passo verso la riforma del Diritto di famiglia per cui come Radicali non finiremo di batterci. Sorveglieremo con attenzione tutti i casi di applicazione della legge, non mancando di far presente eventuali criticità per future manovre correttive. Si tratta come detto di 'un primo piccolo passo', perché sono troppe le aree del Diritto di famiglia che aspettano di essere rimaneggiate dal legislatore: prime tra tutte l'apertura di matrimonio e unioni civili rispettivamente ad omosessuali ed eterosessuali nonché la riforma delle adozioni e della legge 40 per squarciare il velo d'ipocrisia sui genitori singoli ed omosessuali (rimasti fuori dalla legge 76/2016). Auguri alle tante coppie che continueranno ad unirsi civilmente in giro per l'Italia, auguri a tutti per le battaglie che ci aspettano da subito"

Così Riccardo Magi e Leonardo Monaco, rispettivamente segretari di Radicali Italiani e dell'Associazione Radicale Certi Diritti.