---
title: 'Costruire e difendere l''Europa dei diritti'
date: Sat, 04 Jun 2011 07:21:57 +0000
draft: false
tags: [Europa]
---

**Certi Diritti, con il patrocinio di Ilga Europe Torino 2011 e Roma Europride 2011, organizza il convegno 'Costruire e difendere l'Europa dei diritti. I diritti non sono un optional ma una parte fondamentale della strategia di sviluppo europea. E l’Italia?'. Venerdì 10 giugno ore 15 presso la Sede del Parlemento europeo a Roma, via IV novembre 149. Con, tra gli altri, Emma Bonino e Michael Cashman.  
**

Programma:  
  
**Saluto introduttivo**  
SERGIO ROVASIO Segretario Associazione radicale Certi Diritti  
Un rappresentante del Comitato organizzatore Europride 2011

**Sessione introduttiva**  
PIER VIRGINIO DASTOLI  
Quale spazio possono avere le politiche antidiscriminatorie nella Strategia 2020 dell’Unione Europea?  
MARILISA D’AMICO  
È possibile costruire una strategia giudiziaria per il raggiungimento dei diritti laddove sono negati?  
OTTAVIO MARZOCCHI  
Commissione e Parlamento europeo difronte ai temi lgbt

**Tavola rotonda**  
SILVAN AGIUS - ILGA-Europe  
LUCIANO SCAGLIOTTI - ENAR(European Network against racism)  
PIETRO BARBIERI - FISH (membro del Disability European Forum)  
MARIA LUDOVICA TRANQUILLI-LEALI - European Women’s Lobby

**Conclusioni**  
EMMA BONINO Vice Presidente del Senato  
MICHAEL CASHMAN Presidente dell’Intergruppo lgbt del PE  
  
**Co-chair**  
VALERIA MANIERI Associazione Pari o Dispare  
ENZO CUCCO Associazione radicale Certi Diritti

Organizzazione: Certi Diritti  
Con il patrocinio di: Ilga Europe Torino 2011 e Roma Europride 2011