---
title: 'Lettera aperta della Lega Italiana per la Lotta contro l''Aids a Beppe Grillo'
date: Fri, 18 May 2012 08:12:30 +0000
draft: false
tags: [Salute sessuale]
---

In un suo spettacolo risalente al 1998 Beppe Grillo ha affrontato il tema dell'Aids, definendolo "la più grande bufala di questo secolo", negando il legame tra Hiv e Aids.

Egregio Beppe Grillo,  
le scriviamo perché crediamo che sia venuto il momento di chiedere una sua chiara, aggiornata e seria presa di posizione sul tema Hiv/Aids, dato che il suo nome viene molto sbandierato, grazie a un suo spettacolo di qualche anno fa, in innumerevoli mail che riceviamo e post che affollano i siti e i social network nostri e altrui, compreso il suo blog.

Siamo perfettamente consapevoli che lo spettacolo teatrale di un comico, che si avvale di un particolare linguaggio, chiamiamolo iperbolico, non equivale a un programma politico. Altrettanto però non si può dire di parecchi suoi attuali estimatori, che quello spettacolo lo stanno usando contro di noi e contro il buonsenso. Quella, del resto, resta l'unica sua forte, esplicita, pubblica presa di posizione sull'argomento Hiv/Aids: in appoggio alle ipotesi negazioniste. Che oggi vengono riproposte, e con rinnovato vigore, anche facendosi forza della sua sottoscrizione.

Ma veniamo ai fatti. In un suo spettacolo risalente al 1998 lei ha affrontato il tema dell'Aids, definendolo senza mezzi termini “la più grande bufala di questo secolo”, negando il legame tra Hiv e Aids, ovvero negando che sia un virus trasmissibile che danneggia il sistema immunitario favorendo l'insorgere di patologie opportunistiche che possono portare alla morte. L'estratto è reperibile in versioni diverse, per esempio su YouTube. Lo spettacolo era “Apocalisse morbida”, trattava di temi medico scientifici e puntava il dito contro le case farmaceutiche e in generale il business della salute globale.

Erano anni in cui la discussione sul virus era accesa e non avanzata come oggi, anche se erano disponibili già dal 1996 le terapie che ancora oggi permettono la sopravvivenza alle persone che vivono con l'Hiv (gli inibitori della proteasi, non l'AZT). Erano gli anni in cui in Sudafrica il presidente Mbeki abbracciava le teorie dette, appunto, negazioniste, e invece di preoccuparsi di fornire ai propri cittadini le giuste terapie, diceva loro di curarsi col succo di limone, condannandoli a morte certa. Rinnegando Mandela, che l'anno prima, nel 1997, aveva varato la legge “Medicines and Related Substances Control Amendment Act”, per rendere disponibili i farmaci salvavita più economici. Erano anni in cui poteva succedere di farsi prendere dal furore civile e accusare le case farmaceutiche (che sono comunque un interlocutore privilegiato, per quanto non unico, nella lotta contro l'Aids, e non sempre limpido, lo sappiamo anche noi che i loro farmaci li prendiamo tutti i giorni) di speculare sulla sofferenza della gente, se non peggio, ovvero di averla addirittura creata per motivi di profitto.

Perciò, in quegli anni, a Durban (non a caso in Sudafrica) si riunì il mondo della ricerca e dell'attivismo: per ristabilire priorità e verità dei fatti, a tutela dei pazienti. Nel 2000 venne così redatta la Dichiarazione di Durban, sottoscritta da oltre 5mila scienziati, di cui 11 premi Nobel, che sanciva, e sancisce, che l'Hiv è la causa dell'Aids.

Da allora altri importanti passi avanti sono stati fatti, nella ricerca, nella prevenzione e nell'abbattimento dello stigma verso le persone sieropositive. Certo, molto ancora resta da fare, come sappiamo tutti l'Hiv/Aids non è sconfitto, anche se le terapie (dove sono disponibili) permettono la sopravvivenza e una buona qualità e aspettativa di vita. Ma dover combattere quotidianamente ancora oggi con i negazionisti, anche in buona fede per carità, che sembrano negli ultimi tempi essersi ricompattati con inedita aggressività, ci sta portando via energie e una buona dose di pazienza. Soprattutto, il negazionismo porta con sé il rischio che persone che vivono con l'Hiv, o che semplicemente vivono situazioni di rischio, o addirittura istituzioni, possano esserne affascinate o coinvolte, con tutte le conseguenze del caso in termini salute personale e pubblica.

Egregio Beppe Grillo, lei oggi è comunque un politico. Con un ampio e importante seguito. Ci rivolgiamo a lei perché chiarisca una volta per tutte qual è, oggi, il suo pensiero su Hiv e Aids.

Crediamo che ciò non sia dovuto solo a noi, alla LILA, ma alle tante persone, cittadini, che ancora oggi rischiano di ricevere informazioni fuorvianti e potenzialmente pericolose, e non i giusti strumenti, anche culturali, per poter prevenire le infezioni e curare e rispettare le persone che vivono col virus.

Crediamo sia dovuto anche ai rappresentanti del Movimento 5 Stelle, per esempio quelli che siedono nel consiglio regionale dell'Emilia Romagna (regione in cui ogni anno si registrano almeno 400 nuove infezioni), che da tempo stanno lottando anche in sede istituzionale perché resti alta l'attenzione e vengano attivati idonei e aggiornati strumenti preventivi. O quelli di Ravenna, che giusto in questi giorni hanno portato i temi della prevenzione dall'Hiv in commissione consiliare.

Egregio Beppe Grillo, dia una mano alle persone che vivono con l'Hiv e ai loro cari in questa lotta che ormai ha trent'anni e ancora non vede la fine. Alle persone, ai cittadini, che ancora con troppe difficoltà ricevono dalle istituzioni e dai media informazioni corrette e aggiornate, verificate da una comunità formata da medici, ricercatori, clinici, scienziati, ma anche da pazienti e attivisti. Dia una mano a tutti quelli che vivono nei paesi più poveri e che non possono essere curati anche perché l'Italia da anni non versa un euro al Fondo Globale di lotta a Aids, Tubercolosi e Malaria, neppure gli euro promessi.

Abbiamo bisogno di tutti, anche di lei. Abbiamo bisogno di tutto, tranne che di un suo video dove dice che l'Aids è la più grande bufala del secolo.

Grazie.

Alessandra Cerioli  
Presidente LILA Onlus Nazionale  
Como, 17 maggio 2012  
  
**www.lila.it**