---
title: 'SITO INTERNET DI CERTI DIRITTI ATTACCATO PER LA QUARTA VOLTA'
date: Sat, 12 Jun 2010 10:54:31 +0000
draft: false
tags: [Comunicati stampa]
---

Il sito internet dell'Associazione radicale Certi Diritti è stato per la quarta volta in due anni oggetto di aggressioni informatiche da parte di ignoti. Un altra volta appaiono delle scritte in turco collegate a siti per adulti.

Il pirata informatico ha agito inserendo un file contenente un codice che modifica i file interni e il relativo codice. Fortunatamente il nostro webmaster è riuscito a ripristinare il sito recuperando fino all'ultima notizia inserita, questo anche grazie al lavoro di squadra e all'appoggio di tutta l'associazione. Quando si dice che l'unione fa la forza, ecco un chiaro esempio.

Jean Baptiste Poquelin