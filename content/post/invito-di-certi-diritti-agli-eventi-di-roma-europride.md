---
title: 'Invito di Certi Diritti agli eventi di Roma Europride'
date: Fri, 03 Jun 2011 11:11:28 +0000
draft: false
tags: [Europa]
---

**Dall'1 al 12 giugno si svolgerà a Roma l’Europride 2011. Gli eventi programmati sono moltissimi, i due più rilevanti sono il Pride Park in Piazza Vittorio a Roma e la parata dell’11 giugno. Ecco tutti gli eventi ai quali abbiamo aderito o che abbiamo promosso.**

Dall'1 al 12 giugno si svolgerà a Roma l’Europride 2011, alla quale parteciperanno centinaia di migliaia di persone provenienti da tutta Europa. Tra l’altro, nei giorni immediatamente successivi, si voterà per i Referendum, da quello sul nucleare ai cinque milanesi, sui quali siamo direttamente impegnati e ai quali non potremmo far mancare il nostro voto.

  
Abbiamo aderito all’Europride con questa lettera inviata agli organizzatori:

_"I Radicali, da sempre impegnati per la promozione e la difesa dei diritti civili e umani in Italia e nel mondo, aderiscono e partecipano all'Europride 2011 che si svolgerà quest'anno a Roma.  Da sempre riteniamo che la conquista dei diritti civili è possibile se le regole democratiche sono garantite, cosa che purtroppo non avviene in Italia così come abbiamo ben documentato nel dossier 'La peste italiana'._

_Riteniamo che il sistema partitocratico, con la commistione dei privilegi e delle ingerenze vaticane impedisca la conquista di leggi di civiltà e moderne così come avviene in tutto il resto d'Europa.  Ci auguriamo che questo importante evento sia di aiuto agli obiettivi e alle speranze che ci vedono insieme impegnati sullo stesso fronte, in Italia, in Europa e in molti paesi del mondo"._

Oltre a partecipare ad alcuni degli eventi promossi da altre organizzazioni, promuoveremo tre iniziative alle quali ti invitiamo a partecipare:

1)   **Conferenza Stampa presso la sede del Partito Radicale**, giovedì 9 giugno, alle ore 15.30, sui temi che da sempre ci vedono impegnati per il superamento delle diseguaglianze e l’impegno sui diritti civili e umani (con Emma Bonino, Marco Pannella, Kasha Jacqueline Nabagesera, Direttrice Far Uganda, amica e compagna di lotte di David Kato Kisule, il nostro iscritto ucciso lo scorso gennaio; Paolo Patanè, Presidente di Arcigay;  Rita De Santis, Presidente di Agedo e Sergio Rovasio, Segretario Ass.ne Radicale Certi Diritti).  
2)   **Venerdì 10 giugno incontro-seminario presso la sede di Roma del Parlamento Europeo promosso dall’Associazione Radicale Certi Diritti** (h 15,30 – 18,30) su: “Costruire (e difendere) l’Europa dei Diritti. I Diritti non sono un optional ma una parte fondamentale della strategia di sviluppo europea. E l’Italia?”. Tra gli altri, parteciperanno Emma Bonino e Michael Cashman. Con il patrocinio di Europride Roma 2011 e Comitato organizzatore ILGA-Europe Torino 2011.  
 3)   **Parata Europride di sabato 11 giugno** che partirà il pomeriggio da Piazza Esedra – Roma. Ci sarà un grande carro Radicale organizzato dalle Associazioni Radicali aderenti all’Europride, insieme agli iscritti, parlamentari e militanti, con musica (trash, elettronica), interventi al microfono, distribuzione di materiale informativo anche sull’iniziativa nonviolenta in corso.

Un caro saluto,  
Mario Staderini  
Marco Cappato  
Maurizio Turco  
Sergio Rovasio  
Niccolò Figà Talamanca  
Sergio D’Elia

Informazioni logistiche:

Preannuncia la tua partecipazione all’email: [info@radicali.it](mailto:info@radicali.it)

L’Ufficio di Roma del Parlamento Europeo è in Via IV Novembre, 149. Per prenotarsi per il convegno del 10 giugno scrivere a [info@certidiritti.it](mailto:info@certidiritti.it)

Per la Parata EuroPride dell’11 giugno, da tutta Italia sono previsti viaggi organizzati molto economici, ti consigliamo di contattare le associazioni locali per avere tutte le info.

Certi Diritti Parma organizza un pullman con partenza dal parcheggio del casello autostradale di Parma alle 7.00 di sabato 11-6. Zone interessate Reggio Emilia,Modena o Bologna. Per info scrivere a:  [altersala@libero.it](mailto:altersala@libero.it)

Maggiori informazioni sulle iniziative le puoi trovare ai seguenti link:

www.radicali.it  
www.certidiritti.it  
www.europrideroma.it

**Qui di seguito alcuni degli eventi ai quali partecipa**  
**l’Associazione Radicale Certi Diritti**

**Giovedì 2 giugno**  
Ore 18 – 19,30: Conferenza pre-torneo Yellow LGBITE su Transessualismo e sport. Con Porpora Marcasciano – Mit e Sergio Rovasio, Segretario Associazione Certi Diritti. Presso la sede dell’Arcigay di Roma. Via di San Giovanni in Laterano n.10.

**Lunedì 6 giugno**  
Ore 18,30:  FUTURO GAY - Raccontiamoci la comunità LGBTIQ che verrà. A cura del Coordinamento Roma Pride in collaborazione con la Casa Internazionale delle Donne e la partecipazione del Roma Rainbow Choir. Presso la Casa Internazionale delle Donne, Via della Lungara, 19 – Roma.

**Martedì 7 giugno**  
Ore 21: QueerInAction, la cultura cinematografica a tematica LGBTQI nella capitale.  
Nell’ambito della rassegna sul tema "Religioni e Omofobia" dibattito su 4 aree tematiche : religiosa, psicologica, sociologica e legislativa. C/O Pride Park, Piazza Vittorio, Roma.

**Mercoledì 8 giugno**  
Ore 11: Ilga Europe incontro con con la Commissione Diritti Umani del Senato della Repubblica (da confermare).

Ore 18: HUMAN RIGHTS ARE MY PRIDE   La lunga marcia per i diritti LGBT in Europa. A cura della Sezione Italiana di Amnesty International e il Circolo Mario Mieli convegno.  Introduce e modera  Giusy D’Alconzo Direttrice Ufficio Campagne Amnesty International.

Interventi  di Rossana Praitano Presidente Mario Mieli; Vladimir Simonko Presidente Lithuanian Gay League (Lituania) su “La battaglia contro la propaganda omofoba in Lituania: leggi e limiti alla libertà di espressione”. Şevval Kilic  Attivista Istanbul LGBTT Solidarity Association (Turchia) su: “Le violazioni dei diritti umani delle persone transgender in Turchia: transfobia, discriminazioni e violenza della polizia”; Sergio Rovasio  Segretario Associazione Radicale Certi Diritti su: “La direttiva UE antidiscriminazione: ostacoli e prospettive” –  “Affermazione Civile 2: I ricorsi alla Corte Europea dei Diritti dell’Uomo”; Alberto Emiletti, Coordinamento LGBT della Sezione italiana di Amnesty International, Franko Dota, Attivista e coordinatore dello Zagreb Pride (Croazia)  Diritti Lgbt dopo dieci anni di Pride in Croazia.

Presso Pride Park – Piazza Vittorio, Roma

**Giovedì 9 giugno 2011**  
Ore 21: Dinner 4° International Business Leader Forum inizia in meno di due settimana, con il Welcome Dinner giovedì 9 giugno alle 19.30 e la Conferenza venerdì 10 giugno alle 9.00.

**Venerdì 10 giugno**  
Ore 9: 4° International Business Leader Forum a cura dell’Egma – European Gay and Lesbian Managers Association. Es Hotel Radisson – Via Filippo Turati, 171 – Roma. 

Ore 9,30 – 17: "Europa 2020: crescita inclusiva e diritto all'uguaglianza. Orientamento sessuale e identità di genere: sindacato e contrattazione dei diritti per la parità". A cura di Cgil in collaborazione con Etuc (Confederazione Europea dei Sindacati) e Ilga. Presso Cgil Nazionale C/o Sala Sinti. Corso d’Italia, 25 Roma.

Ore 15,30 – 18,30 Costruire (e difendere) l’Europa dei Diritti. I Diritti non sono un optional ma una parte fondamentale della strategia di sviluppo europea. E l’Italia? Incontro promosso da Associazione radicale Certi Diritti presso la sede di Roma del Parlamento Europeo.          Con il patrocinio di: Comitato organizzatore Europride Roma 2011 e Comitato organizzatore ILGA-Europe Torino 2011.

Saluto introduttivo: Sergio Rovasio, Segretario Associazione radicale Certi Diritti            Sessione introduttiva: Pier Virginio Dastoli: “Quale spazio possono avere le politiche antidiscriminatorie nella Strategia 2020 dell’Unione Europea?”; Marilisa D’Amico: “E’ possibile costruire una  strategia giudiziaria per il raggiungimento dei diritti laddove sono negati?”; Ottavio Marzocchi: “Quanto è libera la circolazione delle coppie e delle famiglie in Europa?”. A seguire Tavola rotonda: Silvan Agius, ILGA-Europe; Luciano Scagliotti, ENAR (European Network against racism); Pietro Barbieri, FISH (membro del Disability European Forum); Una rappresentante di European Women’s Lobby. Conclusioni: Emma Bonino, Vice Presidente del Senato; Michael Cashman: Presidente dell’Intergruppo lgbt del Parlamento europeo. Co-chair: Valeria Manieri (Associazione Pari o Dispare); En zo Cucco (Associazione radicale Certi Diritti).

Presso la sede del Parlamento Europeo, Via IV Novembre 119 – Roma.  
Ore 20,30 Riunione del Direttivo dell’Associazione Radicale Certi Diritti. Via di Torre Argentina, 76 – Roma.

**Sabato 11 giugno**  
ore 15 Parata dell’Europride Roma 2011. Carro delle Associazioni Certi Diritti, Luca Coscioni, Radicali Italiani, Nonviolent Radical Party, Nessuno Tocchi Caino, Non c’è Pace Senza Giustizia.