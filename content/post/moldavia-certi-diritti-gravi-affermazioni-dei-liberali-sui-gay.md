---
title: 'Moldavia, Certi diritti: gravi affermazioni dei liberali sui gay'
date: Fri, 18 Mar 2011 17:39:23 +0000
draft: false
tags: [Comunicati stampa, devianza, eldr, GAY, gay pride, liberali, moldavia, natura, RADICALI, sesso]
---

Il partito liberale ha affermato che non voterà la legge anti discriminazioni perchè "l'omosessualità é una deviazione e la natura é la natura". Radicali Italiani chiede all'Eldr di intervenire.  
  
Roma - Bruxelles, 18 marzo 2011  
  
Comunicato Stampa dell'Associazione Radicale Certi Diritti Il Parlamento moldavo sta esaminando il progetto di legge governativo contro le discriminazioni, che copre anche l'orientamento sessuale, sulla base degli accordi UE-Moldavia. Vari partiti, incluso quello liberale, hanno affermato che non voteranno a favore del provvedimento.  
Il leader del partito liberale moldavo Mihai Ghimpu ha affermato **_"siamo liberali, ma siamo per la sanità e vogliamo anche una famiglia sia sana. L'omosessualità é una deviazione, la natura é la natura, ma non significa che dobbiamo metterli in avanti. Con tutto il rispetto per loro, non appoggeremo la proposta di legge"._**  
  
L'Associazione Radicale Certi Diritti chiede al Parlamento moldavo di approvare la proposta di legge anti-discriminazioni includendo l'orientamento sessuale, mentre Radicali italiani ha chiesto all' ELDR (il partito liberale Europeo) di promuovere iniziative nei confronti dei liberali moldavi e di prendere i necessari provvedimenti, tenendo conto che in passato il Sindaco liberale di Chisnau ha proibito il Gay Pride: tali posizioni infatti nulla hanno a che fare con le politiche liberali di difesa dei diritti umani e delle libertà fondamentali.