---
title: 'Mister Gay Europa dedica vittoria a David Kato Kisule'
date: Mon, 18 Apr 2011 12:08:26 +0000
draft: false
tags: [Europa]
---

{jcomments on}{jcomments off}Sabato 16 aprile è stato eletto in Romania Mister Gay Europa l’italiano Giulio Spatola, che ha dedicato la sua vittoria a David Kato Kisule, attivista gay ugandese e iscritto a Certi Diritti ucciso lo scorso 26 gennaio.  
Giulio Spatola, Mister Gay Italia 2010, ha vinto il concorsodi Mister Gay Europe 2011. Spatola, 26 anni, di Palermo ma residente a Roma, èstato eletto durante la finale dell’evento che si è tenuta a Brasov, Romania,sabato notte. “Voglio dedicare la vittoriaa David Kato”, ha detto Giulio al pubblico dopo la premiazione,ricordando così l’attivista gay ugandese massacrato a martellate a gennaio eiscritto all’associazione italiana Certi Diritti.  
  
“Partecipo per portare all’attenzione pubblica la situaione deidiritti lgbt in Italia”, aveva detto il giovane palermitano a Gay.it primadi partire per la Romania. “L’Italiaè in Europa col portafogli e sulla mappa, ma non con la politica e di sicuronon con la mentalità.”  
  
Regista e hotel manager, Spatola ha fra i suoi hobby il cinema, lamusica, la discoteca e la palestra. Nell’agosto scorso è stato incoronato alconcorso organizzato dal portale Gay.it “Mister Gay Italia”. Durante la gara,Giulio aveva raccontato di come igenitori lo avessero portato da diversi psicologi credendo che la suaomosessualità fosse una malattia da cui guarire.  
  
“Avere un rappresentante italiano in giro per l’Europa – dice Alessio DeGiorgi, direttore di Gay.it – punterà un faro sulla situazione di gravemancanza di una legge sui diritti lgbt nel nostro paese. Giulio non rappresentasoltanto la bellezza, ma ha una storia cheè esemplare di un omofobia ancora troppo diffusa”.  
  
**[ARCIGAY, CERTI DIRITTI E NON C'è PACE SENZA GIUSTIZIA  
LANCIANO FONDO PER DAVID KATO KISULE >](campagne/in-memoria-di-david-kato/1071-fondo-in-memoria-di-david-kato-kisule.html)**  
  
fonte: tuttouomini.it