---
title: 'L''AMORE SPIAZZA'
date: Tue, 02 Feb 2010 05:59:49 +0000
draft: false
tags: [Senza categoria]
---

**San Valentino a Magenta**

Un pullman arcobaleno sarà a **Magenta il 14 febbraio 2010**, come prima tappa di una maratona contro l'omofobia che toccherà diverse realtà cittadine.  
Il coordinamento Arcobaleno riunisce le realtà trans*, lesbiche e gay di Milano e provincia e partirà in pullman per sbarcare nelle piazze e incontrare i cittadini e le cittadine della Lombardia.

![alt](http://3.bp.blogspot.com/_sIMqD_S-b7Y/S12EgA-T5JI/AAAAAAAAABM/jWuZR6SltQU/s400/A5magenta_fronte.jpg)

La storia di Magenta evoca la lotta di donne e uomini per la liberazione, così noi manifesteremo in questo luogo per combattere contro le discriminazioni e rivendicare i diritti civili ancora negati.

In un momento in cui in Italia assistiamo a fenomeni d'odio e di violenza verso coloro che sono portatori di diversità, noi visiteremo le città lombarde raccontando di persona le nostre storie.

Vogliamo parlare delle nostre vite e dei nostri amori per farci conoscere e per diminuire le distanze che creano un muro di pregiudizi.

Nel giorno in cui si celebra l'amore, va in piazza l'amore che rivendica diritti, per la dignità delle scelte di vita.

lamorespiazza@gmail.com

[http://www.facebook.com/group.php?gid=454604630342&ref=mf](http://www.facebook.com/group.php?gid=454604630342&ref=mf)

**Le associazioni promotrici :**

**Agedo** \- Cornelio Belloni (339/5651451)

**ArciLesbica Zami Milano** \- Stefania Cista (334/6823825)

**Arcobaleni in marcia** \- Roberto Caponio (339/75239059) Giorgio Piana (348/2932564)

**Certi Diritti** \- Gianmario Felicetti ( 329/9045945)

**C.i.g. Milano** \- Marco Mori (339/2354274)

**Famiglie Arcobaleno** \- Arianna Giliberti (334/1540531)

**Gay lib** \- Andrea Toselli (333/2126490)

**Gay Statale** \- Enrico Caruso (348/6289678)

**Guado - Cristiani Omosessuali** \- Glauco Bettera (340/3849025)

**Kob** \- Elisa Manfredi (349/3698372)

**Linea Lesbica** \- Lucia Giansiracusa (320/4186499)

**Milk** \- Stefano Aresi (346/5402606)

**Rose di Gertrude** \- Sergio Prato (347/2695262)

**Soggettività Lesbica**-Paola schiavetti (349/2901399)