---
title: 'SABATO 11-9 A CARRARA LA SAGRA DEL FINOCCHIO'
date: Thu, 09 Sep 2010 07:35:16 +0000
draft: false
tags: [Comunicati stampa]
---

**L’ASSOCIAZIONE RADICALE CERTI DIRITTI PARTECIPERA’ ALLA ‘SAGRA DEL FINOCCHIO’ DI CARRARA SABATO 11 SETTEMBRE. CHIESTA AL VESCOVO LA CANCELLAZIONE.**

**LA RICHIESTA AL VESCOVO DI INTERVENIRE CONTRO LA SAGRA E’ PATETICA, SI CHIEDA CONTO ALLA CHIESA DEGLI STERMINI DI OMOSESSUALI DI CUI SI E’ RESA RESPONSABILE NEL CORSO DEI SECOLI.**

Roma, 9 settembre 2010 

L’Associazione Radicale Certi Diritti parteciperà sabato 11 settembre alla ‘Sagra del Finocchio’, “1° Bar/Camp convivere con la diversità “ promossa dai radicali e dall’Arci di Carrara. La giornata prevede incontri e dibattiti su diverse tematiche relative alle malattie sessualmente trasmissibili, alla salute, i diritti civili, l’omofobia, la transfobia e le altre forme di violenza contro le persone lgbt.

Alle ore 16 Sergio Rovasio presenterà la campagna di Affermazione Civile lanciata due anni fa dall’Associazione Radicale Certi Diritti.

Alle ore 18 lo scrittore Pasquale Quaranta presentera' il suo libro dal titolo 'Omosessualita' e Vangelo'. Durante la giornata sara' possibile visitare la mostra fotografica 'Olocausto e triangoli rosa'.

Chiudera' la serata lo spettacolo del gruppo Drag Queen dal titolo 'Queen sister show'.

Al consigliere comunale Stefano Benedetti (La Destra per Gli Italiani), che si è appellato al Vescovo di Massa Carrara per chiedere che intervenga sul Sindaco per revocare l’autorizzazione alla manifestazione, diciamo che al Vescovo sarebbe stato più opportuno chiedere conto delle gravi persecuzioni di cui si è macchiata la chiesa nel corso dei secoli, contro le persone lesbiche ed omosessuali.

 La stessa parola finocchio deriva dal fatto che i semi di questa verdura venivano usati per coprire la puzza causata dal rogo di esseri umani. E’ forse ora che la chiesa chieda perdono di questi atti criminali commessi per almeno 6-7 secoli in tutta Europa.

**TESTO DELLA LETTERA DEL CONSIGLIERE COMUNALE STEFANO BENEDETTI AL VESCOVO:**

 Sua Eminenza, Le scrivo queste poche righe per sensibilizzarla ad una questione che a mio avviso ha una portata enorme rispetto a quella che dovrebbe essere la dignità umana ed il rispetto verso la religione, la comunità cristiana e i cittadini tutti.

Mi riferisco alla già famosa " festa del finocchio " ( con iniziali volutamente minuscole), il cui solo nome dovrebbe far vergognare gli organizzatori, pederasti, lesbiche, atei e tutto ciò che di più schifoso esiste al mondo, basta pensare che tanti di questi individui, sono gli stessi che scaldano le serate notturne dei pervertiti che si svolgono in quel di Torre del Lago.

Ma ognuno é libero di fare quello che vuole, l' importante che rispetti la libertà altrui e che abbia la sensibilità di non offendere la religione e soprattutto i Santi amati e pregati da coloro che la professano, ma non solo, da tutti i credenti e forse anche da tanti non credenti.

Con questa festa tutta la comunità Cristiana Apuana sarà offesa nel proprio spirito e Piazza S. Francesco profanata da orde di maniaci, spavaldi contro natura che dovrebbero, invece, festeggiare da un' altra parte, lontano dalla città e dallo sguardo innocente dei bambini.

Per questo motivo, la invito a nome di tanti cittadini che la pensano come me, a farsi garante della nostra richiesta nei confronti del Sindaco affinché revochi l' autorizzazione agli organizzatori della festa in quella piazza, dedicata ad un Santo amato e venerato da tutti proprio per la Sua semplicità umana e religiosa.

Non é pensabile che la festa sia stata dislocata in Piazza S. Francesco, solamente per una questione logistica e che il Sindaco non abbia pensato all' impatto che avrebbe la presenza di pederasti e lesbiche senza pudore in una Piazza che, anche se non consacrata, rappresenta l' essenza della cristianità ed é dedicata ad una Santo che per propria indole non ha nulla a che fare con gli spogliarelli delle drag queen o con i temi della comunità gay, che se si sentono discriminati dalla società, significa che sono vittime di se stessi per il proprio modo di comportarsi : gente che si isola e che si ritrova nelle zone gay solo per fare sesso, casino e niente altro.

Mi auguro che questo appello venga accolto e che il Sindaco Zubbani comprenda la gravità della decisione di spostare la festa in Piazza S. Francesco.

Credo comunque che abbiamo il diritto di manifestare di svolgere la festa, ma sicuramente senza ostacolare la società e offendere la religione.

Massa, 3 Settembre 2010

Il Cittadino Stefano Benedetti

( Consigliere Comunale )