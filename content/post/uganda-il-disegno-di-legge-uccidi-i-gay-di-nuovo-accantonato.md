---
title: 'Uganda: Il disegno di legge "uccidi i gay" di nuovo accantonato'
date: Sun, 15 May 2011 20:39:37 +0000
draft: false
tags: [Africa]
---

di Reuters Africa del 13 Maggio 2011. Traduzione di Alba Montori.

Il progetto di legge ugandese che prevedeva la pena di morte per i gay  "recidivi", sembra essere stato accantonato ancora una volta venerdì scorso, quando non è stato discusso in parlamento dopo aver provocato una protesta internazionale.

L'oratore dl Parlamento Edward Ssekandi lo ha sciolto venerdì affermando che non c'era abbastanza tempo per discutere il disegno di legge. Il nuovo parlamento sarà insediato mercoledì prossimo.

"Penso che ci aspettavamo che sarebbe accaduto", ha detto a Reuters un parlamentare che non ha voluto essere nominato . "La pressione era stata troppo forte negli ultimi due anni. Doveva sparire."

Il progetto di legge è previsto per un'audizione il Mercoledì, ma  sulla carta per quel giorno non sembra esserci.

Il Movimento anti-gay in Uganda ha acquistato grande notorietà, quando la normativa è stata introdotta originariamente nel 2009.

Il presidente Usa Barack Obama ha denunciato la situazione come "odiosa" e il segretario di Stato Hillary Clinton ha incontrato il presidente ugandese Yoweri Museveni per esprimergli le sue forti preoccupazioni.

Fatto oggetto di tanta pressione a livello internazionale il progetto di legge è stato silenziosamente accantonato , ma gli attivisti temevano che sarebbe passato dopo la vittoria elettorale di Museveni febbraio. Ma  ora sembra essere stato messo fuori servizio dopo che gli attivisti stranieri  sono aumentati enormemente questa settimana, con una campagna intensiva, poichè temevano che potesse essere inserito e approvato all'ultimo momento.

"Questo è stato un disegno di legge pericoloso e c'è un sacco di tensione e di disordini nel paese," ha dichiarato il Vescovo ugandese  Christopher Senyonjo attivista per i diritti dei gay a sessione parlamentare conclusa.

"Temiamo fortemente che possano cogliere l'occasione per fare del male a qualcuno. Questo disegno di legge non deve mai vedere la luce del giorno, perché la mafia potrebbe usarlo per commettere terribili crimini contro le persone."

Molti ugandesi sono risentiti dell'attenzione internazionale, dichiarano che mentre la maggior parte delle persone non sono d'accordo con l'omosessualità, ma quelli che hanno appoggiato la proposta di legge sono stati una minoranza.

Il disegno di legge potrebbe essere reintrodotto nella prossima sessione parlamentare, anche se un certo numero di parlamentari ha dichiarato a Reuters che questo è improbabile in considerazione delle critiche piovute dai governi stranieri.

Anche se l'omosessualità è illegale in molti Stati africani, l'Uganda  recentemente si è guadagnata la reputazione di "capitale mondiale dell'omofobia  ", col disappunto di molti, nel paese.

David Kato, uno dei militanti gay più in vista  del paese, è stato picchiato a morte con un martello all'inizio di quest'anno dopo essere stato posto alla pubblica attenzione come un "bandito" in un giornale anti-gay , che ha incitato la gente a ucciderli perché erano gay.

Un uomo ha confessato l'omicidio dopo essere stato arrestato. La polizia ha detto che ha ucciso Kato dopo che l'attivista gli aveva fatto delle avances sessuali.

Gli attivisti per i diritti dei gay hanno dichiarato a Reuters che al momento temono che la polizia stia coprendo il vero movente di omofobia per proteggere gli aiuti occidentali su cui si basa questo paese dell'Africa orientale.

Testo originale:

http://af.reuters.com/article/topNews/idAFJOE74C0HP20110513