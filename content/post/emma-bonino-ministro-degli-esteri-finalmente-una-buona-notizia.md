---
title: 'Emma Bonino Ministro degli Esteri. Finalmente una buona notizia'
date: Sat, 27 Apr 2013 14:48:45 +0000
draft: false
tags: [Politica]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 27 aprile 2013

È stata resa nota la lista dei ministri del governo Letta. Un'ottima notizia è sicuramente Emma Bonino al Ministero degli Esteri, ma anche l'esistenza, finalmente, di un ministero per le Pari Opportunità, affidato a Josefa Idem, e di un ministero dell'Integrazione affidato a un'altra donna, Cecile Kyenge, prima donna nera a diventare ministra della Repubblica italiana.

L'Associazione Radicale Certi Diritti esprime la massima soddisfazione per l'affidamento della carica di ministro degli Esteri a Emma Bonino, la cui determinazione e impegno a favore dei diritti umani ben conosciamo. Soddisfazione anche per la scelta di dare dignità ministeriale alla tematica delle Pari Opportunità - affidata a Josefa Idem, che nel gennaio di quest'anno si era dichiarata favorevole al matrimonio egualitario - e a quella dell'Integrazione, affidandola a Cecile Kyenge, una donna nera, proprio nell'anno centenario della nascita di Rosa Louise Parks, l'attivista afroamericana che il 1° dicembre 1955 compì un gesto, semplice e rivoluzionario, come quello di sedersi in un posto riservato ai bianchi sugli autobus di linea di Montgomery (Alabama, USA), e subendo per questo un processo che produsse nel 1956 una sentenza importante della Corte suprema degli USA, con la quale si decretava l'incostituzionalità della segregazione razziale.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: "Facciamo i migliori auguri di buon lavoro a Emma Bonino - sicuri che, da oggi, il tema dei diritti umani, e quindi dei diritti delle persone LGBTI, nel mondo troverà nell'Italia una nuova e più forte alleata -, Josefa Idem - incoraggiandola a tenere fede alle sue dichiarazioni a favore del matrimonio egualitario e quindi di un pieno riconoscimento del diritto di uguaglianza a tutti i cittadini italiani - e a Cecile Kyenge - incoraggiandola a seguire le orme di Rosa Parks. Come ha ricordato il presidente degli Stati Uniti Barack Obama, la lotta per i diritti delle persone LGBTI s'inserisce nel solco delle storiche lotte per i diritti delle donne e dei neri. La scelta di queste donne in tre ministeri per noi chiave ci rende ottimisti. Offriamo, naturalmente la nostra disponibilità a un'immediata e intensa collaborazione con tutte e tre le neo-ministre per la difesa dei diritti umani in Italia e nel mondo".