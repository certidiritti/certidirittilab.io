---
title: 'DOMANDE E RISPOSTE'
date: Sat, 22 Jan 2011 14:22:17 +0000
draft: false
tags: [Affermazione Civile]
---

DOMANDE E RISPOSTE

PARLANDO DI AFFERMAZIONE CIVILE UN PO' SOPRA LE RIGHE... E ANCHE PER QUESTIONI MOLTO CONCRETE

### **QUANTO COSTA RICORRERE A LIVELLO EUROPEO?**

Come già detto, gli avvocati che collaborano insieme a Certi Diritti per le iniziative di Affermazione Civile offriranno gratuitamente la loro assistenza legale.

Restano dunque a carico della coppia le spese processuali (varie spese burocratiche richieste dal sistema giudiziario), oltre che il rimborso delle spese vive affrontate dagli avvocati (viaggi ecc…).

Eventuali sentenze negative, che comporteranno il pagamento di tutte le spese processuali, saranno divise tra la coppia e l'associazione Certi Diritti.

In particolare, i ricorsi presentati davanti alle Corti Europee comporteranno delle spese maggiori, sempre superiori ai mille euro.

E' importante che le coppie valutino con attenzione la decisione di voler affrontare queste spese.

Sono spese impegnative, ma speriamo che possano essere sostenibili per molte coppie, specie se commisurate all'obiettivo - importantissimo - di vedersi riconosciuto un diritto fondamentale.

Purtroppo, l'associazione radicale Certi Diritti non può invece sostenere in prima persona queste spese. Però, anche noi vogliamo dare un contributo concreto, e per tutti quei ricorsi che saranno accettati dalle Corti, come associazione ci impegneremo a sostenere metà delle spese necessarie per l'udienza, attraverso iniziative di raccolta fondi e sensibilizzazione.

### **AFFERMAZIONE CIVILE E’ UNA INIZIATIVA POLITICA?**

Affermazione Civile è una iniziativa giuridica che vede nel riconoscimento del diritto al matrimonio tra persone anche dello stesso sesso la massima realizzazione del principio di uguaglianza contro l'omofobia e la discriminazione basata sull'orientamento sessuale.

Affermazione Civile si incardina perfettamente nell'iniziativa "Amore Civile" un progetto legislativo e culturale dal respiro molto più ampio, per la riforma del diritto di famiglia a 360 gradi. All'interno di Amore Civile il matrimonio tra persone dello stesso sesso è solo una goccia nel mare, e in un certo senso si ridimensiona nel "mare magnum" delle ingiustizie che lo stato italiano infligge alle famiglie e alle persone italiane in nome di un familismo di stato tradizionalista.

Affermazione Civile nasce dalla fiducia nelle istituzioni e nella democrazia. In questa fiducia si incardina con naturalezza e senza imbarazzo alcuno anche l'iniziativa politica, sempre rispettando i giusti equilibri e senza forzare le situazioni o, peggio ancora, i toni.

Certi Diritti è una associazione legata ai Radicali Italiani, ma non per questo ha mai voluto politicizzare o fare propria Affermazione Civile, che noi speriamo possa realmente propagarsi a macchia d'olio tra tante realtà associative italiane, anche eterosessuali.

Non per questo Certi Diritti abdica alla sua innata specificità politica. Certi Diritti è una associazione che politica, che ha dimostrato alle coppie e alle istituzioni italiane la serietà del suo impegno. Nella giusta misura, Certi Diritti vuole fare iniziativa politica anche attraverso Affermazione Civile, non sulla vita delle coppie ma insieme alle coppie e alla loro vita insieme.

Le coppie che condividono il nostro "stile" pacifico, democratico e propositivo, troveranno una piena sintonia con la nostra campagna.

E allo stesso tempo ci auguriamo anche che molte altre coppie preferiranno altri modi di fare Affermazione Civile, stimolando il coinvolgimento di altre realtà e associazioni, così come anche si propone di fare il Comitato "Sì, lo voglio"

In ogni caso Affermazione Civile è e resterà sempre una iniziativa genuina, non artefatta, che vuole portare la vita reale delle persone e delle famiglie italiane, anche omosessuali, di fronte alle istituzioni.

Riassumendo: Affermazione Civile è una iniziativa giuridica, che tutti possono intraprendere e fare propria.

Affermazione Civile, così come realizzata da Certi Diritti vuole essere uno spunto anche per iniziative politiche.

Affermazione Civile non è e sarà mai una iniziativa politicizzata.

### **CON AFFERMAZIONE CIVILE SI CONQUISTERA' IL DIRITTO AL MATRIMONIO?**

Affermazione Civile ha come obiettivo il riconoscimento del diritto al matrimonio civile per le coppie dello stesso sesso.

Nessuno può avere la certezza che Affermazione Civile permetterà di raggiungere questo obiettivo.

Non c'erano garanzie quando i neri d'america si batterono contro la segregazione razziale.

Non c'erano garanzie per il suffragio universale riconosciuto alle donne.

Crediamo che l'unione faccia la forza, e unire iniziative giudiziarie ad un più ampio discorso politico e culturale è sicuramente una scelta vincente.

### IL MATRIMONIO GAY RISCHIA DI DANNEGGIARE IL MATRMIONIO TRADIZIONALE?

Prima di tutto, per essere precisi, non stiamo parlando di matrimonio gay, come se fosse qualcosa di diverso dal matrimonio etero.

E non esiste un matrimonio tradizionale da contrapporsi ad un matrimonio "innovativo"...

Se ci riferiamo al matrimonio come una istituzione dello Stato, il matrimonio è il matrimonio. Così come il voto è il voto. Non ci possono essere aggettivi "discriminanti" da aggiungere. Dove c'è il matrimonio, come istituzione, non può esserci stigma. Altrimenti si snatura il matrimonio stesso e si ferisce, profondamente, il tessuto sociale.

Il voto delle donne non è differente dal voto degli uomini giusto? E così, la celebrazione di un matrimonio civile tra due persone dello stesso sesso, non è differente dalla celebrazione di un matrimonio tra due persone di sesso opposto.

Il matrimonio è una istituzione che, per sua natura, è deve rimanere libera e liberale, in altre parole, inclusiva. Quanto sono odiosi i vincoli al matrimonio imposti dallo stato? Abbiamo già dimenticato le leggi razziali? Odiosi vincoli messi in nome della tradizione, della razza, di principi ideologici e statalistici. Tutte cose che venivano prima dei coniugi e delle loro scelte.

Quando si parla di matrimonio, mettere in secondo piano l'amore è un suicidio.

Liberare il matrimonio dal compromesso eterosessista e dal travisamento mashista non danneggia niente e nessuno. Anzi, è vero il contrario. Se il matrimonio venisse esplicitamente definito come eterosessuale dalle istituzioni, allora quella sì, sarebbe una grave minaccia per la libertà dei cittadini, e un danno incalcolabile per tutti i matrimoni.

### IL MATRIMONIO GAY IN ITALIA. NON E' UN'UTOPIA?

Era un'utopia il divorzio? E la libertà delle donne di scegliere se abortire?

Era un'utopia il suffragio universale?

Era un'utopia rimuovere l'odioso reato di adulterio – solo per le donne?

Era un'utopia ribellarsi ai matrimoni riparatori?

Sì. Erano tutte utopie.

Le utopie, quando si affrontano con coraggio determinazione e lucidità, si vincono, si conquistano, si raggiungono. Per il bene di chi verrà dopo di noi, magari.

E oggi l'Italia deve affrontare diverse utopie: quella del testamento biologico e del fine vita, quella della fecondazione assistita, e anche quella dei matrimoni tra coppie dello stesso sesso.

Certi Diritti c'è. Vuole esserci.

Il principio di uguaglianza di tutti i cittadini davanti alla legge... il principio di autodeterminazione, nella vita e nella morte... questi, siamo d'accordo, sono principi non negoziabili. Non nel senso che non ci deve essere dialogo con chi non è sulla nostra lunghezza d'onda. Ma nel senso che sono principi lasciati alla totale libertà degli individui. Ognuno deve essere libero di scegliere come meglio crede, senza nuocere a nessuno. Non ci sono contrapposizioni. Ognuno, nella libertà può scegliere se sposarsi con una persona di sesso ooposto, o dello stesso sesso. Come vedete, è talmente semplice, che proprio, semplicemente, non c'è nulla da negoziare.

Perciò, se credete veramente che il matrimonio tra omosessuali è un'utopia, e che ci si debba accontentare di meno, allora, per coerenza si dovrebbe essere d'accordo con chi si batte per eliminare il divorzio, per tornare agli aborti clandestini, e magari un giorno si presenterà qualcuno che vorrà togliere il diritto di voto delle donne.

Se vogliamo essere cittadini, e non sudditi, non possiamo vivere adagiati sulle utopie del passato, e non avere il coraggio di affrontare quelle presenti.

### DOPO LA SENTENZA L'UNICO MODO DI OTTENERE GIUSTIZIA E' QUELLO DI RIVOLGERSI IN EUROPA?

Assolutamente non è l'unico modo, ma è una strada che possiamo e dobbiamo cominciare a percorrere. Dagli altri stati membri, sono tante le cause pendenti alle Corti Europee per questioni legati alla discriminazione delle coppie dello stesso sesso. A maggior ragione, possiamo farlo anche noi dall'Italia, visto che in Italia non c'è nemmeno una legge contro l'omofobia.

Dobbiamo essere consapevoli che, se la Corte costituzionale fatica a riconoscere pienamente la discriminazione subita dalle coppie dello stesso sesso in Italia, allora possiamo rivolgerci anche altrove. Oltre che tornare a bussare, con tempistiche adeguate, alla stessa porta.

Nel frattempo ci auguriamo che possa esserci l'adeguata sensibilizzazione nei giornali, nei media, ovunque la gente sia interessata a farsi un'opinione e riflettere con la propria testa.