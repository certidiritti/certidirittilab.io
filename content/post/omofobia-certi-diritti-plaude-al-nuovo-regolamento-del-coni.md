---
title: 'OMOFOBIA: CERTI DIRITTI PLAUDE AL NUOVO REGOLAMENTO DEL CONI.'
date: Wed, 18 May 2016 14:37:41 +0000
draft: false
tags: [Politica]
---

[![stop-omofobia](http://www.certidiritti.org/wp-content/uploads/2010/06/stop-omofobia.jpg)](http://www.certidiritti.org/wp-content/uploads/2010/06/stop-omofobia.jpg)Si è appreso ieri che il presidente del Coni Giovanni Malagò, d’intesa con il direttore dell'Unar, Francesco Spano, ha modificato il proprio Codice di comportamento sportivo inserendo tra le cause di potenziale discriminazione anche «l’orientamento sessuale».

«È dal 2014 che [chiediamo](http://www.certidiritti.org/wp-content/uploads/2014/02/richiesta_di_iniziative_contro_le_discriminazioni_basate_su_orientamento_sessuale_e_identita_di_genere_anche_in_vista_di_SOCHI.pdf) e lavoriamo per questo risultato che oggi celebriamo con gran gioia. Spiace però che le nostre richieste non siano state accolte integralmente e non si siano state incluse anche l’identità e l’espressione di genere tra le cause di potenziale discriminazione. Continueremo a lavorare su questo». Lo dice Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti.

Roma, 18 maggio 2016