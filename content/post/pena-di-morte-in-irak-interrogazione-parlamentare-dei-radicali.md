---
title: 'PENA DI MORTE IN IRAK: INTERROGAZIONE PARLAMENTARE DEI RADICALI'
date: Wed, 08 Apr 2009 10:44:05 +0000
draft: false
tags: [Comunicati stampa, GAY, irak, pena di morte]
---

**PENA DI MORTE IN IRAK: ALLARME PER 128 CONDANNATI A MORTE, ANCHE PER OMOSESSUALITA’. LE AUTORITA’ ANNUNCIANO 20 ESECUZIONI A SETTIMANA. ALLARME DI AMNESTY INTERNATIONAL E DELLE ONG UMANITARIE.**

**INTERROGAZIONE PARLAMENTARE DEI DEPUTATI RADICALI – PD.**

Il grave incremento di condanne a morte in Irak desta grave preoccupazione tra le Ong internazionali che si battono per la difesa dei diritti umani. Amnesty International ha recentemente lanciato un appello per salvare la vita a 128 condannati a morte, tra questi anche persone accusate di omosessualità. Le autorità irakene hanno fatto sapere di voler eseguire le condanne a morte a gruppi di 20 a settimana. Lo scorso febbraio il Governo italiano aveva fatto proprio un Ordine del Giorno dei deputati radicali del Pd che prevede specifiche clausole per assicurare il rispetto dei diritti umani analoghi a quelli previsti dall'Unione europea relativamente al suo impegno di cooperazione con l’Irak.

**Su questa vicenda i deputati radicali del Pd, primo firmatario Matteo Mecacci, hanno depositato la seguente interrogazione parlamentare a risposta scritta:**

Per Sapere – premesso che:

\- Il 9 marzo 2009 il Consiglio giudiziario supremo iracheno ha informato Amnesty International dell'avvenuta conferma da parte del Consiglio presidenziale dell'Iraq di 128 condanne a morte;

\- tutte le 128 persone sono a rischio di esecuzione imminente. Le autorità hanno annunciato l'intenzione di eseguire le condanne a morte a gruppi di 20 persone a settimana.;

\- Nessun dettaglio è stato reso disponibile dal Consiglio giudiziario supremo in merito ai casi delle 128 persone a rischio di esecuzione, neanche riguardo alle loro identità. Non è noto se tra le 128 persone vi siano prigionieri trasferiti dalla custodia statunitense a seguito dell'Accordo tra il governo americano e quello iracheno, entrato in vigore il 1° gennaio 2009 che prevede il trasferimento di detenuti dalla custodia statunitense a quella irachena e il ritiro delle truppe Usa dall'Iraq entro la fine del 2011;

 \- Le condanne a morte sono state inizialmente comminate dalle Corti penali di Baghdad, Bassora e di altre città e province con accuse basate sul Codice penale iracheno e sulla Legge anti-terrorismo, tra le quali omicidio e rapimento. Amnesty International teme che molti imputati possano essere stati condannati a seguito di processi iniqui non conformi agli standard internazionali;

\- secondo alcune ong e associazioni per la difesa dei diritti delle persone gay, tra i 128 condannati a morte vi sono anche persone accusate di omosessualità; secondo il portavoce dell’organizzazione umanitaria, Iraqi Lgbt, Ali Hili, le uccisioni per pena capitale hanno avuto un allarmante incremento con l’insediamento del nuovo regime nel 2004 e sono costate la vita a 17 attivisti della sua organizzazione;

\- in Irak la pena di morte è stata sospesa nel 2003 mentre si trovava sotto l'occupazione della coalizione guidata dagli Usa. Tuttavia, da quando è stata reintrodotta dal governo iracheno, nell'agosto 2004, centinaia di persone sono state condannate a morte. Nel 2006 sarebbero state eseguite almeno 65 condanne, molte a seguito di processi irregolari;

\- Nel 2007 almeno 199 persone sarebbero state condannate e 33 messe a morte; nel 2008 le persone condannate sono state circa 285 e almeno 34 quelle messe a morte. Il totale potrebbe essere molto più alto rispetto ai dati ufficiali poiché le cifre fornite della stampa irachena sulle condanne a morte sono sottostimate.

\- il 24 febbraio 2009 il Governo italiano ha accolto un ordine del giorno (Atto Camera 9/2037/1) dei deputati radicali del Gruppo Pd che, tenendo conto della considerazione degli “accordi di cooperazione stipulati dall'Unione europea con i Paesi in via di sviluppo contengono precise clausole legali che ne subordinano l'attuazione al loro rispetto” e che “il trattato di amicizia, partenariato e cooperazione con l'Iraq prevede l'impegno al rispetto dei diritti umani solo nella parte relativa ai principi generali”, lo impegna a “a prevedere negli accordi di cooperazione bilaterali clausole per assicurare il rispetto dei diritti umani analoghi a quelli previsti dall'Unione europea”;

per sapere:

- quali iniziative il Ministro intende promuovere verso la autorità irakene affinché venga salvata la vita ai 128 irakeni condannati a morte;

- se non ritenga il Ministro intervenire affinché gli aiuti dell’Italia all’Irak siano vincolati al rispetto dei più elementari diritti civili e umani;

- se non ritenga urgente il Ministro sollecitare le autorità irakene ad aderire alla richiesta Onu di moratoria contro la pena di morte;

\- se non ritenga inoltre il Ministro che, in base all’impegno assunto dal Governo il 24 febbraio 2009, accogliendo l’odg citato in premessa, vi sia anche un obbligo istituzionale a intervenire urgentemente verso l’Irak in relazione al mancato rispetto dei diritti civili e umani.