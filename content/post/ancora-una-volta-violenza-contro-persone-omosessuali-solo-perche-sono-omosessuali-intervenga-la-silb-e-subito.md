---
title: 'Ancora violenza contro persone omosessuali solo perchè sono omosessuali. Intervenga subito la SILB. Solidarietà ad Arcigay'
date: Sun, 18 Mar 2012 21:19:30 +0000
draft: false
tags: [Politica]
---

18 marzo 2012

comunicato stampa dell'associazione radicale Certi Diritti

Marco Coppola, presidente di Arci Gay Verbania e membri della segreteria nazionale, insieme ad altri militanti di Arcigay hanno subito aggressioni e violenze private solo perchè sono gay. Si ripete quindi una nsituazione già vista e non più sopportabile di episodi gravi di violenz e discriminazione che nessuna istituzione riesce a domare.

In casi precedenti di violenze contro donne o contro persona immigrate erano stata interpellate ed erano anche intervenute le associazioni che raggruppano le discoteche italiane. Ora sappiamo che il Just in fa parte della SILB, Associazione italiana imprese di intrattenimento. E' il caso, quindi che oltre alle Istituzioni nazionali e locali, per quanto di loro competenza, la SILB si faccia promotrice di una vera e propria campagna contro sessismo e omofobia nei locali aderenti. Così come in tutta Europa accade.

L'Associazione Radicale Certi Diritti esprime la sua più sentita solidarietà ad Arcigay per questo ennesimo atto di inciviltà avvenuto contro suoi rappresentanti.