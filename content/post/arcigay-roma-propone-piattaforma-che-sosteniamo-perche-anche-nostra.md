---
title: 'ARCIGAY ROMA PROPONE PIATTAFORMA CHE SOSTENIAMO PERCHE'' ANCHE NOSTRA!'
date: Thu, 11 Mar 2010 14:30:19 +0000
draft: false
tags: [Comunicati stampa]
---

**LE PROPOSTE PRESENTATE DA ARCIGAY ROMA A TUTTI I CANDIDATI DEL LAZIO SONO LE STESSE CONTENUTE NEL PROGRAMMA DELLA LISTA BONINO-PANNELLA.**

_**Dichiarazione di Sergio Rovasio, Segretario dell’Associazione Radicale Certi Diritti, candidato tra i capilista della Lista Bonino-Pannella nel Lazio.**_

Sottoscrivo pienamente la piattaforma di Arcigay Roma in difesa dei diritti delle persone lgbt(e). Quanto richiesto dal loro documento rientra nell’ambito di competenze della Regione Lazio e sono anche i temi per i quali si batte da sempre l’Associazione Radicale Certi Diritti di cui sono il Segretario.

Inoltre le proposte che  Arcigay Roma ha proposto a tutti i candidati sono le stesse contenute nel programma politico della Lista Bonino Pannella per il Lazio. Qui in basso il capitolo che include la parte relativa al ‘Rispetto delle diversità’.

Tra i punti  del programma pubblicato nel sito [www.listaboninopannella.it](http://www.listaboninopannella.it/) si legge:

Una Regione per il rispetto delle diversità

La Regione sosterrà politiche antidiscriminatorie, in particolare di lotta all’omofobia e alla transfobia e a qualsiasi forma di razzismo, favorendo iniziative di educazione civica e sessuale e un’attività di sensibilizzazione nelle scuole medie inferiori e superiori in materia di prevenzione del bullismo. Maggiore attenzione sarà data alle attività specifiche di prevenzione delle malattie sessualmente trasmissibili. Occorre altresì riconsiderare i soggetti destinatari degli interventi di sostegno economico e socio.sanitario previsti dalla programmazione regionale, facendo riferimento alla famiglia anagrafica così come è individuata e descritta nella legge del 1989 e accrescere l’opera di prevenzione delle discriminazioni nel mondo del lavoro.