---
title: 'Español'
date: Sun, 21 Jun 2009 07:18:13 +0000
draft: false
tags: [Senza categoria]
---

“CERTI DIRITTI”
===============

ASOCIACIÓN RADICAL
==================

Nuestras prioridades son:
-------------------------

... (de) empeñarnos fuertemente para ayudar a difundir la campaña de afirmación civil para el reconocimiento del derecho al matrimonio entre personas del mismo sexo,  aumentando el numero de parejas que deciden de apoyar esta iniciativa con un acto público y con la promoción en la comunidad LGBT italiana.  
... (de) empeñarnos fuertemente para llegar a una plataforma común entre todo el movimiento LGBT unido, en modo laico, para perseguir objetivos comunes, más allá de las singulares diferencias, legítimas, pero parciales e ideales.  
La asociación dedica sus esfuerzos en la memoria de Makwan Moloudzadeh (el joven de 21 años que fue ahorcado en Irán, el 5 de diciembre de 2007, porque acusado de tener relaciones homosexuales) y en nombre de todos aquellos que han sufrido abuso, discriminación y violencia por sus orientaciones sexuales.

AFIRMACIÓN CIVIL: una acción concreta para un derecho que nadie puede negar a las parejas homosexuales: el derecho de casarse.
------------------------------------------------------------------------------------------------------------------------------

En Italia el matrimonio entre personas del mismo sexo no está prohibido, pero las personas homosexuales no pueden casarse.  
Ninguna norma prohíbe el matrimonio homosexual, pero el Registro Civil se niega a preparar la documentación. Un derecho negado a causa de antiguos prejuicios, que no está justificado en la legislación italiana, por la cual legítimamente se puede llegar a celebrar los matrimonios homosexuales.  
Este es el proposito de Afirmación Civil: romper el prejuicio y dar contenido y prospectiva a el amor entre personas homosexuales que desean casarse.  
Una inciativa importante dentro de la justicia y el derecho con la que Certi Diritti y la red de promoción, Red Lenford, siguen paso a paso para que las parejas homosexuales puedan coronar su proyecto de amor a través de la decisión del juez.  
Como inspiración de nuestra iniciativa hay las campañas de desobediencia civil no violenta que en el pasado, como por ejemplo en los Estados Unidos contra las leyes de segregación, han llevado en los países democráticos a un progreso social, en el ámbito de los derechos civiles y las reformas institucionales, mediante la superación de leyes injustas que limitaban las oportunidades y la felicidad de las personas.

AMOR CIVIL: una propuesta de Reforma del Derecho de Familia
-----------------------------------------------------------

Certi Diritti ha participado en el trabajo de la Conferencia Permanente para la reforma del derecho de familia, coordinando la elaboración relativa a la definición de las distintas formas de familia y de unión. Este es también un proyecto dirigido a la clase política italiana, creado en el marco de la iniciativa "Amor Civil", que tiene por objeto la introducción de unos derechos importantes y, hoy en día, negados, entre los que tienen especial relevancia los relacionados con las formas de familia, adopción y niños.