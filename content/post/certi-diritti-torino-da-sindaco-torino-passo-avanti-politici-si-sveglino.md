---
title: 'CERTI DIRITTI TORINO: DA SINDACO TORINO  PASSO AVANTI, POLITICI SI SVEGLINO!'
date: Sat, 27 Feb 2010 14:28:57 +0000
draft: false
tags: [Comunicati stampa]
---

ASSOCIAZIONE RADICALE CERTI DIRITTI DI TORINO: “ANTONELLA E DEBORA, INSIEME AL SINDACO CHIAMPARINO CI FANNO FARE UN PASSO AVANTI VERSO L’EUROPA. L’ISTAT SE NE E’ ACCORTO?”. TUTTE LE FORZE POLITICHE APRANO GLI OCCHI E LE ORECCHIE E CERCHINO DI CONOSCERE LA REALTA’, NON IL SIMULACRO DELLE LORO VERITA’ IDEOLOGICHE.

Sabato 27 febbraio 2010

Enzo Cucco, Gabriele Murgia e Gian Mario Felicetti, membri del Direttivo dell’Associazione radicale Certi Diritti , salutano e ringraziano Antonella e Debora per la loro scelta, tutta gandhiana, di “essere il cambiamento che vogliamo vedere nel mondo”. Di testimoniare, quindi, che esistono diverse forme di famiglie, che di fronte alla legge devono avere pari opportunità di riconoscimento, sostegno e sviluppo. E non chiudere gli occhi e le orecchie come fanno coloro che la realtà della nostra società la guardano attraverso le lenti della ideologia, politica o religiosa che sia.

Il Sindaco Chiamparino, attraverso il suo gesto, ci aiuta ad esser un po’ più europei, ad una società che allarga i suoi orizzonti, include e protegge ogni forma di convivenza. Una Europa dove in molti paesi il diritto al matrimonio tra persone dello stesso sesso e quasi ovunque il diritto al riconoscimento delle unioni, vengono garantiti e sostenuto da maggioranze di centrodestra come di centro sinistra.

La situazione italiana è invece ancora alle prese con chiusure antistoriche e oscurantiste, come quella dell’ISTAT che si appresta ad avviare il nuovo censimento senza includere indicatori sulla dimensione del numero di convivenze tra persone dello stesso sesso nel nostro paese. Non si vuole conoscere perché non si vuole ri-conoscere il diritto e le pari opportunità che la costituzione italiana e la legge europea ha da tempo sancito.

Le attività dell’Associazione radicale Certi Diritti, che insieme alla Rete Lenford hanno dato vita all’iniziativa “Affermazione civile” che è stato l’atto iniziale da cui è scaturita la cerimonia di oggi, continueranno con maggior vigore e determinazione. Sia a livello locale, chiedendo al Consiglio comunale che approvi la proposta di iniziativa popolare per pari opportunità per le unioni civili, sia chiedendo al Sindaco di farsi promotore in seno all’ANCI di una iniziativa di studio e promozione di nuove politiche locali anche a favore delle nuove forme di famiglia.

In vista della prossima scadenza del 23 marzo, quando la corte costituzionale dovrebbe pronunciarsi sui ricorsi contro il divieto di pubblicazione del matrimonio che le coppie di Affermazione civile hanno prodotto in tutta Italia.

Torino, 27 febbraio 2010

Per ulteriori informazioni:  
Enzo Cucco 347.0431401  
Gabriele Murgia 347.2511137  
Gian Mario Felicetti, 329.9045945, coordinatore nazionale campagna  
“Affermazione civile”