---
title: 'TURISMO SESSUALE E SFRUTTAMENTO DEI BAMBINI - CAMPAGNA ALDE CON PETIZIONE'
date: Wed, 16 Jul 2008 11:50:05 +0000
draft: false
tags: [abuso minori, ALDE, Comunicati stampa, sfruttamento minori, tratta di essere umani]
---

E' partita la campagna contro il turismo e lo sfruttamento sessuale dei bambini promossa del Gruppo dei Liberali e Democratici del Parlamento europeo.  
[Clicca qui](http://www.sayno.eu/) per saperne di più e [firmare la petizione online](http://www.sayno.eu/) , dove è presenta anche un video pubblicitario impressionante.  
  
La campagna combina le due tematiche del **traffico di esseri umani** e degli **abusi sui bambini,** spesso collegate tra loro, con il proposito di informare sulla dimensione e la complessità di questi problemi, spesso invisibili.  
  
Tra gli obiettivi della campagne quello della creazione di un **numero europeo di emergenza** per le vittime di sfruttamento e la pubblicazione di un **rapporto annuale sul traffico di esseri umani in Europa.**