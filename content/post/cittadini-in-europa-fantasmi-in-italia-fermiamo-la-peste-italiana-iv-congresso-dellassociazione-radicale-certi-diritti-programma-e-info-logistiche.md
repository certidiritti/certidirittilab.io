---
title: 'CITTADINI IN EUROPA, FANTASMI IN ITALIA:  FERMIAMO LA PESTE ITALIANA! IV CONGRESSO dell''ASSOCIAZIONE RADICALE CERTI DIRITTI - Programma e info logistiche'
date: Fri, 15 Oct 2010 04:37:05 +0000
draft: false
tags: [Comunicati stampa, CONGRESSO, invito, programma, roma]
---

**CITTADINI IN EUROPA, FANTASMI IN ITALIA:**

**FERMIAMO LA PESTE ITALIANA!**

### **IV CONGRESSO ****dell'****ASSOCIAZIONE RADICALE CERTI DIRITTI**

### **Roma, 27-28 novembre 2010**

**Sabato 27 Novembre 2010  
presso l’Hotel Palatino Roma, via Cavour 213  
  
**

Ore 10-11  
**RELAZIONI degli organi statutari**  
Segretario, Presidente, Tesoriere  
  
  
Ore 11-14  
** I DIRITTI LGBT IN ITALIA E NEL MONDO**

Mario STADERINI, segretario Radicali Italiani  
Ivan SCALFAROTTO, vice-presidente PD  
Sonia ALFANO, eurodeputata IDV  
Benedetto DELLA VEDOVA, deputato FLI

Saluto della Dottoressa Agnese Canevari, UNAR

Marco CAPPATO, segretario Associazione Luca Coscioni  
Ottavio MARZOCCHI, funzionario e membro dell'intergruppo lgbt PE  
Santos FELIX, Casal Lambda - Barcellona  
Nikolai ALEXEYEV, organizzatore del Pride di Mosca  
Kato David KISULE, attivista diritti lgbt in Uganda

  
Ore 14-15  
Pausa pranzo  
  
  
Ore 15-17

_**'Laicità e progresso'**_  
MARILISA D'AMICO, ordinario di Diritto costituzionale e avvocato cassazionista

_**'Cultura di genere e diritti glbt'**_  
ELIO MATASSI, direttore del Dipartimento di Filosofia all' Università di 'Roma Tre'

**_'Il corpo recluso e la situazione delle carceri in Italia'  
_**RITA BERNARDINI, deputata radicale

**_'L'Intersessualità nella società Italiana'  
_**MICHELA BALOCCHI, dottoressa di ricerca in Sociologia e Sociologia Politica, Università di Firenze

_**'Il fallimento delle politiche proibizioniste sulla prostituzione'**_  
PIA COVRE, presidente del Comitato per i diritti civili delle prostitute

**_'I diritti, le persone e la legge 40/04'  
_**FILOMENA GALLO, avv. esperta di Diritto di Famiglia, Diritto Comunitario e Biodiritto e Vice Segretario Associazione Luca Coscioni

**_'Omofobia e Omogenitorialità: prospettive liberali'  
_**CHIARA LALLI, docente di Logica e Filosofia della Scienza Università 'La Sapienza' di Roma

Ore 17-21  
**Commissioni di approfondimento tematico e di iniziativa politica**  
  
1\. Matrimonio gay: dopo la sentenza la nostra battaglia continua  
PRESIDENTE: Gian Mario Felicetti  
  
2\. Certi diritti civili in Europa e nel mondo  
PRESIDENTE: Ottavio Marzocchi  
  
3\. Verso l'affermazione civile delle persone trans  
PRESIDENTE: Maria Gigliola Toniollo

Ai lavori delle commissioni hanno confermato la loro presenza:  
**Nikolai Alexeyev**, organizzatore del Pride di Mosca - avv. **Ileana Alesso** \- **Arcietero** \- **Marco Beltrandi**, deputato radicale - **Rocco Berardo**, consigliere regionale Lazio - **Rita Bernardini**, deputata radicale - avv. **Massimo Clara** \- avv. Nicola Coco - **Paola Concia**, deputata PD - avv. **Marilisa D'Amico** \- **Kato David Kisule**, attivista ugandese diritti lgbt - **Leila Deianis**, presidente Libellula - **Rita De Santis**, Presidente Agedo - **Santos Félix**, Casal Lambda Barcellona - **Giuseppina La Delfa**, Presidente Famiglie Arcobaleno - prof. **Chiara Lalli** \- **Valeria Manieri**, Radicali Italiani - prof. **Elio Matassi** \- **Matteo Mecacci**, deputati radicale - **Paolo Patanè**, Presidente Arcigay - **Francesca Polo**, Presidente Arcilesbica - **Angelo Pezzana**, fondatore 'Fuori!' - **Donatella Poretti**, senatrice radicale - **Daniele Priori**, segretario GayLib - **Francesca Rufino**, vicepresidente Libellula - **Carlo Santacroce**, Presidente 3D - **Ivan Scalfarotto**, vice presidente PD - **Antonio Stango**, Partito Radicale Transnazionale - **Alessandro Valera**, European Alternatives e SEL - **Gianni Vattimo**, eurodeputato IDV - **Mina Welby - UAAR **

**Domenica 28 Novembre 2010  
presso la sede dei Radicali in Via di Torre Argentina, 76  
**

Ore 9,15-10,15  
**Relazione delle Comissioni**  
\- Gian Mario Felicetti  
\- Ottavio Marzocchi  
\- Maria Gigliola Toniollo

  
Ore 10,15-14  
**Dibattito generale degli iscritti**

  
Ore 14-16  
**VOTAZIONI**  
Presentazione modifiche statutarie (Relative discussione e votazioni)  
Mozione generale e mozioni particolari (Discussione e votazioni)  
Elezione degli organi Statutari (Presentazione candidature, eventuale discussione e votazioni)  
  
  
Ore 16  
**_Oltre le intenzioni: come e perché dobbiamo lavorare insieme per i diritti lgbti_**

_T_avola rotonda con (non tutte le associazioni hanno ancora confermato la partecipazione):  
ArciGay, ArciLesbica, Agedo, Famiglie Arcobaleno, GayLib, 3D, MIT,  
Mario Mieli, Associazione Transgenere e Libellula.

[**A questo link le informazioni logistiche per il pernottamento a Roma >>>**](tutte-le-notizie/870-iv-congresso-certi-diritti-informazioni-logistiche.html)  
**Certi Diritti consiglia un elenco di B&B, Ostelli, Pensioni a buon prezzo nelle vicinanze del centro storico di Roma.**

Per gli studenti e i senza reddito stiamo organizzando sistemazioni presso amici e compagni di Roma. Invitiamo coloro che vivono a Roma a darci disponibilità ad ospitare i congressisti e chi è di fuori Roma a segnalarci tale necessità all’indirizzo e-mail: [info@certidiritti.it](mailto:info@certidiritti.it)