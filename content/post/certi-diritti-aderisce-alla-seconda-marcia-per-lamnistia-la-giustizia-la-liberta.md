---
title: 'Certi Diritti aderisce alla Seconda marcia per l’amnistia, la giustizia, la libertà'
date: Fri, 23 Mar 2012 13:36:28 +0000
draft: false
tags: [Politica]
---

La marcia avrà luogo a Roma domenica 8 aprile alle ore 10.

comunicato stampa dell'associazione radicale Certi Diritti

23 marzo 2012

Come associazione che si batte per il rispetto dei diritti umani e civili siamo preoccupati per la costante fragranza di reato nella quale il nostro paese si pone per via del disumano sovraffollamento delle carceri, per l'abuso della pratica della carcerazione preventiva e per "l'irragionevole durata dei processi che costituisce un grave pericolo per lo stato di diritto", come ha detto il Comitato dei Ministri del Consiglio d'Europa sanzionando per l'ennesima volta l'Italia.

Dal 1959 al 2010 la Corte Europea dei Diritti Umani ha condannato l’Italia 2.121 volte per violazioni della Convenzione. L’Italia si è così classificata al secondo posto dietro la Turchia (2.573 violazioni) e prima della Russia (1079) e della Polonia (874), ma quel che è più grave a costantemente ignorato le sentenze della CEDU perpetrando queste gravi violazioni dei diritti umani e alimentando a dismisura i ricorsi alla stessa CEDU rallentandone e ostacolandone i lavori anche su altri temi. Questa "peste italiana" sta mettendo a rischio la stessa sopravvivenza della CEDU così come la conosciamo, come dimostra il tentativo di riforma proposto dalla Gran Bretagna.

Non ultimo le carceri sono un luogo d'imperante sessuofobia dove i detenuti non hanno diritto né alla sessualità né all'affettività, al contrario di quanto accade nei paesi più civili d'Europa dove esistono stanze dell'amore in cui i detenuti possono incontrare i propri partner. Questo è un elemento non secondario nel determinare l'elevato tasso di violenza carnale presente nelle nostre carceri. La sessuofobia si ripercuote infine anche sulla salute dei detenuti poiché impedisce la distribuzione di preservativi in carcere per prevenire la trasmissione di malattie veneree in caso di rapporti sessuali tra detenuti.

Per tutte queste ragioni, l'Associazione Radicale Certi Diritti aderisce con convinzione alla Seconda marcia per l’amnistia, la giustizia, la libertà.

**[ADERISCI >](http://radicali.it/primopiano/20120321/seconda-marcia-l-amnistia-giustizia-libert-natale-2005-pasqua-2012)**