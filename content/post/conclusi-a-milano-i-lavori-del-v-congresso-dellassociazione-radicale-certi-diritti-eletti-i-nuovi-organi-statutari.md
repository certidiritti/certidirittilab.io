---
title: 'Conclusi a Milano i lavori del V Congresso dell''associazione radicale Certi Diritti. Eletti i nuovi organi statutari'
date: Sun, 04 Dec 2011 16:50:26 +0000
draft: false
tags: [Politica]
---

Yuri Guaiana è il nuovo segretario dell'associazione, Enzo Cucco nuovo presidente e John Francis Onyengo, avvocato di David Kato Kisule, presidente onorario. Confermato il tesoriere Giacomo Cellottini.

Milano, 4 dicembre 2011

Sono terminati oggi a Milano i lavori del V Congresso dell’Associazione Radicale Certi Diritti. Il Congresso ha votato una Mozione generale che impegna l’Associazione a considerare prioritaria la lotta contro ogni forma di sessuofobia, iniziative per la regolamentazione della prostituzione e contro ogni forma di proibizionismo, sostenere in ogni sede la campagna di Affermazione civile e la Riforma del Diritto di Famiglia, promuovere azioni contro ogni forma di discriminazione e impegnarsi per dare concretezza alla scelta transnazionale insieme al Partito Radicale Nonviolento, transnazionale e transpartito.

Il Congresso ha eletto segretario Yuri Guaiana, presidente Enzo Cucco e presidente onorario Francis Onyango (l’avvocato ugandese di David Kato Kisule). Confermati il tesoriere Giacomo Cellottini, la rappresentante presso il comitato di Radicali Italiani Maria Gigliola Toniollo e e revisore dei conti Luca Piva.

**ISCRIVITI O CONTRIBUISCI ALL'ASSOCIAZIONE  
PER IL 2012 >>>>** **[http://www.certidiritti.it/iscriviti](iscriviti)**