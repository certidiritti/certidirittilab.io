---
title: 'MOBILITAZIONE IN TUTTA ITALIA CONTRO VERGOGNA GOVERNO ITALIANO'
date: Sun, 08 Feb 2009 11:18:40 +0000
draft: false
tags: [Comunicati stampa]
---

Continua in tutta Italia, senza che i media nazionali ne diano notizia, la mobilitazione dei laici, degli anticlericali, di coloro che hanno a cuore la difesa dei diritti civili e umani, contro la vergognosa azione liberticida del Governo italiano sulla vicenda Englaro. Dopo le manifestazioni promosse a Roma venerdì 6 e sabato 7 febbraio a Palazzo Chigi e quella di Torino, che hanno visto la partecipazione di migliaia di cittadini, in altre città italiane sono previste manifestazioni, sit-in, fiaccolate con la partecipazione di Certi Diritti.

Tra le altre segnaliamo quelle di:

Firenze domenica 8 febbraio alle ore 11.30 davanti alla Prefettura

Catania domenica 8 febbraio alle ore 20 in Piazza Duomo con Aurelio Mancuso Presidente nazionale di Arcigay

Milano, domenica 8 febbraio davanti al Sagrato

Trieste, domenica 8 febbraio alle ore 11.30

Parma martedì 10 febbraio dalle ore 13 alle ore 15 davanti alla sede della Prefettura