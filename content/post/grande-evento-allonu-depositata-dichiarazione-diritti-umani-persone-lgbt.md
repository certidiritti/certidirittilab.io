---
title: 'GRANDE EVENTO ALL''ONU: DEPOSITATA DICHIARAZIONE DIRITTI UMANI PERSONE LGBT'
date: Fri, 19 Dec 2008 12:05:07 +0000
draft: false
tags: [Comunicati stampa]
---

**STORICO EVENTO ALLE NAZIONI UNITE PER I DIRITTI UMANI DELLE PERSONE LGBT: STOP ALLE PERSECUZIONI. LA RAPPRENSENTATE ONU DIRITTI UMANI: “LA CRIMINALIZZAZIONE DELL'OMOSESSUALITA EQUIVALE ALL'APARTHEID”.**

**_Dichiarazione di Sergio Rovasio, segretario dell'associazione Certi Diritti, e di Ottavio Marzocchi, responsabile per le questioni europee:_**

"L’Associazione Radicale Certi Diritti esulta per lo storico evento di ieri notte alle Nazioni Unite sul fronte dei diritti umani delle persone LGBT: l'Argentina ha infatti presentato all'Assemblea Generale dell'ONU, a nome dei 66 paesi firmatari, e per la prima volta nella storia dell'ONU, la dichiarazione sponsorizzata dalla Francia e co-firmata da tutti i paesi dell'Unione europea sulla decriminalizzazione universale dell'omosessualità. I Ministri degli esteri olandese Verhagen  e la Ministra francese per i diritti umani Yade hanno successivamente illustrato l'iniziativa nel corso di un evento di alto livello all'ONU.

Tra i paesi firmatari, oltre ai 27 membri dell’Unione Europea e quasi tutti i paesi sudamericani, vi sono Israele, Nepal, Giappone insieme ad alcuni paesi africani. Sorprende la mancata firma degli Stati Uniti. Il Vaticano, evidentemente in forte imbarazzo, ha tentato una giustificazione incomprensibile per non avere sostenuto la dichiarazione.

Il Parlamento europeo, la Commissione ed il Consiglio UE avevano dibattuto - su iniziativa di Marco Cappato, Eurodeputato Radicale - ed appoggiato pubblicamente la dichiarazione all'ONU. Anche il Governo italiano si era impegnato, in risposta ad una interrogazione del deputato radicale del Pd Matteo Mecacci, a sostenere in ogni sede la depenalizzazione dell’omosessualità all’Onu.

L'Associazione Radicale Certi Diritti ringrazia in particolare la Commissaria dell'ONU per i diritti umani Navanethem Pillay per il suo appoggio all'iniziativa. La Commissaria ha paragonato la criminalizzazione dell'omosessualità all'apartheid, ambedue "contrarie al diritto internazionale ed ai valori condivisi di dignità, inclusione e di rispetto per tutti".

Certi Diritti si augura che sia possibile, come sulla pena di morte, rafforzare ulteriormente l'iniziativa e portare ed approvare il documento francese alle Nazioni Unite come Risoluzione affinché nei 77 paesi dove l’omosessualità è considerata un reato e nei 7 paesi dove è prevista la pena di morte si cambi subito rotta”.