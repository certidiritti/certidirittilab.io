---
title: 'GAY, FIRENZE: CORTE DI APPELLO SI RISERVA DECISIONE SU MATRIMONIO'
date: Fri, 20 Jun 2008 14:26:02 +0000
draft: false
tags: [Comunicati stampa]
---

I GIUDICI DELLA CORTE, CHIAMATI PER PRIMI, IN ITALIA, A PRONUNCIARSI SUL MATRIMONIO OMOSESSUALE, HANNO VOLUTO ASCOLTARE IN CAMERA DI CONSIGLIO I LEGALI DI PIOMBONI E PEGORARO. FORSE NEI PROSSIMI GIORNI LA SENTENZA  
  
  
I giudici della **Corte d’Appello di Firenze**, riuniti oggi in **camera di consiglio**, **si sono riservati di decidere sul ricorso presentato da Francesco Piomboni e Matteo Pegoraro**, la **coppia omosessuale che ha richiesto le pubblicazioni al Comune di Firenze il** 29 marzo 2007 e dopo il diniego ha iniziato un’azione legale per affermare il proprio diritto a contrarre matrimonio.

“Siamo soddisfatti,” commentano Pegoraro e Piomboni “perché **i giudici hanno preferito ascoltare i nostri legali prima di emettere ogni decisione** e soprattutto hanno dimostrato professionalità e volontà di emettere una sentenza ben ponderata. **Hanno compreso appieno che la questione riguarda il rispetto del principio di uguaglianza e di non discriminazione**, alla cui base stanno gli articoli 2 e 3 della nostra Costituzione; hanno altresì dimostrato di aver studiato attentamente gli atti, e, di fatto, hanno deciso di rifletterci bene. **Abbiamo fiducia nella giustizia**” continua la coppia “e **ci auguriamo che le nostre istanze vengano accolte**, non come una mera provocazione ma come una vera e propria rivendicazione dei diritti a noi spettanti in qualità di cittadini e una ferma volontà di vedere riconosciuta e tutelata a tutti gli effetti dallo Stato la nostra unione”.

Intanto, prosegue l'azione di affermazione civile promossa dall'**Associazione Radicale Certi Diritti**, e **ieri mattina una coppia gay si è recata all'Ufficio di Stato Civile del Comune di Firenze, dove ha ufficialmente richiesto le pubblicazioni di matrimonio**. Palazzo Vecchio comunicherà a giorni ai due uomini l'esito della loro richiesta, che se verrà respinta sarà impugnata in giudizio. In questi giorni, in altre città d'Italia, una trentina di coppie sta per fare lo stesso.

**La sentenza definitiva della Corte d'Appello sul ricorso di Piomboni e Pegoraro è attesa con tutta probabilità a giorni.**

**Per ulteriori informazioni:**

Tel: (+ 39) 334-8429527 – (+39) 3381747342

[www.lastranafamiglia.it](http://www.lastranafamiglia.it/)

[francescoematteo@lastranafamiglia.it](mailto:francescoematteo@lastranafamiglia.it)