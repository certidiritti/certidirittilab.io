---
title: 'PORTA PIA: COMMEMORAZIONE DEL COMUNE UNA FARSA, OGGI ORE 16 QUELLA VERA'
date: Sat, 20 Sep 2008 08:12:35 +0000
draft: false
tags: [Comunicati stampa]
---

XX SETTEMBRE: LA COMMEMORAZIONE DELLA BRECCIA DI PORTA PIA DI QUESTA MATTINA UNA FARSA. RICORDATI I SOLDATI PAPALINI E PAPA PAOLO VI NEL SILENZIO GENERALE DELLE ISTITUZIONI.

LA VERA E UNICA COMMEMORAZIONE DELLA BRECCIA DI PORTA PIA SI FARA’ OGGI POMERIGGIO ALLE ORE 16 CON LA BANDIERA DELL’EUROPA.

Roma, 20 settembre 2008

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

“Questa mattina, nell’anniversario della Breccia di Porta Pia, ci siamo recati come Associazione Radicale Certi Diritti alla commemorazione ufficiale che il Comune di Roma, insieme ai rappresentati della Regione e della Provincia, promuove dal 1871 nel luogo della breccia in Corso d’Italia.

I rappresentanti del Comune, che hanno preso la parola, hanno ricordato i soldati papalini caduti durante gli scontri con i Bersaglieri, dando lettura dei loro nomi (e non di quelli dei bersaglieri caduti) Successivamente è stata addirittura ricordata la figura di Paolo VI. Nessuno dei rappresentanti delle istituzioni presenti ha contestato tali affermazioni, ad eccezione di alcuni cittadini che hanno fischiato gli oratori. Appena gli oratori hanno concluso gli interventi è stato tolto l’audio e i rappresentanti delle Associazioni presenti non sono potuti intervenire. Alcuni rappresentanti delle Associazioni presenti, tra le altre l’Uaar di Roma, Certi Diritti, la Fondazione Massimo Consoli, Radicali Italiani, hanno invitato i cittadini ad essere presenti oggi pomeriggio alle ore 16 nello stesso luogo per la commemorazione vera della Breccia di Porta Pia con alcuni parlamentari di diversi schieramenti politici con la consegna della bandiera dell’Europa”.