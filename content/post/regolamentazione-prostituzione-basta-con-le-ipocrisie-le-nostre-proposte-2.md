---
title: 'REGOLAMENTAZIONE PROSTITUZIONE: BASTA CON LE IPOCRISIE. LE NOSTRE PROPOSTE'
date: Sun, 13 Jul 2008 07:03:23 +0000
draft: false
tags: [Comunicati stampa]
---

**PROSTITUZIONE E FISCO.** _Dal caso della prostituta di Parma costretta a pagare tasse per un lavoro che nessuna legge considera tale, alla legalizzazione della vendita di prestazioni sessuali. **Dalla diretta voce dei protagonisti, le proposte di Radicali Italiani**_ ROMA, GIOVEDI' 17 LUGLIO, ORE 15, SALA STAMPA DEL SENATO Partecipano alla conferenza stampa: **Donatella Poretti**, Senatrice Radicale - PD**Marco Perduca**, Senatore Radicale - PD**Daniela Bertaso**, accompagnatrice;**Luca Berni**, avvocato di una delle prostitute condannate; **Pia Covre**, Presidente Comitato Diritti Civili delle Prostitute; **Bruno Mellano**, Presidente di Radicali Italiani;**Sergio Rovasio** Segretario dell'associazione Certi Diritti