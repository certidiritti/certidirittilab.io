---
title: 'OBIEZIONE DI COSCIENZA E UNIONI CIVILI'
date: Sat, 09 Jul 2016 09:57:43 +0000
draft: false
tags: [Diritto di Famiglia]
---

[obiezione di coscienza](http://www.certidiritti.org/wp-content/uploads/2016/07/obiezione-di-coscienza.pdf) [![obiezione-di-coscienza-499](http://www.certidiritti.org/wp-content/uploads/2016/07/obiezione-di-coscienza-499-300x147.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/07/obiezione-di-coscienza-499.jpg)Alcuni leader politici, deputati e senatori, sindaci, assessori, dirigenti e funzionari ripetono che esiterebbe un diritto a non applicare la Legge 76/2016 (Unioni civili e Coppie di fatto) con particolare riferimento alla registrazione delle unioni stesse. La chiamano “obiezione di coscienza” riferendosi ad anni e anni di lotte, a volte nobili a volte meno, di coloro che non intendono rispettare il dettato di una Legge perché contrario al loro convincimento. 

"Di fronte a questa situazione, l'Associazione Radicale Certi Diritti intende ricordare qualche dato e offrire qualche indicazione concreta alle coppie o agli attivisti e alle attiviste che si trovassero in difficoltà con un [documento](http://www.certidiritti.org/wp-content/uploads/2016/07/obiezione-di-coscienza.pdf) specifico che si può trovare sul nostro sito" dichiara il segretario Yuri Guaiana.

Roma, 9 luglio 2016