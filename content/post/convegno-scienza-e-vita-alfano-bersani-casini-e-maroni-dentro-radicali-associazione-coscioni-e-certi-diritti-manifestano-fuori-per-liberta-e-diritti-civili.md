---
title: 'Convegno scienza e vita: Alfano, Bersani, Casini e Maroni dentro. Radicali, Associazione Coscioni e Certi Diritti manifestano fuori per libertà e diritti civili'
date: Fri, 18 Nov 2011 12:35:16 +0000
draft: false
tags: [Politica]
---

"Noi saremo fuori a ricordare i diritti che vengono negati a milioni di italiani: dalle coppie di fatto al testamento biologico, dalla fecondazione assistita alla ricerca scientifica".

Roma, 18 novembre

Venerdì 18 ottobre, dalle ore 15, Radicali Italiani, l’Associazione Luca Coscioni e l'Associazione Radicale Certi Diritti manifesteranno all’esterno del Centro Congressi TV2000 di via Aurelia 796.

All’interno del Centro, nell’ambito del 8° Convegno nazionale di “Scienza e Vita” titolato “Scienza e cura della vita: educazione alla democrazia”, Alfano, Bersani, Maroni e Casini assisteranno alla lectio magistralis del Cardinal Bagnasco sui temi della bioetica.

Durante la manifestazione, che sarà aperta da una conferenza stampa a cui parteciperanno tra gli altri Mario Staderini, Segretario di Radicali Italiani, Filomena Gallo, Segretario dell’Associazione Coscioni e Sergio Rovasio, Segretario dell'Associazione Radicale Certi Diritti, sarà allestita una colorata rappresentazione di quanto avverrà all’interno.

“Nel momento in cui la politica  si reca a prendere lezioni di democrazia dal Presidente della CEI e da Scienza e Vita, ovvero dai due soggetti protagonisti dell’illegale sabotaggio del referendum sulla legge 40” –affermano Staderini, Gallo e Rovasio- “noi saremo fuori a ricordare i diritti che vengono negati a milioni di italiani: dalle coppie di fatto al testamento biologico, dalla fecondazione assistita alla ricerca scientifica.”