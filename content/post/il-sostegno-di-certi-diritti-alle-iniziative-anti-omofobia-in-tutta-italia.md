---
title: 'IL SOSTEGNO DI CERTI DIRITTI ALLE INIZIATIVE ANTI-OMOFOBIA IN TUTTA ITALIA'
date: Wed, 09 Sep 2009 07:56:22 +0000
draft: false
tags: [Comunicati stampa]
---

**Associazione Radicale Certi Diritti**  
**Centro di iniziativa politica nonviolenta, giuridica e di studio   
per la promozione e la tutela dei diritti civili   
in materia di identità di genere, scelte, comportamenti e orientamenti sessuali**.  
Via di Torre Argentina, 76 – 00186 Roma – Telefono: 06-689791 Fax: 06-68803861  
e-mail: [segretario@certidiritti.it](mailto:segretario@certidiritti.it) [www.certidiritti.it](http://www.certidiritti.it/)

Carissimi iscritti di Certi Diritti, simpatizzanti e amici,

gli atti di violenza contro persone omosessuali e transessuali avvenuti negli ultimi giorni stanno generando un moto, spesso spontaneo, di reazione civile. Fiaccolate, presidi e manifestazioni si moltiplicano in molte città italiane con una velocità tale che è impossibile tenere traccia di tutte le iniziative in campo.  

Perciò vi invitiamo a contattare i nostri referenti locali, per avere informazioni su cosa sta accadendo nelle nostre città, oppure farvi voi stessi promotori e comunicarci le vostre iniziative.

Questo il nostro appello: **"Scendiamo in piazza, tutti, e facciamo corpo con quelli che, come noi, si battono per una Italia più civile e umana, per una democrazia matura e rispettosa di tutte e di tutti."**  
  
Mai come in questi giorni è importante partecipare e far partecipare tutte le persone che, a prescindere dal loro orientamento sessuale, ritengono doveroso esprimere il rifiuto per un mondo di violenze e discriminazioni.   
  
L'omofobia potrebbe sgonfiarsi proprio occupando lo spazio che abbiamo lasciato per troppo tempo vuoto con il nostro silenzio.  
  
Nel ringraziarvi per l'aiuto e la collaborazione che potrete darci vi invitiamo a visitare il sito [www.certidiritti.it](http://www.certidiritti.it/) per trovare i nominativi ed i contatti dei referenti locali della nostra Associazione sul territorio nazionale.  
  
Un caro saluto,

Clara Comelli, Presidente

Sergio Rovasio, Segretario