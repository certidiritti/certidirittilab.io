---
title: 'INT. A S. ROVASIO: "PER SUPERARE TUTTI I PREGIUDIZI"'
date: Wed, 19 May 2010 22:12:07 +0000
draft: false
tags: [Senza categoria]
---

• da la Voce Repubblicana del 19 maggio 2010

di Lanfranco Palazzolo

**Sergio Rovasio, il Presidente della Repubblica Giorgio Napolitano ha ricevuto lunedì scorso le associazioni che si battono per il riconoscimento dei diritti civili e contro l'omofobia, proprio nella giornata mondiale contro l'omofobia...**  
E' stato un gesto positivo e straordinario che il Presidente della Repubblica abbia accettato la proposta dell'onorevole Paola Concia di ricevere le associazioni Lgbt nel corso della giornata internazionale contro l'omofobia. E' stato un gesto simbolico molto importante. Nel corso dell'incontro ci sono stati degli interventi molto positivi e di alto livello. Il Capo dello Stato ha ascoltato con grande attenzione i temi che gli sono stati sottoposti nel corso dell'incontro".

**All'incontro era presente anche il ministro per le Pari Opportunità Mara Carfagna. Cosa ha detto?**  
"Il ministro Mara Carfagna ha detto due cose molto importanti. Lei stessa si è resa conto che grazie al lavoro di confronto che ha svolto in questi mesi ha superato una forma di pregiudizio che sembrava nutrire. Il ministro lo ha ammesso, spiegando di aver superato questa posizione. La Carfagna ha tenuto a ringraziare tutti e ha spiegato quanto sia importante trovare nuove leggi per garantire i diritti e garantire le pene per chi commette atti di violenza nei confronti delle persone gay e lesbiche, e per le minoranze in generale. E già questo è un fatto molto importante. Il ministro si è augurato che vengano approvate quanto prima le norme l'omofobia. Questo vorrà dire implicitamente che il governo le sosterrà in sede parlamentare. Questo è uri impegno molto importante".

**Avreste preferito che l'iniziativa di una legge contro l'omofobia fosse del Parlamento? Con una maggiore esperienza si sarebbe potuto evitare l'affondamento del provvedimento...**  
"Quello che è accaduto lo scorso ottobre è un vero e proprio atto di viltà. Si è approfittato del Regolamento della Camera per fare lo sgambetto ad un provvedimento che non si voleva approvare. E' stata utilizzata l'ideologia e una visione integralista della storia e della società per impugnare uno strumento scorretto - come quello dell'incostituzionalità - che nulla aveva a che vedere con il provvedimento. Il ministro Carfagna era d'accordo con quella legge, ma aveva proposto degli aggiustamenti chiedendo il ritorno del testo in commissione".

**In quella occasione aveva sbagliato Paola Concia ad affrontare il voto sulla costituzionalità della legge?**  
"No, è stato il gruppo del Pd a sottovalutare la questione. Non dimenticate che a quell'epoca c'era ancora la Binetti e tutta la componente integralista del Pd".