---
title: 'REGALEREMO AL CARDINALE POLETTO LA COSTITUZIONE ITALIANA'
date: Sun, 14 Feb 2010 10:45:01 +0000
draft: false
tags: [Comunicati stampa]
---

CERTI DIRITTI REGALERA' AL CARDINALE POLETTO DI TORINO TESTO DELLA COSTITUZIONE ITALIANA.   
  
L'Associazione Radicale Certi Diritti regalerà al Cardinale Poletto il testo della Costituzione Italiana

Ci farebbe piacere infatti, aldilà delle convinzioni religiose del cardinale Poletto, che lo stesso verifichi, una volta per tutte, come l'art. 29 della Carta fondamentale della Repubblica non specifica che il matrimonio è una relazione tra un uomo e una donna. Ma che, genericamente,recita "il matrimonio è ordinato sull'uguaglianza morale e giuridica dei coniugi, con i limiti stabiliti dalla legge a garanzia dell'unità familiare".

Non chiediamo al Cardinale di cambiare opinione riguardo le unioni tra persone dello stesso sesso. Gli chiediamo soltanto di non stravolgere la realtà. Ai politici invece chiediamo di dare piena attuazione ai principi di uguaglianza e non discriminazione che la Costituzione italiana prevede. Il matrimonio tra persone dello stesso sesso dev'essere garantito a tutela di migliaia di persone che sanno, realisticamente, di vivere in un Paese laico e democratico e non in un appendice del Vaticano.  

  

[http://www3.lastampa.it/torino/sezioni/politica/articolo/lstp/136902/](http://www3.lastampa.it/torino/sezioni/politica/articolo/lstp/136902/)