---
title: 'PARLAMENTO EUROPEO: INTERROGAZIONE SU MANCATE AZIONI ANTIOMOFOBIA ITALIA'
date: Fri, 30 Oct 2009 10:05:26 +0000
draft: false
tags: [Comunicati stampa]
---

**CERTI DIRITTI: INTERROGAZIONE AL PARLAMENTO EUROPEO SULLA MANCANZA DI AZIONE DA PARTE DEL GOVERNO  ITALIANO CONTRO L'ONDATA DI ATTACCHI OMOFOBICI E SULL'APPROVAZIONE IN AULA DELL A PREGIUDIZIONALE DI COSTITUZIONALITA’ CHE HA SOSPESO LA VOTAZIONE DEL PROVVEDIMENTO ANTIOMOFOBIA.**

**Bruxelles, 29 ottobre 2009**

Depositata interrogazione da parte delle deputate europee olandesi Jeanine Hennis-Plasschaert e Sophie In't Veld e dei deputati italiani Sonia Alfano e Gianni Vattimo, del gruppo Liberale e Democratico europeoI deputati europei Jeanine Hennis-Plasschaert, Sophie In't Veld, Sonia Alfano e Gianni Vattimo, del gruppo Liberale e Democratico europeo, hanno depositato oggi, accogliendo la proposta del responsabile europeo dell’Associazione Radicale Certi Diritti, e collaboratore del gruppo, Ottavio Marzocchi, un'interrogazione alla Commissione Europea ed al Consiglio Europeo sulla persistente mancanza d'azione da parte delle autorità italiane contro l'ondata di attacchi omofobici in Italia. Partendo dai recenti attacchi omofobici di Ostia e Milano, gli eurodeputati sottolineano l'assenza di iniziative forti di contrasto di tali fenomeni da parte del Governo italiano e ricordano il rigetto della legge anti-omofobia da parte del Parlamento italiano con l'approvazione della pregiudiziale di costituzionalità, che accomuna l'orientamento sessuale - termine scritto e ben definito nei Trattati UE e nella Carta dei diritti fondamentali - alla pedofilia, alla zoofilia, all'incesto, alla necrofilia.

Nel testo si chiede alla Commissione ed al Consiglio di rafforzare le iniziative europee in materia di lotta all'omofobia ed agli atti omofobici attraverso l'introduzione di un'aggravante ed il lancio di campagne di informazione, di richiamare le autorità italiane al significato di  ''orientamento sessuale", definizione che non puo' essere in alcun modo interpretata come indicato nella pregiudiziale di costituzionalità approvata e di chiedere agli Stati membri di stigmatizzare anche pubblicamente gli atti omofobici.

Segue il testo dell'interrogazione (in EN):

Question to the Commission and the Council

by Jeanine HENNIS-PLASSCHAERT, Sophie IN'T VELD, Gianni VATTIMO, Sonia ALFANO

on the Persistent lack of action by Italian authorities against the increase in homophobic attacks

After a wave of violent homophobic acts in Italy and in the absence of any governmental action, a draft law on the creation of an aggravating sanction for homophobic acts - aimed at harmonizing the sanctioning of homophobia with racism and xenophobia - has been rejected by the plenary of the Chamber of Deputies. A motion of unconstitutionality was approved instead, stating that the draft law violates the Italian Constitution since "sexual orientation" can be interpreted to cover incest, paedophilia, zoophilia, sadisms, necrophilia, masochism, etc. These events have been stigmatized by the UN Commissioner for Human Rights, while the Equality Minister promised that she will propose a new draft law with aggravating circumstances for all the grounds for discrimination foreseen by art. 13 TEC and including transphobia. In the meantime, homophobic attacks continue: a 30 years old person has been persecuted, violently beaten up and wounded by a group of right-wing extremists in Ostia (Rome), while another attack took place in Milan. The Prime Minister Berlusconi, the Interior Minister Maroni, the Equality Minister Carfagna never made public interventions to stigmatize homophobic attacks, hereby creating an atmosphere of public condoning of such acts.

How will the Council / the Commission ensure that homophobia and homophobic acts are fought in the EU and its Member States, notably in the framework of the Stockholm Programme and the entry into force of the Lisbon Treaty? Doesn't the Council / the Commission think that "sexual orientation" in the European and Member States' legal order is to be interpreted to cover LGBT orientation and not other acts such as those listed in the Italian motion? Will it make it clear to Italian and other Member States' authorities? Will the Council / the Commission propose to enlarge the Framework Decision on Racism and Xenophobia to cover also sexual orientation? will it ensure that EU funds are used to finance campaigns against discriminations based on sexual orientation? Doesn't the Council / the Commission believe that it is important that governments take action and take a firm and public stance against homophobic attacks?