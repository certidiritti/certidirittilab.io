---
title: 'ALLA CAMERA VOTO CONTRO PDL ANTIOMOFOBIA: VATICANO BRINDA, GAY MUOIONO'
date: Tue, 13 Oct 2009 15:52:01 +0000
draft: false
tags: [Comunicati stampa]
---

**LEGGE ANTIOMOFOBIA: QUANTO AVVENUTO OGGI IN AULA E’ DIMOSTRAZIONE DI IPOCRISIA. MENTRE IL VATICANO BRINDA, I MORTI E I FERITI LGBT ENTRANO NELL'OBLIO. GRAZIE AL LAVORO DELLA CONCIA, LAVORIAMO INSIEME PER LEGGI DI TOLLERANZA E GIUSTIZIA.**

**Dichiarazione di Rita Bernardini, deputata radicale pd e Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

“Quanto avvenuto oggi in aula sul provvedimento contro l’omofobia è la dimostrazione, ove ve ne fosse ancora bisogno, che chiedere semplici aggravanti di reati senza proporre nulla per battere il clima di intolleranza e di violenza, rischia di farci rimanere tutti con un pugno di mosche.

Mentre oggi in Vaticano si brinda, i morti e i feriti per causa di violenza omofobica e transfobica entrano sempre più nell’oblio. Per l’ennesima volta ha vinto l’ipocrisia e la falsità, tipici di questo paese che ignora il pregiudizio e l’odio sempre più diffusi contro le persone più doboli e che è incapace di combatterli con adeguate e specifiche campagne educative e informative, sul piano sociale e culturale.

Ringraziamo Paola Concia per l’enorme lavoro che ha svolto in qualità di relatrice del provvedimento antiomofobia; dobbiamo continuare a lavorare insieme per migliorare questo paese, per promuovere leggi di tolleranza e giustizia, per proteggere le persone lesbiche, gay, bisessuali, transessuali,transgender, esposte al pregiudizio, all’odio e ai diritti negati”.