---
title: 'I diritti umani nella Federazione russa di Putin: Libertà di espressione, minoranze, diritti'
date: Sun, 17 Nov 2013 15:18:28 +0000
draft: false
tags: [Russia]
---

Venerdì 22 novembre 2013 dalle 17.00 alle 20.00

Università degli Studi di Trieste Aula Bachelet

A pochi giorni dall'incontro bilaterale Italia-Russia, l'Associazione Radicale Certi Diritti, Famiglie Arcobaleno e il Circolo Arcobaleno Arcigay, in collaborazione con il Comitato per le Pari Opportunità dell'Università degli Studi di Trieste organizzano:

**I diritti umani nella Federazione russa di Putin: Libertà di espressione, minoranze, diritti**

Come aiutare i cittadini e le cittadine russe nelle loro richieste di difesa e promozione dei propri diritti umani? Quale è il ruolo delle organizzazioni internazionali? Che tipo di influenza possono avere le Convenizoni e i trattati internazionali? Quali tipo di interlocuzione deve essere attivata, o si deve tentare di attivare, con il sistema politico e istituzionale russo?

Queste sono le domande al centro di questo incontro organizzato in previsione del bilaterale Russia-Italia e centrato su tre ambiti sui quali si concentra l'attenzioen internazionale, ovvero i diritti delle persone lgbti, la libertà di espressione e i diritti delle comunità migranti.

Attraverso le dirette testimonianze di coloro che seguono da vicino, direttamete in Russia, sia casi che sono sui media internazionali sia casi che invece non vanno sulle prime pagine dei giornali, si intende sollevare il tema delal difesa e della promozione dei diritti umani in Russia, con particolare riferimento al valore che hanno, e possono avere, i trattati internazionali.

**PROGRAMMA:**

**Saluti delle Istituzioni e degli organizzatori:**

Yuri Guaiana, Segretario dell'Associazione Radicale Certi Diritti

Andreaa Tamaro, Presidente Circolo Arcobaleno Arcigay

Fabiana Martini, Vicesindaco di Trieste

**Introduzione:**

Giuliano Prandini, Amnesty International Italia

Paolo Atzori, Funzionario al Parlamento Europeo presso i non iscritti

**Testimonianze:**

Angelo Pezzana (fondatore dle Fuori! che manifestò per primo a Mosca nel 1977)

**Proiezione di un estratto del documentario "INVANO MI ODIANO - racconto sui cristiani LGBT"** di Yulia Matsiy

**Intervengono**

Yulia Matsiy, VARCO - REFO

Björn van Roozendaal, ILGA-Europe

**Tavola rotonda**

**"La libertà di espressione e il controllo dei media"**

**Proiezione di "The Word & the Bullet"**. Un film di Svetlana Svistunova. Sottotitolato in inglese.

**Tavola rotonda finale con:**

Teresa Tonchia , docente di diritti umani e filosofia politica Università di Trieste

Antonio Stango, PRNTT

**Conclusioni di Enzo Cucco, Associazione Radicale Certi Diritti**

**E' online anche l'[evento Facebook della Conferenza](https://www.facebook.com/events/454895354619615/?fref=ts).**