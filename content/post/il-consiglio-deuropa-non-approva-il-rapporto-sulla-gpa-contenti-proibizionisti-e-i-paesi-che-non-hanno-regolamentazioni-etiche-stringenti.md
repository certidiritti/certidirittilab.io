---
title: 'IL CONSIGLIO D’EUROPA NON APPROVA IL RAPPORTO SULLA GPA: CONTENTI PROIBIZIONISTI E I PAESI CHE NON HANNO REGOLAMENTAZIONI ETICHE STRINGENTI.'
date: Wed, 12 Oct 2016 09:38:33 +0000
draft: false
tags: [Gestazione per altri]
---

[![1452617825_vanessa2-600x335](http://www.certidiritti.org/wp-content/uploads/2016/09/1452617825_vanessa2-600x335-300x167.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/09/1452617825_vanessa2-600x335.jpg)L’Assemblea Parlamentare del Consiglio d’Europa ha bocciato il timidissimo rapporto sulla gestazione per altri che chiedeva l’introduzione per la difesa dei diritti dei minori nati grazie alla gpa.

I proibizionisti nostrani ed europei preferiscono non guardare alle questioni in gioco e nascondersi dietro la rassicurante ideologia dei divieti tout court per la felicità di quei Paesi, come l’Ucraina e la Federazione Russa, che prosperano sull'assenza di regole a tutela delle donne e di tutte le parti coinvolte.

Non a caso la maggioranza dei rappresentanti ucraini hanno votato contro il rapporto: i divieti negli altri Paesi favorisce il turismo riproduttivo, anche verso l’Ucraina.

I gruppi che hanno votato in maggioranza a favore sono stati quelli della Sinistra Europea Unita (a favore all’unanimità), l’Alleanza dei Liberali e dei democratici e i socialisti, ma in Italia il proibizionismo ha sfondato le tradizionali linee di divisione politica con buona pace del principio di autodeterminazione della donna e della scelte riproduttive.

L’unica soluzione per rispettare i diritti umani di tutte le parti coinvolte è la regolamentazione della gestazione per altri secondo principi etici sintetizzati in questo [documento](http://www.certidiritti.org/wp-content/uploads/2016/09/323161648-MHB-Ethical-Surrogacy-Italian-Statement-of-Principles.pdf), non la sua proibizione.

_Roma, 12 ottobre 2016 _