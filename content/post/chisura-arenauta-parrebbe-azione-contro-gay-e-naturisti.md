---
title: 'CHISURA ARENAUTA PARREBBE AZIONE CONTRO GAY E NATURISTI'
date: Sun, 01 Aug 2010 09:30:30 +0000
draft: false
tags: [arenauta, Comunicati stampa, GAY, naturismo, nudismo, spiaggia]
---

  
SPIAGGIA ARENAUTA DI GAETA SOTTO ASSEDIO: AZIONE PER COLPIRE LA COMUNITA’ GAY E LE PERSONE CHE PRATICANO NATURISMO? INFORMEREMO LA PROCURA DELLA REPUBBLICA AFFINCHE’ SI VERIFICHI LA REGOLARITA’ DI QUANTO STA AVVENENDO.

_**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti e Presidente del Comitato per la difesa e la tutela della spiaggia dell’Arenauta:**_

“Quanto sta avvenendo in questi giorni nella spiaggia dell’Arenauta di Gaeta (Latina), ha dell’incredibile. La recinzione di alcune centinaia di metri di parte della spiaggia, in particolare quella non soggetta a speculazioni edilizie e considerata ancora ‘libera’, fa venire alcuni sospetti. La ragione addotta che motiverebbe questa operazione di vero e proprio 'ingabbiamento' dell'area frequentata da decenni dalla comunità gay, è che vi sarebbero pericoli di frane e crolli di cui francamente le migliaia e migliaia di persone che da sempre la frequentano non si erano mai accorte.

Non vorremmo che tutto questa operazione avesse come obiettivo quella di spaventare le migliaia di persone che da decenni frequentano quella parte di spiaggia, che verrebbero ormai letteralmente ingabbiati come animali in un recinto che sta per essere completato in questi giorni.

Sono in corso alcune iniziative spontanee da parte dei cittadini contro questa azione che non riesce a trovare spiegazioni.

Già alcuni anni fa vi fu la brillante idea di far presidiare nel periodo estivo quella parte di spiaggia da agenti privati armati con l’obiettivo di spaventare le persone che praticavano il naturismo. Grazie all’intervento di decine di parlamentari la società di agenti privati fu richiamata dalla Prefettura di Latina e fu revocata loro la licenza con un richiamo alle autorità comunali che attivarono una apposita convenzione economica con le guardie armate. Il tutto sanzionato dal Governo che intervenne con forte decisione contro questo vero e proprio sopruso.

Nelle prossime ore invieremo una segnalazione alla Procura della Repubblica di Latina affinchè si possa verificare se esiste effettivamente un pericolo di frane in quella zona della spiaggia e se l’intervento che si sta facendo corrisponde ad una vera esigenza di messa in sicurezza dell’area".

**Ufficio Stampa Associazione Certi Diritti: 06-68979250**