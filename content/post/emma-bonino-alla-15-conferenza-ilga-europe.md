---
title: 'Emma Bonino alla 15° Conferenza Ilga Europe'
date: Wed, 02 Nov 2011 10:29:00 +0000
draft: false
tags: [Movimento LGBTI]
---

Venerdì 28 ottobre la vice presidente del Senato della Repubblica ha partecipato ai lavori di Ilga Europe. Presenti anche Paola Concia, Angelo Pezzana e Jeff Dudgeon.

comunicato stampa di Ilga Europe Torino 2011

Durante i lavori del pomeriggio di venerdi 28 ottobre 2011 della XV Conferenza ILGA-Europe in corso a Torino è intervenuta Emma Bonino, vicepresidente del Senato, in un panel dedicato al tema della Conferenza (Diritti umani e valori tradizionali) con la presenza di George Tugushi, difensore civico della Georgia.

Emma Bonino ha innanzitutto sottolineato l'importanza del messaggio del Presidente della Repubblica, soprattutto nel nostro Paese dove non è usuale che le massime cariche dello Stato esprimano il loro sostegno ad iniziative anche politiche del movimento lgbt. Tuttavia non può passare inosservato che al massimo degli onori concessi fa eco lo zero dei diritti riconosciuti dal Parlamento, anche questa è una delle contraddizioni italiane, paese in cui vi sono solo licenze private e pubbliche proibizioni.

Per Emma Bonino i diritti umani sono sempre prioritari rispetto ai valori tradizionali, che invece spesso sono solo retaggio del passato.

La vicepresidente del Senato ha sottolineato il pericolo che deriva dall'aumento delle manifestazioni di razzismo in Europa. Razzismo contro i Rom e gli immigrati, ma che facilmente può diffondersi anche contro altre cosiddette diversità o minoranze.  
In particolare segnala il caso ungherese come esempio del cattivo funzionamento del'Unione Europea, incapace di sanzionare chi ne fa parte se compie atti di limitazione della libertà individuale e collettiva chiaramente in contrasto con i Trattati.

Emma Bonino si è rivolta direttamente ad ILGA-Europe suggerendo di cercare alleanze con tutte le altre minoranze, di non temere di perdere la propria identità o la priorità dei propri obiettivi, perchè solo così si adotta un approccio universale e più credibile.

*Foto di Sandro Mattioli