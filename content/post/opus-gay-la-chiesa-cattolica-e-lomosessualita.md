---
title: 'OPUS GAY. La Chiesa cattolica e l''omosessualità'
date: Mon, 13 Dec 2010 14:27:16 +0000
draft: false
tags: [Comunicati stampa]
---

[![opusgay](http://www.certidiritti.org/wp-content/uploads/2010/12/opusgay.jpg)giovedì 16 dicembre · si è svolta a Roma la presentazione del libro di Iliaria Donatio](eventi/details/59-opus-gay-la-chiesa-cattolica-e-lomosessualita.html)

Al seguente link l'audio-video di Radio Radicale

[http://www.radioradicale.it/scheda/317493/opus-gay-la-chiesa-cattolica-e-l-omosessualita](http://www.radioradicale.it/scheda/317493/opus-gay-la-chiesa-cattolica-e-l-omosessualita)

di Ilaria Donatio - Newton Compton editori  
  
UN PERCORSO TRA LE STORIE INTENSE E CONTRADDITTORIE  
...DI RELIGIOSI E FEDELI GAY E LESBICHE

"Entrambi – valori e vissuto – si sono, in parte, relativizzati: una vera liberazione. Una conquista. (…) Ma qui non ci sono tesi da dimostrare. Solo alcuni fatti che ho provato a raccontare, e la proposta di altrettanti percorsi di lettura".  
  
"È l’etica della differenza. Che, se solo diventasse un bene comune – proprio  
come l’acqua che beviamo, l’aria che respiriamo, la terra che calpestiamo – avrebbe una portata rivoluzionaria sulle vite di tutti. E capovolgerebbe il nostro punto di vista di persone integrate, riconosciute, privilegiate, tutelate. Eterosessuali, anche".  
  
Ne hanno discusso con l'autrice:  
  
Gianni Gennari (Rosso Malpelo), editorialista de L’Avvenire  
  
Sergio Rovasio, segretario dell’associazione radicale Certi Diritti  
  
Natascia Esposito, gruppo gay e lesbiche credenti Nuova Proposta  
  
Ha presieduto Alessandro Litta Modignani, Radicali Italiani

ILARIA DONATIO è nata a Lecce e vive a Roma, dove lavora come giornalista freelance. E’ stata nella redazione on-line di «Reuters Italia», di «Formiche» e ha collaborato con la web-tv dei Radicali. Oggi scrive per «Micromega», «L’Unità» e altre testate, occupandosi in particolare di cultura gay e società civile.