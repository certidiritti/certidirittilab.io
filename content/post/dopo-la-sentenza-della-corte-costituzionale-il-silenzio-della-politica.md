---
title: 'DOPO LA SENTENZA DELLA CORTE COSTITUZIONALE IL SILENZIO DELLA POLITICA'
date: Wed, 21 Jul 2010 07:49:11 +0000
draft: false
tags: [Affermazione Civile, CORTE COSTITUZIONALE, matrimonio gay, Sentenza, Senza categoria]
---

Un articolo di **Marilisa D'Amico**, Avvocata, Ordinario di Diritto costituzionale all'Università Statale di Milano, membro del collegio di difesa delle coppie e dell'Associazione Radicale Certi Diritti presso la Corte Costituzionale, per _[Affari Italiani](http://www.affaritaliani.it)_.

* * *

La sentenza n. 138 del 2010 della Corte costituzionale italiana costituisce un passo determinante, anche se non definitivo, in tema di riconoscimento dei diritti delle coppie omosessuali e, anche, del fondamentale “diritto al matrimonio”.  
Di questa decisione sta circolando una lettura riduttiva che sosterrebbe, a mio avviso sbagliando, che secondo la Corte il “matrimonio” fra persone dello stesso sesso potrebbe essere raggiunto soltanto con la revisione dell’art. 29 Cost. il quale non si presterebbe ad una lettura “evolutiva”.

Questa interpretazione della sentenza n. 138 del 2010 non è accettabile, per tre ordini di motivi:

**1.** nell’impianto della decisione la lettura degli artt. 3 e 29 Cost., basata sull’assunto che l’originaria previsione dell’art. 29 Cost., pur conoscendo il fenomeno dell’omosessualità, ha inteso disciplinare soltanto il matrimonio fra persone eterosessuali, costituisce un punto molto debole del ragionamento. Sembra simile a quello che la Corte statuì, nella sentenza n. 421 del 1995, bocciando le cd. quote rosa; su quel principio sbagliato, la Corte è ritornata nella sentenza n. 49 del 2003, che costituisce una vera overruling. Similmente, nulla impedirebbe alla Corte, in futuro, di riconoscere la debolezza della sua interpretazione soltanto “storica” dell’art. 29 Cost.  
Con questo metro, allora, si sarebbe potuta dichiarare costituzionalmente illegittima anche la previsione del divorzio.

**2.** La stessa Corte costituzionale, basandosi sull’art. 2 Cost., e sull’art. 117 Cost. (reso più forte dalla recente sentenza della Corte costituzionale austriaca), ha riconosciuto non solo il valore costituzionale della “coppia omosessuale”, ma ha definito come “necessario” l’intervento del legislatore per una disciplina organica e si è riservata il diritto ad intervenire a correzione di tutte le specifiche violazioni di specifici diritti.  
Questa decisione costituisce un monito al legislatore molto forte: non si parla di facoltà, ma di necessità di intervento.

**3.** Il segnale, da parte della Corte, è chiarissimo: di fronte al vuoto normativo, che pone l’Italia in una situazione del tutto unica rispetto agli altri Paesi europei (Portogallo, Austria, … ma per uscire dall’Europa, anche Argentina), la Corte opportunamente rinvia al ruolo del legislatore e, dunque, della politica nel dotarsi di una disciplina organica. Sarà comunque il legislatore, nella sua discrezionalità, a decidere in ordine ad una piena equiparazione, anche nel nome “matrimonio” delle unioni omosessuali rispetto a quelle eterosessuali.

Il terreno, però, è oggetto di profonda evoluzione, come testimonia anche la recentissima decisione della Corte europea ([Schalk e Kopf v. Austria](http://www.radioradicale.it/scheda/307881/matrimoni-gay-marilisa-damico-commenta-la-sentenza-della-corte-europea-dei-diritti-delluomo), del 24 giugno 2010).

La Corte non si limita a rinviare genericamente alla politica, ma si ritaglia uno spazio di intervento dinnanzi a situazioni specifiche: questo rende il monito, a mio avviso, ancora più cogente.

In conclusione, chi cerca ancor oggi, anche in buona fede, letture ulteriori e diverse della sentenza, non fa altro che perdere tempo prezioso e ignora l’indicazione chiara e inequivocabile del giudice costituzionale: la necessità, non più procrastinabile, di una legge.

La “via giudiziaria”, nel riconoscimento e nella tutela dei diritti, non può e non deve sostituirsi alla “via politica”, ma soltanto offrire sostegno e aiuto quando quest’ultima si dimostri lenta, insufficiente o inadeguata, come sempre più spesso in Italia succede.