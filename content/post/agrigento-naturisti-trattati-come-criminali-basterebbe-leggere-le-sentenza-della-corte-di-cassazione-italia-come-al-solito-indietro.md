---
title: 'Agrigento: naturisti trattati come criminali, basterebbe leggere le sentenza della Corte di Cassazione. Italia come al solito indietro'
date: Tue, 07 Aug 2012 11:59:36 +0000
draft: false
tags: [Politica]
---

Chi pratica naturismo non è perseguibile. All'estero si incrementa il turismo, in Italia si criminalizza.

Roma, 7 agosto 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

La notizia che almeno venti persone che praticavano naturismo in una spiaggia di Torre Salsa a Siculiana, provincia di Agrigento, sono state denunciate per atti osceni ha dell’incredibile. Mentre la Corte di Cassazione con le sentenze 1765 e 3557 del 2000  ha stabilito che chi pratica il naturismo non è perseguibile, e mentre in Parlamento giacciono da diverse Legislature proposte di legge che ne chiedono la regolamentazione, assistiamo ogni anno ad azioni di accanimento inutili, costose e di vera e propria persecuzione che considera criminali coloro che preferiscono prendere il sole integrale.

Come già ricordato da una sentenza della Corte di Cassazione del 1988,  non si può applicare a chi pratica il naturismo l'art. 726 del c. p. (atti contrari alla pubblica decenza) perché il naturista "sta" nudo e trovandosi in uno "stato" non compie alcun "atto" ed è agli "atti" che l'articolo 726 si riferisce. Accusare quelle persone di un qualcosa che non è nemmeno quindi previsto dalle leggi è del tutto inutile e la Corte di Cassazione ha sempre confermato questo orientamento.

Il Parlamento si muova e calendarizzi le diverse proposte di legge su questo tema, a cominciare dal Disegno di Legge S.1265 sulla legalizzazione della pratica del naturismo presentato dai Senatori Radicali eletti nel Pd Donatella Poretti e Marco Perduca.  
Mentre all’estero il naturismo è regolamentato anche per incrementare il turismo, in Italia, come al solito assistiamo a queste incredibili azioni.