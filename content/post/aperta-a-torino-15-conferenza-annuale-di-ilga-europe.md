---
title: 'Aperta a Torino 15° conferenza annuale di Ilga-Europe. Tra i partecipanti Certi Diritti, Emma Bonino e Angelo Pezzana'
date: Thu, 27 Oct 2011 15:14:04 +0000
draft: false
tags: [Movimento LGBTI]
---

Dal 27 al 30 ottobre 340 delegati di 40 Paesi per la conferenza dal titolo “Diritti umani e valori tradizionali: scontro o dialogo?”. Ilga Europe è un'organizzazione di cui fanno parte le più importanti associazioni Lgbt europee, Certi Diritti è iscritta fin dalla sua fondazione. A Torino sarà presente anche Anna Grotz, la transessuale polacca appena eletta deputata.

Roma-Torino, 27 ottobre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti:

Si è aperta oggi a Torino la 15° conferenza annuale di ILGA Europe, organizzazione di cui fanno parte le più importanti Associazioni Lgbt europee. L’evento si svolge in un anno ricco di anniversari che ha visto la Città di Torino protagonista:  il 150° anniversario dell’unità d’Italia con  Torino capitale, la nascita  a Torino,  40 anni fa del “Fuori!”, il primo movimento gay italiano. A Torre Pellice, in provincia di Torino, si svolse 30 anni fa la prima conferenza ILGA World, ospitata dai Valdesi  e organizzata dal “Fuori!”. A Torino ricorre anche il decimo anniversario dell’istituzione del servizio LGBT del Comune, il primo e più longevo esempio italiano di ufficio che permette l’implementazione di politiche antidiscriminatorie a livello locale.

La conferenza è stata organizzata dal Comitato ILGA Europe Torino 2011 di cui fanno parte varie associazioni locali, tra le quali il Coordinamento Torino Pride, e nazionali, tra queste anche l’Associazione Radicale Certi Diritti che partecipa con una delegazione composta da Enzo Cucco, Gabriele Murgia e Yuri Guaiana.

Il titolo della conferenza è: “Diritti umani e valori tradizionali: scontro o dialogo?”. Si articola in quattro giornate di lavoro, dal 27 al 30 ottobre, e vi partecipano 343 delegati da più di 40 paesi d’Europa e varie personalità del panorama politico europeo.  Si confronteranno sui diritti delle persone LGBT in Europa e sulla lotta alle discriminazioni.

Tra le personalità partecipano Emma Bonino, vice-presidente del senato; Angelo Pezzana, fondatore del FUORI!, Robert Biedron, primo parlamentare polacco dichiaratamente gay, Anna Grodz la neoeletta parlamentare transessuale in Polonia, Jeffrey Dudgeon, che per primo ricorse nel 1981 alla  Corte Europea dei Diritti dell’Uomo a Strasburgo la cui sentenza, dopo 6 anni di giudizio, portò alla decriminalizzazione dell’omosessualità in Irlanda, Joke Swiebel e fondatrice, nel 1999, dell’Intergruppo per i diritti LGBT al Parlamento Europeo.

I lavori si articoleranno in sedute plenarie e workshop dedicati, tra l’altro, alle strategic litigations, alla lotta alla violenza omofoba, a come interagire con la Commissione europea, alle politiche di integrazione nell’ambito del lavoro, alla condivisione di buone prassi, al bullismo omofobo, a servizi sanitari inclusivi, al libero movimento dei diritti, all’HIV,all’intersessualità. E’ da segnalare poi un workshop specifico dedicato all’Italia, intitolato: “Misoginia e omofobia nell’Italia di Berlusconi: l’estetica femminile e il corpo lesbico.

L’Associazione Radicale Certi Diritti contribuirà, con la sua esperienza, soprattutto ai lavori sulle strategic litigations, le buone prassi e il libero movimento dei diritti,  presentando lo stato della propria attività, anche in previsione dei seminari più operativi che si terranno a Strasburgo in novembre ai quali parteciperà una propria delegazione.