---
title: 'Yuri Guaiana confermato nel board di ILGA-Europe'
date: Sat, 04 Nov 2017 16:11:30 +0000
draft: false
tags: [Movimento LGBTI]
---

![23172679_10155579388700973_194786361345570886_n](http://www.certidiritti.org/wp-content/uploads/2017/11/23172679_10155579388700973_194786361345570886_n-300x300.jpg) Siamo felici di annunciare che Yuri Guaiana, nostro membro di direttivo e già segretario di Certi Diritti, è stato rieletto nel board di Ilga-Europe col sostegno di tutta la delegazione italiana alla conferenza annuale di Varsavia. Buon lavoro, Yuri!