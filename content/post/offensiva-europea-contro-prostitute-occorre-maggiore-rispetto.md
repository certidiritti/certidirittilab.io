---
title: 'Offensiva europea contro prostitute. Occorre maggiore rispetto'
date: Wed, 29 Jun 2011 13:56:15 +0000
draft: false
tags: [Lavoro sessuale]
---

**Campagna dell'integralismo ideologico contro la prostituzione in Europa, promossa dall'European Women, inutile e dannosa. Occorre maggiore rispetto per la dignità delle persone che si prostituiscono. Lettere di protesta al PE.**

**Roma, 29 giugno 2011**

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Da alcuni giorni è stata lanciata dalla European Women una campagna europea contro la prostituzione che nega il diritto all’autodeterminazione e alla libera scelta delle persone che scelgono liberamente  questa professione.  
L’European Women, che si ispira ad una visione ideologica di stampo fondamentalista, pretende di imporre la sua visione di una società “libera dalla prostituzione” considerando di fatto tutte le prostitute “persone che subiscono una violenza perché pagate per quel servizio”. Forse è bene ricordare a questa Associazione che la libertà di scegliere cosa fare del proprio corpo, nel pieno della propria responsabilità e autodeterminazione, è un basilare diritto umano e nessuno si può arrogare il diritto di decidere per gli altri cosa è buono oppure no. Già in Italia siamo abituati ad un certo modello di fondamentalismo di stampo pseudo-religioso che vuole imporre a tutti una visione di stampO clericale  e di negazione di diritti a chi non ne ha.  
Altro discorso merita invece l’aspetto criminale della tratta e della schivitù di cui molte prostitute sono vittime.  Il Governo italiano ha tagliato un anno fa i fondi per dare protezione e aiuto a chi decide di denunciare questa violenza criminale. Lo stesso Numero Verde antitratta è diventato un numero unico e non più di tipo territoriale, facendo perdere così l’efficacia riguardo interventi immediati e mirati.  
Noi siamo al fianco delle Sex Worker perché siamo consapevoli che questa professione, se fosse regolamentata come lo è già in molti paesi europei, consentirebbe a tantissime prostitute di uscire dalla clandestinità e di avere parità e diritti come tutte le persone lavoratrici. E’ evidente che vi sarebbero meno violenze, illegalità e soprusi che oggi non sono nemmeno registrati perché il fenomeno è sempre più clandestinizzato dalle fallimentari e ridicole Ordinanze dei Comuni.  
E’ importante mobilitarsi contro l’offensiva dell’European Women scrivendo una lettera di protesta al proprio parlamentare europeo italiano di riferimento e al Responsabile del Programma Progresso dell’Unione Europea.