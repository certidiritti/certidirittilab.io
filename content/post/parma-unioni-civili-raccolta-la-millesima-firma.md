---
title: 'PARMA. UNIONI CIVILI, RACCOLTA LA MILLESIMA FIRMA'
date: Mon, 23 Jun 2008 07:41:47 +0000
draft: false
tags: [Comunicati stampa]
---

di Giulia Viviani  
  
II Se l'istituzione del registro per le unioni civili a Parma divide amministrazione provinciale e comunale, i cittadini non hanno invece molti dubbi. La petizione promossa dall'associazione radicale «Certi diritti» ha raggiunto ieri l'obiettivo delle mille firme,  raccolte in poco più di un mese. «Non c'è stato bisogno di fare opera di convincimento per ottenere le firme - spiega Luca Marola di «Certi diritti» - le persone sanno benissimo cosa sono le unioni civili. La società spesso è più avanti della politica».  

La raccolta è iniziata il 17 maggio, giornata mondiale contro l'omofobia, e terminerà il 28 giugno in concomitanza con il gay pride. Tra chi vive un unione di fatto c'è anche l'assessore provinciale ai Servizi Sociali, Tiziana Mozzoni: «Istituire il registro è una presa di coscienza della realtà, talvolta la politica è incapace di interpretare i bisogni dei cittadini». Ma con la consegna delle mille firme, il consiglio comunale sarà obbligato a discutere della questione. Alcuni assessori comunali hanno infatti dichiarato la loro adesione alla richiesta promossa dalla petizione. Nessun dubbio invece in Provincia, perché ieri in via Mazzini c'era anche l'assessore allo Sport, Emanuele Conte. «Il raggiungimento della millesima firma - dice - mette in risalto la realtà di Parma. Il riconoscimento delle coppie di fatto, attraverso l'istituzione del registro, è un diritto che può migliorare la vita di molte persone. Senza differenza di sesso».  

  

![Parma2.jpg](http://www.certidiritti.org/wp-content/uploads/2008/06/cimg2218.jpg "Parma2.jpg")  
![parma3.jpg](http://www.certidiritti.org/wp-content/uploads/2008/06/parma3.jpg "parma3.jpg")  
  
  

  

  
  
E' possibile scaricare e visionare le foto [in questo link](index.php?option=com_docman&task=cat_view&gid=18&Itemid=79)