---
title: 'QUESTORE DI ROMA FORMALIZZA NO A MARCIA ANTICLERICALE: NOI CI SAREMO'
date: Wed, 16 Sep 2009 15:02:25 +0000
draft: false
tags: [Comunicati stampa]
---

**ANTICLERICALE.NET E CERTI DIRITTI: IL QUESTORE DI ROMA HA COMUNICATO CON ATTO FORMALE SCRITTO IL DIVIETO ALLA MARCIA DEL 19 SETTEMBRE RICORREREMO GLI STRUMENTI PREVISTI DALLA LEGGE, CON RICORSO STRAORDINARIO AL CAPO DELLO STATO. APPUNTAMENTO A TUTTI I CITTADINI A PORTA PIA, SABATO 19, A PARTIRE DALLE 14. DOMANI ALLA CAMERA QUESTION-TIME SULL’INTERROGAZIONE URGENTE PRESENTATA DAI DEPUTATI RADICALI.**

**_Comunicato delle associazioni radicali Anticlericale.net e Certi Diritti_**

Il Questore di Roma, Giuseppe Caruso, ci ha comunicato nella serata di ieri, con atto formale scritto, il divieto a tenere la marcia anticlericale convocata per sabato 19 settembre, che si sarebbe dovuta svolgere da Porta Pia a piazza Pio XII, davanti al Vaticano. Sono stati autorizzati solo i comizi, che si terranno a Porta Pia a partire dalle ore 14.

La Questura ha altresì rifiutato di prendere atto della disponibilità delle associazioni radicali convocatici della manifestazione a svolgere la marcia “in fila indiana, camminando sui marciapiedi, rispettando i semafori”.

Consideriamo questa decisione gravemente lesiva dei diritti costituzionali dei cittadini italiani, e preannunciamo sin d’ora che ci opporremo con tutti gli strumenti previsti dalle leggi vigenti, compreso il ricorso straordinario al Capo dello Stato.

Confermiamo altresì che sabato 19 settembre, a partire dalle ore 14, saremo a Porta Pia per commemorare la data storica del 20 settembre 1870, quando la città di Roma fu finalmente liberata da quel potere temporale dei Papi che il fascismo prima e la partitocrazia poi hanno ormai pienamente restaurato.

Facciamo appello a tutte le associazioni, i cittadini, le personalità che hanno a cuore la laicità dello Stato ed i principi dello Stato di diritto a partecipare all’iniziativa con bandiere, striscioni, cartelli, fermo restando che “in qualche modo” commemoreremo ugualmente gli eventi simbolicamente identificati nei luoghi che la marcia avrebbe dovuto toccare.

Maurizio Turco, Carlo Pontesilli, Michele De Lucia

(Presidente, Segretario e Tesoriere di Anticlericale.net)

Sergio Rovasio e Clara Comelli

(Segretario e Presidente di Certi Diritti)