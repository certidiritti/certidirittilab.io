---
title: 'SERVIZI SUL SESSO IN CAMBIO DI DIRITTI: CAMPAGNA DELLE SEX WORKERS'
date: Fri, 02 Apr 2010 05:03:13 +0000
draft: false
tags: [Comunicati stampa]
---

**SERVIZI SUL SESSO IN CAMBIO DI DIRITTI: CAMPAGNA DELLE SEX WORKERS**  
  
Le/I sex workers annunciano una provocatoria campagna per sensibilizzare sui Diritti negati.   
  
Da domani fina al 10 aprile attraverso il sito [www.lucciole.org](http://www.lucciole.org/)  partendo dal sito delle lucciole on line, si dipanerà un percorso virtuale per sensibilizzare la società sulla mancanza di rispetto e di protezione sociale di cui soffre la categoria e la sua utenza.

Durerà per una settimana l’offerta della campagna “io ci sto…e tu?” che prevede la distribuzione di buoni –invito ai sostenitori per consumare incontri con le attiviste/i del Comitato per i Diritti Civili delle Prostitute.   
  
Covre dice “E’ la prima iniziativa del genere in Italia. La situazione è grave e questa campagna che ci proponiamo di fare è strategica sia nel metodo che nel merito”.  
  
Tutti invitati quindi a tener d’occhio il nostro sito e seguire le “strade” giuste a scanso di equivoci e anche a scanso di sanzioni.   
  

Oggetto: 2° Comunicato Stampa Campagna

Dopo il lancio della cartolina ora arrivano i buoni-invito  
  
La campagna lanciata dal Comitato per i Diritti Civili delle Prostitute il 1° aprile, entra nella seconda fase.   
Durerà per quasi una settimana, fino al 10 Aprile l’offerta della campagna “io ci sto…e tu?” che prevede la distribuzione di buoni - invito ai sostenitori per incontri con le attiviste/i del Comitato per i Diritti Civili delle Prostitute.   
Dice Covre “la nostra società è permeata dalla doppia morale, tanti cercano i servizi dai/lle sex workers ma si continua a fingere che questa sia una degenerazione.” La stigmatizzazione e la criminalizzazione del lavoro sessuale, purtroppo producono abusi e violenze contro chi esercita, ed è una situazione inaccettabile. Vogliamo con questa Campagna sollecitare i molti uomini italiani, ad opporsi a tali crimini e ingiustizie, sia a livello nazionale che locale.   
Da oggi i buoni-invito per usufruire delle prestazioni gratuite sono scaricabili in Internet attraverso il sito delle lucciole on line. [http://www.facebook.com/l/7900e;www.lucciole.org](http://www.facebook.com/l/7900e;www.lucciole.org)  
Le attiviste si stanno preparando ad accogliere le richieste e da martedì sarà possibile usufruire dei loro servizi. Chiunque desideri sostenerci e contattare una sex worker potrà telefonare ai numeri messi a disposizione dalle attiviste.  
I buoni-invito sono a disposizione sia di uomini che di donne, senza discriminazione di genere.

![io_ci_sto_big](http://www.certidiritti.org/wp-content/uploads/2010/04/io_ci_sto_big.jpg)