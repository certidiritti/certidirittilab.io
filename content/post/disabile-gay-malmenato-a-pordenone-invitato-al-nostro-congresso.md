---
title: 'DISABILE GAY MALMENATO A PORDENONE: INVITATO AL NOSTRO CONGRESSO'
date: Thu, 12 Mar 2009 16:36:35 +0000
draft: false
tags: [Comunicati stampa]
---

DISABILE GRAVE MALMENATO PERCHE’ GAY: INVITATO AL CONGRESSO DI CERTI DIRITTI.  
LA VIOLENZA ALIMENTATA DAL PREGIUDIZIO E DA CULTURA OMOFOBICA.  
   
Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti: “Quanto accaduto a Pordenone contro un ragazzo disabile perché omosessuale, denota un livello drammatico di ignoranza, pregiudizio e odio purtroppo molto diffusi. Una certa classe politica e certamente quella clericale hanno gravi responsabilità in questo perché continuamente considerano l’omosessualità un nemico, un fatto contronatura, alimentando così forme incredibili di violenza verso il prossimo, verso il più debole.  Questo fondamentalismo politico e religioso alimenta una cultura di offesa verso le persone più deboli che è allarmante. Prova ne è il fatto che a Pordenone nessuno dei passanti, mentre il disabile veniva malmenato da tre bulli, è interventuo per difenderlo, aiutarlo, proteggerlo.  
Abbiamo invitato il disabile aggredito al 2° Congresso dell’Associazione Radicale Certi Diritti che terremo a Bologna sabato 14 marzo per tutta la giornata. Ci auguriamo che accolga l’invito e venga da noi per essere aiutato, ascoltato e considerato da tutti noi una persona normale”.