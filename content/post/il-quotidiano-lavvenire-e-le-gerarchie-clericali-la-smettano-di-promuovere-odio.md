---
title: 'IL QUOTIDIANO L''AVVENIRE E LE GERARCHIE CLERICALI LA SMETTANO DI PROMUOVERE ODIO'
date: Fri, 09 Apr 2010 13:08:40 +0000
draft: false
tags: [Comunicati stampa]
---

I**L QUOTIDIANO DELLA CEI "L'AVVENIRE" ATTACCA DURAMENTE IL CONSIGLIO D’EUROPA E I GOVERNI EUROPEI PERCHE’ DIFENDONO I DIRITTI UMANI DELLE PERSONE OMOSESSUALI. ALTRO CHE MESSAGGIO CRISTIANO, SIAMO ALLA PROMOZIONE DELL' ODIO.**

**Roma, 9 aprile 2010**

**Dichiarazione di Sergio Rovasio ed Ottavio Marzocchi, Segretario e responsabile delle questioni europee dell'Associazione Radicale Certi Diritti:**

"Oggi abbiamo assistitio all'ennesimo attacco del quotidiano della CEI  l'Avvenire contro i diritti delle persone LGBT(E) e  contro coloro che difendono lo Stato di Diritto, la democrazia, i diritti umani, il principio di eguaglianza,  alla base della civiltà giuridica contemporanea che si afferma sempre più in Europa.

Questa volta le gerarchie clericali, diretta emanazione dello Stato teocratico della "Città del Vaticano", se la prendono contro la Risoluzione sulla lotta alle discriminazioni basate sull'orientamento sessuale e sull'identità di genere adottata dal Consiglio d'Europa e dal suo Comitato dei Ministri nei giorni scorsi.

L'Avvenire solleva una ridicola, strampalata e falsa incompatibilità col Trattato di Lisbona, aggiungendo che la Risoluzione approvata pone le premesse per "esiti preoccupanti riguardo le adozioni per le coppie gay, le unioni omosessuali, la procreazione assistita e la propaganda nelle scuole". E’ semplicemente patetico che il quotidiano dei Cardinali cerchi di distrarre i suoi lettori e l’opinione pubblica inventandosi incompatibilità del tutto infondate sul piano del diritto comunitario europeo da questioni molto gravi che riguardano il fenomeno della pedofilia, sempre più diffuso nelle diocesi cattoliche di molti paesi del mondo, così come dai gravi silenzi e coperture di rilevanza criminale di cui si sono resi responsabili molti esponenti della chiesa cattolica.

Ci auguriamo che i vescovi e le gerarchie cattoliche sappiano fare tesoro delle drammatiche miserie che sono sotto gli occhi dell'opinione pubblica internazionale, per ritornare e riscoprire l’ amore e il bene verso il prossimo, recuperando nuovamente i valori di fratellanza della loro Chiesa e recuperare l'umanità perduta ”.