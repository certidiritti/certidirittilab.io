---
title: 'presentazione libro e dibattito Pannella-Eribon'
date: Tue, 23 Nov 2010 11:16:03 +0000
draft: false
tags: [Senza categoria]
---

Il 5 giugno 2004 il sindaco di un piccolo comune francese sposa una coppia di uomini, dando vita a uno straordinario dibattito che coinvolge tutta la società francese, mettendo in crisi il Partito Socialista e le diverse correnti della psicoanalisi. Didier Eribon, che con un gruppo di intellettuali e giuristi fu all’origine di questa eccezionale mobilitazione per l’uguaglianza dei diritti, racconta la cronaca, le reazioni e le riflessioni che questo movimento civile seppe suscitare.

**Venerdì 26 novembre, ore 17.30, presso il Partito Radicale**

**via di Torre Argentina, 76 - Roma**

si terrà la presentazione del libro:

“Su questo istante fragile” - Diario: gennaio-agosto 2004

di **Didier Eribon** \- Edizioni Homolegens

Ne discute con l’autore

**Marco Pannella**

Introduce **Annalisa Romani**, traduttrice e curatrice del libro

Partecipa **Sergio Rovasio**, segretario di Certi Diritti

Presiede **Alessandro Litta Modignani**, Radicali Italiani

_Didier Eribon è professore presso la facoltà di filosofia, scienze umane e sociali dell’Università di Amiens (Francia). E’ stato Visiting Professor all’Università di Berkeley, California._