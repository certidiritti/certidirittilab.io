---
title: 'Giovanardi è il capo dei bla-bla-bla della politica italiana. Eviti battute da osteria della periferia vaticana'
date: Thu, 20 Oct 2011 22:34:57 +0000
draft: false
tags: [Politica]
---

Il sottosegretario reinventa la Costituzione per compiacere le orecchie di vescovi e cardinali.

Roma, 20 ottobre 2011

Comunicato Stampa dell'Associazione Radicale Certi Diritti

Ieri per l'ennesima volta il (molto) Sottosegretario per la famiglia "tradizionale-eterosessuale-sposata-con-prole" Monsignor Giovanardi ha replicato le sue performance mediatiche sul nulla. Mai una volta che nei suoi bla-bla-bla affronti la questione dell’aumento dei divorzi e del calo dei matrimoni. Da bravo esperto in bla-bla-bla getta le sue parole, pesanti ed inutili, su una società disastrata anche per colpa della sua ostinazione – e di molti altri clericalfondamentalisti - di non volere nemmeno le unioni civili in Italia, che aiuterebbero sia le coppie eterosessuali sia quelle omosessuali.

Giovanardi afferma, tra le tante cose, che "il Parlamento italiano dovrebbe modificare la Costituzione, passaggio obbligato per introdurre il riconoscimento delle unioni civili". Così dicendo, dimostra di non sapere niente della Costituzione, nè della sentenza della Corte costituzionale n. 138/2010 che invece chiede esplicitamente al Parlamento di legiferare per il riconoscimento dei diritti delle coppie dello stesso sesso, senza modifiche costituzionali, e scegliendo liberamente tra matrimonio civile o unioni civili.

E' triste che questo (molto) Sottosegretario della famiglia si vanti di non tenere in alcuna considerazione le coppie non sposate alimentando così una vera e propria discriminazione di Stato. Più che un Ministro per la famiglia si comporta e parla come un ministro contro le famiglie, il tutto per asservire e alimentare la casta clerical-vaticana. Ma perché mai nessuno domanda a Monsignor Giovanardi del perché questo Governo ha tagliato i finanziamenti alle famiglie italiane (quella tradizionale, da lui tanto amata e difesa) di oltre il 90%?

L'associazione radicale Certi Diritti sostiene ogni tipo di azione giudiziaria e politica a favore del riconoscimento dei diritti e doveri di tutte le coppie conviventi non sposate che, prima o poi, nonostante il Monsignore di turno, arriveranno per legge anche in Italia con la Riforma del Diritto di famiglia.

Giovanardi ha dimostrato anche un miserabile livello politico da osteria di periferia vaticana dichiarando che quando  verranno riconosciute le unioni civili e il matrimonio civile per tutti “continuerà ad essere sposato con una donna". Forse ha sentito la necessità di dichiarare questo  perchè aveva come riferimento di grande pensiero politico i suoi amici democristiani che si battevano negli anni '70 contro il divorzio e poi, fatta la legge, ne hanno usufruito a man bassa con i monsignori e i vescovi zitti come mosche!