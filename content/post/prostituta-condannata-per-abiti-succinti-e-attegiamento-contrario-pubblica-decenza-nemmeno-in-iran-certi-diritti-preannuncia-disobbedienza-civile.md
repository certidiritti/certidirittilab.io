---
title: 'Prostituta condannata per abiti succinti e attegiamento contrario pubblica decenza. Nemmeno in Iran. Certi Diritti preannuncia disobbedienza civile'
date: Tue, 17 Jan 2012 22:16:54 +0000
draft: false
tags: [Lavoro sessuale]
---

Multe alle prostitute troppo succinte: la Cassazione faccia appello alla classe poltica per legalizzare la professione anzichè fare appelli a un maggior decoro sui vestiti delle lucciole. Certi Diritti farà disobbedienza civile.

Roma, 17 gennaio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti:  
E’ davvero sorprendente l’appello lanciato dalla Cassazione alle prostitute con la sentenza n.1387 che  le invita ad un maggior ‘decoro’ riguardo il loro modo di vestire e di atteggiarsi. La Terza sezione penale della Corte anziché occuparsi delle gonne troppo corte o dei reggiseni stimola-ormoni delle prostitute poteva dedicare alcune righe alla sempre più urgente necessità di legalizzare il lavoro delle Sex-Workers, alla condizione di grave degrado in cui sono costrette ad operare  e a tutto quel mercato criminale che alimenta un giro di denaro stimato in almeno 5 miliardi di euro e che coinvolge circa nove milioni di italiani.

Il nostro è davvero uno strano paese: si colpisce una prostituta per strada sanzionandola con 800 Euro per come si veste e addirittura si motiva la sanzione per il suo “atteggiarsi con troppa disinvoltura per strada”. Quali sarebbero gli ‘atti contrari alla pubblica decenza’? . Tutta l’accusa si basa quindi sul modo di vestire e l'atteggiamento assunto dalla sex-worker che, a detta della Suprema Corte,  ha rappresentato una «condotta contraria al sentimento di costumatezza così come inteso tuttora dalla comunità/collettività sociale».

Siamo in Italia, non in Iran. E anche se il Vaticano detta legge a molti dei politici che sovente con le prostitute ci vanno di nascosto, alimentando così ipocrisie a non finire, è pur vero che ognuno può ancora vestirsi nel pieno della libertà, senza catalogazioni, marchi e stili comportamentali.

L’Associazione Radicale Certi Diritti preannuncia sin da ora, appena il clima lo consentirà, un’azione di disobbedienza civile: ci recheremo con abiti succinti e con gli stessi atteggiamenti contrari al sentimento di costumatezza assunti dalla prostituta e ci faremo anche noi multare.