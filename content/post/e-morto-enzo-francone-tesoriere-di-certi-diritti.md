---
title: 'E'' MORTO ENZO FRANCONE, TESORIERE DI CERTI DIRITTI'
date: Sun, 29 Nov 2009 18:49:50 +0000
draft: false
tags: [Comunicati stampa, ENZO FRANCONE, ESPONENTE, FUORI, lgbt]
---

**E' MORTO ENZO FRANCONE, TRA I FONDATORI DEL FUORI, STORICO ESPONENTE DEL MOVIMENTO LGBT ITALIANO, TESORIERE DELL'ASSOCIAZIONE RADICALE CERTI DIRITTI.**

Roma, 29 novembre 2009

E’ morto questa sera a Torino Enzo Francone, esponente storico del movimento lgbt italiano, tra i fondatori del Fuori!, il primo movimento gay italiano, tesoriere dell’Associazione Radicale Certi Diritti.

Enzo Francone, da sempre radicale, era  impegnato sin dai primi anni ’70  nelle iniziative locali e nazionali del movimento gay italiano al fianco di Angelo Pezzana ed Enzo Cucco. La sua scomparsa, causata da una grave forma di cancro, è una perdita umana e politica di grande rilievo. Le sue lotte, le sue battaglie sono entrate nella storia del movimento gay italiano.

Fu arrestato diversi anni fa a Mosca e  poi aTeheran nell’ambito delle lotte nonviolente del Fuori e dei radicali contro i soprusi di quei regimi verso le persone omosessuali. Il suo storico impegno nella città di Torino  lo ha reso un riferimento di grande importanza nel coordinamento Torino Pride, grande esempio di coesione e vicinanza tra le associazioni lgbt che si battono per l’eguaglianza dei diritti.

Certi Diritti piange la perdita di una persona straordinaria che con il suo impegno ha contribuito alla crescita e al prestigio dell’Associazione.

Enzo francone ha sempre caratterizzato il suo impegno per il riconoscimento dei diritti delle persone lesbiche, gay e transessuali con una grande forza umana e sempre con il sorriso. Questo è il motivo per cui tutti gli volevano tanto bene.

**Il sito dell'associazione radicale Certi Diritti rimarrà con i colori scuri in segno di lutto**