---
title: 'COMUNE DI ROMA COMMEMORA PORTA PIA CHIEDENDO PARERE E MODALITA’ AL VATICANO'
date: Tue, 27 Jul 2010 11:41:50 +0000
draft: false
tags: [Comunicati stampa, PORTA PIA, RADICALI, roma, vaticano, XX settembre]
---

**DOPO GLI ZUAVI PONTIFICI QUEST'ANNO SARA' LA VOLTA DEL RICORDO DEL PAPA RE? RADICALI, LIBERALI, ANTICLERICALI, LAICI FARANNO LA VERA COMMEMORAZIONE IL XX SETTEMBRE 2010.**

Roma, 27 luglio 2010

Dichiarazione di **Sergio Rovasio**, Segretario Associazione Radicale Certi Diritti:

"Quasi due anni fa, il 20 settembre 2008, il Comune di Roma, in occasione della commemorazione della presa di Porta Pia, ebbe la geniale idea di commemorare (a nostre spese) i 19 zuavi pontifici, per lo più mercenari al soldo del Papa Re, anziché l'eroica impresa di 49 bersaglieri caduti per liberare la città di Roma per farla diventare capitale dell'Italia.

Lo scorso anno, il 20 settembre 2009, fu impedito ai radicali di svolgere una marcia per ricordare i luoghi della liberazione di Roma dallo Stato pontificio e fu permesso soltanto di svolgere l'annuale commemorazione a Porta Pia.

Quest'anno, la commemorazione del 140° anno della liberazione di Roma ha superato ogni incredibile fantasia diplomatico-istituzionale di asservimento al potere clericale del Vaticano: il Comune di Roma, secondo fonti giornalistiche, avrebbe chiesto il parere al Vaticano su quali sarebbero stati gli storici graditi e quali no per lo svolgimento dei diversi eventi promossi per le commemorazioni garantendo che gli stessi si sarebbero svolti senza la partecipazione di forze anticlericali e antivaticane. Ci viene il sospetto che quest'anno vogliano commemorare il Papa Re, responsabile di atti e azioni gravissime contro la popolazione romana durante il suo regno.

Possiamo preannunciare sin d'ora che al più presto ci organizzeremo con tutte le realtà radicali, liberali, repubblicane, laiche, libertarie, anticlericali, per fare il prossimo XX Settembre una commemorazione adeguata a ricordare i 140 anni della Breccia di Porta Pia, senza chiedere pareri a Stati esteri, tanto meno a Stati teocratici che vivono di impunità e privilegi, e per onorare la memoria degli eroi del Risorgimento italiano".