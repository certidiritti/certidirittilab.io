---
title: 'PARLAMENTO EUROPEO APPROVA NUOVA DIRETTIVA ANTIDISCRIMINAZIONE'
date: Thu, 02 Apr 2009 08:39:26 +0000
draft: false
tags: [Comunicati stampa]
---

PE/ANTIDISCRIMINAZIONE: CAPPATO E CERTI DIRITTI: "APPROVAZIONE DELLA NUOVA DIRETTIVA ANTI-DISCRIMINAZIONE E' UN SUCCESSO PER L'EUROPA DEI DIRITTI"  
   
Dichiarazione di Marco Cappato, eurodeputato Radicale al PE, di Sergio Rovasio e Ottavio Marzocchi, rispettivamente Segretario e responsabile questioni europee di Certi Diritti: "L'approvazione da parte del PE della nuova direttiva anti-discriminazione é un importante passo avanti verso un'Europa dei diritti, dell'eguaglianza e senza discriminazioni. La direttiva, se approvata anche dal Consiglio, allargherà la protezione per i cittadini europei contro le discriminazioni basate sulla disabilità, l'orientamento sessuale, la religione e le credenze e l'età, dal settore dell'impiego ai settori dell'educazione, dell'accesso ai beni ed ai servizi. La relazione del PE chiede che siano coperte anche le discriminazioni multiple, quelle per associazione, i contratti pubblici.  
   
Il PE ha inoltre rimosso dal testo della direttiva, spostandolo in un considerando, il riferimento alle leggi nazionali in merito allo status matrimoniale e familiare ed ai diritti riproduttivi, lasciando in questo modo aperta la questione della possibile incompatibilità delle leggi nazionali che non prevedono unioni tra persone dello stesso sesso con il principio d'eguaglianza. Inoltre, le Chiese e le religioni, quando conducono attività coperte dal diritto comunitario, non potranno discriminare.  
   
I tentativi del PPE di bocciare la direttiva, di mutilarla escludendo il settore dell'educazione, le discriminazioni multiple, rafforzando le esclusioni per le Chiese e le religioni, sono stati rigettati dal PE, che ha difeso il principio della laicità dello Stato contro le ingerenze delle Chiese, che hanno fatto un lobbying serrato sui deputati europei senza riuscire nel loro intento.  
   
Allo stesso tempo i compromessi proposti in commissione dalla relatrice verde Buitenweg per venire incontro al PPE, e successivamente confermati in plenaria, lasciano nella direttiva una serie di deroghe di dubbia compatibilità con il principio di eguaglianza, contro le quali ci siamo battuti in commissione e in plenaria. Ci auguriamo che il Consiglio e la Commissione europea modifichino e quindi adottino la direttiva per assicurare che il principio di eguaglianza prevalga, contro le ragioni di coloro che vogliono mani libere per discriminare contro omosessuali e lesbiche, religioni minoritarie, persone anziane o giovani, disabili".