---
title: 'CD: ok Commissione europea su coppie internazionali ma non basta'
date: Mon, 21 Mar 2011 12:11:54 +0000
draft: false
tags: [Diritto di Famiglia]
---

La commissione europea promuove due proposte per la tutela dei diritti delle coppie internazionali sposate o registrate, ma non basta. Occorre procedere anche al mutuo riconoscimento delle coppie dello stesso sesso in tutta l'Unione europea.  
  
Roma – Bruxelles, 21 marzo 2011  
   
Comunicato Stampa dell’Associazione Radicale Certi Diritti

La Commissione europea ha lanciato due proposte di regolamento europeo sulla facilitazione e semplificazione delle procedure relative ai diritti di proprietà delle ‘coppie internazionali’ (quando uno dei due partner è di un altro paese dell’Ue) in caso di divorzio o di morte di uno dei due coniugi, sia in merito alla determinazione della Corte competente che della legge da applicare. La Commissione ha deciso di splittare la proposta in due parti: una per le coppie sposate e una per le coppie registrate, così come riconosciuto in 14 paesi dell’Unione Europea.  
   
Sebbene la Commissione affermi che le due proposte sono neutrali rispetto al sesso delle persone sposate o in partnership registrata ed al loro orientamento sessuale; nella sostanza **i due regolamenti non cambieranno molto riguardo la tutela delle persone LGBT laddove non vi sono diritti riconosciuti dalle leggi nazionali**. Difatti la Commissione stessa mette le mani avanti dicendo che i regolamenti non armonizzano le norme nazionali sul matrimonio, le partnership registrate, le succession, i divorzi e la tassazione delle proprietà.  
   
Una coppia dello stesso sesso sposata nei Paesi Bassi sarà trattata dal Regolamento nello stesso modo di una coppia di sesso diverso in merito ai diritti patrimoniali  e comunque il Regolamento non prevede il pieno e mutuo riconoscimento tra paesi membri dell’Ue. E questa è certamente una grave lacuna che prima o poi dovrà essere superata in ambito europeo.  
   
Sarebbe stato anche  possibile accorpare le due proposte in un unico regolamento, cosa che la Commissione non ha fatto, creando due regimi diversi.  
   
L’Associazione Radicale Certi Diritti chiede alla Commissione ed all'Unione europea di accorpare le due proposte e di **procedere velocemente con una Roadmap per i diritti delle coppie lesbiche e gay con proposte che assicurino il reale mutuo riconoscimento delle coppie dello stesso sesso**, sposate o in partnership registrata, in tutti gli Stati membri UE.