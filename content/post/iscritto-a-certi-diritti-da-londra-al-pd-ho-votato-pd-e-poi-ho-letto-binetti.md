---
title: 'ISCRITTO A CERTI DIRITTI, DA LONDRA,  AL PD: HO VOTATO PD E POI HO LETTO  BINETTI!'
date: Sun, 06 Apr 2008 10:24:55 +0000
draft: false
tags: [binetti, certi diritti, Comunicati stampa, DICO, londra, pd]
---

Ai vertici del Pd, loro indirizzi 

Cari amici,

Vivendo a Londra ho giá votato da una settimana, per il PD. 

Leggo con sgomento le dichiarazioni di Paola Binetti (e apparentemente non sarebbe l’unica dei candidati del PD a pensarla cosí) secondo cui non voterebbe mai a favore di DICO et similia.

 In Gran Bretagna i DICO sembrerebbero una proposta medievale anche a gran parte del partito Conservatore (senza parlare di Labour e Lib-Dem); possibile che nel PD non si sia ancora trovato accordo neanche su quelli?In Italia parte del PD sta ancora assumendo posizioni che qui vengono forse riproposte da frange dell’UKIP ma non trovano accoglienza in nessuno dei maggiori partiti.Personalmente credo che i DICO continuino a dare una risposta largamente insufficiente a diritti anacronisticamente negati, ma in mancanza di meglio riesco ad accettare che siano un timido passo avanti.Ho votato per il PD per le garanzie offerte da Walter Veltroni di non vedere piú le cialtronate di parlamentari e ministri che una volta eletti iniziano a prendere dal programma quello che piú gli garba, scordando di avere assunto un impegno complessivo con tutti gli elettori; un Partito Democratico all’americana, dove anime diverse cercano una soluzione di sintesi depurate da ideologie e fondamentalismi. Per favore, fatemi vedere che non mi avete preso in giro.Sarei lieto di leggere una smentita pubblica dell’on. Binetti, che verrá eletta anche grazie al voto di tanti gay e lesbiche e da cui non sarebbe dignitoso limitarsi a farsi pagare lo stipendio senza rispondere politicamente alle loro istanze.Convincere 5 amici a votare PD si puó fare; se gli si dá la sicurezza di non preparare un altro episodio di commedia all’italiana. Per favore non rispondetemi personalmente; a dichiarazioni pubbliche va risposto con dichiarazioni pubbliche.

Grazie.

Adolfo Sansolini - Londra