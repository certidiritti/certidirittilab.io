---
title: 'I deputati di San Pietroburgo hanno infine approvato un divieto di "propaganda gay"'
date: Wed, 29 Feb 2012 13:34:32 +0000
draft: false
tags: [Russia]
---

Articolo pubblicato ieri sul sito russo 'Kontury' sulla nuova normativa discriminatoria contro l'omosessualità in Russia. Di Nikolay Baev, traduzione di Antonio Stango.

Oggi in occasione della sessione dell'Assemblea legislativa di San Pietroburgo è stato adottato nella terza e ultima lettura un disegno di legge per vietare la propaganda della pedofilia, così come di "sodomia, lesbismo, bisessualità e transgender" tra i minori. Ora la legge andrà alla firma del governatore della capitale del nord Georgy Poltavchenko, noto come "ortodosso clericale".

Il disegno di legge è stato introdotto dal gruppo 'Russia Unita', e il suo passaggio in Assemblea legislativa è stata accompagnato da numerose proteste di attivisti LGBT, nonché da dichiarazioni del Dipartimento di Stato, di ministri degli Esteri di Gran Bretagna, Francia e Irlanda e del Senato di Amburgo - città sorella di San Pietroburgo.

Tuttavia, nessuna protesta ha influenzato i piani dei deputati oggi hanno approvato la legge con 26 voti a favore, cinque contrari e uno di astensione. Da notare che ad astenersi è stato uno dei leader della 'Yabloko' all'Assemblea legislativa, Grigory Yavlinsky, mentre gli altri del suo partito hanno votato contro.

La legge discriminatoria in realtà vieta la libertà di espressione, di riunione e di associazione per omosessuali a San Pietroburgo; è stata sostenuta dai deputati del gruppo 'Russia Unita', del Partito Comunista, e da alcuni membri del gruppo 'Russia Giusta'. I deputati del Partito Liberal Democratico semplicemente non hanno partecipato alla votazione.

La legge introduce un sistema di multe per tale 'propaganda': per le persone fisiche senza incarichi pubblici fino a 5.000 rubli, per i funzionari fino a 50.000 rubli, e per le persone giuridiche fino a 500.000.

In realtà, la nuova legge può servire come un utile strumento per la cessazione di qualsiasi attività pubblica delle persone gay, lesbiche, bisessuali e transgender della città, dai seminari e convegni scientifici alle manifestazioni di piazza, che possono facilmente essere proibite dalle autorità con il pretesto che possano venire a conoscenza di bambini.

Ieri si sono svolte proteste contro la legge omofoba di San Pietroburgo in diverse città del mondo. I partecipanti alle dimostrazioni presso ambasciate e missioni di Russia a Parigi, Londra, Bruxelles, Roma, Delhi, New York e altre città hanno chiesto al governatore di non firmare la nuova legge Poltavchenko.

Tuttavia, la probabilità che Poltavchenko firmi il documento discriminatorio è più che reale: forse il governatore 'ortodosso' potrebbe rifiutare ai clericali, che hanno fatto pressione per il disegno di legge, il piacere di introdurre un divieto per tutte le persone apertamente omosessuali?

Inoltre, la legge che è stata approvata a San Pietroburgo si inserisce perfettamente nelle politiche socio-conservatrici del partito di governo 'Russia Unita' e del suo leader, Vladimir Putin, che più volte hanno collegato le attività di omosessuali con la «grave situazione demografica» in Russia.

Il divieto di 'propaganda omosessuale' opera già dal 2006 nella regione di Ryazan e dal 2011 nella regione di Arkhangelsk.

All'inizio di quest'anno un progetto di legge simile ha superato la seconda lettura nella Duma della regione di Kostroma e un altro è stato preparato per l'adozione da parte dell'Assemblea legislativa della regione di Novosibirsk.

Nikolay Baev

**iscriviti alla newsletter >[  
](newsletter/newsletter)[](newsletter/newsletter)[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**