---
title: 'Depositata oggi al Comune di Roma proposta di delibera su Unioni Civili. Decine di associazioni mobilitate per raccolta firme'
date: Fri, 17 Feb 2012 13:23:03 +0000
draft: false
tags: [Diritto di Famiglia]
---

Sul blog teniamofamiglia.blogspot.com il testo della delibera di iniziativa popolare.

Comunicato Stampa di Radicali Roma e dell’Associazione Radicale Certi Diritti

Roma, 17 febbraio 2012

Questa mattina alcuni rappresentanti di associazioni e organizzazioni cittadine hanno depositato al Comune di Roma una proposta di Delibera di iniziativa popolare per il riconoscimento delle Unioni Civili. A partire da oggi, ed entro tre mesi, così come previsto dallo Statuto del Comune di Roma, verranno raccolte le 5.000 firme necessarie  per portare tale proposta all’attenzione del Consiglio Comunale di Roma.

Obiettivo dell’iniziativa è quello di riconoscere le Unioni Civili con riferimento alla legge sulla famiglia anagrafica del 1989 ed estendere i servizi, gli aiuti e l’assistenza del Comune di Roma anche a quei nuclei familiari sempre più diffusi nella Capitale, così come già avvenuto a Torino, Napoli e tra non molto anche a Milano.

In questa iniziativa sono impegnate decine di organizzazioni, associazioni  e personalità che si battono per la promozione e difesa dei diritti civili.

Hanno aderito e partecipano all’iniziativa:

Radicali Roma, Associazione Radicale Certi Diritti, Circolo di cultura omosessuale Mario Mieli,  Sel Regionale, Sel Roma Area metropolitana, Forum queer Sel, Uaar Roma, Giovani Idv, Arcigay Roma, Gay Center, Consulta romana per la laicità delle istituzioni, Arcilesbica Roma, Agedo Roma, Roma Rainbow Choir, QueerLab, PianetaQueer.it, Famiglie Arcobaleno, Yellow Sport, Fondazione Massimo Consoli,  Gayroma.it, DiGayProject, Luiss Arcobaleno, Gay & Geo gruppo trekking Roma, A.F.F.I.Associazione Federativa Femminista Internazionale della Casa Internazionale delle Donne.

Al seguente link il testo della proposta di delibera di iniziativa popolare:

[http://teniamofamiglia.blogspot.com/p/mettici-la-firma.html](http://teniamofamiglia.blogspot.com/p/mettici-la-firma.html)  
  

**iscriviti alla newsletter >[  
](newsletter/newsletter)[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**