---
title: 'CALO DEMOGRAFICO PER COLPA DEI GAY? IL PAPA SMETTA DI OFFENDERE LESBICHE E GAY'
date: Sat, 10 May 2008 17:45:44 +0000
draft: false
tags: [Comunicati stampa]
---

> Dichiarazione di Sergio Rovasio, Segretario Associazione Certi Diritti:  
> "Che il Papa sostenga che i diritti alle coppie gay non devono esistere perche' c'e' gia' un forte calo demografico in corso, e' un fatto grave e offensivo, innanzitutto verso le persone lesbiche e gay.  

Peraltro incolpare i gay del calo demografico e non, semmai, le famiglie cattoliche che usano abitualmente il preservativo o la pillola delgiorno dopo, e' espressione di ipocrisia tendente a nascondere un fenomeno molto diffuso nel mondo cattolico. La nuova tesi del Papa e' piu' da osteria che da fine teologo e dimostra - fosse una novita' - una forte e preoccupante attenzione verso il mondo lgbt. Se la sua ossessione sul tema del calo demografico e' cosi pregnanate faccia eliminare il voto di castita' alle suore e ai preti che, si sa, non sono persone necessariamente omosessuali."