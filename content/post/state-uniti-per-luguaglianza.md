---
title: 'STATE UNITI per l''Uguaglianza'
date: Wed, 26 Mar 2014 16:27:20 +0000
draft: false
---

[![tessera_certi_diritti_fronte_def](http://www.certidiritti.org/wp-content/uploads/2014/03/tessera_certi_diritti_fronte_def.jpeg)](http://www.certidiritti.org/wp-content/uploads/2014/03/tessera_certi_diritti_fronte_def.jpeg)**Questa è la tessera dell’Associazione Radicale Certi Diritti per l’anno 2014**. La riceverai se quest’anno deciderai di sostenere le nostre battaglie per l’uguaglianza nel nome della libertà e della responsabilità sessuale delle persone.

**Non percepiamo alcun finanziamento pubblico, e la nostra unica fonte di sopravvivenza è il contributo di chi decide di far suoi gli obiettivi che ci poniamo per quest’anno sottoscrivendo la quota associativa di 50 o 25 euro** (quest’ultima più bassa per venire incontro a studenti, disoccupati, pensionati e senza reddito).

**Perché la scelta di questa tessera**? Il 2014 vedrà tutti gli italiani alle prese con l’appuntamento alle urne per le **elezioni europee**. La scelta dei rappresentanti italiani per il prossimo quinquennio di legislatura sarà cruciale anche sul fronte dell’affermazione dell’uguaglianza per gli LGBTI in Europa.

Certi Diritti ha già confermato il suo impegno per questa tornata elettorale, garantendo energie e risorse per **incoraggiare i candidati italiani a sottoscrivere i 10 punti di impegno sulle questioni LGBTI promossi da ILGA-Europe**.

La tessera di iscritto che riceverai - semmai vorrai intraprendere con noi questo viaggio che dura un anno - contiene un invito importante agli elettori, ai candidati e alle Istituzioni italiane ed europee: “restare uniti” per l’uguaglianza, per il rispetto dei diritti umani e per la libertà. Il gioco di parole tra “State Uniti” e “**Stati Uniti**” è un messaggio chiaro per **un’unione federale che catalizzi un processo di sviluppo complessivo per una proficua cooperazione in favore delle libertà e dei diritti.**

Altro nostro obiettivo in vista delle elezioni europee del 25 maggio è quello di **informare gli italiani sull’importanza del loro voto**. Per questo abbiamo prodotto un piccolo vademecum dell’ “elettore consapevole”* che distribuiremo nelle città in cui siamo presenti, magari anche con il tuo aiuto se contattandoci su info@certidiritti.it ci comunicherai l’intenzione di volerne ricevere alcuni per aiutarci nella diffusione.

Ora tocca a te.

Grazie per quello che vorrai fare.

[![pulsante_iscriviti_state_uniti](http://www.certidiritti.org/wp-content/uploads/2014/03/pulsante_iscriviti_state_uniti.png)](http://www.certidiritti.org/iscriviti/ "Iscriviti")
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

          *Elezioni 2014: la tua occasione! \[gallery columns="6" link="file" ids="5663,5662"\]