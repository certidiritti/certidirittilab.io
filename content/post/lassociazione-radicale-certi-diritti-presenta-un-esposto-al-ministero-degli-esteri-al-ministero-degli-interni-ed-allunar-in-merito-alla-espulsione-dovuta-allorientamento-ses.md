---
title: 'L’Associazione radicale Certi diritti presenta un esposto al Ministero degli Esteri, al Ministero degli Interni ed all’UNAR in merito alla espulsione dovuta all’orientamento sessuale di un insegnante italiano in servizio all’Asmara'
date: Fri, 26 Apr 2013 06:20:56 +0000
draft: false
tags: [Africa]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 26 aprile 2013

Un docente di lettere in servizio presso la scuola italiana di Asmara nei giorni scorsi è stato espulso dal'Eritrea a causa del suo orientamento sessuale. L'insegnante palermitano, sposato in Spagna dal 2008 con un ragazzo cileno dello stesso sesso, nei giorni scorsi è stato costretto dalle Autorità eritree ad abbandonare il suo lavoro e ad allontanarsi precipitosamente dal Paese, tutto questo senza che venissero fornite delle motivazioni ufficiali alla sua espulsione e soprattutto senza che vi fosse alcun motivo imputabile allo stesso (nessuna violazione della legge eritrea e nessun comportamento scorretto in ambito scolastico o extrascolastico). L'Ambasciatore italiano ad Asmara dott. Marcello Fondi, che è immediatamente intervenuto sulla questione, a seguito di trattative con le autorità eritree, ha fornito al docente solo una motivazione ufficiosa e cioè che questi veniva considerato dalla autorità eritree sostanzialmente un "individuo pericoloso e potenzialmente destabilizzatore dell'ordine morale e pubblico del Paese". Dovendo prima di tutto tutelare la sua integrità fisica e su esplicito suggerimento dell'Ambasciata italiana, il docente è rientrato precipitosamente in Italia.

L'Associazione radicale Certi diritti, che segue la vicenda, ha presentato immediatamente un esposto all'UNAR (Ufficio nazionale Antidiscriminazioni) e al Ministero degli Interni italiano affinché si intervenga e si costringa il Ministero eritreo a chiarire l'accaduto fornendo le motivazioni ufficiali dell'espulsione. L'indubbia gravità di quanto successo è evidente tanto più che nessun comportamento censurabile è attribuibile all'insegnante italiano, né l'ACCORDO TECNICO SULLO STATUS DELLE SCUOLE ITALIANE IN ASMARA E DEL LORO PERSONALE del 21 settembre 2013 menziona l'orientamento sessuale come elemento per l'attribuzione di un incarico lavorativo presso la scuola italiana in Eritrea.

Quanto successo ha avuto conseguenze sia sul piano psico-fisico che sul piano lavorativo (per questo si attende un intervento del MAE) del docente che è rimasto vittima di una inaccettabile discriminazione e di una violazione dei diritti umani e civili. Pertanto è stato richiesto un intervento immediato a chiarimento dell'accaduto presso le autorità eritree e l'assunzione di provvedimenti che tutelino le persone omosessuali che dovessero trovarsi in situazioni analoghe.