---
title: 'Matrimonio tra persone dello stesso sesso: decisione Usa storica'
date: Wed, 23 Feb 2011 20:01:12 +0000
draft: false
tags: [bernardini, certi diritti, clinton, Comunicati stampa, GAY, obama, RADICALI, rovasio, USA]
---

Il cammino per il superamento delle diseguaglianze continua contro i fondamentalismi religiosi e ideologici. Governo italiano prenda esempio da Obama e non dal vaticano.

Roma, 23 febbraio 2011

  
Comunicato Stampa dell'Associazione Radicale Certi Diritti

La decisione del Presidente degli Stati Uniti Obama di non difendere più nelle Corti Federali  il ‘Marriage Act’, che di fatto riconosce l’istituto del matrimonio soltanto alle persone eterosessuali, è un altro grande passo importante per il superamento delle diseguaglianze.

La decisione di fatto consente ai singoli Stati una maggiore libertà legislativa e impedirà che l’Amministrazione Usa si opponga qualora si legiferasse in favore del matrimonio tra persone dello stesso sesso.

Grazie al Presidente Obama l’accesso all’istituto del matrimonio per le coppie dello stesso diventerà più facile e un altro ostacolo è così crollato nella strada che porta al superamento delle diseguaglianze.

Ne esce sconfitto il fondamentalismo religioso e ideologico.  Il governo italiano prenda esempio dal Presidente Obama e la smetta di genuflettersi al Vaticano.