---
title: 'CONFETTI ON. PELINO A MATRIMONIO GAY IN SPAGNA: SCONCERTO E STUPORE'
date: Sun, 23 Nov 2008 12:14:41 +0000
draft: false
tags: [Comunicati stampa]
---

**SPOSI GAY E CONFETTI PELINO: SCONCERTO E STUPORE PER CONFETTI VIOLA ‘PELINO’ A MATRIMONIO GAY DI MADRID. I CONFETI SONO PRODOTTI DALLA DITTA ITALIANA DELL’ON.  PELINO DEPUTATA PDL CHE VOLEVA CONTINAURE A VENDERLI SOLO ALLE PERSONE ‘NORMALI’.**

**Lettera aperta di Sergio Rovasio, Segretario dell’Associazione Radicale Certi Diritti, all’on. Paola Pelino:**

Onorevole Paola Pelino,

abbiamo letto con concerto e stupore che al matrimonio tra due gay avvenuto ieri in Spagna, sono stati distribuiti agli ospiti confetti viola, forse quelli fatti apposta per le coppie gay denominati ‘gay bride’, prodotti con mandorle provenienti, guardi un po’, da San Francisco, e “offerti”, secondo fonti attendibili, dall’azienda abruzzese Pelino della sua famiglia  alla festa di nozze.  La notizia ci trova sgomenti perché lei stessa dichiarò a Il Giornale nel  maggio 2006 che i confetti dell’azienda di famiglia ‘vorrei continuare a venderli alle coppie normali’.

Possibile che dopo due anni i confetti ‘Pelino’ siano ancora presenti alle feste di nozze gay in giro per il mondo?

Uno dei due ragazzi convolato a nozze a Madrid è italiano, si chiama Roberto ed è dovuto andare in Spagna a sposarsi perché in Italia gli è impedito. Il matrimonio è stato officiato dal deputato socialista spagnolo Pedro Zerolo che si è anche augurato che in Italia si possano presto sposare le coppie gay.  
Gentile Onorevole, le pare normale che questo ragazzo italiano, andato in Spagna per sposarsi con il suo compagno, si ritrovi con i confetti Pelino, quelli che lei voleva fossero prodotti solo per ‘persone normali’, sul tavolo imbandito per la festa? Lei non ha nulla da dire?

**Sergio Rovasio**

**Segretario Associazione Radicale Certi Diritti**