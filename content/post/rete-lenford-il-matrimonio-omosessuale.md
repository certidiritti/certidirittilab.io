---
title: 'RETE LENFORD: IL MATRIMONIO OMOSESSUALE'
date: Fri, 03 Jul 2009 14:40:21 +0000
draft: false
tags: [Senza categoria]
---

  
• **Venerdì 19 giugno 2009, ore 15.00 - 18.30** IL MATRIMONIO OMOSESSUALE - Parità di diritti e riconoscimento sociale Tavola rotonda aperta al pubblico organizzata da Milk Milano in collaborazione con Rete Lenford e il Centro Interdipartimentale di Studi e Ricerche "Donne e Differenze di Genere".

  
**PROGRAMMA:  
**Saluti introduttivi:  
\- Chiara Martucci (Centro Interdip. di Studi e Ricerche "Donne e Differenze di Genere")  
\- Stefano Aresi (Milk Milano)  
\- Paola Ronfani (Univ. Milano Statale)

**Interventi:**  
CHIARA BERTONE (Università degli Studi del Piemonte Orientale): Una minaccia a quale famiglia? La costruzione dell'opposizione tra famiglia e omosessualità.  
FABIO REGIS (Milano): "L'amore forte" come paradigma fondativo del matrimonio.  
PERSIO TINCANI (Università degli Studi di Bergamo): Il matrimonio omosessuale: l'argomento dell'offesa.  
DANIELA DANNA (Università degli Studi di Milano): Diritti delle coppie o diritti delle/i individue/i?  
MATTEO WINKLER (Università Commerciale Luigi Bocconi in Milano): Separate but Equal. Unioni civili o matrimonio?  
DAVIDE GALLIANI (Università degli Studi di Milano): L'eguaglianza nella diversità.  
FRANCESCO BILOTTA (Università degli Studi di Udine): La persona, l’amore, il diritto.

Coordina la tavola rotonda:  
PATRIZIA BORSELLINO (Comitato Scientifico Rete Lenford)

[http://www.milkmilano.com/circolo\_di\_cultura\_omosessuale\_milk_milano/images/LocandinaCol.gif](http://www.milkmilano.com/circolo_di_cultura_omosessuale_milk_milano/images/LocandinaCol.gif)

Di seguito i file mp3 dell'incontro:

[http://www.milkmilano.com/tavolaRotonda/1\_Pres\_Chiara_Martucci.mp3](http://www.milkmilano.com/tavolaRotonda/1_Pres_Chiara_Martucci.mp3)  
[http://www.milkmilano.com/tavolaRotonda/2\_Intro\_Patrizia_Borsellino.mp3](http://www.milkmilano.com/tavolaRotonda/2_Intro_Patrizia_Borsellino.mp3)  
[http://www.milkmilano.com/tavolaRotonda/3\_stefano\_Aresi.mp3](http://www.milkmilano.com/tavolaRotonda/3_stefano_Aresi.mp3)  
[http://www.milkmilano.com/tavolaRotonda/4\_Paola\_Ronfani.mp3](http://www.milkmilano.com/tavolaRotonda/4_Paola_Ronfani.mp3)  
[http://www.milkmilano.com/tavolaRotonda/5\_Chiara\_Bertone.mp3](http://www.milkmilano.com/tavolaRotonda/5_Chiara_Bertone.mp3)  
[http://www.milkmilano.com/tavolaRotonda/6\_Fabio\_Regis.mp3](http://www.milkmilano.com/tavolaRotonda/6_Fabio_Regis.mp3)  
[http://www.milkmilano.com/tavolaRotonda/7\_Persio\_Tincani.mp3](http://www.milkmilano.com/tavolaRotonda/7_Persio_Tincani.mp3)  
[http://www.milkmilano.com/tavolaRotonda/8\_Daniela\_Danna.mp3](http://www.milkmilano.com/tavolaRotonda/8_Daniela_Danna.mp3)  
[http://www.milkmilano.com/tavolaRotonda/9\_Matteo\_Winkler.mp3](http://www.milkmilano.com/tavolaRotonda/9_Matteo_Winkler.mp3)  
[http://www.milkmilano.com/tavolaRotonda/10\_Davide\_Galliani.mp3](http://www.milkmilano.com/tavolaRotonda/10_Davide_Galliani.mp3)  
[http://www.milkmilano.com/tavolaRotonda/11\_Francesco\_Bilotta.mp3](http://www.milkmilano.com/tavolaRotonda/11_Francesco_Bilotta.mp3)  
[http://www.milkmilano.com/tavolaRotonda/12\_Dibattito\_e_conclusione.mp3](http://www.milkmilano.com/tavolaRotonda/12_Dibattito_e_conclusione.mp3)