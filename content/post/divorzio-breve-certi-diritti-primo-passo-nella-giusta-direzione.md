---
title: 'Divorzio Breve. Certi Diritti: primo passo nella giusta direzione'
date: Thu, 23 Apr 2015 08:25:31 +0000
draft: false
tags: [Diritto di Famiglia]
---

[![(DPA) DIVORZIO](http://www.certidiritti.org/wp-content/uploads/2015/04/divorzio1-300x221.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/04/divorzio1.jpg)«La nuova legge sul divoro breve andrebbe chiamata separazione breve, ma è comunque un primo passo nella direzione giusta». Questo il commento di Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti. Preferivamo che la legge passasse senza che venisse stralciata in Senato la possibilità di saltare del tutto la fase della separazione per le coppie che lo richiedessero di comune accordo come avviene in Francia e Regno Unito, ma certo, sei mesi sono meglio dei tre anni che ci sono stati inflitti sinora. Un dato di preoccupazione più serio riguarda invece la previsione di poter sciogliere il vincolo matrimoniale in un anno in caso di conflittualità: considerato lo stato in cui versa la giustizia civile italiana, se si renderà necessario andare in tribunale, sarà difficile che questa tempistica venga rispettata. «A quarant’anni dall’ultima riforma del diritto di famiglia si preferisce andare avanti con piccole riforme settoriali che fanno poco e troppo tardi, quando si dovrebbe invece costruire una riforma complessiva del diritto di famiglia capace di soddisfare le esigenze di tutti, unificare finalmente le norme già esistenti in tema di diritto di famiglia e dare così luogo a un'unica e organica legislazione», conclude Guaiana.

Comunicato stampa dell’Associazione Radicale Certi Diritti.