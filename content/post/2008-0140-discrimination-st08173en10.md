---
title: '2008-0140-Discrimination-ST08173.EN10'
date: Thu, 15 Apr 2010 14:55:02 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 15 April 2010

Interinstitutional File:

2008/0140 (CNS)

8173/10

SOC 240

JAI 270

MI 94

  

  

  

  

  

NOTE

from :

The Presidency

to :

The Working Party on Social Questions

No. prev. doc. :

7349/1//10 REV 1 SOC 185 JAI 201 MI 80

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached a set of drafting suggestions prepared by the Presidency, with a view to the next meeting of the Working Party on Social Questions, which is scheduled for 22 April 2010.

Changes in relation to the previous versions[\[1\]](#_ftn1) are indicated as follows: new text is in **bold**, and deletions are marked "**\[…\]**".

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Recitals 17a-h and 19b-20c**

(17a) This Directive covers the application of the principle of equal treatment in the fields of social protection, education and access to goods and services within the limits of the competences of the European Union.

The Member States are responsible for the organisation and content of systems of social protection, health care and education, as well as for the definition of who is entitled to receive social protection benefits, medical treatment and education.

(17b) Social protection includes social security, social assistance, **social housing** and health care. Consequently, this Directive applies with regard to rights and benefits which are derived from general or special social security, social assistance and healthcare schemes, which are  statutory  or provided either directly by the State, or by private parties in so far as the provision of those benefits by the latter is funded by the State. In this context, the Directive applies with regard to benefits  in cash, benefits in kind and services, irrespective of whether the schemes involved are contributory or non-contributory. The abovementioned schemes include, for example, \[access to\] the branches of social security defined by Regulation 883/2004/EC on the coordination of social security systems[\[2\]](#_ftn2), as well as schemes providing for benefits or services granted for reasons related to the lack of financial resources or risk of social exclusion.

(17c)

(17d) All individuals enjoy the freedom to contract, including the freedom to choose a contractual partner for a transaction. This Directive should not apply to economic transactions undertaken by individuals for whom these transactions do not constitute a professional or commercial activity.

  

In particular, this Directive does not apply to transactions related to housing which are performed by natural persons, when the activities in question do not constitute a professional or commercial activity.

In this context, the concept of professional or commercial activity may be defined in accordance with the national laws and practice of the Member States.

(17e) This Directive does not alter the division of competences between the European Union and the Member States in the areas of education and social protection. It is also without prejudice to the essential role and wide discretion of the Member States in providing, commissioning and organising services of general economic interest.

(17f)  The exclusive competence of Member States with regard to the organisation of their social protection systems includes decisions on the setting up, financing and management of such systems and related institutions as well as on the substance and delivery of benefits and health services and the conditions of eligibility. In particular Member States retain the possibility to reserve certain benefits or services to certain age groups or persons with disabilities**. The Member States also retain their competences in respect of the organisation of their** **social housing services, including the management or allocation of such services and determining the conditions of eligibility.**

Moreover, this Directive is without prejudice to the powers of the Member States to organise their social protection systems in such a way as to guarantee their sustainability.

(17g) The exclusive competence of Member States with regard to the organisation of their educational systems and the content of teaching and of educational activities, including the provision of special needs education, includes the setting up and management of educational institutions, the development of curricula and other educational activities and the definition of examination processes. In particular Member States retain the possibility to set age limits for certain education activities. However, there may be no discrimination in the access to educational activities, including the admission to and participation in classes or programmes and the evaluation of students' performance.

  

(17h) This Directive does not apply to matters covered by family law including marital status and adoption, and laws on reproductive rights. It is also without prejudice to the secular nature of the State, state institutions or bodies, or education.

…

(19b) Measures to ensure accessibility for persons with disabilities, on an equal basis with others, to the areas covered by this Directive play an important part in ensuring full equality in practice. Such measures should **comprise** the identification and elimination of obstacles and barriers to accessibility, **as well as the prevention of new obstacles and barriers**. **\[…\].** They should not impose a disproportionate burden.

**(19c)    Such measures should aim at achieving accessibility including with regard to, _inter alia_, the physical environment, transportation, information and communication technology and systems, and services, within the scope of the Directive as defined in Article 3. The fact that access might not always be possible to achieve in full equality with others may not be presented as a justification for not adopting all measures to increase as far as possible accessibility to persons with disabilities.**

(19d) Improvement of accessibility can be provided by a variety of means, including application of the "universal design" principle. According to the United Nations Convention on the Rights of Persons with Disabilities, “universal design” means the design of products, environments, programmes and services to be usable by all people, to the greatest possible extent, without the need for adaptation or specialised design. “Universal design” should not exclude assistive devices for particular groups of persons with disabilities where this is needed.

  

(20)   Legal requirements[\[3\]](#_ftn3) and standards on accessibility have been established at European level in some areas while Article 16 of Council Regulation 1083/2006 of 11 July 2006 on the European Regional Development Fund, the European Social Fund and the Cohesion Fund and repealing Regulation (EC) No 1260/1999[\[4\]](#_ftn4) requires that accessibility for disabled persons is one of the criteria to be observed in defining operations co-financed by the Funds. The Council has also emphasised the need for measures to secure the accessibility of cultural infrastructure and cultural activities for people with disabilities[\[5\]](#_ftn5).

(20a) In addition to general measures to ensure accessibility, individual measures to provide reasonable accommodation play an important part in ensuring full equality in practice for persons with disabilities to the areas covered by this Directive.

(20b) In assessing whether measures to ensure accessibility or reasonable accommodation would impose a disproportionate burden, account should be taken of a number of factors including**, _inter alia_,** the size and resources of the organisation or enterprise, as well as the **estimated** costs **\[…\]** of such measures. A disproportionate burden would arise, for example, where significant structural changes would be required in order to provide access to movable or immovable property which is protected under national rules on account of their historical, cultural, artistic or architectural value.

(20c) The principle of accessibility is established in the United Nations Convention on the Rights of Persons with Disabilities. The principles of reasonable accommodation and disproportionate burden are established in Directive 2000/78/EC[\[6\]](#_ftn6) and the United Nations Convention on the Rights of Persons with Disabilities.

**Articles 3, 4, 4a, 4b and 15**

Article 3  
Scope

1.       Within the limits of the competences conferred upon the European Union, the prohibition of discrimination shall apply to all persons, as regards both the public and private sectors, including public bodies, in relation to:

(a)     Social protection, including social security, social assistance, **social housing** and healthcare:

(b)

(c)          Education;

(d)     and the access to the  supply of goods and other services which are available to the public, including housing.

Subparagraph (d) shall apply to natural persons only insofar as they are performing a professional or commercial activity defined  in accordance with national laws and practice.

2.       **Notwithstanding paragraph 1, this Directive does not apply to:**

(a)     matters covered by family law, including marital status and adoption, and the benefits dependent thereon, as well as  laws on reproductive rights**;**

(b)     the organisation of Member States' social protection systems, including decisions on the setting up, financing and management of such systems and related institutions as well as on the substance and delivery of benefits and services and the conditions of eligibility;

(c)     the competences of the Member States to determine the type of health services provided and the conditions of eligibility;

  

(d)     the organisation **and funding** of the Member States' educational systems, **including the organisation of education for people with special needs,** the content of teaching and **\[…\]** activities **within educational institutions**, **and** the **conditions of eligibility**;

3.       **\[…\]** Member States may provide that differences of treatment based on a person's religion or belief in respect of admission to educational institutions, the ethos of which is based on religion or belief, in accordance with national laws, traditions and practice, shall not constitute discrimination.

These differences of treatment shall not justify discrimination on any other ground referred to in Article 1.

3a.     This Directive is without prejudice to national measures authorising or prohibiting the wearing of religious symbols.

4.       This Directive is without prejudice to national legislation ensuring the secular nature of the State, State institutions or bodies, or education, or concerning the status and activities of churches and other organisations based on religion or belief.

5.       This Directive does not cover differences of treatment based on nationality and is without prejudice to provisions and conditions relating to the entry into and residence of third-country nationals and stateless persons in the territory of Member States, and to any treatment which arises from the legal status of the third-country nationals and stateless persons concerned.

Article 4

Accessibility for persons with disabilities

1.             Member States shall take the necessary and appropriate measures to ensure accessibility for persons with disabilities, on an equal basis with others, within the areas set out in Article 3. **These measures should not impose a disproportionate burden.**

**1a (new) Accessibility includes general anticipatory measures to ensure the effective implementation of the principle of equal treatment in all areas set out in Article 3 for persons with disabilities, on an equal basis with others, and with a medium or long-term commitment.**

2.             Such measures shall **comprise** the identification and elimination of obstacles and barriers to accessibility, **as well as the prevention of new obstacles and barriers** **in the areas covered in this Directive**.

3.       **\[…\]**

4. **\[…\]**

5.         **\[…\]**

**6 (new)    Paragraphs 1 and 2 shall apply to private and social housing only as regards the common parts of buildings with more than one housing unit. This paragraph shall be without prejudice to Article 4(7) and Article 4a.**

**7 (new)    Member States shall progressively take the necessary measures to ensure that sufficient private and / or social housing is accessible for people with disabilities.**

_Article 4a  
Reasonable accommodation for persons with disabilities_

1.       In order to guarantee compliance with the principle of equal treatment in relation to persons with disabilities, reasonable accommodation shall be provided within the areas set out in Article 3, unless this would impose a disproportionate burden.

2.       Reasonable accommodation means necessary and appropriate modifications and adjustments where needed in a particular case**,** to ensure to persons with disabilities access on an equal basis with others.

3. **\[…\]**

4. **\[…\]**

**_Article 4b (new)  
\[…\] Provisions concerning accessibility and reasonable accommodation_**

1.[\[7\]](#_ftn7) For the purposes of assessing whether measures necessary to comply with **Articles 4 and 4a** would impose a disproportionate burden, account shall be taken, in particular, of:

a)             the size and resources of the organisation or enterprise;

b)             **the estimated cost;**

**c)** **\[…\]**

**d)** **the life span of infrastructures and objects which are used to provide a service;**

e)             the historical, cultural, artistic or architectural value of the movable or immovable property in question;

**f)** **whether the measure in question is impracticable or unsafe.**

The burden shall not be deemed disproportionate when it is sufficiently remedied by measures existing within the framework of the disability policy of the Member State concerned.

**2.       Articles 4 and 4a shall apply to the design and manufacture of goods, unless this would impose a disproportionate burden. For the purpose of assessing whether a disproportionate burden is imposed in the design and manufacture of goods, consideration shall be taken of the criteria set out in article 4b(1).**

**3.       Articles 4 and 4a shall not apply where European Union law provides for detailed standards of specifications on the accessibility or reasonable accommodation regarding particular goods or services**[\[8\]](#_ftn8)**.**

…

Article 15

Implementation

1.       Member States shall adopt the laws, regulations and administrative provisions necessary to comply with this Directive by …. at the latest \[4 years after adoption\]. They shall forthwith inform the Commission thereof and shall communicate to the Commission the text of those provisions.

When Member States adopt these measures, they shall contain a reference to this Directive or be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.

2.             In order to take account of particular conditions, Member States may, if necessary, establish that the obligation to ensure accessibility as set out in Articles 4 **and 4b** has to be **\[...\]** complied with by, **\[…\]** **at the latest**, \[**5 years after adoption**\] regarding new buildings, facilities**, vehicles** and infrastructure, as well as existing buildings, facilities and infrastructure undergoing significant renovation **\[…\]** and by **\[…\]** \[**20 years after adoption**\] regarding all other existing buildings, facilities**, vehicles** and infrastructure.

Member States wishing to use any of these additional periods shall inform the Commission at the latest by the date set down in paragraph 1 giving reasons. Member States shall also communicate to the Commission by the same date an action plan laying down the steps to be taken and the timetable for achieving the gradual implementation of Article 4 **\[, including its paragraph 7\]**. They shall report on progress every two years starting from this date.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) Changes to Recitals 19b-20c and Articles 4, 4a, 4b and 15 are indicated in relation to doc. 7758/10, and changes in relation to Recitals 17a-h and Article 3 are indicated in relation to doc. 6847/10.

[\[2\]](#_ftnref2) OJ L 166, 30.4.2004, p. 1.

[\[3\]](#_ftnref3) Regulation (EC) No. 1107/2006 and Regulation (EC) No 1371/2007.

[\[4\]](#_ftnref4) OJ L 210, 31.7.2006, p.25. Regulation as last amended by Regulation (EC) No 1989/2006 (OJ L 411, 30.12.2006, p.6).

[\[5\]](#_ftnref5) OJ C 134, 7.6.2003, p.7

[\[6\]](#_ftnref6) OJ L 303, 2.12.2000, p. 16.

[\[7\]](#_ftnref7) Includes elements previously contained in Article 4a.

[\[8\]](#_ftnref8) See former Article 4(5).