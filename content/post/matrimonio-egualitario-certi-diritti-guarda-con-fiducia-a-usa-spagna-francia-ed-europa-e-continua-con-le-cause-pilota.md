---
title: 'Matrimonio egualitario: Certi Diritti guarda con fiducia a Usa, Spagna, Francia ed Europa. E continua con le cause pilota'
date: Wed, 07 Nov 2012 10:30:30 +0000
draft: false
tags: [Matrimonio egualitario]
---

Dopo la Corte costituzionale spagnola anche Maine e Washington, Maryland dicono sì al matrimonio egualitario e il governo francese ha presentato oggi la proposta annunciata in campagna elettorale. Per quanto resisterà l'Italia nel suo ruolo di baluardo della tradizione e del fondamentalismo cattolico?

  
comunicato stampa dell'Associazione radicale Certi Diritti

Roma, 7 novembre 2012.

Questa notte non c'è stata solo la riconferma di Obama, primo presidente americano favorevole al matrimonio egualitario, ma è anche la prima volta che il via libera ai matrimoni tra persone dello stesso sesso arriva con un referendum. Succede negli Stati di Washington, Maryland e Maine.

Solo il giorno prima la Corte costituzionale spagnola ha riconosciuto la legittimità del matrimonio egualitario, introdotto nell'ordinamento spagnolo nel 2005. A fare ricorso era stato il partito popolare di Mariano Rajoy, oggi al Governo ma allora all’opposizione, sostenendo che la legge che estendeva alle coppie omosessuali questo diritto violava "l'istituzione fondamentale del matrimonio" e la Costituzione. La Spagna con questa sentenza conferma che i diritti civili non devono essere legati al partito che governa, ma sono diritti fondamentali che appartengono a tutte le persone.

E stamane il Governo francese, rispettando uno degli impegni assunti in campagna elettorale, ha approvato il disegno di legge per il riconoscimento del matrimonio civile tra persone dello stesso sesso.

Continua, quindi, a passi sempre più veloci e importanti, il riconoscimento del matrimonio egualitario nei principali paesi occidentali. E l’Associazione radicale Certi Diritti si augura, e lotta per questo, affinché si estenda al più presto, a tutti i Paesi dell’Unione europea una legislazione che riconosca in pieno l’uguaglianza delle persone omosessuali e delle loro famiglie.

E in Italia? La Corte costituzionale con la sentenza 138 del 2010, ottenuta grazie alla campagna di Affermazione civile che nel 2008 Certi Diritti lanciò insieme a Rete Lenford, ha chiesto al Parlamento di legiferare in materia riconoscendo diritti alle unioni tra persone omosessuali prima mai dichiarati così esplicitamente. Riconoscimento confermato da una sentenza della Corte di Cassazione del 2012.

Il Parlamento tace, e le discriminazioni continuano.

Per quanto ancora il nostro Paese riuscirà a resistere nel suo ruolo di baluardo del peggior bigottismo antiliberale in Europa? Per noi la battaglia per il matrimonio egualitario continua. Nelle piazze come nei tribunali, con cause pilota che hanno l'obiettivo di far esprimere le Corti su quegli ambiti in cui le coppie dello stesso sesso stabilmente conviventi hanno diritto ad un trattamento uguale a quello delle coppie sposate: la pensione, il lavoro, le tasse, la salute.