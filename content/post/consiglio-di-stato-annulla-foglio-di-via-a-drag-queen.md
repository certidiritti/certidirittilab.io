---
title: 'CONSIGLIO DI STATO ANNULLA FOGLIO DI VIA A DRAG QUEEN'
date: Tue, 29 Jun 2010 13:31:16 +0000
draft: false
tags: [Comunicati stampa]
---

Pubblichiamo qui di seguito il testo integrale di un  Comunicato Stampa diffuso dal **Movimento Omosessuale Sardo**. La vicenda riguarda la sentenza del Consiglio di Stato che annulla la decisione di stampo clerico-fascista presa dalla Questura di Sassari nei confronti della Drag Queen **Nikita Ball**, che nel luglio 2009 fu fermata e cacciata con un 'foglio di via' dalla città di Sassari perchè scambiata per una prostituta.

Sulla vicenda era stata presentata un'[interrogazione parlamentare dei deputati radicali](tutte-le-notizie/423-drag-queen-fermata-per-17-ore-tra-offese-e-degrado-interrogazione.html) \- pd rimasta ad oggi senza rispsota.

**Comunicato stampa** **del Movimento Omosessuale Sardo**

Oggetto: _il Consiglio di Stato sospende il foglio di via erogato dalla Questura di Sassari ad una Drag Queen_

Michele Cicogna, in arte Nikita Balli, la drag queen toscana “fermata” lo scorso Luglio, trattenuta per 18 ore in Questura a Sassari ed allontanata con foglio di via triennale dal Questore, può finalmente ritornare nella nostra città.

A stabilirlo è una sentenza del Consiglio di Stato (N. 03781/2010, nota 1) che, in seguito al ricorso presentato dall'avv. Pietro Diaz riconoscendo la mancanza di motivazioni alla base dell'ordine di rimpatrio e la conseguente limitazione alla sua libertà personale, corregge il parere negativo del TAR della Sardegna (nota 2) e sospende, da subito, il foglio di via emesso lo scorso anno.

In poche righe il Consiglio di Stato ristabilisce quello stato di diritto e quel rispetto della legge che la Questura di Sassari e il TAR Sardegna avevano sistematicamente ignorato.

Michele fu fermato durante una retata di prostitute immigrate e, dopo una notte in questura, venne allontanato dalla città con un atto palesemente illegale. Infatti, la legge n. 1423 del 1956, modificata nel 1988, non riconosce in alcun modo totale discrezionalità al Questore nell'erogazione del foglio di via, ma impone di accertare, con “elementi di fatto”, la sussistenza dei due presupposti previsti dalla legge: che la persona svolga, o possa svolgere, “un'attività delittuosa” e che, per questo, possa rappresentare un pericolo per la sicurezza pubblica. Elementi totalmente assenti nel caso in questione considerato che Michele arrivò in Sardegna, per una settimana di vacanza in Costa Smeralda, il giorno del suo arresto e che, a bordo della sua macchina, entrò a Sassari solo poche ore prima con l'intenzione di incontrare un amico e passare una serata ad Alghero. Mai si sarebbe aspettato di essere privato di qualsiasi diritto, trattenuta per diciotto ore in Questura senza cibo e acqua e rispedito a casa con foglio di via “nel corso di un servizio finalizzato al contrasto del fenomeno dello sfruttamento della prostituzione e dell'immigrazione clandestina”. Nessuna “attività delittuosa”, poiché nemmeno quella ipotizzata, la prostituzione, risulta esserlo; nessun pericolo per la sicurezza ma un semplice travestimento che si trasforma in un “crimine” da punire con fermo di polizia, schedatura ed espulsione.

Un atto immotivato ed autoritario per il quale, l'ormai ex Questore di Sassari, non fornì alcuna giustificazione, arrogandosi il diritto di espellere dalla città chiunque desiderasse, con una discrezionalità tale che la legge 1423 non ipotizzava nemmeno prima della riforma del 1988. Una decisione politica che nulla ha a che vedere con la gestione dell'ordine pubblico o con l'amministrazione della giustizia. Grave da parte di un Questore, gravissimo da parte del TAR, organo preposto proprio a ristabilire la giustizia laddove vengono lesi diritti individuali in maniera non conforme all'ordinamento giuridico. Unico in Italia, il TAR Sardegna stravolge la legge, che nemmeno cita, confonde “attività delittuosa” (contro la legge) con “comportamenti” e, nell'ordinanza 01070 del 2009, respinge la nostra richiesta di sospensione con una motivazione allarmante: “il Questore, in maniera del tutto discrezionale, può espellere chiunque sulla base del comportamento o, persino, di ipotesi di comportamento”. Anche se tale comportamento non costituisce reato, come nel caso della prostituzione* (vedi note 3 e 4). Ci chiediamo se tale condotta sia ascrivibile a semplice ignoranza della legge e superficialità o ad un preciso intento discriminatorio, di carattere omofobico e transfobico. Elemento, questo, già presente alla base dell'emissione del foglio di via motivato unicamente dal fatto che Michele era vestito con abiti femminili e appariscenti da drag queen e, per questo, considerata una prostituta.

Un'ordinanza preoccupante, quella del TAR Sardegna, che pone la Questura al di sopra della legge, quando invece il suo compito è proprio quello di applicarla, e nega, alle e ai cittadini, quei diritti civili e politici sanciti dalla Costituzione e dalle leggi vigenti.

Lo scorso anno, a seguito di un'interrogazione del consigliere comunale Dario Satta, il sindaco di Sassari, Gianfranco Ganau, sostenne che la Questura dovrebbe considerare, oltre alla legge, il parere dell'amministrazione comunale nella gestione dei fenomeni del territorio e, rispetto al fatto specifico, ribadì la totale estraneità dell'amministrazione a politiche discriminatorie basate sull'orientamento sessuale e sull'identità di genere. Per questo chiediamo ora al Sindaco, al quale facciamo i nostri auguri per la rielezione, di condannare quell'ingiusta espulsione dalla sua città di un cittadino italiano per il semplice fatto di essere travestito. E chiediamo di condannare anche questa modalità repressiva di lotta allo sfruttamento della prostituzione e immigrazione clandestina che colpisce unicamente le vittime stesse dello sfruttamento, le prostitute, e non gli sfruttatori. Come se per combattere la discriminazione e la violenza omofobica si decidesse di arrestare e colpire le persone omosessuali e non i loro aggressori.   
Salutiamo con soddisfazione il cambio al vertice della Questura di Sassari e ci auguriamo che il nuovo Questore anteponga la legge e il diritto a pregiudizi personali o a convinzioni politiche, spesso alla base di azioni arbitrarie ed inutilmente autoritarie.

Chiediamo inoltre alle e ai consiglieri regionali, come anche agli organi di controllo e supervisione della magistratura, di intervenire con tutti gli strumenti a loro disposizione per condannare o, in qualche modo, censurare, l'operato del TAR Sardegna e per verificare la preparazione e la conoscenza della legge dei magistrati che lo compongono.

A Michele tutta la nostra solidarietà per l'ingiustizia subita e le scuse per le offese e la violenza psicologica subita.

Per il MOS  
Massimo Mele   
cell 3408885907

Note:

\* Per quanto neanche in questo caso sarebbe stato possibile erogare il foglio di via: “\[...\], si deve ritenere che in mancanza di una norma nel nostro ordinamento che punisca la prostituzione come fattispecie di reato, non è possibile emanare nei confronti delle prostitute, per il solo fatto di esercitare attività di meretricio, le misure di prevenzione previste dalla più volte citata legge n. 1423/56. (Sentenza T.A.R. Lombardia di Milano 17 settembre 2009, n. 4679 )”

  

1.  [Sentenza Consiglio di Stato sul caso Cicogna](http://www.giustizia-amministrativa.it/DocumentiGA/Consiglio%20di%20Stato/Sezione%206/2010/201003781/Provvedimenti/201002910_15.xml)
2.  [Sentenza TAR Sardegna su ricorso avv. Diaz per Michele Cicogna](http://www.giustizia-amministrativa.it/DocumentiGA/Cagliari/Sezione%201/2009/200901070/Provvedimenti/200900453_05.XML)
3.  [Sentenza TAR Milano](http://www.webalice.it/cstfnc73/ancoralucciolavincecontar.htm)
4.  [Sentenza TAR Napoli](http://www.iusna.net/index.php?option=com_content&task=view&id=128&Itemid=2)