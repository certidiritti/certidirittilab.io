---
title: 'APPELLO AL PRESIDENTE DELLA REPUBBLICA: ONOREFICIENZA PER MARIA LUISA'
date: Fri, 26 Jun 2009 13:29:07 +0000
draft: false
tags: [Comunicati stampa]
---

[**WWW.GAY.IT**](http://www.GAY.IT) **E L'ASSOCIAZIONE RADICALE CERTI DIRITTI LANCIANO UN APPELLO AL PRESIDENTE DELLA REPUBBLICA PER CONFERIRE UNA MEDAGLIA AL VALOR CIVILE A MARIA LUISA**

La ragazza che rischia di perdere un occhio per aver difeso l'amico gay merita una medaglia al valore civile. Via alla raccolta firme per il presidente della Repubblica. Moltissime adesioni "vip".

**Maria Luisa, la studentessa 26enne di Napoli** che ha rischiato di perdere un occhio per [aver difeso l'amico gay](http://www.gay.it/channel/attualita/26780/Napoli-picchiata-a-sangue-per-aver-difeso-l-amico-gay.html), è stata operata questa mattina. I medici stanno tentando di restituirle almento parazialmente la vista. Il suo gesto è eroismo puro in quanto ha messo a rischio la propria stessa vita in difesa di un coetaneo vittima della [violenza](/keyword/284/Violenza.html) omofoba. Per questo Gay.it, l'associazione radicale Certi Diritti, le [associazioni](/keyword/33/Associazioni.html) Arcigay [Napoli](/keyword/653/Napoli.html) e I-Ken Onlus, hanno lanciato una raccolta firme perché il presidente della Repubblica Giorgio Napolitano le conferisca una medaglia al valore civile.

[![Gay.it - Una medaglia al valore civile per Maria Luisa](http://images.gay.it/foto_articoli/135x135/ra/ragazzaappello.jpg "Gay.it - Una medaglia al valore civile per Maria Luisa")](#)**Il testo dell'appello** \- _"Signor Presidente, il 23 giugno 2009, nella centrale piazza Bellini a [Napoli](/keyword/653/Napoli.html), una studentessa di 26 anni, Maria Luisa Mazzarella , nella circostanza di trovarsi a difendere un proprio amico omosessuale dalle offese e dalle violenze fisiche per opera di un gruppo di coetanei, è stata lei stessa oggetto di un duro atto di [violenza](/keyword/284/Violenza.html) verbale e fisica che le ha procurato lesioni su tutto il corpo e l'ha esposta al rischio di perdere addirittura un occhio.  
In un contesto sociale in cui si moltiplicano gli atti di [violenza](/keyword/284/Violenza.html) dettati dall'odio nei confronti di cittadini con un differente orientamento sessuale e che spesso si consumano nell'indifferenza generale di coloro che vi assistono, il gesto di Maria Luisa assume un innegabile valore non solo simbolico. Ci permettiamo pertanto di chiederLe di valutare la possibilità di concedere a Maria Luisa la medaglia al valor civile per aver messo a rischio la propria stessa vita in difesa di un coetaneo vittima della [violenza](/keyword/284/Violenza.html) omofoba.  
Confidiamo nella Sua sensibilità in modo che Maria Luisa possa vedersi conferita la massima onorificenza della Repubblica.  
"_

[![Gay.it - Una medaglia al valore civile per Maria Luisa](http://images.gay.it/foto_articoli/135x135/ma/Maurizio_Costanzo.jpg "Gay.it - Una medaglia al valore civile per Maria Luisa")](#)**Sono già moltissime le adesioni** dei personaggi della politica e dello spettacolo giunte alla redazione di Gay.it. Fra i nomi dei politici spiccano quelli del portavoce di [Silvio Berlusconi](/keyword/244/Silvio-Berlusconi.html) [Daniele Capezzone](/keyword/76/Daniele-Capezzone.html), i deputati del PD Ermete Realacci e [Paola Concia](/keyword/648/Paola-Concia.html). Dal mondo dello spettacolo sono arrivate, tra le altre, le adesioni di [Maurizio Costanzo](/keyword/177/Maurizio-Costanzo.html), [Fabio Canino](/keyword/102/Fabio-Canino.html) e [Alessandro Cecchi Paone](/keyword/10/Alessandro-Cecchi-Paone.html). Dal movimento gay spiccano i nomi di [Imma Battaglia](/keyword/587/Imma-Battaglia.html) e [Franco Grillini](/keyword/113/Franco-Grillini.html) e del portavoce del Pride di [Genova](/keyword/688/Genova.html) Alberto Villa.

Medaglia al valore civile per Marialuisa  
  
**Al Presidente della Repubblica On. Dr. Giorgio Napolitano**  
  
  
Signor Presidente,  
il 23 giugno 2009, nella centrale piazza Bellini a Napoli, una studentessa di 26 anni, Maria Luisa Mazzarella , nella circostanza di trovarsi a difendere un proprio amico omosessuale dalle offese e dalle violenze fisiche per opera di un gruppo di coetanei, è stata lei stessa oggetto di un duro atto di violenza verbale e fisica che le ha procurato lesioni su tutto il corpo e l'ha esposta al rischio di perdere addirittura un occhio.  
  
In un contesto sociale in cui si moltiplicano gli atti di violenza dettati dall'odio nei confronti di cittadini con un differente orientamento sessuale e che spesso si consumano nell'indifferenza generale di coloro che vi assistono, il gesto di Maria Luisa assume un innegabile valore non solo simbolico. Ci permettiamo pertanto di chiederLe di valutare la possibilità di concedere a Maria Luisa la medaglia al valor civile per aver messo a rischio la propria stessa vita in difesa di un coetaneo vittima della violenza omofoba.  
  
Confidiamo nella Sua sensibilità in modo che Maria Luisa possa vedersi conferita la massima onorificenza della Repubblica.  
  
Cordialmente,  
i cittadini di seguito firmatari.  

[](http://www.gay.it/unamedagliapermarialuisa/)

[E' possibile firmare l'appello per il Presidente della Repubblica a questo indirizzo.](http://www.gay.it/unamedagliapermarialuisa/)

[http://www.gay.it/unamedagliapermarialuisa/](http://www.gay.it/unamedagliapermarialuisa/)