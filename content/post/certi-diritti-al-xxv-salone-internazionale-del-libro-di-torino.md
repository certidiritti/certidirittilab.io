---
title: 'Certi Diritti al XXV Salone Internazionale del libro di Torino'
date: Mon, 07 May 2012 21:56:14 +0000
draft: false
tags: [Politica]
---

Venerdì 11 maggio alle ore 21 presentazione del libro 'Certi Diritti che le coppie conviventi non sanno di avere', edito da Stampa Alternativa. Saranno presenti, tra gli altri, il co-autore Bruno De Filippis e l'assessora alle Pari Opportunità della città di Torino Maria Cristina Spinosa.

**XXV SALONE INTERNAZIONALE DEL LIBRO**  
**Sala Arancione**  
**Lingotto Fiere, Via Nizza 280 - Torino**

Presentazione del libro 'Certi diritti che le coppie conviventi non sanno di avere', breve manuale di sopravvivenza per famiglie e coppie non sposate che, in attesa di un Parlamento italiano che continua ad essere cieco sordo e muto, vuole assicurarsi il miglior futuro possibile, per sé e i propri figli.  
A cura di Bruno de Filippis, Filomena Gallo, Gabriella Friso e Gian Mario Felicetti, edito da Stampa Alternativa, 2012

Ne parlano:

Bruno de Filippis, magistrato, tra gli autori del volume  
Maria Cristina Spinosa, Assessore alle Pari Opportunità della Città di Torino  
Manuela Naldini, sociologa  
Marina Notaristefano, avvocato familiarista

coordina: Stefanella Campana, giornalista 

**[ACQUISTA IL LIBRO ONLINE >](http://www.stampalternativa.it/libri/978-88-6222-281-5/bruno-de-filippis-/certi-diritti-che-le-coppie.html)**