---
title: 'Spiaggia Arenauta: appello alla stampa locale e nazionale per scongiurare lo scempio!'
date: Fri, 06 Aug 2010 12:23:17 +0000
draft: false
tags: [Comunicati stampa]
---

**SPIAGGIA DELL’ARENAUTA: LO SCEMPIO CONTINUA. FACCIAMO APPELLO A TUTTI MASS MEDIA LOCALI E NAZIONALI A VERIFICARE COSA STA AVVENENDO ALL’ARENAUTA, ORMAI INGABBIATA, CUI E’ IMPEDITO L’ACCESSO A MIGLIAIA DI TURISTI.**  
**APPUNTAMENTO DOMENICA 8 AGOSTO DALLE ORE 10.  
Roma – Gaeta, 6 agosto 2010**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti e Presidente del Comitato per la difesa e la tutela della Spiaggia dell’Arenauta:**

“Il Consigliere del Pd di Gaeta, Fabio Luciani, sottolinea della necessità della messa in sicurezza di parte della spiaggia dell’Arenauta evocando la tragedia di Ventotene. Sia ben chiaro che nessuno di noi ha intenzione né di dimenticare né di sottovalutare i pericoli, ove ve ne fossero, che potrebbero essere causati da frane e/o caduta massi nelle zone oggetto dell’intervento del Comune di Gaeta.

Ciò che non comprendiamo, ed è anche per questo che chiediamo un incontro urgente con il Sindaco di Gaeta, è il motivo per il quale è stata messa letteralmente ingabbiata, con una lunga rete, anche una parte della spiaggia dove non ci sono né montagne, né rocce, né niente intorno che possa arrecare pericolo ai turisti. Questo è quello che non riusciamo a capire e su cui nessuno ci vuole dare una risposta. E guarda caso, gli interventi di ingabbiamento riguardano un tratto di spiaggia che non è ancora stato dato in concessione e che è frequentato da persone che ci pare proprio che debbano essere colpite perché non gradite a qualcuno.

E’ evidente che la mancata chiarezza sull’operato dei tecnici del Comune fa venire dubbi sulla bontà di questo intervento, così urgente che viene fatto dopo ben 14 anni dall’ordinanza della Capitaneria di Porto! E se i pericoli ci sono, quali sarebbero? E per quale motivo in questi 14 anni di questi pericoli non se ne è mai manifestato alcuno? Quali documenti e relazioni di tecnici, geologi, esperti possono avvalorare un intervento così invasivo e di distruzione dell’ambiente che da sempre caratterizza la Spiaggia dell’Arenauta?

**Invitiamo la stampa e i media locali a verificare tutto questo e ad essere presenti domenica 8 agosto, dalle ore 10, in quel tratto di spiaggia, per fotografare, filmare, documentare questa assurda situazione venutasi a creare in uno dei più bei tratti di mare di tutta la costa del Sud del Lazio, letteralmente deturpato e ingabbiato. Ringraziamo i giornalisti che hanno posto l’attenzione sui lati oscuri della vicenda.**

**Invitiamo alla Conferenza stampa, fissata nello stesso luogo alle ore 12, il  Consigliere comunale del Pd di Gaeta, Fabio Luciani insieme ai consiglieri regionali dei Verdi, di Idv e dei radicali che parteciperanno a questo importante incontro. Ci auguriamo inoltre che il Sindaco riceva al più presto una delegazione delle nostre Associazioni per scongiurare il prosieguo della deturpazione ambientale e paesaggistica della Spiaggia dell’Arenauta”.**

**Ufficio Stampa: 06-68979250  
cell 337-798942**