---
title: '21 MARZO, PRIMAVERA DI UNA NUOVA ERA'
date: Tue, 16 Mar 2010 11:27:18 +0000
draft: false
tags: [Comunicati stampa]
---

**_Domenica 21 marzo, primo giorno di primavera, il comitato “Sì, lo voglio”  dà appuntamento a Roma, in Piazza SS. Apostoli alle ore 17:30, a chiunque abbia a cuore la pari dignità  civile per tutti i cittadini, alla vigilia di un momento storico rappresentato dalla sentenza della Corte Costituzionale sulla legittimità dei matrimoni fra persone dello stesso sesso._**

“Siamo alla vigilia di una nuova era per i diritti civili dei gay e delle lesbiche di questo Paese, fra gli ultimi in Europa ad affrontare questa importante questione”, dichiarano le associazioni lesbiche e gay, la maggioranza delle quali si sono unite nel comitato **“Sì, lo voglio”**, che supporta e coordina tutte le iniziative legate al riconoscimento del matrimonio civile per le coppie omosessuali.

Il prossimo 23 marzo la Corte Costituzionale si pronuncerà infatti in merito alla costituzionalità di due dei quattro ricorsi presentati ai tribunali di Trento, Venezia, Ferrara e Firenze da coppie di persone dello stesso sesso, impugnando in sede giudiziale il rifiuto alle pubblicazioni ricevuti dai rispettivi comuni di residenza.

Il tutto nell’ambito della campagna di **“Affermazione Civile”,** portata avanti a livello nazionale dall’**Associazione Radicale** **Certi Diritti** e da **Rete Lenford** Avvocatura per i diritti LGBT, che ha coinvolto oltre trenta coppie.

In Italia il matrimonio civile fra persone dello stesso sesso **non è vietato** **da nessuna disposizione del Codice Civile** e risponde al principio della **“pari dignità sociale”**delle cittadine e dei cittadini, sancito dall'articolo 3 della Costituzione, nonché ai principi dell’articolo 29 che in nessun caso parla di maschio e femmina, ma solo di **“società naturale fondata sul matrimonio”**, a sua volta **“ordinato sull’uguaglianza morale e giuridica dei coniugi”**.

Domenica 21 marzo sarà l’inizio della primavera, il giorno in cui si festeggia l’uscita dai rigori invernali e si accoglie la luce che prende il sopravvento sul buio; in tale data, naturalmente simbolica, abbiamo deciso di raccoglierci in piazza per dare il benvenuto a questa sentenza che, comunque vada, sarà un evento epocale per il nostro Paese, in cui fino a poco tempo fa sarebbe stato impensabile anche solo osare ipotizzare un matrimonio fra due donne o due uomini.

**“PRIMAVERA DI UNA NUOVA ERA”** è il titolo che abbiamo scelto per la nostra manifestazione, al fine di  sancire l’inizio di una nuova fase per i diritti civili delle cittadine e dei cittadini di questo Paese, indipendentemente dal loro orientamento sessuale.

Vogliamo infatti cogliere questa occasione per discutere pubblicamente, con tutta la cittadinanza, il senso dei ricorsi e delle azioni legate ad **“Affermazione Civile”**, portate oggi avanti dal comitato **“Sì, lo voglio”**, anche grazie alla presenza delle coppie che questi ricorsi li hanno presentati.

Nostro intento è approfondire il tema del matrimonio fra persone dello stesso sesso, per informare, informarsi e condividere insieme sogni, speranze e conoscenza, magari assieme a coloro che, in coppie di sesso diverso, godono già di questo diritto.

Oltre ad interventi di esperti e di personalità  coinvolte nel tema, tutti i presenti potranno esprimersi ed esporre le proprie opinioni dibattendole in un pubblico e sereno dibattito.

Chiediamo a chi interverrà di portare con sé, oltre alle bandiere rainbow, quelle delle associazioni e delle forze politiche, soltanto una fiaccola, un fiore o un bouquet di fiori bianchi, con su un biglietto in cui si esprima quel che si desidera sul matrimonio fra persone dello stesso sesso. Al termine dell'iniziativa chi vorrà potrà, a titolo puramente personale, andare a posare il proprio fiore o il proprio bouquet, insieme al biglietto, davanti alla vicina sede della Consulta, in Piazza del Quirinale 41.

L’appuntamento è quindi **domenica 21 marzo alle ore 17:30 in Piazza SS. Apostoli a Roma**, così come in altre cento piazze italiane dove verranno allestiti dei presidi, per salutare assieme l’inizio di questa **PRIMAVERA DI UNA NUOVA ERA**.

**Comitato “Sì, lo voglio”  
**