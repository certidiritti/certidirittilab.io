---
title: 'INVITO ALLE ASSOCIAZIONI GLBT ALLA MOBILITAZIONE'
date: Tue, 28 Sep 2010 08:44:21 +0000
draft: false
tags: [Comunicati stampa, consiglio regionale, glbt, lazio, mobilitazione, OMOFOBIA, pdl, transfobia]
---

L'ASSOCIAZIONE RADICALE CERTI DIRITTI INVITA LE ASSOCIAZIONI LGBT DEL LAZIO A MOBILITARSI AFFINCHE' IL CONSIGLIO REGIONALE CALENDARIZZI E APPROVI QUANTO PRIMA LA PDL 40 CONTRO L'OMOFOBIA E LA TRANSFOBIA.

Domani inizia al Consiglio Regionale del Lazio, presso la III Commissione, l'iter di discussione della proposta di legge regionale n. 40 a firma dei Consiglieri Regionali radicali Giuseppe Rossodivita e Rocco Berardo contro l'omofobia e la transfobia. L'Associazione Radicale Certi Diritti si augura che la classe politica regionale si attivi nei fatti, approvando questo provvedimento che dovrà poi essere calendarizzato dalla IX Commissione Pari Opportunità e Affari Sociali, per poi giungere in aula. Con una lettera di Certi Diritti alle principali associazioni lgbt del Lazio è stato chiesto di mobilitarsi affinchè i Consiglieri regionali del Lazio vengano sensibilizzati su questo importante provvedimento che nella Regione Liguria è stato approvato lo scorso anno.  
Al seguente link il testo integrale. della Legge regionale n.40 contro l'omofobia e la transfobia.

[PDL regione Lazio contro discriminazione identità digenere.pdf](http://www.certidiritti.org/wp-content/uploads/2010/09/pdl_regione_lazio_contro_discriminazione_identit_di_genere.pdf)