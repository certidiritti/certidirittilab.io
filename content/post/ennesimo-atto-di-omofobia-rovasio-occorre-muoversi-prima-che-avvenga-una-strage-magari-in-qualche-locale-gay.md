---
title: 'ENNESIMO ATTO DI OMOFOBIA, ROVASIO: OCCORRE MUOVERSI PRIMA CHE AVVENGA UNA STRAGE, MAGARI IN QUALCHE LOCALE GAY'
date: Mon, 24 Aug 2009 11:51:14 +0000
draft: false
tags: [Comunicati stampa]
---

#### Roma, 24 agosto 2009

L’associazione Radicale Certi Diritti esprime tutta la sua solidarietà e vicinanza all’amico Daniele Priori, vice presidente di Gaylib aggredito a Rimini, vittima dell’ennesimo caso di omofobia. Come ricordato già ieri riteniamo che la situazione è gravissima, gli atti violenti di omofobia che stanno caratterizzando l’estate italiana 2009 non possono lasciare nell’indifferenza la classe politica e di governo del nostro paese che continua, invece, a comportarsi in modo ipocrita e attende che si verifichi una strage, magari all’interno di un locale gay. Bisogna, da subito, avviare iniziative,  campagne di informazione, di educazione, assistenza e aiuto a tutti i livelli. L’Italia è forse l’unico paese in Europa ad non essersi attivato in tal senso e le conseguenze potrebbero essere tragiche.