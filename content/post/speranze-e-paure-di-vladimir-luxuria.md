---
title: 'SPERANZE E PAURE di VLADIMIR LUXURIA'
date: Tue, 13 Oct 2009 11:54:53 +0000
draft: false
tags: [Comunicati stampa]
---

**_Pubblichiamo il seguente intervento di Vladimir Luxuria, pubblicato oggi, martedì 13 ottobre, sul quotidiano 'L'altro' e nel sito www.vladimirluxuria.it_**

**SPERANZE E PAURE di Vladimir Luxuria**

Stavo riascoltando un cd di qualche anno fa, dal titolo “Gay Right Compilation”, un album che raccoglie inni musicali sui diritti di gay, lesbiche e trans. Tra questi brani ce n’è uno, dal titolo “Casi strani” cantato da Francesco Bertolini. Ho ricordato: Francesco aveva 26 anni, si era trasferito a Roma dalla bellissima zona sarda della Maddalena, sognava di diventare famoso come cantante e che le sue foto comparissero sui giornali. Francesco sui giornali ci finì per un altro motivo: la notte del 15 luglio del 2001 in un cespuglio a Montecaprino con la testa fracassata.

Qualcuno parlò di rapina ma purtroppo la matrice dell’omicidio era chiara: omofobia. Il luogo era un noto luogo di aggregazione gay e non si fracassa la testa a qualcuno per togliere soldi e cellulare, sarebbe bastata una minaccia, Francesco era esile ed effeminato. Era quello che la giurisdizione USA definisce “hate crime”, crimine di odio, dove la rapina serve solo per depistare le indagini o come ultimo gesto di disprezzo verso la vittima. Roma detiene il triste primato di città con il più alto numero di “_omocidi_, la omo/lesbo/transfobia non è nulla di nuovo (se non la sua drammatica sequenza), l’odio nei nostri confronti è storico, sia quando ci nascondevamo e qualcuno ci scovava per condannarci ai roghi o ai campi di concentramento nazisti, sia oggi quando dimostriamo il nostro affetto (come per la coppia aggredita davanti al Gay Village) o quando non possiamo celare ciò che siamo (come nel caso di tante transgender, soprattutto quando sono nella prima fase del processo transitorio).

Nel 1971 lo psicologo George Weinberg coniò il termine “omofobia”, la parola servì solo a definire e riconoscere un fenomeno tragicamente esistente ma senza nome, ai danni di persone senza voce e senza diritti.  Abbiamo dovuto aspettare il susseguirsi di episodi di violenza in tutta Italia per vedere accendere i riflettori su una questione in realtà già da anni proposta da me, Titti DE Simone, Franco Grillino, Gianpaolo Silvestri, la sinistra e i radicali…

Noi proponemmo nella scorsa legislatura l’allargamento di una legge già esistente (e non un reato a hoc sull’omofobia come proposto oggi dall’Italia dei Valori), la legge Mancino del 1993 che prevedeva misure urgenti contro la discriminazione razziale, etnica e religiosa. Il centro-destra, i teo-dem e l’Udeur votarono contro e Paola Binetti (l’assenteista che ha fatto approvare lo scudo fiscale!) votò su questo la sfiducia al governo Prodi.

Io sono una persona assolutamente ottimista e positiva e per fortuna guardo avanti e attorno, non ho la testa rivolta al passato, per cui cerco di collaborare per quello che posso a questo nuovo tentativo di legiferare in tal senso, anche se non sono più una parlamentare.

La Commissione Giustizia alla Camera ha approvato un testo unificato, di cui è relatrice l’on. Paola Concia del PD, che sarà nei prossimi giorni in discussione e votazione in Aula. Si è accantonata la legge Mancino e il reato “omofobia” creato appositamente per inserire invece nell’articolo 61 del Codice Penale l’aggravante nei delitti non colposi (cioè quelli intenzionali) quando viene colpita la vita e l’incolumità, la personalità e la libertà individuale, e (e questo mi interessa molto) la “libertà morale” per finalità inerenti all’orientamento o alla discriminazione sessuale della persona offesa.

Rispetto a questo testo nutro speranze e paure.

La mia prima paura, lo dico sinceramente, è che alla fine non se ne faccia nulla… lo so, sembro una Cassandra, ma perdonatemi, per esperienza troppe volte sembrava che qualcosa stesse per succedere, che era lì per lì…e poi.. ingerenze vaticane, smidollati parlamentari, svilimenti della laicità…

Per carità, sono sicura che questa volta verrò sbugiardata, meglio, la gioia sarà più grande!

Un’altra paura, questa purtroppo più concreta, è che venga promulgata la prima legge discriminatoria nei confronti delle persone transgender: il testo unificato contempla infatti tra le vittime solo le “finalità inerenti all’orientamento e discriminazioni sessuali” cancellando “l’identità di genere” prevista in prima stesura e osteggiata dalla destra in Commissione.

Non è una questione di lana caprina, le parole hanno un loro valore e soprattutto in un testo di legge persino una virgola può essere fondamentale. Esiste una terminologia usata in ambito psicologico, medico e scientifico in tutto il mondo: gay, lesbiche e trans non sono la stessa cosa, abbiamo delle specificità diverse. Per quello che riguarda il rapporto con il dato anagrafico e con il proprio corpo, gay, lesbiche ed eterosessuali (orientamento sessuale) non hanno quella conflittualità delle e dei trans che non si identificano totalmente o parzialmente nel genere della nascita e non si conformano al dato culturale del ruolo di genere.

Se non si specifica la nostra identità (se non piace identità di genere metteteci quello che volete: trans, transessuale, transgender, transgenere, disforia di genere, extra taxa…), l’importante è inserirci, altrimenti vedremo l’avvocato difensore di uno che ha preso a sprangate una trans scagionare il criminale dalle aggravanti previste invece se la vittima è lesbica, gay o bisessuale.

Abbiamo incontrato la Ministra alle pari Opportunità, Mara Carfagna, che si è impegnata a battersi in Aula sul reinserimento di noi trans sia nella legge sia in una campagna di pubblicità progresso dopo quella presto in onda sulla omofobia su reti private e pubbliche in orari di massimo ascolto.

Qualcuno ha storto il muso su questo incontro, qualcuno ha contestato la sua lettera in piazza alla manifestazione “Uguali” di sabato scorso.

Io credo, invece, che su questi temi dobbiamo comportarci come hanno fatto le associazioni in Gran Bretagna mettendo in competizione i _tories e i labour_ andando al rialzo sulle proprie rivendicazioni e non sempre al ribasso come avviene da noi. Raccogliamo le sfide e i cambiamenti della società, delle persone e dei politici, cerchiamo di capire se anche da noi è possibile una presa di posizione all’interno del centro-destra capace di mettersi in discussione, aprire su certi temi, difendere la laicità e la Costituzione,  capire oggi il problema della violenza e magari domani il problema di chi ama ma non è tutelato da leggi dello Stato in nome della difesa del matrimonio tradizionale, che in realtà non dovrebbe essere difeso da chi ci crede al punto tale da volerlo anche per sé, i matrimoni si difendono dal precariato e dalla disoccupazione, veri motivi per cui in Italia abbiamo il più alto numero di figli che fino a tarda età vivono a casa con mamma e papà.

Non si può attaccare il Ministro Carfagna per non averci mai voluto incontrare prima e poi fischiarla se scrive una lettera di impegno che viene letta in sua assenza (e questo è colpa nostra per ché la abbiamo invitata tardi!)

Non ci conviene continuare ad avere solo nella sinistra, nel PD, nei radicali gli unici interlocutori, così una parte politica si impigrisce (tanto questi continueranno a votare noi…) e l’altra si deresponsabilizza. Il risultato di tutto questo è “_ideologizzare_” e schierare_partiticamente_ quei bisogni di tanti che magari della politica non gliene frega neanche niente!

Dobbiamo liberarci di reciproche diffidenze, conoscerci, parlarci.

Non mi interessa chi si prenderà il merito di una legge sull’omofobia, voglio che venga fatta.

Lo chiedo per quel piccolo risarcimento alle tante vittime lesbo, gay e trans della storia che come in un’antologia di Spoon River, aspettano solo che qualcuno li ascolti.