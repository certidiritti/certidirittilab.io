---
title: 'TRANSESSUALE SUORA VEGLIA PER I GAY, NO DELLA CURIA'
date: Sat, 05 Apr 2008 08:11:50 +0000
draft: false
tags: [Comunicati stampa]
---

da Corriere.it

**TORINO** — Cristina ha 40 anni, l'abito lungo e nero, il velo che le incornicia il viso. Si sveglia all'alba per le Lodi e prega molto, fino a sera, alternando il raccoglimento con l'assistenza e il lavoro. È suora francescana, castità, obbedienza e povertà, ma ha dovuto cercarsi una chiesa che la accogliesse perché la sua, quella cattolica romana, le ha «chiuso le porte»: quando è nata, Cristina era maschio, Marco, ma a 20 anni ha cambiato sesso, per poi trovare la sua vocazione. Un caso unico, difficile da accettare per i vescovi fedeli al Papa.

**Ora è una religiosa della Riconciliazione, la piccola comunità monastica nata in Italia dalla chiesa vetero-cattolica**, vicina a quella anglicana. È stata sua, ieri sera, la testimonianza più toccante alla veglia di preghiera torinese per le vittime di omofobia e transfobia. La serata avrebbe dovuto svolgersi nella chiesa cattolica di San Pietro a Cavoretto, ma ieri alle 18 è arrivato il contrordine: «Mi hanno comunicato che l'appuntamento era annullato — dice don Paolo Fini —. Avevo dato volentieri la chiesa, poi le stesse persone hanno preferito rinviare, non era più opportuno. Volevano che tutto avvenisse nella discrezione ma questa condizione è mancata». La veglia si è svolta nella chiesa vetero-cattolica. «Sono nata a Sezze e cresciuta dalle suore — dice Cristina — perché mamma e papà lavoravano. Fin da piccola mi sono sentita femmina. Il ricordo più brutto? Quando mi hanno sequestrato la Barbie. A 13 anni ho parlato a mia madre, mi ha accettato. Appena maggiorenne mi sono operata». Cristina si commuove di fronte alle immagini di San Francesco: «Il Cantico delle Creature è il primo testo che ho imparato, e non lo dimentico. Ma la mia vita da cattolica è diventata un inferno. Ho seguito un corso ad Assisi per le vocazioni, ho frequentato le Clarisse, poi mi hanno indirizzata al mio vescovo, a Latina. Saputa la verità ha detto parole terribili: "Sei nata uomo, non potrai né sposarti né prendere i voti". Sono scappata in Inghilterra, in un monastero anglicano. Oggi sono suora francescana e vetero-cattolica. Parlo con Dio, assisto, nel mio cuore resta il sogno di contemplare il Signore in monastero. La castità non mi pesa, la solitudine sì».

Vera Schiavazzi  
**05 aprile 2008**