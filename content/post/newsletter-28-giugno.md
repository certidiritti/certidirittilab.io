---
title: 'Newsletter 28 Giugno'
date: Wed, 31 Aug 2011 10:13:02 +0000
draft: false
tags: [Politica]
---

**![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)**

Con giugno si chiude il mese dell’orgoglio lgbt(e), un mese molto importante.   
A 42 anni dalla [rivolta di Stonewall](http://www.google.it/url?sa=t&source=web&cd=1&ved=0CCUQFjAA&url=http%3A%2F%2Fit.wikipedia.org%2Fwiki%2FMoti_di_Stonewall&rct=j&q=stonewall&ei=q6kITtXDIMXssgap37nFDA&usg=AFQjCNE992CSOP6-xotnya3ROve-7fZm-g&sig2=1kgR2i3xGaWfGiRuvBc1xQ&cad=rja) del 27 giugno 1969, lo stato di New York ha approvato il matrimonio tra persone dello stesso sesso, diventando il sesto stato Usa a legalizzare le unioni gay.

Intanto il presidente Obama, che in un incontro pubblico aveva dichiarato "le coppie gay meritano gli stessi diritti civili di tutte le altre coppie", la settimana prossima riceverà per la prima volta alla Casa Bianca il Gay Pride di Washington e la Commissione Diritti Umani dell'Onu ha votato una risoluzione che afferma l'eguaglianza dei diritti a prescindere dall'orientamento sessuale e dall'identità di genere.

Ma qualcosa si muove anche in Italia. Dopo il grande successo dell’Europride di Milano, sabato scorso si sono svolti il Pride di Napoli, al quale ha partecipato il sindaco De Magistris, e quello di Milano, che per la prima volta ha avuto il patrocinio del comune. A Milano il giorno dopo si è anche celebrato il primo matrimonio gay in una chiesa valdese.

Dopo la lettera aperta inviata al sindaco di Bologna dal responsabile delle questioni europee di Certi Diritti che a settembre sposerà il suo compagno in Spagna, Certi Diritti Milano ne scrive una all’assessore Majorino: per l’anagrafe dei diritti si prenda come modello Torino e non Padova.

Buona lettura

ASSOCIAZIONE
============

**Stato di New York approva il matrimonio gay.  
[E’ il sesto stato Usa >](tutte-le-notizie/1179-stato-di-new-york-approva-nozze-gay-e-il-sesto-stato-usa.html)**

**Diritti gay: la rivoluzione di Obama sia un esempio.  
[Leggi >](tutte-le-notizie/1178-diritti-gay-la-rivoluzione-civile-di-obama-sia-un-esempio.html)**

**Matrimonio gay in chieda: i valdesi rompono il tabù.   
[Di Natalia Aspesi >](rassegna-stampa/1173-matrimonio-gay-in-chiesa-i-valdesi-rompono-il-tabu.html)**

**Certi Diritti Milano scrive all’assessore Majorino:   
[anagrafe pubblica come a Torino >](tutte-le-notizie/1182-certi-diritti-scrive-a-majorino-anagrafe-diritti-come-a-torino.html)**

**Lettera aperta al sindaco di Bologna Merola.   
[Leggi >](tutte-le-notizie/1169.html)**

**Voto della Camera su Pdl anti-omofobia rinviato a luglio.   
[Leggi >](tutte-le-notizie/1168.html)**

**Voto storico all'ONU sui diritti delle persone LGBT.  
[Leggi >](tutte-le-notizie/1172.html)**

**Diritti dei gay, l'appello dei radicali: è ora che l'Italia si adegui alla risoluzione Onu.   
[Leggi >](http://www.iltempo.it/2011/06/17/1265828-diritti_appello_radicali_italia_adegui_alla_risoluzione.shtml)**

**Certi Diritti ha partecipato al Gay Pride di Budapest.   
[Leggi >](http://www.radicali.it/20110620/certi-diritti-ha-partecipato-al-gay-pride-di-budapest)**

**Pride di Barcellona ospita comunità gay di Tel Aviv.   
[Leggi >](tutte-le-notizie/1180-pride-di-barcellona-ospita-comunita-lgbt-di-tel-aviv.html)**

**Caso lesbica ugandese, Certi Diritti: si mobiliti anche l'Italia.  
[Leggi>](tutte-le-notizie/1166.html)**

**Sciopero della sete, Pannella in ospedale.   
[Leggi>](http://www.agenziaradicale.com/index.php?option=com_content&task=view&id=12484&Itemid=53)**

RASSEGNA STAMPA
===============

**Milano, sfila il gay pride: anche noi vogliamo il matrimonio.  
[Leggi >](rassegna-stampa/1181-milano-sfila-il-gaypride-anche-noi-vogliamo-il-matrimonio.html)**

**Campania Pride 2011, a Napoli sindaco De Magistris in testa al corteo.  
[Leggi >](http://www.videocomunicazioni.com/notizie/lorgoglio-gay-sfila-a-napoli-in-testa-al-corteo-il-sindaco-de-magistris.html)**

**Lui cambia sesso e il giudice lo fa divorziare dalla moglie‎.   
[Leggi >](http://www.gay.it/channel/attualita/31977/Lui-cambia-sesso-e-il-giudice-lo-fa-divorziare-dalla-moglie.html)**

**"Fuori meridionali e gay"**Così le 'black-list' delle interinali. **  
[Guarda il video >](http://inchieste.repubblica.it/it/repubblica/rep-it/2011/06/21/video/fuori_meridionali_e_gay_cos_le_black-list_delle_interinali-17998833/1/)**

**Operaio insultato dai colleghi perché gay. Azienda condannata per mobbing omofobo.  
[Leggi >](http://mattinopadova.gelocal.it/cronaca/2011/06/16/news/operaio-insultato-dai-colleghi-perche-gay-azienda-condannata-per-mobbing-omofobo-4448090)**

**I gay? Aberrazione genetica, una patologia da curare con le medicine.   
Parola del sindaco di Sulmona.  
[Leggi >](http://www.queerblog.it/post/11407/i-gay-aberrazione-genetica-una-patologia-da-curare-con-le-medicine-parola-del-sindaco-di-sulmona)**

**Formigoni vs Lady Gaga.  
[Leggi >](http://www.ilfattoquotidiano.it/2011/06/14/formigoni-vs-lady-gaga/118074/)**

**Diritti e orientamento sessuale: l’Emilia Romagna e il coraggio di Vasco Errani.  
[Leggi >](http://www.ilfattoquotidiano.it/2011/06/18/diritti-e-orientamento-sessuale-lemilia-romagna-e-il-coraggio-di-vasco-errani/119434/)**

**Merola incontra la comunità gay: nessuna volontà di allargare al centro. **Il sindaco: «In Italia manca il riconoscimento delle coppie di fatto e il diritto ai matrimoni gay, io sono favorevole».**   
[Leggi >](http://corrieredibologna.corriere.it/bologna/notizie/politica/2011/20-giugno-2011/merola-incontra-comunita-gay-nessuna-volonta-allargare-centro-190906515913.shtml)**

_Omofobia,_ nei pronto soccorso toscani sarà "Codice Rosa".   
[Leggi >](http://www.gay.it/channel/attualita/31974/Omofobia-nei-pronto-soccorso-toscani-sara-Codice-Rosa.html)

**I matrimoni gay sono una pericolosa follia. Parola di politico francese.   
[Leggi >](http://www.queerblog.it/post/11414/i-matrimoni-gay-sono-una-pericolosa-follia-parola-di-politico-francese)**

**Gli elettori del Liechtenstein approvano una legge che riconosce la coppia gay.   
[Leggi >](http://www.queerblog.it/post/11430/gli-elettori-del-liechtenstein-approvano-una-legge-che-riconosce-la-coppia-gay)**

**Ilarion Alfeev, vescovo ortodosso, contro i diritti dei gay e delle donne durante una visita in Egitto.   
[Leggi >](http://www.queerblog.it/post/11416/ilarion-alfeev-vescovo-ortodosso-contro-i-diritti-dei-gay-e-delle-donne)**

**La chiesa anglicana consentirà ai preti gay di diventare vescovi.   
[Leggi >](http://www.fanpage.it/la-chiesa-anglicana-consentira-ai-preti-gay-di-diventare-vescovi/)**

**Al Gay Pride 2011 a Zagabria migliaia di persone sfilano per i diritti gay.   
[Leggi >](http://www.gaywave.it/articolo/al-gay-pride-2011-a-zagabria-migliaia-di-persone-sfilano-per-i-diritti-gay/31863/)**

_Gay_ Pride 2011 a Tel Aviv, migliaia di partecipanti.   
[Leggi >](http://../Downloads/%20http:/www.gaywave.it/articolo/gay-pride-2011-a-tel-aviv-migliaia-di-partecipanti/31757/)

**Ghana: lascia che siano gay.  
[Leggi >](http://it.globalvoicesonline.org/2011/06/ghana-lascia-che-siano-gay/)**

**Pastore 'libera e aiuta' giovani ragazzi gay – (in inglese)  
[Guarda il video >](http://www.queerblog.it/post/11412/pastore-libera-e-aiuta-giovani-ragazzi-gay-video-shock)**

**Arcivescovo di New York paragona il matrimonio gay ai regimi comunisti.  
[Leggi >](http://www.queerblog.it/post/11398/arcivescovo-di-new-york-paragona-il-matrimonio-gay-ai-regimi-comunisti)**

**Matrimoni gay, si vota a New York L'Onu approva risoluzione sull'uguaglianza.   
[Leggi >](http://www.repubblica.it/esteri/2011/06/17/news/matrimoni_gay_si_vota_a_new_york_bloomberg_i_repubblicani_approvino-17829553/)**

**Gay, approvata storica risoluzione dal Consiglio diritti Onu.  
[Leggi >](http://www.tmnews.it/web/sezioni/top10/20110617_143130.shtml)**

STUDI, RICERCHE E MATERIALI INFORMATIVI
=======================================

**Libro bianco prodotto dal progetto Ahead sul ruolo delle autonomie locali nella lotta all'omofobia. Può essere una buona base di lavoro per chi si volesse impegnare a livello locale.   
[Leggi >](http://www.comune.torino.it/politichedigenere/bm~doc/libro-bianco_ita_web.pdf)**

**Omofobia. Vogliono uccidere la legge.  
[Leggi le pregiudiziali di Costituzionalità](http://www.mariomieli.org/spip.php?article2327)**

**[>](http://www.mariomieli.org/spip.php?article2327)**  
**Diritti delle persone lgbt: l'Italia è a zero****.   
[Leggi >](http://../Downloads/%20http:/www.agoravox.it/Diritti-delle-persone-lgbt-l.html)**

**L'omofobia? Omosessualità repressa, lo dice uno studio.  
[Leggi >](http://../Downloads/%20http:/www.gay.it/channel/sesso/31982/L-omofobia-Omosessualita-repressa-lo-dice-uno-studio.html)**

**Francia. Omofobo e codardo. **Uno studio sull’omofobia.**   
[Leggi >](http://www.west-info.eu/it/omofobo-e-codardo-francia-sos-homophobie-internet-transgender/)**

LIBRI E CULTURA
===============

**Amore e vita gay: ecco le differenze con quella etero.  
[Guarda le Foto >](http://www.queerblog.it/post/11396/amore-e-vita-gay-ecco-le-differenze-con-quella-etero-foto)**

** “Sicilia Queer Film Fest”: il primo festival di cinema gay, tutto siciliano.  
[Leggi >](http://www.newnotizie.it/2011/06/18/sicilia-queer-film-fest-il-primo-festival-omosex-tutto-siculo/)**

**Uma Thurman con Human Rights per i matrimoni gay.   
[Leggi >](http://www.diredonna.it/uma-thurman-con-human-rights-per-i-matrimoni-gay-43699.html)**

**Flaminia Nucci, _[L’amore che non osa dire il suo nome](http://www.magiedizioni.com/magiedizioni/amore_che_non_osa_dire_il_suo_nome.html)_****_Psicologia dell'omosessualità maschile e femminile_****, Magi Ed., 15,00**

**Da rileggere:**  
**Simona Argentieri, _[A qualcuno piace uguale](http://www.einaudi.it/libri/libro/simona-argentieri/a-qualcuno-piace-uguale/978880620146)_, Einaudi 2010, € 10,00  **

**[Hai rinnovato l’iscrizione a Certi Diritti per il 2011? Basta un click >](partecipa/iscriviti-e-contribuisci.html)**

[www.certidiritti.it](http://www.certidiritti.it/)
==================================================