---
title: 'APPELLO AI PARTITI E AI SENATORI: IRRICEVIBILE UNA LEGGE SENZA STEPCHILD ADOPTION'
date: Sun, 07 Feb 2016 13:27:38 +0000
draft: false
tags: [Diritto di Famiglia]
---

[![coppia-gay_AFFIDAMENTO](http://www.certidiritti.org/wp-content/uploads/2015/10/coppia-gay_AFFIDAMENTO-300x175.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/10/coppia-gay_AFFIDAMENTO.jpg)Roma - In relazione ai retroscena giornalistici sulle scelte interne al PD e alle notizie provenienti dal Movimento Cinque Stelle di queste ore che lasciano presagire la possibilità di una legge sulle unioni civili approvata senza la stepchild adoption, le associazioni Lgbt unite ribadiscono che il testo attuale è già un compromesso rispetto alle rivendicazioni del movimento e che ulteriori passi indietro sono giudicati irricevibili.

La stepchild adoption è una misura di civiltà che non concede nuovi diritti alle persone Lgbt di questo Paese, ma semplicemente riconosce un minimo di tutela a cittadini italiani minorenni che non possono essere discriminati per l'orientamento sessuale dei propri genitori. Questa non lo diciamo noi, ma lo dicono le numerose sentenze dei tribunali di questo Paese, quelle della corte europea dei diritti dell'uomo che già ha richiamato il nostro Paese perché inadempiente sul campo dei diritti umani.

Una legge sulle unioni civili senza la stepchild adoption sarebbe manifestamente incostituzionale e aprirebbe a una valanga di ricorsi. Chi vuole una legge sulle unioni civili con formule di questo genere lo fa contro le persone Lgbt, le famiglie, le loro figlie e i loro figli, non a loro favore.

Agedo Anddos Arc ARCI Arcigay Arcilesbica Associazione radicale Certi Diritti Azione Gay e Lesbica CCO Mario Mieli Circolo Tondelli CondividiLove Coordinamento Torino Pride Edge Equality Italia Famiglie Arcobaleno Gay Center Gaynet Italia I Mondi Diversi Ireos La Fenice Gay Luiss Arcobaleno MIT Nuovi Diritti Cgil Nazionale Polis Aperta Rete Genitori Rainbow UAAR