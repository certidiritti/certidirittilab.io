---
title: '"Se oggi siamo qui a festeggiare..."'
date: Tue, 23 Jul 2013 18:03:18 +0000
draft: false
tags: [Politica]
---

Tratto da un intervento di Clara Comelli, già Presidente dell'Assocazione Radicale Certi Diritti

Se oggi siamo qui a festeggiare questa unione è merito solo e soltanto di Laura.

Infatti è solo grazie alla sua perseveranza ed al suo grande impegno civile e politico che oggi si può affermare che è stato fatto il primo grande e decisivo passo per il riconoscimento del matrimonio egualitario, e che, da adesso in poi, non si potrà più tornare indietro sulla strada dell'eguaglianza tra le coppie eterosessuali e quelle omosessuali.

Non deve trarre in inganno e non deve sminuire la valenza del nostro essere qui , il fatto che quello che si è celebrato oggi sia solo e soltanto un matrimonio simbolico.

Lo sappiamo tutti che lo stesso non avrà alcuna conseguenza a livello giuridico e non darà a Laura ed Ivan nessun tipo di diritto.

Ciònondimeno aver avuto dal Comune il permesso di celebrare un matrimonio simbolico, direi che significa che questa società è ormai pronta a riconoscere che anche un uomo e una donna devono avere il diritto di sposarsi e di formare una famiglia come noi omosessuali.

Basta con le discriminazioni, basta dire che gli etero sono contronatura e che non possono adottare i bambini , basta chiedere loro di pagare le tasse e non concedere loro il sacrosanto diritto di vedersi riconosciuti quando sono in coppia.

E' l'amore che crea un'unione e una famiglia e oggi Laura ed Ivan sono qui a dimostrarci che anche loro sono capaci di stare insieme, di volersi bene e di provvedere l'uno all'altra. Basta con questi luoghi comuni che vogliono gli eterosessuali come delle persone che fanno sesso promiscuo e che non riescono ad avere legami duraturi come noi omosessuali.

Dobbiamo raggiungere il livello di democrazia degli altri Paesi Europei , dove già da molti anni le coppie eterosessuali possono sposarsi ed adottare o anche soltanto riconoscere i figli del o della partner.

Ad oggi sono molte le coppie eterosessuali che decidono di andare all'estero per sposarsi. Anche in Inghilterra la Regina Elisabetta ha di recente detto "Yes".

Non è possibile che in Italia i nostri politici siano genuflessi ai voleri del Vaticano. Basta con le ingerenze di una Chiesa che continua a dire che la famiglia è una e cioè quella creata da due donne o da due uomini.

Ringrazio quindi Laura per avermi invitata. Lei sa che la mia associazione " Diritti certi per gli etero" è da sempre impegnata per il riconoscimento del matrimonio tra uomini e donne. E' dovere morale e civile di noi omosessuali impegnarci per i nostri fratelli e sorelle etero.

Ecco perché siamo oggi qui ed ecco perché continueremo a starvi vicino fino a quando avremo ottenuto per voi gli stessi diritti di cui noi già godiamo.

GOD SAVE THE QUEEN