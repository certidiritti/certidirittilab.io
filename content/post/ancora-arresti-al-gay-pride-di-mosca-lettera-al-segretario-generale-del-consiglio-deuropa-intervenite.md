---
title: 'Ancora arresti al Gay Pride di Mosca. Lettera al Segretario Generale del Consiglio d''Europa: intervenite!'
date: Tue, 29 May 2012 08:39:20 +0000
draft: false
tags: [Russia]
---

Roma, 27 maggio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Oggi ci è giunta da Mosca la notizia che al Gay pride promosso da alcune associazioni e attivisti dei diritti civili, davanti alla sede del Municipi della capitale russa, sono stati arrestati diversi militanti, tra questi il promotore del gay pride, Nikolaj Alexeyev, già aggredito e fermato negli anni scorsi per lo stesso motivo. Il Parlamento Europeo proprio di recente ha chiesto con una Risoluzione il rispetto del diritto di manifestare e della libertà di espressione anche per le comunità lgbt dei paesi dell’Est europeo, Russia inclusa.

L’Associazione Radicale Certi Diritti ha inviato oggi una lettera al Segretario Generale del Consiglio d’Europa , di cui la Federazione Russa fa parte, denunciando, tra l’altro che “non si può più tollerare questo comportamento gravissimo delle autorità russe che vìola tutte le regole, in primis la Carta Europea dei Diritti dell’Uomo e delle Libertà’”. Nella lettera si chiede di  attivare quanto prima a livello di Comitato dei Ministri  una iniziativa congiunta di tutti i paesi civili e democratici contro questa vera e propria barbarie. La Russia, in tema di diritti Lgbt ha sistematicamente violato la Raccomandazione CM/REC (2010)5  approvata dal Comitato dei Ministri  che dà delle indicazioni ben precise agli Stati membri sulle misure volte a combattere la discriminazione fondata sull’orientamento sessuale o sull’identità di genere.

Proprio nei giorni scorsi il Segretario Generale del Consiglio d’Europa , Thorbjørn Jagland, ha ricordato con preoccupazione che la discriminazione e i pregiudizi nei confronti delle persone lesbiche, gay, bisessuali e transgender non possono essere ignorati e ha anche denunciato che “le persone LGBT devono ancora affrontare atteggiamenti intolleranti e barriere sociali nella maggior parte, se non nella totalità, degli Stati membri del Consiglio d’Europa. Recenti sentenze della Corte europea dei diritti dell’uomo segnalano divieti ingiustificati, oppure ostacoli amministrativi imposti alle parate del gay pride denunciando il comportamento di illiberale di alcuni di questi paesi”.  
Lo stesso Parlamento Europeo con la Risoluzione approvata giovedì scorso denuncia questi comportamenti di alcuni paesi dell’Est europeo e chiede che quanto prima vengano superati.