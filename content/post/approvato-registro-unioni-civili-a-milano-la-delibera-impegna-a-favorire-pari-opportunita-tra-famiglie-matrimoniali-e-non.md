---
title: 'Approvato registro unioni civili a Milano: la delibera impegna a favorire pari opportunità tra famiglie matrimoniali e non'
date: Fri, 27 Jul 2012 08:52:22 +0000
draft: false
tags: [Diritto di Famiglia]
---

Salvo il cuore della delibera che impegna il comune a superare situazioni di discriminazione e favorire pari opportunità tra famiglie matrimoniali e non matrimoniali.

Comunicato dell’Associazione Radicale Certi Diritti

Milano, 27 luglio 2012

L’Associazione Radicale Certi Diritti saluta con favore l’approvazione del registro sulle unioni civili a Milano che rende la città più accogliente, laica ed europea. Un estenuante dibattito consiliare ha trasformato una delibera asciutta sul modello torinese in una più articolata che prevede un autonomo registro delle unioni civili riferito però all'art 4 del DPR 223 del 1989 che parla di famiglia anagrafica. A parte la scelta eccessivamente burocratica rimane integro il cuore del provvedimento che impegna il Comune a garantire “condizioni non discriminatorie” di accesso ai suoi servizi e nelle materie di propria competenza.

L’Associazione Radicale Certi Diritti, insieme ad altre realtà milanesi, aveva quasi completato una raccolta firme su una delibera basata sul modello torinese che ha certamente contribuito ad accelerare i tempi dell’approvazione del registro. Salutiamo con favore la scelta di lasciare al Consiglio comunale la titolarità della materia poiché questo ha permesso di sviluppare un vasto dibattito nelle istituzioni e in città.

Da Milano rilanciamo ora la battaglia per il matrimonio egualitario che consenta a tutti i cittadini, a prescindere dal loro orientamento sessuale, il pieno accesso all’istituto del matrimonio civile e che permetta così di sanare la più odiosa delle discriminazioni contenute nel nostro ordinamento giuridico.