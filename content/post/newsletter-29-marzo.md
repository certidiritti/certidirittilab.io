---
title: 'Newsletter 29 Marzo'
date: Fri, 30 Mar 2012 15:08:28 +0000
draft: false
tags: [Politica]
---

![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)

qualche giorno fa [la Questura di Reggio Emilia ha rilasciato il permesso di soggiorno a un ragazzo uruguayano, sposato in Spagna con un italiano](questura-rilascia-permesso-di-soggiorno-a-uruguayano-sposato-in-spagna-con-italiano-che-aveva-vinto-ricorso-al-tribunale-di-reggio-emilia), che lo scorso 13 febbraio aveva vinto il ricorso in tribunale dopo che la Questura gli aveva negato il documento.   
**Questo permesso di soggiorno è il primo documento istituzionale nella storia italiana che dà efficacia al riconoscimento dello status famigliare delle coppie omosessuali.**  
Un altro grande passo di civiltà reso possibile grazie al coraggio di Flavio e Rafael e al lavoro dell’Avv. Giulia Perin ([qui](http://www.rai.tv/dl/RaiTV/programmi/media/ContentItem-915c08af-342a-42da-97ae-df7b5f27b49f.html#p=0) una sua intervista),  ma anche grazie al tuo aiuto. Perché senza il contributo degli iscritti, tutte le iniziative promosse dal 2008 dall’associazione radicale Certi Diritti non sarebbero state possibili.

Senza il tuo contributo non avremmo avuto la forza di iniziare nel nostro Paese** la battaglia per l’uguaglianza dei diritti e contro la sessuofobia**, non avremmo potuto lanciare la campagna di Affermazione Civile per il matrimonio tra persone dello stesso sesso e forse oggi non ci sarebbe la storica sentenza 138/10 della Corte costituzionale.

Senza il sostegno degli iscritti non avremmo potuto lavorare alla** proposta di Riforma del Diritto di Famiglia, **depositata dai parlamentari radicali insieme a [tante altre proposte di legge](diritto-di-famiglia-matrimonio-unioni-civili-e-adozioni-tutte-le-proposte-radicali-in-parlamento), e non saremmo riusciti a** denunciare leggi omofobe e violenze **contro le persone lgbti in Russia e in Africa, dove il 26 gennaio 2011 è stato ucciso il nostro compagno ugandese David Kato Kisule. **Quindi grazie.**

Il lavoro da fare però è ancora tanto.   
Un mese fa abbiamo lanciato a [Roma](http://teniamofamiglia.blogspot.com/), poi a [Milano](http://metticilafirma.it/) e fra un po’ a Pescara ed in altre città italiane, una **raccolta firme** per una proposta di delibera comunale affinché, negli ambiti di competenza del comune, vengano riconosciuti i diritti delle coppie di fatto.  
Abbiamo pubblicato il libro [‘Certi Diritti che le coppie conviventi non sanno di avere’](certi-diritti-che-le-coppie-conviventi-non-sanno-di-avere), una sorta di manuale di sopravvivenza per le coppie etero e gay che non vogliono o non possono ancora sposarsi ([intervista all’autore Bruno De Filippis](http://affaritaliani.libero.it/cronache/coppie-di-fatto230312.html?refresh_ce)), e [chiesto al governo di ritirare la circolare Amato](bene-ministro-fornero-non-solo-parlamento-ma-anche-governo-puo-fare-la-sua-parte-a-cominciare-dal-ritiro-della-circolare-amato-n-55-del-2007) che vieta la t rascrizione in Italia dei matrimoni tra persone dello stesso sesso celebrati all’estero per motivi di ordine pubblico.

Abbiamo creato un network internazionale di esperti e militanti per l’affermazione dei diritti civili e umani in tutto il mondo e lanciato [la prima App, per iPhone iPad e ora anche per Android, sui diritti civili e i temi della liberazione sessuale](app-di-certi-diritti-anche-su-android-notizie-sui-diritti-civili-lgbte-sui-temi-della-liberazione-sessuale-anche-dallestero).   
Per **sabato 21 aprile 2012 stiamo organizzando una conferenza nazionale, a cui seguirà un appello, sulla legalizzazione della prostituzione, **organizzata dall’associazione radicale Certi Diritti, CGIL nazionale Nuovi Diritti e Comitato per i diritti civili delle prostitute**. (**Roma, Palazzo Valentini** - **Sala di Liegro, Via IV Novembre, 119/A).

**P.S. 1 **La Seconda marcia per l'amnistia, la giustizia, la libertà, a cui ha aderito Certi Diritti, è stata rimandata al **25 aprile**. [Su Notizie Radicali le ragioni dello slittamento.](http://notizie.radicali.it/articolo/2012-03-28/editoriale/la-seconda-marcia-lamnistia-rimandata-al-25-aprile-ecco-le-ragioni)

**P.S. 2** Ti consigliamo la lettura di [Anche quella omosessuale è una "famiglia"](http://www.lastampa.it/_web/cmstp/tmplRubriche/editoriali/gEditoriali.asp?ID_blog=25&ID_articolo=9929), intervento di Vladimiro Zagrebelsky.

**Grazie ancora per il sostegno che ci hai dimostrato in questi anni.**  
Se non hai ancora rinnovato l’iscrizione per il 2012 puoi farlo tramite [PayPal](https://www.paypal.com/it/cgi-bin/webscr?cmd=_flow&SESSION=itcuD-W7O42cYcHSwcEUmiLwff584RiibycZNT5w4mbJ9VQLlHO15K8ODQG&dispatch=50a222a57771920b6a3d7b606239e4d529b525e0b7e69bf0224adecfb0124e9b61f737ba21b08198d7a214dbf6ac4f3cd23680b71ef37df3),  
tramite bonifico bancario sul Conto Corrente IT 34 E 08327 03221 000000003165  
o tramite bollettino postale al **C/C n. 1001131539**, intestati  all’**Associazione Radicale Certi Diritti, specificando nella causale iscrizione 2012 o contributo.**

### **[**www.certidiritti.it**](http://www.certidiritti.it/)**