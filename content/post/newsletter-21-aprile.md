---
title: 'Newsletter 21 Aprile'
date: Sat, 28 Apr 2012 04:04:10 +0000
draft: false
tags: [Politica]
---

![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)

E' in corso a Roma la [conferenza nazionale sulla legalizzazione della prostituzione](sabato-21-aprile-a-roma-conferenza-nazionale-sulla-legalizzazione-della-prostituzione-con-giuristi-avvocati-prostitute-i-e-politici), organizzata dall’associazione radicale Certi Diritti, Comitato per i diritti civili delle prostitute e da Cgil-Nuovi Diritti. Sarà poi disponibile l’audio-video sul sito di Radio Radicale.

Giovedì è iniziato alla Commissione Giustizia della Camera **l’iter parlamentare della legge sulle unioni civili**. Tra le proposte anche la [pdl 1065](http://www.camera.it/Camera/view/doc_viewer_full?url=http%3A//www.camera.it/_dati/leg16/lavori/stampati/pdf/16PDL0024810.pdf&back_to=http%3A//www.camera.it/126%3FPDL%3D1065%26leg%3D16%26tab%3D2) a prima firma Rita Bernardini.  
Cosa prevede il testo? Su Notizie Radicali Matteo Mainardi, di Radicali Italiani e della segreteria di Certi Diritti, ci spiega [le conseguenze “rivoluzionarie” di una riforma necessaria>](http://notizie.radicali.it/articolo/2012-04-19/editoriale/unioni-civili-le-conseguenze-rivoluzionarie-di-una-riforma-necessaria)

**La deputata radicale Rita Bernardini ha fatto sapere che chiederà anche l'abbinamento alla discussione della proposta C.1064 per i matrimoni gay** che prevede, infatti che «la diversità di sesso tra gli sposi non è condizione necessaria per contrarre matrimonio». La decisione di abbinare le due proposte sul matrimonio tra persone dello stesso sesso, una di Rita Bernardini e l’altra di Paola Concia, è stata rimandata.

**>**Si parla di coppie di fatto anche in Senato, dove ieri Del Pennino (Pri) e Donatelli Poretti (Radicali) hanno presentato un **ddl sull’****unione solidale**.

**>**Intanto continua la raccolta firme per le proposte popolari a Roma dove** [sette consiglieri hanno firmato la proposta per riconoscimento famiglie di fatto](http://www.liberoquotidiano.it/news/980608/Roma-Radicali-7-consiglieri-firmano-proposta-per-riconoscimento-famiglie-di-fatto-2.html).**Questa** [la dichiarazione di Riccardo Magi di Radicali Roma.](http://www.radicali.it/20120414/dichiarazione-di-riccardo-magi-14-aprile-2012)**

**>Certi Diritti denuncia: [un ragazzo di 24 anni lasciato senza assistenza medica per curare la sifilide. Il farmaco non si trova in Italia ma in Vaticano! ](ragazzo-di-24-anni-lasciato-senza-assistenza-medica-per-curare-la-sifilide-il-farmaco-non-si-trova-in-italia-ma-in-vaticano)** 

**>**Il Presidente della Repubblica, Giorgio Napolitano, ha ricevuto due diverse lettere in relazione alla recente sentenza della Corte di Cassazione resa su un caso di matrimonio contratto all'estero tra cittadini dello stesso sesso: la prima dai senatori Gasparri e Giovanardi, l'altra dalla onorevole Paola Concia e dalla deputata al Parlamento Europeo Sonia Alfano.  
[A tutti ha risposto il Segretario generale della Presidenza della Repubblica, Donato Marra >](http://www.quirinale.it/elementi/Continua.aspx?tipo=Notizia&key=25134)

**>**Una delegazione di radicali tra cui** Sergio Rovasio di Certi Diritti **hanno inutilmente cercato di visitare il Centro Immigrazione ed Espulsione di Ponte Galeria di Roma. [Il Senatore Marco Perduca chiede venga unificata la normativa per l’accesso ai CIE](http://www.agenparl.it/articoli/news/politica/20120413-immigrati-perduca-radicali-uniformare-normativa-per-accesso-a-cie) (Centri di identificazione ed espulsione)

**>Riprendono le trasmissioni  “Spazio Certi Diritti”:** Riccardo Cristiano intervista l'Avvocato **Lucio Dattola,** consigliere nazionale Arcigay, sull'ultima sentenza della Corte di Cassazione in merito alle coppie di fatto e i migranti. **[Guarda l’intervista>](http://www.liberi.tv/webtv/2012/04/16/video/spazio-certi-diritti-intervista-lucio-dattola-consigliere)**

**> [CERTI DIRITTI che le coppie conviventi non sanno di avere](http://www.comune.grosseto.it/phpbb/viewtopic.php?p=24670)**: all’interno del Forum civico del Comune di Grosseto alcuneanticipazioni di ciò che troverete nel libro.  
**  
Appuntamenti**

**Roma, 21 aprile: [Conferenza nazionale sulla legalizzazione della prostituzione](conferenza-nazionale-sulla-legalizzazione-della-prostituzione)**

**Roma, 25 aprile: [Seconda marcia per l'Amnistia, la giustizia e la libertà](http://radicali.it/20120413/seconda-marcia-l039amnistia-giustizia-liberta039)**

**Milano: [9 zone contro l'omofobia](http://www.radioradicale.it/scheda/350366/milano-9-zone-contro-lomofobia-proiezioni-e-dibattiti-sul-film-due-volte-genitori-conferenza-stampa-di-com). **Proiezioni e dibattiti sul film "Due volte genitori".

**News**

Brasiliano sposa una ex trans riminese: [per la Questura, le nozze non sono conformi alle leggi italiane, ma il giudice dà ragione alla coppia >](http://www.gay.it/channel/attualita/33470/Sposa-una-ex-trans-italiana-la-questura-nega-soggiorno.html)

[Il Consiglio regionale della Lombardia ha respinto](http://www.gay.it/channel/attualita/33469/La-Russa-Lombardia-boccia-censura-dichiarazioni-omofobe-.html) una mozione di censura all'assessore Romano La Russa per le sue dichiarazioni antigay. E lui torna all’attacco: [per colpa loro ci estingueremo](http://milano.repubblica.it/cronaca/2012/04/17/news/gay_l_assessore_la_russa_torna_all_attacco_porteranno_all_estinzione_questa_societ-33481877/)

Ungheria: [Otto anni di galera per i gay che si abbracciano](http://www.giornalettismo.com/archives/253714/otto-anni-di-galera-per-i-gay-che-si-abbracciano/)

Baghdad: “[La mia famiglia vuole uccidermi perché sono gay](http://www.giornalettismo.com/archives/254870/la-mia-famiglia-vuole-uccidermi-perche-sono-gay/)”

[Primavera araba gay?](http://www.giornalettismo.com/archives/255614/primavera-araba-gay/)

[“The people demand the end of discrimination”](un-concorso-di-scrittura-sui-diritti-lgbti-nella-regione-mena-e-la-primavera-araba): concorso di scrittura sul tema dei diritti LGBTI e la rivoluzione araba >  
  
**Sostenere l’associazione radicale Certi Diritti costa [meno di un caffè a settimana](iscriviti) e da oggi puoi iscriverti con carta di credito con una semplice telefonata allo 06 6826!**

  
[www.certidiritti.it](http://www.certidiritti.it/)