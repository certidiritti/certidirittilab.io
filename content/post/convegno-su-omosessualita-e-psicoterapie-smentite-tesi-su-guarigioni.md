---
title: 'CONVEGNO SU OMOSESSUALITA'' E PSICOTERAPIE: SMENTITE TESI SU ''GUARIGIONI'''
date: Sun, 08 Nov 2009 10:31:05 +0000
draft: false
tags: [Comunicati stampa]
---

Roma, 8 novembre 2009

**Alle Associazioni e agli esponenti della Comunità lgbt(e)**

Si è svolto ieri a Roma, presso la Biblioteca nazionale, un importante convegno internazionale su “Omosessualità e psicoterapie” promosso dal Prof. Vittorio Lingiardi, medico psichiatra e psicanalista, Ordinario presso la Facoltà di Psicologia 1 dell’Università di Roma ‘La Sapienza’ dove dirige la 2° scuola di Specilizzazione in Psicologia clinica.

Il convegno, forse l’unico di questo genere in Italia, ha trattato e analizzato il tema delle ‘terapie riparative’, nate negli Usa, tra le altre, dall’Associazione Narth e ispirate dal fondamentalismo religioso degli evangelici cristiani, che anche in Italia, per altre vie, si stanno purtroppo diffondendo, è stato un grande esempio di confronto, chiarimento e affermazione della scienza davanti ad una platea di specialisti ed alcuni esponenti del movimento lgbt(e).

Soprattutto nella parte finale del convegno vi sono stati momenti di grande confronto scientifico tra gli psichiatri dott. Paolo Rigliano e il dott. Tonino Cantelmi su alcuni approcci e studi riguardo le cosiddette ‘terapie’ riparative.

**Tra i punti più rilevanti chiariti dal convegno, con elementi, dati e studi scientifici, vi sono:**

a) l’omosessualità non è una malattia;

b) le “terapie riparative” non hanno alcun fondamento scientifico;

c) quasi tutte le persone che si sono sottoposte a tali pseudo “terapie” sono rimaste omosessuali, le pochissime che dichiarano di essere ‘guarite’ dall’omosessualità sono per lo più persone che lavorano nell’ambito di associazioni che promuovono o gestiscono le ‘terapie riparative’;

d) coloro che svolgono l'attività di psichiatri o psicoterapeuti, e promuovono le ‘terapie riparative’, sono passibili di denuncia presso gli albi e istituti professionali per la totale infondatezza scientifica di questi metodi;

e) affidarsi alle conoscenze di uno specialista serve quindi non a ‘esssere o diventare normali’ ma a imparare ad accettarsi....

Certamente dimentico molte cose, per questo in coda a questo intervento metto un lancio dell’Agenzia Ansa di ieri che raccoglie altre informazioni e qui di seguito un breve testo storico su quanto accadde a un convegno di psichiatri nel 1972.

Credo che tutto il movimento lgbt(e) italiano debba essere riconoscente al Prof. Vittorio Lingiardi per aver promosso questo convegno, di grandissimo livello scientifico e di grande aiuto per la nostra battaglia di superamento delle diseguaglianze.

Un caro saluto,

Sergio Rovasio

[www.certidiritti.it](../../undefined/)

**Nel 1972 a Sanremo ad un convegno di psichiatria sull’omosessualità accadde qualcosa di storico.**

**Vi riporto un intervento tratto dal sito [www.gay.it](http://www.gay.it) del 7 giugno 2002**

**di Stefano Bolognini:**

“... In Italia era da poco nato il primo movimento gay, il Fuori!, che raccoglieva un modesto gruppo di militanti e che era salito alla ribalta delle cronache qualche mese prima del 5 aprile 1972 per aver scritto una lettera, firmata con i nomi e cognomi dei militanti, di protesta ad un quotidiano nazionale che aveva bistrattato l'omosessualità, decise di agire chiamando a raccolta anche i militanti di altri paesi europei. A Sanremo quella mattina i luminari delle scienze sessuologiche furono accolti da una piccola folla arrabbiata che gridava: "Normali, normali". Era la ribellione dei potenziali pazienti che oltre a gridare mostravano cartelli con scritte di questo tenore: "Psichiatri siamo venuti a curarvi", "Psichiatri ficcatevi i vostri elettrodi nei cervelli", "La normalità non esiste", "Primo e ultimo congresso di sessuofobia" e così via...  
Ogni cartello era una veemente dichiarazione di guerra e i quaranta contestatori presenti erano assolutamente consci che si stava compiendo un gesto storico. Per la prima l'omosessualità lottava a viso aperto. La rabbia era molta.  
I congressisti, non troppo lungimiranti ma questo lo hanno già attestato i loro studi, decisero di chiamare la polizia rendendo memorabile l'evento. Le forze dell'ordine sequestrarono i cartelli ai militanti e due di essi furono portati in commissariato.  
Il convegno incominciò comunque e tra gli iscritti a parlare si proposero regolarmente anche alcuni contestatori. Angelo Pezzana, presidente del Fuori, aprì le danze con il celeberrimo "Sono un omosessuale e sono felice di esserlo" di fronte ai congressisti sbigottiti. Il giorno dopo intervenne una militante francese che si scagliò contro la sessuofobia. Il terzo giorno ignoti lanciarono fialette di gas derattizzante, che è decisamente puzzolente, nella sala e il congresso fu interrotto….”.

SALUTE: TERAPIE CONTRO OMOSESSUALITA' 'INUTILI E DANNOSE'

(ANSA) - ROMA, 7 NOV - Malattia, immoralita', errore. Sono in tanti a pensare ancora all'omosessualita' come disordine morale che va curato, persino invertito. Omosessualita' e psicoterapia sono state oggi al centro di un convegno internazionale che si e' svolto a Roma per delineare lo stato della ricerca scientifica e sociale, in un momento in cui si assiste alla preoccupante diffusione di episodi omofobici.  
Ha meno di dieci anni il documento prodotto dall'American Psychiatric Association (APA) in cui viene negata l'antica associazione omosessualita' - disturbo mentale; mentre, solo qualche mese fa, sempre l'APA ha ribadito le proprie posizioni contro le cosiddette terapie riparative che, in estrema sintesi, mirerebbero a riorientare l'identita' sessuale delle persone, 'riconvertendole' a relazioni etero.  
Come ha sottolineato il professor Vittorio Lingiardi, direttore della 2ø Scuola di Specializzazione in Psicologia Clinica dell'Universita' 'Sapienza' di Roma, 'siamo spesso di fronte a chi si rivolge a un terapeuta per risolvere l'enigma circa la propria identita', per liberarsi di quell'ingombrante inquilino che abita la sua casa'. Il terapeuta pero' non deve trovare soluzioni, ma ha l'obbligo di ascoltare le domande che spingono i pazienti a dubitare di se stessi. Le terapie riparative invece, a cui spesso le persone sono indirizzate dalla famiglia, dalle comunita' locali e da quelle religiose, potrebbero peggiorare le condizioni mentali dei pazienti, esasperando il senso di vergogna e di disprezzo verso verso se stessi. Si rinforza insomma il conflitto interiore a discapito di una ricerca piu' profonda. Uno dei piu' noti promotori della terapia riparativa e' l'associazione NARTH, fondata dallo psicanalista Charles Socarides nel 1992, che incoraggia, nel caso degli uomini, l'identificazione con una figura maschile, riforzando al contempo i cosiddetti 'ruoli di genere tradizionali' per ottenere uno spostamento dall'omo all'eterosessualita'. Tuttavia la testimonianza di molti pazienti sottoposti a terapia riparativa ha rilevato come questi, nello sforzo di cambiare il proprio orientamento, si siano ritrovati ingabbiati in un mondo di finzione in cui la scoperta della propria identita' gradualmente si allontana.  
Per molte persone, scoprire di essere gay e' giustamente motivo di stress, laddove pregiudizio, discriminazione e violenza sono solo alcune delle cause scatenanti. Affidarsi alle conoscenze di uno specialista serve quindi non a essere o diventare normali, ma a imparare ad accettarsi. Amare se stessi e' l'unico rimedio contro i dubbi che fanno spesso piu' paura delle certezze. Un cammino lungo, forse doloroso, in cui il disagio non sia sopito ma ascoltato. (ANSA)  
  
Y94 07-NOV-09 19:46