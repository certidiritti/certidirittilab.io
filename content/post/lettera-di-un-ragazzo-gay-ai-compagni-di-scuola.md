---
title: 'Lettera di un ragazzo gay ai compagni di scuola'
date: Thu, 03 Mar 2011 16:47:36 +0000
draft: false
tags: [Comunicati stampa]
---

La lettera di uno studente dell'istituto Bertacchi di Lecco rivolta ai suoi compagni di scuola è stata letta durante l'assemblea sull'omosessualità.

26 febbraio 2011

Sono un ragazzo e sono gay. È vero, sono gay, ma prima di tutto sono un ragazzo che come voi è alla ricerca di se stesso, di uno scopo nella vita e della propria felicità. La mia natura vi spaventa? Forse perché non la conoscete come dovreste, perciò…Accantonate i vostri pregiudizi, fatemi spazio tra di voi e vi accorgerete che non sono così diverso.

Ho pensato di scrivere questa lettera riportando frammenti di esperienze, avute con diverse persone, che collegate tra loro andranno a formare un’unica e coesa esperienza: la mia.

Mio padre: a lui non l’ho ancora detto e il nostro rapporto è abbastanza ambivalente, manca di comunicazione e ciò mi impedisce di farmi avanti. Ricordo che quando ero piccolo, lui era fra quelli che mi prendevano in giro, storpiando sempre il mio nome al femminile; spesso ho dovuto sopportare continui paragoni con gli altri ragazzi perché io “non ero come loro” diminuendo vertiginosamente la mia autostima. Quando ho preso consapevolezza del mio orientamento sessuale mi sono sentito solo e terrorizzato, ma soprattutto in colpa per essere così. “Cosa dirà il papà?” era il pensiero più assillante, perché ho sempre fatto di tutto per piacergli. Purtroppo genitori e figli cadono nell’errore di pretendere che l’altro rispecchi la figura del figlio o del padre ideale, rimanendo delusi se ciò non avviene. Sicuramente l’ha intuito, ma entrambi non abbiamo avuto ancora il coraggio di affrontare l’argomento e confrontarci.

Mia madre: a lei l’ho detto 4 o 5 mesi fa, abbracciandola. Lei si è ritratta e mi ha chiesto: “perché mi dai questo dispiacere?” e poi “Non dovrai dirlo a nessuno”. Due frasi che mi hanno graffiato il cuore. Mamma, io ho bisogno di parlare! Per i primi giorni è diventata più affettuosa e premurosa, ma la comunicazione è sempre stata carente tra noi e la normalità è tornata subito, anche se mi aspettavo un miglioramento del nostro rapporto, trasformandolo in qualcosa di più intimo e sincero.

Mia sorella: quando glielo ho detto lei si è preoccupata dicendomi “Prenderai le malattie”; successivamente mi ha offerto la sua mano avvertendomi “Io non ti aiuto con la mamma e il papà”; poi mi ha rassicurato “Era peggio se dicevi di aver una malattia” e infine la perla di saggezza “Ma tu sei come Maicol del grande fratello?”. Con mia sorella non ho più parlato di questa cosa da giugno e anche con lei speravo di migliorare la qualità del nostro rapporto fraterno, spesso teso e distaccato. Speranze vane.

Il fidanzato di mia sorella: ha sempre disprezzato i gay con toni molto offensivi. E quando lo faceva?... quando io ero presente!

Un amica: “Quando avete rapporti sessuali tra di voi…Chi è il maschio e chi la femmina?”. Cara amica…Tu stai sopra o stai sotto col tuo ragazzo? Imbarazzante ciò che alcuni eterosessuali se ne vanno in giro a dire…

Compagna di scuola: “I gay mi fanno schifo e dovrebbero essere ammazzati”. Cara, vorresti vedermi morto e ti faccio così schifo? Eppure insieme abbiamo passato momenti meravigliosi, no?

Un’altra compagna di scuola: “Se avessi un figlio gay lo rinchiuderei in una stanza a guardare film porno finché non gli piacerà la fig@”. Sicura di star bene? Sarebbero i genitori omosessuali quelli incapaci di crescere figli? Spero un giorno di poter insegnare, con il mio compagno, a mio figlio un’ottima educazione e di farlo crescere con principi sani ed equilibrati.

A proposito: avete paura che i figli dei gay crescano loro stessi gay? Io sono figlio di eterosessuali;  
avete paura che i figli dei gay vengano presi in giro? Mi chiamano “ricchione” fin dalle elementari, mi hanno mortificato più volte al pub di fronte a tutti chiedendomi se avessi voluto fare dei pomp*ni, mi hanno messo dentifricio sui capelli mentre tornavo a casa in treno…I miei genitori sapete che sono etero?

Professoressa: quando le ho confidato di essere gay ho avuto coraggio e tanta fortuna, perché lei è stata la prima persona ad aiutarmi concretamente portandomi all’associazione Renzo e Lucio e a rassicurarmi dicendomi “non sei contro natura; lo saresti se non seguissi le tue pulsioni”. Io non credo che l’omosessualità sia una malattia. Diventerei malato quel giorno che decidessi di reprimermi e sforzarmi di rientrare in una natura che dal mio punto di vista va davvero contro corrente.

Prendo spunto dall’esperienza con la mia professoressa per rivolgermi a possibili ragazzi gay e ragazze lesbiche: abbiate coraggio, prendete saldamente in mano le redini della vostra vita e guidate voi il vostro destino verso la direzione che più vi attira e non sbaglierete. Non nascondo quanto a volte sia difficile rimanere in sella, ma non è necessario galoppare e ho conosciuto persone meravigliose che si prendono cura di me, insegnandomi a rispettare me stesso e che mi aiutano a domare questa selvaggia bestia che è la vita. Da soli non è facile. Mostratevi agli altri per quello che siete perché qualcuno vi ama già.

A tutti voi: grazie della partecipazione e riflettete su voi stessi e sulle persone che vi stanno accanto e fate tesoro di quello che avete appena ascoltato. Le diversità impauriscono solo perché non le si conosce. Dunque, cosa aspettate?

P.S. Un forte abbraccio a due meravigliose amiche che hanno accettato di partecipare questa mattina!

Un ragazzo come voi

da [www.renzoelucio.it](http://www.renzoelucio.it/)