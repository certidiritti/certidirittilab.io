---
title: 'PARLAMENTO APPROVA NORMA ANTIDISCRIMINATORIA SUL POSTO DI LAVORO'
date: Sun, 07 Mar 2010 11:26:32 +0000
draft: false
tags: [Senza categoria]
---

NEL SILENZIO GENERALE IL PARLAMENTO HA APPROVATO UNA PICCOLA MA IMPORTANTE NORMA ANTIDISCRIMINATORIA IN MATERIA DI LAVORO. IL COSIDDETTO “COLLEGATO LAVORO” CONTIENE IL DIVIETO DI OGNI FORMA DI DISCRIMINAZIONE NELLEPUBBLICHE AMMINISTRAZIONI RICHIAMANDO LE DIRETTIVE COMUNITARIE. PICCOLO PASSO AVANTI PER LE PERSONE LGBT E PER TUTTE LE ALTRE CATEGORIE SOGGETTE A RISCHIO DISCRIMINAZIONE.

Roma, 7 marzo 2010

COMUNICATO STAMPA DELL'ASSOCIAZIONE RADICALE CERTI DIRITTI

Le buone notizie non fanno notizia, e l’approvazione da parte del Senato del cosiddetto “collegato lavoro”, lo scorso 3 marzo, è stata coperta dalle polemiche sulle norme che riguardano l’arbitrato e i diritti dei lavoratori licenziati, lasciando in ombra, tra le altre cose, che lo stesso collegato comprende un articolo, il 21, decisamente innovativo per la realtà italiana.  
Infatti con questo articolo si estende alla pubblica amministrazione l’obbligo di operare per le pari opportunità e “l’assenza di ogni forma di discriminazione, diretta o indiretta, relativa al genere, all’età, all’orientamento sessuale, alla razza, all’origine etnica, alla disabilità, alla religione o alla lingua …”, sia per quanto riguarda accesso al lavoro che retribuzione formazione e promozioni.

L’articolo prevede inoltre altre novità, come la possibilità di finanziare azioni positive per il superamento di situazione di  
disuguaglianza e discriminazione, e l’istituzione di un comitato unico di garanzia per le pari opportunità, che ingloba i precedenti comitati per le pari opportunità.  
  
Si tratta semplicemente dell’applicazione delle Direttive comunitarie e degli articoli dei Trattati, in particolare del 19 del TFCE e dell’art. 21 della carta dei diritti fondamentali, ma pur sempre una piccola ma molto significativa riforma del nostro ordinamento, che l’Associazione radicale Certi Diritti saluta come un segno importante  
di questo Parlamento.  
  
Ora i Comuni, le Province e soprattutto le Regioni non hanno più alibi nella lotta contro le discriminazioni. Si muovano!