---
title: 'AFFERMAZIONE CIVILE 2011'
date: Sat, 22 Jan 2011 14:19:32 +0000
draft: false
tags: [Affermazione Civile]
---

AFFERMAZIONE CIVILE 2011

La battaglia per il Diritto al Matrimonio come principio di uguaglianza continua, e guarda anche all'Europa. 

Certi Diritti riparte con le iniziative giuridiche di Affermazione Civile, su tre fronti principali:

1.  **Riconoscimento del diritto al matrimonio in Italia:** dopo il pronunciamento della Corte Costituzionale, il nostro prossimo obiettivo è la Corte Europea dei Diritti dell'Uomo.
2.  **Trascrizione in Italia di matrimoni/unioni civili avvenute all'estero:** perché sono sempre di più le coppie di persone che sono costrette a vivere all'estero perché al proprio partner non viene riconosciuta la libertà di circolazione e non viene concesso il permesso di soggiorno, np gli viene facilitata la cittadinanza.
3.  **Riconoscimento dei singoli diritti in Italia:** perché la corte costituzionale ha chiaramente invitato ogni cittadino discriminato dall'ordinamento italiano a chiedere giustizia negli specifici campi. Dalla pensione, ai diritti del lavoro, dalle tasse pagate alla pensione, dalla retribuzione al TFR, dalla possibilità di avere figli alla successione ereditaria.

AAA COPPIE OMOSESSUALI CERCASI (MA ANCHE ETERO…)

Cerchiamo coppie decise a percorrere una causa pilota fino al livello Europeo, pronte ad affrontare un percorso che potrà essere lungo diversi anni, impegnativo anche da un punto di vista economico.

Cerchiamo anche coppie che siano pienamente consapevoli dei propri diritti di cittadinanza e soprattuttoconsapevoli delle discriminazioni che subiscono.

Infatti,  occorrerà portare davanti al giudice l'evidenza e le prove della propria vita insieme, della discriminazione subita.

Per partecipare ad Affermazione Civile ogni coppia dovrà riuscire a trasformare quel senso vago e indefinibile di frustrazione e rabbia (sensazione che ogni coppia non sposata vive in Italia), in qualcosa di concreto, qualcosa che può far cambiare idea ad un giudice, qualcosa che apra la coscienza dell'opinione pubblica.

Ogni coppia ha delle sue caratteristiche specifiche, ha una storia propria. Non tutti però avranno la coscienza, la determinazione e la convinzione di affrontare tutto questo pubblicamente, specialmente se il rischio è mettere a nudo i propri punti deboli. Ma non possiamo non considerare che si tratta di quegli stessi punti deboli che la legge dovrebbe aiutarci a proteggere e che invece si ostina ad ignorare.

_Ma in concreto, di cosa stiamo parlando? Abbiamo cercato di creare una lista di esempi pratici. M_a sicuramente la realtà si dimostrerà molto più fantasiosa di ogni possibile lista, classificazione, ed è impossibile prevedere con limpidezza i mille modi con i quali l'amore si intreccia con le nostre storie personali.

Se pensate di essere voi quella coppia che può e vuole dare il proprio contributo per la lunga battaglia per i diritti civili che dobbiamo affrontare, contattateci e scriveteci a [affermazionecivile@certidiritti.it](mailto:affermazionecivile@certidiritti.it)

Insieme agli avvocati che ci sostengono individueremo le possibilità concrete di avviare una causa pilota, a livello nazionale oppure internazionale, approfondendo ogni dettagli necessario.

Per approfondire, segui questi link: 

**[DOMANDE E RISPOSTE](index.php?option=com_content&view=article&id=999&Itemid=250): **PARLANDO DI AFFERMAZIONE CIVILE UN PO' SOPRA LE RIGHE... E ANCHE PER QUESTIONI MOLTO CONCRETE

**[GLI INGREDIENTI PER UNA CAUSA PILOTA](index.php?option=com_content&view=article&id=998&Itemid=249):** LE COPPIE. CERTI DIRITTI. E GLI AVVOCATI. UNITI DA UNA PASSIONE PER LA POLITICA. QUELLA VERA.