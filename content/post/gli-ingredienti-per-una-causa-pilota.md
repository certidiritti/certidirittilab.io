---
title: 'GLI INGREDIENTI PER UNA CAUSA PILOTA'
date: Sat, 22 Jan 2011 14:20:50 +0000
draft: false
tags: [Affermazione Civile]
---

GLI INGREDIENTI PER UNA CAUSA PILOTA

Le coppie. Certi Diritti. E gli avvocati. Uniti dalla fiducia nelle istituzioni e da una passione per la politica, quella vera.

Gli ingredienti per una causa pilota di successo sono molti. Per descriverli tutti, occorrerebbe un libro intero, e l'esperienza di persone che per il momento non possono dedicarsi alla scrittura di questo sito.

Tuttavia ci sono alcuni ingredienti che riteniamo essere fondamentali... ti diremo quali sono, e come possono essere amalgamati al meglio.

In questo link trovi una presentazione movimentata e interessante. Altrimenti leggi qui di seguito...

**Il primo ingrediente sono le coppie.** Sotto ti aiuteremo a trovare i pezzi della vostra vita insieme che potrebbe essere importante presentare davanti ad un giudice. Ti spigheremo perché cerchiamo anche coppie eterosessuali. E ti diremo cosa puoi fare per spargere la voce...

**Il secondo ingrediente è Certi Dirtti.** Dopo l'esperienza degli anni passati, abbiamo deciso di adottare una chiara linea di iniziativa politica, e dei contatti solidi con le realtà europee e internazionali che si battono per il diritto al matrimonio.

**Il terzo ingrediente sono gli avvocati che ci sostengono.** In questo link puoi scoprire chi c'è in prima linea ad aiutarci, perché abbiamo scelto proprio loro. Scoprirai chi altro sta preparando ricorsi come i nostri. E troverai anche qualche spiegazione su cosa significa "andare in Europa", ovvero: cosa è la Corte Europea dei Diritti dell'uomo e che differenza c'è con la Corte di Giustizia Europea.

**Ma non basta:** questi tre ingredienti sono legati dalla passione per la democrazia, il rispetto e la fiducia nelle istituzioni, la convinzione che il principio di uguaglianza è la cosa che più serve al benessere della società moderna.

**La ciliegina sulla torta** sono la consapevolezza che questa battaglia può essere lunga e impegnativa, e che per vincere una guerra può essere necessario mettere in conto qualche sconfitta temporanea.

Le coppie
---------

### **QUALI CASI CONCRETI POSSIAMO PORTARE DAVANTI AI GIUDICI?**

Alcune coppie hanno delle necessità di assistenza reciproca, magari per importanti motivi di salute di uno dei partner.

Oppure sono impegnate nell'assistenza di familiari comuni, ai quali dedicano denaro, tempo, spazio della propria casa comune.

Alcune coppie hanno dovuto fare i "salti mortali" per acquistare un appartamento con un mutuo in comune, altri per pagare l'affitto.

Alcune coppie subiscono discriminazioni di trattamento sul lavoro per via di assicurazioni sanitarie/infortuni che non ricoprono il convivente more uxorio dello stesso sesso.

Altre ricevono una pensione o una retribuzione oppure pagano tasse senza aver mai potuto poter dichiarare un coniuge a carico.

Altre hanno un partner non comunitario e non gli viene riconosciuto il permesso di soggiorno.

Per altre coppie si profila la necessità impellente di designarsi a vicenda eredi legittimi, senza poi dovere pagare delle tasse aggiuntive perché non si viene riconosciuti parenti.

Altre coppie non hanno accesso alla fecondazione assistita perché omosessuali.

Altre coppie non riescono a vedersi riconosciuta l'autorità parentale così importante per la crescita dei propri figli, figli ai quali non viene riconosciuto il diritto di avere due genitori.

Si potrebbe continuare ancora per molto tempo, ma siamo convinti che la realtà è molto più fantasiosa di ogni possibile lista, classificazione, ed è impossibile prevedere con limpidezza i mille modi con i quali l'amore si intreccia con le nostre storie personali.

Se pensate di essere voi quella coppia che può e vuole dare il proprio contributo per la lunga battaglia per i diritti civili che dobbiamo affrontare, contattateci e scriveteci a

affermazionecivile@certidiritti.it

Potremo individuare insieme agli avvocati che ci sostengono le concrete possibilità di avviare una causa pilota, a livello nazionale oppure internazionale, approfondendo ogni dettagli necessario.

### **SIAMO PROPRIO NOI LA COPPIA GIUSTA?**

Avviare una causa pilota per Affermazione Civile può non essere semplicissimo: è un percorso che può durare diversi anni, durante il quale occorrerà prevedere un impegno economico, la collaborazione con avvocati e con le associazioni, la possibilità di interagire con l'opinione pubblica, attraverso iniziative, convegni, interviste o quanto altro sarà possibile fare.

Con l'esperienza fatta nei primi due anni di Affermazione Civile abbiamo capito che le cause pilota hanno maggior successo quando la coppia riesce a vincere il timore di vedere questioni molto personali strumentalizzate dalla politica e dai media. Non ci sono regole, ma generalmente questo timore si vince più facilmente quando la coppia è capace di parlare al proprio interno, se riesce a costruire un rapporto di fiducia con gli avvocati e con Certi Diritti, e di sicuro può aiutare anche sapere di avere il supporto esplicito e visibile di parenti ed amici.

Fino ad oggi, molte delle coppie che hanno partecipato ad Affermazione Civile hanno dato precedenza al bisogno di privacy, che è sempre stato rispettato con la massima serietà e impegno da parte di tutti.

Per il futuro, le cause pilota che vogliamo intentare insieme a voi saranno un percorso giuridico che tenteremo di riproporre e valorizzare attraverso ogni altra dinamica democratica, comunicativa. Ogni evento giuridico avrà pieno senso se potrà essere utilizzato per fare leva sull'opinione pubblica e per costruire una iniziativa anche politica sul percorso giuridico che si intenta.

### PERCHE' CERCHIAMO ANCHE COPPIE ETEROSESSUALI

In Italia non esiste nessun riconoscimento nemmeno per le coppie eterosessuali. Questo non va dimenticato per tanti motivi.

Prima di tutto perché Certi Diritti pensa sì al diritto al matrimonio, ma in quanto "traguardo di uguaglianza". Tuttavia, all'interno del progetto di riforma del diritto di famiglia, Amore Civile, Certi Diritti ha sostenuto la necessità che per tutte le coppie in Italia sia riconosciuto anche il diritto ad unirsi civilmente.

Inoltre, perché l'impegno di voler coinvolgere coppie eterosessuali è la dimostrazione che battersi per un principio di uguaglianza non è una cosa riservata al mondo omosessuale, ma riguarda tutte e tutti, indistintamente.

Infine, c'è anche un fondamento giuridico: ogni riconoscimento giuridico che verrà ottenuto per coppie eterosessuali non sposate, dovrà necessariamente essere valido anche per le coppie omosessuali, pena una palese contraddizione con il principio di non discriminazione in base all'orientamento sessuale che vige in Europa.

### AIUTACI A DIFFONDERE LA VOCE

Se anche tu ci credi, aiutaci a sviluppare AFFERMAZIONE CIVILE:

se fai parte di una coppia omosessuale che si vuole sposare contattaci per partecipare all'iniziativa;

se fra le tue amicizie ci sono coppie di gay e lesbiche, fagli conoscere AFFERMAZIONE CIVILE e invitali a mettersi in contatto con noi.

dacci una mano a diffondere AFFERMAZIONE CIVILE tra tutte le persone che conosci attraverso i tuoi contatti, il tuo blog, il tuo sito internet, Facebook e gli altri social network

partecipa alla discussione nel nostro forum e facci sapere che cosa ne pensi.

Certi Diritti
-------------

### PERCHE' CONTINUIAMO A PUNTARE SUL DIRITTO AL MATRIMONIO

Le sentenze della CEDU, lette in modo disincantato, non lasciano molto spazio per una immediata e chiara sentenza del diritto al matrimonio.

Non solo i nostri detrattori, ma anche tante persone che genuinamente si impegnano per il diritto all'uguaglianza delle persone LGBT in Italia e in Europa, hanno argomenti per ritenere prematuro un ricorso alla CEDU per il diritto al matrimonio.

Ad esempio:

\- i trattati dell'Unione Europea lasciano ai singoli stati piena libertà nel campo del Diritto di Famiglia

\- già altre sentenze hanno affermato chiaramente che non sussiste necessariamente un Diritto al Matrimonio tra persone dello stesso sesso, almeno non finché non ci sarà un numero sufficiente di stati dell'Unione Europea che riconoscono il matrimonio tra persone dello stesso sesso

E' anche vero che in Europa si hanno ampi margini di garanzia per quanto riguarda il diritto di libera circolazione e il diritto di non discriminazione, e il diritto di uguaglianza di tutti i cittadini europei.

E non a caso, per Certi Diritti Affermazione Civile è sempre stata centrata sul Diritto al Matrimonio proprio come principio di uguaglianza. E su questo andremo avanti.

Riteniamo che attendere (a tempo indeterminato) tempi migliori, non renda giustizia alle persone gay e lesbiche italiane, e ai loro cari.

Pensiamo che procedere con un atteggiamento prudentista non paghi. La possibilità di non ricevere un sì pieno da parte della CEDU non significa necessariamente una sconfitta, poiché in ogni sentenza sono molti i principi giuridici e umanitari che possono essere espressi. E la storia dimostra che tutte le battaglie più importanti, tutte le pietre miliari del diritto, non sono state conquistate con una vittoria secca e sicura, senza rischi.

Al contrario, tutto è stato raggiunto dopo una serie di sconfitte, non senza la capacità di azzardare mosse apparentemente inutili, o rischiose.

Dunque, Certi Diritti, vuole continuare da subito la strada della via giudiziaria, a livello nazionale ed Europeo.

### UNA INIZIATIVA GIURIDICA, MA ANCHE UNA INIZIATIVA POLITICA

Certi Diritti desidera che ogni azione giudiziaria incardinata possa essere una cassa di risonanza e spunto di iniziativa politica, di dibattito pubblico, di riflessione per la comunità LGBT e l'opinione pubblica in generale. Intendiamo utilizzare tutti gli strumenti pacifici e democratici a nostra disposizione per fare pressione anche alle corti, al fine di influenzare - per quanto lecito e possibile - un giudizio il più possibile vicino alle esigenze del Paese reale e non di una politica ormai irreale e lontana dai tempi moderni e dalla vita delle persone.

Per noi affermazione civile non si esaurisce nelle aule di tribunale. E il contraddittorio che vogliamo portare a galla non sarà solo quello tra gli avvocati e i giudici, ma anche quello tra i giudici e la vita reale delle persone, le aspettative non tanto dell'opinione pubblica, quanto dell'Italia onesta, che crede nelle Istituzioni, nell'Europa, nella democrazia.

### CERTI DIRITTI E ILGA EUROPE: UNO SGUARDO SULL'EUROPA

Da più di due anni Certi Diritti collabora con ILGA Europe, in particolare anche per armonizzare il più possibile le iniziative giuridiche nel contesto dell'attivismo LGBT europeo.

Già nel 2009 Certi Diritti ha tenuto un proprio workshop durante la conferenza annuale e qui abbiamo sostenuto l'importanza di riconoscere anche la dimensione internazionale del riconoscimento del diritto al matrimonio.

Inoltre nel 2010 Certi Diritti ha partecipato ad un seminario di formazione proprio sulla gestione di cause pilota a livello europeo.

Questo impegno garantisce a Certi Diritti di attingere ancora più profondamente ad un ampio know how sia professionale, sia di esperienza diretta di tante altre associazioni e persone che hanno ricorso o vogliono ricorrere alla Corte Europea.

E' un impegno che prendiamo in carico per dare la maggiore credibilità possibile alla nostra campagna giuridica e all'iniziativa politica ad essa sottesa.

Gli avvocati
------------

### GLI AVVOCATI CHE COLLABORANO CON NOI

Per sostenere le coppie che vogliono partecipare ad Affermazione Civile Certi Diritti continua la collaborazione con avvocati volontari, che mettono a disposizione la loro professionalità offrendo assistenza gratuita per i ricorsi e per sostenere le cause presso le aule dei tribunali.

In particolare ci sostengono Marilisa D'Amico, ordinario di diritto costituzionale all'Università degli Studi di Milano, e Massimo Clara, avvocato civilista e cassazionista. Tutti hanno fatto parte del collegio di difesa dinanzi alla Corte Costituzionale in difesa delle coppie di Affermazione Civile, durante l'udienza del 23 Marzo 2010.

Collaboriamo anche con Alessandro Geraldi e Stefano Mossino, avvocati dell'area radicale.

Abbiamo contatti anche con altri avvocati, i quali oltre ad avere una provata competenza nella difesa dei diritti civili, sono in piena sintonia con lo spirito di iniziativa politica che è implicito in ogni battaglia, anche giuridica, per i diritti civili.

Inoltre, sempre grazie ai radicali, contiamo sul pieno appoggio e collaborazione di Filomena Gallo, che da anni si batte insieme alla associazione Luca Coscioni contro la legge 40, oltre che sull'appoggio di tutta una serie di esperti e giuristi specializzati sui ricorsi in sede Europea.

### LA SITUAZIONE DEI RICORSI IN EUROPA

In Europa sono molte le nazioni che si battono o si sono battute per il diritto al matrimonio.

Attualmente ci sono diversi casi pendenti alla CEDU, proprio per il riconoscimento del diritto al matrimonio:

in Francia erano stati celebrati dei matrimoni tra persone dello stesso sesso nel 2004 (da parte di un sindaco illuminato) e la CEDU è stata chiamata a pronunciarsi sul loro annullamento.

In Russia una coppia di donne non ha accettato di non vedersi riconosciuto in patria il matrimonio celebrato in Canada.

Dall'Inghilterra, pende un ricorso alla CEDU congiunto con coppie eterosessuali, poiché le coppie gay non possono sposarsi, mentre le coppie etero non possono unirsi civilmente.

Inoltre, le iniziative sul matrimonio che procedono a livello nazionale sono molte di più: Austria, Cipro, Germania, Grecia, Lituania, Polonia e Slovacchia. Oltre, ovviamente, all'Italia.

Ma non ci sono solo iniziative legali sul matrimonio: infatti molte leggi sulle unioni civili mettono in luce delle gravi discriminazioni e limitazioni delle libertà fondamentali delle persone. E così, ad esempio, un cittadino italiano e un cittadino neozelandese attendono una sentenza della CEDU per vedersi riconosciuto il diritto di libera circolazione in Europa (il neozelandese non si è visto riconosciuto il permesso di soggiorno, e sono stati obbligati ad andare a vivere in un altro paese!)

E poi ci sono questioni più specifiche, ma non per questo meno "importanti", che pendono davanti alla Corte di Giustizia. Come il ricorso di un Cittadino Tedesco che riceve dallo stato una pensione di anzianità minore di quanto potrebbe essere solo perché è unito civilmente con una persona dello stesso sesso invece che di sesso opposto.

### COSA SIGNIFICA ANDARE IN EUROPA?

L'Europa è una istituzione molto complessa e articolata, impossibilie da spiegare, neanche lontanamente, in questo contesto.

Più semplicemente vogliamo provare a chiarire che a livello Europeo sono possibili due principali tipi di iniziative giuridiche, differenti per iter giudiziario, contenuto del ricorso, effetti es efficacia di una possibile sentenza.

Proviamo ora a dare delle vaghe linee guida, assolutamente non esaustive, ma ci auguriamo utili per i possibili, differenti obiettivi della campagna di Affermazione Civile a livello europeo.

-**--LA CORTE EUROPEA DEI DIRITTI DELL'UOMO**

I cittadini Italiani possono ricorrere alla Corte Europea dei Diritti dell'Uomo perché l'Italia ha aderito alla relativa Convenzione, insieme a tanti altri stati (Russia compresa, ad esempio), che non necessariamente fanno parte dell'Unione Europea.

Il ricorso alla Corte Europea è possibile solo dopo che siano stati esauriti tutti i gradi di giudizio previsti dalla singola nazione. Tuttavia, dopo la sentenza della Corte Costituzionale, una sentenza della cassazione potrebbe non essere necessaria, anche visto che la giurisprudenza in tema di diritto di famiglia è talmente consolidata da non dare speranza di successo.

La Corte Europea può essere interpellata su tutti quelli che sono i Diritti Fondamentali dei cittadini, ivi compreso dunque, il diritto a contrarre matrimonio, o quello di vederselo riconosciuto in uno stato membro dell'Unione Europea.

Le sentenze della Corte Europea dei Diritti dell'Uomo non sono vincolanti, quindi anche in caso di vittoria piena l'Italia non sarà obbligata a riconoscere il matrimonio tra persone dello stesso sesso. Tuttavia è possibile che l'Italia subisca ad ogni modo una condanna, che può essere utilizzata sul piano politico, per fare leva affinché in Italia si prendano decisioni contro la discriminazione delle persone omosessuali.

**---LA CORTE DI GIUSTIZIA EUROPEA**

La funzione della Corte di Giustizia Europea è garantire che la legislazione dell'UE sia interpretata e applicata in modo uniforme in tutti i paesi dell'Unione e che la legge sia quindi uguale per tutti.

Per ricorrere alla Corte di Giustizia non è necessario esaurire tutti i gradi di giudizio.

La Corte va interpellata per specifiche questioni previste esplicitamente dalla legislazione dell'unione, ivi compresi la libera circolazione per i cittadini europei (compresi i partner non europei di cittadini europei) o diritti riguardante la legislazione sul lavoro.

Le sentenze della Corte sono vincolanti, quindi "efficaci". Lo stato membro ha l'obbligo di adeguarsi alla sentenza. In caso contrario si devono pagare multe salatissime.

**Per riassumere:**

*   la CEDU sarà interpellata per le questioni di principio, proprio per il riconoscimento del diritto al matrimonio.
*   La CGE per casi specifici, concreti. Piccole cose, magari, che però possono avere un concreto effetto sulla vita di milioni di persone in Italia.