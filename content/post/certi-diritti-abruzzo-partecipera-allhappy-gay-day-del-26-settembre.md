---
title: 'CERTI DIRITTI – ABRUZZO PARTECIPERÀ  ALL''HAPPY GAY DAY DEL 26 SETTEMBRE'
date: Tue, 22 Sep 2009 14:02:42 +0000
draft: false
tags: [Comunicati stampa]
---

**L'Associazione Radicale Certi Diritti – Abruzzo conferma che la sua  partecipazione all’evento** **Happy Gay Day per dire basta al razzismo omofobico che nel nostro Paese trova terreno fertile. Facciamo appello ai cittadini abruzzesi a partecipare all’appuntamento di Pineto del 26 settembre.**

Il razzismo omofobico ha raggiunto livelli di vera e propria emergenza.  L’Associazione Radicale Certi Diritti, impegnata in tutta Italia nella lotta all’omofobia e alla transfobia chiede che la Regione, le Province e i Comuni si mobilitino come già avviene in tutto il resto d’Italia.

Dopo la comunicazione della “contro manifestazione” indetta da un gruppuscolo di estrema destra, tra l’altro non autorizzata dalla Questura, l’Associazione Radicale Certi Diritti, seguendo la scia dei Radicali Italiani e Radicali Transnazionali, conferma che parteciperà all’iniziativa e invita  gli abruzzesi a partecipare in massa all’Happy Gay Day in modo PACIFICO, LAICO e LIBERALE.

Ringraziamo da subito le Forze dell’Ordine e la Prefettura di Teramo per le garanzie assicurate allo svolgimento della manifestazione. A quanti hanno a cuore la libertà, la laicità e i diritti, e ritengono che sia giunta l’ora, anche in Italia, così come nel resto del mondo civile, di porre fine alle discriminazioni basate sull’orientamento sessuale, rivolgiamo un appello a partecipare con noi all’ Happy Gay Day che si terrà a Pineto (TE), il 26 settembre 2009, presso il Lido Marco Polo.