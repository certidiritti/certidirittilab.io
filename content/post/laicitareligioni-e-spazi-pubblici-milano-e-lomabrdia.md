---
title: 'LAICITA'',RELIGIONI E SPAZI PUBBLICI MILANO E LOMABRDIA'
date: Mon, 08 Feb 2010 06:44:32 +0000
draft: false
tags: [Senza categoria]
---

_Mercoledì 17 febbraio 2010, ore 21.00  
Sala Alessi di palazzo marino. Piazza della Scala 2, Milano_

Interventi dello storico **Massimo L. Salvadori** e di **Fulvia Colombini** \- Cgil, **Dounia Ettaib** \- Donne Arabe d'Italia, **Valerio Federico** \- Ass. Luca Coscioni, **Grazia Villa** \- Ass. La Rosa Bianca, **Luciano Zappella** \- Ass. 31 Ottobre: per una scuola laica e pluralista.  
Introduce: **Donatella De Gaetano**, coordinatrice della Consulta; conduce: **Luciano Belli Paci**\- Circolo Carlo Rosselli.

_Diffondi l'iniziativa con il nostro_ [volantino](http://www.milanolaica.it/wp-content/uploads/2010/02/ConsultaLaicità17FEBB10.pdf)_!_

Per il suo primo compleanno di attività la Consulta milanese rinnova l'appuntamento del 17 febbraio nella Sala Alessi di Palazzo Marino in piazza della Scala 2, alle ore 21, con una iniziativa di ampia portata che intende far luce sullo stato della laicità nelle relazioni tra istituzioni cittadine e regionali e la popolazione.

Della data del 17 Febbraio è nota la richiesta del Coordinamento nazionale delle Consulte laiche cittadine perché sia riconosciuta come Giornata della liberta' di coscienza, di religione e di pensiero, in ricordo e anniversario della concessione nel 1848 dei diritti civili ai valdesi e poi agli ebrei e, nel 1600, della morte di Giordano Bruno sul rogo.

Sul tema "Laicità, Religioni e Spazi Pubblici a Milano e in Lombardia", saranno impegnati esponenti di organismi associativi e sindacali che chiedono da tempo alle istituzioni il pieno rispetto della laicità, il rifiuto della discriminazione negli spazi pubblici e nelle istituzioni, la valorizzazione dell'accoglienza e dell'integrazione delle diversità presenti.

Concluderà la serata lo storico Massimo L. Salvadori, dell'Università di Torino, con la relazione: "Democrazia e Laicità oggi in Italia".