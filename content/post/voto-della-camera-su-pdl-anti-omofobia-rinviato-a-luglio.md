---
title: 'Voto della Camera su Pdl anti-omofobia rinviato a luglio'
date: Fri, 17 Jun 2011 06:04:54 +0000
draft: false
tags: [Politica]
---

**Confermati sit-it e maratona oratoria delle associazioni lgbt(e).**

**Roma, 16 giugno 2011**  
  

  
**Comunicato Stampa Associazione Radicale Certi Diritti**

Il voto su pdl anti- omofobia (inlcuse pregiudiziali e richiesta di sospensiva), è ulteriormente slittato a luglio.  Presumibilmente alla settimana successiva anche se sarà la Conferenza dei Capigruppo a decidere se la prima o seconda settimana di Luglio. Confermati sit-it e maratona oratoria delle associazioni lgbt(e).

Questa è una notizia ufficiale, visto quanto deciso oggi alla riunione della Conferenza dei Capigruppo della Camera dei deputati.