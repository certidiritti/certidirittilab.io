---
title: 'Congresso Associazione Radicale Certi Diritti in diretta streaming'
date: Fri, 26 Nov 2010 21:48:08 +0000
draft: false
tags: [Associazione radicale Certi Diritti, certi diritti, chat, Comunicati stampa, diretta, diretta streaming, roma, streaming]
---

Sabato 27 e domenica 28 novembre il [congresso dell'Associazione Radicale Certi Diritti](index.php?option=com_content&view=article&id=850:iv-congresso-associazione-radicale-certi-diritti&catid=1&Itemid=55) verrà trasmesso in diretta streaming come avvenuto alla precedente edizione. Chi non si potrà recare a Roma a seguire i lavori potrà recarsi al sito dell'Associazione Radicale Certi Diritti per assistere in diretta ai lavori e potrà porre domande attraverso una apposita chat che verrà inserita nel sito durante i lavori e che permetterà a tutti di porre domande e chiedere chiarimenti.