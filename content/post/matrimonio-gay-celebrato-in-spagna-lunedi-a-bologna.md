---
title: 'Matrimonio gay celebrato in Spagna. Lunedì a Bologna Certi Diritti chiederà la trascrizione in Italia. In caso di diniego ricorso alla giustizia europea'
date: Fri, 26 Aug 2011 12:20:01 +0000
draft: false
tags: [Affermazione Civile]
---

Il 20 agosto due membri dell'associazione radicale Certi Diritti, Ottavio Marzocchi e Ximo Nogueroles Garcia, si sono sposati in Spagna. Lunedì a Bologna chiederemo al sindaco di trascrivere l'atto sul registro dei matrimoni. In caso di opposizione ricorso alla Cedu. Appuntamento alle ore 12 in Piazza Maggiore con personalità e associazioni lgbte.

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Lo scorso 20 agosto si sono sposati in Spagna due membri dell’Associazione Radicale Certi Diritti, Ottavio Marzocchi, bolognese, funzionario a Bruxelles del Parlamento Europeo, membro del Consiglio Generale del Partito Radicale Nonviolento, responsabile delle questioni europee di Certi Diritti e Joaquin Nogueroles Garcia, funzionario della Commissione Europea.

Lunedì 29 agosto,  a Bologna,  Ottavio Marzocchi e Joaquin Nogueroles Garcia, accompagnati  da Sergio Rovasio e Giacomo Cellottini, Segretario e Tesoriere dell’Associazione Radicale Certi Diritti,  Zeno Gobetti, Segretario Associazione Radicale Giorgiana Masi, Monica Mischiatti, Segretaria Associazione Radicali Bologna, Franco Grilini, Consigliere Regionale Idv ed esponente storico del movimento lgbt, Emiliano Zaino, Presidente Arcigay Il Cassero di Bologna, Cathy La Torre, Capogruppo Sel al Consiglio Comunale di Bologna, si recheranno alle ore 12 in Piazza Maggiore a Bologna, presso l’Ufficio del Sindaco Virginio Merola, per presentare la richiesta di trascrizione dell'Atto della loro unione sul registro dei matrimoni del Comune, chiedendo quindi una copia dell’atto del matrimonio tradotta e autenticata, così come previsto dalle legge per le coppie eterosessual che si sposano all’estero.

Tale iniziativa rientra nella campagna di Affermazione Civile lanciata oltre due anni fa in Italia con l’obiettivo di consentire alle coppie gay di accedere all’istituto del matrimonio civile. La coppia italo-spagnola intende avviare, con il supporto delle Associazioni che si battono per la promozione e la difesa dei diritti civili, qualora il Comune opponesse il diniego, una iniziativa giudiziaria presso la Corte Europea dei Diritti dell’Uomo e presso la Corte di Giustizia Europea affinchè il loro matrimonio venga riconosciuto anche in Italia, oltre che in Belgio dove il matrimonio tra persone dello stesso sesso è possibile da diversi anni. Maggiori dettagli verranno illustrati nel corso dell’incontro con i giornalisti davanti alla sede del Comune di Bologna.

Ottavio Marzocchi, funzionario della Commissione Libe per il Gruppo Alde del Parlamento Europeo, è da anni impegnato in Europa sui temi del superamento e per l’adeguamento delle legislazioni in materia di unioni civili e matrimonio tra persone dello stesso sesso nei paesi membri dell’Ue. La cerimonia civile svoltasi in Spagna è stata celebrata dal fratello di Joaquin Nogueroles Garcia, Consigliere Comunale spagnolo, con un discorso che verrà riproposto in una cerimonia simbolica che si svolgerà a Bologna domenica pomeriggio e che verrà pubblicato nei prossimi giorni nel sito certidiritti.it

Aderiscono all'iniziativa, oltre ai già citati esponenti del movimento, le Associazioni: Arcigay "Il Cassero", Arcilesbica Bologna, AGEDO Bologna, Famiglie Arcobaleno Bologna.

Ufficio Stampa: 06-68979250 – 337/798942

Ricevi la newsletter di Certi Diritti >