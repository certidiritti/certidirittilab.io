---
title: 'Letta e Bonino, al G20 sollevate la questione diritti umani in Russia'
date: Mon, 02 Sep 2013 09:55:58 +0000
draft: false
tags: [Russia]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 2 settembre 2013

Chiediamo al governo italiano di prendere una posizione chiara ed esplicita contro la continua violazione dei diritti umani in Russia durante il G20 che si terrà a San Pietroburgo i prossimi 5 e 6 settembre. La legge contro la cosiddetta "propaganda di rapporti sessuali non tradizionali" tra i minori è l'esempio più eclatante di come in Russia si stiano mettendo a repentaglio non solo i diritti delle persone LGBTI, ma la fondamentale libertà d'espressione di tutti i cittadini russi. Altri esempi sono la legge sui cosiddetti "agenti stranieri" - che ha colpito varie associazioni che si occupano di diritti umani in genere e anche associazioni culturali -, la legge Dima Yakovlev - che introduce il divieto per i cittadini americani di adottare bambini russi e di finanziare l'attività "politica" delle ONG russe, una legge che impone restrizioni all'utilizzo di Internet dando al governo la possibilità di chiudere un sito web che pubblichi materiale contrario alla morale pubblica, una legge penale che estende la definizione di tradimento sino a poter includere le attività internazionali di advocacy a favore dei diritti umani e altre leggi che limitano gravemente la libertà d'assemblea e d'associazione. Nel complesso fa impressione che si stiano reintroducendo repressioni alle libertà che erano presenti nel sistema dittatoriale sovietico.

Sinora la Russia ha ignorato gli appelli delle ONG che difendono i diritti umani, quelli del Comitato per i Diritti Umani delle Nazioni Unite e quelli della Corte Europea dei Diritti dell'Uomo, per questo il governo italiano deve sollevare la questione e chiedere fermamente che la Russia inverta questa tendenza e rispetti gli impegni e obblighi internazionali riguardanti i diritti

umani che ha sottoscritto.

Nei giorni del 3 e 8 settembre ci uniremo anche in Italia al coro di voci che si leva da varie città del mondo quali Londra, New York, Seattle, Rio de Janeiro, Montevideo, Ottawa e Parigi, supportando la campagna globale promossa da All Out anche grazie all'hashtag #Russia4Love e il global kiss-in ToRussiaWithLove".

In Italia manifesteremo: il 3 settembre a Napoli dalle ore 18 alle ore 20 davanti la Prefettura in Piazza del Plebiscito e a Milano dalle ore 18.30 alle ore 20 davanti la Prefettura in Corso Monforte, 31; l'8 settembre alle ore 15 a Firenze in via Guicciardini, 15 a Roma davanti l'Ambasciata russa dalle ore 15 in via Gaeta 5, a Palermo in viale Orfeo, Mondello, a Bari in Corso Benedetto Croce, a Perugia in Piazza 4 Novembre e a Milano, alle 17.30, in piazza San Babila.

Rita De Santis – AGEDO

Flavio Romani – Arcigay

Paola Brandolini – ArciLesbica

Yuri Guaiana - Associazioni Radicale Certi Diritti

Aurelio Mancuso - Equality Italia

Giuseppina La Delfa - Famiglie Arcobaleno

Cecilia D'Avos e Fabrizio Paoletti - Rete genitori Rainbow