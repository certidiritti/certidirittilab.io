---
title: 'NOZZE OMO  ORA LA LEGGE È POSSIBILE'
date: Thu, 13 May 2010 12:26:12 +0000
draft: false
tags: [Senza categoria]
---

Da Vanity Fair n°18 del 12 maggio 2010 a pag.48  
Di Paolo Hutter

**NOZZE OMO**

**ORA LA LEGGE È POSSIBILE**

_Una sentenza della Corte Costituzionale apre nuovi scenari su matrimonio e diritti delle coppie omosessuali. Qui una costituzionalista spiega perché_

«All’opinione pubblica è arrivato il messaggio che la Corte Costituzionale ha respinto i nostri ricorsi per estendere il matrimonio alle coppie omosessuali. Ma la parola "inammissibile" ha, per noi giuristi costituzionali, un altro significato: vuoi dire che la faccenda non compete alla Corte ma al Parlamento, che dovrà legiferare per riconoscere le unioni gay». Marilisa D'Amico, docente di Diritto Costituzionale all'Università di Milano, è stata per l'Associazione Certi Diritti co-relatrice della causa davanti alla Corte Costituzionale. E, sottolinea, «nelle motivazioni della sentenza si chiarisce che per "società naturale" su cui si fondala famiglia (articolo 29 della Costituzione) non si intende l'eterosessualità» .

**Che importanza hanno le motivazioni di una sentenza?**

«Indicano una linea da seguire. Per la Corte stessa, e per i suoi futuri successivi pronunciamenti, sono un vincolo. Per il Parlamento sono solo una forte esortazione. Sapevamo che era quasi impossibile che la Corte spalancasse direttamente le porte del matrimonio ai gay: puntavamo a una sentenza che rinviasse al Parlamento in modo vincolante».

**E perché  non lo ha fatto?**

«Ho l'impressione che si sia trattato di una sentenza di compromesso. Si ammette che i concetti di famiglia e di matrimonio non si possono ritenere cristallizzati all'epoca in cui la Costituzione entrò in vigore, ma poi si aggiunge che le unioni omosessuali non possono essere ritenute omogenee al matrimonio. Nelle motivazioni si dice: "L'aspirazione al riconoscimento non può essere realizzata soltanto attraverso un'equiparazione al matrimonio". Quel "soltanto" è un compromesso».

**Ciononostante lei sembra soddisfatta.**

«È la prima volta che si include solennemente l'unione omosessuale nelle formazioni sociali "ove si svolge la personalità dell'uomo" ( articolo 2 della Costituzione) e si parla del diritto a un riconoscimento giuridico nei modi, nei tempi e nei limiti stabiliti dalla legge. Il che significa che una legge bisogna farla. E ora è aperta la strada per i ricorsi alla Corte Europea dei Diritti dell'Uomo».