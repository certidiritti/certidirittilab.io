---
title: 'MAURO RINUNCIA A CANDIDATURA PRESIDENTE DEL PE: GRAZIE DA CERTI DIRITTI'
date: Mon, 06 Jul 2009 09:44:39 +0000
draft: false
tags: [Comunicati stampa]
---

**CERTI DIRITTI: SODDISFAZIONE PER LA DECISIONE DI MARIO MAURO DI RITIRARSI DALLA CORSA A PRESIDENTE DEL PARLAMENTO EUROPEO**

Roma, 6 luglio 2009

_**Dichiarazione di Sergio Rovasio, Segretario di Certi Diritti:**_

"L'Associazione Radicale Certi Diritti si complimenta con Mario Mauro, capodelegazione della PDL al Parlamento europeo, per la sua decisione di ritirarsi dalla corsa alla carica di Presidente del PE, decisione presa con Silvio Berlusconi dopo avere ricevuto le pressioni del presidente del gruppo PPE al Parlamento Europeo. La nostra associazione aveva già espresso la propria contrarietà a tale candidatura ed espresso il timore di dovere assistere ad un "Buttiglione 2", ovvero all'ennesima sonora bocciatura dell'Italia davanti all'Europa per la visione troppo integralista-religiosa di questi candidati, imposti nelle sedi europee, che non riscuotono neanche il consenso del gruppo politico europeo di appartenenza.

La sconfitta di Mauro é il risultato di una serie di cause: innanzitutto alcuni punti di forza di Buzek, il candidato alternativo del PPE, che é dell'Europa dell'Est ed é ex primo ministro polacco e protestante. Mauro é, come ben sanno i suoi colleghi, di idee cattoliche integraliste - come provano le sue dichiarazioni raccolte nel dossier di Certi Diritti - che si scaglia nei discorsi politici contro la regolamentazione delle coppie dello stesso sesso affermando l'importanza della famiglia naturale, in totale contraddizione con i divorziati e gli organizzatori di feste con veline e amichette varie di suoi autorevoli colleghi di partito; Mauro aveva anche partecipato alla scuola di formazione per veline candidate al PE e difeso alcune ipocrite e assurde posizioni interne del suo partito in totale contraddizione con 'le radici cristiane dell'Europa' da lui tanto sbandierate. Grazie quindi a Mauro per la sua saggia decisione.

Naturalmente ci auguriamo che Mauro possa continuare a predicare le sue idee da altri pulpiti piuttosto che quello della presidenza del PE. Se egli sarà Presidente della Commissione Esteri, ad esempio, potrà meglio condurre le sue battaglie per la libertà religiosa, con il sostegno ampissimo di forze politiche e sociali, in Italia, in Europa e nel mondo".