---
title: 'CONFERENZA NAZIONALE SULLA LEGALIZZAZIONE DELLA PROSTITUZIONE'
date: Wed, 04 Apr 2012 20:38:14 +0000
draft: false
tags: [Lavoro sessuale]
---

INVITO   
CONFERENZA NAZIONALE SULLA LEGALIZZAZIONE DELLA PROSTITUZIONE  
Roma, 21 aprile 2012

Il prossimo 21 aprile l’Associazione Radicale Certi Diritti, il Comitato per i Diritti Civili delle Prostitute e la Cgil-Nuovi Diritti, terranno a Roma una Conferenza Nazionale sulla Legalizzazione della Prostituzione alla quale ti invitiamo a partecipare.

Qui di seguito il programma quasi definitivo. Per preannunciare la partecipazione ti invitiamo a darcene comunicazione all’indirizzo e-mail: [info@certidiritti.it](mailto:%3Ca%20href=)

[Scarica il programma](http://www.certidiritti.org/wp-content/uploads/2012/04/Prostituzione - 21 aprile agg 11042012_copy.doc)

[info@certidiritti.it](mailto:%3Ca%20href=)"  
Tel. [06-68979250](tel:06-68979250)