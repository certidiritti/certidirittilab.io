---
title: 'Comunicato del Comitato Internazionale sui Diritti del Lavoratori dell''Industria del Sesso in Europa'
date: Sun, 02 Feb 2014 16:58:17 +0000
draft: false
tags: [Lavoro sessuale]
---

L'Associazione Radicale Certi Diritti condivide integralmente e sottoscrive il contenuto del comunicato del Comitato Internazionale sui Diritti del Lavoratori dell'Industria del Sesso in Europa che diffondiamo volentieri

Amsterdam, 24 Gennaio 2014

Un approccio retrogrado ai diritti dei lavoratori dell'industria del sesso: salute e sicurezza vengono ora tutelate dal Comitato del Parlamento Europeo per i Diritti della Donna e la Parità di Genere.\[i\]\[1\]

Il Comitato Internazionale per i diritti dei lavoratori dell'industria del sesso in Europa (International Committee on the Rights of Sex Workers in Europe - ICSRE) condanna duramente la decisione del Comitato del Parlamento Europeo per i Diritti della Donna e la Parità di Genere (FEMM) di criminalizzare i clienti delle prostitute.

Contrariamente a quanto richiesto dalle organizzazioni di sex workers di tutta Europa, molte organizzazioni sanitarie e della società civile, FEMM ha votato in favore di una misura repressiva che, con il pretesto di proteggere le donne, ne aumenterà solo la vulnerabilità.

Siamo allibiti nel vedere che Mary Honeyball, politica londinese e autrice della misura approvata oggi, possa ignorare così ostentatamente il fatto che la criminalizzazione dei clienti non sia stata mai efficace nel ridurre il fenomeno prostitutivo e della tratta. Tutt'altro, è stata infatti indicata come una delle cause dello sfruttamento della prostituzione clandestina. E contrariamente a quanto affermato da Ms Honeyball ha portato anche alla criminalizzazione della prostituzione.

Tra le altre cose la polizia svedese menziona che "Nel 2009 il National Bureau of Investigation stimò che solo a Stoccolma e in provincia ci fossero 90 centri massaggio Thai, la maggiorparte dei quali impiegavano donne provenienti dalla tratta a scopo prostitutivo. Nel corso del 2011/2012 il numero dei centri massaggi Thai era cresciuto di più del 100%, arrivando a 250 centri, e in tutta la Svezia se ne contavano almeno 450".\[ii\]

Numerosi altri rapporti e saggi supportano questa tesi. In Norvegia, Pro-Sentret, il centro ufficiale per il supporto alle prostitute di Oslo, pubblica nel 2012 il rapporto annuale, evidenziando che il numero delle prostitute non è affatto diminuito e che anzi anche i crimini contro le prostitute erano in aumento. \[iii\] Questo studio, basato su un questionario sottoposto a 123 prostitute, dimostra anche come le prostitute stesse abbiano più riserve di prima nel chiedere aiuto alla polizia, per paura di essere stigmatizzate e con la convinzione di essere trattate come criminali.

Un aspetto preoccupante della criminalizzazione dei clienti concerne la salute dei sex workers, specialmente per quanto riguarda l'HIV/AIDS e altre malattie sessualmente trasmissibili. Non è sorprendente che in un recente articolo dell'Observer Ms Honeyball ignori completamente la domanda. In Francia, nei mesi precedenti al voto per l'Assemblea Nazionale, una vasta gamma di organizzazioni denunciò la mancanza di attenzione alla pandemia dell'HIV e il ruolo cruciale dei sex workers nella lotta alla diffusione, da parte di Najat Valaud-Belkacem, Ministro per i Diritti delle Donne.

100 organizzazioni francesi e altre 100 organizzazioni internazionali hanno firmato un Manifesto contro la criminalizzazione dei clienti, questo gruppo include anche organizzazioni sanitarie come Doctors of the World e ovviamente associazioni di prostitute. \[iv\]

Siamo particolarmente stupiti di come Ms Honeyball possa ignorare le raccomandazioni di esperti del suo proprio paese, come UKNSWP, un'organizzazione-ombrello con oltre 60 membri in tutto il Regno Unito che lavora in prima linea per offrire servizi a lavoratrici e lavoratori dell'industria del sesso in tutto il paese. La loro risposta alla consultazione avvenuta recentemente in Scozia non lascia dubbi circa il fatto che nè gli operatori sociali nè le persone direttamente coinvolte nell'attività di prostituzione siano contrari alla misura approvata in Svezia.\[v\]

"Le ricerche mostrano come la criminalizzazione della prostituta o del cliente possa avere risultati negativi, pericolosi e talvolta fatali, per le prostitute, specialmente coloro che operano sulla strada. Per loro infatti, la criminalizzazione spesso porta a spostamenti forzati e li costringe a lavorare in luoghi più remoti e quindi più pericolosi. Questo aumenta le possibilità di divenire vittime di violenza ed espone questa frangia dei sex workers a condizioni di vulnerabilità e sfruttamento.

Nella prostituzione "non in strada" la criminalizzazione del cliente aumenta la stigma legato ai lavoratori dell'industria del sesso e gli operatori temono con questa legge di diventare bersagli di indagini, diminuendo così considerevolmente la loro fonte di guadagno. Questo agisce come una enorme barriera nella denuncia di crimini da parte dei sex workers, aumentando la loro vulnerabilità in tutti i frangenti. Molti di coloro che commettono crimini contro le prostitute lo fanno perchè sanno di godere di un certo grado di impunità, e in con questa legge questi crimini resteranno sempre più impuniti".

Il Comitato del Parlamento Europeo per i Diritti delle Donne e la Parità di Genere sta tristemente seguendo la stessa traccia, mettendo l'ideologia davanti alla salute e ealla sicurezza delle donne e degli uomini coinvolti in prima persona, incurante dei suggerimenti offerti dai vari stakeholders del settore e degli studi fatti. Come ad esempio il rapporto pubblicato a dicembre 2011 dal Gruppo di Studio sull'HIV e l'Industria del sesso di UNAIDS.

"Gli Stati dovrebbero smettere di criminalizzare la prostituzione o le attività ad essa associate. La decriminalizzazione dovrebbe includere anche il commercio di prestazioni sessuali, la gestione (regolata legalmente) di prostitute e case di piacere e tutte le altre attività legate all'industria del sesso".

Quanti pubblicazioni e rapporti devono essere pubblicati prima che gruppi femministi e organizzazioni varie inizino ad ascoltare coloro coinvolti in prima persona in questo settore? Ma più importante, quanti dei nostri colleghi dovranno essere uccisi perchè qualcuno decida fnalmente di ascoltarci? I sex workers hanno una voce. E questa voce dice NO al modello Svedese.

Cordialmente,

Luca Stevenson

Coordinatore, Comitato Internazionale sui Diritti del Lavoratori dell'Industria del Sesso in Europa

[http://www.sexworkeurope.org/](http://www.sexworkeurope.org/)

* * *

\[i\] http://www.ibtimes.co.uk/european-parliament-vote-nordic-model-prostitution-that-fines-clients-1433466?utm\_source=hootsuite&utm\_campaign=hootsuit

\[ii\] http://www.polisen.se/Global/www%20och%20Intrapolis/Informationsmaterial/01%20Polisen%20nationellt/Engelskt%20informationsmaterial/Trafficking\_1998\_/Trafficking\_report\_13_20130530.pdf

\[iii\] http://www.thelocal.no/20120622/rip-up-prostitution-law-says-top-oslo-politician

\[iv\] http://site.strass-syndicat.org/2013/09/manifeste-contre-la-penalisation-des-prostituees-et-de-leurs-clients/

\[v\] http://www.uknswp.org/wp-content/uploads/UKNSWP\_Scotland\_consultation\_response\_dec_2012.pdf