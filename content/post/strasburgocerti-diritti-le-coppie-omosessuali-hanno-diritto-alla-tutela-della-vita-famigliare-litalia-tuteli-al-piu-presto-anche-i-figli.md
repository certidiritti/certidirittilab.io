---
title: 'STRASBURGO/CERTI DIRITTI: LE COPPIE OMOSESSUALI HANNO DIRITTO ALLA TUTELA DELLA VITA FAMIGLIARE. L''ITALIA TUTELI AL Più PRESTO ANCHE I FIGLI.'
date: Thu, 30 Jun 2016 16:05:14 +0000
draft: false
tags: [Diritto di Famiglia]
---

[![Corte-di-Strasburgo-1024x659](http://www.certidiritti.org/wp-content/uploads/2015/12/Corte-di-Strasburgo-1024x659-300x193.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/12/Corte-di-Strasburgo-1024x659.jpg)"Complimenti a ILGA-Europe e all'avvocato Robert Wintemute per il successo che ha importanza per tutti gli quegli Stati europei che ancora non tutelano adeguatamente i diritti umani delle coppie omosessuali. Sono avvertiti, non è più ammissibile e verranno sanzionati", dichiara Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti.

"Purtroppo però la sentenza è arrivata troppo tardi per Roberto Taddeucci e Douglas McCall che, a causa delle continue violazioni dei diritti umani da parte dello Stato italiano, sono stati costretti a rifugiarsi ad Amsterdam dove ora vivono. Nel 2016 questo non è più accettabile: basta rifugiati all’estero per amore, basta viaggi della speranza per una piena eguaglianza. Dopo il primo passo sulle unioni civili che sanano anche casi come questo, occorre riconoscere finalmente il matrimonio egualitario per garantire, non solo le coppie che si spostano da un paese all'altro, ma anche le famiglie con figli", conclude Guaiana.

Roma, 30 giugno 2016