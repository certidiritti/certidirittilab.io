---
title: 'Dopo la Francia socialista, anche la Gran Bretagna conservatrice dice sì al matrimonio egualitario. L''Italia rimane Vaticana'
date: Tue, 05 Feb 2013 20:28:54 +0000
draft: false
tags: [Europa]
---

Comunicato stampa dell’Associazione Radicale Certi Diritti.

Roma, 5 febbraio 2013  

La proposta di legge per rendere il matrimonio egualitario in Inghilterra e Galles è stato approvata dalla Camera dei Comuni in seconda lettura con 400 voti a favore e 175 contro. Ora il provvedimento passa alla Camera alta del Parlamento, the House of Lords, che dovrà pronunciarsi a maggio. Poi tornerà ai Commons per un secondo voto, ma il primo ostacolo è stato superato. Anche la Gran Bretagna conservatrice, dopo la Francia socialista, straccia l’Italia in materia di diritto di eguaglianza.

Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti, dichiara: “Mentre la Gran Bretagna conservatrice si aggiunge alla Francia socialista nella marcia verso l’uguaglianza, l’Italia vaticana rimane ferma al medioevo e continua a discriminare i suoi cittadini. Il confronto tra le dichiarazioni del Primo Ministro inglese, il conservatore Cameron e quelle di Bersani sul tema è imbarazzante. Il primo dice: “questo è un sì all’eguaglianza, ma anche a rendere la nostra società più forte \[…\] penso che questo sia un importante passo avanti per il nostro paese”. Bersani rimane invece uno strenuo sostenitore delle unioni civili secondo un non meglio specificato modello tedesco per “evitare traumi”. Evidentemente i politici italiani, di tutti i colori, sono più preoccupati dei traumi che possono causare oltre Tevere che dei cittadini italiani, i quali possono continuare a essere discriminati e vedersi preclusa la possibilità di scegliere liberamente chi sposare! In Europa il matrimonio egualitario è realtà già in Norvegia, Svezia, Danimarca, Islanda, Olanda, Belgio, ma anche Spagna e Portogallo, paesi cattolici quanto l’Italia. Questa piaggeria dei politici italiani nei confronti del Vaticano, sulla pelle cittadini non è davvero più ammissibile”.