---
title: 'Giornata mondiale contro la droga. Mettiamo fine alla War on Drug'
date: Thu, 23 Jun 2011 11:59:41 +0000
draft: false
tags: [Politica]
---

**I diritti umani prima di tutto. Politiche innovative di riduzione del danno e prevenzione anche in carcere.**

**Il 26 giugno ricorre la Giornata mondiale contro la Droga. In questi stessi giorni la Guerra alla Droga compie 40 anni. www.lila.it**

Lanciata da Richard Nixon il 17 giugno del 1971, la “War on Drugs” non ha scalfito le cifre dei consumi di tutte le droghe e quelle dei fatturati delle criminalità organizzate globali. Che al contrario sono decisamente aumentati, così come sono aumentati i costi umani e sociali legati all'abuso di sostanze stupefacenti, per non parlare di quelli militari e di polizia. La Guerra alla Droga è stata un fallimento.

È venuto il momento di cambiare strategia. Di fondare gli interventi di contenimento dei consumi e riduzione del danno su presupposti diversi da quelli ideologici e opportunisti perseguiti finora, ovvero di fare  riferimento alle evidenze scientifiche e empiriche, così come indicato dal Report sulla War on Drugs presentato il 2 giugno scorso dalla Global Commission on Drug Policy.

«Non si capisce quale differenza ci sia tra il rendere disponibile una siringa sterile e il rendere disponibile un luogo pulito e sicuro dove utilizzarla. In entrambi i casi non si tratta di consenso sociale all'utilizzo di sostanze, ma di un intervento sanitario, che non produce un aumento di consumo di stupefacenti, bensì il contrario, oltre a un drastico calo delle overdose e delle infezioni da Hiv e epatiti», afferma Alessandra Cerioli, presidente nazionale della Lega Italiana per la Lotta contro l'Aids.

La LILA chiede che anche in Italia si proceda verso strumenti innovativi di riduzione del danno, comprese le stanze per l'autoconsumo in sicurezza (safe-injection room). In Svizzera, Germania, Canada, Australia, dove sono state realizzate, ormai da anni, e monitorate, hanno dimostrato la loro efficacia. La prevalenza di Hiv tra i consumatori di eroina risulta ridotta e contenuta nei Paesi che hanno introdotto strumenti innovativi di riduzione del danno. In Italia, che è bene ricordare essere il paese dell'Europa occidentale che storicamente consuma più eroina, la prevalenza media è del 12  per cento, una percentuale piuttosto alta (in Germania, per esempio, è del 3-5 per cento), che in alcune regioni arriva a superare il 30 per cento. E proprio le Regioni, vista l'indisponibilità del governo, potrebbero avviare tali strumenti.

«Le stanze devono ripondere anche alle nuove modalità di consumo, e di policonsumo», continua la presidente Cerioli, «le vecchie strategie di riduzione del danno, che hanno comunque arginato la trasmissione dell'Hiv, non sono più sufficienti, oggi le diverse droghe non solo si inettano, si fumano e si inalano. Servono inoltre nuovi kit di prevenzione per limitare ad esempio la diffusione dell'epatite C, che può essere trasmessa “sniffando” con lo stesso strumento».

La LILA chiede anche che, come raccomandato da tutte le agenzie internazionali, a partire dalle Nazioni Unite, e come già accade in oltre 60 Paesi, gli strumenti di riduzione del danno, preservativi e siringhe sterili, vengano introdotti anche negli Istituti penitenziari. Visto anche che oltre il 50 per cento della popolazione detenuta è tale per motivi legati al consumo di droga, questa introduzione appare quantomai urgente.

«Le carceri italiane sono piene di consumatori e piccoli spacciatori, grazie a un impianto legislativo orientato alla repressione. Un impianto che non produce dissuasione, ma solo ulteriore marginalità. È necessario rivedere la legge Fini-Giovanardi, di cui nessuno parla più da molto, troppo tempo. La Guerra alla Droga è la migliore amica dell'Hiv, è venuto il momento di cambiare paradigma e di mettere al primo posto i diritti umani», conclude Alessandra Cerioli.

www.lila.it