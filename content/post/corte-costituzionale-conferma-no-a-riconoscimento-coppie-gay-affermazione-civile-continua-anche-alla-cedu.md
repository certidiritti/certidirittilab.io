---
title: 'Corte Costituzionale conferma no a riconoscimento coppie gay. Affermazione Civile continua anche alla Cedu'
date: Thu, 06 Jan 2011 10:24:29 +0000
draft: false
tags: [Comunicati stampa]
---

**![CEDU](images/stories/CEDU.jpg)CORTE COSTITUZIONALE DICE ANCORA NO ALLE COPPIE LESBICHE E GAY CHE CHIEDONO L’ACCESSO ALL'ISTITUTO DEL MATRIMONIO.  AFFERMAZIONE CIVILE CONTINUA! LE COPPIE GAY E LESBICHE CHE SI SONO SPOSATE ALL’ESTERO CHIEDANO PARITA’ DI RICONSOCIMENTO ANCHE IN ITALIA.**

**Comunicato Stampa dell’Associazione Radicale Certi Diritti:**

**Roma, 6 gennaio 2010**

La campagna di Affermazione Civile prevede di presentare ricorso contro i Comuni che oppongono il diniego alle coppie lesbiche e gay che chiedono le pubblicazioni matrimoniali. A seguito di questa campagna la Corte Costituzionale ha deciso di respingere tutti i ricorsi loro inviati. L’ultimo è stato depositato lo scorso 3 gennaio. Nella decisione  si legge che la Corte:

a) dichiara la manifesta inammissibilità della questione di legittimità costituzionale degli articoli 93, 96, 98, 107, 108, 143, 143-bis, 156-bis, 231 del codice civile, sollevata, in riferimento all’articolo 2 della Costituzione, dal Tribunale di Ferrara con l’ordinanza indicata in epigrafe;

b) dichiara la manifesta infondatezza della questione di legittimità costituzionale degli articoli sopra indicati del codice civile, sollevata, in riferimento agli articoli 3 e 29 della Costituzione, dal Tribunale di Ferrara con la medesima ordinanza.

Tale decisione è l’esatta fotocopia della penultima sentenza dello scorso ottobre sullo stesso tema. La Corte evidentemente ha preferito lo status quo che nulla a che vedere con i cambiamenti sociali e culturali del nostro paese, già immobilizzato da una classe politica sempre più asservita al potere clericale vaticano.

**L’Associazione Radicale Certi Diritti continua la sua lotta per il superamento delle diseguaglianze e rilancia la via legale, la campagna di ‘Affermazione Civile’, allargando anche alle coppie lesbiche e gay, costrette a sposarsi all’estero, l’invito a intentare ricorsi oltre che in Italia anche alla Corte Europea dei Diritti dell’Uomo affinchè l'unione venga riconosciuta dall'Italia.** Occorre ricordare che il mancato riconoscimento delle coppie che si sono sposate in altri paesi membri dell’Ue, avviene in aperta violazione dei Trattati di Nizza e di Lisbona.

per contattarci info@certidiritti.it