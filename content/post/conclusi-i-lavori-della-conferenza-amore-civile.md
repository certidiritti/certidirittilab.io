---
title: 'CONCLUSI I LAVORI DELLA CONFERENZA ''AMORE CIVILE'''
date: Thu, 15 May 2008 06:20:34 +0000
draft: false
tags: [Comunicati stampa]
---

Si sono conclusi questa mattina a Roma, [i lavori della Conferenza su 'Amore civile, nuove forme di convivenza e relazioni affettive'](http://www.radioradicale.it/amore-civile-nuove-forme-di-convivenza-e-relazioni-affettive-roma-10-12-maggio-2008) dedicata allo studio e l'analisi sulle carenze legislative in materia di diritto di famiglia, promossa dall'Associazione Luca Coscioni, Associazione Radicale Certi Diritti e Radicali Italiani. Ai lavori hanno partecipato personalità politiche, accademici, rappresentanti di Associazioni lgbt, tra le altre, DiGayProject, Aricgay, Mario Mieli.  
  
I lavori sono stati trasmessi in diretta su Radio Radicale ed [è possibile riascoltarli a questo link](http://www.radioradicale.it/amore-civile-nuove-forme-di-convivenza-e-relazioni-affettive-roma-10-12-maggio-2008)  
  
Alla Conferenza stampa conclusiva, presieduta da Enzo Cucco, dal giurista Bruno De Filippis, e dalla deputata del Pd Paola Concia, alla quale hanno partecipato parlamentari radicali  e del Pd, erano presenti gli esponenti delle Associazioni lgbt Aurelio Mancuso, Presidente nazionale di Arcigay; Maria Gigliola Toniollo, Cgil Nuovi Diritti; Liela Deianis, Presidente Libellula2001; Andrea Maccarone, Circolo di Cultura omosessuale Mario Mieli.  
  
Durante la Conferenza Stampa sono stati illustrate alcune delle Proposte di legge che verranno depositate  in questi giorni, tra le altre alcune per la difesa e i diritti per le persone lgbte:  
  
\- Matrimonio esteso alle persone lesbiche e gay;  
  
\- Unioni civili;  
  
\- Lotta all'omofobia (già depositata da Franco Grillini nella scorsa Legislatura);  
  
\- Norme per la tutela dei diritti delle persone Transessuali (già depositata da Franco Grillini nella scorsa Legislatura, testo più aggiornato rispetto a quello che depositarono i radicali nel 2006). Al seguente link [la registrazione audio-video della Conferenza Stampa](http://www.radioradicale.it/scheda/253638)  
  
   
Nei prossimi giorni i testi delle Proposte di Legge saranno disponibili nei seguenti siti:  
  
www.radicali.it  
  
www.certidiritti.it  
  
www.radioradicale.it  
   
_Organizzazione della Conferenza: Diego Galli e Sergio Rovasio_