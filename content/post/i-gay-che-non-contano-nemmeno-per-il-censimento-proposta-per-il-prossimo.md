---
title: 'I GAY CHE NON CONTANO, NEMMENO PER IL CENSIMENTO: PROPOSTA PER IL PROSSIMO'
date: Wed, 27 Jan 2010 12:08:53 +0000
draft: false
tags: [Comunicati stampa]
---

#### I GAY CHE NON CONTANO

di [Carlo D'Ippoliti](http://www.lavoce.info/lavocepuntoinfo/autori/pagina804.html) Da www.laVoce.info 26.01.2010

Non è semplice studiare le caratteristiche socio-economiche della popolazione lesbica, gay, bisessuale, transgender attraverso indagini campionarie. Per questo il censimento decennale è un'occasione unica: include un numero molto elevato di osservazioni e nello stesso tempo le stime basate su di esso sono per definizione rappresentative dei valori corrispondenti all'intera popolazione. Anche l'Italia potrebbe così impostare le sue politiche sulla base di dati invece che di opinioni precostituite. Il ruolo fondamentale dell'Istat.

_In questo inizio di 2010 l'Istat si accinge alla preparazione del prossimo **censimento**della popolazione italiana. Si tratta di un’occasione particolarmente importante per i ricercatori che si occupano di popolazioni marginali o relativamente piccole, come ad esempio la popolazione lesbica, gay, bisessuale, transgender (Lgbt)._

_LE VICENDE DELL’ULTIMA RILEVAZIONE_

_Lo studio delle caratteristiche socio-economiche di questa popolazione tramite indagini campionarie è reso particolarmente difficoltoso per l’esigua numerosità campionaria che si otterrebbe contando la popolazione di interesse in un campione rappresentativo dell’intera popolazione. Viceversa, l’uso di **campioni** non casuali - cioè costruendo il campione proprio andando a intervistare la parte di popolazione di interesse - potrebbe fornire numerose osservazioni, ad esempio, sulla sotto-popolazione nella provincia di Roma, con l’inconveniente però di produrre risultati non facilmente generalizzabili o in un certo senso non indicativi dei “veri” valori dell’intera popolazione Lgbt **(1)**.  
Ecco dunque che il censimento costituisce una soluzione pressoché unica: include un numero molto elevato di osservazioni (praticamente l’intera popolazione) e al contempo le stime basate su di esso sono, per definizione, rappresentative dei “veri” valori corrispondenti all’intera popolazione.  
Purtroppo, però, la forza dei numeri ha un valore comunicativo che trascende la cultura e la scienza, per approdare alla politica. Così, in occasione dello scorso censimento, molte coppie di persone dello stesso sesso (con o senza figli) [compilarono un solo modulo](http://www.gay.it/channel/attualita/10732/IL-CENSIMENTO-GAY-C%2592%25C8.html) e si dichiararono come **persone conviventi non sposate**. Poi, però l'Istat ha considerato queste osservazioni come “dati incongruenti”, e ha di suo arbitrio [riattribuito il valore della risposta](http://www.gay.it/channel/attualita/17993/CANCELLATE-LE-COPPIE-GAY.html) “convivente dell’intestatario” con un’altra risposta: “altra persona convivente senza legami di parentela”. Con questa scelta, l’Istituto ha reso non riconoscibili le unioni affettive da quelle di altra natura, come ad esempio la convivenza del personale collaboratore domestico, o degli studenti fuori sede.  
Una possibile motivazione è che, a differenza di molte indagini campionarie, il questionario del censimento è auto-compilato, dunque passibile di **errori** e false o mancate dichiarazioni. Questi problemi, però, non andrebbero risolti modificando ex-post i dati, o negando la realtà, ma adottando apposite tecniche di analisi, che in fase di stima tengano conto delle fonti di disturbo nell’informazione raccolta. **(1)**  
Nel 2004 rispondendo a un’interrogazione da dodici parlamentari, l’allora sottosegretario per i rapporti con il Parlamento espose una seconda possibile ragione della decisione di riattribuire le risposte fornite dai cittadini e dalle cittadine, come si legge nel [resoconto stenografico della seduta](http://www.gaynews.it/view.php?ID=27867) alla Camera: “i **dati personali** idonei a rilevare la vita sessuale \[__sic\] di un individuo sono considerati, ai sensi dell'articolo 4 del decreto legislativo n. 196 del 2003, dati sensibili e pertanto, ai sensi dell'articolo 20 dello stesso decreto legislativo, il trattamento dei dati sensibili da parte dei soggetti pubblici è consentito solo se conformi a disposizioni fissate dal legislatore; occorre cioè l'autorizzazione effettuata da una espressa disposizione di legge”.  
Se da un lato, tale indicazione può illuminare questa volta la stesura del **regolamento** di esecuzione del censimento (si è ancora in tempo), rimane alquanto controverso il fatto che, di fronte a un vuoto normativo in materia di protezione dei dati, questi vengano modificati dall’ente che, eventualmente_ _contra legem, li ha raccolti. Inoltre, non è privo di rilevanza che i dati raccolti contenessero informazioni sulle coppie Lgbt così come loro stesse avevano deciso di presentarsi: avevano infatti sempre la possibilità di compilare due diversi questionari, o di dichiararsi “altri conviventi senza relazioni di parentela”. **(2)**_

COME SFATARE I LUOGHI COMUNI

_Del resto, l’informazione sulla popolazione Lgbt nel censimento è raccolta e utilizzata da numerosi paesi senza che questo generi barriere di impossibilità sul piano statistico né giuridico. Così ad esempio negli Stati Uniti, proprio grazie ai dati del censimento, i ricercatori hanno scoperto che, a dispetto degli **stereotipi** sui “ricchi ed eleganti” gay, secondo i luoghi comuni avvantaggiati dal non avere figli, le coppie omosessuali godono di un **reddito medio****(3)**  
Anche in Italia, l’informazione che si ricaverebbe non è scontata a priori, come ad esempio si può vedere con un semplice esperimento. Considerando solo le coppie di persone adulte dello stesso sesso, non legate da vincoli di parentela né affinità, è possibile ottenere 151 osservazioni nel database dell’Indagine dei bilanci delle famiglie italiane della Banca d’Italia nel 2007. Emerge che le coppie dello stesso sesso conviventi sarebbero più frequenti nel Nord e nelle Isole, e meno al Sud, e che il loro reddito medio individuale è pari a circa 17.500 euro annui, contro i 18.600 della media della popolazione italiana. Secondo diverse altre misure, il reddito delle coppie conviventi sembrerebbe **inferiore alla media**, sebbene la distribuzione dei titoli di studio non mostri differenze significative (si veda tabella sotto). Più basso del complesso della popolazione, e sono a maggior rischio di povertà._

_Tabella 1. Distribuzione dei titoli di studio_

Totale Popolazione

Coppie conviventi  
dello stesso sesso

Titolo di studio

Numero

Percentuale

% Cumulata

Numero

Percentuale

% Cumulata

Nessuno

2.284

11,78

11,78

9

5,45

5,45

licenza elementare

4.197

21,65

33,43

43

26,06

31,52

scuola media inferiore

5.621

29

62,43

50

30,3

61,82

Diploma professionale (3 anni)

112

5,78

68,2

13

7,88

69,7

Diploma media superiore

4.564

23,54

91,75

41

24,85

94,55

laurea breve

142

0,73

92,48

4

2,42

96,97

Laurea

1.416

7,3

99,78

5

3,03

100

post-laurea

42

0,22

100

Totale

19.386

100

165

100

_Fonte: nostre elaborazioni su dati Banca d’Italia_

Queste osservazioni evidentemente non sono rappresentative dell’intera popolazione Lgbt e oltre a includere il caso dei conviventi non legati da relazioni affettive, soffrono della grave limitazione costituita dal numero eccessivamente basso. Ad ogni modo, indicano come un’analisi più approfondita e meglio documentata potrebbe rivelare una realtà di marginalizzazione e **discriminazione** di segno opposto agli stereotipi dominanti.È importante che l’Italia accetti la sfida di governare la realtà cercando di conoscerla, impostando le politiche sulla base dei dati disponibili invece delle opinioni precostituite. In un tale salto di qualità, il ruolo di un ente come l’**Istat** è fondamentale: ci auguriamo che in questo come in altri passaggi storici, se ne mostri all’altezza.

* * *

_**(1)** Queste osservazioni non vogliono negare l’evidenza di problemi di selezione nel campione delle persone che deciderebbero di “uscire allo scoperto”, seppure in forma anonima, nel censimento. Semplicemente, questa fonte di dati sará pressoché a costo zero e presenterá una forma di distorsione diversa dalle fonti campionarie, dunque un’ottima occasione di studio e confronto.  
**(2)** Si veda ad esempio il metodo proposto da Berg, N. e Lien, D. “Same-sex sexual behaviour: US frequency estimates from survey data with simultaneous misreporting and non-response”,_ _AppliedEconomics, Volume [38](http://www.informaworld.com/smpp/title~db=all~content=t713684000~tab=issueslist~branches=38#v38), Issue [7](http://www.informaworld.com/smpp/title~db=all~content=g746012360) April 2006, pages 757 - 769.  
**(3)** Questa volta il margine discrezionale è ridotto, rispetto ai censimenti precedenti, dalla pubblicazione di un regolamento del Parlamento europeo e del Consiglio (Ce 763/2008). Ad ogni modo, il regolamento individua solo i criteri generali e le variabili minime che devono essere rilevate in ogni censimento degli Stati membri. Così, ad esempio è lasciato alle autorità nazionali di decidere se chiedere solo il sesso biologico delle persone, o anche l'identità di genere, ovvero se lasciare che le persone trans possano decidere in qualche genere essere classificate.  
**(4 )** Si veda ad esempio M.V. Lee Badgett (2001)_ _Money, Myths, and Change, University of Chicago Press, Chicago._