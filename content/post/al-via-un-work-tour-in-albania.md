---
title: 'Al via un work tour di Certi Diritti in Albania'
date: Wed, 13 Mar 2013 14:59:03 +0000
draft: false
tags: [Europa]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 13 marzo 2013

L'Associazione Radicale Certi Diritti e PINK Embassy / LGBT Pro Albania hanno organizzato un work tour in Albania nel quale Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, incontrerà attivisti del mondo LGBTI albanese, esponenti politici e dei media nazionali, studenti e la più ampia comunità LGBTI con l'intento di condividere informazioni e buone prassi, nonché l'esperienza italiana nella lotta per l'affermazione dei diritti delle persone LGBTI.

Il progetto si articolerà in una serie di appuntamenti che avranno luogo dal 14 al 21 marzo 2013. Tra essi vi saranno:

*   un incontro con S.E. Massimo Gaiani, ambasciatore italiano a Tirana;
*   un incontro con Vasilika Hysi, deputata socialista al parlamento albanese;
*   un incontro con Artur Nura, membro del Consiglio Generale del [Partito Radicale Nonviolento Transnazionale e Transpartito](http://www.radicalparty.org/);
*   la partecipazione alla conferenza regionale "LGBT rights in the Western Balkans – Achievements and Challenges";
*   alcune lezioni sulle cause pilota avviate in Italia all'Università di Tirana e all'Università di New York a Tirana.
*   varie presentazioni sulla situazione dei diritti delle persone LGBTI in Italia.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: "siamo molto felici di quest'iniziativa, frutto della grande attenzione che prestiamo all'ambito transnazionale. Italia e Albania hanno profondi legami storici e culturali che permetteranno di rendere particolarmente fruttuosa questa condivisione d'informazioni, esperienze e buone prassi. Particolare importanza riveste l'incontro con l'ambasciatore italiano in Albania che dimostra così attenzione al tema e un avvicinamento alla sensibilità di altre missioni europee. L'incontro con Artur Nura permetterà di consolidare un rapporto tra attivisti LGBTI e gli attivisti radicali albanesi. L'occasione permetterà anche un confronto con gli attivisti di altri paesi balcanici che si riuniranno per identificare i passi comuni da intraprendere per fare avanzare i diritti delle persone LGBTI nella regione, nonché la costruzione di una rete virtuosa di contatti. Questo scambio di informazioni, esperienze e buone prassi permetterà una nuova e maggiore collaborazione in favore del rispetto dei diritti umani tra le associazioni e, auspicabilmente, tra le più ampie comunità LGBTI di una regione europea fondamentale".