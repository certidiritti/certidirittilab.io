---
title: 'Sul caso di Emilio Rez, cantautore romano sfrattato perché gay'
date: Sun, 13 Jun 2010 15:27:02 +0000
draft: false
tags: [Senza categoria]
---

[Cantautore gay denuncia: «Buttato fuori da casa perchè omosessuale»](http://roma.corriere.it/roma/notizie/cronaca/10_giugno_13/gay-sfrattato-casa-1703192172205.shtml)

[Radicali denunciano: 'Gay insultato e sfrattato intervenga sindaco Alemanno'](http://roma.repubblica.it/cronaca/2010/06/13/news/radicali_giovane_gay_insultato_e_sfrattato_intervenga_sindaco_alemanno-4808661/)  
[](http://www.agi.it/roma/notizie/201006131713-cro-r010184-omofobia_radicali_alemanno_intervenga_per_gay_cacciato_da_casa)

[OMOFOBIA: RADICALI, ALEMANNO INTERVENGA PER GAY CACCIATO DA CASA](http://www.agi.it/roma/notizie/201006131713-cro-r010184-omofobia_radicali_alemanno_intervenga_per_gay_cacciato_da_casa)  
[](http://www.gay.it/channel/attualita/29982/Ancora-Omofobia-a-Roma-cacciato-da-casa-perche-gay.html)

[Ancora Omofobia a Roma: cacciato da casa perchè gay](http://www.gay.it/channel/attualita/29982/Ancora-Omofobia-a-Roma-cacciato-da-casa-perche-gay.html)  
[](http://www.libero-news.it/regioneespanso.jsp?id=432176)

[Gay: Ass. Certi Diritti, a Roma ragazzo omosessuale cacciato da casa in affitto](http://www.libero-news.it/regioneespanso.jsp?id=432176)  
[](http://www.infooggi.it/articolo/sei-gay-cantautore-sfrattato-di-casa/1791/)

['Sei gay': cantautore sfrattato di casa](http://www.infooggi.it/articolo/sei-gay-cantautore-sfrattato-di-casa/1791/)  
[](http://www.diariodelweb.it/Articolo/Italia/?d=20100613&id=148246)

[Nuovo caso omofobia a Roma, ragazzo gay cacciato da casa affitto](http://www.diariodelweb.it/Articolo/Italia/?d=20100613&id=148246)