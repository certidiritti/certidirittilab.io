---
title: 'Dodici domande che le associazioni vorrebbero venissero fatte a Putin'
date: Sat, 23 Nov 2013 10:41:34 +0000
draft: false
tags: [Russia]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti.

Roma, 23 novembre 2013

Si è svolto ieri a Trieste il convegno "I diritti umani nella Federazione russa di Putin" - organizzato da Associazione Radicale Certi Diritti, Circolo Arcobaleno Arcigay, Famiglie Arcobaleno e Agedo, in collaborazione con il Comitato per le Pari Opportunità dell'Università degli Studi di Trieste - dove attivisti russi, organizzazioni non governative italiane e internazionali, docenti universitari e funzionari europei hanno fatto il punto sulla grave situazione russa e su come aiutare i cittadini della Federazione a difendere i propri diritti umani.

La richiesta pressante al governo italiano, all'Unione Europea, alle associazioni e alla stampa che è emersa durante i lavori è quella di continuare a porre il tema dei diritti umani, non in forma retorica e cerimoniale, ma puntuale e precisa, mettendo il Presidente russo di fronte alle sue responsabilità.

Tra i 25 trattati e accordi che verranno siglati dai governi russo e italiano il 26 novembre prossimo nel capoluogo giuliano, vi sarà anche quello che inaugurerà l'anno del turismo italo-russo. Durante questo anno molti turisti italiani - tra i quali molte persone LGBTI, famiglie omogenitoriali con bambini e giornalisti - avranno la possibilità di visitare più facilmente il grande paese euroasiatico. L'ondata di violenza omofobia che sta attraversando il Paese dopo il varo, il 29 giugno scorso, della legge contro la "propaganda delle relazioni sessuali non tradizionali" solleva, però, molte preoccupazioni riguardo la vaghezza della legge e la sicurezza dei turisti italiani LGBTI. Si teme molto anche che dopo le olimpiadi la situazione possa ulteriormente aggravarsi sia sotto il profilo giuridico, sia sotto quello sociale.

Chiediamo quindi al Governo italiano di sollevare la questione durante il prossimo bilaterale Italia-Russia chiedendo garanzie alle autorità russe affinché garantiscano che le aggressioni omofobe che già si stanno verificando vengano adeguatamente investigate, perseguite e prevenute. I cittadini italiani hanno anche il diritto di viaggiare pienamente informati, chiediamo quindi al Governo italiano - ma anche ai mas media, agli operatori turistici, al CONI - di domandare alle autorità russe di rispondere ad alcune domande specifiche, riportate in calce a questo comunicato, relative all'ambiguità della legge contro la "propaganda delle relazioni sessuali non tradizionali". È necessario avere delle risposte immediate poiché l'anno del turismo italo-russo è già iniziato e molti turisti sono già in viaggio.

Associazione Radicale Certi Diritti

Arcigay

**Domande relative alle implicazioni della legislazione russa per i turisti italiani LGBTI.**

• Due persone dello stesso sesso che si tenessero per mano o si baciassero in pubblico violerebbero la legge russa? Cosa accadrebbe se queste manifestazioni d'affetto venissero riprese dalla stampa, dalla televisione o diffuse su internet.

• Indossare o portare simboli LGBTI (come i colori dell'arcobaleno) da singoli o gruppi di cittadini italiani e russi verrebbe sanzionato? Ancora, sarebbe legalmente rilevante se questi simboli venissero diffusi dai media o su internet?

• Cosa accadrebbe se una persona parlasse a favore dei diritti delle persone LGBTI in una conversazione privata o in pubblico?

• I turisti italiani, e i cittadini russi, potrebbero parlare positivamente, in privato o con i media, delle proprie famiglie se composte da persone dello stesso sesso?

• Sarebbe possibile distribuire materiale concernente i diritti umani anche di coloro che hanno "relazioni sessuali non tradizionali"?

• Il bambino di una coppia di persone dello stesso sesso può entrare in Russia? Se sì, lo stesso bambino rischierebbe di essere sottratto ai propri genitori?

**Domande relative alle implicazioni della legislazione russa per le imprese e i media coinvolti nell'anno del turismo italo-russo.**

• I media che si occuperanno dell'anno del turismo italo-russo potranno esaminare anche la situazione delle persone LGBTI in Russia? I giornalisti italiani e russi verranno trattati diversamente?

• Fare domande sulla legge contro la "propaganda delle relazioni sessuali non tradizionali" verrebbe considerata una violazione della medesima legge?

• Intervistare giovani al di sotto dei 18 anni che si identificano come gay, lesbiche, bisessuali, transessuali o intersessuali in merito alla loro esperienza di vita costituirebbe una violazione della legge russa?

• Dare una rappresentazione positiva di relazioni o famiglie di persone russe o italiane dello stesso sesso violerebbe la legge russa? Se sì verrebbero sanzionati gli intervistati o i giornalisti?

• Le imprese italiane potrebbero includere nella loro pubblicità per l'anno del turismo italo-russo anche delle coppie di persone dello stesso sesso, dei messaggi a favore dei diritti delle persone LGBTI o dei simboli LGBTI?

Le risposte alle domande sopra elencate varia a seconda della cittadinanza delle persone?