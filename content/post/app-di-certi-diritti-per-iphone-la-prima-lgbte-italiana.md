---
title: 'App di Certi Diritti per iPhone, la prima lgbt(e) italiana'
date: Thu, 25 Aug 2011 10:27:25 +0000
draft: false
tags: [Politica]
---

Notizie e aggiornamenti sui temi dei diritti civili e della liberazione sessuale direttamente e gratis sul tuo Iphone. E da settembre grazie alla App sarà possibile consultare anche la nostra newsletter.

Roma, 25 agosto 2011

Comunicato Stampa dell'Associazione Radicale Certi Diritti

A seguito di una ristrutturazione tecnica del sito, anche la App dell'Associazione Radicale Certi Diritti è stata aggiornata, offrendo ancora più contenuti rispetto alla versione iniziale. Da oggi puoi ricevere news e aggiornamenti sui temi dei diritti civili, della liberazione sessuale, della campagna di Affermazione Civile per il superamento delle diseguaglianze delle persone lgbt(e), della lotta allo strapotere clerical-familista-mulinarobianco e contro il sistema partitocratico-clericale del nostro paese.

Invitiamo tutti i nostri utenti I-Phone a scaricare l'aggiornamento della App di Certi Diritti, direttamente accedendo alla sezione Aggiornamenti dell'AppStore.

Ad oggi gli utenti che hanno scaricato la nostra app sono più di 350 e ci auguriamo che questa nuova versione ci aiuti a diffondere ancora di più l'utilizzo di questa app.  
Infatti è finalmente possibile visualizzare direttamente da I-Phone la newsletter dell'Associazione Radicale Certi Diritti e sono stati potenziate le funzionalità di interazione con commenti e interventi da parte degli utenti.

Infine, abbiamo dedicato una sezione per indicare agli utenti tutti i modi con i quali è possibile sostenerci proprio perchè Certi Diritti è un' Associazione Radicale che in quanto tale non percepisce, direttamente o indirettamente, nessun contributo pubblico nè di partito. Siamo fuori dalle caste e dalle cricche, paghiamo l'ICI sulle nostre case (per chi le ha) e non pesiamo indebitamente sulla crisi economica! Le nostre economie si basano solo sui contributi e sull'impegno volontario di chi ci sostiene.

Per ricevere la nostra NewsLetter (da settembre) è sufficiente andare al sito www.certidiritti.it e seguire le indicazioni.