---
title: 'L''OFFENSIVA NEODOGMATICA, LA REPUBBLICA 19-12-2008, di Gad Lerner'
date: Sat, 20 Dec 2008 11:35:36 +0000
draft: false
tags: [Comunicati stampa]
---

**"Gli ebrei non saranno sottoposti a trattamenti peggiori di quello usato loro per secoli e secoli dai papi"  
  
L'offensiva neodogmatica**

**di Gad Lerner -** **La Repubblica 19 dicembre 2008**

Ogni giorno di più la Chiesa di Benedetto XVI mostra un volto arcigno alle donne e agli uomini del suo tempo. Accusa di «statolatria» il governo spagnolo colpevole di «indottrinamento laico». Scomunica le sentenze della magistratura italiana sul caso Englaro, paragonandole a una condanna a morte. Proclama l'impossibilità del dialogo  
interreligioso, raccomandando di «mettere tra parentesi la propria fede» quando ci si confronta con le altrui confessioni.  
II papa stesso si erge a maestro di dottrine politiche affermando nell'insolita, entusiastica, lettera a Marcello Pera - che l'unica cultura liberale possibile sarebbe quella radicata nell'immagine cristiana di Dio. Sposando così la forzatura identitaria  
del "dobbiamo dirci cristiani" e vincolando le scelte etiche della collettività al principio unilaterale dell'agire "come se Dio ci fosse". Il Dio trinitario cristiano, naturalmente, per l'ennesima volta nominato invano.  
  
L'attacco diretto alla Spagna segnala il disorientamento con cui la Chiesa reagisce alla perdita del ruolo di guida esclusiva della morale pubblica, nell'epoca della biopolitica. Sfiduciato nella sua capacità di esercitare una testimonianza evangelica, Benedetto XVI punta sul rafforzamento di un fronte laico conservatore che assuma la dottrina cattolica come ideologia dell'"ordine naturale"; per influenzare così le scelte inedite che le democrazie sono chiamate a compiere di fronte ai progressi tecnico-scientifici e all'evoluzione dei comportamenti familiari.  
  
Ma il tono virulento che ormai contraddistingue l'attuale pontificato - più politico che teologico - rivela tutta la sua debolezza proprio quando deve fare i conti con le vicissitudini storiche da cui tale debolezza scaturisce. Non a caso il predecessore Giovanni Paolo II aveva impostato il Giubileo del bimillenario cristiano su un tema controverso come la "purificazione della memoria", vincendo le perplessità della Congregazione per la Dottrina della fede. Se la gerarchia cattolica oggi soffre un deficit di credibilità in Spagna, ciò non deriva anche dalla sua infausta alleanza col franchismo? E non a caso, nell'Italia clericale ma scristianizzata di oggi, abbiamo dovuto assistere a una reazione tanto stizzita dopo le parole di Gianfranco Fini sulla vergogna del 1938. Un'offensiva autoassolutoria che sarebbe stata impensabile solo qualche anno fa.  
  
Ho provato disagio di fronte alla raffica di dichiarazioni lanciate all'unisono da storici cattolici che pure avevano scritto pagine tutt'altro che reticenti quando il clima era diverso. A sentirli ora, irriconoscibili, è parso quasi che la vicenda delle leggi razziali  
non riguardasse la Chiesa, e anzi la Chiesa potesse andare orgogliosa del modo in cui si comportarono allora i suoi principali esponenti.

  
Tale superba rappresentazione di sé medesima, aggravata dall'uso di parole sprezzanti nei confronti di chi osa metterla in dubbio, si scontra con una mole di documenti incontrovertibili e noti da tempo. Basterebbe rileggere la corrispondenza tra il gesuita Pietro Tacchi Venturi e il segretario di Stato della Santa Sede, Luigi Maglione,  
nelle settimane successive alla caduta del fascismo. Quando gli alti prelati si adoperarono per evitare che Badoglio cancellasse in toto la normativa sugli ebrei, «la quale secondo i nostri principii e le tradizioni della Chiesa cattolica ha bensì disposizioni che vanno abrogate, ma ne contiene pure altre meritevoli di conferma». Di fatto nel 1943 il Vaticano chiedeva solo la "riabilitazione" degli ebrei convertiti. Che gli altri restassero pure discriminati: le leggi razziali andavano corrette ma non soppresse.  
  
Del resto sette anni prima, il 14 e il 19 agosto 1938, l'Osservatore romano aveva pubblicato due articoli in cui - dopo aver vantato le benemerenze accumulate dai papi in difesa degli ebrei nel corso della storia - rivendicava le proibizioni cui essi venivano assoggettati, motivate non da "ostracismo di razza", bensì dalla «difesa della religione e dell'ordine sociale, che si vedeva minacciato dall'ebraismo». Questo era il modo in cui la Chiesa pensò di reagire alla svolta razzista del regime. Perché stupirsene, visto che negli stessi giorni il governo Mussolini rassicurava per iscritto padre Tacchi Venturi con le seguenti, beffarde parole: "Gli ebrei non saranno sottoposti a trattamenti peggiori di quello usato loro per secoli e secoli dai papi». Erano trascorsi meno di settant'anni dalla definitiva chiusura del ghetto di Roma.  
  
Oggi che il dialogo ebraico-cristiano è di nuovo ostacolato dalla pretesa teologica di conversione del popolo di Gesù, sarebbe bene che, invece di sbandierare una dura opposizione alle leggi razziali che purtroppo non c'è mai stata, gli uomini di Chiesa ricordassero la dottrina antigiudaica vigente nel 1938 (e sconfessata solo nel 1965) : cioè l'accusa di "deicidio" con cui venivano spiegati diciannove secoli di discriminazioni. Tanto è vero che il Vaticano denunciava come perniciose le posizioni di leadership culturale assunte dagli ebrei nelle democrazie occidentali. Come stupirsi se poi la società italiana tollerò l'infamia delle leggi razziali?  
  
Tutto ciò è stato materia dolorosa di riflessione nella Chiesa cattolica, da Giovanni XXIII a Giovanni Paolo II. Ma ora di nuovo scatta l'anatema. Contro Gianfranco Fini, inchiodato alle sue origini fasciste.  
  
E contro Walter Veltroni, colpevole di avergli dato ragione.  
  
Colpisce il richiamo all'ordine rivolto ieri da Avvenire ai dirigenti cattolici del Partito democratico: perché non criticate il vostro segretario, lasciando tale incombenza solo alla pattuglia dei "teodem"?  
  
L'offensiva neodogmatica della Chiesa arcigna non può fare ameno di questi richiami caricaturali all'infallibilità. Il dubbio è bandito, fede e ragione coincidono così come dottrina e natura. Che si tratti di bioetica, di ordinamento familiare, di finanziamento delle scuole cattoliche, odi interpretazioni storiche.  
  
Stranamente tale severità viene meno solo allorquando i politici amici contraddicono i precetti evangelici dell'accoglienza e sparano accuse di "catto-comunismo" sui vescovi che li richiamano. Perché la Chiesa arcigna s'illude di lucrare vantaggi dal conservatorismo laico, e lo supporta a costo di trasmettere disagio in chi vive il  
cristianesimo come testimonianza di vita. In diversi incontri pubblici cui ho partecipato nelle settimane scorse dentro sedi parrocchiali e istituzionali, mi è capitato per la prima volta di sentire applausi rivolti a sacerdoti e fedeli che criticavano apertamente il papa.