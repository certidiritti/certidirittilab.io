---
title: '2008-0140-Discrimination-ST14495.EN10'
date: Tue, 05 Oct 2010 12:20:53 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 5 October 2010

Interinstitutional File:

2008/0140 (CNS)

14495/10

LIMITE

SOC 614

JAI 807

MI 357

  

  

  

  

  

NOTE

from :

General Secretariat

to :

The Working Party on Social Questions

on :

19 October 2010

No. prev. doc. :

13883/10 SOC 561 JAI 759 MI 317

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

In preparation of the next meeting of the Working Party, which is provisionally scheduled for 19 October 2010, delegations will find attached a note from the SI delegation.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Questions for SQWP debate on housing**

**\[Document 13883/10\]**

**SI replies**

**1\.** **Situation in Member States**

Both the Racial Equality Directive (2000/43/EC) and the Gender Goods and Services Equality Directive (2004/113/EC) cover housing available to the public in their scope. Several MS have gone beyond these two grounds in their protection against discrimination.

a)       To what extent does your national legislation against discrimination apply to housing?

According to the Act Implementing the Principle of Equal Treatment (Article 2), e{0><}0{>qual treatment shall be ensured irrespective of sex, nationality, racial or ethnic origin, religious or other belief, disability, age, sexual orientation or other personal circumstance, in particular in relation to:<0}

-                {0><}0{>access to and supply of goods and services, which are available to the public, including housing.<0}

It follows that the Act covers equal treatment in all areas.

b)      Do you distinguish (for this purpose) between private and public/social housing?

The Act Implementing the Principle of Equal Treatment doesn’t distinguish between private and public housing.

  

c)       Do you have statistics on the number of cases of discrimination in the area of housing, and if so, what is the most prevalent ground?

Discrimination in the area of housing is a problem in Slovenia because non-profit rental housing is –due to lack of such housing in bigger cities - limited only to Slovene citizens or EU nationals having residential permit and on condition of reciprocity.

We do not have any statistical data; however we are being criticised before international bodies for having such limitation in our legislation (Housing Act and executive acts).

**2\.** **Social housing**

a)       Do you consider social housing to be a service (for the purposes of EU law)?

No.

b)           If not, do you consider it to be a part of the social protection system?

Yes, non-profit rental housing in Slovenia is a part of the wider social protection system.

**3\.** **Private life**

In some cases, the principle of equality might be in conflict with the right to private life, in particular in the area of housing (e.g. renting out a room in your own apartment).

a)       Is this better taken into account by an exclusion of transactions in the area of private life from the scope of the Directive, or by an exclusion of activities which are not commercial or professional?

We agree with the current wording of Art. 17(d):

_“In particular, this Directive does not apply to transactions related to housing which are performed by natural persons, when the activities in question do not constitute a professional or commercial activity._

_In this context, the concept of professional or commercial activity may be defined in accordance with the national laws and practice of the Member States.”_

  

b)           How is commercial/professional housing defined in your national legislation?

Firstly we would like to explain that the Housing act regulates types of residential buildings, conditions for maintaining residential buildings, conditions for planning dwellings, ownership relations and management of multi-dwelling buildings, housing rental relations, building and sale of new dwellings, help in obtaining and in the use of dwellings, the presence and tasks of the state in the housing sphere, competencies and tasks of municipalities in the housing sphere, competencies of official bodies and organisations operating in the housing sphere, registers and inspection supervision of the implementation of the provisions of this act.

Housing act divides types of rental housing:

_-        housing which is given for non-profit rent and housing which is provided to those entitled to rent non-profit housing (non-profit rental housing);_

_-   housing which is freely placed on the market (market rental housing);_

_-        housing which is devoted to rent in order to satisfy service needs (service rental housing);_

_-        housing devoted to institutional care of elderly persons, retired persons or special groups of the adult population (committed rental housing).”  
  
_

Organization established with the aim of promoting/enhancing housing defined in national legislation (Housing act):

**1)      The Housing Fund of RS**

The Housing Fund of RS (hereafter, the Fund) is a public financial and real estate fund founded for financing and implementing the National Housing Programme, stimulating housing construction, renovation and maintenance of housing and residential buildings.

  

The founder of the Fund is the Republic of Slovenia. Funds for the business of the fund shall be provided: by the national budget; from funds created by the sale of social housing; by committed grants of domestic and foreign legal and natural persons; from funds created by the issue of securities of the Fund; from income created by use of the assets of the Fund or the state in managing the Fund; from income created by its own business.

For providing funds for financing or providing loans for tasks defined in the National Housing Programme, the Fund may take long-term domestic and foreign loans or issue guarantees.

From collected resources, the Fund shall:

-        provide long-term loans with favourable interest rates to natural and legal persons for obtaining non-profit rental housing;

-        provide long-term loans under favourable interest rates to natural persons for obtaining their own housing and residential buildings by purchase, and for the construction or maintenance and reconstruction of housing and residential buildings;

-        invest in the construction of housing and building land;

-        provide help in repaying loans;

-        carry on business in real estate for the purpose of ensuring the public interest;

-        provide financial stimulants for long-term housing saving, especially in the form of premiums for savings deposits of natural persons;

-        stimulate various forms of providing owner occupied and rented housing: with annuity purchase of housing and providing it for lease, by the sale of housing by instalment purchase, by co-investment with public and private investors and similar; and

-        perform other legal tasks and tasks for implementing the National Housing Programme.  

**2)      Non-profit housing organisations**

A non-profit housing organisation is a legal person founded for obtaining, managing and leasing non-profit housing and obtaining and managing its own housing under special conditions. Conditions are prescribed by the minister for housing matters.

**4\.** **Disability - reasonable accommodation**

a)       In your view, what should the obligation to provide reasonable accommodation mean for providers of housing?

b)      Is there legislation on the accessibility of buildings (private as well as public/social) in your country?

The Construction Act (Article 17) reads as follows: “In addition to fulfilling the essential requirements, all works in public use that are newly constructed pursuant to the provisions of this act and works in public use that are reconstructed pursuant to the provisions of this act must ensure that functionally impaired persons are able to access, enter and use the works without physical obstructions or communicational hindrances.”.

**5.       Disability - accessibility**

a)       What should the obligation to provide for accessibility measures encompass in the area of housing?

b)           Should there be a difference between old and new buildings?

The proposed Act on Equal Treatment of the Disabled (ZIMI) ensures that all buildings in public use should be accessible for disabled. Accessibility of existing buildings needs to be ensured at first reconstruction/renovation but at the latest in 15 years after entering the Act in the force.

  

c)       Do you support immediate applicability with respect to new buildings?

Yes.

d)      Should there be a difference between the obligations for private and public/social housing respectively?

Not in the case of buildings in public use.

e)  Are there public funds available in your country for the adaptation of housing (private as well as public/social)?

No. However, that should be the case in the future.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_