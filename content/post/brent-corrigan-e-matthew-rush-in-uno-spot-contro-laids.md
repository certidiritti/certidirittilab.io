---
title: 'Brent Corrigan e Matthew Rush in uno spot contro l''aids'
date: Wed, 16 Mar 2011 00:55:51 +0000
draft: false
tags: [aids, Brent Corrigan, Comunicati stampa, GAY, hiv, Matthew Rush, porno, PRESERVATIVO, sesso, spot, video]
---

Niente alone viola come nel famoso spot del 1989, niente messaggi terroristici o Ambra che ti invita ad usare il preservativo. Nella campagna contro l'Aids del sito **www.DCfuckit.org** i protagonisti sono i due porno attori gay che, in maniera esplicita e divertente, spiegano come usare correttamente il preservativo.

Il progetto nasce dalla comunità gay di Washington D.C., dove il numero dei casi di Aids tra gli uomini afro-americani è 12 volte quello della media nazionale.  
La sua forza sta nell'approccio provocatorio alla prevenzione. Il tradizionale linguaggio clinico lascia il posto all'ironia, alla semplicità e ... ad immagini esplicite.  
  
**[Ecco il video >>>](http://www.youtube.com/watch?v=XN1AbMHwCsM&feature=player_embedded&oref=http%253A%252F%252Fwww.gayprider.com%252Fbrent-corrigan-e-matthew-rush-testimonial-di-una-campgna-contro-laids%252F&has_verified=1)**

**[Qui](http://www.fc-kits.org/homebase.html)** la versione vietata ai minori di 18 anni.

fonte: Advocate.com