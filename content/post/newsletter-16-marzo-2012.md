---
title: 'Newsletter 16 Marzo 2012'
date: Sun, 25 Mar 2012 09:48:34 +0000
draft: false
tags: [Politica]
---

![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)

Una settimana fa **Alfano** ha provato ad imitare Berlusconi (o Giovanardi): _"con la sinistra al governo matrimonio tra uomini"_ (e tra donne no?). [**Oltre a noi**](alfano-esprime-peggiore-cultura-contro-civilta-liberale-e-moderna-si-legga-sentenza-corte-costituzionale-138-2010) e al [**segretario di Radicali Italiani Mario Staderini**](http://www.radicali.it/rassegna-stampa/int-m-staderini-tentativo-contare-di-pi), gli ha subito risposto la capogruppo Pd al Senato**Anna Finocchiaro**: _"dire bugie e offendere gli avversari, e non solo, è uno sport già praticato dalla destra"._ Perché dire che il Pd è per i diritti delle persone lgbt è un'offesa, ma soprattutto una bugia. Ce lo conferma **Rosy Bin di**: _"il matrimonio è solo etero". _

Nessuno ha pensato di far parlare chi in Italia si batte per il diritto al matrimonio tra persone dello stesso sesso, nessuno ha parlato della campagna di Affermazione civile che nel 2010 ha portato alla **storica sentenza 138/10 **della Corte costituzionale che afferma: il Parlamento deve riconoscere i diritti delle coppie gay.** Questa è la peste italiana.**   
[**Intervento di Sergio Rovasio su Il Riformista >**](la-peste-italiana-e-le-balle-del-2012)

Ma non saranno i nostri politici a fermare la storia.   
Questa settimana [**il Parlamento Europeo ha approvato due importanti documenti in difesa delle persone Lgbt**](parlamento-europeo-approva-due-importanti-documenti-in-difesa-delle-persone-lgbt-respinti-emendamenti-contro-matrimonio-same-sex) e ha respinto emendamenti contro matrimonio tra persone dello stesso sesso.   
Ieri poi è arrivata un'altra **storica sentenza della Cassazione** che, pur non riconoscendo il diritto a trascrivere all'anagrafe il matrimonio contratto all'estero da una coppia di Latina,[**ha riconosciuto il diritto alla vita famigliare delle coppie gay**](corte-di-cassazione-riconosce-diritto-a-vita-famigliare-di-coppia-gay-che-aveva-chiesto-trascrizione-matrimonio-fatto-allestero).

La [**campagna di Affermazione Civile**](affermazione-civile) dell'Associazione Radicale Certi Diritti viene così rafforzata. E Certi Diritti rilancia: [**il governo italiano ritiri la circolare Amato che vieta ai Comuni la trascrizione dei matrimoni tra persone dello stesso sesso contratti all'estero**](dopo-cassazione-e-parlamento-europeo-certi-diritti-chiede-il-ritiro-della-circolare-amato-sui-matrimonio-contratti-allestero-esposto-allunar).

L'obiettivo principale dell'Associazione radicale Certi Diritti rimane sempre lo stesso:**l'uguaglianza dei diritti e quindi il matrimonio tra persone dello stesso sesso.**  
Ma continua la nostra iniziativa per il riconoscimento dei diritti delle coppie conviventi perché [**non si può più aspettare!**](http://www.radicali.it/primopiano/20120313/riconoscimento-unioni-civili)

A **Roma continua la raccolta firme **per una proposta di delibera popolare sulle unioni civili ([**qui**](http://www.radicali.it/20120310/da-roma-riparte-richiesta-dal-basso-riconoscimento-delle-famiglie-di-fatto) la dichiarazione del segretario di Radicali Roma e [**qui**](http://teniamofamiglia.blogspot.com/) il sito con tutte le info).  
A **Milano** alla raccolta firme per le unioni civili e le pari opportunità per tutti si aggiunge anche quella per [**una proposta di delibera per regolamentare la prostituzione.**](http://notizie.radicali.it/articolo/2012-03-15/editoriale/regolamentare-la-prostituzione-la-ragionevole-proposta-radicale) In attesa di una conferenza nazionale sulla legalizzazione della prostituzione che si terrà a Roma il prossimo 21 aprile.

Inoltre:

[San Pietroburgo: il governatore firma legge omofoba >](http://feeds.blogo.it/~r/queerblog/it/~3/YnTCIeZcdyQ/story01.htm)

[Gli ortodossi: estenderla a tutta la Russia >](http://www.tmnews.it/web/sezioni/top10/20120312_162316.shtml)

[Bologna intitolerà una via alla storica leader trans Marcella di Folco >](http://www.bolognatoday.it/cronaca/Marcella-Di-Folco-trans-ttitolazione-via.html)

[Il sesso dei disabili e l'abbraccio di una madre >](http://invisibili.corriere.it/2012/03/09/il-sesso-dei-disabili-e-labbraccio-di-una-madre/)

[Regno Unito: consultazione pubblica sul same-sex marriage >](governo-inglese-lancia-consultazione-pubblica-su-nozze-gay-nonostante-parere-contrario-chiesa-classe-politica-italiana-impari)

[Svizzera: arrivano i box per la prostituzione >](http://www.tio.ch/Svizzera/News/672783/Arrivano-i-box-per-la-prostituzione)

[Nuova legge nello Utah: vietato parlare di gay a scuola >](http://feeds.blogo.it/~r/queerblog/it/~3/haim9VNczPY/story01.htm)

[Iraq: Chi non rinnega la sua omosessualità muore >](http://www.google.it/url?sa=t&rct=j&q=lesbiche&source=newssearch&cd=22&ved=0CDIQqQIwATgU&url=http%3A%2F%2Fwww.giornalettismo.com%2Farchives%2F210707%2Fchi-non-rinnega-la-sua-omosessualita-muore%2F&ctbm=nws&ei=CBhdT_6-O4zzsgas3Yz-Cw&usg=AFQjCNHI1_bzVF1PIlWxGu4eKilefmInhQ&cad=rja)

L'associazione radicale Certi Diritti è sempre in prima linea su importanti battaglie storiche per i diritti civili, la laicità dello Stato e la lotta alla sessuofobia. **Ma senza il tuo aiuto non potremo più continuare la nostra iniziativa politica.** Il contributo degli iscritti è per noi di vitale importanza. **[Rinnova l'iscrizione o contribuisci a Certi Diritti](iscriviti)** e invia questa mail ai tuoi amici. Grazie grazie grazie.

  
[www.certidiritti.it](http://www.certidiritti.it/)