---
title: 'Parlamento Europeo approva la Relazione Howitt sui Diritti Umani. Due paragrafi sull''impegno contro discriminazioni verso persone lgbte'
date: Tue, 24 Apr 2012 08:42:08 +0000
draft: false
tags: [Europa]
---

Certi Diritti ringrazia i deputati europei per questo ulteriore gesto di attenzione sulla lotta alle discriminazioni.

Roma-Bruxelles, 24 aprile 2012

Il Parlamento Europeo ha approvato lo scorso 18 aprile la relazione Howitt sui diritti umani nel mondo, che contiene due paragrafi (113 e 114) dedicati alla protezione dei diritti LGBTE.

Il paragrafo 113 della Relazione Howitt approvata dal Parlamento Europeo "elogia il Consiglio, il Servizio Estero di Azione Europea, l'Alto Rappresentante per la Politica estera europea, la Commissione e gli Stati membri,  per il loro impegno a favore dei diritti umani delle persone lesbiche, gay, bisessuali e transessuali (LGBT) nelle relazioni bilaterali con i paesi terzi, in sedi internazionali e attraverso l'EIDHR; accoglie favorevolmente la reintroduzione da parte dell'Assemblea generale delle Nazioni Unite dell'orientamento sessuale come motivo di protezione daesecuzioni stragiudiziarie, sommarie o arbitrarie e si compiace degli sforzi dell'UE a tale fine; invita la Commissione a raccomandare il ritiro dell'identità di genere dall'elenco dei disturbi mentali e comportamentali nei negoziati sull'undicesima versione della classificazione internazionale delle malattie (ICD-11) e a cercare una riclassificazione non patologizzante; ribadisce che il principio della non discriminazione, che comprende altresì motivi di sesso e orientamento sessuale, non dovrà essere compromesso nel quadro del partenariato ACP-UE; ribadisce la sua richiesta alla Commissione di elaborare una tabella di marcia globale contro l'omofobia, la transfobia e la discriminazione sulla base dell'orientamento sessuale e l'identità di genere che, su queste basi, affronti inoltre il problema delle violazioni dei diritti umani nel mondo fondate sugli stessi motivi; invita gli Stati membri a concedere asilo a chi sfugge alle persecuzioni nei paesi in cui le persone LGBT sono considerate alla stregua di criminali, prendendo in considerazione i timori di persecuzione ben fondati dei richiedenti e affidandosi alla loro auto-identificazione di lesbiche, gay, bisessuali o transessuali".

Al paragrafo 114 il Parlamento Europeo "accoglie favorevolmente lo strumentario (toolkit) adottato dal gruppo di lavoro del Consiglio sui diritti umani nel 2010 al fine di aiutare le istituzioni dell'UE, gli Stati membri, le delegazioni e altri organismi dell'UE a reagire tempestivamente quando siano violati i diritti umani delle persone LGBT; invita la Commissione ad affrontare le cause strutturali di tali violazioni e il Consiglio ad operare verso la definizione di orientamenti vincolanti in questo settore".

L’Associazione Radicale Certi Diritti ringrazia i parlamentari europei che hanno votato la relazione Howitt  che conferma l’impegno di questa istituzione  per la difesa dei diritti delle persone LGBTE dalle violenze, persecuzioni, discriminazioni subite nel mondo.

**[Il testo del rapporto Howitt come approvato dal PE >](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P7-TA-2012-0126+0+DOC+XML+V0//IT&;language=IT)**