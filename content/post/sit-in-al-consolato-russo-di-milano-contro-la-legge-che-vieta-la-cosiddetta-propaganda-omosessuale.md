---
title: '1 febbraio sit-in al consolato russo di Milano contro la legge che vieta la cosiddetta ''propaganda omosessuale'''
date: Thu, 31 Jan 2013 09:25:14 +0000
draft: false
tags: [Russia]
---

Comunicato stampa dell’Associazione Radicale Certi Diritti

Milano, 31 gennaio 2013

Il 25 gennaio 2013, la Duma ha approvato in prima lettura, con 388 voti favorevoli su un totale di 450 deputati, la legge contro la cosiddetta 'propaganda omosessuale'. Se adottata anche in seconda e terza lettura, nonché firmata dal Presidente, verrà estesa a tutta la Federazione russa la sciagurata legge, già in vigore a San Pietroburgo e in altre otto regioni, che non colpisce soltanto le persone Lgbti, ma tutti coloro che usano la parola omosessuale in un loro scritto, in un loro spettacolo o in una loro canzone, a prescindere dal loro orientamento sessuale.

Dopo aver scritto al Ministro degli Esteri Terzi e al Presidente della Repubblica Napolitano,  per chiedere di prendere tutte le misure possibili per contrastare questa legge l’Associazione Radicale Certi Diritti, contro questa legge liberticida, **domani 1 febbraio indice un sit-in in piazzale Segesta (angolo V.le Stratico), a pochi passi dal Consolato russo di Milano a partire dalle ore 18.00 ([https://www.facebook.com/events/503426319707643/](https://www.facebook.com/events/503426319707643/)). I partecipanti indosseranno un bavaglio per simboleggiare il tentativo di limitare la libertà d’espressione della proposta di legge.**

Al sit-in hanno aderito CIG Arcigay Milano, Associazione Enzo Tortora - Radicali Milano, Associazione Radicali Senza Fissa Dimora, GayLib, RGR - Rete Genitori Rainbow, BESt - Bocconi Equal Students, Le Rose di Gertrude

Nei prossimi giorni sit-in simili si terranno anche in altre città italiane, a partire da Napoli.

Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti, dichiara: “gli effetti di questa sciagurata proposta di legge si fanno già sentire: due giorni fa un’insegnate è stata licenziata per aver criticato la proposta di legge, mentre ieri sul muro del museo appartamento di Vladimir Nabokov, autore del celebre romanzo Lolita, è apparsa la scritta ≪pedofilo≫. Il museo, che si trova a San Pietroburgo, era già stato attaccato il 10 gennaio. Di fronte a questi tentativi del governo russo di silenziare i suoi stessi cittadini violando anche le convenzioni internazionali sottoscritte, non possiamo non far sentire la nostra voce forte e chiara, nella speranza che raggiunga i nostri amici russi. Roma prenda esempio da Berlino e convochi immediatamente l’ambasciatore russo”.