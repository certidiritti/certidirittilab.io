---
title: 'Bene Vanity Fair su petizione per unioni civili. Il Ministro Fornero tenga conto delle iniziative in corso a Roma e Milano'
date: Tue, 20 Mar 2012 14:07:25 +0000
draft: false
tags: [Politica]
---

La petizione sulle unioni civili è un atto di civiltà e ci auguriamo che venga promosso da altri media. Il ministro e il governo tengano conto anche delle iniziative in corso in tutta Italia e di quanto affermato dal PE  e dalla Cassazione.

Roma, 20 marzo 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

La notizia che Vanity Fair, uno dei settimanali più importanti d’Italia, si è fatto promotore di una petizione in favore  delle unioni civili è molto importante e ci auguriamo che altri media seguano questo esempio.

Speriamo che il Ministro del Welfare e per le Pari Opportunità, Elsa Fornero, tenga conto, e agisca di conseguenza, su quanto richiesto dalla petizione e dalle diverse iniziative che ormai in tutto il paese i cittadini sostengono a stragrande maggioranza e che continuano ad essere ignorate da quasi tutta la classe politica.

Il Parlamento Europeo, il Consiglio d’Europa, la Corte Costituzionale, la Corte di Cassazione, insieme al grande successo della raccolta firme in corso a Roma e Milano per il riconoscimento delle Unioni civili, danno indicazioni ben precise che non si possono più ignorare per l’affermazione dei diritti e il superamento delle diseguaglianze.

Ringraziamo gli artisti e le personalità che hanno finora firmato la petizione di Vanity Fair e chiederemo a tutti i nostri sostenitori, iscritti, e ai parlamentari Radicali di aderire a questa importante iniziativa.

**Possono aggiungere tutti la loro firma su [www.vanityfair.it](http://www.vanityfair.it)**

  
**[vivi a Roma? vieni a firmare >](http://teniamofamiglia.blogspot.com/)**

**[vivi a Milano? vieni a firmare >](http://metticilafirma.it/)**