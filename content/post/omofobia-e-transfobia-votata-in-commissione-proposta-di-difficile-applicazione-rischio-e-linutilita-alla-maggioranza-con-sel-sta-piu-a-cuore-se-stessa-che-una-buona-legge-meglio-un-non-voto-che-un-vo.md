---
title: 'Omofobia e Transfobia: votata in Commissione proposta di difficile applicazione, rischio è l''inutilità: alla maggioranza (con Sel?) sta più a cuore se stessa che una buona legge. Meglio un non voto che un voto inutile.'
date: Tue, 23 Jul 2013 12:08:17 +0000
draft: false
tags: [Politica]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti.

Roma, 23 luglio 2013

La Commissione Giustizia ha votato una nuova proposta di legge contro omofobia e transfobia, approvando un emendamento congiunto di PD e PDL. L'emendamento azzera tutte le proposte di modifica presentate e complica la situazione perchè rende la legge stessa quasi inutile. C'è l'estensione della legge Mancino, ma riferita a reati ed atti discriminatori "fondati sull'omofobia e transfobia". Questi due termini, famosi giornalisticamente, non hanno alcuna base giuridica precisa, e costringeranno i giudici a forzate interpretazioni, esponendo la legge stessa o alla non applicazione o ad una valanga di ricorsi. Omo e transfobia, infatti, letteralmente significano fobia, paura dell'omosessualità e della transessualità. Mentre è evidente a chiunque si occupi di queste vicende che è l'odio ad armare la mano dei violenti e dei discriminatori.

Assurdi appaiono i tentativi di deputati del PD e del PDL di salvare la cosiddetta libertà di espressione. Se la Mancino violasse il diritto di espressione per le persone LGBTI, allora lo farebbe anche per migranti, credenti e minoranze linguistiche. Per salvare la maggioranza si è arrivati a un compromesso pericoloso, perché se in Aula non passassero gli emendamenti annunciati avremmo un testo di dubbia applicazione ed efficacia, con problema di doverlo modificare al senato.

L'Associazione Radicale Certi Diritti ha sostenuto da sempre che se riforma "europea" si voleva attuare bisognava affrontare seriamente il tema dei crimini d'odio basati su orientamento sessuale, identità di genere ed espressione di genere, anche e soprattutto sul piano della prevenzione. Così come in Europa si è realizzato, e non aggiungere una pezza alla Mancino, tanto per poter sbandierare un risultato.

Se la Camera dei Deputati non voterà emendamenti consistenti che ripristino almeno la piena applicazione della Mancino ai reati ed ai casi di discriminazione basati su orientamento sessuale, identità di genere ed espressione di genere, il suo sforzo sarà vano, ed il risultato ottenuto del tutto inutile.

Meglio non votarla una legge che non serve a niente.