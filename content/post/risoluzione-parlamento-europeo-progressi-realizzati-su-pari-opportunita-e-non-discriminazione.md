---
title: 'RISOLUZIONE PARLAMENTO EUROPEO - PROGRESSI REALIZZATI SU PARI OPPORTUNITA'' E NON DISCRIMINAZIONE'
date: Thu, 19 Jun 2008 18:08:30 +0000
draft: false
tags: [Comunicati stampa]
---

![](http://www.centromusicajam.it/images/stories/immagini_sito/bandiera%20europa.gif)[Il PE esprime preoccupazione per ](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P6-TA-2008-0212+0+DOC+XML+V0//IT) le carenze nel recepimento e nell'attuazione, da parte di molti Stati membri, delle direttive 2000/43/CE e 2000/78/CE e per la mancanza di informazione dei cittadini europei... ... sui possibili mezzi giuridici in caso di discriminazione. Sollecita inoltre azioni della Comunità per porre termine alla gerarchia tra fattori di discriminazione nel livello di protezione contro la discriminazione, nonché per estendere la normativa attuale alla discriminazione nei beni e servizi ed in altre aree tuttora non coperte. La Commissione dovrebbe pubblicare la propria proposta al riguardo il 2 luglio 2008 per estendere il campo d’applicazione delle direttive ex art. 13 TCE a situazioni diverse da impiego e occupazione. La Risoluzione chiede anche interventi più efficaci nell’assistenza alle vittime e nel campo delle sanzioni.  
  
[Testo ufficiale](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P6-TA-2008-0212+0+DOC+XML+V0//IT)