---
title: 'Onu pubblica il primo rapporto sui Diritti Umani delle persone Lgbt. I Governi hanno sottovalutato le persecuzioni'
date: Sat, 17 Dec 2011 11:07:31 +0000
draft: false
tags: [Transnazionale]
---

L’Onu pubblica il primo rapporto sui diritti umani delle persone lesbiche, gay, bisessuali e transessuali. Passo storico che documenta la situazione delle persecuzioni e dell'odio tenuti nascosti e/o sottovalutati dai governi.  
   
Roma, 17 dicembre 2011  
   
Comunicato Stampa dell’Associazione Radicale Certi Diritti  
   
L’Associazione Radicale Certi Diritti pubblica qui di seguito la notizia della diffusione del primo rapporto dell'Onu sui Diritti Umani delle persone Lgbt nel mondo; chiede che i Governi tengano conto di quanto denunciato e di rispettare le raccomandazioni indicate con lo scopo di combattere le violazioni dei diritti umani tenuti nascosti e/o sottovalutati. Gli Stati, così come chiedono le Ong Non c’è Pace Senza Giustizia e il Partito Radicale Nonviolento, transnazionale e transpartito, dovrebbero abrogare le leggi che criminalizzano l'orientamento sessuale, porre in essere normative per contrastare le discriminazioni ed assicurare l'accertamento delle responsabilità per ogni grave violazione dei diritti delle persone LGBTI.  
   
Il primo rapporto in assoluto delle Nazioni Unite sui diritti umani delle persone gay, lesbiche bisessuali e transessuali (LGBT)  descrive come, nel mondo, queste persone vengono uccise o soffrono di violenza motivata dall’odio, torture, detenzione, criminalizzazione e discriminazione nel lavoro, nella sanità e nell’educazione a causa del loro orientamento sessuale reale o percepito o della loro identità di genere.  
   
Il rapporto, diffuso il 15 dicembre 2011 dall’ufficio ONU per l’Alto Commissario dei diritti umani (OHCHR) a Ginevra, definisce “un modello di violazioni dei diritti umani \[…\] che richiede un intervento” e dice che i governi fanno spesso finta di non vedere violenze e discriminazioni basate sull’orientamento sessuale e sull’identità di genere.  
   
Secondo il rappporto la violenza omofobica e transfobica è stata registrata in ogni regione del mondo e spazia dall’assassinio, al rapimento, alla violenza carnale, alle minacce psicologiche, all’arbitraria deprivazione della libertà.  
   
Le persone LGBT sono spesso oggetto di abusi organizzati da estremisti religiosi, gruppi paramilitari, neo-nazisti, nazionalisti e così via. Lesbiche e transgender sono particolarmente a rischio di violenza famigliare o all’interno della loro stessa comunità.  
   
“La violenza contro le persone LGBT tende ad essere particolarmente feroce in confronto ad altri crimini basati sul pregiudizio”, il rapporto nota citando dati che indicano come i crimini omofobi sono caratterizzati “da un alto grado di crudeltà e brutalità”.  
   
Violenze e discriminazioni spesso non vengono denunciati poiché le vittime non si fidano della polizia, temono vendette o preferiscono non rivelare il loro essere LGBT.  
   
Il rapporto - preparato in risposta alla richiesta del Consiglio per i diritti umani del 17 giugno scorso - si rifà a informazioni incluse in precedenti rapporti ONU, statistiche ufficiali sui crimini d’odio e dati forniti da organizzazioni regionali e non governative.  
   
Nel rapporto, Navi Pillay, l’Alto Commissario ONU per i Diritti Umani, esorta i governi a revocare le leggi che criminalizzano l’omosessualità, abolire la pena di morte per reati riguardanti relazioni sessuali consensuali, armonizzare l’età del consenso tra rapporti omo e eterosessuali e approvare leggi contro le discriminazioni.  
   
In 76 paesi rimane illegale avere rapporti omosessuali e in almeno 5 paesi - Iran, Mauritania, Arabia Saudita, Sudan e Yemen - si rischia ancora la pena capitale.  
   
Navi Pillay si raccomanda che gli Stati Membri investighino prontamente tutti gli omicidi e le violenze perpetrate a causa del orientamento sessuale reale o percepito o dell’identità di genere e che gli Stati Membri istituiscano sistemi di registrazione per tali incidenti.  
   
L’Alto Commissario esorta tutti i Paesi a fare in modo che nessuno che scappa da una persecuzione basata sull’orientamento sessuale o l’identità di genere si rimpatriato in un territorio dove la loro vita o libertà sia minacciata e che le leggi regolanti l’asilo riconoscano che l’orientamento sessuale o l’identità di genere siano un motivo valido per chiedere lo status di rifugiato.  
   
Campagne d’informazione pubblica dovrebbero essere introdotte, specialmente nelle scuole, per contrastare l’omofobia, mentre la polizia dovrebbe formata per assicurare alle persone LGBT un trattamento appropriato.  
   
Charles Radcliffe, il capo della sezione dedicata alle questioni globali dell’OHCHR, ha detto alla radio ONU che “una delle cose scoperte è che se la legge riflette sentimenti omofobi, allora legittima a sua volta l’omofobia diffusa nella società. Se lo Stato tratta delle persone come se fossero di seconda classe o, peggio, dei criminali, allora invita la gente a fare altrettanto”.  
   
Egli ha sottolineato che tutti gli Stati membri dell’ONU sono obbligati dalla legge internazionale sui diritti umani a decriminalizzare l’omosessualità, aggiungendo che sarebbe importante convincere, piuttosto che ammonire, gli Stati a cambiare le loro leggi.  
   
“Credo che abbiamo visto l’opinione degli Stati cambiare significativamente negli anni recenti. Circa 30 paesi hanno decriminalizzato l’omosessualità negli ultimi due decenni circa”.  
   
Charles Radcliffe, ha detto che pur garantendo la libertà religiosa di tutti, “nessun credo religioso o valore culturale prevalente può giustificare la soppressione dei diritti fondamentali di alcune persone”.  
   
Il rapporto, che sarà discusso dai membri del Consiglio il prossimo marzo, è stato rilasciato dato che i vertici dell’ONU hanno espresso sempre maggiori preoccupazioni circa la violazione dei diritti umani delle persone LGBT.  
   
L’anno scorso, nel suo discorso per la giornata mondiale dei Diritti Umani, il segretario generale Ban Ki-moon disse che “come uomini e donne di coscienza, rigettiamo le discriminazioni in generale e in particolare quelle basate sull’orientamento sessuale o l’identità di genere”.  
   
Anche Navi Pillay, in una pubblica conversazione la settimana scorsa, ha invocato la fine del bullismo e di altre forme di persecuzione ai danni delle persone LGBT.  
   
Traduzione dell’articolo UN issues first report on human rights of gay and lesbian people in  “UN News Center”, 15 dicembre 2011, [](http://www.un.org/apps/news/story.asp?NewsID=40743&Cr=discrimination&Cr1)[http://www.un.org/apps/news/story.asp?NewsID=40743&](http://www.un.org/apps/news/story.asp?NewsID=40743&);Cr=discrimination&Cr1=, a cura di Yuri Guaiana  
   
   
L’Associazione Radicale Certi Diritti si congratula con tutte le organizzazioni della società civile e gli attivisti i cui sforzi a difesa dei diritti LGBTI hanno consentito all'OHCHR di occuparsi finalmente di questa crescente e manifesta lacuna nel rispetto universale dei diritti umani fondamentali.