---
title: 'Statuto'
date: Thu, 27 Mar 2008 19:17:02 +0000
draft: false
tags: [Senza categoria]
---

**ASSOCIAZIONE RADICALE CERTI DIRITTI**

** Statuto con le modifiche approvate dal VIi Congresso**  
**Milano, 10-12 Gennaio 2014  
**

Art. 1 - DENOMINAZIONE

1.1 - È costituita, nel rispetto del codice civile e della L. 383/2000, l'associazione di promozione sociale denominata "Associazione Radicale Certi Diritti".

1.2 - L'Associazione ha come sottotitolo quanto segue:

"Centro di iniziativa politica nonviolenta, giuridica e di studio per la promozione e la tutela dei diritti civili, per la responsabilità e la libertà sessuale delle persone."

1.3 - L'Associazione si ispira alle lotte di quanti hanno combattuto e vinto il razzismo, la segregazione e la discriminazione per garantire l'universalità dei diritti civili. L'Associazione dedica le sue attività alla memoria di Makwan Moloudzadeh e a chiunque abbia sofferto abusi, discriminazioni e violenze a causa della propria sessualità.

1.4 - L'Associazione Radicale Certi Diritti è soggetto costituente il Partito Radicale Nonviolento Transnazionale e Transpartito. Le modalità di cooperazione con il PRNTT sono stabilite di comune accordo.

Art. 2 - SEDE

2.1 - L'Associazione ha sede legale in Roma, Via di Torre Argentina 76, non ha scopo di lucro, la sua durata è illimitata. Gli eventuali utili non possono essere ripartiti, neanche indirettamente.

2.2 - Il trasferimento della sede sociale non comporta modifica statutaria.

Art. 3 - SCOPI ED ATTIVITÀ DELL'ASSOCIAZIONE

3.1 - L'Associazione svolge le sue attività nel settore della tutela dei diritti civili, avendo come scopi:

a - la prevenzione e il contrasto di ogni forma di violenza, maltrattamento e discriminazione, diretta o indiretta, connesse all'identità di genere e all'orientamento sessuale della persona;

b - la promozione della piena libertà e responsabilità sessuale della persona;

c – la promozione, anche in forma associata con altre istituzioni e associazioni, della tutela e dell'assistenza alle persone vittime di violenza, maltrattamento e discriminazione, diretta o indiretta, compresa l'assistenza legale e la presentazione in giudizio;

d- la promozione di specifiche iniziative contro le discriminazioni multiple;

Al fine di raggiungere lo scopo su citato l'Associazione può:

3.2 - promuovere iniziative politiche nonviolente, culturali e sociali per difendere e sostenere le persone e le coppie nella tutela dei diritti connessi alla loro identità di genere, al loro orientamento o preferenza sessuale;

3.3 - realizzare studi e ricerche, formazione e aggiornamento, su temi connessi allo scopo, con particolare attenzione ai problemi legati all'esclusione dal pieno godimento dei diritti previsti dalla Carta costituzionale e dalla normativa internazionale, europea, nazionale e regionale;

3.4 - promuovere, anche insieme ad altre organizzazioni di livello locale, nazionale, europeo e internazionale, iniziative comuni volte al raggiungimento degli scopi dell'Associazione.

3.5 - promuovere iniziative di comunicazione relative agli scopi e alle attività dell'Associazione.

3.6 - Tutte le attività non conformi agli scopi sociali sono vietate.

3.7 - Le attività dell'Associazione e le sue finalità sono ispirate a principi di pari opportunità tra uomini e donne e rispettose dei diritti inviolabili della persona.

Art. 4 – GLI ISCRITTI

4.1 - Chiunque può aderire all'Associazione.

4.2 - Gli iscritti devono versare la quota associativa stabilita dal Segretario.

4.3 - Non è ammessa la figura dell'iscritto temporaneo.

4.4 - La quota associativa è intrasmissibile.

4.6 - Le attività svolte dagli iscritti a favore dell'Associazione e per il raggiungimento dei fini sociali sono svolte prevalentemente a titolo di volontariato e gratuitamente. L'Associazione può, in caso di particolare necessità, assumere lavoratori dipendenti o avvalersi di prestazioni di lavoro autonomo, anche ricorrendo ai propri associati.

Art. 5 - DIRITTI DEGLI ISCRITTI

5.1 - Gli iscritti hanno diritto di eleggere gli organi sociali e di essere eletti negli stessi, se hanno superato la maggiore età e sono in regola con il pagamento della quota annuale.

5.2 - Tutti gli iscritti hanno i diritti di informazione e di controllo stabiliti dalle leggi e dal presente Statuto.

5.3 - Ciascun iscritto, a prescindere dalle diverse categorie di cui al punto 4.2 del presente Statuto, ha diritto a un solo voto.

5.4 - Tutti gli iscritti hanno diritto di accesso a documenti, delibere, bilanci, rendiconti e registri dell'Associazione.

Art. 7 - GLI ORGANI SOCIALI

7.1 - Gli organi dell'Associazione sono:

\- Il Congresso  
\- Il Presidente  
\- Il Segretario  
\- Il Tesoriere  
\- Il Revisore dei conti

7.2 - Tutte le cariche sociali sono assunte e assolte a totale titolo gratuito. Sono ammessi rimborsi spese nella misura e con le modalità stabilite dal Comitato direttivo.

Il Segretario ha la facoltà di proporre un Comitato scientifico e/o una Consulta giuridica e un Presidente onorario dell'Associazione al Comitato direttivo, che si esprime sia sui nomi proposti che sui compiti e la durata del loro mandato.

7.3 - Il Presidente, il Segretario e il Tesoriere sono nominati per un anno e sono rieleggibili.

7.4 - Le cariche sociali non sono cumulabili

Art. 8 - IL CONGRESSO

8.1 - Il Congresso è il massimo organo deliberativo dell'Associazione, di cui stabilisce annualmente gli obiettivi d'azione. È costituita da tutti i soci in regola con il versamento della quota annuale.

8.2 - Il Congresso si riunisce in sessione ordinaria una volta all'anno, e si tiene in forma pubblica. È convocato dal Segretario mediante l'invio, anche solo via e-mail, dell'ordine del giorno ai soci, almeno un mese prima del suo svolgimento.

8.3 - Qualora il Segretario non provveda alla convocazione del Congresso in sessione ordinaria entro il 31 dicembre di ciascun anno, il Congresso può essere convocato su iniziativa di almeno il 50 % degli Iscritti, a spese dell'Associazione, secondo le modalità di cui al comma 2.

8.4 - Il Congresso in sessione ordinaria discute, approva ed elegge con la maggioranza dei voti dei soci presenti:

\- l'ordine dei lavori  
\- il regolamento del Congresso  
\- il bilancio annuale (su proposta del Tesoriere)  
\- la mozione annuale  
\- le modifiche allo Statuto  
\- le cariche sociali

8.5 - Il Congresso si riunisce in sessione straordinaria su iniziativa del Segretario in accordo con il Tesoriere, ovvero su richiesta motivata di almeno un terzo dei soci, mediante l'invio dell'ordine del giorno agli Iscritti almeno un mese prima dell'adunanza. Tale ordine del giorno non può essere modificato.

8.6 - Il Congresso può riunirsi in sessione straordinaria per far decadere il Segretario e/o il Tesoriere, con mozione di sfiducia motivata, recante l'indicazione del nome del/i candidato/i a sostituirlo/i. La mozione di sfiducia può essere proposta da almeno il 50% + 1 dei soci. Se approvata, la mozione comporta l'immediata sostituzione del Segretario e/o del Tesoriere con il/i candidato/i indicato/i nella mozione stessa.

8.7 - Il Congresso si riunisce in sessione straordinaria, con le modalità previste al comma 5, in caso di dimissioni del Segretario e/o del Tesoriere.

8.8 - Le deliberazioni del Congresso sono valide se adottate a maggioranza dei voti validamente espressi. Non è ammesso il voto per delega.

8.9 - Il Congresso delibera i modi e le forme dello scioglimento dell'Associazione.

Art. 9 – CENTRI DI INIZIATIVA LOCALE

9.1 - In ciascuna Provincia si può costituire una articolazione territoriale dell'Associazone denominata "Centro di iniziativa locale dell'Associazione radicale Certi Diritti".

9.2 - Ogni Centro può essere promosso da un nucleo di almeno 10 iscritti all'Associazione nelle stesse province, ed è costituita secondo le regole che il Segretario, sentito il Comitato Direttivo, adotterà neel rispetto del presente statuto.

9.3 - La costituzione del Centro è autorizzata dal Segretario, d'intesa con il Tesoriere e il Presidente, e consente l'utilizzazione del logo dell'Associazione, che è e rimane di proprietà dell'Associazione stessa.

9.4 – I Centri hanno la finalità di organizzare e sostenere l'attività e le iniziative dell'Associazione in conformità alle decisioni adottate dai suoi organi deliberativi e alle indicazioni fornite dai suoi organi esecutivi, nonché di promuovere e raccogliere le iscrizioni all'Associazione: a tale scopo, esse possono assumere iniziative autonomamente determinate e finanziate nel rispetto del presente statuto.

9.5 – Il segretario, d'intesa col tesoriere, può revocare la concessione del diritto di utilizzazione del logo e della dizione "Centro di iniziativa locale dell'Associazione Certi Diritti" al singolo gruppo locale nel caso in cui le attività del Centro stessa non siano conformi a quanto previsto dallo Statuto, dal Regolamento di cui all'articolo 9.6 e della mozione congressuale.

9.6 - I rapporti tra l'Associazione e i Centri, nonché le competenze ed i compiti di queste ultime, potranno essere ulteriormente dettagliati mediante un apposito regolamento, da approvare in sede di Comitato Direttivo dell'Associazione.

Norma transitoria: Laddove a livello provinciale non vi sia un numero sufficiente di iscritti ma vi siano le condizioni per costituire un Centro di iniziativa locale anche considerando gli iscritti di altre Province nella stessa regione, il Segretario può valutare se autorizzare o meno la costituzione del Centro.

Art. 10 - IL PRESIDENTE

10.1 È eletto dal Congresso. Il Presidente vigila sull'osservanza dello Statuto e presiede i Congressi dell'Associazione.

10.2 Il Presidente custodisce la copia originale di tutti i documenti approvati dall'Assemblea degli Iscritti e li rende accessibili qualora gli venga richiesto da uno o più Iscritti.

10.3 Il Presidente esercita, di concerto con il Segretario, la rappresentanza politica dell'Associazione.

Art. 11 - Il SEGRETARIO

11.1. Il Segretario è il rappresentante legale dell'Associazione;

11.2. Il Segretario è eletto dal Congresso; promuove e coordina l'attività politica e garantisce l'attuazione dei deliberati del Congresso. Ha la responsabilità legale e politica dell'Associazione e presenta al Congresso una relazione sull'attività svolta.

11.3 - Il Segretario assicura la circolazione delle informazioni tra gli Iscritti, incentivando l'utilizzo dello strumento telematico. A tal fine, ed anche per una migliore circolazione delle informazioni sulle attività dell'Associazione verso l'esterno della stessa, può nominare uno o più Responsabili per l'informazione relativamente agli strumenti telematici per lo scambio delle informazioni tra gli iscritti e per l'ufficio stampa.

11.4 - Il Segretario può attribuire incarichi per specifiche iniziative politiche, di studio, di coordinamento, costituendo uno o più Comitati e nominando, per ognuno di essi, un Coordinatore che ne è responsabile. Coloro che svolgono tali attività sono membri di diritto del Direttivo, senza diritto di voto. Può inoltre costituire una Giunta di segreteria, informandone il Comitato direttivo.

11.5 - Il Segretario ha facoltà di:  
\- creare un Ufficio di Segreteria, con il compito di supportare l'organizzazione e il coordinamento delle attività associative;  
\- creare un Consiglio direttivo dell'Associazione con il compito di coadiuvare gli organi statutari nell'attuazione delle mozioni congressuali e delle iniziative assunte dall'Associazione. Il consiglio direttivo è costituito dal Segretario, sentito il Presidente e il Tesoriere e nomina al suo interno un coordinatore che, di concerto con il Segretario e il Tesoriere organizza i lavori del Direttivo stesso. Del Direttivo fanno parte di diritto i referenti dei Centri di iniziativa locale. Le riunioni del Consiglio direttivo possono essere svolte anche per via telematica. I pareri del Consiglio direttivo sono di natura consultiva;  
\- nominare un presidente onorario dell'Associazione, scelto d'intesa con il Tesoriere e il Presidente;

Art. 12 – IL TESORIERE

12.1 Il Tesoriere promuove e coordina le attività organizzative e le attività di reperimento dei fondi necessari per lo svolgimento delle attività dall'Associazione.

12.2. Cura l'ordinaria amministrazione delle attività e del patrimonio dell'Associazione.

12.3 Tiene un registro degli Iscritti ed informa di ogni suo aggiornamento il Segretario e il Presidente dell'Associazione.

12.4. È responsabile della raccolta e della custodia delle quote di adesione e degli altri contributi, nonché del pagamento delle spese; ha la responsabilità diretta dei Conti correnti bancari e postali intestati all'Associazione.

12.5. Predispone e redige il bilancio e lo presenta al Congresso degli Iscritti per l'approvazione.

12.6 Il Tesoriere può nominare una Giunta di Tesoreria che lo coadiuvi nell'adempimento del mandato. Della nomina informa il Consiglio direttivo.

Art. 13 – IL REVISORE DEI CONTI

13.1 - Il revisore dei conti è eletto dall'Assemblea degli Iscritti.

13.2 - Esercita i poteri e le funzioni previsti dagli articoli 2403 e seguenti del codice civile.

13.3 - Agisce di propria iniziativa, su richiesta di uno degli organi oppure su segnalazione anche di un solo aderente fatta per iscritto e firmata.

13.4. - Il revisore riferisce annualmente all'Assemblea con relazione scritta, firmata e distribuita a tutti i soci.

Art. 14 - I MEZZI FINANZIARI

14.1. I mezzi finanziari per il funzionamento dell'Associazione provengono:

\- dalle quote versate dai soci nella misura decisa dal segretario, di concerto con il tesoriere e il Presidente, e ratificata dal Comitato direttivo;

\- da contributi, donazioni, lasciti in denaro o in natura provenienti da persone e/o istituzioni pubbliche e private;

\- da iniziative promozionali.

14.2. I fondi dell'Associazione non possono essere investiti in forme che prevedano la corresponsione di un interesse.

14.3. Ogni mezzo che non sia in contrasto con lo Statuto e con le leggi dello Stato Italiano può essere utilizzato per appoggiare e sostenere i finanziamenti all'Associazione e arricchire il suo patrimonio.

14.4. Il Comitato direttivo potrà rifiutare qualsiasi donazione che sia tesa a condizionare in qualsivoglia modo l'Associazione.

Art. 15 - BILANCIO

15.1 - I bilanci sono predisposti dal Tesoriere e approvati dal Congresso dell'Associazione.

15.2 - I bilanci sono depositati presso la sede dell'Associazione almeno 20 giorni prima del Congresso e possono essere consultati da ogni iscritto.

15.3 - I bilanci approvati dal Congresso sono pubblici.

Art. 16 - MODIFICHE STATUTARIE

16.1- Il presente statuto è modificabile con voto favorevole della maggioranza dei presenti al Congresso. Ogni modifica o aggiunta non può essere in contrasto con gli scopi sociali, con la dottrina, con il regolamento interno e con la Legge italiana.

Art. 17 - SCIOGLIMENTO DELL'ASSOCIAZIONE

17.1 - Per deliberare lo scioglimento dell'Associazione e la devoluzione del patrimonio occorre il voto favorevole della maggioranza dei soci presenti in Congresso straordinario.

17.2 - Il Congresso che delibera lo scioglimento dell'Associazione nomina uno o più liquidatori e delibera sulla destinazione del patrimonio che residua dalla liquidazione stessa.

17.3 - La devoluzione del patrimonio viene effettuata con finalità di pubblica utilità a favore di associazioni di promozione sociale.

Art. 18 - DISPOSIZIONI FINALI

Per tutto ciò che non è espressamente previsto si applicano le disposizioni contenute nel codice civile e nelle leggi vigenti in materia.

* * *

**PROPOSTA DI REGOLAMENTO DEI "GRUPPI DI INIZIATIVA LOCALE" **

Art. 1

1\. L'Associazione Radicale Certi Diritti in base allo statuto vigente può essere rappresentata territorialmente dai "Gruppi di iniziativa locale". Ogni gruppo dovrà sempre essere costituito da almeno 10 iscritti all'Associazione.

Art. 2

1\. La richiesta di costituzione di un "Gruppo di iniziativa locale" con in allegato l'elenco degli aderenti, dovrà essere inoltrata al Segretario e al Tesoriere dell'Associazione.

2\. Il Segretario, d'intesa con il Presidente e il Tesoriere, che svolgerà una verifica sulla regolarità dell'iscrizione all'Associazione di tutti i componenti del gruppo, autorizza la costituzione del Centro di iniziativa locale e consente l'utilizzazione del logo dell'Associazione, che è e rimane di proprietà dell'Associazione stessa.

3\. I Gruppi d'iniziativa locale sono autorizzati all'uso del simbolo dell'Associazione alle condizioni del successivo comma 4 e la responsabilità del suo corretto utilizzo è in capo al responsabile del Gruppo di iniziativa locale.

4\. E' proibita la diffusione dei simboli modificati (logo semplice senza il nome dell'associazione, logo dell'associazione che riporti la città del referente, ecc...).

Art. 3

1\. I "Gruppi di iniziativa locale" sostengono l'Associazione nazionale e sono punti di riferimento decentrati della stessa.

2\. I "Gruppi di iniziativa locale" organizzano iniziative coerenti con la mozione approvata dal Congresso e/o iniziative di autofinanziamento.

3\. I "Gruppi di iniziativa locale" possono raccogliere le quote di iscrizione all'Associazione nazionale , impegnandosi a versarle sul conto della stessa, ad aggiornare costantemente il tesoriere sul numero di tessere sottoscritte e fornendo periodicamente alla tesoreria la matrice delle ricevute, al fine di permetterne l'integrazione nelle scritture contabili del bilancio associativo.

4\. I Gruppi d'iniziativa locale devono promuovere e sostenere le iniziative dell'Associazione nazionale nell' ambito territoriale di competenza..

5\. I "Gruppi di iniziativa locale"devono inviare i comunicati stampa a nome dell'Associazione alla la mailing list dei gruppi locali, prima della loro diffusione .

Art. 4

1\. Il responsabile del gruppo di iniziativa locale viene eletto dall'assemblea degli iscritti all'associazione della provincia considerata a maggioranza semplice.

2\. Il responsabile del gruppo di iniziativa locale coordina le iniziative del gruppo locale, assicura e promuove il collegamento con gli organi associativi.

3\. Il responsabile del gruppo di iniziativa locale può richiedere al Tesoriere l'invio di materiale politico o destinato all'autofinanziamento. Gli articoli destinati all'autofinanziamento vengono loro affidati in conto vendita ed è responsabilità dei referenti locali e dei responsabili dei gruppi locali rendicontare periodicamente la situazione dei contributi raccolti. Il Tesoriere comunicherà la quota del ricavato da destinare alle attività locali e quella da restituire all'Associazione.

4\. Gli articoli destinati all'autofinanziamento vengono affidati ai responsabili dei gruppi locali in conto vendita ed è loro responsabilità rendicontare periodicamente la situazione dei contributi raccolti. Il Tesoriere comunicherà la quota del ricavato da destinare alle attività locali e quella da restituire all'Associazione.

5\. I gruppi locali dovranno, nel rispetto delle normative vigenti, prevedere la stesura e l'approvazione annuale di un bilancio proprio e che dovrà essere inviato al Tesoriere nazionale.

Art. 5

1\. Il responsabile della gestione dei gruppi locali gestisce la mailing list dei gruppi locali informando il direttivo delle iniziative da loro intraprese e sollecita i gruppi locali a sostenere le iniziative dell'Associazione nazionale nell'ambito territoriale di competenza.

2\. L'organizzazione di iniziative e manifestazioni da parte dei "Gruppi di iniziativa locale" deve essere comunicata al responsabile della gestione dei gruppi locali, che a sua volta la comunica agli organi dirigenti dell'Associazione, attraverso la mailing list dei gruppi locali.

Art. 6

1\. Il Segretario d'intesa con il Tesoriere e il Presidente può revocare la concessione del diritto di utilizzo del logo e della dizione "Centro di iniziativa locale Certi Diritti(...)" al singolo gruppo locale, nel caso in cui le sue attività non siano conformi a quanto previsto dallo Statuto, dal presente regolamento e dalla mozione congressuale.