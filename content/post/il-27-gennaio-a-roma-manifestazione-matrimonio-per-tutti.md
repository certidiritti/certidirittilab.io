---
title: 'Il 27 gennaio a Roma manifestazione "matrimonio per tutti"'
date: Wed, 23 Jan 2013 22:07:58 +0000
draft: false
tags: [Matrimonio egualitario]
---

A piazza Farnese dalle 14 alle 17 in contemporanea con la manifestazione francese per l'approvazione della legge 'mariage pour tous'.

Il Circolo di Cultura Omosessuale “Mario Mieli”,  Agedo Roma, Arcigay Roma, Associazione Libellula, Associazione Radicale Certi Diritti, Compagnia teatrale inconTrans-tabile, Controviolenzadonne, Di Gay Project, Famiglie Arcobaleno Roma, Fondazione Massimo Consoli, Gay Center, Gaynet Italia, Luiss Arcobaleno, Nuova Proposta – donne e uomini omosessuali cristiani, Queerlab, Rete Genitori Rainbow e Roma Rainbow Choir, in occasione della Manifestazione parigina a sostegno della legge francese sul matrimonio egualitario - “Mariage pour tous” - che si terrà domenica 27 gennaio a Parigi, organizzano, in contemporanea con quella parigina,  un sit - in a Piazza Farnese dalle ore 14.00 alle ore 17.00.

“Dopo le parole del Presidente Obama, che nel  discorso di inaugurazione del suo secondo mandato ha paragonato  le battaglie per i diritti civili di gay e lesbiche a quelle dei neri degli anni Sessanta, siamo ancora più determinati nel chiedere che il nostro Paese si allinei con  le più importanti democrazie mondiali. E’ paradossale che l’Italia sia il terzo mondo dei diritti civili”, queste le parole degli organizzatori della manifestazione.

“L’agenda del prossimo governo non può ancora colpevolmente ignorare la questione omosessuale. Nonostante qualche avanzamento del centrosinistra, i programmi delle coalizioni alle prossime elezioni denotano una manifesta arretratezza sociale e culturale che ci allontana dall’Europa dei diritti civili”.

“In un Paese democratico, solidale e al passo con i tempi, la politica non può più disattendere le richieste della  società civile, dei gay, delle lesbiche e delle persone trans. Il “Matrimonio per tutti”, in Italia come in Francia, è la battaglia di civiltà per una piena uguaglianza su cui il movimento lgbtq non arretrerà. Per questo domenica saremo in piazza in supporto delle richieste dei manifestanti francesi, consapevoli di essere protagonisti di una comune stagione dei diritti che ci deve vedere tutte e tutti uniti nella costruzione di un’Europa e di un Paese migliori”.