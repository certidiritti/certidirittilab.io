---
title: 'GAY PRIDE DI MOSCA: CORTE EUROPEA DEI DIRITTI UMANI CONDANNA RUSSIA PER AVER VIOLATO CONVENZIONE EUROPEA. NIKOLAY ALEXEYEV, ORGANIZZATORE DEL PRIDE DI MOSCA, SARA'' A ROMA IL 27 E 28 NOVEMBRE 2010, AL CONGRESSO DI CERTI DIRITTI'
date: Thu, 21 Oct 2010 10:53:47 +0000
draft: false
tags: [Senza categoria]
---

dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti

Strasburgo - Roma, 21 novembre 2010

La Corte Europea dei Diritti dell'Uomo ha oggi condannato la Russia per aver violato la Convenzione Europea per la salvaguardia dei Diritti dell'Uomo e delle Libertà fondamentali, proibendo i Mosca Pride degli anni 2006, 2007 e 2008. La Corte Europea nel motivare la sua decisione ha sottolineato che "il divieto di svolgimento del Gay Pride moscovita non può giustificarsi per semplici rischi di altre dimostrazioni. In questo modo ogni dimostrazione sarebbe proibita e la società sarebbe privata dell'ascolto dei diversi punti di vista, in questo modo vengono così violati i principii stessi della Convenzione".

Il ricorso alla Corte Europea dei Diritti dell'Uomo era stato presentato da Nikolai Alexeyev, promotore del Gay Pride di Mosca, che fu violentemente aggredito e poi fermato dalle forze dell'ordine nel 2007 insieme, tra gli altri, al radicale Marco Cappato, allora deputato europeo ed Ottavio Marzocchi, funzionario del Parlamento Europeo, membro dell'Associazione Radicale Certi Diritti, insieme a decine di altri esponenti e militanti dei diritti civili e umani accorsi a Mosca da tutta Europa.

Nikolay Alexeyev parteciperà ai lavori del IV Congresso dell'Associazione Radicale Certi Diritti che si svolgeranno a Roma nei giorni 27 e 28 novembre 2010.