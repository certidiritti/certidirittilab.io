---
title: 'Arrestato a Mosca Yuri Guaiana, consegnava firme contro persecuzioni gay in Cecenia'
date: Thu, 11 May 2017 10:16:51 +0000
draft: false
tags: [Russia]
---

![18341785_10155046949305973_1169901487401636601_n](http://www.certidiritti.org/wp-content/uploads/2017/05/18341785_10155046949305973_1169901487401636601_n-225x300.jpg)"Yuri Guaiana, membro del direttivo dell'Associazione Radicale Certi Diritti è stato arrestato questa mattina a Mosca mentre consegnava per conto dell'Ong AllOut le firme di cittadini da tutto il mondo che chiedono giustizia e verità sulle persecuzioni di gay in cecenia. Siamo in contatto con la Farnesina che sta seguendo la vicenda e gli avvocati sul posto, sappiamo che Yuri sta Bene e conosciamo le coordinate della caserma dove è trattenuto. A breve aggiornamenti"

Così Leonardo Monaco, segretario dell'Associazione Radicale Certi Diritti. Yuri Guaiana ha avuto modo di dichiarare: "Siamo qui per consegnare più di 2 milioni di firme al procuratore generale. Nonè mai avvenuto prima, più della popolazione cecena chiede che si faccia un'inchiesta efficace e che si fermino subito arresti, torture e uccisioni di gay. I cittadini russi meritano di vivere in libertà e in uno stato di diritto. La russia deve rispettare i trattati internazionali che ha sottoscritto. Nessuno deve sacrificare la propria libertà e la propria vita solo a causa di quello che si è e di chi si ama, né in Cecenia né da nessun'altra parte"