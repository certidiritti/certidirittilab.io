---
title: 'Corte di Cassazione riconosce ''diritto a vita famigliare'' di coppia gay che aveva chiesto trascrizione matrimonio fatto all''estero'
date: Fri, 16 Mar 2012 00:21:32 +0000
draft: false
tags: [Diritto di Famiglia]
---

Il ministero degli interni ritiri la circolare che vieta la trascrizione dei matrimoni contratti all'estero da coppie gay.

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Roma, 15 marzo 2012

La decisione della Corte di Cassazione che riconosce il  ‘Diritto alla vita familiare’ di una coppia gay sposata all’estero è un altro passo avanti per il superamento delle diseguaglianze nel nostro paese.  Pur non riconoscendo il diritto a trascrivere all’anagrafe il matrimonio contratto all’estero da una coppia di Latina, la sentenza  precisa quello che la classe politica continua a negare.

Difatti, la storica sentenza della Corte di Cassazione, la n. 4184 di oggi, sottolinea che “i componenti della coppia omosessuale, conviventi in stabile relazione di fatto, anche se secondo la legislazione italiana non possono far valere nè il diritto a contrarre matrimonio, nè il diritto alla trascrizione del matrimonio contratto all'estero, tuttavia -a prescindere dall'intervento del legislatore in materia- quali titolari del diritto alla vita famigliare è  nell'esercizio del diritto inviolabile di vivere liberamente una condizione di coppia e del diritto alla tutela giurisdizionale di specifiche situazioni, segnatamente alla tutela di altri diritti fondamentali, possono adire i giudici comuni per far valere, in presenza di specifiche situazioni, il diritto ad un trattamento omogeneo a quello assicurato dalla legge alla coppia coniugata”.

La sentenza si è ispirata a quanto già detto dalla Corte Costituzionale con la sentenza 138/2010 che sollecitava le coppie gay conviventi a ricorrere alla giustizia qualora la classe politica non intervenisse per il riconoscimento dei diritti.

La Corte di Cassazione chiarisce che “è stata radicalmente superata la concezione secondo cui la diversità di sesso dei nubendi è presupposto indispensabile, per così dire naturalistico, della stessa esistenza del matrimonio”. Il no alla trascrizione delle nozze gay  “non dipende più dalla loro inesistenza e neppure dalla loro invalidità, semplicemente dalla loro inidoneità a produrre, quali atti di matrimonio, qualsiasi effetto giuridico nell'ordinamento italiano”.

La campagna di Affermazione Civile dell’Associazione Radicale Certi Diritti viene così rafforzata e rilanciata.  Alla luce di questa storica sentenza, la prima della Corte di Cassazione su questo tema, l’Associazione Radicale Certi Diritti chiede al Governo italiano di ritirare quanto prima la circolare Amato che vieta ai Comuni la trascrizione dei matrimoni tra persone dello stesso sesso contratti all’estero.

**[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**