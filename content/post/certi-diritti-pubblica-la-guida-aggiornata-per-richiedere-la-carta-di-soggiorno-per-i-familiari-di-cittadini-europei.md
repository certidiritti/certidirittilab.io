---
title: 'Certi Diritti pubblica la guida aggiornata per richiedere la Carta di Soggiorno per i familiari di cittadini europei'
date: Mon, 26 Aug 2013 08:26:16 +0000
draft: false
tags: [Affermazione Civile]
---

Roma, 26 agosto 2013

In seguito all'approvazione della legge di adeguamento alle normative europee pubblicata pochi giorni fa sulla gazzetta ufficiale e che entrerà in vgore il 4 settembre, l'Associazione Radicale Certi Diritti rilascia la guida aggiornata per le coppie che intendono richiedere il permesso di soggiorno per il partner straniero dello stesso sesso.

Ringraziamo Gabriella Friso che ha curato il documento.

[**DOWNLOAD>>>**](notizie/comunicati-stampa/item/download/42_92b34461af6aafd3162e9ae083045970)

  
[istruzioni\_aggiornate\_al_24.8.13.pdf](http://www.certidiritti.org/wp-content/uploads/2013/08/istruzioni_aggiornate_al_24.8.13.pdf)