---
title: '2008-140-Discrimination-ST08711.EN10'
date: Mon, 19 Apr 2010 15:05:07 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 19 April 2010

Interinstitutional File:

2008/0140 (CNS)

8711/10

LIMITE

SOC 268

JAI 312

MI 106

  

  

  

  

  

NOTE

from :

Council General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

8173/10 SOC 240 JAI 270 MI 94

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached a note from the Dutch delegation with a view to the meeting of the Working Party on Social Questions on 22 April 2010.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Note from the Dutch Delegation**

This note has been based on the Presidency drafting suggestions of 15 April 2010 (document 8173/10).

Additions and modifications are in **bold** and deletions are marked **“\[…\]”**

**Article 4b (new) paragraph 1 (d)  Doc. 8173/10 (15/4/2010)**

**Life span of goods**

NL welcomes the  suggested Article 4b (new), paragraph 1(d), except for the reference to “infrastructures”. Most manifestations of infrastructure are considered to be part of public space, which does not / should not fall within the scope of this Directive. A reference to infrastructures may cause legal uncertainty in this respect.

**Art. 3.6 (new) (Doc. 6563/10) and Article 4b (new), paragraph 4 (new) (Doc. 8173/10)**

**Public Space**

Public space as defined in the note from the Dutch delegation 15430/09, was confirmed by the Commission as falling outside the scope of the proposed directive. In order to improve legal certainty, in doc. 6563/10 NL suggested a new paragraph 3.6. as follows: ”This directive does not apply to the public space such as road- and water infrastructures.”

Given the meaning of this notion in the context of accessibility and reasonable accommodation of the supply of goods and services, the Dutch delegation suggests also to take into consideration the following addition to Article 4b (new), paragraph 4 (doc. 8173/10):

**“This directive shall not apply to public space.”**

  

Consideration:

Public space means all built and other physical, non constructed (e.g. green fields) environment, accessible to the public, such as roads, rail and water infrastructures, pavements, parks, green areas, nature, natural scenery, etc. regardless of whether access to such “environments" is paid or unpaid. Physical planning and organisation of public space is deemed to be a competence of Member States.

Another option is to restrict the exclusion of public space to the Articles on accessibility. The NL suggestion will then read:  
  
"**Articles 4 and 4a shall not apply to public space_._”**

**Article 4b (new), paragraph 3 (document 8173/10)**

NL welcomes the suggested Article 4b (new), paragraph 3. NL wants to make it clear however, that in areas where EU regulation on passenger rights and technical specifications for the different means of transport exist, this directive does not apply. The Netherlands therefore suggests referring to the _areas_ where standards or specifications are applicable and that are excepted from this Directive.

To ensure a clear demarcation of the directive in this respect, providing for legal certainty, the article should read as follows:

“Articles 4 and 4a shall not apply **to areas** where European Union Law provides for detailed standards **\[…\]** **or** specifications **\[…\]**.

**Housing**

NL understands that the aim of the directive is to prohibit the refusal of housing to individuals on discriminatory grounds and subscribes to this basic principle. However, it would be disproportionate to require that all existing and new buildings, from which housing is offered, should be made accessible for persons with a disability in advance. When necessary and appropriate this aim could be better reached by adapting a building upon request. Also, different forms of disability require different adjustments. If in principle all dwellings have to be adjusted  beforehand, they would in some cases have to be adjusted again in case a person without a disability or a person with different  adjustment needs moved in. This may cause unnecessary remodelling of for instance apartments. Moreover proportionate adjustments can be easily and effectively be carried out the moment a person with a disability wants to rent a particular house. This leaves without prejudice that in the case of apartment blocks it can be necessary to ensure accessibility by general measures, e.g. concerning the accessibility of common premises or the presence of an elevator. In this respect, the directive should not interfere with the different systems in member states.

NL has taken note of the new suggestions of the Presidency regarding housing and intends to study them in depth.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_