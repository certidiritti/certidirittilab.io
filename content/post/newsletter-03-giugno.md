---
title: 'Newsletter 03 Giugno'
date: Wed, 31 Aug 2011 10:07:24 +0000
draft: false
tags: [Politica]
---

**![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)**

Mentre il voto alla Camera sul provvedimento anti omofobia e transfobia continua a slittare, a Roma sono iniziati gli eventi dell’Europride, che terminerà l’11 giugno con la parata alla quale Certi Diritti parteciperà con un carro insieme all’ass. Luca Coscioni, Radicali Italiani, Nonviolent Radical Party, Nessuno Tocchi Caino, Non c’è Pace Senza Giustizia. [Sul nostro sito trovate tutti gli eventi promossi d](tutte-le-notizie/1153-invito-di-certi-diritti-agli-eventi-di-roma-europride.html)[a ](tutte-le-notizie/1153-invito-di-certi-diritti-agli-eventi-di-roma-europride.html)[Certi Diritti e quelli a cui la nostra associazione è stata invitata a partecipare.](tutte-le-notizie/1153-invito-di-certi-diritti-agli-eventi-di-roma-europride.html)

A giugno ricorrono anche i 30 anni dai primi casi di Aids e chi viene nominato ambasciatore Onu per la lotta al virus? Il sottosegretario Giovanardi!

I risultati dei ballottaggi danno però nuove speranze alla comunità Lgbt.

Buona lettura

SPECIALE BALLOTTAGGI
====================

**Il Centrosinistra stravince, 15 sindaci su 20 capoluoghi regione.   
[Leggi >](http://www.notiziegay.com/?p=76116)**

**A Milano vince Pisapia, a Napoli De Magistris a valanga.   
[Leggi >](http://blog.panorama.it/italia/2011/05/30/a-milano-vince-pisapia-a-napoli-de-magistris-a-valanga/)**

**De Magistris vince a Napoli e la comunità Lgbt esulta.  
[Leggi >](http://www.queerblog.it/post/11283/de-magistris-vince-a-napoli-e-la-comunita-lgbt-esulta)**

**iuliano Pisapia vs Letizia Moratti: dieci momenti da ricordare.   
[Leggi >](http://www.02blog.it/post/8195/giuliano-pisapia-vs-letizia-moratti-dieci-momenti-da-ricordare#continua)**

ASSOCIAZIONE
============

**EUROPRIDE ROMA 2011: tutti gli eventi con Certi Diritti.   
[Leggi >](tutte-le-notizie/1149.html)**

**Italia: no a riconoscimento matrimoni gay fatti all'estero.  
[Leggi >](tutte-le-notizie/1150.html)**

**Gay Pride di Mosca: ancora incidenti e arresti anche contro gay.   
[Leggi >](tutte-le-notizie/1148.html)**

**Rinviato ancora voto su legge contro omofobia e transfobia.   
[Leggi >](tutte-le-notizie/1151-slitta-a-meta-giugno-voto-su-omofobia-e-transfobia-alla-camera.html)**

**Governo affida la lotta all'Aids a Giovanardi.  
[Leggi >](tutte-le-notizie/1146.html)**

RASSEGNA STAMPA
===============

**Peter Boom è morto.  
[Leggi >](http://www.occhioviterbese.it/occhioviterbese/cronaca-viterbese/9718-peter-boom-e-deceduto.html)**

**Europride 2011: Claudia Gerini madrina della manifestazione.   
[Leggi >](http://www.romatoday.it/eventi/europride-2011-claudia-gerini-madrina.html)**

**“L'Europride oltraggia il Papa”.  
[Leggi >](http://espresso.repubblica.it/dettaglio/leuropride-oltraggia-il-papa/2152410/13)**

**Carlo Giovanardi ambasciatore Onu per la lotta all’Aids? In rivolta le associazioni.  
[Leggi >](http://www.notiziegay.com/?p=75904)**

**Matrix, trasmissione del 24 maggio, "Chiesa e pedofilia".  
[Guarda >](http://www.latvsulweb.com/programmi-tv/matrix/puntata-matrix-del-24052011/?type=med&code=video/matrix/full/228136/chiesa-e-pedofilia.html)**

**L’opinione diversa. Apartheid “protettivo” per gli omosessuali?   
[Leggi >](http://www.notiziegay.com/?p=75927)**

**“Estendere la Legge Mancino all’omofobia”. **Andrea Barducci scrive ai parlamentari toscani.**   
[Leggi >](http://gaynews24.com/?p=21287)**

**Milano gay: alla Bocconi scritte omofobe: "I froci si curano con il gas e l'Hiv è la vostra punizione".   
[Leggi >](http://milano.repubblica.it/cronaca/2011/05/25/news/bocconi_nuove_scritte_omofobe_il_gas_di_auschwitz_contro_i_gay-16710633/)**

**Treviso: il ristorante che prevede lo sconto anche per le coppie gay.  
[Leggi >](http://www.queerblog.it/post/11275/treviso-il-ristorante-che-prevede-lo-sconto-anche-per-le-coppie-gay)**

**Tabù e pregiudizi,ecco l'Italia omofoba.   
[Leggi >](http://www.fareitalia.com/218_tab_e_pregiudizi__ecco_l_italia_omofoba)**

**UE: Gozi (pd), pregiudizi bloccano maggioranza su libera circolazione coppie internazionali.  
[Leggi >](http://www.agenparl.it/articoli/news/esteri/20110526-ue-gozi-pd-pregiudizi-bloccano-maggioranza-su-libera-circolazione-coppie-internazionali)**

**Vaticano. Bandiere arcobaleno via dalle chiese, ricordano i gay.   
[Leggi >](http://www.notiziegay.com/?p=75817)**

**Nuove dal Vaticano: il condom è inutile per fronteggiare la diffusione del virus Hiv.  
[Leggi >](http://www.notiziegay.com/?p=76031)**

**Francia. ****“Il matrimonio gay? E’ come il sesso tra uomo e animali”.****   
[Leggi >](http://www.giornalettismo.com/archives/127301/il-matrimonio-gay-e-come-il-sesso-tra-uomo-e-animali/)**

**Chiesa di Scozia. Sì alle unioni civili per gay e lesbiche.   
[Leggi >](http://gaynews24.com/?p=21231)**

**Corsaro(Pdl)** **“I matrimoni gay sono una causa della crisi economica”.****   
[Leggi >](http://www.giornalettismo.com/archives/126807/i-matrimoni-gay-sono-una-causa-della-crisi-economica/)**

**Mosca, scontri al Gay Pride: manifestanti aggrediti.  
[Leggi e guarda il video >](http://www.ilquotidianoitaliano.it/esteri/2011/05/news/mosca-scontri-al-gay-pride-manifestanti-aggrediti-87699.html/)**

**Il Consiglio d’Europa condanna gli scontri per il pride a Mosca. Serve dibattito su diritto a manifestare.  
[Leggi >](http://gaynews24.com/?p=21361)**

**Pride negato a Mosca. Usa, dipartimento di Stato preoccupato per violenze in Russia.  
[Leggi >](http://gaynews24.com/?p=21354)**

**Israele. Riconosciuti i diritti di eredità al partner di un gay sopravvissuto all'Olocausto.  
[Leggi >](http://www.queerblog.it/post/11256/niente-gay-pride-a-mosca-il-comune-non-lo-autorizza)**

**Stupri “correttivi” in Sud Africa. Il caso di Noxolo Nogwaza.   
[Leggi >](http://www.notiziegay.com/?p=75977)**

**Texas. Coppie gay non servite ai tavoli dalla (finta) cameriera omofoba. Ecco la reazioni della gente.  
[Leggi e guarda il video >](http://www.queerblog.it/post/11251/coppie-gay-non-servite-ai-tavoli-dalla-finta-cameriera-omofoba-ecco-la-reazioni-della-gente-video)**

**Il sindaco di New York Michael Bloomberg: "Sì ai matrimoni gay! Basta disuguaglianze!"   
[Leggi >](http://www.queerblog.it/post/11262/il-sindaco-di-new-york-michael-bloomberg-si-ai-matrimoni-gay-basta-disuguaglianze)**

**Cile: il Presidente della Repubblica si dice favorevole alle unioni gay.   
[Leggi >](http://www.queerblog.it/post/11259/cile-il-presidente-della-repubblica-si-dice-favorevole-alle-unioni-gay)**

STUDI, RICERCHE E MATERIALI INFORMATIVI
=======================================

**Resoconto della XIV Commissione permanente (Politiche dell'Unione europea) contro la proposta della commissione europea per riconoscimento principio sussidiarietà del 26.05.11**(sul riconoscimento e all'esecuzione delle decisioni in materia di effetti patrimoniali delle unioni registrate, dello stesso sesso o etero nell’UE)**.  
[Leggi >](http://www.camera.it/453?shadow_organo_parlamentare=1507&bollet=_dati/leg16/lavori/bollet/201105/0526/html/14)**

**Donne e lesbiche, doppia discriminazione.** Intervista a** Gabi Calleja **_Membro del Consiglio di amministrazione dell’associazione ILGA-Europe.**   
[Leggi >](http://www.west-info.eu/it/donne-e-lesbiche-doppia-discriminazione/)**_

LIBRI E CULTURA
===============

**I 10 film gay da vedere assolutamente – Classifica. [Leggi >](http://www.vivacinema.it/articolo/cinema-gay-la-top-ten-dei-film-da-vedere-assolutamente/32331/)**

**Il cinema _gay_ va in scena al Queering Roma, dal 3 al 5 Giugno 2011. [Leggi >](http://www.gaywave.it/articolo/il-cinema-gay-va-in-scena-al-queering-roma-dal-3-al-5-giugno-2011/31371/)**

**‎**** Ferzan Ozpetek: “le persone hanno diritti, perché i gay no?” [Leggi >](http://www.notiziegay.com/?p=75983)**

**Pier Vittorio Tondelli: polverosa icona libertina con più fans che lettori. [Leggi](http://www.ilgiornale.it/cultura/polverosa_icona_libertina_piu_fan_che_lettori/30-05-2011/articolo-id=526238-page=0-comments=1)****[>](http://www.ilgiornale.it/cultura/polverosa_icona_libertina_piu_fan_che_lettori/30-05-2011/articolo-id=526238-page=0-comments=1)**

**C’era l’omosessualità dietro la nascita della Beat Generation. E’ raccontata in un libro da William S. Burroughs e Jack Kerouac. [Leggi >](http://www.notiziegay.com/?p=75886)**

**Classificazioni sospette: l’orientamento sessuale di fronte alla legge. [Leggi >](http://www.digayproject.org/Archivio-notizie/classificazioni_sospette.php?c=4455&m=15&l=it)**

**William S. Burroughs, Jack Kerouac, _[E gli ippopotami si sono lessati nelle loro vasche](http://www.adelphi.it/libro/9788845925894)_, Adelphi, €17,00**

**Martha C. Nussbaum_,[Disgusto e umanità. L’orientamento sessuale di fronte alla legge](http://torino2011.saggiatore.it/disgusto-e-umanita/)_****, Il Saggiatore, € 21,00**

[www.certidiritti.it](http://www.certidiritti.it/)
==================================================