---
title: 'La Lituania sta per approvare provvedimento contro le persone gay'
date: Mon, 15 Nov 2010 19:22:24 +0000
draft: false
tags: [Comunicati stampa]
---

**CERTI DIRITTI CHIEDE ALLA COMMISSIONE EUROPEA, AL CONSIGLIO ED ALLE AUTORITA LITUANE DI NON VIOLARE LA LIBERTA DI ESPRESSIONE SUI DIRITTI LGBT. IL GRUPPO ALDE DEL PARLMENTO EUROPEO SCRIVE ALLA COMMISSARIA EUROPEA.**

 **Comunicato Stampa dell’Associazione Radicale Certi Diritti:**

Il Parlamento lituano ha deciso venerdi scorso, 12 Novembre, di esaminare ed approvare in prima lettura un progetto di emendamento che introduce nel codice amministrativo una multa dai 580 ai 2.900 Euro per "promozione dell'omosessualità", comportamento non definito nel Diritto lituano.

L'emendamento é stato presentato dall'on. Grazulis, che nel corso del Gay Pride Baltico della scorsa estate - al quale l’Associazione Radicale  Certi Diritti aveva partecipato con una delegazione composta da Ottavio Marzocchi e Joaquin Nogueroles - aveva rotto la barriera della polizia ed aggredito i partecipanti; perseguito dalle autorità, il Parlamento lituano aveva negato l'autorizzazione a procedere contro di lui.

 Si cerca di colpire in modo illegittimo la libertà di espressione e di manifestazione, in particolare i gay pride, in applicazione della contestata legge sulla "protezione dei minori contro le informazioni dannose", già condannata dalle istituzioni europee e disapplicata dalle autorità lituane fino ad oggi proprio per l'assenza di norme di applicazione: norme che ora sono in preparazione attraverso sanzioni pecuniarie.

Oggi, l'Alde, il gruppo liberale e democratico del Parlamento Europeo, ha scritto alla Commissaria europea per i diritti fondamentali Vivane Reding per chiederle di raccogliere informazioni in occasione della visita in corso in Lituania, e di condannare il progetto di emendamento nel caso le informazioni di stampa si rivelassero corrette.

Certi Diritti si unisce all'iniziativa, chiamando la Commissione, il Consiglio e le autorità lituane a non permettere che la libertà di espressione sia violata con l'approvazione di tale emendamento, prevista per il 16 dicembre.