---
title: 'Certi Diritti pubblica il comunicato stampa dell''Avv. Alexander Schuster sulla sentenza del Tribunale di Rovereto che consente la riattribuzione del genere anagrafico senza l''obbligo di sottoporsi a sterilizzazione chirurgica e la sentenza integrale'
date: Thu, 01 Aug 2013 13:01:26 +0000
draft: false
tags: [Transessualità]
---

Comunicato Stampa dell'avvocato Alexander Schuster

1 agosto 2013

Con una sentenza depositata il 3 maggio 2013 e passata in giudicato a fine luglio il Tribunale di Rovereto ha compiuto un passo importante per la tutela delle persone trans. Dal 1997 ad oggi si registrano solo tre sentenze che riconoscono ad una persona che non intende sottoporsi ad un'operazione chirurgica e senza che sia accertata la sterilità della stessa il diritto ad ottenere il cambio del genere anagrafico. I tre precedenti del 1997, 2011 e 2012 erano, tuttavia, una giurisprudenza isolata del Tribunale di Roma. La decisione roveretana è a quanto consti la prima che fa proprio questo indirizzo fuori dalla capitale. Essa presenta due elementi di rilievo rispetto ai precedenti in materia.

Da una parte fa proprio il principio di diritto secondo cui «nei casi di transessualismo accertato il trattamento medico chirurgico previsto dalla legge 164/82 è necessario nel solo caso in cui occorre assicurare al soggetto transessuale uno stabile equilibrio psicofisico, qualora la discrepanza tra psicosessualità ed il sesso anatomico determini nel soggetto un atteggiamento conflittuale di rifiuto nei confronti dei propri organi genitali, chiarendo che laddove non sussista tale conﬂittualità non è necessario l'intervento chirurgico per consentire la rettifica dell'atto di nascita». Dirimente diviene finalmente il benessere psicofisico del soggetto: un intervento chirurgico è necessario solo dove sia utile per rimediare alla eventuale conflittualità vissuta dalla persona.

Il secondo elemento di rilievo rispetto alle precedenti statuizioni consiste nell'ancorare l'interpretazione della legge allo stesso dato costituzionale. Sostiene il collegio di giudici, infatti che «\[t\]ale condivisa interpretazione poggia, per un verso, sulla considerazione che il dato letterale della legge 164/1982 legittima una rettificazione di sesso anche in assenza di preventivo intervento chirurgico, e ciò in quanto prevede solo che debba essere autorizzato quando necessario, (senza peraltro precisare i termini dello stato di necessità e nemmeno specificare se per caratteri sessuali debbano intendersi quelli primari o secondari e fino a che punto debbano essere modificati) e, per altro verso, su una lettura costituzionalmente orientata della normativa in parola, ponendosi sulla scia della pronuncia della Corte costituzionale n. 161/1985 che ha identificato un concetto ampio di identità sessuale ex art. 2 e 32 Costituzione».

Le disposizioni pertinenti devono essere interpretate letteralmente, senza, cioè, esigere condizioni che in esse non appaiono. Soprattutto, una tale interpretazione – e qui si colloca la più importante novità di questa sentenza – s'impone perché dettata dalla stessa Costituzione e, in particolare, dagli articoli 2 e 32. Appare manifesta in questo approccio del tribunale l'influenza del dibattito internazionale sui diritti umani, di cui è stato protagonista anche il Commissario per i diritti umani del Consiglio di Europa. Questi aveva già nel 2008 criticato la sterilizzazione quale condizione imposta ancora in molti ordinamenti alle persone trans per ottenere la riattribuzione del genere anagrafico.

Questa sentenza favorevole maturata fuori dalle mura capitoline appare consolidare un chiaro revirement giurisprudenziale: imporre l'azzeramento permanente della capacità riproduttiva delle persone trans costituisce violazione dei loro diritti fondamentali ed è inconferente con la tutela della loro identità sessuale. Una chiara speranza per quelle persone trans che fino ad oggi hanno desistito dall'ottenere la «rettificazione del sesso» per i rischi e le complicazioni imposte da operazioni chirurgiche assai complesse e spesso onerose.

**Di seguito il testo della sentenza Tribunale di Rovereto, n. 194 dd. 3 maggio 2013:**

REPUBBLICA ITALIANA

IN NOME DEL POPOLO ITALIANO

ll Tribunale di Rovereto

riunito in Camera di Consiglio nelle persone dei magistrati:

1) Corrado PASCUCCI, Presidente

2) Mariateresa DIENI, Giudice rel.

3) Ilaria Cornetti, Giudice

ha pronunciato la seguente

SENTENZA

nella causa civile iscritta al n. 1027/2012 del r.g.c, promossa con atto di citazione depositato il 28/06/2012

DA

NC nato il XX/XX/196X in XX C.F. XX

rappresentato e difeso dall'avv. Alexander Schuster del Foro di Trento, con studio in Trento, via C. Abba n. 8, giusta procura a margine, domiciliato presso l'avv. Mauro Bondi con studio in Rovereto, C.so Bettini n.7;

-ricorrente-

E con l'intervento del PUBBLICO MINISTERO presso il Tribunale di Rovereto; - convenuto - intervenuto -

OGGETTO: rettificazione sesso.

CONCLUSIONI PER l'attore

In via principale:

\- ordinare ai sensi del d.lgs. n. 150/2011, art. 31, la rettifica degli atti anagrafici dell'attore NC, disponendo che I'Ufficiale dello stato civile del Comune di XX (TN) modifichi tutti i documenti relativi nel senso che risulti quale genere giuridico-anagrafico quello femminile e quale prenome quello di "L.";

In via subordinata e salvo gravame:

\- Previa valutazione positiva della rilevanza e non manifesta infondatezza, sollevare questione di legittimità costituzionale degli articoli 2, legge 14 aprile 1982, n. 164 (Norme in materia di rettificazione di attribuzione di sesso) e 31, comma 4, d.lgs. 1 ° settembre 2011 n. 150 (Disposizioni complementari al codice diprocedura civile in materia di riduzione e semplificazione dei procedimenti civili di cognizione, ai sensi dell'articolo 54 della legge 18 giugno 2009, n. 60), in riferimento agli articoli 2, 3, 1 ° comma, 3, comma 2, 13, 1° c., 31, 1° c., e 32, 117, c. 1° Cost., congiuntamente agli articoli 3, 8 e 14 CEDU, nonché agli articoli 1, 3, 7, Carta dei diritti fondamentali dell'UE, quali norme interposte, con rimessione degli atti alla Corte costituzionale.

\- Rimessione degli atti alla Corte di giustizia dell'UE ai sensi dell'art. 287 TFUE per l'interpretazione degli articoli 1, 3, 7, Carta dei diritti fondamentali dell'UE, art. 20 TFUE, art. 4, c. 3, TUE, nonché direttive n. 2006/54/CE e 2004/113/CE complessivamente interpretate, in riferimento agli articoli 2, legge 14 aprile 1982, n. 164, e 31, comma 4, d.lgs. 1° settembre 2011 n. 150.

CONCLUSIONI DEL P.M. : "nulla oppone".

SVOLGIMENTO DEL PROCESSO E MOTIVI DELLA DECISIONE

Con atto di citazione notificato al p.m., secondo il disposto dell'art. 31 del d.lgs n.150 del 2011, il signor NC ha chiesto che venisse disposta la rettificazione degli atti dello stato civile nella parte relativa al sesso (da maschile a femminile) ed al nome (da N. a L.).

A fondamento della propria richiesta allegava :

di non essere sposato e di non avere figli;

di avere maturato durante il proprio percorso di vita un'identità di genere femminile;

di essersi sottoposto ad un percorso terapeutico endocrinologico al fine di realizzare un adeguamento dei caratteri sessuali secondari;

di avere diritto alla variazione dei propri dati anagrafici anche in assenza di interventi chirurgici diretti all'attribuzione di una anatomia corrispondente al sesso femminile.

La causa veniva istruita con l'assunzione di CTU ed all'udienza del 20.2.2013 l'attore precisava le proprie conclusioni come indicato in epigrafe.

La causa veniva decisa nella camera di consiglio del 2.5.2013.

Ritiene il collegio, alla luce delle risultanze degli atti, che la domanda possa trovare accoglimento.

Risulta dalla documentazione in atti:

una diagnosi per il signor C. fin dall'anno 2007, di disturbo d'identità di genere;

che dal 2009 lo stesso pratica terapia ormonale femminilizzante che ha determinato un mutamento del suo aspetto (e il probabile azzeramento del suo potenziale fecondante);

che tale terapia ha consolidato ulteriormente la sua identità femminile, tanto che dal 2010 inizia socialmente a presentarsi come "L.".

Durante il percorso psicologico della durata di cinque anni, ha espresso con continuità il desiderio di ottenere e mantenere un aspetto femminile.

Il suo aspetto è gradevolmente femminile e si presenta al femminile (si veda relazione a firma della dott. Godano dell'ASL TO 1 – regione Piemonte).

Nella stessa relazione si legge che la paziente esclude di sottoporsi nell'immediato ad intervento chirurgico di rassegnazione poiché con la terapia ormonale ha ottenuto l'equilibrio che le dà benessere. La medesima relazione conclude affermando «in considerazione delle condizioni personali ed attuali di L. una tale operazione presenta inevitabili rischi per la salute e, quando non ritenuta necessaria dal soggetto, sarebbe produttiva di rilevanti squilibri . . .».

ll nominato CTU, dott. Bincoletto , ha escluso a carico del signor C. la presenza di psicopatologia ed ha osservato che la sessualità dello stesso è orientata in senso femminile. ll CTU ha poi evidenziato di non avere elementi, in assenza di psicopatologia, in merito alla bontà ovvero negatività dell'eventuale intervento chirurgico per la modifica dei caratteri sessuali primari sulla psiche del periziando. ll dott. Bincoletto ha poi dato atto di aver registrato la contrarietà del periziando all'intervento stesso che (egli giudica complesso e pericoloso) ed il desiderio della persona di essere considerata a tutti gli effetti una donna.

ll problema da affrontare è quello legato alla possibilità di procedere alla rettifica dei dati anagrafici in assenza di trattamento chirurgico per l'adeguamento dei caratteri sessuali ai sensi della legge n. 164/82 come modificata dal d.lgs. n. 150 del 2011.

Ritiene questo tribunale di poter condividere l'orientamento espresso dal tribunale di Roma negli arresti del 18.10.1997 (in DFP , 1998, 1033 ) e dell'11.3.2011, secondo il quale nei casi di transessualismo accertato il trattamento medico chirurgico previsto dalla legge 164/82 è necessario nel solo caso in cui occorre assicurare al soggetto transessuale uno stabile equilibrio psicofisico, qualora la discrepanza tra psicosessualità ed il sesso anatomico determini nel soggetto un atteggiamento conflittuale di rifiuto nei confronti dei propri organi genitali, chiarendo che laddove non sussista tale conflittualità non è necessario l'intervento chirurgico per consentire la rettifica dell'atto di nascita.

Tale condivisa interpretazione poggia, per un verso, sulla considerazione che il dato letterale della legge 164/1982 legittima una rettificazione di sesso anche in assenza di preventivo intervento chirurgico, e ciò in quanto prevede solo che debba essere autorizzato quando necessario, (senza peraltro precisare i termini dello stato di necessità e nemmeno specificare se per caratteri sessuali debbano intendersi quelli primari o secondari e fino a che punto debbano essere modificati) e, per altro verso, su una lettura costituzionalmente orientata della normativa in parola, ponendosi sulla scia della pronuncia della Corte costituzionale n. 161/1985 che ha identificato un concetto ampio di identità sessuale ex art. 2 e 32 Costituzione.

Ritiene conseguentemente il collegio che, a fronte delle risultanze della documentazione clinica allegata e della CTU e delle argomentazioni sopra svolte, deve essere accolta la domanda svolta dall'attore e deve essere ordinata la richiesta rettifica dell'attribuzione del sesso nei registri dello stato civile da maschile a femminile con assunzione da parte dell'attore del nome di L.

Con riferimento alle spese di giudizio, attesa la particolare natura della controversia, deve escludersi che possa configurarsi la soccombenza di una delle parti e quindi che sussista l'esigenza di regolamentazione delle medesime.

Le spese di CTU andranno poste a carico dell'attore, nel cui interesse è stata disposta.

Rilevato che l'attore è ammesso al gratuito patrocinio, le spese di CTU, da prenotarsi a debito secondo il disposto dell'art. 131 DPR 115/2002, vengono liquidate (avendo riguardo al disposto dell'art. 130 DPR 115/2002 ) in complessivi euro 1000,00, oltre accessori di legge.

P.Q.M.

Il tribunale dl Rovereto, ogni contraria azione ed eccezione disattesa, definitivamente pronunciando così provvede:

in accoglimento dalla domanda,

ordina all'ufficiale dello stato civile di XX di rettificare l'atto di nascita di NC, nel senso che l'indicazione sesso "maschile" deve essere corretta in sesso "femminile" e che il nome "N." deve essere corretto in "L."

Pone a carico dell'attore (ammesso al patrocinio a spese dello Stato) le spese di CTU che si liquidano in euro 1000,00, oltre accessori di legge e delle quali si dispone la prenotazione a debito ex art. 131 DPR 115/2002.

Così deciso in Rovereto nella camera di consiglio del 2.5.2013.

Il Presidente Il Giudice rel.

Dott. Corrado Pascucci Dott.ssa Mariateresa Dieni