---
title: 'BOLOGNA: MATRIMONIO GAY. NO LEGGI NUOVE APPLICHIAMO QUELLE ESISTENTI'
date: Thu, 10 Dec 2009 06:18:29 +0000
draft: false
tags: [Comunicati stampa]
---

_**DICHIARAZIONE DI SERGIO ROVASIO, SEGRETARIO NAZIONALE DELL’ASSOCIAZIONE RADICALE CERTI DIRITTI**_

Dopo che Sergio Chiamparino ha annunciato di voler sposare simbolicamente entro l'anno una coppia lesbica, ci auguriamo che anche il sindaco di Bologna Flavio Del Bono decida di fare altrettanto. È un buon inizio – sostiene Sergio Rovasio, segretario nazionale dell’Associazione radicale Certi diritti – ma non basta.

Dal 2008 l'Associazione radicale Certi Diritti insieme all'Avvocatura per i diritti LGBT - Rete Lenford ha intrapreso una battaglia legale denominata 'Affermazione civile' per il riconoscimento del diritto al matrimonio per le coppie dello stesso sesso. Noi sosteniamo – prosegue Rovasio – che nel nostro Paese non c’è alcun divieto per le coppie omosessuali di contrarre matrimonio. E, con l’entrata in vigore del Trattato di Lisbona, qualsiasi dubbio al riguardo è stato spazzato via. Non serve alcuna nuova legge, basta applicare quella vigente. La società è cambiata e l’omosessualità non è più buon motivo per continuare a discriminare dei cittadini.

Sono più di venti le coppie che in tutta Italia hanno deciso di intraprendere questa importante battaglia per l'uguaglianza – conclude Sergio Rovasio – tra cui c’è già una coppia bolognese che discuterà a metà gennaio il caso davanti al Tribunale della città. L'associazione Certi Diritti è vicina al portavoce di Rete Laica, Maurizio Cecconi, e continuerà a dare sostegno alle coppie gay e lesbiche di Bologna che intendono sposarsi non solo simbolicamente.