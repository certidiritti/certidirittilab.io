---
title: 'AL CONGRESSO DI RADICALI ITALIANI APPROVATI I DOCUMENTI DI CERTI DIRITTI'
date: Tue, 04 Nov 2008 17:36:14 +0000
draft: false
tags: [Affermazione Civile, Comunicati stampa, CONGRESSO, divorzio, transessuali, unioni civili]
---

Al Congresso di Radicali Italiani sono stati approvati a stragrande maggioranza tutti i docuemnti proposti dall'Associaizone Radicale Certi Diritti su campagna di Affermazione civile, prostituzione, transessuali e divorzio breve. Di seguito i testi...

1) Mozione Particolare sulla prostituzione
==========================================

Il VII Congresso di Radicali Italiani,

Premesso che,

- in Parlamento sta per iniziare la discussione del Disegno di Legge del Governo italiano per combattere la prostituzione;

- tale provvedimento è proibizionista, criminogeno, volto a colpire la dignità delle lavoratrici e dei lavoratori del sesso e dei loro clienti e aggrava la condizione delle prostituite e prostituiti coercitivamente;

- già molte amministrazioni locali hanno approvato ordinanze e delibere che anticipano l’orientamento sanzionatorio causando violenze e violazioni dei più elementari diritti civili e umani;

- l’aggravio in senso proibizionista delle politiche sulla prostituzione rende questo fenomeno sempre più clandestino, producendo e alimentando l’illegalità, l’assenza di controlli, aggravando la condizione delle persone più vulnerabili come migranti, transessuali, travestiti, e, in genere, i più poveri;

- la totale mancanza di assistenza, informazione, aiuto e sostegno alle lavoratrici e ai lavoratori del sesso provoca danni sociali;

- il prossimo 13 dicembre in tutta Europa, proprio per quanto sta accadendo in Italia, le “sex workers” hanno promosso manifestazioni e iniziative; in Italia, a Roma, lo stesso giorno, si terrà una manifestazione-happening che vedrà protagoniste decine di associazioni che si battono per i diritti civili;

Impegna Radicali Italiani:

- a sostenere le proposte e i disegni di legge fino ad ora presentati volti al riconoscimento della professione di lavoratrici/ori del sesso

- ad aderire e partecipare alla manifestazione del 13 dicembre prossimo nell’ambito delle iniziative promosse in tutta Europa “contro la violenza sulle lavoratrici e sui lavoratori del sesso”;

- a promuovere manifestazioni e iniziative di contrasto e di proposta in occasione della discussione del provvedimento governativo, la cui discussione inizierà nei prossimi giorni in Parlamento

Primo firmatario: Sergio Rovasio

2) Mozione particolare sulle azioni di affermazione civile
==========================================================

Il 7° Congresso di Radicali Italiani, riunito a Chianciano dal 30 ottobre al 2 novembre 2008,

Premesso che

\- L’Italia è uno degli ultimi Paesi tra le democrazie occidentali a non aver riconosciuto alcuna forma di riconoscimento delle unioni affettive;

- il diritto al matrimonio per le persone omosessuali e/o al riconoscimento giuridico delle unioni civili è al centro di numerosi documenti e prese di posizione del Parlamento europeo e della Commissione europea;

- ogni tentativo operato fino ad oggi per introdurre una qualche forma di riconoscimento delle unioni civili è fallita, non solo grazie alla potente rinascenza del fondamentalismo cattolico e al clericalismo delle forze politiche italiane, ma anche all’incapacità delle forze laiche (o cosiddette tali) di adottare strategie efficaci perché le leggi del nostro Paese si adeguino al sentire della stragrande maggioranza dei suoi cittadini;

Considerato che

- di fronte alla situazione di blocco che si sta realizzando su questo tema, così come su altri temi connessi alla libertà di scelta degli individui ed al riconoscimento pieno delle diversità, è necessario procedere con iniziative puntuali, anche parziali, ma che consentano di tener vivo il confronto pubblico e cerchino di trovare una via per scalfire il muro che le forze clericali e dei loro sodali fintamente laici, hanno costruito tra la società e i nuovi diritti che richiede;

Viste

- l’iniziativa di affermazione civile che l’Associazione radicale Certi Diritti in collaborazione con la Rete Lenford, ha avviato affinchè attraverso l’azione in giudizio di coppie omosessuali che ricevono il rifiuto da parte degli uffici anagrafici comunali di pubblicare il proprio annuncio di matrimonio, si riesca a far esprimere le corti (fino all’Alta corte di giustizia europea) sulla evidente disparità di diritti tra persone omosessuali ed eterosessuali;

- le iniziative delle stesse associazioni per ottenere il riconoscimento dei matrimoni e delle unioni riconosciute in altri Paesi del mondo e in Italia;

Impegna

- gli organi dirigenti di Radicali Italiani a sostenere e promuovere le iniziative di affermazione civile citate, sia attraverso le associazioni e i punti di riferimento radicale, sia attraverso una collaborazione concreta nella sua realizzazione;

Invita

- i parlamentari radicali a continuare e potenziare la loro iniziativa di sostegno di queste azioni di affermazione civile.

Primo firmatario: Enzo Cucco

3) Mozione particolare sul divorzio breve
=========================================

Il Congresso di Radicali Italiani sostiene l’iniziativa del tavolo permanente per la riforma globale del diritto di famiglia avviata con il progetto “Amore Civile”; iniziativa che vede coinvolta la Lega italiana per il Divorzio Breve insieme ad associazioni, professionisti, docenti, giuristi e semplici cittadini interessati all’evoluzione dell’istituto matrimoniale e al conseguente adeguamento del dato normativo.

Dà mandato agli organi dirigenti del movimento di porre in essere tutte le iniziative e le azioni necessarie volte ad inserire nel dibattito politico i temi riguardanti l’evoluzione ed il superamento della c.d. “famiglia tradizionale” a partire dalla richiesta di immediata calendarizzazione nelle opprtune sedi parlamentari delle pdl in materia di diritto di famiglia presentate dai deputati e dai senatori radicali eletti nelle liste del PD; in particolare i disegni di legge riguardanti il divorzio breve, gli unici potenzialmente in grado di raccogliere un consenso trasversale in Parlamento.

Primo firmatario: Diego Sabatinelli

RACCOMANDAZIONE
===============

Il 7° Congresso dei Radicali Italiani, riunito a Chianciano dal 30 ottobre al 2 novembre 2008,

Premesso che

la realtà che le persone transessuali nel nostro paese devono affrontare è ancora gravida di problemi irrisolti, non solo quelli connessi all'applicazione della legge, ma anche quelli relativi ad una società e ad una cultura prevalente che, al di là di qualche spot nel mondo dello spettacolo, è ancora imbevuta di pregiudizio e transfobia;

Ricordato che

fu grazie all'iniziativa di deputati radicali che si pervenne all'approvazione della legge 164 del 1982 che, pur avendo rappresentato l'avanguardia in Europa in materia di riconoscimento del cambio di identità di genere, oggi deve essere adeguata e riformata;

i deputati radicali hanno presentato una pdl di riforma della legge 164 che accoglie tutte le necessarie modifiche di questa norma e che, oggi come allora, si deve tentare il coinvolgimento del più ampio numero di parlamentari per una riforma necessaria per rilanciare il confronto sui temi dei diritti delle persone transessuali e della lotta alla sessuofobia, omofobia e transfobia;

Invita

gli organi dirigenti di Radicali Italiani ed i deputati radicali a sostenere e promuovere tutte le iniziative necessarie affinché si possa giungere entro breve tempo alla riforma della legge 164/1982.