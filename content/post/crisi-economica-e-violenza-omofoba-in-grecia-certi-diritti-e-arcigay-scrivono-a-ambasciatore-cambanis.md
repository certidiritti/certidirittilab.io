---
title: 'Crisi economica e violenza omofoba in Grecia: Certi Diritti e Arcigay scrivono a ambasciatore Cambanis'
date: Tue, 13 Nov 2012 11:31:14 +0000
draft: false
tags: [Europa]
---

Comunicato stampa dell’Associazione Radicale Certi Diritti

Roma, 13 novembre 2012

L’organizzazione LGBTI ΟΛΚΕ - Ομοφυλοφιλική Λεσβιακή Κοινότητα Ελλάδας, ha recentemente denunciato la grave situazione greca dove i militanti di “Alba Dorata” e di altri gruppi nazionalisti, reagiscono alla grave crisi economica in cui versa il Paese, trovando capri espiatori tra tutti i “diversi”, immigrati e persone LGBTI (Lesbiche, Gay, Bisessuali, Transessuali e Intersessuali), sempre più sepsso oggetto di una violenta furia espiatrice.

In pochi giorni sono stati denunciati alla polizia almeno 5 attacchi omofobi. Uno di questi è avvenuto nella zona di Kerameikos, ad Atene, mettendo a rischio la vita di un attivista per i diritti LGBTI, Sapountzakis.

La situazione delle lavoratrici sessuali transgender è ancor più drammatica: continue percosse, arresti e controlli in questura, effettuati dalla polizia in collaborazione con il Centro ellenico per il controllo delle malattie e la prevenzione, allo scopo, preteso, di prevenire il rischio che queste lavoratrici possano contribuire alla diffusione dell’HIV (quando, invece, secondo la legge ellenica, è il cliente a dover prendere ogni misura di precauzione).

Di fronte a questa denuncia l’Associazione Radicale Certi Diritti e Arcigay hanno scritto all’ambasciatore greco a Roma, Michael E. Cambanis, chiedendogli d’intercedere presso il Governo greco affinché intervenga immediatamente presso la polizia per far cessare gli abusi sulle persone trans e per assicurare prontamente i responsabili degli attacchi omofobi alla giustizia. È altresì fondamentale - scrivono Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti e Paolo Patanè, presidente nazionale di Arcigay -  “che si costruisca un rapporto di fiducia e collaborazione tra la comunità LGBTl e le Autorità greche attraverso l’istituzione di un Osservatorio sui crimini omo/transfobici e l’avvio di corsi di formazione per le forze dell’ordine”.

“Tali misure - concludono i rappresentanti delle due associazioni italiane - costituirebbero un deterrente efficace e un messaggio politico importante, per restituire allo Stato Ellenico quell’orgoglio e dignità, che hanno consentito alla cultura greca di divenire così influente in  
tutta Europa”.