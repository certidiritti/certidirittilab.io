---
title: 'Registro delle unioni civili a Palermo importante, ma servono benefici reali'
date: Wed, 09 Nov 2011 10:51:33 +0000
draft: false
tags: [Diritto di Famiglia]
---

Nota congiunta delle associazioni:

Articolo Tre Palermo  
Arcigay Palermo  
Associazione radicale David Kato Kisule-Radicali Palermo

  
Il consiglio comunale di Palermo, con una maggioranza schiacciante, ha approvato oggi la mozione trasversale rispetto agli schieramenti, che istituisce il registro delle unioni civili, certificando le "convivenze basate su vincoli affettivi che si protraggono da almeno un anno presso l’ufficio comunale competente, senza discriminazioni di razza, etnia, sesso, genere, handicap ed orientamento sessuale" ed invita il sindaco ad attivarsi per la discussione e la votazione del ddl regionale in materia.

Si tratta di un passo importante, del riconoscimento del fatto che in città esiste una minoranza numerosa che non resta in silenzio, che reclama attenzione e diritti. Otto anni fa una mozione molto simile è stata respinta; ma oggi, anche grazie a due pride che hanno visto una grandiosa partecipazione, il clima in città e nel Consiglio comunale è molto diverso.Quello che ci auguriamo, però, è che questo sia solo un primo passo verso una reale parità di dignità e diritti. La semplice istituzione del registro delle unioni civili, infatti, non è sufficiente, perché non cambia in alcun modo la vita quotidiana delle persone trans, lesbiche, gay e bisessuali della città. Per garantire un cambiamento profondo occorre che associazioni e istituzioni lavorino insieme in maniera più stretta di quanto non abbiano fatto fino ad oggi. Serve un centro di accoglienza per le persone trans, che spesso vengono cacciate da casa e si trovano senza mezzi per strada; servono percorsi contro le discriminazioni e il bullismo; servono percorsi di integrazione; servono, lo ribadiamo con forza, pari diritti e opportunità per tutti, indipendentemente da genere e orientamento sessuale.

\- Associazione Omosessuale Articolo Tre Palermo  
\- Arcigay Palermo  
\- Associazione radicale David Kato Kisule-Radicali Palermo  
[http://articolotrepalermo.blogspot.com/2011/11/registro-delle-unioni-civili-palermo.html](http://articolotrepalermo.blogspot.com/2011/11/registro-delle-unioni-civili-palermo.html)