---
title: 'La via legale per il diritto al matrimonio funziona. Buone notizie dal Brasile e dagli Usa. In Italia la campagna di Certi Diritti continua'
date: Fri, 28 Oct 2011 05:02:57 +0000
draft: false
tags: [Americhe]
---

Le Corti di Brasile e Stati Uniti riconoscono diritto al matrimonio. In Italia continua la campagna di Affermazione Civile.

Roma, 28 ottobre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Le notizie che provengono dal Brasile e dagli Stati Uniti sono molto importanti per chi ha scelto la via legale per vedere riconosciuti i diritti all’accesso all’istituto del matrimonio per le persone dello stesso sesso.

Ieri, la Corte di Cassazione del Brasile (Superior Tribunal de Justica) ha riconosciuto la legalita' del matrimonio civile tra due donne che avevano deciso di registrare il loro matrimonio civile presso un notaio. Di fronte al diniego hanno attivato la giustizia ma un giudice di Porto Alegre, e poi il Tribunale di giustizia del Rio Grande avevano respinto la loro richiesta. Il ricorso al Tribunale Superiore di Giustizia (STJ) ha riconosciuto infine le ragioni della coppia che risulta così sposata civilmente.

Oggi è la volta del Tribunale distrettuale di Boston che ha accolto un ricorso del Service Legal Defense Network, gruppo di avvocati, che assiste  sette militari Usa contro il Defense Marriage Act, legge federale che impone il matrimonio solo per le coppie eterossuali. Secondo il Tribunale di Boston il Defense Marriage Act è incostituzionale. Su questa decisione è stato presentato un ricorso e ora dovrà esprimersi la Corte d’Appello del primo circuito federale.

Lo stesso Presidente Usa Barack Obama aveva dichiarato un mese fa che il Governo Usa non avrebbe più fatto ricorso al sostegno di questa legge federale qualora i singoli  Stati Usa vi si fossero appellati per impedire l’approvazione di leggi di riconoscimento del matrimonio civile tra persone dello stesso sesso.

L’Associazione Radicale Certi Diritti continua la sua iniziativa politico-legale con la campagna di Affermazione Civile affinchè vengano riconosciuti anche in Italia i matrimoni civili contratti all’estero da coppie formate da persone dello stesso sesso, contro ogni ingiustizia e discriminazione. Le iniziative legali sostenute da Certi Diritti sono rivolte ai Tribunali e le Corti d’Appello italiane, la Corte Europea dei Diritti dell’Uomo e la Corte di Giustizia Europea.