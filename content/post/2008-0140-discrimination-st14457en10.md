---
title: '2008-0140-Discrimination-ST14457.EN10'
date: Mon, 04 Oct 2010 12:17:20 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 4 October 2010

Interinstitutional File:

2008/0140 (CNS)

14457/10

LIMITE

SOC 609

JAI 804

MI 353

  

  

  

  

  

NOTE

from :

General Secretariat

to :

The Working Party on Social Questions

on :

19 October 2010

No. prev. doc. :

13883/10 SOC 561 JAI 759 MI 317

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

In preparation of the next meeting of the Working Party, which is provisionally scheduled for 19 October 2010, delegations will find attached a note from the BG delegation.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Presidency questionnaire concerning housing**

**\[Document 13883/10\]**

**BG replies**

**1\.** **Situation in Member States**

Both the Racial Equality Directive (2000/43/EC) and the Gender Goods and Services Equality Directive (2004/113/EC) cover housing available to the public in their scope. Several MS have gone beyond these two grounds in their protection against discrimination.

a)       To what extent does your national legislation against discrimination apply to housing?

b)      Do you distinguish (for this purpose) between private and public/social housing?

_The respective Law on Protection against Discrimination in it’s Art.37 does not allow refusal of access to goods and services, or offer of goods and services with lower quality or within worse conditions, based on discrimination on any of the grounds for discrimination, specified in any European or international legally binding instrument, to which Bulgaria is a party /Art.4(1)/. Housing is offered on the regional level by the respective municipalities. There are also specialized institutions for social services._

c)       Do you have statistics on the number of cases of discrimination in the area of housing, and if so, what is the most prevalent ground?

_Such statistics was not presented by the Commission on Protection against Discrimination._

**2\.** **Social housing**

a)       Do you consider social housing to be a service (for the purposes of EU law)?

b)      If not, do you consider it to be a part of the social protection system?

_Social housing is part of the social protection system._

**3\.** **Private life**

In some cases, the principle of equality might be in conflict with the right to private life, in particular in the area of housing (e.g. renting out a room in your own apartment).

a)       Is this better taken into account by an exclusion of transactions in the area of private life from the scope of the Directive, or by an exclusion of activities which are not commercial or professional?

_The above cited law does not cover private life__._ _Activities, which are commercial or professional, are easier to define, so the exclusion of such which are not would be more appropriate in respect to interpretation._

b)           How is commercial/professional housing defined in your national legislation?

_The following types of housing are defined into the respective national legislation: communal housing, housing for old people, and housing for children._

_The Law on the Communal Property stipulates everything in respect to the types of communal houses, their purpose and way of letting out, selling and exchange. There is an opportunity to let out houses from the communal reserve fund for persons/families in need, or to such with social or health related problems._

_The implementation act of the Law on Social Assistance stipulates the following three specialized institutions for social services: homes for children /deprived of parental care; with physical disabilities; with mental disabilities/; homes for adults with disabilities /with mental retardation; with mental disorders; with physical disabilities; with sensory disorders; with dementia/; and homes for elderly._

_Persons subject to domestic violence could use accommodation in the so called crisis centers._

**4\.** **Disability - reasonable accommodation**

a)       In your view, what should the obligation to provide reasonable accommodation mean for providers of housing?

_-        should know the needs of the respective persons;_

_-        should be acquainted with the respective legislation, incl. whether the accommodation provided is adequate for the needs of the user;_

_-        have a complex approach to the problems in respect to accommodation of people with disabilities._

b)      Is there legislation on the accessibility of buildings (private as well as public/social) in your country?

_The following legislation applies in this respect:_

_Law on Integration of People with Disabilities, section 4- Conditions for accessible living and architectural environment /Art.32 and 33/._

_Law on Spatial Development, section 3- Rules in respect to buildings._

_Decree #4/01.07.2009 for the Design, construction and maintenance of buildings in the context of the requirements for accessible environment, including for people with disabilities._

**5.** **Disability - accessibility**

a)       What should the obligation to provide for accessibility measures encompass in the area of housing?

_The system of measures for insuring of accessible housing environment should be a complex one, considering the requirements for accessible environment as a whole, as well as in respect to communication and infrastructure, which are a compulsory element for accessibility as a whole and housing accessibility in particular. The above cited Decree #4/2009 covers those issues. Taking into consideration the importance of the problem and to insure the rights of people with disabilities, the conduct of information campaigns and training of the respective personnel /which is being done by the Ministry of Regional Development and Public Works/, for the implementation of the respective norms for the design and construction of buildings in urban territories, is of utmost importance._

b)      Should there be a difference between old and new buildings?

_There are normative requirements for insuring accessible architectural environment_ _for the whole population, taking into consideration the specific needs of people with reduced mobility, incl. people with disabilities. At the same time it should be taken into account that the adaptation of old buildings to the new requirements is highly related to time and investments. That is the reason a longer transition period is necessary in respect to old buildings, as well as derogation for buildings which are architectural and historical monuments._

c)       Do you support immediate applicability with respect to new buildings?

_The implementation of the above normative requirements is inseparable part of the start of exploitation of new buildings._

  

d)      Should there be a difference between the obligations for private and public/social housing respectively?

_Taking into account that buildings should take care of specific /and in the general case identical in respect to the disability/ requirements, a differentiated approach, based on the status of property is not advisable. At the same time it should be considered that for private houses there could be differences, but only in respect to additional elements, without violating the normative requirements_

e)  Are there public funds available in your country for the adaptation of housing (private as well as public/social)?

_There is no national specialised fund for such activities. There is a programme, run by the Ministry of Labour and Social Policy through the Agency for People with Disabilities, for financing of projects for access to cultural, sport and historical sites with international, national or local character._

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_