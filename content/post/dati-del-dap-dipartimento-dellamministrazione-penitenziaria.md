---
title: 'Dati del DAP (Dipartimento dell''Amministrazione Penitenziaria)'
date: Mon, 28 Feb 2011 17:58:20 +0000
draft: false
tags: [Politica]
---

**Dati del DAP (Dipartimento dell'Amministrazione Penitenziaria)** relativi alle **presenze di detenuti trans nei penitenziari italiani (aggiornati al 14 gennaio 2011)**

**Capienza regolamentare  
**

**Capienza tollerabile  
**

**presenti  
**

**C.C.ALBA**

**10  
**

**10**

**8**

**C.C. MILANO San Vittore  
**

**0**

**0  
**

**11**

**C.C.RIMINI**

**7  
**

**7  
**

**10  
**

**C.C.BELLUNO**

**16**

**32**

**24  
**

**C.C.VERONA**

**0**

**0  
**

**4  
**

**C.C.FIRENZE Sollicciano  
**

**8  
**

**10  
**

**25  
**

**C.C.TERNI**

**10**

**20  
**

**3 (sez. non funzionante)  
**

**C.C.REBIBBIA ROMA N.C.1  
**

**15**

**20  
**

**25  
**

**C.C.TERAMO**

**2  
**

**4  
**

**4  
**

**C.C.NAPOLI Poggioreale  
**

**22  
**

**33  
**

**32  
**

**C.C.FOGGIA**

**4  
**

**4  
**

**5  
**

**C.C.POTENZA**

**0  
**

**0  
**

**4 (non funzionante)  
**

**C.C.AGRIGENTO**

**2  
**

**4  
**

**4  
**

**C.R.AUGUSTA  
**

**0  
**

**0  
**

**1  
**

**C.C.PALERMO Ucciardone  
**

**0  
**

**0  
**

**5 (non funzionante)  
**

**C.C.PALERMO Pagliarelli  
**

**0  
**

**0  
**

**1 (non funzionante)  
**

**C.C.TRAPANI  
**

**0**

**0**

**2 (non funzionante)  
**

[http://www.clandestinoweb.com/in-primo-piano/20951-la-voce-dietro-le-sbarre-transessuali-in-cella-tra-sovraffollamento-e-d.html](http://www.clandestinoweb.com/in-primo-piano/20951-la-voce-dietro-le-sbarre-transessuali-in-cella-tra-sovraffollamento-e-d.html)