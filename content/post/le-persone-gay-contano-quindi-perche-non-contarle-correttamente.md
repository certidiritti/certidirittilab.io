---
title: 'Le persone gay contano, quindi perché non contarle correttamente?'
date: Mon, 18 Apr 2011 10:35:33 +0000
draft: false
tags: [Politica]
---

Circa 19 milioni di americani (8,2%) dichiarano di aver avuto rapporti sessuali con una persona dello stesso sesso, e quasi 2 (l'11%) dicono di essere attratti da persone dello stesso sesso. Ma oggi, piuttosto che quantificare la popolazione, è più opportuno capire come le persone LGBT vivono la propria vita. Articolo pubblicato dal Washington Post il 8 aprile 2011.

  
Negli anni 60 i primi attivisti gay trovarono uno strano  passaggio scritto in un libro del 1948 del famoso sessuologo Alfred Kinsey che diceva “circa il 10% dei maschi sono esclusivamente gay… per almeno 3 anni tra i 16 e i 55 anni”. In base a quanto scritto si dedusse che 10% della popolazione fosse gay, nonostante lo studio di Kinsey non fosse stato condotto con l’idea di fare una stima della popolazione.  
  
La stima del 10% non aveva delle basi scientifiche, bensì politiche. In quel periodo gli attivisti gay avevano bisogno di provare che esistesse una comunità  gay. Uno su dieci era un numero esagerato per dare supporto a tale idea. Quando ero giovane e non avendo ancora fatto il coming out mi ha fatto un certo effetto sapere che in un aula di 50 persone 4 fossero gay.  
  
Ma la percentuale non era così grande da incidere eccessivamente in una società ancora molto a disagio con l'idea di omosessualità. Il fatto che si parla ancora di una tale percentuale testimonia la genialità di questa strategia politica.  
Molti americani non hanno idea di quanti gay e lesbiche ci siano. Da un sondaggio della Gallup del 2002 emergeva che un americano su sei non sapeva esprimere alcuna stima al riguardo, mentre il resto stimava la comunità LGBT al 20%  
  
In qualità di  demografo che studia la comunità lesbica, gay, bisessuale e transgender, mi è stato chiesto innumerevoli volte quante persone LGBT esistono. La politica può ancora avere un ruolo nella formazione della risposta, ma certamente non c'è più bisogno di dimostrare che le persone gay esistono. Oggi, piuttosto che quantificare la popolazione è invece più opportuno capire come le persone LGBT  vivano la propria vita. Quanti se ne sposano? Quanti di questi hanno figli? Quanti fanno il servizio militare? Subiscono discriminazioni?  
  
Queste dati sono importanti, poiché legislatori, giudici ed elettori in tutto il paese stanno discutendo su come queste persone dovrebbero vivere la loro vita. Tutte le parti meritano di essere informate da nuove ricerche piuttosto che affidarsi a ricerche che risalgono a  60 anni fa.  
  
Dovremmo essere in grado di ricercare sui luoghi a cui gli studiosi e i policy makers si rivolgono per ottenere informazioni circa la salute e il benessere degli americani - tutti gli americani. Il censimento decennale del Census Bureau e l’American Community Survey sono le fonti primarie di informazioni demografiche in questo paese. Oppure il National Health Interview Survey, una fonte primaria di informazioni sulla salute degli americani. O la Current Population Survey, la fonte preminente di informazioni sul benessere economico della nazione.  O la National Crime Victimization Survey, da dove si ottiene la maggior parte dei nostri dati sulle esperienze  criminali.  
  
Ma la ricerca di tali fonti per raccogliere informazioni sulle persone LGBT sarebbe in gran parte inutile. Nessuno di esse pone domande di orientamento sessuale o identità di genere.  
  
Recentemente ho rivisto i risultati di 11 studi a larga scala condotti dal 2004, 7 negli Stati Uniti e quattro a livello internazionale. Facendo una media tra le indagini fatte negli USA,  si evince che circa 9 milioni di americani (il 3,8% degli adulti) si dichiarano di orientamento LGBT. Questa percentuale equivale al   numero della popolazione del New Jersey.  
  
Circa 19 milioni di americani (8,2%) dichiarano di aver avuto rapporti sessuali con una persona dello stesso sesso, e quasi 26 milioni (l'11%) dicono di essere attratti da persone dello stesso sesso, vale a dire l'equivalente della popolazione del Texas.  
  
Ma da demografo non mi interessa limitare la mia ricerca a semplici medie statistiche. Ho partecipato a decine di incontri con rappresentanti di istituti di statistica federale e ho chiesto loro il motivo per cui non censiscono la popolazione LGBT. Mi dicono che si preoccupano del fatto che gli intervistati possano rifiutarsi  di rispondere  a tali domande o, peggio ancora, possano rifiutarsi di partecipare al sondaggio. Non saprebbero neanche quali domande porre.  
  
Dovrebbero censire solo quelli che si identificano esplicitamente con termini come "lesbica", "gay" o "bisex"? O dovrebbero forse esaminarne il comportamento sessuale? O l'attrazione sessuale? E per la popolazione transgender dovrebbero includere solo coloro che hanno esplicitamente cambiato sesso oppure  prendere in considerazione un più ampio gruppo di persone che esprimono il loro sesso in modi che non sono facilmente conformi alle nozioni tradizionali di maschio e femmina?  
  
La Fondazione Ford ha recentemente finanziato uno studio di cinque anni in cui gli studiosi hanno considerato questi importanti quesiti. Essi hanno concluso che le preoccupazioni circa la non-risposta o il rifiuto di sottoporsi all'intervista sono infondate. Gli intervistati rifiutano di rispondere a domande circa il loro reddito molto più spesso di quanto non lo facciano per domande riguardanti il loro orientamento sessuale o identità in genere.  
  
Per quanto riguarda le preoccupazioni su ciò che intendiamo per LGBT, non esiste una definizione precisa. Gli studiosi del campo convergono su una serie di domande che indagano i diversi aspetti dell'orientamento sessuale, tra cui l'auto-identificazione, il comportamento sessuale e l'attrazione sessuale, documentando questa  best practice  per valutare l'identità di genere.  
  
Non tutte le indagini devono comprendere necessariamente domande su ogni aspetto dell'orientamento sessuale e dell'identità in genere. Le indagini in questo campo dovrebbero impegnarsi solo su quelle sfere che vale la pena ricercare in base allo scopo dell’indagine stessa.  
  
Un sondaggio che intenda indagare sulle discriminazioni sul posto di lavoro potrebbe, per esempio, focalizzarsi sui gay dichiarati, visto che potrebbe essere difficile dimostrare che si è vittime di discriminazioni, a meno che il proprio orientamento sessuale sia stato dichiarato.  
  
Un sondaggio che considera i fattori di rischio dell'HIV potrebbe includere domande sul comportamento sessuale, visto che è il modo di contagio più probabile. Un sondaggio sui giovani potrebbe invece concentrarsi maggiormente sull’attrazione sessuale, dato che molti giovani non hanno avuto esperienze sessuali e non saprebbero come definirsi.  
  
In realtà, il numero di persone che si identifica come LGBT può essere diverso dal numero di coloro che dicono di avere esperienze sessuali con persone dello stesso sesso e il numero di coloro che riconoscono le attrazioni dello stesso sesso. Il fatto che sussistono  differenze in queste stime non significa che non possiamo censire la popolazione LGBT con precisione. Piuttosto, esse dimostrano che l'orientamento sessuale e l'identità di genere sono concetti complessi meritevoli di ulteriori studi.  
  
L'Istituto di Medicina della National Academies ha recentemente reso pubblica un'analisi completa dello stato della ricerca sulla salute e il benessere della comunità LGBT. Le note del rapporto indicano l'inclusione della comunità LGBT attraverso una vasta gamma di indagini federali e un maggiore supporto per la ricerca LGBT finanziata da fondi federali. Questa è un'ottima notizia. Le agenzie federale di statistica potrebbero e dovrebbero seguire i suggerimenti forniti. Tali dati potrebbero fornire le basi per la ricerca necessaria per capire la vita degli americani LGBT, che sono stati troppo spesso emarginati dai ricercatori, così come dalla società in generale.  
  
Sento spesso lamentele da parte di membri della comunità LGBT sulla parificazione dei loro diritti,  poiché si sentono una grande comunità.  
  
Da demografo invece ho un'idea un po' diversa: sono impressionato dalla parità dei diritti che esiste nonostante l'esigua grandezza della comunità LGBT.  
  
Entrambe le idee sono sicuramente valide, ma entrambe dipendono in parte da una valutazione accurata della comunità LGBT – a prescindere da come la si voglia definire.  
  
Speculare con ipotesi sulla realtà delle persone produce inaccuratezze; sono i numeri invece a dare certezze. La realtà del nostro sistema politico è che non si conta nulla a meno che non si viene contati. È quindi il momento di smettere di affidarsi a vecchie stime e condurre un censimento accurato.