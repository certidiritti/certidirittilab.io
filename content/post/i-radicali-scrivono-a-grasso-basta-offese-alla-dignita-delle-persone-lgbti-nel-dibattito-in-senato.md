---
title: 'I RADICALI SCRIVONO A GRASSO: BASTA OFFESE ALLA DIGNITÀ DELLE PERSONE LGBTI NEL DIBATTITO IN SENATO.'
date: Tue, 09 Feb 2016 11:25:09 +0000
draft: false
tags: [Politica]
---

[![ed610085_smush_parl_3](http://www.certidiritti.org/wp-content/uploads/2014/06/ed610085_smush_parl_3-300x110.png)](http://www.certidiritti.org/wp-content/uploads/2014/06/ed610085_smush_parl_3.png)L'Associazione Radicale Certi Diritti [ha scritto](http://www.certidiritti.org/wp-content/uploads/2016/02/Lettera-Grasso.pdf) oggi al presidente del Senato Pietro Grasso per chiedergli di prendere posizione contro le ripetute offese alla dignità delle persone LGBTI che vengono ripetute in aula durante il dibattito sul ddl Cirinnà.

"Non a favore o contro il ddl, non a favore o contro l’omosessualità, ma a favore della nostra Costituzione e dei suoi principi a cui tutte e tutti, compresi i Senatori della Repubblica, siamo soggetti" [scrive](http://www.certidiritti.org/wp-content/uploads/2016/02/Lettera-Grasso.pdf) Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti.

"Ovviamente non è in discussione la possibilità di esprimere i propri valori ed il proprio giudizio sul ddl in questione e più in generale sulla vita delle persone gay, lesbiche, transessuali e intersessuali", [precisa](http://www.certidiritti.org/wp-content/uploads/2016/02/Lettera-Grasso.pdf) Guaiana, "ma quando si ascoltano affermazioni che, palesemente ignoranti, offendono la dignità di milioni di cittadini e cittadine italiani, che sono in tutto uguali di fronte alla legge, alla scienza, alla morale ed al senso comune, rimaniamo oltre che basiti anche molto scandalizzati".

"Si rendono conto i Senatori" [chiede](http://www.certidiritti.org/wp-content/uploads/2016/02/Lettera-Grasso.pdf) Guaiana, "che affermazioni così discriminatorie contro donne, immigrati, ebrei, rom, sinti e disabili non sono accettabili ne ammesse in un tale consesso"?