---
title: 'Il papa come Berlusconi: vuole saturare i media con i suoi messaggi pre-elettorali contro diritti lgbt. Le parole non bastano più. Certi Diritti lancia sito dedicato alle famiglie non sposate e guida su come sposarsi in Portogallo'
date: Fri, 21 Dec 2012 13:39:08 +0000
draft: false
tags: [Europa]
---

comunicato stampa Associazione radicale Certi Diritti

Roma, 21 dicembre 2012

Dopo la vergogna di aver ricevuto con tanta ufficialità la portavoce del Parlamento ugandese, paladina della nuova proposta di legge antiomosessuale (che se saremo fortunati prevederà “solo” l’ergastolo per gli omosessuali e non più la pena di morte) il Papa continua imperterrito il suo martellamento pre-elettorale contro il matrimonio egualitario. Anche oggi torna sull’argomento, ed ogni occasione sarà buona, da qui alle prossime elezioni politiche per ribadire uno dei concetti che meno hanno bisogno del suo magistero: ora e sempre contro i diritti delle persone lgbt..  
   
Contro questo comportamento non bastano più le parole, dobbiamo produrre fatti, iniziative concrete, per contrapporre speranza e futuro all’oscura chiusura di un Papa che guarda al peggior passato della Chiesa e della Società.  
   
Le famiglie lgbt esistono e cercano di vivere in mezzo a mille impedimenti. E si moltiplicano anche quelle coppie dello stesso sesso che vogliono compiere il grande passo di responsabilità e futuro rappresentato dal matrimonio.  
   
Oggi, dedicandolo a tutte quelle coppie che, anche eterosessuali, non unite in matrimonio e che hanno bisogno di informazioni, chiarimenti, indicazioni pratiche su come sopravvivere in un paese che fa finta che non esistano, l’Associazione radicale Certi Diritti lancia una nuova iniziativa.   
   
**[www.sosfamiglie.it](http://www.sosfamiglie.it/)**  
   
un sito dedicato a come affrontare e provare a risolvere i tanti problemi che le coppie non sposate vivono in Italia. Tutto costruito sul lavoro dei volontari e delle volontarie della nostra associazione e da un piccolo gruppo di avvocati specializzati.  
   
L’altra iniziativa che oggi la nostra Associazione lancia è una scheda informativa su come le coppie formate da persone dello stesso sesso possono, se lo vogliono, sposarsi in Portogallo. Uno dei pochi Paesi europei che celebra matrimonio anche tra cittadini non portoghesi.  
**[Qui la guida >](notizie/comunicati-stampa/item/download/22_24960e9187ee7ab1c53b5953b3114c2b)**

Questo è il nostro modo per rispondere, nei fatti e non a parole, alle ripetute esternazioni pre-elettorali papaline.

  
[sposarsi\_in\_Portogallo\_definitivo\_1.pdf](http://www.certidiritti.org/wp-content/uploads/2012/12/sposarsi_in_Portogallo_definitivo_1.pdf)