---
title: 'All''Onu Vaticano contro risoluzione lgbt con tesi infami'
date: Tue, 22 Mar 2011 17:50:36 +0000
draft: false
tags: [Transnazionale]
---

La Colombia presenta nuovo documento per la depenalizzazione dell'omosessualità sostenuta da 83 paesi. Il Vaticano, all'Onu spesso alleato dei peggiori paesi dittatoriali, è intervenuto per creare confusione riguardo la definizione di “orientamento sessuale” e i comportamenti pedofili e incestuosi.  
  
Ginevra – Roma, 23 marzo 2011

  
Comunicato Stampa dell’Associazione Radicale Certi Diritti

Il cammino per il superamento delle diseguaglianze nei confronti delle persone lesbiche e gay va avanti anche all’Onu. Dopo le proposte di Risoluzione che sullo stesso tema presentate nel 2006 sostenute da 54 Stati e nel 2008 sostenute da 68 Stati, oggi a Ginevra alla Commissione Diritti Umani delle Nazioni Unite è stata depositata, da parte della Colombia, un’altra proposta di Risoluzione, che chiede di mettere fine alle violenze e discriminazioni fondate sull’orientamento sessuale e l’identità di genere delle persone, sostenuta da 83 Stati. Era stata la Francia a sensibilizzare sin dal 2006 i paesi membri e grazie principalmente alla sua azione si è giunti a questo risultato. La delegazione francese ha fatto sapere che  continuerà la sua azione all’Onu per richiedere una depenalizzazione universale dell’omosessualità così come richiesto dalla società civile e dai difensori dei diritti umani in tutto il mondo.  
Durante la presentazione del documento alcuni dei paesi firmatari hanno preannunciato di voler presentare il testo di una Risoluzione di condanna agli  80 paesi del mondo che perseguitano penalmente l’omosessualità.  
Come al solito il Vaticano, tramite il suo rappresentante all’Onu, l’arcivescovo Silvano M. Tomasi non ha perso occasione per inserirsi e ficcare il naso su vicende che non riguardano certo la religione cattolica. Difatti, il gerarca vaticano, rappresentante di uno Stato teocratico, che all’Onu è spesso alleato dei paesi dittatoriali e/o teocratici, è intervenuto sostenendo tesi ridicole e stupide che cercano di alimentare confusione riguardo la definizione di “orientamento sessuale” e i comportamenti pedofili e incestuosi.    
La faccenda è molto seria e nonostante questo i rappresentanti di molte delegazioni non hanno potuto trattenere le risate mentre  ascoltavano e leggevano sbigottiti l’intervento dell’arcivescovo.