---
title: 'VI congresso di Certi Diritti - il programma'
date: Wed, 06 Mar 2013 11:40:22 +0000
draft: false
tags: [Politica]
---

**VERSO L'UGUAGLIANZA**

**Iomimpegno.org contro il proibizionismo sui corpi e sugli affetti**

**il congresso ha il patrocinio del Comune di Napoli  
ed è aperto a tutti  
  
INCONTRI PRECONGRESSUALI  
  
Venerdì 5 aprile 2013**  
Antisala dei Baroni, Maschio Angioino  
  

Ore 16,30 – 19,30  
Incontro dibattito sul tema:  
**“Regolamentare la prostituzione. Come ridare dignità e sicurezza alle persone che scelgono di prostituirsi. Gli interventi locali possibili senza dimenticare la lotta alla criminalità ed alla tratta degli esseri umani”**

Partecipano:**  
Pia Covre**, Comitato per i diritti civili delle prostitute**  
Kemal Ordek**, Attivista europeo per i diritti dei e delle sex workers   
**Andrea Morniroli**, Associazione Dedalus  
**Gigliola Toniollo**, CGIL Nuovi Diritti**  
Coordina Giuseppina Tommasielli**, Assessore alle Pari Opportunità della Città di Napoli

  
Ore 20  
**Benvenuti a Napoli!  
Incontro con aperitivo aperto a tutte e a tutti.  
**Presso **Bubble Six, P.zza S.Maria La Nova 22/23**  
Organizzato in collaborazione con le Associazioni radicali napoletane e il Coordinamento Campania Rainbow. Presenti i rappresentanti delle associazioni LGBTI italiane.  

  
**Sabato 6 aprile ore 2013**  
Antisala dei Baroni, Maschio Angioino  
  

Ore 9,00  
Apertura delle iscrizioni al Congresso

Ore 9,30  
Apertura del Congresso. Presentazione e votazione della proposta di Presidenza, di Regolamento e di Ordine del Giorno.

Ore 10,00  
Relazioni del segretario, del tesoriere e degli organi del PRNTT

Ore 10,30  
Saluto del sindaco di napoli **LUIGI DE MAGISTRIS  
**e di **Vanni Piccolo**, in rappresentanza di UNAR, Ufficio Nazionale Antidiscriminazioni Razziali della Presidenza del Consiglio dei Ministri

Ore 11  
Tavola rotonda su:  
**Dal diritto di famiglia al diritto delle famiglie: la strada verso il matrimonio egualitario**

Introduzione di **Bruno Defilippis**  
Intervengono: **Ivan Scalfarotto** (deputato PD), **Alessandro Zan** (senatore SEL), **Benedetto Della Vedova** (senatore Scelta Civica), **Luis Alberto Orellana** (senatore M5S), **Joël Le Déroff** (ILGA-Europe), **Adrian Trett** (LGBT+ Liberal Democrats), **Ryan J. Davis** (consulente del Presidente Obama per la campagna sul tema della marriage equality).

Ore 14,30  
Ripresa dei lavori congressuali e dibattito generale Ore 14,30

Ore 16,00  
**Valori tradizionali vs Diritti umani: Uganda, Russia ed il resto del mondo tra vecchi e nuovo fondamentalismi**  
Partecipano:  
**Chaouki Khalid**, deputato PD; **Antonio Stango**, Partito Radicale Nonviolento Transnazionale Transpartito**; Renato Sabbadini** (ILGA);  
**Charles Radclife**, Chief, Global Issues Section Office of the UN High Commissioner for Human Rights (OHCHR); **Altin Hazizaj** (1st Ambassador, Pink Embassy); **Olga Lenkova** (Communications manager dell'associazione LGBT "Coming Out" di San Pietroburgo)**;**  
Conclude **Elisabetta Zamparutti**, Associazione Nessuno Tocchi Caino  

Ore 18 – 19,30  
Seguito del dibattito generale

Ore 19,30  
Termine per la presentazione delle proposte di mozione generale e di modifiche statutarie

  
**Domenica 7 aprile 2013  
**Cgil Campania, Via Torino, 16  

Ore 9,30  
Ripresa dei lavori congressuali, continuazione del dibattito generale e interventi esterni

Ore 10,30  
Termine per la presentazione degli emendamenti alla proposta di mozione generale ed alle modifiche statutarie. Termine per la presentazione delle mozioni particolari, della mozione generale, delle mozioni particolari

A seguire:

Presentazione, discussione e votazione di:

•    proposte di emendamento allo statuto  
•    proposte di mozione generale  
•    proposte di mozioni particolari  
•    candidature e votazioni per gli agli organi statutari

Al termine delle operazioni di voto e dopo aver proclamato gli eletti, chiusura del Congresso.

  
Hanno preannunciato la loro presenza al Congresso:

**Sergio Lo Giudice**, Senatore PD  
**Dejanira Piras**, M5S  
**Antonello Sannino**, portavoce del Coordinamento campania Rainbow  
**Marilisa D'Amico**, Consigliere comunale Milano  
**Filomena Gallo**, Segretario Associazione Luca Coscioni  
**Maurizio Turco,** Partito Radicale Nonviolento Transnazionale Transpartito**Flavio Romani,** Segretario nazionale Arcigay  
**Aurelio Mancuso,** Equality Italia**Giordana Curati,** Arcilesbica**  
Paolo Patanè**, Comitato Palermo Pride  
**Carlo Cremona**, IKen Onlus

  

**[SCARICA IL PROGRAMMA IN WORD >](notizie/comunicati-stampa/item/download/32_8fed28b74c5de79ccf4ba93fac024106)  
**

**[LA PAGINA FACEBOOK DELL'EVENTO >](http://www.facebook.com/events/551938604841039/)**

**CONSIGLI PER IL PERNOTTAMENTO:**

**1 OSTELLO WELCOME INN**  
Via Santa Teresa degli Scalzi, 8    
tel. 081.1957 9762  
[www.welcomeinn.it](http://www.welcomeinn.it/)

**2 CORRERA 241 LIFESTYLE HOTEL 3***  
Via Correra, 241    
tel. 081.1956 2842  
[www.correra.it](http://www.correra.it)

**3 HOTEL MAISON DEGAS 3***  
Calata Trinità Maggiore,53  
tel. 081.0608252  
[www.maisondegas.it](http://www.maisondegas.it)

**4 GOLDEN HOTEL 3***  
Via dei Fiorentini, 51  
tel. 081.2514192  
[www.goldenhotel.it](http://www.goldenhotel.it)

**5 B&B I VISCONTI  
**Via Pasquale Scura, 77  
tel. 081.5529124[  
www.napolibandb.it/camere](http://www.napolibandb.it/camere)

  
Parties in città:  
**[www.criminalcandy.com](http://www.criminalcandy.com)**  
[**www.freelovers.it  
www.aperipride.it  
**](http://www.freelovers.it)

Per maggiori informazioni scrivi a [info@certidiritti.it](mailto:info@certidiritti.it)

  
[VI\_congresso\_Certi\_Diritti\_3\_4\_13.doc](http://www.certidiritti.org/wp-content/uploads/2013/03/VI_congresso_Certi_Diritti_3_4_13.doc)