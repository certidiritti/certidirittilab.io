---
title: 'ANCHE CERTI DIRITTI AL BARCAMP: ESPERIMENTI DEMOCRATICI. PER PARTECIPARE...'
date: Wed, 01 Oct 2008 12:50:40 +0000
draft: false
tags: [Comunicati stampa]
---

l'Associazione Radicale Certi Diritti parteciperà al BarCamp 2008 'Esperimenti democratici' nella crisi della (non) Democrazia che si terrà a Roma E che vedrà coinvolte diverse centinaia di perssone da tutta Italia oltre ad Associazioni, blogger, comitati di cittadini e realtà civiche di molte città italiane.

Il tema della relazione che verrà svolta da Sergio Rovasio, Segretario Ass. Certi Diritti tratterà il seguente tema:

**"Diritti civili in Italia: richiesta di aiuto ai paesi dell'Ue per la grave situazione italiana. Idee, proposte e iniziative".**

Il [BarCamp](BarCamp) **Esperimenti democratici** ha come obiettivo quello di affrontare il tema della crisi della (non)democrazia, mettere a confronto esperimenti di iniziative politiche dal basso, proporre nuovi strumenti di partecipazione politica.

Protagonisti dell'evento sono i blogger, gli attivisti, i movimenti, i comitati di cittadini e le organizzazioni civiche che vorranno usare il [BarCamp](BarCamp) per partecipare a un confronto sugli strumenti a disposizione dei cittadini per attivarsi sulle questioni di loro interesse, sulle pratiche partecipative e sulle riforme istituzionali necessarie per rinnovare la democrazia.

Il [DemCamp](DemCamp) "Esperimenti democratici" è parte della conferenza "La crisi della (non)democrazia" promossa da [RadioRadicale.it](http://radioradicale.it/) in partnership con la Fondazione per la cittadinanza attiva che si apre il 3 ottobre con un convegno intitolato "Cittadinanza e partecipazione nella crisi della (non)democrazia" ([Qui il programma completo della conferenza](http://www.radioradicale.it/programma-provvisorio-della-conferenza-esperimenti-democratici))

Tutti i dettagli dell'inizaitiva al seguente link:  [http://barcamp.org/demcamp](http://barcamp.org/demcamp)