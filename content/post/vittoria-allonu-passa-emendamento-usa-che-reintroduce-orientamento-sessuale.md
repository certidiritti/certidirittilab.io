---
title: 'Vittoria all''ONU, passa emendamento Usa che reintroduce orientamento sessuale'
date: Thu, 23 Dec 2010 08:30:43 +0000
draft: false
tags: [Comunicati stampa]
---

**ALL’ONU PASSA EMENDAMENTO USA CHE REINTRODUCE LA CONDANNA PER GLI ATTI DI VIOLENZA EXTRAGIUDIZIARI ANCHE PER MOTIVI DI ODIO PER L’ORIENTAMENTO SESSUALE DELLE VITTIME.**

**COMUNICATO STAMPA DELL'ASSOCIAZIONE RADICALE CERTI DIRITTI:**

Roma, 22 dicembre 2010

Lo scorso 17 novembre all’Onu, su iniziativa di alcuni paesi dittatoriali e teocratici, era stato votato un documento, che deve essere rinnovato ogni due anni, che condannava gli atti di violenza ed esecuzioni extragiudiziali, sommarie ed arbitrarie, determinate dall’odio   per l’orientamento sessuale delle vittime. L'iniziativa di Marocco e Mali, promossa  a nome dei paesi africani e islamici, fece cancellare l’espressione ‘orientamento sessuale’, sostituendola con un più generico ‘ragioni discriminatorie su qualsiasi base’. Il voto passò con 79 favorevoli e 70 contrari.

Ciò avrebbe causato una maggiore impunità per coloro che promuovono in ogni angolo della terra azioni violente contro le persone lesbiche, gay, transessuali e transgender, in particolare laddove l'omosessualita' e' considerata un reato penale e dove tale violenza e' quindi legge dello Stato. E’ evidente che questi paesi si sarebbero sentiti più legittimati a perseguire per legge le persone lgbt, non esistendo una condanna generale dell’Onu alle violenze determinate dall’odio per orientamento sessuale.

Su proposta degli Stati Uniti è stato reintrodotto nel documento l’emendamento che prevede  l’orientamento sessuale tra i casi di condanna della violenza. Hanno votato a favore 93 paesi, 55 hanno votato contro e 27 si sono astenuti.

**Qui di seguito l’elenco dei paesi e come hanno votato:**

In favor of amendment restoring sexual orientation to UNGA resolution on executions (93):

Albania, Andorra, Angola, Antigua-Barbuda, Argentina, Armenia, Australia, Austria, Bahamas, Barbados, Belgium, Belize, Bolivia, Bosnia-Herzegovina, Brazil, Bulgaria, Canada, Cape Verde, Chile, Colombia, Costa Rica, Croatia, Cyprus, Czech Republic, Denmark, Dominica, Dominican Republic, Ecuador, El Salvador, Estonia, Fiji, Finland, France, Georgia, Germany, Greece, Grenada, Guatemala, Honduras, Hungary, Iceland, India, Ireland, Israel, Italy, Japan, Latvia, Liechtenstein, Lithuania, Luxembourg, Malta, Mauritius, Marshall Island, Mexico, Micronesia, Monaco, Montenegro, Nauru, Nepal, Netherlands, New Zealand, Nicaragua, Norway, Palau, Panama, Papua New Guinea, Paraguay, Peru, Poland, Portugal, Republic of Korea, Republic of Moldova, Romania, Rwanda, Saint Kitts and Nevis, Samoa, San Marino, Serbia, Slovakia, Slovenia, South Africa, Spain, Sweden, Switzerland, The Former Yugoslav Republic of Macedonia, Timor-Leste, Tonga, Ukraine, United Kingdom, United States, Uruguay, Vanuatu, Venezuela

Opposed to amendment (55):

Afghanistan, Algeria, Azerbaijan, Bahrain, Bangladesh, Benin, Botswana, Brunei Dar-Sala, Burkina Faso, Burundi, China, Comoros, Congo, Democratic People's Republic of Korea, Democratic Republic of Congo, Djibouti, Egypt, Gambia, Ghana, Indonesia, Iran, Iraq, Jordan, Kazakhstan, Kuwait, Lebanon, Libya, Malawi, Malaysia, Mauritania, Morocco, Namibia, Niger, Nigeria, Oman, Pakistan, Qatar, Russia, Saint Lucia, Saudi Arabia, Senegal, Sierra Leone, Solomon Islands, Somalia, Sudan, Syria, Swaziland, Tajikistan, Tunisia, Uganda, United Arab Emirates, Tanzania, Yemen, Zambia, Zimbabwe

Abstained (27):

Belarus, Bhutan, Cambodia, Eritrea, Guinea, Guinea-Bissau, Guyana, Haiti, Jamaica, Kenya, Lao, Lesotho, Liberia, Maldives, Mali, Mongolia, Mozambique, Philippines, Saint Vincent and the Grenadines, Sao Tome Principe, Singapore, Sri Lanka, Suriname, Thailand, Togo, Trinidad and Tobago, Vietnam

Did not vote/Absent (17):

Cameroon, Central African Republic, Chad, Cote D'Ivoire, Cuba, Equatorial Guinea, Ethiopia, Gabon, Kiribati, Kyrgyzstan, Madagascar, Myanmar, Seychelles, Turkey, Turkmenistan, Tuvalu, Uzbekistan