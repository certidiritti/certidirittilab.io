---
title: 'Unioni civili, delibera non ancora calendarizzata in violazione statuto'
date: Wed, 14 Nov 2012 16:19:16 +0000
draft: false
tags: [Diritto di Famiglia]
---

Dichiarazione di Riccardo Magi, segretario di Radicali Roma, e di Sergio Rovasio, Associazione Radicale Certi Diritti

Roma, 14 novembre 2012  

Sabato 17 novembre scadrà il termine entro il quale, in base allo statuto del comune di Roma, l'assemblea capitolina avrebbe dovuto calendarizzare e discutere la delibera di iniziativa popolare sul "Riconoscimento delle unioni civili e sostegno alle nuove forme familiari", depositata il 17 maggio scorso con le firme di oltre 7000 cittadini romani.

Al momento la discussione della delibera non è inserita nell'ordine dei lavori dell'ultima seduta utile, quella convocata per giovedì 15 novembre, né siamo stati contattati per illustrare la proposta all'assemblea in qualità di comitato promotore, come previsto dallo statuto e dal regolamento.

Ci rivolgiamo dunque al Presidente dell'assemblea, alla conferenza dei capigruppo e a tutti i consiglieri perché si attivino immediatamente per evitare l'ennesima violazione dello statuto e un nuovo colpo all'iniziativa popolare dei cittadini, da parte di un'istituzione che viola le proprie regole fondamentali con vergognosa disinvoltura.

Le migliaia di cittadini che hanno sottoscritto la proposta chiedono che anche la Capitale - al pari di altre città come Torino, Napoli, Milano - si impegni nel garantire parità di accesso a tutte le attività e ai servizi forniti dal comune alle famiglie di fatto e a quelle basate sul matrimonio; e si impegni nella lotta a ogni forma di discriminazione, in piena attuazione della Carta dei diritti fondamentali, della nostra Costituzione e delle pronunce della Consulta e della Cassazione sul diritto alla vita familiare.

La nostra proposta non esula dalle competenze dei comuni e non è anticostituzionale, come si affanneranno a dire gli oppositori. Al contrario, punta a sanare una lesione dei diritti civili, senza danneggiare minimamente le famiglie basata sul matrimonio.

L’iniziativa, lanciata da Radicali Roma e dall’Associazione Radicale Certi Diritti, ha subito riunito come promotori moltissime associazioni e soggetti: Circolo di cultura omosessuale Mario Mieli, Sel Roma Area metropolitana, Forum queer Sel, Uaar Roma, Giovani Idv, Arcigay Roma, Gay Center, Consulta romana per la laicità delle istituzioni, Arcilesbica Roma, Agedo Roma, Roma Rainbow Choir, QueerLab, PianetaQueer.it, Famiglie Arcobaleno, Yellow Sport, Fondazione Massimo Consoli, Gayroma.it, DiGayProject, Luiss Arcobaleno, Gay & Geo gruppo trekking Roma, A.F.F.I. Associazione Federativa Femminista Internazionale della Casa Internazionale delle Donne, Associazione Riprendiamoci la politica , Psi Roma, Pd XV municipio, TILT, Nuova proposta (donne e uomini omosessuali cristiani) e molti altri.