---
title: 'Corte Costituzionale a favore diritti unioni civili. La nuova sentenza ulteriore passo avanti'
date: Fri, 14 Jan 2011 11:13:39 +0000
draft: false
tags: [Comunicati stampa]
---

**LA CORTE COSTITUZIONALE ANCORA A FAVORE DEI DIRITTI DELLE UNIONI CIVILI. IMPORTANTE SENTENZA A FAVORE DELLA LEGGE FINANZIARIA EMILIANA. UN ALTRO PASSO AVANTI VERSO LA PIENA UGUAGLIANZA DI TUTTI.**

Roma, 14 gennaio 2011

La Corte Costituzionale ha annunciato ieri di aver rigettato il ricorso presentato dal Governo contro la legge finanziaria del 2010 della Regione Emilia Romagna con la quale gli interventi di assistenza sociale sono estesi a tutte le forme di famiglia, sulla base della definizione della stessa stabilita dalla legge anagrafica, e quindi di fatto aprendo alle unioni civili.

Quella della Regione Emilia Romagna è stata una scelta semplice ed efficace pe rintrodurre il principio di non discriminazione nell’ordinamento regionale senza attendere leggi nazionali di riconoscimento delle altre forme familiari non matrimoniali.

Dichiarazione di Sergio Rovasio, segretario nazionale dell’Associazione radicale Certi Diritti:

“La sentenza della Corte, dimostra che la strada intrapresa da alcune amministrazioni locali e regionali, di concreto riconoscimento dei diritti per le forme familiari non matrimoniali, è giusta, e dà i suoi frutti. Si deve continuare sul solco tracciato dal Comune di Torino, dalla Regione Emilia Romagna, dai tanti altri Comuni e Province che con iniziative singole (contro l’omofobia, a favore dei diritti delle unioni civili, ecc.) dimostrano che in questo paese le riforme si possono fare, e che sono perfettmente costituzionali e che costituiscono concreti passi avanti verso la piena uguaglianza di tutti e tutte di fronte alla legge. La Corte Costituzionale ha di fatto azzittito gli esponenti del fondamentalismo politico e religioso, anche del Governo, sempre pronti a promuovere odio e discriminazione”.