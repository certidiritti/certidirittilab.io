---
title: 'Vaticano cita l''Onu per spiegare che tra 20 anni metà popolazione sarà gay'
date: Sun, 09 Jan 2011 15:15:34 +0000
draft: false
tags: [20 anni, cardinale, chiesa, Comunicati stampa, GAY, glbt, unesco, vaticano, vescovo]
---

**DELIRI VATICANI: IL CARDINALE  ANTONELLI, PRESIDENTE PONTIFICIO CONSIGLIO FAMIGLIA, AVREBBE DETTO, SECONDO IL VESCOVO DI CORDOVA, CHE “L’UNESCO HA PROGRAMMATO PER I PROSSIMI 20 ANNI CHE LA META’ DELLA POPOLAZIONE MONDIALE SARA’ OMOSESSUALE”. DALLA DENIGRAZIONE ALLA FALSIFICAZIONE, USANDO L'ONU.**

**Dichiarazione di Sergio Rovasio, Segretario dell'Associazione Radicale Certi Diritti:**

“Il Vaticano non si capacita del fatto che esiste l’omosessualità, anzi, che è sempre esistita. Ora scopriamo che durante l’omelia del Vescovo di Cordova presso la chiesa della Sagrada Famiglia di Barcellona, tenuta in pompa magna il 26 dicembre scorso (qui il testo integrale [http://estaticos.elmundo.es/documentos/2011/01/01/homilia.pdf](http://estaticos.elmundo.es/documentos/2011/01/01/homilia.pdf)) lo stesso ha confessato ai fedeli astanti (probabilmente dal bigotto rimasti sbigottiti) che il Cardinale Ennio Antonelli, Presidente del Pontificio Consiglio della Famiglia avrebbe confidato al Vescovo che “l’Unesco ha programmato per i prossimi 20 anni, che la metà della popolazione mondiale sarà omosessuale. Per questo, attraverso distinti programmi, è inserita la materia dell’ideologia di genere, già presente nelle nostre scuole’.

Insomma, siamo passati dalle offese e denigrazioni della chiesa cattolica, nei confronti delle persone lgbte, agli allarmismi deliranti di vescovi e cardinali utilizzando niente po’ po’ di meno che l’Unesco. Ovviamente, come spesso accade, approfittando dell’ignoranza di molte persone.

Consigliamo al Cardinale Antonelli di fare subito una smentita ed evitare così di mettersi nel ridicolo più di quanto si sia già messo. In alternativa gli consigliamo, se almeno si vuole salvare,  di farsi un viaggio sulla navetta spaziale di Star Trek, almeno sarà salva la sua integrità e non rischia di finire nei prossimi 20 anni tra quella metà “peccaminosa”, certamente meno ridicola, della popolazione”.