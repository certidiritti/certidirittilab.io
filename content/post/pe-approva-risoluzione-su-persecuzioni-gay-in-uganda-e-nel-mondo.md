---
title: 'P.E. APPROVA RISOLUZIONE SU PERSECUZIONI GAY IN UGANDA E NEL MONDO'
date: Fri, 18 Dec 2009 14:45:28 +0000
draft: false
tags: [Comunicati stampa]
---

**CERTI DIRITTI SALUTA L'APPROVAZIONE AL PE DI UNA RISOLUZIONE SUI DIRITTI LGBT IN UGANDA E NEL MONDO**

  

Bruxelles, 18 dicembre 2009

  

Certi diritti si felicita per l'approvazione a larghissima maggioranza da parte del PE ieri pomeriggio della risoluzione sulla proposta di legge contro l'omosessualità in Uganda, che chiede alle istituzioni europee di adottare una politica globale sui diritti LGBT nel mondo.

  

La risoluzione chiede alle autorità ugandesi di non approvare la proposta di legge depositata al parlamento, che prevede un'inasprimento delle pene contro gli omosessuali, includendo la pena di morte e l'obbligo di denunciare alla polizia i nomi delle persone LGBT, e di rivedere la legislazione nazionale allo scopo di depenalizzare l'omosessualità.

La risoluzione inoltre prende atto del fatto che in Africa l'omosessualità è legale soltanto in 13 paesi ed è un reato punibile in 38 paesi, tra i quali la Mauritania, il Sudan e la Nigeria settentrionale che prevedono per l'omosessualità addirittura la pena di morte.

Per tale motivo il PE invita il Consiglio e alla Commissione un intervento urgente presso le autorità ugandesi e chiede, qualora la proposta di legge fosse adottata, di riconsiderare l'impegno europeo nei confronti dell'Uganda, proponendo inoltre un altro paese come sede della conferenza di revisione dello Statuto di Roma in programma per il 31 maggio 2010.

Il PE invita Consiglio, Commissione e Stati membri a compiere un'analisi della situazione di tutti i paesi terzi per quanto riguarda le esecuzioni, la criminalizzazione o la discriminazione sulla base dell'orientamento sessuale e di adottare sulla base di tale analisi misure concertate a livello internazionale per promuovere il rispetto dei diritti umani in tali paesi attraverso mezzi opportuni.

Certi Diritti denuncia il fatto che tale legge sia appoggiata dai leader locali delle principali religioni, unite ecumenicamente contro i diritti delle persone LGBT.

Certi diritti si felicita per l'approvazione di tale risoluzione, alla stesura della quale ha collaborato anche Ottavio Marzocchi, in qualità di collaboratore del gruppo ALDE al PE e responsabile per le questioni europee di Certi diritti.

Il testo della risoluzione é disponibile a: [http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P7-TA-2009-0119+0+DOC+XML+V0//IT&language=IT](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P7-TA-2009-0119+0+DOC+XML+V0//IT&language=IT)

segue il testo:

Risoluzione del Parlamento europeo del 17 dicembre 2009 sulla proposta di legge contro l'omosessualità in Uganda

Il Parlamento europeo ,

–   visti gli obblighi e gli strumenti internazionali in materia di diritti umani, compresi quelli previsti dalle convenzioni delle Nazioni unite sui diritti dell'uomo e dalla Convenzione europea per la salvaguardia dei diritti umani e delle libertà fondamentali, che assicurano il rispetto dei diritti umani e delle libertà fondamentali e proibiscono la discriminazione,

–   visto l'accordo di Partenariato tra i membri del gruppo degli stati dell'Africa, dei Caraibi e del Pacifico, da un lato, e la Comunità europea e i suoi Stati membri, dall'altro, firmato a Cotonu il 23 giugno 2000[(1)](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P7-TA-2009-0119+0+DOC+XML+V0//IT&language=IT#def_1_1) (accordo di Cotonou) e le sue disposizioni in materia di diritti umani, in particolare l'articolo 9,

—

–   visti gli articoli 6 e 7 del trattato sull'Unione europea (TUE) e l'articolo 19 del trattato sul funzionamento dell'Unione europea (TFUE), che impegnano l'Unione europea come pure gli Stati membri ad affermare i diritti umani e le libertà fondamentali e prevedono strumenti di lotta contro la discriminazione e le violazioni dei diritti umani a livello di Unione europea,

–   vista la Carta dei diritti fondamentali dell'Unione europea, in particolare l'articolo 21, che proibisce la discriminazione fondata sull'orientamento sessuale,

–   visto il complesso delle attività che l'Unione europea svolge al fine di combattere l'omofobia e la discriminazione fondata sull'orientamento sessuale,

–   viste le sue precedenti risoluzioni sull'omofobia, la tutela delle minoranze e le politiche antidiscriminatorie, in particolare la risoluzione del 18 gennaio 2006 sull'omofobia in Europa[(2)](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P7-TA-2009-0119+0+DOC+XML+V0//IT&language=IT#def_1_2) , quella del 15 giugno 2006 sull'intensificarsi della violenza razzista e omofoba in Europa[(3)](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P7-TA-2009-0119+0+DOC+XML+V0//IT&language=IT#def_1_3) e quella del 26 aprile 2007 sull'omofobia in Europa[(4)](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P7-TA-2009-0119+0+DOC+XML+V0//IT&language=IT#def_1_4) ,

–   vista la riunione della commissione per gli affari politici dell'Assemblea parlamentare paritetica ACP-UE svoltasi a Luanda il 28 novembre 2009,

–   vista la risoluzione dell'Assemblea parlamentare paritetica ACP-UE del 3 dicembre 2009, sull'integrazione sociale e culturale e la partecipazione dei giovani,

–   visto l'articolo 122, paragrafo 5, del suo regolamento,

A.   considerando che il 25 settembre 2009 il deputato David Bahati ha presentato al parlamento ugandese una proposta di legge contro l'omosessualità denominata "Anti Homosexuality Bill 2009",

B.   considerando che la proposta di legge prevede l'introduzione di pene più severe al fine di criminalizzare l'omosessualità e punire con l'ergastolo o la pena di morte le persone ritenute lesbiche, gay, bisessuali o transgenere (LGBT),

C.   considerando che la proposta di legge comprende una disposizione in base alla quale qualsiasi persona, anche eterosessuale, che non segnali entro ventiquattro ore l'identità di tutte le persone lesbiche, gay, bisessuali o transgenere che conosce, o che sostenga i diritti umani di persone lesbiche, gay, bisessuali o transegenere, è passibile di reclusione fino a un massimo di tre anni,

D.   considerando che il progetto di legge in questione prevede l'annullamento, da parte dell'Uganda, di tutti gli impegni internazionali o regionali assunti dal paese che siano considerati in contrasto con le disposizioni di tale progetto di legge,

E.   considerando che la legge è già stata condannata dal Commissario europeo De Gucht, dai governi britannico, francese e svedese, come pure dal Presidente degli Stati Uniti Obama e dal presidente e dal vicepresidente della commissione per gli affari esteri della Camera dei rappresentanti degli Stati Uniti,

F.   considerando che il progetto di legge è stato denunciato da organizzazioni non governative di tutto il mondo e nella stessa Uganda come un grande ostacolo alla lotta contro l'HIV e l'AIDS nella comunità omosessuale,

G.   considerando che in Africa l'omosessualità è legale soltanto in 13 paesi ed è un reato punibile in 38 paesi, tra i quali la Mauritania, il Sudan e la Nigeria settentrionale che prevedono per l'omosessualità addirittura la pena di morte, e che l'approvazione di una legge siffatta in Uganda potrebbe avere effetti a cascata su altri paesi africani, nei quali le persone sono o potrebbero essere perseguitate in ragione del loro orientamento sessuale,

1.   sottolinea che l'orientamento sessuale è una questione che rientra nella sfera del diritto individuale alla vita privata, garantito dalla legislazione internazionale in materia di diritti umani, secondo cui l'uguaglianza e la non discriminazione dovrebbero essere promosse e la libertà di espressione garantita; condanna pertanto la proposta di legge del 2009 contro l'omosessualità;

2.   chiede quindi alle autorità ugandesi di non approvare la proposta di legge e di rivedere la legislazione nazionale allo scopo di depenalizzare l'omosessualità;

3.   ricorda al governo ugandese i suoi obblighi secondo il diritto internazionale e l'accordo di Cotonou, che invita al rispetto dei diritti umani universali;

4.   ricorda le dichiarazioni della Commissione africana per i diritti dell'uomo e della Commissione delle Nazioni Unite per i diritti umani secondo cui uno Stato non può, attraverso la legislazione nazionale, venir meno agli obblighi internazionali assunti in materia di diritti dell'uomo;

5.   è estremamente preoccupato per l'eventualità che i donatori internazionali e le organizzazioni non governative e umanitarie debbano riconsiderare o cessare le loro attività in determinati settori se la proposta diventerà legge;

6.   respinge fermamente qualsiasi iniziativa volta all'introduzione della pena di morte;

7.   chiede al Consiglio e alla Commissione un intervento urgente presso le autorità ugandesi e, qualora la proposta di legge fosse adottata e si verificassero violazioni della legislazione internazionale in materia di diritti umani, di riconsiderare l'impegno nei confronti dell'Uganda, proponendo inoltre un altro paese come sede della conferenza di revisione dello Statuto di Roma in programma per il 31 maggio 2010;

8.   chiede al Consiglio, alla Commissione e agli Stati membri di analizzare la situazione nei paesi terzi per quanto riguarda le esecuzioni, la criminalizzazione o la discriminazione sulla base dell'orientamento sessuale e di adottare misure concertate a livello internazionale per promuovere il rispetto dei diritti umani in tali paesi attraverso mezzi opportuni, inclusa la collaborazione con le organizzazioni non governative locali;

9.   incarica il suo Presidente di trasmettere la presente risoluzione al Consiglio, alla Commissione, ai governi e ai parlamenti degli Stati membri, al Presidente della Repubblica dell'Uganda e al Presidente del parlamento ugandese.

![](http://mail.google.com/mail/?ui=2&ik=76949319fc&view=att&th=125a2244c2210f48&attid=0.0.1&disp=emb&zw)

[(1)](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P7-TA-2009-0119+0+DOC+XML+V0//IT&language=IT#ref_1_1)

GU L 317 del 15.12.2000, pag. 3.

[(2)](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P7-TA-2009-0119+0+DOC+XML+V0//IT&language=IT#ref_1_2)

GU C 287 E del 24.11.2006, pag. 179.

[(3)](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P7-TA-2009-0119+0+DOC+XML+V0//IT&language=IT#ref_1_3)

GU C 300 E del 9.12.2006, pag. 491.

[(4)](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P7-TA-2009-0119+0+DOC+XML+V0//IT&language=IT#ref_1_4)

GU C 74 E del 20.3.2008, pag. 776.