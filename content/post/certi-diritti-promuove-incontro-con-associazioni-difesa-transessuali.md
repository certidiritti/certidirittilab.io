---
title: 'CERTI DIRITTI PROMUOVE INCONTRO CON ASSOCIAZIONI DIFESA TRANSESSUALI'
date: Sat, 02 Jan 2010 14:45:14 +0000
draft: false
tags: [Comunicati stampa]
---

EMERGENZA VIOLENZA CONTRO TRANSESSUALI: GIOVEDI’ 7 GENNAIO INCONTRO A ROMA DELLE ASSOCIAZIONI PER LA DIFESA DEI DIRITTI E DELLA DIGNITA’ DELLE PERSONE TRANSESSUALI.  PROPOSTA DI PIANO DI AZIONE COMUNE.

Promosso dall’Associazione Radicale Certi Diritti, si svolgerà giovedì 7 gennaio 2010, dalle ore 14,30,  presso la sede dei radicali, Via di Torre Argentina 76, un incontro tra le Associazioni che si battono per la difesa dei diritti e della dignità delle persone transessuali per concordare un piano di azione comune rivolto alle istituzioni, ai media e alla società civile finalizzato al superamento della grave situazione di emergenza di cui sono vittime le transessuali.

**Tra i punti che verranno discussi:**

\- Richiesta di incontro urgente con il Ministro degli Interni;

\- Lettera-appello agli editori e direttori dei media italiani riguardo la grave distorsione che viene data all’immagine delle persone transessuali;

\- Richiesta di incontro con la Presidente della Commissione Giustizia della Camera dei deputati per la calendarizzazione delle Proposte di Legge 1066 e 1433 per maggiori tutele delle persone transessuali e per la proposta di legge (2802 e 2807) contro l’omofobia e la transfobia tornata in discussione;

\- Testo da diffondere agli addetti ai servizi sanitari, sociali e di Pubblica sicurezza (Vigili Urbani, Carabinieri, Polizia di Stato, Polizia penitienziaria) riguardo migliori e adeguati trattamenti nei confronti delle persone transessuali;

\- Documento – linee guida da proporre e/o concordare con Governo, Sindacati e Associazioni riguardo l’inserimento e la lotta alle discriminazioni nel mondo del lavoro delle persone transessuali.

L’incontro è aperto alle Associazioni, a tutte/i coloro che si trovano d’accordo ad avviare azioni specifiche comuni.

All’incontro hanno per ora confermato la loro presenza:

Leila Deianis – Associazione Libellula,

Rita Bernardini, deputata radicale; Roma;

Maria Gigliola Toniollo – Cgil Nuovi Diritti;

Porpora Marcasciano, Movimento Identità Transessuali;

Paola Concia, deputata Pd;

Avv. Federica Pezzoli, free lance;

Deborah Orlandini, Circolo Pier Paolo Pasolini – Lecce;

Darianna Saccomani, membro staff organizzativo Congresso italiano Transessuali e Transegender;

Imma Battaglia, Presidente DiGayProject;

Mirella Izzo, Presidente Associazione Crisalide PanGender;

Gruppo EveryOne;

Ivan Scalfarotto, Vice Presidente Assemblea Nazionale Partito Democratico;

Savero Aversa, Queer.SEL, per la cultura delle differenze di Sinistra Ecologia e Libertà;

**Per preannunci di partecipazione e/o comunicazioni:**

[info@certidiritti.it](mailto:info@certidiritti.it)