---
title: 'Lo stato delle minoranze sessuali in Uganda dopo la morte dell’attivista per i diritti umani David Kato'
date: Sat, 03 Dec 2011 15:30:34 +0000
draft: false
tags: [Africa]
---

 Intervento di Francis Onyanga, avvocato di David Kato Kisule, al V congresso nazionale dell'Associazione Radicale Certi Diritti.

Milano, 3 dicembre 2011.

  

Sono molto onorato e mi sento privilegiato per il fatto di essere qui con voi oggi a fare un breve discorso sulla situazione delle minoranze sessuali in Uganda dopo la morte dell’attivista per i diritti umani, David Kato. Ho incontrato David per la prima volta durante la conferenza ICC a Kampala. Mi è stato presentato da Elio che allora lavorava per “Non c’è pace senza giustizia”. David stava cercando degli avvocati non omofobi per rappresentare la comunità LGBTI nei tribunali.

Come molti di voi sapranno, David Kato venne ucciso nel gennaio di quest’anno nella sua abitazione nel distretto di Mukono a 20 km a est della capitale ugandese, Kampala.

Quando venne ucciso, Kato era il rappresentante ufficiale di Sexal Minorities Uganda, un’associazione ombrello di omosessuali che lavorava anche per il raggiungimento della piena eguaglianza legale e sociale delle minoranze sessuali.

Nel suo ruolo, Kato lavorò in un ambiente estremamente ostile, ma nonostante tutto, divenne coraggiosamente il volto pubblico degli omosessuali in un paese omofobo, sacrificando così la sua sicurezza personale per una causa nobile.

Il suo sacrificio è testimonianza della sua convinzione, passione, energia e interesse genuino nella lotta per i diritti delle minoranze sessuali in Uganda.

Non ho dubbi che la risposta internazionale al suo omicidio brutale, inclusa quella di eminenti leader globali come Barack Obama, è stato un riconoscimento del ruolo cruciale che Kato ha giocato nel portare nell’agenda locale e internazionale lo stato delle minoranze sessuali in Uganda. 

In novembre, una Corte incarcerò per 30 anni un uomo chiamato Enock Nsubuga che confessò l’omicidio di Kato.

La reclusione fu un indicatore che lo stato ugandese è interessato a proteggere i suoi cittadini a prescindere dal loro orientamento sessuale e che non condona l’omicidio.

Sono stato il legale di Kato per alcune delle battaglie legali che stava combattendo a favore delle minoranze sessuali in Uganda. Come avvocato per i diritti umani, credo fermamente che combattesse genuinamente per la causa chiaramente espressa nel preambolo della Dichiarazione della Nazioni Unite sui diritti umani: “gli eguali ed inalienabili diritti di tutti i membri della famiglia umana sono alla base della libertà, della giustizia, della pace nel mondo”.

Ho rappresentato Kato nei tribunali e al Consiglio ugandese dei media, organo regolatore dei media in Uganda.

Quando The Rolling Stone, un giornale locale ugandese, pubblicò immagini di persone ritenute gay, dando indicazioni di dove vivevano, Kato presentò un protesta formale al Consiglio ugandese dei media al fine di impedire a The Rolling Stone l’ulteriore pubblicazione di materiale che metteva in pericolo la vita delle minoranze sessuali. Tristemente, egli fu ucciso prima che il caso potesse concludersi.

La morte di Kato deprivò il gruppo delle minoranze sessuali in Uganda del suo volto pubblico e di un leader coraggioso in un momento in cui ne aveva ancora bisogno.

Adesso, mentre parlo, una proposta di legge che tenta di imporre la pena di morte per alcuni atti omosessuali non è ancora stata ritirata dal parlamento ugandese, nonostante le proteste internazionali contro di essa. Il pubblico ugandese è ancora generalmente ostile ai dirtti delle minoranze sessuali. Per queste ragioni gay, lesbiche e transessuali vivono ancora in un’atmosfera d’incertezza e paura, nonostante siano cittadini che per la legge hanno gli stessi diritti degli altri.

Ci vorrà uno sforzo concentrico delle organizzazioni della società civile, degli avvocati per i diritti umani, della comunità internazionale, della leadership progressista ugandese, di gruppi e individui affinché i diritti delle minoranze sessuali in Uganda vengano riconosciuti e accettati. 

Grazie molte.

Francis Onyango