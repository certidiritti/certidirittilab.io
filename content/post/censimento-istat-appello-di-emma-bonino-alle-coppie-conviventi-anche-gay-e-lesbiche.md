---
title: 'Censimento Istat: appello di Emma Bonino alle coppie conviventi, anche gay e lesbiche'
date: Wed, 05 Oct 2011 12:39:54 +0000
draft: false
tags: [censimento, coppie gay, emma bonino, istat, Movimento LGBTI]
---

La Vicepresidente del Senato in un video invita tutte le coppie gay e lesbiche conviventi affinché si dichiarino come tali nel questionario del Censimento Istat 2011.

"In molti accusano - credo giustamente - la politica di ipocrisia. Io volevo dire a tutti di non avere paura: chi è convivente scriva che è convivente; e se è convivente con persone delle stesso sesso, lo scriva".  
E' questo l'appello della Vicepresidente del Senato Emma Bonino per il Censimento Istat 2011, che per la prima volta conterà anche le coppie gay.

**Guarda il video >**  
[http://www.gay.it/channel/attualita/32512/Emma-Bonino-testimonial-del-censimento-coppie-gay.html](http://www.gay.it/channel/attualita/32512/Emma-Bonino-testimonial-del-censimento-coppie-gay.html)

**La GUIDA per la corretta compilazione dei moduli Istat >**  
[http://www.gay.it/faicontareiltuoamore/](http://www.gay.it/faicontareiltuoamore/)

  
**EMMA BONINO E' ISCRITTA A CERTI DIRITTI, E TU?**  
[http://www.certidiritti.it/iscriviti](iscriviti)