---
title: 'Parlamento ugandese rinvia voto su legge antigay'
date: Sat, 14 May 2011 08:41:23 +0000
draft: false
tags: [Africa]
---

**La Commissione aveva appena votato una risoluzione in cui raccomandava che l'aula votasse il provvedimento che contemplava anche la pena di morte per gli omosessuali. Dedichiamo questa giornata a David Kato Kisule.**  
  
**Roma, 13 maggio 2011**  
  
Comunicato Stampa dell’ Associazione Radicale Certi Diritti, Non c’è Pace Senza Giustizia,  Arcigay, Nessuno Tocchi Caino, Gruppo EveryOne, Cgil Nuovi Diritti   
  
Abbiamo appena saputo da Kampala -  Uganda  che il Parlamento, nella sua ultima seduta,  ha deciso di non votare la legge contro le persone omosessuali rinviando la discussione a dopo le elezioni.  La Commissione affari legali aveva licenziato l’altro ieri sera il provvedimento anti-gay chiedendo con una Risoluzione che venisse applicata la pena di morte, che solo alcuni giorni fa era stata ‘sostituita’ con l’ergastolo, per chi avrebbe commesso il reato di omosessualità.  
  
Le Ong che operano a Kampala e che erano pronte ad aiutare e proteggere gli esponenti della comunità gay ugandese, hanno inviato messaggi di ringraziamento per la straordinaria mobilitazione avvenuta in tutto il mondo contro questa assurda proposta di legge del 2009 e che i promotori volevano far approvare  prima della chiusura del Parlamento.  
  
In pochissimi giorni centiniaia di migliaia di cittadini di decine di paesi di tutto il mondo, si sono mobilitati con petizioni e iniziative che chiedevano  di bloccare il Bill anti-gay; molte  organizzazioni umanitarie si erano mobilitate per sollecitare i Governi a intervenire sul Presidente ugandese Yoweri Museveni per non firmare il provvedimento, qualora fosse stato approvato, così come avevano già fatto gli Stati Uniti che avevano minacciato un embargo internazionale contro l’Uganda.  
  
Il nostro impegno e quello di tutti coloro che hanno a cuore la promozione e la difesa dei diritti civili e umani anche in Uganda, continua. Non possiamo non dedicare questa buona notizia al nostro compagno e amico David Kato Kisule, ucciso dal fanatismo e dall’odio del fondamentalismo religioso, impegnato lui stesso in prima fila contro questo ignobile provvedimento legislativo.