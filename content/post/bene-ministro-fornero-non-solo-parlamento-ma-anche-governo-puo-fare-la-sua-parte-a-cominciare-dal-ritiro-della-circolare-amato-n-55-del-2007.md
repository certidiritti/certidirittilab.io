---
title: 'Bene Ministro Fornero, non solo Parlamento ma anche Governo può fare la sua parte, a cominciare dal ritiro della Circolare Amato n. 55 del 2007'
date: Tue, 27 Mar 2012 15:03:33 +0000
draft: false
tags: [Politica]
---

Roma, 27 marzo 2012  
Comunicato Stampa dell’Associazione Radicale Certi Diritti

“Il Ministro Fornero nella lettera a Vanity Fair si impegna nella lotta alle discriminazioni e sottolinea l’importante lavoro fin qui svolto dal suo dicastero che comprende anche le Pari Opportunità.

Non è vero però che spetta solo al Parlamento occuparsi di questioni relative ai diritti delle coppie conviventi. Il Governo potrebbe da subito ritirare la Circolare Amato n.55 del 18 ottobre 2007  (Protocollo n. 15100/397/0009861), con oggetto: «Matrimoni contratti all’estero tra persone dello stesso sesso. Estratti plurilingue di atti dello stato civile»  che dice che i Comuni non possono trascrivere i matrimoni delle coppie gay sposate all’estero. 

La Circolare Amato motiva questo adducendo ragioni di ‘ordine pubblico’, peraltro materia questa non applicabile al diritto di famiglia, come già ribadito in più occasioni dalla Corte di Cassazione.  Già questo sarebbe un grande passo del nostro Governo nel cammino della lotta alle discriminazioni e per il superamento delle diseguaglianze; ciò diventa sempre più urgente  anche alla luce della Sentenza della Corte Costituzionale 138/2010, della sentenza della Corte di Cassazione n. 4184 del 15 marzo 2012 e della sentenza del Tribunale di Reggio Emilia del 13 febbraio 2012.

Ricordiamo al Ministro che l’articolo 21 della Carta dei diritti fondamentali dell’Unione europea vieta ogni forma di discriminazione fondata sull’orientamento sessuale e l’articolo 9 della stessa Carta, in sintonia con la nostra Costituzione, stabilisce che la disciplina legislativa di ogni Stato può modulare variamente le modalità di esercizio dei distinti diritti di sposarsi e di costituire una famiglia, ma non in forme tali che possano portare alla vanificazione dell’uno o dell’altro.