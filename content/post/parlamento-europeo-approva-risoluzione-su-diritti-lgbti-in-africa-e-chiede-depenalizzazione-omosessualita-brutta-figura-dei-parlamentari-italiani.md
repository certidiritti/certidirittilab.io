---
title: 'Parlamento Europeo approva Risoluzione su diritti Lgbti in Africa e chiede depenalizzazione omosessualità. Brutta figura dei parlamentari italiani'
date: Fri, 06 Jul 2012 07:02:39 +0000
draft: false
tags: [Africa]
---

La risoluzione condanna la violenza e le discriminazioni e sollecita la depenalizzazione dell'omosessualit. Fondamentalisti del Ppe impongono il ritiro della firma del gruppo. La non bella figura degli italiani.

Bruxelles-Roma, 6 luglio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Il Parlamento Europeo ha ieri approvato una Risoluzione d’urgenza cofirmata dai gruppi socialista, liberale, verde, comunista e conservatore-riformista sulla violenza contro le donne lesbiche e sui diritti di lesbiche, gay, bisessuali, transgender e intersessuali (LGBTI) in Africa.  
La risoluzione era stata inizialmente co-firmata anche dal gruppo PPE, che ha deciso successivamente di ritirare la firma e di dare indicazione di voto contrario a seguito di una riunione nel corso della quale i deputati più conservatori e fondamentalisti hanno prevalso.  
La risoluzione é stata approvata con 53 voti favorevoli, 24 contrari e 4 astenuti. I pochissimi deputati italiani che hanno partecipato al voto si sono divisi tra contrari ed astenuti: Mario Mauro (capodelegazione del PdL al PE) e Silvestris (PPE) hanno votato contro, mentre Angelilli (PPE, vice-presidente del PE) si é astenuta.  
Il testo della Risoluzione condanna la situazione di violenza, discriminazione e stigmatizzazione delle persone LGBTI in Africa, in particolare la pratica degli ‘stupri correttivi’ sulle donne lesbiche, chiede la depenalizzazione mondiale dell’omosessualità, la fine delle persecuzioni e condanna il ruolo dei gruppi religiosi fondamentalisti nell’alimentare odio e violenza.  La situazione in Camerun, Sud Africa, Liberia, Malawi, Uganda, Nigeria viene indicata come particolarmente preoccupante.  Il Parlamento Europeo chiede all’Unione Europea di continuare a sostenere l’azione delle ONG per i diritti delle persone LGBTI e di fare pressione sugli Stati africani affinché siano interrotte violenze, discriminazioni e persecuzioni.  
L’Associazione Radicale  Certi Diritti ringrazia i gruppi politici e i parlamentari europei per l’approvazione della Risoluzione, che rappresenta un richiamo importante per l’affermazione dei diritti civili in Africa e nel resto del mondo. L’impegno del Parlamento Europeo è importante anche per la momoria di David Kato Kisule, militante dei diritti LGBTI in Uganda,  ucciso poche settimane dopo aver partecipato ad una riunione del Parlamento Europeo. Occorre rimarcare la solita non bella figura di molti parlamentari europei italiani per questo importante voto: tra  divisioni, assenze e contrarietà non hanno certo dimostrato attenzione al tema dei diritti civili e umani.

Il testo della risoluzione:  
  
**Risoluzione del Parlamento europeo sulla violenza contro le donne lesbiche e sui diritti di lesbiche, gay, bisessuali, transgender e intersessuali (LGBTI) in Africa (2012/2701(RSP))**

Il Parlamento europeo,

–   viste la Dichiarazione universale dei diritti dell'uomo (UDHR), la Convenzione internazionale sui diritti civili e politici (ICCPR) e la Carta africana dei diritti dell'uomo e dei popoli (ACHPR),

–   viste la Convenzione sull'eliminazione di ogni forma di discriminazione nei confronti della donna (CEDAW) e la piattaforma d'azione di Pechino, che sottolineano il diritto di tutte le donne a esercitare il controllo sulle questioni relative alla loro sessualità e decidere liberamente e in modo responsabile in merito, senza coercizioni, stigmatizzazioni e violenze,

–   viste la risoluzione del Consiglio dei diritti dell'uomo delle Nazioni Unite A/HRC/17/19, del 17 giugno 2011, sui diritti umani, l'orientamento sessuale e l'identità di genere, e la relazione dell'alto commissario delle Nazioni Unite per i diritti umani, del 17 novembre 2011, sulle leggi e pratiche discriminatorie e gli atti di violenza contro singoli individui fondati sull'orientamento sessuale e l'identità di genere,

–   vista la discussione a livello di esperti in seno al Consiglio dei diritti dell'uomo delle Nazioni Unite in merito ai diritti umani, all'orientamento sessuale e all'identità di genere, del 7 marzo 2012,

–   vista la dichiarazione di Navanethem Pillay, alto commissario per i diritti umani delle Nazioni Unite, al gruppo di esperti in materia di diritti umani, orientamento sessuale e identità di genere, in occasione della 19a sessione del Consiglio delle Nazioni Unite per i diritti umani tenutasi il 7 marzo 2012,

–   vista la relazione annuale 2012 di Amnesty International sulla situazione dei diritti umani nel mondo, secondo cui l'intolleranza nei confronti di lesbiche, gay, bisessuali e transgender (LGBT) è aumentata in Africa,

–   viste la seconda revisione dell'accordo di partenariato tra i membri del gruppo degli Stati dell'Africa, dei Caraibi e del Pacifico, da un lato, e la Comunità europea e i suoi Stati membri, dall'altro (accordo di Cotonou), e le clausole sui diritti umani ivi contenute, in particolare l'articolo 8, paragrafo 4, e l'articolo 9,

–   visti l'articolo 2, l'articolo 3, paragrafo 5, e l'articolo 21 del trattato sull'Unione europea (TUE) e l'articolo 10 del trattato sul funzionamento dell'Unione europea (TFUE), che sanciscono l'impegno dell'Unione europea e degli Stati membri a favore della difesa e della promozione dei diritti umani universali nonché della tutela dei singoli individui, nell'ambito delle loro relazioni con il resto del mondo,

–   visto il piano d'azione dell'Unione europea sulla parità di genere e l'emancipazione delle donne nello sviluppo (2010-2015),

–   viste le dichiarazioni del vicepresidente della Commissione/alto rappresentante dell'Unione per gli affari esteri e la politica di sicurezza e del presidente del Parlamento europeo in occasione della giornata internazionale contro l'omofobia nel 2010, 2011 e 2012,

–   visto lo strumentario per la promozione e la tutela dell'esercizio di tutti i diritti umani da parte di lesbiche, gay, bisessuali e transgender (LGBT) adottato dal Consiglio (strumentario LGBT),

–   viste la proposta di regolamento del Parlamento europeo e del Consiglio che istituisce uno strumento per il finanziamento della cooperazione allo sviluppo, presentata dalla Commissione il 7 dicembre 2011 (COM(2011)0840), e la comunicazione della Commissione, del 13 ottobre 2011, dal titolo "Potenziare l'impatto della politica di sviluppo dell'Unione europea: un programma di cambiamento" (COM(2011)0637),

–   viste le sue risoluzioni del 17 dicembre 2009 sulla proposta di legge contro l'omosessualità in Uganda(1), del 16 dicembre 2010 sulla cosiddetta "legge Bahati" e la discriminazione nei confronti della popolazione LGBT in Uganda(2), del 17 febbraio 2011 sull'uccisione di David Kato in Uganda(3) e del 28 settembre 2011 sui diritti umani, l'orientamento sessuale e l'identità di genere nel quadro delle Nazioni Unite(4),

–   vista la sua risoluzione del 7 maggio 2009 sull'integrazione della dimensione di genere nelle relazioni esterne dell'Unione europea e nel consolidamento della pace/dello Stato(5),

–   visti l'articolo 122, paragrafo 5, e l'articolo 110, paragrafo 4 del suo regolamento,

A. considerando che tutti gli esseri umani nascono liberi ed eguali in dignità e diritti; che tutti gli Stati hanno l'obbligo di prevenire la violenza e l'incitamento all'odio fondati sull'orientamento sessuale, l'identità di genere e l'espressione di genere nonché di rispettare i principi di parità tra donne e uomini;

B.  considerando che le donne lesbiche, bisessuali, transgender e intersessuali godono dei medesimi diritti di tutte le altre donne e di tutti gli altri uomini, e che tali diritti vanno tutelati a prescindere dall'orientamento sessuale, dall'identità di genere o dall'espressione di genere;

C. considerando che alcuni paesi africani hanno svolto un ruolo di primo piano nelle azioni volte a difendere i diritti umani e le libertà fondamentali; che la costituzione post-apartheid del Sud Africa è stata la prima al mondo a vietare la discriminazione basata sull'orientamento sessuale e che il Sud Africa è stato il promotore della risoluzione del Consiglio dei diritti dell'uomo delle Nazioni Unite 17/19 sui diritti umani, l'orientamento sessuale e l'identità di genere;

D. considerando che vi sono leader e movimenti politici in grado di guidare il processo verso il cambiamento e il rafforzamento dei diritti umani e di quelli delle donne, nonché di lesbiche, gay, bisessuali, transgender e intersessuali (LGBTI) in Africa;

E.  considerando la crescente stigmatizzazione e violenza contro le donne lesbiche, bisessuali, transgender e intersessuali, nonché contro le donne percepite come tali, da parte delle forze pubbliche e di polizia, delle famiglie delle interessate e dei membri della comunità in Africa, ovvero un fenomeno che costituisce una preoccupazione condivisa come testimoniano le numerose dichiarazioni del Segretario generale delle Nazioni Unite, Ban Ki Moon, e dell'alto commissario per i diritti umani delle Nazioni Unite, Navanethem Pillay, nonché la risoluzione 17/19, adottata dal Consiglio per i diritti dell'uomo delle Nazioni Unite il 17 giugno 2011, sui diritti umani, l'orientamento sessuale e l'identità di genere;

F.  considerando che in occasione della discussione annuale sulle donne impegnate nella difesa dei diritti umani, tenutasi il 25 e 26 giugno 2012 in seno al Consiglio delle Nazioni Unite per i diritti dell'uomo, Margret Sekaggya, relatore speciale delle Nazioni Unite sui difensori dei diritti umani, ha sottolineato che le violazioni nei confronti delle donne impegnate nella difesa dei diritti umani assumono una forma specifica legata al genere e spaziano dall'abuso verbale basato sul genere all'abuso sessuale e allo stupro; che le donne sono considerate come oppositrici delle norme sociali consolidate, della cultura o delle tradizioni oppure delle prescrizioni religiose, e di conseguenza sono stigmatizzate; che le donne impegnate nella difesa dei diritti umani necessitano di un'attenzione particolare poiché le sofferenze alle quali sono sottoposte nel loro lavoro in alcuni casi superano quelle dei colleghi uomini;

G. considerando che le donne che violano le norme sociali e culturali rischiano di essere etichettate come lesbiche e quindi di cadere vittime di comportamenti violenti da parte di uomini e/o di trattamenti degradanti e che ciò ha l'effetto di reprimere l'espressione della sessualità e la libertà di scelta di tutte le donne, anche eterosessuali; che i diritti in materia di sessualità sono legati all'autonomia fisica e alla libertà di scelta di tutte le donne;

H. considerando che in Africa l'omosessualità femminile è legale in 27 paesi e illegale in altri 27, che l'omosessualità maschile è legale in 16 paesi e illegale in 38, che l'omosessualità è punita con la pena di morte in Mauritania e Sudan nonché in parti della Somalia e della Nigeria, e che un deputato ha presentato al parlamento ugandese un disegno di legge che prevede la pena di morte per l'omosessualità;

I.   considerando che le leggi che definiscono reato le relazioni tra persone dello stesso sesso e l'omosessualità contribuiscono a creare un clima che favorisce la violenza contro le donne lesbiche o considerate tali;

J.   considerando che si registrano in tutte le regioni del mondo uccisioni, torture, detenzioni, violenze, stigmatizzazioni e incitazioni all'odio nei confronti delle persone LGBTI, talvolta legittimate dalla legge; che si sono verificati ripetuti atti di violenza e aggressione contro le lesbiche in diversi paesi africani;

K. considerando che la lotta per l'uguaglianza e la giustizia nonché per la visibilità e i diritti delle lesbiche è strettamente legata alla lotta globale a favore dei diritti umani delle donne; che anche le lesbiche, al pari di molte altre donne, subiscono violenze, non solo in quanto donne ma anche in ragione del loro orientamento sessuale;

L.  considerando che nel febbraio 2012 in Camerun dieci donne sono state arrestate e altre tre sono state accusate per la prima volta per comportamenti omosessuali; che gli arresti e i pestaggi da parte della polizia proseguono e che l'ultimo caso documentato risale al 24 giugno 2012; che l'avvocato Alice Nkom ha subito in numerose occasioni minacce di morte e di violenza per aver difeso persone accusate di omosessualità; che una riunione di persone LGBTI a Yaoundé è stata violentemente interrotta da una banda il 19 maggio 2012;

M. considerando che il senato della Liberia sta attualmente discutendo una proposta per estendere ulteriormente il divieto imposto sulle relazioni omosessuali previsto dalla legislazione in vigore; che aumentano le intimidazioni da parte dei media e del pubblico nei confronti delle persone LGBTI; che uomini armati hanno recentemente attaccato due donne lesbiche in Liberia;

N. considerando che in Malawi l'omosessualità femminile è stata vietata per la prima volta nel gennaio 2011; che il nuovo presidente Joyce Banda ha dichiarato l'intenzione di invitare il parlamento ad abrogare le leggi che definiscono reato l'omosessualità;

O. considerando che in Nigeria un disegno di legge per il divieto dei matrimoni omosessuali intende definire reato la registrazione, il funzionamento e la sussistenza di determinate organizzazioni nonché i relativi incontri o cortei, vietando così attività che rientrano strettamente nella sfera privata; che il dibattito in merito a tale disegno di legge ha contribuito ad aumentare le tensioni e le minacce nei confronti delle persone LGBTI;

P.  considerando che in Sud Africa i cosiddetti "stupri correttivi" di lesbiche e donne transgender non accennano a diminuire; che le discussioni sulla tutela costituzionale delle vittime di discriminazioni fondate sull'orientamento sessuale stanno fomentando le violenze nei confronti delle persone LGBTI; considerando che l'attivista gay Thapelo Makutle è stato recentemente torturato e ucciso, che la ventiduenne lesbica Phumeza Nkolonzi ha ricevuto un colpo di arma da fuoco alla testa in ragione del suo orientamento sessuale e che Neil Daniels è stato pugnalato, mutilato e arso vivo in quanto gay;

Q. considerando che lo Swaziland si sta impegnando attivamente per la prevenzione e la cura dell'HIV/AIDS presso le categorie a rischio, anche per quanto concerne le donne, e gli uomini che hanno rapporti sessuali con altri uomini, nonostante nel paese l'omosessualità sia considerata reato;

R.  considerando che in Uganda, nel febbraio e nel giugno 2012, le forze di polizia e il ministero dell'Etica e dell'integrità hanno interrotto riunioni private di attivisti dei diritti umani senza mandato e in violazione della libertà di riunione dei cittadini; che il ministro prevede la messa al bando di 38 organizzazioni ritenute attive nel settore dei diritti umani delle persone LGBTI; che il disegno di legge contro l'omosessualità presentato nel 2009 è tuttora in fase di discussione e che lo stesso potrebbe comprendere disposizioni inaccettabili tra cui la pena di morte; che dai processi e dalle indagini condotti in Uganda e negli Stati Uniti è emerso il ruolo svolto, tra gli altri, da Scott Lively e da Abiding Truth Ministries, un gruppo fondamentalista evangelico statunitense, nell'ambito non solo della diffusione dell'odio e dell'intolleranza fondati sull'orientamento sessuale ma anche dell'introduzione della legge;

Discriminazioni e violenze nei confronti delle lesbiche in Africa

1.  condanna in maniera decisa qualunque forma di violenza e discriminazione contro le lesbiche nei paesi africani interessati dal fenomeno, ivi incluse le manifestazioni estreme come gli stupri "correttivi" e le altre forme di violenza sessuale;

2.  esprime il proprio deciso sostegno per le campagne e le iniziative finalizzate all'abolizione di tutte le leggi discriminatorie nei confronti delle donne e delle persone LGBTI; invita i paesi africani dove tuttora sono in vigore leggi discriminatorie ad abrogarle immediatamente, anche per quanto concerne quelle che vietano l'omosessualità o che legittimano le discriminazioni nei confronti delle donne dal punto di vista dei diritti in materia di stato civile, proprietà e successione;

3.  conferma che la lotta per i diritti fondamentali e i diritti umani delle lesbiche in Africa è strettamente connessa alla tutela della salute e dei diritti di tutte le donne in ambito sessuale e riproduttivo; invita pertanto l'Unione europea ad assumere un deciso impegno, in termini di risorse e politiche, a sostegno dei diritti e della salute in ambito sessuale e riproduttivo nel quadro delle relazioni con i paesi partner africani;

4.  invita le competenti autorità africane a tutelare efficacemente tutte le donne dagli omicidi, dai cosiddetti stupri "correttivi" e dalle altre forme di violenza sessuale assicurando altresì i responsabili alla giustizia;

5.  rileva che la stigmatizzazione e le violenze nei confronti delle donne lesbiche, bisessuali, transgender e intersessuali spesso sono strettamente legate alla discriminazione;

6.  esprime la propria solidarietà e il proprio sostegno per tutti gli attori che si battono per un più deciso programma di interventi a favore dei diritti delle donne;

7.  invita la Commissione e gli Stati membri a sostenere le organizzazioni africane delle donne e quelle di LGBTI nella loro lotta per la parità, l'autonomia fisica e il diritto alla libertà sessuale per tutte le donne e le persone LGBTI; evidenzia, nel contempo, la necessità di riservare particolare attenzione alle lesbiche appartenenti al movimento che riunisce donne e LGBTI, oltre che ad altri movimenti sociali, al fine di denunciare le doppie e talvolta molteplici discriminazioni cui sono soggette le lesbiche nei paesi africani;

8.  invita la Commissione, il Servizio europeo per l'azione esterna (SEAE) e gli Stati membri ad accelerare la realizzazione degli obiettivi stabiliti dal piano d'azione dell'Unione europea sulla parità di genere e l'emancipazione delle donne nello sviluppo, riservando particolare attenzione ai diritti delle donne lesbiche, bisessuali, transgender e intersessuali, sia nell'ambito delle relazioni con i paesi terzi che in sede di concessione del loro sostegno alle organizzazioni non governative e ai difensori dei diritti umani;

I diritti delle persone LGBTI in Africa

9.  invita tutti i 76 paesi del mondo in cui l'omosessualità è illegale, ivi inclusi i 38 paesi africani, a depenalizzarla;

10\. denuncia l'incitamento all'odio e alla violenza fondati sull'orientamento sessuale, l'identità di genere o l'espressione di genere; invita i summenzionati paesi a rispettare il diritto delle persone LGBTI alla vita e alla dignità; condanna qualunque atto di violenza, discriminazione, stigmatizzazione e umiliazione nei loro confronti;

11\. invita i leader politici e religiosi a condannare le persecuzioni e le discriminazioni fondate sull'orientamento sessuale e a prendere posizione in maniera decisa contro l'omofobia unendosi all'appello alla solidarietà e alla giustizia, in contrapposizione all'ingiustizia e al pregiudizio, formulato dall'arcivescovo Desmond Tutu;

12\. invita il SEAE, la Commissione e gli Stati membri a ricordare ai paesi africani, nell'ambito del dialogo politico con questi ultimi, l'obbligo di tenere fede agli impegni assunti in virtù di strumenti e convenzioni internazionali vincolanti in materia di diritti umani, e in particolare di rispettare e sostenere il diritto alla non discriminazione per motivi legati all'orientamento sessuale e all'identità di genere;

13\. accoglie favorevolmente il fatto che alcuni paesi africani, tra cui Capo Verde, la Repubblica centrafricana, il Gabon, la Guinea-Bissau, il Malawi, le isole Maurizio, il Ruanda, Sao Tomé e Principe, il Sud Africa e lo Swaziland, si sono detti contrari a considerare reato l'omosessualità, oppure si sono impegnati a depenalizzarla, o ancora hanno garantito alle persone LGBTI l'accesso all'assistenza sanitaria;

14\. invita il gruppo dei paesi ACP ad avviare un dialogo aperto, costruttivo e improntato al rispetto reciproco;

15\. invita i paesi africani a garantire l'incolumità dei difensori dei diritti di LGBTI e chiede all'UE di sostenere la società civile africana con programmi finalizzati allo sviluppo di capacità;

16\. insiste affinché gli accordi commerciali e i programmi di sviluppo includano clausole non negoziabili in materia di diritti umani e non discriminazione, soprattutto nel quadro dello Strumento di cooperazione allo sviluppo e dell'Accordo di Cotonou, e siano in linea con il nuovo approccio alle politiche per lo sviluppo dell'UE orientato ai diritti umani, non da ultimo per quanto concerne le azioni volte a contrastare le discriminazioni fondate sul genere e l'orientamento sessuale;

17\. esorta la Commissione, il SEAE e gli Stati membri ad avvalersi dello strumentario LGBT nella sua totalità per incoraggiare i paesi terzi a depenalizzare l'omosessualità, contribuire a ridurre le violenze e le discriminazioni nonché tutelare i difensori dei diritti umani delle persone LGBTI;

18\. invita la Commissione a continuare a finanziarie le organizzazioni non governative che si prodigano per la tutela dei diritti delle persone LGBTI, in particolare attraverso lo strumento europeo per la democrazia e i diritti dell'uomo (EIDHR);

19\. ricorda agli Stati membri l'obbligo imposto loro dalla direttiva 2004/83/CE (rifusione) di proteggere o dare asilo ai cittadini di paesi terzi in fuga dalle persecuzioni subite (o che rischiano di subire) nei rispettivi paesi d'origine in ragione del loro orientamento sessuale o della loro identità sessuale;

20\. invita la Commissione, e in particolare il suo vicepresidente nonché alto rappresentante dell'Unione per gli affari esteri e la politica di sicurezza Catherine Ashton, a intraprendere iniziative concrete, avvalendosi di tutti gli strumenti a disposizione, per esercitare pressioni a favore della tutela dalle discriminazioni e dalle persecuzioni fondate sull'orientamento sessuale e per sollevare le questioni in esame nell'ambito delle relazioni e dei dialoghi dell'UE con i paesi terzi; invita la Commissione a inserire tali problematiche nella tabella di marcia contro l'omofobia che in più occasioni il Parlamento europeo ha chiesto di elaborare(6);

21\. incarica il suo Presidente di trasmettere la presente risoluzione al Consiglio, alla Commissione, al vicepresidente/alto rappresentante dell'Unione per gli affari esteri e la politica di sicurezza, agli Stati membri, al Segretario generale del gruppo dei paesi ACP, a tutti gli ambasciatori di detti paesi presso l'Unione europea, al parlamento sudafricano, all'Unione africana e alle sue istituzioni.

[http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+MOTION+P7-RC-2012-0389+0+DOC+XML+V0//IT](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+MOTION+P7-RC-2012-0389+0+DOC+XML+V0//IT)