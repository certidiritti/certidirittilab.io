---
title: 'Ridicole affermazioni dell''arcivescovo di NY sul matrimonio gay'
date: Sat, 18 Jun 2011 09:22:00 +0000
draft: false
tags: [Americhe]
---

**Vaticano pensi ai suoi scandali invece di parlare a vanvera contro diritti gay.**  
   
**Roma, 17 giugno 2011**  
   
**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti**

  
Ormai siamo abituati ad ascoltare le più demenziali e folli teorie da parte dei rappresentanti dello Stato teocratico della Città del Vaticano, ma certo, sostenere come ha fatto oggi l'Arcivescovo di New York,  Timothy Dolan, Presidente della Conferenza episcopale degli Usa,  che "il legislatore non può arrogarsi compiti da Stato etico" e poi ancora che "ogni desiderio o moda non può tramutarsi in un diritto" fa davvero ridere (o piangere, dipende dalle sensibilità).  Il tutto perchè il Parlamento dello Stato di New York sta per votare il 'Marriage Equality Act', legge che, se approvata, consentirebbe il matrimonio tra persone dello stesso sesso, dopo che già cinque Stati Usa lo hanno deciso. E' molto probabile che il Senato  approvi in queste ore il provvedimento, che è stato presentato con una procedura d'urgenza dal Governatore democratico Andrew Cuomo.

Forse converrebbe che questi esponenti dell'integralismo fondamentalista dedicassero il loro tempo alle bancarotte che alcune diocesi Usa hanno dovuto dichiarare per far fronte a tutte le spese processuali delle migliaia di vittime di violenza contro i minori di cui si sono resi gravemente responsabili moltissimi  loro rappresentnati. Pensassero anche a quanto stanno combinando con lo Ior e con strani ricicli di denaro di provenienza dubbia di cui hanno parlato persino molti media a loro vicini. Non si preoccupino di coloro che, liberamente, potranno scegliere se sposarsi o no e la smettano una buona volta di ficcare il naso dove non dovrebbero. La loro è proprio una mania... Davvero roba da Freud ".