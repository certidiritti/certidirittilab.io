---
title: 'MATRIMONI GAY: OGGI LA SENTENZA DELLA CORTE SUPREMA DELLA  CALIFORNIA.'
date: Thu, 15 May 2008 07:32:55 +0000
draft: false
tags: [Comunicati stampa]
---

Oggi alle 19 la Corte Suprema della California si pronuncerà sul diritto al matrimonio per le persone omosessuali.Questo [l’annuncio ufficiale dell’imminente pronunciamento](http://www.courtinfo.ca.gov/courts/supreme/SF051508.PDF). Segue una ricostruzione dell'iter californiano... **BATTAGLIA LEGALE PER IL MATRIMONIO IN CALIFORNIA (USA)  
a cura di Marco Papi  
**  
**IMPORTANZA**  
  
La California è uno dei più grandi e rilevanti Stati USA.  
E’ lo Stato più popoloso del Nord America: ci vive il 12 percento della popolazione statunitense.  
Al suo interno ha sede la più popolata Contea degli Stati Uniti: la Contea di Los Angeles; ma anche la famosa Silicon Valley, Hollywood e ovviamente la libertaria San Francisco.  
Economicamente parlando, se fosse indipendente sarebbe l’ottava potenza mondiale.  
  
La Corte Suprema della California è la massima Corte dello Stato.  
E’ notoriamente una delle Corti più rispettate e ascoltate d’America. Se si vince lì, nel giro di poco si vince dappertutto negli Stati Uniti con immaginabili conseguenze a livello globale.  
Sessant’anni fa la Corte Suprema della California fu la prima Corte Suprema americana a mettere fine alla discriminazione razziale nel matrimonio: la famosa sentenza nel caso Perez v. Sharp fu la prima sentenza suprema a legalizzare i matrimoni tra persone di razza diversa.  
Entro qualche anno i matrimoni tra persone di razza diversa divennero legali dappertutto in America grazie anche al pronunciamento della Corte Suprema USA (vedi Loving v. Virginia).  
  
**IL MAXI-CASO LEGALE DAVANTI ALLA CORTE SUPREMA**  
  
Il maxi-caso legale si chiama “In re Marriage Cases”.  
E’ un pacchetto di ben sei cause legali avviate a inizio 2004 combinate assieme.  
  
**Origine**  
  
Il 12 Febbraio 2004 la Città-Contea di San Francisco comincia a rilasciare licenze di matrimonio anche alle coppie dello stesso sesso in barba alle norme che limitano il matrimonio alle coppie eterosessuali, modificando tra l’altro il linguaggio delle stesse, prescritto dalle autorità statali, di modo da renderlo neutro dal punto di vista del genere sessuale.  
Subito migliaia di gay si recano in Comune per sposarsi, anche affollandone gli ingressi per giorni persino di notte e sotto la pioggia portando i dipendenti comunali a presentarsi anche nelle giornate di festa. La Città vista l’affluenza decide qualche giorno dopo di rilasciare licenze di matrimonio solo su appuntamento; a soli cinque giorni da tale decisione la Città ha già ricevuto una valanga di prenotazioni tale che il primo appuntamento possibile è a due mesi di distanza. L’iniziativa matrimoniale di San Francisco cattura sin dall’inizio l’attenzione di tutti i media locali e americani in generale ed è presto imitata in altri Stati USA.  
  
Il 23 Febbraio 2004 a Los Angeles due coppie omosessuali tra cui il famoso attivista glbt reverendo Troy Perry e il suo partner avviano una causa legale dopo essersi visti negare il matrimonio dalla Contea di Los Angeles. Chiedono che le norme statali che limitano il matrimonio alle coppie uomo-donna vengano dichiarate incostituzionali, quindi inapplicabili.  
Nel frattempo, varie organizzazioni conservatrici si rivolgono anch’esse ai giudici chiedendo di dichiarare costituzionali le norme statali che limitano il matrimonio alle coppie uomo-donna.  
Partono, così, altre due cause che riguardano la costituzionalità delle norme matrimoniali.  
  
Un mese dopo il suo inizio, l’11 marzo, l’iniziativa matrimoniale di San Francisco viene stoppata dalla Corte Suprema della California. In quel mese sono riuscite a ottenere licenza di matrimonio quindi sposarsi circa 4000 coppie omosessuali.  
I matrimoni sono stati tutti dichiarati nulli alcuni mesi dopo dalla stessa Corte Suprema.  
  
Immediatamente dopo lo stop, a San Francisco varie coppie dello stesso sesso avviano anch’esse una causa legale chiedendo che le norme statali che limitano il matrimonio alle coppie uomo-donna vengano dichiarate incostituzionali, quindi inapplicabili. Alla causa si uniscono la principale associazione glbt della California (Equality California) e un’importante associazione glbt che fa riferimento all’area metropolitana di San Francisco (Our Family Coalition). A rappresentare le coppie dello stesso sesso e le due associazioni ci sono il National Center for Lesbian Rights, Lambda Legal, l’American Civil Liberties Union e altri.  
Nello stesso tempo anche San Francisco avvia una causa legale con lo stesso obbiettivo.  
In seguito un’ulteriore causa per il matrimonio viene avviata da altre coppie dello stesso sesso.  
  
**Le sei cause legali procedono congiuntamente dinnanzi alla Corte Superiore** di San Francisco.  
Il 13 Aprile 2005 la sentenza di primo grado: le norme statali che limitano il matrimonio alle coppie uomo-donna vengono dichiarate incostituzionali. Alla decisione non viene dato effetto immediato per consentire eventuali appelli da parte dei contrari ai matrimoni tra omosessuali.  
La decisione viene appellata. La Corte d’Appello il 5 Ottobre 2006 si pronuncia in modo opposto. La questione arriva così dinnanzi alla Corte Suprema della California, che accetta subito di esaminare il caso e si mostra molto intenzionata a comprendere bene la questione tant’è che a metà 2007 richiede alle parti in causa del materiale scritto supplementare.  
Il 4 Marzo 2008 ecco i dibattimenti orali dinnanzi ai giudici supremi.  
La Corte Suprema della California è solita pronunciarsi entro 90 giorni dai dibattimenti orali.  
  
**Argomenti**  
  
La partita si gioca principalmente sul convincere i giudici supremi che solo ed esclusivamente l’apertura del matrimonio alle coppie dello stesso sesso è eguaglianza.  
  
Lo Stato, infatti, che è la principale parte avversa difende l’esclusione matrimoniale in vigore affermando che i gay l’eguaglianza ce l’hanno già per effetto della normativa statale sull’unione registrata, e che lo Stato ha ragione a limitare il matrimonio alle coppie eterosessuali per ragioni che vanno dal ‘così è sempre stato’ al rispetto del sentire comune.  
Le coppie omosessuali, le associazioni glbt e San Francisco – così come le numerosissime altre associazioni, Città e Contee che sostengono questa battaglia legale anche inviando materiale scritto ai giudici – argomentano che il matrimonio è un’istituzione unica, che non può avere equivalenti in quanto radicatissima, universale, istantaneamente compresa da tutti in ogni dove, dalla portata personale e sociale che è categoricamente ineguagliabile. Indicano come miope e fuori dalla realtà insistere nel voler fare finta che il matrimonio sia solo un mero insieme di conseguenze legali, e affermano senza mezzi termini che escludere i gay dal diritto di unirsi in matrimonio con la persona di propria scelta confinandoli in una unione registrata a se stante vuol dire negare loro piena cittadinanza, piena espressione della propria personalità, piena libertà di decidere per la propria vita, vuol dire riproporre la logica segregazionista del “separati ma eguali” tanto in vigore decenni fa nei confronti delle persone di colore, vuol dire marchiarli in quanto inferiori rispetto a tutte le altre persone, vuol dire promuovere stigma e invitare la cittadinanza a atti di discriminazione nei loro confronti.  
  
Alla Corte si chiede di valutare la questione in maniera approfondita (vedi il concetto legale di strict scrutiny) in virtù del fatto che la discriminazione fondata sull’orientamento sessuale merita la stessa attenzione della discriminazione fondata sulla razza e l’esclusione dal matrimonio è discriminazione fondata sull’orientamento sessuale (gli etero possono sposare la persona amata mentre i gay no), in virtù del fatto che l’esclusione dal matrimonio costituisce discriminazione fondata sul sesso (un uomo non può sposare un uomo mentre una donna sì; una donna non può sposare una donna mentre un uomo sì) nonché promozione degli stereotipi basati sul sesso di appartenenza, in virtù del fatto che il diritto di unirsi in matrimonio è da considerarsi diritto fondamentale, ed in virtù del fatto che l’esclusione dal matrimonio viola le il diritto alla privacy e il diritto alla libera espressione.  
  
In nome di tutte le garanzie di libertà, eguaglianza, privacy, libera espressione, ecc scritte nella Costituzione della California, si richiede che le norme statali che limitano il matrimonio alle sole coppie uomo-donna siano dichiarate incostituzionali, quindi inapplicabili.  
  
**Possibili scenari**  
  
Tre gli scenari più probabili: la Corte dichiara le norme incostituzionali e le coppie omosessuali si possono unire in matrimonio subito; oppure la Corte si rifiuta di dichiarare le norme incostituzionali; oppure la Corte dichiara le norme incostituzionali ma prolunga lo status quo per un determinato periodo di tempo ordinando al legislatore di cancellarle.  
  
**Associazioni intervenute a favore**  
  
Importanti organizzazioni di specialisti, tra cui:  
L’Associazione Americana Psicologi  
L’Associazione Americana Psichiatri  
L’Associazione Americana Assistenti Sociali  
L’Associazione Americana Psicanalisti  
L’Associazione Americana Antropologi  
L’Accademia Americana Avvocati Matrimonialisti  
L’Associazione Californiana Psicologi  
L’Accademia Americana dei Pediatri sezione California  
L’Associazione Californiana Assistenti Sociali  
eccetera  
  
Storiche organizzazioni per i diritti civili dei neri, tra cui:  
La NAACP California  
Il NAACP Legal Defense and Educational Fund  
eccetera  
  
Altre importanti organizzazioni per i diritti civili in generale o di minoranze, tra cui:  
L’ACLU - American Civil Liberties Union  
Il Mexican American Legal Defense and Educational Fund  
L’Asian American Legal Defense and Education Fund  
ERA – Equal Rights Advocates  
eccetera  
  
Importanti associazioni locali di avvocati, tra cui:  
L’Associazione degli Avvocati di San Francisco  
L’Associazione degli Avvocati della Contea di Los Angeles  
L’Associazione degli Avvocati di Beverly Hills  
L’Associazione degli Avvocati della Contea di Santa Clara  
eccetera  
  
Varie organizzazioni religiose, come:  
L’Unione degli Ebrei Riformati  
Le Metropolitan Community Churches  
La United Church of Christ  
La Unitarian Universalist Association  
eccetera  
  
Le principali organizzazioni glbt californiane e statunitensi, come:  
Equality California  
Lambda Legal  
Human Rights Campaign  
National Center for Lesbian Rights  
Freedom To Marry  
Marriage Equality USA  
GLAD - Gay and Lesbian Advocates and Defenders  
Family Pride  
National Gay and Lesbian Task Force  
PFLAG - Parents and Friends of Lesbians and Gays  
Children of Lesbians and Gays Everywhere  
Equality Federation (Empire State Pride Agenda – New York, Love Makes a Family Connecticut, Garden State Equality – N.Jersey, Equality Vermont, Vermont Freedom To Marry Task Force,…)  
eccetera  
  
  
  
  
**Città e Contee intervenute a favore**  
  
La Città di Los Angeles  
La Città di San Diego  
La Città di San Jose  
La Città di Sacramento  
La Città di Long Beach  
La Città di Laguna Beach  
La Città di Oakland  
La Città di Santa Rosa  
La Città di Berkeley  
La Città di Santa Monica  
La Città di Santa Cruz  
La Città di Palm Springs  
La Città di West Hollywood  
La Contea di Santa Clara  
La Contea di San Mateo  
La Contea di Marin  
La Contea di Santa Cruz  
eccetera  
  
\-\- Le Città di Sacramento e Laguna Beach si sono aggiunte in seguito a tutte le altre, pertanto non sono visibili nel documento inviato alla Corte Suprema dal resto delle Città californiane.  
  
Particolarmente degno di nota è stato il gesto del Sindaco repubblicano di San Diego.  
  

**LE LEGGI DELLA CALIFORNIA**

**Matrimonio**

Fino agli anni settanta le norme in materia di matrimonio erano sessualmente neutre.  
Nel 1977 tali norme sono state riformulate di modo da definire il matrimonio in quanto unione “tra un uomo e una donna” (California Family Code section 300, section 301). Nel 2000 all’interno delle norme in materia di riconoscimento dei matrimoni celebrati altrove è stato scritto tramite referendum popolare che “solo il matrimonio di un uomo e una donna è valido o riconosciuto in California” (California Family Code section 308.5); quest’ultima revisione ha generato numerosi dibattiti, tutt’ora in corso e inseriti nella battaglia legale per il matrimonio, fra chi la interpreta in quanto riferita esclusivamente alle nozze celebrate altrove e chi la interpreta in modo ben più ampio attribuendo ad essa una valenza generale.

**Per vedere il California Family Code:**  
http://www.leginfo.ca.gov/cgi-bin/calawquery?codesection=fam&codebody=&hits=All

Il Parlamento ha approvato l’apertura del matrimonio alle coppie omosessuali per ben due volte (una volta nel 2005 e un’altra nel 2007), ma in entrambi i casi il Governatore repubblicano Arnold Schwarzenegger ha posto il veto affermando che una decisione in questo campo spetta unicamente agli elettori o ai giudici, riferendosi alla battaglia legale in corso.

**Unioni registrate**

Molte Città e Contee offrono alle coppie, indipendentemente dal sesso quindi siano esse omosessuali o eterosessuali, la possibilità di registrarsi ufficialmente.  
Vedi San Francisco, Sacramento (la capitale), Contea di Los Angeles, Berkeley, West Hollywood, Oakland, Palm Springs, Palo Alto, Santa Barbara, Santa Monica, eccetera.

A livello statale è prevista la possibilità di registrarsi ufficialmente per effetto di una legge sulle convivenze approvata anni fa (Assembly Bill 26 of 1999) che ha aggiunto nel California Family Code un capitolo specifico (Division 2.5). Sono ammesse quasi solo le coppie omosessuali; le coppie eterosessuali sono ammesse solo se almeno uno dei due partner supera i sessantadue anni. Inizialmente la registrazione non comportava tante conseguenze dal punto di vista giuridico, ma nel corso degli anni la normativa è stata modificata e ora conferisce ai partner molti dei vantaggi legali del matrimonio; nel 2007 il Senato ha approvato una proposta di legge (Senate Bill 11) mirante a consentire la registrazione a tutte le coppie eterosessuali.

Queste unioni, locali o statali, si chiamano “domestic partnership”.  
I registri locali e il registro nazionale si chiamano “domestic partnership registry”.

**ALCUNI LINK UTILI**  
_\- [Informazioni sulla domestic partnership locale di San Francisco](http://www.sfgov.org/site/countyclerk_index.asp?id=5540)_

_\- [Il documento per creare una domestic partnership locale a San Francisco](http://www.sfgov.org/site/uploadedfiles/countyclerk/docs/dpapplication.pdf)_

_\- [Informazioni sull’eventuale cerimonia](http://www.sfgov.org/site/countyclerk_page.asp?id=15175)_

_\- [Per prenotare online la propria cerimonia](http://https://services.sfgov.org/ceremony/intro.asp)_

_\- [Informazioni sulla domestic partnership statale](http://www.sos.ca.gov/business/sf/sf_dp.htm)_

_\- [Il documento per creare una domestic partnership secondo le leggi statali](http://www.sos.ca.gov/dpregistry/forms/sf-dp1.pdf)_

**FONTI**  
[_Sito della Corte Suprema della California, sezione dedicata al maxi-caso legale_](http://www.courtinfo.ca.gov/courts/supreme/highprofile)

_**Siti delle varie organizzazioni glbt e altri siti informativi:**_

http://www.nclrights.org  
http://www.eqca.org  
http://www.lambdalegal.org  
http://www.marriageequality.org  
http://www.freedomtomarry.org  
http://samesexmarriage.typepad.com  
http://www.sfgov.org/site/cityattorney_page.asp?id=67359

_**Siti di notizie glbt**_  
http://www.365gay.com  
http://www.advocate.com  
http://www.gay.com/news