---
title: 'UNA PIAZZA IN OGNI CITTA'' PER I DIRITTI UMANI'
date: Sat, 06 Dec 2008 08:13:12 +0000
draft: false
tags: [Comunicati stampa]
---

**Trieste, Milano, Padova, Bologna, Firenze, Perugia, Napoli, Cosenza, Catania: dopo Roma, sono le città nelle quali si stanno organizzando sit-in, volantinaggio, conferenze stampa, mostre etc... a difesa dei Diritti dell'Uomo.**

_Certi Diritti parteciperà a quante più iniziative possibile, convinta che la mobilitazione di massa sia il modo migliore per smuovere le coscienze e diffondere una cultura di rispetto e civilità._

**TRIESTE** \- Friuli Venezia Giulia (per info: trieste@certidiritti.it)

Sit-in di sabato 6 dicembre 2008 Roma - ore 17.00 Piazza Pio XII

**MILANO** \- Lombardia (per info: milano@certidiritti.it)

Sit-in in zona Piazza Duomo mercoledì 10 dicembre - anniversario  
della Dichiarazione Universale dei Diritti dell'Uomo - ore 17.30

**TORINO** \- Piemonte (per info: torino@certidiritti.it)

Mercoledì 3 Dicembre, manifestazione davanti l'arcivescovado di Torino - ore 12,30  
[(vedi info, foto e video della manifestazione già avvenuta)](index.php?option=com_content&view=article&id=231:video-e-news-manifestazione-a-torino-contro-arcidiocesi&catid=1:ultime&Itemid=55)  

**PADOVA** \- Veneto

Volantinaggio sabato 6 dicembre in Prato della Valle

**BOLOGNA** \- Emilia-Romagna

Volantinaggio di sensibilizzazione e distribuzione di fiori di fronte  
alle chiese lunedì 8 dicembre; sit-in Piazza Nettuno nei pomeriggi di  
martedì 9 e mercoledì 10 dicembre

**FIRENZE** \- Toscana

Sit-in nel centro cittadino lunedì 8 dicembre ore 17

**PERUGIA** \- Umbria

Conferenza stampa presso la sede Arcigay in via Pallotta 42 sabato 6  
dicembre ore 10

**NAPOLI** \- Campania (per info: napoli@certidiritti.it)

Sit-in davanti al Duomo sabato 6 dicembre alle ore 11

**COSENZA** \- Calabria

Mostra di fotografie sulle violenze omofobiche - itinerante nelle  
strade della città in occasione dei Test Days.

**CATANIA -** Sicilia (per info: catania@certidiritti.it)

Azione performativa in piazza Duomo il domenica 7 dicembre dalle 10  
alle 13