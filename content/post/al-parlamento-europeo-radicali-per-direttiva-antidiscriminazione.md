---
title: 'AL PARLAMENTO EUROPEO RADICALI  PER DIRETTIVA ANTIDISCRIMINAZIONE'
date: Tue, 03 Mar 2009 14:29:11 +0000
draft: false
tags: [ANTIDISCRIMINIAZIONE, CAPPATO, Comunicati stampa, direttiva, EMENDAMENTI, MARZOCCHI, parlamento europeo]
---

AL PARLAMENTO EUROPEO I RADICALI DEPOSITANO EMENDAMENTI PER RAFFORZARE LA DIRETTIVA ANTI-DISCRIMINAZIONE CONTRO OPPOSIZIONE DI ALCUNI STATI MEMBRI.   
   
   
Dichiarazione di Marco Cappato, eurodeputato Radicale al PE  e Ottavio  Marzocchi, responsabile per le questioni europee dell'Associazione Radicale Certi Diritti: "Marco Cappato, Eurodeputato Radicale, ha depositato oggi una serie di emendamenti alla proposta di direttiva della Commissione europea sulla lotta alle discriminazioni, basata sull'articolo 13 del Trattato CE, che mirano a rafforzarla sostanzialmente.  
   
Gli emendamenti cancellano e ribaltano radicalmente una serie di deroghe al principio di eguaglianza che erano state proposte dalla Commissione per accomodare alcune resistenze degli Stati membri. Gli emendamenti depositati affermano che la direttiva si applica a tutti i settori, siano essi di competenza nazionale ed europea, e che il non riconoscimento delle unioni tra persone dello stesso sesso é una discriminazione non conforme alla direttiva.  
   
Gli altri emendamenti mirano ad assicurare che il principio di eguaglianza si applichi negli Stati membri ed a livello dell'UE, per tutti i settori coperti dalla legislazione nazionale ed europea, includendo i settori attualmente esclusi, ovvero: le discriminazioni basate sull'orientamento sessuale che negano uno status matrimoniale, familiare, civile alle persone dello stesso sesso; tutte le attività economiche e commerciali; l'educazione; le discriminazioni multiple; quelle basate sui diritti e la salute sessuale e riproduttiva; la libertà di circolazione ed il mutuo riconoscimento; lo status delle chiese e delle organizzazioni religiose; lo status dei cittadini degli Stati terzi, l'immigrazione e l'asilo; gli organi anti-discriminazione nazionale dovranno inoltre compiere attività di verifica dell'applicazione della direttiva negli Stati membri e cooperare con l'Agenzia dei Diritti fondamentali.  
   
Al contempo, gli emendamenti cancellano le deroghe previste dalla proposta della Commissione in merito all'educazione (organizzazione e contenuti; possibilità di discriminare sulla base della religione e di vietare di portare segni religiosi), la sicurezza sociale e sanità, i servizi di interesse generale; le attività economiche che non siano commerciali o professionali; il riferimento allo status privilegiato delle chiese viene emendato al fine di assicurare che non vi siano discriminazioni tra le chiese, tra gli individui e tra organizzazioni religiose e non religiose; le discriminazioni basate sull'età; l'ordine pubblico, sicurezza pubblica, salute pubblica; lo status dei cittadini degli Stati terzi, immigrazione ed asilo.  
   
La commissione libertà pubbliche voterà sulla relazione dell'onorevole olandese dei verdi Buitenweg e sugli emendamenti il 16 marzo prossimo. Ci auguriamo che la relatrice e la maggioranza della commissione libertà pubbliche vogliano - invece che inseguire il PPE e gli Stati membri più recalcitranti nel costellare di deroghe assurde la proposta di direttiva -  avere il coraggio di dare un segnale forte come PE per assicurare che si faccia un vero passo avanti nella lotta alle discriminazioni".   
   
   
Gli emendamenti e la relazione in italiano sono disponibili su:  
[http://www.europarl.europa.eu/activities/committees/amendmentsCom/comparlDossier.do?dossier=LIBE%2F6%2F65317&body=LIBE&language=IT](http://www.europarl.europa.eu/activities/committees/amendmentsCom/comparlDossier.do?dossier=LIBE%2F6%2F65317&body=LIBE&language=IT)  
   
Gli emendamenti di Marco Cappato sono 24, ovvero i numeri:  
34, 43, 54, 83, 88, 90, 101, 102, 103, 104, 114, 170, 182, 188, 191, 206, 216, 217, 228, 229, 236, 309, 310, 312