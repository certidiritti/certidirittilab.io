---
title: 'VIOLENZA OMOFOBICA: OCCORRE STRAGE PER MUOVERSI?'
date: Sun, 23 Aug 2009 14:01:50 +0000
draft: false
tags: [Comunicati stampa]
---

COPPIA GAY AGGREDITA A ROMA: LA VIOLENZA OMOFOBICA FRUTTO DEL FONDAMENTALISMO RELIGIOSO E IDEOLOGICO. DOBBIAMO ATTENDERE UNA STRAGE PER INTERVENIRE CONTRO L’OMOFOBIA?

#### Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

#### “Abbiamo deciso di non unirci al coro di chi, per coerenza o per ipocrisia, urla allo scandalo e alla vergogna per l’ennesimo atto di violenza omofobica, che questa volta sta mettendo a rischio la vita di un ragazzo che ha la colpa di essere gay, che ha ‘osato’ abbracciare e/o baciare il suo compagno davanti al Gay Villane di Roma ieri notte. Quanto accaduto, ne siamo certi,  si ripeterà molto presto, e certamente in forma più o meno grave, perché il ‘contesto’, il ‘clima di violenza contro le persone gay mai come oggi trova terreno fertile, se non addirittura aiuto e sostegno da chi dovrebbe considerare questo fenomeno un’emergenza e invece fa finta di nulla, voltandosi dall’altra parte, magari sotto un ombrellone. Ci riferiamo a coloro che alimentano l’odio con “fatwe” religiose o ideologiche denigrando gli omosessuali, così come a coloro che, per motivi di asservimento politico, continuano a dare  giudizi pesanti contro le persone gay e le loro scelte di vita, la loro felicità.

#### Nei giorni scorsi avevamo segnalato alcuni altri episodi, forti e gravi per la loro carica violenta, avvenuti contro le persone gay. Sappiamo bene che l’ignoranza e l’egoismo politico, ormai così diffusi, impediranno qualsiasi presa di coscienza della gravità del fenomeno dell’omofobia e dei gravi danni sociali e culturali che essa produce. Quanto accaduto è ulteriore sintomo della grave involuzione che stiamo vivendo sul piano dei diritti civili e umani e che ci mette tra i paesi più a rischio, al pari di quelli dittatoriali e teocratici che per legge si accaniscono contro le persone gay.

#### Cosa dobbiamo attendere per far prendere coscienza alla nostra classe politica che ci governa della necessità di avviare subito iniziative volte all’educazione, all’informazione, all’aiuto e all’assistenza sul tema dell’omofobia? Dobbiamo aspettare che avvenga un attentato con strage in un locale gay? Questo è quello che l’ignoranza attende purtroppo affinché si accenda una lampadina, noi speriamo che questo si possa evitare. Ma non siamo ottimisti”.