---
title: 'Manifesti con donna seminuda e codice a barre contro prostituzione è azione ignobile e lesiva delle donne e delle prostitute. Firma l''appello per la legalizzazione!'
date: Wed, 18 Jul 2012 13:05:40 +0000
draft: false
tags: [Lavoro sessuale]
---

Roma, 18 luglio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

L’immagine di una donna a seno nudo, con stampato sul corpo il codice a barre con la scritta: “prima di comprarla rifletti”, apparso per le strade di Roma è un messaggio falso, fuorviante e gravemente offensivo e lesivo di tutte le donne e  della dignità di centinaia di migliaia di persone che si prostituiscono, donne e uomini.

Sappiamo che buona parte della classe politica è incapace di agire con coerenza, senza ipocrisie e che, per esempio, gli effetti che si vogliono denunciare sulla prostituzione di strada si potrebbero benissimo combattere legalizzando le prestazioni sessuali, come un qualsiasi altro lavoro.

La denuncia fatta da Matilde Spadaro e Vincenzo Vecchio, consiglieri di centro-sinistra del XII Municipio di Roma è ridicola proprio perché oltre all’immagine gravemente offensiva e il messaggio ‘Prima di comprarla rifletti’ considerano il fenomeno “oggettivamente insostenibile dal punto di vista del degrado ambientale e del decoro, ma anche dal punto di vista etico”.  Fanno intervenire la morale sulle prestazioni sessuali confondendo il tutto rispetto, semmai, ad un problema di ‘ordine pubblico’.

L’Associazione Radicale Certi Diritti nel ribadire che tali problemi si risolvono con la legalizzazione/regolamentazione della prostituzione, esprime al Comitato per i Diritti Civili delle prostitute la sua piena solidarietà e vicinanza per l’offesa a loro recata dai manifesti affissi a Roma e darà ogni supporto anche legale alle azioni di denuncia e richiesta di sequestro dei manifesti affissi a Roma.

Il dibattito contro le politiche proibizioniste e repressive dell’amministrazione Alemanno contro la prostituzione a Roma sono miseramente fallite, occorre che si apra un dibattito serio e pragmatico, senza offese, denigrazioni e falsificazioni della realtà.

[FIRMA L'APPELLO PER LA LEGALIZZAZIONE  
DELLA PROSTITUZIONE >>>](campagne-certi-diritti/itemlist/category/85-legalizzazione-prostituzione)