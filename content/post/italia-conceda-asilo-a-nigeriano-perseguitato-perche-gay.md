---
title: 'Italia conceda asilo a nigeriano perseguitato perchè gay'
date: Mon, 09 May 2011 12:19:09 +0000
draft: false
tags: [Politica]
---

**Arcigay, EveryOne e l’Associazione radicale Certi Diritti fanno appello al Ministro dell'Interno Roberto Maroni e al Ministro per le Pari Opportunità Mara Carfagna perché sia conferito con estrema urgenza lo status di rifugiato a Joshua J., 28enne nigeriano e omosessuale.**  
Entro giovedì 12 maggio prossimo Joshua deve lasciare il territorio nazionale perché la commissione territoriale sull'asilo di Caserta ha respinto la sua richiesta obiettando che in Nigeria non sono riscontrabili abusi concreti nei confronti di persone omosessuali da  
parte del Governo o delle autorità. In realtà in Nigeria il codice penale prevede fino a 14 anni di detenzione per omosessualità e negli stati federali del Paese nei quali è applicata la Sharia la pena per gli omosessuali è di 100 frustate per gli uomini non sposati, e di 1 anno di prigione e la morte per lapidazione per gli uomini sposati, anche se vi sono casi di omosessuali non sposati mandati comunque al patibolo.  
  
Proprio per questo Joshua è fuggito dalla Nigeria, dove rischia torture e morte, nel 2008, dopo che la famiglia musulmana del ragazzo con cui aveva avuto una relazione lo ha scoperto e, minacciandolo di morte, lo ha denunciato alle Autorità di polizia.  
  
A febbraio del 2008 un articolo del Nigeria Observer ha pubblicato la sua foto e un mandato di cattura in cui la polizia fissa una taglia per la consegna di Joshua alle autorità. Alla richiesta d'asilo era stata allegata la traduzione giurata (che trovate in allegato) con fotocopia del quotidiano nigeriano dove la polizia, al momento della fuga del giovane dalla Nigeria, aveva pubblicato il mandato di cattura per atti omosessuali con tanto di foto e nome di Joshua.  
  
Sul caso di Joshua è stata presentata un'interrogazione al Senato Da Giuseppe Valditara di FLI che per ora non ha ottenuto una risposta.  
  
“Siamo quindi a chiedere – spiegano i rappresentanti delle associazioni – “che il Governo italiano attivi immediatamente, attraverso il Ministero dell’Interno e il Ministero delle Pari opportunità, tutte le procedure necessarie per scongiurare il ritorno coatto di Joshua J.  in Nigeria, che lo metterebbe in serissimo pericolo di vita".