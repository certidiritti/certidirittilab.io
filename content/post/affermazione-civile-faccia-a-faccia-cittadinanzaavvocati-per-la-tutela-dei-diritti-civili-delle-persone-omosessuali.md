---
title: 'Affermazione Civile: faccia a faccia cittadinanza/avvocati per la tutela dei diritti civili delle persone omosessuali'
date: Tue, 09 Nov 2010 05:58:50 +0000
draft: false
tags: [Affermazione Civile, Senza categoria]
---

Certi Diritti organizza a Milano un momento di incontro tra la cittadinanza LGBT milanese e gli avvocati che hanno sostenuto le coppie di Affermazione Civile presso la Corte Costituzionale **Martedì 16 Novembre presso il Negozio Civico “CHIAMAMILANO” in Largo corsia dei Servi dalle ore 18:30 fino alle 20:30**, orario di chiusura del negozio civico.

L’iniziativa di “Affermazione Civile”, partita nel 2007, ha portato alla recente sentenza della Corte Costituzionale 138/2010,  che oltre a riconoscere la vita familiare delle coppie omosessuali, e ad invitare il parlamento a legiferare in materia, promuove il ricorso alla giustizia per velocizzare il riconoscimento di specifici diritti davanti alla legge. A tale importante punto fermo hanno contribuito, con un costante impegno, gli avvocati di Avvocatura LGBT Rete Lenford che hanno preparato il terreno agli avvocati che ci hanno rappresentato davanti alla Corte: Marilisa D'Amico, Massimo Clara, Ileana Alesso.

Coincidenza significativa, proprio in questi giorni a Milano (8-10 Novembre 2010)  si terrà la conferenza della famiglia promossa dal Governo ed organizzata dal sottosegretario Carlo Giovanardi, che, ignorando completamente la sentenza della Suprema Corte, intende (letteralmente) “prendere in considerazione ed eventualmente sviluppare opportuni criteri di adeguatezza Familiare”, senza tenere in conto le diverse e molteplici famiglie che si affermano sempre più nella società moderne, con necessità di forme di regolamentazione, aiuto e sostegno. E senza tenere in conto testamento biologico, matrimonio civile tra persone omosessuali, divorzio breve, mediazione familiare, assegnazione di case alle famiglie anche conviventi bisognose, modifiche alla legge per l’affido condiviso, unioni civili, filiazione, procreazione assistita, adozione, responsabilità genitoriale, ecc…

**Sentiamo perciò il bisogno di rilanciare l’iniziativa legata alle tematiche di “Affermazione Civile”** per dare l’opportunità a tutte le famiglie LGBT di avere più chiarezza sulle possibilità di tutelare i propri  diritti davanti ad una legge che le ignora.

Certi Diritti organizza a Milano un momento di incontro tra la cittadinanza LGBT milanese e gli avvocati che hanno sostenuto le coppie di Affermazione Civile presso la Corte Costituzionale **Martedì 16 Novembre presso il Negozio Civico “CHIAMAMILANO” in Largo corsia dei Servi dalle ore 18:30 fino alle 20:30**, orario di chiusura del negozio civico.

L'incontro è finalizzato a:

*   promuovere una cultura del diritto nella cittadinanza,

*   stimolare la fiducia e conoscenza reciproca tra la comunità LGBT e gli  avvocati

*   approfondire i reali problemi che vivono le persone LGBT e la consapevolezza delle eventuali discriminazioni subite

*   individuare possibili cause pilota laddove la legge stessa discrimina in base all'orientamento sessuale di individui e coppie

_E' importante chiarire che non si tratterà di un momento di consulenza legale su casi specifici di discriminazione, ma di conoscenza reciproca._

Durante questo momento di incontro sarà anche possibile recuperare informazioni sulle iniziative di dell’associazione radicale Certi Diritti.

**Certi Diritti, Milano**

[www.certidiritti.it](undefined/)