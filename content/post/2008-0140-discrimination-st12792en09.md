---
title: '2008-0140-Discrimination-ST12792.EN09'
date: Mon, 31 Aug 2009 15:19:54 +0000
draft: false
tags: [Senza categoria]
---

  

  

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 31 August 2009

Interinstitutional File:

2008/0140 (CNS)

12792/09

SOC 478

JAI 544

MI 307

  

  

  

  

  

NOTE

from :

The Presidency

to :

The Working Party on Social Questions

on :

7 September 2009

No. prev. doc. :

12096/09 SOC 449 JAI 485 MI 276

No. Cion prop. :

11531/09 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached a set of Presidency drafting suggestions concerning the above proposal.

Changes in relation to the previous version (doc. 11774/09) are indicated as follows: additions are in **bold** and deletions are marked **\[…\]**.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

Proposal for a

COUNCIL DIRECTIVE

on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

(9)     Therefore, legislation should prohibit discrimination based on religion or belief, disability, age or sexual orientation in a range of areas outside the labour market, including social protection, education and access to and supply of goods and services, including housing**.** Services should be taken to be those within the meaning of Article 50 of the EC Treaty. **\[…\]**

…

(11)     **Deleted.**

…

(12a) (new)  In **\[…\]** **accordance with the** judgment **of the Court of Justice** **\[…\]** in Case C-303/06[\[1\]](#_ftn1), **\[…\]** it is **\[...\]** appropriate to provide explicitly for protection from discrimination by association on all grounds covered by this Directive. Such discrimination occurs, inter alia, when a person is treated less favourably, or harassed, because, in the view of the discriminator, he or she is associated with persons of a particular religion or belief, disability, age or sexual orientation, for instance through his or her family, friendships, employment or occupation.

…

  

(14a) Differences in treatment in connection with age **\[…\]** may be permitted under certain circumstances if they are objectively and reasonably justified by a legitimate aim and the means of achieving that aim are appropriate and necessary. Such differences of treatment may include, for example, special age conditions regarding access to certain goods or services such as alcoholic drinks, arms, or driving licences.

(15)   Actuarial and risk factors related to disability and to age are used in the provision of insurance, banking and other financial services. These should not be regarded as constituting discrimination where service providers have shown, by **relevant** actuarial principles, **accurate** statistical data or medical knowledge, that  such factors are determining factors for the assessment of risk**. \[…\]**

(16) **Deleted.**

(17)   While prohibiting discrimination, it is important to respect other fundamental rights and freedoms, including the protection of private and family life and transactions carried out in that context, the freedom of religion, **\[…\]** the freedom of association**, the freedom of expression and the freedom of the press. Therefore the simple expression of a personal opinion or the display of religious symbols or messages should be presumed as not constituting harassment.**

(17b) **Deleted.**

(17b) (new) **\[…\]** This Directive covers access to social protection, which includes both social security and social assistance, as well as health care, **thereby providing comprehensive protection against discrimination in this field.** Consequently, the Directive applies with regard to access to rights and benefits which are derived from general or special social security**, \[…\]** social assistance **and healthcare** schemes, which are provided either directly

  

by the State, or by private parties in so far as the provision of those benefits by the latter is funded by the State. In this context, the Directive applies with regard to \[…\] **pecuniary benefits,** **\[…\]** benefits in kind **and services**, irrespective of whether the schemes involved are contributory or non-contributory. The abovementioned schemes include, for example, access to the branches of social security defined by Regulation 883/2004/EC on the coordination of social security systems[\[2\]](#_ftn2), **as well as \[…\]** schemes providing for benefits granted for reasons related to the lack of financial resources or risk of social exclusion**.**

(17c) (new) In the context of the free movement of workers, the Court of Justice has defined the concept of “social advantages” as covering all rights or benefits which, whether or not linked to a contract of employment, are generally granted to national workers primarily because of their objective status as workers or by virtue of the mere fact of their residence on the national territory and whose extension to workers who are nationals of other Member States therefore seems likely to facilitate the mobility of the latter workers within the Community. This Directive does not cover rights and benefits derived from a contract of employment, such as remuneration, which are covered by Directive 2000/78/EC. However, access to social protection as such is covered by the present Directive. In this context, for the purposes of this Directive, in accordance with its objective of providing a comprehensive protection from discrimination, the concept of social advantages covers any rights or benefits which can facilitate the social integration of a person, other than those derived from a contract of employment or from a social protection scheme.

(17d) (new) All individuals enjoy the freedom to contract, including the freedom to choose a contractual partner for a transaction. This Directive should not apply to economic transactions undertaken by individuals for whom these transactions do not constitute a professional or commercial activity. In this context, the concept of professional or commercial activity may be defined in accordance with the national laws and practice of the Member States.

(17e) (new) This directive does not alter the division of competences between the European Community and the Member States in the areas of education and social protection, including social security and health care. It is also without prejudice to the essential role and wide discretion of the Member States in providing, commissioning and organising services of general economic interest.

  

(17f) (new) **\[…\] The** exclusive competence **of Member States with regard to \[…\]** the organisation of **\[…\] their social protection systems \[…\]** includes decisions on the setting up, **financing** and management of such **\[…\] systems** and **related** institutions as well as on **\[…\] the substance and delivery  of** benefits **and health services and the conditions of eligibility.** In particular Member States retain the possibility to reserve certain **\[…\]** benefits **or services** to certain age groups or persons with disabilities. **Moreover, this Directive is without prejudice to the powers of the Member States to organise their social protection systems in such a way as to guarantee their sustainability.**

(17g) (new) \[…\] **The** exclusive competence **\[…\]** **of Member States with regard to** **\[…\]** the content of teaching or activities and the organisation of national educational systems, including the provision of special needs education, **\[…\]** includes the setting up and management of educational institutions, the development of curricula and other educational activities and the definition of examination processes. In particular Member States retain the possibility to set age limits for certain education activities. However, **there may be no discrimination in the** access to educational activities, including the admission to and participation in classes or programmes and the evaluation of students' performance **\[…\]**.

(17h) (new) This Directive does not apply to matters covered by family law including marital status and adoption, and laws on reproductive rights. It is also without prejudice to the secular nature of the State, state institutions or bodies, or education. **\[…\]**

(18)     **Deleted.**

Article 1  
Purpose

This Directive lays down a framework for combating discrimination on the grounds of religion or belief, disability, age, or sexual orientation, with a view to putting into effect in the Member States the principle of equal treatment within the scope of Article 3.

Article 2  
Concept of discrimination

1.       For the purposes of this Directive, the “principle of equal treatment” shall mean that  there shall be no direct or indirect discrimination on any of the grounds referred to in Article 1.

2.       For the purposes of paragraph 1, the following definitions apply:

(a)     direct discrimination shall be taken to occur where one person is treated less favourably than another is, has been or would be treated in a comparable situation, on any of the grounds referred to in Article 1;

(b)     indirect discrimination shall be taken to occur where an apparently neutral provision, criterion or practice would put persons of a particular religion or belief, a particular disability, a particular age, or a particular sexual orientation at a particular disadvantage compared with other persons, unless that provision, criterion or practice is objectively justified by a legitimate aim and the means of achieving that aim are appropriate and necessary.

3.       Harassment shall be deemed to be a form of discrimination within the meaning of paragraph 1, when unwanted conduct related to any of the grounds referred to in Article 1 takes place with the purpose or effect of violating the dignity of a person and of creating an intimidating, hostile, degrading, humiliating or offensive environment. **\[…\]** In this context, the concept of harassment may be defined in accordance with the national laws and practice of the Member States.

3a.     Discrimination includes direct discrimination or harassment due to a person's association with persons of a certain religion or belief, persons with disabilities, persons of a certain age or of a certain sexual orientation.

4.       An instruction to discriminate against persons on any of the grounds referred to in Article 1 shall be deemed to be discrimination within the meaning of paragraph 1.

  

5.       Denial of reasonable accommodation in a particular case as provided for by Article 4 (1)(a) of the present Directive as regards persons with disabilities shall be deemed to be discrimination within the meaning of paragraph 1.

6.       Notwithstanding paragraph 2, **\[…\]** differences of treatment on grounds of age **\[…\]** shall not constitute discrimination, if **\[…\]** they are  objectively and reasonably  justified by a legitimate aim, and if the means of achieving that aim are appropriate and necessary. **In this context Member States may specify aims which can be considered to be legitimate.**

Such differences of treatment may include the fixing of a specific age for access to social protection, including social security and healthcare; social advantages; education; and certain goods or services which are available to the public.

**6a.     (new) Notwithstanding paragraph 2, differences of treatment of persons with a disability shall not constitute discrimination, if they are aimed at protecting their health and safety and if the means of achieving that aim are appropriate and necessary.**

7.       Notwithstanding paragraph 2, in the provision of financial services**,** **\[…\]** proportionate differences in treatment where, for the product in question, the use of age or disability is a determining factor in the assessment of risk based on relevant actuarial principles, accurate statistical data or  medical knowledge **shall not be considered discrimination for the purposes of this Directive**.

8.       This Directive shall be without prejudice to **\[…\]** measures laid down in national law which, in a democratic society, are necessary for public security, for the maintenance of public order and the prevention of criminal offences, for the protection of health and the protection of the rights and freedoms of others.

Article 3  
Scope

1.       Within the limits of the powers conferred upon the Community, the prohibition of discrimination shall apply to all persons, as regards both the public and private sectors, including public bodies, in relation to access to:

(a)          Social protection, including social security and healthcare;

(b)          Social advantages;

(c)          Education;

(d)         and the supply of, goods and other services which are available to the public, including housing.

Subparagraph (d) shall apply to individuals only insofar as they are performing a professional or commercial activity defined  in accordance with national laws and practice.

2.       This Directive does not alter the division of competences between the European Community and the Member States. In particular it does not apply to:

(a)     matters covered by family law, including marital status and adoption, and laws on reproductive rights;

(b)     the organisation of **\[…\] Member States'** social **\[…\] protection          systems, including decisions on the setting up, financing and management of such systems and related institutions as well as on the substance and delivery of benefits and services and the conditions of eligibility;**

(c) **\[…\]**the powers of Member States to determine **\[…\]** the type of health services provided **and the conditions of elegibility; and**

(d)     the content of teaching or activities and the organisation of \[…\] **Member States'** educational systems, including the provision of special needs education.

3.       Member States may provide that differences of treatment  based on religion or belief in respect of access to educational institutions, the ethos of which is based on religion or belief, in accordance with national laws, traditions and practice, shall not constitute discrimination. **\[…\]**

3a.     This Directive is without prejudice to national measures authorising or prohibiting the wearing of religious symbols.

4.       This Directive is without prejudice to national legislation ensuring the secular nature of the State, State institutions or bodies, or education, or concerning the status and activities of churches and other organisations based on religion or belief. **\[…\]**

5.       This Directive does not cover differences of treatment based on nationality and is without prejudice to provisions and conditions relating to the entry into and residence of third-country nationals and stateless persons in the territory of Member States, and to any treatment which arises from the legal status of the third-country nationals and stateless persons concerned.

Article 5  
Positive action

1.  With a view to ensuring full equality in practice, the principle of equal treatment shall not prevent any Member State from maintaining or adopting specific measures to prevent or compensate for disadvantages linked to religion or belief, disability, age, or sexual orientation.

2**.** (new) The principle of equal treatment shall be without prejudice to the right of Member States to maintain or adopt more favourable provisions for persons of a **\[…\]** **given** age or for persons with disabilities as regards conditions for access to social protection, including social security and healthcare; social advantages; education; and certain goods or services which are available to the public, in order to promote their economic, cultural or social integration.

…

Article 14a (new)  
Gender mainstreaming

In accordance with **the objective of** Article 3(2) of the EC Treaty, Member States shall, when implementing this Directive, **\[…\]** take into account the objective of equality between men and women.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) Case C-303/06, Coleman v. Attridge, judgment of 17 July 2008, nyr.

[\[2\]](#_ftnref2) OJ L 166, 30.4.2004, p. 1.