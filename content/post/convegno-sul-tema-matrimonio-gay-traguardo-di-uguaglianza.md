---
title: 'Convegno sul tema: “Matrimonio gay: traguardo di uguaglianza”'
date: Sun, 11 Oct 2009 09:59:33 +0000
draft: false
tags: [Senza categoria]
---

### Interventi

*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390622 "Riproduci nel player")
    
    #### Sergio Rovasio
    
    [![Altri eventi con oratore Sergio Rovasio](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/sergio-rovasio "Altri eventi a cui ha partecipato Sergio Rovasio")  
    
    segretario dell'Associazione radicale "Certi diritti"
    
    ##### 15:004' 32"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390623 "Riproduci nel player")
    
    #### La storia di tre coppie gay e lesbiche che hanno deciso di chiedere il riconoscimento del matrimonio in Italia
    
    ##### 15:0417' 43"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390624 "Riproduci nel player")
    
    #### Maria Teresa Meli
    
    [![Altri eventi con oratore Maria Teresa Meli](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/maria-teresa-meli "Altri eventi a cui ha partecipato Maria Teresa Meli")  
    
    giornalista "Corriere della sera"
    
    ##### 15:224' 49"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390625 "Riproduci nel player")
    
    #### Enzo Cucco
    
    [![Altri eventi con oratore Enzo Cucco](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/enzo-cucco "Altri eventi a cui ha partecipato Enzo Cucco")  
    
    direttore della Fondazione Sandro Penna di Torino
    
    ##### 15:272' 40"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390626 "Riproduci nel player")
    
    #### Sergio Rovasio
    
    [![Altri eventi con oratore Sergio Rovasio](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/sergio-rovasio "Altri eventi a cui ha partecipato Sergio Rovasio")  
    
    segretario dell'Associazione radicale "Certi diritti"
    
    ##### 15:2924' 13"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390627 "Riproduci nel player")
    
    #### Alexander Schuster
    
    [![Altri eventi con oratore Alexander Schuster](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/alexander-schuster "Altri eventi a cui ha partecipato Alexander Schuster")  
    
    costituzionalista, Avvocatura per i diritti LGBT – Rete Lenford
    
    ##### 15:5319' 34"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390628 "Riproduci nel player")
    
    #### Gian Mario Felicetti
    
    [![Altri eventi con oratore Gian Mario Felicetti](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/gian-mario-felicetti "Altri eventi a cui ha partecipato Gian Mario Felicetti")  
    
    direttivo Associazione Radicale Certi Diritti
    
    ##### 16:1317' 34"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390629 "Riproduci nel player")
    
    #### Luigi Pannarale
    
    [![Altri eventi con oratore Luigi Pannarale](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/luigi-pannarale "Altri eventi a cui ha partecipato Luigi Pannarale")  
    
    ordinario di Sociologia del Diritto, Università degli Studi di Bari
    
    ##### 16:3116' 8"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390630 "Riproduci nel player")
    
    #### Enzo Cucco
    
    [![Altri eventi con oratore Enzo Cucco](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/enzo-cucco "Altri eventi a cui ha partecipato Enzo Cucco")  
    
    direttore della Fondazione Sandro Penna di Torino
    
    ##### 16:472' 1"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390631 "Riproduci nel player")
    
    #### Antonio Rotelli
    
    [![Altri eventi con oratore Antonio Rotelli](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/antonio-rotelli "Altri eventi a cui ha partecipato Antonio Rotelli")  
    
    presidente Avvocatura per i diritti LGBT - Rete Lenford
    
    ##### 16:497' 4"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390632 "Riproduci nel player")
    
    #### Giuliano Federico
    
    [![Altri eventi con oratore Giuliano Federico](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/giuliano-federico "Altri eventi a cui ha partecipato Giuliano Federico")  
    
    direttore editoriale GAY.tv
    
    ##### 16:562' 57"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390633 "Riproduci nel player")
    
    #### Anna Paola Concia
    
    [![Altri eventi con oratore Anna Paola Concia](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/anna-paola-concia "Altri eventi a cui ha partecipato Anna Paola Concia")  
    
    deputato (PD)
    
    ##### 16:592' 37"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390634 "Riproduci nel player")
    
    #### Alessandro Zan
    
    [![Altri eventi con oratore Alessandro Zan](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/alessandro-zan "Altri eventi a cui ha partecipato Alessandro Zan")  
    
    segretario regionale dell'Arcigay del Veneto
    
    ##### 17:0111' 43"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390635 "Riproduci nel player")
    
    #### Donatella Poretti
    
    [![Altri eventi con oratore Donatella Poretti](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/donatella-poretti "Altri eventi a cui ha partecipato Donatella Poretti")  
    
    senatrice (PD - Radicali)
    
    ##### 17:134' 1"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390636 "Riproduci nel player")
    
    #### Marco Beltrandi
    
    [![Altri eventi con oratore Marco Beltrandi](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/marco-beltrandi "Altri eventi a cui ha partecipato Marco Beltrandi")  
    
    deputato (PD - Radicali)
    
    ##### 17:177' 16"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390637 "Riproduci nel player")
    
    #### Anna Paola Concia
    
    [![Altri eventi con oratore Anna Paola Concia](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/anna-paola-concia "Altri eventi a cui ha partecipato Anna Paola Concia")  
    
    deputato (PD)
    
    ##### 17:2411' 48"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390638 "Riproduci nel player")
    
    #### Alessandro Zan
    
    [![Altri eventi con oratore Alessandro Zan](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/alessandro-zan "Altri eventi a cui ha partecipato Alessandro Zan")  
    
    segretario regionale dell'Arcigay del Veneto
    
    ##### 17:367' 24"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390639 "Riproduci nel player")
    
    #### Donatella Poretti
    
    [![Altri eventi con oratore Donatella Poretti](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/donatella-poretti "Altri eventi a cui ha partecipato Donatella Poretti")  
    
    senatrice (PD - Radicali)
    
    ##### 17:447' 34"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390640 "Riproduci nel player")
    
    #### Marco Beltrandi
    
    [![Altri eventi con oratore Marco Beltrandi](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/marco-beltrandi "Altri eventi a cui ha partecipato Marco Beltrandi")  
    
    deputato (PD - Radicali)
    
    ##### 17:513' 45"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390641 "Riproduci nel player")
    
    #### Enzo Cucco
    
    [![Altri eventi con oratore Enzo Cucco](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/enzo-cucco "Altri eventi a cui ha partecipato Enzo Cucco")  
    
    direttore della Fondazione Sandro Penna di Torino
    
    ##### 17:551' 42"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390642 "Riproduci nel player")
    
    #### Gianni Rossi Barilli
    
    [![Altri eventi con oratore Gianni Rossi Barilli](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/gianni-rossi-barilli "Altri eventi a cui ha partecipato Gianni Rossi Barilli")  
    
    direttore del mensile gay ‘Pride’
    
    ##### 17:579' 20"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390643 "Riproduci nel player")
    
    #### Daniele Nardini
    
    [![Altri eventi con oratore Daniele Nardini](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/daniele-nardini "Altri eventi a cui ha partecipato Daniele Nardini")  
    
    direttore news Gay.it
    
    ##### 18:066' 0"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390644 "Riproduci nel player")
    
    #### Francesco Bilotta
    
    [![Altri eventi con oratore Francesco Bilotta](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/francesco-bilotta "Altri eventi a cui ha partecipato Francesco Bilotta")  
    
    docente di Diritto Privato presso l'Università di Udine, Rete Lenford
    
    ##### 18:127' 43"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390645 "Riproduci nel player")
    
    #### Guido Allegrezza
    
    [![Altri eventi con oratore Guido Allegrezza](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/guido-allegrezza "Altri eventi a cui ha partecipato Guido Allegrezza")  
    
    consigliere dell'Associazione Radicale Certi Diritti
    
    ##### 18:206' 12"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390646 "Riproduci nel player")
    
    #### Paolo Patanè
    
    [![Altri eventi con oratore Paolo Patanè](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/paolo-patane "Altri eventi a cui ha partecipato Paolo Patanè")  
    
    arcigay Sicilia
    
    ##### 18:267' 49"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390647 "Riproduci nel player")
    
    #### Saverio Aversa
    
    [![Altri eventi con oratore Saverio Aversa](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/saverio-aversa "Altri eventi a cui ha partecipato Saverio Aversa")  
    
    ##### 18:345' 30"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390648 "Riproduci nel player")
    
    #### Stefano Risi
    
    [![Altri eventi con oratore Stefano Risi](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/stefano-risi "Altri eventi a cui ha partecipato Stefano Risi")  
    
    ##### 18:395' 24"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390649 "Riproduci nel player")
    
    #### Carlo Santacroce
    
    [![Altri eventi con oratore Carlo Santacroce](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/carlo-santacroce "Altri eventi a cui ha partecipato Carlo Santacroce")  
    
    ##### 18:455' 41"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390650 "Riproduci nel player")
    
    #### Claudia Toscano
    
    [![Altri eventi con oratore Claudia Toscano](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/claudia-toscano "Altri eventi a cui ha partecipato Claudia Toscano")  
    
    ##### 18:504' 22"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390651 "Riproduci nel player")
    
    #### Aldo Brancacci
    
    [![Altri eventi con oratore Aldo Brancacci](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/aldo-brancacci "Altri eventi a cui ha partecipato Aldo Brancacci")  
    
    professore universitario Roma tre
    
    ##### 18:556' 37"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390652 "Riproduci nel player")
    
    #### Andrea Maccarrone
    
    [![Altri eventi con oratore Andrea Maccarrone](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/andrea-maccarrone "Altri eventi a cui ha partecipato Andrea Maccarrone")  
    
    componente del Circolo di cultura omosessuale Mario Mieli
    
    ##### 19:013' 39"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390653 "Riproduci nel player")
    
    #### Mirko Manzi
    
    [![Altri eventi con oratore Mirko Manzi](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/mirko-manzi "Altri eventi a cui ha partecipato Mirko Manzi")  
    
    ##### 19:052' 29"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390654 "Riproduci nel player")
    
    #### Carlo Cremona
    
    [![Altri eventi con oratore Carlo Cremona](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/carlo-cremona "Altri eventi a cui ha partecipato Carlo Cremona")  
    
    presidente di "i Ken ONLUS" di Napoli
    
    ##### 19:076' 37"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390655 "Riproduci nel player")
    
    #### Rita De Santis
    
    [![Altri eventi con oratore Rita De Santis](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/rita-de-santis "Altri eventi a cui ha partecipato Rita De Santis")  
    
    presidente nazionale di "Agedo"
    
    ##### 19:144' 1"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390656 "Riproduci nel player")
    
    #### Francesca Polo
    
    [![Altri eventi con oratore Francesca Polo](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/francesca-polo "Altri eventi a cui ha partecipato Francesca Polo")  
    
    presidente nazionale dell'Associazione Arcilesbica
    
    ##### 19:185' 26"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390657 "Riproduci nel player")
    
    #### Alberto Bignardi
    
    [![Altri eventi con oratore Alberto Bignardi](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/alberto-bignardi "Altri eventi a cui ha partecipato Alberto Bignardi")  
    
    ##### 19:232' 14"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390658 "Riproduci nel player")
    
    #### Sergio Lo Giudice
    
    [![Altri eventi con oratore Sergio Lo Giudice](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/sergio-lo-giudice "Altri eventi a cui ha partecipato Sergio Lo Giudice")  
    
    presidente Commissione diritti e pari opportunità per lgbt comune Bologna
    
    ##### 19:267' 52"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390659 "Riproduci nel player")
    
    #### Luca Amato
    
    [![Altri eventi con oratore Luca Amato](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/luca-amato "Altri eventi a cui ha partecipato Luca Amato")  
    
    coordinatore di Certi Diritti Roma
    
    ##### 19:342' 20"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390660 "Riproduci nel player")
    
    #### Susanna Lollini
    
    [![Altri eventi con oratore Susanna Lollini](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/susanna-lollini "Altri eventi a cui ha partecipato Susanna Lollini")  
    
    ##### 19:362' 5"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390661 "Riproduci nel player")
    
    #### Emanuele Sebastio
    
    [![Altri eventi con oratore Emanuele Sebastio](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/emanuele-sebastio "Altri eventi a cui ha partecipato Emanuele Sebastio")  
    
    ##### 19:3835"
    
*   [![Riproduci nel player](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/button_play_small2.gif)](http://www.radioradicale.it/scheda/288543/convegno-sul-tema-matrimonio-gay-traguardo-di-uguaglianza#int2390662 "Riproduci nel player")
    
    #### Enzo Cucco
    
    [![Altri eventi con oratore Enzo Cucco](http://www.radioradicale.it/sites/www.radioradicale.it/themes/radioradicale/images/application_cascade.gif)](http://www.radioradicale.it/soggetti/enzo-cucco "Altri eventi a cui ha partecipato Enzo Cucco")  
    
    direttore della Fondazione Sandro Penna di Torino
    
    ##### 19:394' 44"
    

Interviene Maria Teresa Meli (Corriere della Sera), Sergio ROvasio (Segretario Associazione Radicale Certi Diritti), Alexander Schuster (Costituzionalista, Avvocatura per i diritti LGBT – Rete Lenford), Gian Mario Felicetti (direttivo Associazione Radicale Certi Diritti), Luigi Pannarale (ordinario di Sociologia del Diritto, Università degli Studi di Bari). Saluto dell’ Avv. Antonio Rotelli (Presidente Avvocatura per i diritti LGBT - Rete Lenford), Giuliano Federico (direttore editoriale GAY.tv), Anna Paola Concia (PD), Barbara Pollastrini (PD), Antonio Paravia (PDL), Beatrice Lorenzin (PDL), Rita Bernardini (Radicali-Pd), Marco Beltrandi (Radicali-Pd), Marco Perduca (Radicali-Pd), Donatella Poretti (Radicali-Pd), Fabio Evangelisti (IDV), Gianni Vattimo (eurodeputato IDV), Alessandro Zan (Sinistra e Libertà), Franco Grillini (direttore Gay Net), Gianni Rossi Barilli (direttore del mensile gay ‘Pride’). Conclusioni: Enzo Cucco (direttore Fondazione Sandro Penna – Torino), Francesco Bilotta (Rete Lenford, Frank Semenzi – editore Pride), Fabio Canino (artista), Aldo Brancacci (professore universitario Roma tre), Vladimir Luxuria (artista), Don Franco Barbero, Emiliano Zaino, Imma Battaglia (DiGayProject), Enrico Oliari (GayLib), Saverio Aversa, Ivan Scalfarotto (PD), Gianni Rossi Barilli, Paolo Patanè, Mario Cirrito, Luca Trentini, Christian Poccia, Cristiana Alicata, Alessandra Russo, Carlo Santacroce, Fabrizio Marrazzo, Guido Allegrezza, Daniele Priori, Pasquale Quaranta, Maria Gigliola Toniollo, Chiara Lalli, Andrea Maccarrone, Sergio Lo Giudice, Federico Boni, Emanuele Sebastio, Daniele Ventrelli,