---
title: 'Invito per giovedì 18 marzo dalle 19,30 al Caffè Letterario - Roma'
date: Mon, 15 Mar 2010 08:49:12 +0000
draft: false
tags: [Senza categoria]
---

Care e cari,

come sapete la campagna elettorale è entrata ormai nel vivo, ora più che mai, per chi mi vuole sostenere, occorre organizzare  tra amici, parenti, amanti, conoscenti, vicini di casa, un vero e proprio passaparola.

Si voterà domenica 28 e lunedì 29 marzo. Oltre a votare per la candidata alla guida della Regione, Emma Bonino, si vota anche per una delle liste della coalizione mettendo a fianco il cognome del candidato. Nel mio caso: Lista Bonino - Pannella con a fianco la preferenza a ROVASIO.

Ce la posso fare davvero ma solo con l'aiuto di chi crede con me in un vero cambiamento anche sul piano dei diritti civili. Non voglio dire semplici parole di circostanza, preferisco la sincerità e la forza delle idee che vincono sempre!

. \- Per conoscere la mia vita politica, il mio programma elettorale e tutto ciò che ci riguarda la mia campagna puoi andare al sito: [www.sergiorovasio.it](http://www.sergiorovasio.it/).

**Spero di potervi incontrare tutte/i Giovedì 18 marzo dalle 19,30 in poi, presso il Caffè Letterario (Via Ostiense, 95 Roma), ci sarà un aperitivo con buffet a sostegno della mia candidatura.  Cercheremo di fare una serata non noiosa, con musica, video, interventi.**

Grazie ed a prestissimo. Un caro abbraccio,

Sergio

**_Sergio Rovasio_**_, candidato nella Lista Bonino-Pannella nella Circoscrizione Roma e Provincia per la Regione Lazio._