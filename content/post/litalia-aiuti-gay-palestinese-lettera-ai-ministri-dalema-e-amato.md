---
title: 'L ITALIA AIUTI GAY PALESTINESE: LETTERA AI MINISTRI D ALEMA E AMATO'
date: Thu, 27 Mar 2008 20:18:52 +0000
draft: false
tags: [AMATO, Comunicati stampa, gay, palestinese, Dalema]
---

25/03/08

L’ITALIA AIUTI L’INGEGNERE GAY PALESTINESE, CONCEDA L’ASILO POLITICO QUALORA GLI VENISSE NEGATO DA ISRAELE. IL RISCHIO E’ CHE TRA UN MESE VENGA DEPORTATO A JENIN DOVE RISCHIA LA MORTE PERCHE’ GAY E PERCHE’ COMPAGNO DI UN ISRAELIANO.

Lettera aperta di Sergio Rovasio, Segretario Associazione radicale Certi Diritti, al Ministro degli Esteri Massimo D’Alema e al Ministro degli Interni Giuliano Amato:

“Onorevoli Ministri,

la vicenda dell’ingegnere gay palestinese di Jenin, che dopo 8 anni di richieste è stato accolto per un mese nello Stato di Israele grazie all’intervento del Generale dell’esercito Yosef Mashlav, è una notizia che merita attenzione, grande rispetto e che dovrebbe essere di insegnamento alle burocrazie europee sempre indifferenti a casi simili.  
Ora per lui sarà possibile vivere con il suo compagno israeliano, malato di cuore, anche se solo per un mese. Occorre muoversi per scongiurare il rischio che venga rispedito a Jenin dove lo attenderebbe la morte certa. Chiediamo il vostro intervento per scongiurare questa possibilità. Come sapete, Signori Ministri, nei territori palestinesi essere gay e avere una relazione con un israeliano è molto pericoloso, si rischia la vita a causa del proprio orientamento sessuale con l’aggravante dell’accusa di collaborazionismo con gli israeliani.  
Le associazioni lgbt Aguda di Tel Aviv, la Open House di Gerusalemme e Al Qaws (Arcobaleno), composte da volontari palestinesi e israeliani, fanno sforzi quotidiani per aiutare i palestinesi gay che, clandestinamente, raggiungono Israele per sfuggire alla diffusissima omofobia. Centinaia di palestinesi, costretti alla clandestinità, per sopravvivere, si prostituiscono nella zona della stazione dei pullman di Tel Aviv, luogo dove, di notte, i volontari di Aguda, offrono loro assistenza e generi di conforto. La vicenda dell’ingegnere palestinese di 43 anni merita aiuto e sostegno, vi chiediamo quindi di intervenire per offrire protezione e aiuto a quest’uomo che rischia tra un mese di dover tornare a Jenin. Del resto si tratta di applicare la Convenzione Onu del 1951 che prevede il riconoscimento del diritto d’asilo anche a chi è perseguitato a causa del suo orientamento sessuale. Ci auguriamo che il Governo italiano dimostri sensibilità ad una vicenda così delicata”.  
Cordiali saluti,  
Sergio Rovasio