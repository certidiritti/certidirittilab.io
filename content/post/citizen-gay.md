---
title: 'Citizen gay'
date: Fri, 23 Apr 2010 19:01:13 +0000
draft: false
tags: [Senza categoria]
---

**[**_![immagine_citizen_gay](http://www.certidiritti.org/wp-content/uploads/2009/11/immagine_citizen_gay.gif)_**](http://bioetiche.blogspot.com/2008/01/intervista-vittorio-lingiardi-su.html)**

Un libro da non perdere per chi si batte per i diritti delle persone LGBT(E): "Citizen gay. Famiglie, diritti negati e salute mentale" di Vittorio Lingiardi. [**Intervista a Vittorio Lingiardi su** _**Citizen gay**_](http://bioetiche.blogspot.com/2008/01/intervista-vittorio-lingiardi-su.html)

Vittorio Lingiardi è psichiatra, psicoanalista, docente della Facoltà di Psicologia 1 della "Sapienza". Ha  pubblicato un libro dal titolo [**Citizen Gay. Famiglie, diritti negati e salute mentale**](http://www.saggiatore.it/index.php?page=boo.detail&id=bk070148) (Il Saggiatore, Milano). Già nel titolo emergono alcuni temi centrali della sua riflessione... [**Leggi tutta l'intevista**](http://bioetiche.blogspot.com/2008/01/intervista-vittorio-lingiardi-su.html)

[http://bioetiche.blogspot.com/2008/01/intervista-vittorio-lingiardi-su.html](http://bioetiche.blogspot.com/2008/01/intervista-vittorio-lingiardi-su.html)