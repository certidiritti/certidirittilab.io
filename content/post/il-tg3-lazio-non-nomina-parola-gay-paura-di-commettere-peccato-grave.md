---
title: 'IL TG3 LAZIO NON NOMINA PAROLA GAY: PAURA DI COMMETTERE PECCATO GRAVE?'
date: Thu, 31 Jul 2008 13:47:06 +0000
draft: false
tags: [Comunicati stampa]
---

**IL TG3 REGIONALE  DEL LAZIO PUR DI NON NOMINARE LA PAROLA GAYVILLAGE ANNUNCIA UN'INIZIATIVA IN UN PARCO DI CUI NESSUNO CONOSCE L'ESISTENZA. STIANO TRANQUILLI, SE SI PRONUNCIA LA PAROLA GAY NON SI COMMETTE PECCATO.**

Dichiarazione di Sergio Rovasio, Segretario Associaizone Radicale Certi Diritti: 

"Il Telegiornale Tg3 Lazio, a conclusione della sua edizione di oggi, giovedì 31 luglio, nell'elencare alcuni eventi della serata nella città di Roma, ha dato appuntamento per uno spettacolo di Cinzia Leone alle ore 22 nel Parco del Ninfeo. L'evento si tiene al GayVillage, all'Eur, e sarebbe stato molto più semplce indicare questo come luogo dell'evento visto che è abitualmente frequentato da mgiliaia di persone che ne conoscono l'ubicazione, mentre quasi nessuno sa dove sia il Parco del Ninfeo.

Di cosa ha paura il Tg3 del Lazio? Che a nominare la parola gay si commetta peccato grave? Stiano tranquilli, solo chi pratica commette peccato, chi commette semplice atto di ipocrisia è salvo per l'eternità".