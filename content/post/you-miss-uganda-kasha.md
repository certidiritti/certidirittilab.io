---
title: 'You miss Uganda, Kasha'
date: Wed, 23 Mar 2011 17:41:12 +0000
draft: false
tags: [Africa]
---

In occasione della sua visita a Brescia, Listalesbica.it pubblica un'intervista di Manuela Fazia a Kasha, ragazza ugandese che, insieme al nostro iscritto David Kato Kisule e Pepe di Sexual Minorities Uganda, aveva vinto la causa contro il giornale 'Rolling Stone' che aveva pubblicato le foto di 100 gay e lesbiche chiedendone l'impiccagione.

Domenica 21 marzo, io ed altre ragazze de Pianeta Viola di Brescia andiamo a dare un caloroso saluto alla nostra amica ugandese Kasha che, in questo periodo di “esilio” dal suo Paese, sta trascorrendo un paio di giorni in una città italiana del Nord.

Il Pianeta Viola aveva invitato questa coraggiosa ragazza in Italia lo scorso maggio.Nel corso delle 2 settimane della sua permanenza, Kasha aveva partecipato a vari incontri in diverse città italiane, organizzati dal Pianeta Viola in collaborazione con associazioni lgbt e con Amnesty International.  
Kasha era venuta a raccontarci del famigerato disegno di legge “antigay” ed ora a distanza di alcuni mesi, pur non essendo (ancora?) quel disegno stato approvato, la situazione è precipitata.

L'appuntamento è in una trattoria del centro.  
Ha un gran guizzo di gioia quando ci vede arrivare. Avevo incontrato Kasha l'ultima volta a Kampala lo scorso gennaio, poco prima che ammazzassero David.  
La guardo, è magrissima. “E' lo stress” mi dice.

Ci racconta che l'odio che ha portato all'uccisione di David sembra avvitarsi in una spirale senza fine.Sono entrati in casa sua ed era chiaro che non si trattava di un furto. Volevano lei, ma fortunatamente in quel momento Kasha non era lì. I suoi vicini di casa sono andati via, troppo pericoloso rimanere a vivere vicino la sua abitazione.

Kasha, insieme a David e Pepe, è uno dei principali bersagli dell'onda omofobica ugandese. Proprio loro tre, infatti, avevano vinto ad inizio anno la causa contro il “Rolling stones”, un giornale scandalistico che aveva pubblicato foto ed informazioni private di tante lesbiche e gay.

All'inizio di febbraio, su energico invito di organizzazioni internazionali tra cui Amnesty International, Kasha ha precipitosamente lasciato l'Uganda. Queste stesse organizzazioni internazionali ora stanno aiutando in loco decine di lesbiche e gay i cui volti sono stati inquadrati dalle televisioni durante i funerali di David e proprio per questo sono perseguitati. Per proteggerli occorre trovare alloggi più sicuri, che vuol dire trasferirli in villaggi diversi da quelli in cui vivono.

Kasha parla quasi a bassa voce. Tensione e preoccupazioni erano evidenti nel suo sguardo anche quando ci eravamo viste a Kampala. Ora si è aggiunta la tristezza.

Farug, l'associazione lesbica di cui Kasha è presidente, è stata costretta a chiudere la propria sede dopo ripetuti attacchi a colpi di pietre e bastoni. L'attività dell'associazione continua, ma con riunioni fatte in segreto nottetempo.

Artefice di queste aggressioni è la gente comune, mentre la polizia fa da spettatore. Il governo infatti si muove seguendo una doppia linea di condotta. Da un lato non attacca direttamente la comunità lgbt per non rischiare di compromettere i rapporti con la comunità internazionale, dall'altro preme perchè il disegno di legge “antigay”, fortemente caldeggiato dalla maggioranza della popolazione, venga approvato.

[www.listalesbica.it](http://www.listalesbica.it)