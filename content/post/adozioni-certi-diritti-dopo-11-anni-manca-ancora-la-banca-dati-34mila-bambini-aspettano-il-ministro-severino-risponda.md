---
title: 'Adozioni, Certi Diritti: dopo 11 anni manca ancora la banca dati. 34mila bambini aspettano, il ministro Severino risponda'
date: Wed, 11 Jan 2012 20:56:32 +0000
draft: false
tags: [Diritto di Famiglia]
---

Roma, 11 gennaio 2011  
   
Comunicato Stampa dell'Associazione Radicale Certi diritti

I Radicali, attraverso il Senatore Marco Perduca, hanno depositato il 16.06.2011 l'interrogazione S.4/05426 \[Istituzione della banca dati relativa ai minori dichiarati adottabili e ai coniugi aspiranti all'adozione nazionale e internazionale\]. L'istituzione della banca dati in questione, prevista dall'articolo 40 della legge n. 149 del 2001, doveva avere sede presso il Ministero della giustizia entro e non oltre centottanta giorni dalla data dell'entrata in vigore della legge del 2001, con indicazione di ogni informazione atta a garantire il miglior esito del procedimento.

Solo nel 2004, quindi già al di fuori dei tempi legali, arriva il decreto che emana le norme di attuazione e di organizzazione della banca dati. Passano altri sette anni e, ad oggi, sul sito Internet del Ministero della giustizia alla pagina "Banca dati dei minori adottabili" è scritto "L'applicativo è stato sviluppato e si è conclusa la fase di test funzionali presso le due sedi di Tribunale dei minorenni di Torino e di Bari. È prossima l'attivazione della fase di collaudo per poter procedere alla diffusione sull'intero territorio nazionale".

Quello che chiediamo, sollecitando la risposta all'interrogazione da parte del Ministro Paola Severino, è quali iniziative si intendono prendere per porre rimedio al ritardo di undici anni nell'istituzione della banca dati in questione.

Fedeli al motto einaudiano "conoscere per deliberare" riteniamo la banca dati dei minori dichiarati adottabili e dei coniugi aspiranti all'adozioni uno strumento di fondamentale importanza per la legiferazione da parte del nostro Parlamento. Conoscere i numeri dei bambini attualmente in istituti, case famiglia ed affidamento temporaneo, ossia quei "figli delle sentenze" in balia dei tribunali rappresenta oggi una realtà scomoda per la politica e per il clericalismo istituzionale che si troverebbero costretti a far fronte all'accetazione di una realtà imbarazzante ed al di fuori dei propri dogmi e slogan. Conoscere i numeri di questi bambini permetterebbe finalmente di aprire un serio dibattito sulle adozioni ai single.

A chi continua a nascondere la presenza di 34.000 minorenni in attesa di essere adottati sul territorio nazionale chiediamo: un istituto da più amore ad un bambino di una persona single?