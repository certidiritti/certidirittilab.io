---
title: 'unioni civili in consiglio comunale a torino'
date: Thu, 10 Jun 2010 19:54:59 +0000
draft: false
tags: [Senza categoria]
---

IL CONSIGLIO COMUNALE DISCUTERA’ DELLA PROPOSTA DI INIZIATIVA POPOLARE PER LA PARITA’ DI DIRITTI E DOVERI TRA UNIONI CIVILI E MATRIMONIALI. UN PRIMO PASSO: NON E’ LA RIVOLUZIONE, MA UNA RIFORMA. INVIATIAMO TUTTE LE FORZE POLITICHE A NON FARE IDEOLOGIA MA A RAGIONARE SULLA REALTA’ DELLE NUOVE FAMIGLIE.

Dichiarazione di Enzo Cucco, Associazione radicale Certi Diritti, Torino

Il Consiglio comunale discuterà, speriamo a breve, la proposta di deliberazione comunale per la quale tutto l’associazionismo laico, radicale, delle donne e delle persone lgbt torinesi ha raccolto migliaia di firme. La proposta chiede una cosa semplice: che il Comune guardi in faccia alla realtà delle famiglie torinesi e che verifichi IN TUTTE LE AREE DI SUA COMPETENZA, e solo in quelle, l’equiparazione di accesso ai diritti (ed ai doveri) anche per quelle unioni che matrimonio non posso essere.

Noi le abbiamo definite unioni civili, perché il diritto, in particolare il Regolamento anagrafico (che è legge dello stato) già definisce “famiglia” anche quelle unioni affettive non matrimoniali. Ma è un dato di fatto che queste unioni, anzi queste famiglie, sono prese in considerazione dall’amministrazione comunale solo nel caso dell’accertamento degli obblighi di assistenza. Producendo così una situazione anomala: le unioni civili esistono di fronte ai doveri, ma non esistono per l’accessa ai diritti.

L’Associazione radicale certi diritti si augura che questo primo passo, moderatamente ma significativamente riformista, possa essere assunto consapevolmente dalla Città che si vanta (a ragione) di essere all’avanguardia nel panorama nazionale in materia di politiche sulla famiglia e diritti delle persone omosessuali. E che diventi anche una occasione per discutere nel merito, e sul serio, su come si è evoluta la famiglia nel nostro paese e sul ruolo che le amministrazioni pubbliche devono e possono assumere per garantire servizi e assistenza SENZA DISCRIMINAZIONI.