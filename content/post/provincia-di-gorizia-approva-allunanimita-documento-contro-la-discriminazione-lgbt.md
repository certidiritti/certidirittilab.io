---
title: 'Provincia di Gorizia approva all''unanimità documento contro la discriminazione lgbt'
date: Mon, 18 Jun 2012 22:27:45 +0000
draft: false
tags: [Politica]
---

comunicato stampa dell'associazione radicale Certi Diritti, Rete Lenford e Circolo Arcigay Trieste

Trieste, 14 giugno 2012

Mercoledì 13 giugno la Giunta della Provincia di Gorizia ha approvato all’unanimità una delibera avente per oggetto il “Contrasto alla discriminazione basata su orientamento sessuale e identità di genere”.  
La Provincia di Gorizia sollecita il Parlamento italiano ad approvare senza ulteriore indugio una norma che estenda il diritto di sposarsi alle coppie di persone dello stesso sesso.

Inoltre la Giunta ha deliberato di aderire a RE.A.DY (Rete nazionale delle pubbliche amministrazioni anti-discriminazioni per orientamento sessuale e identità di genere); di promuovere, a sostegno delle famiglie, politiche e singole iniziative che non producano effetti discriminatori nei confronti di chi non può o non voglia contrarre matrimonio, e tuttavia conviva per vincolo affettivo; di attivare forme di consultazione stabile e formale tra la Provincia di Gorizia e le organizzazioni LGBT presenti sul territorio per valorizzarne le attività, sviluppare percorsi formativi e iniziative comuni.

Infine la Giunta ha dato avvio ad una serie di azioni positive, denominate “DIRITTI CIVILI PER TUTTI”, tra cui la realizzazione di una serie di incontri, convegni e giornate di studio, per l’approfondimento delle tematiche giuridiche e normative, legate ai diritti delle persone omosessuali e transgender e delle coppie omosessuali, il primo dei quali è stato già fissato per il prossimo 29 giugno e il riconoscimento formale, da parte della Provincia, del legame affettivo delle coppie formate da persone dello stesso sesso, un modo, come afferma la delibera stessa «per dimostrare la sensibilità della pubblica amministrazione e per rompere il muro di omofobia che cresce nel silenzio e nella solitudine delle persone».  
Le scriventi associazioni , che hanno contribuito quasi integralmente alla stesura del documento, esprimono grande soddisfazione per questa presa di posizione che va nella direzione concreta di abbattere le discriminazioni basate sull’orientamento sessuale e le identità di genere, sottolineando che le Istituzioni, quando vogliono agire possono farlo e anche con rapidità.

Le stesse Associazioni auspicano una analoga velocità, determinazione e compattezza politica per l’approvazione, al consiglio comunale di Trieste, della mozione avente per oggetto: “Politiche antidiscriminatorie” che il consigliere Faraguna ha depositato il 17 maggio scorso. La mozione in questione è in gran parte mutuata dalla proposta che le 3 associazioni hanno presentato in forma pubblica a novembre dello scorso anno.7

Clara Comelli Associazione radicale Certi Diritti  
Patrizia Fiore Avvocatura per i diritti lgbt – Rete Lenford  
Davide Zotti Circolo Arcobaleno Arcigay Arcilesbica di Trieste