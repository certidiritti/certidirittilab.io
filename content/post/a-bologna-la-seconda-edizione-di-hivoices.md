---
title: 'A Bologna la seconda edizione di HIVoices'
date: Thu, 10 Mar 2011 13:07:10 +0000
draft: false
tags: [arcigay, bologna, cassero, Comunicati stampa, hiv, positivo, salute, sesso]
---

Il 6 7 e 8 maggio il Settore Salute di Arcigay Bologna 'Il Cassero' ripropone 'HIVoices': laboratorio residenziale su sieropositività e identità sessuale.

Omosessualità e sieropositività: due realtà accomunate fin dall’inizio della comparsa del virus HIV nei primi anni ’80 del secolo scorso. E ancora, dopo tutto questo tempo, essere sieropositivi è difficile: una dimensione di vita 'invisibile', un ‘segreto’ da non svelare, spesso e volentieri nei locali lgbtq, nei luoghi di incontro a scopi sessuali, nelle associazioni, in chat.

Per questo è nato HIVoices. Un'esperienza atipica nel panorama lgbtq, un progetto del Settore Salute di Arcigay Bologna ‘Il Cassero’ per favorire i partecipanti nell’acquisizione di strumenti per 'inventare benessere', valorizzando le proprie capacità individuali, in particolare nell’affermazione e accettazione di sé in quanto persona omo/bisessuale che vive con HIV.

HIVoices è già stato realizzato come progetto pilota lo scorso 24-26 settembre 2010, con un gruppo di 26 persone sieropositive e omo/bisessuali. La forte motivazione dei partecipanti e il bisogno di appartenenza al gruppo di pari, emersi durante il laboratorio, hanno confermato l'originalità della proposta formativa e la necessità di riproporlo per una seconda edizione con un nuovo gruppo di partecipanti.

QUANDO: venerdì 6, sabato 7 e domenica 8 maggio 2011.

DOVE: in una struttura attrezzata per gruppi residenziali sull’Appennino romagnolo.

COSTO: 10 € a testa.

SCADENZA ISCRIZIONI: è possibile iscriversi fino a martedì 26 aprile 2011.

INFO ED ISCRIZIONI: [salute@cassero.it](mailto:salute@cassero.it)

**[Per maggiori informazioni visita il sito del Cassero Salute >](http://www.casserosalute.it/campagne_iniziative_hivoices_2-d-278.html)**