---
title: '"La speranza di un giovane omosessuale in Libano è..." Intervista a Rana di Helem'
date: Mon, 24 Jun 2013 07:33:27 +0000
draft: false
tags: [Africa]
---

Luca Fortis e Dario Vese dell'Associazione Radicale Certi Diritti hanno intervistato Rana, membro del comitato amministrativo di Helem, Ong libanese che lavora per il miglioramento delle condizioni degli LGBTI locali. La traduzione è a cura di Luca Fornasari

**Quali motivi vi hanno spinto a creare Helem?**

L'art. 534 del codice penale libanese punisce "tutti gli atti sessuali contro natura", ed è utilizzato per condannare l'omosessualità.

Helem è nata dall'iniziativa di alcuni giovani omosessuali. Nel 1998 un piccolo gruppo sorto su internet, "gay Lebanon", ha dapprima iniziato a raccogliere intorno a sé giovani libanesi omosessuali, poi, in un secondo momento, ha deciso d'incontrarsi di persona. Prende così vita "club free", uno spazio semplice, senza grosse ambizioni, i cui membri sono esclusivamente omosessuali.

Dopo anni di duro lavoro, nel 2004, "club free" si trasforma in "Helem", la prima organizzazione LGBT riconosciuta in Libano, in Medio – oriente e Africa del nord.

**Cosa fa l'organizzazione?**

Le azioni di Helem sono volte prevalentemente all'abolizione dell'art.534 del codice penale libanese, e al rinforzo della coscienza degli individui LGBT.

L'organizzazione ha sede in un centro sociale all'interno del quale i membri possono trovarsi, avere uno spazio di libertà, ma anche incontri guidati per discutere sui temi della comunità LGBT. Helem si occupa anche di pubblicazioni a carattere legale, scientifico, guide di buona salute, riviste ecc... la più recente di queste è uno studio di Nizar Saghiyeh e Ghida Franjieh che forniscono uno strumento di difesa legale contro l'accusa per l'art 534.

Il centro, inoltre, mette a disposizione un assistente sociale per aiutare coloro che attraversano un periodo di confusione, e aiuti medici in collaborazione con organismi più specializzati.

**Qual è la vita della comunità LGBT in Libano?**

Come già detto il codice penale condanna l'omosessualità, per tale motivo le persone vivono la loro sessualità timidamente e in modo segreto. Allo stesso modo la società è molto omofoba, e di conseguenza si fa fatica a rivelare la propria identità sessuale per paura di essere marginalizzati o rifiutati dai familiari.

Di contro esistono luoghi di socializzazione per gli LGBT, il più importante dei quali è proprio Helem, uno spazio che facilita la comunicazione ma soprattutto il duro lavoro dei militanti per i diritti dei LGBT. In questo senso la nostra associazione, i media e gli attivisti in rete (blogger o gruppi su facebook), hanno contribuito in quest'ultimo periodo a creare un clima più piacevole in Libano. Ma c'è ancora molto lavoro da fare per cercare di cambiare le leggi in favore degli individui LGBT, e, soprattutto, per educare la società ad accettare le differenze.

**Esistono luoghi pubblici per incontri culturali o per divertirsi?**

Prima di tutto c'è il centro sociale Helem, uno spazio aperto a tutte le persone, un luogo in cui ogni individuo è rispettato senza che gli siano mai poste domande sulla sua identità di genere o sull'orientamento sessuale, insomma tutti si devono sentire liberi. C'è anche Meem uno spazio riservato alle persone LBT (le donne della comunità) affinché si possano sentire più a loro agio. Infine ci sono diversi locali e discoteche che sono amicali con gli individui e la comunità LGBT.

**La cultura (letteratura, poesia, arte in generale) mediorientale, storicamente, parla di omosessualità?**

L'omosessualità e la transessualità risalgono ai tempi dei faraoni e dei fenici. Dal 1970, circa, il tema è tornato timidamente alla ribalta soprattutto nelle serie Tv e nella letteratura.

**In Libano esiste una visione differente dell'omosessualità tra la comunità islamica e quella cristiana? O i problemi sono gli stessi?**

I problemi con le comunità religiose sono, più o meno, gli stessi, le guide spirituali di tutte le religioni invitano i credenti a rifiutare l'omosessualità.

**Helem ha un dialogo con gli attori politici o istituzionali in generale?**

Attualmente non abbiamo un dialogo continuo con nessun attore politico

**Che cosa possono fare le associazioni LGBT europee al fine d'incoraggiare l'iniziativa di Helem?**

Ogni tipo di supporto da parte di associazioni europee LGBT è il benvenuto. Abbiamo già dei contatti internazionali, ma apprezzeremo tutti i nuovi contatti, così come saremo felici di poter ricevere i nuovi studi o i progressi riguardanti il mondo LGBT. Siamo altresì disponibili alla collaborazione per progetti volti a rinforzarci.

**Qual è la vostra prossima iniziativa?**

Al momento stiamo lavorando per abolire la terapia di conversione che si vende ancora in Libano, vorremmo provare a creare una pressione sugli psicologi/psichiatri affinché esprimano un parere deciso contro il problema. Proprio come la pressione che abbiamo fatto l'anno scorso sull'ordine dei medici riuscendo a vietare i test anali (che si diceva servissero per dimostrare l'omosessualità).

Ci stiamo adoperando, inoltre, per pubblicare una nuova guida per aiutare gli LGBT a saper reagire in caso di arresto, e più in generale su come possano difendere i loro diritti e i mezzi attraverso i quali possiamo aiutarli.

Ben inteso continuiamo sempre il nostro lavoro al centro aiutando a rinforzare la consapevolezza delle persone.

**Come si finanzia Helem? Come si può apportare un contributo? Con l'iscrizione?**

Helem si finanzia attraverso i contributi dei membri, i doni e gli aiuti di organizzazioni straniere. Abbiamo un abbonamento annuale per i membri, e accettiamo delle donazioni economiche dall'esterno, ma è importante rilevare come i benefattori non intervengano nelle politiche dell'organizzazione, e soprattutto, non ammettiamo offerte che provengono da partiti che supportano organizzazioni che non rispettano i diritti dell'uomo.

**Harvey Milk ha introdotto il concetto di speranza nella lotta politica della comunità LGBT. Qual è, oggi, la speranza di un giovane omosessuale in Libano?**

La speranza di un giovane omosessuale in Libano è prima di tutto quella di essere accettato dalla sua famiglia, e poi dalla società...la speranza di un giovane omosessuale è di essere protetto dalla legge..., la grande speranza di un giovane omosessuale in Libano è riuscire a vivere la sua vita normalmente e apertamente...