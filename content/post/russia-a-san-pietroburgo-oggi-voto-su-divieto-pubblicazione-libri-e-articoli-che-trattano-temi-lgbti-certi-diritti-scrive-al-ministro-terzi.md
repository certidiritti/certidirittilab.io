---
title: 'Russia: a San Pietroburgo oggi voto su divieto pubblicazione libri e articoli che trattano temi Lgbti. Certi Diritti scrive al ministro Terzi'
date: Wed, 08 Feb 2012 13:15:39 +0000
draft: false
tags: [Russia]
---

L'Associazione Radicale Certi Diritti scrive al Ministro degli Affari Esteri della Repubblica Italiana, Giulio Maria Terzi di Sant'Agata, per chiedere di agire in tutti i modi possibili contro un proposta di legge che renderebbe illegale scrivere un libro, pubblicare un articolo o parlare in pubblico di omosessualità e che domani potrebbe essere approvata a San Pietroburgo.

Roma, 8 febbraio 2012  
   
Comunicato Stampa dell'Associazione Radicale Certi Diritti

Gli attivisti LGBTI russi di San Pietroburgo denunciano che oggi la città potrebbe approvare una legge che renderebbe illegale scrivere un libro, pubblicare un articolo o parlare in pubblico di omosessualità. Se la legge passasse a San Pietroburgo c’è il rischio che venga approvata anche a livello nazionale. Gli attivisti stanno organizzando una manifestazione di protesta, ma chiedono l’aiuto della comunità internazionale.  
   
Consapevole del rischio per milioni di persone di essere rese invisibili da un tratto di penna vedendo così cancellata la loro esistenza e la loro dignità, l’Associazione radicale Certi Diritti scrive al Ministro degli Affari Esteri della Repubblica Italiana, Giulio Maria Terzi di Sant'Agata per chiedere di agire in tutti i modi possibili per impedire l’approvazione di una legge volta chiaramente a criminalizzare qualunque attività o informazione relativa alle persone LGBTI e alle relazioni tra persone dello stesso sesso, in patente violazione delle libertà di espressione e associazione nonché degli impegni presi dalla Russia ratificando la Convenzione europea per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali. Chiederemo anche l'intervento del Consiglio d'Europa di cui la Russia fa parte.