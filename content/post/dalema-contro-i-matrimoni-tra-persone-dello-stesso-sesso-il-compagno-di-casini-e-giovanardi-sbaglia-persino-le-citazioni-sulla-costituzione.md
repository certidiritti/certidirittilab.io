---
title: 'D''Alema contro i matrimoni tra persone dello stesso sesso: il compagno di Casini e Giovanardi sbaglia persino le citazioni sulla Costituzione'
date: Tue, 13 Sep 2011 11:49:46 +0000
draft: false
tags: [Matrimonio egualitario]
---

D'Alema sul matrimonio gay: soliti pregiudizi, solita ignoranza e supponenza. Il mito del politico brillante e cattivo si colora di patetico di fronte alla realtà che fa finta di non vedere. Bersani che dice? E' ora di cambiare questa classe dirigente.

Roma, 13 settembre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Massimo D'Alema ha di nuovo dato il peggio di se in una illuminante intervista con Zoro. L'intervento di D'Alema, che **cita a sproposito la Costituzione italiana per esprimere la sua contrarietà al matrimonio tra persone  dello stesso sesso**, non è altro che la sbiadita fotocopia delle "brillanti" posizioni (e pregiudizi) sostenute dalla maggioranza del sistema partitocratico italiano sui diritti delle persone omosessuali in Italia. Talmente brillanti e "strategiche" che su questi  temi non hanno fatto nulla, esattamente come i Governi di destra.  
   
Mister baffetto non fa altro che alimentare l’incapacità di costruire strategie vincenti, e far arrendere alla **disperata realpolitik, la stessa del sostegno al regime sanguinario di Gheddafi**, di quando al Pd arrivò all’ultimo momento l’ordine (vediamo chi indovina da chi?) di votare in favore dell’accordo con Gheddafi, o come la tanto propagandata - e mai realmente voluta - legge sul conflitto di interessi. **Questo grande genio della politica opera solo in funzione dell'accordo con i fondamentalisti vaticani, divenuti ormai l’unica possibile ancora di salvezza**, perchè si deve pure sopravvivere nel sistema che ha permesso a  Berlusconi di governare per quasi un ventennio.  
   
Quelle tesi, invece, oltre a rappresentare il solito tic retorico d'alemiano, utile a rafforzare il logoro mito di un politico cinico e ‘senza paura’, svelano nel miglior modo possibile il guaio di una sinistra italiana che deve combattere con una zavorra culturale (prima che politica) che D'Alema rappresenta al meglio.

Una volta i comunisti pensavano che gay e femministe  rappresentavano un falso problema, che i loro problemi si sarebbero risolti automaticamente con l'avvento del socialismo e della dittatura del proletariato. Erano problemi sovrastrutturali, piccolo borghesi.  
   
Oggi la dittatura del proletariato è sostituita da Casini e Giovanardi, la coppia di fatto dell'oscurantismo cattolico. E si usa l'arma del ridicolizzare le posizioni di chi disturba i manovratori: **chiedere il matrimonio civile (civile D'Alema, civile, nessuno ha mai chiesto quello religioso, esattamente come capita in tutti i Paesi europei) e mettere al centro dell'agenda le Riforme di questo Paese, anche quelle che riguardano i diritti e le libertà individuali (al centro D'Alema, al centro, e non al primo posto come lei vorrebbe far credere), disturba le "superiori strategie" di chi è disposto a negare la realtà per raggiungere il potere**, quello più becero, partitocratico, colluso e in simbiosi con la casta clericale e vaticana.  
   
La realtà è fatta di **centinaia di migliaia di coppie e di famiglie lgbt, che SONO famiglie, proprio come dice la Corte Costituzionale, checchè ne pensi e dica D'Alema, insieme ai suoi oramai compagni Casini e Giovanardi**. E che chiedono la cosa più semplice, così come hanno fatto già Spagna, Belgio, Olanda,Portogallo, (giusto per fare qualche esempio): il matrimonio civile, per non condannarli ad essere cittadini di serie B.

Di migliaia di amministratori locali e dirigenti del PD che concretamente tutti i giorni nell'amministrazione della cosa pubblica tentato di abbandonare il cinismo untuosetto della tradizione dalemiana per sostituirlo con un po’ di sano riformismo socialdemocratico.  
   
Noi siamo al fianco dei tanti "ridicoli e poco seri" dirigenti del PD che invece hanno scelto di sposarsi, ma che, anche grazie ai D'Alema nostrani, questa scelta l'hanno potuta esercitare solo all'estero. E insieme continueremo questa lotta per il superamento delle diseguaglianze.  
   
Non è ora di cambiare? Se non ora quando?

Associazione radicale Certi Diritti

**Certi Diritti ha introdotto 3 anni fa la battaglia per l'uguaglianza dei diritti in Italia.**  
**La campagna di Affermazione Civile per il matrimonio tra persone dello stesso sesso ha portato alla storica sentenza 138/2010 della Corte Costituzionale che:**

**1) ha riconosciuto la dignità costituzionale delle unioni omosessuali**

**2) sollecita il Parlamento a varare una disciplina di carattere generale in materia**

**3) parifica i diritti delle coppie omosessuali ai diritti già riconosciuti alla coppie more uxorio eterosessuali**

**4) si riserva d’intervenire per garantire un trattamento omogeneo tra le coppie coniugate e quelle omosessuali**

**5) bandisce il comportamento omofobico.  
  
**

**SOSTIENI CERTI DIRITTI >>> www.certidiritti.it/iscriviti**