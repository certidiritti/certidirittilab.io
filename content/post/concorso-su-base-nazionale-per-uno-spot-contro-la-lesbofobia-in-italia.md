---
title: 'CONCORSO SU BASE NAZIONALE PER UNO SPOT CONTRO LA LESBOFOBIA IN ITALIA'
date: Fri, 23 Apr 2010 15:34:38 +0000
draft: false
tags: [Comunicati stampa]
---

### 60 SECONDI CONTRO LA LESBOFOBIA

Concorso nazionale per uno spot contro la discriminazione verso le lesbiche indetto dal Circolo Arcilesbica Zami di Milano

**SCADENZA 20 MAGGIO 2010**

Abbiamo pensato che quando si parla di omofobia raramente si include la discriminazione contro le lesbiche. Abbiamo pure pensato che la pubblicità progresso abbia bisogno di progresso. E quel progresso siamo noi, **quel progresso sei tu**.

Ti chiediamo uno spot contro la lesbofobia. Non ci importa se sei donna, uomo, trans, se sei etero o lesbica o gay, nemmeno ci interessa la tua età. Ci importa che tu metta in luce la lesbofobia e ne dichiari la tua contrarietà in 60 secondi. Ci importa sapere che ci sei. E che la battaglia cominci, affiliamo le lame della creatività!

Dacci un po’ del tuo tempo, del tuo ozio, della tua voglia di giocosa serietà; mettili al servizio di una campagna contro la discriminazione verso le lesbiche.

Crediamo che quando si parla di noi dovremmo essere noi a deciderne i modi e le forme. Chi vuol darci voce, che ascolti la nostra voce. Ed è ora di parlare. È ora di superare l’idea che la lesbofobia sia solo un’ingiuria chiara ed esplicita lanciata fuori di un locale. 

È  giunta l’ora, anzi: il minuto! … 60 secondi contro la lesbofobia.

**[Circolo Arcilesbica Zami di Milano](http://www.arcilesbicazami-milano.it/60%20secondi%20contro%20la%20lesbofobia.html)  
**