---
title: '2008-0140-Discrimination-ST13046.EN09'
date: Tue, 08 Sep 2009 15:22:51 +0000
draft: false
tags: [Senza categoria]
---

  

  

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 8 September 2009

Interinstitutional File:

2008/0140 (CNS)

13046/09

LIMITE

SOC 502

JAI 563

MI 325

  

  

  

  

  

NOTE

from :

General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

12792/09 SOC 478 JAI 544 MI 307

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Further to the meeting of the Social Questions Working Party on 7 September 2009, delegations will find attached a note from the Irish delegation.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**1.  Further questions and drafting proposals from Ireland, regarding the Draft Commission Proposal for a Council Directive on Implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation**

**Doc. COM(2008) 426 final**

IRELAND has not yet concluded its detailed examination of the proposal and wishes to maintain its general reservation on the text, pending further consideration.

In the interim, this document poses suggestions for amending the proposal.

Ireland suggests that further reading of this proposal should commence with clarification of the material scope of the proposal, in Article 3. Detailed consideration of the permitted differences in treatment and special measures for the accommodation of persons with disabilities are fundamentally dependent on first having a shared understanding of the areas of activity in which the Community can validly and appropriately act in this regard.

In this document,

\* denotes amendments proposed by the Swedish Presidency in its document of 10/07/2009 (ref 11774/09).

\*\* denotes amendments proposed by the Czech Presidency in its document of 14/05/2009 (ref 9807/09).

text in bold, including strikeout, denotes amendment proposed by Ireland.

  

**INDEX**

**Proposal**

**Amendments to Articles**

**Amendments to Recitals**

1

2(2)(a) and delete 2(3a)

12a

2

2(5)

3

2(6)

4

new 2(6A)

5

new 2(6B)

6

2(7)

15

7

2(8)

8

3(3)

9

new 3(4A)

10

7(2)

11

11

12

28

13

16

  

Proposal (1)

Ireland suggests that the proposed Article 2(3a)[\[1\]](#_ftn1) is deleted and that the wording of Article 2(2) is amended to read as follows:

2.(2)         For the purposes of paragraph 1:

(a) direct discrimination shall be taken to occur -

**(i)** where  one person is treated less favourably than another is, has been or would be treated in a comparable situation, on any of the grounds referred to in Article 1 **which exists, or is imputed to the person concerned, or**

**(ii) where a person who is associated with another person**

**(I) is treated, by virtue of that association, less favourably than a person who is not so associated is, has been or would be treated in a comparable situation, and**

**(II) similar treatment of that person on any of the grounds referred to in Article 1 would, by virtue of _paragraph (i),_ constitute discrimination;**

(b)     indirect discrimination shall be taken to occur where an apparently neutral provision, criterion or practice would put persons of a particular religion or belief, a particular disability, a particular age, or a particular sexual orientation at a particular disadvantage compared with other persons, unless that provision, criterion or practice is objectively justified by a legitimate aim and the means of achieving that aim are appropriate and necessary.

Supplemented by amendment to Recital 12a, as follows:

(12a) In its judgment of 17 July 2008 in Case C-303/06, the Court of Justice has held that the prohibition of direct discrimination and of harassment on the grounds of disability laid down by Directive 2000/78/EC is not limited only to people who are themselves disabled. The Court ruled that such prohibition of discrimination also protects an employee who is not disabled but who is treated less favourably than another employee, or who suffers harassment, on the basis of the disability of their child, whose care is provided primarily by that employee. For reasons of clarity, it is therefore appropriate to provide explicitly for protection from discrimination by association on all grounds covered by this Directive. Such discrimination occurs, inter alia, when a person is treated less favourably **than another person** **not so associated was, is or would be treated**, or harassed, because, in the view of the discriminator, he or she is associated with persons of a particular religion or belief, disability, age or sexual orientation, for instance through his or her family, friendships, employment or occupation.

Explanation

Ireland proposes two amendments to the definition of direct discrimination, to cater for

*   discriminatory treatment of a person due to his or her association with another person, and separately,

*   discriminatory treatment of a person due to his or her perceived characteristics (characteristics imputed to that person).

Text is proposed to apply the judgement of the ECJ in the Coleman case. Treatment of a person due to his or her association with another person who has a protected characteristic, which if directed at the second person would be considered direct discrimination, is itself a form of direct discrimination. The text is drawn from Irish law - Section 3(1)(b) of the Equal Status Act 2000.

Irish law allows complaints to be made independently by the associated person and by the person with the protected characteristic.

Ireland also proposes text to cater for discrimination against a person on the basis of a characteristic which is imputed to him or her as a form of direct discrimination, and suggests an amendment to this effect to Article 2(2)(a), drawn again from Irish law - Section 3(1)(1)(iv) of the Equal Status Act 2000.

Ireland suggests that it is not necessary to make separate provision for harassment 'by association', as the definition of harassment is already broadly stated as "... unwanted conduct _related_ to any of the grounds...".

  

Proposal (2)

Ireland suggests that the wording of Articles 2(5) is amended to read as follows:-

2.(5)  Denial of reasonable accommodation in a particular case as provided for by Article 4(1)(a) of the present Directive as regards persons with disabilities shall be deemed to be discrimination within the meaning of paragraph 1, **unless if, by virtue of another provision of this directive, a refusal or failure to provide the service in question to that person would not constitute discrimination**.

Explanation

Ireland suggests this amendment, to clarify potential conflicts in the proposal.

For example, where a particular good or service is not provided to an individual person with a specific disability under the provision regarding public health and safety, the provider should not be required to provide reasonable accommodation to facilitate the person's access to the good or service.

Proposal (3)

Ireland suggests that the wording of Article 2(6) is amended to read as follows:-

**2.(6)(a)** Notwithstanding paragraph 2, Member States may provide that differences of treatment on **the** grounds of age **and disability** shall not constitute discrimination, if, within the context of national law, they are **objectively and reasonably** justified by a legitimate aim, and if the means of achieving that aim are appropriate and necessary. In particular, this Directive shall not preclude the fixing of a specific age for access to social benefits, education and certain goods or services. **Differences in treatment on the ground of age which are required by or under national law shall not be regarded as discrimination on the age ground.**

**(b) Treating a person who has not attained the age of 18 years less favourably or more favourably than another, whatever that person's age, shall not be regarded as discrimination on the age ground.**

  

Explanation

Ireland suggests that separate provision is made for permitted differences in treatment on the ground of disability, and submits proposal no. (5) below in this regard.

Ireland proposes that, for clarity, provisions relating to positive action are dealt with solely in Article 5. It is suggested that this provision relates to permitting differences in treatment on the ground of age to be justified and sets out clearly the margin of appreciation of Member States in that regard.

The appropriate test is set out by the European Court of Justice in its judgement in the Age Action UK case (Case Ref C388/07), which also sets out that it is for the national court to determine whether difference in treatment is reasonable. Ireland suggests that differences in treatment on the ground of age in national law and practice, which have been arrived at through national democratic processes, should enjoy a presumption of legitimacy. This should also be reflected in an appropriate Recital.

Ireland suggests that illustrative lists of instances which are deemed not to be discriminatory should be moved to the Recitals, as such lists have tended to be prescriptively interpreted as closed lists in transposition and/or infringement proceedings.

Ireland also proposes that the age ground should apply only in respect to differences in treatment between adults. The Commission in its Impact Assessment, and the accompanying EPEC study, does not make a case for the existence of inappropriate differences in treatment on the ground of age between adults and children, or between children of varying ages. The issues identified primarily relate to negative stereotyping of young people and resource allocation issues, which are not appropriate to this type of measure.

  

Proposal (4)

Ireland suggests that Article 2 is amended to include a new Article 2(6A), reading as follows:-

**2.(6A) Differences in treatment of persons on any of the grounds listed in Article 1**

**(a)     in relation to the provision or organisation of a sporting facility or sporting event to the extent that the differences are reasonably necessary having regard to the nature of the facility or event and are relevant to the purpose of the facility or event, or**

**(b)     which are reasonably required for reasons of authenticity, aesthetics, tradition or custom in connection with a dramatic performance or other entertainment, shall not be regarded as discrimination on those grounds.**

Explanation

Ireland suggests explicit reference should be made of the following broad circumstances in which differences in treatment on the grounds covered in the Directive should be permitted - namely sporting activities and dramatic/entertainment activities. For example, Ireland feels that age bands for sporting activities are appropriate and that events such as the Special Olympics, Paralympics and Bingham Cup should not be prohibited. Similarly, Ireland feels that a gay and lesbian choir should not be prohibited.

Proposal (5)

Ireland suggests that a new Article 2(6B) is included, to read as follows:-

**2.(6B)**

**(a)     Notwithstanding paragraph 2, Member States may provide that differences of treatment on grounds of disability shall not constitute discrimination, if, within the context of national law, they are objectively and reasonably justified by a legitimate aim, and if the means of achieving that aim are appropriate and necessary.**

**(b)     Where a person has a disability that, in the circumstances, could cause harm to the person or to others, treating the person differently to the extent reasonably necessary to prevent such harm does not constitute discrimination.**

Explanation

Ireland proposes that, for clarity, measures relating to positive action are dealt with solely in Article 5, and proposes that permitted differences in treatment on the ground of disability are covered in a separate new provision in Article 2.

Ireland feels that the justification for direct discrimination on the disability ground , other than in the context of positive action, which is covered in Article 5, should be subject to a very strong test of necessity and of legitimacy of aim.

Cases decided by Ireland's Equality Tribunal which considered health and safety issues and disability, outside the field of employment include the following:

*   DEC-S2006-034 Connery v Coiste An Asgard : the complainant, who had applied and was accepted as a passenger/trainee aboard the sailing vessel Asgard II claimed he was discriminated against when he was subsequently excluded from taking part in an open sea voyage due to the fact that he had Type 1 diabetes. The Tribunal held that this exclusion was justified in the circumstances. (www.equalitytribunal.ie)

Proposal (6)

Ireland suggests that the wording of Article 2(7) is amended to read as follows:

Notwithstanding paragraph 2, in the provision of **insurance and related** financial services Member States may permit **proportionate** differences in treatment where, for the product in question, the use of age or disability is a **relevant** \[key **determining** factor*\]  in the assessment of risk based on relevant \[and accurate*\] actuarial \[principles, accurate or*\] statistical data \[or medical knowledge*\] **and other relevant underwriting or commercial factors and the difference in treatment is reasonable having regard to the data or other relevant factors**.

  

Supplemented by amendment of Recital 15, as follows:

(15)   Actuarial and risk factors related to disability and to age are used in the provision of insurance, banking and other financial services. These should not be regarded as constituting discrimination where \[service providers have shown, by actuarial principles, statistical data or medical knowledge, **or other relevant underwriting or commercial factors** that *\] **such factors are \[…\] determining** **the difference in treatment is reasonable having regard to such** factors for the assessment of risk. \[**These data should be accurate, recent and relevant. The actuarial and risk factors should also reflect positive changes in life expectancy and active ageing as well as increased mobility and accessibility for people with disabilities.***\]

Explanation

Ireland proposes that age and disability may be taken into account in matters related to the assessment of risk. Differences in treatment should be permitted which are reasonable, based on the risk assessment presented, where this can be supported by the data and other relevant underwriting and commercial factors. Ireland suggests that the appropriateness of the treatment should be determined on a case by case basis. A case decided by Ireland's Equality Tribunal which illustrates these issues is:

*   DEC-S2003-116 Ross v Royal and Sun Alliance : refusal by an insurance company to provide a quotation for car insurance to a 77 year old man because of his age. ([www.equalitytribunal.ie](http://www.equalitytribunal.ie/))

Ireland proposes that Recital 15 is amended to exclude the sentence concerning actuarial and risk factors, which is to some extent made redundant by the preceding sentence.  More importantly, the sentence refers to life expectancy, whereas actuaries use chance of death in their calculations, which are different concepts, whose inclusion could ultimately prove to be a controversial in that it does not reflect actual practice.  
Proposal (7)

Ireland suggests that the wording of Article 2(8) is amended to read as follows:

This Directive shall be without prejudice to **general** measures laid down in national law **and practice** which, in a democratic society, are necessary for public security, for the maintenance of public order and the prevention of criminal offences, for the protection of health and the protection of the rights and freedoms of others.

Explanation

Ireland proposes this minor amendment in order to align the text with that of Article 2(5) of Directive 2000/78/EC. Ireland also suggests that this provision should also extend to 'national law and practice' and not solely to measures set out in national law.

Proposal (8)

Ireland suggests that the wording of Article 3(3) is amended to read as follows:-

**3.(3) (a)** \[Member States may provide that differences of treatment*\]  based on religion or belief in respect of access to educational institutions, the ethos of which is based on religion or belief, in accordance with national laws, traditions and practice, \[shall not constitute discrimination. **In such cases, there shall, however, be no discrimination in access to educational institutions on any other ground**.*\]

**(b)     Differences in the treatment of persons on the ground of religion or belief in relation to goods or services provided for a religious purpose does not constitute discrimination for the purpose of this Directive.**

Explanation

Ireland proposes that Article 3(3) is amended to ensure that schools run by religious groups would not be obliged to be co-educational and that schools run by religious groups for pupils with specific disabilities (e.g. schools for the blind) are not prevented from restricting their intake to children with disabilities or with particular disabilities. It is suggested that deleting the last sentence of the first paragraph is sufficient in this regard. It is understood from the context that this exclusion relates ONLY to the ground of religion or belief, as under EU law, exclusions from a general principle are to be interpreted narrowly.

Ireland also wishes to confirm that the prohibition of differential treatment on the ground of religion or belief does not extend to goods or services which are provided for a religious purpose. For example, priority access to hostel accommodation on a pilgrimage route may legitimately be granted to pilgrims, if desired.

Proposal (9)

Ireland suggests including a new paragraph (4A) to Article 3, to read as follows:

**3.(4A) Treating a person differently does not constitute discrimination where the person**

**(a)     is so treated solely in the exercise of a clinical judgment in connection with the diagnosis of illness or his or her medical treatment, or**

**(b)     is incapable of entering into an enforceable contract or of giving an informed consent and for that reason the treatment is reasonable in the particular case.**

Explanation

Ireland wishes to confirm that decisions made **solely** in the exercise of clinical judgement by medical professionals which may result in differences in treatment based on age, disability, sexual orientation or religion or belief are not required to be justified under the criteria set out in this Directive. Cases decided by Ireland's Equality Tribunal which illustrate these issues are:

*   DEC-S2006-069 Mr Hallinan v Dr O’Donnell : quadraplegic man hospitalised for breathing difficulties complained his medical treatment was discriminatory. Tribunal found the decisions made regarding treatment were an exercise of clinical judgement and not due to complainant's disability. ([www.equalitytribunal.ie](http://www.equalitytribunal.ie/))  
    Proposal (10)

Ireland suggests an amendment to the wording of Article 7(2), as follows:

7\. (2) Member States shall ensure that associations, organisations or other legal entities, which have a legitimate interest in ensuring that the provisions of this Directive are complied with, may engage, **as the Member States so determine and in accordance with the criteria laid down by their national law**, either on behalf or in support  of the complainant, with his or her approval, in any judicial and/or administrative procedure provided for the enforcement of obligations under this Directive. **This is without prejudice to national rules of procedure concerning representation and defence before the courts.**

Explanation

Ireland proposes that explicit recognition be given in the text of the directive to the criteria for legal standing in national legal systems, as has been made in the Race Directive, the Framework Employment Directive and the Gender Goods and Services Directive.

Proposal (11)

Ireland suggests amending the wording of Article 11 as follows:

Dialogue with relevant stakeholders.

11.    With a view to promoting the principle of equal treatment, Member States shall encourage dialogue with relevant stakeholders, **in particular non-governmental organisations,** which have, in accordance with their national law and practice, a legitimate interest in contributing to the fight against discrimination on the grounds and in the areas covered by this Directive.

Explanation

Ireland proposes that there is no need in this proposal to highlight engagement with NGOs over other relevant stakeholders.

  

Proposal (12)

Ireland proposes the deletion of Recital 28

(**28). In exercising their powers and fulfilling their responsibilities under this Directive, these bodies should operate in a manner consistent with the United Nations Paris Principles relating to the status and functioning of national institutions for the protection and promotion of human rights.**

Explanation

In regard to Article 12, Ireland proposes Ireland proposes the deletion of Recital 28, which requires both independent _status_ and independent functioning of bodies for the protection and promotion of human rights.

Proposal (13)

Ireland suggests amending the wording of Article 16 as follows:-

Report

16.(1)         Member States **and national equality bodies** shall communicate to the Commission, by …. at the latest and every five years thereafter, all the information necessary for the Commission to draw up a report to the European Parliament and the Council on the application of this Directive.

(2)                   The Commission's report shall take into account, as appropriate, the viewpoints of **national equality bodies,** the social partners  and relevant non-governmental organizations, as well as the EU Fundamental Rights Agency. In accordance with the principle of gender mainstreaming, this report shall, inter alia, provide an assessment of the impact of the measures taken on women and men. In the light of the information received, this report shall include, if necessary, proposals to revise and update this Directive.

  

Explanation

Ireland proposes that, as the Directive is addressed solely to the Member States, it is not appropriate to require national equality bodies to report on progress. It is suggested that this is better phrased by requiring the Commission to include national equality bodies in the list of stakeholders whose views the Commission will take into account in its own reporting.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) Swedish Presidency proposal. ST11774/09 SOC 427 JAI 458 MI 268 (10/07/2009).