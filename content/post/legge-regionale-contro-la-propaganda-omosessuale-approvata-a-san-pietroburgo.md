---
title: 'Legge regionale contro la ''propaganda omosessuale'' approvata a San Pietroburgo'
date: Wed, 29 Feb 2012 13:27:54 +0000
draft: false
tags: [Russia]
---

Approvata la legge che multa chi si dichiara gay e diffonde testi sull'omosessualità. La grave decisione presa nonostante la mobilitazione internazionale. Certi Diritti chiede al Ministro degli Esteri italiano, al Consiglio D'Europa e all'Unione Europea preovvedimenti contro questa vergognosa legge.

Roma, 29 febbraio 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

L’Assemblea parlamentare di S.Pietroburgo ha approvato in via definitiva, con 29 voti a favore, un astenuto e 15 che non hanno partecipato al voto, la legge omofoba che prevede una multa di 500 Rubli per coloro che si dichiarano apertamente omosessuali, 50 mila Rubli per i dipendenti pubblici e 500 mila Rubli per le organizzazioni che promuovono iniziative in difesa dei diritti Lgbti.

Questa grave decisione va contro tutte le raccomandazioni del Consiglio d’Europa di cui la Russa fa parte e viola tutti i trattati sul rispetto dei diritti umani. In questi giorni in molti paesi,  le organizzazioni per la difesa dei Diritti Umani si sono mobilitate davanti alle rappresentanze diplomatiche russe chiedendo che venisse sospeso il provvedimento votato oggi in terza lettura. L’Associazione Radicale Certi Diritti insieme ad altre organizzazioni Lgbti italiane aveva fatto ieri un Flash-mob davanti al consolato russo di Milano.

L’Associazione Radicale Certi Diritti chiederà subito un intervento del Consiglio d’Europa e investirà della questione il Ministro degli Esteri italiano e l’Unione Europea contro questa vergognosa legge omofoba che viola tutti i trattati internazionali che la stessa Russia ha sottoscritto in questi anni.

**iscriviti alla newsletter >[  
](newsletter/newsletter)[](newsletter/newsletter)[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**