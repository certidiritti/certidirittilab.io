---
title: 'NOZZE GAY, ALLA CONSULTA LA PAROLA RISOLUTIVA'
date: Tue, 21 Apr 2009 08:43:50 +0000
draft: false
tags: [Senza categoria]
---

Sui **matrimoni gay** una parola risolutiva potrebbe arrivare dalla **Corte costituzionale**. Dopo l'intervento sulla legge in materia di fecondazione assistita, un'altra fondamentale questione di confine tra etica, società e diritto rischia di dover essere risolta dal giudice delle leggi. Il Tribunale ordinario di Venezia ha infatti rimesso alla Consulta (disponibile sul sito [www.guidaaldiritto.ilsole24ore.com](http://www.guidaaldiritto.ilsole24ore.com/)) il problema del "vuoto" normativo in tema di unioni **tra persone dello stesso sesso**.  
  
Secondo il magistrato veneziano il fatto che le regole del nostro ordinamento civile, così come sistematicamente interpretate, non consentano alle persone di orientamento omosessuale di sposarsi viola almeno quattro principi fondamentali; ovvero parità, uguaglianza, diritto alla famiglia e potestà legislativa (articoli 2, 3, 29 e 117, comma 1, della Carta fondamentale).  
  
**La vicenda nasce dal ricorso di due uomini** contro il rifiuto dell'ufficiale di stato civile del Comune di Venezia di procedere alla pubblicazione delle loro nozze. Si tratta di una delle venti coppie gay - tutte del Centro-Nord - che ha deciso di sposarsi, a suon di marche da bollo. Il Tribunale ha preso atto che, in linea con le risoluzioni del Parlamento europeo e a conferma degli ormai consolidati mutamenti dei modelli e dei costumi familiari, nel diritto di molte nazioni di civiltà giuridica affine alla nostra, si stia delineando una nozione di relazioni familiari tale da includere le coppie omosessuali.

20 aprile 2009

### di Beatrice Dalia

http://www.ilsole24ore.com/art/SoleOnLine4/Norme%20e%20Tributi/2009/04/corte-costituzionale-nozze-gay.shtml?uuid=9b80fe02-2dc6-11de-bf43-2ea9a6202a14&DocRulesView=Libero