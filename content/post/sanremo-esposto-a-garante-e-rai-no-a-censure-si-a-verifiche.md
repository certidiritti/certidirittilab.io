---
title: 'SANREMO: ESPOSTO A GARANTE E RAI, NO A CENSURE SI A VERIFICHE'
date: Thu, 29 Jan 2009 10:04:59 +0000
draft: false
tags: [Comunicati stampa, GAY, luca era gay, OMOFOBIA, omosessuali, povia, sanremo, scandalo]
---

![Sanremo](http://www.musicyes.org/data/musicyes/sanremo.jpg "Sanremo")FESTIVAL DI SANREMO: ESPOSTO AL GARANTE PER LE TELECOMUNICAZIONIE E AL PRESIDENTE DELLA RAI SULLA CANZONE DI POVIA, NO ALLA CENSURA MA OCCORRE VALUTAZIONE SU POSSIBILE VIOLAZIONE CODICE ETICO E ALTRE NORME.

Roma, 29 gennaio 2009

L'Associazione Radicale Certi Diritti ha inviato oggi un esposto al Garante per le Telecomunicazioni e al Presidente della Rai, affinché valutino loro, in base alle norme vigenti, se la canzone di Povia 'Luca era gay' ha, o no, un contenuto che ravvisi un danno verso le persone omosessuali. In Italia vi sono norme che consentono di agire qualora vengano trasmesse notizie contenenti affermazioni false e/o scientificamente prive di fondamento che possono ingenerare pregiudizio per l'integrità, l'immagine e l'onorabilità delle persone omosessuali, ivi compresa la diffusione di tesi che promuovono sofferenza o turbamento a causa della rappresentazione falsa della realtà, . Nell'esposto sono stati richiamate le seguenti norme previste dalla Delibera n.165/06/CSP del Garante per le Telecomunicazioni sul rispetto dei diritti fondamentali della persona, della dignità personale e del corretto sviluppo fisico, psichico e morale dei minori; gli Articoli 2 e 8 del Contratto di servizio vigente con la Rai-Tv; gli Articoli 3 e 4 del D.Lgs del 31 luglio 2005 n. 177; l'art. 2.13.1 del Codice etico della Rai riguardo le responsabilità nei confronti della collettività.