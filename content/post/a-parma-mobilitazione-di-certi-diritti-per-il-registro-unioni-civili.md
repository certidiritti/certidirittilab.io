---
title: 'A PARMA MOBILITAZIONE DI CERTI DIRITTI PER IL REGISTRO UNIONI CIVILI'
date: Thu, 15 May 2008 11:30:26 +0000
draft: false
tags: [Comunicati stampa]
---

Il Gruppo Certi Diritti di Parma ha dato il via alla mobilitazione per la Petizione popolare volta ad ottenere il Registro delle Unioni civili.  
[Clicca qui per la notizia completa.](http://parma.repubblica.it/dettaglio/Le-associazioni-gay-di-Parma:-Subito-il-registro-delle-coppie-di-fatto/1458287) [Clicca qui per vedere la locandina.](index.php?option=com_content&task=view&id=60&Itemid=55)