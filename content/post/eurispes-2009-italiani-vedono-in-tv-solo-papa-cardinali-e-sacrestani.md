---
title: 'EURISPES 2009: ITALIANI VEDONO IN TV SOLO PAPA, CARDINALI E SACRESTANI'
date: Fri, 30 Jan 2009 14:10:43 +0000
draft: false
tags: [Comunicati stampa]
---

### RAPPORTO EURISPES 2009: NESSUNA SORPRESA SUI RISULTATI, ITALIANI SENZA INFORMAZIONE, EDUCAZIONE E SCIENZA. IN TV SOLO VESCOVI, CARDINALI E PAPI.

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti

“Davvero non ci sorprendono i risultati del Rapporto Eurispes 2009 su come gli italiani la pensano in fatto di omosessualità e diritti civili. Dai risultati si evince  che gli italiani sono sempre più consapevoli della necessità di una legge sulle Unioni civili e questo, senz’altro, spiega l’ottimo risultato del 58,9% di favorevoli.

Il nostro Governo farebbe bene a tenerne conto.Non sorprendono invece gli altri risultati in  calo percentuale sui favorevoli alle adozioni e ai matrimoni tra persone gay che passano, confrontati al 2003, rispettivamente dal 27 al 19% e dal 51,6 al 41,7%. Con una classe politica quasi totalmente genuflessa alle gerarchie vaticane, con i Tg  del servizio pubblico che ci riassumono ogni giorno le omelie del Papa e le prediche dei Cardinali, con le  fiction in prima serata su Santi, Preti e Sacrestani, in totale assenza di servizi educativi, di inchiesta, di scienza  e di obiettiva informazione, che non permette di conoscere a fondo questi temi, non potevamo certo aspettarci risultati migliori, quella si che sarebbe stata la vera notizia.