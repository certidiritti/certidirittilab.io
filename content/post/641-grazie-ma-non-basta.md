---
title: '641 grazie, ma non basta'
date: Sat, 25 Aug 2012 13:53:47 +0000
draft: false
tags: [Politica]
---

![LogoCD](http://www.radicalparty.org/file/Logo_Cd_2012.JPG)

ad oggi sono **532** le persone iscritte all’associazione radicale Certi Diritti e **109** quelle che hanno versato un contributo, per un totale di **641** sostenitori nel 2012. L’autofinanziamento è di 10.916 euro contro gli 11.574 dello scorso anno. In cassa rimangono 3.600 euro, che non ci permettono di portare a termine le iniziative in corso e lanciarne di nuove da qui al congresso del prossimo autunno.**Per questo abbiamo bisogno del tuo aiuto**.

Se non avessi ancora rinnovato l’iscrizione ti invitiamo a farlo ora con una semplice telefonata allo **06 6826** o con una delle altre modalità che trovi al link [**www.certidiritti.it/iscriviti**](iscriviti).  
Chi è già iscritto o sostenitore può versare un libero contributo per sostenere le battaglie dell’associazione. **Ti ricordiamo che tutto il nostro lavoro è volontario e solo dal canale dell’autofinanziamento possiamo avere le risorse economiche per promuovere ulteriori iniziative**.

Domani l’associazione parteciperà al [**Pride nazionale di Bologna **](http://www.bolognapride.it/2012/05/30/le-iniziative-del-bologna-pride-a-sostegno-dei-territori-colpiti-dal-sisma/)che vedrà un corteo senza carri e tanti eventi di solidarietà per i terremotati. Appuntamento alle 14.30 a Porta Saragozza.

Certi Diritti ha aderito anche al Pride di Roma, Torino, Napoli, Palermo e Catania.  
[**A Roma il corteo si svolgerà il 23 giugno mentre dal 15 al 17 giugno i giardini di Villa Gordiani ospiteranno il Pride Park dove saremo presenti con un nostro stand.**](http://www.romapride.it/)

Lunedì scorso eravamo invece a Milano per un [**sit-in davanti Palazzo Marino **](associazione-radicale-certi-diritti-sit-in-davanti-a-palazzo-marino-allindomani-dellincontro-mondiale-delle-famiglie)con il qualeabbiamo denunciato l'immobilismo trasversale del Consiglio comunale in materia di diritti civili delle famiglie di fatto. «Se entro questo anno il consiglio comunale non deciderà, assumerò io personalmente con la mia giunta la decisione sul registro delle unioni civili» è stata la risposta delsindaco Pisapia. Intanto **continua la raccolte firme sulle 5 delibere** di iniziativa popolare per una Mi lano laica, libera ed europea. Trovi tutte le informazioni su [**milanoradicalmentenuova**](http://milanoradicalmentenuova.it/).

**Sempre a Milano[ il consiglio di zona 2 su proposta di Yuri Guaiana, segretario diCerti Diritti, ha approvato all’unanimità la mozione per la revoca del gemellaggio con San Pietroburgo](http://www.milanoradicale.it/2012/06/01/il-consiglio-di-zona-2-approva-allunanimita-la-mozione-radicale-per-la-revoca-del-gemellaggio-con-san-pietroburgo/). **La stessa mozione è stata presentata dal consigliere comunale radicale Marco Cappato all’indomani del 29 febbraio quando il parlamento di San Pietroburgo ha approvato la legge che sanziona la cosiddetta “propaganda dell’omosessualità”.

Nell'ambito del “Lgbti Human Rights Tour”, Certi Diritti insieme alla **Sezione italiana di Amnesty International** ha organizzano due eventi a Roma e Milano con attivisti per i dirittiumani in Albania, Russia e Turchia. A Milano l’evento di è svolto il 7 giugno presso il Teatro dell’Elfo mentre quello di Roma della settimana prossima ospiterà anche il padre del ragazzo cileno torturato e ucciso lo scorso marzo perché gay.

**Chi ha qualcosa da dire lo dica ora o taccia per sempre -[ Intervista a Gian Mario Felicetti](http://www.liberi.tv/webtv/2012/06/02/video/chi-ha-qualcosa-dire-dica-ora-o-taccia-sempre-intervista-gian)**, della segreteria nazionale di Certi Diritti e coautore del libro **[Certi diritti che le coppie conviventi non sanno di avere](certi-diritti-che-le-coppie-conviventi-non-sanno-di-avere) su Liberi.tv**. Arriveremo mai al matrimonio fra persone dello stesso sesso? Ci sono novità in merito alle politiche della famiglia? Milano è più gay friendly di altre cit tà?

Fuor di pagina - la rassegna stampa di Certi Diritti. [**Ascolta su radio radicale**](http://www.radioradicale.it/rubrica/1004)**> [Potcasting](http://www.radioradicale.it/podcasting.php?e=1004)>**

**[Firma il manifesto per la legalizzazione della prostituzione >](legalizzazione-prostituzione)**

[**www.certidiritti.org**](http://http./www.certidiritti.org)