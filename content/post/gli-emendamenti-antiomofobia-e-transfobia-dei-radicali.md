---
title: 'GLI EMENDAMENTI ANTIOMOFOBIA E TRANSFOBIA DEI RADICALI'
date: Mon, 12 Oct 2009 08:41:20 +0000
draft: false
tags: [Comunicati stampa]
---

Di seguito i sette emendamenti migliorativi del provvedimento antimofobia oggi in discussione alla Camera dei deputati (il cui voto è previsto domani, martedì 13 ottobre):

**Emendamento 1**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**ART. 1**

L’art. 1 è sostituito dal seguente:

“Art. 1-

1\. Dopo l’art. 1 della Legge n. 122/1993 è aggiunto il seguente:

art. 1-bis (_Discriminazione, odio o violenza per motivi connessi all’identità di genere, all’orientamento sessuale_)

Le previsioni di cui alla presente legge si applicano anche nei casi di atti di discriminazione, odio o violenza per motivi connessi all’identità di genere ed all’orientamento sessuale.

2\. All’art. 3, comma 1, Legge n. 122 del 1993, dopo la parola: “religioso”, sono aggiunte le seguenti: “o basato su orientamento sessuale o identità di genere”

**On. Rita Bernardini**

**On. Matteo Mecacci**

**On. Marco Beltrandi**

**On. Maria Antonietta Farina Coscioni**

**On. Maurizio Turco**

**On. Elisabetta Zamparutti**

**Emendamento 2**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**ART. 1**

L’art. 1 è sostituito dal seguente:

“Art. 1

All’art. 61, comma 1, del codice penale, dopo il numero 11-ter) è aggiunto il seguente:

11-quater) l’aver commesso il fatto per motivi connessi a discriminazione, odio o violenza relativi all’identità di genere e all’orientamento sessuale della persona vittima del reato”

**On. Rita Bernardini**

**On. Matteo Mecacci**

**On. Marco Beltrandi**

**On. Maria Antonietta Farina Coscioni**

**On. Maurizio Turco**

**On. Elisabetta Zamparutti**

**Emendamento 3**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**ART. 1**

All’art. 1 le parole “nei delitti non colposi contro la vita e l’incolumità individuale, contro la personalità individuale, contro la libertà personale e contro la libertà morale” sono soppresse.

**On. Rita Bernardini**

**On. Matteo Mecacci**

**On. Marco Beltrandi**

**On. Maria Antonietta Farina Coscioni**

**On. Maurizio Turco**

**On. Elisabetta Zamparutti**

**Emendamento 4**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**ART. 1**

All’art. 1 sostituire le parole: “per finalità all’orientamento o alla discriminazione sessuale della persona offesa del reato”, con le seguenti: “per motivi connessi all’orientamento sessuale o all’identità di genere della persona vittima del reato. La circostanza aggravante si realizza quando il reato è preceduto, accompagnato o seguito da atti o parole che ledano l’onore della persona vittima del reato o di gruppi di persone di cui fa parte, a ragione del suo orientamento sessuale o identità di genere, vera o presunta”.

**On. Rita Bernardini**

**On. Matteo Mecacci**

**On. Marco Beltrandi**

**On. Maria Antonietta Farina Coscioni**

**On. Maurizio Turco**

**On. Elisabetta Zamparutti**

**Emendamento 5**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**ART. 1**

All’art. 1 sostituire le parole: “per finalità all’orientamento o alla discriminazione sessuale della persona offesa del reato”, con le seguenti: “per motivi connessi all’orientamento sessuale o all’identità di genere della persona vittima del reato”.

**On. Rita Bernardini**

**On. Matteo Mecacci**

**On. Marco Beltrandi**

**On. Maria Antonietta Farina Coscioni**

**On. Maurizio Turco**

**On. Elisabetta Zamparutti**

**Emendamento 6**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**ART. 1**

**All’articolo 1, dopo le parole “discriminazione sessuale” aggiungere le parole “o alla transessualità”.**

**On. Rita Bernardini**

**On. Matteo Mecacci**

**On. Marco Beltrandi**

**On. Maria Antonietta Farina Coscioni**

**On. Maurizio Turco**

**On. Elisabetta Zamparutti**

**Emendamento 7**

**A.C. 1658 e A.C. 1882**

**EMENDAMENTO AL TESTO UNIFICATO APPROVATO DALLA COMMISSIONE**

**ART. 1**

Dopo l’art. 1, è aggiunto il seguente:

“Art. 1 bis .

1\. Il Governo entro novanta giorni dall’entrata in vigore della presente legge, approva, sentite le Commissioni parlamentari competenti, un Piano triennale contro le discriminazioni, con riferimento a quanto previsto dall’art. 13 del Trattato CE, anche in coordinamento con le attività previste ai sensi delle Direttive 2000/43/EC e 2000/78/EC già recepite dall’ordinamento italiano.

2\. Il Piano deve prevedere obiettivi, risorse e metodi di valutazione per il monitoraggio, la prevenzione, il contrasto e l’assistenza alle vittime di discriminazione o atti di violenza connessi all’identità di genere, all’orientamento sessuale, alla religione, all’origine etnica o geografica, all’età o alla condizione di disabilità della persona vittima del reato o della discriminazione. Il Piano deve prevedere, tra l’altro, specifiche campagne di comunicazione sociale ed iniziative di educazione  - anche sui temi connessi alla sessualità umana - nelle scuole e presso l’associazionismo giovanile.

3\. Il Piano deve inoltre prevedere la trasformazione dell’UNAR in Agenzia Nazionale contro le discriminazioni che, in autonomia dal Governo e con l’assegnazione alla stessa di adeguate risorse, diventi il soggetto che sovrintende e gestisce l’attuazione del Piano nazionale stesso ”.

4\. all’onere derivante dal presente articolo si provvede a valere sul fondo per le politiche relative ai diritti e alle pari opportunita' (art. 19, comma 3 del decreto legge 4 luglio 2006, n. 223, convertito con legge 248/2006).

**On. Rita Bernardini**

**On. Matteo Mecacci**

**On. Marco Beltrandi**

**On. Maria Antonietta Farina Coscioni**

**On. Maurizio Turco**

**On. Elisabetta Zamparutti**