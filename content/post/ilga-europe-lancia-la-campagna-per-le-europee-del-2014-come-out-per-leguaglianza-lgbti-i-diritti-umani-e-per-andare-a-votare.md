---
title: 'Ilga-Europe lancia la campagna per le europee del 2014: "Come out per l''eguaglianza lgbti, i diritti umani e per andare a votare"'
date: Mon, 13 Jan 2014 18:40:52 +0000
draft: false
tags: [Europa]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti

Roma, 13 gennaio 2014

Domani al Parlamento Europeo di Strasburgo, ILGA-Europe lancia la sua campana per le elezioni europee che si svolgeranno il 22-25 maggio 2014.

La campagna sarà lanciata dai parlamentari europei e candidati per la prossima legislatura dei 5 più grandi gruppi politici che sono anche i primi firmatari dell'impegno parte della campagna «Come out».

Roberta Metsola (EPP - European People's Party)

Sylvie Guillaume (S&D - Progressive Alliance of Socialists and Democrats)

Sophie in 't Veld (ALDE – Alliance of Liberals and Democrats for Europe)

Ulrike Lunacek (The Greens/EFA – European Free Alliance)

Dennis De Jong (GUE/NGL – Confederal Group of the European United left – Nordic Green Left)

Lo scopo della campagna è duplice. In primo luogo si vuole che i candidati al prossimo Parlamento Europeo e Commissione Europea assicurino la loro determinazione, se eletti, a continuare l'impegno istituzionale per i diritti umani in generale e per quelli delle persone LGBTI in particolare. Come prova del loro impegno ILGA-Europel e i suoi membri, come 'Associazione Radicale Certi Diritti, inviteranno i candidati a firmare una pledge in 10 punti che elenca le questioni fondamentali che ILGA-Europe vuole far avanzare a livello europeo nei prossimi 5 anni.

In secondo luogo si vogliono incoraggiare associazioni e singoli a partecipare alle prossime elezioni, per aumentare il numero di votanti che nel 2009 ha toccato il minimo storico: 43%.

Il sito web della campagna:

conterrà una mappa interattiva che mostrerà la quantità di candidati che firmeranno la pledge

spiegherà perché le elezioni europee sono importanti e dove trovare maggiori informazioni sulle elezioni

fornirà un tool kit per le organizzazioni e i singoli che si vorranno impegnare nei Paesi membri dell'UE