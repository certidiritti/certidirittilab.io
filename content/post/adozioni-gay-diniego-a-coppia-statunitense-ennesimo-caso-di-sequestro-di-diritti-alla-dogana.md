---
title: 'Adozioni gay: Diniego a coppia statunitense, ennesimo caso di sequestro di diritti alla dogana.'
date: Thu, 16 Oct 2014 14:05:34 +0000
draft: false
tags: [Americhe]
---

[![dogana](http://www.certidiritti.org/wp-content/uploads/2014/10/dogana-300x228.jpg)](http://www.certidiritti.org/wp-content/uploads/2014/10/dogana.jpg)"E' possibile che i diritti di cui si beneficia in altri Paesi del mondo vengano costantemente sequestrati alle nostre frontiere? E' una situazione assurda per la quale i nostri governanti dovrebbero vergognarsi."

Così Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, ha commentato il diniego della Procura che a Bologna ha negato l'adozione del figlio naturale della partner omosessuale già avvenuta negli Stati Uniti d'America. La coppia è composta da una donna statunitense con cittadinanza italiana acquisita per discendenza e da una donna statunitense.

"Mentre tarda ancora una legge che estenda il matrimonio civile alle coppie dello stesso sesso, il 'tira e molla' tra Governo e Parlamento sulle unioni civili mantiene questo scenario surreale in cui ci si accanisce persino sui bambini. Siamo arrivati persino al paradosso per il quale una bambina rischia di perdere il riconoscimento del legame con una delle madri e persino con il fratellino. La bimba rischia anche di essere privata anche del diritto di cittadinanza italiana e europea riconosciuto invece al fratellino per discendenza dalla madre cittadina italiana. i due fratellini verrebbero quindi trattati differentemente e la bimba non avrebbe l'opportunità futura di studiare e lavorare in Europa.

L'assenza di una legge che estenda il matrimonio civile alle coppie dello stesso sesso rappresenta ormai un _vulnus_ morale le cui conseguenze sono sempre più spesso pagate dai più piccoli e dai più indifesi. basta giocare con le nostre vite e con quelle dei nostri figli! Uguaglianza, subito!"

Comunicato stampa dell'Associazione Radicale Certi Dirtti