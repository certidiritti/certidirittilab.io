---
title: 'MATRIMONIO GAY E UNIONI CIVILI: ALTRE DUE PROPOSTE DI LEGGE RADICALI'
date: Fri, 16 May 2008 13:33:43 +0000
draft: false
tags: [Comunicati stampa]
---

**E’ necessario adeguarsi all’Europa (e alla California!).  
Dopo le due Proposte di Legge contro l’omofobia e per la difesa dei diritti delle persone transessuali e transgender, i radicali hanno oggi depositato altre due proposte di legge sul matrimonio gay e sulle unioni civili, prima firmataria di entrambe, Rita Bernardini, Segretaria di Radicali Italiani.  
  
****La prima proposta: ‘Modifiche al codice civile in materia di diritto a contrarre matrimonio e di uguaglianza giuridica tra i coniugi’** ha come prima firmataria Rita Bernardini, Segretaria di Radicali Italiani, segue la firma del deputato radicale Marco Beltrandi**.** La proposta è simile a quella già in vigore in Spagna, Belgio e Olanda, propone la modifica del codice civile cambiando i termini marito e moglie in coniugi per dare attuazione a una innovazione in materia di parità di diritti.  
  

**La seconda proposta**: ‘**Modifiche al codice civile e altre disposizioni in materia di unione civile’,** **prima firmataria Rita Bernardini e, a seguire, gli altri deputati radicali,** intende dare forma e sostanza giuridica alla vita di decine di migliaia di «coppie di fatto». E’ stata elaborata grazie al contributo del giurista Stefano Fabeni, Direttore per le questioni lgbt della Global Right di Washington. L'articolo 2 della Costituzione ha, da sempre, richiesto al legislatore un compito preciso e perentorio: garantire la pienezza e l'esaustività della tutela di tutti gli ambiti di relazione sociale in cui queste realtà si affermano, al fine di riconoscere diritti, evitando ingiustificate discriminazioni.  
  
La puntuale codificazione delle unioni civili, la regolamentazione delle posizioni delle parti che le costituiscono, delle dinamiche sociali, contrattuali, di vita di relazione nonché, quando capita, anche delle dinamiche penali, rappresenta, pertanto, in primo luogo, il momento di cessazione di quell'assurda situazione di inesistenza del diritto, di spazio giuridicamente vuoto che lascia da decine di anni senza il necessario contributo normativo migliaia di formazioni sociali, privandole, di fatto e di diritto, della necessaria presenza di quel sussidio normativo atto a riconoscere diritti, doveri e possibilità concrete di vita sociale. Una compiuta disciplina della materia rappresenta, altresì, l'ineludibile punto di arrivo di un lungo percorso politico e giuridico di intenso dibattito, che ha visto negli anni accumularsi «materiali» confermativi, tra cui la Carta dei diritti fondamentali dell'Unione europea e le pronunce del Parlamento europeo e della Corte costituzionale, in ordine alla rilevanza sociale della coppia di fatto, indipendentemente dal suo orientamento sessuale, nonché dell'esigenza che ad essa sia assicurato un riconoscimento vero, formale e sostanziale.