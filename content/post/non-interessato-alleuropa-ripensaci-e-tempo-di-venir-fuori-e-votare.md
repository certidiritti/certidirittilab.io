---
title: 'Traduciamo e pubblichiamo l''articolo di Evelyne Paradis, Direttrice esecutiva di LGA-Europe.'
date: Sat, 10 May 2014 21:53:00 +0000
draft: false
tags: [Politica]
---

[![Evelyne_Paradis](http://www.certidiritti.org/wp-content/uploads/2014/05/Evelyne_Paradis-300x239.gif)](http://www.certidiritti.org/wp-content/uploads/2014/05/Evelyne_Paradis.gif)**Non interessato all’Europa? Ripensaci. È tempo di venir fuori e votare! **

Tra due settimane, dal 22 al 25 maggio 2014, si svolgeranno le elezioni europee. Probabilmente ho smorzato il vostro entusiasmo e desiderio di procedere nella lettura menzionando l’Europa. Probabilmente penserete - «Non sono interessato alle elezioni europee», «che senso ha votare», «l’Europa è lontana e non influisce sulla mia vita», «l’Europa è solo un gruppo di grigi e noiosi burocrati concentrati a Bruxelles», «cos’ha a che fare l’Europa con con i diritti umani e la situazione delle persone LGBTI nel mio paese?».

Bene, ripensateci. L’Europa è molto importante ed è per questo che dovrete votare tra due settimane. magri non ve ne siete accorti, ma dovete ringraziare l’Unione Europea per molti diritti e garanzie di cui godete oggi nel vostro paese come lesbiche, gay, bisessuali e transessuali.

L’UE è stata la prima organizzazione internazionale a riconoscere esplicitamente l’orientamento sessuale, l’identità e l’espressione di genere come fattori di discriminazione in un testo di legge. Inoltre, molti Stati membri dell’UE non avevano alcuna protezione legale contro le discriminazioni sul luogo di lavoro prima dell’adozione della Direttiva UE contro le discriminazioni. Grazie alla Corte di Giustizia Europea, questa protezione è stata estesa anche alle persone transessuali. La medesima Corte ha fatto sì che le coppie dello stesso sesso unite civilmente possano godere esattamente degli stessi _benefits_ sul lavoro e del medesimo trattamento pensionistico delle coppie sposate. Negli ultimi due anni, l’UE ha adottato anche importanti garanzie pre le vittime di crimini d’odio, facendo sì che le vittime di reati a sfondo omo-transfobico ricevano un adeguato sostegno.

L’UE si interessa anche a coloro che vengono a cercare protezione in questo continente e a coloro che vivono al di là delle frontiere europee. L’UE ora assicura che i richiedenti asilo LGBTI siano riconosciuti e non possano essere rimandati nei Paesi d’origine chiedendo loro di far finta di essere chi non sono per evitare di essere perseguitati. Inoltre, l’UE si è fatta leader nella protezione dei diritti umani delle persone LGBTI a livello internazionale, avendo messo la questione al centro della sua politica esterna.

Questi sono tutti significativi passi avanti per le persone LGBTI in Europa e oltre, ma all’UE vengono raramente riconosciute queste vittorie. Non importa. L’essenziale è che milioni di persone abbiano questi diritti e queste garanzie. Chiaramente c’è ancora molto da fare e per cui battersi in Europa. La situazione delle persone LGBTI entro i confini dell’UE varia considerevolmente da un Paese all’altro, ma l’UE è una delle maggiori garanzie che abbiamo per progredire. È una leva importante per fissare degli standards e per assicurarsi che vi siano chiari meccanismi per combattere le discriminazioni quando si verificano.

**Allora, perché è importante venir fuori e votare?**

Tanti di noi hanno lavorato sodo e per lungo tempo per ottenere questi risultati. Questo non è il tempo per compiacersi. Gli euroscettici, movimenti di estrema destra, estremisti religiosi stanno attaccando e minacciando non solo l’Unione Europea, ma anche i suoi principi fondamentali e i valori di dignità, eguaglianza e i diritti umani per tutti. I fondamentali che ci hanno permesso di ottenere cambiamenti negli scorsi decenni.

I votanti «anti-UE» sono molto attivi, motivati e si presenteranno probabilmente in massa dal 22 al 25 maggio. I sondaggi prevedono un’enorme affermazione di queste forze nel prossimo Parlamento Europeo. Questo significa che tutti i risultati ottenuti sinora potrebbero essere messi nuovamente in gioco se non ci impaginassimo e rimanessimo in silenzio.

Ecco perché vi chiedo di venir fuori e votare! Dobbiamo assicurarci che quei candidati che sostengono i diritti umani e l’uguaglianza delle persone LGBTI ottengano il massimo sostegno e il maggior numero possibile di voti. Non possiamo permetterci di compiacerci di quanto ottenuto e starcene a casa. È tempo di agire per un’Europa giusta, eguale e dignitosa per tutti.

**Quindi, politicizzati anche solo per un momento: vieni fuori e vota per i diritti umani, per l’Europa, per l’eguaglianza LGBTI!**

Per vedere i candidati che hanno già firmato la pledge di ILGA-Europe sui diritti umani e l’eguaglianza delle persone LGBTI segui questo link: [http://www.ilga-europe.org/home/how\_we\_work/european_institutions/ep2014/candidate/signed](http://www.ilga-europe.org/home/how_we_work/european_institutions/ep2014/candidate/signed)

Per saperne di più: [http://www.ilga-europe.org/home/how\_we\_work/european_institutions/ep2014](http://www.ilga-europe.org/home/how_we_work/european_institutions/ep2014)

Guarda il video [Come out and vote!](https://www.youtube.com/watch?v=uYAwAfYnjPw)