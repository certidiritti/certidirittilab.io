---
title: 'Associazioni lgbt: Boicottiamo San Pietroburgo, l''omofobia uccide la bellezza'
date: Sat, 03 Mar 2012 13:34:29 +0000
draft: false
tags: [Russia]
---

Associazioni lgbt: Boicottiamo San Pietroburgo, stop ai gemellaggi: l'omofobia uccide la bellezza.

All'indomani dell'approvazione a San Pietroburgo di una grave legge che punisce qualunque manifestazione pubblica delle identità e dei temi delle persone lgbt (lesbiche, gay, bisessuali e  trans), le associazioni lgbt italiane, insieme a singole personalità,  lanciano un appello a tutte le associazioni in Europa e nel mondo perché condividano e diffondano una campagna di boicottaggio del turismo a S.Pietroburgo, la città russa che, scegliendo di essere omofoba e transfobica, ha ucciso la bellezza della sua storia, della sua arte e della sua cultura.

Ci rivolgiamo indistintamente a tutte e tutti perché sia forte e concreta la reazione di non scegliere più San Pietroburgo come meta turistica.

Ci rivolgiamo inoltre alle città e ai comuni in tutto il mondo che siano gemellati a San Pietroburgo (come Milano, Amburgo, Le Havre, Los Angeles, Bordeaux, ReyKjaviK, Graz, Cracovia, Porto Alegre etc.) o che con essa intrattengano rapporti commerciali o culturali affinché li risolvano o li congelino fin quando non verrà ritirata una legge che oltraggia la civiltà e il diritto.

Per adesioni scrivere a [ufficiostampa@arcigay.it.](mailto:ufficiostampa@arcigay.it.)

Elenco adesioni aggiornato alla pagina:[http://www.arcigay.it/34669/boycott-saint-petersburg-lomofobia-uccide-la-bellezza/](http://www.arcigay.it/34669/boycott-saint-petersburg-lomofobia-uccide-la-bellezza/)

**Hanno aderito le associazioni:** Agedo, Arcigay, Arcilesbica, Associazione Radicale Certi Diritti, Comitato Bologna Pride 2012, Consorzio Friendly Versilia, Famiglie Arcobaleno, Gaynet,  Gaystatale Milano,  Libellula associazione transessuale, Linfa, Nuova proposta Roma, Polis aperta, QueerLab Roma,  Rete genitori raimbow