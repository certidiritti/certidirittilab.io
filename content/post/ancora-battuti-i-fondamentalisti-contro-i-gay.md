---
title: 'Ancora battuti i fondamentalisti contro i gay'
date: Thu, 25 Nov 2010 15:57:11 +0000
draft: false
tags: [Comunicati stampa]
---

**COMMISSIONE LIBERTA PUBBLICHE DEL PARLAMENTO EUROPEO IN FAVORE DEI DIRITTI LGBT, SCONFITTO IL FONDAMENTALISMO POLITICO ANTI-GAY.**

 Strasburgo – Roma, 5 novembre 2010

L’Associazione Radicale Certi Diritti si felicita per l'approvazione odierna, in Commissione Libertà Pubbliche, Giustizia ed Affari interni del PE, della relazione sugli aspetti istituzionali della protezione europea dei diritti umani nella UE (relazione Gal). La relazione si basa sulle discussioni, risoluzioni e relazioni approvate dal PE sulla non-discriminazione ed in particolare sulle discriminazioni basate sull'orientamento sessuale delle persone sposate o unite in unione civile, e chiama le istituzioni dell’Unione Europea ad agire per assicurare l'eliminazione di tutte le discriminazioni, sulla base dell'art. 21 della Carta dei Diritti Fondamentali, che include l'orientamento sessuale.

 Sono stati rigettati o sono decaduti una serie di emendamenti depositati dall'onorevole Zaborska e da alcuni deputati PDL che volevano escludere l’orientamento sessuale tra  le cause delle discriminazioni.

 La Commissione Libe ha inoltre approvato un'opinione per la Commissione Giuridica del Parlamento Euroepo che chiede di ricomprendere nelle direttive europee relative al divorzio ed alla separazione non solo gli sposi ma anche le coppie in unione civile.