---
title: 'MATRIMONI SAME-SEX CONTRATTI ALL’ESTERO: I COMUNI LI TRASCRIVANO SUBITO!'
date: Fri, 26 Aug 2016 11:08:14 +0000
draft: false
tags: [Diritto di Famiglia]
---

[![Unioni-Civili-744x445](http://www.certidiritti.org/wp-content/uploads/2016/07/Unioni-Civili-744x445-300x179.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/07/Unioni-Civili-744x445.jpg)Unirsi civilmente in Italia non è esattamente facile come dire «Sì, lo voglio», soprattutto per le coppie che hanno già contratto un matrimonio all’estero. Seguendo numerose di queste coppie, abbiamo riscontrato che la confusione regna sovrana:

*   alcuni comuni vogliono il certificato di matrimonio rilasciato da meno di 6 mesi, altri accettano il certificato autenticato, legalizzato e nel caso apostillato, rilasciato oltre i 6 mesi come sarebbe giusto;
*   alcuni comuni acettano l’estratto dell'atto di matrimonio, mentre altri no;
*   molti comuni affermano di non avere ancora i registri e di non avere il programma per la trascrizione, mentre altri usano un registro provvisorio;
*   alcuni comuni ritengono che i documenti vadano presentati solo al Consolato italiano del Paese dove era avvenuto il matrimonio anche se non era quello di residenza;
*   alcuni comuni non hanno nemmeno i moduli per la presentazione dell'istanza costringendoci a consigliare alle coppie di consegnare quelli disponibili sul sito del Comune di Milano;
*   c' è persino qualche comune che ha detto alle coppie che si sarebbero dovute unire di nuovo civilmente, una vera e propria assurdità!

Insomma è una vera bolgia infernale per le povere coppie! Peccato che la legge sulle unioni civili  (l. 20 maggio 2016, n. 76) dice chiaramente al comma 35 che «le disposizioni di cui ai commi da 1 a 34 acquistano  efficacia a decorrere dalla data di entrata in vigore della presente legge» e quindi tutti questi ritardi e tutte queste difficoltà sono tecnicamente fuori legge. Ricordiamo inoltre che i matrimoni celebrati all'estero sono trascrivibili come unioni civili in Italia , mentre le unioni civili sottoscritte all'estero lo sono soltanto quando modificano lo stato civile delle persone che le hanno sottoscritte. «Nonostante le difficoltà, l’Associazione Radicale Certi Diritti è riuscita a ottenere la trascrizione di vari matrimoni, tra cui alcuni contratti in Brasile e a Oporto. Ma non si può andare avanti così! Chiediamo quindi all’ANCI ‘intervenire per fare in modo che tutti i Comuni italiani rispettino la legge e registrino subito almeno i matrimoni tra persone dello stesso sesso contratti all’estero. Chiediamo inoltre al Governo di fare in fretta ad emanare i decreti applicativi e di fare sì che essi chiariscano in maniera definitiva il nodo delle trascrizioni senza lasciare più adito ad alcun dubbio». Così Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti. _Roma, 26 agosto 2016_