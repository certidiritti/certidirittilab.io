---
title: 'GIORNATA MONDIALE CONTRO L’OMOFOBIA: DOMENICA 17-5 INIZIATIVA A MILANO'
date: Thu, 07 May 2009 08:44:18 +0000
draft: false
tags: [Comunicati stampa]
---

GIORNATA MONDIALE CONTRO L’OMOFOBIA  
Domenica 17 Maggio 2009, presso Via dei Mercanti in Milano, dalle 19 alle 19.30, si svolgerà la manifestazione unitaria cittadina in occasione della Giornata Mondiale contro l’Omofobia.

  
La manifestazione, ispirata ai Day of Silence anglosassoni, consisterà in un sit-in silenzioso, e sarà preceduta dalla distribuzione di materiale informativo per le vie del centro da parte di volontari imbavagliati (dalle ore 17.30 alle ore 19.00). Questo evento nasce dalla necessità di portare a conoscenza della cittadinanza, in modo diretto,l’esistenza e la pericolosa ulteriore crescita dei fenomeni omofobici, sia nella loro manifestazione materiale (violenze fisiche, atti palesemente discriminatori, ecc.) sia nella loro generale presenza nel clima socio-culturale e politico italiano.  
In particolare vanno sottolineate la recente bocciatura da parte del Consiglio Regionale  della Lombardia della proposta di adesione alla stessa Giornata Mondiale contro l’Omofobia e la natura specificatamente omofoba delle motivazioni addotte per tale bocciatura.  
Nei giorni precedenti la manifestazione (dall’11 al 15 Maggio) si terrà presso l’Università degli Studi di Milano Bicocca (piazza dell’Ateneo Nuovo 1, edificio U6, piano Meno Uno, dalle ore 8 alle 20) una mostra su l’omofobia nel XX secolo. La conferenza stampa di presentazione avrà luogo Giovedì 14 Maggio presso il Negozio Civico ChiamaMilano (Largo corsia dei Servi 11) alle ore 12.00.  
L’iniziativa è promossa dal Coordinamento cittadino “Milano contro l’Omofobia”, composto da Agedo - Associazione Genitori E amici Di Omosessuali, Arcilesbica Zami, Associazione culturale contro le discriminazioni sessuali Le Rose di Gertrude, Associazione Radicale Certi Diritti, Circolo di Cultura Omosessuale Harvey Milk, Famiglie Arcobaleno, Gaia 360°, GayLib - Gay liberali di centrodestra, KOB - Kollettivo Omosessuale Bicocca.  
  
PER CONTATTI O ULTERIORI INFORMAZIONI:  
milanocontroomofobia@gmail.com  
Francesco Poirè 348 38 83 413  
Stefania Cista 334 68 23 825  
MILANO CONtRO L’OMOFOBIA  
http://milanocontroomofobia.blogspot.com  
http://www.facebook.com > Milano contro l’omofobia

[SCARICA IL COMUNICATO](http://www.certidiritti.org/wp-content/uploads/2009/05/Comunicato_stampa.pdf)