---
title: 'David Cameron per il matrimonio tra persone dello sesso: "è questione di uguaglianza". Italia impari da lui'
date: Fri, 07 Oct 2011 07:19:23 +0000
draft: false
tags: [Europa]
---

Il primo ministro del Regno Unito a favore del matrimonio tra persone dello stesso sesso. La classe politica italiana impari dai conservatori inglesi anzichè essere conservazione partitocratica.

Roma, 7 ottobre 2011  
Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti

“Il Primo Ministro del Regno Unito ha dichiarato al Congresso dei Tory che il Governo ha allo studio l’approvazione di una legge che consenta alle coppie dello stesso sesso l’accesso all’istituto del matrimonio. Ciò che sorprende è quanto dichiarato dallo stesso leader dei conservatori: < Stiamo discutendo della legalizzazione del matrimonio omosessuale. A quanti hanno delle riserve, dico: Sì, si tratta di uguaglianza, ma anche di qualcos'altro: di impegno. I Conservatori credono nei vincoli che ci uniscono; che la società è più forte quando ci facciamo delle promesse e si sosteniamo a vicenda. Perciò io non sostengo il matrimonio omosessuale malgrado il mio essere conservatore. Sostengo il matrimonio omosessuale perché sono un conservatore>.  
Il Primo Ministro britannico, che è al Governo insieme ai Liberali inglesi, anche loro favorevoli al matrimonio tra persone dello stesso sesso, ha molto da insegnare a quasi tutta la nostra classe politica, contorta nei suoi interessi di casta partitocratica e, ogni volta che si parla di diritti civili e uguaglianza, timorosa dello Stato teocratico vaticano.  

Può anche darsi che un giorno, insieme alle forze progressiste e libertarie, anche del Regno Unito, si promuova una campagna contro l’istituto del matrimonio civile, strumento considerato da molti obsoleto e fallimentare, ma una cosa è certa: fino a quando tale istituto è riservato alle sole coppie eterosessuali vi è  discriminazione nei confronti delle coppie dello stesso sesso. Questo è il motivo per il quale, noi che conservatori non siamo, ci battiamo affinchè anche in Italia si raggiunga questo obiettivo. E’, come dice il Conservatore Cameron, una questione di uguaglianza”.

Di seguito la frase originale pronunciata  il 5 ottobre scorso dal Primo Ministro David Cameron al Congresso dei Tory:

“We're consulting on legalising gay marriage. To anyone who has reservations, I say: Yes, it's about equality, but it's also about something else: commitment. Conservatives believe in the ties that bind us; that society is stronger when we make vows to each other and support each other. So I don't support gay marriage despite being a Conservative. I support gay marriage because I'm a Conservative.”

**ISCRIVITI ALLA NEWSLETTER >**

[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)