---
title: 'Froci and queer, studio comparativo delle forme di esclusione nei confronti di persone LGBT nelle società inglese ed italiana negli ultimi 30 anni (in inglese)'
date: Fri, 29 Jun 2012 07:28:56 +0000
draft: false
tags: [Movimento LGBTI]
---

Questo articolo offre uno studio comparativo delle forme di esclusione societaria nei confronti di persone LGBT nelle societa’ inglese ed italiana negli ultimi 30 anni. Vengono esaminati nello specifico una serie di testi e pubblicazioni e attraverso l’analisi critica del discorso si cerca di fare luce su come le diverse opinioni ed (eventuali) aperture societarie verso sessualita’ ‘non-standard’ abbiano interagito con la costruzione e realizzazione di identita’ LGBT.

[Scarica l'articolo](documenti/download/doc_download/61-froci-and-queers) proposto da Franco Zappettini