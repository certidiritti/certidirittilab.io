---
title: 'GOVERNO SI MUOVE SU UNIONI CIVILI. MAH? NEL CASO PRONTI A COLLABORARE'
date: Fri, 12 Sep 2008 14:11:29 +0000
draft: false
tags: [Comunicati stampa]
---

COPPIE DI FATTO: UNA BUONA NOTIZIA DAL MINISTRO ROTONDI . L'ASSOCIAZIONE RADICALE CERTI DIRITTI E' PRONTA A COLLABORARE.  
  
La notizia che il Ministro Rotondi, insieme al Ministro Brunetta, intendono assumere una iniziativa per il riconoscimento delle coppie di fatto, ancorchè come membri del parlamento e non come iniziativa governativa, è positiva, e l'Associazione Radicale Certi Diritti non puo' che salutarla come il primo fatto nuovo e interessante di questa legislatura.

  

I Ministri Rotondi e Brunetta non sono nuovi ad aperture su quest tema e questo  loro annuncio è sicuramente da sostenere in sede parlamentare. In questo senso l'Associazione Certi Diritti chiede a tutti i parlamentari di seguire con attenzione l'evoluzione di queste proposte e partecipare alla costruzione dei provvedimenti necessari.  
  
Ri cordiamo ai Ministri Rotondi che vi sono alcune altre questioni che il Parlamento può e deve affrontare in tema di diritti delel eprsone gay lesbiche e transessuali, che dalla loro attenzione può trarre vantaggio:  
  
il completo recepimento nel nostro ordinamento di una norma antidiscriminatoria esplicita contro l'omofobia e la transfobia, anche con il riconosicmento delo status giuridico dell'asilo per immigrati omosessuali nei cui paesi di origine l'omosessualità è reato;  
  
il riconoscimento nel nostro Paese delle coppie gay e lesbiche unite in matrimonio o in unione civile nei paesi del Mondo ove questo è possibile;  
  
la riforma della legge sul cambio di stato anagrafico per le persone transessuali.  
  
L'Associazione Radicale Certi Diritti è pronta a collaborare.  
  
Enzo Cucco  
  
Direttivo Associazione radicale Certi Diritti