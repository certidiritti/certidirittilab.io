---
title: 'CONCLUSO CONGRESSO TRANSGENDER, TRANSESSUALI E INTERSESSUATI'
date: Mon, 12 Apr 2010 05:33:50 +0000
draft: false
tags: [Comunicati stampa]
---

**COMUNICATO STAMPA**

**CONGRESSO ITALIANO TRANSGENDER TRANSESSUALI ED INTERSESSUATI**

**Livorno domenica 11 aprile 2010**

\- Si è concluso oggi a Livorno alle ore 16.30 il Congresso Transgender Transessuali Intersessuali che si è svolto in un clima di sereno e di profondo dialogo nella ricerca di nuove modalità di approccio e di rivendicazione per l’affermazione dei diritti delle persone a partire dai diritti negati a coloro che sono collocate socialmente ai margini da una cultura stereotipata.

\- Sono stati affrontati i temi relativi al percorso medico giuridico della transizione, ai rapporti con i media e le altre associazioni del movimento LGBTIQ.

\- E’ stato affrontato il tema del “diritto al lavoro”, non come una pretesa di parte o lobbistica, ma come la rivendicazione per ciascun individuo del proprio diritto ad una dignità come sancito dalla nostra Costituzione.

\- Il Congresso ha affermato la volontà di fare fronte comune con tutto il movimento, nella prospettiva di un sempre più ampio coinvolgimento di tutte le persone, per l’emancipazione sociale collettiva, nella prospettiva di una società capace di cogliere nella diversità la ricchezza e di comprendere che ciascun individuo ha il diritto di vivere normalmente la propria esistenza.

\- Il Congresso ha affermato la necessità di stabilire un nuovo rapporto collaborativo e propositivo con le Istituzioni tutte, poiché noi ribadiamo di essere parte integrante della società e la nostra ferma volontà di avere la piena possibilità di dare il nostro contributo. Di conseguenza chiediamo che la dimensione delle pari opportunità sia prassi fattiva.

\- La nostra intenzione è che le nostre proposte siano condivise, non solo dalla società civile, ma che rappresentino un momento di coesione con tutte le realtà associative che operano ed hanno interesse nel ambito delle identità di genere.

\- E’ stata eletta come “portavoce” Fabianna Tozzi Daneri, la quale sarà supportata ed affiancata da un comitato, composto da Alessandro Comeni, Simona Pisano, Martina Maimonte, Darianna Saccomani.