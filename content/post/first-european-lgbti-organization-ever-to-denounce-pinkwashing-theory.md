---
title: 'First European LGBTI organization ever to denounce "pinkwashing" theory'
date: Mon, 21 Nov 2016 14:47:57 +0000
draft: false
tags: [Movimento LGBTI]
---

[![israel-rainbow-flag](http://www.certidiritti.org/wp-content/uploads/2016/11/Israel-rainbow-flag-300x185.jpg)](http://www.certidiritti.org/2016/11/21/first-european-lgbti-organization-ever-to-denounce-pinkwashing-theory/israel-rainbow-flag/)During its national congress held in Turin on November 18-20, 2016, the Italian LGBTI advocacy organization Associazione Radicale Certi Diritti approved a resolution condemning the pinkwashing theory as a political movement that promotes the de-legitimization of Israel and its boycotting, by exploiting the LGBTI cause.

"The alarming increase in political calls by LGBTI groups to boycott Israel" it is written in the resolution, "diverts from the real battle these group should hold, i.e. the advocacy for the promotion of LGBTI rights among the Palestinian people".

"The battle for the recognition of LGBTI rights in the countries where this community is persecuted, including the Palestinian Authority and Hamas's ruled Gaza Strip, should be a commitment of all the associations for human rights" concludes the resolution. "These organization must reject all the ideologies opposing the existence of Israel as a Jewish and democratic State, the only country in the Middle East protecting sexual orientation and gender identity».

"The resolution wants to be a response mounting BDS movement in Europe of which the pink-washing movement is part», says Leonardo monaco, secretary general ofAssociazione Radicale Certi Diritti.

> **About Certi Diritti:**
> 
> The Associazione Radicale Certi Diritti was founded in 2008 as a constitutive subject of the Nonviolent Transnational Radical Party (PRNTT), founded by historical liberal, pro-EU and pro-Israeli leader Marco Pannella (1930-2016). Certi Diritti is a member of the international network of ILGA (International Lesbian and Gay Association). The organization imported in Italy the method of strategic litigation and has been running advocacy campaigns on marriage equality since 2008.
> 
> Fort further info:
> 
> [http://www.certidiritti.org/mozione-particolare-sul-pinkwashing-approvata-dal-x-congresso/](http://www.certidiritti.org/mozione-particolare-sul-pinkwashing-approvata-dal-x-congresso/)