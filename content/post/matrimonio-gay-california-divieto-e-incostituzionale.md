---
title: 'MATRIMONIO GAY. CALIFORNIA, DIVIETO E'' INCOSTITUZIONALE:'
date: Thu, 05 Aug 2010 12:30:27 +0000
draft: false
tags: [Comunicati stampa]
---

**COMITATO NAZIONALE "SÌ, LO VOGLIO!" per il riconoscimento del diritto al matrimonio civile alle persone omosessuali**

Comunicato stampa  
Giovedì 05 Agosto 2010

  
**MATRIMONIO GAY. CALIFORNIA, DIVIETO E' INCOSTITUZIONALE:  
INCAPACITA' DELLA POLITICA OBBLIGA INTERVENTO MAGISTRATURA.  
**

**Commento alla sentenza del tribunale californiano  
che ha stabilito incostituzionalità del divieto alle nozze gay.**

  
Il giudice federale della California Vaughn R. Walker con la sentenza pronunciata ieri s'è espresso contro il divieto ai matrimoni gay, sancito tramite il referendum noto come "Proposition 8". Il referendum era stato promosso da una coalizione di confessioni religiose.

Secondo il giudice Walker, impedire agli omosessuali di sposarsi è discriminatorio. La Proposition 8, si legge nel pronunciamento, è "incostituzionale" e non fornisce al divieto "alcuna base razionale", ma si limita ad affermare perentoriamente che "le coppie eterosessuali sono superiori alle coppie omosessuali". "La sola disapprovazione morale è una base impropria per negare diritti a omosessuali e lesbiche", continua il giudice, che aggiunge: "è evidente che le concezioni morali e religiose sono l'unica base per credere che le coppie dello stesso sesso siano diverse da quelle eterosessuali". Come dire: uno stato di diritto non può farle proprie.

Walker, rilevando che l’opposizione ai matrimoni gay ha una forte motivazione religiosa, afferma di contro che "l'interesse di uno Stato nel momento in cui mette in atto una norma deve essere per sua stessa natura laico" e che "lo Stato non ha interesse nel rafforzare le credenze morali o religiose senza che ciò sia accompagnato da un proposito laico". Vietare i matrimoni gay è dunque "un artefatto di un tempo in cui i generi erano considerati come caratterizzati da diversi ruoli nella società e nel matrimonio", un tempo "che è passato", sottolinea Walker.

Ancora una volta - così come in Italia con la recente sentenza 138/2010 - l'incapacità della politica di legiferare laicamente a favore dell'uguaglianza di tutti i cittadini e le cittadine di fronte alla legge, sancita dalla nostra Costituzione, ha obbligato la magistratura a intervenire.

Il successo del movimento lgbt californiano è una notizia che scalda i cuori di speranza e le mani di voglia di fare, perché la battaglia per la parità dei diritti riparte con slancio lì dov'era iniziata.

  
Al Comitato "Sì, lo voglio!" aderiscono:

  
3D - Democratici per pari Diritti e Dignità di lesbiche, gay, bisessuali e trans  
Associazione Culturale ARC di Cagliari  
AGEDO – Associazione Genitori di Omosessuali  
Arcigay  
Arcilesbica  
Associazione Radicale Certi Diritti  
Associazione Radicale Enzo Tortora di Milano  
Associazione Radicale Giorgiana Masi di Bologna  
Associazione Stonewall d’iniziativa Gay Lesbica Bisex Trans di Siracusa  
Associazione Viottoli – Comunità cristiana di base di Pinerolo (To)  
Comitato Gay e Lesbiche Prato  
Comitato Provinciale Arcigay Firenze “Il Giglio Rosa” O.N.L.U.S.  
Comitato Provinciale Arcigay Verona “Pianeta Urano”  
Como Gay Lesbica  
Consulta Napoletana per la Laicità delle istituzioni  
Consulta Romana per la laicità delle Istituzioni  
Consulta Torinese per la Laicità delle istituzioni  
Coordinamento Torino Pride LGBT  
Di’ Gay Project  
Famiglie Arcobaleno  
Federazione delle chiese evangeliche in Italia  
Fondazione Critica Liberale  
Fondazione Luciano Massimo Consoli  
GAM – Gruppo alternativo motociclisti gay e lesbiche  
GayLib  
Gruppo Pesce Milano  
Gruppo Pesce Roma  
Ké Group  
Keshet, vita e cultura ebraica  
Italia Laica  
Liberacittadinanza – Rete girotondi e movimenti  
Le Ninfe – GenovaGaya  
Renzo e Lucio  
Rete Laica Bologna  
UAAR – Unione degli Atei e degli Agnostici Razionalisti