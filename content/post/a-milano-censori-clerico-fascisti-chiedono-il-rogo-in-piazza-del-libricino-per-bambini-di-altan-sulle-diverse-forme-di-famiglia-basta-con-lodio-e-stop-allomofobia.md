---
title: 'A Milano censori clerico-fascisti chiedono il rogo in piazza del libricino per bambini di Altan sulle diverse forme di famiglia. Basta con l''odio e stop all''omofobia'
date: Fri, 03 Feb 2012 00:22:19 +0000
draft: false
tags: [Politica]
---

Contro i recenti rigurgiti censori e neofascisti, l’Associazione Radicale Certi Diritti pretende la rimozione di ogni forma di discriminazione basata sull'orientamento sessuale e la promozione di pari opportunità per tutte le famiglie.

Milano, 2 febbraio 2012  
   
Comunicato Stampa dell'Associazione Radicale Certi Diritti  
   
Prima il cosiddetto Popolo delle Libertà di Zona 3 Milano ha chiesto di “bloccare la diffusione” di Piccolo Uovo, il libricino per bambini di Francesca Pardi, illustrato da Tullio F. Altan, che racconta il viaggio di un ovetto per esplorare le varie forme di famiglia esistenti e del suo incontro con la coppia omosessuale di Pinguini in frac e bombetta con figli, oltre che della coppia lesbica di gattine con figli e di tante altre tipologie di famiglia.  
   
Adesso Forza Nuova ne chiede addirittura il rogo in piazza evocando, a una sola settimana dal giorno della memoria, il rogo della biblioteca dell’Istituto per la ricerca sessuale di Magnus Hirschfeld il 6 maggio 1933 a 4 mesi dall’avvento di Hitler al potere e poco dopo l’aggravamento del paragrafo 175 in base al quale centinaia di migliaia di omosessuali finirono nei lager nazisti.  
   
L’Associazione Radicale Certi Diritti condanna fermamente questi rigurgiti censori e neofascisti e chiede con forza che Milano dimostri di essere la città europea adottando immediatamente le necessarie misure volte alla prevenzione e al contrasto di atti e comportamenti basati su odio omofobico nonché la rimozione di ogni forma di discriminazione basata sull'orientamento sessuale e la promozione di pari opportunità per tutte le famiglie.