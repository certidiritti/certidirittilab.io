---
title: 'Dal IV Congresso di Certi Diritti'
date: Tue, 11 Jan 2011 21:09:46 +0000
draft: false
tags: [Senza categoria]
---

da Pride, gennaio 2011

Diritti civili delle persone che si prostituiscono e delle persone transessuali, matrimonio gay, riforma del diritto di famiglia e movimento transnazionale. Questi i temi affrontati a Roma il 27 e 28 novembre nel IV congresso dell'associazione radicale **Certi Diritti**.

Analisi e progettualità per fermare "la deriva partitocratica dell'Italia e la visione politica ispirata al fondamentalismo ideologico e religioso che rende indispensabile rafforzare un impegno", ha dichiarato **Sergio Rovasio**, segretario dell'Associazione. Con questo spirito hanno partecipato ai lavori  parlamentari, personalità della società civile, rappresentanti delle associazioni che si battono in Italia per la promozione e la difesa dei diritti civili e umani delle persone lesbiche, gay, bisessuali, transgender e anche eterosessuali. Emozionante il ricordo di **Enzo Francone**, militante radicale, nelle parole di **Angelo Pezzana**, attivista del Fuori! negli anni Settanta.

Il congresso ha approvato all'unanimità una mozione che impegnerà l'associazione su tre principali temi politici.

Il primo riguarda il rilancio della campagna di Affermazione civile, grazie alla quale la Corte costituzionale si è espressa lo scorso aprile riconoscendo la rilevanza costituzionale delle coppie gay e sollecitando la classe politica a legiferare in materia di riconoscimento di diritti. La campagna di Affermazione Civile promuove iniziative che prevedono il ricorso alle vie legali quando viene negato dalle autorità il riconoscimento del diritto al matrimonio.

Il secondo tema riguarda le attività in ambito transnazionale e a questo scopo Certi Diritti ha riformato il suo statuto in vista della possibilità di divenire una delle associazioni costituenti il Partito Radicale Nonviolento Transnazionale Transpartito.

Infine il terzo tema riguarda il rilancio della conferenza per la riforma del diritto di famiglia, per promuovere un allargamento del riconoscimento dei diritti, contro una concezione clericale e a senso unico della famiglia, che in una società moderna e democratica non può essere l'unico modello considerato legittimo.

La nuova presidente dell'associazione è **Rita Bernardini**, deputata radicale eletta nel Pd, segretario Sergio Rovasio, tesoriere Giacomo Cellottini, Maria Gigliola Toniollo della Cgil Nuovi Diritti sarà il rappresentante di Certi Diritti presso il Comitato di Radicali Italiani.