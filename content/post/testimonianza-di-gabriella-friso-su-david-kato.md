---
title: 'Testimonianza di Gabriella Friso su David Kato'
date: Mon, 07 Feb 2011 06:13:04 +0000
draft: false
tags: [Africa]
---

Qualche giorno fa ci sono stati i funerali di Kato David Kisule, esponente di rilievo del movimento GBTL in Uganda. Io l'ho conosciuto a Roma e ho parlato a lungo con lui... addirittura aveva voluto il mio indirizzo mail e il mio numero di telefono...  
  
Io proprio gli avevo chiesto perchè non rimanesse in Italia chiedendo asilo politico. Lui mi ha risposto che se lui fosse scappato, sarebbe stata una batosta per il movimento ugandese e quindi non l'avrebbe fatto, anche se era ben consapevole di questa possibilità e del fatto che lui fosse in pericolo nel suo Paese. Mi ha anche raccontato che pochi mesi prima la polizia l'aveva arrestato e picchiandolo gli avevano rotto una spalla. A me e non pubblicamente, perchè aveva paura di dirlo, aveva raccontato della fortuna di lavorare con una ONG e mi ha raccontato del suo lavoro nelle carceri. Lì gli omosessuali sono in condizioni davvero inumane.Si considerava fortunato per questo lavoro.  
  
Mi ha fatto moltissime domande sulle persone omosessuali che seguo nella richiesta di asilo, cosa che era uscita nel gruppo di lavoro. La cosa che per lui in Uganda era più urgente era quella di reperire i soldi per seguire i processi che si sono intentati contro le volenze, gli abusi e anche contro il maledetto giornale che aveva pubblicato le foto dei 100. Per questo avevo parlato con Patanè, perchè ARCI gay si facesse carico del pagamento di uno di questi. Mi ha anche parlato della difficoltà di trovare avvocati che li seguissero in queste cause e diceva che anche quelli che erano disponibli, applicavano tariffe alte a causa dei rischi che anche loro correvano. Lui era profondamente convinto che la battaglia in Uganda si sarebbe vinta solo grazie alla pressioni internazionali e che invece se si fosse persa e avessero approvato quella legge aberrante, altri stati avrebbero fatto altrettanto...  
  
Quando ci siamo salutati lui mi ha detto di continuare ad occuparmi dei ragazzi migranti ed io a lui di stare attento... Ma come si poteva esserlo in casa sua????? Quello che si sa è che è stato ucciso a bastonate o con un ascia proprio a casa sua!!!!!!!!!!!  
  
Solo in un giorno ho capito quanto fosse grande come persona e quanta determinatezza avesse in quel corpo minuscolo...  
  
Non ho parole per esprimere la rabbia e il dolore per quanto è accaduto.  
  
Gabriella Friso