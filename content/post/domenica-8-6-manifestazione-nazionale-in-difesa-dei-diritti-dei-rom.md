---
title: 'DOMENICA 8-6 MANIFESTAZIONE NAZIONALE  IN DIFESA DEI DIRITTI DEI ROM'
date: Mon, 09 Jun 2008 10:59:53 +0000
draft: false
tags: [Comunicati stampa]
---

L'Associazione Radicale Certi Diritti parteciperà oggi con il suo Segretario Sergio Rovasio alla Manifestazione nazionale in difesa dei diritti dei Rom che partirà oggi, domenica 8 giugno, dal Colosseo e raggiungerà il Foro Boario all'ex Mattatoio.  
Alla manifestazione parteciperanno parlamentari di diversi gruppi politici e rappresentanti di decine di Associazioni e della Società Civile.