---
title: 'Ministro Fornero si impegna contro discriminazioni persone omosessuali e trans. I politici clerical-fondamentalisti la smettano con i loro grugniti'
date: Wed, 01 Feb 2012 13:05:21 +0000
draft: false
tags: [Politica]
---

L'associazione radicale Certi Diritti chiede un incontro per offrire il suo contributo insieme alle altre associazioni lgbt(e).

Roma, 1 febbraio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

La Ministra del Welfare Elsa Fornero  ieri, durante l’audizione svolta davanti alle Commissioni Lavoro e Affari Costituzionali, ha dichiarato  il suo  “pieno impegno contro le discriminazioni anche verso le persone omosessuali e  transgender” e che “è sotto gli occhi di tutti il grave ritardo culturale, di apertura mentale che il nostro Paese rappresenta in tema di pari opportunità, nell’accesso ai diritti rispetto alle diversità,  che sono molte tra le persone e che non possono essere oggetto e motivo di discriminazione".  La Ministra ha confermato il suo impegno a correggere questa impostazione: "I diritti sono importanti e l’impegno contro le discriminazioni e contro chi le fa sorgere deve essere massimo. La diversità è un valore, deve essere tra le cose che i bambini imparano da piccoli”.

L’Associazione Radicale Certi Diritti ringrazia la Ministra Elsa Fornerò e chiederà un incontro urgente per poter offrire il suo contributo insieme alle altre organizzazioni lgbt(e) italiane, rispetto all’impegno preso sull’attività del suo dicastero.

E’ davvero sorprendente di come appena si accenna al riconoscimento di diritti e lotta alle discriminazioni delle persone lgbt(e), come avviene in tutti i paesi civili e democratici,  subito gli sciacalli della politica reazionaria e clerical-fondamentalista emettano i loro grugniti ipocriti e nauseabondi; prima premettono sempre di essere contrari alle discriminazioni in base all’orientamento sessuale delle persone, poi però subito dicono che è  inaccettabile contestare il naturale rapporto uomo-donna.