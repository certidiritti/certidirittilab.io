---
title: 'Il Mullah Casini e la madre superiora Rosy Bindi fanno prove tecniche di clericalismo d''accatto sulla pelle delle persone lesbiche e gay'
date: Fri, 20 Jul 2012 11:57:51 +0000
draft: false
tags: [Politica]
---

Roma, 20 luglio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Il Mullah Pierferdinando Casini, grande maestro di coerenza che ha persino usufruito della legge sul divorzio, tanto combattuto dai suoi, e la madre superiora Rosy Bindi sono i nuovi alleati del fronte contro i diritti delle coppie omosessuali. Il nuovo asse politico fa a gara a chi la spara più grossa pur di difendere la propria visione clerical-fondamentalista, impregnata di cultura politica basata sul pregiudizio.

Occorre ricordare a lorsignori che la Corte Costituzionale con la sentenza 138/2010 non esclude il matrimonio tra persone dello stesso sesso, come hanno confermato recentemente i giurisiti Zagrebelsky e Rodotà, quindi il mullah Casini nella sua propaganda di demagogia e di populismo anti-gay con le sue affermazioni insulta persino la Corte Costituzionale.

Esistono dei Trattati internazionali, delle Direttive Europee e continue sollecitazioni del Parlamento Europeo e del Consiglio d’Europa sulla promozione di diritti e tutele per le persone lesbiche e gay che certa classe politica, prima di blaterare frasi offensive e deliranti,  dovrebbe conoscere. Essere europeisti a intermittenza qualifica politicamente questi personaggi: troppo comodo esserlo per i temi economici e non sui temi dei diritti civili.

E’ inaccettabile che rappresentanti delle istituzioni diano degli incivili alle coppie omosessuali che chiedono l’accesso all’istituto del matrimonio civile. Casini di fatto ha dato dell’incivile a Obama, Hollande, alla Spagna, all’Olanda e tutti quei paesi dove il matrimonio tra persone dello stesso sesso è realtà. In Italia ci sono centinaia, se non migliaia, di coppie omogenitoriali che sono privi di diritti e riconoscimenti, ora si devono sentire anche le offese del Mullah Casini o di suor Rosy Bindi.

Organizzazioni scientifiche americane, canadesi e tedesche hanno fatto studi  su come i bambini di coppie omosessuali crescono e tutti i  giudizi sono stati positivi.  Tutto questo dimostra che è Casini l'incivile che si muove su linee ideologiche e di pregiudizio sulle quali vuole plasmare una realtà che è diversa a costo di discriminare centinaia di migliaia di cittadini italiani. Noi vogliamo uno Stato di diritto e non uno Stato fondamentalista, clericale e totalitario!