---
title: 'Processo di Revisione Universale 2014'
date: Thu, 17 Apr 2014 22:49:37 +0000
draft: false
tags: [Transnazionale]
---

[![UN-UPR-Geneva](http://www.certidiritti.org/wp-content/uploads/2014/04/UN-UPR-Geneva-300x200.jpg)](http://www.certidiritti.org/wp-content/uploads/2014/04/UN-UPR-Geneva.jpg)La riforma delle Nazioni Unite per i diritti umani ha istituito nel 2008, nell’ambito delle prerogative del nuovo Consiglio per i Diritti Umani delle Nazioni Unite a Ginevra, l’Universal Periodic Review (UPR-Processo di Revisione Universale) una nuova procedura di monitoraggio dei diritti umani che ha per obiettivo la valutazione periodica (ogni 4 anni) dei progressi compiuti da ciascuno dei 192 Paesi delle Nazioni Unite, indipendentemente dalla ratifica dei trattati internazionali e anche in base agli impegni assunti a livello politico (voluntary pledges).

Il 9 febbraio 2010 ha avuto luogo a Ginevra a Palais des Nations, sede del Consiglio per i Diritti Umani delle Nazioni Unite, il primo esame dell'Italia nell'ambito della UPR ([www.upr-info.org](http://www.upr-info.org/)).

Una coalizione di associazioni composta da ILGA-Europe, Centro Risorse LGBTI, Associazione Radicale Certi Diritti, Famiglie Arcobaleno e Intersexioni hanno prodotto due documenti sulla situazione dei diritti umani delle persone LGBTI in Italia in previsione del prossimo esame in ambito UPR a cui l'Italia sarà sottoposta nel corso del 2014. Il [documento più sintetico](http://www.certidiritti.org/wp-content/uploads/2014/04/UPR-JLD-final.pdf) è stato presentato al Consiglio ONU per i diritti umani, [quello più esteso](http://www.certidiritti.org/wp-content/uploads/2014/04/UPR_Italiano1.pdf) è alla base delle osservazioni che stiamo presentando al Governo italiano durante le audizioni con le ONG italiane. La prima si è svolta il 17 aprile 2014, le prossime si svolgeranno a giungo (prima della presentazione del documento  governativo all'ONU), a settembre (prima dell'avvio del procedimento ad ottobre) e a dicembre.

Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti e coordinatore del gruppo di lavoro, afferma: «La Revisione Universale è un momento fondamentale per porre le basi dell’azione governativa sui diritti umani nei prossimi 4 anni. È la prima volta che una coalizione di associazioni presenta alle Nazioni Unite una submission integralmente dedicata ai diritti umani delle persone LGBTI, che finalmente sono entrati a pieno titolo nel procedimento. Ringrazio il Comitato Interministeriale per i Diritti Umani per l’attenzione dimostrata nei confronti delle Organizzazioni non Governative coinvolte con audizioni periodiche nel corso di tutto il Processo di Revisione Universale».

[Osservazioni presentate durante l'incontro con il Comitato Interministeriale per i Diritti Umani del 17 aprile 2014.](http://www.certidiritti.org/wp-content/uploads/2014/04/Intervento-CIDU.pdf)