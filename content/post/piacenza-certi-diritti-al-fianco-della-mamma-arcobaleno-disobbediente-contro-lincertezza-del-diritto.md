---
title: 'Piacenza: Certi Diritti al fianco della mamma arcobaleno disobbediente, contro la incertezza del Diritto.'
date: Tue, 21 Aug 2018 11:47:13 +0000
draft: false
tags: [Diritto di Famiglia]
---

![2948](http://www.certidiritti.org/wp-content/uploads/2018/08/2948.jpg)"Siamo vicini a Sara, la mamma arcobaleno disobbediente di Piacenza, che ha deciso di autodenunciarsi in seguito alla richiesta degli uffici di stato civile della sua città di non menzionare la procreazione medicalmente assistita che ha permesso la nascita della figlia e di dichiarare nell'atto di nascita la presenza di un padre - anche se non menzionato nell'atto - col quale sarebbe avvenuto il concepimento. La stessa pubblica amministrazione, quindi, suggerirebbe di compiere un reato, dichiarare il falso, per sottrarre una bambina all'incertezza cui sarebbe stata condannata dalla schizofrenia delle leggi che esistono e, soprattutto, di quelle che non esistono nel nostro paese"

Così Leonardo Monaco, segretario dell'Associazione Radicale Certi Diritti.

"In bocca al lupo a Sara, Irene e all'avvocato Alexander Schuster che le seguirà in questa disobbedienza civile. C'è grande bisogno di nonviolenza per sfidare l'inerzia e i divieti che insistono sulle nostre vite e svuotano il Diritto."