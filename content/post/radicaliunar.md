---
title: 'RADICALI/UNAR: FRANCESCO SPANO È IL NUOVO DIRETTORE UNAR. AUGURI E BUON LAVORO, MA PER FARE CHE COSA?'
date: Mon, 18 Jan 2016 23:24:42 +0000
draft: false
tags: [Politica]
---

[![unar](http://www.certidiritti.org/wp-content/uploads/2015/09/unar-300x122.png)](http://www.certidiritti.org/wp-content/uploads/2015/09/unar.png)

**Comunicato Stampa dell'Associazione Luca Coscioni, dell'Associazione Radicale Certi Diritti e di Radicali Italiani**

Roma, 18 dicembre 2015

 E’ ufficiale: il nuovo direttore di UNAR sarà Francesco Spano, avvocato toscano, già segretario generale del Maxxi, ex dello staff del Gruoppo PD e di Amato, con esperienza sull’immigrazione. Il nome circolava da prima di Natale, ma resta da capire se il Governo ha proceduto con un avviso pubblico, quanti curricula sono stati presentati ed in base a quale criterio è stato nominato lui.

Dopodiché la questione rimane quella che da sempre noi solleviamo: quale è il posto, il ruolo, gli strumenti che gli organismi di parità giocano in Italia? E’ vero che UNAR (o parte consistente delle sue attività) verrà spostata al Ministero Politiche sociali? Chi si occuperà di immigrazione, disabili, persone LGBTI e della relativa strategia nazionale, di rom e della relativa strategia nazionale? La smetteremo di essere considerati lo zimbello d’Europa perché la nostra Rete antidiscriminazioni è direttamente controllata dal Governo e quindi non è un organo indipendente? È vero che il Governo dopo i fatti che hanno visto coinvolta Meloni ha bloccato il Decreto che approva le procedure di intervento della Rete?

Tutte domande che ripetiamo da tempo e a cui il Governo non dà risposte.

Filomena Gallo - Associazione Luca Coscioni

Yuri Guaiana - Associazione Radicale Certi Diritti​

Riccardo Magi - Radicali Italiani