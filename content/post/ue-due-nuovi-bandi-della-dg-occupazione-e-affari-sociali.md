---
title: 'UE: DUE NUOVI BANDI DELLA DG OCCUPAZIONE E AFFARI SOCIALI'
date: Tue, 10 Jun 2008 17:44:53 +0000
draft: false
tags: [Comunicati stampa]
---

Due nuovi bandi della DG Occupazione e affari sociali: [Call for tender VT/2008/003](http://ec.europa.eu/employment_social/emplweb/tenders/tenders_en.cfm?id=3185) e  
[Call for tender VT/2008/007](http://ec.europa.eu/employment_social/emplweb/tenders/tenders_en.cfm?id=3186) . Mappatura delle best practice e creazione di un network socio economico di esperti nel campo dell'anti-discriminazione. **[Call for tender VT/2008/003 - Mapping Study on the Trade Unions practices in fighting discrimination & promoting diversity](http://ec.europa.eu/employment_social/emplweb/tenders/tenders_en.cfm?id=3185)  
**The purpose of this study is to identify significant/innovative initiatives taken by trade unions to combat discrimination at the workplace on the grounds of race and ethnic origin, religion or belief, age, disability, sexual orientation, and/or to promote diversity in the working environment across the 27 Member States, as well as the EFTA/EEA and candidate countries. It should also help in suggesting how best practices can be made more visible and used, and how the Commission can better support the awareness of trade unions' activities in that field.  
Link:  
  
  
**[Call for tender VT/2008/007 - Establishment of a Network of socio-economic experts in the anti-discrimination field](http://ec.europa.eu/employment_social/emplweb/tenders/tenders_en.cfm?id=3186)  
**The purpose of this contract is to establish and maintain a network of socio-economic experts in the field of discrimination on the grounds of race and ethnic origin, religion or belief, age, disability, sexual orientation as well as on multiple grounds. The network shall provide the Commission with independent expertise and advice.