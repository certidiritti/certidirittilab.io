---
title: 'CERTI DIRITTI E ASSOCIAZIONE COSCIONI A MANIFESTAZIONE NO VAT SABATO 14-2'
date: Fri, 13 Feb 2009 14:52:12 +0000
draft: false
tags: [clericale, Comunicati stampa, MANIFESTAZIONE, no vat, vaticano]
---

**SABATO 14-2 ASSOCIAZIONE LUCA COSCIONI E ASSOCIAZIONE RADICALE CERTI DIRITTI PARTECIPANO ALLA MANIFESTAZIONE ‘NO VAT’ CONTRO LO STRAPOTERE CLERICALE VATICANO.**

**

Sabato 14 febbraio, a Roma, si svolgerà la manifestazione nazionale No Vat, promossa da Facciamo Breccia insieme a Movimenti e Associazioni provenienti da tutta Italia.

**La manifestazione, che partirà alle ore 14 da Piazza Esedra e sfilerà per il centro fino a raggiungere Campo dè Fiori**, ha tra i suoi obiettivi la lotta allo strapotere clericale e Vaticano; per l’autodeterminazione e la libertà di scelta responsabile delle persone in ogni fase della vita; contro la violenza omofobica e transfobica; per l’eliminazione di leggi dogmatiche e clericali come la legge 40 sulla fecondazione assistita; per l’abolizione del Concordato e dei privilegi come l’esenzione Ici e i profitti dell’otto per mille.

Alla manifestazione hanno assicurato, tra gli altri, la loro partecipazione: Marco Cappato, deputato europeo radicale e Segretario Associazione Luca Coscioni; Mina Welby, dirigente radicale ed esponente; Rocco Berardo, Tesoriere Associazione Luca Coscioni, Sergio Rovasio, Segretario Associazione Radicale Certi Diritti.

**