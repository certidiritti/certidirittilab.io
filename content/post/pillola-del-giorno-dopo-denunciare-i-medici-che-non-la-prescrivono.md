---
title: 'PILLOLA DEL GIORNO DOPO: DENUNCIARE I MEDICI CHE NON LA PRESCRIVONO!'
date: Sun, 06 Apr 2008 11:22:42 +0000
draft: false
tags: [Comunicati stampa]
---

Dichiarazione di Sergio Rovasio, Segretario Associazione radicale Certi Diritti**:**

“Quanto documentato oggi dal quotidiano Repubblica a pagina 9, sull’impossibilità nei week end di trovare medici disposti a prescrivere la ‘pillola del giorno dopo’ in diverse città italiane, dimostra il livello di ipocrisia e illegalità che si manifesta sotto la voce ‘obiezione di coscienza’

 Essere obiettore di coscienza, come insegna la storia politica dei radicali, è assumersi fino in fondo le proprie responsabilità, anche andando in galera autodenunciandosi, non certo imponendo a tutti i cittadini la propria visione fondamentalista della vita, violando impunemente la legge. Se i medici ‘obiettori’ non si autodenunciano li denunciamo noi. Nessun direttore sanitario, del resto, deciderebbe di mettere medici Testimoni di Geova nei centri di trasfusione del sangue e se lo facessero, quegli stessi medici pseudo obiettori che non prescrivono le ricette per i medici contraccettivi, avrebbero forse qualcosa da dire.

 La responsabilità della diffusa presunta obiezione di coscienza è quindi anche dei Direttori sanitari che dovrebbero vigilare sulla applicazione della legge, difatti l’obiezione di coscienza per i medici è prevista solo per la Legge 194, quando è in corso una gravidanza, non certo per la prescrizione di un medicinale contraccettivo d’emergenza.

 Il coordinamento legale ‘soccorso civile’ dell’Associazione Luca Coscioni, sostiene e aiuta coloro che si sono imbattuti in Ospedali nel week end con medici che non rispettano la legge non prescrivendo la ‘pillola del giorno dopo’.

Al sito internet [http://www.lucacoscioni.it/pillola\_del\_giorno_dopo](http://www.lucacoscioni.it/pillola_del_giorno_dopo) è possibile scaricare il modulo tipo per denunciare alla magistratura coloro che vogliono imporre a tutti la propria visione fondamentalista della vita.