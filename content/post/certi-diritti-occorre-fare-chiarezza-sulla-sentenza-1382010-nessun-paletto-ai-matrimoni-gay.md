---
title: 'Certi Diritti: occorre fare chiarezza sulla sentenza 138/2010, nessun paletto ai matrimoni gay'
date: Thu, 15 Sep 2011 21:33:34 +0000
draft: false
tags: [Matrimonio egualitario]
---

La sentenza della Corte costituzionale affida totale libertà e responsabilità al legislatore, incluso il matrimonio civile.

A seguito dell’ intervento di D’Alema contro il matrimonio civile tra persone dello stesso sesso, abbiamo appreso che nel PD sta lavorando, da tempo, una commissione interna per formulare una proposta programmatica sulle unioni civili (e speriamo che includano anche le persone eterosessuali).

La commissione è presieduta da Rosy Bindi, che già per i DiCo si era rivolta più volte alle alte gerarchie cattoliche. Non è certo una buona premessa per un risultato politicamente credibile.

Anche perché questa volta le associazioni LGBT propongono il diritto al matrimonio, come innegabile e fondamentale principio di uguaglianza e sulla scia di quello che accade in quasi tutti i paesi democratici.

Le affermazioni di d’Alema hanno generato una dura e responsabile reazione (nessuna delle quali polemica) perché conteneva gravi mistificazioni ed errate interpretazioni della Costituzione e della importante sentenza 138/2010 della Corte Costituzionale. Sentenza che,  lo ricordiamo, è avvenuta grazie principalmente all’impegno politico proprio dell’ Associazione Radicale Certi Diritti. Per la commissione, sentire gli esponenti di questa, come di altre associazioni, non sarà certo difficile né immotivato anche perché molti esponenti del Pd hanno sostenuto e continuano a sostenere convintamente questa battaglia di civiltà.

Nel dubbio, cogliamo l’occasione per fare chiarezza su questa sentenza, che, al contrario di quanto affermato dall'onorevole Paola Concia su L'Unità, non pone alcun paletto al matrimonio tra persone dello stesso sesso, e invece lascia la totale libertà e responsabilità al legislatore, sollecitando quanto prima una soluzione su questo tema. La sentenza può destare confusione perché ad un certo momento si sofferma a descrivere quella che – presumibilmente – era l’intenzione originaria dei costituenti: matrimonio come unione tra uomo e donna indissolubile. In questo però  non ha preteso di fissare tale interpretazione come immutabile e giurisdizionalmente vincolante, vista l’evoluzione sociale degli ultimi decenni. E anche visto che il matrimonio indissolubile non è  già più.

Non solo. La Corte Costituzionale pone anche l’evidenza di una necessaria parificazione tra le coppie coniugate e quelle omosessuali. Cosa, politicamente parlando, obbligherebbe a non farlo attraverso il matrimonio civile (che non tange nessun sentimento religioso) non è chiaro, salvo usare sempre le convinzioni religiose e un certo sentimento di altri che nulla ha a che fare con le decisioni di una coppia.

Se comunque il PD volesse ostinarsi a non aiutare le persone omosessuali a unirsi in matrimonio con rito civile, una cosa è certa: una buona legge che riconosca alle coppie dello stesso sesso le stesse conseguenze del matrimonio è sicuramente bene accetta, da tutti.

In tutto questo, però, è ancor meno chiara l’abitudine che hanno i leader del Partito Democratico di proporre una unione civile per le persone omosessuali solo giustificandola con il postulato del necessario svilimento e disapprovazione del matrimonio tra persone dello stesso sesso. Tale posizione è politicamente debole e poco convincente.

Magari è perché al PD basta sventolare l'idea di una ipotetica possibile legge sulle unioni civili, poiché tutti sanno bene che nessuna persona, né associazione gay e lesbica, avrebbe alcun valido motivo per contrastare una buona legge sulle unioni civili. Quello che  forse il PD non ha ancora capito, è che la comunità LGBT non accetterà più chi ancora trova un valido motivo nel contrastare, anche solo a parole, il diritto al matrimonio. La logica matrimonio vs unioni civili è vecchia e superata.

Tutte le associazioni LGBT “serie” (come direbbe d’Alema) sono consapevoli che, con il PD o senza il PD, ci avvicineremo sempre di più a questa meta. E se accadrà, magari anche grazie ad una buona unione civile. Anche se qualcuno ci tiene a dire il contrario, ogni conquista ci avvicina sempre più all’obiettivo del diritto al matrimonio civile anche per le persone omosessuali.

Non a caso, la campagna giuridica che sta intentando Paola Concia, per il riconoscimento della unione civile che ha contratto in Germania, si sta avviando con l’aiuto degli stessi avvocati che già da tempo, seguono e sostengono la battaglia per il matrimonio civile tra persone dello stesso sesso portata avanti da Certi Diritti. Questo, oltre ad essere una conferma, è il vanto che – piaccia o non piaccia – le battaglie giuridiche e politiche, per le unioni civili e per il matrimonio civile, per le persone eterosessuali e omosessuali, non dividono, ma uniscono le associazioni, le persone e l’opinione pubblica. Chissà se la commissione presieduta da Rosy Bindi se n’è accorta e se vorrà approfondire meglio la questione.

A breve uscirà un libro di Stampa Alternativa che chiarirà ancora meglio quanto dice la sentenza n 138/2010 della Corte Costituzionale.

  
Sergio Rovasio  
Segretario Associazione Radicale Certi Diritti

**[http://www.certidiritti.it/iscriviti](iscriviti)**