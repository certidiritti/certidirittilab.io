---
title: 'BISESSUALITA'' DONO NATURA, LA CHIESA CONTRO CHI SE LA PRENDERA'' ORA?'
date: Sat, 16 Aug 2008 13:25:56 +0000
draft: false
tags: [Comunicati stampa]
---

**BISESSUALITA’: DONO DELLA NATURA, CONFERMA DAL JOURNAL OF SEXUAL MEDICINE. E ORA LA CHIESA COSA FARA’? SE LA PRENDERA’ CON LA DIABOLICA NATURA?**

Dichiarazione di Sergio Rovasio, Segretario Associazione radicale Certi Diritti:

“Secondo uno studio fatto da ricercatori italiani, guidati dal Professor Andrea Camperio Ciani dell’Università di Padova, pubblicato dal Journal of sexual medicine, la bisessualità può avere una base genetica così come già documentato per l’omosessualità. Quanto pubblicato dalla rivista, tra le più autorevoli in tema di sessualità, dimostra che non esistono per la bisessualità o l’omosessualità influenze causate da perversioni o, peggio ancora, da ‘peccati’, semplicemente fa parte della natura. E allora adesso cosa farà la chiesa? Condannerà la diabolica natura? Le persone lgbt sono ancora oggetto di condanna da parte della chiesa e di totale mancanza di diritti, è bene ricordarlo. La politica dovrebbe dare più ascolto alla scienza”.