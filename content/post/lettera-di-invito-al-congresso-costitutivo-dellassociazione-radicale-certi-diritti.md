---
title: 'Lettera di invito al Congresso costitutivo dell''Associazione Radicale Certi Diritti'
date: Tue, 20 Apr 2010 20:35:28 +0000
draft: false
tags: [Senza categoria]
---

Il prossimo **1 marzo** nascerà a **Roma** **l'Associazione Radicale Certi diritti** che ha, tra i suoi obiettivi, quello di battersi in campo politico, giuridico e culturale per **l'accesso all'istituto del matrimonio per le persone omosessuali e per** la tutela dei **diritti civili in materia di identità di genere**; intende caratterizzare il suo impegno in favore di **campagne informative** sull'educazione sessuale, contro la violenza omofobica, contro la penosa, **umiliante e discriminatoria legge 40** sulla fecondazione assistita e per promuovere campagne contro la **violenza domestica sulle donne.**

Con questi e altri obiettivi l'Associazione intende essere protagonista della **Conferenza permanente per la Riforma  del Diritto  di Famiglia** e ispirerà il suo impegno alle lotte nonviolente di quanti hanno combattuto e vinto il razzismo, la segregazione e la discriminazione per garantire l'universalità dei diritti civili.

Anche per questo vogliamo **dedicare la nostra iniziativa alla memoria di Makwan Moloudzadeh** (il ragazzo di 21 anni impiccato in Iran il 5 dicembre 2007 perchè accusato di aver commesso atti omosessuali) e a coloro che hanno sofferto abusi, discriminazioni e violenze a causa della propria condizione sessuale.

**INFORMAZIONI LOGISTICHE SUL CONGRESSO COSTITUTIVO:**

_I lavori, si svolgeranno **sabato 1 marzo**, a **Roma**, dalle ore 9.30 alle ore 20, presso la **Sala delle Bandiere del Parlamento Europeo**, Via Quattro Novembre, 149 (tra Via Nazionale e Piazza Venezia)._  
**Per partecipare e votare al Congresso costitutivo** con inclusa l'iscrizione all'Associazione per il 2008 - occorre versare la quota di almeno 30 Euro. Ci si può iscrivere anche presso la sede congressuale. Senza l'iscrizione non si ha diritto di voto. Ovviamente **anche i non iscritti possono assistere ai lavori.  
**Informazioni e preannunci di partecipazione all'indirizzo [certidiritti@radicali.it](mailto:certidiritti@radicali.it)

Nell'ambito del Congresso sono previste le seguenti relazioni:

**Conferenza permanente per la Riforma del Diritto di Famiglia** di Bruno De Filippis, Giurista

**Le famiglie nella Costituzione, cosa non dice l'art. 29. Trucchi e manipolazioni nel dibattito pubblico** di Mario Di Carlo, Avvocato

**La Rete Lenford - Avvocatura per i diritti LGBT. Associazione italiana di  avvocati  che si occupa della tutela giudiziaria delle persone (e coppie) omosessuali** di Francesco Bilotta, Avvocato, Docente di Diritto Privato presso l'Universit di Udine

**Il movimento di liberazione sessuale, alcune proposte di azione** di Enzo Cucco, Direttore della Fondazione Sandro Penna di Torino

**L'Europa dei diritti** di Ottavio Marzocchi, radicale, funzionario del  Gruppo Alde del Parlamento Europeo

**Le adozioni per le persone gay, perché no?** di Chiara Lalli

**Il no al registro delle unioni civili a Roma** di Massimiliano Iervolino, Segretario Radicali Roma