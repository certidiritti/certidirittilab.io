---
title: 'GIORNATA DELLA MEMORIA'
date: Wed, 26 Jan 2011 12:37:04 +0000
draft: false
tags: [Comunicati stampa, memoria, nazismo, omocausto]
---

**GIORNATA DELLA MEMORIA: A ROMA CERTI DIRITTI PROMUOVE CON ALTRE ASSOCIAZIONI LGBTE COMMEMORAZIONE DELLE VITTIME DELLA VIOLENZA NAZI-FASCISTA**

Comunicato Stampa dell'Associazione Radicale Certi Diritti

 Roma, 25 gennaio 2011

In occasione della Giornata della Memoria - giovedì 27 gennaio - l'Associazione Radicale Certi Diritti insieme ad altre associazioni lgbte terranno alle 18.30 una cerimonia commemorativa per ricordare le vittime della violenza nazi-fascista. L’appuntamento è a Roma, in piazzale Ostiense, dove dal 25 aprile 1995 si erige il  monumento “tutti potenziali bersagli” mai inaugurato ed in stato di abbandono in ricordo delle vittime dell’Olocausto. Le associazioni intendono ricordare attraverso la lettura di documenti e poesie l’eccidio e le persecuzioni perpetrate al popolo ebraico e tutti gli stermini ancora oggi troppo spesso dimenticati fra i quali quello di Rom e Sinti, delle persone disabili e l’”omocausto” di lesbiche e gay.

Le associazioni confidano anche nella sensibilità dell’attuale amministrazione comunale per la realizzazione di un monumento alla Memoria delle persone omosessuali e transessuali perseguitate durante tutti i totalitarismi, così come è già avvenuto nelle città di Amsterdam, Berlino, Auschwitz, Nuengamme, Sachsenhausen e Bologna. Un monumento commemorativo era già stato promesso dalla precedente amministrazione nel 2004 dopo l’approvazione all’unanimità in Consiglio Comunale di una mozione ispirata da una proposta di GayRoma.it e già sottoscritta da larga parte del movimento Lgbte.

**Associazione Radicale Certi Diritti**  
**Radicali Roma  
Di Gay Project  
Circolo di Cultura Omosessuale Mario Mieli  
Fondazione Massimo Consoli  
CGIL Nuovi Diritti  
Associazione Libellula  
GayLib Lazio  
Arcigay Roma  
Agedo Roma  
GayRoma.it **

**\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\***

**Il monumento di piazzale Ostiense a Roma: “Tutti potenziali bersagli”.**

Il **25 aprile del 1995**, in occasione dei festeggiamenti del cinquantesimo anniversario della Liberazione veniva montato in piazzale Ostiense a Roma (luogo storico della resistenza capitolina), il monumento "**tutti potenziali bersagli**" dedicato alle vittime del nazismo e del razzismo, e piu' in generale, a tutte le minoranze perseguitate e ai diversi del mondo.

Era la terza volta, dopo quello a **Giordano Bruno** a Campo de Fiori (promosso da una colletta universitaria) e quello dedicato a **Walter Rossi** nell'omonima piazza finanziato dal movimento degli anni Settanta, che un monumento fosse voluto, ideato, realizzato e finanziato dalla gente comune

Pensato e creato con l'intento di consegnarlo ad un immaginario collettivo il piu' ampio possibile come luogo di memoria che inviti i passanti a riflettere anche solo per pochi istanti sugli effetti delle persecuzioni politiche e razziali, il monumento ottenne il sostegno di associazioni di base, centri sociali e case occupate di Roma, Milano, Ostia e Seregno che si impegnarono a raccogliere i fondi necessari.

La sua vita non fu affatto semplice; oggetto di interpellanze comunali che ne richiedevano la rimozione, solo un'enorme determinazione, la raccolta di firme e le manifestazioni in suo favore hanno consentito ancora oggi di non farlo togliere.

Oggi l'opera e' inserita nel censimento del patrimonio artistico del Comune di Roma, ma e' ancora a rischio rimozione, in quanto il progetto originario di messa in posa definitiva non e' stato ancora realizzato.

L'opera raffigura cinque sagome da tiro al bersaglio con le mani legate dietro la schiena davanti a degli specchi, con cinque triangoli colorati sui loro petti che stanno a ricordare come i nazisti distinguevano gli internati nei loro campi di sterminio: rosa per gli omosessuali, azzurro per gli immigrati, doppio triangolo giallo (Stella di Davide) per gli ebrei, rosso per gli antifascisti e marrone per i nomadi.