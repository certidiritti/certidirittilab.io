---
title: 'MISS ESCORT 2011: LEGALIZZAZIONE DELLA PROFESSIONE DI ESCORT'
date: Fri, 15 Apr 2011 15:16:29 +0000
draft: false
tags: [Lavoro sessuale]
---

Certi Diritti partecipa a Bologna alla prima riunione delle escort italiane. Questa sera elezione di Miss Escort 2011.

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Questa sera a Bologna presso il Bolero Palace  di Altedo, si terrà la prima riunione delle Escort italiane con l’obiettivo, tra gli altri, di promuovere la  legalizzazione della professione.

Per la prima volta, personalità, Associazioni e decine di Escort, provenienti da diverse città italiane, si ritroveranno per confrontarsi, promuovere iniziative e campagne finalizzate alla regolamentazione della professione, contro la clandestinità e i comportamenti ipocriti, ad ore, di coloro che di giorno fanno i moralisti perbenisti e bacchettoni, e di notte sono scatenati in orge e festini di ogni genere.

A Bologna verrà elaborata una Proposta di Legge sulla regolamentazione dell’attività delle persone Escort che verrà proposta ai  Parlamentari Radicali Rita Bernardini, Donatella Poretti e Marco Perduca, che già hanno depositato a inizio Legislatura una proposta per la legalizzazione della prostituzione.

Questa sera, in occasione del meeting, verrà eletta Miss Escort 2011. Tra i membri della giuria ci sarà il Segretario dell’Associazione Radicale Certi Diritti, Sergio Rovasio. Tra i criteri per il voto si terrà conto anche delle proposte elaborate per la promozione e la difesa dei diritti  della professione di Escort.