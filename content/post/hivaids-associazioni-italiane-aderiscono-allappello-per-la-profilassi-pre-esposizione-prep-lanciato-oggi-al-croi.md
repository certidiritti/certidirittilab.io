---
title: 'HIV/AIDS: associazioni italiane aderiscono all''appello per la profilassi Pre-Esposizione (PrEP) lanciato oggi al Croi'
date: Tue, 24 Feb 2015 08:06:58 +0000
draft: false
tags: [Salute sessuale]
---

**[![f64b0864e862d3c122851be4770d6e9a](http://www.certidiritti.org/wp-content/uploads/2012/07/f64b0864e862d3c122851be4770d6e9a-300x168.jpg)](http://www.certidiritti.org/wp-content/uploads/2012/07/f64b0864e862d3c122851be4770d6e9a.jpg)****HIV/AIDS: lanciato l'appello per la profilassi Pre-Esposizione (PrEP)**

**Roma - Una rete di associazioni italiane ha sottoscritto il [Manifesto europeo della prevenzione HIV](http://www.certidiritti.org/wp-content/uploads/2015/02/PrEP-public-letter-ITA-1.pdf)**, lanciato da decine di organizzazioni per chiedere alle industrie farmaceutiche, alle istituzioni nazionali ed europee di rendere la PrEP (profilassi pre-esposizione) disponibile e accessibile in Europa, così come già avviene negli Stati Uniti dal 2012. 

**Il Manifesto, lanciato in occasione della Conferenza sui Retrovirus e sulle infezioni opportunistiche (Croi) dallo European AIDS Treatment Group**, un network di oltre 110 attivisti di 40 paesi europei, e Aides, organizzazione francese di lotta all'Aids, è stato **sottoscritto in Italia da: associazione Radicale Certi Diritti, Plus onlus - network di persone Lgbt sieropositive, Lila - Lega Italiana per la Lotta contro l'Aids, Circolo di cultura omosessuale Mario Mieli e Nadir onlus**. Le associazioni chiedono, tra l’altro, all'azienda farmaceutica Gilead di presentare quanto prima una richiesta di indicazione per Truvada® come PrEP all'Agenzia Europa del Farmaco, la quale dovrebbe anche chiarire i percorsi regolatori per avere accesso alla stessa.

**Numerosi studi (Iprex, Proud, Ipergay), hanno dimostrato l'efficacia della PrEP** nel ridurre drasticamente il rischio di acquisire l'infezione da Hiv attraverso rapporti sessuali tra Maschi che fanno sesso con Maschi (Msm). La stessa **Organizzazione Mondiale per la Salute (Oms) **ha inserito la PrEP nelle sue linee guida, come uno degli strumenti che è possibile utilizzare, come il preservativo, per prevenire il contagio tra le popolazioni più vulnerabili al rischio di infezione. Al governo italiano si chiede, oltre all’accesso alla PrEP, anche di esaminare come rendere la PrEP rimborsabile per coloro che ne hanno bisogno. Le associazioni sono disponibili a lavorare con le istituzioni per trovare un percorso di larga e piena accessibilità sanitaria ed economica al farmaco. 

**Le associazioni chiedono che in Europa e in Italia uomini, donne, transgender possano accedere a una profilassi per prevenire nuove infezioni da HIV**, per cui esiste un protocollo medico già da tempo sperimentato negli Stati Uniti e che è stata consigliata dall'Oms nelle sue linee guida. L'obiettivo è incrementare il numero di strumenti di prevenzione per sviluppare strategie di prevenzione combinate. Occorre garantire a tutti il diritto di scegliere se usare o meno la PrEP e di poterla usare in maniera sicura. Altrimenti il rischio è che si sviluppi ulteriormente un uso informale della PrEP senza adeguato controllo medico. 

**"La PrEP è uno strumento di prevenzione che si è dimostrato scientificamente efficace"**, affermano **Yuri Guaiana**, segretario di Certi Diritti, **Alessandra Cerioli** presidente di Lila, **Massimo Farinella**, referente salute e servizi del circolo Mario Mieli, **Filippo von Schloesser **presidente di Nadir e **Sandro Mattioli** presidente di Plus. "Di fronte ad una pandemia come quella da Hiv, noi non siamo nelle condizioni di rifiutare nessuno strumento di prevenzione", ribadiscono i rappresentanti delle associazioni firmatarie. "La PrEP dev'essere messa a disposizione degli specialisti per le persone ad alto rischio di contagio e, quindi, inserita fra le armi a disposizione del servizio sanitario nazionale".

[Il Comunicato in versione .pdf con i loghi e i contatti delle associazioni](http://www.certidiritti.org/wp-content/uploads/2015/02/CS-PREP-def.pdf).

[Il Manifesto europeo in inglese](http://www.certidiritti.org/wp-content/uploads/2015/02/The-HIV-Prevention-Manifesto-FINAL.pdf).

Le dichiarazioni di Eatg e Aides in [inglese](http://www.certidiritti.org/wp-content/uploads/2015/02/CROI-EATG-AIDES-PR.-EN.pdf), [francese](http://www.certidiritti.org/wp-content/uploads/2015/02/CROI-AIDES-EATG-CP-FR.pdf), [spagnolo](http://www.certidiritti.org/wp-content/uploads/2015/02/CROI-EATG-AIDES-ES.pdf) e [tedesco](http://www.certidiritti.org/wp-content/uploads/2015/02/CROI-2015-EATG-AIDES-DE.pdf).

Organizzazioni promotrici della petizione internazionale: Eatg – European Aids Treatment Group: www.eatg.org Aides - www.aides.org