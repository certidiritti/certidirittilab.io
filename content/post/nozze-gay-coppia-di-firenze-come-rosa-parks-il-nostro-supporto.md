---
title: 'NOZZE GAY: COPPIA DI FIRENZE COME ROSA PARKS, IL NOSTRO SUPPORTO '
date: Fri, 20 Jun 2008 17:16:24 +0000
draft: false
tags: [Comunicati stampa]
---

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

“La coppia gay di Firenze che insieme al giurista Francesco Bilotta ha promosso un’importante iniziativa legale, richiedendo le pubblicazioni per le nozze, con l’obiettivo di vedere riconosciuti diritti di uguaglianza e di civiltà, ha il supporto di tutta l’Associazione Radicale Certi Diritti.

Non è un caso se Matteo Pegoraro e Francesco Piomboni, sono tra i co-fondatori dell’Associazione Certi Diritti ed esprimono con dignità e rigore il loro orgoglio di cittadini gay. Grazie al loro esempio civile, in queste settimane, abbiamo promosso in tutta Italia un’importante iniziativa di ‘affermazione civile’ che vedrà coinvolte decine di coppie lesbiche e gay. Grazie a Francesco e Matteo, che hanno richiesto le pubblicazioni delle nozze al Comune di Firenze, per vedersi riconosciuto il diritto all’accesso all’istituto del matrimonio civile, si è incardinata in Italia un’azione simile a quella di Rosa Parks. L'[1 dicembre](http://it.wikipedia.org/wiki/1_dicembre "1 dicembre") del [1955](http://it.wikipedia.org/wiki/1955 "1955"), a Montgomery, Rosa si rifiutò di obbedire all'ordine del conducente dell'autobus James Blake che le intimava di lasciare il posto a sedere riservato ai bianchi e di spostarsi nella parte posteriore del pullman; Rosa era stanca di essere trattata come una cittadina di seconda classe e rimase al suo posto. Per questo fu arrestata e incarcerata per condotta impropria e per aver violato le norme cittadine.

Siamo tutti Rosa Parks, come lo sono Matteo e Francesco, cittadini come tutti gli altri”.