---
title: 'Tutti gli emendamenti alla delibera sul registro delle unioni civili presentata a Milano'
date: Wed, 25 Jul 2012 12:47:12 +0000
draft: false
tags: [Diritto di Famiglia]
---

Comunicato dell’Associazione Radicale Certi Diritti e del Gruppo Radicale - Federalista Europeo

Milano, 25 luglio 2012

L’Associazione Radicale Certi Diritti e il Gruppo Radicale - Federalista europeo hanno deciso di pubblicare tutti i 75 emendamenti presentati alla Delibera consiliare sul riconoscimento delle unioni civili: [http://www.milanoradicale.it/2012/07/25/lassociazione-radicale-certi-diritti-e-il-gruppo-radicale-federalista-europeo-pubblicano-tutti-gli-emendamenti-presentati-alla-delibera-sul-registro-delle-unioni-civili/](http://www.milanoradicale.it/2012/07/25/lassociazione-radicale-certi-diritti-e-il-gruppo-radicale-federalista-europeo-pubblicano-tutti-gli-emendamenti-presentati-alla-delibera-sul-registro-delle-unioni-civili/)

Da tempo il Gruppo Radicale - Federalista Europeo ha aperto sul suo sito (www.milanoradicale.it) la sezione #openmilano dove si pubblicano in versione integrale i documenti del Comune dei quali il Gruppo entra in possesso in un’ottica di trasparenza delle istituzioni, confermata dalla scelta di pubblicare anche gli emendamenti a una delibera che ha suscitato un ampio dibattito in città.

Yuri Guaiana, segretario nazionale dell’Associazione Radicale Certi Diritti dichiara: «Ringrazio il Gruppo Radicale - Federalista Europeo per aver reso possibile questa azione di trasparenza che facciamo nostra pubblicando gli emendamenti anche sul nostro sito. In questo modo tutti i cittadini milanesi potranno valutare le intenzioni, gli orientamenti e anche la competenza dei loro rappresentanti. Basta una scorsa ai 75 (!) emendamenti per capire come si voglia non solo epurare il documento da qualsiasi riferimento alla famiglia, ma portare attacchi anche più sostanziali riducendo o cancellando l’impegno del comune a tutelare e sostenere le unioni civili. Si arriva sino a proporre che l’amministrazione possa citare in giudizio la coppia “in caso di rapido scioglimento dell’unione civile”. Si tenta poi di svalutare le convivenze per vincolo affettivo equiparandole alle altre forme di coabitazione.  Emerge anche un certo grado di spietatezza quando si chiede di eliminare il comma che garantisce il diritto all’assistenza. Si chiede anche la verifica degli attestati rilasciati ogni 6 mesi, dimenticando che quando si parla di principi, in politica, i numeri sono irrilevanti: “l'Affaire d'un seul est l'affaire de tous”. Soprassediamo ai riferimenti alla poligamia perché non spareremmo mai contro la croce rossa. In generale si nota una certa sciatteria per la quale alcuni emendamenti fanno riferimento ai commi, ma non agli articoli. Alcuni poi dimostrano di non conoscere il regolamento anagrafico 223/1989 chiedendo di inserire procedure già regolamentate dallo stesso. Altri arrivano a emendare il testo stesso del DPR 223/1989, dimostrando una totale incompetenza”.  
  
**sotto è possibile scaricare la delibera e gli emendamenti.**

  
[delibera74.pdf](http://www.certidiritti.org/wp-content/uploads/2012/07/delibera74.pdf)  
[emendamenti_N74.pdf](http://www.certidiritti.org/wp-content/uploads/2012/07/emendamenti_N74.pdf)