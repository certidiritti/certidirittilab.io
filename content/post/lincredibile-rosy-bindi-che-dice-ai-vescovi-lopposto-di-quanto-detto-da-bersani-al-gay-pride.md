---
title: 'L''INCREDIBILE ROSY BINDI CHE DICE AI VESCOVI L''OPPOSTO DI QUANTO DETTO DA BERSANI UNA SETTIMANA FA AL GAY PRIDE'
date: Sun, 17 Jun 2012 12:18:45 +0000
draft: false
tags: [Politica]
---

**Il tutto basato sul pregiudizio. Se il buon giorno di vede dal mattino...**

**Roma, 17 giugno 2012**

**Comunicato Stampa dell'Associazione Radicale Certi Diritti**

E’ davvero sorprendente quanto oggi la Presidente del Pd Rosy Bindi ha detto ai vescovi in tema di diritti civili.

E’ appena passata una settimana dal Gay Pride nazionale di Bologna, dove il Segretario del Pd ha mandato un importante messaggio chiaro sulla necessità di legiferare in tema di lotta all’omofobia e di unioni civili, ispirandosi a Obama e Hollande (“In tutto il mondo le forze progressiste, dal Presidente Usa Obama al neo-eletto Presidente francese Hollande, sono impegnate a costruire un nuovo civismo in cui ciascuna persona possa avere pari diritti e pari opportunità di vita, indipendentemente dal proprio orientamento sessuale e identità di genere") ed ecco che la Presidente del Pd, oggi, su questi temi, fa dichiarazioni del tutto opposte. Ai vescovi ha oggi detto: “ci atterremo ai contenuti della Costituzione e a una consolidata giurisprudenza che non prevede il matrimonio per coppie omosessuali” falsificando quanto la stessa Consulta con la sentenza 138/2010 ha detto, ovvero che il matrimonio tra persone dello stesso sesso è possibile ma deve essere il Parlamento a legiferare.

Non contenta la Bindi si è anche preoccupata di dire ai vescovi che nel programma del Pd esclude che “si possa prevedere l’adozione per le coppie gay, alle quale sono personalmente contraria anche sotto un profilo scientifico”. Altro falso. Quali sarebbero i profili scientifici cui la Presidente del Pd fa riferimento? Gli unici studi seri su questo argomento sono stati fatti dall'American Psychological Association, dall’American Psychiatric Association, dall’ American Academy of Pediatrics, dall’ Australian Psychological Society e dall’ Australian Medical Association, che, con altre associazioni di professionisti che operano nel campo della salute mentale, hanno evidenziato che non sussiste per i minori alcuna differenza, neppure minima, negli effetti della omogenitorialità rispetto alla genitorialità eterosessuale. La Bindi farebbe anche bene a leggersi il rapporto di Human Rights Watch e la sentenza del 24 gennaio 2008 della Corte Europea dei Diritti dell'Uomo che ha stabilito che anche gli omosessuali hanno diritto ad adottare un bambino. Alla Bindi, per scrollarsi di dosso i pregiudizi che caratterizzano queste sue uscite, basterebbe leggere gli studi e la raccolta di documentazione elaborati dallo psichiatra italiano Vittorio Lingiardi proprio su questi temi.

Ma sappiamo di chiedere troppo a chi già nel marzo 2007 diceva "non sarò mai favorevole al riconoscimento del matrimonio tra omosessuali: non si possono creare in laboratorio dei disadattati. E' meglio che un bambino cresca in Africa piuttosto che cresca tra due uomini, o due donne".

Del resto in sei mesi la ex Dc Bindi, da sempre cadetta della Cei, è riuscita a elaborare, quando era Ministro per la famiglia, quell’obbrobrio chiamato ‘Dico’ e ora, dopo un anno di lavoro della Commissione del Pd, da lei presieduta, incaricata di elaborare proposte sul tema del riconoscimento dei diritti civili alle persone gay, va facendo dichiarazioni sempre più deliranti e impregnate del peggior pregiudizio giovanardesco e/o buttiglionesco e in netto contrasto con il Segretario del Pd.

Complimenti! Evviva Hollande, Obama e Bersani, per la Bindi attendiamo. E intanto speriamo che il Pd non la segua.  
  

**sotto in allegato il documento della commissione PD sui diritti civili**

  
[PD_-\_Documento\_sui\_diritti\_civili_-_15.6.2012.doc](http://www.certidiritti.org/wp-content/uploads/2012/06/PD_-_Documento_sui_diritti_civili_-_15.6.2012.doc)