---
title: 'SOS RUSSIA: la campagna a sostegno degli attivisti per i diritti umani delle persone LGBTI in Russia continua fino alla fine di settembre'
date: Tue, 27 Aug 2013 12:57:52 +0000
draft: false
tags: [Russia]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 27 agosto 2013

All'inizio di agosto Associazioni Radicale Certi Diritti, AGEDO, Arcigay, Famiglie Arcobaleno, Equality Italia, Arcilesbica e Rete Genitori Rainbow hanno lanciato la campagna SOS Russia a sostegno delle associazioni che combattono per i diritti umani delle persone LGBTI, ma non solo, nel grande Paese eurasiatico. Alla corsa di solidarietà hanno aderito varie realtà, tra cui: il coordinamento Arcobaleno di Milano, il Gay Center di Roma, l'ARC di Cagliari, il gruppo Lieviti e i blog Il grande colibrì, La versione dell'uomo gay e Il fiore uomo solidale.

Durante questo mese la grave situazione russa ha ricevuto una grande attenzione da parte della stampa internazionale, meno da parte di quella italiana, aumentando la pressione sulle autorità russe. Un primo effetto positivo è che si sono registrate le prime vittorie giudiziarie, ancora tutt'altro che definitive, contro la legge sugli "agenti stranieri" che impone l'obbligo di registrazione su uno speciale registro o il pagamento di pesanti multe. Questo permette alle associazioni russe di guadagnare un po' di tempo e a noi di posticipare la fine della nostra corsa di solidarietà alla fine di settembre e raggiungere quindi l'obiettivo di 10.000€. Ad oggi abbiamo raccolto solo 1045.94€, occorre quindi il rinnovato impegno di tutti per raggiungere l'obiettivo quanto mai necessario visto che gli sviluppi in Russia non sono solo positivi.

Il combinato disposto della legge sugli agenti stranieri e di quella sulla "propaganda di rapporti sessuali non tradizionali" tra i minori hanno dato un implicito via libera alle aggressioni omofobe che infatti sono aumentate esponenzialmente, con tanto di associazioni dedicate alla caccia alle persone LGBTI, per nulla contrastate dalla polizia.

Obiettivo della corsa di solidarietà italiana è quindi quello di sostenere la copertura delle spese legali per le vittime di violenza o discriminazioni basate su orientamento sessuale e identità di genere e di aiutare le associazioni russe multate ai sensi della legge sugli agenti stranieri a pagare le spese legali per sfidare la legge nei fori statali, federali e internazionali.

D'altra parte, il giro di vite delle autorità russe non colpisce solo le persone LGBTI, ma gli attivisti per i diritti umani e la società russa in genere. La legge sulla "propaganda di rapporti sessuali non tradizionali" tra i minori viola la libertà d'espressione di tutti, attori, giornalisti, scrittori, registi, insegnanti, a prescindere dal loro orientamento sessuale o dalla loro identità di genere. La legge sugli agenti stranieri ha colpito varie associazioni che si occupano di diritti umani in genere e anche associazioni culturali: http://www.hrw.org/news/2013/06/25/russia-harsh-toll-foreign-agents-law. Altre leggi approvate di recente limitano gravemente la libertà d'assemblea e d'associazione, oltre ad aver introdotto il divieto di adozioni da parte degli americani e la criminalizzazione della bestemmia.

Non è accettabile che un Paese membro del Consiglio d'Europa infranga così impunemente le regole che pure ha sottoscritto. I diritti umani non conoscono frontiere, a maggior ragione nello spazio europeo, e necessitano di un impegno concreto per essere difesi. Siamo tutti chiamati in causa: la scelta di ciascuno di noi è tra l'indifferenza e un contributo concreto a chi lotta per i diritti umani di tutti.