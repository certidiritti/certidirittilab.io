---
title: 'Battaglia in tribunale per coronare il sogno delle nozze lesbo'
date: Mon, 23 Nov 2009 11:00:25 +0000
draft: false
tags: [Senza categoria]
---

[http://corrieredelveneto.corriere.it/veneto/notizie/cronaca/2009/23-novembre-2009/battaglia-tribunale-coronare-sogno-nozze-lesbo-1602048098031.shtml](http://corrieredelveneto.corriere.it/veneto/notizie/cronaca/2009/23-novembre-2009/battaglia-tribunale-coronare-sogno-nozze-lesbo-1602048098031.shtml)

### ![Coppie lesbo e gay supportate dall'associazione «Certi diritti» (Gobbi-Bergamaschi) ](http://corrieredelveneto.corriere.it/Media/foto/2009/11/23/lesbo2--190x130.jpg)

### La storia - Clara Comelli, presidente dell’associazione «Certi diritti»: «Quello che stiamo facendo in Italia è un percorso già compiuto da altri in Europa»

Battaglia in tribunale per coronare  
il sogno delle nozze lesbo
================================================================

Due ragazze di Conselve citano in giudizio il sindaco «Ci ha negato un diritto, il matrimonio è solo nostro»
------------------------------------------------------------------------------------------------------------

Coppie lesbo e gay supportate dall'associazione «Certi diritti» (Gobbi-Bergamaschi)

**PADOVA** – Sono due ragazze, hanno 24 e 28 anni, sono lesbi­che e si vogliono sposare. Il 22 gennaio 2010 il loro caso verrà discusso nel corso di un’udien­za civile al Tribunale di Padova, contro il sindaco di Conselve, in qualità di rappresentante dello Stato. Perché M.M. e V.S. (que­ste le iniziali delle due ragazze che preferiscono non farsi rico­noscere) hanno impugnato l’at­to di diniego certificato dal loro Comune di residenza, Conselve appunto, e lo hanno portato in Tribunale. Per chiedere la remis­sione dell’atto e ottenere quindi la pubblicazione del matrimo­nio. Come una coppia eteroses­suale, perché «il matrimonio ri­guarda noi due e basta, non al­tri. Deve essere riconosciuto a qualsiasi persona libera», so­stengono mentre si tengono la mano. Come per farsi forza l’una con l’altra.

E sono loro a raccontare co­me sono andate le cose. «Il 2 set­tembre siamo andate all’anagra­fe di Conselve – racconta V., che di anni ne ha 28 e lavora come consulente finanziaria a Padova -, all’incaricato abbiamo chiesto i moduli per la pubblicazione. Prima non c’è stato nessun pro­blema, ma quando abbiamo det­to che ci sposavamo noi due ci siamo sentite dire che non si po­teva fare, così abbiamo chiesto l’atto di diniego ufficiale». Det­to, fatto. Ora quell’atto protocol­lato all’anagrafe comunale col numero 16733, con richiesta iscritta nel processo verbale di pubblicazione al numero 34, verrà discusso in tribunale. Con­testata, dagli avvocati Maria Pia Rizzo e Francesco Bilotta![](chrome://skype_ff_toolbar_win/content/icons/icon_on.png), sarà soprattutto la frase con cui si spiega il rifiuto. «Il nostro ordi­namento non prevede matrimo­nio tra persone dello stesso ses­so perché contrario all’ordine pubblico». Una storia la loro che va avanti da circa tre anni, da quando si sono conosciute una sera all’interno di un locale citta­dino per omosessuali. Dopo po­co più di un anno sono andate a vivere assieme, a Conselve. In una casa per loro stessa ammis­sione «grande, ma che condivi­diamo con due cani».

Quella tra M., 24 anni tecnico informatico, e V. è una storia che a sentirla raccontare ha tut­te le pieghe felici e i lati oscuri che legano ogni coppia. «Io – racconta M. – mi sono scoperta bisessuale fin da subito, ho avu­to una storia importante con un ragazzo, ma adesso è tutto fini­to. Ho trovato la mia compagna, sono serena e felice con lei». Ma la domanda è d’obbligo: la fami­glia come ha preso questa deci­sione? «Bene – continua, men­tre tiene stretta la mano della sua compagna - . I miei genitori hanno accettato, nello stesso pe­riodo poi ha scoperto le carte mia sorella, anche lei omosessuale». Diver­sa la vicenda umana di V., figlia di una ra­gazza madre che ha fatto di tutto per portare la sua bam­bina sulla via per lei giusta. «Le ho detto del mio orienta­mento sessuale quando avevo 15 anni, lei si è ribellata, voleva portarmi dallo psicologo, mi di­ceva che ero malata – riannoda i fili della memo­ria con un po’ d’emozione ­. Adesso però sono cresciuta, lei ha avuto una vita dura, voleva che io fossi felice».

E adesso? «Ha capito che io ho una vita fe­lice. Pensi che ci va a fare la spe­sa e cucina per noi», sorride. Discriminazione? Non tantis­sima, peccato solo per quella volta al Cineplex di Due Carrare quando M. e V. si sono viste ri­fiutare lo «sconto coppia» per­ché lesbiche. Ma la loro non è la sola storia. Il passo che hanno già fatto (di chiedere le pubblica­zioni) è il prossimo step per una coppia lesbo vicentina. S., 44 anni barista, e L., 33 anni agronoma. «Stiamo assieme da tre anni e mezzo – ricorda S. ­. Ho compreso a pieno la mia ses­sualità aiutata da uno psicolo­go, prima mi sentivo insicura, poi verso i 30 anni la svolta. Ora sto bene e non mi sembra giu­sto ci venga rifiutato il matrimo­nio. Anche con me la famiglia è stata una grande famiglia. Mio papà mi ha addirittura detto “era ora che lo dicessi”».

Con L. si è conosciuta in un sito per omosessuali, «Elle per Elle». Ora vivono assieme, in un rap­porto equilibrato anche con le ri­spettive famiglie. «Ci tengo a di­re che internet non è stata una maschera – afferma L. – ma solo un veicolo per conoscersi». Ma a fare da apripista è stata una coppia di gay, resi­denti a Venezia. L’anno scorso G. 48 anni e S. 43 anni, entrambi artisti, hanno chiesto di sposar­si. Rifiuto del Comune, udienza civile e la decisio­ne del Tribunale lagunare di rimettere tutto nelle ma­ni della Corte Costituziona­le per una sentenza che po­trà arrivare tra un anno, cir­ca. «E’ già un successo per noi – spiega Clara Comelli presidente dell’Associazio­ne radicale Certi Diritti ­. Quello che stiamo facendo ora in Italia è il percorso già fatto da altri in Europa».

Nicola Munaro  
**23 novembre 2009**