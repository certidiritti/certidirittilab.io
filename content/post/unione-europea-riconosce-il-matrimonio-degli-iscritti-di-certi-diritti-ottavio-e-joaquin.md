---
title: 'Unione Europea riconosce il matrimonio degli iscritti di Certi Diritti Ottavio e Joaquin'
date: Wed, 23 Nov 2011 15:40:01 +0000
draft: false
tags: [Matrimonio egualitario]
---

L'associazione radicale Certi Diritti si appella al governo Monti affinchè cessino le discriminazioni contro le persone lgbt in italia.

Bruxelles – Roma, 23 novembre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

L'amministrazione dell'Unione Europea ha riconosciuto il matrimonio di Ottavio Marzocchi, italiano, e Joaquin Nogueroles Garcia, spagnolo, coppia gay,  entrambi membri dell’Associazione Radicale Certi Diritti, celebrato in Spagna lo scorso 20 agosto. Ottavio e Joaquin, in qualità rispettivamente di funzionari del Parlamento europeo e del Consiglio Europeo, due tra i  principali organismi istituzionali dell’Unione, avevano depositato nelle settimane scorse i documenti necessari  (l'atto di matrimonio celebrato in Spagna) per il rinnovo delle carte di identità speciali per i funzionari dell'UE e per aggiornare i loro dossier dell'amministrazione.

 L'amministrazione dell’ Unione Europea ha risposto nei giorni scorsi provvedendo a modificare i dati contenuti negli archivi elettronici ed emettere nuove carte di identità. La carta di identità speciale di Ottavio, che reca menzione del fatto che "...coniuge del Sign. Nogueroles Garcia J.", é allegata.

 Ottavio e Joaquin potranno utilizzare questo documento in occasione dei ricorsi che depositeranno per ottenere il riconoscimento del loro matrimonio in Italia. L’Associazione Radicale Certi Diritti chiede al Governo Monti, ed in particolare alle Ministre dell'Interno e della Giustizia, di ritirare la circolare Amato che vieta il riconoscimento in Italia dei matrimoni tra persone dello stesso sesso celebrati all'estero adducendo ragioni  di "ordine pubblico" affinchè operino per assicurare la fine delle discriminazioni contro le persone LGBT in Italia, riconoscendo tali matrimioni ed unioni civili.