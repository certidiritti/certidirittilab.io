---
title: 'A Milano sit-in contro la legge anti-gay in Uganda'
date: Tue, 04 Dec 2012 17:28:12 +0000
draft: false
tags: [Africa]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Milano, 4 dicembre 2012

In occasione del convegno ISPI “Stabilità e instabilità in Africa: il ruolo dell’Uganda” che vedrà la partecipazione del Primo Ministro dell’Uganda, Patrick Amama Mbabazi, e del Ministro degli Esteri Sam Kutesa, l'Associazione radicale Certi Diritti e l'Associazione Enzo Tortora - Radicali Milano hanno organizzato un sit-in contro la legge anti-gay che la presidente del Parlamento Ugandese, Rebecca Kadaga, ha promesso di fare approvare al Parlamento entro metà dicembre. Il sit-in si terrà oggi, martedì 4 dicembre, a partire dalle 17.30 in Largo Bortolo Belotti (all'angolo tra via Clerici e via San Dalmazio), nei pressi di Palazzo Clerci, sede dell'ISPI.

Oggi le persone LGBTI rischiano già 14 anni di carcere se scoperte in Uganda, ma, ciò che Rebecca Kadaga ha definito un "regalo di Natale" ai suoi sostenitori, vuole introdurre pene più severe tra cui l'ergastolo e, persino, la pena di morte per chi risultasse colpevole di "omosessualità aggravata" – quando cioè uno dei partecipanti all'atto sessuale sia minore, sieropositivo, portatore di handicap o "criminale seriale". La PDL vieta anche la "promozione" dei diritti delle persone LGBTI e prevedeva la punizione di tutti coloro che "finanziano o sponsorizzano l’omosessualità" o "incoraggiano l'omosessualità".

Dopo l'omicidio di David Kato Kisule, un attivista iscritto all'Associazione Radicale Certi Diritti, avvenuto due anni fa, diventa anche compito nostro quello di bloccare l'approvazione definitiva di questa legge!

Al sit-in hanno aderito: CIG Arcigay Milano - Associazione lesbica e gay, Sportello Trans ALA Milano Onlus, Guado, Collettivo lgbit Tabù, Arcilesbica Zami, Arcobaleni Inmarcia, Circolo di cultura omosessuale "Harvey Milk" e Gruppo Soggettività Lesbica.