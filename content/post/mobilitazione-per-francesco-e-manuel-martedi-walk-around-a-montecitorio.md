---
title: 'MOBILITAZIONE PER FRANCESCO E MANUEL: MARTEDI'' WALK-AROUND A MONTECITORIO'
date: Mon, 11 Jan 2010 15:17:58 +0000
draft: false
tags: [Comunicati stampa]
---

Martedì 12 gennaio, alle ore 21, anche l'Associazione Radicale Certi Diritti parteciperà alla mobilitazione in Piazza Montecitorio alle ore 21 autoconvocata da singole/i contro il grave silenzio dei media riguardo la loro iniziativa nonviolenta.

Certi Diritti Roma ha deciso di fare la sua riunione settimanale in Piazza Montecitorio, a partire dalle ore 20, per esprimere la sua vicinanza contro il grave silenzio sulla vicenda dello sciopero della fame di Francesco e Manuel giunto all'ottavo giorno. Alle ore 21 inizieremo a girare intorno a Montecitorio con cartelli che chiedono diritti civili e umani per le coppie gay.

Francesco e Manuel si battono per i diritti di tutti. Tutti dobbiamo star loro vicini.