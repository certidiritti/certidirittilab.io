---
title: 'Giornata mondiale contro l''omofobia: Certi Diritti a Torino'
date: Tue, 17 May 2011 21:08:14 +0000
draft: false
tags: [Politica]
---

**![](http://www.progeturepublica.net/wp-content/uploads/2011/05/no-all-omofobia.jpeg)Giornata mondiale contro l’omofobia. Una proposta concreta a Torino per lavorare con le forze di polizia. Iniziativa del gruppo torinese dell’Associazione radicale Certi Diritti.**

Il 17 maggio di ogni anno si celebra nel mondo la Giornata contro l’omofobia e la transfobia ed a Torino tutte le associazioni sono mobilitate per la grande manifestazione del Torino Pride 2011 del 21 maggio prossimo (oltre che per il concerto del 19 maggio al Colosseso).

Ma oltre alle manifestazioni crediamo che si debbano incardinare iniziative concrete e stabili, che provino a colpire alle radici omofobia e transfobia. Ecco perché il gruppo torinese dell’Associazione radicale Certi Diritti ha scritto al Questore di Torino ed al Comandante dei Carabinieri chiedendo di istituire una sezione torinese dell’OSCAD, Osservatorio per la sicurezza contro gli atti discriminatori, istituito presso il Dipartimento della Pubblica Sicurezza del Ministero degli Interni, di cui fanno parte rappresentanti di tutte le forze di polizia attive nel Paese e, in forma consultiva, molte delle associazioni  italiane attive contro ogni forma di discriminazione, comprese quelle delle persone omosessuali, lesbiche e transessuali. Di questo organismo devono far parte i rappresentanti di tutte le associazioni che lottano contro le discriminazioni e che sono più soggette ai reati d’odio (immigrati, rom, ebrei, persone con disabilità, donne) non solo per offrire alle forze di polizia occasioni di formazione e confronto su questi temi, ma anche per operare in modo efficace nella prevenzione di questi reati, così come di tutti i comportamenti potenzialmente e concretamente lesivi dell’identità e della vita delle persone interessate.