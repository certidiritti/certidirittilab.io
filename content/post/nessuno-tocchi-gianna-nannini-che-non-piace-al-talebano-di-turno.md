---
title: 'NESSUNO TOCCHI GIANNA NANNINI, CHE NON PIACE AL TALEBANO DI TURNO'
date: Tue, 24 Aug 2010 08:39:02 +0000
draft: false
tags: [certi diritti, Comunicati stampa, discriminazione, gianna nannini, gravidanza, Sergio Rovasio]
---

**NESSUNO TOCCHI GIANNA NANNINI, COLPEVOLE DI DIVENTARE MAMMA SENZA IL NULLA OSTA DEL TALEBANO DI TURNO.**

_**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**_

“Lo pseudo servizio pubblico informativo della Rai-Tv ha fatto ieri sera, in uno dei suoi Tg di punta,  un ignobile linciaggio nei confronti della cantante Gianna Nannini, ‘colpevole’ di essere in gravidanza, al 5° mese, pur avendo superato i 50 anni, senza che si sappia chi è il padre!

La notizia, anticipata da un settimanale, con fotografia della cantante in un negozio londinese pre-maman, è stata oggetto di un commento del professorone di turno che si è scagliato contro la gravidanza per le donne che superano i 50 anni, sottolineando che ciò che può aver determinato la gravidanza è ‘eticamente un problema’ e che ‘in Italia queste tecniche sono illegali’. Insomma, la cantante Gianna Nannini, una delle pochissime artiste italiane che danno prestigio all’estero, è stata così violentemente dipinta, criminalizzata, offesa, denigrata.

Noi non sappiamo perché Gianna Nannini è in gravidanza, e non ci interessa saperlo, così come non ci interessa sapere se è eterosessuale, bisessuale o lesbica, questione che invece sembra tanto appassionare quell’Italia dalla doppia morale, dalla tripla ipocrisia, dalla quadrupla o quintupla falsità che caratterizza l’operato dei fondamentalisti talebani di turno.

Nessuno tocchi Gianna Nannini, grande artista, futura grande mamma. Le auguriamo ogni bene e anche un sincero ‘tanti auguri figli femmine o maschi’ che siano”.