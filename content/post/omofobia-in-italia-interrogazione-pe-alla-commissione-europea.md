---
title: 'OMOFOBIA IN ITALIA: INTERROGAZIONE PE ALLA COMMISSIONE EUROPEA'
date: Tue, 21 Jul 2009 10:41:38 +0000
draft: false
tags: [Comunicati stampa]
---

**INTERROGAZIONE AL PARLAMENTO EUROPEO SULLE DISCRIMINAZIONI OMOFOBICHE IN ITALIA. i casi delle violenze a Maria Luisa Mazzarella, della poliziotta Luana Zanaga, del ragazzo tredicenne di Pavia, dei controlli allo stand di Certi Diritti a Catania E SULL’OPERATO DEL GOVERNO ITALIANO.**

Su iniziativa di Certi Diritti, Jeanine Hennis-Plasschaert e Sophie In 't Veld, Eurodeputate liberali olandesi , hanno depositato una interrogazione al Parlamento europeo rivolta alla Commissione Europea sulle discriminazioni contro le persone LGBT in Italia compiute da parte delle istituzioni, le autorità e i privati. Di seguito il testo dell'interrogazione:

Interrogazione sulle "Discriminazioni contro le persone LGBT in Italia compiute da parte delle istituzioni, le autorità e i privati" depositata da Jeanine Hennis-Plasschaert e Sophie In 't Veld.

Il 22 giugno, Maria Luisa Mazzarella é stata attaccata da tre persone a Napoli dopo avere difeso alcuni amici da un attacco omofobico; é stata picchiata e ha dovuto subire un'operazione all'occhio.

Il 29 giugno, i Carabinieri - chiamati secondo quanto riportato da una persona che denunciava "comportamenti osceni" - hanno controllato uno stand dell'organizzazione LGBT Certi Diritti a Catania, presso il quale si raccoglievano firme a favore del matrimonio delle persone dello stesso sesso.

Luana Zanaga, un'agente di polizia che lavora a Padova, ha subito persecuzioni e discriminazioni dopo avere dichiarato di essere lesbica: é stata messa sotto inchiesta disciplinaria, sottoposta ad un controllo medico, minacciata dai colleghi ed ha infine ricevuto una lettera del Questore della città che la informava che una procedura per la sua espulsione dalla polizia era stata avviata.

I Ministri italiani per l'eguaglianza e per l'educazione hanno recentemente firmato un accordo per il lancio d'iniziative nell'ambito del programma della "Settimana contro la violenza" previsto per il 12-18 ottobre 2009, che include attività di prevenzione e lotta alla violenza e alla discriminazione quali "l'intolleranza religiosa, razzista e di genere" nelle scuole. L'accordo non menziona l'età, la disabilità e l'orientamento sessuale, sebbene queste siano elencate dell'articolo 13 TCE e nelle direttive europee contro la discriminazione.

Questo accade mentre i media riportano del caso di un ragazzo di 13 anni di Pavia che dopo essere stato perseguitato ed attaccato da alcuni compagni, ha tentato due volte il suicidio e poi é stato costretto a cambiare scuola, anche a causa della sottovalutazione della situazione da parte degli insegnanti e del direttore.

E' la Commsisione al corrente dell'ondata di omofobia operata dalle istituzioni, dalle autorità e da privati contro le persone LGBT in Italia? Cosa intende fare per assicurare che gli Stati membri implementino i programmi contro la discriminazione e la violenza, incluso nelle scuole e nella polizia, al fine di combattere tutte le ragioni di discriminazione - e non solo una selezione di esse? Non ritiene la Commissione che sia necessario condizionare lo stanziamento di fondi europei contro la discriminazione al fatto che tutte le ragioni elencate all'articolo 13 TCE e nelle direttive UE siano affrontate in modo appropriato dagli Stati membri?