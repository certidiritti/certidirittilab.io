---
title: 'Punire ragazzi per un rapporto sessuale non ha senso alcuno. Nelle scuole informazione e preservativi'
date: Sun, 19 Feb 2012 22:38:46 +0000
draft: false
tags: [Salute sessuale]
---

Sesso nel bagno della scuola: la vicenda dei due ragazzi di Bassano Del Grappa sorpresi in un bagno e sospesi dalle lezioni è il frutto di una politica reazionaria, sessuofoba e certamente non educativa. Lettera di Certi Diritti al ministro dell'istruzione.

Roma, 19 febbraio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

La vicenda dei due ragazzi di 15 anni sospesi dalle lezioni per aver avuto un rapporto sessuale nei bagni dell’Istituto Luigi Einaudi di Bassano del Grappa è sintomatica di una cultura sessuofobica e reazionaria che di educativo ha ben poco. Piuttosto occorrerebbe garantire agli studenti una adeguata informazione sessuale anche sulle malattie sessualmente trasmissibili. L’Italia è l’unico paese in Europa che non prevede spazi informativi sul tema della sessualità. Nelle scuole occorre distribuire gratuitamente preservativi e responsabilizzare i ragazzi anche sul tema dei rappporti sessuali. Che tipo di aiuto avranno ora i ragazzi sospesi dalle lezioni?  Ancora più incredibile, fastidioso e del tutto ingiustificato, sul piano educativo, è sospendere la ragazza tre giorni di più del ragazzo perché era entrata nel bagno degli uomini.  

L’Associazione Radicale Certi Diritti ha scritto oggi una lettera al Professor Francesco Profumo, Ministro dell’Istruzione, Università e Ricerca chiedendo di avviare da subito campagne informative nei diversi gradi scolastici che preveda anche  la distribuzione gratuita di preservativi per responsabilizzare i giovani ad una sessualità consapevole e responsabile così come avviene in molti paesi europei. Nella lettera si sottolinea che occorre fare prevenzione sugli atti  di violenza e quelli di bullismo contro le diversità anche sui diversi orientamenti sessuali dei giovani piuttosto che colpire così inutilmente due ragazzi per un rapporto sessuale.