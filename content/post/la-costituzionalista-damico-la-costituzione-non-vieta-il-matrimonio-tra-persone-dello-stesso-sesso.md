---
title: 'La costituzionalista D''Amico: la Costituzione non vieta il matrimonio tra persone dello stesso sesso'
date: Sat, 08 Sep 2012 08:59:22 +0000
draft: false
tags: [Matrimonio egualitario]
---

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Roma, 8 settembre 2012

Di fronte alla vera e propria campagna di disinformazione che, in questi giorni, alcuni esponenti del PD, e non solo, stanno sviluppando sul tema dell’incostituzionalità del pieno accesso per le persone omosessuali all’istituto del matrimonio civile, l’Associazione Radicale Certi Diritti ha chiesto alla prof.ssa Marilisa D’Amico, ordinario di Diritto Costituzionale presso l’Università degli Studi di Milano, membro del collegio di difesa delle coppie omosessuali alla Corte Costituzionale nel 2010 e consigliere comunale del PD a Milano, cosa ne pensa. Ecco il testo che ci ha inviato:

"Leggo sui giornali le affermazioni secondo cui i matrimoni fra persone dello stesso sesso sarebbero “contrari alla Costituzione”. Io, anche collaborando con l’ associazione radicale Certi Diritti, ho sempre sostenuto il contrario, ed il rispetto dovuto alla Carta impone chiarezza.

Nel PD e con tutti coloro che sono impegnati per i diritti civili dobbiamo discutere serenamente, sapendo che sono pienamente legittime opinioni differenti: personalmente, e con me molti autorevoli studiosi di diritto costituzionale, sono convinta che sia discriminatorio negare il matrimonio alle coppie omosessuali,  è un dibattito politico  che non può chiudersi con la proclamazione di una verità obbligatoria per tutti.  
Ma gli argomenti debbono essere di merito, lasciamo ad altri la demagogia e le frasi ad effetto. Bene, nella nostra Costituzione non esiste alcun divieto al pieno riconoscimento del diritto degli omosessuali al matrimonio.  Secondo l’ opinione della Corte Costituzionale - opinione autorevolissima, ma non sacra: è già capitato che la Consulta abbia mutato orientamento…. -  non sussiste l’ obbligo di quel riconoscimento, ma il legislatore sul punto è libero ed una legge che riconoscesse a tutti il diritto al matrimonio sarebbe perfettamente costituzionale, visto che non può dirsi che tutto quanto non è obbligatorio è ipso facto vietato.

Ripeto, discutiamo serenamente, ma rispettando i grandi spazi di libertà che la nostra Costituzione ci offre: è proprio difendendo davanti alla Corte costituzionale il diritto per i gay al matrimonio, che mi sono resa conto di quanto questa sia una battaglia di eguaglianza."

Marilisa D’Amico  
Ordinario di Diritto Costituzionale  
nell’ Università Statale di Milano  
Consigliere Comunale PD a Milano