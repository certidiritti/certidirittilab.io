---
title: 'Newsletter 30 Luglio'
date: Wed, 31 Aug 2011 10:26:16 +0000
draft: false
tags: [Politica]
---

**![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)**

Per la seconda volta in meno di due anni il Parlamento ha affossato la legge contro l’omofobia e la transfobia. La proposta di legge Concia-Soro è stata considerata incostituzionale grazie all’accoglimento delle pregiudiziali di costituzionalità presentate da Pdl, Lega, Udc e l’Italia ha perso di nuovo l’occasione di dare un segnale di condanna per chi aggredisce una persona perché gay o trans.   
Una provocazione è stata fatta  da Aurelio Mancuso: se questa legge non fosse stata approvata, lui avrebbe fatto i nomi dei politici omofobi che fanno sesso con gay e trans.

Cosa ne pensate? Condividete la pratica dell’outing?

Anche questa newsletter, dopo ben 31 numeri, va in vacanza e riprenderà a settembre. Ma le iniziative di Certi Diritti proseguono e anche voi potrete comunque iscrivervi e sostenere l’associazione.

Commenti, critiche e suggerimenti sono sempre ben accetti e saremo felici di leggerli.

Buona lettura e buone vacanze.

ASSOCIAZIONE
============

**PARLAMENTO CIECO, SORDO E MUTO SULL’OMOFOBIA ****.   
[Leggi >](notizie/comunicati-stampa/item/1203.html)**

**Dal 24 luglio su [www.tvradicale.it](http://www.tvradicale.it/) è iniziata una trasmissione di approfondimento sui diritti lgbte, sulla lotta alla sessuofobia e sulle tematiche dell’associazione radicale Certi Diritti.  
Il primo ospite è stato  il nostro tesoriere Giacomo Cellottini. Il programma verrà trasmesso ogni domenica dalle 19,00 alle 19,30.**

**24 luglio: Spazio Certi Diritti, a cura di Riccardo Cristiano con Giacomo Cellottini.  
[Guarda il video >](http://www.certidiritti.org/2011/07/25/240711-spazio-certi-diritti-a-cura-di-riccardo-cristiano-con-giacomo-cellottini/)**

**Ilga diventa membro dell’Ecosoc. Una voce in più sui diritti civili e umani all’Onu.   
[Leggi >](http://www.certidiritti.org/2011/07/27/ilga-diventa-membro-dellecosoc-allonu/)**

**Seminario dell'Associazione radicale Certi Diritti.   
[Leggi >](http://www.certidiritti.org/2011/07/23/seminario-dellassociazione-radicale-certi-diritti/)**

**Ricordato a Tunisi ai lavori del cg del PRNTT David Kato Kisule.  
[Leggi >](notizie/comunicati-stampa/item/1200.html)**

**Pesaro. “Giovani Coppie” per l’edilizia popolare. Radicali interrogano il Comune.   
[Leggi >](http://www.radicali.it/20110722/giovani-coppie-l-edilizia-popolare-radicali-interrogano-comune)**

RASSEGNA STAMPA
===============

**Matteo e Matteo, la prima coppia italiana sposata a New York.  
[Auguri >](http://www.gay.it/channel/attualita/32196/La-prima-coppia-italiana-si-e-sposata-a-New-York.html)**

**Colombia: la Corte costituzionale chiede legge su nozze gay.   
[Leggi >](http://www.gay.it/channel/attualita/32199/Colombia-la-Corte-Costituzionale-chiede-legge-su-nozze-gay.html)**

**Omofobia, affossata la legge. Passano pregiudiziali Udc-Pdl-Lega.   
[Leggi >](http://www.unita.it/italia/omofobia-l-aula-al-voto-br-pregiudiziali-di-costituzionalita-1.317429)**

**«Presto una lista dei politici gay omofobi. Molti di Lega e Pdl»   
[Leggi >](http://www.unita.it/italia/presto-una-lista-dei-politici-gay-br-omofobi-molti-di-lega-e-pdl-1.316550)**

  
**Mancuso e l’outing dei politici omofobi ma gay. Il ruggito del leone o il ruggito del bidone? Il Garante dice sì?   
[Leggi >](http://www.notiziegay.com/?p=1297)**

**Bologna. ****Non gli notificano il diritto d'asilo gay marocchino a rischio espulsione****.   
[Leggi >](http://www.repubblica.it/solidarieta/profughi/2011/07/22/news/non_gli_notificano_il_diritto_d_asilo_gay_marocchino_a_rischio_espulsione-19479663/)**

**Marocchino gay, il tribunale sospende l'ordine di espulsione.  
[Leggi >](http://www.repubblica.it/solidarieta/profughi/2011/07/25/news/marocchino_e_omosessuale_sospeso_l_ordine_di_espulsione-19602585/)**

**Milano****: trans presa a sassate in via Novara e rapinata****.  
[Leggi >](http://www.libero-news.it/news/788323/Milano-trans-presa-a-sassate-in-via-Novara-e-rapinata.html)**

**Coppia gay in fuga dalla Palestina, l'Italia riconosce lo status di rifugiati.   
[Leggi >](http://corrieredelveneto.corriere.it/veneto/notizie/cronaca/2011/26-luglio-2011/coppia-gay-fuga-palestina-italia-riconosce-status-rifugiati-1901177001286.shtml)**

**Brescia. Test a luci rosse, il Garante dice no **I test d'assunzione con domande sulle abitudini sessuali sono illeciti**.   
[Leggi >](http://www.quibrescia.it/index.php?/content/view/27491/218/)**

**Niente Pari Opportunità alla cognata di Casini.   
[Leggi >](http://www.gay.it/channel/attualita/32197/Emilia-Romagna-niente-UDC-alle-Pari-Opportunita.html)**

**Chiese la garrota per i gay. L’ex assessore Prosperini ancora arrestato per tangenti.   
[Leggi >](http://gaynews24.com/2011/07/21/chiesa-la-garrota-per-i-gay-lex-assessore-prosperini-ancora-arrestato-per-tangenti/)**

**«Ero gay, ora ho una moglie Il professor Veronesi non sa di che sta parlando...». Parla Luca di Tolve.   
[Leggi >](http://www.ilgiornale.it/interni/ero_gay_ora_ho_moglie_il_professor_veronesi_non_sa_che_sta_parlando/24-07-2011/articolo-id=536542-page=0-comments=1)**

**Nasce in Puglia il primo portale del turismo amico di tutte le differenze.  
[Leggi >](http://www.adnkronos.com/IGN/Regioni/Puglia/Nasce-in-Puglia-il-primo-portale-del-turismo-amico-di-tutte-le-differenze_312262840870.html)**

**Confessione Reporter e i _gay_uccisi in Iran.   
[Leggi >](http://www.libero-news.it/blog.jsp?id=1638)**

**Antisemitismo sul web. “Fuori la feccia ebraica da atenei e procure”, liste nere e appelli al boicottaggio.   
[Leggi >](http://www.notiziegay.com/?p=1505)**

**Svizzera, elezioni e movimento Lgbt. Gay e lesbiche si mobilitano con proposte ai candidati.   
[Leggi >](http://www.notiziegay.com/?p=1416)**

**Avviso alla Serbia. La lotta alle discriminazioni sessuali, tappa obbligata verso l’Europa.   
[Leggi >](http://www.notiziegay.com/?p=1377)**

**Spagna. Le lesbiche hanno la loro bandiera.  
[Leggi >](http://www.gayprider.com/spagna-lesbiche-bandiera/)**

**Pubblicata guida delle università gay friendly del Regno Unito.   
[Leggi >](http://www.queerblog.it/post/11648/pubblicata-guida-delle-universita-gay-friendly-del-regno-unito)**

**Ghana: ministro ordina la persecuzione degli omosessuali.  
[Leggi >](http://www.queerblog.it/post/11645/ghana-ministro-ordina-la-persecuzione-degli-omosessuali)**

**Nepal: la “Mecca” dei matrimoni gay e del turismo omosessuale.  
[Leggi >](http://www.dirittodicritica.com/2011/07/20/nepal-mecca-matrimoni-gay-76432/)**

**Diritti. Il Pentagono certifica la fine ‘Don’t ask don’t tell’ fra militari.  
[Leggi >](http://gaynews24.com/2011/07/22/diritti-il-pentagono-certifica-la-fine-dont-ask-dont-tell-fra-militari/)**

**Diritti Lgbt. Ancora Barack Obama: sostiene abolizione legge contro matrimonio gay.  
[Leggi >](http://www.notiziegay.com/?p=1254)**

**Kitty e Chreyle, le prime spose gay di New York. **Da oggi nello stato americano sono possibili i matrimoni gay. Attesi oltre 800 «Sì». La prima coppia formata da due nonne**.[Leggi >](http://www.vanityfair.it/news/mondo/2011/07/24/primo-matrimonio-gay-new-york#?refresh=ce)**

**Matrimoni Gay ieri a New York: inizia una nuova era tra proteste e gioia.  
[Leggi >](http://www.corriereinformazione.it/2011072512167/attualita/attualita/matrimoni-gay-ieri-a-new-york-inizia-una-nuova-era-tra-proteste-e-gioia.html)**

**Lobby conservatrice di New York chiede annullamento matrimoni gay.  
[Leggi >](http://gaynews24.com/2011/07/25/lobby-conservatrice-di-new-york-chiede-annullamento-matrimoni/)**

**New York: eletto il primo giudice federale gay.  
[Leggi >](http://qn.quotidiano.net/esteri/2011/07/19/547116-york_eletto_primo_giudice_federale.shtml)**

**Gay e deputato negli Usa cade il tabù.   
[Leggi >](http://www.lastampa.it/_web/cmstp/tmplrubriche/finestrasullamerica/grubrica.asp?ID_blog=43&ID_articolo=2123&ID_sezione=58)**

**Born this way e glitter contro la clinica che vuole guarire i gay   
[Guarda il video >](http://www.queerblog.it/post/11654/born-this-way-e-glitter-contro-la-clinica-che-vuole-guarire-i-gay-video)**

**La chiesa luterana canadese approva i matrimoni gay.   
[Leggi >](http://www.queerblog.it/categoria/religioni)**

**Costa Rica: coppia lesbica si unirà in matrimonio in Messico per rivendicare il diritto all'uguaglianza.  
[Leggi >](http://www.queerblog.it/post/11679/costa-rica-coppia-lesbica-si-unira-in-matrimonio-in-messico-per-rivendicare-il-diritto-alluguaglianza)**

STUDI, RICERCHE E MATERIALI INFORMATIVI
=======================================

**Non si smette mai di essere razzisti.   
[Leggi >](http://rassegnastampa.unipi.it/rassegna/archivio/2011/07/20SIJ3183.PDF)**

**OUTING su Wikipedia.  
[Leggi >](http://it.wikipedia.org/wiki/Outing)**

**Gli anti-casta che tanto piacciono alla vera Casta.   
[Leggi >](http://maovalpiana.wordpress.com/2011/07/21/gli-anti-casta-che-tanto-piacciono-alla-vera-casta/)**

**Tendenze. Video hard venduti sul web: il sesso a pagamento dei ragazzini italiani.   
[Leggi >](http://www.notiziegay.com/?p=1447)**

**I nostri figli di nessuno.  
[Leggi >](http://notizie.radicali.it/articolo/2011-07-25/editoriale/i-nostri-figli-di-nessuno)**

**Gay e media. In prima pagina dopo gli immigrati e per casi di omofobia.   
[Leggi >](http://www.notiziegay.com/?p=1285)**

**Sesso a pagamento? Uomini più pericolosi.  
[Leggi >](http://www.corriere.it/scienze_e_tecnologie/11_luglio_21/ricerca-sesso-pagamento-dipasqua_96f33b8e-b39d-11e0-a9a1-2447d845620b.shtml)**

**Brasile, ogni 36 ore viene ucciso un omosessuale. Uno statuto per i diritti lgbt.  
[Leggi >](http://musibrasil.net/2011/07/uno-statuto-per-i-diritti-lgbt/)**

**Applicata poco e male la Convenzione ONU sulla discriminazione delle donne.[Leggi >](http://superando.eosservice.com/content/view/7724/112/)**

**Hiv: cure personalizzate e prevenzione globale.   
[Leggi >](http://www.corriere.it/salute/11_luglio_25/dossier-aids-terapie-prevenzione-bazzi_37f4da24-b6a0-11e0-b3db-8b396944e2a2.shtml)**

**Scienze. In un gene il segreto per cambiare sesso.   
[Leggi >](http://affaritaliani.libero.it/cronache/scienza_in_gene_cambiare_sesso210711.html?refresh_ce)**

**La soia fa diventare gay!   
[Leggi >](http://www.antikitera.net/news.asp?ID=10701)**

**Vale per etero come per i gay. Hai problemi sessuali? E’ più probabile che tu tradisca il tuo partner. **Studio dell’Università dell’Indiana**.   
[Leggi >](http://www.notiziegay.com/?p=1237)**

**Stato di diritto. **Da domenica la grande mela sarà ancora più Grande. Il 24 luglio, infatti, è il primo giorno di applicazione della recente legge che autorizza nello Stato di New York la celebrazione di matrimoni anche tra persone dello stesso sesso. **  
[Leggi >](http://www.nessundio.net/blog/2011/07/23/5077/)**

LIBRI E CULTURA
===============

**Checco Zalone: "Non ammetto l'idea che una coppia gay adotti un bambino"   
[Leggi >](http://www.queerblog.it/post/11659/checco-zalone-non-ammetto-lidea-che-una-coppia-gay-adotti-un-bambino)**

**Vita da trans a via Gradoli. Arriva la sitcom sul web.   
[Leggi >](http://roma.corriere.it/roma/notizie/cronaca/11_luglio_23/sitcom-web-trans-gradoli-sassi-1901153316762.shtml)**

**“Dolce pace”. A Treviso in mostra la vita quotidiana delle persone Lgbt.   
[Leggi >](http://www.notiziegay.com/?p=1618)**

**E' perugina 'Miss lesbica' "Sogno di sposarmi in Italia"  
[Leggi >](http://www.lanazione.it/umbria/cronaca/2011/07/21/548306-perugina_miss_lesbica.shtml)**

**Sexy fingers: campagna francese per la lotta all'AIDS.   
[Leggi e guarda il video >](http://www.queerblog.it/post/11677/sexy-fingers-campagna-francese-per-la-lotta-allaids)**

**Jane Addams, il premio Nobel che amava le donne.  
[Leggi >](http://www.queerblog.it/post/11647/jane-addams-il-premio-nobel-che-amava-le-donne)**

**Strisce arcobaleno. Nasce Renbooks: solo fumetti a tema gay.   
[Leggi >](http://corrieredibologna.corriere.it/bologna/notizie/cultura/2011/20-luglio-2011/strisce-arcobaleno-1901131525483.shtml)**

**Tristan Garcia, _[La parte migliore degli uomini](http://www.internazionale.it/news/libri/2011/07/16/i-libri-della-settimana-44/)_, Guanda, €18,00**

[www.certidiritti.it](http://www.certidiritti.it/)
==================================================