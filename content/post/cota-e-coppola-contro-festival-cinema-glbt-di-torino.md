---
title: 'Cota e Coppola contro Festival Cinema Glbt di Torino'
date: Thu, 31 Mar 2011 14:30:21 +0000
draft: false
tags: [Movimento LGBTI]
---

Ottima occasione per far perdere le elezioni al candidato di centro-destra. I presdienti dello Stabile, del Regio e della Fiera del Libro non hanno nulla da dire?  
  
Torino, 31 marzo 2011  
  
Dichiarazione di Sergio Rovasio, Segretario nazionale CERTI DIRITTI  
La pietosa tesi dell'Assessore Regionale del Piemonte Coppola, candidato del centro-destra a Sindaco di Torino che dice che "non ha senso che la Regione metta il suo marchio su un festival che non finanzia nè che organizza direttamente", negando così il sostegno della Regione alla storica rassegna del cinema Lgbt di Torino, può sembrare marginale, ma non lo è affatto.  
  
Un atto marginale sul piano concreto ma molto significativo sul piano culturale, che rappresenta in modo trasparente l'estrema debolezza dell'assessore-candidato (il cosiddetto "nuovo") di fronte al vero padrone della Giunta regionale, l'Onorevole Cota, che il giorno dopo la sua elezione aveva esternato il suo pensiero contro il Pride cittadino.  
  
Ci sarebbe da chiedere "coerenza" su queste cose, e quindi ci aspettiamo che lor signori annuncino da subito il ritiro del logo della Regione Piemonte al Torino Film festival, a Cinema Ambiente, ma anche alla Fiera del Libro (che è organizzato da una autonoma Fondazione) e a tutte le altre iniziative di istituzioni culturali piemontesi che, tutte, non sono organizzate dalla Regione Pimeonte ma godono dei finanziamenti della stessa.  
  
In realtà tutti sanno che la politica che si ispira alla peggiore sessuofobia della Giunta Cota sta operando silenziosamente e puntigliosamente per cancellare ovunque sia possibile l'associazione della Regione Piemonte ad iniziative che hanno a che fare col mondo gay, lesbico e transessuale. Ci auguriamo che tutte le istituzioni culturali del Piemonte, non solo quelle politiche, diano risposte decise a questo vero e proprio attacco a ciò che a lor signori non piace.  
  
Non hanno nulla da dire i Presidenti e Direttori artistici del Teatro Stabile, del Teatro Regio e della  Fiera del Libro? Non credono anche loro che questi atti siano preludio di una chiusura anche culturale, dopo la chiusura economica che già si è abbattuta sulal cultura torinese e italiana?  
  
Tanti auguri, infine, al candidato "giovane" Coppola: se pensa di vincere a Torino con queste prese di posizione nella Città che è all'avanguardia sui temi del rispetto dei diritti lgbt e delle unioni civili, vuol dire che ha già perso in partenza. Bell’avvio di campagna, complimenti! I cittadini torinesi sapranno come premiarlo.  
  
Quest'anno abbiamo un motivo in più per non mancare all'appuntamento torinese del Festival del cinema lgbt, tra i più importanti  d’Europa,  non solo della nostra comunità, ma della cultura italiana tutta.