---
title: '2008-0140-Discrimination-ProgressReport-ST16769-RE01.EN08'
date: Mon, 08 Dec 2008 12:02:05 +0000
draft: false
tags: [Senza categoria]
---

  

  

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 9 December 2008

16769/1/08

REV 1

SOC 762

JAI 690

MI 519

  

  

  

  

  

REPORT

from :

The Presidency

to :

Permanent Representatives Committee (Part I) / Council (EPSCO)

No. prev. doc. :

16594/08 SOC 749 JAI 682 MI 509 + ADD 1

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

-    Progress Report

**I.       INTRODUCTION**

On 2 July 2008, the Commission adopted a proposal for a Council Directive aiming to extend the protection against discrimination on the grounds of religion or belief, disability, age or sexual orientation to areas outside employment. Complementing existing EC legislation[\[1\]](#_ftn1) in this area, the proposed Directive would prohibit discrimination on the above-mentioned grounds in the following areas: social protection, including social security and healthcare; social advantages; education; and access to goods and services, including housing.

  

A large majority of delegations have welcomed the proposal in principle, many endorsing the fact that it aims to complete the existing legal framework by addressing all four grounds of discrimination through a horizontal approach.

Most delegations have affirmed the importance of promoting equal treatment as a shared social value within the EU. In particular, several delegations have underlined the significance of the proposal in the context of the UN Convention on the Rights of Persons with Disabilities. However, some delegations would have preferred more ambitious provisions, particularly in regard to disability.

While emphasising the importance of the fight against discrimination, certain delegations have put forward the view that more experience with the implementation of existing Community law is needed before further legislation is adopted at the Community level. These delegations have questioned the timeliness and the need for the Commission’s new proposal, which they see as infringing on national competence for certain issues.

Certain other delegations have also requested clarifications and expressed concerns relating, in particular, to proportionality and subsidiarity, the division of competences, and the practical, financial and legal impact of the proposal.

For the time being, all delegations have maintained general scrutiny reservations on the proposal. CZ, DK, FR, MT and UK have maintained parliamentary scrutiny reservations. CY and PL have maintained linguistic scrutiny reservations. The Commission has meanwhile affirmed its original proposal at this stage and has maintained scrutiny reservations on any changes thereto.

The European Parliament is expected to adopt its Opinion under the Consultation Procedure on 24 March 2009[\[2\]](#_ftn2).

**II.      STATE OF PLAY**

The policy debate that took place in the EPSCO Council on 2 October 2008 confirmed the need for legal advice regarding the division of competences and the scope of the proposed Directive, as well as for greater clarity in respect of its expected impact. In the light of the positive remarks made by many delegations, the Presidency emphasised the need to maintain a high level of ambition and a broad scope encompassing all the discrimination grounds listed in Article 1 of the proposal.

In the light of the discussions to date, the Presidency has tabled a set of drafting suggestions[\[3\]](#_ftn3) concerning Articles 1-4 and the related Recitals, with a view to advancing the discussion of the proposal. In formulating these suggestions, the Presidency has also taken due note of the Commission's Impact Assessment[\[4\]](#_ftn4), as well as of the opinion of the Council Legal Service[\[5\]](#_ftn5), which broadly endorses the legal basis chosen by the Commission and confirms the Council's discretion in deciding what action is needed in the context of legislation proposed under Article 13 of the EC Treaty.

**1.** **Division of Competence, Legal Basis and Subsidiarity (Article 3)**

The Presidency's drafting suggestions clarified the text to the effect that the prohibition of discrimination would apply to _access to_ the fields listed in the scope (Article 3(1)). While most delegations have welcomed this approach as a step in the right direction, they have also agreed that further discussion is needed, particularly with a view to demarcating the division of competences between the Member States and the European Community as precisely as possible. Many delegations would prefer to place the requisite clarifications in the Articles of the draft Directive rather than in the Recitals, although some delegations could accept these clarifications remaining in the Recitals.

  

Further discussion is also needed on the delicate distinction between _access_ to fields such as education, healthcare and social protection, and _the organisation_ of such fields, the latter being an area of national competence. Other issues that require additional examination include _the cross-border dimension_ that underlies the Community competences in the fields listed in the scope, the provisions related to _family law_ (Article 3(2)), and the need to find a balance between anti-discrimination and the _rights of individuals in the private sphere_ (e.g. Article 3(1)).

**2.** **Provisions on Disability (Article 4)**

The Presidency's text seeks to align the provisions on disability more closely with those contained in the UN Convention on the Rights of Persons with Disabilities[\[6\]](#_ftn6), including the definition of "reasonable accommodation". It also clarifies the practical implications of the proposal, provides for a longer implementation period for measures requiring adaptation to existing buildings and infrastructures, and includes a clause stating clearly that the Directive is without prejudice to more detailed sectoral provisions aimed at improving accessibility. Delegations have broadly welcomed the Presidency's text as a step in the right direction. However, discussions have also revealed a need for further examination of the terms used (including "reasonable accommodation") and of the practical and financial implications of the provisions. In particular, delegations have requested clarification of the requirement to provide "effective non‑discriminatory access" _by anticipation_, especially in relation to housing and transport.

  

In this context, several delegations have taken the view that measures to improve access for persons with disabilities should, instead of being subject to strict deadlines, be progressively realised. Particularly strong concerns have been expressed concerning the extent of any provisions requiring the modification of existing buildings and the implied cost of the measures.

**3.** **Legitimate Differences of Treatment Based on Age and Disability**

Delegations have broadly welcomed the Presidency's attempt to clarify the provisions in respect of differences of treatment that should not be seen as discrimination (for example, cheaper public transport offered to children, disabled persons or pensioners). Delegations have also broadly welcomed the proposal to clarify the provisions concerning the assessment of risk by the providers of financial services, including insurance. However, further discussion is needed, particularly on the key notion of "objective and reasonable justification", so as to distinguish clearly between differences of treatment which would not be permitted and those which are justified.

**4.** **Legal Certainty**

In underlining the importance of legal certainty, delegations have expressed the wish to avoid further cases having to be brought before the European Court of Justice (ECJ). They have consequently stressed the need for the clearest possible wording, including in the _definitions_ of key terms, and have underlined the importance of ensuring _consistency with existing legislation_.

**5.           Other Issues**

A large number of questions have also been raised in relation to more specific issues that will require further discussion. These include the following:

-        the potential financial and administrative burden imposed by the provisions, particularly regarding SMEs and the self-employed;

-        the concept of discrimination by association;

-   the issue of gender mainstreaming and the question of multiple discrimination;

-        national legislation ensuring the secular nature of the state and measures concerning the wearing of religious symbols in schools; and

-   the length of the implementation period(s).

Further details of delegations' positions may be found in the most recent outcome of proceedings set out in doc. 16594/08 + ADD 1, and in docs. 12071/08, 12956/08 + COR 1 and 14254/08.

**III.    CONCLUSION**

While substantial progress has been made in the attempt to clarify the text of the draft Directive, particularly in regard to the division of competences and with a view to ensuring legal certainty, there is a clear need for further work on the proposal.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) In particular, Council Directives 2000/43/EC, 2000/78/EC and 2004/113/EC.

[\[2\]](#_ftnref2) Kathalijne Maria Buitenweg (Group of the Greens / European Free Alliance) has been appointed Rapporteur.

[\[3\]](#_ftnref3) The Presidency's text was examined by the Working Party on 21 and 27 November 2008.

[\[4\]](#_ftnref4) Doc. 11531/08 + ADD 1 + ADD 2. The Commission presented the Impact Assessment before the Working Party on 14 October 2008.

[\[5\]](#_ftnref5) Doc. 14896/08, examined by the Working Party on 6 November 2008.

[\[6\]](#_ftnref6) The UN Convention on the Rights of Persons with Disabilities, which has been signed by all the Member States and by the European Community and is awaiting ratification. See docs 12892/08 REV 1 and 12892/08 ADD 1 REV 1.