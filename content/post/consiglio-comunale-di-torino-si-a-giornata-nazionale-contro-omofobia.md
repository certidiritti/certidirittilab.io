---
title: 'CONSIGLIO COMUNALE DI TORINO: SI A GIORNATA NAZIONALE CONTRO OMOFOBIA'
date: Wed, 08 Jul 2009 07:28:57 +0000
draft: false
tags: [Comunicati stampa]
---

Una Giornata nazionale contro l'omofobia. La chiede una mozione approvata dal Consiglio comunale, a fronte del persistere delle discriminazioni nei confronti delle persone omosessuali, che in alcuni Paesi sono legalmente perseguite per il loro orientamento sessuale. Il documento approvato in aula sottolinea come resista anche in Italia un atteggiamento culturale che tende ad attribuire agli omosessuali etichette di perversione o addirittura di malattia, anche se già dal 17 maggio del 1990 l'Organizzazione Mondiale della Sanità ha messo fine ad una lunga tradizione di omofobia medica, depennando l'omosessualità dall'elenco delle patologie mentali.  
Le persone omosessuali, sono inoltre frequentemente oggetto di maltrattamenti, aggressioni e ricatti che non osano denunciare.  
Coerentemente con le risoluzioni del Parlamento Europeo che vincolano gli Stati membri a garantire i diritti delle persone che vivono in Europa e che hanno proclamato il 17 maggio "Giornata Internazionale contro l'omofobia", la Sala Rossa invita il Parlamento italiano a promuoverne il riconoscimento ufficiale e ad adoperarsi attivamente per una serena accettazione e valorizzazione delle diverse culture e mentalità.  
Il documento, approvato all'unanimità con 29 voti favorevoli, è stato sottoscritto dal primo firmatario Francesco Salinas e altri nove consiglieri (Cerutti, Cugusi, Silvestrini, Grimaldi, Domenico Gallo, Bonino, Zanolini, Cassano e Ferrante).