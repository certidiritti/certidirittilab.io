---
title: 'CORTE EUROPEA DIRITTI UMANI CONDANNA POLONIA PER DISCRIMINAZIONE'
date: Tue, 09 Mar 2010 11:57:05 +0000
draft: false
tags: [Comunicati stampa]
---

**LOTTA ALLE DISCRIMINAZIONI: LA CORTE EUROPEA PER I DIRITTI UMANI CONDANNA LA POLONIA PER AVER DISCRIMINATO UN CITTADINO RIMASTO SENZA CASA A CAUSA DEL DECESSO DEL CONVIVENTE.**

**Dichiarazione di Ottavio Marzocchi, responsabile questioni europee dell’Associazione Radicale Certi Diritti:**

"Lunedì 3 marzo la Corte Europea per i Diritti Umani, con voto unanime, ha sentenziato che nonostante la costituzione polacca definisca la famiglia come formata dall'unione di un uomo e una donna, l'impossibilità di succedere al contratto d'affitto del partner dello stesso sesso è una violazione degli articoli 14 (divieto di discriminazione) e 18 (diritto al rispetto della vita privata e famigliare) della Convenzione Europea dei Diritti Umani del 1950.

L'Associazione Radicale "Certi Diritti" sottolinea l'importanza del riferimento all'art. 18 della Convenzione Europea dei Diritti Umani che impone agli Stati sottoscrittori di prendere in considerazione il fatto che non esiste solo un modo di condurre la propria vita privata e che i diritti delle persone, indipendentemente dal loro orientamento sessuale, devono sempre essere rispettati.

Ancora una volta l'Europa afferma risolutamente che la tutela della famiglia non può tradursi nella discriminazione delle coppie dello stesso sesso.

In Italia, non c'è nessuna norma che limiti il matrimonio all'unione tra un uomo e una donna e il 23 marzo la Corte Costituzionale deciderà sulla legittimità del rifiuto delle autorità comunali di autorizzare le pubblicazioni matrimoniali di una coppia dello stesso sesso di Venezia. Speriamo che i principi della civiltà europea riaffermati dalla Corte Europea dei Diritti Umani vengano presi in considerazione".