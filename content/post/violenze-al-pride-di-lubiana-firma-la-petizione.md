---
title: 'VIOLENZE AL PRIDE DI LUBIANA - FIRMA LA PETIZIONE'
date: Thu, 26 Jun 2008 12:02:52 +0000
draft: false
tags: [Comunicati stampa]
---

Al termine del Pride di Lubiana si sono verificati tre distinti atti di violenza omofobica nei confronti di persone che indossavano la maglietta ufficiale del Pride Parade. Certi Diritti invita tutti a firmare e [diffondere questa petizione](http://www.ipetitions.com/petition/homofobija08/) rivolta ai politici sloveni.  
[Clicca qui per i dettagli.](index.php?option=com_content&task=view&id=129&Itemid=53)  
  
  
Fonti slovene: [comitato del pride sloveno](http://www.ljubljanapride.org/sporocilo.htm) .  
  
  
Fonte tradotta in Italiano: [Post di QueerWay](http://www.queerway.it/dblog/articolo.asp?articolo=1074) :  
  
_l primo attacco, ai danni di una ragazzo, è accaduto proprio a ridosso della manifestazione. Due ragazzi hanno attaccato a malmenato un ragazzo ferendolo al volto e stappandogli la T-shirt del Pride gridando insulti contro i "froci". Il secondo attacco, a poca distanza dal primo: tre uomini al grido di "Hanno la maglietta, sono froci!" hanno aggredito un altro ragazzo che aveva appena partecipato alla manifestazione colpendolo ripetutamente e strappandogli di dosso, anche stavolta, la maglietta del Pride._

_Nella serata, poi, ci sono stati ancora due assalti distinti vicino al locale K4 Club. Nel primo è stato aggredito un altro ragazzo mentre nella seconda aggressione è stata presa di mira una coppia che è stata brutalmente malmenata al grido di "dannati froci".  
_

_I destinatari della petizione:  
_

*   _**Al Presidete della Slovenia, Danilo Türk:**  
    Che misure intende prendere per interrompere questa violenza?  
    Come intende gestire l'aumento dell'omofobia nella società?_
*   _**Al Primo Ministro sloveno, Janez Jansa:**  
    Come intende assicurare la sicurezza delle persone che vivono in Slovenia?  
    Quali concreti e sistematici interventi intende proporre e mettere in atto per ridurre il livello di intolleranza della società slovena compresa l'omofobia?  
    Che cosa intende fare perchè la polizia possa essere in grado di identificare la violenza omofoba e perchè sia perseguibile in modo efficiente come tale?_
*   _**Al presidente dell'Assemblea Nazionale, France Cukjati:**  
    Il livello di odio verso gay e lesbiche in Parlamento è alto, come si può vedere in diversi dibattiti parlamentari. Siamo certi che questo influenza l'omofobia nella società, e anche gli attacchi omofobici.  
    Che cosa intende fare per eliminare le espressioni di odio da parte del Parlamento?_
*   _**Al Sindaco di Lubiana, Zoran Jankovic:**  
    Cosa intende fare perchè le aggressioni non si verifichino di nuovo?  
    Che cosa intende fare per rendere Lubiana piacevole, tollerante e sicura come ha dichiarato alla Pride Parade?_