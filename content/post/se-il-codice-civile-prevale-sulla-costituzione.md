---
title: 'SE IL CODICE CIVILE PREVALE SULLA COSTITUZIONE'
date: Wed, 30 Nov -0001 00:00:00 +0000
draft: false
tags: [Senza categoria]
---

Matrimonio omosessuale, se il codice civile prevale sulla Costituzione,  
di Persio Tincani, docente di filosofia del diritto, Università di  
Bergamo  
   
La vicenda del matrimonio omosessuale in Corte costituzionale si è  
conclusa nel modo che in molti prevedevano, cioè con un sostanziale  
rigetto delle questioni di costituzionalità rimesse dalla corte  
d’appello di Trento e dal tribunale di Venezia. Che la decisione fosse,  
in questo senso, prevedibile non ha, però, nulla a che vedere con la  
questione in sé (il matrimonio omosessuale è compatibile con la  
Costituzione?) e molto a che vedere con il fatto che non dobbiamo  
fingerci vergini, del tipo di quelle convinte che ci sia sempre un  
giudice a Berlino. Che la Corte avrebbe respinto le questioni, insomma,  
eravamo più o meno tutti ragionevolmente certi, tanto i favorevoli al  
matrimonio omosessuale, ovvero la stragrande maggioranza dei giuristi  
italiani, quanto la minoranza dei giuristi contrari.  
   
Tutti o quasi tutti, infatti, consideravano assai improbabile che la  
Corte avrebbe deciso nel senso dell’ammissibilità del matrimonio  
omosessuale, in quanto la questione è stata caricata (non importa  
adesso quanto ciò sia stato fatto ad arte) di un significato politico  
pressoché esclusivo, che ha finito per far passare nelle retrovie il  
fatto che si tratti, come ogni altra questione posta di fronte alla  
Consulta, di una faccenda di leggi e di diritto.  
   
Al di là delle argomentazioni sostenute da ciascuno per la tesi della  
fondatezza o dell’infondatezza dei particolari rilievi di  
costituzionalità presenti nei due atti con i quali le corti hanno posto  
la questione di fronte alla Consulta, e ancor di più al di là degli  
argomenti che ciascuno adduce per l’ammissibilità o per  
l’inammissibilità del matrimonio omosessuale nel nostro ordinamento,  
nessuno avrebbe scommesso sul fatto che una parola definitiva sarebbe  
stata pronunciata dalla Corte in merito.  
   
Ciò che stupisce, quindi, non è che la Corte abbia dichiarato non  
fondate le questioni di costituzionalità, ma il modo in cui lo ha  
fatto, cioè con una sentenza, la n. 138/2010 (15 aprile), assai  
criticabile, sia sotto il profilo della tecnica giuridica, sia sotto il  
profilo della mera coerenza argomentativa. I passaggi argomentativi  
fallaci o discutibili della sentenza sono molti. Qui mi limito a  
segnalarne uno.  
   
   
   
Uno dei cavalli di battaglia degli oppositori del matrimonio  
omosessuale è il richiamo alla “natura” presente nell’art. 29 della  
Costituzione, dove si sancisce che “la Repubblica riconosce i diritti  
della famiglia come società naturale fondata sul matrimonio”. Secondo  
l’interpretazione proposta da costoro, il significato di questo  
articolo sarebbe quello di sancire sul livello costituzionale la  
cosiddetta “famiglia naturale”, che sarebbe composta da una moglie, un  
marito e, possibilmente, da un certo numero di figli.  
   
Va da sé che è sufficiente leggere con un minimo di attenzione, o di  
onestà, il testo dell’art. 29 per vedere che le cose non stanno così e  
che non c’è nessuna “famiglia naturale”. Si parla, infatti, di  
“famiglia come società naturale”, il che significa, nel linguaggio  
giuridico, società che le persone formano senza che vi sia necessità di  
norme giuridiche (diversamente da come accade, per esempio, nel caso  
delle società per azioni, che non sarebbero concepibili senza le norme  
giuridiche che le definiscono e che le disciplinano).  
   
Il diritto, insomma, arriva dopo. E la Costituzione, in particolare,  
arriva per stabilire che i diritti “della famiglia” (in realtà diritti  
dei singoli che la compongono) sono riconosciuti a patto che questa  
“società naturale” abbia dato luogo a un matrimonio, secondo le norme  
del diritto civile. La Corte, infatti, non prende neppure in  
considerazione la tesi della “famiglia naturale” e ribadisce, a  
beneficio dei duri, l’ovvietà che il testo della norma costituzionale  
ha il significato di riconoscere, appunto, che le famiglie esistono  
anche senza che vi sia una norma giuridica che le definisce.  
   
Fin qui, nulla da dire. Il punto critico, però, viene subito dopo,  
quando la Corte scrive: “Ciò posto, è vero che i concetti di famiglia e  
di matrimonio non si possono ritenere “cristallizzati” con riferimento  
all’epoca in cui la Costituzione entrò in vigore, perché sono dotati  
della duttilità propria dei princìpi costituzionali e, quindi, vanno  
interpretati tenendo conto non soltanto delle trasformazioni  
dell’ordinamento, ma anche dell’evoluzione della società e dei costumi.  
   
Detta interpretazione, però, “non può spingersi fino al punto  
d’incidere sul nucleo della norma, modificandola in modo tale da  
includere in essa fenomeni e problematiche non considerati in alcun  
modo quando fu emanata”. In particolare, dato che “la questione delle  
unioni omosessuali rimase del tutto estranea al dibattito svoltosi in  
sede di Assemblea \[costituente, N. d. A.\], benché la condizione  
omosessuale non fosse certo sconosciuta \[...\] si deve ribadire, dunque,  
che la norma non prese in considerazione le unioni omosessuali, bensì  
intese riferirsi al matrimonio nel senso tradizionale di detto  
istituto”.  
   
Proviamo a vedere che cosa c’è che non va in questo passaggio,  
cruciale, della sentenza. Se e è vero che i concetti di “famiglia” e di  
“matrimonio” non devono intendersi come cristallizzati nell’epoca in  
cui fu formulato l’art. 29, allora non si capisce per quale motivo  
l’evoluzione della società e dei costumi, pure richiamata dal testo  
della sentenza come elemento da tenere in conto per interpretare la  
norma, non sia qui idonea a ricomprendere nella definizione di  
“famiglia” che può accedere al “matrimonio” quella composta da due  
persone dello stesso sesso. La Corte chiarisce, però, che il ruolo  
dell’interpretazione non può spingersi fino a incidere “sul nucleo  
della norma”, che indicherebbe il matrimonio “nel significato  
tradizionale”.  
   
Qui, come si vede, c’è qualcosa che non quadra. Il fatto che il  
costituente abbia preso in considerazione il matrimonio tradizionale è,  
con ogni probabilità, indubbio. Ma, allo stesso tempo, ciò non può  
essere inteso come un vincolo per l’interprete successivo, almeno non  
se si ammette, come fa la Corte, che i principi costituzionali sono  
contraddistinti da una intrinseca duttilità, data dal loro tener conto  
delle trasformazioni dell’ordinamento e dall’evoluzione “della società  
e dei costumi”.  
   
Questa evidente incongruenza (o le norme sono duttili e seguono  
l’evoluzione dei costumi o non lo sono) viene, però, corretta da  
un’immediata precisazione della Corte stessa, che chiarisce come la  
“duttilità” non possa giungere fino a “incidere sul nucleo della  
norma”. Qual è questo nucleo? L’eterosessualità del matrimonio. E  
perché? Perché lo stabilisce il codice civile: “I costituenti,  
elaborando l’art. 29 Cost., discussero di un istituto che aveva una  
precisa conformazione ed un’articolata disciplina nell’ordinamento  
civile. Pertanto, in assenza di diversi riferimenti, è inevitabile  
concludere che essi tennero presente la nozione di matrimonio definita  
dal codice civile entrato in vigore nel 1942, che \[...\] stabiliva (e  
tuttora stabilisce) che i coniugi dovessero essere persone di sesso  
diverso”.  
   
Ora, affermare che il codice civile stabilisca la diversità di sesso  
degli sposi perché il matrimonio sia valido è perlomeno azzardato. Il  
codice civile, piuttosto, non esplicita mai la condizione necessaria  
della diversità di sesso per un valido matrimonio, tant’è che le  
decisioni giudiziarie esistenti in merito hanno sempre ricavato questo  
elemento da un lavoro interpretativo.  
   
Ma anche se si volesse sorvolare su questo punto – in realtà il centro  
dell’intera questione – il ragionamento esplicitato dalla Corte si  
rivela un controsenso, perché sfocia nella sola conseguenza logica  
possibile di subordinare la Costituzione al codice civile. Che, per  
essere precisi, è proprio il contrario di quello che, invece, si fa a  
rigor di diritto: non interpreto un articolo della Costituzione alla  
luce del codice civile, ma viceversa.  
   
Se così non fosse, infatti, non si capisce quale sarebbe il ruolo di  
una corte costituzionale, e quale tipo di censura potrebbe esercitare  
sul diritto ordinario, se quest’ultimo diviene lo strumento al quale la  
norma costituzionale deve guardare per ricevere significato. | Fonte  
Micromega