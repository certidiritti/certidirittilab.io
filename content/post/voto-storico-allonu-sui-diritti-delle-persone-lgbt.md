---
title: 'Voto storico all''ONU sui diritti delle persone lgbt'
date: Tue, 21 Jun 2011 09:02:17 +0000
draft: false
tags: [Transnazionale]
---

**La Commissione Diritti Umani dell'Onu ha votato una risoluzione proposta dal Sud Africa che afferma l'eguaglianza dei diritti a prescindere dell'orientamento sessuale e dell'identità di genere e condanna discriminazioni contro gay e trans.  Voto di portata storica.**

**Leggi il testo della risoluzione (in italiano) >**

Ginevra, 17 giugno 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti, Non c’è Pace Senza Giustizia e del Partito Radicale nonviolento, transnazionale e transpartito

Oggi a Ginevra, alla Commissione Diritti Umani delle Nazioni Unite è stata votata una storica Risoluzione di condanna delle discriminazioni a causa dell’orientamento sessuale in tutto il mondo dove si sostiene anche che ‘tutti gli esseri umani nascono liberi ed uguali nella loro dignità e diritti e devono godere della libertà e dei diritti senza alcun tipo di distinzione”. La Risoluzione “chiede anche all’Alto Commissariato per i Diritti Umani di avviare uno studio sulle violenze e le discriminazioni basate  sull’orientamento sessuale e sull’identità di genere”  
La Risoluzione è la prima votata su questo argomento dalla Commissione Diritti Umani di Ginevra e secondo il parere unanime di tutte le Ong è di portata storica. La Risoluzione è stata sostenuta da 39 paesi; dagli Stati Uniti, dai paesi dell’Unione Europea, dal Brasile e gli altri paesi dell’America Latina.  Hanno votato a favore 23 paesi, 19 hanno votato contro e 3 si sono astenuti.  
I paesi africani islamici, la Nigeria, il Pakistan si sono opposti dichiarando che la Risoluzione non aveva niente a che fare con i diritti umani fondamenali.

**Dichiarazione di Sergio Rovasio, Segretario Ass.ne Radicale Certi Diritti e Matteo Mecacci, deputato Radicale e rappresentante del Prntt all’Onu:**

“Da nonviolenti Radicali non possiamo che essere felici di questo straordinario passo avanti fatto dalla Commissione Onu per i Diritti Umani di Ginevra di cui il Partito Radicale Nonviolento, transnazionale e transpartito è membro dal 1992. In diverse occasioni il Prntt aveva ceduto il proprio tempo di parola alle organizzazioni lgbt di paesi dove vi sono gravi persecuzioni contro le persone omosessuali e transessuali. La strada per il superamento delle diseguaglianze va avanti con sempre più determinazione. Sarebbe ora che anche l'Italia adeguasse il proprio ordinamento interno seguendo quanto l'Onu, l'Ue e molti altri paesi democratici fanno per la lotta alle discriminazioni”.

**Il testo integrale della Risoluzione (in inglese):**

[http://www.scribd.com/doc/57906437/United-Nations-17-6-2011-SOGI-Resolution](http://www.scribd.com/doc/57906437/United-Nations-17-6-2011-SOGI-Resolution)

**traduzione italiana a cura di Alba Montori:**

L.9 Rev1 (14/06/11)  
Diritti umani, orientamento sessuale e identità di genere

Il Consiglio dei diritti umani, ricordando  
l'universalità, l'interdipendenza, l'indivisibilità e interrelazione dei diritti umani sanciti nella Dichiarazione universale dei diritti dell'uomo conseguentemente elaborate in altri strumenti di promozione sui diritti umani come il Patto internazionale sui diritti economici, sociali e culturali, il Patto internazionale sui diritti civili e politici e altri fondamentali strumenti  attinenti ai diritti umani;

ricordando  
che anchela Dichiarazione Universale dei Diritti Umani afferma che tutti gli esseri umani nascono liberi ed eguali in dignità e diritti e che ciascuno può avvalersi di tutti i diritti e le libertà enunciati in questa Dichiarazione, senza distinzione alcuna, per ragioni di razza, colore , sesso, lingua, religione, opinione politica o di altro genere, origine nazionale o sociale, di ricchezza, di nascita o di altra condizione;

ricordando inoltre  
la risoluzione GA 60/251, che stabilisce che il Consiglio dei Diritti Umani è responsabile nel promuovere il rispetto universale per la protezione di tutti i diritti umani e delle libertà fondamentali per tutti, senza distinzione di qualsiasi genere e in modo equo ed eguale;

Esprimendo grave preoccupazione  
gli atti di violenza e discriminazione, commessi contro persone in tutte le aree del mondo, a causa del loro orientamento sessuale e identità di genere

1\. richiede  
all'Alto Commissario per i diritti umani di avviare uno studio da completarsi entro dicembre 2011, per documentare le leggi discriminatorie e le pratiche e gli atti di violenza contro gli individui in base al loro orientamento sessuale e identità di genere, in tutte le regioni del mondo, e su come le leggi internazionali sui diritti umani possano essere utilizzate per porre fine alla violenza e le violazioni dei diritti umani basate sull'orientamento sessuale e identità di genere  
2.decide  
di convocare una tavola rotonda nel corso della 19a sessione del Consiglio dei diritti umani, informata dei dati contenutinello studio commissionato dall' Alto Commissario e di attuare un dialogo costruttivo, trasparente e consapevole sul tema delle leggi e  delle pratiche discriminatorie e atti di violenza contro gli individui in base al loro orientamento sessuale e identità di genere;  
3.decide  
inoltre che il gruppo discuterà anche un seguito adeguato alle raccomandazioni dello studio commissionato dall'Alto Commissario  
4\. decide  
di continuare ad occuparsi prioritariamente di questo tema.