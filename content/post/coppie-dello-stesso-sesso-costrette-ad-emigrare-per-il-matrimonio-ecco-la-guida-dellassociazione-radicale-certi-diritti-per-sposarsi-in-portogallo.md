---
title: 'Coppie dello stesso sesso costrette ad emigrare per il matrimonio: ecco la guida dell''associazione radicale Certi Diritti per sposarsi in Portogallo'
date: Fri, 21 Dec 2012 12:44:59 +0000
draft: false
tags: [Europa]
---

Comunicato stampa dell'associazione radicale Certi Diritti  
  
Roma, 21 dicembre 2012  
  
Il 10 dicembre si è celebrata la Giornata Mondiale dei Diritti Umani e il segretario generale dell’ONU, Ban Ki-moon, nel messaggio per questa Giornata, ha ricordato che ”Le istituzioni devono rappresentare la società in tutta la sua diversità \[…\] “ .Questa affermazione assume particolare rilievo nella realtà italiana dove i diritti delle persone LGBTI non trovano alcun accoglimento proprio a livello istituzionale e con questi i diritti civili in generale.  
  
Mentre a livello mondiale giungono notizie confortanti sull’approvazione dei matrimoni egualitari per le coppie dello stesso sesso, il Governo italiano ha ignorato per anni l’invito della Corte Costituzionale a legiferare in materia e molti dei politici che saranno i protagonisti della prossima campagna elettorale si spingono al massimo ad ipotizzare l’approvazione di unioni civili, istituti sempre di serie B che non rispondono alla richiesta di uguaglianza che arriva ormai da tutte le associazioni LGBT. In questa situazione molte coppie sono costrette ad emigrare per vedere affermato il loro diritto al matrimonio. Per fornire loro delle utili informazioni, l’Associazione Radicale Certi diritti ha diffuso una guida pratica al matrimonio in Portogallo che, con la Norvegia, non richiede alle coppie la residenza nel Paese per poter celebrare il matrimonio tra persone dello stesso sesso. Nella guida sono contenute tutte le informazioni pratiche per istruire la pratica e le indicazioni per poter realizzare il desiderio di costruire una famiglia anche se formalmente solo all’estero.  
  
L’Associazione Radicale Certi diritti rimarca il suo impegno ad ottenere in Italia la riforma del Diritto di famiglia che comprenda il matrimonio egualitario, e invita le coppie a non accettare le discriminazioni a cui sono sottoposte ma ad iniziare sempre nuove cause pilota per vedere affermati i diritti di cui sono portatori  tutti  i/le cittadini/e.

**[LA GUIDA IN FORMATO PDF >](notizie/comunicati-stampa/item/download/21_9dbe15cac26479acfdca1ac403c63767)**

  
[sposarsi\_in\_Portogallo_definitivo.pdf](http://www.certidiritti.org/wp-content/uploads/2012/12/sposarsi_in_Portogallo_definitivo.pdf)