---
title: 'ORIENTAMENTO SESSUALE E DIRITTI FONDAMENTALI:ITALIA E USA A CONFRONTO'
date: Thu, 08 Apr 2010 00:02:43 +0000
draft: false
tags: [Senza categoria]
---

Gli avvocati Francesco Bilotta (ricercatore confermato dell'Università di Udine e membro fondatore di Rete Lenford - Avvocatura per i diritti LGBT) e Matteo Winkler (membro di Rete Lenford) proporranno un'analisi del rapporto tra le libertà fondamentali, ed in particolare il principio di uguaglianza, che rappresentano i fondamenti della convivenza civile tanto negli Stati Uniti d’America quanto in Europa, e i diritti rivendicati da parte delle coppie omosessual.

Inoltre, eventualmente, si discuterà la sentenza che la Corte Costituzionale dovrebbe emettere il 12 aprile in merito alla costituzionalità del divieto di matrimonio civile tra persone dello stesso sesso.  
  
La conferenza è pubblica e si svolgerà giovedì 15 aprile 2010 alle ore 14.30 nell'aula 7 dell'edificio U2 dell'Università degli Studi di Milano - Bicocca, in Piazza della Scienza, 3.