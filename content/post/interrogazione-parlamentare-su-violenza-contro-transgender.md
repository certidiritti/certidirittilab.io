---
title: 'INTERROGAZIONE PARLAMENTARE SU VIOLENZA CONTRO TRANSGENDER'
date: Wed, 19 Nov 2008 17:00:48 +0000
draft: false
tags: [Comunicati stampa]
---

**Giornata internazionale vittime transgender: depositata alla Camera e al Senato un’interrogazione parlamentare urgente sugli atti di violenza avvenuti nel 2008 contro le persone transgender e transessuali**

Roma, 19 novembre 2008

In occasione della giornata internazionale dedicata alla memoria delle vittime transessuali e transgender che si celebra domani, giovedì 20 novembre, in molti paesi del mondo, i parlamentari radicali hanno oggi depositato al Senato della Repubblica e alla Camera dei deputati la seguente interrogazione parlamentare urgente rivolta al Governo italiano:

Al Presidente del Consiglio;

al Ministro degli Interni;

al Ministro della Sanità

al Ministro dell'Istruzione

al Ministro per le Pari Opportunità;

Per sapere – Premesso che:

\- il 20 novembre è la giornata internazionale dedicata alla memoria delle vittime transgender della violenza omofobica (Transgender day Of Remembrance);

\- secondo un rapporto dell'Arcigay, diffuso dall'Ufficio Diritti umani e lotta alla violenza, nel solo anno 2008, contro le persone trasngender e transessuali, ci sono stati tre omicidi e diversi atti di inaudita violenza in varie parti del paese, in particolare a Roma, che spesso non vengono neanche denunciati;

\- le aggressioni sono quasi sempre ispirate da un atteggiamento di pregiudizio e violenza che ha come cornice l'omofobia, molto diffusa nel nostro paese;

\- le persone transgender e transessuali vivono una condizione soggettiva difficile che necessiterebbe di maggiore aiuto e assistenza da parte delle strutture pubbliche, in particolare in ambito sanitario e sociale;

Per sapere:

\- se il Governo è a conoscenza degli atti di violenza, che in alcuni casi hanno causato il decesso delle vittime, avvenuti in Italia nel 2008 contro le persone transgender e transessuali;

\- se non ritenga necessario incontrare le Associazioni transgender e transessuali insieme a quelle che si occupano della difesa dei loro diritti con l'obiettivo di avviare politiche di educazione, aiuto e assistenza adeguate;

\- se non ritenga altresì necessario avviare forme di sostegno educativo a tutti i livelli, affinché venga combattuto il pregiudizio omofobico e transfobico nel nostro paese;

\- se non ritenga infine urgente promuovere iniziative anche legislative di sostegno, principalmente in ambito sanitario e sociale, affinché le persone transgender e transessuali possano vivere con adeguato aiuto la loro esistenza.