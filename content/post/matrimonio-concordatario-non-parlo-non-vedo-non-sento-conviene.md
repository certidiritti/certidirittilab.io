---
title: 'Matrimonio concordatario: non parlo, non vedo, non sento  (conviene)'
date: Thu, 12 Jan 2012 21:38:05 +0000
draft: false
tags: [Diritto di Famiglia]
---

**Apologo triste, ma con lieto fine**

Tizio e Caia si amano, e si fidanzano.

Stanno molto bene insieme, anche perché condividono un modo di essere: sono una "coppia aperta". Per loro la fedeltà è roba da medioevo, hanno altre storie, se le raccontano pure, nessun problema.

Tizio e Caia sono proprio contenti, e decidono di sposarsi. In fatto di cerimonie, sono un po' perbenisti: va bene essere una coppia aperta, ma l'anello al dito lo si mette in chiesa davanti al prete, ci sono anche i parenti che preferiscono così…

Fanno un bel matrimonio, normalissimo, concordatario.

Un giorno però Tizio e Caia litigano di brutto, succede. Decidono di separarsi.

Il fatto è che uno dei due guadagna molto, molto più dell'altro: quello più povero ha dunque diritto all'assegno mensile, lo dice la legge.

Quando c'è di mezzo il soldo, però, coppia aperta o chiusa che sia, la furbizia fiorisce. Il coniuge ricco (lei o lui, non importa) corre al Tribunale Ecclesiastico: "il mio matrimonio è nullo, escludevamo la fedeltà". Il giudice ecclesiastico gli dà ragione, e fa bene, la fedeltà è uno dei "bona matrimonii" secondo la legge canonica.

Il ricco continua a correre. Con la sentenza di nullità ecclesiastica in mano, arriva dal giudice civile: "il mio matrimonio è stato dichiarato nullo, adesso tu devi rendere efficace per lo Stato la nullità ecclesiastica (così non pago l'assegno)". Il giudice civile si informa, controlla e verifica. Certamente tutti e due i coniugi, il giorno delle nozze, escludevano il dovere di fedeltà, e ciascuno sapeva che l'altro lo escludeva. Il giudice civile così dà ragione al ricco: le nullità del codice sono diverse da quelle ecclesiastiche, ma per il matrimonio concordatario valgono quelle ecclesiastiche, se l'altro coniuge conosceva o poteva conoscere la causa di nullità.

Chi vuol fare il moralista può essere soddisfatto, se vuoi la coppia aperta, ma poi fai l'ipocrita e vai a sposarti in chiesa, pagane le conseguenze.

Lo Stato, però, non fa (non dovrebbe fare) il moralista. Le nullità canoniche sono rispettabilissime, ma per il diritto della chiesa, e non si vede perché il diritto civile debba cedere il passo a prospettazioni di fede, importanti per il credente, ma estranee alla legge laica dello Stato.

In Italia, invece, le cose vanno (per fortuna, non sempre) come nella storia che abbiamo raccontato, assolutamente vera.

Certo, il matrimonio concordatario non ci fa una gran figura.

*       *       *

Lieto fine. La cultura laica, la cultura della Costituzione, è capace di trovare le soluzioni giuste.

"La prolungata convivenza fra i coniugi (nella specie, venti anni) costituisce elemento ostativo alla delibazione (questo è il nome giuridico della dichiarazione di efficacia per lo Stato) di sentenza ecclesiastica di nullità del matrimonio". Sono parole della Cassazione, sentenza 1343 del 2011. Insomma, c'è un giudice a Berlino, ma ce n'è uno anche a Roma….