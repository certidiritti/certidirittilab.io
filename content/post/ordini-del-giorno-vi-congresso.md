---
title: 'Ordini del giorno VI° Congresso'
date: Tue, 09 Apr 2013 08:10:43 +0000
draft: false
tags: [Politica]
---

**APPELLO ALLE ASSOCIAZIONI ITALIANE IMPEGNATE A DIFESA DEI DIRTTI UMANI DELLE PERSONE GAY, LESBICHE, TRANSESSUALI E INTERSESSUALI**

Il VI Congresso dell'Associazione radicale certi diritti ritiene che la straordinaria realtà - nel senso etimologico del termine "straordinario" - che il nostro Paese vive, necessità di straordinarie risposte unitarie per far si che il tema della difesa e della promozione dei diritti umani delle persone lgbt, in Italia e nel mondo, non sia del tutto cancellato dall'agenda politica e istituzionale del Paese.

Per raggiungere questo obiettivo ritiene che le associazioni italiane attive su questi temi debbano riprovare, nuovamente e con maggiore determinazione, a lavorare insieme su precisi obiettivi, anche cercando di condividere strategie innovative. L'esempio che ci arriva da molte realtà europee e non europee, testimoniato dagli importanti interventi ospitati dal nostro Congresso di rappresentanti dei movimenti lgbt di Francia, Gran Bretagna, Argentina, Turchia, Russia, Albania e USA, e di rappresentanti di ILGA e OHCHR, documenta, al di là di ogni possibile dubbio, che l'unità di intenti e di iniziativa dei movimenti sono condizione essenziale per il raggiungimento degli obiettivi di riforma di cui il Paese ha bisogno.

A tal fine il Congresso da mandato specifico agli organi dell'associazione di provare ancora una volta nel tentativo di trovare unità di obiettivi e di strategia con le altre associazioni lgbti italiane, nelle forme che verranno condivise secondo l' "unità laica delle forze" che come radicali ci ha guidato nei nostri rapporti con le altre associazioni.

Tentativo che deve vedere, tra i suoi primi obiettivi, la promozione di forme di coordinamento e interlocuzione continuativa con i neo parlamentari, sia tra di loro che con le associazioni italiane, sui temi degli interventi necessari per garantire dignità e diritti umani delle persone lgbt, in Italia e nel mondo.

**ADESIONE E SOSTEGNO ALLLA INIZIATIVA POPOLARE PROMOSSA DALL'ASSOCIAZIONE LUCA COSCIONI PER L'EUTANASIA LEGALE**

Il VI Congresso dell'Associazione radicale certi diritti aderisce alla campagna di raccolta firme promossa dall'Associazione Luca Coscioni insieme numerose altre realtà italiane, per l'eutanasia legale.

Ringrazia l'Associazione Luca Coscioni per essersi fatta carico della promozione di questa iniziativa che riteniamo necessaria e urgente per smuovere il Parlamento sul tema dell'esercizio pieno della libertà individuale, e si rivolge agli iscritti e simpatizzanti dell'Associazione perché contribuiscano, con tutti i mezzi a loro a disposizione, per raggiungere l'obiettivo delle 50.000 firme e sostenere al campagna nazionale in atto.

**SULLA STRATEGIA NAZIONALE PER IL CONTRASTO ALLE DISCRIMINAZIONI BASATE SU ORIENTAMENTO SESSUALE E DI GENERE**

Il VI Congresso dell'Associazione radicale certi diritti che si è svolto a Napoli dal 5 al 7 aprile 2013 rivolge un sentito ringraziamento al Ministro Elsa Fornero per il messaggio che ha rivolto ai partecipanti e le parole, inequivoche, che ha usato per definire la necessità di iniziative, legislative e non, al fine di garantire pari opportunità ed uguaglianza per tutti e tutte, al di là della personale identità di genere od orientamento sessuale.

Il Congresso coglie l'occasione per sottolineare l'importanza del lavoro svolto dall'UNAR per la definizione della Strategia nazionale contro le discriminazioni basate su orientamento sessuale e identità di genere. Importante per i contenuti che raccoglie e per il metodo che ha finalmente inaugurato anche in Italia di coinvolgimento non solo delle Istituzioni ma anche dell'associazionismo. Strategia che, pur non potendo corrispondere appieno alle richieste contenute nella Raccomandazione del Consiglio d'Europa, rappresenta un ottimo strumento di lavoro per la pubblica amministrazione.

La nostra associazione ha collaborato al monitoraggio indipendente della Raccomandazione del Consiglio d'Europa (a cui la Strategia fa riferimento) promosso da ILGA Europe e, per l'Italia, realizzato dal Centro Risorse lgbt con la collaborazione di Arcigay, Arcilesbica, Agedo, Famiglie Arcobaleno, MIT e Coordinamento Silvya Rivera. E richiamandosi a questo importante lavoro di analisi e documentazione chiede al Ministro di adottare la Strategia con un atto formale, di livello governativo, che consenta al documento di avere la definizione amministrativa che il Consiglio d'Europa si aspetta e la realtà italiana necessità.

Napoli 7 aprile 2013