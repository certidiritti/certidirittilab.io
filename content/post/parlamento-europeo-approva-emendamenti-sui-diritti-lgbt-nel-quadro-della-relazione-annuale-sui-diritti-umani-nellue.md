---
title: 'Parlamento europeo approva emendamenti sui diritti lgbt nel quadro della relazione annuale sui diritti umani nell''UE'
date: Fri, 09 Nov 2012 13:44:09 +0000
draft: false
tags: [Europa]
---

La commissione libertà pubbliche, giustizia ed affari interni ha votato il 6 novembre 2012 la relazione sulla situazione dei diritti umani nell'Unione europea, approvando una serie di emendamenti importanti in merito ai diritti delle persone LGBT di cui Certi Diritti pubblica l'elenco.

La commissione libertà pubbliche, giustizia ed affari interni ha votato il 6 novembre 2012 la relazione sulla situazione dei diritti umani nell'Unione europea, approvando una serie di emendamenti importanti in merito ai diritti delle persone LGBT.  
Gli emendamenti erano stati sponsorizzati sia dai membri dell'intergruppo LGBT al PE, sia dal gruppo liberale e democratico.  
I deputati liberali, socialisti, verdi e comunisti hanno votato a favore, mentre i popolari e conservatori hanno votato contro, seppure con alcune defezioni. La relazione sarà sottoposta al voto della plenaria nel corso della sessione di dicembre.  
   
**Segue il testo degli emendamenti approvati (nel corso delle prossime settimane il PE pubblicherà sul suo sito i testi tradotti):**  
   
Compromise AM 9: Am  160 Kinga Göncz, Am 161 Monika Flašíková Beňová, Tatjana Ždanoka, Renate Weber, Am 164 Kinga Göncz, Am 165 Monika Flašíková Beňová, Tatjana Ždanoka, Renate Weber  
   
Paragraph 14 c (new)  
   
14c. Deplores the fact that not all Member States have properly transposed the Council Framework Decision on combating certain forms and expressions of racism and xenophobia by means of criminal law; calls on Member States to prosecute xenophobia, racism, anti-gypsyism and other forms of violence and hatred against any minority groups, including hate speech; calls on the Member States to ensure: that bias-motivated offences, such as those with racist, xenophobic, anti-Semitic, islamophobic, homophobic or transphobic intent are punishable within the criminal law system; that these offences are properly registered and investigated effectively; that the offenders are prosecuted and punished, and that victims are offered proper assistance, protection and compensation; recalls that on 1 December 2014 this Framework Decision will become fully enforceable;  
   
Discriminazione  
   
Compromise AM 10: Am  170 Renate Weber, Sophia in 't Veld, Am 171 Kinga Göncz,, Am 172 Monika Flašíková Beňová, Am 174 Antigoni Papadopoulou, Am 175 Marie-ChristineVergiat  
   
Paragraph 15  
   
15\. Stresses that the principles of human dignity and equality before the law are the foundations of democratic society; deplores the current blockage of Council negotiations on the Commission's proposal for a horizontal directive extending comprehensive protection against discrimination on all grounds; calls on the Council to act, on the basis of Article 265 TFUE and to adopt the directive;  
   
Orientamento sessuale e identità di genere  
   
23\. invita gli Stati membri a registrare e a investigare i reati generati dall'odio contro persone gay, lesbiche, bisessuali e transgender (LGBT) e ad adottare legislazioni penali che vietino l'istigazione all'odio sulla base dell'orientamento sessuale e dell'identità di genere;  
   
24\. invita la Commissione a proporre una rifusione della decisione quadro del Consiglio sulla lotta contro talune forme ed espressioni di razzismo e xenofobia mediante il diritto penale includendo altre forme di reato generato dall'odio, compreso quello fondato  
sull'orientamento sessuale, sull'identità e l'espressione di genere;  
   
25\. invita gli Stati membri ad adottare un quadro legislativo nazionale per combattere le  
discriminazioni di cui sono vittime le persone LGBT e le coppie dello stesso sesso sulla  
base del loro orientamento sessuale o della loro identità di genere, e li esorta a garantire l'effettiva attuazione del quadro normativo UE esistente e della giurisprudenza della Corte di giustizia dell'Unione europea;  
   
**Emendamento 270**  
Monika Flašíková Beňová, Michael Cashman, Sophia in 't Veld, Raül Romeva i Rueda,  
Renate Weber, Ulrike Lunacek  
Proposta di risoluzione  
Paragrafo 25 bis (nuovo)  
25 bis. accoglie con favore le proposte della Commissione sulla competenza giurisdizionale e sulla legge applicabile agli effetti patrimoniali dei matrimoni e delle unioni registrate; ritiene tuttavia ingiustificata la scelta di due strumenti diversi (COM(2011)127 def. e COM(2011)126 def.) e di un approccio separato per le unioni registrate e per i matrimoni; ritiene che le stesse scelte in materia di competenza giurisdizionale e di legge (Causa C-147/08, Jürgen Römer contro Freie und Hansestadt Hamburg; causa C- 267/06; Tadao Maruko contro Versorgungsanstalt der deutschen Bühnen) applicabile debbano valere in entrambi i casi;  
   
**Emendamento 275**  
Renate Weber, Sophia in 't Veld, Gianni Vattimo, Sonia Alfano  
Paragrafo 26  
26\. invita gli Stati membri che si sono dotati di una legislazione relativa alle coppie dello stesso sesso a riconoscere le norme adottate da altri Stati membri e aventi effetti analoghi; coppie dello stesso sesso a riconoscere le norme adottate da altri Stati membri e aventi effetti analoghi e invita la  
Commissione a presentare una proposta a tal fine; ricorda l'obbligo per gli Stati membri di dare piena attuazione alla direttiva 2004/38/CE del Parlamento europeo e del Consiglio, relativa al diritto dei cittadini dell'Unione e dei loro familiari di circolare e di soggiornare liberamente nel territorio degli Stati membri, valida anche per le coppie dello stesso sesso e per i loro figli; accoglie con favore il fatto che sempre più Stati membri abbiano introdotto e/o adeguato le loro norme sulla coabitazione, sulle unioni civili e sul matrimonio per combattere le discriminazioni basate sull'orientamento sessuale subite dalle coppie di persone dello stesso sesso e dai loro figli e invita gli altri Stati membri a introdurre norme analoghe;  
   
**Emendamento 277**  
Monika Flašíková Beňová, Tatjana Ždanoka, Michael Cashman, Sophia in 't Veld, Raül  
Romeva i Rueda, Renate Weber, Ulrike Lunacek  
Paragrafo 26 bis (nuovo)  
26 bis. invita la Commissione a presentare una proposta per il pieno riconoscimento reciproco degli effetti di tutti gli atti di stato civile nell'Unione europea, compresi il riconoscimento giuridico del genere, i matrimoni e le unioni registrate, al fine di ridurre gli ostacoli discriminatori di natura giuridica e amministrativa per i cittadini che esercitano il loro diritto di  
libera circolazione;  
   
Compromise AM 19: Am 279 (partially covered - first sentence) <RepeatBlock-By><Members>Renate Weber, Sophia in ‘t Veld, Gianni Vattimo, Sonia Alfano, </Members>Am 281 <RepeatBlock-By><Members>Kinga Göncz  
   
   
Paragraph 26 a (new)  
   
26a. Calls on the Commission and the Council to intervene more forcefully against homophobia, violence and discrimination based on sexual orientation, including by calling Member States’ mayors and the police to protect freedom of expression and demonstration on the occasion of LGBT prides; calls on the Commission to use the results of the ongoing FRA survey to prepare an EU Roadmap for equality on grounds of sexual orientation and gender identity, with a view to adoption by 2014;  
   
   
**Emendamento 279**  
Renate Weber, Sophia in 't Veld, Gianni Vattimo, Sonia Alfano  
Paragrafo 26 bis (nuovo)  
26 bis. invita la Commissione e il Consiglio a svolgere un ruolo più attivo nella lotta contro l'omofobia, la violenza e la discriminazione basate sull'orientamento sessuale, anche chiedendo ai sindaci e alle forze di polizia degli Stati membri di proteggere la libertà di espressione e di manifestazione in occasione delle marce dell'orgoglio LGBT e garantendo loro il sostegno ufficiale; invita la Commissione a dare finalmente seguito alle ripetute richieste da parte del Parlamento europeo e delle ONG e a presentare urgentemente la tabella di marcia dell'UE per l'uguaglianza fondata sull'orientamento sessuale e dell'identità di genere da avviare entro il 2013;  
   
**Emendamento 280**  
Monika Flašíková Beňová, Tatjana Ždanoka, Michael Cashman, Sophia in 't Veld, Raül  
Romeva i Rueda, Renate Weber, Ulrike Lunacek  
Paragrafo 26 ter (nuovo)  
26 ter. deplora il fatto che le persone transgender siano ancora ritenute affette da malattie mentali in alcuni Stati membri; invita gli Stati membri a introdurre o a riesaminare le procedure di riconoscimento giuridico del genere sul modello dell'Argentina e a riesaminare le condizioni (inclusa la sterilizzazione forzata) previste per il riconoscimento giuridico del genere; invita la Commissione e l'Organizzazione mondiale della sanità a depennare i disturbi dell'identità di genere dall'elenco dei disturbi mentali e comportamentali e a garantire una riclassificazione non  
patologizzante in sede di negoziati relativi all'11a versione della classificazione internazionale delle malattie (ICD-11);  
   
**Emendamento 283**  
Kinga Göncz  
Paragrafo 26 quater (nuovo)  
26 quater. invita gli Stati membri a garantire la protezione efficace dei partecipanti agli eventi pubblici LGBT, incluse le marce dell'orgoglio, e ad assicurare che questi eventi possano svolgersi in modo legale;  
   
**Emendamento 284**  
Monika Flašíková Beňová, Michael Cashman, Sophia in 't Veld, Raül Romeva i Rueda,  
Renate Weber, Ulrike Lunacek  
Paragrafo 26 quinquies (nuovo)  
26 quinquies. invita gli Stati membri a garantire l'accesso all'occupazione, ai beni e ai servizi senza discriminazioni fondate sull'identità di genere, in conformità alla legislazione dell'UE1;  
   
**Emendamento 285**  
Monika Flašíková Beňová, Tatjana Ždanoka, Renate Weber  
  
**Emendamento 286**  
Monika Flašíková Beňová, Michael Cashman, Sophia in 't Veld, Raül Romeva i Rueda,  
Renate Weber, Ulrike Lunacek  
Paragrafo 26 sexies (nuovo)  
26 sexies. accoglie con favore l'avvio di un'indagine dell'Agenzia dell'Unione europea per i diritti fondamentali (FRA) che raccoglierà dati confrontabili sull'esperienza delle persone LGBT nell'Unione europea e in Croazia;  
   
**Emendamento 289**  
Monika Flašíková Beňová, Michael Cashman, Raül Romeva i Rueda  
Paragrafo 26 decies (nuovo)  
  
26 decies. invita gli Stati membri a dare piena attuazione alla direttiva 2003/86/CE del Consiglio relativa al diritto al ricongiungimento familiare, senza nessuna discriminazione fondata sul genere o sull'orientamento sessuale; ricorda che, secondo la giurisprudenza della Corte europea dei diritti dell'uomo, le coppie dello stesso sesso rientrano nella sfera della vita familiare1;  
   
27\. ritiene che i diritti fondamentali delle persone LGBT sarebbero maggiormente tutelati se esse avessero accesso a istituti giuridici quali coabitazione, partenariato registrato o  
matrimonio; plaude al fatto che sedici Stati membri offrono attualmente queste  
opportunità e invita gli altri Stati membri a prendere in considerazione tali istituti;