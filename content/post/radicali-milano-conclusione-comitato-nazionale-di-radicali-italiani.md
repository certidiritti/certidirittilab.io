---
title: 'RADICALI MILANO - CONCLUSIONE COMITATO NAZIONALE DI RADICALI ITALIANI'
date: Thu, 14 Jan 2010 13:08:06 +0000
draft: false
tags: [Senza categoria]
---

Domenica 10 gennaio si è concluso il Comitato nazionale di _Radicali italiani_, che ha ribadito l'impegno per la **presentazione di liste Bonino-Pannella in tutte le regioni** dove si voterà nel marzo 2010. [Leggi la mozione generale approvata](http://www.radicali.it/view.php?id=151429).

Anche in Lombardia si stanno ultimando le liste e si sta per partire con la raccolta delle quasi 20.000 firme, autenticate e certificate, necessarie a presentarle entro il 26 febbraio.

Abbiamo ora più che mai bisogno di una mano, della disponibilità di qualche ora, della presenza di un autenticatore. Leggi come puoi dare una mano, contattaci, fai girare l'informazione.

Siamo in campo per preparare l'alternativa, [ma serve l'aiuto di tutti](http://www.radicalimilano.it/public/iniziative/visua.asp?dati=ok&id=856).

I PROSSIMI APPUNTAMENTI "RADICALI MILANO"

15/01/2010, ore 16:30, Università Statale di Milano, via Conservatorio 7

[**A Milano Presentazione di** _**Amore Civile**_**, con De Filippis e Bilotta**](http://www.radicalimilano.it/public/iniziative/visua.asp?dati=ok&id=889)

15/01/2010, ore 20:40, Sede - Via Marchesi de Taddei 10 Milano

_**Il mercato del lavoro, il Nord, Pietro Ichino e i radicali**_

16/01/2010, ore 17, Sede - Via Marchesi de Taddei 10 Milano

**[Riunione costitutiva del comitato](http://www.radicalimilano.it/public/iniziative/visua.asp?dati=ok&id=892) per l'intitolazione di una via per gli Studenti iraniani, una via per la LIBERTÀ**

17/01/2010, ore 11.30, Spazio Tadini - Via Jommelli 24

[**Presentazione ufficiale**](http://www.radicalimilano.it/public/iniziative/visua.asp?dati=ok&id=890) **movimento** _**Primo Marzo 2010**_**, primo sciopero degli stranieri**

17/01/2010, ore 14, Sede - Via Marchesi de Taddei 10 Milano

[**Assemblea annuale**](http://www.radicalimilano.it/public/iniziative/visua.asp?dati=ok&id=895) **dell'**_**Associazione Radicali Senza Fissa Dimora**_**. Partecipa Marco Cappato**

18/01/2010, ore 21, Sede - Via Marchesi de Taddei 10 Milano

**Riunione dell'**_**Associazione Enzo Tortora - Radicali Milano**_  [**su regionali e prossime iniziative**](http://www.radicalimilano.it/public/iniziative/visua.asp?dati=ok&id=894)

21/01/2010, ore 21, Sede - Via Marchesi de Taddei 10 Milano

[**Riunione** _**Associazione Radicale Certi Diritt**_**i**](http://www.radicalimilano.it/public/iniziative/visua.asp?dati=ok&id=893)

__xxsup0

COMUNICATI

12/01/2010 **Englaro, Cappato: "**[**Ora Formigoni paghi i danni a Beppino Englaro**](http://www.radicalimilano.it/public/comunicati/visua.asp?dati=ok&id=1019)**"**