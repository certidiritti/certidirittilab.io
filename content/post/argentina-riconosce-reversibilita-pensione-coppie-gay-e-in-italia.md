---
title: 'ARGENTINA RICONOSCE REVERSIBILITA'' PENSIONE COPPIE GAY: E IN ITALIA?'
date: Wed, 20 Aug 2008 13:23:51 +0000
draft: false
tags: [Comunicati stampa]
---

**REVERSIBILITA’ DELLA PENSIONE IN CASO DI MORTE DEL PARTNER E’ CONSIDERATO DALL’ARGENTINA UN DIRITTO UMANO, E L’ITALIA RESTA A GUARDARE…SIAMO DIVENTATI NOI L’ARGENTINA E LORO L’ITALIA.**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

“Le ultime notizie provenienti dall’Argentina oggi ci dicono che il Dipartimento della Previdenza sociale, l’Inps locale, ha deciso di riconoscere la pensione di reversibilità al partner in caso di decesso di uno dei componenti la coppia, indipendentemente dal sesso di entrambi purchè dimostrino di aver vissuto insieme almeno cinque anni. Ormai anche l’Argentina ci ha superati in fatto di riconoscimento di diritti civili. Qui in Italia, invece, stiamo ancora a discutere se Famiglia Cristiana è più di destra o di sinistra perché comunque ha attaccato i Dico e i radicali nel Pd…. Tra recessione economica, diritti mancati, inflazione in rialzo, Ici si – Ici no, militari davanti ai parchi e alle scuole, sembriamo noi divenuti l’Argentina di non molto tempo fa e loro l’Italia della conquista economica e dei diritti”.