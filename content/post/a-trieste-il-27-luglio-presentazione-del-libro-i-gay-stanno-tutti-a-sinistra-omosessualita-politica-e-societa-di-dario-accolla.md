---
title: 'A Trieste il 27 luglio presentazione del libro ''I gay stanno tutti a sinistra. Omosessualità, politica e società'' di Dario Accolla'
date: Tue, 24 Jul 2012 09:05:45 +0000
draft: false
tags: [Movimento LGBTI]
---

Dialoga con l’autore Francesco Bilotta, Avvocato, Università di Udine, Avvocatura per i diritti LGBTI – Rete Lenford. Interviene, tra gli altri, Clara Comelli – Associazione Radicale Certi Diritti.

Il 27 luglio 2012 alle ore 20.00 presso la Terrazza Ausonia dello Stabilimento Balneare Riva Traiana 1 a Trieste, presentazione del libro 'I gay stanno tutti a sinistra. Omosessualità, politica e società' di Dario Accolla, Aracne Editrice 2012.

Dialoga con l’autore Francesco Bilotta, Avvocato, Università di Udine, Avvocatura per i diritti LGBTI – Rete Lenford

Intervengono al dibattito Sabrina Morena – consigliera provinciale di SEL, Clara Comelli – Associazione Radicale Certi Diritti, Pietro Faraguna – consigliere comunale del PD, Paolo Menis – consigliere comunale del Movimento 5 Stelle

Modera Davide Zotti – Presidente Circolo Arcobaleno Arcigay Arcilesbica Trieste

Viviamo in un tempo e in un luogo in cui le più tradizionali conquiste sociali e civili sono messe in discussione, mentre altre stentano a trovare un riconoscimento sul terreno della politica. La questione omosessuale è tra queste. La classe politica si è dimostrata poco interessata ad affrontare seriamente il dibattito sui diritti civili, sulle esigenze delle singole persone omosessuali e transessuali, dei loro legami affettivi, dell’omogenitorialità, ecc. Lo scopo di questo saggio è quello di fare chiarezza in tutti questi ambiti, con la pretesa di rispondere a una domanda fondamentale: chi sono i gay? Inoltre cosa si sa, realmente, delle persone della cosiddetta comunità GLBT?

Dopo la presentazione del libro la serata prosegue con un aperitivo lungo e con il dj set  curato da Acquolina.