---
title: 'Riforma delle adozioni e gay. Ecco cosa produce il fanatismo omofobo.'
date: Fri, 13 Mar 2015 09:21:48 +0000
draft: false
tags: [Diritto di Famiglia]
---

[![foto1](http://www.certidiritti.org/wp-content/uploads/2015/03/foto1.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/03/foto1.jpg)Ieri abbiamo assistito ad uno dei tanti spettacoli vergognosi del nostro Parlamento: il ricatto del centro destra è scattato e una parte consistente del PD, candidamente, ha ammesso che non si poteva inserire la possibilità per i single di adottare perchè se no si sarebbero favorite le coppie dello stesso sesso. "Ecco cosa produce il fanatismo omofobo" dice Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, che prosegue: "invece di avere una legge moderna, che avrebbe garantito a tutti e tutte la possibilità di adottare, si è preferita la solita posizione oscurantista per "non favorire" le coppie dello stesso sesso". Un antipasto di quello che succederà in occasione della discussione sulle unioni civili? E dove sta il coraggio, la dimensione europea, addirittura l'Italia campione sui diritti coem è stato sbandierato nel semetre europeo se su una norma di questo tipo si è fatto un passo indietro? "E' chiaro, finalmente, che il fanatismo antiomosessuale fa danni contro tutti e tutte?", si chiede infine Guaiana, che conclude: "Prepariamoci alla discussione in Senato: se ne vedranno delle belle, anzi delle brutte!"

Comunicato Stampa dell'Associazione Radicale Certi Diritti