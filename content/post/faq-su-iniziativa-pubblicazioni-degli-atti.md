---
title: 'FAQ SU INIZIATIVA PUBBLICAZIONI DEGLI ATTI'
date: Tue, 15 Apr 2008 16:58:45 +0000
draft: false
tags: [Affermazione Civile, FAQ, PUbblicazione degli atti, Senza categoria]
---

  

[**COSA DOBBIAMO FARE PER PARTECIPARE A QUESTA INIZIATIVA?**](#D1)

  

[**POSSO CHIEDERE LE PUBBLICAZIONI IN MODO CHE RIMANGANO UN FATTO PRIVATO? E' POSSIBILE CHE NE VENGANO INFORMATI I GIORNALI O I MEDIA SE SONO CONTRORI*?**](#D2)

[**UNA VOLTA RICHEISTE LE PUBBLICAZIONI, GLI ASPIRANTI CONIUGI SONO VINCOLATI TRA DI LORO? COSA SUCCEDEREBBE SE CI LASCIAMO? OPPURE SE VOGLIAMO SOPSARCI CON UN'ALTRA PERSONA?**](#D3)

[**UNA VOLTA INIZIATO L'ITER DEI RICORSI, RICEVEREMO COMUNICAZIONI A CASA?**](#D4)

[**COSA SUCCEDE SE PERDIAMO LA CAUSA? DOVREMO PAGARE LE SPESE PROCESSUALI? A QUANTO POTREBBERO AMMONTARE?**](#D4)

[**QUESTA E' UNA INIZIATIVA DI DISOBBEDIENZA CIVILE?**](#D5)

\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-

**COSA DOBBIAMO FARE PER PARTECIPARE A QUESTA INIZIATIVA?**

[Contattaci.](scrivici.html?view=category "Scrivi a CertiDiritti per partecipare all'iniziativa di Affermazione Civile") Uno degli avvocati che collaborano con la nostra associazione ti contatterà per spiegarti più praticamente cosa bisognerà fare. Genericamente, si tratterà di

1) prendere un appuntamento dal comune (basta uno dei componenti della coppia)

2) recarsi presso il comune di residenza di uno dei due aspiranti coniugi - in un determinato giorno concordato con voi e  l'ufficiale di stato civile

3) ricevere il documento con cui l'ufficiale di Stato Civile si rifiuta di Pubblicare gli atti

4) a questo punto l'avvocato che segue la vostra richiesta di pubblicazione procededrà con l'impugnazione, procedendo al primo grado di giudizio

Informazioni più dettagliate le trovi [qui](component/content/13.html?task=category&sectionid=4) .

**POSSO CHIEDERE LE PUBBLICAZIONI IN MODO CHE RIMANGANO UN FATTO PRIVATO? E' POSSIBILE CHE NE VENGANO INFORMATI I GIORNALI O I MEDIA SE SONO CONTRARI*?**

**Risposta breve** \- La richiesta di pubblicazione degli atti è un procedimento amministrativo che non richiede alcuna pubblicità fino all'emanzione delle pubblicazioni.  
Poichè certamente nessuna delle nostre richieste di pubblicazione sarà accettata, possiamo concordare con te il grado di riservatezza di cui senti il bisogno.

**Risposta lunga** \- La pubblicazione è l'esito di un procedimento amministrativo che comincia con l'istanza di pubblicazione. Non c'è ragione che questa fase iniziale del procedimento sia pubblica.  
Non troviamo giustificazione giuridica che induca il pubblico ufficiale interpellato a rendere pubblica l'dentità del cittadino che gli abbia rivolto l'istanza tutte le volte in cui la stessa non sia accolta. Attuerebbe un trattamento dei dati in violazione del principio di necessità come prescritto dal Codice per la protezione dei dati personali. In caso di violazioine della riservatezza, creando danni ai cittadini, il pubblico ufficiale può essere chiamato in giudizio a rispondere di tali danni.  
Per assicurarci la tutela della privacy degli aspiranti comiugi, dove richiesto accompagneremo la richiesta delle pubblicazioni con una specifica diffida a non rendere pubbliche le generalità degli istanti in caso di mancato accoglimento della stessa.  
  

  
**UNA VOLTA RICHIESTE LE PUBBLICAZIONI, GLI ASPIRANTI CONIUGI SONO VINCOLATI TRA DI LORO? COSA SUCCEDEREBBE SE CI LASCIAMO? OPPURE SE VOGLIAMO SOPSARCI CON UN'ALTRA PERSONA?**

**Risposta breve -** Poiché la richiesta di pubblicazione non verrà accolta, non ci sarà nessun obbligo in atto.

**Risposta lunga -** Per legge, quando ha luogo l'istanza di pubblicazione, si ha un vincolo equivalente a quello di una promessa di matrimonio scritta (art. 81 cc.). L'obbligo di risarcimento dei danni sostanzialmente scatta:  
a) per un rifiuto ingiustificato;  
b) limitatamente alle spese fatte e alle obbligazioni contratte a causa della promessa.  
  

**UNA VOLTA INIZIATO L'ITER DEI RICORSI, RICEVEREMO COMUNICAZIONI A CASA?**

Le comunicazioni saranno fatte al domicilio eletto, cioè quello dell'avvocato che le rappresenterà in giudizio.  

**COSA SUCCEDE SE PERDIAMO LA CAUSA? DOVREMO PAGARE LE SPESE PROCESSUALI? A QUANTO POTREBBERO AMMONTARE?**

**Risposta breve -** Generalmente, le uniche spese vive per la coppia saranno esclusivamente le spese di cancelleria, per un totale approssimativo di 70 Euro. E’ raro, per quanto possibile, che la coppia incorra in spese maggiori (vedi più sotto, punto 4).

**Risposta lunga -** Le spese processuali consistono in

1) _L'onorario dell'avvocato:_ non verrà richiesto, in segno di artecipazione degli avvocati della Rete Lenford alla causa di questa iniziativa.

2) _Le spese di cancellaria_ (all'incirca 70 Euro)

3) _E' possibile una ulteriore costo variabile_ di circa 5 Euro, nell'eventualità che il giudice disponga la notificazione della controparte.

4) _Le spese della procedura._ Pur trattandosi di un provvedimento di volontaria giurisdizione (è il giudice a decidere se il contraddittorio deve essere integrato dalla presenza della controparte), per quanto inverosimile, può accadere che il giudice ritenga di chiedere alla coppia di farsi carico delle spese della procedura. E’ accaduto solamente in un tribunale, dove la stessa giudice ha imposto a due coppie di farsi carico ciascuna di una spesa di 2000 Euro. Se da un lato questa decisione è stata impugnata, dall’altro l'eventuale onere di queste spese sarebbero a carico della coppia.

**QUESTA E' UNA INIZIATIVA DI DISOBBEDIENZA CIVILE?**

**Rispsota breve -** Propriamente si tratta di una iniziativa di Consapevolezza e Affermazione Civile. Molti parlano impropriamente di Disobbedienza, poiché questa iniziativa contrasta evidentemente il senso comune.

**Risposta lunga -** La legge italiana non solo non vieta il matrimonio tra persone dello stesso sesso, ma prevede esplicitamente che chiunque può chiedere la pubblicazione degli atti e ha diritto ad un documento ufficiale da poter impugnare.

Per essere precisi, l'unica disobbedienza civile sarà praticata dagli avvocati della Rete Lenford che, decidendo di non richiedere nessun onorario, contrasteranno un loro specifico obbligo professionale.

[![cosa_sono_le_pubblicazioni_degli_atti.png](http://www.certidiritti.org/wp-content/uploads/2008/04/cosa_sono_le_pubblicazioni_degli_atti.png "cosa_sono_le_pubblicazioni_degli_atti.png")](affermazione-civile/pubblicazione-degli-atti.html?task=view) [![domande frequenti](http://www.certidiritti.org/wp-content/uploads/2008/04/faq.png "domande frequenti")](affermazione-civile/faq.html?task=view) [![i_fondamenti_giuridici.png](http://www.certidiritti.org/wp-content/uploads/2008/04/i_fondamenti_giuridici.png "i_fondamenti_giuridici.png")](affermazione-civile/fondamenti-giuridici.html)