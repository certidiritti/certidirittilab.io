---
title: 'LE ASSICURAZIONI  PIU'' ALL''AVANGUARDIA DEI POLITICI'
date: Sun, 27 Jul 2008 12:22:55 +0000
draft: false
tags: [Comunicati stampa]
---

"Le nostre assicurazioni sono più serie e affidabili dei politici italiani". Lo dichiara Sergio Rovasio, segretario dell'associazione radicale Certi Diritti, dopo che le Assicurazioni Generali, secondo quanto riportava ieri il quotidiano Il Gazzettino, hanno risarcito un cittadino omosessuale italiano, rimasto "vedovo" del suo compagno ottantenne francese "Viviamo in un Paese" continua Rovasio "in cui la tutela giuridica delle formazioni sociali diverse da un'unione eterosessuale è considerata inesistente dalla politica, ma è ormai voluta dall'intera società civile. Tuttavia, decine di migliaia di coppie lesbiche e gay ne stanno facendo le spese senza che i nostri politici muovano un dito per assicurare loro diritti considerati basilari in gran parte dei paesi dell'Unione Europea".

Certi Diritti continuerà con ancora più forza nella sua azione di affermazione civile, che si propone di portare nelle aule dei tribunali italiani i rifiuti alle pubblicazioni di matrimonio da parte degli ufficiali di Stato civile dei vari Comuni alle coppie omosessuali che chiederanno di essere sposate. "Non si tratta" puntualizza il segretario dell'associazione radicale "né di disobbedienza civile, né di un'azione avventata mirata a fare clamore, bensì di una battaglia per la civiltà dove, proprio perché i politici non sono in grado di rispondere ai nostri bisogni e alle nostre istanze, garantendo l'uguaglianza di tutti i cittadini di fronte alla legge – come vuole la Costituzione –, i cittadini omosessuali sono chiamati in causa per rivendicare i loro diritti fondamentali come individui".

Una quindicina di coppie lesbiche e gay italiane, intanto, sta depositando i ricorsi al diniego delle pubblicazioni ai rispettivi tribunali ordinari, con l'obiettivo di vedersi riconosciuto da un giudice il diritto a contrarre matrimonio secondo i principi di uguaglianza e di non discriminazione, garantiti dalla legge.

**Per ulteriori informazioni:**

Certi Diritti

Tel: (+ 39) 334-8429527 – (+ 39) 337-798942

[www.certidiritti.it](http://www.certidiritti.it/) – [ufficiostampa@certidiritti.it](mailto:ufficiostampa@certidiritti.it)