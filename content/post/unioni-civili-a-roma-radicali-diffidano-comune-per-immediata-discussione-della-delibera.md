---
title: 'Unioni civili a Roma, Radicali diffidano comune per immediata discussione della delibera'
date: Wed, 21 Nov 2012 16:06:05 +0000
draft: false
tags: [Diritto di Famiglia]
---

Roma, 21 novembre 2012

Comunicato di Riccardo Magi, segretario di Radicali Roma, e Sergio Rovasio, Associazione Radicale Certi Diritti

"Diffidiamo l'amministrazione comunale dal continuare a violare lo statuto e attiveremo tutte le azioni necessarie, in tutte le sedi opportune, per il ripristino della legalità statutaria. Ci rivolgeremo al Tar e al difensore civico regionale fino a chiedere il commissariamento ad acta per omissione o ritardo di atti obbligatori, ai sensi dell'art. 136 del Testo unico degli enti locali", lo annunciano in una nota il segretario di Radicali Roma Riccardo Magi e Sergio Rovasio dell'Associazione Radicale Certi Diritti.

"Oggi - continuano - invieremo formale diffida al presidente dell'Assemblea capitolina Pomarici, al sindaco Alemanno, al segretario generale Iudicello e per conoscenza a tutti i capigruppo, affinché si provveda all'immediata calendarizzazione e discussione della delibera di iniziativa popolare sul "Riconoscimento delle unioni civili e sostegno alle nuove forme familiari", nel rispetto dello statuto del comune di Roma. E' infatti in atto una patente violazione della norma che prescrive la discussione delle delibere di iniziativa popolare entro sei mesi dal deposito (art. 8 Statuto)".

La delibera, sottoscritta da oltre 7000 cittadini romani, è stata depositata il 17 maggio scorso, ma ancora non è stata inserita nell'ordine dei lavori e nulla è stato comunicato al comitato promotore per garantire la facoltà di illustrare la proposta al Consiglio.

"La calendarizzazione e la discussione delle delibere di iniziativa popolare non sono atti rimessi alla discrezionalità del presidente dell'assemblea o dei gruppi consiliari in base a considerazioni di opportunità o convenienza politica - spiegano Magi e Rovasio - sono invece atti obbligati dallo statuto in tempi ben precisi e non differibili. Fare carta straccia dello statuto, in particolare rispetto al diritto di partecipazione dei cittadini alla vita pubblica, è un'abitudine di questa assemblea che oggi si conferma ancora una volta. Porteremo avanti questa battaglia di legalità fino a quando i consiglieri eletti dai cittadini romani non rispetteranno queste regole fondamentali".

"La delibera, che porterebbe Roma a riconoscere diritti alle famiglie non matrimoniali come hanno già fatto Torino, Napoli, Milano e altre città italiane, non prevede l'istituzione di un semplice registro delle unioni civili che senza politiche e programmi di intervento ha un valore simbolico. Bensì chiede a Roma Capitale di parificare le coppie sposate e quelle conviventi, cioè le famiglie fondate sul matrimonio e le famiglie di fatto, negli ambiti di competenza comunale garantendo ad esempio parità di accesso a tutte le attività e servizi comunali. Il Regolamento per il riconoscimento delle unioni civili che viene proposto con la delibera impegna dunque Roma Capitale a superare ogni forma di discriminazione nei confronti delle famiglie di fatto", concludono i radicali.

Info: 3338042937