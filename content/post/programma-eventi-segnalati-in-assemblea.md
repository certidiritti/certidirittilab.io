---
title: 'PROGRAMMA EVENTI SEGNALATI IN ASSEMBLEA'
date: Thu, 01 Apr 2010 11:22:23 +0000
draft: false
tags: [Senza categoria]
---

Di seguito il programma degli eventi segnalati in assemblea. Potete ritrovare questi eventi nel nostro calendario.

a.       **Giovedì 1 Aprile 2010**, "Luci contro la pedofilia" dalle 19 in piazza Duomo - fiaccolata contro gli abusi clericali sui minori resi noti in queste settimane, trovate il documento politico della fiaccolata qui [http://www.radicalimilano.it/public/iniziative/visua.asp?dati=ok&id=950](http://www.radicalimilano.it/public/iniziative/visua.asp?dati=ok&id=950)

b.       **Sabato 10 Aprile,** la sera, inaugurazione a Saronno della mostra contro l’omofobia, con i cartelloni che erano stati preparati lo scorso anno dal comitato “Milano Contro l’Omofobia” lanciato da Certi Diritti.

c.        **Domenica 11 Aprile,** direttivo nazionale di Certi Diritti, allargato a forma di assemblea. Per chi fosse interessato a partecipare, ci riuniremo dalle ore 10:30 fino alle ore 16:30 circa presso la solita sede dei radicali a Milano, "Enzo Tortora": via Malachia Marchesi de' Taddei 10 (citofono "radical")

c.      **Domenica 18 Aprile** Certi Diritti è invitata a Como per prendere parte di una  tavola rotonda sulla Corte Costituzionale: presenti, oltre a noi, anche Francesco Bilotta, Anna Paola Concia ed altri personaggi.

d.      **Mercoledì 28 Aprile**, presso il Circolo Arci50, nei pressi di piazzale Lodi, presentazione del libro “La Famiglia Fantasma” di Gian Mario Felicetti, insieme al libro “Una famiglia normale” di Stefano Bolognini

e.      Si prevede una iniziativa per **Domenica 16 Maggio** a Milano in occasione della Giornata Mondiale Contro l’Omofobia. Come Certi Diritti abbiamo già adderito alle iniziative previste sempre per quel giorno, a Bergamo.