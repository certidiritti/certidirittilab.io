---
title: 'Matrimonio gay: la parola alla Corte Costituzionale'
date: Tue, 21 Apr 2009 13:03:31 +0000
draft: false
tags: [Senza categoria]
---

Un annetto e poco più di esistenza e l'Associazione Radicale Certi Diritti assieme alla Rete Lenford mettono a segno un primo colpo: il matrimonio fra omosessuali diventa materia costituzionale.

Affermazione Civile, la campagna condotta dalle due associazioni, è basata sulla consapevolezza dei propri diritti e su una notevole dose di coscienza politica di alcune coppie di omosessuali. Grazie alla loro determinazione a all'aiuto di Certi Diritti e di Rete Lenford, ha preso corpo un movimento di rivendicazione del diritto al matrimonio che passa attraverso l'iter processuale. In effetti, pochi di noi aspettavano che in un tempo così breve si potesse arrivare ad investire del tema addirittura la Corte Costituzionale. Invece le solide e convincenti argomentazioni del Prof. e avvocato Francesco Bilotta devono aver convinto la Corte del Tribunale di Venezia che in effetti il rifiuto delle Pubblicazioni di Matrimonio da parte dello Stato Civile si configurava con una probabile materia di rilevanza costituzionale, con preciso riguardo alla disuguaglianza e alla discriminazione.

Se non intercorrono fatti nuovi, è ragionevole pensare che entro un anno e mezzo circa ci potrà essere un pronunciamento di importanza storica. Potrà accadere che la Corte si pronunci in modo favorevole, determinando l'interpretazione delle disposizioni del codice in modo che gli omosessuali siano ammessi al matrimonio.

Oppure potrà pronunciarsi contro e affermare che gli omosessuali non hanno diritto di accedere al matrimonio. Oppure potrà accadere che la Corte dichiari la sua incompetenza richiamando il Parlamento alla necessità di legiferare in modo chiaro sul tema.

Dal momento del rinvio alla Corte Costituzionale del procedimento si è definitivamente aperta una porta che prima era chiusa. Dal 3 aprile, infatti,  la comunità nazionale è di fronte al problema e non può più fare finta che questo non ci sia. La testimonianza più evidente è lo spazio che il Sole 24 ore dedica oggi all'ordinanza ed ai pareri di insigni costituzionalisti interpellati sul tema. Se non si perverrà ad un verdetto favorevole, o se il Parlamento legifererà contro il diritto degli omosessuali di contrarre matrimonio, si apriranno nuovi scenari politici, nei quali ancor più rilevanza e spazio avrà la battaglia di civiltà di Affermazione Civile, che dall'Italia potrà finalmente approdare all'Alta Corte di Giustizia Europea.

Vedremo come proseguirà. Intanto possiamo registrare il commento di Clara Comelli, Presidente dell'Associazione Radicale Certi Diritti che, nell'apprendere la notizia con entusiasmo e fiducia, "spera che la Consulta accolga la richiesta del Tribunale veneziano in ossequio al principio di uguaglianza e di non discriminazione sancito dalla nostra Carta fondamentale"."L'Associazione - prosegue la Presidente - continua a rimanere vicino a tutte le coppie che hanno aderito all'iniziativa di Affermazione Civile e si augura che altre se ne aggiungano per rafforzare questa importante battaglia di civiltà".

Sergio Rovasio, Segretario dell'Associazione, ha dichiarato: "Per noi e per la comunità lgbt è un momento storico. Il fatto che un Tribunale abbia rinviato alla Consulta la decisione già di per sè è un grande passo avanti. Ci auguriamo che la Corte riconosca la grave discriminazione di cui sono vittime le coppie gay che in Italia  desiderano sposarsi".

Infine, si esprime anche Saveria Ricci, Presidente di Rete Lenford: "L'ordinanza del Tribunale di Venezia è una tappa importante della battaglia che Avvocatura per i diritti LGBT - Rete Lenford (www.retelenford.org) sta conducendo per il rispetto delle persone omosessuali nel nostro Paese. Crediamo fermamente che escludere le coppie dello stesso sesso dalle tutele che discendono dal matrimonio, sia contrario alla nostra Costituzione e agli impegni che l'Italia ha assunto entrando nell'Unione  europea. Confidiamo che la Corte costituzionale prenda in considerazione le argomentazioni del Tribunale di Venezia, che brillano per accuratezza e per rigore giuridico".

Rimandando ad un prossimo post per l'approfondimento sui contenuti dell'ordinanza del Tribunale di Venezia, faccio solo un appello affinchè altre coppie di uomini e donne omosessuali si uniscano alla campagna di Affermazione civile, per darle nuova forza e sostenere questo luminoso esempio di civiltà.