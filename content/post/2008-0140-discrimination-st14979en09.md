---
title: '2008-0140-Discrimination-ST14979.EN09'
date: Fri, 06 Nov 2009 13:23:57 +0000
draft: false
tags: [Senza categoria]
---

  

  

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 6 November 2009

Interinstitutional File:

2008/0140 (CNS)

14979/09

LIMITE

SOC 622

JAI 728

MI 393

  

  

  

  

  

OUTCOME OF PROCEEDINGS

from :

The Working Party on Social Questions

on :

22 October 2009

No. prev. doc. :

14009/09 SOC 567 JAI 645 MI 362

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

**I.       INTRODUCTION**

At its meeting on 22 October 2009, the Working Party on Social Questions continued its examination of the above proposal. The discussion was based on a set of Presidency drafting suggestions[\[1\]](#_ftn1) focusing on the disability provisions (Articles 4, 4a, 4b and 7) and the implementation calendar (Article 15). The Chair noted that the recitals as well as the rest of the articles would also need further discussion at a later date.

Written contributions from MT[\[2\]](#_ftn2) and NL[\[3\]](#_ftn3) had been distributed immediately prior to the meeting, and DK[\[4\]](#_ftn4) also recalled its earlier written submission.

**II.      SUMMARY OF THE DISCUSSION**

**1.       The disability provisions**

The Presidency's text introduced a distinction between "access" (Article 4) and "accessibility" (Article 4a) in respect of "reasonable accommodation", and would provide for separate implementation deadlines for ensuring accessibility regarding buildings, facilities and infrastructure, depending on whether they were new or old, the deadlines suggested being _10 years_ (for the new) and _20 years_ (for the old).

Many delegations broadly welcomed the Presidency's text as a step in the right direction; however, the discussion also revealed the need for further clarification.

Calling for stronger provisions to ensure the application of the rights of persons with disabilities, PT entered a scrutiny reservation. AT, SK and UK also appealed for ambitious disability provisions.

BG, DK, DE, LT, MT, PL and UK recalled their concerns in respect of the cost implications of the provisions, DE entering a scrutiny reservation.

BE called for the inclusion of _a definition_ of a person with a disability in the text.

**2.           The scope**

Many delegations (including BE, DE, IE, IT, LV, SK, FI, UK) urged the need to define the scope as clearly as possible, and called for clarification, in particular, in respect of the obligations created, including anticipatory requirements (DE, IE), and of the links with legislation on transport and passenger rights (IT and FI), and expressed concern regarding the inclusion of the design and manufacture of goods (BE, FI and UK) within the scope. Delegations also expressed concern and asked for clarification regarding the proposed inclusion of the physical environment (NL, SK, FI, UK).

  

NL supported by EL, LV and MT expressed the view that the scope of the Directive (Article 3) would have to be clarified before progress could be made on the disability provisions. DE called for the definitions to be clarified.

As a possible compromise, LU suggested that all existing buildings, facilities and infrastructure be excluded from the scope of the Directive, while it would apply immediately to new buildings. EL, LV and LT supported this idea. BG, DK and MT also saw potential merit in this approach.

**3.       Article 4 (Reasonable accommodation for persons with disabilities)**

EE, IT, NL, PT and SI favoured aligning the text with the definition contained in the United Nations Convention on the Rights of Persons with Disabilities (UNCRPD), NL also alluding to Directive 2000/78/EC[\[5\]](#_ftn5) and making a written suggestion:

"\[R\]easonable accommodation means necessary and appropriate modification and adjustments, where needed in a particular case, to ensure to persons with disabilities access on an equal basis with others".

Also calling for a more general wording, UK warned against sending a negative political signal. NL and UK recalled the merits of the wording tabled by the CZ Presidency[\[6\]](#_ftn6).

UK explained that housing was treated in a specific way in respect of reasonable accommodation under the system applied in the United Kingdom.

BE also saw a need to clarify "reasonable accommodation".

  

DK suggested:

"Reasonable accommodation can also be obtained by schemes in Member States consisting of other measures specifically adapted to the individual needs of a person"[\[7\]](#_ftn7).

DE, LT and FI supported the inclusion of such an addition. LV and AT entered scrutiny reservations.

**\- The distinction between "access" and "accessibility" (Articles 4, 4a and 7(4))**

BE, CZ, DE, EL, FR, IT, and LV saw a need to clarify the meaning of "accessibility" as opposed to "access". IT pointed out that Article 3 currently only referred to "access" and that "accessibility" needed to be included there in an appropriate way.

CZ, ES and IT suggested reordering the text so as to place the general provisions on accessibility contained in Article 4a _before_ the more specific provisions on reasonable accommodation contained in Article 4. NL raised the question as to whether such reordering would be consistent with the UNCRPD.

Several delegations (CZ, EL, DK, IT, PT, AT, FI and UK) questioned the suggestion that the provisions for the defence of rights set out in Article 7(1-2) should not apply to Article 4a.

LU saw merit in the idea of promoting the use of conciliation procedures.

The Commission representative expressed hesitation in respect of the approach suggested by the Presidency, whereby the measures to ensure accessibility (Article 4a) could not be challenged by an individual person through judicial procedures (Article 7(4)).

**4.       Article 4b(4) (Detailed standards or specifications)**

DE, EE, IE, NL, FI and UK suggested that the hierarchy between specific (national and/or sectoral) rules on the one hand, and the general provisions set out in the Directive on the other, be clarified.

NL tabled a written suggestion[\[8\]](#_ftn8), which was supported by FI.

The Commission representative explained that if the Member States _together_ adopted more specific rules at the European Union level, the Directive would no longer apply.

**5.       Article 15 (Implementation)**

For delegations' positions on the suggested implementation dates, see footnotes to the attached text.

EE supported by EL, LV and HU pointed out the need to clarify the provisions in respect of "existing buildings", so as to accommodate the whole building process from planning to construction, for example, by means of a transitional period. BG, EE, EL, IT and LV also suggested a more differentiated approach to different kinds of buildings, so as to take into account the specific situations in different Member States.

PT also asked for clarification in respect of the link with Article 7(4), citing the possibility that a building to be constructed now could become illegal in the future.

IE wished to know what had to be accomplished by the deadlines given: the taking of measures, or, for example, the implementation of standards.

o

o             o

  

All delegations have general scrutiny reservations on the proposal as a whole and scrutiny reservations on the Presidency's drafting suggestions contained in doc 14009/09, as well as on all other suggestions tabled at the meeting.

Delegations' positions on the proposal as a whole are summarised in doc. 14008/09.

For further details, see footnotes to the attached text.

**III.    ANY OTHER BUSINESS**

The Commission representative informed delegations of the state of play in respect of the proposals for the conclusion of the United Nations Convention on the Rights of Persons with Disabilities (UNCRPD) and its Optional Protocol, which are being handled in the Council's Working Party on Human Rights (COHOM)[\[9\]](#_ftn9).

The Presidency gave information regarding the third Equality Summit, to be held in Stockholm on 16-17 November 2009[\[10\]](#_ftn10).

**IV.      CONCLUSIONS**

The Chair informed delegations that a new set of Presidency drafting suggestions would be tabled in due course for discussion by the Working Party on 9 November 2009.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

_Article 4[\[11\]](#_ftn11)  
Reasonable accommodation for persons with disabilities_

In order to guarantee compliance with the principle of equal treatment in relation to persons with disabilities, reasonable accommodation shall be provided within the areas set out in Article 3(1), unless this would impose a disproportionate burden.

Reasonable accommodation means measures needed in a particular case to meet the specific needs of a person with a disability in order to ensure access on an equal basis with others[\[12\]](#_ftn12).

Article 4a (new)Accessibility for  persons with disabilities

**1.**[\[13\]](#_ftn13) Notwithstanding Article 4, Member States shall take the necessary and appropriate measures to ensure accessibility for persons with disabilities, on an equal basis with others, within the areas set out in Article 3[\[14\]](#_ftn14).

**2.** Such measures shall include the identification and elimination of obstacles and barriers to accessibility, \[including with regard to the physical environment[\[15\]](#_ftn15) and to information and communication technology and systems\][\[16\]](#_ftn16).

**3.** Such measures should not impose a disproportionate burden, nor require fundamental alteration or the provision of alternatives[\[17\]](#_ftn17).

Article 4b (new)  
Common Provisions Regarding Reasonable Accommodation and Accessibility

1.       For the purposes of assessing[\[18\]](#_ftn18) whether measures necessary to comply with Articles 4 and 4a would impose a disproportionate burden, account shall be taken, in particular, of[\[19\]](#_ftn19):

  

a)           the size[\[20\]](#_ftn20) and resources of the organisation or enterprise,

b)           the nature of the organisation or enterprise,

c)           the estimated cost, and

d)           the possible benefits[\[21\]](#_ftn21) of increased access for persons with disabilities.

The burden shall not be deemed disproportionate when it is sufficiently remedied by measures existing within the framework of the equal treatment policy of the Member State concerned.

2.         Articles 4 and 4a shall not apply to the design and manufacture of goods[\[22\]](#_ftn22).

3.       Articles 4 and 4a[\[23\]](#_ftn23) shall not require significant[\[24\]](#_ftn24) structural changes to buildings or infrastructures[\[25\]](#_ftn25) which are protected under national rules on account of their historical, cultural or architectural value.

  

4.       This Directive shall be without prejudice[\[26\]](#_ftn26) to the provisions of Community law or national rules providing for detailed standards or specifications on the accessibility of particular goods or services, including public transport[\[27\]](#_ftn27), as long as such rules do not restrict the application of Articles 4 and 4a.

Article 7  
Defence of rights

1.       Member States shall ensure that judicial and/or administrative procedures, including where they deem it appropriate conciliation procedures, for the enforcement of obligations under this Directive are available to all persons who consider themselves wronged by failure to apply the principle of equal treatment to them, even after the relationship in which the discrimination is alleged to have occurred has ended.

2.       Member States shall ensure that associations, organisations or other legal entities, which have, in accordance with the criteria laid down by their national law, a legitimate interest in ensuring that the provisions of this Directive are complied with, may engage, either on behalf or in support of the complainant, with his or her approval, in any judicial and/or administrative procedure provided for the enforcement of obligations under this Directive.

3.       Paragraphs 1 and 2 shall be without prejudice to national rules relating to time limits for bringing actions as regards the principle of equality of treatment.

4.       (new) Paragraphs 1 and 2 shall not apply to obligations arising from Article 4a. Member States may determine that obligations arising from that provision shall be implemented through administrative or conciliation procedures[\[28\]](#_ftn28).

Article 15[\[29\]](#_ftn29)  
Implementation

1.       Member States shall adopt the laws, regulations and administrative provisions necessary to comply with this Directive by …. at the latest \[X years after adoption\]. They shall forthwith inform the Commission thereof and shall communicate to the Commission the text of those provisions.

When Member States adopt these measures, they shall contain a reference to this Directive or be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.

2.       In order to take account of particular conditions, Member States may, if necessary, establish that

a)[\[30\]](#_ftn30) the obligation to provide reasonable accommodation as set out in Article 4 has to be complied with by … at the latest \[X years after adoption\], and

b)      the obligation to ensure accessibility as set out in Article 4a has to be complied with by … at the latest \[10 years after adoption\] regarding new buildings[\[31\]](#_ftn31), facilities and infrastructure and by \[20[\[32\]](#_ftn32) years after adoption\] regarding existing buildings, facilities and infrastructure.

  

Member States wishing to use any of these additional periods shall inform the Commission at the latest by the date set down in paragraph 1 giving reasons. Member States shall also communicate to the Commission by the same date an action plan[\[33\]](#_ftn33) laying down the steps to be taken and the timetable for achieving the gradual implementation of Article 4a. They shall report on progress every two years starting from this date.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) Doc. 14009/09.

[\[2\]](#_ftnref2) Doc. 14786/09.

[\[3\]](#_ftnref3) Doc. 14781/09.

[\[4\]](#_ftnref4) Doc. 12893/09.

[\[5\]](#_ftnref5) See note from NL (doc.14781/09).

[\[6\]](#_ftnref6) See doc. 10072/09.

[\[7\]](#_ftnref7) See doc. 12893/09.

[\[8\]](#_ftnref8) Doc. 14781/09. NL also deleted the words "or national rules" from its suggestion.

[\[9\]](#_ftnref9) See docs. 12892/2/08 REV 2, 12892/08 ADD 1 REV 1, 8321/09, 9277/09 and 12786/09.

[http://www.se2009.eu/en/meetings\_news/2009/11/16/equality\_summit](http://www.se2009.eu/en/meetings_news/2009/11/16/equality_summit)

[\[11\]](#_ftnref11) ES entered a scrutiny reservation on Article 4. DE saw a need to clarify the obligations created, including anticipatory requirements and measures with respect to persons living in sheltered or non-sheltered accommodation respectively.

[\[12\]](#_ftnref12) UK suggested adding "as far as reasonably possible".

[\[13\]](#_ftnref13) IE desired to know whether and how the adequacy or the onerousness of measures taken by the Member States to ensure accessibility would be open to challenge. IE also hoped for greater clarity in respect of the specific obligations created by the provisions.

[\[14\]](#_ftnref14) CZ suggested "...to ensure equal access of persons with disabilities within the areas set out in Article 3". DE reiterated its call for the obligations arising from the provisions to be clarified, including with reference to anticipatory requirements and the delineation of the scope. BE and LV also wished to see the scope clarified. FI entered a scrutiny reservation on Article 4a, particularly in respect of the scope and the question as to whether transport legislation was covered; IT concurred that the scope required clarification in this respect, and pointed out that "transportation" was mentioned in Article 9 of the UNCRPD.

[\[15\]](#_ftnref15) SK, FI and UK asked for the term "physical environment" to be clarified, FI asking how it related to the "buildings, facilities and infrastructures" mentioned in Article 15(2)(b).

[\[16\]](#_ftnref16) FI entered a scrutiny reservation on the links between the disability provisions and legislation in the area of transport and passenger rights. BE, FI and UK requested clarification in respect of the link with Article 4b(2), and the question as to whether the design and manufacture of goods would be covered. NL suggested deleting the words in square brackets, and preferred to see the scope clarified in Article 3 (see doc. 14781/09). LV favoured retaining these words, but explaining their meaning more clearly. CZ and IE supported the approach suggested by NL.

[\[17\]](#_ftnref17) BE, ES, IT and AT were unable to accept narrowing the scope by means of an exemption for new buildings. Alternatively, AT and UK suggested deleting "nor require fundamental alteration or the provision of alternatives ". CION explained that neither "fundamental alteration" nor "the provision of alternatives" would arise in the context of new buildings.

[\[18\]](#_ftnref18) LT suggested that the Member States should be able to assess whether burdens imposed were proportionate or not with respect to the burden placed on public finances; this delegation suggested adding a new penultimate sub-paragraph in Article 4(b)(1), to read as follows: "the Member States, taking into account their national traditions and practices, their legal and administrative systems as well as the situation of their public finances, shall, in accordance with the criteria set out in this paragraph, determine whether the provisions of this Directive impose a disproportionate burden". NL had also made a written suggestion on this issue (see doc. 14781/09).

[\[19\]](#_ftnref19) NL suggested reintroducing "the life span of the goods and services" in Article 4b(1) (see doc. 14781/09). FI and UK did not see a need to include these words.

[\[20\]](#_ftnref20) PT raised the question as to who would assess the "size" and the "nature" of the organization or enterprise (Article 4b(a) and (b)). CION explained that the Directive only fixed the objectives to be attained, and that the Member States were free to decide how to implement it; one option was to issue guidelines in legislation, even if in principle it would ultimately be for the judge to decide.

[\[21\]](#_ftnref21) UK suggested clarifying the meaning of "the possible benefits".

[\[22\]](#_ftnref22) SK was unable to accept Article 4b(2) on the grounds that it weakened the text. Supporting the Presidency's text, UK warned against the imposition of a vast range of requirements on design and manufacturing. CY pointed out that universal design was still alluded to in Recital 19d. DE, PT, AT and FI called for clarification.

[\[23\]](#_ftnref23) HU suggested making a cross-reference to Article 4a only, since Article 4 concerned reasonable accommodation and thus could not entail structural changes.

[\[24\]](#_ftnref24) DE asked for the meaning of "significant" in relation to Recital 19b and the general principle of proportionality to be clarified. CION explained that the aim was to ensure that the main rule -- that there should be reasonable accommodation but not with a disproportionate burden -- could be applied to historic buildings also. IT pointed out that even new buildings could have architectural value.

[\[25\]](#_ftnref25) NL made a written suggestion (see doc. 14781/09). LV also saw merit in the NL approach. NL expressed the view that public areas should not be considered as constituting infrastructure. FR, IT, PT and AT took the view that the provisions in question were best covered in Recital 19b and that Article 4b(3) should be deleted. UK also had misgivings about this paragraph. FI, however, preferred maintaining the provision in the Article. SI entered a scrutiny reservation.

[\[26\]](#_ftnref26) IE suggested replacing "shall be without prejudice" with "shall not apply".

[\[27\]](#_ftnref27) FI entered a scrutiny reservation, with particular reference to rules on accessibility in transport; EE and DE also highlighted the issue of existing rules on transport. EE and UK wanted to know whether there were areas of public transport not yet covered by EC legislation.

[\[28\]](#_ftnref28) CZ, DE, ES, FR, LV, HU, SK and FI entered scrutiny reservations.

[\[29\]](#_ftnref29) LT, HU, MT, PL and RO maintained scrutiny reservations on the implementation dates. NL reiterated its call for a sufficiently long implementation period.

[\[30\]](#_ftnref30) CZ and IT called for the deletion of 15(2)(a), as they saw no need to provide for a transitional period for "reasonable accommodation". MT and NL requested examples of provisions concerning reasonable accommodation whose implementation it would be necessary to postpone. CION also wondered whether the period already provided for in Article 15(1) might not already allow enough time for necessary adjustments to take place.

[\[31\]](#_ftnref31) BE, CZ and AT suggested removing new buildings from 15(2)(b).

[\[32\]](#_ftnref32) AT and PT felt that 20 years might be too long. AT entered a reservation. CZ, EE, DK, LT and FI, for their part, saw 20 years as being potentially too short. HU and IT entered scrutiny reservations on 20 years.

[\[33\]](#_ftnref33) FI asked for "action plans" to be clarified. MT and UK affirmed the need for reporting provisions of some description, MT questioning whether two years was a sufficient interval. CION suggested aligning the reporting procedures on those contained in the UNCRPD.