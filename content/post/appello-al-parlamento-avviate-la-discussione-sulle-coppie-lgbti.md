---
title: 'APPELLO AL PARLAMENTO: AVVIATE LA DISCUSSIONE SULLE COPPIE LGBTI'
date: Mon, 18 Jan 2010 16:01:19 +0000
draft: false
tags: [Comunicati stampa]
---

Sostegno allo sciopero della fame iniziato il 4 gennaio da Francesco e Manuel coppia gay di Savona

Al Presidente del Senato, Sen. Renato Schifani  
Al Presidente della Camera dei Deputati, On. Gianfranco Fini

Ai Capigruppo del Senato della Repubblica;  
Ai Capigruppo della Camera dei Deputati;  
Al Presidente della 2^ Commissione Giustizia del Senato, Sen. Filippo Berselli;  
Al Presidente della Commissione Straordinaria per la tutela e la promozione  dei diritti umani del Senato, Sen. Pietro Marcenaro;  
Al Presidente della Commissione II Giustizia della Camera dei Deputati, On. Giulia Buongiorno;  
Al Presidente della Commissione XII Affari Sociali della Camera dei Deputati On. Giuseppe Palombo

Loro Sedi

In 20 paesi europei sono in vigore leggi che riconoscono, pur nella pluralità  e nella differenza di istituti e strumenti civili, le coppie di persone dello stesso sesso e ad esse attribuiscono precisi diritti e doveri, analogamente a quanto è previsto per le coppe formate da persone di sesso diverso.

* * *

Nella Carta dei Diritti, parte integrante del Trattato di Lisbona, e in diversi atti ufficiali dell’Unione si sollecitano i paesi aderenti a non discriminare le coppie omosessuali e quindi a legiferare in materia.

Milioni di persone omosessuali, transessuali, trans gender, intersessuali in Italia si devono invece confrontare con un’umiliante indifferenza da parte delle istituzioni nazionali rispetto alla necessità  che i loro amori, progetti di vita, diritti umani siano finalmente previsti nell’ordinamento.

Il movimento lgbti  italiano, formato dalle persone che subiscono discriminazioni a causa della loro identità  di genere o del loro orientamento sessuale, da decenni, attraverso grandi manifestazioni  nazionali, iniziative, campagne sociali e culturali, ha tentato di far comprendere alla politica che in assenza di una legge, le coppie di persone lgbti sono consegnate a un’ingiusta e insopportabile clandestinità sociale.

Dal 4 gennaio Francesco e Manuel, una coppia di ragazzi gay di Savona hanno iniziato uno sciopero della fame per richiamare l’attenzione dell’opinione pubblica rispetto al fatto che migliaia di coppie non sono tutelate dallo Stato italiano e non possono accedere a diritti e doveri che sono normali ed imprescindibili per tutti gli altri cittadini.

Il loro gesto segnala più di ogni altra cosa la situazione in cui siamo costretti a vivere e esprime l'impossibilità per tutte e tutti noi di continuare a sopportare quello che è di fatto lo status di fantasmi sociali, ovvero la negazione del nostro diritto ad una vita serena, diritto che riteniamo ci appartenga pienamente in qualità  di cittadine, cittadini e contribuenti di questo Stato, ma negato a causa dell’assenza dei necessari provvedimenti legislativi in questa materia.

Per tutte queste ragioni, facciamo appello alla vostra sensibilità  e, quali rappresentanti di tutto il popolo italiano e dell'unità della nazione, vi chiediamo di superare quelli che per noi sono incomprensibili veti e pregiudizi e di dare un chiaro segnale di interesse avviando al presto nei rami del Parlamento una discussione che porti finalmente al riconoscimento della pari dignità e pari diritti per le persone e le coppie lgbti.

Arcigay, Arcilesbica, Agedo, Associazione Radicale Certi Diritti, Associazione Crisalide PanGeneder, Associazione Lista Lesbica italiana, Associazione Trans Genere, Circolo Mario Mieli di Roma, Coordinamento Torino Pride, Famiglie Arcobaleno, Gay Roma.it,I Ken Onlus Napoli, Ireos Onlus Firenze, Liberamente NOI Roma, Mit, Nuova Proposta Roma, Open Mind Catania, Queer. Sel – Sinistra e Libertà Ecologia  per la cultura differenze, 3 D – Democratici per pari Diritti e Pari Dignità di lesbiche, gay, bisessuali, trans*, Roma Rainbow Choir, Arcigay Napoli, Arcilesbica Napoli, Arcigay Roma, Alessandra Brussato Mestre-Venezia, Guido Allegrezze, Cristiana Alicata, Associazione Renzo e Lucio di Lecco.