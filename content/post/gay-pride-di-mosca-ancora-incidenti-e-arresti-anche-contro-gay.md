---
title: 'Gay Pride di Mosca: ancora incidenti e arresti anche contro gay'
date: Sat, 28 May 2011 09:52:07 +0000
draft: false
tags: [Russia]
---

Nella foto Marco Cappato, deputato europeo Radicale, viene arrestato al Pride di Mosca nel 2007.

Arrestato anche Nikolay Alexeyev, promotore della minifestazione. Gruppi clerico-nazisti responsabili di grave violenze.

Comunicato Stampa dell’ Associazione Radicale Certi Diritti

Roma, 28 maggio 2011

Le notizie che giungono stamane da Mosca, dove per il sesto anno ci sono stati incidenti durante il Gay-Pride, sono gravissime e dimostrano di come i diritti democratici in Russia siano costantemente violati dalle autorità. Stamane, all’inizio della manifestazione, gruppi di clerico-nazisti hanno insultato e aggredito alcune decine di militanti dei diritti civili. La polizia è intervenuta aggredendo e arrestando anche gli esponenti della comunità lgbt(e) moscovita,  vittime loro stessi delle violenze dei gruppi estremisti. Le violenze sono poi continuate davanti alla sede del Comune di Mosca dove altri gruppi nazisti incappucciati hanno malmenato gli esponenti della comunità gay.

Al momento risultano in stato di fermo, sotto arresto, il promotore del Gay Pride Nikolay Alexeiev insieme a diverse decine di persone, fra queste Dan Choi, esponente Usa del movimento gay americano e l’inglese  Peter Tatchell, storico esponente dei diritti civili che si erano recati a Mosca per sostenere e aiutare la comunità lgbt(e) locale.

L’Associazione Radicale Certi Diritti aveva chiesto due giorni fa al Ministro degli Esteri Franco Frattini di allertare l’Ambasciata italiana a Mosca chiedendo l’invio di osservatori, così come deciso da alcune ambasciate occidentali. Tale decisione era stata richiesta da molte Associazione europee  dopo la condanna della Corte Europea dei Diritti dell’Uomo subita dalla Russia lo scorso anno per aver vietato il diritti di manifestare delle persone omosessuali.

Chiediamo, anche con atti formali nelle prossime ore, al Parlamento Europeo e alla Commissione Europea di intervenire nei confronti della Russia affinchè rispetti  le libertà e il diritto di manifestazione dei cittadini.