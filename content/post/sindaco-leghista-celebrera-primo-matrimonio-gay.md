---
title: 'SINDACO LEGHISTA CELEBRERA'' PRIMO MATRIMONIO GAY?'
date: Sun, 14 Feb 2010 10:55:42 +0000
draft: false
tags: [Comunicati stampa]
---

**Matrimonio omosessuale: sarà un sindaco leghista a celebrare il primo ?**

dal sito:  [http://www.tellusfolio.it/](http://www.tellusfolio.it/index.php?prec=/index.php&cmd=v&id=10548)

Apprendiamo con sorpresa dai giornali che il Sindaco leghista di Viggiù, Sandy Cane (foto), ha rifiutato «a malincuore» di unire civilmente in matrimonio due donne che hanno chiesto la pubblicazione degli atti nel suo Comune. Vorremmo ricordare al sindaco di Viggiù - con cui cercheremo di entrare in contatto anche personalmente - che la Legge italiana non impedisce ad una coppia omosessuale di accedere al matrimonio ma che, semplicemente, si è affermata una consuetudine che, per ora, non l’ha permesso. Questo è quanto, da oltre due anni, l'Associazione Radicale Certi Diritti (www.certidiritti.it) e l’avvocatura LGBT - Rete Lenford (www.retelenford.it), stanno cercando di far comprendere al Paese, attraverso l'iniziativa di “Affermazione civile” (www.affermazionecivile.it), che ha portato la questione davanti alla Corte Costituzionale. Siamo in attesa della sentenza della Consulta, prevista per il 23 marzo prossimo: ci auguriamo che porrà fine alla violazione del diritto dei cittadini omosessuali a contrarre matrimonio con una persona del proprio stesso sesso. Come candidati della Lista Bonino-Pannella, ci candidiamo in Lombardia anche per portare dentro alle istituzioni lombarde la voce delle persone gay, bisessuali e transessuali.

**Francesco Poirè**, Segretario dell’Associazione Enzo Tortora – Radicali Milano  
**Gian Mario Felicetti**, Responsabile dell'Associazione Radicale Certi Diritti per la campagna di Affermazione Civile  
**Luca Piva**, Tesoriere dell’Associazione Radicale Certi Diritti