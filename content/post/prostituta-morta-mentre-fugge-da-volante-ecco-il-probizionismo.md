---
title: 'PROSTITUTA MORTA MENTRE FUGGE DA VOLANTE: ECCO IL PROBIZIONISMO'
date: Sat, 11 Oct 2008 13:03:01 +0000
draft: false
tags: [Comunicati stampa]
---

**  Roma, 11 ottobre 2008  **

**PROSTITUTA MORTA MENTRE FUGGE DA POLIZIA: EPISODIO ESEMPLARE DELLA FEROCE CULTURA PROIBIZIONISTA.**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti e Monica Rosellini, Comitato per i diritti civili delle prostitute:**

“Ben pochi hanno oggi da dire qualcosa sulla morte a Bari di una prostituta nigeriana di 20 anni che, per paura di un lampeggiante di una volante della polizia, mentre fuggiva, è stata investita da un’autovettura. Questo è uno dei risultati della “pulizia” in atto, dell’operazione ‘strade pulite’, della vera e propria paura scatenata contro persone deboli che necessitano semmai di informazione, sostegno e, molto spesso, tutela dallo sfruttamento. Questa vicenda non finirà neanche nelle statistiche che la nostra mediocre classe politica di Governo userà per affermare sempre più la sua demagogia contro le prostitute”.