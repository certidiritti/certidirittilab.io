---
title: 'Luxuria, torna a casa'
date: Mon, 17 Feb 2014 15:08:01 +0000
draft: false
tags: [Politica]
---

Nota di Yuri Guaiana ed Enzo Cucco, rispettivamente Segretario e Presidente dell'Associazione Radicale Certi Diritti

Roma, 17 febbraio 2014

Affidiamo al web ed alle persone che sono in contatto con Vladimir Luxuria a Sochi un messaggio semplice e diretto. Torna a casa.

Non esporre la tua persona a rischi ulteriori rispetto a quelli che hai già corso: il risultato lo hai ottenuto e con grande efficacia, perchè i media italiani hanno dovuto tornare su un questione che sembrava relegata alle cronache rosa, e invece stiamo parlando della libertà di espressione e di vita di milioni di persone nella Federazione russa. La tua azione ha messo in evidenza cosa significa ( durante le olimpiadi e sotto i riflettori della stampa internazionale, figuriamoci quando tutto questi sarà finito) dichiarare pubblicamente che "gay è ok". Ogni ulteriore azione potrebbe avere conseguenze pericolose per te e forse anche per la comunità lgbt russa: non a caso nei mesi scorsi progettando una azione collettiva di gruppi europei in Russia durante le olimpiadi abbiamo raccolto l'esplicito invito a soprassedere da parte delle associazioni locali.

Torna a casa quindi, senza ulteriori manifestazioni, e continuiamo insieme la battaglia.