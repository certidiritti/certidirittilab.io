---
title: 'Incontro con Fini e Carfagna gesto di grande rilevanza politica'
date: Tue, 17 May 2011 09:29:40 +0000
draft: false
tags: [Politica]
---

In occasione della Giornata internazionale contro l'omofobia Certi Diritti insieme alle altre associazioni lgbte hanno incontrato il Presidente della Camera e il Ministro Carfagna. La nsotra associazione era presente con il ragazzo gay cui è stata negata la patente perchè gay.

Roma, 17 maggio 2011 

   
Comunicato Stampa dell’Associazione Radicale Certi Diritti  
   
In occasione della giornata internazionale contro l’omofobia e la transfobia l’Associazione Radicale Certi Diritti ha partecipato all’incontro di questa mattina alla Camera dei deputati con il Presidente della Camera on. Gianfranco Fini e il Ministro per le Pari Opportunità On. Mara Carfagna. La delegazione di Certi Diritti era composta da Cristian Friscina, il ragazzo pugliese a cui non è stata rinnovata la patente perché gay e da Sergio Rovasio e Giacomo Cellottini, Segretario e Tesoriere dell’Associazione.  
   
L’incontro è stato molto positivo e si è svolto, grazie all’impegno dell’on. Paola Concia, con tutte le principali associazioni che in Italia si battono per la promozione e la difesa dei diritti delle persone lesbiche, gay e transessuali. Molto importanti e significativi sono stati gli interventi dell'On. Paola Concia, del Presidente di Arcigay Paolo Patanè e del Presidente di Gaylib, Enrico Oliari.  
   
Il Presidente Fini ha letto un importante messaggio del Presidente della Repubblica di grande sensibilità e attenzione ai temi del superamento delle diseguaglienze. Ringraziamo anche il Ministro Mara Carfagna per quanto dichiarato in tema di lotta alle discriminazioni e per il sostegno alla proposta di legge di Paola Concia contro l’omofobia calendarizzata alla Camera il 23 maggio prossimo.  
   
Ci auguriamo ora che la classe politica tutta comprenda quanto sia importante impegnarsi nella lotta contro le discriminazioni e per far approvare  leggi  per il  riconoscimento di diritti, uguaglianza e parità per le coppie omosessuali: dalla lotta all’omofobia e transfobia, alle unioni civili al matrimonio tra persone dello stesso sesso così come ormai avviene in tutti i paesi democratici.  
   
Ci auguriamo che le parole lette durante l'incontro dal Ministro On. Mara Carfagna, dal Presidente della Camera On. Gianfranco Fini e dal Presidente della Repubblica On. Giorgio Napolitano vengano ascoltate da tutta quella classe politica che un giorno sì, e l’altro pure, offende la dignità delle persone omosessuali con dichiarazioni clerico-fondamentaliste.