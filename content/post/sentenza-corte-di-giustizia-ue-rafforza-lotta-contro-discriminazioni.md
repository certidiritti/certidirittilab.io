---
title: 'SENTENZA CORTE DI GIUSTIZIA UE RAFFORZA LOTTA CONTRO DISCRIMINAZIONI'
date: Fri, 11 Jul 2008 13:58:35 +0000
draft: false
tags: [certi diritti, Comunicati stampa, Corte di Giustiza, Ottavio Marzocchi, Sentenza, unione europea]
---

L'UE rafforza ulteriormente la protezione contro le discriminazioni previste all'articolo 13 del Trattato CE, incluse quelle basate sull'orientamento sessuale. La Corte di Giustizia dell'UE, con la sua importante sentenza del 10 luglio sull'applicazione della direttiva anti-discriminazioni basate sulla razza, rafforza ulteriormente la protezione delle persone contro le discriminazioni previste all'articolo 13 del Trattato CE, incluse quelle basate sull'orientamento sessuale.  
Affermando che possono essere considerate discriminazioni dirette vietate dalla direttiva anche le affermazioni pubbliche di un datore di lavoro che dica di non volere assumere "immigrati", si amplia in modo sostaziale la possibilità per le persone di non subire discriminazioni nel settore del lavoro, non solo sulla base della razza, ma anche sulla base del loro orientamento sessuale.  
Infatti un datore di lavoro che dica pubblicamente "non assumo gay" potrà essere portato davanti alle corti, dovrà dimostrare che la sua prassi non é discriminatoria e che risponda a un requisito essenziale e determinante per lo svolgimento dell'attività lavorativa. Se questo non sarà dimostrato, potrà essere condannato per violazione del diritto europeo e sanzionato.  
Certi diritti accoglie con favore tale sviluppo della giurisprudenza comunitaria, e chiede alle autorità italiane di adeguare la propria legislazione al riguardo.  
  
Per maggiori informazioni: Ottavio Marzocchi, ottavio.marzocchi@europarl.europa.eu