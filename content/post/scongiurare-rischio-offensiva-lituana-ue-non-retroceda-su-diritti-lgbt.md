---
title: 'SCONGIURARE RISCHIO "OFFENSIVA LITUANA": UE NON RETROCEDA SU  DIRITTI LGBT'
date: Fri, 13 Nov 2009 14:32:45 +0000
draft: false
tags: [Comunicati stampa]
---

**CERTI DIRITTI: L"OFFENSIVA LITUANA" NON FACCIA RETROCEDERE L'UNIONE EUROPEA SUI  DIRITTI LGBT. IL RISCHIO C'È E OCCORRE SCONGIURARLO!**

**Dichiarazione di Ottavio Marzocchi, reponsabile questioni europee di Certi Diritti, e Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

"Certi Diritti chiede all'Unione europea ed alle sue istituzioni di non retrocedere di fronte ad una offensiva intentata da alcuni partiti omofobici negli Stati membri e nel Parlamento europeo. C'é infatti il rischio concreto che, per non disturbare troppo gli Stati, l'UE faccia un passo indietro sui diritti LGBT:

\- Il 17 settembre 2009 il PE approvava a larga maggioranza una risoluzione di condanna della legge sulla protezione dei minori contro informazioni pubbliche dannose. Tale legge in realtà era volta a proibire e criminalizzare ogni informazione pubblica in merito all'omosessualità che fosse accessibile ai minori. Il PE chiedeva di riformare la legge omofoba, incaricava la sua commissione per le libertà pubbliche di seguire la questione e chiedeva "all'Agenzia dell'Unione europea per i diritti fondamentali di esprimere un parere sulla legge e sugli emendamenti alla luce dei trattati e del diritto dell'Unione europea".

Il 10 novembre il Parlamento lituano ha approvato una risoluzione che chiede al governo di portare il Parlamento europeo in Corte di Giustizia UE per annullare la risoluzione approvata.

Lo stesso giorno il Direttore dell'Agenzia per i Diritti Fondamentali, Morten Kjaerum, scriveva al Presidente del PE, il popolare polacco Buzek, che "la richiesta del PE, cosi come formulata, non ricade nei compiti previsti dal mandato dell'Agenzia".

Il 12 Novembre 2009 il PE ha approvato un emendamento alla risoluzione sul Programma di Stoccolma dell'UE sulla libertà, la giustizia e la sicurezza nei prossimi 5 anni, depositato da Jeanine Hennis-Plasschaert del gruppo ALDE su proposta di Ottavio Marzocchi (responsabile di Certi Diritti per le questioni europee e membro del segretariato del gruppo ALDE) che affermava _"invita gli Stati membri a garantire che il principio del riconoscimento reciproco sia applicato anche alle coppie dello stesso sesso nell'UE – in particolare le coppie sposate, i partner o le coppie di fatto – almeno per quanto riguarda i diritti relativi alla libertà di circolazione"_. Il gruppo PPE ha deciso di astenersi sul voto finale della risoluzione solamente a causa dell'approvazione di questo emendamento, nonostante fossero stati votati e depositati 494 emendamenti. Il PPE ha anche chiesto di cambiare il testo in vista del voto in plenaria che avverrà tra due settimane a Strasburgo.

Certi Diritti si appella al Direttore dell'Agenzia per i diritti fondamentali ed in particolare al PE perché non rinuncino, ma anzi continuino a richiamare gli Stati membri al rispetto dei valori europei e dei diritti fondamentali, inclusi quelli LGBT.