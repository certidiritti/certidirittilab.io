---
title: 'INTERROGAZIONE DEPUTATI RADICALI SU OMOFOBIA DI STATO IN LITUANIA'
date: Fri, 17 Jul 2009 11:30:41 +0000
draft: false
tags: [Comunicati stampa]
---

**INTERROGAZIONE PARLAMENTARE SU LEGISLAZIONE OMOFOBICA IN LITUANIA. PARLARE DI OMOSESSUALITA’ IN LITUANIA PUO’ COSTARE LA PRIGIONE, INTERROGAZIONE DEI DEPUTATI RADICALI SUL MANCATO RISPETTO DEI DIRITTI CIVILI E UMANI.**

**I Deputati radicali hanno stamane depositato, primo firmatario Matteo Mecacci, il testo di un’interrogazione parlamentare sulla nuova legislazione lituana che prevede anche l’arresto per coloro che parleranno pubblicamente di omosessualità.**

Al Presidente del Cosniglio dei Minsitri

Al Minsitro degli Esteri

Per sapere - premesso che:

Il Parlamento lituano ha approvato il 14 luglio 2009 gli emendamenti della legge sulla tutela dei minori contro effetti negativi della Pubblica Informazione. La legge afferma che "un effetto negativo sullo sviluppo dei minori" è causata da "informazioni pubbliche riguardanti i rapporti omosessuali" e che l'infomrazione su questi temi "sfida i valori della famiglia". Tale disposizione pone le informazioni sull’ omosessualità alla pari di quelle sulle questioni come la rappresentazione di violenza fisica, i casi di violenza con lesioni irreversibili e addirittura l’istigazione al suicidio. La legge fa espressamente divieto alla diffusione di informazioni sull’ omosessualità, in particolare se questa può essere letta da parte dei minori.

Il Presidente della Lituania, che ha posto il veto alla legge in quanto il testo approvato è stato redatto in termini vaghi e poco chiari, ha chiesto al Parlamento di riconsiderare tale votazione e ha chiesto garanzie riguardo un nuovo testo e che sia conforma "ai principi costituzionali dello Stato di diritto, la certezza del diritto, e di chiarezza giuridica e che il testo non sia in conflitto con le garanzie di una società aperta e pluralistica della democrazia".

Alcune Ong dei diritti umani e alcuni deputati europei hanno chiesto più volte alle istituzioni dell'Unione europea di intervenire per rivedere il progetto di legge prima della sua approvazione; peraltro è stato fatto anche un ricorso alla Corte costituzionale. Inoltre gli emendamenti al codice penale e amministrativo saranno esaminati in autunno, gli stessi prevedono la criminalizzazione, da parte di privati o persone giuridiche, di chi "propaga l'omosessualità" nelle aree pubbliche; tali proposte prevedono sanzioni amministrative con multa fino a 1500 euro, o addirittura l’ arresto.

Per sapere:

se il Governo italiano, di concerto con quelli europei, firmatari della proposta di depenalizzazione dell’omosessualità presso la Commissione Diritti Umani dell’Onu, intende avviare iniziative nei confronti del Governo lituano affinché venga eliminata questa vera e propria forma di persecuzione di Stato nei confronti delle persone omosessuali;

se il Governo italiano non ritiene che tale legge, e gli emendamenti ad essa collegati, siano incompatibili con i diritti umani e le libertà fondamentali delle persone, sanciti anche dalle Convenzioni Internazionali ed europee e in particolare con la libertà di espressione che, include il diritto di cercare, ricevere e diffondere informazioni, e che è incompatibile con il diritto comunitario e le politiche in materia di lotta contro la discriminazione;

se il Governo non ritiene che tale legge è in contraddizione con la Carta dei diritti fondamentali, con l'articolo 6 del TUE e l'articolo 13 del trattato CE, vale a dire i valori fondamentali su cui si fonda l'Unione europea?

Come intende muoversi il Governo italiano al fine di assicurare che la Lituania rispetti i suoi obblighi nel quadro dei Trattati europei e nell’ambito del diritto internazionale? Intende sostenere l’ attivaazione, se necessario, della procedura di cui all'articolo 7 del trattato sull'Unione europea nei confronti della Lituania?