---
title: 'COME ISCRIVERSI O VERSARE UN CONTRIBUTO'
date: Mon, 19 Apr 2010 00:21:20 +0000
draft: false
tags: [Senza categoria]
---

**Tutto il nostro lavoro è volontario** e solo dal canale dell’**autofinanziamento**  
possiamo avere le risorse economiche e l'aiuto per promuovere ulteriori iniziative.  
  
Nella tradizione radicale, per noi **"prendere la tessera"**  
non ha valenza identitaria o di appartenenza, ma significa dire:  
**"quest'anno io condivido i vostri obiettivi e li faccio miei. L'anno prossimo vedremo."**

**ANNO 2011**

La quota di iscrizione è **50 Euro**

La quota di iscritto sostenitore è **150 Euro**

La quota di iscrizione per persone senza reddito è di **25 Euro**

**CON PAYPAL**

 

Quote di iscrizione

Ordinaria €50,00 Sostenitore €150,00 Senza reddito €25,00

  ![](https://www.paypal.com/it_IT/i/scr/pixel.gif)

  
  
  
  

   ![](https://www.paypal.com/it_IT/i/scr/pixel.gif)

-

**CON BONIFICO BANCARIO**

sul Conto Corrente intestato a:

**Associazione Radicale Certi Diritti**  
presso BCC di Roma, Ag. 21  
IBAN: **IT 34 E 08327 03221 000000003165**  
BIC: ROMAITRR

Specifica nella causale **Iscrizione anno 2011** OPPURE **Contributo**.

Invia una mail a **[tesoriere@certidiritti.it](mailto:tesoriere@certidiritti.it?subject=Iscrizione%20con%20bonifico%20bancario)** per comunicare l'avvenuto bonifico, indicando:

**nome e cognome, luogo e data di nascita, indirizzo postale**.

Questo è l'unico modo che abbiamo per poterti considerare iscritto e inviarti la tessera associativa. Il sistema bancario, infatti, non ci fornirà alcun tuo recapito, ma solo il tuo nome. In assenza di tue indicazioni, saremo costretti a considerare il tuo pagamento come un contributo anonimo. Grazie per la collaborazione!