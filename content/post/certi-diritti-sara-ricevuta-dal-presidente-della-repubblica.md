---
title: 'CERTI DIRITTI SARA’ RICEVUTA DAL PRESIDENTE DELLA REPUBBLICA'
date: Fri, 14 May 2010 07:57:47 +0000
draft: false
tags: [Comunicati stampa]
---

**GIORNATA MONDIALE CONTRO L'OMOFOBIA: L'ASSOCIAZIONE RADICALE CERTI DIRITTI, INSIEME AD ALTRE ASSOCIAIZONI, LUNEDI' 17 MAGGIO SARA’ RICEVUTA DAL PRESIDENTE DELLA REPUBBLICA.   
  
CERTI DIRITTI CONSEGNERA' AL PRESIDENTE LE OLTRE 12.000 FIRME RACCOLTE CON IL PORTALE [GAY.IT](http://www.gay.it/channel/attualita/29788/Omofobia-Napolitano-incontera-le-associazioni-gay.html) PER CONFERIRE A MARIA LUISA MAZZARELLA UNA MEDAGLIA  AL VALORE CIVILE.**

  
In occasione della giornata mondiale contro l'omofobia, che si celebra in tutto il mondo lunedì 17 maggio, l'Associazione Radicale Certi Diritti, insieme ad altre associazioni che si battono per la difesa dei diritti civili e umani, in particolare delle persone lgbt, sarà ricevuta al Quirinale dal Presidente della Repubblica.  L'incontro si svolgerà grazie all'intervento dell'on. Paola Concia, che ringraziamo.  
  
L'incontro sarà anche l'occasione per consegnare personalmente al Capo dello Stato la documentazione circa l'iniziativa promossa insieme al portale Gay.it affinché sia conferito ad una ragazza di Napoli, Maria Luisa Mazzarella, un riconoscimento al valore civile per aver rischiato la vita nel difendere un amico omosessuale da un'aggressione omofoba avvenuta lo scorso anno nella piazza centrale della città nell'indifferenza dei passanti. L'appello, promosso dal portale [gay.it](http://www.gay.it/channel/attualita/29788/Omofobia-Napolitano-incontera-le-associazioni-gay.html) insieme all’Associazione Radicale Certi Diritti, è stato firmato da 12.300  cittadini, tra questi molte personalità della cultura e dello spettacolo.