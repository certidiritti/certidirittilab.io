---
title: 'Newsletter 06 Maggio'
date: Wed, 31 Aug 2011 08:44:02 +0000
draft: false
tags: [Politica]
---

**![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)**

Questo numero della newsletter sembra quasi “monografico”, la star del momento è Giovanardi che non perde mai l’occasione di dimostrare la sua omofobia scatenata facendosi deridere anche oltre le Alpi … Ma il “povero” sottosegretario non è rimasto solo a difendere la “famiglia costituzionale”, con lui Buttiglione e la Binetti rappresentano l’Italia più retrograda.

Ma c’è anche un’altra Italia, che riconosce il valore di tutte le famiglie come hanno fatto Ikea, Eataly e EasyJet. E il 23 maggio ritorna in Parlamento la legge contro l’omofobia. Vi terremo aggiornati.

Buona lettura

**IN MEMORIA DI DAVID KATO KISULE**
===================================

**L’Uganda si modera … “Sei gay? Ti do l’ergastolo” anziché la pena di morte.  
[Leggi >](http://www.notiziegay.com/?p=74161)**

**Uganda, arrestato per la quinta volta leader opposizione Kizza Besigye.   
[Leggi >](http://www.blitzquotidiano.it/agenzie/uganda-arrestato-per-la-quinta-volta-leader-opposizione-kizza-besigye-837583/)**

**In Uganda si mette male.  
[Leggi >](http://www.ilpost.it/2011/04/29/in-uganda-le-cose-si-mettono-male/)**

**Donna gay violentata e uccisa in Sudafrica.   
[Leggi >](http://www.gaywave.it/articolo/donna-gay-violentata-e-uccisa-in-sudafrica/30563/)**

[**SOSTIENI IL FONDO DAVID KATO KISULE >**](campagne/in-memoria-di-david-kato/1071-fondo-in-memoria-di-david-kato-kisule.html)

ASSOCIAZIONE
============

**Gay: Buttiglione come il suo compagno di sacrestia Giovanardi.   
[Leggi >](tutte-le-notizie/1105.html)**

**Giovanardi parla cinque giorni a settimana di gay: roba da Freud.   
[Leggi >](tutte-le-notizie/1109.html)**

**Risposta di Certi Diritti alla Commissione europea.   
[Leggi >](tutte-le-notizie/1108.html)**

**Guaiana (Lista Bonino Pannella): Stiffoni (Lega) come Ahmadinejad. E si dimentica pure dei LOS Padania.   
[Leggi >](http://www.radicalimilano.it/public/comunicati/visua.asp?dati=ok&id=1159)**

**Noi dentro Ikea, Giovanardi fuori dal mondo.   
[Leggi >](http://www.radicali.it/20110501/comunicato-stampa-lista-bonino-pannella-flash-mob-corsico-milano-noi-dentro-ikea-giovanardi)**

**Mina Welby: I malati sono sacrificati alla campagna elettorale la loro volontà non conta.   
[Leggi >](http://www.radicali.it/rassegna-stampa/int-m-welby-malati-sono-sacrificati-alla-campagna-elettorale-loro-volont-non-conta)**

RASSEGNA STAMPA
===============

**Europride 2011: lo spot ufficiale.  
[Guarda >](http://www.youtube.com/watch?v=ujy-1KTbht0&feature=player_embedded)**

**A Palermo il pride 2011 gira attorno all’impegno politico.  
[Leggi >](http://gaynews24.com/?p=20450)**

**Il Pd ha deciso, sostiene l’Europride.   
[Leggi >](http://gaynews24.com/?p=20541)**

**Fiamma Tricolore: organizzeremo un etero Pride per aiutare le ragazze madri. Vergognoso il video ufficiale di Europride.  
[Leggi>](http://gaynews24.com/?p=20492)**

**Calendarizzata per il 23 maggio la legge contro omofobia. Ne da l’annuncio Anna Paola Concia.   
[Leggi >](http://gaynews24.com/?p=20525)**

**Giovanardi va all'Ikea (età di lettura: da 3 anni in su).  
[Leggi >](tutte-le-notizie/1106.html)**

**L’Ikeafobia di Giovanardi sbarca in Francia.  
[Leggi >](http://www.giornalettismo.com/archives/123635/l%E2%80%99ikeafobia-di-giovanardi-sbarca-in-francia/)**

**Carlo Giovanardi - ''Pazza Ikea'' **Il sottosegretario Giovanardi “ ci canta” la canzone “Pazza l’Ikea”.Geniale. Semplicemente ironico, amaramente reale e attuale. Bello il testo, ottima l’idea del gruppo Arcietero. **  
[Guarda il video >](http://www.youtube.com/watch?v=rzCpMpSjlCU&feature=player_embedded)**

**Coppie di fatto. Carlo Giovanardi, è battaglia per avere i matrimoni gay.   
[Leggi >](http://gaynews24.com/?p=20658)**

**Gay: Giovanardi, baci in pubblico ostentazione che infastidisce.  
[Leggi >](http://www.asca.it/news-GAY__GIOVANARDI__BACI_IN_PUBBLICO_OSTENTAZIONE_CHE_INFASTIDISCE-1013247-ORA-.html)**

**Nuova polemica dopo l’Ikea. Famiglia aperta, caso alla Spezia: anche le 5 Terre sono “friendly”?   
[Leggi >](http://www.notiziegay.com/?p=74241&)**

**Ikea. Dopo Giovanardi tocca alla Binetti: quello spot “provocatorio perché sovverte il valore famiglia”.  
[Leggi >](http://www.notiziegay.com/?p=74228&)**

**Rocco Buttiglione (ancora) contro i gay: "I bambini delle famiglie tradizionali, da grandi, pagheranno le pensioni ai gay!"   
Leggi >**

**Gay, Eataly come Ikea: "Aperti a tutte le famiglie come Ikea"  
[Leggi >](http://www.blitzquotidiano.it/agenzie/gay-eataly-come-ikea-aperti-a-tutte-le-famiglie-come-ikea-839911/)**

**Pure EasyJet percula Giovanardi.   
[Leggi >](http://www.notiziepolitica.it/2011/04/pure-easyjet-percula-giovanardi.html)**

**Roma. Rapiscono una transessuale, lo picchiano e lo derubano: arrestati.   
[Leggi >](http://www.romatoday.it/cronaca/banda-del-ducato-minacce-prostitute-transessuale.html)**

**Il sindaco leghista di Treviso: «Multerei i gay che si baciano nei luoghi pubblici»   
[Leggi >](http://www.gazzettino.it/articolo.php?id=147112)**

**Stiffoni (Lega): “Da noi non ci sono gay, abbiamo un dna diverso”   
[Leggi >](http://www.giornalettismo.com/archives/123374/stiffoni-gay-lega/)**

**Intervista a S. Milk: mio zio mi insegnò che gay è bello.  
[Leggi >](http://www.zeroviolenzadonne.it/rassegna/pdfs/4760b74b6d2f6018416fb8e6a9d5363f.pdf)**

**Don Cantini. Il prete che ha violentato bambini per vent’anni rimarrà impunito.  
[Leggi >](http://www.giornalettismo.com/archives/123616/il-prete-che-ha-violentato-bambini-per-ventanni-rimmarra-impunita/)**

**Sit-in in piazza Montecitorio a Roma organizzato dall’associazione ‘Luca Coscioni’ contro il ddl Calabrò sul biotestamento.  
[Guarda il video >](http://www.ilfattoquotidiano.it/2011/04/27/biotestamento-marino-pd-b-usa-la-legge-contro-i-magistrati/107427/)**

**Testamento biologico quest'Italia così lontana dall'Europa.  
[Leggi >](http://www.radicali.it/rassegna-stampa/testamento-biologico-questitalia-cos-lontana-dalleuropa)**

**Wojtyla beato. Sesso, pedofilia, Opus Dei e dittature di destra ma la lotta al comunismo lo assolve?   
[Leggi >](http://www.blitzquotidiano.it/cronaca-mondo/wojtyla-beato-sesso-pedofilia-opus-dei-comunismo-839038/)**

**Francia:** **Deportato perché gay, ora è legione d’onore. ****  
[Leggi >](http://www.giornalettismo.com/archives/123322/deportato-perche-gay-ora-e-legione-donore/)**

**Mio padre, l'eroe.** Pensieri sulla vita e l'universo di una lesbica siriana**.   
[Leggi >](rassegna-stampa/1107.html)**

**Regno Unito: la Ong cattolica non può discriminare i genitori gay.  
[Leggi >](http://www.queerblog.it/post/11079/regno-unito-la-ong-cattolica-non-puo-discriminare-i-genitori-gay)**

**Dal Principe William apertura alle nozze gay: se due persone si amano e vogliono sposarsi è giusto che lo possano fare.   
[Leggi >](http://gaynews24.com/?p=20611)**

**'Dont' say gay": in Tennessee una legge per non parlare di omosessualità nelle scuole.   
[Leggi >](http://www.queerblog.it/post/11082/dont-say-gay-in-tennessee-una-legge-per-non-parlare-di-omosessualita-nelle-scuole)**

**Nord America. Abusi sessuali, ordine religioso cattolico fallisce per i risarcimenti che ha dovuto pagare.   
[Leggi >](http://www.notiziegay.com/?p=74333)**

**Usa. Carcere e diritti gay. Consentite le visite coniugali in prigione.   
[Leggi >](http://gaynews24.com/?p=20454)**

**Prelato argentino invoca l'intercessione di Giovanni Paolo II perché Dio perdoni i politici che hanno approvato il matrimonio gay.   
[Leggi >](http://www.queerblog.it/post/11113/prelato-argentino-invoca-lintercessione-di-giovanni-paolo-ii-perche-dio-perdoni-i-politici-che-hanno-approvato-il-matrimonio-gay)**

STUDI, RICERCHE E MATERIALI INFORMATIVI
=======================================

**Indagine on line: [questionario sul mondo LGBTQ e il lavoro ](http://www.iosonoiolavoro.it/questionario)anonimo.  
Per compilarlo sono necessari circa 15 minuti Si può farlo fino al 15 maggio 2011.**

**Ulteriori informazioni sul sito: [www.iosonoiolavoro.it](http://www.iosonoiolavoro.it/)**

**Decalogo dei diritti dei pazienti gay, lesbiche, bisessuali e transessuali.   
[Leggi >](http://oknotizie.virgilio.it/go.php?us=5841427cda837e57)**

**Fantasie sessuali e web. Le donne sono interessate alle storie tra le coppie di uomini gay.   
[Leggi >](http://gaynews24.com/?p=20530)**

**Sesso non protetto per un’italiana su due.  
[Leggi >](http://www.diredonna.it/sesso-non-protetto-per-unitaliana-su-due-35338.html)**

LIBRI E CULTURA
===============

**'Liberi Amori Possibili'. Rassegna di teatro omosessuale a Milano.   
[Leggi >](http://milano.mentelocale.it/31032-liberi-amori-possibili-rassegna-di-teatro-omosessuale-a-milano/)**

**Cinema Verité: il primo gay della storia della tv.  
[Leggi >](http://www.parrotized.it/societa/cinema-verite-il-primo-gay-della-storia-della-tv.htm)**

**8 maggio per ricordare Keith Haring.   
[Leggi e guarda il video >](http://www.queerblog.it/post/11105/gay-day-2-8-maggio-per-ricordare-keith-haring)**

**Miguel Bosè è diventato papà:"Sono l'uomo più felice sulla terra"**Due gemelli grazie all'utero in affitto. **  
[Leggi >](http://www3.lastampa.it/spettacoli/sezioni/articolo/lstp/399781/)**

**Il transessuale thailandese che sta spopolando ai Thailand's Got Talent.   
[Guarda](http://superdecio.blogspot.com/2011/04/il-transessuale-thailandese-che-sta.html)**** [il video >](http://superdecio.blogspot.com/2011/04/il-transessuale-thailandese-che-sta.html)**

**Libri. Dopo 120 anni pubblicato “Dorian Gray” di Oscar Wilde senza censure.[Leggi >](http://www.notiziegay.com/?p=74321&)**

**Il ‘Premio Frascati Pari opportunità’ assegnato ad un autore gay ma il libro è introvabile.   
[Leggi >](http://www.notiziegay.com/?p=74324&utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+blogspot%2Fnotiziegay+%28Notizie+gay%29&utm_content=Google+Reader)**

**Diego Dalla Palma, _[A nudo](http://www.ibs.it/code/9788820049614/dalla-palma-diego/nudo.html)_, Sperling & Kupfer, € 17,00**

**Gianfranco Meneo, _[Transgender. Le sessualità disobbedienti](http://www.edizioni-palomar.it/schedaprodotti.asp?IDProdotto=541)_, Palomar, € 15,00**

[www.certidiritti.it](http://www.certidiritti.it/)
==================================================