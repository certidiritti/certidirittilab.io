---
title: 'Spending Review: per le adozioni tagli alla spesa con allargamento dei diritti anche single e coppie conviventi'
date: Mon, 14 May 2012 09:52:10 +0000
draft: false
tags: [Diritto di Famiglia]
---

Dichiarazione di Donatella Poretti, Senatrice Radicale, e Matteo Mainardi, Giunta di Segreteria Associazione Radicale Certi Diritti.

Roma, 14 maggio 2012

E' giunto il momento di effettuare una spending review anche nel settore delle adozioni attraverso un ampliamento dei diritti di adozione relativamente a persone singole e di coppie non unite dal vincolo matrimoniale eterosessuali ma anche omosessuali.

In Italia 35.000 minorenni vivono al di fuori del nucleo familiare e molti di questi, a causa del calo costante delle coppie disponibili ad adottare causato a sua volta dai proibitivi e antiquati limiti imposti dalla legge e costi associati, passeranno la propria vita, fino al raggiungimento della maggiore età, in case famiglia, istituti e presidi residenziali socio-assistenziali. Ciò, oltre a sopprimere il diritto dei minorenni a diventare figli, comporta ingenti spese per lo Stato che potrebbero agevolmente essere destinate ad interventi di aiuto alle famiglie adottive con reddito medio-basso.

Chiediamo al Governo di prendere in considerazione una spending review di questo tipo oltre alla semplificazione burocratica delle adozioni internazionali eliminando le attività di ricezione con decreto delle sentenze di adozione pronunciate all’estero da sostituirsi con un riconoscimento automatico sulla scia di altri Paesi europei.