---
title: 'REGIONE LAZIO E LOTTA ALL''OMOFOBIA E TRANSFOBIA'
date: Thu, 14 Oct 2010 09:45:38 +0000
draft: false
tags: [Comunicati stampa]
---

**La Commissione di Vigilanza sul pluralismo dell'informazione del Consiglio Regionale del Lazio, ha dato parere favorevole, per quanto di competenza come Commissione secondaria,  alla  proposta di legge radicale contro le discriminazioni sessuali e di genere. Astenuti Pdl e Lista Polverini.**

Giuseppe Rossodivita: “Astensione inspiegabile e incoerente rispetto alle dichiarazioni rese alla stampa dalla Presidente Polverini in occasione delle aggressioni agli omosessuali verificatesi, anche ultimamente, a Roma e nel Lazio"

Nel corso della seduta odierna la III Commissione consiliare di Vigilanza sul pluralismo dell’informazione ha reso parere favorevole, per quanto di propria competenza, sulla  Proposta di legge n. 40 “Norme contro le discriminazioni determinate dall’orientamento sessuale o dall'identità di genere” presentata il 12 luglio scorso dai consiglieri della Lista Bonino Pannella Giuseppe Rossodivita e Rocco Berardo. La III Commissione, in sede secondaria, ha espresso parere favorevole sull'intera proposta di legge dopo avere esaminato favorevolmente i due articoli relativi all'impegno della Regione a realizzare campagne di comunicazione contro i comportamenti discriminatori basati sugli orientamenti sessuali e sull'identità di genere ed all'impegno, allo stesso scopo, a monitorare la programmazione televisiva locale.

“Se per un verso, con il voto della III Commissione,  registriamo un avvio positivio dell'iter legislativo, per altro verso” ha dichiarato Giuseppe Rossodivita, capogruppo della Lista Bonino Pannella e vicepresidente della III Commissione “l’astensione dei consiglieri del Pdl e della Lista Polverini appare inspiegabile ed incoerente con le dichiarazioni di impegno nella lotta alle discriminazioni e alla violenza contro le persone omosessuali e transessuali rilasciate dalla Polverini e da esponenti di spicco del Pdl  in occasione della brutale aggressione alla coppia gay inglese avvenuta nel settembre scorso nella nostra regione od ancora di recente in occasione del torneo di tennis Lgbt.  Ancora una volta la Presidente Polverini e la maggioranza che la sostiene, fanno esattamente il contrario di quello che dicono attraverso i giornali e le Tv. Purtroppo non ne siamo più sorpresi.".