---
title: 'DISCRIMINATO DALL''UDC PERCHE'' GAY, LETTERA DI CERTI DIRITTI: VIENI DA NOI'
date: Thu, 23 Oct 2008 11:08:48 +0000
draft: false
tags: [Comunicati stampa]
---

DIRIGENTE DELL'UDC DISCRIMINATO PERCHE' GAY: VENGA DA NOI RADICALI, LA ASCOLTEREMO SENZA IPOCRISIE E ALCUNA DISCRIMINAZIONE.  
   
Roma, 23 ottobre 2008  
   
L'Associaizone Radicale Certi Diritti ha inviato oggi una lettera di invito al Congresso di Radicali Italiani ad Alberto Villa, esponente dell'Udc emarginato perchè gay, di seguito il testo inviato da Sergio Rovasio, Segretario Associaizone Radicale Certi Diritti: Egregio Dottor Alberto Villa,  
abbiamo letto di quanto le è accaduto all'interno dell'Udc dopo il suo 'outing' e dopo il suo appoggio al Gay Pride di Genova, previsto nel 2009. Le esprimiamo tutta la nostra solidarietà e vicinanza e la invitiamo a partecipare al Congresso di Radicali Italiani, che si svolgerà da giovedì 31 ottobre a domenica 2 novembre a Chianciano Terme. Nell'ambito del Congresso di Radicali Italiani, la invitiamo a partecipare ai lavori della Commissione :"IPOTECA CONTEMPORANEA CONTRO LA CIVILTA': contro tutti i proibizionismi per la libertà e la responsabilità delle persone", di cui sono relatore in Congresso, che si insedierà la sera di giovedì 30 e proseguirà i suoi lavori nella giornata di venerdì 31 ottobre. La accoglieremmo a braccia aperte, senza ipocrisie, senza alcuna discriminazione, con tanta voglia di ascoltarla.  
   
Sergio Rovasio  
Segretario Associazione Radicale Certi Diritti  
[www.certidiritti.it](http://www.certidiritti.it)