---
title: 'Marco Pannella su David Kato, appello ad autorità italiane'
date: Thu, 27 Jan 2011 16:38:15 +0000
draft: false
tags: [Comunicati stampa, david kato, marco pannella, Uganda]
---

**PANNELLA: DAVID KATO, ONORATO PER LA SUA VITA E PER LA BATTAGLIA IDEALE CHE LO HA PORTATO AD ESSERE ASSASSINATO, COMMEMORATO UFFICIALMENTE DAL PRESIDENTE DEL PARLAMENTO EUROPEO JERZY BUZEK. SEGNALO QUESTO E ALTRO ALLE MASSIME AUTORITÀ  DI ROMA, MAGARI DAI DUE LATI DEL TEVERE**

Roma, 27 gennaio 2011

**Dichiarazione di Marco Pannella**

La bella, profonda, così tempestiva dichiarazione scritta del presidente del Parlamento Europeo Jerzy Buzek, per deplorare ufficialmente e protestare contro il regime ugandese per l’assassinio del compagno radicale di Certi Diritti, David Kato, è un segnale felicemente contraddittorio rispetto al desolato deserto europeo nel quale, purtroppo, tutti noi possiamo constatare di vivere.

Mi permetterei di sottolineare anche alle massime autorità della nostra Repubblica che - ancorché si parli di un Radicale e quindi, secondo il nostro regime, da clandestinizzare se possibile anche in morte - una loro parola sulla morte di David Kato onorerebbe non solo la memoria di questo nostro compagno, che di per sé non ne necessita, ma la Repubblica stessa, ancorché sarebbe evidente solo se si procedesse a una Tac del regime nostrano.

E visto che ci siamo, vorremmo segnalare alla loro attenzione che un altro nostro compagno, Biram Dah Abeid, consigliere generale del Partito Radicale Nonviolento, è da settimane in carcere in Mauritania, colpevole di un'azione contro la realtà schiavistica che riguarda almeno il 25 per cento di quella popolazione, e che riteniamo naturalmente in grave pericolo.