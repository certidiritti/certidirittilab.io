---
title: 'Angels in America I e II'
date: Sun, 13 Feb 2011 09:18:39 +0000
draft: false
tags: [Politica]
---

Ferdinando Bruni e Elio De Capitani insieme alla compagnia Teatro dell'Elfo portano in scena il capolavoro di Tony Kushner dal 13 febbraio al 27 aprile 2011.

  
L’edizione integrale di Angels in America ha inaugurato il teatro Elfo Puccini, la nuova sede della compagnia, con uno spettacolo memorabile: sei ore e mezza intense e incalzanti che hanno emozionato gli spettatori come in poche altre occasioni. Doveva essere un evento unico, ma il successo ha convinto tutti a riproporre quest’epopea contemporanea anche in altri contesti. 

**[Per tutte le informazioni sullo spettacolo e la tournée >](http://www.elfo.org/stagioni/20102011/tournee/angelsinamericamaratona.html)**

Certi Diritti ringrazia la compagnia Teatro dell'Elfo per il contributo all'associazione e per la commemorazione di David Kato Kisule realizzata al Teatro Valle di Roma il 12 febbraio 2011.