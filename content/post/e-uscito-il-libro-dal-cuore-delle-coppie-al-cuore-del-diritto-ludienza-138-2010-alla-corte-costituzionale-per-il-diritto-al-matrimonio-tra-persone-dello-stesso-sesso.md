---
title: 'E'' uscito il libro ''Dal cuore delle coppie al cuore del diritto. L''udienza 138/2010 alla Corte costituzionale per il diritto al matrimonio tra persone dello stesso sesso'''
date: Wed, 02 Nov 2011 16:05:57 +0000
draft: false
tags: [Diritto di Famiglia]
---

Nel volume, a cura di Yuri Guaiana ed edito da Stampa Alternativa, **il percorso politico e legale per il diritto al matrimonio tra persone dello stesso sesso che ha portato alla sentenza delle Corte Costituzionale 138 del 14 aprile 2010**.  
  
  
**VUOI COMPRARE O REGALARE IL LIBRO?  
PUOI ACQUISTARLO A** **10 EURO (COMPRESE LE SPESE DI SPEDIZIONE)  
SUL SITO DI CERTI DIRITTI. LEGGI SOTTO COME FARE**

L’Associazione Radicale Certi Diritti ha raccolto in un libro **il percorso politico e legale per il diritto al matrimonio tra persone dello stesso sesso, che ha portato alla sentenza delle Corte Costituzionale 138 del 14 aprile 2010**. Il libro, curato da Yuri Guaiana ed edito da Stampa Alternativa, s’intitola 'Dal cuore delle coppie al cuore del diritto' e ha come obiettivo quello di cristallizzare nella memoria collettiva il significato politico della battaglia di **Affermazione Civile**, campagna finalizzata al superamento delle diseguaglianze per l’accesso all’istituto del matrimonio civile delle coppie dello stesso sesso.

  
Cardini di questa campagna sono le t**ante coppie di persone dello stesso che si sono messe in gioco in prima persona con l'impegno esemplare di supporto legale di esperti avvocati e giuristi**. Il libro - dopo gli interventi dei dirigenti dell’Associazione Radicale Certi Diritti: Enzo Cucco, membro del direttivo, Gian Mario Felicetti, responsabile di Affermazione Civile e del Segretario nazionale, Sergio Rovasio - si apre con le testimonianze di due delle coppie protagoniste, Matteo Pegoraro, attivista per i diritti umani, ed Enrico Oliari, presidente di GayLib.  
Nel volume si propone anche la Memoria di Costituzione davanti alla Corte dell’ avv. Francesco Bilotta, membro di Avvocatura per i Diritti LGBT – Rete Lenford, tra i più impegnati ed esperti su questo fronte di iniziativa legale.  
  
Il volume entra poi nel cuore della questione con la trascrizione dell’udienza alla Corte Costituzionale del 23 marzo 2010 con  gli interventi, tra gli altri, dei **costituzionalisti prof.ssa Marilisa D’Amico, del prof. Vittorio Angiolini, e del prof. Vincenzo Zeno Zencovich. e dgli Avvocati Massimo Clara e Ileana Alesso** che, con gli altri, avevano fatto parte, gratuitamente, del Collegio di Difesa davanti alla Consulta.  
Dopo la trascrizione, Yuri Guaiana porta l’attenzione su alcuni elementi politicamente rilevanti della sentenza con una sua riflessione.  
  
Il volume si conclude con un’appendice documentaria con l’ordinanza del Tribunale di Venezia, che aveva anche sollevato la questione di legittimità costituzionale, portando così alla Sentenza della Corte costituzionale n. 138 del 14 aprile 2010,  e al Comunicato Stampa del 15 aprile 2010 dell’Associazione Radicale Certi Diritti e del Collegio di difesa sulle motivazioni della sentenza con elencati i tre punti più significativi.

**Il libro si può ordinare al seguente indirizzo e-mail: [  
tesoriere@certidiritti.it](mailto:tesoriere@certidiritti.it)  ****(allegando copia del versamento effettuato).****Il costo del libro è di** **10 Euro (comprese le spese di spedizione) che si possono effettuare tramite bonifico bancario al conto IT 34 E 08327 03221 000000003165 o con versamento sul conto corrente postale n. C/C n. 1001131539, specificando nella causale: libro ‘Dal cuore delle coppie al cuore del diritto’.**