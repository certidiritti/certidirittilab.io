---
title: 'CERTI DIRITTI HA INCONTRATO CONSOLE GENERALE SPAGNA'
date: Thu, 25 Mar 2010 08:34:24 +0000
draft: false
tags: [Comunicati stampa]
---

In occasione dell'incontro con il Console Generale del Regno di Spagna Emilio Fernández-Castaño organizzato dal Centro di Documentazione Europea (CDE) e la Biblioteca di Scienze Politiche 'Enrica Collotti Pischel' sul tema «Il Semestre di Presidenza spagnolo del Consiglio dell Unione europea », l'Associazione Radicale Certi Diritti chiede la posizione della Presidenza spagnola sul non riconoscimento delle coppie dello stesso sesso nelle sole Italia e Grecia, alla luce del Trattato di Lisbona e della Carta di Nizza, che riconoscono il matrimonio come un diritto fondamentale da garantire a tutti i cittadini europei, lasciando agli Stati membri la sola facoltà di stabilire le forme nelle quali garantire tale diritto.

**Il Console Generale del Regno di Spagna**, pur non volendo esprimersi sul merito della legislazione italiana, ammette che «il diritto di famiglia è uno degli elementi della vita della società e del cittadino europeo che non è ancora totalmente né armonizzata né integrata». Dopo aver individuato le ragioni di questo stato di cose in un'Europa «formata da degli ordinamenti giuridici nazionali e da autorità giudiziarie nazionali», Emilio Fernández-Castaño **afferma che «sul piano del diritto di famiglia questa assenza di coordinazione è un problema»**, come dimostra, per esempio, l'alta frequenza di dispute consolari per l'affidamento dei minori. **«Si vede - continua il Console Generale del Regno di Spagna - che ancora non siamo a un livello soddisfacente nel campo di diritti che sono molto importanti»**.

E' evidente come l'anomalia italiana crei gravi problemi anche a livello europeo minando, tra l'altro, la libera circolazione dei cittadini europei garantita dal trattato di Schengen che pure l'Italia ha sottoscritto. Speriamo che la situazione possa cambiare già all'indomani del 12 aprile, quando la Corte Costituzionale deciderà sulla costituzionalità del divieto di sposarsi opposto alle coppie dello stesso sesso.