---
title: 'Grave imporre divorzio a coppia perchè partner cambia sesso'
date: Thu, 16 Jun 2011 13:35:27 +0000
draft: false
tags: [Diritto di Famiglia]
---

**Imporre il divorzio contro la volontà della coppia perchè uno dei due ha cambiato sesso viola i diritti civili e umani della persona. Gravissima la decisione della Corte d'Appello di Bologna. **

**Roma, 16 giugno 2011**

Comunicato Stampa dell’Associazione Radicale Certi Diritti:

E’ gravissimo quanto deciso dalla Corte d’Appello di Bologna che ha imposto ad una coppia di Bologna il divorzio perché uno dei due partner ha cambiato sesso. Questo è  un evidente caso di violazione dei diritti civili e umani della persona.  Ci sono tutti i presupposti per presentare un ricorso alla Corte Europea dei Diritti dell’Uomo contro l’Italia.

   
Vogliamo esprimere alla coppia di Bologna tutta la nostra vicinanza e solidarietà.  
La nostra Associazione ha già incardinato alcune iniziative giudiziarie  alla Corte Europea dei Diritti dell’Uomo a causa di evidenti discriminazioni nei confronti di coppie dello stesso sesso e offrirà il suo aiuto e supporto anche alla coppia di Bologna.

   
La strada per la piena uguaglianza dei diritti deve andare avanti. Il tentativo di Stato di ‘normalizzazione’ della coppia è un atto impositivo  gravissimo e del tutto inacettabile.”.