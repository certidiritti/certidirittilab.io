---
title: 'Come si fa a escludere tra i reati d''odio quelli contro omosessuali e transessuali? Pericoloso e incomprensibile accordo sulla sicurezza internazionale: perché l''OSCAD lo ha sottoscritto?'
date: Thu, 30 May 2013 13:00:06 +0000
draft: false
tags: [Politica]
---

Comunicato stampa dell'Associazione radicale Certi Diritti

Roma, 30 maggio 2013

E' un autentico accordo contro natura quello che pare sia stato siglato tra diversi organismi di sicurezza, patrocinato dall'OCSE e dall'OSCAD e siglato all'Università cattolica di Milano (coincidenza?) in merito ai reati d'odio. Con una motivazione ridicola sono stati esclusi i reati derivanti da odio contro le persone omosessuali e transessuali tra tutti i reati di odio che saranno oggetto di studio e collaborazione tra i diversi paesi firmatari.

La motivazione sarebbe nel non accordo tra tutti i paesi nel considerare questi reati come sensibili per la sicurezza internazionale, così come la mancanza di una posizione comune sulla definizione stessa di reato d'odio a causa di transfobia e omofobia.

Possibile che OCSE e OSCAD abbianno accettato una tale vegognosa esclusione? Quali Paesi hanno messo il veto? E come mai l'OCSE lo ha accettato visto che nei suoi documenti e nelle sue posizioni ufficiali ha sempre, giustamente, incluso trai reati di odio anche quelli contro le persone omosessuali e transessuali?

Si tratta di una esclusione immotivata, e pericolosa per gli sviluppi che può avere, in tutta Europa (soprattitto nei paesi dell'Est dove le violenze sono all'ordine del giorno) con gravi ripercussioni anche a livello nazionale ove comincia, molto a fatica, la discussione sui progetti di legge contro omofobia e transfobia.

Con una lettera aperta al Governo e al Parlamento l'associazione radicale certi diritti chiede un imemdiato intervento di chiarificazione, anche sulla posizione italiana in materia.

**[Link alla lettera](documenti/download/doc_download/76-letterareatidiodio)**

[**Addendum alla lettera**](documenti/download/doc_download/81-addendum-lettera-reati-dodio)