---
title: 'Cancellazione rassegna cinema Queer Univestà di Roma, Consiglieri regionali di 5 gruppi scrivono a Rettore, Prefetto e Questore'
date: Tue, 08 Jun 2010 10:05:04 +0000
draft: false
tags: [Senza categoria]
---

**CANCELLAZIONE RASSEGNA CINEMA QUEER ALL’UNIVERSITA’ DI ROMA, CONSIGLIERI REGIONALI RADICALI E DI ALTRI QUATTRO GRUPPI SCRIVONO AL RETTORE FRATI, AL PREFETTO, AL QUESTORE: RIPRISTINARE SUBITO LA RASSEGNA.**

**Di seguito il testo della lettera inviata oggi al Rettore dell’Università di Roma, al Prefetto e al Questore di Roma:**

Roma, 8 giugno 2010

Egregi Signori,  
  
riteniamo gravissima la decisione di impedire lo svolgimento della rassegna cinematografica sulla tematica “Queer” patrocinata dalla Provincia di Roma e che si sarebbe dovuta inaugurare oggi negli spazi dell’Università La Sapienza di Roma. Leggiamo dai giornali di oggi che la Digos ha formalmente smentito le motivazioni riferite dal Rettore dell’Università  -  relative a supposti problemi di ordine pubblico – a giustificazione dell’annullamento dell’evento.  
In considerazione di quanto sopra, ed atteso il venir meno di qualsiasi motivazione a sostegno dell’intervenuta decisione di annullare la manifestazione, con la presente siamo a chiedere che la stessa venga immediatamente ripristinata.  
  
Distinti saluti.  
  
Giuseppe Rossodivita Capogruppo Lista Bonino Pannella, Federalisti Europei;  
Rocco Berardo, Consigliere regionale Lista Bonino Pannella, Federalisti Europei;  
Angelo Bonelli, Capogruppo dei Verdi;  
Filiberto Zaratti Sinistra e Libertà;  
Tonino D'Annibale Gruppo Pd;  
Ivano Peduzzi Federazione della Sinistra