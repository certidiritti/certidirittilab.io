---
title: 'ALTRE ADESIONI ALLA FIACCOLATA: PARTECIPERA''  ANCHE VLADIMIR LUXURIA'
date: Thu, 04 Dec 2008 13:48:05 +0000
draft: false
tags: [Comunicati stampa]
---

DEPENALIZZAZIONE OMOSESSUALITA' ONU: IL VATICANO ALLEATO DEI PAESI DITTATORIALI ISLAMICI. SABATO 6 DICEMBRE FIACCOLATA SIT-IN IN PIAZZA PIO IX DELLE ASSOCIAZIONI LAICHE E LGBT. TRA I PARTECIPANTI ANCHE VLADIMIR LUXURIA.

Comunicato Stampa dell' Associazione Radicale Certi Diritti

Sabato 6 dicembre dalle ore 17 si terrà una fiaccolata sit-in in Piazza Pio IX, nel territorio italiano confinante con il Vaticano contro la grave decisione del Vaticano di non sostenere la depenalizzazione dell'omosessualità all'Assemblea Generale dell'Onu.

Il Vaticano contribuisce così alla persecuzione delle persone omosessuali. In oltre 80 paesi del mondo l'omosessualità è considerata un reato, in 9 paesi è prevista la pena di morte. Alla manifestazione, promossa dall'Associazione Radicale Certi Diritti, Arcigay e Arcilesbica, parteciperanno anche le più importanti associazioni lgbt italiane, tra queste DigayProject, Circolo Mario Mieli, GayLib oltre che esponenti della società civile, deputati e senatori radicali del Pd con Radicali Italiani, Associazione Luca Coscioni e Nessuno Tocchi Caino.

Ha oggi preannunciato la sua partecipazione al Sit-in anche Vladimir Luxuria.