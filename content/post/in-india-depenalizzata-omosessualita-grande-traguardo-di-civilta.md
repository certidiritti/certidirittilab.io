---
title: 'IN INDIA DEPENALIZZATA OMOSESSUALITA'', GRANDE TRAGUARDO DI CIVILTA'''
date: Thu, 02 Jul 2009 12:54:51 +0000
draft: false
tags: [Comunicati stampa]
---

INDIA: CON DEPENALIZZAZIONE OMOSESSUALITA’ RAGGIUNTO UN IMPORTANTE TRAGUARDO DI CIVILTA’. ORA E’ IL TURNO DI ALTRI 79 PAESI. IN CINQUE DI QUESTI E’ PREVISTA LA PENA DI MORTE.

**_Comunicato Stampa dell’Associazione Radicale Certi Diritti:_**

Da oggi, per oltre un miliardo di persone nel mondo, l’omosessualità non è più un reato. Questo importantissimo traguardo lo ha raggiunto oggi l’India che si sottrae così dall’elenco dei paesi che perseguono gli atti omosessuali. Sono quindi rimasti 79 paesi in tutto il mondo che per legge perseguono gli omosessuali. In cinque di questi è prevista la pena di morte. E’ la prima volta che l’Alta Corte dell’India cancella una legge del 1861, ereditata dal periodo della colonizzazione inglese. I rapporti considerati di ‘sesso contro natura’ erano puniti con il carcere fino a 10 anni da una legge chiamata ‘Sezione 377’ che, seppur non applicata, era l’alibi delle forze dell’ordine per perseguire, in alcuni casi, le persone omosessuali.

Il ricorso davanti al tribunale di New Delhi contro la legge ‘Sezione 377’ era stato presentato dalla Naz Foundation, un gruppo gay che si batte per i diritti degli omosessuali e la lotta all'Aids, trovando la dura opposizione dei leader delle comunita' musulmane e cristiane, guarda caso, che giudicano i rapporti omosessuali sempre "contro natura".