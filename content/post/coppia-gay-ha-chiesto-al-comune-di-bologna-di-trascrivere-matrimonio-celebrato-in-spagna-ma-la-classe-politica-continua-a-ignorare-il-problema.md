---
title: 'Coppia gay ha chiesto al Comune di Bologna di trascrivere matrimonio celebrato in Spagna ma la classe politica continua a ignorare il problema'
date: Tue, 30 Aug 2011 20:47:55 +0000
draft: false
tags: [Diritto di Famiglia]
---

Il comune non ha ricevuto Ottavio e Joaquin, che insieme a parenti ed amici hanno quindi ‘occupato’ simbolicamente la Sala Rossa dove solitamente si svolgono i matrimoni con rito civile prima di recarsi presso gli uffici anagrafici del Comune per presentare per la richiesta di trascrizione del matrimonio fatto in Spagna.  
   
Questa iniziativa fa parte della Campagna di Affermazione Civile. Dopo il comunicato un documento spiega in cosa consiste >

Bologna, 29 agosto 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Questa mattina la coppia gay che si è sposata lo scorso 20 agosto in Spagna, accompagnata da amici, parenti, personalità politiche si è recata al Comune di  Bologna per essere ricevuta dal Sindaco per chiedere la trascrizione del loro matrimonio nei registri del Comune, così come previsto quando si contrae matrimonio all’estero.

La coppia composta da Ottavio Marzocchi, bolognese, funzionario a Bruxelles del Parlamento Europeo, membro del Consiglio Generale del Partito Radicale Nonviolento, responsabile delle questioni europee di Certi Diritti e Joaquin Nogueroles Garcia, funzionario della Commissione Europea, era accompagnata da amici, numerosi parenti venuti appositamente dalla Spagna,  Sergio Rovasio e Giacomo Cellottini, Segretario e Tesoriere di Certi Diritti, Emiliano Zaino, Presidente di Arcigay Il Cassero, Monica Mischiatti, Presidente di Radicali Bologna e Zeno Gobetti, Segretario dell’Associazione Radicale Giorgiana Masi, Franco Grilini, Consigliere Regionale Idv ed esponente storico del movimento lgbt, Emiliano Zaino, Presidente Arcigay Il Cassero di Bologna, Cathy La Torre, Capogruppo Sel al Consiglio Comunale di Bologna,  Carlo Santacroce, Presidente di 3D.

Nessun rappresentante politico della Giunta comunale ha ricevuto la delegazione che ha quindi ‘occupato’ simbolicamente la Sala Rossa dove solitamente si svolgono i matrimoni con rito civile. Dopo aver fatto un po’ di foto e atteso che almeno un funzionario ricevesse la delegazione la coppia si è recata presso gli uffici anagrafici del Comune di Bologna dove alcuni funzionari, molto gentili, hanno spiegato quali documenti occore presentare per la richiesta di trascrizione del matrimonio fatto in Spagna. I funzionari hanno anche spiegato che in base ad una Circolare Amato del Ministero degli Interni non sarà possibile comunque fare la trascrizione e che ne verrà data comunicazione scritta alla coppia sposata.

L’Associazione Radicale Certi Diritti assisterà la coppia che ha già preannunciato di voler fare ricorso, in caso di diniego del Comune, alla Corte Europea dei Diritti dell’Uomo e alla Corte di Giustizia Europea per il mancato rispetto dei Trattato di Nizza, sulla libera circolazione dei cittadini all’interno dell’Ue e del Trattato di Lisbona sulla lotta alle discriminazioni anche in base all’orientamento sessuale.

Di seguito una nota esplicativa della campagna di Affermazione Civile alla quale hanno aderito decine di coppie in tutta Italia.

Per ricevere la NewsLetter di Certi Diritti è sufficiente registrarsi qui:  
www.certidiritti.it/newsletter/newsletter  
   
  
  

**ASSOCIAZIONE RADICALE CERTI DIRITTI  & AFFERMAZIONE CIVILE  
**

**La nascita**

Affermazione Civile è una campagna nazionale di iniziativa giuridica volta al riconoscimento del matrimonio civile per le coppie dello stesso sesso.

Il lancio della campagna risale all’Aprile del 2008, per opera di due associazioni:

1) “Associazione radicale Certi Diritti” che ha sostenuto lo sviluppo della campagna, mantenuto i rapporti con le coppie coinvolte e sensibilizzato l’opinione pubblica con iniziative politiche, culturali, e di partecipazione popolare.

 2) “Rete Lenford – Avvocatura per i diritti LGBT”, una associazione di avvocati che ha definito la strategia giuridica dell’iniziativa e ha difeso davanti ai tribunali le coppie che hanno aderito alla campagna.

**Il metodo**

La campagna si è sviluppata cercando su tutto il territorio italiano coppie di persone dello stesso sesso intenzionate a richiedere la pubblicazione degli atti di matrimonio negli uffici preposti. Ottenuto il prevedibile rifiuto in forma di documento ufficiale (ai sensi dell’articolo 98 del Codice Civile) gli avvocati preposti dalla Rete Lenford lo hanno impugnato davanti ai tribunali.

Con l’aiuto di Certi Diritti, tutte le coppie coinvolte sono state scrupolosamente assistite, prima nel percorso di comprensione dell’iniziativa, poi nel percorso giuridico delle sentenze. Solo chi lo desiderava, veniva coinvolto in iniziative di sensibilizzazione dell’opinione pubblica: comunicati stampa e articoli di giornale, partecipazione a conferenze stampa, testimonianze in convegni sul tema del diritto di famiglia, partecipazioni ad eventi di piazza.

Più genericamente la campagna si pone all’interno di un più ampio progetto di Riforma del Diritto di famiglia che vede coinvolti i temi più svariati argomenti famigliari (piena uguaglianza delle donne nei vincoli famigliari, parità di diritti per i figli nati fuori dal matrimonio, adozioni, varietà delle forme di famiglia riconosciute, testamento biologico, divorzio breve, violenza domestica ed altro ancora).

**I primi risultati**

Nel tempo utile per la campagna sono state coinvolte circa 25 coppie sul vario territorio nazionale (prevalentemente centro e nord italia),

Le cause relative alla campagna hanno raggiunto circa 13 circondari di tribunale su 166, almeno 6 corti d’appello su 26. Non abbiamo ancora avuto una sentenza di cassazione.

Alle udienze si sono aggiunti numerosi corsi di formazione per avvocati, conferenze accademiche e convegni culturali sul tema. Visto il panorama culturale – nelle aule di tribunale così come nella politica e nella società – consideriamo positivo ed importante l’impatto delle sentenze e di queste iniziative.

Ben 4 cause sono state rinviate direttamente alla Corte Costituzionale senza attendere il massimo grado di giudizio.

Il clima positivo che si è generato alla notizia di una sentenza della Corte costituzionale su un tema così poco affrontato dalla politica ha facilitato la nascita di un Comitato nazionale per il riconoscimento del matrimonio civile tra persone dello stesso sesso, “Sì, lo voglio”.

La sentenza 138/2010 della Corte Costituzionale

La sentenza della Corte Costituzionale 138/2010 del 14 aprile 2010 non riconosce nell’immediato il diritto al matrimonio civile come da noi richiesto. Allo stesso tempo non lo esclude a priori, e lancia dei chiari messaggi positivi:

1) ha riconosciuto la dignità costituzionale delle unioni omosessuali

2) sollecita il Parlamento a varare una disciplina di carattere generale in materia

3) parifica i diritti delle coppie omosessuali ai diritti già riconosciuti alla coppie more uxorio eterosessuali

4) si riserva d’intervenire in futuro per garantire un trattamento omogeneo tra le coppie coniugate e quelle omosessuali

5) bandisce il comportamento omofobico

La sentenza 138/2010 rappresenta in sè un traguardo importante, poiché per la prima volta  la Repubblica Italiana si è espressa in merito al diritto di famiglia per le coppie dello stesso sesso.

Lo spirito della sentenza è stato confermato da una più recente sentenza della Corte Europea dei diritti dell’uomo, (Schalk & Kopf vs Austria, 24 Giugno 2010) che, tra l’altro, afferma esplicitamente che le famiglie costituite da coppie dello stesso sesso sono famiglie a tutti gli effetti.

**I prossimi passi: Affermazione Civile 2.0**

Da Novembre scorso, quando abbiamo rilanciato la nuova fase di affermazione civile, abbiamo avviato una collaborazione con alcune coppie. Perciò vale la pena tirare un po’ le somme di questi mesi passati a vedere insieme che tipo di lavoro abbiamo impostato:

1)      DIRITTO AL MATRIMONIO: La richiesta per il riconoscimento del diritto al matrimonio è stato portato di fronte alla Corte Europea dei Diritti dell’Uomo, come annunciato già lo scorso Giugno. Sono due le coppie che hanno partecipato a questo ricorso, guidato da Marilisa D’Amico, professore ordinario di Diritto Costituzionale, e Massimo Clara. Al momento siamo in attesa che il ricorso venga accettato dalla CEDU e in tal caso ci prepareremo per l’udienza.

2)      RICONOSIMENTO DELLA NAZIONALITA’ DEI FIGLI NON BIOLOGICI / NON PARTORITI: Dalla Spagna, abbiamo due famiglie i cui figli non si vedono riconosciuta la cittadinanza italiana, essendo la madre italiana la madre non biologica, oppure la madre biologica che però non ha partorito il figlio. In questo caso, non passeremo attraverso la “classica” via di impugnare il diniego che queste coppie hanno ricevuto dal consolato. Invece stiamo procedendo appellandoci ai tribunali spagnoli. Nell’ipotesi che il ricorso sarà accolto, provvederemo poi a richiedere allo stato italiano di rispettarlo, in virtù delle convenzioni europee sul  riconoscimento delle sentenze emesse dai tribunali di altri stati. Ricorso seguito dal nostro iscritto a Barcellona, Avvocato Ugo Millul in collaborazione con l’avvocatessa Gemma Calvet i Barot.

3)      RICONGIUNGIMENTO FAMIGLIARE: Abbiamo poi alcune coppie in cui uno dei partner non è cittadino europeo, e nonostante matrimonio/ unione civile contratta all’estero e nonostante il cittadino non europeo goda già di un  permesso di soggiorno europeo di lungo periodo rilasciato da altri stati europei, in italia non viene concesso il ricongiungimento famigliare, né viene riconosciuto il permesso di soggiorno di lungo periodo rilasciato da altri stati europei (e in teoria validi su tutto il territorio europeo). Ricorso seguito da Gabriella Friso, esperta nei servizi per gli immigrati.

**Sono in fase di elaborazione altri ricorsi:**

4)      TRASCRIZIONE DEL MATRIMONIO CONTRATTO ALL’ESTERO: Ottavio Marzocchi, socio fondatore e rappresentante di Certi Diritti a Bruxelles, si è unito in matrimonio in Spagna con un cittadino spagnolo. A Bologna è appena stata fatta una manifestazione in seguito al diniego di trascrizione in Italia del suo matrimonio ed è in fase di studio un ricorso per il mancato riconoscimento del diritto al matrimonio.

5)      DIRITTO ALLA FECONDAZIONE ETEROLOGA: Ricorso che verrà effettuato insieme ad altre coppie eterosessuali sul diritto alla fecondazione eterologa, attualmente negata in Italia con una pena prevista di una multa fino ad un milione di Euro. Ricorso seguito dall’avvocatessa Filomena Gallo, Vice Segretario Associazione Luca Coscioni per la libertà di ricerca scientifica.

6)      DIRITTO ALLA VITA FAMIGLIARE: Ricorso che verrà effettuato insieme ad altre coppie eterosessuali sul diritto al riconoscimento della vita famigliare per le coppie non sposate che conducono dei progetti di vita stabili. Ricorso seguito dall’avvocatessa Filomena Gallo, Vice Segretario Associazione Luca Coscioni per la libertà di ricerca scientifica.

Le possibilità di altri nuovi ricorsi, sono potenzialmente infiniti, e ovviamente dipendono dalla dalla disponibilità delle famiglie italiane ad intraprendere una causa pilota sulle discriminazioni subite. Crediamo che, continuando sulla rivendicazione esplicita del diritto al matrimonio, sono molto importanti anche i ricorsi sugli specifici diritti, in particolare anche il diritto alla pensione di reversibilità, specialmente in seguito alla sentenza della Corte di Giustizia Europea sul caso Romer (Grand Chamber 10.05.2011).

Il contesto generale

L’Italia è una delle poche nazioni (insieme a Grecia e Polonia) dell’Unione Europea – e l’unica tra le sue fondatrici – a non avere alcun riconoscimento delle famiglie costituite da coppie dello stesso sesso. E nemmeno nessun riconoscimento delle coppie non sposate anche di sesso opposto. Una palese discriminazione e un anacronismo infausto che devono essere superati senza indugi alcuni.

Siamo convinti che il matrimonio civile sottende al più basilare principio di uguaglianza, e dà un efficacissimo supporto anche alla lotta contro l’omofobia. Ogni altro riconoscimento intermedio (delle efficaci unioni civili, ad esempio) per noi costituirà un’importante conquista strategica verso questo obiettivo finale.

Inoltre, considerata lo stretto legame con lo Stato vaticano, è anche vero che finché l’Italia continuerà a porsi sullo scacchiere della diplomazia internazionale come capofila degli stati contrari al riconoscimento dei pieni diritti civili per le persone e le famiglie LGBT, anche la criminalizzazione dell’omosessualità nel mondo continuerà ad avere terreno fertile. Ne sono prova le ripetute esternazioni del Vaticano all’ONU che si è sempre opposto strenuamente alla depenalizzazione dell’omosessualità nel mondo. Impegnarsi per il riconoscimento del diritto al matrimonio aiuta l’Italia deve smarcarsi in maniera netta da queste posizioni, e isolare il Vaticano in queste sue posizioni disumane e criminofile.

Tutto il nostro lavoro è svotlo in maniera trasparente, senza adesioni a logge massoniche segrete. La nostra forza è la piena fiducia nelle istituzioni democratiche, e la consapevolezza di aver dato, in questi anni di lavoro, prova di serietà e credibilità.

**Sostieni la nostra campagna**  
**www.certidiritti.it/iscriviti**