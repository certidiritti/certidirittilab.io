---
title: '2008-0140-Discrimination-OP-SOC-220410-ST08887-RE01.EN10'
date: Tue, 11 May 2010 21:00:00 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 12 May 2010

Interinstitutional File:

2008/0140 (CNS)

8887/1/10

REV 1

LIMITE

SOC 283

JAI 332

MI 111

  

  

  

  

  

OUTCOME OF PROCEEDINGS

from :

The Working Party on Social Questions

on :

22 April 2010

No. prev. doc. :

8173/10 SOC 240 JAI 270 MI 94

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

**I.         INTRODUCTION**

At its meeting on 22 April 2010, the Working Party continued its examination of the above proposal on the basis of a set of Presidency drafting suggestions[\[1\]](#_ftn1). Delegations broadly welcomed the Presidency's attempt to clarify the scope of the draft Directive, including with respect to the provisions concerning housing. However, the discussion also revealed the need for extensive further work.

**II.        MAIN ITEMS DISCUSSED**

The discussion focused on Articles 3, 4, 4a, 4b and 15 and the corresponding recitals.

**Scope**

**\- "Access to" (Article 3(1))**

UK, supported by CZ, LT and MT suggested clarifying the delimitation of the scope by reintroducing the words "access to" in Article 3(1)[\[2\]](#_ftn2).

**\- Fields excluded from the scope (Article 3(2))**

The Presidency had simplified the wording of the chapeau, removing the explicit mention of the fact that the Directive did not alter the division of competences between the EU and its Member States. CZ, FR and UK preferred the chapeau as worded in doc 6092/10, UK stressing that the new wording made a material difference to the delineation of the scope, and FR recalling that Case C-144/04 "Mangold" had extended EC competences. FR entered a reservation. Cion supported the current wording. MT entered a scrutiny reservation on Article 3(2).

\- "**Social housing" (Articles 3(1)(a) and 4(6) and Recital 17b)**

The Presidency's text sought to clarify the scope by including an explicit reference to "social housing". This was welcomed by many delegations (BE, BG, CY, DK, FR, LU, AT, NL, PL, PT, FI, SK, SE, UK) as a step in the right direction.

In this context, BE, BG, DK, IE, IT, LT, MT, FR, NL, PL and SK also saw a need to clarify what was meant by "social housing", for example, by means of a definition, BG, ES, NL and SE expressing the view that defining this concept at the national level might be most appropriate. Cion, similarly, made the observation that adding a reference to "social housing" would be logical and consistent with the scope of Directive 2000/43/EC.

  

LU made the point that the scope of the Directive should be defined in the context of the provision of a service.

BE, IT and LT entered scrutiny reservations.

DE expressed the view that including social housing within the scope violated the principle of subsidiarity. CZ, LT and HU were also unable to accept the inclusion of the words "social housing".

FI and UK wondered whether all relevant types of housing were covered by the terms "private and social housing" Article 4(6). PL reiterated its view that the _use_ of the building should be the determining factor, and EE and UK reiterated the opinion that a case-by-case approach was more appropriate than a sweeping obligation to make _all_ housing accessible. ES stressed that accessibility measures were a way of obviating the need to take reasonable accommodation measures later.

More discussion will be needed on the above issues, as well as on other aspects of the scope, including "social protection" (Article 3(1)(a)), "education" (Article 3(1)(c))[\[3\]](#_ftn3), "benefits dependent on family law" (Article 3(2)), "infrastructure" (Article 4b(1)(d)) and "the design and manufacture of goods" (Article 4b(2)).

LU pointed out that nearly all the outstanding difficulties voiced by delegations were related to the measures concerning existing buildings, facilities and infrastructure. LU suggested therefore examining the question as to whether the reference to "all other existing buildings, facilities and infrastructure" in Article 15(2) was conducive to unanimity at the Council level.

**The disability provisions (Articles 4, 4a and 4b)**

Certain delegations (EE, NL and UK) saw a need to clarify the precise scope of the disability provisions with respect to "physical environment" and "infrastructure"[\[4\]](#_ftn4). Practical examples of situations where the obligations stemming from the Directive were unclear included the interface between a bus and a bus stop and hiking trails, restaurants etc. located in natural settings. NL supported by BG, FR and LT suggested excluding "public space" from the scope.

CZ reiterated its concern with respect to the use of the terms "access" and "accessibility", which, it felt, should be distinguished more clearly. (See United Nations Convention on the Rights of Persons with Disabilities (UNCRPD), Article 9, where the term "accessibility" appears in the title, but the term used in the first paragraph is "access".) Cion undertook to reflect on the issue. CZ also made the point that refusing "access" due to a "disproportionate burden" was not justified (see Article 4(1) and Recital 19b).

**The implementation calendar (Article 15)**

Several delegations reiterated the view that longer implementation periods were needed, particularly with respect to existing housing etc.

Certain delegations (MT, FI) called for clarification with respect to the interrelationship between the implementation date (Article 15(1)) and the dates in Article 15(2).

\* \* \*

DE reaffirmed its general reservation on the proposal.

  

LT also reiterated its doubts regarding the need for the proposal, on the grounds that it encroached on national competences and conflicted with the principles of subsidiarity and proportionality.

All delegations have maintained scrutiny reservations on all the new suggestions and general scrutiny reservations on the proposal. CZ, DK, FR, MT and UK have maintained parliamentary scrutiny reservations, CY and PL maintaining linguistic scrutiny reservations. The Commission has meanwhile affirmed its original proposal at this stage and has maintained a scrutiny reservation on any changes thereto. Delegations' general positions are summed up in doc. 14008/09; see also doc. 16063/09 + ADD 1, doc. 5790/10 + COR 1 and doc. 6847/10.

Further details of the discussion are set out in the footnotes in the Annex. Changes in relation to the previous version (doc. 8173/10) are set out as follows: new text is in **bold** and deletions are marked **\[…\]**.

**III.    CONCLUSION**

The Presidency undertook to table a set of new drafting suggestions on the basis of the discussion. The next meeting of the Working Party is scheduled for 3 May 2010.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Recitals 17a-h and 19b-20c**

(17a) This Directive covers the application of the principle of equal treatment in the fields of social protection[\[5\]](#_ftn5), education[\[6\]](#_ftn6) and access to[\[7\]](#_ftn7) goods and services within the limits of the competences of the European Union.

The Member States are responsible for the organisation and content of systems of social protection, health care and education, as well as for the definition of who is entitled to receive social protection benefits, medical treatment and education.

(17b) Social protection includes social security[\[8\]](#_ftn8), social assistance, social housing  and health care. Consequently, this Directive applies with regard to rights and benefits which are derived from general or special social security, social assistance and healthcare schemes, which are  statutory  or provided either directly by the State, or by private parties in so far as the provision of those benefits by the latter is funded by the State. In this context, the Directive applies with regard to benefits  in cash, benefits in kind and services, irrespective of whether the schemes involved are contributory or non-contributory. The abovementioned schemes include, for example, \[access to\] the branches of social security defined by Regulation 883/2004/EC on the coordination of social security systems[\[9\]](#_ftn9), as well as schemes providing for benefits or services granted for reasons related to the lack of financial resources or risk of social exclusion.[\[10\]](#_ftn10)

(17c)

  

(17d) All individuals enjoy the freedom to contract, including the freedom to choose a contractual partner for a transaction. This Directive should not apply to economic transactions undertaken by individuals for whom these transactions do not constitute a professional or commercial activity.

In particular, this Directive does not apply to transactions related to housing which are performed by natural persons, when the activities in question do not constitute a professional or commercial activity.

In this context, the concept of professional or commercial activity may be defined in accordance with the national laws and practice of the Member States.

(17e) This Directive does not alter the division of competences between the European Union and the Member States in the areas of education and social protection. It is also without prejudice to the essential role and wide discretion of the Member States in providing, commissioning and organising services of general economic interest.

(17f)  The exclusive competence of Member States with regard to the organisation of their social protection systems includes decisions on the setting up, financing and management of such systems and related institutions as well as on the substance and delivery of benefits and health services and the conditions of eligibility. In particular Member States retain the possibility to reserve certain benefits or services to certain age groups or persons with disabilities. The Member States also retain their competences in respect of the **definition and the** organisation of their social housing services, including the management or allocation of such services and determining the conditions of eligibility[\[11\]](#_ftn11).

Moreover, this Directive is without prejudice to the powers of the Member States to organise their social protection systems in such a way as to guarantee their sustainability.

  

(17g) The exclusive competence of Member States with regard to the organisation of their educational systems and the content of teaching and of educational activities[\[12\]](#_ftn12), including the provision of special needs education, includes the setting up and management of educational institutions, the development of curricula and other educational activities and the definition of examination processes. In particular Member States retain the possibility to set age limits for certain education activities. However, there may be no discrimination in the access to educational activities, including the admission to and participation in classes or programmes and the evaluation of students' performance[\[13\]](#_ftn13).

(17h) This Directive does not apply to matters covered by family law including marital status and adoption, and laws on reproductive rights. It is also without prejudice to the secular nature of the State, state institutions or bodies, or education.

…

(19b) Measures to ensure accessibility for persons with disabilities, on an equal basis with others, to the areas covered by this Directive play an important part in ensuring full equality in practice. Such measures should comprise the identification and elimination of obstacles and barriers to accessibility, as well as the prevention of new obstacles and barriers. They should not impose a disproportionate burden.[\[14\]](#_ftn14)

  

(19c)    Such measures should aim at achieving accessibility including with regard to, _inter alia[\[15\]](#_ftn15)_, the physical environment, transportation, information and communication technology and systems, and services, within the scope of the Directive as defined in Article 3. The fact that access might not always be possible to achieve in full equality with others may not be presented as a justification for not adopting all measures to increase as far as possible accessibility to persons with disabilities.[\[16\]](#_ftn16)

(19d) Improvement of accessibility can be provided by a variety of means, including application of the "universal design" principle. According to the United Nations Convention on the Rights of Persons with Disabilities, “universal design” means the design of products, environments, programmes and services to be usable by all people, to the greatest possible extent, without the need for adaptation or specialised design. “Universal design” should not exclude assistive devices for particular groups of persons with disabilities where this is needed.

(20)   Legal requirements[\[17\]](#_ftn17) and standards on accessibility have been established at European level in some areas while Article 16 of Council Regulation 1083/2006 of 11 July 2006 on the European Regional Development Fund, the European Social Fund and the Cohesion Fund and repealing Regulation (EC) No 1260/1999[\[18\]](#_ftn18) requires that accessibility for disabled persons is one of the criteria to be observed in defining operations co-financed by the Funds. The Council has also emphasised the need for measures to secure the accessibility of cultural infrastructure and cultural activities for people with disabilities[\[19\]](#_ftn19).

(20a) In addition to general measures to ensure accessibility, individual measures to provide reasonable accommodation play an important part in ensuring full equality in practice for persons with disabilities to the areas covered by this Directive.

  

(20b) In assessing whether measures to ensure accessibility or reasonable accommodation would impose a disproportionate burden, account should be taken of a number of factors including**,** _inter alia_, the size and resources[\[20\]](#_ftn20) of the organisation or enterprise, as well as the estimated costs of such measures. A disproportionate burden would arise, for example, where significant structural changes would be required in order to provide access to movable or immovable property which is protected under national rules on account of their historical, cultural, artistic or architectural value.

(20c) The principle of accessibility is established in the United Nations Convention on the Rights of Persons with Disabilities. The principles of reasonable accommodation and disproportionate burden are established in Directive 2000/78/EC[\[21\]](#_ftn21) and the United Nations Convention on the Rights of Persons with Disabilities.

**Articles 3, 4, 4a, 4b and 15**

Article 3  
Scope

1.       Within the limits of the competences conferred upon the European Union, the prohibition of discrimination shall apply to all persons, as regards both the public and private sectors, including public bodies, in relation to[\[22\]](#_ftn22):

(a)     Social protection[\[23\]](#_ftn23), including social security, social assistance, social housing and healthcare:

  

(b)[\[24\]](#_ftn24)

(c)          Education[\[25\]](#_ftn25);

(d)     and the access to[\[26\]](#_ftn26) the  supply of goods and other services which are available to the public, including housing[\[27\]](#_ftn27).

Subparagraph (d) shall apply to natural persons only insofar as they are performing a professional or commercial activity defined  in accordance with national laws and practice.

2.[\[28\]](#_ftn28) Notwithstanding paragraph 1, this Directive does not apply to:

(a)     matters covered by family law, including marital status and adoption, \[and the benefits dependent thereon\][\[29\]](#_ftn29), as well as  laws on reproductive rights**;**

(b)     the organisation of Member States' social protection systems, including decisions on the setting up, financing and management of such systems and related institutions as well as on the substance and delivery of benefits and services and the conditions of eligibility;

(c)     the competences of the Member States to determine the type of health services provided and the conditions of eligibility;

  

(d)[\[30\]](#_ftn30) the organisation and funding of the Member States' educational systems, including the organisation of education for people with special needs, the content of teaching and activities within educational institutions, and the conditions of eligibility[\[31\]](#_ftn31);

3.[\[32\]](#_ftn32) Member States may provide that differences of treatment based on a person's religion or belief in respect of admission to educational institutions, the ethos of which is based on religion or belief, in accordance with national laws, traditions and practice, shall not constitute discrimination.

These differences of treatment shall not justify discrimination on any other ground referred to in Article 1.

3a.     This Directive is without prejudice to national measures authorising or prohibiting the wearing of religious symbols.

4.       This Directive is without prejudice to national legislation ensuring the secular nature of the State, State institutions or bodies, or education, or concerning the status and activities of churches and other organisations based on religion or belief.

5.       This Directive does not cover differences of treatment based on nationality and is without prejudice to provisions and conditions relating to the entry into and residence of third-country nationals and stateless persons in the territory of Member States, and to any treatment which arises from the legal status of the third-country nationals and stateless persons concerned.

Article 4

Accessibility for persons with disabilities

1.             Member States shall take the necessary and appropriate measures to ensure accessibility for persons with disabilities, on an equal basis with others, within the areas set out in Article 3. These measures should not impose a disproportionate burden.[\[33\]](#_ftn33)

1a (new) Accessibility includes \[general anticipatory measures\] to ensure the effective implementation of the principle of equal treatment in all areas set out in Article 3 for persons with disabilities, on an equal basis with others\[, and with a medium or long-term commitment\] [\[34\]](#_ftn34).

2.             Such measures shall comprise the identification and elimination of obstacles and barriers to accessibility, \[as well as the prevention of new obstacles and barriers\][\[35\]](#_ftn35) in the areas covered in this Directive.

3.

4.

5.

  

6\. (new)   Paragraphs 1 and 2 shall apply to private and social housing[\[36\]](#_ftn36) only as regards the common parts[\[37\]](#_ftn37) of buildings with more than one housing unit. This paragraph shall be without prejudice to Article 4(7) and Article 4a.

7\. (new) [\[38\]](#_ftn38) Member States shall \[progressively take the necessary measures\] [\[39\]](#_ftn39) to ensure that sufficient private and / or social housing is accessible[\[40\]](#_ftn40) for people with disabilities.

_Article 4a  
Reasonable accommodation for persons with disabilities_

1.       In order to guarantee compliance with the principle of equal treatment in relation to persons with disabilities, reasonable accommodation shall be provided within the areas set out in Article 3, unless this would impose a disproportionate burden.

2.       Reasonable accommodation means necessary and appropriate modifications and adjustments where needed in a particular case**,** to ensure to persons with disabilities access on an equal basis with others.

3.

4.

_Article 4b (new)  
Provisions concerning accessibility[\[41\]](#_ftn41) and reasonable accommodation_

1.[\[42\]](#_ftn42) For the purposes of assessing whether measures necessary to comply with Articles 4 and 4a would impose a disproportionate burden, account shall be taken, in particular, of:

a)             the size and resources of the organisation or enterprise[\[43\]](#_ftn43);

b)             the estimated cost[\[44\]](#_ftn44);

c)

d)            the life span[\[45\]](#_ftn45) of infrastructures[\[46\]](#_ftn46) and objects which are used to provide a service;

e)             the historical, cultural, artistic or architectural value of the movable or immovable property in question;[\[47\]](#_ftn47)

f)              whether the measure in question is impracticable or unsafe.[\[48\]](#_ftn48)

The burden shall not be deemed disproportionate when it is sufficiently remedied by measures existing within \[the framework of the disability policy of\][\[49\]](#_ftn49) the Member State concerned.

  

2.       Articles 4 and 4a shall apply to the design and manufacture[\[50\]](#_ftn50) of goods, unless this would impose a disproportionate burden. For the purpose of assessing whether a disproportionate burden is imposed in the design and manufacture of goods, consideration shall be taken of the criteria set out in article 4b(1).

3.       Articles 4 and 4a shall not apply where European Union law provides for detailed standards of specifications on the accessibility or reasonable accommodation[\[51\]](#_ftn51) regarding particular goods or services[\[52\]](#_ftn52).

…

Article 15[\[53\]](#_ftn53)

Implementation

1.       Member States shall adopt the laws, regulations and administrative provisions necessary to comply with this Directive by …. at the latest \[4[\[54\]](#_ftn54) years after adoption\]. They shall forthwith inform the Commission thereof and shall communicate to the Commission the text of those provisions.

  

When Member States adopt these measures, they shall contain a reference to this Directive or be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.

2.             In order to take account of particular conditions, Member States may, if necessary, establish that the obligation to ensure accessibility as set out in Articles 4 and 4b has to be complied with by, at the latest, \[5[\[55\]](#_ftn55) years after adoption\] regarding new buildings[\[56\]](#_ftn56), facilities, vehicles and infrastructure[\[57\]](#_ftn57), as well as existing[\[58\]](#_ftn58) buildings, facilities and infrastructure undergoing significant renovation[\[59\]](#_ftn59) and by \[20[\[60\]](#_ftn60) years after adoption\] regarding all other existing buildings, facilities, vehicles[\[61\]](#_ftn61) and infrastructure.

Member States wishing to use any of these additional periods shall inform the Commission at the latest by the date set down in paragraph 1 giving reasons. Member States shall also communicate to the Commission by the same date an action plan laying down the steps to be taken and the timetable for achieving the gradual implementation of Article 4 \[, including its paragraph 7\]. They shall report on progress every two years starting from this date.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) See doc. 8173/10 and explanatory note in doc. 8872/10. A note from NL was also distributed prior to the meeting (doc. 8711/10).

[\[2\]](#_ftnref2) See doc. 14896/08, p. 29.

[\[3\]](#_ftnref3) EL lifted its reservation on the inclusion of education within the scope.

[\[4\]](#_ftnref4) See doc. 8711/10.

[\[5\]](#_ftnref5) IE, MT and FR called for the deletion of "social protection".

[\[6\]](#_ftnref6) IE and UK reiterated their calls for education to be removed from the scope.

[\[7\]](#_ftnref7) UK suggested adding "and supply of". Cf. Directive 2000/43/EC.

[\[8\]](#_ftnref8) FI suggested "…social security, **social services and other forms of** social assistance…"

[\[9\]](#_ftnref9) OJ L 166, 30.4.2004, p. 1.

[\[10\]](#_ftnref10) MT suggested deleting the last sentence.

[\[11\]](#_ftnref11) CY considered this reference to "social housing" to be redundant. CZ asked for the term "allocation" to be clarified. FR entered a scrutiny reservation.

[\[12\]](#_ftnref12) EL suggested: "…including, **among others**, the provision of special needs education, **\[…\]** the setting up and management of educational institutions, the development of curricula and other educational activities , **the appointment and any issue concerning teachers' career development**…"

[\[13\]](#_ftnref13) UK expressed hesitation with respect of the inclusion of student evaluations in the scope. Cion explained that students should be protected against discriminatory evaluations.

[\[14\]](#_ftnref14) CZ suggested deleting the last sentence.

[\[15\]](#_ftnref15) IT, MT and NL expressed doubts regarding the examples cited. EE raised the question as to the extent of the concept of the physical environment.

[\[16\]](#_ftnref16) HU suggested mirroring the content of Recital 19c in the articles. MT entered a scrutiny reservation on Recital 19c.

[\[17\]](#_ftnref17) Regulation (EC) No. 1107/2006 and Regulation (EC) No 1371/2007.

[\[18\]](#_ftnref18) OJ L 210, 31.7.2006, p.25. Regulation as last amended by Regulation (EC) No 1989/2006 (OJ L 411, 30.12.2006, p.6).

[\[19\]](#_ftnref19) OJ C 134, 7.6.2003, p.7

[\[20\]](#_ftnref20) IT, MT and FI suggested reintroducing "nature".

[\[21\]](#_ftnref21) OJ L 303, 2.12.2000, p. 16.

[\[22\]](#_ftnref22) CZ, LT, MT and UK suggested reinserting "access to". See doc. 14896/08, p. 29.

[\[23\]](#_ftnref23) IE, MT and FR called for the deletion of "social protection".

[\[24\]](#_ftnref24) AT preferred to see "social advantages" included in the text. Cf. Directive 2000/43/EC.

[\[25\]](#_ftnref25) IE and UK reiterated their calls for education to be removed from the scope.

[\[26\]](#_ftnref26) FI and UK suggested "access to **and** supply". Cf. Directive 2000/43/EC.

[\[27\]](#_ftnref27) NL supported by IE and IT suggested adding "insofar as they are offered outside the area of private and family life". See doc. 6563/10. Cion pointed out that fundamental rights with respect to private and family life were already protected in the draft Directive.

[\[28\]](#_ftnref28) CZ, FR and UK preferred the chapeau as worded in doc 6092/10. FR entered a reservation. MT entered a scrutiny reservation on Article 3(2). Cion supported the current wording.

[\[29\]](#_ftnref29) BE, FR, NL, AT, FI and Cion suggested deleting the words in square brackets. LT, MT and PL, for their part, wished to keep these words, MT also calling for a reference to "public policy". FR and MT maintained reservations.

[\[30\]](#_ftnref30) AT entered a scrutiny reservation. CZ preferred the wording used in doc. 6092/10.

[\[31\]](#_ftnref31) BE, EE, FI and Cion saw a need to clarify the meaning of "conditions of eligibility". BE entered a scrutiny reservation. EL suggested: "the organisation **and funding** of the Member States' educational systems, **and other areas such as,** the provision of special needs education, the setting up and management of educational institutions, the development of curricula and other educational activities, the appointment and any issue concerning teachers' career development and the definition of examination processes **and** the **conditions of eligibility".**

[\[32\]](#_ftnref32) FR reiterated its view that Article 3(3) should be deleted, as it conflicted with the French constitution. Cion pointed out that the wording was non-binding ("_may_ provide").

[\[33\]](#_ftnref33) CZ suggested deleting the words in square brackets. LT saw a need for clarification.

[\[34\]](#_ftnref34) DK, EE, EL, DE, FR, IT, CY, HU, MT,  NL, RO and UK saw a need to clarify the practical implications of Article 4(1a), particularly with respect to the words in square brackets. CY entered a scrutiny reservation.

[\[35\]](#_ftnref35) NL suggested deleting the words in square brackets. EE, MT and UK saw a need for clarification. See also Recital 19b.

[\[36\]](#_ftnref36) Re. "social housing", see also Article 3(1)(a).

[\[37\]](#_ftnref37) DE, FR, LV, LT, AT, SE saw a need to clarify this term, FR and AT entering scrutiny reservations. BE and CZ pointed out that the individual flats themselves should be covered in relevant cases. Echoing this point, Cion underlined the Member States' competence for implementing the Directive in practice.

[\[38\]](#_ftnref38) CY suggested deleting this provision. DE, EE, LT, MT and PL also had doubts regarding the obligation being created. LT and NL suggested moving this provision to a Recital. Concurring, Cion pointed out that the UNCRDP contained a similar declaratory provision (Article 28), which could be cited in the recitals. AT entered a scrutiny reservation.

[\[39\]](#_ftnref39) DE, LV, LT, HU and PL called for clarification.

[\[40\]](#_ftnref40) PT suggested "adaptable".

[\[41\]](#_ftnref41) CZ suggested "access".

[\[42\]](#_ftnref42) Includes elements previously contained in Article 4a.

[\[43\]](#_ftnref43) IT, MT and FI reiterated their wish to see "the nature of the enterprise" reintroduced into the text.  Responding to MT, Cion explained that the Member States were free to decide whether to lay down rules on the size and resources of enterprises, or whether to leave the matter to the courts.

[\[44\]](#_ftnref44) EE, NL and Cion saw a need to mention the need for (or benefits of) measures as well as their cost. ES explained that "rights" were more relevant than the notion of "benefits" in the disability context. EE suggested adding "environmental impact" as a criterion.

[\[45\]](#_ftnref45) FR called for clarification.

[\[46\]](#_ftnref46) NL suggested deleting "infrastructures". EE supported by MT and UK suggested addressing the "life span" or recurring nature of certain services (e.g. a series of concerts as opposed to a one-off gig); Cion supported taking into account this aspect.

[\[47\]](#_ftnref47) AT suggested deleting this provision.

[\[48\]](#_ftnref48) EE, IT, NL, AT and UK suggested deleting this provision. PL also had doubts about the reference to "unsafe" measures. LV considered that the reference to "impracticable" measures could be useful for balancing costs and benefits.

[\[49\]](#_ftnref49) FI supported by LV and Cion suggested deleting the words in square brackets.

[\[50\]](#_ftnref50) DE saw a contradiction, detrimental to legal certainty, between "the access to and supply of goods and services" on the one hand, and "the design and manufacture of goods" on the other. UK, supported by IT, MT, SE, called for the extent and impact of this provision to be clarified, including with respect to liability issues and precise obligations created. UK maintained a scrutiny reservation. BG, CZ, EE and FI also expressed doubts. Should the issue of seeking to improve the supply of adaptable goods and services be included, FI suggested addressing it in Article 3. BG and CZ supported this perspective. ES and SK highlighted the crucial importance of design and manufacture for the improvement of accessibility.

[\[51\]](#_ftnref51) CZ preferred the earlier version of and questioned the inclusion of "reasonable accommodation".

[\[52\]](#_ftnref52) FI supported by FR, NL and SE provisionally suggested: "Articles 4 and 4a shall not apply **to areas** where European Union \[**and national*\]** law provides for detailed standards of specifications on the accessibility or \[**the requirement for****\] reasonable accommodation regarding particular goods or services**, including directives and regulations that provide for the assistance of disabled passengers**". (\* SE suggested adding the words in square brackets. ** NL suggested adding the words in square brackets.)

[\[53\]](#_ftnref53) HU, MT, PL and FI maintained scrutiny reservations, PL questioning the setting of deadlines.

[\[54\]](#_ftnref54) MT called for a longer period.

[\[55\]](#_ftnref55) BE preferred immediate applicability with respect to new buildings. EE, for its part, saw a need for a longer period. LV suggested 10 years.

[\[56\]](#_ftnref56) IT expressed the view that the Member States should be free to define "new buildings".

[\[57\]](#_ftnref57) IT expressed doubts with respect to the inclusion of vehicles and called for a specific transposition period for ICT and infrastructure.

[\[58\]](#_ftnref58) IT wished for reassurance that the "disproportionate burden" provision also applied to existing buildings.

[\[59\]](#_ftnref59) FI felt that more time was needed for buildings undergoing "significant renovation" and called for this concept to be clarified. LT also saw a need for a longer period (30 years).

[\[60\]](#_ftnref60) FR entered a reservation with respect to inter-urban transport and road infrastructure.

[\[61\]](#_ftnref61) BE, CZ, DE and HU expressed doubts regarding the feasibility of adapting existing vehicles. UK expressed the view that the provisions ought to apply to transport services, not vehicles.