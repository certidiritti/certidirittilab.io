---
title: 'DUE RAGAZZI GAY IRANIANI CONDANNATI ALLA LAPIDAZIONE. APPELLO DELLE ASSOCIAZIONI PER I DIRITTI CIVILI E UMANI'
date: Fri, 21 Jan 2011 08:51:42 +0000
draft: false
tags: [Comunicati stampa]
---

**Testo integrale della lettera inviata al Governo italiano e al Presidente della Commissione Europea.**

Roma, 20 gennaio 2011

In Iran nei giorni scorsi, due ragazzi di 20 e 21 anni, sono stati condannati alla lapidazione per aver registrato un video nel quale sarebbero protagonisti di un rapporto sessuale.

Il video è stato scoperto sul cellulare di Ayub e Mosleh dagli agenti nella regione del Kurdistan in Iran. Secondo diverse fonti pare che la lapidazione dei giovani ragazzi sia stata ordinata per venerdì 21 gennaio 2011.

La notizia è stata pubblicata da un giornale curdo e successivamente sarebbe stata comunicata dal comitato internazionale contro la lapidazione, che già ha lanciato una campagna mondiale contro la lapidazione di Sakineh Ashtiani condannata alla lapidazione per adulterio.

Gli attivisti lgbt(e) iraniani e le organizzazioni per i diritti umani hanno segnalato diversi casi di sentenze di condanne a morte per l’omosessualità negli ultimi due anni.

Secondo quanto dichiarato da Soheila Vahdati, difensore dei diritti umani per la Human Rights Watch (HRW), Amnesty International e l’Iranian Queer Organisation di San Francisco, in Iran non si fa distinzione tra stupro e rapporti omosessuali, i familiari di queste persone non si preoccupano di difendere i loro cari da queste brutali punizioni, preferendo continuare a vivere nella cultura della vergogna.

La situazione è piuttosto delicata ed è davvero difficile riuscire a salvare la vita dei due ragazzi. Il comitato internazionale contro la lapidazione ha esortato i giovani a difendere e sostenere la coppia gay al fine di non permettere questa tragedia.

I giudici ordinano condanne alla lapidazione grazie ad un decreto legge del 1983 che elenca i reati prescritti da Dio, e quindi del codice penale islamico che riflette i precetti del corano.

Diversi esponenti religiosi sciiti si sono opposti alla lapidazione.

La situazione degli iraniani omosessuali è davvero delicata e in gran parte dei casi le persone accusate di reato omosessuali hanno poche possibilità di ricevere un processo equo e giusto.

Sebbene la lapidazione sia molto frequente in Iran, è difficile quantificarne il numero dato che molti non vengono dichiarati pubblicamente. Diversi membri del parlamento iraniano si stanno battendo affinché sia approvata una legge contro la lapidazione.

Da ormai molti anni siamo a conoscenza dell’azione repressiva del regime iraniano contro i dissidenti, le minoranze etniche, le persone lesbiche e gay.

Questo grave atto rappresenta l’ennesimo episodio di disprezzo delle Convenzioni Internazionali per i Diritti dell’Uomo, peraltro sottoscritte anche dall’Iran ed è in palese contrasto con la Moratoria Onu contro la pena di morte e la richiesta di molti paesi di depenalizzazione dell’omosessualità.

Il rapporto 2009 di Iran Human Rights denuncia il sistema di “esecuzioni arbitrarie effettuate per procurare terrore”. Nel 2009 le esecuzioni capitali in Iran sono state 402, il 20% in più rispetto all’anno precedente. Le esecuzioni infatti hanno avuto un picco a ridosso delle elezioni iraniane del giugno scorso (50 esecuzioni a maggio, 94 a luglio, delle quali 50 nella sola Teheran). Inoltre, nonostante una ordinanza governativa del 31 gennaio 2008 sostanzialmente le vietasse, le impiccagioni in pubblico l’anno scorso sono state nove.

L’atteggiamento antidemocratico e repressivo di qualunque forma di espressione contraria al regime pervasivo dei Mullah affonda le proprie radici nel modello del terrore, affinché l’uomo ridotto a pura materia priva di contenuti vi si adatti incondizionatamente.

Per questi motivi  chiediamo un intervento immediato del Governo italiano, da sempre impegnato per l'adozione di una Risoluzione per la Moratoria Universale delle esecuzioni capitali che incontra un crescente sostegno in ambito ONU, e della Commissione Europea, nei confronti delle autorità iraniane affinché venga scongiurata questa azione di violenza inaudita contro i due ragazzi.

**Nessuno Tocchi Caino**

**Associazione Radicale Certi Diritti**

**Arcigay**

**Radicali Italiani**

**Associazione Luca Coscioni**

**Non c’è Pace Senza Giustizia**

**Nuova Proposta, donne e uomini omosessuali cristiani**

**3D-Democratici per pari Diritti e Dignità di lesbiche, gay, bisessuali e trans**

**Agedo - Associazione genitori, parenti e amici di omosessuali**

**Associazione Famiglie Arcobaleno**

**Gruppo Everyone**

**Di Gay Project**

**Circolo di Cultura omosessuale Mario Mieli - Roma**