---
title: 'SOCCORSO CIVILE - SOS PILLOLA DEL GIORNO DOPO - SE IL MEDICO TE LA NEGA, TE LA DIAMO NOI'
date: Sun, 29 Jun 2008 11:09:37 +0000
draft: false
tags: [Comunicati stampa, Luca Coscioni, Pillola del giorno dopo, ricetta pillola del giorno dopo, RU486, Soccorso Civile]
---

**Una struttura pubblica ti ha negato la prescrizione della pillola del giorno dopo?**  
Se sei a ROMA o a MILANO, puoi ricevere ASSISTENZA IMMEDIATA chiamando i numeri di [Soccorso Civile](http://www.lucacoscioni.it/soccorso_civile) ed ottenendo subito la ricetta.  
  
  
A Roma puoi chiamare il numero 333 9856046 tutti i giorni feriali dalle 09:00 alle 19:00, e non stop dalle 09:00 del sabato mattina fino alle 09:00 del lunedì mattina.  
A Milano puoi chiamare il numero 345 5011223 non stop dalle 18:00 del venerdì pomeriggio fino alle 08:00 del lunedì mattina.  
Oltre alla ricetta, riceverai tutta l'assistenza necessaria per denunciare i medici e le strutture che ti hanno negato un tuo diritto.