---
title: 'CERTI DIRITTI: KILLED IN UGANDA DAVID KATO KISULE, LGBT ACTIVIST'
date: Thu, 27 Jan 2011 11:27:23 +0000
draft: false
tags: [Comunicati stampa]
---

![UGANDA](images/stories/FOTO_PER_ARTICOLI/UGANDA.jpg)Radical Association Certi Diritti and ONG, Non c’è Pace Senza Giustizia grieve over David Kato Kisule’s death, high importance exponent of SMUG organization (Sexual Minorities Uganda) and foremost representative of Ugandan gay movement.

According to first informations reported, David was killed yesterday afternoon, at his home in unclear circumstances. Kampala’s police began the investigations which are under way yet.  
Thanks to Elio Polizzotto and ONG, Non c’è Pace Senza Giustizia’s care, David Kato took part in Certi Diritti’s 4th convention - last November - to denounce the awful conditions and the lynchings which gay and lesbian people are submitted to in Uganda, promoted by religious fundamentalism’s organizations.

On October 16th 2010, David’s pictures had been printed in the front page, among many others, by Ugandan magazine Rolling Stones during a persecutory campaign against homosexual people. Hatred climate against homosexual people is nourished by evangelist readers who found rich soil in the population which live in indigence and desperation. 

David, providing high courage and determination, had began a prosecution against the magazine Rolling Stones and last January 7th, Ugandan High Court, had condemned the magazine of violation of impingement’s law, defending persecuted gay people.High Court had found that none of the people whose pictures had been printed on the magazine, had committed an offence, according to Ugandan penal code.

Si ringrazia per la traduzione Lorenzo Lucarelli.

info@certidiritti.it