---
title: 'Perchè votare Lista Bonino-Pannella alle Europee del 6-7 giugno...'
date: Fri, 15 May 2009 08:37:54 +0000
draft: false
tags: [Senza categoria]
---

![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)

Care e cari amici,

vogliamo farvi conoscere alcune delle iniziative promosse dai radicali al Parlamento Europeo riguardo la difesa dei diritti delle persone lgbt. Lo facciamo perchè il 'conoscere per deliberare' è fondamentale per decidere chi votare. Noi anche per questo ti chiediamo di dare un voto e un volto ai radicali della Lista Bonino-Pannella che, da sempre, difendono i diritti delle persone lesbiche, gay, transessuali, transgender, bisessuali. Lo hanno  fatto in questi anni al Parlamento Europeo e, se rieletti,  continueranno a farlo.  

Uno dei primi episodi della legislatura che si sta chiudendo riguarda la cacciata di Buttiglione da Bruxelles per il voto contrario espresso dai deputati del Gruppo Alde (tra cui Marco Pannella) alla sua carica di Commissario Europeo. Al Parlamento Europeo in questi anni sono stati votati documenti  per la difesa dei diritti delle persone lgbt, sempre grazie al lavoro  straordinario di un gruppo di persone. Tra tutte vi segnaliamo il  nome della persona che ha permesso tutto questo, quello di Ottavio Marzocchi, radicale, funzionario del Gruppo Alde che ancora in questi giorni ha scritto gli emendamenti del documento  sui diritti umani consentendo di approvare alcuni punti finalizzati ad una maggiore tutela dei diritti delle persone lgbt. Per pochi voti, invece, e grazie alle tante astensioni del Gruppo Pse, non è  passato l'emendamento che condannava la tesi antisicientifiche e  false del papa sul preservativo.

Fatta questa premessa vi segnaliamo che la Lista Bonino/Pannella  ha tra i suoi candidati, oltre ai radicali storici (Bonino, Pannella, Welby), anche tre nomi che meriterebbero una particolare attenzione per il loro impegno sui temi lgbt(e), si tratta di:

\- **Marco Cappato**, Segretario dell'Associazione Luca Coscioni,  candidato tra i capilista nelle Circoscrizioni Nord-Ovest, Nord-Est, Centro e Sud;

- **Sergio Rovasio**, Segretario dell'Associazione Radicale Certi  Diritti, candidato nella Circoscrizione Centro (Lazio, Toscana,Umbria e Marche);

\- **Roberto Mancuso**, Reponsabile Certi Diritti Lecce, candidato  nella Circoscrizione Sud;

  

### **Di seguito l'elenco degli iscritti a Certi Diritti candidati della Lista Bonino/Pannella alle elezioni europee del 6-7 giugno 2009. Ovviamente vi invitiamo a dare la preferenza anche a questi nomi:**

1.  **Angiolo BANDINELLI – Circ.ne Nord-ovest**
2.  **Rita BERNARDINI – Circ.ne Centro**
3.  **Marco CAPPATO - Circ.ni Nord-ovest, Nord-est,Centro,Sud**
4.  **Antonella CASU – Circ.ne Isole**
5.  **Gianmarco CICCARELLI – Circ.ne Isole**
6.  **Roberto CICCIOMESSERE – Circ.ne Isole**
7.  **Sergio D’ELIA – Circ.ne Sud**
8.  **Michele DE LUCIA – Circ.ne Nord-ovest**
9.  **Maria Antonietta FARINA COSCIONI - Circ.ne Nord-ovest**
10.  **Valeria MANIERI – Circ.ne Sud**
11.  **Marco MARCHESE – Circ.ne Sud**
12.  **Monica MISCHIATTI – Circ.ne Nord-est**
13.  **Sergio Pasquale RAVELLI – Circ.ne Nord-Ovest**
14.  **Sergio ROVASIO – Circ.ne Centro**
15.  **Wilhelmine detta Mina Welby SCHETT WELBY Circ.ni Nord-Ovest, Nord-est, Centro, Sud**
16.  **Mario STADERINI, Circ.ne Centro**
17.  **Sergio Augusto STANZANI GHEDINI, Circ.ne Nord-Est**

Di seguito trovate una documentazione molto utile e corposa che vi consigliamo di studiare con attenzione per capire cosa è stato fatto dai radicali al Pe.

Grazie per l'attenzione, buon voto!

**[www.certidiritti.it](http://www.certidiritti.it/)**

**PERCHE’ VOTARE LISTA BONINO-PANNELLA ALLE PROSSIME ELEZIONI EUROPEE DEL 6-7 GIUGNO 2009...**
----------------------------------------------------------------------------------------------

**I RADICALI, PARLAMENTO EUROPEO E DIRITTI LGBT**

**nella legislatura 2004-2009**

a cura di Ottavio Marzocchi

**Intergruppo LGBT**

Come nelle precedenti legislature, gli Eurodeputati Radicali Emma Bonino, Marco Pannella e Marco Cappato sono stati membri, sponsors e fondatori dell'intergruppo LGBT al PE, garantendo il sostegno del gruppo ALDE all'istituzione dell'intergruppo in occasione dell'inizio della legislatura; Ottavio Marzocchi, collaboratore del gruppo ALDE e responsabile di Certi Diritti per le questioni europee, ha seguito i lavori dell'intergruppo negli ultimi 12 anni.

### Caso Buttiglione

La legislatura é iniziata con il rigetto da parte del Parlamento europeo della candidatura di Rocco Buttiglione a commissario italiano per il portafoglio dei diritti fondamentali, la giustizia e la sicurezza: grazie in particolare al voto di Marco Pannella - che a votato due volte "no" su Buttiglione come commissario europeo e  per il portafoglio indicato, contro la linea del gruppo ALDE - ed alla campagna che i radicali hanno lanciato nel PE e nel gruppo liberale e democratico, Buttiglione é stato bocciato dalla commissione libertà pubbliche, costringendo il Presidente della Commissione europea Barroso a chiedere al governo italiano un candidato sostitutivo.

### Campagna del gruppo ALDE per l'eguaglianza

Su proposta di Marco Cappato, Eurodeputato della Lista Bonino, il gruppo liberale e democratico al PE ha lanciato nel corso della legislatura una campagna per l'eguaglianza - unico gruppo parlamentare ad avere un simile progetto - per portare i deputati europei alle gay prides più problematiche e pericolose.

Marco Cappato ha partecipato al gay pride di Mosca assieme al militante radicale Ottavio Marzocchi, dove sono stati malmenati dai contro-manifestanti di estrema destra e dai fondamentalisti religiosi ortodossi ed arrestati dopo avere distribuito l'appello che numerosi parlamentari avevano firmato per chiedere al sindaco di Mosca di permettere il gay pride. Dopo essere stati trattenuti alla stazione di polizia alcune ore, hanno testimoniato nel processo - rivelatosi una farsa - contro 3 militanti radicali russi che erano stati accusati dalle autorità di avere "intralciato il traffico", in realtà per avere organizzato il gay pride. Il Presidente del PE ha scritto una lettera di protesta a Putin ed al sindaco di Mosca su quanto avvenuto.

Marco Cappato con il gruppo ALDE ha organizzato una conferenza al PE che ha affrontato tutte le tematiche LGBT, con la partecipazione di deputati europei, rappresentanti del Consiglio, della Commissione, degli Stati membri, delle ONG, del commissario del consiglio d'Europa per i diritti umani[\[1\]](#_ftnref1).

  

Le 3 risoluzioni del PE sull'omofobia in Europa
-----------------------------------------------

Il PE é stato particolarmente attivo nel corso della legislatura sull'omofobia e sui diritti LGBT, approvando 3 risoluzioni sull'omofobia, alla stesura delle quali ha lavorato Ottavio Marzocchi per il gruppo ALDE. L'impatto principale delle risoluzioni é stato quello di rafforzare un consenso europeo sul fatto che le manifestazioni come i gay prides debbano essere autorizzate, di attirare l'attenzione dell'opinione pubblica sulle situazioni nelle quali tali manifestazioni sono proibite o dove gli organismi di sicurezza pubblica non garantiscono la protezione dei partecipanti, e di mandare un segnale chiaro agli Stati membri dell'UE (ma anche ad altri Stati, quali la Russia) che politiche o leggi omofobiche sarebbero state denunciate fermamente dal PE e dall'Unione europea.

La **risoluzione sull'omofobia nell'Unione europea**[\[2\]](#_ftnref2), approvata il 18 gennaio 2006 e frutto di un compromesso tra i principali gruppi politici del PE, nasceva in reazione ad una serie di eventi preoccupanti avvenuti nei paesi dell'Europa dell'Est, ed in particolare in Polonia, dove il gay pride era stato vietato nel 2005, vari esponenti dei partiti di destra arrivati al governo avevano fatto dichiarazioni violente contro gli omosessuali, mentre il governo aveva deciso di chiudere "l'ufficio del plenipotenziario per l'eguaglianza" paventando di sostituirlo con un ufficio per la protezione della famiglia. Nella risoluzione il PE chiede:

-  agli Stati membri di _"promuovere e adottare il principio dell'uguaglianza nelle loro società e nei loro ordinamenti giuridici"_ in materia di coppie dello stesso sesso,

-  alla Commissione europea di rafforzare ed estendere la lotta alle discriminazioni basate sull'orientamento sessuale in tutti i settori e di portare davanti alla Corte di Giustizia gli Stati membri inadempienti in merito alle direttive anti-discriminazioni già approvate

-  di assicurare la libertà di circolazione delle coppie dello stesso sesso attraverso il mutuo riconoscimento di tali unioni

-  il PE condanna dell'omofobia e delle sue manifestazioni, _"spesso giustificate con motivi di...libertà religiosa e diritto all'obiezione di coscienza"_, come pure l'uso _"di un linguaggio incendiario, carico di odio o minaccioso da parte di ...capi religiosi"._

-  la risoluzione impegnava inoltre il PE ad organizzare un seminario il 17 maggio 2006 in occasione della Giornata internazionale contro l'omofobia, seminario che ha visto la partecipazione del Presidente del PE Josep Borrell, del Presidente della Commissione giustizia ed affari interni del PE Jean-Marie Cavada, nonché del Commissario per i diritti umani del Consiglio d'Europa Thomas Hammarberg.

Solamente cinque mesi dopo, il 15 giugno 2006, il PE elaborava ed approvava la **risoluzione sulla recrudescenza delle violenze razziste ed omofobe in Europa**[\[3\]](#_ftnref3). Essa faceva seguito ad una serie di eventi di violenza razzista avvenuti in vari Stati europei, nonché alla proibizione del gay pride a Mosca ed al timore che la Polonia negasse nuovamente l'autorizzazione per il gay pride del 2006, avendo un Ministro incitato alla violenza contro i partecipanti al gay pride. La risoluzione attaccava frontalmente le autorità governative, politiche e religiose in Russia e Polonia:

-  le autorità russe erano criticate non solo per il divieto del gay pride ma anche per non avere assicurato la sicurezza dei dimostranti - tra i quali alcuni deputati nazionali ed europei - ed esprimeva _"profonda preoccupazione per il ruolo avuto dai politici russi e dalle organizzazioni religiose nell'incitare alla violenza e all'odio contro le persone GLBT"_.

-  in merito alla situazione polacca, il PE _"ricorda alla Polonia i suoi impegni e obblighi ai sensi ...dell'articolo 6 del trattato UE_ (ndr: sul rispetto dei diritti fondamentali), _e le eventuali sanzioni in caso di inadempimento",_ chiede _"alla Commissione di verificare se le azioni e dichiarazioni del Ministro dell'istruzione polacco siano conformi all'articolo 6 del trattato UE"_, ed esprime infine preoccupazione per la _"generale recrudescenza dell'intolleranza...omofoba in Polonia, in parte fomentata da tribune religiose come Radio Maryja"_.

-  Il Consiglio dei Ministri della UE é inoltre criticato per non avere ancora approvato la Decisione Quadro sul razzismo e la xenofobia (bloccata a causa di un veto del governo Berlusconi), che dovrà coprire, secondo il PE, anche l'omofobia.

La **risoluzione del Parlamento europeo del 26 aprile 2007 sull'omofobia in Europa**[\[4\]](#_ftnref4)  nasce come seguito delle risoluzioni precedenti ed in particolare come reazione alle iniziative e dichiarazioni di leaders politici e religiosi e di esponenti governativi in Polonia, dove un'ondata di omofobia sponsorizzata dal governo di estrema destra dei fratelli Kazinsky (l'uno Presidente della Repubblica, l'altro del governo) rischiava di sfociare nell'adozione di una serie di misure discriminatorie contro le persone LGBT. Il PE:

-  considerando che _"le dichiarazioni e le azioni dei dirigenti politici e religiosi hanno un impatto considerevole sull'opinione pubblica e che quindi essi hanno l'importante responsabilità di contribuire in modo positivo a un clima di tolleranza e parità"_, condannava _"i commenti discriminatori formulati da dirigenti politici e religiosi nei confronti degli omosessuali, in quanto alimentano l'odio e la violenza, anche se ritirati in un secondo tempo, e chiede alle gerarchie delle rispettive organizzazioni di condannarli"_,

-  faceva l'inventario delle affermazioni e iniziative di esponenti governativi e di politici contro le persone LGBT, quali:

-  l'annuncio da parte del vice primo ministro e ministro della pubblica istruzione polacco di un progetto di legge destinato a punire la "propaganda omosessuale" nelle scuole, le cui disposizioni prevedono il licenziamento, l'imposizione di sanzioni o la detenzione per i responsabili d'istituti scolastici, gli insegnanti e gli alunni implicati in casi di "attivismo" a favore dei diritti LGBT nelle scuole, nonché il licenziamento degli insegnanti che renderanno pubblica la propria omosessualità, proposte che avevano ricevuto il sostegno del primo ministro polacco;

-  l'iniziativa del difensore civico per l'infanzia polacco di compilare un elenco di impieghi per i quali gli omosessuali non sono idonei;

-  quella dell'Ufficio del procuratore di Stato che ha ordinato l'esecuzione di controlli dei fondi delle organizzazioni LGBT in relazione a "movimenti criminali" e alla loro presenza nelle scuole al fine di trovare tracce di attività criminale, senza risultato alcuno e il respingimento da parte del governo polacco del finanziamento di progetti patrocinati da organizzazioni LGBT nel quadro del programma europeo Gioventù;

-  l'iniziativa del governo polacco che ha licenziato il direttore del Centro per la formazione degli insegnanti e ha vietato la distribuzione di un manuale ufficiale del Consiglio d'Europa contro la discriminazione;

-  Il PE chiedeva quindi alle _"competenti autorità polacche ad astenersi dal proporre o dall'adottare una legislazione quale quella descritta dal vice primo ministro nonché ministro della pubblica istruzione polacco, o dal porre in atto misure intimidatorie nei confronti delle organizzazioni LGTB"_, nonché a _"condannare pubblicamente e a prendere misure contro le dichiarazioni rilasciate da leader pubblici incitanti alla discriminazione e all'odio sulla base dell'orientamento sessuale; è del parere che qualsiasi altro comportamento costituirebbe una violazione dell'articolo 6 del trattato UE"_, di _"facilitare la realizzazione dell'Anno delle pari opportunità per tutti 2007 e chiede alla Commissione di controllare lo svolgimento di tale anno, in particolare la clausola secondo cui i fondi sono erogati a condizione che tutti i motivi di discriminazione siano stati affrontati equamente in tutti i programmi nazionali"._ Indiceva infine per il _"17 maggio di ogni anno la Giornata internazionale contro l'omofobia"._

Risoluzione sul caso di Seyed Mehdi Kazemi
------------------------------------------

Su iniziativa di Marco Cappato, il PE ha consacrato un dibattito con la Commissione ed il Consiglio in plenaria ed approvato il 13 marzo 2008 una risoluzione comune sul caso di Seyed Mehdi Kazemi, cittadino iraniano omosessuale di diciannove anni, che aveva chiesto asilo nel Regno Unito e si era visto respingere la sua domanda. Temendo l'espulsione, Kazemi era fuggito nei Paesi Bassi dove aveva presentato domanda di asilo. Le autorità olandesi, dopo aver esaminato la sua domanda, avevano deciso di rimandarlo nel Regno Unito. Anche grazie all'intervento del Parlamento Europeo, le autorità britanniche hanno infine deciso di riesaminare il caso e di concedere l'asilo politico, evitando cosi che fosse rinviato verso l'Iran, dove la prassi abituale delle autorità é quella di incarcerare, torturare e giustiziare gli omosessuali, come peraltro era accaduto all'ex partner di Kazemi. La risoluzione richiamava anche il caso analogo di Pegah Emambakhsh, chiusosi con la decisione da parte delle autorità britanniche hanno deciso, a seguito delle pressioni internazionali, di non deportarla in Iran. La risoluzione é importante anche perché il PE afferma chiaramente che le norme europee implicano che debba essere concesso l'asilo politico alle persone sulla base del loro orientamento sessuale [\[5\]](#_ftnref5).

Relazione di Marco Cappato sul rispetto dei Diritti Umani nel mondo
-------------------------------------------------------------------

Il Parlamento Europeo ha approvato l'8 maggio 2008 il rapporto di Marco Cappato sul rispetto dei diritti umani nel mondo, con 533 voti favorevoli, 63 contrari e 41 astensioni. Tra i punti centrali del rapporto, che commenta i risultati delle politiche europee nell'ambito dei diritti umani nel 2007, _"Deplora che l'Unione europea abbia ottenuto finora scarsi risultati nel favorire cambiamenti politici in Russia, in particolare per quanto riguarda...le discriminazioni fondate sull'orientamento sessuale","'reitera il suo appello affinché tutte le discussioni con i paesi terzi, gli strumenti, i documenti e le relazioni, comprese le relazioni annuali, in materia di diritti umani e democrazia affrontino in modo esplicito i temi relativi alla discriminazione, tra cui le questioni riguardanti le minoranze... e le persone di qualsiasi orientamento sessuale, associando pienamente le loro organizzazioni",_ e  _"chiede alla Commissione e al Consiglio di prendere iniziative dell'Unione europea a livello internazionale volte a combattere le persecuzioni e le discriminazioni basate sull'orientamento sessuale e l'identità di genere, ad esempio promuovendo una risoluzione sulla questione a livello delle Nazioni Unite e appoggiando le ONG e gli attori che promuovono l'uguaglianza e la non discriminazione; condanna il fatto che molti paesi abbiano criminalizzato il comportamento omosessuale, che l'Iran, l'Arabia Saudita, lo Yemen, il Sudan, la Mauritania, gli Emirati Arabi Uniti e parti della Nigeria impongano la pena di morte per atti omosessuali, che 77 paesi abbiano leggi che consentono alle autorità statali di perseguire ed eventualmente irrogare pene detentive per atti omosessuali e che numerosi paesi, quali il Pakistan, il Bangladesh, l'Uganda, il Kenya, la Tanzania, lo Zambia, il Malawi, il Niger, il Burkina Faso, la Malaysia e l'India (paese nel quale le disposizioni del codice penale in materia sono attualmente oggetto di revisione giurisdizionale) abbiano leggi che prevedono l'irrogazione di pene detentive che vanno da 10 anni all'ergastolo"._ La relazione infine _"appoggia pienamente i principi di Yogyakarta sull'applicazione del diritto internazionale in materia di diritti umani in relazione all'orientamento sessuale e all'identità di genere; chiede con insistenza agli Stati membri di concedere l'asilo alle persone che rischiano di subire persecuzioni nei loro paesi di origine a causa del loro orientamento sessuale o della loro identità di genere"._

Anche in occasione delle altre risoluzioni annuali della commissione affari esteri sui diritti umani nel mondo una serie di emendamenti sul rispetto dei diritti LGBT erano stati depositati ed approvati, ad esempio nella relazione Obiols i Germà , che include emendamenti depositati in plenaria da Marco Cappato e Sophie In't Veld. La relazione, oltre a criticare le violazioni dei diritti umani in Russia e le discriminazioni basate sull'orientamento sessuale, "_accoglie con favore la dichiarazione innovativa, sostenuta da 66 nazioni tra cui tutti gli Stati membri dell'Unione europea, presentata all'Assemblea generale delle Nazioni Unite il 18 dicembre 2008, con cui si conferma che la protezione internazionale dei diritti dell'uomo include l'orientamento sessuale e l'identità di genere e si riafferma il principio di non discriminazione, il quale richiede che i diritti umani si applichino allo stesso modo ad ogni essere umano, prescindendo dall'orientamento sessuale e dall'identità di genere"_ ed _"insiste nuovamente presso il Consiglio e la Commissione affinché l'Unione europea adotti in ambito internazionale iniziative finalizzate a combattere la persecuzione e la discriminazione fondata sull'orientamento sessuale e sull'identità sessuale, promuovendo, ad esempio, una risoluzione in materia in seno alle Nazioni Unite e concedendo aiuti alle ONG e ai soggetti impegnati nella promozione dell'uguaglianza e della non discriminazione"_.

Dibattito al PE sull'iniziativa francese all'ONU sulla depenalizzazione universale dell'omosessualità
-----------------------------------------------------------------------------------------------------

Su iniziativa di Marco Cappato, il PE ha dibattuto il 17 dicembre 2008 con la Commissione europea ed il Consiglio e espresso il sostegno trasversale all'Iniziativa francese all'ONU sulla depenalizzazione dell'omosessualità e sulla lotta alle discriminazioni e persecuzioni basate sull'orientamento sessuale[\[6\]](#_ftnref6), contro l'opposizione del Vaticano e degli Stati fondamentalisti e totalitari.

  

Relazione del PE sui diritti fondamentali nell'Unione europea, 2004-2008
------------------------------------------------------------------------

Il Parlamento europeo ha approvato il 14 gennaio 2009 con 401 voti favorevoli e 220 contrari la relazione Catania sui diritti umani nell'UE per gli anni 2004-2008, che conteneva una serie di emendamenti radicali approvati in sede di commissione libertà pubbliche[\[7\]](#_ftnref7), in tal modo _"chiedendo una nuova stagione di diritti civili in Europa"_. Tra gli emendamenti di Marco Cappato approvati in commissione, vi sono quelli concernenti la protezione dei diritti LGBT (mutuo riconoscimento tra Stati e adozione di leggi sulle unioni civili e matrimoni gay, politiche antidiscriminazione, asilo politico per le persone LGBT), spingendo Cappato ad augurarsi che _"anche in Italia si scelga di andare nella direzione di maggiori libertà su questi temi, smettendola di obbedire alle prescrizioni vaticane che, come nel caso dell'opposizione alla proposta europea di depenalizzazione dell'omosessualità nel mondo, si mostrano sempre più rigidamente intolleranti"._ La relazione riteneva _"che le affermazioni discriminatorie di esponenti politici, sociali e religiosi estremisti contro gli omosessuali alimentino l'odio e la violenza e chiede una loro condanna da parte degli organi dirigenti competenti"_ e affermava _"A questo proposito sottoscrive sentitamente l'iniziativa francese, appoggiata da tutti gli Stati membri, per la depenalizzazione universale dell'omosessualità, poiché in 91 paesi l'omosessualità costituisce ancora reato penale, e in taluni casi addirittura passibile di pena capitale"_. La relazione contiene inoltre altri paragrafi sui diritti LGBT nel capitolo della relazione intitolata "Orientamento sessuale" (dal par. 71 al 78) nonché in merito alla direttiva anti-discriminazioni ed alla decisione quadro sul razzismo e la xenofobia da allargare anche all'omofobia.

La nuova direttiva anti-discriminazione
---------------------------------------

La Commissione europea aveva in programma fin dall'inizio della legislatura 2004-2009 il completamento del mandato politico dell'articolo 13 TCE sull'anti-discriminazione, attraverso l'adozione di una nuova direttiva che lottasse contro le discriminazioni basate sull'orientamento sessuale, le religione e le credenze, l'età e l'handicap nei settori precedentemente lasciati scoperti dalle altre direttive, ovvero l'accesso ai beni ed ai servizi incluso l'alloggio, l'educazione, la protezione sociale[\[8\]](#_ftnref8). Superando le resistenze di alcuni Stati membri, che spingevano la Commissione verso una direttiva sulla sola disabilità e l'elaborazione di una serie di raccomandazioni non vincolanti per orientamento sessuale, religione ed età, e grazie alle insistenze del PE (che aveva richiesto per una decina di volte tale proposta legislativa[\[9\]](#_ftnref9)), dei partiti politici europei e delle associazioni LGBT, la Commissione licenziava finalmente il 2 luglio 2008[\[10\]](#_ftnref10) la direttiva sulla promozione del principio di eguaglianza nei settori al di fuori dell'impiego (detta anche direttiva orizzontale anti-discriminazioni)

La direttiva veniva accolta con favore dal Parlamento europeo, che però criticava anche il fatto che _"la proposta di direttiva lasci sussistere sostanziali lacune a livello della protezione giuridica contro la discriminazione, segnatamente in ragione di un vasto numero di eccezioni relative all'ordine pubblico, alla sicurezza pubblica e alla salute pubblica, alle attività economiche, allo stato civile e di famiglia e ai diritti riproduttivi, all'istruzione e alla religione; teme che, anziché vietare la discriminazione, queste "clausole di salvaguardia" possano in realtà servire a codificare pratiche discriminatorie esistenti"_[_\[11\]_](#_ftnref11)_._ In linea con tali considerazioni, Marco Cappato depositava 24 emendamenti alla direttiva in sede di commissione libertà pubbliche del PE, al fine di rafforzarla sostanzialmente, che  cancellavano e ribaltavano radicalmente le deroghe al principio di eguaglianza che erano state proposte dalla Commissione per accomodare alcune resistenze degli Stati membri. Cappato affermava in un comunicato di illustrazione degli emendamenti depositati che questi _"affermano che la direttiva si applica a tutti i settori, siano essi di competenza nazionale ed europea, e che il non riconoscimento delle unioni tra persone dello stesso sesso é una discriminazione non conforme alla direttiva. Gli altri emendamenti mirano ad assicurare che il principio di eguaglianza si applichi negli Stati membri ed a livello dell'UE, per tutti i settori coperti dalla legislazione nazionale ed europea, includendo i settori attualmente esclusi, ovvero: le discriminazioni basate sull'orientamento sessuale che negano uno status matrimoniale, familiare, civile alle persone dello stesso sesso; tutte le attività economiche e commerciali; l'educazione; le discriminazioni multiple; quelle basate sui diritti e la salute sessuale e riproduttiva; la libertà di circolazione ed il mutuo riconoscimento; lo status delle chiese e delle organizzazioni religiose; lo status dei cittadini degli Stati terzi, l'immigrazione e l'asilo; gli organi anti-discriminazione nazionale dovranno inoltre compiere attività di verifica dell'applicazione della direttiva negli Stati membri e cooperare con l'Agenzia dei Diritti fondamentali. Al contempo, gli emendamenti cancellano le deroghe previste dalla proposta della Commissione in merito all'educazione (organizzazione e contenuti; possibilità di discriminare sulla base della religione e di vietare di portare segni religiosi), la sicurezza sociale e sanità, i servizi di interesse generale; le attività economiche che non siano commerciali o professionali; il riferimento allo status privilegiato delle chiese viene emendato al fine di assicurare che non vi siano discriminazioni tra le chiese, tra gli individui e tra organizzazioni religiose e non religiose; le discriminazioni basate sull'età; l'ordine pubblico, sicurezza pubblica, salute pubblica; lo status dei cittadini degli Stati terzi, immigrazione ed asilo"_[\[12\]](#_ftnref12).

Anche a causa del tentativo della relatrice, l'olandese dei verdi Buitenweg, di fare un compromesso con il PPE, numerosi emendamenti radicali sono stati rigettati o sono decaduti a seguito dell'approvazione di emendamenti di compromesso. Il 18 marzo 2009 la commissione libertà pubbliche approvava la sua relazione finale (approvata con 34 voti favorevoli, 7 contrari e 4 astenuti), che non elimina _"le deroghe al principio di eguaglianza che una maggioranza di centro sinistra avrebbe potuto assicurare...Rimangono le deroghe sull'ordine pubblico, non viene eliminata la possibilità di discriminare nei contratti tra privati, nel settore dell'educazione sulla base della religione, tra religioni e Chiese; rimangono i richiami controproducenti al diritto nazionale in merito al diritto matrimoniale, di famiglia ed alla salute (da intendersi riproduttiva)"._ Per i radicali era necessario _"fare giocare al Parlamento europeo, che é solamente consultato in materia, il suo tradizionale e importante ruolo di pioniere dei diritti nell'UE, invece che fare "realpolitik" a discapito delle persone discriminate",_ modificando il testo in plenaria [\[13\]](#_ftnref13). Il PE, in occasione dell'approvazione della direttiva in plenaria, confermava _"i compromessi proposti in commissione dalla relatrice verde Buitenweg per venire incontro al PPE...che lasciano nella direttiva una serie di deroghe di dubbia compatibilità con il principio di eguaglianza, contro le quali ci siamo battuti in commissione e in plenaria"_. L'approvazione della direttiva era comunque giudicato come un importante passo avanti e degna di sostegno, anche per controbilanciare le pressioni da parte dell'industria, del PPE e di un gruppo di Stati membri (capitanati dalla Germania) per fare saltare la direttiva. La palla passava quindi al Consiglio ed alla Commissione europea, per _"assicurare che il principio di eguaglianza prevalga, contro le ragioni di coloro che vogliono mani libere per discriminare contro omosessuali e lesbiche, religioni minoritarie, persone anziane o giovani, disabili"_ [\[14\]](#_ftnref14).

  

Relazione sulla libera circolazione nell'UE
-------------------------------------------

Il PE ha approvato il 2 aprile 2009 un'importante relazione sull'applicazione della direttiva sulla libera circolazione nell'UE (relazione Valean, scritta con il contributo di Ottavio Marzocchi), che ricomprende una serie di emendamenti depositati da Marco Cappato ed approvati dal PE. La direttiva sulla libera circolazione prevede che gli Stati membri abbiano un _obbligo di garantire_ il diritto alla libera circolazione per i cittadini comunitari e per i loro familiari, incluso se extra-comunitari, definiti all'articolo 2 come il coniuge oppure il partner che abbia contratto con un cittadino comunitario un'unione registrata alla condizione che lo Stato membro ospitante equipari tale unione registrata al matrimonio, sulla base della legislazione nazionale. L'articolo 3 prevede inoltre _l'obbligo di agevolare_ l'ingresso e il soggiorno del _"partner con cui il cittadino dell'Unione abbia una relazione stabile debitamente attestata"_. L'interpretazione di tali norme ha dato vita ad un'applicazione da parte degli Stati membri molto variata, lasciando le coppie dello stesso sesso che comprendono un cittadino extra-comunitario in una posizione vulnerabile dal punto di vista dell'ingresso e del soggiorno sul territorio comunitario[\[15\]](#_ftnref15).

Nella sua relazione, il PE si occupa di tale problema: richiama innanzitutto _"la relazione dell'Agenzia dell'Unione europea per i diritti fondamentali, intitolata Omofobia e discriminazione basata sull'orientamento sessuale negli Stati membri"_ ; considera che tra le problematiche principali nell'applicazione della direttiva vi é quella dell' _"interpretazione restrittiva, da parte degli Stati membri, dei concetti di "familiare" (articolo 2), di "ogni altro familiare" e di "partner" (articolo 3), in special modo per quanto riguarda le coppie dello stesso sesso e il loro diritto alla libera circolazione ai sensi della direttiva 2004/38/CE**"**_ e indica che _"In Italia...i matrimoni tra persone dello stesso sesso non costituiscono una base per il riconoscimento del diritto alla libera circolazione"._

La relazione chiede quindi _"agli Stati membri di dare piena attuazione ai diritti sanciti dall'articolo 2 e dall'articolo 3 della direttiva 2004/38/CE, e di riconoscere tali diritti non soltanto ai coniugi di sesso diverso, ma anche ai partner legati da un'unione registrata, ai membri del nucleo familiare e ai partner – comprese le coppie dello stesso sesso riconosciute da uno Stato membro – a prescindere dalla loro nazionalità e fatto salvo il loro mancato riconoscimento nel diritto civile di un altro Stato membro, in accordo con i principi di reciproco riconoscimento, uguaglianza, non discriminazione, dignità e rispetto della vita privata e familiare; invita gli Stati membri a tenere presente che la direttiva impone l'obbligo di riconoscere la libera circolazione di tutti i cittadini dell'Unione (comprese le coppie dello stesso sesso), senza imporre il riconoscimento dei matrimoni fra persone dello stesso sesso; a tale riguardo, esorta la Commissione a formulare orientamenti rigorosi, traendo spunto dalle conclusioni contenute nella relazione dell'Agenzia dell'Unione europea per i diritti fondamentali nonché a monitorare tali questioni;" ed "invita la Commissione a formulare proposte adeguate, nel quadro del programma di Stoccolma, al fine di garantire la libera circolazione senza alcuna discriminazione fondata sulle motivazioni di cui all'articolo 13 del trattato CE, e a trarre spunto dall'analisi e dalle conclusioni contenute nella relazione dell'Agenzia dell'Unione europea per i diritti fondamentali_;"[\[16\]](#_ftnref16).

  

Interrogazioni presentate da Marco Cappato alla Commissione ed al Consiglio in materia di diritti LGBT
------------------------------------------------------------------------------------------------------

Marco Cappato ha depositato una serie di interrogazioni parlamentari sulle questioni LGBT, qui sotto raggruppate per temi[\[17\]](#_ftnref17):

Asilo politico in Europa per cittadini omosessuali di Stati terzi:

*   Direttiva 2004/83/CE del Consiglio e attribuzione della qualifica di rifugiato sulla base dell'orientamento sessuale
*   Espulsione di Seyed Mehdi Kazemi, ragazzo iraniano omosessuale, portando alla sua non espulsione a seguito dell'approvazione di una risoluzione al PE proposta da Cappato
*   Richiesta di asilo presentata in Grecia da un uomo di origine iraniana ed omosessuale

Libertà di manifestazione, proibizioni dei gay prides in Europa

*   Divieto del Gay Pride in Moldova, mancanza di tutela da parte della polizia e violazione dei diritti umani
*   Recenti decisioni e iniziative omofobiche delle autorità lituane e polacche e LGBT pride lettone
*   Probizione gay prides in Russia e arresti

Unione europea, mutuo riconoscimento dei matrimoni omosessuali, delle civil partnership e delle coppie di fatto nella UE:

*   Caso di Frédéric Minvielle, concernente il mancato riconoscimento da parte della Francia di un matrimonio tra persone dello stesso sesso contratto nei Paesi Bassi e risultante nella privazione della nazionalità francese
*   Ricongiungimento familiare per le coppie omosessuali composte da un cittadino italiano e straniero (sentenza della Corte di Cassazione italiana vs cittadino neozelandese e di un italiano)

Antidiscriminazione

*   caso in GB dove una funzionaria si era rifiutata di trascrivere un'unione civile di una coppia dello stesso sesso invocando motivi di coscienza: Sentenza del Tribunale del lavoro del Regno Unito, del 30 maggio 2008, nella causa L. Ladele/Consiglio di Islington
*   Direttiva 2000/78/CE, proibizione delle discriminazioni sull'orientamento sessuale e trasposizione nel diritto nazionale
*   Direttiva 2004/38/CE e discriminazione basata sull'orientamento sessuale
*   Direttiva 2000/78/CE e attuazione delle disposizioni in materia di orientamento sessuale
*   Divieto di trascrizione dei matrimoni omosessuali in Italia

Diritti LGBT nel mondo

*   Arresti e persecuzioni contro persone omosessuali
*   Situazione delle persone lesbiche, gay, bisessuali e transgender (LGBT) in Bielorussia
*   Situazione delle persone lesbiche, gay, bisessuali e transgender (LGBT) in Russia
*   Arresto di attivisti russi LGBT a un seggio elettorale
*   Questioni legate alle persone lesbiche, gay, bisessuali e transessuali (LGBT) in Egitto
*   Situazione delle persone LGBT in Marocco
*   in Irak
*   Egitto, Bahrein e Marocco

Allargamento

*   Standard comunitari in materia di antidiscriminazione, libertà di circolazione e diritti umani nei paesi candidati, in via d'adesione e vicini
*   Lambdaistanbul, associazione per i diritti LGBT in Turchia, perseguitata dalle autorità

  

  

* * *

  

  

\[1\] [vedi qui](http://www.alde.eu/en/details/news/alde-conference-lgbt-rights-in-the-framework-of-the-project-alde-4-equality)

  

  

\[2\]Il testo della risoluzione approvata dal PE é disponibile a [questo sito](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P6-TA-2006-0018+0+DOC+XML+V0//IT&language=IT ).

  

  

\[3\]Il testo della risoluzione approvata dal PE disponibile a [questo sito](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P6-TA-2006-0273+0+DOC+XML+V0//IT).

  

  

\[4\] [vedi qui](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P6-TA-2007-0167+0+DOC+XML+V0//IT)

  

  

\[5\] [testo approvato dal PE](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P6-TA-2008-0107+0+DOC+XML+V0//IT).

  

  

\[6\] vedi [comunicato di Marco Cappato](http://lnx.marcocappato.it/node/38507).

  

  

\[7\] Comunicato stampa di Marco Cappato sull'approvazione della relazione Catania in [commissione](http://lnx.marcocappato.it/comment/reply/38481) ed in [plenaria](http://www.lucacoscioni.it/comunicato/diritti-umani-cappato-radicali-il-parlamento-europeo-approva-le-proposte-radicali-su-norm).

  

  

\[8\] Sulla base dell'art. 13 TCE sono state già adottate una serie di direttive: una sulla razza che copre il settore dell'impiego, dell'educazione e dell'accesso ai beni ed ai servizi; una sull'orientamento sessuale, la disabilità, l'età e la religione che copre il solo settore dell'impiego. La nuova direttiva riguardava le discriminazioni basate sull'orientamento sessuale, la disabilità, l'età e la religione e copre i settori educazione, accesso ai beni e servizi, protezione sociale.

  

\[9\] tra i vari rapporti che lo chiedevano, in particolare i rapporti Zdanoka a seguito dell'approvazione di un emendamento di Marco Cappato e Sophie In't Veld, che "sollecita la Commissione a presentare quanto prima la sua proposta di direttiva orizzontale riguardante l'attuazione del principio della parità di trattamento al di fuori del lavoro, ivi compresi l'accesso a beni, servizi e alloggi, l'istruzione, la protezione sociale e i vantaggi sociali, l'immigrazione e l'asilo, coprendo tutti i motivi di discriminazione di cui all'articolo 13 del trattato CE" e la relazione Lynne. Questi due rapporti, assieme alle 3 risoluzioni sull'omofobia, la relazione sulla nuova direttiva anti-discriminazioni Buitenweg ed al Catania, sono ritenuti da ILGA-Europe le relazioni più importanti della legilsatura, [vedi qui](http://www.ilga-europe.org/europe/campaigns_projects/ep2009/what_can_you_do/get_to_know_the_candidates_and_their_position_on_human_rights_and_lgbt_issues/summary_of_voting_records_of_members_of_the_european_parliament_recent_resolutions_relevant_to_the_rights_of_lgbt_people_2004_2009 )

  

  

\[10\] Comunicati di Certi Diritti: [Gaynews](http://www.gaynews.it/view.php?ID=78140) ; [Certi Diritti](index.php?option=com_content&task=view&id=141&Itemid=55) ;

  

  

\[11\] vedi ad esempio il par. 37 della [relazione Catania](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P6-TA-2009-0019+0+DOC+XML+V0//IT)

  

  

\[12\] [vedi qui](index.php?option=com_content&view=article&id=298:al-parlamento-europeo-radicali-per-direttiva-antidiscriminazione&catid=1:ultime&Itemid=55)

  

  

\[13\] [vedi qui](http://lnx.marcocappato.it/node/38760)

  

  

\[14\] [vedi qui](index.php?option=com_content&view=article&id=328:parlamento-europeo-approva-nuova-direttiva-antidiscriminazione&catid=1:ultime&Itemid=55)

  

  

\[15\] la questione del mutuo riconoscimento dei matrimoni, delle unioni civili o delle convivenze da un punto di vista del diritto civile e familiare nell'UE é purtroppo nelle mani degli Stati membri, dato che i Trattati UE lasciano a loro piene competenze in tali settori.

  

  

\[16\] [vedi qui](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+TA+P6-TA-2009-0203+0+DOC+XML+V0//IT)

  

  

\[17\] tutte le interrogazioni parlamentari di Marco Cappato sono disponibili su [questo link](http://www.europarl.europa.eu/sidesSearch/sipadeMapUrl.do?L=IT&PROG=QP&SORT_ORDER=DAA&S_REF_QP=%&S_RANK=%&AUTHOR_ID=4740&LEG_ID=6)

* * *

  

  

  

![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)

![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)

![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)

![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)![](http://www.certidiritti.org/wp-content/uploads/2009/05/1240309220.jpeg)