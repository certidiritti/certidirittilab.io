---
title: 'Elezione di Luigi Manconi a commissione per i diritti umani è garanzia per tutti e tutte. Congratulazioni e buon lavoro dall''Associazione radicale Certi Diritti'
date: Fri, 24 May 2013 12:02:19 +0000
draft: false
tags: [Politica]
---

Comunicato dell'Associazione Radicale Certi Diritti.

Roma, 24 maggio 2013

L'Associazione radicale Certi Diritti ha accolto con grandissimo favore la nomina di Luigi Manconi a presidente della Commissione per i Diritti umani del Senato. Luigi è attento e attivo su questi temi da molti anni, e segue da vicino le questioni che sono oggetto del nostro impegno anche attraverso l'iscrizione alla nostra Associazione. Siamo certi che saprà continuare e rinnovare l'attenzione alle questioni lgbt ed in generale l'attenzione sui diritti sessuali delle persone già avviata dal suo predecessore, Pietro Marcenaro, e insieme agli auguri gli assicuriamo il nostro sostegno.