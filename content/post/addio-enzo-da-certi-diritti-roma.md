---
title: 'ADDIO ENZO DA CERTI DIRITTI ROMA'
date: Mon, 30 Nov 2009 14:05:15 +0000
draft: false
tags: [Senza categoria]
---

Carissime e carissimi tutti,

con la dipartita di Enzo Francone abbiamo perso un altro pezzo importante della nostra storia. Enzo, per Torino e per il nord tutto, ha rappresentato quello che per noi qui a Roma è stato Massimo Consoli. Due persone eccezionali accomunate dall’impegno per un unico obiettivo, in cui credevano nonostante i decenni di militanza, nonostante le delusioni, nonostante spesso le loro parole suonassero agli orecchi delle nuove generazioni come prive di significato.

Di Enzo sentii parlare per la prima volta negli anni ’90, quando da presidente del CIG, Arcigay Milano, mi occupai di organizzare delle conferenze sulla storia del movimento gay italiano. Fu Giovanni Dall’Orto, durante uno di questi incontri, a raccontare di lui, della nascita del FUORI insieme a Pezzana e delle tante azioni che hanno contraddistinto quei memorabili anni di impegno.

Quando ho deciso di ricominciare a lavorare in prima linea per il movimento, sono stato orgoglioso di entrare a far parte di un’associazione come Certi Diritti in cui a militare, oltre a Sergio Rovasio ed Enzo Cucco![](chrome://skype_ff_toolbar_win/content/icons/icon_offline.png), c’era proprio quell’Enzo Francone di cui già conoscevo le gesta.

Lo conobbi di persona al II° Congresso di CD, quando fu eletto tesoriere; mi colpirono moltissimo la sua energia e voglia di fare per risanare le esangui finanze della nostra associazione. Aveva idee modernissime, legate alla pluridecennale esperienza e ci stimolò tutti a fare di più per aumentare gli iscritti a Certi Diritti.

Lo incontrai poi a Roma prima del Gay Pride, quando venne a consegnarci lo striscione nuovo che lui aveva fatto fare, spendendo la metà di quanto avremmo speso qui, così come per le bandiere, grazie ai suoi mille contatti, patrimonio che speriamo non vada perduto.

Il giorno del Pride di Genova fece impressione a tutti la sua vitalità e per giorni agli incontri dell’associazione e nelle nostre mailing list non si parlò altro che della sua mitica danza sul carro, da lui allestito,  sulle note di YMCA, canzone che probabilmente amava tanto poiché gli ricordava gli anni d’oro della sua militanza.

Purtroppo poco dopo arrivò come un fulmine a ciel sereno la notizia della sua malattia, molto più grave di quanto si pensasse all’inizio, e oggi questo dolorosissimo evento.

Ti saluto Enzo, un saluto affettuoso da me e da tutti i membri di Certi Diritti Roma; da laico ed ateo l’unico augurio che mi sento di farti è che le generazioni future non scordino mai il tuo nome, cosicché tu possa continuare ad essere un esempio per tutti noi.

Ci mancherai.

Luca “Lucky” Amato  
Coordinatore di Certi Diritti Roma

P.S. A causa di questo lutto la riunione di CD Roma fissata per oggi, 30 novembre, è rinviata a data da definirsi.