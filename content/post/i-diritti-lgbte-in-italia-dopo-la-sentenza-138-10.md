---
title: 'I diritti lgbt(e) in Italia dopo la sentenza 138/10'
date: Wed, 29 Feb 2012 07:42:25 +0000
draft: false
tags: [Diritto di Famiglia]
---

Durante l'incontro saranno raccolte le firme per le unioni civili a Roma.

La campagna di Affermazione Civile per il riconoscimento del diritto al matrimonio tra persone dello stesso sesso portò, nel marzo 2010, alla sentenza 138/10 della Corte Costituzionale. Cosa ha significato quella sentenza? Cosa è successo dopo? Quali sono gli obiettivi del movimento lgbt e perchè si ritorna a parlare di unioni civili?

Partendo dal libro 'Dal cuore delle coppie al cuore del diritto. L'udienza 138/2010 alla Corte costituzionale per il diritto al matrimonio tra persone dello stesso sesso', nell'incontro di venerdì cercheremo di dare una risposta a queste domande e a molte altre domande.

  
L'appuntamento sarà anche l'occasione per raccogliere le firme per le unioni civili a Roma.

**I DIRITTI LGBT(E) IN ITALIA DOPO LA SENTENZA 138/10  
venerdì 2 marzo ore 18  
presso la sede del Partito Radicale  
via di Torre Argentina, 76 Roma**

programma

modera  
DARIO ACCOLLA, attivista e blogger

intervengono  
On. RITA BERNARDINI, Radicali-Pd  
YURI GUAIANA, Segretario Associazione radicale Certi Diritti  
GIUSEPPINA LA DELFA, Presidente Famiglie Arcobaleno  
ROSSANA PRAITANO, Presidente Mario Mieli  
CRISTIANA ALICATA, PD XV Municipio Roma  
SERGIO ROVASIO, Associazione radicale Certi Diritti  
Avv. MASSIMO CLARA  
Avv. MARIO DI CARLO, Consulta Romana per la laicità delle istituzioni;  
SERENA MARCIANO', per il Forum Queer di Sinistra e Libertà;  
SALVATORE MARRA, Cgil Nuovi Diritti - Roma;

evento facebook:  
[http://www.facebook.com/events/188516671256651/?context=create](http://www.facebook.com/events/188516671256651/?context=create)