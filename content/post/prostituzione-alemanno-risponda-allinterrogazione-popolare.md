---
title: 'Prostituzione, Alemanno risponda all’interrogazione popolare'
date: Tue, 12 Jul 2011 09:38:31 +0000
draft: false
tags: [Lavoro sessuale]
---

**Il sindaco Alemanno, troppo impegnato a girovagare di notte in moto, non ha ancora risposto alle 7 interrogazioni popolari da noi depositate due mesi fa con 1500 firme di cittadini romani.**

Dichiarazione di Riccardo Magi, segretario di Radicali Roma

I dati ufficiali dello studio condotto sugli effetti dell’ordinanza antiprostituzione a Milano hanno rivelato che questo strumento è inefficacie, anzi controproducente. La percentuale di multe pagate è inferiore al 10% mentre l’unico effetto registrato dagli operatori sociali è il temporaneo spostamento verso la periferia del fenomeno e l’aumento di episodi di violenza legati alla prostituzione.

Nel frattempo il sindaco Alemanno, troppo impegnato a girovagare di notte in moto, non ha ancora risposto alle 7 interrogazioni popolari da noi depositate due mesi fa con 1500 firme di cittadini romani. Una di queste riguarda proprio i dati relativi all’attuazione della sua ordinanza cosiddetta antiprostituzione. I cittadini romani con l’interrogazione, che in base allo statuto doveva ricevere una risposta entro il 4 luglio, chiedono al sindaco: quante contravvenzioni sono state elevate in base all’ordinanza, per quale importo complessivo, quante di queste sono state contestate a presunti clienti e quante a presunte/i prostitute/i; quali siano i tassi di recidività riscontrati; quante sanzioni risultano pagate e per quale importo complessivo; quanti interventi e in che fasce orarie ha eseguito la Polizia municipale al fine di applicare l’ordinanza e con quale costo per l’impiego ordinario e straordinario dei mezzi e del personale; quante persone si stima siano state avvicinate ai servizi sociali  per effetto dell’ordinanza e quanti cittadini extraccomunitari abbiano attenuto il permesso di soggiorno a seguito della denuncia dei propri sfruttatori o comunque per essersi sottratti a “violenze o grave sfruttamento” come previsto dalla legge; quali misure siano state attivate dal momento dell’entrata in vigore dell’ordinanza per potenziare tutte quelle politiche di tipo sociale volte a prevenire il fenomeno della prostituzione; se, a seguito dell’entrata in vigore della citata ordinanza, vi sia stato un incremento o, al contrario, un decremento dei casi di violenza contro le persone che si prostituiscono.

Rispondere a queste domande darebbe l’occasione al sindaco di rendersi conto, a distanza di quasi tre anni dall’emanazione, che un fenomeno sociale così complesso e vasto come quello della prostituzione non può essere affrontato come un semplice fenomeno di sicurezza e ordine pubblico. Ma Alemanno ancora non risponde…

Fonte: [http://www.radicaliroma.com/wp/2011/07/prostituzione-alemanno-risponda-allinterrogazione-popolare-sugli-effetti-della-sua-ordinanza/](http://www.radicaliroma.com/wp/2011/07/prostituzione-alemanno-risponda-allinterrogazione-popolare-sugli-effetti-della-sua-ordinanza/)