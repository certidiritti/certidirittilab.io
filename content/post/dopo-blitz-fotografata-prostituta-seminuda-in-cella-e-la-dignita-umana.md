---
title: 'DOPO BLITZ FOTOGRAFATA PROSTITUTA SEMINUDA IN CELLA: E LA DIGNITA'' UMANA?'
date: Tue, 12 Aug 2008 08:04:18 +0000
draft: false
tags: [Comunicati stampa]
---

**A PARMA FOTOGRAFATA SEMINUDA PROSTITUTA IN CELLA: ORMAI E’ FAR WEST. MA QUANTO SONO BRAVI I NOSTRI AMMINSITRATORI-SCERIFFI! E LA DIGNITA’ UMANA? INTERROGAZIONE PARLAMENTARE DEI SENATORI RADICALI MARCO PERDUCA E DONATELLA PORETTI.**

_**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**_

“La moda del Sindaco o Assessore Sceriffo, che tanto va di moda quest’estate, ha fatto un altro bel colpo, di cui ci sarebbe semmai da vergognarsi.. A Parma l’Assessore alla sicurezza, Costantino Monteverdi, ha avuto la bella idea di convocare stampa, fotografi e cineoperatori nel mentre si avviava un’operazione antiprostituzione con il bel risultato che, all’interno del Comando della Polizia Municipale di Parma, è stata fotografata, seminuda, una delle prostitute nigeriane fermate nel blitz (vedi foto al seguente link: [http://parma.repubblica.it/dettaglio/Prostituzione-dopo-il-blitz-la-rabbia:-Quella-foto-e-una-vergogna/1500460](http://parma.repubblica.it/dettaglio/Prostituzione-dopo-il-blitz-la-rabbia:-Quella-foto-e-una-vergogna/1500460) ).

Ciò che sorprende e stupisce è la totale indifferenza alla dignità, al rispetto e alla tutela della privacy della persona. Secondo quanto riportato da Repubblica online (edizione di Parma), l’Assessore alla sicurezza della città, di fronte alle reazioni negative delle Associazioni di volontariato e difesa dei diritti civili, ha dichiarato che non c’è stata violenza e ‘tutto si è svolto secondo le procedure’. Ma di che parla costui? Ci auguriamo che quanto prima venga data risposta all’interrogazione parlamentare dei Senatori radicali Marco Perduca e Donatella Poretti, depositata stamane che ricostruisce nel dettaglio tutta la vicenda”.

**\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_**

**Interrogazione scritta - Al Ministro dell'Interno**

Per sapere - Premesso che:

Il quotidiano la Repubblica, nella versione on line, il giorno 11 agosto ha pubblicato una fotografia che dichiara essere stata scattata all'interno di una cella di sicurezza del comando della Polizia Municipale di Parma.

La foto si riferirebbe ad una prostituta di nazionalità nigeriana arrestata nel corso di un'operazione contro la prostituzione condotta fra l'8 ed il 9 agosto, avvenuta alla presenza di giornalisti, fotografi e dell'Assessore alla sicurezza del Comune di Parma, Costantino Monteverdi seguita di pochi giorni ad una analoga effettuata dai Carabinieri.

Nell'articolo a corredo si sosteneva che la donna "si era lasciata cadere a terra senza più forze".

Sempre nell'articolo, l'assessore assicura che non c'è stata alcuna violenza e che tutto si è svolto secondo le procedure, sostenendo che la donna sarebbe stata rinchiusa in una camera di sicurezza con pareti in gomma (contrariamente a quanto appare nella fotografia pubblicata, dove si vedono un pavimento polveroso ed un elemento architettonico evidentemente manufatto in muratura o cemento, privo di protezioni in gomma). In dispacci di agenzia, l'assessore ha sottolineato che non è in corso nessuna "stretta contro la prostituzione", ma che si è trattato di un normale controllo anti-prostituzione e che a Parma non è prevista alcuna ordinanza ad hoc rafforzativa rispetto alle disposizioni del decreto Maroni.

Secondo la polizia municipale, una volta portata al Comando la donna si è rifiutata di fornire le proprie generalità agli agenti scoppiando in lacrime e urla. Pare che temesse la reazione dello sfruttatore.

La donna, infine, secondo l'assessore, è stata rilasciata il mattino successivo insieme alle altre donne controllate.

Per sapere:

\- se non ritenga il Ministro che i poteri e le forme di intervento dei reparti della polizia municipale devono essere conformi alle disposizioni di legge e che le persone sottoposte a provvedimenti anche temporanei di limitazione della libertà hanno il diritto ad essere trattati civilmente e con tutte le garanzie previste dalla legge,.

\- se la predetta operazione di controllo anti-prostituzione effettuata dalla polizia municipale di Parma rientri nell'ambito delle sue competenze, sia per quanto concerne la tipologia di controllo, sia per quanto riguarda le modalità di trattamento della prostituta fermata;

\- per quali motivi l'operazione è stata preannunciata alla stampa con il coinvolgimento di giornalisti e fotografi, unitamente alla presenza dell'assessore senza che venisse adottata alcuna precauzione a garanzia e tutela delle persone fermate;

\- se risponde al vero che presso la polizia municipale di Parma esiste una struttura di detenzione di sicurezza dotata di imbottiture perimetrali, tali da proteggere l'incolumità di ospiti irrequieti.

\- se risponde al vero che la prostituta ritratta nella foto, oggetto dell’articolo ([http://parma.repubblica.it/dettaglio/Prostituzione-dopo-il-blitz-la-rabbia:-Quella-foto-e-una-vergogna/1500460](http://parma.repubblica.it/dettaglio/Prostituzione-dopo-il-blitz-la-rabbia:-Quella-foto-e-una-vergogna/1500460)) sia stata effettivamente custodita in detta struttura di sicurezza;

\- Quali provvedimenti intenda prendere il Ministro per verificare quanto accaduto.

**Sen. Marco Perduca**

**Sen. Donatella Poretti**