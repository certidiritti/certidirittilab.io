---
title: 'Solidarietà ad Antonio Parisi, vittima di aggressione omofobica'
date: Wed, 11 May 2011 11:12:10 +0000
draft: false
tags: [Politica]
---

**Antonio, che a Trieste organizza la serata Jotassassina, è stato insultato e preso a pugni a Lubiana. Oggi a Trieste le associazioni accoglieranno Buttiglione con cartelli contro l'omofobia.**  
  
**Trieste, 11 maggio 2011**  
  
**Comunicato Stampa dell’Associazione Radicale Certi Diritti**  
  
L'associazione radicale Certi Diritti esprime solidarietà ad Antonio Parisi, Direttore artistico di Jotassassina, che ha subito un’aggressione prima verbale e poi fisica, che gli ha procurato serie lesioni al volto. Il fatto è  avvenuto a Lubiana, al rientro da una serata trascorsa alla discoteca Tiffany. Il giovane è stato prima insultato e poi aggredito con pugni e nessuno è intervenuto in sua difesa. Solo in un secondo momento è giunta la polizia ma non è stata chiamata neppure l’ambulanza. Antonio Parisi rientrato a Trieste si è nuovamente dovuto sottoporre a cure ospedaliere.  
  
Antonio Parisi è molto conosciuto a Trieste per la sua "creatura" Jotaassassina, Associazione che da tempo organizza in città  serate di incontro e condivisione per persone gay ed etero.  
  
Clara Comelli , dirigente dell’Associazione Radicale  Certi Diritti, che vive a Trieste come Antonio , dopo averlo sentito al telefono riporta con piacere le sue parole: "quest'atto mi fa sentire più coraggioso e forte e ora sto bene".  
  
Proprio raccogliendo lo spirito di queste parole Clara Comelli assieme al presidente ed i volontari del  circolo Arcobaleno Arcigay Arcilesbica di Trieste, saranno a manifestare oggi pomeriggio a Trieste fuori dall'incontro che vedrà ospite l'on Buttiglione dal titolo "Al centro la famiglia per una Trieste migliore".  
  
Lo faranno "indossando" i manifesti della campagna dei cosiddetti baci gay, ovvero quelli che saranno affissi in tutta Italia in occasione della giornata mondiale contro l'omofobia. Un gesto significativo per condannare qualsiasi dichiarazione atta a fomentare odio e violenza omofoba e per dichiarare che non esiste solo la Famiglia , ma le Famiglie e che per tutte queste, è arrivato il momento dei diritti.