---
title: 'SE GASPARRI FOSSE GAY, O TRAVESTITO O TRANSESSUALE... MA...'
date: Wed, 24 Jun 2009 12:31:16 +0000
draft: false
tags: [Comunicati stampa]
---

SE GASPARRI FOSSE GAY O UN TRAVESTITO O UNA TRANSESSUALE LO DIFENDERMMO NOI PER PRIMI. MA NON LO E’ E LUI NON SI PREOCCUPA MINIMAMENTE DELLA LORO CONDIZIONE.

Roma, 24 giugno 2009

Il Capogruppo del Pdl al Senato, cui piace tanto fare la politica con i ‘se’, se fosse gay, o un travestito o una transessuale, avrebbe certamente il nostro aiuto e supporto, lo difenderemmo insomma da ingiurie, offese, violenze gratuite così come accade a queste persone ogni giorno, ogni ora. Dato però che Gasparri ci risulta non essere né gay, né un travestito né una transessuale, la ‘lobby gay’ che solitamente difende queste persone, lo ignora. Di sicuro sappiamo che lui è uno di quei politici che nulla fanno per la comunità lgbt. Se Gasparri invece di fare dichiarazioni politiche sui ‘se’, a partire dal ‘se Berlusconi fosse gay’,  facesse più azioni concrete contro l’omofobia, la violenza contro le donne, leggi per il riconoscimento di diritti per le coppie gay ed eterosessuali conviventi, come avviene in tutti i paesi europei, compresi quelli governati dal centro-destra, egli stesso si renderebbe conto che tutto il paese ne trarrebbe un gran beneficio. Altra cosa dal chiacchiericcio sui ‘se’.