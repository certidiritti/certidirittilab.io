---
title: 'IX CONGRESSO DI RADICALI ITALIANI: APPROVATA LA MOZIONE PARTICOLARE SULLA RIFORMA DEL DIRITTO DI FAMIGLIA'
date: Thu, 04 Nov 2010 08:10:43 +0000
draft: false
tags: [Chianciano, CONGRESSO, diritto di famiglia, mozione particolare, Radicali Italiani, Senza categoria, Sergio Rovasio]
---

La mozione particolare impegna Radicali Italiani ad un’immediata e forte mobilitazione affinchè la mattina dell’8 novembre 2010, davanti alla sede governativa della Conferenza nazionale sulla Famiglia, si svolga a Milano un sit-in nonviolento di contestazione.

Presentata con la prima firma di Sergio Rovasio, segretario di Certi Diritti, la mozione è stata approvata a larga maggioranza dal Congresso radicale

**MOZIONE PARTICOLARE RIFORMA DIRITTO DI FAMIGLIA**

Il IX Congresso di Radicali Italiani, riunito a Chianciano i giorni 29 ottobre- 1 novembre 2010,

considerato l’impegno di molte personalità, Associazioni radicali e non, per la realizzazione del progetto di Amore Civile, finalizzato alla Riforma del Diritto di famiglia, depositato recentemente dai deputati e senatori in Parlamento come proposta e disegno di legge;

considerato che l’Italia è uno dei pochi paesi europei a non aver ancora adeguato la sua legislazione in materia di diritto di famiglia, ignorando completamente anche quanto stabilità dai Trattati di Nizza e di Lisbona, lasciando così spazio a molteplici forme di discriminazione;

considerato che l’attuale governo italiano ha sempre sbandierato demagogicamente i valori della famiglia tradizione (modello mulino bianco), ispirandosi ad una visione fondamentalista-talebana-vaticana, impregnata di ideologia reazionaria e conservatrice di forte ipocrisia familista, senza che abbia mai tenuto conto delle diverse e molteplici famiglia che si affermano sempre più nella società moderne, con necessità di forme di regolamentazione, aiuto e sostegno;

considerata anche la totale inadempienza rispetto ad alcuni impegni presi da esponenti del governo, ad inizio legislatura, riguardo un possibile intervento di riforma del Codice civile in materia di unioni civili;

considerato che i giorni 8 9 10 novembre 2010, il governo terrà a Milano una Conferenza sulla Famiglia, inaugurata, tra gli altri, dall’Arcivescovo di Milano con relazione di apertura del Presidente del Consiglio Silvio Berlusconi e Carlo Giovanardi, alla quale interverranno esponenti dell’integralismo cattolico, con l’obiettivo dichiarato “che porterà all’elaborazione del Piano nazionale delle politiche per la famiglia” del governo italiano;

considerato che non è stata invitata nessuna delle associazioni che in Italia sono impegnate sui temi che riguardano la promozione e la difesa dei diritti civili, in particolare su: testamento biologico, parentela, matrimonio civile tra persone omosessuali, divorzio breve, cognome della madre e/o padre dei figli, mediazione familiare, assegnazione di case alle famiglie anche conviventi bisognose, modifiche alla legge per l’affido condiviso, unione civili, filiazione, procreazione assistita, adozione, responsabilità genitoriale, ecc. e che pertanto tale Conferenza governativa non sarà inclusiva delle diverse proposte e realtà del paese;

impegna Radicali Italiani ad un’immediata e forte mobilitazione affinchè la mattina dell’8 novembre 2010, davanti alla sede governativa della Conferenza di Milano, si svolga un sit-in nonviolento e, a seguire, si prenda parte al convegno promosso da Radicali Italiani e dall’associazione Radicale Certi Diritti su ‘Le famiglie italiane tra politica, società, diritto ed economia’ che affronterà con studiosi, esperti politici e rappresentanti di associazioni i temi della campagna di Amore Civile per la Riforma del Diritto di Famiglia quale vera proposta alternative di governo per la laicità delle istituzioni d la libertà e responsabilità delle persone.