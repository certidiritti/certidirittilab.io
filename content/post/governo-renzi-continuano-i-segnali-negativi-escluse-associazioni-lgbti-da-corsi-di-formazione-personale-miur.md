---
title: 'Governo Renzi: continuano i segnali negativi, escluse associazioni LGBTI da corsi di formazione personale MIUR'
date: Wed, 24 Sep 2014 09:33:56 +0000
draft: false
tags: [Movimento LGBTI]
---

[![7165-Miur](http://www.certidiritti.org/wp-content/uploads/2014/09/7165-Miur-300x224.jpg)](http://www.certidiritti.org/wp-content/uploads/2014/09/7165-Miur.jpg)Dopo un blocco di 6 mesi, il 18 settembre ci è stato comunicato che l’iniziativa di formazione e sensibilizzazione contro omofobia e transfobia diretta alle figure apicali del MIUR riprenderà nel mese di ottobre. Formalmente l’iniziativa era stata sospesa per motivi tecnici, in realtà il Ministero non è riuscito a sostenere le polemiche che da una parte della gerarchia cattolica, dell’associazionismi familiare e dall’interno del Ministero stesso erano pervenute contro questa iniziativa. Apparentemente, il programma non ci è stato mostrato, il nuovo corso è stato totalmente internalizzato e rimodulato rispetto al programma iniziale. Ciò significa che il corso è stato sottratto alle attività della Rete Ready e che le associazioni LGBTI vengono escluse dalla partecipazione. Non sappiamo cosa pensi UNAR dell’esclusione della Rete Ready, ma sicuramente noi troviamo scandalosa l’esclusione delle associazioni LGBTI, il cui coinvolgimento non è solo previsto dalla stessa Strategia (e il Ministero ne era a conoscenza dal primo momento) ma si rende necessario se si vuole fare quel passaggio dalla teoria alla pratica che realmente si può concretizzare nelle scuole italiane, forti delle esperienze ormai decennali che le associazioni stesse hanno su questa materia. Noi teniamo fermo quanto dichiarato dalla prof.ssa Giannini, durante l’incontro con le associazioni LGBTI, sul grande interesse del Ministero a combattere ogni forma di discriminazione e a tutelare ogni “alterità”. Perché questo avvenga occorre però che il corso si svolga (e questo pare essere garantito) e che si faccia completa chiarezza sul programma e sui soggetti coinvolti. Chiediamo quindi al Ministero e alla Ministra personalmente di comunicare ufficialmente le modalità di svolgimento del corso e di salvaguardare l’attuazione della Strategia nazionale LGBTI.  

Associazione Libellula - Leila Daianis

Associazione Radicale Certi Diritti - Yuri Guaiana

Circolo di Cultura Omosessuale Mario Mieli - Andrea Maccarrone

Circolo Tondelli LGBTI - Giuseppe Sartori

Dì Gay Project - Maria Laura Annibali

Equality Italia - Aurelio Mancuso

I Ken Campania - Carlo Cremona

Renzo e Lucio - Mauro Pirovano

Rete Genitori Rainbow - Cecilia d'Avos e Fabrizio Paoletti

Stonewall - Tiziana Biondi