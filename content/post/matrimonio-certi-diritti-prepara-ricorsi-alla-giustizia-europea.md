---
title: 'Matrimonio: Certi Diritti prepara ricorsi alla giustizia europea'
date: Tue, 29 Mar 2011 09:34:43 +0000
draft: false
tags: [Affermazione Civile]
---

Certi Diritti rilancia la campagna per il matrimonio gay, che ora guarda all'Europa, e invita le coppie dello stesso sesso a presentarsi nei comuni e chiedere le pubblicazioni. E' grazie a questa iniziativa chiamata 'Affermazione Civile' che si è giunti alla sentenza 138/2010.

Roma, 27  marzo 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Il 15 Aprile di un anno fa la Corte Costituzionale (sentenza 138/2010) è intervenuta sulla decisione relativa alla costituzionalità del rifiuto al matrimonio per le coppie dello stesso sesso.  
  
Nella sentenza sono stati sanciti inequivocabilmente i seguenti principi:  
• Le coppie omosessuali hanno rilevanza costituzionale e non sono un mero fatto privato;  
• Il matrimonio tra persone dello stesso sesso è compatibile con l’attuale Costituzione;  
• Il Parlamento ha il dovere di legiferare in materia con una regolamentazione organica e generale, dunque non privatistica e parziale, se non lo fa si può ricorrere alla Corte per vedersi riconosciuti alcuni diritti.  
  
La sentenza è stato frutto di un movimento di impegno civico chiamato “Affermazione Civile”, il cui spirito è stato sempre condiviso tra l’Associazione Radicale Certi Diritti, la rete di avvocatura LGBT - Rete Lenford e le decine di coppie che, dal 2007, si sono recate a decine nei loro comuni di residenza per chiedere le pubblicazioni degli atti e quando il Comune opponeva loro il diniego si sono incardinate le iniziative legali.  
Ad un anno da questa sentenza è giunta l’ora di alzare il tiro, e procedere con nuovi ricorsi nei confronti della Giustizia Europea, aprendo Affermazione Civile ad ogni coppia, ad ogni associazione e ad ogni avvocato e giurista che voglia condividere questa nuova fase della campagna.  
  
Per questo, proprio in occasione dell’anniversario di questa importante sentenza, la nostra Associazione Certi Diritti, annuncia che, grazie all’aiuto e all’assistenza di alcuni autorevoli giuristi e avvocati, capitanati da Marilisa d’Amico, Massimo Clara e, tra gli altri, da Filomena Gallo, sta per presentare **ricorsi alla Corte Europea dei Diritti dell’Uomo e alla Corte di Giustizia Europea per vederci riconosciuti quei diritti negati dalla classe politica italiana**.  
  
In questa seconda fase di Affermazione Civile **chiediamo ai cittadini, alle coppie dello stesso sesso, alle Associazioni che si battono per la promozione e la difesa dei diritti civili, di diventare loro stessi protagonisti facendosi loro stessi promotori di questa battaglia**.  
Non dobbiamo vincere noi ma il paese intero ed è per questo che occorre dare forza e impegno civile alla ‘via legale’, proprio perché quasi tutta la classe politica italiana rimane indifferente alle richieste di una parte della società.  
  
Occorre considerare **questa campagna come un vero e proprio Pride**, un gesto concreto, non solo simbolico, che ha l’obiettivo di scardinare il muro di chi ci è contro e dare, dignità, legalità e speranza a milioni di cittadini.