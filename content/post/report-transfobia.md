---
title: 'REPORT TRANSFOBIA'
date: Mon, 16 Nov 2009 13:52:11 +0000
draft: false
tags: [Comunicati stampa]
---

### REPORT

dei principali atti di violenza transfobica accaduti in Italia nel corso del 2009 tratti dagli articoli comparsi sulla stampa

(Arcigay non è responsabile dei contenuti)

3/01/2009

NAPOLI. Un transessuale, del quale non si conosce ancora l'identità, è stato trovato morto in vico Sant'Alessio, nella zona del Lavinaio, vicino a piazza Mercato, nel centro di Napoli. Gli investigatori ancora non hanno chiarito se sia stato ucciso a coltellate o a colpi di pistola.

03/01/2009

PERUGIA. Non aveva più’ una casa dove andare (era stata sequestrata dalla Polizia giorni fa perchè lì dentro si esercitava la prostituzione) e l’unico rifugio che era riuscito a trovare era una vecchia Ford Escort parcheggiata e, pare, abbandonata dal proprietario in via del Macello. Lui D.S., transessuale brasiliano di 24 anni, clandestino, che fino a poco tempo fa abitava in un palazzo della zona, dopo avere sfondato un vetro, si era sistemato nella vecchia Ford. Una coperta, il sedile posteriore come letto: era tutta la sua casa, un posto dove andarsi a riposare. Quell’auto però con il vetro rotto ha insospettito una pattuglia della volante, che l’altra sera è andata a scoprire cosa fossse successo. Gli agenti si sono trovati davanti il giovane trans avvolto nella sua coperta profondamente addormentato. Dal momento che non aveva rubato l’auto, il viados è stato solo denunciato per danneggiamento e subito è stata avviata la pratica per il rimpatrio. Ma non sarà facile rimandarlo in Brasile — almeno in tempi rapidi — perchè il giovane, che pare abbia diverse gravi patologie, dovrà essere prima sottoposto a una lunga serie di accertamenti clinici. Dopodiché se le sue condizioni di salute lo consentiranno sarà rimpatriato.

14/01/2009

MILANO. Rifiuta una prestazione gratuita e viene rapinato. Vittima un transessuale brasiliano che, dopo aver detto di no a un egiziano, è stato spinto a terra e rapinato della borsetta. Il nordafricano, con precedenti per rapina e furto, è poi scappato, ma non ha fatto molta strada: è stato fermato ed arrestato da un equipaggio dei carabinieri. L'episodio è avvenuto l'altra sera, poco prima delle 22.30, in via Novara, all'angolo con via Romanello.

20/01/2009

ROMA. Due pregiudicati romeni, di 26 e 35 anni, sono stati arrestati dai Carabinieri del Nucleo Radiomobile di Roma, dopo aver tentato di rapinare un transessuale brasiliano 31 anni. I romeni, dopo aver colpito il transessuale con una bottiglia, hanno tentato di scippargli la borsa: il brasiliano, seppur dolorante, ha opposto resistenza al tentativo dei due, che se la sono data a gambe elevate. Un equipaggio del Nucleo Radiomobile, in transito proprio in quel momento, attirato dalle urla del brasiliano, dopo essersi accertato delle condizioni del trans, si è messo sulle tracce dei due fuggitivi, intercettandoli e bloccandoli nei pressi di via Emilio Longoni. Arrestati con l'accusa di tentata rapina e lesioni personali, i due romeni sono stati associati al carcere di Regina Coeli. Il trans, visitato dai sanitari dell'ospedale Sandro Pertini, è stato dimesso con una prognosi di 8 giorni.

29/01/2009

PARMA. Un ucraino di 20 anni è stato arrestato la scorsa notte dai carabinieri a Ponte Taro dopo aver tentato di rapinare un viado armato di una pistola ad aria compressa. Il giovane, attorno all’1.30, si è fermato in un’area di servizio dove il viado stava aspettando i clienti. Dopo averlo fatto salire in macchina, gli ha però puntato al collo la pistola intimandogli di consegnare quello che aveva nella borsa. Il transessuale ha reagito mordendogli una mano, ma l’altro gli ha sparato sette colpi ferendolo leggermente. A quel punto il viado ha sfilato dal cruscotto le chiavi dell’auto ed è scappato nei campi. L’ucraino prima ha provato a inseguirlo, poi ha chiamato i carabinieri dicendo di essere stato aggredito

22/02/2009

CASERTA. Provincia di Caserta, l´aggressore sceglie la sua vittima: è un transessuale in attesa di clienti. Punta al telefonino che ha in mano. Così, a bordo della sua Smart, si avvicina con fare ammiccante lungo la strada di periferia di Maddaloni (Caserta) e chiede al giovane il prezzo per una prestazione sessuale. La vittima si avvicina, si china verso il finestrino aperto e a quel punto il bandito gli strappa il cellulare di mano. Quindi schiaccia il piede sull´acceleratore, e fugge via con il bottino. Solo che la vittima non molla, si aggrappa alla portiera e il bandito lo trascina per alcuni metri. La vittima si ferisce in più parti del corpo: lividi e contusioni fino a quando, stremato, non lascia la presa. Il malvivente è intanto lontano. La sua auto però viene segnalata alla polizia, e gli agenti del commissariato di Acerra, ieri, notano la Smart in via Calabricito. Alzano la palina dell´alt ma il bandito non si ferma. Forza il posto di blocco, fugge. Inseguito e preso lungo la strada statale 162. Sotto il sedile, il malvivente - Fabio Scognamiglio, 24 anni - nasconde ancora il cellulare del transessuale. Viene arrestato.

07/03/2009

MONTESILVANO (PE). Il suo sogno era operarsi per diventare davvero, finalmente, una donna. Ma non ce l’ha fatta M.T., in arte Michelle, transessuale di 32 anni originaria di Giulianova che ieri mattina si è impiccata dopo aver affidato le sue ultime parole a un cartellone alto un metro trovato dai carabinieri sul tavolo di casa sua, in via Grecia. Quasi a voler gridare la disperazione che le scoppiava nel cuore, Michelle ha scritto «La vita era diventata impossibile» e poi, dopo aver firmato quattro lettere indirizzate a parenti e amici, ha messo in atto il suo gesto estremo. Sono stati i vicini di casa a dare l’allarme, alle sette e mezza di ieri mattina: il corpo era sul balcone, al primo piano di una delle palazzine di fronte a Porto Allegro.

27/04/2009

ROMA. Due 40enni hanno rapinato la borsetta a un trans: a bordo della loro auto, nella notte hanno avvicinato un transessuale brasiliano sulla via Casilina, e minacciandolo con una pistola lo hanno costretto a consegnarla. Ha però assistito un carabiniere, che dopo aver chiesto l´intervento di altri militari le ha seguite, bloccate e arrestate. La pistola era ad aria compressa, senza tappo rosso. Risponderanno di concorso in rapina aggravata

04/05/2009

FIRENZE. Nella notte tra il 2 e 3 maggio, al parco delle Cascine di Firrenze due ragazze trans sono state aggredite da una decina di ragazzi italiani tra i 17 e i 25 anni armati di spranghe di ferro: lo denuncia Matteo Pegoraro, attivista per i diritti umani col Gruppo Everyone, il quale afferma che "il gruppo di transfobici le ha dapprima minacciate, insultate e successivamente ha scagliato la propria violenza sulle loro automobili. Si tratta di un episodio gravissimo, che riporta all'attualità il dramma di migliaia di persone omosessuali e transessuali che nel nostro Paese devono fare i conti, rimettendoci in prima persona, con la discriminazione e il pregiudizio, che sfociano ormai sempre più frequentemente in episodi incontrollati di violenza".

30/06/2009

BOLOGNA. Due transessuali sono finiti in ospedale questa notte a Bologna dopo essere stati vittime di uno scherzo di cattivo gusto: due auto li hanno affiancati, mentre ieri sera si prostituivano in via Stalingrado, e hanno sparato contro di loro con un estintore. I due sono stati colpiti agli occhi e hanno dovuto farsi medicare, ricevendo cinque e sette giorni di prognosi. Un terzo transessuale che ha assistito alla scena, però, e' subito balzato in macchina e si e' messo a inseguire gli aggressori. Gli e' stato dietro fino a via Massarenti, schivando anche l'estintore che a un certo punto gli e' stato lanciato contro da una delle due auto in corsa. Imperterrito, e' riuscito a inseguire gli autori del gesto fino a Massarenti, poi li ha persi di vista. Il fatto e' successo stanotte intorno alle tre in via Stalingrado all'altezza del Mercatone Uno: due auto, una grigia e una nera, si sono accostate ai due trans ed e' subito partito il getto dell'estintore. La Polizia e' stata poi allertata dal personale del Pronto soccorso dell'ospedale Sant'Orsola. I due feriti sono un ragazzo di Ragusa di 32 anni e un comasco di 38. Il terzo uomo che si e' messo a inseguire le due auto e' anch'egli di Ragusa e ha 31 anni: secondo quanto ha riferito alla Polizia, non e' la prima volta che capitano fatti del genere. Lui stesso aveva sentito parlare di precedenti episodi di scherno contro suoi "colleghi". L'estintore e' stato ritrovato all'incrocio tra via del Lavoro e via Vezza.

04/07/2009

BOLOGNA. Pochi giorni dopo essere finito nel mirino di alcuni teppisti, è stato rapinato da un finto cliente ma l'ha fatto arrestare dalla polizia. Ancora una disavventura per un transessuale di 32 anni, originario di Ragusa, che si prostituisce in via Stalingrado. Verso le tre dell'altra notte è stato avvicinato da uno straniero che, spacciandosi per un cliente, gli ha strappato di mano la borsetta, trascinandolo a terra. Il rapinatore è scappato in auto ma il ragusano, nonostante la caduta che gli ha provocato lesioni guaribili in tre giorni, è riuscito ad annotare la targa dell'auto. La polizia ha rapidamente individuato l'aggressore, Tesfaj Tewelde, etiope di 43 anni, regolare e con precedenti specifici, rintracciato nella sua abitazione e ancora in possesso della borsa appena trafugata. Già nel 2001 era stato denunciato per avere derubato un transessuale, sempre in zona Fiera. Questa volta l'etiope è stato arrestato per rapina e lesioni finalizzate alla rapina. Il trentaduenne siciliano è uno dei due transessuali che, lunedì scorso, erano stati bersaglio del raid di alcuni giovani, che avevano scaricato loro in faccia il getto di due estintori.

10/07/2009

BELLUNO. Due tappe probatorie fondamentali per l’agente penitenziario di Belluno accusato da un detenuto transessuale, rinchiuso nell’apposita sessione di Baldenich, di averlo costretto ad un rapporto orale. Ieri il magistrato titolare dell’inchiesta ha affidato l’incarico ad un professionista di eseguire l’esame del Dna.. Nel pomeriggio, in carcere, si è svolto invece un confronto tra le parti, alla presenza dell’avvocato difensore e del pubblico ministero, per raccogliere le testimonianze di tutti i soggetti coinvolti. Ma sarà il risultato del Dna a scrivere la parola decisiva, senza possibilità di errore. L’indagato non si è sottratto all’esame, affermando di essere stato accusato ingiustamente. Diversa la versione che sarebbe stata resa da altri detenuti, secondo la quale l’uomo avrebbe più volte abusato della sua posizione, costringendo i trans a soddisfare le sue voglie, forte del potere che la divisa e il ruolo gli conferiscono. Ma, in questo caso, la vittima ha fatto scattare la trappola. Ora l’agente è indagato ufficialmente per violenza sessuale commessa con abuso di potere. Reato pesantissimo per il quale rischia davvero di passare dall’altra parte delle sbarre.

13/07/2009

RIVA LIGURE (IM).Un transessuale di 26 anni, italiano, è stato soccorso dai medici del 118, dopo la segnalazione di due passanti, che lo hanno visto a terra, ieri alle 12.30, sanguinante, sulla strada verso Pompeiana. Il 26enne, che lavora nelle discoteche della zona, sarebbe stato violentato. I Carabinieri della locale stazione hanno parlato con il 26enne che, però, avrebbe deciso di non sporgere querela. Molte le ipotesi sul fatto, tra cui quella che il giovane possa essere stato violentato da qualche persona, che aveva conosciuto il transessuale, poco prima in un locale.

22/07/2009

FIRENZE. Dopo una breve vacanza trascorsa a Gardaland, 3 amiche, 2 transessuali ed una donna, si sono fermate a Firenze per qualche ora. Verso l’una, nella centrale piazza della Signoria, le tre sono state avvicinate da 3 giovani stranieri che hanno iniziato a fare volgari apprezzamenti contro di loro. Alle rimostranze verbali da parte di una di loro, i 3 hanno cominciato ad inverirle contro, a picchiarla selvaggiamente colpendola alla testa. Vanessa, la giovane transessuale, è caduta a terra priva di sensi. Il fortunoso intervento di una pattuglia dei carabinieri, che ha chiamato un’ambulanza, ha evitato che la situazione degenerasse ancora di più. Condotta in ospedale la transessuale ha passato lì la notte. I tre aggressori sono stai identificati e denunciati.

04/09/2009

MILANO. E' accusato di ripetuta violenza sessuale ai danni di una transessuale brasiliana, condannata per furto. L'indagato è un agente della polizia penitenziaria in servizio nel carcere milanese di San Vittore dove la trans è detenuta. I fatti si sarebbero svolti tra giugno e settembre del 2008, ma la notizia si è diffusa solo adesso perché pare che l'imputazione ai danni dell'agente non sia stata immediata. Il sostituto procuratore della Repubblica Isidoro Palma avrebbe svolto prima delle lunghe indagini per verificare la possibilità di iscrivere l'uomo nel registro degli indagati dato che le uniche prove ai suoi danni erano le dichiarazioni della trans. Secondo la vittima, le violenze sarebbero state quattro, tutte verificatesi durante le ore notturne, tranne una avvenuta nel pomeriggio. Il luogo prescelto dall'agente per consumare la violenza sarebbe stato il locale utilizzato dai sottufficiali nel quale portava la trans dopo averla prelevata dalla sua cella.  
I colleghi dell'uomo, come era prevedibile, si sono schierati tutti in sua difesa sostenendo, tra l'altro, che sarebbe impossibile portare qualcuno in quella stanza senza essere notati.

08/09/2009

LECCE. Luana Ricci, anni 46, sposata con due figli, un ragazzo di 18 anni e una ragazza di 15, all’anagrafe risulta ancora essere Marco Della Gatta. Diplomata in pianoforte al Conservatorio di Lecce e in musica jazz a quello di Bari, Luana vanta un curriculum di tutto rispetto, nell’ambiente musicale gode di grande rispetto e la sua arte è molto apprezzata. Dal 1991 lavora per la Diocesi, pur non avendo mai firmato un contratto e non avendo mai percepito i contributi. Ora che la Curia l’ha cacciata senza nemmeno una spiegazione e lei farà valere i propri diritti. Però prima di abbandonare per sempre il suo lavoro di organista , tornerà in chiesa ancora un paio di volte vestita da uomo per rispetto alle coppie che si devono sposare. Il ben servito lo ha ricevuto il 31 agosto scorso perché grazie all’Onig (Osservatorio nazionale sull'identità di genere che ha una sede anche a Bari) da un anno e mezzo ha iniziato il suo percorso di transizione e ora grazie agli ormoni le è cresciuto il seno.

29/09/2009

ROMA. Costringeva il convivente transessuale argentino a prostituirsi, ma è stato arrestato dai carabinieri del nucleo operativo di Pomezia per favoreggiamento della prostituzione e dell’immigrazione clandestina e anche per detenzione e spaccio di sostanze stupefacenti. C.P., catanese di 39 anni, viveva con il transessuale in un appartamento di Torvaianica da dove partiva ogni sera per accompagnare l’argentino a lavorare sui marciapiedi di Trigoria. Il sudamericano era guardato a vista dal suo aguzzino che aspettava in auto, lontano dalla strada, che il convivente finisse la giornata di “servizio” per poi riportarlo a casa.

29/09/2009

PISA. Una bravata pagata a caro prezzo da tre giovani pisani di buona famiglia', finiti nella casa circondariale Don Bosco dopo aver derubato un transessuale. I protagonisti di questo ennesimo squallido episodio microcriminalità sono tre ventenni che abitano a Marina di Pisa. C.A. di 28 anni, S.M. di 22 anni ed E.V di 21 anni sono stati tratti in arresto dalla polizia perché responsabili, in concorso tra loro, del furto con strappo in danno di un viado ventiquattrenne.

15/10/2009

ROMA. Un trans di circa 45 anni è stato investito nella notte tra martedì e mercoledì all'Eur da un'auto che poi è fuggita. Secondo le prime informazioni, la donna, circa 45 anni, è stata investita da un'auto tra mezzanotte e le due nella zona dell'Eur intorno al Palalottomatica. I ricordi sono molto confusi, ma le amiche presenti al fatto, raccontano che la macchina, con all'interno un paio di persone, avesse girato intorno alla donna più di una volta. Poi, ha cominciato a seguirla fino ad investirla. A qual punto la 45enne, straniera, ma in Italia da 25 anni, sarebbe stata investita. L'auto a quel punto è fuggita lasciandola lì per terra senza sensi.