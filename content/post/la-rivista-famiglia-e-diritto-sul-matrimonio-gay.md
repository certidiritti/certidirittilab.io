---
title: 'LA RIVISTA "FAMIGLIA E DIRITTO" SUL MATRIMONIO GAY'
date: Tue, 03 Aug 2010 08:24:28 +0000
draft: false
tags: [Comunicati stampa, CORTE COSTITUZIONALE, famiglia e diritto, matrimonio gay, Sentenza, si lo voglio]
---

MATRIMONIO GAY. PUBBLICATO COMMENTO ALLA SENTENZA DELLA CONSULTA: NON È NECESSARIA REVISIONE COSTITUZIONALE  
  
La più prestigiosa rivista italiana del settore, "Famiglia e diritto", pubblica nel numero corrente 07/2010 un commento alla sentenza 138/2010.

  
Il commento della rivista "Famiglia e diritto", la più prestigiosa edizione italiana del settore, a firma di Marco Gattuso, magistrato, fa il punto della situazione sulla questione dell'accesso al matrimonio civile per le persone omosessuali, dopo la sentenza della Corte Costituzionale.  
  
I passaggi fondamentali del commento rappresentano altrettanti punti fondamentali per la lotta all'uguaglianza.  
  
Così leggiamo che "la Corte compie un deciso passo in avanti, individuando nella "unione omosessuale, intesa come stabile convivenza tra due persone dello stesso sesso", una "formazione sociale" tutelata dall'art. 2 Cost.. Si configura dunque il diritto fondamentale al libero sviluppo della persona anche nell'ambito della coppia omosessuale". La Corte infatti "annette una specifica rilevanza costituzionale alla stessa nozione giuridica di orientamento sessuale, universalmente intesa come orientamento verso persone dell'opposto o del proprio genere e con tale accezione già recepita dall'art. 21 della Carta dei diritti fondamentali".  
  
La Corte declina il contenuto di tale diritto fondamentale rilevando che "a tale unione non spetta soltanto il diritto "di vivere liberamente una condizione di coppia" ma altresì "il riconoscimento giuridico con i connessi diritti e doveri". Si ha qui un passaggio di portata storica, che segna il superamento d'ogni concezione volta a consumare la vocazione liberale del sistema giuridico italiano nel mero rispetto della vita privata".  
  
Affermando che "in forza della Costituzione a tale comunità "spetta un riconoscimento giuridico" che "necessariamente" richiede una disciplina, la Consulta individua difatti una lacuna nella nostra legislazione e chiama il legislatore a colmarla". "L'eventuale apertura del matrimonio implica scelte rimesse alla discrezionalità del legislatore", che dunque ha piene possibilità d'azione.  
  
"In tutta la motivazione, la Corte non introduce alcun elemento espressamente diretto a condizionare la discrezionalità del Legislatore, né sarebbe conforme alla sua pregressa giurisprudenza in materia familiare coartare, in un senso o nell'altro, la volontà parlamentare".  
  
"Dalla lettura della sentenza non emerge peraltro alcun argomento per sostenere che l'apertura del matrimonio violi diritti od interessi di terzi e della famiglia eterosessuale e che dunque si contrapponga alla ratio di garanzia della norma. Ne consegue che se al Legislatore ordinario è preclusa dall'art. 29 Cost. una normativa che limiti i diritti della famiglia, non deve ritenersi preclusa, invece, la ridefinizione per via legislativa della nozione di "matrimonio" in senso non limitativo ma, anzi, inclusivo".  
  
La Corte afferma altresì che "resta "riservata alla Corte Costituzionale la possibilità d'intervenire a tutela di specifiche situazioni" ove "sia riscontrabile la necessità di un trattamento omogeneo tra la condizione della coppia coniugata e quella della coppia omosessuale, trattamento che questa Corte può garantire con il controllo di ragionevolezza". L'affermazione della sussistenza d'un diritto fondamentale conduce dunque ad assicurare tutela giuridica ogni qualvolta sia riscontrabile la necessità d'una eguale protezione".  
  
Quest'ultima affermazione della Corte "è di particolare impatto, posto che alla luce di tale indicazione, si dovrà ritenere che ogni giudice sia chiamato ad accertare di volta in volta se nella situazione data "sia riscontrabile la necessità di un trattamento omogeneo", verificando preventivamente se il dispositivo favorevole alla coppia coniugata possa essere applicato anche alla coppia omosessuale".  
  
La sentenza della Corte, quindi, spalanca le porte alle via giudiziaria per l'ottenimento della piena uguaglianza, fatto, questo, tanto più importante quanto permane immobile il panorama politico italiano.  
  
"È inoltre significativo che la Corte preannunci l'intenzione di porre a confronto la coppia omosessuale e la coppia "coniugata". La notazione pare di particolare importanza, poiché sarebbe allora una mera petizione di principio negare un diritto riconosciuto alla coppia coniugale sul solo rilievo della mancanza del vincolo matrimoniale, venendosi a negare in radice proprio quella verifica in concreto postulata dalla Corte".  
  
"Appare a tale proposito d'un certo interesse che dalla lettura della sentenza, fatto salvo il mancato accesso all'istituto del matrimonio, la Corte non enunci alcuna disomogeneità ontologica tra affettività etero ed omosessuale".  
  
  
COMITATO "SÌ, LO VOGLIO!"  
per il riconoscimento del diritto  
al matrimonio civile alle persone omosessuali  
  
Comunicato stampa  
Giovedì 30 Luglio 2010