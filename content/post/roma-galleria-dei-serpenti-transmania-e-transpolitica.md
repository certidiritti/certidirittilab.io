---
title: 'ROMA, GALLERIA DEI SERPENTI - TRANSMANIA E TRANSPOLITICA'
date: Wed, 28 Jan 2009 09:26:09 +0000
draft: false
tags: [Comunicati stampa]
---

[![Clicca sull'immagine per vederla nelle sue dimensioni reali](http://www.certidiritti.org/wp-content/uploads/2009/01/flyer_ridotto_retro.jpg "flyer_ridotto_retro.jpg")](http://www.certidiritti.org/wp-content/uploads/2009/01/flyer_ridotto_retro.jpg)

Mercoledì 28 gennaio, a Roma, alla Galleria dei Serpenti (Via dei Serpenti, vicino Via Cavour)  a partire dalle ore 20, per tutta  la sera, si terrà un importante evento culturale dedicato al mondo delle persone transessuali.  
  
Di    seguito trovi i dettagli dell'iniziativa promossa e organizzata da Luciano Parisi con la partecipazione di Certi Diritti e altre Associazioni.

  
Priscilla:  dal deserto alle porte di S.Pietro Transmania e transpolitica  
A **Roma il 28 Gennaio 2009 ore 20,00** Vernissage

**Artisti :**

**Sandro Martini** \- fotografia  
  
**Alessandra Bramante** \- pittura

  
**(Esposizione dal 28-1 al 4-2)**  
  
ore    21,30       performance

ore 22,00          video-arte di    Sandro Martini: laughing Bum

video  di luciano Parisi  "pride 2007" - transmania, la dea a 2 lingue

**A seguire dibattito con**

**Roberta Franciolini** \- M.I.T.Roma  
  
**Sergio Rovasio -** Associazione radicale Certi Diritti  
  
**Giulio Allegrezza**  
  
**Andrea Maccarrone**

Galleria dei Serpenti - Roma via de serpenti 32

  

  

**Programma definitivo**  
  
"Benvenuti nel nostro mondo  imperfetto" I quadri saranno gia' in galleria dal martedi  sera. l'instalalzione domani mercoledi alle 12,00   vernissage alle ore 20,00   apertura con il video-arte, in contemporanea, di  Sandro Martini di Berlino VJ set con immagini archivio  image event(pride e altro)Angelo J. alle 21,30 probabile intervento di Serafino Iorli.

Alle 22,00 presentazione video **Trasmonia** di Luciano  Parisi e Angelo J.

Lettura in contemporanea dei testi del video a cura di Giorgio  Gigliotti alle 22,15 apertura conferanza dibattitto con certi  diritti e mit,introduzione **Luciano Parisi** e **Guido Allegrezza**.

Interverranno **Roberta Franciolini** e **Sergio Rovasio** e **Andrea Maccarrone** per la  cultura sono state invitate diverse persone politiche che possono interagire con domande e interventi.

Alle 23,00 circa dJset con  **Ugo Sances** festa festa (con  risate a martello) sarà allestita una lotteria a sorteggio di  alcuni quadri esposti i biglietti saranno di 5 euro cd. performer entratteniment alla  porta Qeer  Andrenaline   chiedo ai gruppi politici  e culturali di portare documenti politici fotocopiati o programmi da  distribuire durante la serata.