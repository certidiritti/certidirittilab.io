---
title: 'FAMIGLIA? NO, FAMIGLIE! TUTTI A MILANO L''8 NOVEMBRE'
date: Wed, 03 Nov 2010 13:18:57 +0000
draft: false
tags: [Comunicati stampa]
---

L'Associazione Radicale Certi Diritti sarà presente a Milano - con un sit-in, una conferenza stampa ed un seminario - lunedì 8 novembre, insieme a Radicali Italiani e ad Emma Bonino, in occasione dell'inaugurazione della Conferenza sulla famiglia organizzata dal sottosegretario Giovanardi, che nei giorni scorsi ha annunciato la presenza del Presidente del Consiglio Silvio Berlusconi.

Lunedì **8 novembre**, si inaugurerà a **Milano** la **Conferenza governativa sulla famiglia** organizzata e promossa proprio da coloro che fino ad oggi hanno negato politiche di riforma e di adeguamento legislativo riguardo i diversi ambiti delle molteplici forme di famiglia nel nostro paese.

Abbiamo potuto visionare il programma della Conferenza e vedere gli spot governativi su molti media italiani e ci pare evidente che la Conferenza esclude dal confronto le associazioni e le realtà che da sempre sono impegnate sui temi che riguardano le proposte di Riforma del diritto di famiglia, in particolare su testamento biologico, unioni civili, matrimonio gay, cognome padre/madre ai figli, adozioni, e molto altro.

Il progetto di Amore Civile, che è stato realizzato dopo due anni di lavoro di esperti e associazioni, radicali e non, si è concretizzato in una proposta di Riforma complessiva del Diritto di Famiglia, il tutto è stato raccolto in una proposta di legge depositata nei due rami del Parlamento dai deputati e senatori radicali e in volume della Mimesis Edizioni curato da Francesco Bilotta e Bruno De Filippis dal titolo '[Amore civile, dal diritto della tradizione al diritto della ragione](http://mimesisedizioni.blogspot.com/2010/01/amore-civile-presentato-il-15-1-milano.html)'.

Abbiamo convocato per lunedì 8 novembre, alle ore 9,30, **un sit-in con Conferenza Stampa** davanti alla sede che ospiterà l'evento governativo, il MIC (Milano Convention Centre), in **via Giovanni Gattamelata 5**, a Milano, al quale vi invitiamo a partecipare insieme a **Emma Bonino, Mario Staderini, Marco Cappato, Rita Bernardini, Marco Perduca** e **Sergio Rovasio**, con anche Associazioni e militanti impegnati sul fronte dei diritti civili e umani.

A seguire, dalle ore 10,30, per tutta la giornata, terremo una seminario (contro-conferenza) sul progetto di Riforma del Diritto di Famiglia. Ovviamente vi invitiamo a partecipare anche a questo importante evento che si svolgerà presso la Sala Lauree dell’Università di Milano, presso la Facoltà di Scienze Politiche, Via Conservatorio, 7 – Milano e vedrà la partecipazione di alcune delle personalità che hanno partecipato al Progetto di Amore Civile

* * *

  
**Di seguito il programma del convegno-seminario:**

**Organizzato dall’Associazione Radicale Certi Diritti e Radicali Italiani**

**LE FAMIGLIE ITALIANE tra politica, società, diritto ed economia  
**  
**Lunedì 8 novembre 2010 - dalle 10:30 alle 19:00  
Sala Lauree  
Università degli Studi di Milano - Facoltà di Scienze Politiche**  
**via Conservatorio, 7, Milano**

* * *

Il dibattito pubblico è sempre più sclerotizzato attorno a una definizione fallace di famiglia tradizionale che esclude inevitabilmente una grande quantità di cittadini italiani. Le implicazioni di quest’impostazione sono politiche, giuridiche, sociali ed economiche: ad esempio i cittadini omosessuali si vedono negato il diritto fondamentale di sposarsi e di costituire una famiglia; la realtà delle famiglie omogenitoriali continua a essere negata; le coppie che decidono di porre fine al loro rapporto sono vessate da innumerevoli pastoie, gli individui sono sempre più marginalizzati da un modello organicistico di welfare state. Di fronte a questa situazione  insostenibile l’**Associazione Radicale Certi Diritti** apre uno spazio di dibattito e confronto non su cosa debba o non debba essere considerato famiglia (al singolare), ma sulla realtà delle famiglie italiane, sui loro cambiamenti, su come il diritto possa riconoscere e regolare questa composita realtà e sulla misura nella quale il welfare state debba farsene carico.

Dopo gli auguri di buon lavoro del preside della facoltà di Scienze politiche, prof. Daniele Checchi e il saluto introduttivo di Sergio Rovasio, segretario dell’Associazione radicale Certi Diritti, il seminario si articolerà in tre sessioni:

1.      DI COSA PARLIAMO QUANDO PARLIAMO DI FAMIGLIE?  
Evoluzione di una definizione tra storia, antropologia, cultura e scienze.

Coordina **Paola Ronfani**, ordinario di Sociologia Giuridica, della Devianza e Mutamento Sociale. Ne parlano:

**Enrichetta Buchli**, didatta e docente della Scuola di Psicoterapia  
**Stefano Ciccone**, fondatore dell’associazione e rete nazionale Maschile Plurale  
**Giuseppina La Delfa**, presidente dell’Associazione Famiglie Arcobaleno

**Chiara Lalli**, docente di Logica e Filosofia della Scienza e di Epistemologia delle Scienze Umane  
  
2.      WELFARE A MISURA DI CAMBIAMENTI:  
Spunti di riflessione e tracce per un lavoro futuro sui principali cambiamenti delle famiglie italiane e sul loro rapporto con il welfare state. Coordina Emma Bonino, vice presidente del Senato. Ne parlano:

**Valeria Manieri**, radicali italiani  
**Giorgio Vaccaro**, avvocato-presidente dell'associazione circolo psicogiuridico  
  
3.      AMORE CIVILE:  
Una proposta di riforma del diritto di famiglia italiano. Coordina Marilisa D’Amico, ordinario di Diritto Costituzionale e avvocato cassazionista.

Ne parlano:  
  
**Ileana Alesso**, avvocato amministrativista e cassazionista, formatrice e docente in master universitari;  
**Massimo Clara**, avvocato civilista e cassazionista, formatore presso pubbliche amministrazioni e istituzioni ospedaliere;  
**Bruno De Filippis**, Giurista;  
**Elfo Frassino**, presidente del Coordinamento Nazionale Associazioni e Comunità di Ricerca Etica Interiore Spirituale;  
**Filomena Gallo,** avvocato specializzata in Diritto Minorile, Diritto di Famiglia, Diritto pubblico e vice segretario dell’Associazione Luca Coscioni;  
**Diego Sabatinelli**, segretario della Lega Italiana per il Divorzio Breve;