---
title: 'RICONOSCIUTO MATRIMONIO GAY IN ITALIA: ERRORE O NO E'' ORA DI MUOVERSI'
date: Mon, 10 Aug 2009 14:37:38 +0000
draft: false
tags: [Comunicati stampa]
---

**MATRIMONIO GAY: IN ATTESA CHE L’ITALIA RICONOSCA NON PER ERRORE IL MATRIMONIO GAY, ANCHE FATTO ALL’ESTERO, CONTINUA LA CAMPAGNA DI AFFERMAZIONE CIVILE PROMOSSA DA CERTI DIRITTI E RETE LENFORD.**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

“In attesa che la classe politica italiana comprenda che occorre quanto prima regolamentare le unioni civili ed estendere l’istituto del matrimonio alle coppie gay, la notizia che in Italia è stato riconosciuto il primo matrimonio tra persone omosessuali fatto all’estero è un fatto storico, di grande rilevanza civile. Purtroppo il valore di questo riconoscimento si regge solo sull’errore o interpretazione di un nome al femminile di uno dei due componenti la coppia di due uomini gay, certo non sufficiente a scardinare l’ottusità di quasi tutta la classe politica italiana.

Ricordiamo che in Parlamento sono state depositate proposte di legge, da parte dei parlamentari radicali del Pd, volte alla regolamentazione delle unioni civili e in favore del matrimonio gay. A sostegno del matrimonio gay è in corso in Italia una campagna nazionale di ‘Affermazione civile’, promossa dalle Associazioni Certi Diritti e dall’Avvocatura Lgbt – Rete Lenford, alla quale hanno finora aderito una trentina di coppie gay, che hanno presentato presso i loro Comuni di residenza le richieste delle pubblicazioni finalizzate al matrimonio. Quando il Comune oppone loro il rifiuto si incardinano le iniziative legali contro tali decisioni. Attualmente già due Tribunali, quelli di Venezia e di Trento, hanno rimesso alla Corte Costituzionale la decisione ritenendo fondate le ragioni delle coppie ricorrenti. Entro una decina di mesi la Corte si dovrà esprimere”.

Maggiori informazioni: [www.certidiritti.it](http://www.certidiritti.it/)