---
title: 'A TORINO MOBILITAZIONE CON CERTI DIRITTI PER REGISTRO UNIONI CIVILI'
date: Sat, 13 Sep 2008 17:30:41 +0000
draft: false
tags: [Comunicati stampa]
---

**Torino: presentata proposta di delibera di iniziativa popolare “Riconoscimento di pari opportunità per le unioni civili”**

**Torino, 11 settembre 2008** **• Comunicato dell'Associazione Radicale Satyagraha**

**Questa mattina, a Torino, presso l’Ufficio Relazioni Pubbliche della città di Torino, è stata presentata la Proposta di delibera di iniziativa popolare denominata “Riconoscimento di pari opportunità per le unioni civili”.**

**Il documento è stato sottoscritto dai seguenti presentatori: Stefano Mossino, Enzo Cucco, Jolanda Casigliani, Tullio Monti, Lidia Rizzo, Marco Chiauzza, Stefano Michele, Jean-Jacques Peyronel, Silvio Viale, Maurizio Nicolazzo, Vincenzo Francone, in rappresentanza delle seguenti associazioni: Associazione Radicale Satyagraha, Associazione Certi Diritti, Consulta Torinese per la Laicità delle Istituzioni, Casa delle Donne, Circolo Maurice, Centro Evangelico di Cultura “Arturo Pascal”, Comitato Torinese per la Laicità della Scuola, Associazione Adelaide Aglietta, Gruppo Lambda, ArciGay Torino.**

**Non appena il Segretario Generale del Comune comunicherà l’ammissibilità del testo, sarà convocata una conferenza stampa di presentazione della delibera da parte del Comitato promotore, il quale vede l’adesione di molte altre associazioni, oltre a quelle già citate e che speriamo possa ulteriormente estendersi anche a singole personalità**

**Da quel momento inizierà la raccolta delle 1500 firme necessarie, per un periodo di quattro mesi come previsto dal regolamento del comune di Torino.**

**Jolanda Casigliani –Associazione Radicale Satyagraha**

**Stefano Mossino - Associazione Radicale Satyagraha**

**Vincenzo Cucco - Associazione Certi Diritti**