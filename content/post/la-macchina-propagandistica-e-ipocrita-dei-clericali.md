---
title: 'La macchina propagandistica e ipocrita dei clericali'
date: Fri, 29 Oct 2010 07:56:15 +0000
draft: false
tags: [Comunicati stampa]
---

**La macchina propagandistica e ipocrita dei clericali di entrambe le sponde del Tevere si è messa in moto per negare l’esistenza di famiglie diverse da quelle del mulino bianco.  
Anche noi SAREMO a milano l’8 novembre, per parlare della realta’ delle famiglie esistenti e per presentare le proposte di riforma del diritto di famiglia.**

**ROMA, 29 OTTOBRE 2010**

**C****omunicato Stampa di Radicali Italiani e dell’Associazione Radicale Certi Diritti:**

La Conferenza Episcopale Italiana ha appena pubblicato gli Orientamenti pastorali per il decennio 2010-2020, sul tema Educare alla vita buona del Vangelo, dove sostiene che tra i tanti fattori che minano e destabilizzano l’istituto familiare, ci sono le convivenze di fatto e i divorzi sempre più numerosi, nonché "i tentativi di equiparare alla famiglia forme di convivenza tra persone dello stesso sesso".

E’ evidente che la CEI sta facendo pubblicità alla Conferenza Nazionale sulla Famiglia governativa organizzata a Milano dal Sottosegretariato della Presidenza del Consiglio dei Ministri con delega alle politiche familiari, Sen. Carlo Giovanardi, ma la cui agenda è stata praticamente stilata dalla CEI medesima. Si prepara così l'opinione pubblica ad assorbire i contenuti assolutisti della Conferenza. Iniziando a parlare di queste cose già 10 giorni prima dell’inizio della Conferenza, si abbassano le barriere e le resistenze e si vuole far credeere al paese che esiste solo un modello di famiglia, già propagandata attraverso spot tv.

**L’Associazione Radicale Certi Diritti e Radicali Italiani rilanciano allora l’invito a partecipare e aderire al sit-in fuori dal Milano Convention Centre (sede della Conferenza governativa ispirata dalla Cei) alle 9.45 dell’8 novembre nonché ad essere presenti allo spazio di dibattito e confronto tra giuristi, sociologi, psicologi e antropologi non su cosa debba o non debba essere considerato famiglia (al singolare), ma sulla realtà delle famiglie italiane, che si incontreranno  alle 10.30, sempre dell’8 novembre, presso la Sala Lauree dell’Università degli Studi di Milano - Facoltà di Scienze Politiche in via Conservatorio, 7.**