---
title: 'Ministro Terzi risponde a Certi Diritti sulla legge contro la cosiddetta propaganda omosessuale in Russia'
date: Tue, 19 Feb 2013 13:12:26 +0000
draft: false
tags: [Russia]
---

Comunicato Stampa dell’Associazione Radicale Certi Diritti.

Roma, 19 febbraio 2013

Il ministro degli Affari Esteri, Giulio Terzi, risponde il 12 febbraio alla sollecitazione dell’Associazione Radicale Certi Diritti sulla legge contro la cosiddetta propaganda omosessuale in Russia.

In risposta alla lettera inviata dall’Associazione Radicale Certi Diritti il 27 gennaio scorso, il  Ministro afferma che la problematica è ben nota al Ministero che la “sta monitorando attentamente anche tramite la nostra ambasciata a Mosca, in coordinamento la locale Delegazione dell’UE e le altre Rappresentanze europee”. Terzi aggiunge: “Condivido la Sua preoccupazione per l’estensione a livello federale di tale regolamentazione, che potrebbe significativamente limitare l’azione delle organizzazioni che tutelano i diritti delle persone LGBT e contribuire a rafforzare atteggiamenti discriminatori”. Secondo le informazioni in possesso del Ministero “mancherebbe al momento una definizione chiara della fattispecie, che sarebbe rinviata alla successiva elaborazione da parte della “Commissione famiglia, donne e minori” della stessa Duma in vista della seconda lettura del provvedimento”. Il Ministro conclude ribadendo “che il nostro Paese rifiuta ogni forma di discriminazione basata sull’identità di genere e l’orientamento sessuale e, sulla base di tale principio, non mancherà di partecipare alle ulteriori iniziative che dovessero essere intraprese in ambito europeo relativamente al caso da Lei segnalato”.

Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti, dichiara: “Ringrazio molto il Ministro per la cortese e puntuale risposta. Siamo confortati nel sapere che le l’Italia è allineata con i suoi partners europei sul caso in questione e che il Ministro condivide le nostre preoccupazioni sul recente voto alla Duma. Ci preme sottolineare, ancora una volta, che in gioco sono, non soltanto i diritti delle persone LGBT e anche I, ma il diritto alla libertà d’espressione di tutti. Ci auguriamo che l’Italia possa essere promotrice di azioni diplomatiche specifiche, naturalmente sempre in accordo con i nostri partners europei, per sventare l’approvazione definitiva di questa proposta di legge liberticida”.