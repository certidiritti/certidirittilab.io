---
title: 'RAPPORTO ONU: "GUERRA AL TERRORISMO" COLPISCE DIRITTI CIVILI, QUELLI LGBT'
date: Wed, 21 Oct 2009 07:03:09 +0000
draft: false
tags: [Comunicati stampa]
---

**LEGGI ANTITERRORISMO-DIRITTI UMANI: LA "GUERRA AL TERRORISMO" COLPISCE I DIRITTI CIVILI, IN PARTICOLARE QUELLI DELLE PERSONE LGBT, LO DICE IL RAPPORTO ONU DI MARTIN SCHEININ.**

**Dichiarazione di Matteo Mecacci, Deputato Radicale – Pd e Sergio Rovasio, Segretario dell’Associazione Radicale  Certi Diritti:**

"L'attacco in corso alle Nazioni Unite contro l’adozione del rapporto del Relatore speciale dell'ONU sull'impatto delle politiche anti-terroriste sui diritti umani, Martin Scheinin - rapporto che analizza l'impatto negativo delle politiche anti-terrorismo sui diritti umani, con particolare riferimento a quelli delle persone lgbt - rappresenta l’ennesimo tentativo di limitare la difesa dei diritti umani fondamentali in particolare delle persone transessuali e transgender.

Come alcuni mesi fa l’alleanza tra paesi autoritari e teocratici, ispirati soprattutto dal fondamentalismo religioso, ha impedito l’adozione di una Risoluzione per la depenalizzazione dell’omosessualità – che purtroppo vige ancora in 60 paesi – oggi si cerca di impedire l’emersione di una semplice verità così ben espressa dal Rapporto Scheinin: le persone transessuali e transgender subiscono vessazioni e limitazioni delle loro libertà e diritti molto pesanti, a causa di leggi frutto di una cultura emergenzialista e securitaria. Ci auguriamo che il Governo italiano si adoperi pubblicamente, da Roma e  da New York, per ottenere al più presto l’approvazione del Rapporto Scheinn e che venga indicato quale Relatore speciale dell’Onu per i diritti delle persone lgbt. Il silenzio di questi mesi e settimane è bene che abbia termine”.