---
title: 'Vice Comandante dell''Arma dei Carabinieri offende gay, polizia, Gdf, Magistrati, morti per suicidio. Esposto di Certi Diritti a Unar e Oscad'
date: Wed, 27 Jun 2012 11:38:15 +0000
draft: false
tags: [Politica]
---

Roma, 27 giugno 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

L’Associazione Radicale Certi Diritti ha oggi inviato una lettera esposto all’Unar e all’Oscad per denunciare le gravissime affermazioni del Vice Comandante dell’Arma dei Carabinieri, Clemente Gasparri, fratello del Capo Gruppo Pdl al Senato, che, secondo quanto riportato da ‘Il Fatto Quotidiano’ e secondo quanto denunciato dal Cocer dell’Arma dei Carabinieri martedì 26 giugno, è intervenuto nell’ambito del Corso di aggiornamento della Scuola Ufficiali dei Carabinieri di Roma con le seguenti frasi:

“Bene! Chi si è dato la morte lo ha fatto senza motivo, senza dare o lasciare spiegazioni. Come si può affidare a queste persone \`psicolabili\` la sicurezza delle nostre comunità nazionali? L\`Arma, ai miei tempi, era granitica! C\`è in atto un processo inesorabile di sgretolamento. Ammettere di essere gay, magari facendolo su un social network, come un graduato della Guardia di Finanza, non è pertinente allo status di Carabiniere. L\`Arma è come un treno in corsa, i passeggeri sono vincolati, prima di scendere, alla responsabilità di lasciare pulito il posto occupato. Gli ufficiali del Ruolo Speciale che fanno il ricorso, i giovani ufficiali dell\`applicativo che fanno istanze per avvicinarsi alla famiglia, gli omosessuali che ostentano la loro condizione, sono in sintesi tutti passeggeri sciagurati dell`antico treno, potenzialmente responsabili della sporcizia o del deragliamento. II Comandante Generale è una persona seria, che cammina da solo con il suo autista, non come il capo della Polizia che cammina con la scorta e una fila di macchine avanti e dietro. Che cambia tra i due? Il Comandante Generale è, forse, meno importante del capo della Polizia? Sono stato più volte chiamato dai magistrati come persona informata sui fatti, lo non ho paura di loro e loro lo hanno capito, e se lavorassero di più non avrebbero di certo tanto arretrato”.

Nella lettera-esposto l’Associazione Radicale Certi Diritti sollecita l’Unar a intervenire viste le gravissime tesi discriminatorie espresse dal Vice Comandante dell’Arma dei Carabinieri e chiede all’Oscad di verificare se l’organismo interforze della Poliza di Stato, nato con l’obiettivo di contrastare ogni forma di discriminazione, è a conoscenza di quanto accaduto e quali iniziative intende promuovere.

I deputati Radicali, primo firmatario Maurizio Turco, hanno oggi depositato una interrogazione urgente al Ministro della Difesa per sapere "quali immediate azioni intenda intraprendere per accertare se detti fatti corrispondano al vero e, in tale caso, sanzionare l'autore del gravissimo gesto anche attraverso la sospensione precauzionale dal servizio del generale Clemente Gasparri che con il suo comportamento avrebbe leso innanzitutto la dignità e la reputazione dell’Arma dei carabinieri e della stragrande maggioranza di essi".