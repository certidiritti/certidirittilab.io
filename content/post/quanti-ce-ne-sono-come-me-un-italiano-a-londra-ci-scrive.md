---
title: 'Quanti ce ne sono come me? Un italiano a Londra ci scrive'
date: Wed, 14 Sep 2011 21:04:29 +0000
draft: false
tags: [Politica]
---

Ciao Gabriella,  
mi chiamo Andrea e anche se vivo a Londra da più di dieci anni mi sento sempre italiano.  
Quando mi sono iscritto alla newsletter, non mi sarei mai aspettato una risposta personalizzata ma, come puoi capire, è stata molto ben accettata.

vi seguo da Londra poichè l’oggetto del sito [http://www.certidiritti.it](http://www.certidiritti.it) mi tocca molto profondamente. In realtà, una delle ragioni più importanti per cui vivo qua è il fatto che in Italia i DIRITTI CIVILI (moderni) sono pressochè inesistenti. Per alcuni la mia decisone potrebbe essere vista come una fuga o, peggio ancora, una codardia; io la vedo come sopravvivenza, una scelta integrale alla persona che sono, il mio dire “no” alle ipocrisie italiane, il rispettare le preferenze dei miei genitori che non vogliono parlare o far sapere della mia diversità e realtà che vivo... Comunque, quanti ce ne sono come me? Sarebbe bene parlarne. Non puoi immaginarti che bello sia svegliarsi di mattina e non sentirsi sbagliati, sentirsi senza i senzi di colpa, sentirsi vivi e contenti di essere quello che si è.

Le istituzioni dovrebbero cercare di educare i cittadini ad accettare le diversità, al rispetto invece di sprecare risorse nel censurare e coprire LA VERITA’, LA REALTA’!

Grazie ancora per la mail e GRAZIE INFINITAMENTE a te e tutti voi per l’impegno dimostrato nel tentare di migliorare le condizioni di persone vere che soffrono per colpa dell’ipocrisia ed il “perbenismo” ampiamente diffusi in Italia.

Andrea

**iscriviti alla newsletter >>>**

**[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**