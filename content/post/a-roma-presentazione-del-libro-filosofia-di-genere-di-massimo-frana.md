---
title: 'A ROMA PRESENTAZIONE DEL LIBRO: FILOSOFIA DI GENERE DI MASSIMO FRANA'
date: Sat, 29 May 2010 06:25:29 +0000
draft: false
tags: [Comunicati stampa]
---

Sabato 29 maggio dalle ore 20,00 alle 21,00 presso la Casa interzionale del cinema,  (Villa Borghese - Roma)

presentazione del libro "Filosofia di genere. Un libro per la cura dell'omofobia", di **Massimo Frana**, edizioni Croce, Roma.

**Interverranno:**  
**Prof.ssa Paola Balducci**, Docente di Procedura Penale, Università del Salento;  
**Prof. Elio Matassi**, Direttore del Dipartimento di Filosofia, Università di Roma Tre;  
**Sergio Rovasio**, Segretario nazionale dell'Associazione radicale "Certi Diritti".

 L'evento è aperto al pubblico.