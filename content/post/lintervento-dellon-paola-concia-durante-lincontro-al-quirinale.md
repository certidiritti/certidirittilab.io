---
title: 'L''INTERVENTO DELL''ON. PAOLA CONCIA DURANTE L''INCONTRO AL QUIRINALE'
date: Mon, 17 May 2010 10:29:45 +0000
draft: false
tags: [Comunicati stampa]
---

I**NTERVENTO DI PAOLA CONCIA DURANTE L'INCONTRO CON IL PRESIDENTE DELLA REPUBBLICA GIORGIO NAPOLITANO CON LE ASSOCIAZIONI LGBT.  
**

> **Pubblichiamo il testo integrale letto dall'On. Paola Concia durante l'incontro avuto stamane, lunedì 17 maggio, al Quirinale del Presidente della Repubblica On. Giorgio Napolitano con la Associazioni lgbt.**

Caro Presidente Napolitano,

la devo innanzitutto ringraziare per aver voluto celebrare anche in Italia la 6° giornata internazionale contro l’omofobia. Non è un ringraziamento formale, è il ringraziamento di chi sa che le sue parole sono le parole di cittadini e cittadine italiani senza diritti e senza voce. La prenda come una voce corale la mia, la mia è la voce di tutti. Di quelle tante donne e uomini che vogliono vivere alla luce del sole, senza paura.

Ma anche quella dei cittadini e le cittadine che hanno a cuore un paese migliore, migliore perché inclusivo, perché rispettoso di chi è diverso. Un paese in cui la piena cittadinanza di omosessuali e transessuali sia uno dei valori condivisi.

Oggi è un momento storico per noi omosessuali e transessuali italiani. Il NOSTRO Presidente della Repubblica celebra con NOI e per NOI la 6° Giornata Internazionale contro l’omofobia. Con le associazioni qui presenti che sono espressione della vasta comunità omosessuale e transessuale italiana.

Una giornata che la Comunità Europea ha voluto istituire con grande determinazione. E oggi per la prima volta anche il nostro paese celebra in modo solenne questa giornata nella quale voglio innanzitutto ricordare le tante vittime della violenza omofoba e trasnfobica.

Ogni giorno Presidente lei ci ricorda e ci insegna quanto sia importante per la vita di ogni cittadino ispirarsi all’Europa, rispettare e dare piena attuazione alla nostra Carta costituzionale.

Noi siamo uno dei paesi fondatori la Comunità Europea e con essa condividiamo quell’insieme di principi che ci fanno europei. Uno di questi è il principio della non discriminazione di omosessuali e transessuali scritto in tutti i trattati comunitari, ma soprattutto nel trattato di Lisbona: la Costituzione Europea. Ogni paese ha il dovere di rispettare questi principi e di promuovere azioni di carattere legislativo e culturale affinchè anche gay, lesbiche e trans possano vivere liberi dalla violenza, dal pregiudizio e nella piena uguaglianza e cittadinanza. Questo è il senso di questa giornata: ricordare, invitare, sollecitare.

Per me che sono parlamentare della Repubblica italiana rispettare la Costituzione significa seguirne i percorsi, quei meravigliosi cammini dentro i quali i nostri padri fondatori ci hanno guidato e ci guidano ancora. I cammini della costruzione quotidiana di ciò che ci tiene insieme, che ci fa comunità. Una comunità dove si rispettano doveri e diritti, per tutti, nessuno escluso.

Ma il nostro Parlamento un pezzo di strada lo deve ancora fare, quello dell’ approvazione di una legge contro l’omofobia e la transfobia. E’ un traguardo importante che ha visto nell’ottobre scorso una battuta d’arresto. Il traguardo dovrà arrivare, ce lo chiede il senso profondo di una democrazia che deve tendere alla eliminazione delle disuguaglianze. E’ dovere del Parlamento guidare i percorsi del rispetto, della lotta al pregiudizio e alla violenza.

Come è dovere del Parlamento seguire i suggerimenti della Corte Costituzionale. Nel mese scorso la nostra massima corte ha emesso una sentenza che afferma (secondo i dettami dell’articolo due della costituzione) che le coppie omosessuali devono avere un riconoscimento giuridico e che questo spetta al Parlamento.

Il Parlamento italiano ha molto lavoro da fare per rispondere non solo a ciò che l’Europa ormai ci chiede da decenni, ma per essere all’altezza di un compito: quello del rispetto della vita e della dignità dei suoi concittadini.

Questo compito a mio parere, avendo a che fare con principi fondanti, dovrà essere assolto tutti insieme, e oggi Presidente grazie a lei noi ci sentiamo per un giorno cittadini come tutti gli altri.

Che oggi sia l’inizio di una nuova stagione per i diritti