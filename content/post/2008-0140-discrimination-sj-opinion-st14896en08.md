---
title: '2008-0140-Discrimination-SJ-Opinion-ST14896.EN08'
date: Wed, 29 Oct 2008 12:03:29 +0000
draft: false
tags: [Senza categoria]
---

-

  

  

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 29 October 2008

Interinstitutional File:

2008/0140 (CNS)

14896/08

LIMITE

JUR    463

SOC    647

JAI      582

MI       410

  

  

  

  

  

OPINION OF THE LEGAL SERVICE[*](#_ftn1)

1)   to :

2)   Social Questions Working Party of 6 November 2008

3)   No. Cion prop. :

4)   11531/08 SOC 411 JAI 368 MI 246

5)   Subject :

6)   Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

7)   - EC Competence

8)   - Scope of Article 13 EC

9)   - Subsidiarity and proportionality

**Introduction**

1.       During the discussions of the Social Questions Working Group on 15 September 2008, the question has been raised as to whether the scope of the proposed draft Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation could be adequately encompassed within Article 13 (1) of the EC Treaty. This opinion aims to respond to that question.

  

**I.       Community competence and legal basis**

2.       According to settled case-law, the choice of the legal basis for a Community measure must be based on objective factors which are amenable to judicial review, including in particular the aim and content of the measure[\[1\]](#_ftn2). It is therefore necessary to examine these in more detail.

**II.      Aim and content of the proposal**

3.       Pursuant to Article 1 of the proposal, the Directive's purpose is to lay down a framework for combating discrimination on the grounds of religion or belief, disability, age, or sexual orientation, with a view to putting into effect in the Member States the principle of equal treatment other than in the field of employment and occupation.

4.       According to the Commission, the proposal aims to establish a framework for the prohibition of discrimination on the above-mentioned grounds, thus providing for a uniform minimum level of protection within the European Union for people who may suffer discrimination. It aims, furthermore, to complete the existing EC legal framework under which the prohibition of discrimination on grounds of religion or belief, disability, age or sexual orientation currently only applies to employment, occupation and vocational training[\[2\]](#_ftn3). At present, legal protection from discrimination outside employment only covers sex and racial and ethnic origin[\[3\]](#_ftn4). Moreover, Directive 2004/113/EC[\[4\]](#_ftn5) implements the principle of equal treatment between men and women in the access to and supply of goods and services.

  

5.       Article 3 of the proposal establishes the scope of the Directive in the following terms:

Article 3  
Scope

_1\. Within the limits of the powers conferred upon the Community, the prohibition of discrimination shall apply to all persons, as regards both the public and private sectors, including public bodies, in relation to:_

_(a) Social protection, including social security and healthcare;_

_(b) Social advantages;_

_(c) Education;_

_(d) Access to and supply of goods and other services which are available to the public, including housing._ \[…\]

6.       Article 4 of the proposal provides that, in relation to persons with disabilities, in order to guarantee compliance with the principle of equal treatment, effective non-discriminatory accessibility shall be provided by anticipation, to the extent that such measures do not impose a disproportionate burden, nor require fundamental alteration of the social protection, social advantages, health care, education, or goods and services in question or require the provision of alternatives thereto. Further to this obligation, where needed in a particular case, reasonable accommodation shall be provided unless this would impose a disproportionate burden.

  

**III.    THE SCOPE OF ARTICLE 13 EC**

7.       Article 13 (1) EC is enshrined in part one of the Treaty, entitled Principles. It reads as follows:

_Article 13_

_1\. Without prejudice to the other provisions of this Treaty and within the limits of the powers conferred by it upon the Community, the Council, acting unanimously on a proposal from the Commission and after consulting the European Parliament, may take appropriate action to combat discrimination based on sex, racial or ethnic origin, religion or belief, disability, age or sexual orientation._ \[…\]

8.       The Council Legal Service has had the opportunity in the past to consider certain questions relating to the scope of Article 13 EC. In particular, in its Opinion of 30 May 2000, the Legal Service analysed the scope _ratione personae_ of Article 13 EC, by addressing the specific question of the applicability to third-country nationals of provisions adopted in accordance with Article 13[\[5\]](#_ftn6). Furthermore, the Legal Service addressed the subject matter and scope of a proposal for a Council Directive implementing the principle of equal treatment between women and men in the access to and supply of goods and services[\[6\]](#_ftn7) in an Opinion of 7 April 2004[\[7\]](#_ftn8).

  

9.       In Opinion 8301/04, the Council Legal Service considered that "it can be inferred from the manner in which it is worded, that Article 13 allows the Institutions to act within the areas in which the Community already possesses competence by virtue of other Treaty provisions [\[8\]](#_ftn9). Article 13 does not allow therefore the Institutions to legislate on non-discrimination in areas where Member States remain competent [\[9\]](#_ftn10). This follows from Article 5 of the Treaty, which allows the Community to act only where powers have been conferred upon it." These points should be further developed.

10.     It results from its wording that Article 13(1) is not directly applicable but simply confers a faculty upon the Council to act by adopting appropriate measures to combat discrimination. Whereas, on the one hand, the Council has been given a wide margin of appreciation with respect to the type of measures that it considers appropriate to combat discrimination, on the other hand, a double condition has been enshrined in this provision that seems to reflect the wish to limit the scope of Art 13 as an enabling provision. Indeed the Council is empowered to act _within the limits of the powers conferred by it upon the Community_ and _without prejudice to the other provisions of this Treaty_. These conditions will be analysed in turn:

**Without prejudice to the other provisions of this Treaty**

11.     Article 13 EC is an enabling provision of a general nature which co-exists with other provisions of the Treaty to the extent that it does not affect their application. As a transversal provision, Article 13 may not limit the effects or scope of other Treaty provisions. For example, it cannot be used in a manner which restricts the effects of the Treaty provisions concerning the fundamental freedoms of circulation within the European Community.

  

12.     In accordance with this proviso, the relationship between Article 13 EC and other legal bases of the Treaty has to be understood in the sense that, depending on the breadth of the action that is foreseen, recourse is possible both to Article 13 EC or to other legal bases in the treaty which allow for the adoption of anti discrimination measures. Accordingly, Article 13 is not to be used where other, more specific, legal bases exist in the Treaty, which provide the Community with sufficient authority to act. So, for example, in the case of legislation which exclusively concerns equal pay for male and female workers, Article 141 (3) EC would be a sufficient and appropriate legal basis. Conversely, to the extent that a measure covers multiple grounds of discrimination, including, among others, sex discrimination in the workplace, then Article 13 EC is the appropriate legal basis.

**Within the limits of the powers conferred by it upon the Community**

13.     A second requirement set out in Article 13 is that the Community action should be taken within the limits of the powers conferred by the Treaty upon the Community. This means that the scope of Article 13 is limited by the boundaries of competences conferred upon the Community elsewhere in the Treaty.

14.     Therefore, under Article 13(1) EC, the Council can take measures to combat discrimination at Community level only to the extent that another Article in the Treaty confers upon it the necessary legislative power, and under the conditions foreseen in that other Article. Accordingly, Article 13(1) is a general provision which sets out the objectives to be attained, but which makes Community action dependent upon the existence, elsewhere in the Treaty, of the means to attain that objective.

  

15.     This implies that the faculty of the Council to act is limited to the extent that it cannot disrespect specific Treaty prohibitions against harmonisation. It follows, in particular, that a measure taken under Article 13 cannot interfere with Member State's competence for the content of teaching and the organisation of education systems and their cultural and linguistic diversity, nor with the organisation and delivery of health services and medical, nor the organisation and financing of national systems of social security. To take the case of education, Community measures cannot interfere with national rules concerning "special needs education", nor with the content and form of, for example, religious or civic education programmes.

16.     In this context, a further element may be added. The term "within the limits of the power conferred upon the Community by the Treaty" have to be interpreted in the light of the jurisprudence of the Court. In its judgment in Case C-144/04, the Court held that the principle of prohibition of discrimination on grounds of religion or belief, disability, age or sexual orientation finds its source in the constitutional traditions common to the Member States and in various international instruments[\[10\]](#_ftn11). The Court therefore concluded that the principle of non-discrimination on grounds of age[\[11\]](#_ftn12) has to be considered as a general principle of Community law[\[12\]](#_ftn13).

17.     If that is the case, then the general principle of Community law with respect to non discrimination on the grounds of age also has to be considered as being covered by the wording "powers conferred by the Treaty upon the Community" in Article 13.

  

18.     It follows from this conclusion that the Community has the power to establish under Article 13(1) a general framework setting out the conditions, limits and modalities for the application of that general principle of Community law, without, however, encroaching on the application of other Articles of the Treaty.

**IV.    COMMUNITY COMPETENCE WITH REGARD TO THE SCOPE OF THE PROPOSED DIRECTIVE**

19.     Against this background, it is necessary to analyse, in turn, the scope of the Directive as set out in draft Article 3 of the proposal, in order to ascertain whether the proposed action may be covered by Community competence.

**1\. Access to and supply of goods and services**

20.     The scope of the draft Directive concerns mainly the _access to and supply of goods and services_. Accordingly, the provision of and access to some services mentioned in the draft Directive such as housing, transport, healthcare, private education, financial services and insurance are covered under this paragraph.

21.     In its Opinion on the subject matter and scope of Council Directive implementing the principle of equal treatment between women and men in the access to and supply of goods and services[\[13\]](#_ftn14), also based on Article 13, the Council Legal Service explicitly addressed the question of the goods and services covered by the said Directive [\[14\]](#_ftn15).

  

22.     The definition of the term 'services' within the meaning of the Treaty, and the particular areas covered, were discussed in points 9 to 22 of the said Opinion. In particular, it was explained that according to the Court of Justice, under the first paragraph of Article 50 EC, services are considered to be services within the meaning of the Treaty where they are normally provided for remuneration, and that the essential characteristic of remuneration lies in the fact that it constitutes consideration for the service in question.[\[15\]](#_ftn16) [\[16\]](#_ftn17)

23.     In accordance with the criteria provided by Article 50 EC and the interpretation given by the Court, private education, which is provided against consideration, falls within the concept of "services" for the purposes of Community law[\[17\]](#_ftn18). The same does not apply to public education. The Court of Justice considered that public education fell under a category of activities performed by the State in the fulfilment of its social, cultural, educational and legal obligations, which lacked economic motivation. As a result, the Court has excluded public education from the category of services[\[18\]](#_ftn19).

  

24.     Similarly, health care is regarded as a service for the purposes of Community law[\[19\]](#_ftn20). According to the Court in _Kohll_, Case C-158/96, and _Webb,_ Case 279/80, the  application of national social security rules may also fall under the category of services. In fact, the Court considered that it is settled case law that the special nature of certain services does not remove them from the ambit of the fundamental principle of freedom of movement[\[20\]](#_ftn21), so that the fact that the national rules at issue in the main proceedings were social security rules could not exclude application of Articles 59 and 60 of the Treaty[\[21\]](#_ftn22).

25.     The Treaty provisions on services are found in Chapter 3 therein. Article 49, read in conjunction with Article 52 and Article 47, provides the Community with powers to ensure that restrictions on the freedom to provide services within the Community are abolished. These include the power to adopt directives for the harmonisation of national laws and regulations to the extent necessary to ensure their free circulation within the Community.

26.     With regard to goods, Article 23 (2) of the Treaty describes them as "products originating in Member States and products coming from third countries which are in free circulation in Member States". The Court of Justice has defined "goods" as "products which can be valued in money and which are capable, as such, of forming the subject of commercial transactions"[\[22\]](#_ftn23) [\[23\]](#_ftn24) [\[24\]](#_ftn25).  The Community powers in this context are broad and include the possibility of adopting harmonisation measures which are deemed appropriate to ensure their free circulation within the Community.

  

27.     Community competence to address the cross-border circulation of goods is addressed by the Treaty in Title I, read in conjunction with Article 95 EC. Community competence to address the cross-border circulation of services is set out in Title III, Chapter 3 of the Treaty. In both instances, Community powers are broad and include the possibility of adopting harmonisation measures as deemed appropriate to ensure the free circulation of goods and services within the Community.

28.     It is necessary to address now the other areas in which the Commission proposes to implement the principle of non-discrimination.

**2\. Social protection, social security, and social advantages**

29.     The Community enjoys competence to address non-discrimination with regard to employment matters. Article 141 provides a specific legal basis for the Community to ensure that the principle of equal pay for male and female workers is applied. Furthermore, in accordance with Article 137 TEC the Community is entitled to adopt, by means of directives, minimum requirements for gradual implementation concerning, _inter alia_, social security and social protection of workers (Article 137 (c)); the integration of persons excluded from the labour market (Art 137 (h)) and equality between men and women with regard to labour market opportunities and treatment at work( Art 137 (i)). Even if only by way of minimum harmonisation, Community action is possible in the context of employment with regard to the fields covered by the draft Directive on social protection and social security. In this context, enactment of a Community measure is not dependent on the requirement of the existence of a cross border element.

30.     To the extent that minimum harmonisation is possible in the field of employment for the integration of persons excluded from the labour market, it is primarily a matter for the Council's discretion to consider whether, in the context of a measure that it adopts under Article 13, it is appropriate to include the areas of social protection, social security, and social advantages.

  

31.     Against this background, it is useful to check whether, in the current state of Community law, Community action is also possible outside the field of employment.

**2.1. Social Security**

32.     With regard to the inclusion of Social Security  in the scope of a Directive which applies outside the field of employment, and considering the impossibility to address in detail the case law in this field, the following points can be made:

33.     Firstly, Regulation (EC) No 883/2004 [\[25\]](#_ftn26), which is to replace Regulation (EC) No 1408/71[\[26\]](#_ftn27), applies to all nationals of a Member State: workers, self employed, students and inactive persons, including persons who do not work temporarily or who have never worked during their lifetime[\[27\]](#_ftn28). This scope  goes beyond employment and occupation.

34.     Secondly, it should be recalled that both these regulations were construed as coordination instruments of the national systems of social security. The competence for the organisation and financing of social security systems remains with the Member States. Community legislation in this context has been underpinned by a fundamental coordination principle, namely, the principle of non-discrimination on the grounds of nationality[\[28\]](#_ftn29) which, according to the Court, prohibits not only overt discrimination based on the nationality of the

  

beneficiaries of social security schemes, but also all covert forms of discrimination which, through the application of other distinguishing criteria, lead in fact to the same result[\[29\]](#_ftn30). In this context, it is settled case law that Article 42 of the Treaty and Regulation No 1408/71 provide only for the aggregation of insurance periods completed in different Member States and do not regulate the conditions under which those insurance periods are constituted. The conditions governing the right or obligation to become a member of a social security scheme are therefore a matter to be determined by the legislation of each Member State, provided that there is no discrimination in that regard between the nationals of the host Member State and those of other Member States[\[30\]](#_ftn31).

35.     Lastly, as was noted in paragraph 24 of this Opinion, according to the Court in _Kohll_, Case C-158/96, and _Webb,_ Case 279/80, the  application of national social security rules may also fall under the category of services. In fact, the Court considered that it is settled case law that the special nature of certain services does not remove them from the ambit of the fundamental principle of freedom of movement[\[31\]](#_ftn32), so that the fact that the national rules at issue in the main proceedings were social security rules could not exclude the application of Articles 59 and 60 of the Treaty[\[32\]](#_ftn33). Depending on the concrete situation addressed in each case, social security benefits may indeed be regarded as services by the Court. The rules concerning services would accordingly apply to them.

36.     Against this background, it can be concluded that non-discriminatory access to social security benefits outside the field of employment can, in the context of cross border situations, be regarded as a domain which the Council could decide to cover in the Directive, provided that this inclusion does not interfere with Member States' responsibility for determining the conditions governing the rights or obligations for persons to become members of a social security scheme.

  

**2.2. Social advantages**

37.     Some delegations asked about the meaning of "social advantages". On the basis of Articles 12, 18, 40, 44 and 52 EC, the Community has acted to establish the legal framework on the right of citizens of the Union and their family members to move and reside freely within the territory of the Member States. It has done so through the adoption of Directive 2004/38/EC[\[33\]](#_ftn34), which builds upon the _acquis_ of Council Regulation (EEC) No 1612/68 of 15 October 1968 on freedom of movement for workers within the Community[****\[34\]****](#_ftn35). Article 24 of Directive 2004/38/EC provides for equal treatment for all Union citizens residing lawfully in the territory of a Member State. With regard to social advantages, there is long standing case law on the meaning of this concept which was developed on the basis of Article 7 (2) of Regulation 1612/68.

38.     According to the Court, the concept of "social advantages" covers all rights or benefits which "whether or not linked to a contract of employment, are generally granted to national workers primarily because of their objective status as workers or by virtue of the mere fact of their residence on the national territory and whose extension to workers who are nationals of other Member States therefore seems likely to facilitate the mobility of such workers within the Community"[\[35\]](#_ftn36). Accordingly, student grants[\[36\]](#_ftn37) and non discriminatory admission to education for the children of migrant workers[\[37\]](#_ftn38) have been recognised by the Court as social advantages.

  

Unemployment benefits paid to young people who have just completed their studies and are the dependents of workers were also regarded as social advantages[\[38\]](#_ftn39). The Court also ruled that certain social security and social assistance benefits, such as a social benefit providing a general guarantee of a minimum subsistence allowance [\[39\]](#_ftn40), childbirth loans[\[40\]](#_ftn41), family benefits[\[41\]](#_ftn42) and unemployment benefits[\[42\]](#_ftn43) could be considered as social advantages[\[43\]](#_ftn44). The right to live together in the host State with an unmarried partner[\[44\]](#_ftn45) and the right to use one's own language in Court[\[45\]](#_ftn46) were also considered as social advantages.

39.     In Case _Grzelczyk_, the Court concluded that Community citizens who are lawfully resident in the territory of another Member State can rely on Article 12 EC in order to claim social assistance benefits on the same conditions as nationals of the host Member State[\[46\]](#_ftn47).

40.     In view of the scope _ratione personae_ of Directive 2004/38/EC and in accordance with the case law, non discriminatory access to social advantages for nationals of Member States, albeit under certain conditions, namely that they reside lawfully in the territory of the host State, can be covered by Articles 12, 18, 40, 44 and 52 EC, which concern the free circulation of persons within the Community. The right to equality of treatment in relation to social advantages does not depend on the existence of a contract of employment. Against this background, the Council may, if it deems it appropriate, include access to social advantages in the scope of the draft directive, it being understood that the actual setting up and definition of the contents and modalities of any such social advantages remains within the competence of Member States.

  

41.     Further clarification is required from the Commission as to what it considers to be **social protection**. Does the Commission assimilate this concept to social assistance? Social assistance is excluded from the scope of the social security regulations discussed above. However, it follows from the Court's case law that social assistance benefits may, under certain conditions, also be covered by the Treaty provisions concerning the right to equal treatment, i.e. Article 12 EC and citizenship in Article 18 EC[\[47\]](#_ftn48).

**3\. Health Care**

42.     It follows from Article 152 TEC that Community competence in public health does not entitle the institutions to intervene in the organisation and delivery of health services and medical care. The powers conferred upon the Community focus on the objective of ensuring the protection and improvement of human health. Harmonisation is specifically excluded in the context of measures designed to protect and improve human health (Article 152 paragraph, 4 c). The organisation and delivery of health services therefore remains within the remit of Member States competence.

43.     This does not mean, however, that the provision of, and access to, health care falls outside Community competence. Firstly, as was noted in paragraph 24 of this opinion, healthcare is a service for the purposes of Article 50 of the Treaty[\[48\]](#_ftn49). This means that the Treaty rules concerning the free movement of services apply in this context[\[49\]](#_ftn50). In this respect it should be

  

noted that it is settled case law that patients enjoy some rights under the common market rules and the general provisions on freedom of movement and equal treatment. For example in Case C- 286/82, _Luisi and Carbone_, the Court took the view that Article 49 includes the right to move to other Member States in order to receive services. In this context, the Court held that persons receiving medical treatment must be regarded as recipients of services for the purposes of Community law. Accordingly, for persons receiving medical care abroad, non-discriminatory access to healthcare can also be based on Article 13 of the Treaty.

44.     Secondly, there is already Community legislation that helps to facilitate cross-border healthcare[\[50\]](#_ftn51). Under the regulations on the coordination of social security systems, persons for whom a medical treatment becomes necessary during a stay in the territory of another Member State are entitled to the same benefits as patients insured in the host Member State. The regulations also provide for patients to be able to seek healthcare in another EC country, subject to prior authorisation from their own system. Such authorisation must, in particular, be granted if the care cannot be provided within a medically justifiable period of time in the country of origin[\[51\]](#_ftn52).

45.     It follows that there is Community competence with regard to cross border access to health care on the basis of the Treaty provisions concerning the free circulation of services, it being understood that the organisation and delivery of health services remain within the remit of Member States competence.

  

**Education**

46.     Community competence in the field of education is set out in Article 149 of the Treaty[\[52\]](#_ftn53). Article 149 EC establishes that the Community "shall contribute to the development of quality education by encouraging cooperation between Member States and, if necessary, by supporting and supplementing their action". The Community is to do so while "fully respecting the responsibility of the Member States for the content of teaching and the organisation of education systems and their cultural and linguistic diversity". For this purpose, Community action may under Article 149 (2) EC be aimed at, _inter alia,_ "encouraging mobility of students and teachers"; "developing exchanges of information and experience on issues common to the education systems of the Member States" and "encouraging the development of youth exchanges and of exchanges of socioeducational instructors". Paragraph 4 of Article 149 EC establishes a significant limit to Community action to the extent that it enables the Council to adopt incentive measures _excluding any harmonisation_ of the laws and regulations of the Member States[\[53\]](#_ftn54).

47.     It follows that, Article 149 EC provides powers for the Community to act on the basis of Article 13 under a double condition: (a) that the measure envisaged under Article 13 is not a harmonisation measure and (b) that it fully respects the responsibility of the Member States for the content of teaching and the organisation of education systems and their cultural and linguistic diversity.

  

48.     To the extent that these two explicit limits imposed by Article 149 are strictly respected, the Council Legal Service is of the opinion that the right of access to education without discrimination can be affirmed in the context of a Directive adopted on the basis of Article 13 EC. In particular, this means that a Community measure adopted pursuant to Article 13 EC may go beyond simple encouragement but may not, in any event, interfere with national rules concerning the opportunity for, organisation and contents of education, notably in the context of, "special needs education" or of religious or civic education programmes. At Community level, a directive to be adopted by the Council can only provide that, where such programmes are offered at national level, access thereto should be offered on a non-discriminatory basis.

49.     In this context, the Council Legal Service takes the view that the proposed Directive limits itself to providing that discrimination with regard to the grounds referred in the Directive shall be prohibited. Such a prohibition cannot in itself be regarded as a harmonisation measure as it does not have the effect of approximating the legislations of Member States but simply of making the principle of non discrimination applicable with regard to the areas that it covers.

**V.      EC COMPETENCE TO ADOPT THE ENVISAGED MEASURES ON THE BASIS OF ARTICLE 13 EC**

50.     In its impact assessment[\[54\]](#_ftn55), the Commission states that the policy response to combat discrimination needs to meet certain overall objectives:

1.           To increase protection from discrimination on grounds of age, disability, sexual orientation and religion or belief;

  

2.           To ensure legal certainty for economic operators and potential victims across the Member States in terms of the extent of protection against discrimination outside the labour market on grounds of age, disability, sexual orientation and religion or belief;

3.           To enhance social inclusion and promote the full participation of all groups in society and the economy.

51.     The first objective coincides with the objective set out in Article 13 EC. The introduction of Article 13 in the EC Treaty by the Treaty of Amsterdam had precisely the effect of authorising the Community to take measures in order to combat discrimination at Community level. For this purpose, this Treaty provision added a number of grounds on the basis of which discrimination may be combated, in addition to the existing grounds relating to nationality and sex, and in the area of employment. Accordingly, Article 13 EC left the Council with a wide margin of discretion to determine the measures which it considers appropriate to achieve the objective that it sets out, on the condition, however, that these measures remain within the boundaries of Community competence.

52.     The second objective corresponds to the scope of Community powers to act in order to ensure the free circulation of goods, services and persons within the Community. In this context, it follows from the preceding chapter that, with the exception of non discrimination in matters of employment (Article 137 EC), where no cross-border element is necessary for the enactment of Community legislation, Community competence exists in all the areas analysed in respect of situations which concern intra-Community circulation of goods, services and persons. In this context, it is ultimately a matter for the Council to ascertain whether

  

Community intervention is necessary and appropriate in order to allow citizens and businesses to exercise their rights pursuant to the Internal Market. In doing so, the Council can, among other elements, take account of the data provided by the Commission in its Impact Assessment[\[55\]](#_ftn56). The Commission points out in its impact assessment that within the current legislative framework, providers of goods and services are faced with differing national provisions on discrimination when operating across borders, which influence their capacity and willingness to develop markets for services in other countries. Furthermore, the existence of differing levels of protection from discrimination influence individuals' decisions  to make use of their rights of free movement, for instance to travel, work or study in another Member State. As, according to the Commission, persons with disabilities account for 16% of the Community population, a significant number of persons may indeed benefit from some additional coherence in the current legal framework. To the extent that the Council deems it necessary to take action to address situations at cross-border level, it may conclude that it is also necessary that the principle of non discrimination applies as well to internal situations because of the inextricable connection between the two situations. This approach would also prevent situations in which reverse discrimination would occur. This is, evidently, a question of political judgement to be taken by the Community legislator.

53.     The third objective is in conformity with the objective set out in Article 137 EC which provides powers for the Community to take measures of minimum harmonisation in order to, _inter alia_, promote the integration of persons excluded from the labour market \[Art. 137 (1) h) and (2) b)\]. It follows from the Commission's outline of the costs and impact on individuals and society[\[56\]](#_ftn57), that a lack of action to address discrimination ultimately results in social exclusion, and particularly in exclusion from the labour market for the persons concerned.

  

54.     Against this background, it is primarily a matter for the Council' s discretion to determine whether, in the context of a measure that it adopts under Article 13, action is necessary, and to decide accordingly on the type and breadth of action that it deems appropriate. The Council may conclude that action is needed to combat discrimination regarding all or only some of the grounds covered by Article 13. Similarly, the Council may want to opt for a broader or more limited scope for the Directive. When considering these options, however, the faculty of the Council to act is limited to the extent that it cannot disrespect the specific Treaty prohibition with respect to the exclusion of measures of harmonisation. In particular, a measure taken under Article 13 cannot interfere with Member State's competence for the content of teaching and the organisation of education systems and their cultural and linguistic diversity, nor with the organisation and delivery of health services and medical care, nor with the organisation and financing of national systems of social security.

55.     When assessing whether or not action is appropriate at Community level, the Council must take into account the principle of subsidiarity and proportionality enshrined in Article 5 of the EC Treaty.

**VI.      PRINCIPLE OF SUBSIDIARITY**

56.     Pursuant to the second sub-paragraph of Article 5 of the EC Treaty, in areas which do not fall within its exclusive competence, the Community is to take action only if and in so far as the objectives of the proposed action cannot be sufficiently achieved by the Member States and can therefore, by reason of the scale or effects of the proposed action, be better achieved by the Community.

  

57.     It follows from the use in this Article of the words "insofar as", "sufficiently" and "better", which presuppose a value judgment, that subsidiarity is essentially a political and subjective principle; to apply it, the institutions must, throughout the examination of a proposal for a legislative act, exercise their powers of discretion, weighing up the advantages and disadvantages.

58.     In order to give guidance and clarity to that substantive examination, Protocol no 30 annexed to the Treaty provides the following guidelines:

"_the issue under consideration has transnational aspects which cannot be satisfactorily regulated by action by Member States;"_

59.     Without prejudging the result of the political analysis to be made, the following aspects, among others, could be examined under this guideline:

-               the effects of differing levels of protection from discrimination in the various Member States in the mobility of persons at EC level.

-               the impact of the absence of a level playing field within the Community for the access to and provision of services and goods within the Community.

-               the need to ensure legal certainty for economic operators and potential victims across the Member States

-               the cost/benefit ratio of addressing these questions at transnational level.

"_\- actions by Member States alone or lack of Community action would conflict with the requirements of the Treaty (such as the need to correct distortion of competition or avoid disguised restrictions on trade or strengthen economic and social cohesion) or would otherwise significantly damage Member States' interests;"_

60.     In this context, and without prejudice to the final conclusion of the debate, the substantive examination could refer, _inter alia_, to issues such as:

−               the implication of the conclusion by the European Community of the UN Convention on Rights of Persons with Disabilities, adopted on 13 December 2006 at the United Nations.

−               the extent to which the adoption of a Community Directive covering discrimination on the ground of disability could facilitate implementation of the UN Convention by Member States and provide a coherent approach within the European Community.

" _\- action at Community level would produce clear benefits by reason of its scale or effects compared with action at the level of the Member States"._

−               the preamble of the Directive would have to show that, and why, the EC legislator is convinced that this is the case.

61.     The above issues as well as others mentioned by the Commission in the Explanatory Memorandum and the Impact assessment could be viewed in the light of this guideline.

62.     Without prejudice to the final conclusion of the debate, the Legal Service is of the opinion that, in any event, the justification in relation to the principle of subsidiarity needs to be reinforced in line with the provisions of Protocol 30, namely point 4 therein.

  

**VII.     PRINCIPLE OF PROPORTIONALITY**

63.     The principle of proportionality, as enshrined in the third subparagraph of Article 5 of the EC Treaty, requires that any action by the Community shall not go beyond what is necessary to achieve its objective. In particular, the question of less restrictive alternatives has to be considered. Proportionality refers to both the extent and intensity of Community action and aims to avoid excessive Community legislation. When there is a choice between several appropriate measures _"recourse must be had to the least onerous, and the disadvantages caused must not be disproportionate to the aims pursued"_[\[57\]](#_ftn58).

64.     It emerges from the impact assessment that the Commission has assessed five alternative policy options, varying from no Community action to a multi-ground directive. According to the findings of the Commission, mere guidance would not offer legal certainty and would therefore not be as effective as the adoption of legally binding measures. The Commission has chosen the instrument of a multi-ground Directive.

65.     In this respect, the Court of Justice recognises that the legislator has wide discretion, and the Court consequently limits its review to verifying that the measure in question is not vitiated by any manifest error and that the authority concerned has not manifestly exceeded the limits of its discretion[\[58\]](#_ftn59). The Legal Service therefore concludes that the Council can in principle conclude, without violating the proportionality principle, that the measures are necessary to achieve the objective of improving the current legal framework for anti discrimination.

  

**VIII. ISSUES CONCERNING LEGAL CERTAINTY**

66.     Notwithstanding the conclusions that the Council may draw with regard to the principles of subsidiary and proportionality, the Council Legal Service would like to point out that there is a need to ascertain, in the context of the discussions to be held on this proposal, the extent to which legal clarity is achieved with the current drafting. In order to adopt a text that achieves the objective of legal certainty, further clarification will be needed, _inter alia_, in relation to:

-        some of the concepts used such as _reasonable accommodation_ and _fundamental alteration._ In particular the term _reasonable accommodation_ should be defined, as it constitutes a central concept of the Directive which may have significant implications for the persons and businesses concerned;

-        the type and extent of obligations that will apply in concrete to manufacturers of goods, and in particular the measures that would be required from them in order to guarantee accessibility to the products that they place on the market. The reference to access to goods: does it mean that the goods have to be produced in a way that will make them user-friendly for all persons concerned or simply that goods have to be physically accessible for the persons in question? In practice, does it mean that the goods should be manufactured in a way that permits their use by persons with a handicap or simply that they are placed in a public space (such as in a shop or on the internet) which ensures access for disabled persons?

-        the type and extent of obligations that will apply to service providers, in particular in the transport sector. In this context, due to the horizontal implications of this Directive, the relevant Council formations should be consulted with regard to the impact of this proposal in their sectors;

  

-        the appropriateness of the " one size fits all" approach which results from the adoption of a multi-ground directive whose operative part in fact only relates to the ground of disability. In particular, to what extent do the detailed provisions concerning disability apply in the case, for example, of discrimination with regard to religion or belief, age or sexual  orientation? The potential implications of the approach taken of a multi-ground directive should be addressed in order to ensure that unintended effects will not prompt a stream of legal disputes in Member States.

**IN CONCLUSION**

67.     The Council Legal Service is of the opinion that under Article 13(1) EC the Council can take measures to combat discrimination at Community level to the extent that another Article in the Treaty confers upon it the necessary legislative power, and under the conditions foreseen in that other Article.

68.     The competence for the organisation, delivery and financing of social security and healthcare systems remains within the remit of Member States competence. The setting-up and definition of social advantages granted at national level remains within the competence of the Member States. Similarly the responsibility for the content of teaching and the organisation of education systems is a matter which falls within the competence of the Member States.

  

69.     With the exception of non-discrimination measures in matters of employment pursuant to Article 137 EC, where no cross-border element is necessary for the enactment of Community legislation, Community competence exists in the policy areas examined, to the extent necessary to ensure the free circulation of persons, goods, and services, and establishment within the Community. Should the Council deem it necessary to take action to address situations at cross-border level, it may conclude that it is also necessary that the principle of non-discrimination applies as well to internal situations because of the inextricable connection between the two.

70.     The Council Legal Service concludes therefore that, within the limits described in paragraphs 68 and 69 above, access with regard to the policy areas referred in Article 3 of the proposal is a matter which can, if the Council so wishes, be covered in the context of a Directive adopted on the basis of Article 13 EC.

71.     Against this background, it is a matter for the Council' s discretion to determine whether, in the context of a measure that it adopts under Article 13, action is needed in order to tackle discrimination that may amount to a restriction to the freedom of movement of persons, goods and services within the Community. When assessing these options, however, the faculty of the Council to act is limited to the extent that it cannot disrespect specific Treaty prohibitions of measures of harmonisation. In particular, a measure taken under Article 13 cannot interfere with the Member States' competence for the content of teaching and the organisation of education systems and their cultural and linguistic diversity, nor with the organisation and delivery of health services and medical care, nor with the organisation and financing of national systems of social security.

  

72.     Having regard to the points made above, for reasons of legal certainty, the following point should be clarified in the proposal:

Article 3 - Scope

_1\. Within the limits of the powers conferred upon the Community, the prohibition of discrimination shall apply to all persons, as regards both the public and private sectors, including public bodies, in relation to **access to**:_

_(a) Social protection, including social security and healthcare;_

_(b) Social advantages;_

_(c) Education;_

_(d) and the supply of goods and other services which are available to the public, including housing._ \[…\]

**73\.** Likewise, the Council Legal Service is of the opinion that the text of the proposal has to   be clarified in the light of the comments made in paragraph 66 of this Opinion.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[*](#_ftnref1) **This document contains legal advice protected under Article 4(2) of Regulation (EC) No 1049/2001 of the European Parliament and of the Council of 30 May 2001 regarding public access to European Parliament, Council and Commission documents, and not released by the Council of the European Union to the public. The Council reserves all its rights in law as regards any unauthorised publication.**

[\[1\]](#_ftnref2) See for example Case C-440/05, _Commission v. Council_, \[2007\] ECR I-9097, paragraph 61.

[\[2\]](#_ftnref3) Directive 2000/78/EC of 27 November 2000 establishing a general framework for equal treatment in employment and occupation, OJ L 303 of 2.12.2000, p. 16.

[\[3\]](#_ftnref4) Directive 2000/43/EC already implements the principle of equal treatment between persons irrespective of racial or ethnic origin in employment and vocational training as well as in social protection, social advantages, education and access to goods and services. Directive 2000/43/EC of 29 June 2000, implementing the principle of equal treatment between persons irrespective of racial or ethnic origin, OJ L 180 of 19.7.2000, p.22

[\[4\]](#_ftnref5) Directive 2004/113/EC of 13 December 2004, implementing the principle of equal treatment between men and women in the access to and supply of goods and services, OJ L 373 of 21.12.2004, p.37.

[\[5\]](#_ftnref6) Opinion of the Legal Service of 30 May 2000, Doc. 9035/00, JUR 180 SOC 208.

[\[6\]](#_ftnref7) Directive 2004/113/EC of 13 December 2004, implementing the principle of equal treatment between men and women in the access to and supply of goods and services, OJ L 373 of 21.12.2004, p.37.

[\[7\]](#_ftnref8) Opinion of the Legal Service of 7 April 2004, contained in Document 8301/04 JUR 184 SOC 171.

[\[8\]](#_ftnref9) A parallel approach was taken by the Court in Case C-152/82, _Forcheri v. Belgian State_, \[1983\] ECR 2323, paragraph13. The Court made the distinction between the areas in which the Treaty had conferred competence upon the Community Institutions and where the Institutions were therefore free to legislate and others, such as educational and vocational training policy, where only the opportunity for such measures fell within the scope of the Treaty.

[\[9\]](#_ftnref10) This is for example the case for the establishment of national curricula, the organisation of health care services, or religious orders.

[\[10\]](#_ftnref11) Case C-144/04, _Mangold v Helm_ \[2005\] ECR I-9981, paragraph 74.

[\[11\]](#_ftnref12) Ibid. paragraph 75.

[\[12\]](#_ftnref13) It follows from the Court's arguments in paragraph 74 of that judgment, that the same conclusion is to be drawn for the other reasons of discrimination referred to in paragraph 74.

[\[13\]](#_ftnref14) Legal Opinion of 7 April 2004, contained in Document 8301/04 JUR 184 SOC 171.

[\[14\]](#_ftnref15) Directive 2004/113/EC of 13 December 2004, implementing the principle of equal treatment between men and women in the access to and supply of goods and services, OJ L 373 of 21.12.2004, p.37.

[\[15\]](#_ftnref16) Accordingly, the Court has, on the one hand, specified that recreational services do not lose their economic nature because of their recreational or sporting nature Joined Cases C-51/96 and C-191/97 _Deliège_ \[2000\] ECR-I-2549, paragraph 55, nor due to the special nature of certain services, which may include application of national social security rules Case C-158/96, _Raymond Kohll v. Union des caisses de maladie_ \[1998\] ECR I-1931, paragraph 20 and 21 and Case 279/80, _Webb_ \[1981\] ECR 3305, paragraph 10. On the other hand, the Court has clarified that Article 50 of the Treaty does not require that the service be paid for by those for whom it is performed Case 352/85, _Bond van Adverteerders and Others_, \[1988\] ECR 2085, paragraph 16, and Joined Cases C-51/96 and C-191/97 _Deliège_ \[2000\] ECR I-2549, paragraph 56.

[\[16\]](#_ftnref17) Under this approach are to be considered services in the sense of Article 50, among others, activities such as management consultancy, certification and testing, maintenance, facilities management and security, advertising services, recruitment services, services provided by commercial agents, legal or tax consultancy, property services, such as those provided by estate agencies, construction services, architectural services, distributive trades, organisation of trade fairs and exhibitions, car-hire, security services, tourist services, including travel agencies and tourist guides, audiovisual services, sports centres and amusement parks, leisure services, personal domestic services, such as assistance for old people.

[\[17\]](#_ftnref18) Case C-76/05, _Schwarz and Gootjes-Schwarz_, \[2007\] ECR I-6849, paragraphs 39 to 40 and Case C-318/05, _Commission v Germany_, \[2007\] ECR I-6957, paragraphs 67 to 72.

[\[18\]](#_ftnref19) This was clearly stated in Case 263/86, _Belgian State v René Humbel and Marie-Thérèse Edel_, \[1988\] ECR 5365  , paragraphs 17 to 20.

[\[19\]](#_ftnref20) According to the Court, it is settled case-law that medical activities fall within the scope of Article 50 of the Treaty, there being no need to distinguish in that regard between care provided in a hospital environment and care provided outside such an environment. Case C-157/99, _Geraets-Smits_,  \[2001\] ECR I-5473, paragraph 53.

[\[20\]](#_ftnref21) Case C-158/96, _Raymond Kohll v. Union des caisses de maladie_ \[1998\] ECR I-1931, paragraph 20 and 21 and Case 279/80, _Webb_ \[1981\] ECR 3305, paragraph 10.

[\[21\]](#_ftnref22) Case C-158/96, _Raymond Kohll_, paragraph 21.

[\[22\]](#_ftnref23) Case C-7/68, _Commission v Italy_ \[1968\] ECR 423.

[\[23\]](#_ftnref24) For the question of whether incorporeal goods are also included in the scope of the Directive see _mutatis mutandis_ the opinion of the Legal Service in Doc. 8301/04 JUR 184 SOC 171, points 23 to 27.

[\[24\]](#_ftnref25) Accordingly, the rules on free movement of goods are applicable to any products that may be valued in money and obtained against consideration, including, as the case may be, articles of artistic, historic, archaeological or ethnographic value and also products such as non-recyclable waste. Case C-2/90, _Commission v Belgium_, \[1992\] ECR I-4431, paragraphs 23-28.

[\[25\]](#_ftnref26) Adopted on the basis of Article 42 EC and Article 308 EC to cover self employed persons, students and inactive persons. Regulation (EC) No 883/2004 of the European Parliament and of the Council of 29 April 2004 on the coordination of social security systems, OJ L 166 of 30.4.2004, p. 1-123.

[\[26\]](#_ftnref27) Council Regulation (EC) No 1408/71 of 14 June 1971 on the application of social security schemes to employed persons, to self-employed persons and to members of their families moving within the Community, OJ L 149 of 5.7.1971, p. 2.

[\[27\]](#_ftnref28) The only condition is that the persons in question are or have been subject to the social security legislation of one or more Member States.

[\[28\]](#_ftnref29) This principle is enshrined in articles 4 and 5 of Regulation 883/2004 EC.

[\[29\]](#_ftnref30) Case C-131/96, _Mora Romero_, \[1997\] ECR I-3659, paragraph 32.

[\[30\]](#_ftnref31) Case C-349/87, _Paraschi_,\[1991\] ECR I-4501, paragraph 15.

[\[31\]](#_ftnref32) Case C-158/96, _Raymond Kohll v. Union des caisses de maladie_, \[1998\] ECR I-1931, paragraph 20 and 21 and Case 279/80 _Webb_, \[1981\] ECR 3305, paragraph 10.

[\[32\]](#_ftnref33) Case C-158/96, _Raymond Kohll_, paragraph 21.

[\[33\]](#_ftnref34) Directive 2004/38/EC of the European Parliament and of the Council of 29 April 2004 on the right of citizens of the Union and their family members to move and reside freely within the territory of the Member States, OJ L 158 of 30.4.2004, p. 77-123.

[\[34\]](#_ftnref35) OJ L 257 of 19.10.1968, p. 2. Regulation as last amended by Regulation (EEC) No 2434/92 (OJ L 245 of 26.8.1992, p. 1).

[\[35\]](#_ftnref36) Case C-249/83, _Hoeckx_, \[1985\] ECR 973, paragraph 20.

[\[36\]](#_ftnref37) Case C- 39/86, _Lair_,  \[1988\] ECR 3161, paragraphs 22-23.

[\[37\]](#_ftnref38) Case C- 9/74, _Casagrande_, \[1974\] ECR. 773, paragraph 14.

[\[38\]](#_ftnref39) Case C-278/94, _Commission v Belgium_ \[1996\] E.R I-4307.

[\[39\]](#_ftnref40) Case C-249/83, _Hoeckx_,  \[1985\] ECR 973, paragraph 22.

[\[40\]](#_ftnref41) Case C-65/81, _Reina_, \[1982\] ECR 33, paragraph 13.

[\[41\]](#_ftnref42) Case C-85/96, _Martinez Sala_, \[1998\] ECR I-2691, paragraph 28.

[\[42\]](#_ftnref43) Case 94/84, _Deak_, \[1985\] ECR 1873, paras 21-22; Case C-57/96, _Meints_, \[1998\] ECR I-6689 and Case C-278/94, _Commission v Belgium_, \[1996\] ECR I-4307.

[\[43\]](#_ftnref44) Other examples of social advantages: Minimum income benefits for old people (Case 261/83, _Castelli_ \[1984\] ECR 3199, 11), special old-age allowances (Case 157/84, _Frascogna_, \[1985\] ECR 1739, 21-2). benefits to cover funeral expenses (Case C-237/94, _O'Flynn_, \[1996\] ECR I-2617, paragraph 14)

[\[44\]](#_ftnref45) Case 59/85, _Reed_, \[1986\] ECR 1283, paragraph 28.

[\[45\]](#_ftnref46) Case 137/84, _Mutsch_,  \[1985\] ECR 2681, paragraph 17.

[\[46\]](#_ftnref47) Case C-184/99, _Grzelczyk_ \[2001\]_,_ ECR I-6193, paragraph 46.

[\[47\]](#_ftnref48) See Court's rulings in _Grzelczyk_ and _Martinez Sala_.

[\[48\]](#_ftnref49) According to the Court, it is settled case-law that medical activities fall within the scope of Article 50 of the Treaty, there being no need to distinguish in that regard between care provided in a hospital environment and care provided outside such an environment. Case C-157/99, _Geraets-Smits_,  \[2001\] ECR I-05473, paragraph 53.

[\[49\]](#_ftnref50) As a matter of fact also the rules concerning the free movement of persons and goods, and the rules on the freedom of establishment also apply to health care as, on the one hand, medicines and health products are goods and, on the other hand, health professionals are workers or self-employed workers.

[\[50\]](#_ftnref51) Council Regulation (EC) No 1408/71 of 14 June 1971 on the application of social security schemes to employed persons, to self-employed persons and to members of their families moving within the Community, Title III, Chapter 1, Articles 18 to 36. (OJ L 149 of 5.7.1971, p. 2) and Regulation (EC) No 883/2004 of the European Parliament and of the Council of 29 April 2004 on the coordination of social security systems, Title III, Chapter 1, Articles 17 to 35 (OJ L 166 of 30.4.2004, p. 1-123).

**[\[51\]](#_ftnref52) For an overview of the current legislative framework, case law and size of cross-border healthcare at Community level see namely, the Commission Staff Working Document, Accompanying document to the Proposal for a Directive of the European Parliament and of the Council on the application of patients' rights in cross-border healthcare, Impact Assessment, Doc. 11307/08 ADD1 SAN 136 SOC 389 MI 234 CODEC 904.**

[\[52\]](#_ftnref53) With regard to the provision of private education, it should be noted that it is regarded as a service for the purposes of Article 50 of the Treaty. Case C-76/05, _Schwarz and Gootjes-Schwarz_, \[2007\] ECR I-6849, paragraphs 39 to 40 and Case C-318/05, _Commission v Germany_, \[2007\] ECR I-6957, paragraphs 67 to 72.

[\[53\]](#_ftnref54) Similarly, with regard to vocational training Article 150 enables the Council to take measures to _inter alia_ facilitate access to vocational training and encourage mobility of instructors and trainees and particularly young people, which can be regarded as consistent with the objective of non discrimination. Here again, the measures taken shall exclude any harmonisation of the laws and regulations of the Member States.

[\[54\]](#_ftnref55) Commission document SEC(2008) 2180 also in Council Doc. of 7 July 2008, doc. 11531/08 ADD 1 SOC 411 JAI 368 MI 246, (page 26).

[\[55\]](#_ftnref56) Commission document SEC(2008) 2180 also in Council Doc. of 7 July 2008, doc 11531/08 ADD 1 SOC 411 JAI 368 MI 246.

[\[56\]](#_ftnref57) See pages 74 to 81 of the Commission's impact assessment.

[\[57\]](#_ftnref58) Case C-310/04, _Spain v Council_ \[2006\] ECR I-7285, paragraph 97. For an extensive analysis of the case law on proportionality, see the opinion of the Legal Service in Doc. 13457/06 JUR 370 TELECOM 89 COMPET 255 MI 172 CONSOM 88 CODEC 1019 and Opinion in Doc 13841/08, JUR 348 SAN 211 SOC 563 MI 348 CODEC 1264.

[\[58\]](#_ftnref59) See Case C-233/94, _Germany v European Parliament and Council_, \[1997\] ECR I-2405; Case C-84/94, _UK v Council_ \[1996\] ECR I-5755; Case C-310/04, _Spain v Council_, see citation in previous footnote.