---
title: 'INIZIATIVA NAZIONALE DI DEPOSITO PUBBLICAZIONI DI MATRIMONIO TRA COPPIE GAY'
date: Thu, 22 May 2008 06:23:42 +0000
draft: false
tags: [Affermazione Civile, certi diritti, Comunicati stampa, Mestre]
---

Sabato 24 maggio ore 11,30 presso hotel Plaza, [viale stazione 36 a Mestre](http://maps.google.it/maps?q=viale+stazione+36+,+Mestre&ie=UTF8&ll=45.483394,12.232461&spn=0.01348,0.040169&z=15) , conferenza stampa di presentazione dell’iniziativa nazionale di **Affermazione Civile**, lanciata dall’Associazione Radicale Certi Diritti, volta a supportare legalmente  tutte le coppie omosessuali  che desiderano [richiedere al loro comune la “pubblicazione degli atti” per  la celebrazione del matrimonio.](index.php?option=com_content&task=view&id=41&Itemid=72) L'iniziativa porterà molte coppie gay a presentare contemporaneamente in vari comuni  richiesta di pubblicazioni di nozze, con l’obiettivo di  innescare i conseguenti atti amministrativi per impugnarli in giudizio con la collaborazione degli avvocati della Rete Lenford.  
La forma di lotta ripropone una strada già percorsa in altre nazioni: Canada, Massachusetts e California. Allo stesso tempo è una assoluta novità nel panorama italiano.  
  
Per illustrare l’iniziativa politica e legale interverranno:  
  
\- Francesco Bilotta  Docente diritto privato Università di Udine e componente Rete Lenford  
\- Clara Comelli  Presidente Associazione Certi Diritti  
\- Franco Fois  componente Comitato Nazionale Radicali Italiani  
  
Per ulteriroi informazioni contattare:  
Franco Fois 3474192117