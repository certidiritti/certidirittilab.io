---
title: 'Commissione Europea conferma sostegno per diritti Trans'
date: Sun, 08 May 2011 11:54:52 +0000
draft: false
tags: [Transessualità]
---

**La Commissione europea conferma il suo sostegno ai diritti delle persone trans e intersex. Certi Diritti ringrazia la commissaria europea Viviane Reding per l'impegno preso con i deputati europei.**

Bruxelles-Roma, 6 maggio 2011

La Commissaria europa Viviane Reding, Vice-Presidente della Commissione europea e responsabile per i diritti fondamentali, la giustizia e la lotta alle discriminazioni, ha affermato che la Commissione "riconosce l'importanza di supportare la promozione dell'eguaglianza per le persone transgender. La Strategia sull'eguaglianza tra donne e uomini nel periodo 2010-2015 prevede lo studio e l’analisi di  specifiche questioni relative alla discriminazione sessuale in relazione all'identità di genere. Attraverso il programma Progress, la Commissione ha deciso di destinare fondi che permetteranno ad ILGA-Europe, l’organizzazione di cui fanno parte le organizzazioni europee lgbt, di sviluppare alcune attività in supporto dei diritti delle persone transgender e contribuire a rafforzare la capacità di coloro che sono impegnati nella società civile nel promuovere i loro diritti. La Commissione ha inoltre lanciato recentemente uno studio sulla "discriminazione contro le persone trans ed intersessuali sulla base del sesso, identità sessuale ed espressione sessuale". Quando i risultati di tale studio saranno disponibili, la Commissione esaminerà le azioni di follow up necessarie".

**Questo è quanto si legge nella risposta che la Commissaria Europea Reding ha dato all’ interrogazione dei parlamentari europei Michael Cashman (S&D) , Marije Cornelissen (Verts/ALE) , Sophia in 't Veld (ALDE) , Ulrike Lunacek (Verts/ALE) , Eva-Britt Svensson (GUE/NGL) , Antonyia Parvanova (ALDE) , Sirpa Pietikäinen (PPE) , Raül Romeva i Rueda (Verts/ALE) , Rui Tavares (GUE/NGL) and Britta Thomsen (S&D).**

Il testo dell’interrogazione è  disponibile al seguente sito:

**

[http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+WQ+E-2011-002442+0+DOC+XML+V0//IT](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+WQ+E-2011-002442+0+DOC+XML+V0//IT)

**

L’Associazione Radicale Certi Diritti si unisce alle associazioni per i diritti delle persone transessuali e transgender nel ringraziare la Commissione europea per le iniziative intraprese per il riconoscimento dei loro diritti fondamentali.