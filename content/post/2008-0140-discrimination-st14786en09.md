---
title: '2008-0140-Discrimination-ST14786.EN09'
date: Wed, 21 Oct 2009 12:22:27 +0000
draft: false
tags: [Senza categoria]
---

  

  

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 21 October 2009

Interinstitutional File:

2008/0140 (CNS)

14786/09

LIMITE

SOC 612

JAI 709

MI 386

  

  

  

  

  

NOTE

from :

General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

14009/09 SOC 567 JAI 645 MI 362

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached a note from the Maltese delegation with a view to the meeting of the Social Questions Working Party on 22 October 2009.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Malta’s Comments on the Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation**

With regard to Article 3(2)a, Malta would like to suggest the following amendment:

This Directive does not alter the division of competences between the European Community and the Member States. In particular Inter alia, it does not apply to:

(a)          matters covered by family law and public policy related thereto, marital or family status, and adoption, and laws on reproductive rights.’

With regard to Recital 17b, Malta would like to propose the following amendment:

(17b) Within the limits of the powers conferred upon the Community  this Directive covers access to social protection, which includes social security, \[…\] social assistance, \[…\] and health care, thereby providing comprehensive protection against discrimination in this field. Consequently, the Directive applies with regard to access to rights and benefits which are derived from general or special social security, social assistance and healthcare schemes, which are provided either directly by the State, or by private parties in so far as the provision of those benefits by the latter is funded by the State. In this context, the Directive applies with regard to \[…\] benefits in cash, benefits in kind and services, irrespective of whether the schemes involved are contributory or non-contributory. The abovementioned schemes include, for example, access to the branches of social security defined by Regulation 883/2004/EC on the coordination of social security systems[\[1\]](#_ftn1), as well as schemes providing for benefits or services granted for reasons related to the lack of financial resources or risk of social exclusion.

  

Such an amendment is important to reflect the wording in Article 3. Furthermore, it is felt that the last sentence is not required as the Recital as a whole well describes the schemes in question. Alternatively, Malta is willing to accept the deletion of the words ‘for example, access to’ as the context of this sentence is to describe the schemes concerned not the access to these schemes.

Malta would also like to propose the following amendment to Recital 17h to reflect the amendments proposed in Article 3(2)a

This Directive does not apply to matters covered by family law and public policy related thereto including marital or family status, and adoption, and laws on reproductive rights. It is also without prejudice to the secular nature of the State, state institutions or bodies, or education

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *