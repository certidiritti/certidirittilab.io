---
title: 'Newsletter 22 Luglio'
date: Wed, 31 Aug 2011 10:25:14 +0000
draft: false
tags: [Politica]
---

**![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)**

La legge contro l’omofobia e la transfobia slitta ancora. La Camera dovrebbe votarla il 26 luglio dalle ore 15.

Il 19 luglio, giorno in cui era previsto il voto, l’associazione radicale Certi Diritti era in piazza Montecitorio insieme alle altre associazioni lgbt(e) con lo slogan ’legge incostituzionale? Omofobia parlamentare’.  
Come il 13 ottobre 2009, anche quest’anno infatti la legge molto probabilmente sarà bocciata preventivamente a causa delle pregiudiziali di costituzionalità presentate dai deputati del PDL, della Lega e dell’Udc. **  
[Qui il video della maratona oratoria a Montecitorio >](http://www.radioradicale.it/organizzatori/associazione-radicale-certi-diritti)**

Dal 15 al 17 luglio il direttivo di Certi Diritti si è incontrato sul Lago di Como per fare il punto sulle iniziative in corso e preparare le campagne che saranno lanciate a settembre.   
**[Qui alcune brevi interviste registrate durante i lavori >   ](http://www.tvradicale.it/node/421) **

**Ti ricordiamo che la nostra associazione vive solo grazie all’autofinanziamento dei suoi iscritti e non riceve soldi pubblici.  
[Se deciderai di iscriverti, leggi qui come farlo. ](partecipa/iscriviti.html)**

**[E’ possibile fare anche una semplice donazione > ](partecipa/iscriviti.html) **

RASSEGNA STAMPA
===============

**Ravenna. “La prof ha detto che i gay sono malati”. Accuse di omofobia da una studentessa.   
[Leggi >](http://www.ilrestodelcarlino.it/ravenna/cronaca/2011/07/15/544384-prof_detto.shtml)**

**Roma. ****Fori, uova e insulti ad un giovane gay omofobia, tre aggressioni in 15 giorni****.  
[Leggi >](http://roma.repubblica.it/cronaca/2011/07/18/news/passeggia_lungo_i_fori_imperiali_uova_e_insulti_ad_un_omosessuale-19279883/)**

**Legge contro l’omotransfobia. L’amore incostituzionale.  
[Leggi >](http://www.notiziegay.com/?p=1094)**

**Domenico Scilipoti: "Avete mai visto due uomini fare dei figli?"   
[Leggi e guarda il video >](http://gayitaly.it/gay-chat/2011/07/domenico-scilipoti-che-oggi-votera-sulla-legge-contro-lomofobia-avete-mai-visto-due-uomini-fare-dei-figli-video/)**

**“Onorevoli gay, faccio i nomi”.   
[Leggi >](http://www.giornalettismo.com/archives/134252/gay-omosessuali-parlamento/)**

**Rodotà: uguaglianza e dignità per le persone omosessuali.   
[Leggi >](http://www.queerblog.it/post/11618/rodota-uguaglianza-e-dignita-per-le-persone-omosessuali)**

**Aids, figuraccia Italia. L'unico Paese che non paga.   
[Leggi >](http://www.unita.it/scienza/aids-figuraccia-italia-br-l-unico-paese-che-non-paga-1.314781)**

**Milano. ****A Palazzo Marino sbarcano le quote trans****.  
[Leggi >](http://www.radicali.it/rassegna-stampa/palazzo-marino-sbarcano-quote-trans)**

**Toscana. Diritti: Monaci, Consiglio contrario a ogni discriminazione.   
[Leggi >](http://www.parlamento.toscana.it/node/8237)**

**Disposizioni di fine vita. Fatto l'inganno trovata la legge.   
[Leggi >](http://www.radicali.it/rassegna-stampa/fatto-linganno-trovata-legge)**

**Non farti scippare il futuro - Mobilitazione contro lo scippo dei 4 miliardi della finanziaria ai danni delle donne.  
[Guarda ed ascolta su Radio Radicale >](http://www.radioradicale.it/scheda/331897/non-farti-scippare-il-futuro-mobilitazione-contro-lo-scippo-dei-4-miliardi-della-finanziaria-ai-danni-dell)**

**Testamento biologico: è un sequestro di persona.  
[Leggi >](http://www.radicali.it/rassegna-stampa/testamento-biologico-un-sequestro-di-persona)**

**La legge sul testamento biologico che la Camera si appresta a votare è un'amara bugia... Incontro stampa di Mina Welby, Beppino Englaro ed Ignazio Marino.   
[Ascolta su Radio Radicale >](http://www.radioradicale.it/scheda/331826/la-legge-sul-testamento-biologico-che-la-camera-si-appresta-a-votare-e-unamara-bugia-incontro-stampa-di-mi)**

**Lo zoo di 105 querelato da 'un trans': e su Facebook esplode l'omofobia.  
[Leggi >](http://it.paperblog.com/lo-zoo-di-105-querelato-da-una-trans-e-su-facebook-esplode-l-omofobia-488947/)**

**Identità di genere, è scontro tra il Vaticano e l’Onu.  
[Leggi >](http://vaticaninsider.lastampa.it/homepage/vaticano/dettaglio-articolo/articolo/vaticano-onu-identita-di-genere-vatican-gender-identity-identidad-de-genero-4864/)**

**Svizzera. “Ero lesbica, ma mia madre mi ha costretta a prostituirmi”  
[Leggi >](http://www.giornalettismo.com/archives/134091/ero-lesbica-ma-mia-madre-mi-ha-costretta-a-prostituirmi/)**

**I gay sono persone fallite e distrutte secondo il cardinale di Monaco.  
[Leggi >](http://www.queerblog.it/post/11605/i-gay-sono-persone-fallite-e-distrutte-secondo-il-cardinale-di-monaco)**

**In Russia una politica difende gli assassini dei gay!   
[Leggi >](http://www.gaywave.it/articolo/in-russia-una-politica-difende-gli-assassini-dei-gay/32553/)**

**Il prossimo sindaco di New York potrebbe essere una lesbica.   
[Leggi >](http://www.queerblog.it/post/11617/il-prossimo-sindaco-di-new-york-potrebbe-essere-una-lesbica)**

**California, la storia dei gay entra nelle scuole.  
[Leggi >](http://www.digayproject.org/Home/california_la.php?c=4597&m=9&l=it)**

**Corte Usa ordina stop immediato a divieto su militari gay.  
[Leggi >](http://it.reuters.com/article/topNews/idITMIE76605E20110707)**

**Università statunitense offre alternative per l'alloggio delle persone transessuali.  
[Leggi >](http://www.queerblog.it/post/11586/universita-statunitense-offre-alternative-per-lalloggio-delle-persone-transessuali)**

**Cina: la televisione nazionale difende i gay a seguito di dichiarazioni omofobe di un'attrice.   
[Leggi >](http://www.queerblog.it/post/11592/cina-la-televisione-nazionale-difende-i-gay-a-seguito-di-dichiarazioni-omofobe-di-unattrice)**

STUDI, RICERCHE E MATERIALI INFORMATIVI
=======================================

**Mentre i giornali tradizionali perdono lettori sul web i ragazzi cercano l’informazione.  
[Leggi >](http://www.notiziegay.com/?p=921)**

**Da solidarietà a energia pulita il nuovo dizionario degli italiani. **Un interessante lavoro sul nuovo lessico degli italiani e le parole del futuro. C'è anche il matrimonio gay che "provoca un disagio "mediamente" ampio, ma ottengono un'adesione molto convinta nei settori di sinistra radicale"**   
[Leggi >](http://www.repubblica.it/politica/2011/07/18/news/nuovo_dizionario_italiani-19258877/?ref=HREC1-7)**

**Il corpo femminile, la cultura, la politica.  
[Leggi >](http://www.statoquotidiano.it/14/07/2011/il-corpo-femminile-la-cultura-la-politica/52848/)**

**Il Governo italiano sotto esame dal Comitato CEDAW delle Nazioni Unite.  
[Leggi >](http://www.vitadidonna.org/politica/pari-opportunita/il-governo-italiano-sotto-esame-dal-comitato-cedaw-delle-nazioni-unite-7406.html)**

**Una rete europea di esperti legali nel settore della non-discriminazione.  
[Leggi >](http://www.ilsole24ore.com/art/economia/2011-07-04/rete-europea-esperti-legali-092251.shtml?uuid=Aad7Y8kD)**

**Gonorrea come il batterio killer: scoperto un nuovo ceppo resistente agli antibiotici.  
[Leggi >](http://www.corriere.it/salute/11_luglio_11/gonorrea-ceppo-resistente-giappone_1a6d6f28-abc6-11e0-a665-5070e23b7a33.shtml)**

**In Argentina ad un anno dall’entrata in vigore, celebrati quasi 2.700 matrimoni gay.   
[Leggi >](http://gaynews24.com/2011/07/15/in-argentina-ad-un-anno-dallentrata-in-vigore-celebrati-quasi-2-700-matrimoni-gay/)**

LIBRI E CULTURA
===============

**"Quando due padri possono bastare"   
[Leggi >](http://www3.lastampa.it/costume/sezioni/articolo/lstp/411979/)**

**Il regime iraniano e l’omosessualità.  
[Leggi >](http://www.ticinolibero.ch/?p=73387)**

**Discriminazioni. Nel padovano Elton John caccia i Rom.  
[Leggi >](http://www.notiziegay.com/?p=951)**

**Vanity Fair: Luca Carboni contrario alle adozioni gay e ai matrimoni Lgbt (perché hanno senso solo come sacramento).  
[Leggi >](http://www.queerblog.it/post/11589/vanity-fair-luca-carboni-contrario-alle-adozioni-gay-e-ai-matrimoni-lgbt-perche-hanno-senso-solo-come-sacramento)**

**Lady Gaga cittadina onoraria Sidney per difesa diritti gay.  
[Leggi >](http://www.wallstreetitalia.com/article/1167535/lady-gaga-cittadina-onoraria-sidney-per-difesa-diritti-gay.aspx)**

**Alessandra Cenni, [Gli occhi eroici](http://www.mursia.com/index.php?page=shop.product_details&flypage=flypage.tpl&product_id=2783&category_id=100&option=com_virtuemart&Itemid=58) _Sibilla Aleramo, Eleonora Duse, Cordula Poletti: una storia d'amore nell'Italia della Belle Epoque,_Mursia, €17.00**

**Antonella Montano, [Mogli, amanti, madri lesbiche](http://www.mursia.com/index.php?page=shop.product_details&category_id=91&flypage=flypage.tpl&product_id=2342&option=com_virtuemart&Itemid=58) _Sentimenti, sesso, convivenza: le nuove sfide della coppia_,** **Mursia,** **€17.00**

[www.certidiritti.it](http://www.certidiritti.it/)
==================================================