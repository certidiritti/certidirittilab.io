---
title: 'ECCO COME IL VATICANO COMMEMORA LA GIORNATA DELLA MEMORIA'
date: Thu, 28 Jan 2010 07:52:15 +0000
draft: false
tags: [Comunicati stampa]
---

GAY: CONSIGLIO EUROPA: INGERENZA VATICANA ALL'ATTACCO IN EUROPA

L'Associazione Radicale Certi Diritti: Ma i parlamentari italiani rappresentano il Vaticano o l'Italia? Il Consiglio d'Europa verifichi e protesti col Vaticano per le ingerenze!

Il Vaticano ha scritto ai deputati del Consiglio d'Europa chiedendo loro di affossare la risoluzione sulle discriminazioni sessuali e sull'identità di genere e di fare eleggere il più cattolico dei 3 candidati italiani alla Corte europea dei diritti dell'uomo, anche al fine di influenzare la futura sentenza in appello sul crocefisso. In tale lettera, disponibile in francese su [http://www.werktitel.be/wp-content/uploads/2010/01/Fax-nuntius2.pdf](http://www.werktitel.be/wp-content/uploads/2010/01/Fax-nuntius2.pdf) , il Vaticano invita i deputati a contattare per ogni questione tre deputati eletti in Italia: Volonté, Farina e Gatti. Ora, mai era successo prima che il Vaticano indicasse tre deputati come se fossero i capogruppo dello Stato o del Partito Vaticano all'assemblea parlamentare del Consiglio d'Europa, che non prevede rappresentazione per gli Stati non democratici - ovvero quelli che non svologono elezioni e non hanno rappresentanza parlamentare - come lo è lo Stato del Vaticano. ci auguriamo che l'assemblea parlamentare del Consiglio d'Europa verifichi se i deputati italiani Volonté, Farina e Gatti sono rappresentanti dell'Italia o di un altro Stato e protesti con il Vaticano per le indebite ingerenze.

Qui di seguito il testo della lettera del Vaticano: [http://www.werktitel.be/wp-content/uploads/2010/01/Fax-nuntius2.pdf](http://www.werktitel.be/wp-content/uploads/2010/01/Fax-nuntius2.pdf)