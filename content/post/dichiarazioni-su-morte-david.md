---
title: 'Dichiarazioni su morte david'
date: Fri, 04 Feb 2011 08:04:30 +0000
draft: false
tags: [Africa]
---

Abbiamo raccolto in questa pagina tutte le dichiarazioni sull'assasinio di David Kato, iscritto dell'Associazione radicale Certi Diritti ed attivista per i diritti civili ed umani, in particolare dei diritti glbt in Uganda.

[Ucciso in Uganda David Kato, esponente del movimento lgbt](tutte-le-notizie/1005-ucciso-in-uganda-david-kato-esponente-del-movimento-lgbt.html)
------------------------------------------------------------------------------------------------------------------------------------------------

[CERTI DIRITTI: KILLED IN UGANDA DAVID KATO KISULE, LGBT ACTIVIST](tutte-le-notizie/1006-certi-diritti-killed-in-uganda-david-kato-kisule-lgbt-activist.html)
-------------------------------------------------------------------------------------------------------------------------------------------------------------

[Marco Pannella su David Kato, appello ad autorità italiane](tutte-le-notizie/1007-marco-pannella-su-david-kato-appello-ad-autorita-italiane.html)
--------------------------------------------------------------------------------------------------------------------------------------------------

[Rossodivita commemora Kato al Consiglio regionale del Lazio](tutte-le-notizie/1017-rossodivita-commemora-kato-al-consiglio-regionale-del-lazio.html)
-----------------------------------------------------------------------------------------------------------------------------------------------------

[Marco Perduca in Senato sull'uccisione di David Kato Kisule](tutte-le-notizie/1023--perduca-sulluccisione-di-un-attivista-per-i-diritti-degli-omosessuali-in-uganda-.html)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

[Agenzie Stampa sulla morte di David Kato Kisule](tutte-le-notizie/1025-agenzie-stampa-sulla-morte-di-david-kato-kisule.html)
-----------------------------------------------------------------------------------------------------------------------------