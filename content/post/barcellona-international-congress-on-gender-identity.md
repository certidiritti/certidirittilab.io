---
title: 'BARCELLONA: INTERNATIONAL CONGRESS ON GENDER IDENTITY'
date: Thu, 03 Jun 2010 12:49:19 +0000
draft: false
tags: [Comunicati stampa]
---

### International Congress On Gender Identity And Human Rights - Barcelona, 4-6 JUN 2010 

**L’Associazione Radicale Certi Diritti parteciperà ai lavori dell’International Congress on Gender Identity and Human Righs che si svolgerà a Barcellona dal 4 al 6 giugno 2010. Certi Diritti sarà rappresentata dall’Avvocato Ugo Millul, membro del Direttivo.**

I lavori prevedono 5 diversi temi:

Tema 1. Violencia, Criminalización y relaciones institucionales  
Tema 2. Identidad de género y acceso a los servicios de salud  
Tema 3. La discriminación legal o de facto en el acceso a los servicios sociales  
Tema 4. El proceso de cambio de documentos de identificación o cambio de sexo legal  
Tema 5. Creación de una red global trans

Transgender is a constant that appears in all known human societies in all ages, regardless of the achievements that modern medicine has made in this area to enable a greater adaptation of the physical appearance of the transgender person, to the gender sense as their own. The responses that different societies have given this reality of the human being, however, have varied widely over time and in different parts of our world. Some societies have accepted in a greater or lesser degree this reality, creating social tools and laws to improve the integration of the transgender people into the society, as members with full rights. Others have expressed varying degrees of rejection and/or prosecution and suppression of transsexuality promoting policies of violent eradication, re-education or social persecution, and the result has generate serious human rights violations.

Maggiori informazioni al seguente link:

[http://www.congenid.org/en.html](http://www.congenid.org/en.html)