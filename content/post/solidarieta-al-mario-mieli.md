---
title: 'SOLIDARIETA'' AL MARIO MIELI '
date: Fri, 18 Apr 2008 16:41:46 +0000
draft: false
tags: [Comunicati stampa, Mario Mieli]
---

In seguito [all'assalto](http://www.lastampa.it/redazione/cmsSezioni/cronache/200804articoli/32023girata.asp) subito dal Circolo Culturale [Mario Mieli](http://www.mariomieli.org) , l'associazione Certi Diritti esprime la solidarietà all'associazione. 

Questo il messaggio che il segretario ha inviato al Circolo a nome di tutta l'associazione.

Caro Andrea; cari amici del Mario Mieli,

a nome dell'Associazione Radicale Certi Diritti vi esprimo tutta la nostra solidarieta' e vicinanza per il vile atto di violenza accaduto.

Un caro saluto,

Sergio Rovasio

Segretario Associazione Radicale Certi Dirtti

[www.certidiritti.it](http://www.certidiritti.it)