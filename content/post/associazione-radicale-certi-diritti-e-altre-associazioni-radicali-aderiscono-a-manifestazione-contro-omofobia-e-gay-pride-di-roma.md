---
title: 'Associazione Radicale Certi Diritti e altre associazioni radicali aderiscono a manifestazione contro omofobia e Gay Pride di Roma'
date: Thu, 21 Jun 2012 12:36:31 +0000
draft: false
tags: [Politica]
---

Sabato al Roma Pride (ore 15 piazza della Repubblica) sfileremo con tre mezzi elettrici tematici su unioni civili/matrimonio tra persone dello stesso sesso, legalizzazione della prostituzione e prevenzione malattie sessualmente trasmissibili.

Roma, 21 giugno 2012

Comunicato Stampa dell'Associazione Radicale Certi Diritti

L’Associazione Radicale Certi Diritti, insieme a Radicali Italiani, Radicali Roma, le Associazioni Luca Coscioni, Nessuno Tocchi Caino, Non c’è Pace senza Giustizia e il Gruppo Lista Bonino Pannella, Federalisti Europei al Consiglio Regionale del Lazio, aderiscono e partecipano alla manifestazione contro l'omofobia prevista in Piazza Farnese a Roma, la sera di venerdì 22 giugno e al Gay Pride che sfilerà per le vie di Roma sabato pomeriggio 23 giugno 2012.

Al Gay Pride di Roma i Radicali hanno previsto l’allestimento di tre mezzi elettrici sui temi della prevenzione delle malattie sessualmente trasmissibili (con la distribuzione gratuita di migliaia di preservativi), per la legalizzazione della prostituzione, anche maschile, e sulle unioni civili/matrimonio tra persone dello stesso sesso. Tutti temi che caratterizzano da sempre l’impegno dell’Associazione Radicale Certi Diritti da quando è nata.

I Radicali chiedono che venga quanto prima calendarizzata la Proposta di Legge sulla Riforma del Diritto di famiglia, depositata due anni fa dalla deputata Radicale – Pd Rita Bernardini così come ormai avviene in tutti i paesi democratici e contro il potere della casta clericale che impedisce all’Italia di affermare principi di laicità e uguaglianza anche sul fronte dei diritti civili, che include il matrimonio tra persone dello stesso sesso. Tra le richieste anche quella di calendarizzare la proposta di delibera popolare al Comune di Roma per la regolamentazione delle Unioni Civili depositata dalle associazioni Lgbt della Capitale con  oltre 8.000 firme raccolte.

Al Consiglio Regionale del Lazio  nel corso della seduta di mercoledì 20 giugno  i Capigruppo dei Radicali Giuseppe Rossodivita e di Sel Luigi Nieri, sono intervenuti in aula per condannare gli atti di violenza subiti nei  giorni scorsi da Guido Allegrezza, militante Lgbt di Roma e altre persone omosessuali. Durante gli interventi in aula i Consiglieri hanno ricordato che la città di Roma ha il più alto tasso di atti di violenza omofobica in Italia e hanno chiesto che vengano calendarizzate quanto prima le proposte di legge depositate quasi due anni fa alla Regione che hanno l’obiettivo di prevenire ogni forma di violenza con apposite campagne di informazione e di aiuto alle persone Lgbt(e), così come già avviene nella Regione Liguria che, su questo tema, ha approvato un’ottima legge.