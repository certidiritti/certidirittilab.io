---
title: 'MATRIMONIO GAY-CORTE COSTITUZIONALE: DEPOSITATA MEMORIA COSTITUZIONE'
date: Mon, 20 Jul 2009 12:36:10 +0000
draft: false
tags: [Comunicati stampa]
---

MATRIMONIO GAY: DEPOSITATA OGGI ALLA CORTE COSTITUZIONALE LA MEMORIA DI COSTITUZIONE DELLA COPPIA DI VENEZIA.

Roma, 20 luglio 2009

Comunicato Stampa dell’Associazione Radicale Certi Diritti

“E’stata oggi depositata alla Corte Costituzionale la memoria di costituzione della coppia di Venezia che, aderendo alla campagna di Affermazione Civile, promossa in tutta Italia dall’Associazione Radicale Certi Diritti, in collaborazione con l’Associazione Avvocatura per i diritti LGBT – Rete Lenford, aveva chiesto all’ufficiale dello Stato Civile del proprio Comune di residenza le pubblicazioni di matrimonio. A seguito del diniego loro opposto, era stata incardinata dinanzi al Tribunale di Venezia un ricorso per far dichiarare illegittimo il rifiuto. Il Tribunale, con ordinanza del 3 aprile 2009, ha rimesso alla Corte Costituzionale la questione di legittimità chiedendo alla Corte se negare alle coppie formate da persone dello stesso sesso l’accesso all’istituto del matrimonio sia conforme alla Costituzione italiana.

Il collegio di Avvocati, coordinato da Francesco Bilotta, nella memoria di costituzione ha, tra l’altro, affermato che:

“Realizzarsi pienamente come persona significa poter vivere fino in fondo il proprio orientamento sessuale, scegliendo come partner di vita, all’interno di una relazione giuridica qualificata, qual è il matrimonio, una persona del proprio sesso. Solo così si potrebbe garantire l’effettivo godimento in ambito famigliare per le persone omosessuali del diritto a realizzarsi come persona”.

Inoltre hanno ricordato che:

“Così come abbiamo imparato, anche grazie alle sentenze della Corte Costituzio-nale italiana e delle più alte Corti straniere, che un uomo non è superiore a una donna, una persona di colore non è inferiore a una persona bianca, un cristiano non è migliore di un ebreo, è arrivato il momento di affermare che una persona omosessuale non merita una dignità inferiore, sul piano giuridico, rispetto a una persona eterosessuale anche quando decide di contrarre matrimonio”.