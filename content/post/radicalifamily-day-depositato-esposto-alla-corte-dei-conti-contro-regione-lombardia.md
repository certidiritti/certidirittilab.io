---
title: 'RADICALI/FAMILY DAY: DEPOSITATO ESPOSTO ALLA CORTE DEI CONTI CONTRO REGIONE LOMBARDIA'
date: Sun, 21 Feb 2016 12:03:30 +0000
draft: false
tags: [Politica]
---

[![pirellone](http://www.certidiritti.org/wp-content/uploads/2016/01/pirellone-300x225.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/01/pirellone.jpg)Milano, 21 febbraio 2016

I radicali di Certi Diritti, grazie all’avv. Andrea Bullo, hanno depositato un [esposto](http://www.certidiritti.org/wp-content/uploads/2016/02/Esposto-Maroni.pdf) alla Sezione Giurisdizionale della Corte dei Conti per la Lombardia chiedendo al Procuratore Regionale se la condotta di Regione Lombardia in merito al Family Day non possa integrare un’ipotesi di danno erariale.

Ricordiamo che nella seduta n. 150 del 22 gennaio 2016, la Giunta regionale della Lombardia ha deliberato che «il coinvolgimento della Regione» al Family Day «prevede (-) la partecipazione di Assessori e Consiglieri alla manifestazione a Roma; (-) la presenza del Gonfalone Regionale; (i) l’esposizione sulla facciata di Palazzo Pirelli della scritta “Family Day».

Per i radicali di Certi Diritti «non v’è chi non veda come l’utilizzo dei beni strumentali della Regione, l’invio del Gonfalone Regionale e la partecipazione di Assessori e Consiglieri ad una manifestazione di carattere nazionale, caratterizzata da una rappresentatività soltanto parziale delle comunità insistenti sul territorio regionale Lombardo integri, oltre ad una frontale violazione delle funzioni che lo Statuto assegna alla Regione, l’indebito utilizzo di risorse pubbliche per il compimento di attività propagandistiche nell’interesse delle sole forze politiche di maggioranza, se non dei soli soggetti apicali che hanno ottenuto od otterranno visibilità dall’iniziativa. Ciò, in patente spregio del vincolo di rappresentanza unitaria delle realtà politiche e sociali insistenti sul territorio gravanti sull’Istituzione regionale, e con macroscopica ed inescusabile trascuratezza nell’utilizzo di risorse pubbliche».