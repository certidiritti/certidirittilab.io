---
title: 'Conferenza Stampa su Censimento martedì 11 ottobre ore 13 alla Camera'
date: Tue, 11 Oct 2011 04:01:41 +0000
draft: false
tags: [Politica]
---

Censimento italiano: per la prima volta le coppie gay ed etero conviventi si potranno dichiarare (purchè lo sappiano). Appello alla stampa per diffondere questa informazione.

**Martedì 11 ottobre 2011, alle ore 13, presso la Sala Stampa della Camera dei Deputati (Via della Missione, 4 –Roma) CONFERENZA STAMPA con**

**Emma Bonino**, Vice Presidente del Senato;

**Flavia Perina**, deputato Fli;

**Anna Paola Concia**, deputata Pd;

**Carlo D'Ippoliti**, Ricercatore Università La Sapienza – Roma;

**Sergio Rovasio**, Segretario Associazione Radicale Certi Diritti;

**Paolo Patanè**, Presidente di Arcigay;

**Valeria Manieri**, Associazione Pari o Dispare;

**Antonio Rotelli**, Presidente Avvocatura Lgbt - Rete Lenford;

**Massimo Farinella**, Circolo Mario Mieli – Roma;

La campagna di sensibilizzazione ‘Fai contare il tuo amore’ è stata promossa dal portale [www.gay.it](http://www.gay.it/), e dalle Associazioni Arcigay, Associazione Radicale Certi Diritti, Avvocatura Lgbt – Rete Lenford, Circolo di cultura omosessuale Mario Mieli; Associazione DiGayProject.

Tutte le informazioni sulle modalità di compilazione del questionario Istat per le coppie conviventi, gay ed eterosessuali, si possono trovare al seguente link:    [http://www.gay.it/faicontareiltuoamore](http://www.gay.it/faicontareiltuoamore)