---
title: 'BOLOGNA, DISTRIBUZIONE PRESERVATIVI DAVANTI AL LICEO MINGHETTI'
date: Wed, 02 Jun 2010 15:35:37 +0000
draft: false
tags: [Senza categoria]
---

### A favore dell’educazione sessuale nelle scuole.

**Venerdì 4 giugno**, i giovani socialisti di Bologna, l’Associazione radicale Giorgiana Masi di Bologna, all’Associazione radicale Certi Diritti, e gli Studenti Coscioni, promuovono una distribuzione gratuita di preservativi, davanti al Liceo Scientifico Minghetti, con lo scopo di incentivare gli istituti scolastici e le istituzioni locali e statali a sostenere campagne di educazione sessuale.

I ragazzi appartenenti alle quattro organizzazioni sottolineano come uno dei gravi handicap che da sempre affligge l’istruzione pubblica è l’assenza di una informazione sessuale all’interno delle scuole che abbia il coraggio di parlare di metodi contraccettivi, di sensibilizzare i giovani in merito alle malattie sessualmente trasmissibili, di informare sulle strutture sanitarie pubbliche esistenti nella nostra città che si occupano di prevenzione sessuale.  
Per i rappresentanti delle quattro associazioni bolognesi, questa sarà solo una delle prime iniziative sul tema.  
Federazione Giovani Socialisti Bologna  
Associazione Radicale Giorgiana Masi  
Associazione Radicale Certi Diritti  
Studenti Coscioni