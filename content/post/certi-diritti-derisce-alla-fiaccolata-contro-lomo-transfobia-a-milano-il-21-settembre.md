---
title: 'Certi Diritti derisce alla fiaccolata contro l’omo-transfobia a Milano il 21 settembre'
date: Fri, 17 Sep 2010 23:47:30 +0000
draft: false
tags: [MANIFESTAZIONE, Milano, OMOFOBIA, Senza categoria]
---

Di seguito il comunicato congiunto con altra associazioni attive a Milano**:  
**

**“e io non ho paura”** **Fiaccolata contro l’omo-transfobia 21 settembre 2010 a Milano**

_Adesione e supporto alla fiaccolata “e io non ho paura” organizzata per rispondere al silenzio dei media e della politica di fronte ai reati d’odio omofobico e transfobico._

Arcobaleni in Marcia, CIG Arcigay Milano, Arcilesbica Zami, Associazione Radicale Certi Diritti, GayLib, Gruppo Soggettività lesbica, KOB Kollettivo Omosessuale Bicocca aderiscono e invitano a partecipare alla manifestazione-fiaccolata “e io non ho paura” che si terrà martedì 21 settembre alle ore 20.30 dai Bastioni di porta Venezia organizzata su iniziativa di un gruppo spontaneo di cittadine e cittadini con l’obiettivo di abbattere la cortina di silenzio alimentata da una classe politica disattenta di fronte ai reati d’odio a matrice omo-transfobica.

Oggi, in Italia, nessuna norma è prevista nell'ordinamento a tutela delle persone gay, lesbiche, bisessuali e trans e nonostante sia nuovamente in calendario la discussione di proposte di legge in Parlamento nessun partito e movimento politico sembra realmente interessato a rendere prioritaria nell’agenda politica una legge adeguata di respiro europeo, come auspicato dal Presidente della Repubblica.

Le associazioni che firmano questo comunicato partecipano a questa manifestazione spontanea su invito degli organizzatori, tutti uniti in una comunità ideale di gruppi, movimenti e liberi cittadini che credono nell’importanza della visibilità e dell’impegno civico e individuano il silenzio come alleato della violenza e l'indifferenza della politica come sua mandante.

Una manifestazione che vuole spezzare il binomio vittima=debole, per affermare la nostra forza civile e politica.  
Nel richiedere l’estensione della legge Mancino anche per i reati omo-trasfobici ci asupichiamo che tale traguardo di civiltà non venga associato a quei dispositivi autoritari e liberticidi, come i “pacchetti sicurezza” tanto cari a chi odia i “diversi”.

Arcobaleni in Marcia  
Arcigay Milano CIG Centro di Iniziativa Gay  
Arcilesbica Zami Milano  
Associazione radicale Certi Diritti  
GayLib  
Gruppo Soggettività Lesbica  
KOB Kollettivo Omosessuale Bicocca