---
title: 'DOCUMENTO RISERVATO DEL CONSIGLIO UE: IL TESTO IN ESCLUSIVA'
date: Tue, 28 Oct 2008 17:18:13 +0000
draft: false
tags: [Comunicati stampa]
---

**CERTI DIRITTI DENUNCIA, SULLA BASE DI UN DOCUMENTO CONFIDENZIALE DEL CONSIGLIO UE, OBIEZIONI E RETICENZE PRETESTUOSE DA PARTE DEI GOVERNI DI ALCUNI STATI MEMBRI DELL'UE SULLA DIRETTIVA ANTI-DISCRIMINAZIONE

Certi Diritti é venuta in possesso di un documento confidenziale e non accessibile relativo al dibattito che ha avuto luogo al Consiglio in merito alla proposta della Commissione sulla direttiva anti-discriminazione. Come si puo' leggere nel documento, una serie di Stati membri, capeggiata dalla Germania e seguita dalla Repubblica Ceca, Irlanda, Italia e molti altri, hanno sollevato una serie di obiezioni e reticenze sulla direttiva europea. Il testo del documento é disponibile sul sito di Certi Diritti.

Dichiarazione di Marco Cappato, Eurodeputato Radicale, Sergio Rovasio, Segretario di Certi Diritti e Ottavio Marzocchi, responsabile per le questioni europee:

_"La direttiva anti-discriminazione proposta dalla Commissione europea é stata accolta in modo - a dir poco - gelido da parte degli Stati membri. Come rivela il documento confidenziale, la Germania assieme ad altri Stati membri, Italia inclusa, hanno sollevato una serie di obiezioni e riserve sul contenuto della proposta, licenziata dalla Commissione europea su esplicita e ripetuta richiesta del Parlamento europeo._

_Sebbene la direttiva sia simile ad altre direttive europee già in vigore, approvate dagli stessi Stati membri (la direttiva sulla razza e la direttiva sull'impiego), questi ora sollevano obiezioni di merito e di procedura evidentemente pretestuose, come peraltro chiarito dalla Commissione nelle sue risposte ai quesiti degli Stati membri._

_Ci auguriamo che questo difficile inizio di discussione sulla proposta di direttiva non sia una obiezione di principio all'allargare la protezione attualmente prevista nella UE per le discriminazioni a sfondo razziale alle persone disabili, omosessuali, anziane, o di religioni minoritarie, e che gli Stati vogliano dare reale attuazione ai Trattati ed all'articolo 13 TUE, che permette all'Unione di elaborare politiche di lotta alle discriminazioni, nell'ambito della creazione di uno spazio di democrazia, libertà ed eguaglianza in Europa"._

Nota:

\- l'UE ha adottato due direttive anti-discriminazione sulla base dell'art. 13 TUE: una sulla razza (che vieta le discriminazioni basate sulla razza nei settori dell'impiego, dell'educazione, dei beni e dei servizi) ed una sull'impiego (che vieta le discriminazioni basate sul sesso, orientamento sessuale, età, religione nel settore dell'impiego)

\- la Commissione europea ha licenziato il 2 luglio scorso una proposta di direttiva per vietare le discriminazioni basate sul sesso, orientamento sessuale, età e religione nei settori rimasti fuori dalla direttiva precedente: educazione, beni e servizi)

\- il Consiglio deve adottare all'unanimità la direttiva perché questa entri in vigore; il PE sta esaminando la proposta di direttiva, ma é solamente consultato (ovvero non ha potere legislativo); nel corso delle prime discussioni, la Germania ha contestato la correttezza giuridica dell'atto basato sull'articolo 13 del Trattato EU; altri Stati hanno sollevato obiezioni e riserve.

IL TESTO DEL DOCUMENTO DI CUI SIAMO VENUTI IN POSSESSO:

  

**COUNCIL OF**

**THE EUROPEAN UNION**

**Brussels,**  **22 October 2008**

**14254/08**

**LIMITE**

**SOC 594**

**JAI 531**

**MI 363**

OUTCOME OF PROCEEDINGS

from :

The Working Party on Social Questions

on :

14 October 2008

No. prev. doc. :

12956/08 SOC 494 JAI 451 MI 306 + COR 1

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

I. INTRODUCTION

At its meeting on 14 October 2008, the Working Party on Social Questions continued its examination of the above proposal.[\[1\]](#_ftn1)

The Presidency outlined the further work on the proposal foreseen under the French Presidency in the light of the policy debate that took place in the EPSCO Council on 2 October 2008. The Commission representative gave a detailed presentation of the impact assessment accompanying the proposal. The remainder of the discussion focused on Articles 5-18 of the proposal.

  

A large majority of delegations (BE, BG, DK, EE, EL, ES, FR, IE, LT, LU, HU, MT, NL, AT, PL, SI, SK, FI, SE, UK) have welcomed the proposal in principle, many endorsing the fact that it aims to complete the existing legal framework by addressing all four grounds of discrimination through a horizontal approach.

Most delegations have affirmed the importance of promoting equal treatment as a shared social value within the EU. In particular, several delegations have underlined the significance of the proposal in the context of the UN Convention on the Rights of Persons with Disabilities. However, some delegations (ES, AT, PT, SI) would have preferred more ambitious provisions, particularly in regard to disability.

While emphasising the importance of the fight against discrimination, DE has put forward the view that more experience with the implementation of existing Community law is needed before further legislation is adopted at the Community level. This delegation has questioned the timeliness and the need for the Commission’s new proposal, which it sees as infringing on national competence for certain issues[\[2\]](#_ftn2).

Pending further examination of the proposal, CZ has informed delegations of its concerns in regard to proportionality and subsidiarity, in the light of a Resolution adopted by the Senate of the Czech Parliament.

All delegations have maintained general scrutiny reservations on the proposal at this stage.

CZ, DK, FR, MT and UK have maintained parliamentary scrutiny reservations. In this connection, NL has informed delegations of the position adopted by the NL Parliament, which has requested clarification of the scope and the envisaged practical, legal and financial impact of the proposal[\[3\]](#_ftn3).

CY and PL have maintained linguistic scrutiny reservations.

  

II. FURTHER WORK UNDER THE FRENCH PRESIDENCY

In the light of the policy debate that took place in the EPSCO Council on 2 October 2008, the Chair outlined the French Presidency's plans for further work on this proposal, as follows:

Horizontal issues relating to _the legal base_ and _subsidiarity_ will be examined on the basis of the forthcoming Opinion of the Council Legal Service.

Articles 2, 3 and 4 will be examined with a view to improving legal certainty, especially in regard to the provisions on _age and disability_, and in order to clarify the dynamic between the provisions of the Directive and areas of national competence, in particular, _education, health and social advantages_.

The issues to be examined in this context will include the exclusion of activities in _the private sphere_ from the scope (Article 3(1)) and key concepts relating to the provisions on disability, including "_effective non-discriminatory access_" and "_reasonable accommodation_" (Article 4).

III. IMPACT ASSESSMENT

The Commission representative presented the impact assessment accompanying the proposal, as contained in doc. 11531/08 ADD 1[\[4\]](#_ftn4). She reminded delegations that additional documents were available on the Commission's website, namely, the mapping study[\[5\]](#_ftn5) undertaken at the preparatory stage, and a study compiled by the European Evaluation Policy Consortium (EPEC)[\[6\]](#_ftn6).

  

The Commission representative and several delegations acknowledged the challenges involved in compiling detailed data on matters relating to discrimination. Responding to the concern raised by DE in regard to the quality and completeness of the data on which the impact assessment was based, the Commission representative explained that research available at the national level had been used, and that a degree of extrapolation was necessary. As regards the issue of education, she pointed out that statistics showed that people with disabilities were less likely to receive education at a high level and that many gay and lesbian students suffered bullying at school.

Several delegations (DE, IT, MT, NL, PL) underlined the importance of analysing the impact of the proposed provisions further at the national and sub-national level; DE drew attention to its written comments[\[7\]](#_ftn7). Several delegations (EL, IT, MT, NL, SE) also stressed the importance of legal certainty as a prerequisite for assessing the impact of the provisions foreseen.

Certain delegations raised the concern of the impact of the proposed Directive on SMEs (DE, EL, MT, PL). The Commission representative recalled that the size of the organisation had been specifically mentioned as one of the criteria for determining whether measures envisaged for the benefit of persons with disabilities would impose a disproportionate burden (Article 4(1)(b)).

Responding to concerns raised by IT regarding the need to study the economic impact of the provisions in greater depth, including in the area of financial services, the Commission representative recalled the exemptions contained in the proposal (see Article 2(7)) and reiterated that the Commission would take the lead in setting up a dialogue with the insurance and banking industries[\[8\]](#_ftn8).

  

Responding to IE, the Commission representative confirmed that the impact of Directive 2000/43/EC had been followed up in a Commission report, published in 2006[\[9\]](#_ftn9), which had shown that national legislation against racial discrimination had been adopted and Equality Bodies had been established in the Member States. She underlined the difficulty of ascertaining whether legislation _per se_ had a preventative effect and concurred with IE that non-legislative measures also played a vital role in the fight against discrimination.

DK requested clarification regarding the definitions of "disability" and "long-term illness" respectively.

PT expressed the view that discrimination in the area of education was an issue of particular concern and merited action to promote equal opportunities.

The Chair concluded that both the Commission and the Member States faced a difficult task in collating data on discrimination, and that the discussion had shown the importance of carefully assessing the cost implications of the proposed provisions as well as of discrimination itself.

IV. DISCUSSION ON ARTICLES 5-18

The Working Party examined Articles 5-18 of the proposal. The discussion focused, in particular, on the Defence of Rights (Article 7) and the Burden of Proof (Article 8).

The Commission representative also provided an update of the ongoing examination of the proposals concerning the conclusion of the UN Convention on the Rights of Persons with Disabilities and its Optional Protocol by the European Community[\[10\]](#_ftn10), which are being handled by the Working Party on Human Rights.

Details of the discussion are set out in footnotes below.

  

V. ANY OTHER BUSINESS

The Commission representative reminded delegations that the first meeting of the Non-Discrimination Governmental Expert Group[\[11\]](#_ftn11) was scheduled to take place on 18 November 2008. She invited delegations that have not yet done so to submit their nominations for members of this group.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

ANNEX

_Article 5  
Positive action_

With a view to ensuring full equality in practice, the principle of equal treatment shall not prevent any Member State[\[12\]](#_ftn12) from maintaining or adopting specific measures to prevent or compensate for disadvantages linked to religion or belief, disability, age, or sexual orientation.

_Article 6  
Minimum requirements_

1. Member States may introduce or maintain provisions which are more favourable to the protection of the principle of equal treatment than those laid down in this Directive.

2. The implementation of this Directive shall under no circumstances constitute grounds for a reduction[\[13\]](#_ftn13) in the level of protection against discrimination already afforded by Member States in the fields covered by this Directive_._

  

CHAPTER II  
REMEDIES AND ENFORCEMENT

_Article 7  
Defence of rights_

1. Member States shall ensure that judicial and/or administrative procedures, including where they deem it appropriate conciliation procedures, for the enforcement of obligations under this Directive are available to all persons who consider themselves wronged by failure to apply the principle of equal treatment to them, even after the relationship in which the discrimination is alleged to have occurred has ended.

2. Member States shall ensure that associations, organisations or other legal entities, which have a legitimate interest in ensuring that the provisions of this Directive are complied with, may engage[\[14\]](#_ftn14), either on behalf or in support[\[15\]](#_ftn15) of the complainant, with his or her approval, in any judicial and/or administrative procedure provided for the enforcement of obligations under this Directive[\[16\]](#_ftn16).

  

3. Paragraphs 1 and 2 shall be without prejudice to national rules relating to time limits for bringing actions as regards the principle of equality of treatment.

_Article 8  
Burden of proof_

1. Member States shall take such measures as are necessary, in accordance with their national judicial systems, to ensure that, when persons who consider themselves wronged because the principle of equal treatment has not been applied to them establish, before a court or other competent authority, facts from which it may be presumed that there has been direct or indirect discrimination, it shall be for the respondent to prove[\[17\]](#_ftn17) that there has been no breach of the prohibition of discrimination.

2. Paragraph 1 shall not prevent Member States from introducing rules of evidence which are more favourable to plaintiffs.

  

3. Paragraph 1 shall not apply to criminal procedures.

4. Member States need not apply paragraph 1 to proceedings in which the court or competent body investigates the facts of the case[\[18\]](#_ftn18).

5. Paragraphs 1, 2, 3 and 4 shall also apply to any legal proceedings commenced in accordance with Article 7(2).

_Article 9  
Victimisation_

Member States shall introduce into their national legal systems such measures as are necessary to protect individuals from any adverse treatment or adverse consequence as a reaction to a complaint or to proceedings[\[19\]](#_ftn19) aimed at enforcing compliance with the principle of equal treatment.

_Article 10  
Dissemination of information_

Member States shall ensure that the provisions adopted pursuant to this Directive, together with the relevant provisions already in force, are brought to the attention of the persons concerned by appropriate means throughout their territory.

_Article 11  
Dialogue with relevant stakeholders_

With a view to promoting the principle of equal treatment, Member States shall encourage dialogue with relevant stakeholders, \[in particular non-governmental organisations, which have, in accordance with their national law and practice, a legitimate interest in contributing to the fight against discrimination on the grounds and in the areas covered by this Directive.\][\[20\]](#_ftn20)

  

_Article 12_[_\[21\]_](#_ftn21)  
_Bodies for the Promotion of Equal treatment_

1. Member States shall designate a body or bodies for the promotion of equal treatment of all persons irrespective of their religion or belief, disability, age, or sexual orientation. \[These bodies may[\[22\]](#_ftn22) form part of agencies charged at national level with the defence of human rights or the safeguard of individuals' rights, including rights under other Community acts including Directives 2000/43/EC and 2004/113/EC.\][\[23\]](#_ftn23)

  

2. Member States shall ensure that the competences of these bodies include:

– without prejudice to the right of victims and of associations, organizations or other legal entities referred to in Article 7(2), providing independent assistance to victims of discrimination in pursuing their complaints about discrimination,

– conducting independent surveys concerning discrimination,

– publishing independent reports and making recommendations on any issue relating to such discrimination.

CHAPTER III  
FINAL PROVISIONS

_Article 13  
Compliance_

Member States shall take the necessary measures to ensure that the principle of equal treatment is respected and in particular that:

(a) any laws, regulations and administrative provisions contrary to the principle of equal treatment are abolished;

(b) any contractual provisions, internal rules of undertakings, and rules governing profit-making or non-profit-making associations contrary to the principle of equal treatment are, or may be, declared null and void or are amended.

_Article 14_[_\[24\]_](#_ftn24)  
_Sanctions_

Member States shall lay down the rules on sanctions applicable to breaches of the national provisions adopted pursuant to this Directive, and shall take all measures necessary to ensure that they are applied. Sanctions may comprise the payment of compensation, which may not be restricted by the fixing of a prior upper limit[\[25\]](#_ftn25), and must be effective, proportionate and dissuasive.

_Article 15  
Implementation_

1. Member States shall adopt the laws, regulations and administrative provisions necessary to comply with this Directive by …. at the latest \[two years[\[26\]](#_ftn26) after adoption\]. They shall forthwith inform the Commission thereof and shall communicate to the Commission the text of those provisions and a correlation table[\[27\]](#_ftn27) between those provisions and this Directive.

  

When Member States adopt these measures, they shall contain a reference to this Directive or be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.

2. In order to take account of particular conditions, Member States may, if necessary, establish that the obligation to provide effective access as set out in Article 4 has to be complied with by … \[at the latest\] four[\[28\]](#_ftn28) \[years after adoption\].

Member States wishing to use this additional period shall inform the Commission at the latest by the date set down in paragraph 1 giving reasons.

  

_Article 16  
Report_

1. Member States and national equality bodies[\[29\]](#_ftn29) shall communicate to the Commission, by …. at the latest and every five years thereafter, all the information necessary for the Commission to draw up a report to the European Parliament and the Council on the application of this Directive.

2. The Commission's report shall take into account, as appropriate, the viewpoints of the social partners[\[30\]](#_ftn30) and relevant non-governmental organizations, as well as the EU Fundamental Rights Agency. In accordance with the principle of gender mainstreaming, this report shall, inter alias, provide an assessment of the impact of the measures taken on women and men. In the light of the information received, this report shall include, if necessary, proposals to revise and update this Directive.

_Article 17  
Entry into force_

This Directive shall enter into force on the day of its publication in the Official Journal of the European Union.

  

_Article 18  
Addressees_

This Directive is addressed to the Member States.

Done at Brussels,

_ For the Council_

_ The President_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) See also doc. 12071/08.

[\[2\]](#_ftnref2) See also doc. DS 823/08.

[\[3\]](#_ftnref3) See doc. 14483/08.

[\[4\]](#_ftnref4) A summary appears in doc. 11531/08 ADD 2.

[\[5\]](#_ftnref5) [http://ec.europa.eu/employment\_social/fundamental\_rights/pdf/pubst/stud/mapstrand1_en.pdf](http://ec.europa.eu/employment_social/fundamental_rights/pdf/pubst/stud/mapstrand1_en.pdf)

[\[6\]](#_ftnref6) http://ec.europa.eu/employment\_social/fundamental\_rights/pdf/org/exstud_en.pdf

[\[7\]](#_ftnref7) See doc. DS 823/08.

[\[8\]](#_ftnref8) See doc. 11531/08, “Explanatory Memorandum”, p. 5.

[\[9\]](#_ftnref9) See doc. 14793/1/06 REV 1.

[\[10\]](#_ftnref10) Docs. 12892/08 REV 1 and 12892/08 ADD 1 REV 1.

[\[11\]](#_ftnref11) See doc. 11454/08.

[\[12\]](#_ftnref12) Responding to BE, the Commission representative confirmed that private actors such as service providers (as well as the Member States) could also engage in positive action, and that they could adopt such measures themselves if the national legal framework so provided.

[\[13\]](#_ftnref13) Responding to IE, which has sought assurances that certain national practices (e.g. cheap tickets for young persons) could continue under the Directive, the Commission representative explained that the Directive should not be used as a pretext for lowering the level of protection _as a whole_, and that the Member States would still be free to change their legislation after the Directive entered into force.

[\[14\]](#_ftnref14) IE entered a reservation on Article 7. This delegation suggested adding here: "as the Member States so determine and in accordance with the criteria laid down by their national law". DE, EL, IT and AT, similarly, favoured retaining a reference to the criteria laid down by national law (cf. Directives 2000/43/EC and 2000/78/EC).

[\[15\]](#_ftnref15) Responding to DE and AT, the Commission representative confirmed that a request from the person concerned was needed before associations, organisations or other legal entities could represent a complainant before a court. She also explained that in the Commission's view, such entities should be able to act either on behalf or in support of complainants. NL, for its part, considered that the text should not be modified in a way that would limit the ability of these entities to perform their work.

[\[16\]](#_ftnref16) IE suggested adding a new sentence at the end of the paragraph, as follows: "This is without prejudice to national rules of procedure concerning representation and defence before the courts." The Commission representative explained that Article 7(2) was not intended to lay down rules regarding representation in court, which were a matter of national procedural law.

[\[17\]](#_ftnref17) Citing potential difficulties, especially in the private sphere, CZ was unable to support this provision. NL and DE raised the question as to what kind of proof service providers accused of discrimination could present in their defence, DE also warning against increasing the administrative burden. IE and LU also questioned the rationale for reversing the burden of proof in the area of goods and services, as opposed to the employment sphere. IT entered a scrutiny reservation. Responding, the Commission representative explained that the imbalance of power that was characteristic of employment relationships was not the only reason for shifting the burden of proof, but issues relating to access to information were also relevant, as affirmed by the European Court of Justice. The Commission representative also recalled that the scope of Directive 2000/43/EC already extended beyond the employment sphere, and stated that experience from pioneering countries did not suggest that extending the protection against discrimination beyond the employment sphere necessarily led to a substantial increase in the number of court cases. IE and LU, however, remained concerned in this regard.

[\[18\]](#_ftnref18) Responding to AT and PT, the Commission representative confirmed that the wording used was identical to that used in Article 9 of Directive 2004/113/EC and Article 8 of Directive 2000/43/EC, with the exception that the present text refers explicitly to "proceedings in which the court or competent body _investigates_ the case" (as opposed to the theoretical formulation used previously "_it is for_ the court or other competent authority _to investigate_ the case").

[\[19\]](#_ftnref19) IE asked why the term "proceedings" was chosen, instead of "legal proceedings", the term used in Directives 2000/78/EC and 2004/113/EC. Responding, the Commission representative explained that no change in substance was intended.

[\[20\]](#_ftnref20) BG suggested replacing the words in square brackets with "including the relevant non-governmental organisations". EL suggested deleting the words in square brackets. Responding to AT, the Commission representative explained that the relevant stakeholders could include churches and religious organisations. She undertook to reflect on the possibility of deleting the words "in particular, non-governmental organisations".

[\[21\]](#_ftnref21) Alluding to its written suggestion set out in doc. DS 819/08, IE explained that the Paris Principles were not legally binding and should not be referred to in Recital 28; this delegation also pointed out that the Paris Principles contained prescriptive provisions regarding the independent _status_ (as opposed to the independent _function_) of national institutions for the promotion of human rights. Responding, the Commission representative explained that the intention was only to ensure that the Equality Bodies exercised their functions independently, and that the recitals did not contain rules but merely aimed to facilitate the interpretation of the Directive. Thus the cross-reference would help to clarify the fact that the Equality Body could be identical to the body foreseen in Article 33 of the UN Convention on the Rights of Persons with Disabilities. DK supported the suggestion by IE that the reference to the Paris Principles be deleted from Recital 28.

[\[22\]](#_ftnref22) Responding to NL, the Commission representative explained that the aim of this provision was to allow the Member States to decide for themselves on the institutional structure chosen. She also pointed out that certain countries were moving towards a system involving a single equality body with competence for all the discrimination grounds.

[\[23\]](#_ftnref23) BG suggested deleting the words in square brackets. Responding to AT, the Commission representative agreed that it would make sense for the equality bodies also to cover the remit of Directive 2000/78/EC (i.e. employment and occupation). AT also expressed the view that the wording of Directive 2004/113/EC should be followed here in respect of _the tasks_ of the equality bodies.

[\[24\]](#_ftnref24) DE affirmed the importance of proportionality and underlined the differences between the respective legal systems of the Member States. The Commission representative explained that the intention was to allow sufficient discretion for national legislators and judges, but that sanctions were required regardless of the type of discrimination (indirect or direct; intentional or inadvertent), in order to ensure effective protection.

[\[25\]](#_ftnref25) IE expressed the view that upper limits should be permissible here, as they also were in Directive 2000/43/EC. The Commission representative explained that if upper limits were set too _low,_ effective sanctions would not be ensured.

[\[26\]](#_ftnref26) BG, EL, LV, MT, PL, FI, SE and UK preferred a longer transposition period. Giving their tentative reactions, ES and IT also favoured a longer transposition period.

[\[27\]](#_ftnref27) BG, EL, ES, IT, MT, AT, PL and PT were unable to support the inclusion of an obligation to produce correlation tables in this Article. (See also Recital 31.) The Commission representative explained that such tables were an essential tool for the Commission and could also help to avoid unnecessary infringement proceedings.

[\[28\]](#_ftnref28) EL, LV, MT, UK considered that 4 years was too short. Giving their preliminary positions, NL and SE also considered that 4 years might not be enough. NL requested clarification as to how the length of the period had been chosen; the Commission representative pointed out that very few Member States had actually used the additional implementation period offered under Directive 2000/78/EC. PT entered a scrutiny reservation. This delegation raised the question as to which provisions in Article 4 qualified for the extended implementation period of 4 years. Following the meeting, the Commission representative clarified this issue to the effect that _the extended implementation period applied to Article 4(1)(a) ("effective non-discriminatory access") only_. The Commission representative suggested revisiting the implementation dates once the questions raised in connection with Article 4 had been clarified.

[\[29\]](#_ftnref29) EL entered a reservation. This delegation took the view that the Member States themselves, not the Equality Bodies, should furnish information to the Commission. The Commission representative undertook to reflect on the possibility of moving the mention of the Equality Bodies to the second paragraph of the Article.

[\[30\]](#_ftnref30) BG, EL, AT and SE wished to replace "social partners" with "relevant stakeholders". The Commission representative explained that the social partners had valuable information that was relevant to policies aimed at making it possible for people to work.





**