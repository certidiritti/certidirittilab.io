---
title: 'Bene i nuovi moduli d''iscrizione per le scuole comunali a Milano, ora avanti per favorire pari opportunità e integrazione sociale, culturale ed economica per tutti'
date: Fri, 07 Feb 2014 16:05:21 +0000
draft: false
tags: [Politica]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti

Milano, 7 febbraio 2014

Dal prossimo 14 febbraio il Comune di Milano farà un altro piccolo passo contro le discriminazioni e per una burocrazia dal volto più umano: come già avviene per la modulistica d'iscrizione gli asili statali, nei moduli di iscrizione alle scuole d'infanzia comunali di Milano i genitori non dovranno più specificare se sono il padre o la madre del bambino che vogliono iscrivere.

Il Regolamento per il riconoscimento delle Unioni Civili prevede che «Il Comune provvede, attraverso singoli atti e disposizioni degli Assessorati e degli Uffici competenti, a tutelare e sostenere le unioni civili, al fine di superare situazioni di discriminazione, favorire pari opportunità, integrazione e lo sviluppo nel contesto sociale, culturale ed economico del territorio» e che «Gli atti dell'Amministrazione devono prevedere per le unioni civili pari condizioni di accesso ai servizi».

I nuovi moduli sono pertanto atti amministrativi che attuano semplicemente il regolamento e non necessitano quindi di alcun passaggio in Consiglio comunale.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: «apprendiamo con piacere la decisone di utilizzare nuovi moduli di iscrizione alle scuole d'infanzia da parte dell'amministrazione comunale. È un piccolo, ma significativo passo avanti nella giusta direzione. Adesso si prosegua ad attuare il Regolamento per il riconoscimento delle Unioni Civili con altri atti che favoriscano pari opportunità, integrazione e sviluppo nel contesto sociale, culturale ed economico del territorio milanese anche per le coppie unite civilmente, ma soprattutto si dia piena attuazione alla Delibera d'iniziativa popolare in materia di prevenzione, contrasto e assistenza alle vittime di discriminazione e per le pari opportunità per tutti».