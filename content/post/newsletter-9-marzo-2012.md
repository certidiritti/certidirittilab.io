---
title: 'Newsletter 9 Marzo 2012'
date: Sun, 25 Mar 2012 09:44:39 +0000
draft: false
tags: [Politica]
---

![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)

L'Associazione radicale Certi Diritti, ha lanciato [anche a Milano una raccolta firme su due proposte di delibera di iniziativa popolare](a-milano-raccolta-firme-su-due-proposte-di-delibera-di-iniziativa-popolare-per-registro-unioni-civili-e-pari-opportunita-per-tutti):  la prima per l’istituzione di un registro delle Unioni Civili, la seconda  perché vengano date pari opportunità a tutti, contrastare e prevenire le forme di discriminazione basate sul genere, la disabilità, l'orientamento sessuale, l'età, l'etnia e il credo religioso. **Milano si prepara ad accogliere Benedetto XVI  con una spesa di 3,1 milioni di euro. **  
Noi lo accoglieremo con la raccolta firme per chiedere il riconoscimento dei diritti di tutti i cittadini.

Continua a Roma l’analoga iniziativa. Gli aggiornamenti su [Teniamo Famiglia](http://teniamofamiglia.blogspot.com/)  
Il segretario dell’Associazione, Yuri Guaiana, sta percorrendo l’Italia per ricordare che la Corte Costituzionale ha sollecitato inutilmente il Parlamento (il 14 aprile saranno due anni) a riconoscere le unioni gay.** "I diritti LGBT(E) in Italia dopo la sentenza 138/10"  **è il dibattito tenuto a  Roma in cui è stato presentato il libro _["Dal cuore delle coppie al cuore del Diritto”, che puoi acquistare sul nostro sito.](e-uscito-il-libro-dal-cuore-delle-coppie-al-cuore-del-diritto-ludienza-138/2010-alla-corte-costituzionale-per-il-diritto-al-matrimonio-tra-persone-dello-stesso-sesso)  
_[Su radio Radicale il video >](http://www.radioradicale.it/scheda/346956)  Dopo Perugia, Ancona e Roma Yuri Guaiana sarà a Vicenza il 16 marzo e a Torino il  21 marzo alle ore 21,00 per l’incontro Fare famiglia senza matrimonio.

**Legge omofoba contro la libertà di espressione a San Pietroburgo  
**L’appello delle associazioni lgbt :[“stop ai gemellaggi: l'omofobia uccide la bellezza”](associazioni-lgbt-boicottiamo-san-pietroburgo-lomofobia-uccide-la-bellezza).[Il testo del disegno di legge >](traduzione-in-italiano-del-disegno-di-legge-contro-la-propaganda-gay-che-potrebbe-diventare-legge-se-firmata-dal-governatore-di-san-pietroburgo)   
[Firma l’appello per chiede al governatore di San Pietroburgo di non firmare la legge >](fermiamo-la-legge-omofoba-di-san-pietroburgo-firma-lappello-di-amnesty)  
**  
Onu: **il Segretario generale Ban Ki-moon alle persone lgbt: [ogni attacco contro di voi è un attacco ai valori universali >](segretario-generale-dellonu-in-difesa-delle-persone-lgbt-non-siete-soli-ogni-attacco-contro-di-voi-e-un-attacco-ai-valori-universali)

**Regno Unito: **[Gay tanzaniano ottiene asilo](http://www.tellusfolio.it/index.php?prec=%2Findex.php&cmd=v&id=14134) dopo campagna internazionale sostenuta anche dal Partito  Radicale e dall’Associazione radicale Certi Diritti

**Lucio Dalla:** non ci interessa dibattito su sua omosessualità ma  [non accettiamo offese da parte di uomini di chiesa >](teologo-per-difendere-lucio-dalla-denigra-persone-gay-non-accettiamo-offese-dalla-congrega-che-ha-sterminato-per-secoli-cittadini-diversi)

**Trieste:** dopo l’interrogazione parlamentare sulla sospensione degli interventi chirurgici per il cambio di sesso, presentata da Rita Bernardini,  [interviene il Direttore generale dell’Ospedale.](http://www.radicalifvg.org/wp/2012/02/20/cambio-di-sesso-si-fara-solo-fuori-orario/)

**Roma: **Certi Diritti ospite al [Roma Comics and Games 2012: Fumetti, Musica e Cosplay](http://www.net1news.org/roma-comics-and-games-2012-fumetti-musica-e-cosplay.html "Roma Comics and Games 2012: Fumetti, Musica e Cosplay")  
**  
News  
**[Coppia gay: ci sposiamo a Oslo. L’intervista >](http://www.ilrestodelcarlino.it/rimini/cronaca/2012/03/04/675923-coppia-gay-oslo-rimini.shtml)

[Milano: coppie di fatto, passo indietro sull' iter della delibera >](http://archiviostorico.corriere.it/2012/marzo/01/Coppie_fatto_passo_indietro_sull_co_7_120301002.shtml)

[La dignità e la giustizia cancellate dall´ipocrisia ](http://www.dirittiglobali.it/index.php?view=article&catid=28:diritti-umani-a-discriminazioni&id=29267:la-dignita-e-la-giustizia-cancellate-dallaipocrisia-&format=pdf&ml=2&mlt=yoo_explorer&tmpl=component) L´ultimo è il Maryland. Sono già otto gli Stati americani che hanno riconosciuto il matrimonio tra persone dello stesso sesso. In Italia vince l’ipocrisia.

[Tavarnelle Val di Pesa, quando i diritti contano](http://met.provincia.fi.it/news.aspx?n=111642): edilizia convenzionata giovane e anche gay >

[La battaglia dei gay per il diritto alla felicità >](http://www.repubblica.it/esteri/2012/03/07/news/matrimonio_gay-31078622/)

[USA: i nonni che combattono per i matrimoni gay >](http://www.giornalettismo.com/archives/207459/i-nonni-che-combattono-per-i-matrimoni-gay/?utm_source=twitterfeed&utm_medium=twitter)

Ricevi questa newsletter perché almeno una volta hai sostenuto l’associazione radicale Certi Diritti.

**Se non hai ancora rinnovato l’iscrizione puoi farlo alle poste, tramite bonifico bancario o paypal. [Non riceviamo finanziamenti pubblici e solo l’autofinanziamento degli iscritti ci permette di realizzare azioni politiche per i diritti civili e umani >](iscriviti)**

Ci scusiamo con gli iscritti per il ritardo sulla tessera 2012. Arriverà molto presto!  
Per consigli, critiche e segnalazioni puoi scriverci a [info@certidiritti.it](mailto:<a href=)" target="_blank" style="color: rgb(255, 102, 0); ">[info@certidiritti.it](mailto:info@certidiritti.it)

Puoi seguirci su [Facebook](http://www.facebook.com/certidiritti), [Twitter](https://twitter.com/#!/certidiritti), [Flickr](http://www.flickr.com/photos/certidiritti) e [Youtube](http://www.youtube.com/certidiritti)

  
[www.certidiritti.it](http://www.certidiritti.it/)