---
title: 'Corte di Giustizia Europea: stessi diritti a chi contrae un''unione civile o si sposa con partner dello stesso sesso'
date: Sat, 14 Dec 2013 23:14:54 +0000
draft: false
tags: [Diritto di Famiglia]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 13 dicembre 2013

L'Associazione Radicale Certi Diritti accoglie con grande soddisfazione la sentenza di ieri della Corte di Giustizia Europea per la quale il rifiuto di accordare un congedo matrimoniale anche alle coppie dello stesso sesso che contraggono un'unione civile costituisce una discriminazione diretta basata sull'orientamento sessuale.

Il caso è stato portato davanti alla Corte dalla Francia in relazione a un lavoratore del Credito Agricolo che si è visto negare il congedo poiché esso sarebbe stato riservato al matrimonio e non anche al PACS, allora unica possibilità aperta alle persone dello stesso sesso. Benché il contratto di lavoro collettivo parli solo di matrimonio, per la Corte Europea, il congedo e i relativi benefici devono essere garantiti anche alle coppie dello stesso sesso che contraggono un'unione civile se, e solo se, questo è l'unico istituto aperto alle persone omosessuali.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: "Mentre l'Europa va avanti nel affermare e garantire il diritto di eguaglianza per tutti i suoi cittadini, l'Italia rimane ferma al Medioevo ed espunge dal dibattito pubblico questi temi fondamentali. La sentenza della Corte dovrà ora essere applicata non solo nella giurisdizione francese, ma anche in quella degli altri Stati membri che hanno una situazione simile. L'Italia per ora è esclusa, non avendo nemmeno un istituto simile al PACS francese. Se per la Corte Europea è discriminazione diretta, e non solo indiretta, privare i cittadini omosessuali che contraggono un'unione civile del congedo matrimoniale, non avere il matrimonio egualitario, ma nemmeno un istituto che regolamenti le unioni civili è da considerarsi ormai veramente al di fuori della civiltà europea".