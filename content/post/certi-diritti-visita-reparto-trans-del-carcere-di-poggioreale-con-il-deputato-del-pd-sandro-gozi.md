---
title: 'Certi Diritti visita reparto trans del carcere di Poggioreale con il deputato del PD Sandro Gozi'
date: Thu, 04 Apr 2013 21:06:55 +0000
draft: false
tags: [Transessualità]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 4 aprile 2013

In occasione del VI congresso dell'**Associazione Radicale Certi Diritti** che si terrà **[a Napoli dal 5 al 7 aprile 2013](http://www.comune.napoli.it/flex/cm/pages/ServeBLOB.php/L/IT/IDPagina/21219)**, una delegazione guidata dall'on. Sandro Gozi e composta dal segretario dell'Associazione Radicale Certi Diritti, Yuri Guaiana, e dal tesoriere dell'Associazione Radicale per la Grande Napoli, Roberto Gaudioso, visiterà il carcere di Poggioreale il 5 aprile alle 10. Alle 12.15, alla fine della visita, di fronte al carcere, è indetta una conferenza stampa nella quale la delegazione renderà noto l'esito dell'ispezione.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: "La situazione carceraria italiana è drammatica, come dimostrano le numerose sentenze di condanna della CEDU e come noi radicali, soprattutto Marco Pannella e Rita Bernardini, denunciamo inascoltati da troppo tempo dentro e fuori le istituzioni. Particolarmente svantaggiate sono le persone transessuali, spesso incarcerate per reati minori, che spesso non possono continuare le cure ormonali, vengono tenute in isolamento per le difficoltà di collocamento o assegnate a reparti secondo il genere indicato sui documenti che non coincide con quello effettivo. Poche in Italia sono le strutture penitenziarie con reparti appositi, tra queste il carcere di Poggioreale. Intendiamo verificare le condizioni di questo reparto, ma non solo. Molte altre sono infatti le questioni carcerarie che afferiscono direttamente ai temi dell'Associazione Radicale Certi Diritti: i dati e le condizioni delle persone omosessuali detenute, la violenza sessuale, il tabù della distribuzione di preservativi, il diritto all'effettività dei detenuti. Ringrazio molto l'on. Sandro Gozi che ci con questa visita onora il suo mandato parlamentare".