---
title: 'PRESIDENTE DI GAYLIB: GRAZIE A CERTI DIRITTI E RETE LENFORD SI VA ALLA CONSULTA'
date: Mon, 17 Aug 2009 09:37:48 +0000
draft: false
tags: [Comunicati stampa]
---

**Coppie gay di Trento all’Alta Corte: Arcigay non c’entra nulla**

**Roma, 17 agosto 2009**

**Riceviamo dal Presidente di GayLib:**

Leggendo i toni entusiastici della lettera a “Il Trentino” con i quali il presidente di Arcigay Trento, Stefano Co’ esprime la sua soddisfazione circa la sentenza della Corte d’Appello in merito al “Matrimonio gay”, trovo doveroso mettere un “altolà” e forse togliermi qualche sassolino dalla scarpa.

Non nei confronti di Stefano Co’, si intende, il quale da anni regge Arcigay Trento superando anche momenti difficili, ma più in generale dell’associazionismo omosessuale di sinistra che fin dagli antichi tempi aveva rifiutato la via del matrimonio in quanto già quello eterosessuale sarebbe stato un istituto “borghese” e soprattutto fallimentare: se il matrimonio fra uomo e donna è in crisi, perché scimmiottarlo noi gay?

Tant’è che la politica di Arcigay si è sempre caratterizzata per la lotta al conseguimento di nuovi istituti giuridici paralleli, forse un po’ camuffati e comunque di serie “B”, dalle Unioni Civili al PACS alla francese, dalla Parnership all’inglese fino poi a sfociare nella farsa dei DiCo, dove, trattandosi di diritti e doveri dei singoli conviventi, spariva la coppia e ci si metteva insieme con una raccomandata; la filosofia politica di Arcigay è stata volta a ottenere il riconoscimento dei diritti civili, o meglio, individuali in quanto coppia di fatto… come due nonnine che vivono insieme, ci dissero. Ma possiamo il e il mio compagno che abbiamo comprato casa insieme e che ci amiamo nei momenti facili e difficili, avere i diritti di due nonnine che vivono insieme?

Ed è inutile negare che Arcigay abbia sempre rifiutato a che le coppie di persone dello stesso sesso potessero essere considerate famiglia, poiché nella strategia associativa era divenuto di primaria importanza non dar vita a elementi di frattura nei confronti dei propri partiti di riferimento, correndo di conseguenza la possibilità di perderne l’appoggio politico (e quale seggiola qua e là). Inoltre si voleva togliere agli oppositori del mondo cattolico e di destra quell’arma che voleva i gay distruttori della famiglia tradizionale (vedi Giuliana), come se i nuclei famigliari italiani fossero in crisi per ciò che fanno i gay sotto le lenzuola e non per l’alto costo della vita, il caro-alloggi, il fatto che per mandare il figlio all’università necessiti un solido mutuo in banca.

Poi vi fu la sventola sonora di Zapatero, che con il matrimonio gay in Spagna aveva dato una lezione di stile e di laicità ai colleghi di tutta Europa, mentre noi ci consolavamo con Prodi, Rutelli e la Binetti: Arcigay ne uscì frastornata, ma ancora non abbracciava la strategia del matrimonio gay. E non lo fece neppure in occasione di un nostro recente incontro con il Presidente della Camera Gianfranco Fini, dove, se pur l’argomento era l’omofobia, gli esponenti di Arcigay non seppero sfruttare l’occasione di patrocinare il matrimonio, arrivando addirittura a mostrarsi contrariati al mio intervento in questo senso.

Eppure si tratta dell’uovo di Colombo: nessun articolo della Costituzione della Repubblica specifica che il matrimonio civile deve debba essere esclusivamente fra uomo e donna.

GayLib, qualche singolo esponente del movimento gay e l’associazione radicale Certi Diritti hanno invece sempre creduto nell’allargamento del matrimonio in base al principio dell’uguaglianza fra i cittadini, tant’che fu quest’ultima a promuovere in accordo con la rete di giuristi “Landford” l’iniziativa che ha poi portato alla sentenza della Corte d’Appello di Trento ed al suo rinvio alla Corte Costituzionale; già in occasione del Congresso di “Certi Diritti”, tenutosi in marzo a Bologna, venne denunciato l’assoluto boicottaggio dell’azione di Certi Diritti e della Rete Landford da parte di Arcigay, che forte dei suoi 170.000 iscritti non ha sostenuto minimamente l’iniziativa e non ha invitato nessuna coppia gay a seguire l’esempio delle due che oggi a Trento, grazie all’avvocato Schuster, portano il loro grido di libertà e di giustizia all’Alta Corte.

Enrico Oliari

Presidente nazionale GayLib

335.6622440

[www.gaylib.it](http://www.gaylib.it/)

[>www.notiziegay.it](http://www.notiziegay.it/)