---
title: 'Seminario dell''Associazione radicale Certi Diritti'
date: Sat, 23 Jul 2011 10:32:00 +0000
draft: false
tags: [Politica]
---

Dal 15 al 17 luglio 2011 a Dorio (Lecco) si è tenuta una riunione di direttivo dell'Associazione Radicale Certi Diritti. La riunione con taglio da vero e proprio seminario, ha avuto lo scopo du organizzare e rilanciare le iniziative dell'Associazione: la continuazione della campagna di affermazione civile, la riorganizzazione della comunicazione, il congresso di fine anno e altro ancora. Per informazioni su tutte le attività dell'Associazione Certi Diritti basta collegarsi al sito www.certidiritti.it Nelle interviste del video potrete ascoltare un riassunto del seminario attraverso le voci di Enzo Cucco, Yuri Guaiana, Gianmario Felicetti e Sergio Rovasio.

[http://www.tvradicale.it/](http://www.tvradicale.it/)