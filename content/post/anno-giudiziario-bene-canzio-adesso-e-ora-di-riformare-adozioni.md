---
title: 'Anno giudiziario: bene Canzio. Adesso è ora di riformare adozioni'
date: Thu, 26 Jan 2017 12:16:47 +0000
draft: false
tags: [Diritto di Famiglia]
---

![1996645_1057548_20150526_cassazione](http://www.certidiritti.org/wp-content/uploads/2017/01/1996645_1057548_20150526_cassazione-300x168.jpg)"A stravolgere la realtà è la stasi di un legislatore che continua a negare l'esistenza di famiglie in carne ed ossa che chiedono diritti. Il presidente Giovanni Canzio non ha fatto altro che ribadire la volontà della Corte di Cassazione di continuare a tutelare l'interesse fondamentale del minore sulla scia dei pronunciamenti positivi su adozioni del figlio del partner dello stesso sesso cui abbiamo assistito negli ultimi tempi. Il suo appello ad una legge dello Stato che regolamenti la materia è un gesto di sensibilità da parte del primo presidente e, allo stesso tempo, l'ennesimo segnale che ci troviamo a che fare con un Parlamento oramai rassegnato ad essere il bersaglio delle corti, che certificano a colpi di sentenze l'inadeguatezza delle Camere a creare soluzioni ai problemi che toccano il cuore della vita dei cittadini" Così Leonardo Monaco segretario dell'Associazione Radicale Certi Diritti.