---
title: 'Fatto il Governo, subito in consiglio i decreti sulle unioni civili'
date: Wed, 14 Dec 2016 15:26:54 +0000
draft: false
tags: [Diritto di Famiglia]
---

![gentiloni-755x515](http://www.certidiritti.org/wp-content/uploads/2016/12/gentiloni-755x515-300x205.jpg)"Con la fiducia incassata al Senato, adesso, il Governo Gentiloni è pronto ad affrontare gli adempimenti ereditati dal precedente esecutivo. Tra questi sollecitiamo la rapida emanazione dei decreti attuativi sulle unioni civili, che attendono di essere licenziati da Palazzo Chigi per dare piena effettività alla legge sulle unioni civili. All'ambiguità di un ordinamento che prevede isituti diversi per etero ed omosessuali non può sommarsi la precarietà dovuta all'essere arrivati a fine anno con la proroga dei decreti ponte sulla legge 76/2016." Così Leonardo Monaco, segretario dell'Associazione Radicale Certi Diritti. Commentando la composizione della nuova squadra di governo, conclude: "a fronte della soddisfazione per il nome di Valeria Fedeli all'Istruzione, motivata dal suo lungo impegno per rendere le scuole luoghi di tolleranza e comprensione delle differenze, speriamo che la politica estera italiana possa continuare e migliorare l'approccio sul fronte diritti umani, consapevoli che ciò richiederà l'attenta vigilanza del capo del Governo"