---
title: 'Legge 14 aprile 1982, n. 164'
date: Mon, 21 Jun 2010 06:00:56 +0000
draft: false
tags: [Senza categoria]
---

(in Gazz. Uff., 19 aprile, n. 106).  
**Norme in materia di rettificazione di attribuzione di sesso.  
  
_Art. 1_**

La rettificazione di cui all'art. 454 del codice civile si fa anche in forza di sentenza del tribunale passata in giudicato che attribuisca ad una persona sesso diverso da quello enunciato nell'atto di nascita a seguito di intervenute modificazioni dei suoi caratteri sessuali.  
  

**_Art. 2_**

La domanda di rettificazione di attribuzione di sesso di cui all'art. 1 è proposta con ricorso al tribunale del luogo dove ha residenza l'attore.  
Il presidente del tribunale designa il giudice istruttore e fissa con decreto la data per la trattazione del ricorso e il termine per la notificazione al coniuge e ai figli.  
Al giudizio partecipa il pubblico ministero ai sensi dell'art. 70 del codice di procedura civile.  
Quando è necessario, il giudice istruttore dispone con ordinanza l'acquisizione di consulenza intesa ad accertare le condizioni psico-sessuali dell'interessato.  
Con la sentenza che accoglie la domanda di rettificazione di attribuzione di sesso il tribunale ordina all'ufficiale di stato civile del comune dove fu compilato l'atto di nascita di effettuare la rettificazione nel relativo registro.  
  

**_Art. 3_**

Il tribunale, quando risulta necessario un adeguamento dei caratteri sessuali da realizzare mediante trattamento medico-chirurgico, lo autorizza con sentenza.  
In tal caso il tribunale, accertata la effettuazione del trattamento autorizzato, dispone la rettificazione in camera di consiglio.  
  

**_Art. 4_**

La sentenza di rettificazione di attribuzione di sesso non ha effetto retroattivo. Essa provoca lo scioglimento del matrimonio o la cessazione degli effetti civili conseguenti alla trascrizione del matrimonio celebrato con rito religioso. Si applicano le disposizioni del codice civile e della legge 1° dicembre 1970, n. 898, e successive modificazioni.  
  

**_Art. 5_**

Le attestazioni di stato civile riferite a persona della quale sia stata giudizialmente rettificata l'attribuzione di sesso sono rilasciate con la sola indicazione del nuovo sesso e nome.  
  

**_Art. 6_**

Nel caso che alla data di entrata in vigore della presente legge l'attore si sia già sottoposto a trattamento medico-chirurgico di adeguamento del sesso, il ricorso di cui al primo comma dell'art. 2 deve essere proposto entro il termine di un anno dalla data suddetta.  
Si applica la procedura di cui al secondo comma dell'art. 3.  
  

**_Art. 7_**

L'accoglimento della domanda di rettificazione di attribuzione di sesso estingue i reati cui abbia eventualmente dato luogo il trattamento medico-chirurgico di cui all'articolo precedente.  
  

_La presente legge, munita del sigillo dello Stato, sarà inserita nella Raccolta ufficiale delle leggi e dei decreti della Repubblica italiana. E` fatto obbligo a chiunque spetti di osservarla e di farla osservare come legge dello Stato._

_Data a Ventimiglia, addì 14 aprile 1982_

_PERTINI_

_Spadolini - Darida - Rognoni_

_Visto, il Guardasigilli: darida_