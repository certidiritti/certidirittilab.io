---
title: 'Bergoglio/Stefanini. Certi Diritti: Inaccettabile l''atteggiamento del Vaticano'
date: Fri, 24 Apr 2015 13:36:49 +0000
draft: false
tags: [Politica]
---

[![vaticano_piovra](http://www.certidiritti.org/wp-content/uploads/2015/04/vaticano_piovra.png)](http://www.certidiritti.org/wp-content/uploads/2015/04/vaticano_piovra.png)«Con il no a Laurent Stefanini, cattolico praticante oltre che gay, Papa Francesco compie una pura e semplice discriminazione basata sull’orientamento sessuale del diplomatico nominato dall’Eliseo ambasciatore di Francia presso la Santa Sede». Lo dichiara Yuri Guaiana, segretario dell’Associazione Radicale Certi Dirtti, che continua: «La scusa della legge sul matrimonio egualitario in vigore in Francia non regge poiché allora la Santa Sede dovrebbe rifiutare gli ambasciatori di ben 21 Paesi, dall’Argentina alla Nuova Zelanda. dal Canada alla cattolicissima Spagna e, forse, a breve anche di Irlanda e Stati Uniti». Guaiana conclude: «Se il Vaticano dovesse continuare a rifiutare Stefanini, la Francia dovrebbe rifiutarsi di nominare un altro ambasciatore e sospendere le relazioni diplomatiche con la Santa Sede».

Comunicato Stampa dell’Associazione Radicale Certi Diritti