---
title: 'Rosario Crocetta e la prova del nove'
date: Tue, 30 Oct 2012 08:24:28 +0000
draft: false
tags: [Politica]
---

Perché la notizia dell’elezione di Rosario Crocetta è importante?  
Di Enzo Cucco, Presidente Associazione Radicale Certi Diritti.  
  
30 ottobre 2012

Perché la notizia dell’elezione di Rosario Crocetta è importante?  
Perché è la prova del nove di quello che l’elezione di Nichi Vendola ha rappresentato: non una eccezione, bensì il segno che l’Italia, a dispetto della sua classe dirigente (tutta e non solo quella che ha residenza di là dal Tevere) non si fa impressionare più di tanto di fronte alla visibilità dell’orientamento sessuale dei candidati. E questa è una buona notizia. Una di quelle notizie che ci aiutano a credere che anche il nostro Paese ha un futuro normale, e non quello che appare dalle sgangherate cronache che leggiamo quotidianamente.

Ma oltre ad essere un’ottima prova del nove è anche una buona notizia per i siciliani e le siciliane? Lo vedremo nei prossimi mesi, dopo che il gioco delle alleanze prossime venture avranno prodotto governo e scelte politiche concrete.

Certo, l’alleanza con un Partito, l’UDC, che in Parlamento per affossare una proposta di legge contro l’omofobia non trovò niente di meglio che costruire una criminale similitudine tra orientamento omosessuale e zoofilia (in seguito portando l’autore di tanta raffinata cultura giuridica nientepopodimeno che al vertice del Consiglio Superiore della Magistratura) non lascia sperare nulla di rivoluzionario. Almeno sui temi dei diritti umani e della loro protezione in un paese come l’Italia che fa ancora una maledetta fatica a considerare i diritti delle persone omosessuali come diritti umani, appunto, e non come capriccioso epifenomeno modernista.

Le Regioni, soprattutto quelle a statuto speciale, hanno competenze e mezzi che sono decisivi per uno sviluppo inclusivo e davvero orientato alla lotta contro ogni forma di discriminazione. La scelta di Rosario Crocetta di non fare della propria omosessualità una bandiera della candidatura sgombra il campo da quelle aspettative che non sempre i politici omosessuali e transessuali che hanno ricoperto incarichi elettivi hanno saputo corrispondere.

Ora siamo tutti più liberi: lui di interpretare al meglio le richieste di uguaglianza e tutti noi nel valutare le sue scelte per quello che saranno. Al di là della retorica e dei buoni sentimenti.

Proprio come in un Paese normale dovrebbe capitare sempre, con tutti.

Buon lavoro Presidente Crocetta!

**Enzo Cucco**  
Presidente Associazione Radicale Certi Diritti

[http://gayindependent.blogspot.it/](http://gayindependent.blogspot.it/)