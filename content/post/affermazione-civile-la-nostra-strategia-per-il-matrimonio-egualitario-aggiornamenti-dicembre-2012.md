---
title: 'Affermazione civile: la nostra strategia per il matrimonio egualitario. Aggiornamenti dicembre 2012'
date: Fri, 07 Dec 2012 13:34:48 +0000
draft: false
tags: [Affermazione Civile]
---

Tutti gli aggiornamenti sulla campagna di Affermazione Civile iniziata nel 2008 per il riconoscimento anche in Italia del matrimonio egualitario.

Dopo anni di rodaggio, possiamo dire che le campagne giuridiche avviate in Italia per raggiungere obiettivi di uguaglianza stanno avendo gli effetti previsti, nello stato di diritto e anche nella vita sociale e politica del paese. Questo è stato possibile principalmente per un motivo: quello giuridico, è un processo che fino ad oggi si è mostrato essere graduale, ma che nondimeno è sempre stato percorso consolidando senza compromessi il principio di uguaglianza: il solo che può garantire il pieno riconoscimento della dignità di ogni cittadino davanti alla legge e alla società.  
  
Ma procediamo con ordine. La sentenza 138/2010 della Corte costituzionale, resta l’esito più importante, ed è il risultato della fase iniziale di Affermazione Civile, condotta da Certi Diritti insieme agli avvocati di Rete Lenford. Sono tutte le sentenze che si sono susseguite nel 2012 ad averla confermata una pietra miliare e un punto di riferimento solido. Specialmente quando afferma “la necessità di un trattamento omogeneo tra la condizione della coppia coniugata e quella della coppia omosessuale, trattamento che questa Corte può garantire con il controllo di ragionevolezza”.  
  
E così, il Tribunale di Reggio Emilia cita questa sentenza nell’ordinare alla prefettura il rilascio del permesso di soggiorno (previsto per i coniugi di cittadini di  Paesi membri dell’Unione europea) ad una coppia dello stesso sesso italo-uruguayana sposata in Spagna (sentenza del 13/02/2012).  
  
La Cassazione, allo stesso modo, cita la stessa sentenza per riaffermare che “I componenti della coppia omosessuale, conviventi in stabile relazione di fatto \[…\] possono adire i giudici comuni per far valere, in presenza di specifiche situazioni, il diritto ad un trattamento omogeneo a quello assicurato dalla legge alla coppia coniugata.” (sentenza 4184 del 15 Marzo 2012).  
  
Infine, la Corte di Appello di Milano, cita entrambe le sentenze della Corte costituzionale e della Cassazione per affermare che la cassa mutua di categoria non può negare il diritto alle prestazioni assistenziali alla coppia dello stesso sesso stabilmente convivente. (sentenza 7176 del 31/08/2012)  
  
Anche la politica segna il passo e ormai, pure continuando a riproporre il falso principio secondo il quale i matrimoni gay non sarebbero costituzionali, non riesce più a darsi credibilità agli occhi di una opinione pubblica che è sempre meno disposta a subire acriticamente le istanze elettoralistiche della discriminazione omofobica.  
  
**AFFERMAZIONE CIVILE – LA NOSTRA STATEGIA**  
E’ in questo contesto che si focalizzano i nuovi obiettivi dell’Associazione Radicale Certi Diritti, all’interno della campagna di Affermazione Civile.  
Da un lato, sarà importante continuare a fare in modo che le Corti si esprimano sugli specifici diritti per i quali le coppie dello stesso sesso stabilmente conviventi abbiano diritto ad un trattamento uguale a quello delle coppie sposate: la pensione, il lavoro, le tasse, la salute ecc... Su tutti questi diritti dovrà essere vagliato e spiegato un principio di ragionevolezza che rafforzerà inevitabilmente lo stato di diritto di tutte le coppie dello stesso sesso.  
  
In secondo luogo, occorrerà scardinare i (sempre più fragili) paradigmi discriminatori che ancora resistono nei giudizi delle corti. In riferimento alla sentenza della Cassazione, noi crediamo fermamente che le persone sposate all’estero abbiano il diritto di vedere trascritto in Italia il proprio matrimonio contratto all’estero, e di vedersi riconosciuto lo stato civile di “Coniugato” o “Coniugata”. Anche per questo, continueremo a batterci (in questo caso fuori dalle aule dei tribunali) anche affinché venga ritirata la circolare Amato che vieta la trascrizione dei matrimoni contratti all’estero da persone dello stesso sesso.  
  
In riferimento alla sentenza della Corte costituzionale, noi crediamo fermamente che vietare a due persone di sposarsi solo perché sono dello stesso sesso sia un atto discriminatorio. E siamo soprattutto convinti che sia possibile arrivare ad una evidenza giuridica di queste nostre convinzioni.  
  
Forse, cosa più importante di tutte, crediamo che i traguardi raggiunti e i metodi impiegati in questa campagna giudiziaria che chiamiamo “Affermazione Civile” costituisce un vero tesoro di civiltà al quale la politica, volente o nolente non può non attingere e un faro di giustizia civile e sociale verso il quale anche le posizioni più omofobe e resistenti non possono non orientarsi se vogliono evitare il suicidio.  
  
**UN CASO PARTICOLARE - LA LIBERA CIRCOLAZIONE E LA CARTA DI SOGGIORNO**  
In tutto questo, meritano un approfondimento particolare gli sviluppi attuali a seguito della sentenza del Tribunale di Reggio Emilia sulla libera circolazione.  
Prima di tutto questa sentenza è importante perché è la prima sentenza definitiva che è stata tecnicamente vinta sul tema delle discriminazioni delle famiglie LGBT. Inoltre, a seguito della sentenza, per la prima volta le istituzioni dello Stato hanno rilasciato un documento ufficiale che riconosce l’esistenza di una famiglia composta da due persone dello stesso sesso.  
  
I passi successivi a questa vittoria sono quelli di consolidare la sentenza in modo tale che i principi espressi dal giudici possano essere, quanto prima possibile, fruibili non necessariamente attraverso un ricorso al giudice (prassi certamente eccezionale e inadeguata ad un diritto) ma semplicemente attraverso procedure di ordinaria amministrazione.  
Fortunatamente, dopo la sentenza, siamo stati contattati da tantissime coppie dello stesso sesso (e anche eterosessuali!) che adesso sono maggiormente in grado di riconoscere quei casi in cui il rifiuto di un permesso di soggiorno può essere una discriminazione.  
Per questo ci è stato possibile aiutare queste coppie a formulare richieste di permessi di soggiorno in varie Questure italiane, valorizzando anche quelle coppie che si sono costituite in modo leggermente differente dalla coppia pilota: magari perché hanno contratto un matrimonio fuori dall’UE, o perché si sono uniti tramite una Unione Civile ecc...  
E i primi risultati già ci sono stati: ci sono Questure, come quella di Milano, Roma, Treviso, Rimini  che – a seguito della sentenza – hanno rilasciato i permessi di soggiorno a coppie dello stesso sesso, senza rendere necessario il ricorso al giudice.  
  
Il Ministero dell’Interno ha anche emanato una Circolare il 26 ottobre 2012 in cui, facendo riferimento alla Sentenza della Corte Costituzionale 138/10 e all’ordinanza del Tribunale di Reggio Emilia, legittima il rilascio della Carta di soggiorno per familiari di cittadini comunitari al componente extra EU di famiglie dello stesso sesso. E’ questo un importante riconoscimento sebbene soltanto ai fini del soggiorno regolare in italia.  
  
**PER FINIRE**  
C’è un’ultima cosa da dire, forse la più importante: per poter fare tutto questo c’è stato bisogno, e ce ne sarà bisogno sempre di più, dell’ingrediente più importante: coppie che siano consapevoli dei propri diritti.    
Ne è la prova il fatto che due delle sentenze del 2012 non sono avvenute per l’impegno diretto di una associazione LGBT, quanto per l’iniziativa autonoma di cittadini, coppie capaci di vedersi protagoniste del percorso che è necessario per vedere riconosciuti i diritti di cui la legge priva una parte dei suoi cittadini.

**[SOSTIENI CERTI DIRITTI >>>](partecipa/iscriviti)**