---
title: 'Gay/ Matrimonio, Mancuso: Sentenza potrebbe slittare al 12 aprile - Virgilio'
date: Wed, 24 Mar 2010 11:07:57 +0000
draft: false
tags: [Senza categoria]
---

[Virgilio](http://notizie.virgilio.it/notizie/cronaca/2010/03_marzo/23/gay_matrimonio_mancuso_sentenza_potrebbe_slittare_al_12_aprile,23519182.html)

### "Milioni di persone attendono questo pronunciamento"

Roma, 23 mar. (Apcom) - La Corte Costituzionale rimanderebbe alla prossima seduta del 12 aprile il pronunciamento sul matrimonio gay. Lo afferma Aurelio Mancuso, citando fonti bene informate, sottolineando come "non si può nascondere che in tutta Italia migliaia di persone gay e lesbiche trattengono il fiato, facendosi attraversare da contrastanti sentimenti: dal pessimismo più nero ad una positiva fiducia".

"Questa indiscrezione aumenta la tensione - afferma l'ex presidente di Arcigay -. Bisogna rendersi conto che milioni di italiane e italiani attendono questo pronunciamento con personale partecipazione, essendo materia che riguarda la loro vita concreta, la loro possibilità di costruire finalmente progetti di vita nella pienezza della cittadinanza. E' inutile a questo punto fare congetture, attendiamo con pazienza una sentenza che inciderà fortemente anche sulla strategia dell'intero movimento lgbt italiano".