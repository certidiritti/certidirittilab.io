---
title: '2008-0140-Discrimination-ST06400.EN10'
date: Fri, 12 Feb 2010 15:39:49 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 12 February 2010

Interinstitutional File:

2008/0140 (CNS)

6400/10

LIMITE

SOC 106

JAI 134

MI 48

  

  

  

  

  

NOTE

from :

The General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

16063/09 SOC 706 JAI 836 MI 434

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached a note from the Slovak delegation with a view to the meeting of the Working Party on Social Questions on 18 February 2010.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**Note from the Slovak delegation**

We consider the EQUINET (European network of equality bodies) opinion on the role and position of equality bodies of a substantial nature. Therefore Slovakia supports strengthening the position of national equality bodies in the proposed Directive.

**The “Paris principles”** establish standards that enjoy general recognition. They offer valuable and broadly accepted standards for the establishment of equality bodies and can guide the Member States in the establishment of bodies that are fully able to carry out the functions as defined by the legislation.

**It is important to retain the recital and the reference to the “Paris Principles” in the Directive** (Recital 28 in the Commission proposal).

**Article 2 para 3**

**We would prefer to keep the original definition of harassment**, which gives a clear concept of this form of discrimination and has been already used successfully in the previous equality Directives. The new version of the definition of harassment could make the legislation very difficult to operate and could create confusion.

**Article 12**

**The provision should contain a reference to the rights protected under the Directives 2000/43/EC and 2004/113/EC.** This would certainly lend greater clarity and coherence to the legislation concerned and highlight the role of equality bodies.

**Article 16**

Equality bodies have a vital role in monitoring the effectiveness of the equality Directives in practice. The national equality bodies are the institutions that have theoretical and practical knowledge in the implementation of the anti-discrimination legislation. They can evaluate its real effectiveness in practice in different social contexts. The current wording of Article 16(2) on reporting to the Commission weakens the position of national equality bodies.

**We would suggest sticking with the original text of the Article 16(1) as set out in the original Commission proposal, which states that national equality bodies shall communicate information to the European Commission, so as to enable the Commission to draft its report.**

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_