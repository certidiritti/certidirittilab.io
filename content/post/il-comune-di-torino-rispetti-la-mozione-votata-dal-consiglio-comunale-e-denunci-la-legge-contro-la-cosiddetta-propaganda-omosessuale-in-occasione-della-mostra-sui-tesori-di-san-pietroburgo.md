---
title: 'Il Comune di Torino rispetti la mozione votata dal consiglio comunale e denunci la legge contro la cosiddetta "propaganda omosessuale" in occasione della mostra sui tesori di San Pietroburgo'
date: Tue, 04 Jun 2013 14:38:35 +0000
draft: false
tags: [Russia]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti.

Roma, 5 giugno 2013

Il prossimo 6 giugno sarà inaugurata a Torino una mostra sui tesori di San Pietroburgo, nell'ambito dell'accordo di cooperazione siglato dalle due città il 15 novembre 2012. Si tratta della prima occasione per il Comune di Torino di rispettare la mozione, approvata il 29 aprile 2013, che impegna esplicitamente Sindaco e la Giunta a "non attivare nuove iniziative nell'ambito dell'accordo bilaterale di collaborazione senza fare presente alle Autorità di San Pietroburgo la contrarietà della Città di Torino alla Legge del Parlamento di San Pietroburgo del 29 febbraio 2012**". **Tale legge, approvata il 29 febbraio 2012, vieta la cosiddetta “propaganda omosessuale” con la falsa e vergognosa motivazione che parlare di omosessualità in pubblico sia dannoso per i minori. Si tratta di una patente violazione dei diritti umani come denunciato dal Consiglio d’Europa, dall’Unione Europea, e da un elenco lunghissimo di ONG.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, afferma: <<Se il Comune di Torino vuole rispettare gli impegni presi con il Consiglio Comunale che, il 18 luglio 2011, ha approvato anche delle linee programmatiche in cui si afferma, tra l'altro, che "la lotta ad ogni discriminazione basata sul genere, sull'identità e sull'orientamento sessuale rientra ormai a pieno titolo nella cultura della nostra città che negli anni si è dimostrata aperta e permeabile alle istanze della comunità GLBT", non può esimersi dal pronunciare pubblicamente, in questa occasione, la propria contrarietà a una legge chi viola così gravemente il diritto umano fondamentale alla libertà d'espressione. Per questo abbiamo scritto al sindaco Fassino. Riteniamo però necessaria anche una presa di posizione a più alto livello e quindi abbiamo scritto al ministro per i Beni Culturali Massimo Bray affinché condanni anch'egli la sciagurata legge>>.

Per un approfondimento:

**Il testo della mozione approvato dal Consiglio Comunale di Torino in [Italiano](documenti/download/doc_download/78-mozione-comune-torino-sanpietroburgo), [Inglese](documenti/download/doc_download/79-odg-torino-sanpietroburgo-ingl) e [Russo](documenti/download/doc_download/80-odg-torino-sanpietroburgo-russo).**