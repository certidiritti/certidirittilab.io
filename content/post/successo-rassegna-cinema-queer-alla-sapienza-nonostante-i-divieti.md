---
title: 'SUCCESSO RASSEGNA CINEMA QUEER ALLA SAPIENZA: NONOSTANTE I DIVIETI'
date: Wed, 09 Jun 2010 08:44:53 +0000
draft: false
tags: [Comunicati stampa]
---

Ieri sera, martedì 8 giugno, si è inaugurata la Rassegna cinematografica Queer all'Università La Sapienza di Roma. Lo si deve ai ragazzi che con coraggio si sono presentati ugualmente all'Università per proiettare il primo film previsto dal programma. Alla serata inaugurale ha partecipato Sergio Rovasio, Segretario Associazione Radicale Certi Diritti che nel suo intervento ha sottolineato la gravità di quanto avvenuto sul divieto opposto dal Rettore dell'Università allo svolgimento della rassegna per supposti "motivi di sicurezza" dovuti a rischi di attacchi da parte di gruppi di fascisti omofobi, secondo quanto avrebbe comunicato la Digos.

La stessa Digos, su Repubblica di ieri, ha smentito quanto dichiarato dal Rettore dell'Università. L'atteggiamento del Rettore della Sapienza  è grave, autoritario e scorretto sotto diversi punti di vista.  Negare ad un gruppo di studenti, a poche ore dalla sua inaugurazione, lo svolgimento di una rassegna di cinema queer, inventandosi delle motivazioni ridicole e patetiche dimsotra delle grave situazione di 'governo' di una delle più imporanti sedi formative italiane.

Grazie allo straordinario e divertente film della serata inaugurale, in programmazione ieri sera, "A dirty shame", il clima di tensione e nervosismo si è stemperato. Ringraziamo quanti hanno con coraggio e determinazione voluto essere presenti alla Sapienza e dimostrare così ai prepotenti e agli arroganti che la lotta per il superamento delle diseguaglianze va avanti e nessuno la può fermare.