---
title: 'Fuor di pagina'
date: Fri, 14 Feb 2014 14:06:32 +0000
draft: false
---

**_Fuor di pagina_** è la nostra rassegna stampa settimanale dedicata alle libertà sessuali e ai diritti civili. L'appuntamento è **ogni martedì alle 23:30 sulle frequenze di Radio Radicale**. Non riesci a seguirci per quell'ora? Fuor di pagina è disponibile in streaming e in [**Podcast**](http://tinyurl.com/qzb5clv)! \[wp\_rss\_multi_importer showgroup="0" category="1"\]