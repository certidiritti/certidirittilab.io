---
title: 'IL PAPA: "L''OMOSESSUALITA'' NON E'' MAI MORALMENTE GIUSTA"'
date: Tue, 23 Nov 2010 14:12:13 +0000
draft: false
tags: [Comunicati stampa]
---

**IL PAPA: "L'OMOSESSUALITA' NON E' MAI MORALMENTE GIUSTA". CERTI DIRITTI: INVECE LO IOR? LA BANDA DELLA MAGLIANA? L'8XMILLE? SI'?**

_**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti.**_

Roma, 23 novembre 2010

"Dato che ancora non avevamo capito qual'era la posizione del papa riguardo l'omosessualità, né tantomeno lo avevamo capito, quando, da Capo della Congregazione per la dottrina della fede, negli anni '80 e '90, aveva scatenato vere e proprie epurazioni, punizioni e cacciate contro i preti gay o che aiutavano le persone gay, finalmente oggi, dopo tanto attesa e suspense, siamo venuti a sapere che l’omosessualità "non è mai moralmente giusta".

In attesa della sua prossima pronuncia sul tema, che, ne siamo certi, avverrà entro la prossima settimana, chiediamo alla chiesa, nel frattempo,  di farci sapere se invece è "moralmente giusto":

\- Seppellire in una propria chiesa romana gli affiliati ad una feroce banda criminale, leggasi ‘banda della Magliana’;

\- Gestire una banca che è perennemente sotto inchiesta, leggasi caso Marcinkus oppure, più recentemente, il riciclaggio di denaro con transizioni molto sospette;

\- Far credere negli spot tv che l’8xmille è destinato ai poveri, mentre viene anche utilizzato per pagare pullman e panini ai partecipanti alle manifestazioni;

\- Tenere nascosti in tutto il mondo, per anni, con anche documenti firmati, i responsabili dei casi di pedofilia;

\- Costruire immobili nella città di Roma utilizzando i privilegi dell’extraterritorialità;

\- Sollecitare il Governo a destinare più fondi alla scuola cattolica privata togliendoli al capitolo del 5Xmille;

\- Sostenere all’Onu i paesi che si battono contro la depenalizzazione dell’omosessualità e difendere quelli dove è prevista la pena di morte;

Certi di una non risposta su questi temi molto marginali, attendiamo con ansia il prossimo bollettino contro le persone omosessuali.