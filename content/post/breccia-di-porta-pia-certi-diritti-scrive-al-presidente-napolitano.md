---
title: 'BRECCIA DI PORTA PIA: CERTI DIRITTI SCRIVE AL PRESIDENTE NAPOLITANO'
date: Sat, 12 Sep 2009 09:37:41 +0000
draft: false
tags: [Comunicati stampa]
---

**BRECCIA DI PORTA PIA: SUL DIVIETO DELLA QUESTURA PER LA MARCIA ANTICLERICALE DI SABATO 19 SETTEMBRE 2009 LETTERA DI CERTI DIRITTI AL PRESIDENTE NAPOLITANO. “NEL 150° ANNIVERSARIO DELL’UNITA’ D’ITALIA E’ ASSURDO NEGARE QUESTA COMMEMORAZIONE”. **

Roma, 12 settembre 2009

**_Di seguito il testo della lettera inviata oggi al Presidente della Repubblica da Clara Comelli e Sergio Rovasio, Presidente e Segretario dell’Associazione Radicale Certi Diritti:_**