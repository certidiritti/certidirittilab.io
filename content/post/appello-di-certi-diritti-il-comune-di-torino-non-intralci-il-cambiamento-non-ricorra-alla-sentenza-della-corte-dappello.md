---
title: 'Appello di Certi Diritti: il Comune di Torino non intralci il cambiamento, non ricorra alla sentenza della Corte d''Appello'
date: Wed, 07 Jan 2015 13:46:29 +0000
draft: false
tags: [Diritto di Famiglia]
---

[![mamme-lesbiche-281476](http://www.certidiritti.org/wp-content/uploads/2015/01/mamme-lesbiche-281476-300x200.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/01/mamme-lesbiche-281476.jpg)"Con il via libera alla trascrizione dell'atto di nascita per un bambino nato in Spagna con tecnica eterologa da una donna spagnola e una donna italiana la Corte d'Appello torinese percorre la strada già battuta dal Tribunale dei Minori di Roma ribadendo di agire nell'interesse del minore. Sta al comune di Torino non ricorrere a questa decisione per permettere all'Italia di proseguire il suo travagliato percorso verso le obbligate riforme di civiltà nell'ambito del Diritto di Famiglia."

Così Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, ha commentato le ottime notizie giunte dal capoluogo piemontese.

"E' il primo caso di genitorialità congiunta riconosciuta fin dalla nascita, un passo avanti straordinario che garantirebbe a tante coppie di non dover lasciare alla dogana i diritti dati per consolidati in altri paesi dell'Unione Europea. Adesso anche il Parlamento riconosca i nostri figli!".