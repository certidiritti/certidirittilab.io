---
title: 'Alfano esprime peggiore cultura contro civiltà liberale e moderna. Si legga sentenza Corte Costituzionale 138/2010'
date: Sun, 11 Mar 2012 12:14:25 +0000
draft: false
tags: [Diritto di Famiglia]
---

Roma, 10 marzo 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Il Segretario del Pdl Alfano con le sue affermazioni dimostra non solo di essere  fuori dai valori liberali ma anche fuori dalla destra europea. In tutta Europa le destre promuovono iniziative a livello parlamentare e governativo per la difesa dei diritti delle coppie lesbiche e gay.

In Spagna il Partito Popolare è stato tra i sostenitori delle prime proposte sulle unioni civili, in Gran Bretagna il Primo Ministro conservatore si è detto d’accordo per il matrimonio tra persone dello stesso sesso e in Europa il Ppe sostiene le diverse iniziative che mirano a tutelare i diritti delle coppie conviventi. Sappiamo di chiedere troppo ma Alfano dovrebbe leggersi la sentenza della Corte Costituzionale 138/2010 che sollecita la classe politica a legiferare sui temi del riconoscimento dei diritti alle coppie gay anziché continuare ad alimentare antiche e conservatrici posizioni clericali.

E’ davvero sorprendente sentir dire da Alfano che i socialisti spagnoli sono al minimo storico perché hanno fatto leggi come quella del matrimonio tra persone dello stesso sesso. E allora il Pdl che in pochi mesi è calato di oltre il 15%  dovremmo associarlo alle 'innovative e ‘moderne’ politiche  sulla famiglia del mulinaro bianco Giovanardi? Bene, sarà senz'altro così!

**[Iscriviti alla newsletter >](newsletter/newsletter)**