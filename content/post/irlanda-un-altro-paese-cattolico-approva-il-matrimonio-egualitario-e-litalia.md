---
title: 'Irlanda. Un altro Paese cattolico approva il matrimonio egualitario. E l''Italia?'
date: Sat, 23 May 2015 14:25:25 +0000
draft: false
tags: [Europa]
---

![A Yes supporter shows of her campaign badge in central Dublin in Ireland](http://www.certidiritti.org/wp-content/uploads/2015/05/download-300x189.jpg)"Il primo paese al mondo ad approvare il matrimonio egualitario attraverso un voto popolare è stato un paese cattolico. Allo stesso tempo l'Italia fa ancora fatica ad approvare una legge sulle unioni civili", nota Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti. "Speriamo che ora la si smetta con la solfa che i cattolici sarebbero contro il matrimonio egualitario e che l'Italia si metta al passo con gli altri grandi paesi cattolici europei come Spagna, Francia e, appunto, Irlanda", continua Guaiana. "Noi riteniamo, con Ayn Rand, che i diritti individuali e i diritti umani non possono essere sottoposti al voto perché la maggioranza non ha il diritto di votare contro i diritti di una minoranza. La funzione politica dei diritti, in una visione liberale, è precisamente quella di proteggere le minoranze dall'oppressione della minoranza. Ma siccome il sistema irlandese richiedeva un referendum per cambiare la costituzione, ci congratuliamo con gli attivisti irlandesi per l'ottimo lavoro svolto e con il popolo irlandese per la lezione data a tutto il mondo". Guaiana conclude sottolineando: "la destra italiana, ma anche la sinistra, dovrebbe imparare dalla destra irlandese che ha fatto campagna per il sì capendo che non si può speculare politicamente sui diritti umani e individuali".

Comunicato stampa dell'Associazione Radicale Certi Diritti

**Aiutaci a portare avanti le nostre battaglie! [Iscriviti](http://www.certidiritti.org/iscriviti/) o [fai una donazione](http://www.certidiritti.org/donazioni/)**