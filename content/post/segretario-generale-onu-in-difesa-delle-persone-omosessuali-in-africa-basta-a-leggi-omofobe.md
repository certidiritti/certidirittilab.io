---
title: 'Segretario Generale Onu in difesa delle persone omosessuali in Africa: basta a leggi omofobe'
date: Sun, 29 Jan 2012 11:56:22 +0000
draft: false
tags: [Africa]
---

Importante intervento in difesa di gay e lesbiche in Africa del Segretario generale dell'Onu Ban Ki-Moon. Uganda, Nigeria, Mauritania, Sudan e molti altri paesi ascoltino le sue parole.

Roma, 29 gennaio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

L’Associazione Radicale Certi Diritti esprime il suo più vivo apprezzamento su quanto ha dichiarato oggi il Segretario Generale dell’Onu, Ban Ki-Moon ad Addis Abeba al vertice dell’Unione Africana in difesa delle persone omosessuali perseguitate in Africa.  La gravissima situazione della condizione degli omosessuali in Africa è drammatica, in alcuni di questi paesi, Somalia, Sudan, Nigeria, Mauritania,  è prevista per legge la pena di morte.

In Uganda, dove è stato ucciso un anno fa David Kato Kisule, e in Sierra Leone, è previsto per legge l’ergastolo e ci sono alcune proposte di legge, alimentate dal fondamentalismo religioso, che prevedono l’inasprimento delle pene. In molti altri paesi africani ci sono leggi che perseguitano le persone omosessuali con gravi sanzioni, da 3 a 15 anni di galera e, in alcuni casi, i lavori forzati; tra questi Zambia, Zimbawe, Togo, Tanzania, Swaziland,  Burundi, Botswana, Liberia, Algeria, Angola, Seychelles, Mozambico,  Mauritius, Marocco, Malawi, Libia, Liberia, Kenia, Gibuti, Guinea, Gambia, Eritrea, Etiopia, Camore, Camerun, fino alla Tunisia.

Occorre che i governi democratici, l’Unione Europea, e gli Stati del Nord America, vincolino gli aiuti umanitari al rispetto dei diritti civili e umani delle persone anche omosessuali. Il rispetto della Dichiarazione Universale dei Diritti Umani deve essere la base di ogni rapporto bilaterale con questi Stati. Come ha detto oggi il Segretario Generale dell’Onu “la discriminazione sulla base dell’identità sessuale è stata ignorata o perfino approvata da numerosi Stati per troppo tempo, combattere queste discriminazioni e' una sfida, il futuro dell'Africa dipende anche dall'investimento nei diritti civili, politici, economici, sociali e culturali'.