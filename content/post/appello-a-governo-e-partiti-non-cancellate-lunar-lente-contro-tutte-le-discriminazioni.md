---
title: 'Appello a Governo e Partiti: Non cancellate l’UNAR, l’ente contro tutte le discriminazioni'
date: Thu, 12 Jul 2012 13:26:51 +0000
draft: false
tags: [Politica]
---

Anche Certi Diritti tra i firmatari dell'appello

Tutte le adesioni aggiornate alla pagina web: [http://www.arcigay.it/36987/appello-a-governo-e-partiti-non-cancellate-unar/](http://www.arcigay.it/36987/appello-a-governo-e-partiti-non-cancellate-unar/)

Numerose sigle dell’associazionismo italiano, tutte impegnate nell’affermazione dei diritti e della dignità delle persone e contro ogni violenza e discriminazione, hanno condiviso un percorso di crescita, conoscenza reciproca, condivisione di obiettivi che ha visto nell’attività svolta da UNAR,  negli ultimi tre anni, un motore importante e un punto di riferimento.

In questi tre anni, l’Ufficio nazionale contro le discriminazioni introdotto con il recepimento di direttive europee sulla parità di trattamento e contro le discriminazioni ha infatti scritto pagine importanti nella diffusione di prassi antidiscriminatorie, costruzione di reti,  contrasto ai fenomeni di discriminazione e apertura di tavoli che hanno creato preziose relazioni, sollecitando straordinarie sinergie e ottenendo riconoscimenti dal Consiglio d’Europa, dalla Commissione europea e dalle Nazioni Unite.

Unar ha messo in campo attività finanziate in larghissima misura da fondi europei e  grava assai poco sul bilancio del nostro Paese e soprattutto dovrebbe essere assunto a modello per la capacità di utilizzo dei fondi europei.

Esprimiamo dunque sgomento e massima preoccupazione nel constatare come l’enorme lavoro svolto dall’ente, grazie alla direzione di Massimiliano Monnanni, sia in pericolo a causa di un’applicazione indiscriminata della spending review che non ne riconosce i meriti. Un’attenta valutazione politica doveva essere esercitata prima di arrivare a conseguenze che oggi  rischiano di stroncare il futuro stesso dell’ufficio, attraverso la contemporanea perdita della direzione,  il drammatico ridimensionamento dell’organico , la dispersione  di competenze,  conoscenze e esperienze assolutamente insostituibili in un momento complesso come quello che viviamo.

Solo negli ultimi mesi  l’UNAR ha avviato  piani di attività fondamentali che necessitano di impulso e coordinamento forte e di un altrettanto forte coinvolgimento delle autonomie locali e dell’associazionismo: la Strategia nazionale di inclusione dei ROM, Sinti e Camminanti ; il Piano nazionale di azione contro razzismo e xenofobia; il Programma per l’applicazione della Raccomandazione del Consiglio d’Europa su orientamento sessuale e identità di genere;  l'apertura e la programmazione di attività di Unar al contrasto della discriminazione sulla base della disabilità.

Denunciamo pubblicamente il rischio che si spezzi qualunque continuità d’azione nel contrasto alle discriminazioni, con gravi infrazioni di obblighi derivanti da trattati e direttive dell’Unione e gravi e concrete sofferenze per la vita di tante persone.  Riteniamo urgentissima un’assunzione di responsabilità delle Istituzioni e dei partiti, e invochiamo una nuova riflessione da parte del Governo e del Presidente del Consiglio,  perché si adottino tutte le soluzioni possibili per mantenere ad UNAR,  e al nostro Paese, le condizioni per una seria strategia di contrasto alle discriminazioni tutte, in un momento in cui sulla convivenza civile, l’equità, la dignità, si gioca tanta parte della nostra capacità e credibilità nel rilancio dell’Italia.  

AGEDO

ARCI

Arcigay

Associazione Nevo Drom

Associazione Sucar Drom

Associazione radicale “Certi diritti”

ENAR - European Network Against Racism Italia

Federazione Rom e Sinti Insieme

FISH – Federazione Italiana per il Superamento dell’Handicap

MIT – Movimento identità transessuale

Parks - Liberi e Uguali

Rete Lenford

Sinti nel mondo

Telefono Azzurro