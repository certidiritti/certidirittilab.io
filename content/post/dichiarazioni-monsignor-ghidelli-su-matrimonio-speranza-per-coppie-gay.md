---
title: 'DICHIARAZIONI MONSIGNOR GHIDELLI SU MATRIMONIO: SPERANZA PER COPPIE GAY'
date: Wed, 19 Aug 2009 06:55:51 +0000
draft: false
tags: [Comunicati stampa]
---

MATRIMONIO: SVOLTA DEL VATICANO? RINGRAZIAMO MONSIGNOR GHIDELLI PER LA SUA APERTURA SULL’ACCESSO ALL’ISTITUTO DEL MATRIMONIO ‘PER OGNI CREATURA’. SPERANZE PER LE COPPIE GAY.

Roma, 19 agosto 2009

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

“Con grande sorpresa e gioia abbiamo letto le dichiarazioni di Monsignor Carlo Ghidelli, Vescovo di Lanciano Ortona, presidente dei vescovi di Abruzzo e Molise e membro del Consiglio episcopale  
permanente che ha ieri dichiarato: “'Penso di dover affermare il diritto naturale universale di ogni creatura al  
mondo di contrarre nozze sia con membri della propria religione che di altre fedi. Si tratta di un diritto naturale e come tale non puo' essere negato'.

L’Associazione Radicale Certi Diritti, insieme all’Avvocatura lgbt – Rete Lenford, ha lanciato da oltre un anno la campagna di ‘Affermazione Civile’ che consiste nel richiedere da parte delle coppie gay al proprio Comune di residenza le pubblicazioni matrimoniali. Quando il Comune oppone il diniego si incardina così un’iniziativa legale. Grazie a questa campagna i Tribunali di Venezia e Trento hanno rimesso alla Corte Costituzionale la decisione sulle ragioni dei ricorrenti considerandole fondate”. Questa iniziativa parte dal presupposto che ‘ogni creatura al mondo ha il diritto di contrarre nozze’, proprio come dice Monsignor Ghidelli, che pubblicamente ringraziamo per questa attenzione alla richiesta di felicità delle coppie gay”.