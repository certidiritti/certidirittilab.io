---
title: '2008-0140-Discrimination-ST15174.EN10'
date: Thu, 28 Oct 2010 12:27:46 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 28 October 2010

Interinstitutional File:

2008/0140 (CNS)

15174/10

LIMITE

SOC 681

JAI 865

MI 388

  

  

  

  

  

NOTE

from :

The Presidency

to :

The Working Party on Social Questions

on :

4 November 2010

No. prev. doc. :

13883/10 SOC 561 JAI 759 MI 317

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Further to the meeting of the Working Party on Social Questions on 19 October, and the written comments received in response to the questionnaire on the provisions concerning _housing_ (doc. 13883/10), delegations will find attached a set of drafting suggestions prepared by the Presidency concerning Recitals 17, 17(d) and 20a and Articles 3(1), 4, 4a, 4b and 15 (Annex I), together with an overview of the approach taken (Annex II).

Changes in relation to the previous version (doc. 10511/2/10 REV 2) are indicated as follows: new text is in **bold** and deletions are marked **\[...\]**.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX I**

Proposal for a

COUNCIL DIRECTIVE

on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

…

(17)   While prohibiting discrimination, it is important to respect other fundamental rights and freedoms, including the protection of private and family life **\[…\]**, the freedom of religion, the freedom of association, the freedom of expression, the freedom of the press and the freedom of information.

…

(17d) All individuals enjoy the freedom to contract, including the freedom to choose a contractual partner for a transaction. **\[…\] The Directive only covers the access to and supply of goods and services, including housing, which are available to the public and which are offered outside the context of private and family life.**

…

(20a) In addition to general measures to ensure accessibility, individual measures to provide reasonable accommodation play an important part in ensuring full equality in practice for persons with disabilities to the areas covered by this Directive. **In the provision of housing, reasonable accommodation includes the duty of the provider to change practices, policies and procedures that may represent barriers to persons with disabilities. The provider should not be required to make structural alterations to the premises or to pay for them, but should accept such alterations, if they are funded otherwise.**

…

Article 3  
Scope

1.       Within the limits of the competences conferred upon the European Union, the prohibition of discrimination shall apply to all persons, as regards both the public and private sectors, including public bodies, in relation to:

(a)     social protection, including social security, social assistance, social housing and healthcare;

(b)

(c)          education;

(d)     access to and supply of goods and other services **\[…\]**, including housing**, which are available to the public and which are offered outside the context of private and family life**.

**\[…\]**

...

Article 4

Accessibility for persons with disabilities

1.             Member States shall take the necessary and appropriate measures to ensure accessibility for persons with disabilities, on an equal basis with others, within the areas set out in Article 3. These measures should not impose a disproportionate burden.

1a.     Accessibility includes general anticipatory measures to ensure the effective implementation of the principle of equal treatment in all areas set out in Article 3 for persons with disabilities, on an equal basis with others\[, and with a medium or long-term commitment\].

  

2.             Such measures shall comprise the identification and elimination of obstacles and barriers to accessibility, \[as well as the prevention of new obstacles and barriers\] in the areas covered in this Directive.

3.

4.

5.

6.       Paragraphs 1 and 2 shall apply to **\[…\]** housing only as regards the common parts of buildings with more than one housing unit. This paragraph shall be without prejudice to Article 4(7) and Article 4a.

7.       Member States shall progressively take the necessary measures to ensure that sufficient **\[…\]** housing is accessible for people with disabilities.

_Article 4a  
Reasonable accommodation for persons with disabilities_

1.       In order to guarantee compliance with the principle of equal treatment in relation to persons with disabilities, reasonable accommodation shall be provided within the areas set out in Article 3, unless this would impose a disproportionate burden.

2.       Reasonable accommodation means necessary and appropriate modifications and adjustments where needed in a particular case, to ensure to persons with disabilities access on an equal basis with others.

3. **\[****In the provision of housing, paragraphs 1 and 2 shall not require the provider to make structural alterations to the premises or to pay for them. Subject to paragraphs 1 and 2, the provider shall accept such alterations, if they are funded otherwise.\]**

  

4.

_Article 4b  
Provisions concerning accessibility and reasonable accommodation_

1.       For the purposes of assessing whether measures necessary to comply with Articles 4 and 4a would impose a disproportionate burden, account shall be taken, in particular, of:

a)             the size and resources of the organisation or enterprise;

b)             the estimated cost;

c)

d)            the life span of infrastructures and objects which are used to provide a service;

e)             the historical, cultural, artistic or architectural value of the movable or immovable property in question;

f)              whether the measure in question is impracticable or unsafe.

The burden shall not be deemed disproportionate when it is sufficiently remedied by measures existing within \[the framework of the disability policy of\] the Member State concerned.

2.       Articles 4 and 4a shall apply to the design and manufacture of goods, unless this would impose a disproportionate burden. For the purpose of assessing whether a disproportionate burden is imposed in the design and manufacture of goods, consideration shall be taken of the criteria set out in article 4b(1).

3.       Articles 4 and 4a shall not apply where European Union law provides for detailed standards or specifications on the accessibility or reasonable accommodation regarding particular goods or services.

…

Article 15

Implementation

1.       Member States shall adopt the laws, regulations and administrative provisions necessary to comply with this Directive by …. at the latest \[4 years after adoption\]. They shall forthwith inform the Commission thereof and shall communicate to the Commission the text of those provisions.

When Member States adopt these measures, they shall contain a reference to this Directive or be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.

2.             In order to take account of particular conditions, Member States may, if necessary, establish that the obligation to ensure accessibility as set out in Articles 4 and 4b has to be complied with by, at the latest, \[5 years after adoption\] regarding new buildings, facilities, vehicles and infrastructure, as well as existing buildings, facilities and infrastructure undergoing significant renovation and by \[20 years after adoption\] regarding all other existing buildings, facilities, vehicles and infrastructure.

Member States wishing to use any of these additional periods shall inform the Commission at the latest by the date set down in paragraph 1 giving reasons. Member States shall also communicate to the Commission by the same date an action plan laying down the steps to be taken and the timetable for achieving the gradual implementation of Article 4 \[, including its paragraph 7\]. They shall report on progress every two years starting from this date.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

**ANNEX II**

**Explanations for drafting suggestions**

**_1\. Protection of private and family life_**

_Many delegations indicated in their written contributions or in the debate that the most relevant basis for a possible exemption is the protection of private and family life._

_The reference to "which are offered outside the context of private and family life" would exclude situations, where housing space is offered within the provider's own house or apartment or where facilities (e.g. kitchen, bathroom) are shared, from the scope of the Directive. In such situations, where the housing provider's privacy is at stake, a limited exemption from the principle of non-discrimination seems to be justified._

_The reference to "which are available to the public" ensures that only housing which is offered on the market (e.g. by advertising in newspapers or on the Internet, or through professional agencies) is covered by the Directive._

_On the other hand, very few, if any, Member States seem to have a legal definition of "commercial or professional activity", in particular in the area of housing. The exact scope of the existing exemption is therefore not clear, and might go beyond what is necessary. Moreover, in the debate, no problematic situations which would not be covered by the exemption on private life were presented. The reference to "commercial or professional activity" therefore appears to be redundant._

_Article 3 (1)(d) is therefore adapted accordingly. This also has the advantage of ensuring more consistency with the scope of Directive 2004/113/EC implementing the principle of equal treatment between men and women in the access to and supply of goods and services._

_Recital 17d is also adapted accordingly. The reference to "transactions carried out in that context" is deleted from Recital 17, as it does not fit in its enumeration of fundamental rights._

**_2\. Social housing_**

_Delegations indicated that no distinction was generally made in their national equal treatment law or in accessibility legislation between private and social/public housing._

_They also indicated that "social housing" could take many different forms according to each national system (e.g. specific social organisations providing public housing, private provision of social housing with the help of public funds), but that it would in any event be covered by the Directive either as part of the social protection system (Article 3 (1)(a)) or as a service (Article 3 (1)(d))._

_It therefore does not seem to be necessary to make a distinction between different forms of housing. The reference to "private and social" in Article 4 (6) and (7) is therefore deleted._

_Since "social housing" seems to be part of the social protection system in many Member States, the reference to it is maintained in Article 3 (1)(a)._

**_3\. Reasonable accommodation_**

_Reasonable accommodation for people with disabilities is not about anticipatory measures to ensure accessibility, but about providing "ad hoc" solutions needed in a particular case when a disabled person wants to access a service. They are normally quite easy to provide._

_Since housing is generally a long-term contractual relationship, the question arises what the duty to provide reasonable accommodation would mean for providers of housing. Does it only cover the duty to adjust procedures, regulations or user instructions, or does it require the provider to make physical and structural adaptations to the premises? Such alterations could be quite costly for the provider of housing._

_The Presidency suggests clarifying the duty to provide reasonable accommodation in the area of housing in the text of the Directive along the following lines:_

-     _Providers of housing would be required to change practices, policies and procedures that may represent barriers to persons with disabilities, as well as to make minor adjustments at the premises. This would correspond to the general duty of reasonable accommodation that applies to other service providers._

-     _They would not be required to make structural alterations at the premises (e.g. adding ramps or handrails, widening doors) or to pay for them, as this could be very costly._

-     _However, they could **not** oppose such structural alterations if they were paid for by the tenant or by public funds and if:_

o   _they did not impose a "disproportionate burden" (in this context, issues such as the historical value of buildings could also be taken into account; cf. Article 4b (1)(e) and (f));_

o   _they were "necessary and appropriate" (i.e. they were reasonable and did not go beyond what was necessary or what could be expected)._

_These suggestions are set out in the new Article 4a(3 ) as well as in the new text in Recital 20a. Article 4a(3) is in square brackets. Whether the text should be in a recital and/or in an article is subject to further discussion by the delegations._

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_