---
title: 'CIAO MARCO.'
date: Thu, 19 May 2016 14:36:50 +0000
draft: false
tags: [Politica]
---

[![DSC_0012](http://www.certidiritti.org/wp-content/uploads/2016/05/DSC_0012-300x222.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/05/DSC_0012.jpg)Nell’Italia lacerata dal terrorismo degli anni ’70, Marco Pannella ha saputo accogliere il neonato movimento LGBTI italiano del FUORI! (Fronte Unitario Omosessuale Rivoluzionario Italiano) ed è sempre stato al fianco delle lotte di liberazione individuale delle persone LGBTI. In quegli stessi anni [scriveva](http://www.radicali.it/20120111/prefazione-al-libro-underground-pugno-chiuso) ad Andrea Valcarenghi: “Credo sopra ad ogni altra cosa al dialogo, e non solo a quello spirituale: alle carezze, agli amplessi, alla conoscenza, come a fatti non necessariamente d'evasione o individualistici - e tanto più privati mi appaiono, tanto più pubblici e politici, quali sono, m'ingegno che siano riconosciuti”. Ringraziamo Marco per la forma che ha dato alla storia sua, nostra e di questo Paese. Roma 19 maggio 2016