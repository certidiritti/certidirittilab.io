---
title: 'Gli negano patente perchè gay. Dopo Catania ora tocca a Brindisi'
date: Wed, 11 May 2011 08:11:25 +0000
draft: false
tags: [Politica]
---

**

**L’incredibile vicenda ad un ragazzo che vive in Puglia. Quanto è avvenuto a Catania non è servito a cancellare l'obbrobrio discriminatorio. Chiesto l'intervento dell'Unar.  
**

**I Radicali, prima firmataria Rita Bernardini, hanno depositato oggi un0interrogazione urgente al governo. Il testo integrale.**

**

Comunicato stampa dell’Associazione Radicale Certi Diritti

Roma, 11 maggio 2011****

Cristian Friscina, un ragazzo residente in Puglia (Brindisi), titolare di una patente di guida emessa dalla motorizzazione civile di Brindisi nel 1999, si è visto negare il rinnovo della patente perché secondo i documenti ‘risulterebbero patologie che potrebbero risultare di pregiudizio per la sicurezza della guida’. Dalle risultanze della comunicazione trasmessa dall’Ospedale Militare Bonomo di Bari, dove il ragazzo era stato mandato quando si era dichiarato omosessuale durante il periodo della leva, risulta che tale comunicazione “fa sorgere dubbi sulla persistenza dei requisiti di idoneità psicofisica prescritti per il possesso della patente”. Le conseguenze di questo diniego continuano a produrre un grave danno, in particolare in ambito lavorativo, per la non possibilità di potersi muovere.

I deputati Radicali, prima firmataria Rita Bernardini, Presidente dell’Associazione Radicale Certi Diritti, hanno oggi depositato una interrogazione urgente ai Ministri dei Trasporti e della Difesa dove viene documentata e ricostruita la gravissima discriminazione subita da Cristian Friscina.  Qui sotto il testo integrale.

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

“Cristian Friscina, il cittadino pugliese a cui è stato negato il rinnovo della patente perché gay, non è che un esempio di forma di discriminazione che avviene oggi in Italia contro una persona omosessuale. Grazie all’aiuto legale del Presidente di Rete Lenford, Avvocato Antonio Rotelli e al coinvolgimento dell’Unar, l’ Ufficio Antidiscriminazione del Ministero delle Pari Opportunità, siamo certi che la vicenda si risolverà in suo favore, come già avvenuto a Catania due anni fa con anche il risarcimento da parte del Ministero dei Trasporti dei danni provocati dal diniego alla guida ad un cittadino in ragione del suo orientamento sessuale. Aiuteremo Cristian e gli saremo vicini, ci auguriamo che episodi così gravi e odiosi non avvengano più nel nostro paese”.

**Interrogazione urgente a risposta scritta**

**Ai Ministri delle infrastrutture e dei Trasporti e della Difesa**

**Per sapere - premesso che:**

il Sig. Friscina Cristian, è un ragazzo residente in Puglia, titolare di patente di guida emessa dalla motorizzazione civile di Brindisi il 28/04/1999;

il 04/06/2009, il Sig. Friscina ha proceduto al rinnovo decennale della patente di guida, presso l'USL di Brindisi (distretto di Mesagne). Le procedure di rinnovo prevedono che ci si sottoponga ad una visita medica per valutare la permanenza dell’idoneità alla guida;

il Sig. Friscina è risultato perfettamente idoneo e la validità della patente gli è stata rinnovata. Come per prassi, gli è stato anche rilasciato un certificato medico che consente di continuare a guidare (Allegato n. 1) in attesa di ricevere a mezzo posta il tagliando adesivo attestante il rinnovo che si applica sulla patente, da parte del Ministero delle Infrastrutture e dei Trasporti;

il 21 dicembre 2010, tuttavia, si è dovuto recare presso gli uffici della Motorizzazione civile di Brindisi per sollecitare l’invio del predetto tagliando che a distanza di un anno e mezzo dal rinnovo non gli era ancora pervenuto;

in quella sede, però, apprendeva dell’esistenza di un provvedimento (Allegato n. 2) con il quale gli era imposta “la revisione della patente di guida mediante nuovo esame di idoneità psicofisica”, sulla base di una “comunicazione del 14/01/2000 dell'Ospedale Militare Bonomo di Bari”; ciò nonostante la validità della patente gli fosse stata rinnovata il 04/06/2009 a seguito di visita medica;

il provvedimento si limitava a disporre la revisione della patente di guida del Sig. Friscina “vista la comunicazione n. 17 Osp. Mil. Bonomo Bari in data 14/01/2000 dalla quale risulta che la S.V., il 13/01/2000 presentava delle patologie che potrebbero risultare di pregiudizio per la sicurezza della guida, considerato che le risultanze di tale comunicazione fanno sorgere dubbi sulla persistenza nella S.V. dei requisiti di idoneità psicofisica prescritti per il possesso della patente di guida”;

il riscontro di fatti determinanti è presupposto perché sorgano dubbi sulla persistenza dei requisiti fisici e/o psichici prescritti o dell'idoneità tecnica alla titolarità della patente di guida e il provvedimento mancava del tutto della loro indicazione, in violazione dell'articolo 3 della legge sul procedimento amministrativo e dell'art. 128 del codice della strada;

il provvedimento mancava, quindi, delle motivazioni che hanno portato alla sua adozione e mancava anche, in allegato, della comunicazione dell'Ospedale militare di Bari di cui si ignora il contenuto, ma dal quale si evincerebbe che il Sig. Friscina nel 2000 “presentava delle patologie che potrebbero risultare di pregiudizio per la sicurezza della guida”;

si dà il caso che nell'anno 2000 il Sig. Friscina sia stato esonerato dal servizio di leva obbligatorio, proprio dall'Ospedale militare di Bari, conseguendo un foglio di congedo assoluto. La ragione di tale congedo era legata esclusivamente al fatto che, essendo il Sig. Friscina persona omosessuale, in caserma sarebbe stato esposto al rischio di discriminazione e intolleranza in ragione del proprio orientamento omosessuale;

egli non presentava alcun tipo di patologia, né tale può essere considerata l'omosessualità come l'eterosessualità di una persona. In nessun caso l'orientamento sessuale di una persona può essere considerata causa di inidoneità psichica o fisica alla titolarità della patente di guida;

nella comunicazione dell'Ospedale militare sembrerebbe addirittura che l’omosessualità venga definita 'patologia', fatto di per sé assurdo;

qualora l'orientamento omosessuale del ricorrente fosse stato il motivo della sospensione della patente di guida, l’atto amministrativo sarebbe intrinsecamente e sostanzialmente nullo in quanto adottato in mancanza assoluta di presupposti e ledendo i diritti costituzionali fondamentalissimi dell'individuo, a partire dalla dignità personale. La nullità di tale atto si estendenderebbe a qualsiasi atto consequenziale che lo abbia preso o lo prendesse come suo presupposto;

a tal proposito si richiamano anche le sentenze del TAR Sicilia n. 2353 del 2005 e quella del Tribunale di Catania del 29 gennaio 2008, che hanno dichiarato la nullità del provvedimento che disponeva la revisione della patente di guida in un caso identico a quello per il quale si ricorre. Il tribunale di Catania, di recente confermato dalla Corte di Appello, ha anche condannato i ministeri interrogati al risarcimento del danno subito dal ricorrente/attore;

il 20/01/2011, il Sig. Friscina si è nuovamente recato presso l'ufficio della motorizzazione civile di Brindisi per depositare un ricorso contro il provvedimento che gli era stato consegnato il 21/12/2010, protocollato con il numero 237 III /15 del 20/01/2011;

a distanza di ben tre mesi, il 15/04/2011, il Sig. Friscina si è recato nuovamente presso l’Ufficio della motorizzazione per avere informazioni sull’esito del proprio ricorso, ma paradossalmente non vi era nessun funzionario in grado di dirgli alcunché, neppure se l’esame del ricorso era stato avviato. Solo tre giorni dopo la Motorizzazione faceva pervenire a mezzo posta una comunicazione nella quale confermava che il ricorso era stato inoltrato alla Direzione Generale territorialmente competente (allegati 2 e 4);

nel frattempo il Sig. Friscina continua ad avere la patente sospesa ed in conseguenza di ciò a subire notevoli danni,

**Per sapere:**

se i Ministri interrogati sono in possesso di informazioni sul caso descritto; se non ritengono di intervenire per risolvere con urgenza la paradossale situazione descritta; come intendano chiedere risarcire il Sig. Friscina a nome della Pubblica amministrazione; se non intendano adottare un provvedimento generale che vieti l’adozione di simili provvedimenti nei confronti della persone con orientamento omosessuale.

Rita Bernardini

Marco Beltrandi

Maria Antonietta Farina Coscioni

Matteo Mecacci

Maurizio Turco

Elisabetta Zamparutti