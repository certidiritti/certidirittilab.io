---
title: 'L''intervento di Ottavio Marzocchi al VI° Congresso dell''Associazione Radicale Certi Diritti'
date: Tue, 16 Apr 2013 07:11:53 +0000
draft: false
tags: [Politica]
---

Alcuni (S)Punti su Diritti LGBT in Italia ed in Europa

Congresso Associazione Radicale Certi Diritti 2013

Cari amici,

Mi spiace non essere con voi al congresso dell'Associazione Radicale Certi Diritti, alla quale sono iscritto fin dalla sua fondazione con continuato entusiasmo e convinzione.

Non sono in Europa purtroppo ma vi mando alcuni punti scritti come possibile, come nota su iphone, sperando nella vostra comprensione.

Come molti di voi sanno, ho la fortuna di potere seguire lo sviluppo dei diritti LGBT in Europa grazie al mio lavoro di funzionario radicale del gruppo liberale e democratico al PE e di responsabile per le questioni relative ai diritti civili, la giustizia e gli affari interni.

Da quando sono arrivato al PE oramai 16 anni fa ho seguito i lavori di ILGA, ILGA-Europe e dell'intergruppo LGBT al PE, animando un lavoro comune con i deputati radicali e poi quelli liberali e con Certi Diritti.

Recentemente ho partecipato a un seminario di ILGA Europe per discutere del loro futuro piano d'azione assieme ad alcuni altri sostenitori di lunga data, istituzionali e non, riconoscimento del fatto che noi radicali siamo conosciuti e riconosciuti per il nostro costante attivismo, nelle istituzioni e fuori di esse.

Tutto questo per dire che voglio cogliere questa occasione per fare il punto assieme a voi su Diritti LGBT, Europa ed Italia, ma non solo.

\- Europa in crisi economica e democratica

L'Europa attraversa una pesante fase di crisi economica, ma anche democratica. Le classi politiche nazionali sono sempre più chiuse in sè stesse, volte a difendere i loro privilegi ed incapaci di riforme, come in Italia; o sono incapaci di affrontare la crisi in arrivo per tempo o di lottare contro la corruzione, come in Spagna; in altri paesi la classe politica si è barricata al potere creando un regime che scivola dal conservatorismo nazionalista al totalitarismo, come in Ungheria. Proprio qui la comunità LGBT, come gli ebrei, i rom, le opposizioni democratiche diventano un nemico da discriminare, emarginare, colpire. L'ennesima riforma della Costituzione ungherese ha costituzionalizzato, tra l'altro, la definizione di famiglia come unione di un uomo e di una donna, escludendo cosi le coppie LGBT o le famiglie monoparentali.

Di fronte alle recrudescenze contro-riformatrici, razziste, xenofobe, ai reati motivati dall'odio, alle discriminazioni che rinascono e possono esplodere a livello nazionale, l'Europa manca di adeguati strumenti di intervento. Il PE ha chiesto di utilizzare al massimo i poteri dell'UE, come le sanzioni basate sull'articolo 7 TUE per le violazioni dei calori e principi comuni europei, ma sia la Commissione che gli Stati Membri non l'hanno mai fatto sinora. Al PE manca la maggioranza necessaria per attivare tale articolo, dato che il PPE è contrario al suo uso ad esempio per l'Ungheria. Sarà forse necessario modificare i Trattati per introdurre un meccanismo di allarme, valutazione e sanzione delle violazioni della democrazia, stato di diritto, diritti umani ed eguaglianza - peraltro già esistente nel settore economico e di bilancio. Alcuni Stati membri si sono finalmente mossi in questo senso chiedendo un maggiore ruolo e poteri europei. In ogni caso, i diritti LGBT sono oramai parte integrante del bagaglio europeo in materia di diritti fondamentali - infatti ogni paese che chiede di aderire all'UE viene esaminato sotto questo profilo sulla base dei cosiddetti criteri di Copenhagen - e tale meccanismo permetterà un maggiore controllo e dinamica nel mantenimento e progresso di tali diritti.

Il PE si pronuncerà su questi temi nel quadro della relazione annuale del deputato liberale belga Louis Michel. In prospettiva, la revisione dei Trattati sarà fondamentale, e dovremo chiedere riforme coraggiose per difendere con più forza i diritti umani, inclusi quelli LGBT.

Notizia positiva di oggi: i negoziati per l'adesione dell'UE alla Convenzione europea dei diritti dell'uomo si sono conclusi positivamente! Rimane da superare lo scetticismo del governo conservatore inglese, ma sarebbe inimmaginabile un ulteriore blocco a causa di uno Stato UE sui 47 del Consiglio d'Europa...o comunque inaccettabile!

\- la Direttiva sulla Libera circolazione

Tra i successi europei più importanti in generale e per i diritti LGBT c'è sicuramente la direttiva sulla libera circolazione dei cittadini. Nonostante sia il risultato di una serie di compromessi tra PE, Commissione e Stati membri, essa ha garantito pur con alcuni limiti il diritto fondamentale dei cittadini comunitari e delle loro famiglie a circolare nell'Ue e a godere del diritto di entrata, soggiorno e residenza senza ostacoli illegittimi.

La definizione di famiglia prevista dalla direttiva include le coppie unite civilmente, se lo stato membro prevede tali unioni; in ogni caso le unioni civili e le relazioni stabili devono essere prese in piena considerazione al fine della concessione del diritto di entrata e residenza. Sulla base di tali norme Certi Diritti ha ottenuto che tribunali e governo, con la Ministra Cancellieri, sorpassassero la circolare Amato, che violava la direttiva. Oggi insomma il partner non-comunitario sposato con un cittadino comunitario dello stesso sesso puó ottenere la residenza in Italia. Si tratta insomma di un riconoscimento mutuo delle coppie LGBT limitato peró alla sola libera circolazione. Bisognerà ora allargare tale diritto ai partners 'semplici' o uniti civilmente ed assicurarsi che la giurisprudenza sia sviluppata o trasformata in legge, attraverso ad esempio la riforma del diritto di famiglia in Italia.

A livello europeo, la Commissione presenterà probabilmente nuove linee guida per l'applicazione della direttiva, invece che proporre una sua revisione come richiesto da alcuni Stati membri, in primis Gran Bretagna e Germania, che vogliono limitare la libera circolazione in fase di crisi economica. Non rimane da augurarsi che tali linee guida comprendano chiaramente la proibizione delle discriminazioni basate sull'orientamento sessuale come previsto dall'art 21 della Carta dei Diritti Fondamentali, sulla base della quale la Commissione ha iniziato - a quanto afferma - procedimenti di infrazione contro numerosi Stati membri. Se le linee guida comprenderanno a chiare lettere tale proibizione per gli Stati UE di negare il mutuo riconoscimento e la libera circolazione delle coppie dello stesso sesso, un chiaro passo avanti sarebbe fatto in Europa. Potremmo ad esempio scrivere come Certi Diritti una lettera alla Commissione per spronarla a farlo.

\- Diritto di famiglia, mutuo riconoscimento dei documenti di stato civile e dei loro effetti

Il trattato di Lisbona prevede - purtroppo - che solo gli aspetti transnazionali del diritto di famiglia siano di competenza UE. Una procedura speciale si applica in questo caso, che prevede l'unanimità. Per il resto, il diritto di famiglia rimane di competenza nazionale, come voluto da alcuni Stati membri in occasione della stesura del Trattato.

Insomma la Commissione potrebbe immediatamente presentare una proposta sul mutuo riconoscimento del diritto di famiglia nell'UE naturalmente per quanto riguarda gli aspetti transnazionali.

La Commissione ha però deciso di seguire la strada del 'mutuo riconoscimento dei documenti di stato civile e dei loro effetti' nell'UE. Sebbene il titolo suoni burocratico ed amministrativo, l'impatto è di ampia portata: assicurare che i documenti di nascita, morte, matrimonio, filiazione, etc siano riconosciuti in tutta l'UE e che abbiano gli stessi effetti ovunque. Tradotto: coppia LGBT sposata e con figli in Belgio si trasferisce in Italia ed è riconosciuta come tale: sposata e con figli, come provato dai relativi documenti i cui effetti sono diritti e doveri.

Come potete immaginare, sarebbe un grande passo in avanti. La proposta della Commissione è attesa nelle prossime settimane, vedremo fino a che punto la Commissione saprà resistere al lobbying nazionalista della 'sussidiarietà' - in realtà schermo per nascondere repressione e restrizioni dei diritti - o vaticano - che si è lanciato da mesi con petizioni e pressioni per neutralizzare la proposta (per memoria: Certi Diritti aveva depositato un documento a favore del mutuo riconoscimento in occasione della consultazione pubblica; il PE ha ripetutamente espresso il suo parere favorevole all'iniziativa).

\- Direttiva orizzontale Anti- discriminazioni

Sono passati 5 lunghi anni da quando la Commissione ha presentato la proposta di direttiva per promuovere il principio di eguaglianza e lottare contro le discriminazioni, incluse quelle basate sull'orientamento sessuale, nei settori dell'educazione, dei beni e dei servizi, della casa. La Germania, assieme a pochi altri Stati membri, hanno opposto un veto sostanziale. Nel frattempo il Consiglio ha lavorato sul testo al fine di cercare di ottenere un testo accettabile da tutti, visto il requisito dell'unanimità previsto dall'art 19 del TFUE.

La caduta del governo Merkel CDU-CSU-liberali tedeschi potrebbe permettere lo sblocco della direttiva, sempre che qualche altro Stato non decida di porre il veto. Il PE ha chiesto per l'ennesima volta il mese scorso in una risoluzione sul razzismo, la xenofobia e i crimini di odio, che il Consiglio approvi la direttiva al più presto. Una possibilità che era stata ventilata era quella di scindere la direttiva in più direttive che affrontassero i vari motivi di discriminazione, disabilità, da una parte e orientamento sessuale, religione ed età dall'altra.

Mi sembra che sia necessario ed urgente raccogliere firme a livello transnazionale su un appello ai governi degli Stati UE per chiedere loro di approvare la direttiva al più presto.

\- La revisione della Decisione Quadro sul Razzismo e la Xenofobia

Il prossimo novembre la Commissione presenterà la sua relazione sull'applicazione della Decisione Quadro sul Razzismo e la Xenofobia. Il PE ha ripetutamente richiesto che questa sia allargata per prevedere la criminalizzazione e l'introduzione di un'aggravante per i crimini omofobici. Se questo verrà proposto ed approvato a livello europeo, anche l'Italia, dove ripetutamente una legge simile è stata bocciata, dovrà farlo.

\- Roadmap per i diritti LGBT, sondaggio dell'Agenzia per i Diritti Fondamentali e possibile relazione del PE

Il gruppo liberale e democratico aveva lanciato la proposta di una roadmap per i diritti LGBT e chiesto alla Commissione di prepararla. Tale richiesta è stata poi appoggiata dal PE e da ILGA Europe, e sembra che una serie di Stati siano favorevoli a questa proposta. Nel frattempo il PE ha compiuto uno studio sulla roadmap, in preparazione di una relazione sui diritti delle persone LGBT che potrebbe preparare, anche sulla base del sondaggio che l'Agenzia per i diritti fondamentali lancerà il 17 maggio in occasione della Giornata mondiale contro l'omofobia sulla situazione delle persone LGBT in Europa. Per tale giornata, come in passato, le istituzioni europee sponsorizzeranno una serie di eventi e messaggi di leaders delle istituzioni europee. Chissà se l'Italia si unirà a tali celebrazioni con un messaggio di Napolitano o del primo ministro chiunque esso sia?

\- Gli sviluppi sul matrimonio egualitario e 'pour tous - per tutti' in Francia, in Gran Bretagna e negli USA e la penosamente arretrata situazione italiana

Mentre il matrimonio si apre alle persone dello stesso sesso in Francia ed in Gran Bretagna, e negli USA la Corte Suprema sta esaminando la questione della proibizione dei matrimoni omosessuali decisa da alcuni Stati americani, la classe politica ed i partiti italiani rimangono chiusi su sé stessi come dimostrato in occasione delle ultime elezioni. Se la Bindi si è opposta al matrimonio per tutti, Vendola l'ha rivendicato (per sé almeno), il PD farfuglia di un modello tedesco -inesistente, Monti ha parlato di famiglia secondo i dettami vaticani, noi radicali abbiamo parlato di carceri ed amnistia - pour tous?... Nessun partito insomma ne ha fatto bandiera o tema principale per le elezioni - sebbene i sondaggi Eurispes mostrassero l'interessamento degli elettori per tali temi - come peraltro è accaduto in generale sui diritti civili. O forse per qualunque tema, dato che in Italia la politica non discute di riforme da fare, per poi farle o assicurarsi che vengano fatte se si vincono le elezioni. Anche contro tutto ciò tanti hanno votato Grillo, provando il nuovo con speranza contro il vecchio disperante, rischiando poi di ritrovarsi da capo alle prossime elezioni, appunto perché anche Grillo non sembra mettere gli obiettivi politici di riforma promossi dal suo movimento come fulcro per negoziare un possibile governo per attuarli.

Forse in questa fase di marasma della politica si potrebbe tentare di promuovere come Certi Diritti riforme puntuali, dalla lotta all'omofobia, alle unioni civili ed al matrimonio egualitario, formando un'Alleanza di cittadini e deputati di Grillo, di SEL, del Pd, del PDL, eccetera, per promuovere riforme del diritto di famiglia da approvare subito. Magari partendo proprio dai deputati che partecipano al Congresso di Certi Diritti.

Come iscritto mi auguro che sia possibile fare tutto ciò assieme e partendo innanzitutto dall'area radicale, da Radicali Italiani al Partito Radicale, sperando che l'area esca dalla crisi che sta vivendo assicurando innanzitutto all'Italia ed agli italiani una opzione politica pulita, trasparente, liberale, liberista e libertaria, antiproibizionista, dei diritti umani, di cui c'é tanto bisogno oggi ed in futuro.

Vi ringrazio e vi auguro buon lavoro, a tutti noi!

Ottavio