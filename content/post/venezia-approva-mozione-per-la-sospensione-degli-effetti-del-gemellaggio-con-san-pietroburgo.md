---
title: 'Venezia approva mozione per la sospensione degli effetti del gemellaggio con San Pietroburgo'
date: Tue, 29 Jan 2013 09:18:37 +0000
draft: false
tags: [Russia]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti

Roma, 28 gennaio 2013

Apprendiamo che oggi è stata approvata a piena maggioranza la mozione che la consigliera ha presentato con Bruno Venturini (Unione di Centro) per la sospensione immediata dell'accordo di collaborazione tra il Comune di Venezia e la Città di San Pietroburgo.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: "ringraziamo sentitamente Camilla Seibezzi e Bruno Venturini per l'ottimo lavoro fatto. Dopo Milano, finalmente anche Venezia dà un chiaro segnale in difesa dei diritti umani, della democrazia e dello Stato di diritto. Il voto veneziano è di particolare importanza perché arriva a pochi giorni dall'approvazione, in prima lettura, da parte della Duma di una legge simile a quella di San Pietroburgo che, per Håkon Haugli, rapporteur dell'Assemblea Parlamentare del Consiglio d'Europa (PACE) per i diritti delle persone LGBT, rappresenta "un tentativo di imporre una restrizione alle libertà fondamentali". Viene ora da chiedersi cosa aspetti la Torino, guidata dal democratico Fassino, a unire la propria voce a quella di Milano e Venezia".