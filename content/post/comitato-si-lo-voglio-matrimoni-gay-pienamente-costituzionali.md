---
title: 'COMITATO SI LO VOGLIO: MATRIMONI GAY PIENAMENTE COSTITUZIONALI'
date: Sat, 17 Apr 2010 05:31:37 +0000
draft: false
tags: [Comunicati stampa]
---

COMITATO NAZIONALE "SÌ, LO VOGLIO!"  
"CONSULTA IMPONE RICONOSCIMENTO GIURIDICO DELLE COPPIE OMOSESSUALI. MATRIMONI GAY SONO PIENAMENTE COSTITUZIONALI"

I portavoce del Comitato nazionale "Sì, lo voglio!" in merito alle motivazioni della sentenza n. 138/2010 della Corte Costituzionale .

Abbiamo atteso un giorno dopo la pubblicazione, da parte della Corte Costituzionale, delle motivazioni della sentenza n.138/2010, che ha rigettato il ricorso presentato dalle coppie protagoniste della campagna di "Affermazione Civile", volta ad ottenere l'accesso al matrimonio anche per le persone omosessuali.

Abbiamo atteso un giorno per aver modo di studiare le motivazioni redatte dalla Consulta e per poter ricevere la consulenza dei giuristi che ci hanno affiancato durante questo percorso.

Le motivazioni della sentenza contengono importanti aspetti positivi.

La Corte ha stabilito che la Costituzione Italiana impone il riconoscimento giuridico delle unioni omosessuali e che questo "necessariamente postula una disciplina di carattere generale", attualmente assente. Dunque, la Corte individua nella legislazione ordinaria una lacuna. Tutti - forze politiche e Istituzioni - da oggi dovranno confrontarsi con la presenza di un vuoto legislativo illegittimo per la nostra Costituzione.

Queste le parole della Corte, che crediamo valgano come monito al Parlamento: "Per formazione sociale deve intendersi ogni forma di comunità, semplice o complessa, idonea a consentire e favorire il libero sviluppo della persona nella vita di relazione, nel contesto di una valorizzazione del modello pluralistico. In tale nozione è da annoverare anche l'unione omosessuale, intesa come stabile convivenza tra due persone dello stesso sesso, cui spetta il diritto fondamentale di vivere liberamente una condizione di coppia, ottenendone - nei tempi, nei modi e nei limiti stabiliti dalla legge - il riconoscimento giuridico con i connessi diritti e doveri".

Il secondo aspetto positivo della sentenza è che la Corte reputa plausibile che il legislatore s'attardi - come l'esperienza purtroppo insegna - a riconoscere giuridicamente le coppie omosessuali e pertanto, in permanenza di tale, illegittimo, vuoto, la Consulta stessa si riserva di tutelare le unioni omosessuali ogni qual volta si presentino singole esigenze di tutela omogenee a quelle delle coppie eterosessuali sposate.

La Corte afferma: "Può accadere, infatti, che, in relazione ad ipotesi particolari, sia riscontrabile la necessità di un trattamento omogeneo tra la condizione della coppia coniugata e quella della coppia omosessuale, trattamento che questa Corte può garantire con il controllo di ragionevolezza". Così la Consulta disegna una strada chiara e chiama tutti i giudici italiani, in presenza di tale vuoto di tutela, a garantire le coppie omosessuali in ogni occasione in cui si manifestano esigenze omogenee alle coppie eterosessuali. In mancanza di tutela, la Corte si riserva di dare una lettura costituzionalmente orientata o di cancellare la norma illegittima.

La via giudiziaria all'ottenimento della parità di diritti e dell'uguaglianza non s'è chiusa, come temevano alcuni, con la pronuncia della Corte Costituzionale. Anzi, al contrario, la battaglia giudiziaria per l'ottenimento dei diritti s'apre oggi a mille possibilità, che andranno valutate e ponderate con cura, partendo dalla disponibilità data dalla Corte Costituzionale stessa ad intervenire laddove ci sia una disparità di trattamento tra le coppie eterosessuali sposate e quelle omosessuali.

Riteniamo di poter leggere in prospettiva le motivazioni del rigetto del ricorso in relazione all'art. 29 della Costituzione. La Corte Costituzionale si limita, infatti, ad affermare di non poter intervenire in maniera "creativa" su un caso che non era stato preso in considerazione dai Costituenti per essere messo sotto la tutela dell'art. 29 Cost. Confortati dal parere di importanti giuristi, possiamo affermare, però, che questa argomentazione è debole e si presta ad essere rivista in futuro, così come altre simili decisioni sono state superate in passato.

Quel che è certo è che la Corte Costituzionale spazza via ogni dubbio circa la rilevanza costituzionale delle unioni fra persone dello stesso sesso e conferma la legittimità costituzionale del matrimonio civile per le coppie omosessuali, che è una delle scelte possibili del legislatore, a cui affida il compito di stabilire in quali forme avverrà il riconoscimento giuridico delle coppie omosessuali, che è però necessario. Il Comitato "Sì, lo voglio!" si augura quindi di non dover ascoltar più, tanto a destra quanto a sinistra, politici sostenere l'incompatibilità dei matrimoni gay con la Costituzione Italiana.

Il Parlamento italiano non può più eludere l'impegno di dare una disciplina alle unioni omosessuali. Ai rappresentati politici tutti e specialmente a quelli che si dicono progressisti e liberali spetta ora l'obbligo di dare sostanza a un diritto chiaramente riconosciuto.

Il Comitato invita quanti hanno esultato sostenendo tale tesi a leggere con la dovuta attenzione le motivazioni della sentenza. Nella fretta di portare a casa una vittoria, ancor prima di conoscere le ragioni della Corte, hanno clamorosamente sbagliato le loro previsioni.

  
Il Comitato "Sì, lo voglio!" ringrazia di cuore tutte le coppie che, con la loro disponibilità, hanno permesso al movimento LGBT e all'Italia tutta di compiere tre fondamentali passi in avanti verso l'ottenimento dei diritti civili per le unioni omosessuali. E' un cammino che continueremo a percorrere assieme.

  
**Per il Comitato nazionale "Sì, lo voglio!", i portavoce**

Imma Battaglia (Di'Gay Project)  
Enzo Cucco (Associazione radicale Certi Diritti)  
Maurizio Cecconi (Rete Laica Bologna)  
Paolo Patanè (Arcigay)  
Francesca Polo (Arcilesbica)

**Al Comitato nazionale "Sì, lo voglio!" attualmente aderiscono**

3D - Democratici per pari Diritti e Dignità di lesbiche, gay, bisessuali e trans  
Associazione Culturale ARC di Cagliari  
AGEDO - Associazione Genitori di Omosessuali  
Associazione Radicale Certi Diritti  
Associazione Radicale Enzo Tortora di Milano  
Associazione Radicale Giorgiana Masi di Bologna  
Associazione Viottoli - Comunità cristiana di base di Pinerolo (To)  
Comitato Gay e Lesbiche Prato  
Comitato Provinciale Arcigay Firenze "Il Giglio Rosa" O.N.L.U.S.  
Comitato Provinciale Arcigay Verona "Pianeta Urano"  
Como Gay Lesbica  
Coordinamento Torino Pride LGBT  
Di' Gay Project  
Federazione delle chiese evangeliche in Italia  
Fondazione Critica Liberale  
Fondazione Luciano Massimo Consoli  
GAM - Gruppo alternativo motociclisti gay e lesbiche  
GayLib  
Gruppo Pesce Milano  
Gruppo Pesce Roma  
Ké Group  
Keshet, vita e cultura ebraica  
[italialaica.it](http://italialaica.it/)  
Liberacittadinanza - Rete girotondi e movimenti  
Le Ninfe - GenovaGaya  
Renzo e Lucio  
Rete Laica Bologna  
UAAR - Unione degli Atei e degli Agnostici Razionalisti