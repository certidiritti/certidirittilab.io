---
title: 'Gran Bretagna: 8 coppie etero gay lesbiche fanno ricorso alla CEDU'
date: Wed, 02 Feb 2011 14:42:18 +0000
draft: false
tags: [Affermazione Civile, CEDU, certi diritti, civil partnership, Comunicati stampa, coppie, CORTE COSTITUZIONALE, d'amico, equal love, etero, GAY, matrimonio gay, out rage, pink news, ricorsi, rovasio, Sentenza, uk, unioni civili]
---

Otto coppie inglesi, quattro eterosessuali e quattro gay, hanno oggi depositato ricorsi alla Corte Europea dei Diritti dell'Uomo contro la legislazione discriminatoria sul matrimonio e sulla civil-partnership del Regno Unito.

Roma, 2 febbraio 2011

**Comunicato stampa dell’Associazione Radicale Certi Diritti**

Otto coppie inglesi, quattro eterosessuali e quattro gay/lesbiche, hanno depositato ciascuna un ricorso finalizzato al superamento delle diseguaglianze in materia di diritto di famiglia nel Regno Unito.

Le quattro coppie eterosessuali hanno fatto ricorso perché non è consentito loro di accedere alla Civil Partnership, la legge che regolamente le unioni civili solo per le coppi omosessuali; le quattro coppie gay/lesbiche hanno invece fatto ricorso perché non è consentito loro di accedere all’istituto del matrimonio civile.

Il ricorso delle otto coppie dimostra che la lotta alle discriminazioni riguarda tutte/tutti i cittadini e non è una questione che riguarda soltanto le persone lesbiche e gay,  in Gran Bretagna così come in tutti i paesi europei e democratici.

La campagna fa parte di una iniziativa promossa dall’organizzazione inglese ‘Out Rage’,  alla quale hanno aderito diverse organizzazioni, ed è stata denominata  ‘Equal=Love’. L’iniziativa è seguita da un team di avvocati che si battono per il superamento delle diseguaglianze.

Il sito ufficiale della campagna [http://equallove.org.uk/](http://equallove.org.uk/)

Il logo della campagna: [http://www.certidiritti.it/images/fbfiles/images/equal\_love\_logo_blog.gif](http://www.certidiritti.org/wp-content/uploads/2011/02/equal_love_logo_blog.gif)

ANCHE CERTI DIRITTI CERCA COPPIE PER CAUSE PILOTA A LIVELLO EUROPEO.  
**[PER SAPERNE DI PIU' >](campagne/diritto-al-matrimonio/affermazione-civile-2011.html)**