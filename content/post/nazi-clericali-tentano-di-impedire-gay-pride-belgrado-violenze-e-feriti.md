---
title: 'NAZI-CLERICALI TENTANO DI IMPEDIRE GAY PRIDE BELGRADO, VIOLENZE E FERITI'
date: Sun, 10 Oct 2010 13:25:11 +0000
draft: false
tags: [Comunicati stampa]
---

**VIOLENZE A GAY PRIDE DI BELGRADO: GRAVISSIMO QUANTO ACCADUTO, LA VIOLENZA OMOFOBA DEI GRUPPI NAZIONALISTI E DEL CLERO CONFERMA DA CHE PARTE STA L’ODIO E IL PREGIUDIZIO, IN SERBIA E IN TUTTO IL MONDO.**

Roma, 10 ottobre 2010

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

“Quanto accaduto oggi per le vie di Belgrado, dove le forze dell’ordine sono oggi duramente intervenute per difendere lo svolgimento del gay pride, contro le violenza di gruppi nazifascisti e clericali, è gravissimo. Il tentativo di colpire la persone lgbt(e) in Serbia così come in molti paesi del mondo dimostra di quanto sia diffuso l’odio e il pregiudizio. Non è un caso se tali manifestazioni di odio avvenute oggi a Belgrado, che hanno causato oltre 80 feriti nel corso di una vera e propria guerriglia urbana ingaggiata dai fanatici contro le forze dell’ordine, sono state promosse dai nazionalisti serbi e alcuni gruppi religiosi ortodossi insieme ad alcuni preti che all’urlo di ‘morte ai froci’, ‘la caccia è cominciata’ e ‘siamo qui per dire a tutti che quelli sono malati e che solo dio li può salvare’ hanno pure dato fuoco a due sedi di partito. E’ forse utile anche ricordare che ieri sera si era svolta una manifestazione denominata ‘passeggiata della famiglia’.

Secondo le notizia che ci sono giunte da un gruppo di persone che hanno partecipato al gay pride blindato, ci sarebbero alcune persone ferite in modo molto grave. Sembra che il Governo, le autorità e le forze dell’ordine abbiano fatto di tutto per garantire lo svolgimento della manifestazione anche se il clima di paura e spavento era diffusissimo tra i manifestanti.

Ci auguriamo che il rappresentante dell’Unione Europea in Serbia, Vincent Degert e dell’Osce, Dimitri Kipreos, che avevano annunciato la loro presenza al gay pride, riferiscano quanto prima su quanto accaduto”.