---
title: 'WIKI-RIGHTS, pubblicati i file riservati del Consiglio Europeo che bloccano la Direttiva antidiscriminazione della Commissione'
date: Fri, 10 Dec 2010 09:41:25 +0000
draft: false
tags: [certi diritti, commissione, Comunicati stampa, diritti civili, diritti umani, parlamento europeo, trasparenza, unione europea, wiki-rigths]
---

**WIKI-RIGHTS: IL 10 DICEMBRE, GIORNATA MONDIALE DIRITTI UMANI, PUBBLICATI I DOCUMENTI RISERVATI DEL CONSIGLIO EUROPEO DELL'UE. ECCO LA PROVA CHE ALCUNI PAESI, CON ARGOMENTI FITTIZI, BLOCCANO LA DIRETTIVA ANTIDISCRIMINAZIONI DELLA COMMISSIONE EUROPEA.**

**Certi Diritti lancia "Operazione trasparenza" sulla direttiva UE bloccata. Chiede ai governi italiano, tedesco e ceco di interrompere il boicottaggio.**

**Comunicato Stampa dell'Associazione Radicale Certi Diritti (inviato alla stampa europea accreditata a Bruxelles e alle principali organizzazioni lgbt europee):**

Bruxelles – Roma, 10 dicembre 2010

Il 10 dicembre 2010, in occasione della Giornata mondiale per i Diritti Umani, l’Associazione Radicale Certi Diritti ha lanciato l’"operazione trasparenza" sulla direttiva anti-discriminazioni proposta dalla Commissione Europea  nel 2008, pubblicando sul suo sito tutti i documenti del Consiglio di cui siamo venuti in possesso e che rivelano l'opposizione costante e pretestuosa di alcuni Governi, in particolare di quello italiano, tedesco e ceco, rispetto all'eguaglianza dei cittadini senza discriminazione, con l’obiettivo di tenere bloccata tale Direttiva.

La direttiva anti-discriminazioni, se approvata, permetterebbe di allargare la lotta contro le discriminazioni basate sull'orientamento sessuale, sull'età, sulla religione e sulla disabilità dal solo campo del lavoro (già coperto dalla direttiva in vigore 200/78/CE) anche ai campi della sicurezza sociale, dell'educazione, dell'accesso ai beni e servizi, includendo l'accesso all'abitazione ed alla casa.

In occasione della discussione sulla situazione della Direttiva nel Consiglio, svoltasi al Parlamento Europeo, in  Commissione Libertà Pubbliche  lo scorso 30 novembre, la Presidenza belga ha affermato che numerosi Stati membri hanno sollevato costantemente veti ed obiezioni in merito al presunto non rispetto dei principi di sussidiarietà, proporzionalità e sicurezza giuridica; si tratta ovviamente di pretesti politici per bloccare la Direttiva. La stessa Presidenza belga di turno ha chiesto ai Governi di sbloccare i lavori.

La Commissaria Reding, in rappresentanza della Commissione Europea, ha tenuto incontri con i Governi nazionali sperando di sbloccare la situazione, senza successo. Per la Commissione Europea  rimane importante assicurare che la direttiva non sia smembrata in 4 direttive distinte (sulla religione, sull'orientamento sessuale, sull'età e sulla disabilità).

**L’Associazione Radicale Certi Diritti ha così deciso di rendere pubblici , per la prima volta,   oltre 70 file del Consiglio Europeo dell'UE, indirizzati al Working Party on Social Question, dove sono documentate per iscritto le proposte e le obiezioni, anche quelle fittizie, dei vari Stati membri rispetto alla Direttiva della Commissione.**

**I file, in inglese, sono consultabili al seguente link:**

**[www.certidiritti.it/wiki-rights.html](wiki-rights.html)**

[info@certidiritti.it](mailto:info@certidiritti.it)