---
title: 'Governo affida la lotta all''Aids a Giovanardi'
date: Thu, 26 May 2011 11:44:31 +0000
draft: false
tags: [Salute sessuale]
---

**Roma, 26 maggio 2011. Le politiche di prevenzione di lotta contro Hiv/Aids in Italia e nel mondo sono state consegnate nelle mani del sottosegretario alla Presidenza del Consiglio con delega alla Droga e alla Famiglia Carlo Giovanardi.**

In vista del prossimo Meeting di Alto Livello dell'ONU in materia di Hiv/Aids (UNGASS) che si terrà a New York ai primi di giugno, le regole le detta infatti il Dipartimento  Politiche Antidroga che fa capo alla Presidenza del Consiglio, come chiaramente emerso nell'incontro preparatorio al Meeting che si è svolto lunedì scorso presso il ministero degli Affari esteri.

Come già denunciato dalle associazioni italiane, l'unica (l'unica!) proposta avanzata in sede Onu da parte del governo italiano è la cancellazione della dicitura “riduzione del danno” dal documento su Hiv/Aids che verrà licenziato dal Meeting Onu e che dovrà essere adottato dagli Stati membri. Una proposta sostenuta solamente, oltre che dall'Italia, da Vaticano, Russia e Iran. Alla quale va ora aggiunta la richiesta di cancellazione della dicitura che fa riferimento dei programmi con terapie sostitutive per gli oppiacei (per esempio metadone e buprenorfina), come risulta da documenti ufficiali.

Tale posizione è stata ribadita durante la citata riunione da Giovanni Serpelloni, Capo del Dipartimento Politiche Antidroga, che ha motivato la volontà di cassare la dicitura specifica “riduzione del danno” per sostituirla con quella generica di “riduzione del rischio”, con il timore che tale terminologia possa aprire le porte a provvedimenti quali l'avvio di stanze per l'autoconsumo sicuro di stupefacenti e i programmi sostitutivi con eroina per consumatori che abbiano fallito in precedenti programmi di recupero. Un timore peraltro infondato, dato che sia le agenzie sia le associazioni hanno ben dimostrato, storicamente e in diverse sedi, di avere altre priorità e di difendere la definizione di riduzione del danno per motivazioni che nulla hanno a che vedere con le posizioni personali o il consenso elettorale.

Ora il veto italiano alla definizione di riduzione del danno rischia di minare l'intervento globale contro la diffusione dell'Hiv come descritto e promosso dalle maggiori agenzie sanitarie e non solo: UNODC, UNAIDS, Organizzazione mondiale della Sanità, Commissione Europea e molte altre. In molti Paesi i pochi interventi sanitari rivolti ai consumatori di sostanze sono fondati proprio sull'appoggio delle agenzie internazionali.

Le associazioni di lotta all'Aids e di difesa dei diritti civili intendono denunciare e osteggiare tale irresponsabile comportamento in tutte le sedi possibili, nazionali e internazionali. Anche tenendo conto dell'impotenza più volte espressa, da parte di rappresentanze ufficiali, a fronte di direttive indiscutibili che “provengono direttamente da Roma”.

Le associazioni sottolineano inoltre l'inopportunità della battaglia governativa di retroguardia anche alla luce della ormai consolidata incapacità o assenza dell'Italia nel far fronte a vecchie e nuove emergenze in tema di Hiv/Aids. Campagne sui preservativi più uniche che rare, timide e inefficaci, nessuna campagna di prevenzione diretta a uomini che fanno sesso con uomini, dei quali si nega semplicemente l'esistenza, così come per lavoratrici e lavoratori del sesso e loro clienti, e sono solo alcuni esempi. Disimpegno nella lotta globale contro Hiv/Aids, in seguito al mancato versamento dei contributi 2009 e 2010 promessi al Fondo Globale per la Lotta contro l’Aids, la Tubercolosi e la Malaria, e alla mancanza di impegni finanziari futuri verso lo stesso.

Scelte di non intervento che in questo Paese conosciamo bene: non si fa alcunché, perché sennò poi chissà che succede. E così quello che succede è che le infezioni in Italia sono in aumento e che nel mondo si continua a infettarsi e a morire di Aids nonostante esistano terapie in grado di evitare la progressione della malattia e di frenare il contagio, anche con la complicità italiana. Senza contare l'enorme costo, economico e di vite umane, che la mancata prevenzione sempre comporta.

E ora la scelta di affidare l'ultima parola sulla posizione italiana nella lotta contro l'Aids al sottosegretario Giovanardi e al suo Dipartimento Antidroga, presente in una riunione in cui ha brillato di converso l'assenza del ministero della Salute  che, invece, nel resto del mondo e anche in Italia è il vero regista delle politiche di lotta all'Aids sia sul piano nazionale che internazionale. Giovanardi, il senatore che si è appena scagliato contro la sobria pubblicità dell'Ikea che raffigurava una famiglia composta da due persone dello stesso sesso. Che vota da sempre contro ogni proposta di legge che condanni l'omofobia. Che ha sostenuto in Parlamento che non vi è dimostrazione che il preservativo sia efficace nella prevenzione dell'infezione da Hiv. Che alla morte di un ragazzo come Stefano Cucchi ha sentenziato: ”Cucchi è morto perché drogato e sieropositivo”.

Le associazioni chiedono al governo italiano di rivedere la propria posizione e di sospendere la richiesta di stralcio della definizione “riduzione del danno” dal documento che verrà presentato in sede ONU. Chiedono inoltre che siano rispettate le funzioni delle persone incaricate sia in Italia che nelle sedi internazionali di occuparsi di lotta contro l'Hiv/Aids, evitando dirigismi che finiscono col mutilare le normali attività di consultazione e lobbing con  i corrispettivi esteri. Chiedono poi di conoscere al più presto come sarà composta la delegazione di New York e se sia stata prevista, e in quale forma, la presenza del mondo delle associazioni così come raccomandato dalle stesse Nazioni Unite.

Chiedono infine al governo di chiarire quali sono le sue priorità in materia di lotta contro Hiv/Aids. Se la linea è quella di assecondare le spinte del sottosegretario Giovanardi o di avviare finalmente adeguate politiche di prevenzione a tutela della salute pubblica fondate sull'evidenza scientifica e sulle indicazioni delle agenzie internazionali piuttosto che su posizioni ideologiche che rischiano di far tornare indietro di 20 anni la lotta all'Aids. Ovvero se intende presentarsi nelle sedi internazionali con una faccia e una posizione credibili, o incredibili.

LILA – Lega Italiana per la Lotta contro l'Aids  
ANLAIDS  
NADIR ONLUS  
Osservatorio Italiano sull’Azione Globale contro l’AIDS  
NPS Italia Onlus  
ARCIGAY  
MIT Movimento identità Transessuale  
MARIO MIELI Circolo di Cultura Omosessuale  
CDCP Comitato per i Diritti Civili delle Prostitute  
CNCA – Coordinamento Nazionale delle Comunità di Accoglienza  
Villa Maraini  
FORUM DROGHE