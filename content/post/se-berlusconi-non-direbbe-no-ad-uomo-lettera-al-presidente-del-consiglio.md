---
title: 'Se berlusconi non direbbe  no ad uomo... Lettera al Presidente del Consiglio'
date: Thu, 23 Dec 2010 11:09:10 +0000
draft: false
tags: [Senza categoria]
---

Buongiorno,

leggo dai giornali che il Presidente del Consiglio Onorevole Silvio Berlusconi abbia detto: "Io non so dire dei no, non l'ho mai saputo fare e la mia fortuna è stata che nessun gay è venuto mai a farmi una proposta perché alla terza volta avrei chiesto di spiegarmi tecnicamente come si fa e ci sarei stato".

Buongiorno,

leggo dai giornali che il Presidente del Consiglio Onorevole Silvio Berlusconi abbia detto: "[Io non so dire dei no, non l'ho mai saputo fare e la mia fortuna è stata che nessun gay è venuto mai a farmi una proposta perché alla terza volta avrei chiesto di spiegarmi tecnicamente come si fa e ci sarei stato](http://www.corriere.it/politica/speciali/2010/la-fiducia/notizie/14-12-barzelletta-gay_65847f2c-0759-11e0-a25e-00144f02aabc.shtml)".

Ecco, le fortune di Silvio sono terminate.

Sono gay. Cittadino italiano.

E sono qui per chiedere al Presidente del Consiglio Onorevole Silvio Berlusconi di avere un rapporto con me.

Sono qui per chiedere al Presidente del Consiglio Onorevole Silvio Berlusconi di avere un rapporto con me.

Sono qui per chiedere al Presidente del Consiglio Onorevole Silvio Berlusconi di avere un rapporto con me.

Bene. L'ho chiesto tre volte, e ora non saprà dire di no. Gli spiegherò anche come si fa.

Con il denaro che avrò in cambio per la sua istintiva generosità, realizzerò il mio sogno di scrivere un romanzo. Il romanzo avrà successo (solo per il fatto che è stato scritto dal primo e unico uomo che abbia avuto un rapporto sessuale con Silvio Berlusconi) e con i soldi del romanzo finanzierò campagne pubbliche che favoriscano una cultura più inclusiva nei confronti di tutte le diversità.

Vi ringrazio.

Se riuscite a pubblicare questa lettera e a farla giungere al Presidente del Consiglio prima delle 11:30 la mia proposta sarà ancora considerata tecnicamente valida.

Altrimenti, nel caso non passasse la fiducia, si consideri il mio appello rivolto a Silvio Berlusconi come cittadino, e non come Presidente del Consiglio.

Con simpatia e fiducia nella comprensione del Premier... se passasse la fiducia al governo, questo incontro me lo deve eccome!

Gian Mario Felicetti