---
title: 'Sabato 15-1 visita radicali presso reparto transessuali Carcere di Rebibbia'
date: Sat, 15 Jan 2011 08:02:13 +0000
draft: false
tags: [Comunicati stampa]
---

**TRANSESSUALI DETENUTE: SABATO 15 GENNAIO DELEGAZIONE DELL’ASSOCIAZIONE RADICALE CERTI DIRITTI IN VISITA AL CARCERE DI REBIBBIA.**

Roma, 14 gennaio 2011

Sabato 15 gennaio 2011, alle ore 11, una delegazione dell’Associazione Radicale Certi Diritti composta dalla Presidente Rita Bernardini, deputata radicale del Pd, Sergio Rovasio, Segretario e da Leila Deianis, Presidente dell’Associazione Libellula, terrà una visita nel Carcere di Rebibbia. E’ prevista anche una visita, tra gli altri, presso il reparto delle persone transessuali.