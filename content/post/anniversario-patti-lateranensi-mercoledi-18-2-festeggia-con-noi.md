---
title: 'ANNIVERSARIO PATTI LATERANENSI: MERCOLEDI'' 18-2 FESTEGGIA CON NOI'
date: Tue, 17 Feb 2009 08:00:24 +0000
draft: false
tags: [Comunicati stampa]
---

ANNIVERSARIO PATTI LATERANENSI: IL 18 FEBBRAIO I RADICALI, INSIEME A DECINE DI ASSOCIAZIONI LAICHE, RICORDERANNO IN PIAZZA LA FIRMA DEL CONCORDATO: "L'INGERENZA NON ERA NEI PATTI".

Domani, mercoledì 18 febbraio, in occasione dell'80° anniversario della firma dei Patti Lateranensi, e della revisione del Concordato tra Stato italiano e Chiesa Cattolica, del 18 febbraio 1984, diverse Associazioni e personalità, promuovono dalle ore 15 alle ore 19 un Sit-in a Roma, davanti alla sede dell'Ambasciata italiana presso la Santa Sede in Viale delle Belle Arti, 2, dove è previsto un ricevimento delle massime autorità clericali e vaticane con i massimi rappresentanti dello Stato italiano.

Alla manifestazione "L'ingerenza non era nei Patti" hanno finora aderito e parteciperanno: Radicali Italiani, l'Associazione Radicale Certi Diritti, Radicali Roma, l'Associazione Luca Coscioni, l'Associazone per il Divorzio Breve, insieme alla Federazione romana del Partito Socialista, il Forum delle Donne Socialiste, l'Associazione Rosarcobaleno, il Circolo Mario Mieli e il Circolo UAAR di Roma. Alla manifestazione hanno assicurato la loro presenza dirigenti e parlamentari radicali insieme a personalità che si battono per la libertà e la laicità dello Stato.

Molto probabilmente parteciperà anche Marco Pannella.

Domani, mercoledì 18 febbraio, in occasione dell'80° anniversario della firma dei Patti Lateranensi, e della revisione del Concordato tra Stato italiano e Chiesa Cattolica, del 18 febbraio 1984, diverse Associazioni e personalità, promuovono dalle ore 15 alle ore 19 un Sit-in a Roma, davanti alla sede dell'Ambasciata italiana presso la Santa Sede in Viale delle Belle Arti, 2, dove è previsto un ricevimento delle massime autorità clericali e vaticane con i massimi rappresentanti dello Stato italiano.

Alla manifestazione "L'ingerenza non era nei Patti" hanno finora aderito e parteciperanno: Radicali Italiani, l'Associazione Radicale Certi Diritti, Radicali Roma, l'Associazione Luca Coscioni, l'Associazone per il Divorzio Breve, insieme alla Federazione romana del Partito Socialista, il Forum delle Donne Socialiste, l'Associazione Rosarcobaleno, il Circolo Mario Mieli e il Circolo UAAR di Roma. Alla manifestazione hanno assicurato la loro presenza dirigenti e parlamentari radicali insieme a personalità che si battono per la libertà e la laicità dello Stato.

Molto probabilmente parteciperà anche Marco Pannella.