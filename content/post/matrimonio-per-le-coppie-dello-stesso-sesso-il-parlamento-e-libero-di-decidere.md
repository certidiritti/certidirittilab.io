---
title: 'Matrimonio per le coppie dello stesso sesso: il Parlamento è libero di decidere'
date: Thu, 20 Sep 2012 08:13:42 +0000
draft: false
tags: [Matrimonio egualitario]
---

Appello al Parlamento di giuristi e docenti universitari sul matrimonio tra persone dello stesso sesso.  
Da anni la questione del matrimonio omosessuale occupa un posto rilevante nell’agenda politica. Ferma naturalmente la piena legittimità dei diversi orientamenti valoriali, e delle conseguenti opinioni sulle proposte legislative in discussione, riteniamo che debba esserci chiarezza in ordine ai precetti costituzionali che regolano il tema.  
  
Com’è noto, più di un Giudice sottopose la normativa oggi vigente al vaglio della Corte Costituzionale, ritenendo discriminatoria l’esclusione del matrimonio in ragione del solo orientamento sessuale, e la questione fu decisa dalla Consulta con la sentenza 138 del 2010.  
  
Il giudizio della Corte regolatrice fu che **il riconoscimento – esplicitamente formulato – del valore costituzionale della formazione sociale costituita da persone del medesimo sesso non comportasse necessariamente il diritto al matrimonio, con espresso invito al legislatore a provvedere sul punto.** Si deve purtroppo constatare come questo autorevole monito non sia stato ascoltato.  
  
Se è dunque auspicio che la prossima legislatura affronti e decida la questione, **dev'essere sottolineato che – secondo l’opinione pur restrittiva della Corte – il legislatore, nell’esercizio della sua discrezionalità, non è vincolato da un divieto della Carta costituzionale, che non prevede e non impone che il matrimonio sia riservato alle coppie eterosessuali.**  
  
Il fatto che sia stata esclusa la sussistenza di una discriminazione nell’attuale limitazione dell’accesso all’istituto matrimoniale non significa infatti che questa limitazione sia costituzionalmente obbligatoria ed ineludibile, posto che una previsione siffatta non c’ è nella nostra Costituzione (e infatti la stessa sentenza 138 non postula questo divieto).  
  
Il Parlamento italiano discuterà dunque in piena libertà: il nostro auspicio è che pervenga alla decisione che garantisca la maggior libertà e la più forte inclusività sociale per tutti i cittadini.

MARILISA D'AMICO, Università di Milano  
ALBERTO ALESSANDRI, Università commerciale Bocconi  
ALESSANDRO BERNARDI, Università di Ferrara  
ERNESTO BETTINELLI, Università di Pavia  
PAOLO BIANCHI, Università di Camerino  
NERINA BOSCHIERO, Università di Torino  
GIUDITTA BRUNELLI, Università di Ferrara  
MIA CAIELLI, Università di Torino  
EVA CANTARELLA, Università di Milano  
PAOLO CARETTI, Università di Firenze  
ROBERTA DE MONTICELLI, Università Vita-Salute San Raffaele  
GIANMARIO DEMURO, Università di Cagliari  
GIOVANNI DI COSIMO, Università di Macerata  
ALFONSO DI GIOVINE, Università di Torino  
EMILIO DOLCINI, Università di Milano  
CARLO FLAMIGNI, Università di Bologna  
NATALINA FOLLA, Università di Trieste  
CARLO FUSARO, Università di Firenze  
MARCO GEREMIA, Avvocato  
PAOLO GIANGASPERO, Università di Trieste  
MAURO GRONDONA, Università di Genova  
RICCARDO GUASTINI, Università di Genova  
GUIDO NOTO LA DIEGA, Università di Palermo  
PAOLO MAGRO, Università di Palermo  
ANNA MARIA MAUGERI, Università di Catania  
LUCA NIVARRA, Università di Palermo  
LUIGI PANNARALE, Università di Bari  
VANIA PATANE’, Università di Catania  
BARBARA PEZZINI, Università di Bergamo  
CLAUDIA PECORELLA, Università di Milano - Bicocca  
ANNA PINTORE, Università di Cagliari  
CARLO ALBERTO REDI, Università di Pavia e Accademico dei Lincei  
SAVERIO REGASTO, Università di Brescia  
PAOLO RIDOLA, Università di Roma 'La Sapienza'  
EMILIA ROSSI, Avvocato penalista Torino  
MELANIA SALAZAR, Università di Reggio Calabria  
MARCO SCARPATI, Università di Parma  
FABIO SOMMOVIGO, Università di Bologna  
MAURO VOLPI, Università di Perugia  
ROBERTO TONIATTO, Università di Trento  
FEDERICO ZAPPINO, Università di Sassari  
ROSA ZAMBIANCHI, Avvocato civilista Milano  
  
I giuristi e docenti unviersitari che vogliono sottoscrive l'appello  
possono inviare una mail a info@certidiritti.it  
  
**[Sostieni l'associazione radicale Certi Diritti >>>](partecipa/iscriviti)**