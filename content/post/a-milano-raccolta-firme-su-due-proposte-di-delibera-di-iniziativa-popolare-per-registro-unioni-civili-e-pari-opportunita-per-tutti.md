---
title: 'A Milano raccolta firme su due proposte di delibera di iniziativa popolare per registro unioni civili e pari opportunità per tutti'
date: Thu, 08 Mar 2012 12:45:23 +0000
draft: false
tags: [Politica]
---

A Milano L’Associazione radicale Certi Diritti ha lanciato una raccolta firme su due proposte di delibera di iniziativa popolare per avviare un percorso partecipato su due temi di grande urgenza e importanza:

**Unioni Civili:** istituire un registro delle Unioni Civili, impegnando anche Assessorati e Uffici competenti a tutelare e sostenere le unioni civili, al fine di superare situazioni di discriminazione e favorirne l'integrazione e lo sviluppo nel contesto sociale, culturale ed economico del territorio;

**Pari Opportunità per Tutti:** per contrastare e prevenire le forme di discriminazione basate sul genere, la disabilità, l'orientamento sessuale, l'età, l’etnia e il credo religioso; anche attraverso l'istituzione di un Settore specifico che svolga funzioni di monitoraggio, ricerca e assistenza alle vittime di discriminazioni dirette e indirette.

**segui la campagna su www.metticilafirma.it**  
  

Tra i sostenitori dell’iniziativa ci sono:

ELIO DE CAPITANI, regista, attore, autore, teatro dell’Elfo

MONI OVADIA, attore teatrale, drammaturgo, scrittore, compositore e cantante

FERDINANDO BRUNI, attore, regista, autore, scenografo, teatro dell’Elfo

IDA MARINELLI, attrice, teatro dell’Elfo

MARCO PESATORI, scrittore e astrologo

DIEGO PASSONI, conduttore radiofonico, conduttore televisivo e ballerino

ROSARIA IARDINO, Equality Milano

LORELLA ZANARDO, Il corpo delle donne

MARCO MORI, CIG - ArcigayMilano

CLAUDIO CIPELLETTI, regista italiano

FELIX COSSOLO, giornalista fondatore del mensile "Babilonia"

FRANCESCA PARDI, Famiglie Arcobaleno

MARIA SILVIA FIENGO, Famiglie Arcobaleno

ASA

GAYSTATALE

GIUSEPPE ROTONDO, insieme per il PD

FACCIAMO RETE

LUCA FORMENTON, presidente del gruppo editoriale Il Saggiatore

ANNA MARIA MARCONI, Direttore della Clinica Ostetrica e Ginecologica, AO San Paolo

MARIA PAOLA CANEVINI, docente di Neurofisiopatologia

VITTORIO LINGIARDI, ordinario di Psicologia Dinamica

PAOLO RIGLIANO, psichiatra e psicoterapeuta

VITTORIO ANGIOLINI, ordinario di Diritto Costituzionale

MASSIMO CLARA, avvocato

EVA CANTARELLA, ordinario di Istituzioni di Diritto romano e di Diritto greco antico