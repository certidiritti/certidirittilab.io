---
title: 'FINALMENTE IL GOVERNO SI MUOVE CONTRO L''OMOFOBIA'
date: Thu, 12 Nov 2009 17:06:16 +0000
draft: false
tags: [Comunicati stampa]
---

ERRORE ESCLUDERE LE PERSONE TRANSESSUALI, LE PIU' COLPITE DALLA VIOLENZA. ORA OCCORRE MUOVERSI PER LEGGE CONTRO L'OMOFOBIA.  
  
COMUNICATO STAMPA DELL'ASSOCIAZIONE RADICALE CERTI DIRITTI  
  
Finalmente in Italia è stata aperta la prima breccia nel muro del silenzio  
istituzionale contro l'omofobia.  
  
L'associazione radicale Certi Diritti plaude all'iniziativa del ministero per le pari opportunità che ha varato una campagna mediatica (tv, giornali, cartellonistica stradale e sugli autobus e brochure da distribuire nelle scuole) contro le discriminazioni basate sull'orientamento sessuale.  
  
Eterosessuale, omosessuale, non importa.E' questo il forte messaggio che lo spot manderà al popolo italiano. L'unico diverso da stigmatizzare, nel filmato presentato ieri dal ministro Carfagna, è quello che si taccia di reati di omofobia.  
Gran bel risultato per un governo di centro destra che sdogana pubblicamente, per la prima volta in questo Paese, la parola omofobia.  
  
Accanto ai meritati, convinti e dovuti plausi però l'Associazione radicale Certi diritti deve evidenziare una grave mancanza. La campagna non riguarda le persone transessuali. Forse sarebbe bastato ricordare che l'Italia ha il record nel mondo di  violenze e omicidi contro le persone trans. La transfobia poteva e doveva quindi entrare di diritto in questa campagna e sarebbe davvero bastato un niente: scrivere la parola transessuale accanto agli altri orientamenti.  
Speriamo quindi  che le assicurazioni  del ministro Carfagna di occuparsi di questo problema siano reali e concrete.  
  
Da oggi la caduta a domino dei pregiudizi sarà più facile. Sarà più facile ragionare su una buona legge contro l'omofobia  ( l'estensione della legge Mancino potrebbe essere la soluzione) e chiedere , per le coppie formate da persone dello stesso, l'accesso al matrimonio. A patto però che il centro sinistra ritrovi i suoi ideali di laicità ed uguaglianza, che il centrodestra continui a livellarsi agli standard europei e che la comunità lgbt non abbassi la guardia e, con coraggio, chieda i diritti senza ribassi e senza sconti.  
  
Clara Comelli  
Presidente Associazione Radicale Certi Diritti

Di seguito il link allo [spot contro l'omofobia](index.php?option=com_content&view=article&id=497&catid=23&Itemid=85)