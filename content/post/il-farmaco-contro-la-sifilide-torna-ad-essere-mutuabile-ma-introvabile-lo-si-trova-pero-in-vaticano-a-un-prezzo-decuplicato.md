---
title: 'Il farmaco contro la sifilide torna ad essere mutuabile, ma introvabile. Lo si trova però in Vaticano a un prezzo decuplicato'
date: Thu, 21 Jun 2012 09:30:18 +0000
draft: false
tags: [Salute sessuale]
---

Da fascia 'C' è passato alla fascia 'A', lo dice la gazzetta ufficiale del 4 giugno scorso. Il farmaco è ancora introvabile a Roma nonostante l'aumento preoccupante dei casi di sifilide.

Roma, 20 giugno 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Con una nota pubblicata sulla Gazzetta Ufficiale n. 128 del  4 giugno scorso,  il farmaco Benzilpenicillina-Benzatinicafiala (iniettabile per uso IM1.200.000 UI/2,5) necessario per la cura della sifilide è stato inserito dalla Fascia C alla fascia A, quindi a carico del Servizio Sanitario Nazionale.  Nelle scorse settimane, Massimo Frana,  rappresentante delll’Associazione Radicale Certi Diritti, aveva denunciato l’odissea di un ragazzo brasiliano che, da lui accompagnato, si era visto prescrivere dal centro Malattie Sessualmente Trasmissibili  dell’Ospedale Policlinico di Roma il farmaco senza che lo riuscisse a trovare nemmeno a pagamento in almeno sei farmacie di Roma.  Dovettero rivolgersi alla farmacia del Vaticano dove il medicinale costa più di dieci volte del prezzo di quando era reperibile (le fiale con l’anestetico costano 25 Euro l’una mentre quelle senza, che procurano un fortissimo dolore, costano 6 Euro l’una). Su questa vicenda i parlamentari radicali e i Consiglieri Regionali Radicali del Lazio,  avevano depositato due distinte interrogazioni urgenti ancora senza risposta.

E’ davvero sorprendente che mentre a Roma e in Italia aumentano i casi di sifilide il farmaco continui ad essere irreperibile nella Capitale e invece reperibilissimo nella farmacia dello Stato Vaticano.

Non sappiamo se la decisione di rendere il farmaco mutuabile derivi dalla nostra denunica. Resta il fatto che adesso occorre vigilare affinchè diventi al più presto reperibile nelle farmacie di Roma e d’Italia.