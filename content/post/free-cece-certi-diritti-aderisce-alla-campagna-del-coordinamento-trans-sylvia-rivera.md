---
title: 'Free Cece: Certi Diritti aderisce alla campagna del coordinamento trans Sylvia Rivera'
date: Tue, 13 Nov 2012 15:58:48 +0000
draft: false
tags: [Transessualità]
---

Aderisci anche tu! Basta inviare una mail al procuratore Michel Freeman ([citizeninfo@co.hennepin.mn.us](mailto:citizeninfo@co.hennepin.mn.us)) per chiedere la liberazione di CeCe Mc Donald. Leggi la sua storia.

  
Il 20 novembre è il TDor, Giornata di commemorazione delle vittime dell’odio transfobico, divenuta Giornata internazionale contro la transfobia.  
  
Quest'anno il nome di CeCe Mc Donald si sarebbe aggiunto a quello delle altre persone transgender uccise. Durante il candle light avremmo letto con il nodo in gola di lei, giovane donna trans afro-americana, e della sua vita stroncata a Minneapolis, U.S.A., dalla violenza imbevuta di odio per lei, colpevole di essere non solo una donna trans, ma anche di pelle nera. Invece no, CeCe è viva, per fortuna. Perché si è difesa ed è sopravvissuta. E' morto invece uno degli aggressori che l'avevano attaccata. CeCe Mac Donald è ora imprigionata, condannata a 41 mesi di carcere: il prezzo della legittima difesa di una donna trans nera colpevole di aver ucciso nella colluttazione l'uomo bianco che cercava di ammazzarla.  
**Il Coordinamento Trans Sylvia Rivera sceglie quest'anno di dedicare il TDoR a CeCe Mc Donald** per chiederne la liberazione unendosi alla sua lotta contro il razzismo e l'odio verso le persone transessuali, che può armare la violenza assassina, cosi come permeare le istituzioni al punto da condizionare e orientare le sentenze dei tribunali.  
TDoR è una festa civile. Uniamo il ricordo doloroso e commosso delle donne e gli uomini transessuali e transgender uccisi alla lotta di tutt@ quell@ che sopravvivono e lottano ogni giorno per il proprio diritto a esistere, combattendo così contro tutte le discriminazioni.

Rispetto e autodeterminazione. **Questo è anche l'anno della campagna mondiale contro la patologizzazione delle persone trans, STP2012, che chiede l'eliminazione del transessualismo dall'elenco delle malattie mentali.** La stigmatizzazione delle persone trans come malate mentali ne condiziona e, di fatto, impedisce l'autodeterminazione, con le conseguenze soggettive e sociali che ben conosciamo e che ogni anno il TDoR ci aiuta a ricordare.

Il Coordinamento Trans Sylvia Rivera

Per chiedere la liberazione di CeCe scrivi al procuratore Michel Freeman [citizeninfo@co.hennepin.mn.us](mailto:citizeninfo@co.hennepin.mn.us)  
e visita il blog supportcece.wordpress.com

sito italiano che spiega bene tutta la vicenda: [http://www.anarkismo.net/article/21526](http://www.anarkismo.net/article/21526)

**[IL TESTO DELLA MAIL DA INVIARE AL PROCURATORE >>>>>>>>>](http://www.circolopink.it/Free-Ce-Ce-lettera-procuratore.htm)**