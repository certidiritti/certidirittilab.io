---
title: 'Unioni civili: Parlamento discuta anche di matrimonio tra persone dello stesso sesso, unico traguardo per superamento diseguaglianze'
date: Sun, 22 Apr 2012 13:01:55 +0000
draft: false
tags: [Matrimonio egualitario]
---

Roma, 22 aprile 2012

Comunicato Stampa dell'Associazione Radicale Certi Dirtti

Il 19 aprile scorso è partito in Commissione Giustizia della Camera dei Deputati, in sede referente, l'esame di 9 proposte di legge sulle Unioni Civili escludendo le proposte di legge riguardanti il matrimonio omosessuale e una riforma complessiva del diritto di famiglia assegnate anch’esse alla Commissione Giustizia.

L’Associazione Radicale Certi Diritti chiede che, nella discussione in Commissione Giustizia alla Camera dei Deputati, alle 9 Pdl sulle unioni di fatto vengano abbinate le due proposte sul matrimonio civile per le persone dello stesso sesso.

I veti della partitocrazia italiana vogliono impedire persino una discussione su questo tema che in molti paesi del mondo è diventata già realtà. Ci auguriamo che la prossima settimana non prevalgano interessi vatican-partitocratici e si possa discutere anche delle proposte di legge di Rita Bernardini e Paola Concia sul matrimonio tra persone dello stesso sesso che hanno l’obiettivo di superare le diseguaglianze per le persone lesbiche e gay. Vi è inoltre una proposta di legge dei Radicali, prima firmataria Rita Bernardini,  che propone una Riforma complessiva del Diritto di Famiglia che include anche la regolamentazione delle Unioni Civili e il matrimonio tra persone dello stesso sesso.

La sentenza della Corte Costituzionale 138/2010, grazie alla campagna di Affermazione Civile, ha sollecitato il Parlamento a legiferare su questi temi e sarebbe ora che la classe politica si muovesse in questa direzione.

**[ISCRIVITI ALLA NEWSLETTER >](newsletter/newsletter)**

**[SOSTIENI CERTI DIRITTI >](iscriviti)**