---
title: 'Bilancio 2013'
date: Mon, 13 Jan 2014 09:35:10 +0000
draft: false
tags: [Politica]
---

**STATO PATRIMONIALE**

**ATTIVITA’**

**PASSIVITA’**

Cassa Contanti

1 276.92

Creditori Diversi

887.80

Carta Prepagata

223.80

**DEBITI DIVERSI**

**887.80**

**CASSA**

**1 500.72**

Risconti Passivi

1 050.00

Banca c/c

6 447.78

**RETTIFICHE PASSIVE DI BILANCIO**

**1 050.00**

c/c Postale

1 873.71

Avanzo Cumulato

9 820.13

Paypal

287.34

**CAPITALE SOCIALE**

**9 820.13**

**BANCHE C/C**

**8 608.83**

Credito vs. Partito Radicale

900.00

**CREDITI**

**900.00**

**PERDITA D’ESERCIZIO**

**748.38**

**TOTALE ATTIVO**

**11 757.93**

**TOTALE PASSIVO**

**11 757.93**

**CONTO ECONOMICO**

**ENTRATE**

**13 702.01**

Contributi

10 588.94

Contributi ad iniziative

3 090.00

Interessi attivi

23.07

**COSTI**

**SERVIZI (Telematici, Postali, Oneri bancari, Commissioni)**

**1 435.01**

**INFORMAZIONE E AUTOFINANZIAMENTO**

_Stampa e spedizione tessere_

_297.28_

_Sito web_

_768.60_

_Materiale e gadget_

_874.84_

**RIUNIONI**

**50.00**

**CONTRIBUTI EROGATI**

**250.00**

_Iscrizione Ilga-Europe_

_150.00_

_Contributo Arcigay_

_100_

**INIZIATIVE**

**8 552.72**

_Roma Pride_

_240.00_

_Milano Pride_

_200.00_

_Sos Russia_

_5 413.72_

_Love is right_

_200.00_

_Iniziative varie (spostamenti per conferenze e soggiorni per iniziative)_

_2 404.00_

_Riunioni varie_

_95.00_

**V CONGRESSO MILANO**

**150.00**

_Spese viaggi e soggiorni_

_150.00_

**VI CONGRESSO NAPOLI**

**1 311.39**

_Spese viaggi e soggiorni_

_1 004.39_

_Spese varie_

_307.00_

**VII CONGRESSO**

**544.05**

_Sala Congresso_

544.05

**SOPRAVVENIENZE PASSIVE**

**216.50**

**PERDITA D’ESERCIZIO**

**748.38**