---
title: 'Cardinal Bertone offende parlamentari e milioni di famiglie: grave ingerenza negli affari italiani. La migliore risposta è del Parlamento Europeo'
date: Thu, 24 May 2012 09:59:23 +0000
draft: false
tags: [Politica]
---

Le sue tesi sono una grave ingerenza e un'offesa a milioni di famiglie conviventi che chiedono riconoscimento diritti.

Roma, 24 maggio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Quello che ha detto ieri il Cardinale Tarcisio Bertone in una delle massime sedi istituzionali della Repubblica italiana, la Camera dei deputati, è molto grave e offensivo dei principi democratici e di libertà che dovrebbero essere sacre in una democrazia e invece in Italia non lo sono.

Proprio mentre la Commissione Giustizia ha finalmente iniziato la discussione sulle diverse proposte di legge che chiedono la regolamentazione delle unioni civili e del matrimonio tra persone dello stesso sesso, così come richiesto ancora oggi dal Parlamento Europeo ai suoi Stati membri, l’intervento del Segretario della Cei  appare come una grave ingerenza negli affari interni italiani, un vero e proprio intervento a gamba tesa.  Milioni di persone, gay ed eterosessuali,  chiedono che quanto prima venga approvata una legge che riconosca diritti finora negati proprio a causa di quel potere clericale, sostenuto da una classe politica per lo più ipocrita e corrotta, che vuole imporre una visione familistica non corrispondente alla realtà.  

Sostenere che  “la famiglia e' originata dal diritto naturale” e che “non è a disposizione della nostra volonta'” dimostra di come la chiesa voglia imporre una sua visione a chi invece intende vivere con amore, responsabilità e consapevolezza la sua unione con regole chiare e certe.

Il Cardinale Bertone in riferimento alla famiglia ha poi detto che  non può  esserci “manipolazione a livello legislativo e giuridico da maggioranze variabili formatesi a seconda delle sensibilita' di un dato momento storico”, offendendo quindi gli stessi parlamentari, “manipolatori” che stanno discutendo proprio di questo. Quando però le maggioranze variabili gli davano l’8Xmille, l’esenzione Ici per le attività commerciali della chiesa, e altri privilegi di casta super tutelata, lor signori stavano tutti zitti, in silenzio ad attendere il bottino dei privilegi e dei denari che arriva a quasi cinque miliardi di euro ogni anno.

E’ ora che l’Italia la smetta di genuflettersi alla casta clerical-partitocratica e finalmente avvii un cammino di libertà, democrazia e civiltà, così come oggi lo stesso Parlamento Europeo ha chiesto ai suoi paesi membri, nel testo della Risoluzione approvata a stragrande maggioranza, proprio riguardo le tutele e l’accesso a istituti giuridici quali coabitazione, unione registrata o matrimonio. Lo si può fare a partire da una moderna riforma complessiva del diritto di famiglia, al passo con l’Europa e dei più civili paesi democratici. Lo strumento già c’è ed è a disposizione di tutti, basta andare a vedere la proposta di legge n. 3607 depositata alla Camera dei depuati dai parlamentari Radicali, prima firmataria Rita Bernardini.