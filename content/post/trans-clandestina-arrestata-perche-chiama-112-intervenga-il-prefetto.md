---
title: 'TRANS CLANDESTINA ARRESTATA PERCHE'' CHIAMA 112: INTERVENGA IL PREFETTO'
date: Sun, 10 Aug 2008 11:52:13 +0000
draft: false
tags: [Comunicati stampa]
---

**TRANSESSUALE ARRESTATA PERCHE’ DENUNCIA AGGRESSORE: DOVEVA MORIRE PER NON ESSERE PORTATA IN CARCERE? CHIEDIAMO L’IMMEDIATO INTERVENTO DEL PREFETTO DI ROMA.**

Roma, 10 agosto 2008 

**_Dichiarazione di Antonella Casu, Segretaria di Radicali Italiani e Sergio Rovasio, Segretario Associazione Certi Diritti:_**

“Ci appare francamente strano il comportamento delle forze dell’ordine che oggi a Roma hanno arrestato una transessuale brasiliana clandestina che ha avuto il coraggio di chiamare i Carabinieri perché un uomo le stava sfondando la porta di casa sua. Cosa doveva fare per salvarsi? Farsi uccidere così tutti si sarebbero disperati (a parole) per l’ennesimo episodio di violenza ed emarginazione?

La scorsa settimana a Milano, la transessuale brasiliana clandestina che aveva denunciato la scomparsa della sua amica, rapita e poi uccisa da due ragazzi, ha avuto dalle forze dell’ordine un permesso speciale che le permetterà di rimanere in Italia regolarmente per alcuni mesi. Perché nel caso di Roma non si applica la stessa regola?

Ci auguriamo che il Prefetto di Roma, Dottor Carlo Mosca, intervenga al più presto per aiutare la transessuale brasiliana che ha avuto il merito di riporre la sua totale fiducia nelle forze dell’ordine e ora invece si trova in prigione”.