---
title: 'Prostituzione Roma: Alemanno proroga per quarta volta ordinanza. Ecco i dati della sua politica fallimentare'
date: Tue, 14 Feb 2012 16:14:21 +0000
draft: false
tags: [Lavoro sessuale]
---

Errare è umano, ma perseverare è diabolico. Alemanno proroga per quarta volta ordinanza sulla prostituzione. Comitato diritti civili prostitute e Certi Diritti diffondono i dati fallimentari della politica proibizionista.

Roma, 14 febbraio 2012

Comunicato Stampa del Comitato per i Diritti Civili delle Prostitute e dell’Associazione Radicale Certi Diritti

Mai la massima latina “Errare humanum est, perseverare autem diabolicum” è apparsa così azzeccata rispetto all’operato del Sindaco Gianni Alemanno. Non contento di aver prorogato l’ordinanza proibizionista antiprostituzione già  tre volte, pur avendo fallito su tutti i fronti, oggi ha deciso la quarta proroga fino al marzo 2013. Ciò che è incredibile è che queste operazioni di repressione non sono servite a niente. Su oltre 21.000 operazioni anti-prostituzione svolte nel territorio del Comune di Roma, solo 600 di queste sono state pagate a seguito di sanzioni (fonte: Sindacato Fp Cgil della Polizia Municipale).

Il Sindaco di Roma fornisca alla stampa anche i dati che finora ha tenuto nascosti  - che non ha ritenuto di trasmettere nemmeno quando ha risposto all’interrogazione popolare  presentata dai Radicali di Roma nel giugno 2011 - relativi all’applicazione della delibera anti-prostituzione. Non ha comunicato quante persone avevano pagato le multe tanto sbandierate ma soprattutto non ha fornito i dati sui costi delle operazioni (personale, mezzi, straordinari, propaganda, ecc) sottraendo risorse alla lotta alla criminalità vera. I soldi del mercato del sesso continuano così a finire nell’illegalità e nelle attività criminali.

E’ evidente che se fornisse questi dati dovrebbe dichiarare fallita miseramente questa operazione repressiva-proibizionista che incrementa sempre più la marginalizzazione delle prostitute e che aumenta i casi di violenza, stupri e rapine che non vengono nemmeno più denunciati per paura.

Le politiche repressive non fanno altro che alimentare la violenza e gli omicidi di prostitute che solo negli ultimi due mesi, in Italia sono stati sei.