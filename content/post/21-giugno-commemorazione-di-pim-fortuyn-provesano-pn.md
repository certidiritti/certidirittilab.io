---
title: '21 GIUGNO - COMMEMORAZIONE DI PIM FORTUYN - PROVESANO (PN)'
date: Thu, 19 Jun 2008 17:47:31 +0000
draft: false
tags: [Commemorazione, Comunicati stampa, GayLib, Pim Fortuyn]
---

![](http://www.gaylib.it/news/qpimfortuynfondation.jpg)**GayLib e Certi Diritti il 21 giugno sulla tomba di Pim Fortuyn: "Contro la cultura dell'omofobia occorre un'identità forte"  
**GayLib (gay liberali di centrodestra) interverrà SABATO 21 GIUGNO 2008, in occasione delle giornate internazionali dedicate al Gay Pride, presso il cimitero di Provesano (Pn)...

GayLib (gay liberali di centrodestra) interverrà SABATO 21 GIUGNO 2008, in occasione delle giornate internazionali dedicate al Gay Pride, presso il cimitero di Provesano (Pn) col fine di commemorare il grande Pim Fortuyn, leader della destra olandese, divenuto martire della libertà e simbolo per tutti gli omosessuali di destra.

L’esempio di Pim, il suo stile, la sua sobrietà, l’eleganza e soprattutto le idee e i valori saldi nella tutela delle libertà individuali e dell’identità occidentale grazie alla quale la comunità gay ha trovato il coraggio e lo spazio per lottare in difesa dei propri diritti, ha fatto sì che la nostra scelta per questo Pride 2008 cadesse su Pim Fortuyn.

![](http://www.gaylib.it/news/news45-2.jpg)Provesano si trova a una trentina di chilometri da Pordenone ed è una frazione di San Giorgio della Richinvelda (non lontano da Casarza della Delizia). 

Ecco un primo programma

Ore 10,30 - Raduno di fronte all’ingresso del cimitero

Ore 11,00 – Arrivo sulla tomba.

Ore 11.05 - Deposizione di una corona di fiori ornata di nastri rainbow

Ore 11,15 – Discorso dei presidenti di GayLib, della Pim Fortuyn Foundation e delle eventuali autorità presenti

Ore 12,00 – Conferenza stampa presso la casa di Pim Fortuyn

Ore 12,30 - Pranzo

Ore 14,30 - Partenza per il Gay Pride di Lubiana (SLO)

Siamo disponibili per ogni informazione

Enrico Oliari – PresidenteCell. 335/6622440  
Daniele Priori – VicepresidenteCell. 328/6323820  
Marco Anselmo Jouvenal – Cordinatore nazionaleCell. 338/7554565

![](http://www.gaylib.it/news/qgaylib.jpg)_“Il 21 giugno ricorderemo la memoria di Pim Fortuyn, il leader omosessuale della destra olandese barbaramente assassinato nel 2002 perché latore di idee di giustizia e di libertà laddove, nella sua Olanda, le fallimentari politiche di integrazione di una popolazione islamica intransigente sono sfociate e sfociano tutt’oggi in aggressioni e violenze nei confronti dei gay”. Lo afferma Enrico Oliari, presidente di GayLib (gay di centro-destra), il quale sostiene che “Pim era un omosessuale dichiarato, dalla cultura __ raffinata, orgogliosamente gay ed orgogliosamente di destra: un esempio per me e per tutti noi. Sabato 21 alle ore 11.00 ci incontreremo presso la sua tomba a Provesano (PN), in quella terra che Pim amava e che voleva come sua seconda dimora”._

_“I Gay Pride italiani – ha continuato il presidente di GayLib – sono manifestazioni di sinistra che discriminano chi di sinistra non è, tant’è che anche quest’anno a Bologna sono state escluse sistematicamente e volutamente le nostre iniziative culturali. D’altro canto ci dicono che il gay pride è una festa, ma davvero non vedo cosa ci sia da festeggiare in un’Italia che, a differenza dell’Olanda di Pim, non riconosce i diritti delle le coppie omoaffettive”._