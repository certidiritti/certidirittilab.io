---
title: 'Emma Bonino premiata al Gay Village. Il suo messaggio >'
date: Sun, 11 Sep 2011 12:38:17 +0000
draft: false
tags: [Politica]
---

Emma Bonino per la politica (e Luciana Littizzetto per lo spettacolo), entrambe iscritte per il 2011 all’Associazione Radicale Certi Diritti, elette da un sondaggio su [www.gayvillage.it](http://www.gayvillage.it) "le eterosessuali più amate dai gay". 

Care amiche e cari amici del Gay Village,  
Non posso essere lì con voi a ritirare di persona il premio Awards perché in questo momento sono in Slovenia, su invito del Presidente, per un Forum sul rapporto del Consiglio di Europa alla cui stesura ho partecipato come membro di un gruppo di personalità europee; il documento si intitola "Vivere insieme - conciliare diversità e libertà nell'Europa del XXI secolo".  
  
Sono molto grata a tutte/i voi per aver indicato il mio nome tra coloro  che sentite più vicini a voi, alle vostre giuste istanze. Questo mi aiuta a continuare, con costanza e impegno, il lavoro che da anni porto avanti insieme agli amici e compagni Radicali.  
  
Come sapete, da tanti anni ci battiamo per il superamento delle diseguaglianze e per rendere questo paese migliore, più laico e meno clericale, più libero e meno imprigionato nel suo sistema partitocratico, fatto di caste e ipocrisie di ogni genere, che impedisce il raggiungimento di riforme e progresso. E’ certamente molto grave che ancora oggi la maggior parte della classe politica rimanga  arroccata su posizioni retrograde, conservatrici e non guardi invece a quanto i Trattati di Nizza e di Lisbona indicano ai paesi membri dell’Unione Europea, sulla lotta alle discriminazioni e sull’uguaglianza di ogni persona.  
  
Insieme all’Associazione Radicale Certi Diritti, alla quale sono iscritta insieme a Luciana Littizzetto, altra amica da voi più votata, ci battiamo per far diventare il nostro paese più europeo, più civile e più al passo con i tempi moderni. Abbiamo grandi difficoltà  di ogni genere ed è per questo che vorrei qui lanciare a tutti voi un appello che spesso uso dire agli amici che mi dimostrano affetto e vicinanza: “amatemi di meno e cercate di aiutarmi di più”. Ci sono molti modi, certamente uno dei più importanti è quello di sostenere le lotte, le battaglie, le speranze dei Radicali e, perché no?, sicuramente anche le vostre!  
  
Vi ringrazio di cuore e vi abbraccio,  
Emma Bonino

  
[Luciana Littizzetto e Emma Bonino, iscritte a Certi Diritti, "le due eterosessuali più amate dai gay". Leggi >](luciana-littizzetto-e-emma-bonino-iscritte-a-certi-diritti-le-due-eterosessuali-piu-amate-dai-gay)  
  
[Iscriviti alla newsletter >](newsletter/newsletter)  
  
[Iscriviti e contribuisci a Certi Diritti >](iscriviti)