---
title: 'INVITO AL CAFFE'' LETTERARIO DI ROMA: DOCUMENTARIO ''IL SOFFIO DELLA TERRA'''
date: Sat, 28 Mar 2009 09:06:10 +0000
draft: false
tags: [Comunicati stampa]
---

LA MEDIA DIGITAL STUDIO-   
Presenta

“IL SOFFIO DELLA TERRA”  
perché vivere è una scelta

di Stefano Russo 

SABATO 28 MARZO - ORE 17.15 - CAFFE’ LETTERARIO  SEGUITO DA DIBATTITO

intervengono  
il regista Stefano Russo  
gli attori protagonisti Fabio De Caro e Enrico Ianniello  
i produttoriDavide Contessa e Marisa Evangelista, la troupe

Mina Welby  
Mario Staderini della Associazione “Luca Coscioni” di Roma  
Sergio Rovasio dell’Associazione “Certi Diritti”  
il cantante Enrico Capuano

Il cortometraggio, che parteciperà ai più importanti Festival internazionali, affronta un tema, quello dell’eutanasia, che negli ultimi mesi ha suscitato non poche polemiche. L'intento è porre l’attenzione su un tema di scottante attualità senza esprimere giudizi o sentenze definitive, ma raccontando una storia tra le tante che, in silenzio, si consumano ogni giorno nel nostro Paese. 

Il corto, nato dalla penna di Stefano Russo, autore premiato in diversi concorsi di sceneggiatura, e Luigi Barbieri, con la collaborazione di Guglielmo Finazzer, story editor di “Un posto al sole”, racconta, in poco più di 15 minuti, la storia di Nicola Gabriele.  
Siamo alla fine degli anni '90, Nicola è inchiodato ad un letto d'ospedale a causa di una malattia degenerativa che ha minato le sue capacità motorie e respiratorie. Nonostante ciò, Nicola conduce una vita ricca di relazioni: la sorella Giulia spesso va a fargli visita e Daniele, il dottore che lo ha in cura, è diventato negli anni il suo migliore amico. Ma a Nicola tutto questo non basta. In una mattina di luce viene recapitata in ospedale un'apparecchiatura che può liberarlo da quel letto, un respiratore portatile...

Protagonisti del corto Fabio De Caro, attore di forte spontaneità, ed Enrico Ianniello, anche autore e regista che da anni collabora con il gruppo dei Teatri Uniti di Napoli e che attualmente è in scena a Parigi con il dramma teatrale, da lui tradotto, “Chiove” .  
Le musiche originali sono di Pasquale Catalano, compositore delle musiche dei film di Paolo Sorrentino (“L'uomo in più” e “Le conseguenze dell'amore”) e della serie TV “Romanzo Criminale”.

Produttori del corto Davide Contessa (Media Digital Studio) e Marisa Evangelista.

Info: [www.ilsoffiodellaterra.it](http://www.ilsoffiodellaterra.it) \- [www.myspace.com/stefanorusso1970](http://www.myspace.com/stefanorusso1970)  
Per vedere il trailer:  
[http://www.mediadigitalstudio.it/content/VIDEO-Trailer\_IL\_SOFFIO\_DELLA\_TERRA.html](http://www.mediadigitalstudio.it/content/VIDEO-Trailer_IL_SOFFIO_DELLA_TERRA.html)

Info:  
Caffè Letterario – via Ostiense, 95 (Roma) – [www.caffeletterarioroma.it](http://www.caffeletterarioroma.it) \- 0657302842 

Media Digital Studio - Davide Contessa  
0645505736 - 3491656950  [davide.contessa@virgilio.it](mailto:davide.contessa@virgilio.it) \- [www.mediadigitalstudio.it](http://www.mediadigitalstudio.it)

Ufficio Stampa - Angela Iantosca  
[iantoscaangela@gmail.com](mailto:iantoscaangela@gmail.com) – 3381065420

IL SOFFIO DELLA TERRA

ATTORI: Fabio De Caro, Enrico Ianniello, Gisella Szaniszlò, Simona Iaccio, Bruno Grillo, Giuliano Russo

REGIA  Stefano Russo  
SCENEGGIATURA  Stefano Russo, Luigi Barbieri  
con la collaborazione di Guglielmo Finazzer  
DIRETTORE DELLA FOTOGRAFIA  Rocco Marra  
MONTAGGIO  Davide Contessa  
MUSICHE ORIGINALI  Pasquale Catalano  
FONICO IN PRESA DIRETTA  Dario Todero  
microfonista – Ivan Cassese  
AIUTO REGIA  Alberto V. De Rosa  
PRODUZIONE  Davide Contessa  
SCENOGRAFIA Peppe Cerillo  
assistente Francesca Macor  
TRUCCO & PARRUCCO   Adele Paga  
COSTUMI   Anna Facchino  
MONTAGGIO DEL SUONO   Carlo Licenziato   (Groove Studio)  
ASSISTENTE ALLA REGIA   Paky Perna  
SEGRETERIA DI PRODUZIONE   Claudia Moretti  
OPERATORE   Rocco Marra  
ASSISTENTE OPERATORE   Paolo Cafiero  
OPERATORE JIMMY-LIB   Claudio Crimani  
MACCHINISTA   Ezio Pierattini  
MAKING OF   Alfredo Di Fede  
TRADUZIONI   Oliver Tringham  
I PERSONAGGI

NICOLA GABRIELE  
Quarantenne dall’aspetto invalidato dalla lunga degenza in ospedale. I capelli scompigliati gli donano un’aria bizzarra. Dal suo sguardo si evince la profondità della sua natura.

DANIELE SALA  
Trentanove anni, di cui più della metà dedicati allo studio e alla professione medica. Nonostante ricopra un ruolo di prestigio all’interno dell’ospedale, la barba incolta e gli occhiali gli donano un aspetto informale. Il suo approccio con la realtà è estremamente razionale, tranne quando entrano in ballo i sentimenti: l’amicizia con Nicola sarà il suo “difetto tragico” e lo condurrà molto lontano dai territori sicuri della razionalità.

GIULIA GABRIELE  
Trentenne sorella di Nicola, che ama il fratello e va spesso a trovarlo in ospedale, facendogli sentire il suo calore umano.

ANDREA  
Trentenne infermiera dai caratteri mediterranei. La sua indole materna e la sua voglia di aiutare il prossimo ne fanno un’infermiera modello.