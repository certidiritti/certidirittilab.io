---
title: 'E'' morto Giancarlo Scheggi, storico militante radicale di Firenze'
date: Thu, 30 Jun 2011 08:50:46 +0000
draft: false
tags: [Politica]
---

**Giancarlo Scheggi era uno storico militante fiorentino delle lotte radicali. Iscritto a Certi Diritti dal 2008, non ha fatto in tempo a rinnovare l'iscrizione per il 2011.**

**Firenze - Roma, 30 giugno 2011**

Ieri, dopo un lungo ricovero in terapia intensiva all’Ospedale di Firenze, è morto Giancarlo Scheggi, storico militante fiorentino delle lotte Radicali. Si era iscritto a Certi Diritti al momento della sua fondazione nel marzo 2008 e si era sempre interessato alle nostre battaglie apprezzandone convintamente gli obiettivi.

Giancarlo era una persona con un cuore bellissimo. Sempre disponibile, generoso e sorridente con tutti. Non siamo riusciti a chiedergli il rinnovo di iscrizione per il 2011 perché le sue condizioni di salute non ce lo hanno permesso. Lo avrebbe fatto con sincera convinzione.

L’Associazione Radicale Certi Diritti abbraccia calorosamente tutta la sua famiglia e quanti lo hanno amato.

Il miglior ricordo di Giancarlo riteniamo che lo si possa trovare nei seguenti messaggi del Direttore di Notizie Radicali Valter Vecellio e del Presidente di Arcigay Firenze, anch’egli iscritto a Certi Diritti:

Il nostro amico e compagno Giancarlo faceva parte di quei radicali "storici" e insieme "ignoti" loro sì, capaci di resistere, resistere, resistere, e insieme di esistere, esistere, esistere.  
C'è un numero di "Notizie Radicali" di qualche anno fa dove si raccolsero le storie di alcuni di loro, storie che poi più diffusamente abbiamo riversato in un volume. Rileggere la storia di Giancarlo consola un poco, forse.  
   
"Durante un viaggio di lavoro sul treno da Firenze a Milano nel 1974, ad un mese dal  
voto sul DIVORZIO conobbi nel sedile  di fronte una signora, non bella (ed uso un  
eufemismo) di profonda spiritualità,. Mi parlava di ecologia, era contro i pesticidi  
, amante della natura e della libertà. Come non potevamo cascare sull'argomento di  
attualità nella vita politica: referendum sul divorzio. Io cattolico di ferro, felicemente sposato e con figli non potevo nemmeno immaginare  che "sciagurati" osassero sciogliersi da un vincolo sacro. La discussione andò avanti fino a Milano ed io fui abbastanza poco cortese, certo delle mie convinzioni.  
Mi rimase impresso il suo appello accorato: Se conferma il divorzio,  da cristiano,  
contribuirà  far  rinascere l'amore  in altrettante  famiglie, oggi  disgregate. Fu un seme gettato , il suo, in terreno fertile. Al referendum  votai convinto  il mio NO all'abolizione  del divorzio proprio pensando  con spirito cristiano agli altri. Ringraziando Iddio e mia moglie Anna non abbiamo avuto necessità di separarci. E  due settimane fa abbiamo festeggiato 45 anni di matrimonio.  
L'anno dopo  maturai l'iscrizione al  PR e nel 1976 preferii inviare un  contributo per la tessera, avendo capito che nel mio lavoro   era considerato serio l'iscriversi ad altri partiti., ma il partito mi recapitò la tessera   di sostenitore non iscritto che si distingueva dall'originale solo nel colore (viola).  Feci  tesoro di questa tessera e da allora sono stato iscrito  non solo al PR, ma a tutti i soggeti politici che nel tempo  si manifestarono fino  alla iscrizione per il 2009  che concretizzerò prima del congresso, insieme al contribto  a R:I: per complessivi 1000 euro.  
Ma qualora fosse stato necessario rafforzarmi nelle mie convinzioni  nel 1976,  
avendo già tre figli in tenera età, in perfetta armonia con mia moglie  decidemmo di  
rinunziare  alla nascita del 4' figlio per motivi molto seri e fu ancora il P:R che  
incontrai.  
Allora gli appuntamenti per le interruzioni di gravidanza  venivano dati vicino al  
bar del Panteon......  
Certo senza le mie iscrizioni e la militanza   Radicale  avrei avuto un'altro  
percorso  nella mia azienda (Standa) ben diverso. Ma sono fiero  di essermi giocato  
la Dirigenza avendo scelto la mia libertà.   
Conservo gelosamente le tessere di tutti i soggetti  politici della galassia  
radicale  per tutti gli anni dal 1975 al 2008 che porterò in visione al prossimo  
congresso, con la speranza  di aggiungerne ancora molte, molte altre.  
un abraccio".

Un arrivederci all'attivista radicale e grande amico Giancarlo Scheggi  
   
Firenze, 30 giugno 2011  
   
Caro Giancarlo,  
prima o poi, lo sapevo, mi sarebbe arrivata la notizia della tua scomparsa. Era diventata ahimè una certezza dopo l'ultima volta che c'eravamo visti, a casa tua, poco tempo fa: volesti convocare me e Francesco, il mio compagno, per un saluto "estremamente importante e urgente", e io avevo capito. Avevo capito che quello che volevi era la possibilità di dirci addio, o arrivederci, come si deve, mentre mi guardavi coni tuoi occhi lucidi che riflettevano così bene la purezza del tuo essere. Grande amico mio e fedele compagno di battaglie e avventure, mi hai insegnato così tanto che sembra banale anche solo ricordare alcuni dei tuoi messaggi più potenti! Ricordo che una volta ti dissi, un po' sconfortato, a una manifestazione per i diritti LGBT: "Giancarlo, siamo sempre gli stessi, sempre troppo pochi, sempre troppo in silenzio". Tu mi guardasti e, con quell'espressione che assumevi quando era il momento di essere serio e arrivare al cuore di chi ti stava accanto, mi dicesti: "Caro Matteo, non è importante il numero, né come si manifesta. È importante esserci, e testimoniare la nostra partecipazione e vicinanza a una battaglia, con la nostra determinazione, con la voglia di non mollare mai. Non arrenderti mai, perché eravamo pochi anche nelle grandi battaglie radicali del passato, eppure i risultati sono stati straordinari". Quel giorno promisi a me stesso che avrei seguito quel tuo insegnamento come un dogma per la vita, e oggi me lo hai ricordato, ancora una volta. Hai lasciato il segno, caro Giancarlo. Un disegno indelebile che niente e nessuno potrà mai scalfire dentro me: un disegno che racconta la vita di un grande uomo, che ha lottato strenuamente per ciò in cui credeva, che si è sempre speso per gli altri, che ha vinto la gara più difficile, con saggezza e onore, dimostrando la straordinaria potenza della sua anima. Mi piace ricordarti gioioso e birichino, allampanato e sognatore, e perdermi nei tuoi occhi in cui ti si specchiava il cuore. Mi piace sorridere ricordando quando mi dicevi: "abbiamo la stessa età, entrambi nati lo stesso giorno, l'11 aprile. Sì, c'è quei cinquant'anni di scarto...". Il tuo spirito giovane mi ha mostrato spesso quanto io, al contrario, apparissi vecchio, e indebolito dalle difficoltà della vita che tu prendevi di petto, con dignità e fiducia. Sei stato un grande attivista e un grande maestro, e in quanto tuo allievo cercherò di continuare a farti sorridere, lassù, e sperare in un'Italia migliore, in un mondo migliore. Voglio che il tuo sogno non rimanga un sogno, voglio che i tuoi occhi continuino a brillare per l'eternità e ti prometto che farò del mio meglio per continuare a costruire con altri amici e compagni quello che tu hai iniziato. Ti voglio bene, Giancarlo, e, dovunque tu sia, ti mando un abbraccio colmo d'amore e un pizzicotto di burla e simpatia come quelli che solevi darmi tu, ogniqualvolta ci ritrovavamo insieme a sorridere e a lottare. Grazie di cuore, amico mio".  
Matteo Pegoraro  
[http://notizie.radicali.it/articolo/2011-06-30/editoriale-direttore/addio-giancarlo-che-la-terra-ti-sia-lieve](http://notizie.radicali.it/articolo/2011-06-30/editoriale-direttore/addio-giancarlo-che-la-terra-ti-sia-lieve)