---
title: 'VI Congresso Associazione radicale Certi Diritti'
date: Fri, 01 Mar 2013 13:55:37 +0000
draft: false
tags: [Politica]
---

**VERSO L'UGUAGLIANZA**_**IOMIMPEGNO.ORG**_ CONTRO IL PROIBIZIONISMO SUI CORPI E SUGLI AFFETTI  
  

Quest’anno si celebra il centenario della nascita di Rosa Louise Parks, l’attivista afroamericana che il 1° dicembre 1955 compì un gesto, semplice e rivoluzionario, come quello di sedersi in un posto riservato ai bianchi sugli autobus di linea di Montgomery (Alabama, USA), e subendo per questo un processo che produsse nel 1956 una sentenza importante della Corte suprema degli USA, con la quale si decretava l’incostituzionalità della segregazione razziale.  
Rosa è diventata il simbolo, uno dei più forti e cristallini, per chi nel mondo lotta per i diritti e contro ogni forma di razzismo, avendo come valore di riferimento il metodo nonviolento, la fede nella giustizia e l’impegno personale.

Queste caratteristiche sono esattamente **i tre pilastri su cui si basa la nascita e l’attività dell’Associazione radicale Certi Diritti che intende dedicare il suo VI Congresso nazionale a Rosa e a tutti gli uomini e le donne che come lei hanno partecipato con il proprio corpo e la propria speranza, alle battaglie di libertà**. Tre caratteristiche che sono costitutive della cultura e della storia radicale di cui la nostra associazione è parte, e che rendono la nostra associazione una proposta di militanza attiva, dove l’elemento dell’impegno personale è fondativo dell’associazione stessa. **La campagna 2013, rivolta ai nuovi deputati e senatori così come ai cittadini e alle cittadine italiane [www.iomimpegno.org](http://www.iomimpegno.org)** **ne è un esempio**.

Il Congresso si svolge a **Napoli**, città che vide uno straordinario impegno su questi temi negli anni ’70 ( il 22 settembre del 1975 a Napoli fu organizzata una Marcia e un Convegno per la Liberazione Sessuale dal Partito radicale, dal Movimento di Liberazione della Donna e dal Fuori!) e che vede con la nuova amministrazione una rinnovata attenzione sui temi di libertà e autodeterminazione legati alla vita sessuale delle persone.

Da Napoli quindi, **un Congresso per prepararci al confronto con il prossimo Parlamento – ed i prossimi Consigli regionali** – affinchè la smettano di essere ciechi, sordi e muti di fronte alla vita reale degli uomini e delle donne e sappiamo assumersi le proprie responsabilità. Per diventare pienamente europei, nella salvaguardia dei diritti e non solo nel governo economico della nostra società.

**[PROGRAMMA e INFO LOGISTICHE >](http://www.certidiritti.org/2013/03/06/vi-congresso-di-certi-diritti-il-programma/)**