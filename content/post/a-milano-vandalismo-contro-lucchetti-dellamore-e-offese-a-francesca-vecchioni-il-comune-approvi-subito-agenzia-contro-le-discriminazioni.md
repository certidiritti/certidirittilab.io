---
title: 'A Milano vandalismo contro lucchetti dell''amore e offese a Francesca Vecchioni: Il comune approvi subito agenzia contro le discriminazioni'
date: Thu, 20 Sep 2012 18:18:06 +0000
draft: false
tags: [Politica]
---

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Milano, 20 settembre 2012

Inqualificabile atto omofobo quello avvenuto ieri a Milano, dove degli sconosciuti hanno con escrementi i cosiddetti 'lucchetti dell'amore gay', che le associazioni LGBTI milanesi avevano legato sotto la statua di Leonardo da Vinci in piazza Scala a Milano, di fronte al Comune, in ricordo dei simbolici matrimoni omosessuali celebrati in quella piazza nel 1992. L’Associazione Radicale Certi Diritti esprime la sua solidarietà a Francesca Vecchioni, presa particolarmente di mira dall’atto vandalico.

Dichiarazione di Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti: “Questo gesto non offende solo la comunità LGBTI milanese, ma tutti i cittadini che credono che Milano possa essere una città aperta ed europea. Il Comune ha fatto benissimo ad approvare il registro delle unioni civili, ma evidentemente questo non basta: occorre che si approvi subito anche l’Agenzia antidiscriminazioni chiesta da più di 5000 cittadini che hanno sottoscritto le proposte di delibera di iniziativa popolare promosse dal Comitato Milano Radicalmente Nuova di cui anche l’Associazione Radicale Certi Diritti fa parte. Occorrono politiche serie di prevenzione e contrasto dei fenomeni di discriminazione, maltrattamenti e violenza che, evidentemente, sono purtroppo ben presenti a Milano”.