---
title: 'L''unione fa la forza! Cosa manca alla strategia italiana per il diritto al matrimonio'
date: Thu, 29 Mar 2012 21:42:51 +0000
draft: false
tags: [Movimento LGBTI]
---

di Enzo Cucco, presidente associazione radicale Certi Diritti.

Le recenti sentenze del Tribunale di Reggio Emilia e della Corte di Cassazione son lì a dimostrare che chi, come l’Associazione radicale Certi Diritti, ha puntato sulla strategia delle cause pilota per cercare di ottenere il diritto al matrimonio civile per le persone dello stesso sesso, aveva visto giusto. Ma insieme alla soddisfazione di vedere i primi risultati concreti, tutti figli di quella sentenza della Corte costituzionale di cui a breve celebreremo due anni di vita, c’è la preoccupazione per l’estrema fragilità di questi primi passi e per la reazione di quanti, retrogradi clericali o raffinati intellettuali neo convertiti , non staranno a mani conserte aspettando che il processo di riforma in atto si compia.

Furori ideologici conditi da sfottò intellettuale (istruttivo leggere cosa scrive il foglio in questi giorni e come cerca di trasformare il diritto al matrimonio in una frivola moda a stelle e strisce figlia di postmodernismo e salotti chic) alimenteranno il blocco avversario che avremo di fronte che farà di tutto per rendere innocua qualsiasi riforma del diritto di famiglia. Una riforma che si accorga che le famiglie composte da coppie dello stesso sesso esistono, hanno figli e figlie, e godono di diritti e doveri costituzionalmente protetti.

Io però non riesco ad essere preoccupato più di tanto del fronte del NO. Non che ne disconosca potenza e capacità inibitoria, anzi dobbiamo aspettarci di tutto in questo senso: se qualcuno si fosse dimenticato la volgarità di alcune delle invettive di Fanfani contro il divorzio nel 1974 basterebbe ricordare cosa è accaduto poco tempo fa sul caso Englaro, di fronte al quale le battute di G&G (Giovanardi & Gasparri, Dio li fa e poi ….) sono poco più che barzellette.

Quello che più mi preoccupa è come si compone il fronte del SI, di quale strategia si è dotato, quali sono le tappe di questa strategia, i mezzi in campo, quali argomenti e quali iniziative riuscirà ad adottare per costruire quel consenso su una iniziativa che, per la prima volta, vediamo come possibile. E soprattutto quanto è consapevole della posta in gioco.

  
**Il fronte del sì**

Ma da chi è composto il fronte del SI al matrimonio? Ci sono  coloro che producono solo comunicati stampa e mai si sono sognati di fare una iniziativa concreta in tal senso. O quelli che (e son tanti) quando si parla di questo argomento ti guardano con lo sguardo di chi la sa lunga ( una sintesi freack tra John Wayne e Amanda Lear … )  e ti dicono “ si si, certo, il matrimonio…” e poi impegnano il loro tempo cercando di legittimarsi nei confronti del partito di appartenenza come coloro che troveranno una sintesi in un non meglio identificato progetto di legalizzazione delle convivenze. Negli ultimi mesi il fronte del SI si è arricchito di alcuni importanti esponenti dello show business italiano che, finalmente e con semplici parole spiegano in cosa consista l’ingiustizia e la discriminazione contro le famiglie omosessuali in questo paese \[*\]. Ma il nucleo forte di chi non solo ci crede ma investe in questa strategia le proprie risorse (umane ed economiche) è davvero piccolo: il primo fu Francesco Bilotta, che tentò, invano, di convincere Arcigay, ed invece riuscì, senza colpo ferire, a convincere la neonata associazione radicale certi diritti che di questa iniziativa ha fatto, giustamente, il suo biglietto da visita principale.  Ma soprattutto le coppie (una trentina quelle che hanno trovato la forza di mettersi in gioco personalmente, e la nostra riconoscenza nei loro confronti non sarà mai abbastanza) e una manciata di avvocati e giuristi che con dedizione e competenza costruiscono il puzzle di questa battaglia.

A questo primo gruppo si sono aggiunti l’On. Concia e il capogruppo del PD al Comune di Bologna Lo Giudice che, traendo le conseguenze politiche di una scelta personale, si stanno organizzando per attivare altre cause pilota. Anche Arcigay ha annunciato iniziative su questo tema,  ma ancora non sappiano esattamente cosa.

Basta tutto questo per raggiungere un obiettivo così grande e ambizioso? Basteremo di fronte al fuoco mediatico che quando si comincerà a discutere sul serio in Parlamento si scatenerà su tutti noi?  E soprattutto saremo capaci di resistere alle sirene che tenteranno di condurci sugli scogli di una proposta minima , al cui confronto i DICO saranno oro?

  
**Consapevolezza**

Non penso che nel nostro paese ci sia piena consapevolezza sul significato dell’estensione del diritto al matrimonio civile. Tantomeno nel movimento lgbt. Si tratta dell’abbattimento dell’ultimo, più significativo, ostacolo alla piena uguaglianza sostanziale, e non solo formale, delle persone omosessuali in questo paese. Non è un obiettivo estraneo al disegno costituzionale né frutto del capriccio di una minoranza agguerrita e alla moda, come qualcuno ci descrive. Ne, tantomeno, esiste contrapposizione (contenutistica o temporale) tra il diritto alla dignità e quello all’eguaglianza. In altre parole trovo stucchevole e fuori dal tempo coloro che si inventano una “maggiore facilità” o una più impellente necessità a favore di una legge contro l’omofobia per l’Italia. Da una tale legge non nascerebbe alcun diritto in più, mentre il riconoscimento del matrimonio sarebbe, di per sé, un potente contributo contro ogni forma di sessuofobia e omofobia costringendo la società intera ad interrogarsi in modo profondo su temi che tutti conoscono, come la famiglia e i figli.

E soprattutto penso che la nettezza di questa richiesta sia il principale investimento affinchè il  risultato finale della battaglia (lunga o breve che sia)  possa essere all’altezza delle aspettative e dei bisogni di chi quei diritti e quei doveri oggi non può sceglierli., come invece le persone eterosessuali possono.

Anche tra i più vicini dei nostri amici, ogni tanto, vien fuori il dubbio, la mezza frase, il richiamo a critiche su questa richiesta, così semplice eppure così difficile da comprendere. Vi sono coloro che pensano che la società non sia pronta, senza saper leggere i segni che sempre di più si moltiplicano in quella direzione. Altri pensano che il diritto al matrimonio non sia una priorità. Tutto legittimo, e dovremo investire molte energie perché l’obiettivo e la  strategia siano compresi. Ma la vera domanda è un’altra: come mai coloro che la pensano allo stesso modo, ovvero coloro che riconoscono il valore strategico della battaglia per il diritto al matrimonio civile non lo sono abbastanza per trasformare questa consapevolezza in maggiore forza? Perché non si uniscono?

  
**Forza**

Ho già elencato le scarse forze a disposizione del fronte del SI. Ed è chiaro che devono essere moltiplicate. Servono più coppie, più avvocati disponibili a mettersi in gioco, più denaro per coprire le spese legali che aumenteranno, più risorse per cercare di essere attori dell’agorà mediatica su questi temi, e non solo semplici comparse.  E soprattutto servirebbe un luogo ove ci si possa confrontare sulla strategia, far crescere la qualità delle argomentazioni e migliorare l’ efficacia delle scelte politiche e giudiziarie che dobbiamo assumere.

Invece accade che, dopo la sentenza della Corte costituzionale, Rete Lenford e l’Associazione radicale Certi Diritti non stanno più realizzando insieme la campagna di Affermazione civile, ed ancora non sappiamo esattamente il perché. Già abbiamo detto degli annunci di Paola Concia, Sergio Del Giudice e dell’Arcigay: felicissimi, ovviamente, che vadano avanti e producano iniziative, ma gli appelli e le disponibilità a parlarsi su questo obiettivo son tutti caduti nel vuoto. Non parliamo poi del Comitato Si, lo voglio!, che a ridosso della sentenza della Corte costituzionale sembrava lo strumento per rilanciare l’iniziativa di affermazione civile a tutto campo, ed è invece morto prima di nascere. Esattamente come è accaduto per la super annunciata federazione lgbt italiana o - non me lo auguro ma lo temo – come potrebbe accadere con il Coordinamento Pride nazionale. E’ come se le cose più semplici - l’unione fa la forza, per esempio -  siano difficilissime per il movimento lgbt, incapace di anteporre interessi di parte (di parte associativa, di parte personale, di parte partitica) a quelli più generali.

Ma dove la troviamo la forza di cui abbiamo bisogno per andare avanti? Come facciamo a superare questa situazione?

  
**La scelta**

Non è stato il medico a prescriverci unità, e nemmeno, lo dico subito, l’obiettivo dell’unità può essere considerato prioritario rispetto all’iniziativa politica. In più penso che, da ottimista qual sono, prima o poi l’obiettivo verrà raggiunto: anche divisi si può raggiungere la stessa meta.

Sbaglia, però, chi pensa che il tempo (e la fatica, i soldi, i sacrifici….) che dobbiamo aspettarci sia legato solo a condizioni esterne dall’influenza del fronte del SI. Certo, contano le condizioni politiche generali, conta il coraggio e la lungimiranza dei partiti e la prevalenza degli interessi collettivi sugli interessi ideologici di una parte del Paese. Ma esiste anche un ruolo, anzi DOBBIAMO costruire un ruolo per quel fronte del SI che non può sedersi a un ipotetico tavolo di trattative (chi? in rappresentanza di chi? e discutere con chi….) prima ancora che la fase delle cause pilota giunga a compimento. Prima ancora che il Parlamento prenda atto che esiste la necessità di garantire uguaglianza sostanziale e non solo formale alle persone omosessuali.

So perfettamente che i mesi che abbiamo di fronte non sono i migliori per rilanciare la necessità di unire il fronte del SI: la fase congressuale dell’Arcigay, con i fisiologici sussulti che porta con sé, e sullo sfondo il rinnovamento del Parlamento, sono gli ingredienti migliori per rendere frizzante (diciamo così…) una realtà di confronto tra le diverse, potenziali anime del fronte del SI, oggi del tutto inconsistente.

Eppure credo che per essere all’altezza delle necessità del momento e cogliere le opportunità che si aprono (l’opinione pubblica che cambia, il tempo che passa, le cause pilota, l’Europa …) si deve avere la forza di fare una scelta: serve costruire e irrobustire il fronte del SI, sostenerlo, espanderlo oltre i confini di influenza diretta dei nostri gruppi.  Serve, in altre parole, continuare la strategia, magari correggendola, perché le cause pilota sono lo strumento più potente ed efficace affinchè il Parlamento decida al meglio.

La scelta di unire il fronte del SI è nelle mani di coloro di cui ho fatto nomi e cognomi (noi compresi) e di tutti e tutte gli altri e le altre che credono in questa strategia. Non ci sono particolari condizioni, né passi indietro da fare: basta scegliere di unirsi.

Si, lo so, quello che ho scritto sembra una mozione degli affetti. Può darsi.

Ma qualcuno per favore mi può spiegare perché chi è d’accordo non si mette insieme?

Enzo Cucco

30 marzo 2012

gayindependent.blogspot.it