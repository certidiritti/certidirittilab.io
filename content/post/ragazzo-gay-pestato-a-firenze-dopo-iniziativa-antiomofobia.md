---
title: 'RAGAZZO GAY PESTATO A FIRENZE DOPO INIZIATIVA ANTIOMOFOBIA'
date: Fri, 11 Sep 2009 13:06:42 +0000
draft: false
tags: [Comunicati stampa]
---

GAY: RADICALI COMUNE, PROVINCIA E REGIONE SI COSTITUISCANO PARTE CIVILE PER RAGAZZO PESTATO IN CENTRO A FIRENZE  
  
Dichiarazione del Senatore Radicale Marco Perduca, eletto in Toscana nelle liste del Pd e Sergio Rovasio Segretario dell'Associazione Radicale Certi Diritti: "Nell'esprimere tutta la nostra solidarieta' e vicinanza al ragazzo brutalmente aggredito la notte del 9 scorso e ai suoi cari, ci appelliamo alle istituzioni locali Comune, Provincia e Regione con le quali avevamo co-promosso la fiaccolata sui ponti fiorentini, affinche' si costituiscano parte civile nella denuncia per aggressione. Dalle parole occorre iniziare a passare ai fatti, vista la grande partecipazione popolare di mercoledi' scorso sicuramente interpreterebbero l'attenzione cittadini alla lotta contro ogni discriminazione. Contiamo di poter visitare la vittima dell'aggressione nei prossimi giorni.