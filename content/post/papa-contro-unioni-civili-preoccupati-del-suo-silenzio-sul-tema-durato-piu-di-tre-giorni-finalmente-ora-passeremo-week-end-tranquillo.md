---
title: 'PAPA CONTRO UNIONI CIVILI: PREOCCUPATI DEL SUO SILENZIO SUL TEMA DURATO PIU’ DI TRE GIORNI. FINALMENTE ORA PASSEREMO WEEK END TRANQUILLO'
date: Fri, 14 Jan 2011 15:17:28 +0000
draft: false
tags: [Comunicati stampa]
---

Roma, 14 gennaio 2011

**Comunicato Stampa dell’Associazione Radicale Certi Diritti**

Ormai non passa giorno senza che il papa, o qualche gerarca clericale di turno, non ci dicano come la pensino su unioni civili, sesso, preservativi, matrimonio tra persone dello stesso sesso, diritti e responsabilità delle donne in materia di aborto, testamento biologico e libertà individuali.

Effettivamente eravamo un po’ preoccupati dal fatto che questa settimana il Vaticano ci avesse offerto su questi temi soltanto due esternazioni, mancava la terza che ora ci farà passare un week end più sereno e tranquillo.

L’esponente vaticano di turno poteva almeno fare un qualche accenno  al fatto che mentre nella Regione Lazio stanno chiudendo decine e decine di ‘Pronto Soccorso’ e Ospedali, alla Regione Lazio gli esponenti politici servi e genuflessi al Vaticano, hanno deciso di pagare, nel regime di convenzione sanitaria, le prestazioni e i ricoveri dell’ospedale cattolico Gemelli con una percentuale superiore dell’8% rispetto a tutti gli altri centri clinici privati regionali, così come denunciato dai Consiglieri Regionali radicali Giuseppe Rossodivita e Rocco Berardo.  Queste sono le vergogne di cui nessuno parla mai, mentre parlare contro unioni civili e gay va sempre bene, anche tre volte a settimana.