---
title: 'Leggi omofobe in Nigeria e Camerun: Ministro degli Esteri italiano risponde a lettera-appello di Certi Diritti'
date: Tue, 27 Dec 2011 23:01:52 +0000
draft: false
tags: [Africa]
---

Il segretario di Certi Diritti Yuri Guaiana e il Senatore Radicale-Pd Marco Perduca avevano scritto al ministro Giulio Terzi di Sant'Agata per denunciare la situazione nei due paesi. Attivati i canali diplomatici e dell'Unione Europea per sollecitare il rispetto dei trattati internazionali e dei diritti umani.

Roma, 24 dicembre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Il Ministro degli Esteri del Governo italiano, On. Giulio Sterzi di Sant’Agata, ha risposto ad una lettera inviata nei giorni scorsi dal Segretario dell’Associazione Radicale Certi Diritti, Yuri Guaiana e dal Senatore Radicale-Pd Marco Perduca, nella quale si sollecitava un intervento urgente nei confronti della Nigeria e del Camerun che recentemente hanno attivato nei loro paesi gravi forme persecutorie nei confronti delle persone omosessuali.

Il Ministro degli Esteri nella lettera ha segnalato che in Nigeria, subito dopo l’approvazione da parte del Senato del Bill anti-omosessualità si è attivata la sede di Rappresentanza dell’Unione Europea che, insieme all’Ambasciatore Usa, ha sollecitato le Commissioni Giustizia e Diritti Umani al rispetto delle convenzioni internazionali e di quanto prevede la stessa Costituzione nigeriana.

Il Ministro degli Esteri italiano ha altresì segnalato che riguardo il Camerun, dove è in corso una revisione di legge che inasprirebbe le pene per la comunità Lgbt,  e dove sono stati condannate recentemente tre persone omosessuali a cinque anni di prigione, c’è stato, in rappresentanza degli ambasciatori europei, un intervento del Capo Delegazione dell’Unione Europea nei confronti del Primo Ministro nel quale gli si esprime viva preoccupazione e si segnala che i prossimi aiuti saranno vincolati al rispetto delle convenzioni internazional. Già negli incontri Ue-Camerun erano stati fatti interventi in tal senso, richiamando alla necessità del rispetto dei diritti umani dei cittadini.

L’Associazione Radicale Certi Diritti ringrazia il Ministro degli Esteri del Governo italiano per l’attenzione dimostrata sulla vicenda.