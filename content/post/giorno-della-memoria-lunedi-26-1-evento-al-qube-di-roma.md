---
title: 'GIORNO DELLA MEMORIA: LUNEDI'' 26-1 EVENTO AL QUBE DI ROMA'
date: Sun, 25 Jan 2009 11:10:41 +0000
draft: false
tags: [Comunicati stampa]
---

**GIORNO DELLA MEMORIA 2009**

**La memoria degli altri, da un’idea diVittorio Pavoncello**

**Il Giallo e il Rosa – Shoà e Homocaust – due genetiche per uno sterminio**

_Roma, evento non stop organizzato dall’Associazione Ecad presso il locale Qube, lunedì 26 gennaio insieme all’Associazione Radicale Certi Diritti e le Associazioni lgbt italiane._

In occasione del Giorno della Memoria, dedicato alla Shoà, che ogni anno si tiene il 27 gennaio, l'Associazione Radicale Certi Diritti, insieme alle più importanti Associazioni lgbt italiane, partecipa con proprie iniziative all'evento di Roma 'Il Giallo e il Rosa' che quest'anno intende ricordare lo sterminio nazista degli ebrei e degli omosessuali.

L'evento, denominato 'La memoria degli altri', promosso dall'Associazione Ecad (Ebraismo Culture Arti Drammatiche) da un'idea di Vittorio Pavoncello,  si svolgerà a Roma, per tutta la giornata di lunedì 26 gennaio, nella cornice multipiano del locale 'Qube' con concerti, teatro, mostre, arte, stand, incontri e dibattiti.