---
title: 'Abbabusiness'
date: Fri, 23 Apr 2010 19:11:52 +0000
draft: false
tags: [Senza categoria]
---

![abbabusiness](http://www.certidiritti.org/wp-content/uploads/2009/11/abbabusiness.jpg)Generazione _abba_. Nelle imprese d'eccellenza - in Europa e nel mondo intero – sta uscendo allo scoperto la comunità GLBT – acronimo americano che definisce da vent'anni gay, lesbiche, transessuali e transgender.

Al bando la clandestinità di un tempo, questi lavoratori _abba_ fanno sempre più spesso coming out in azienda, dimostrando oggi apertamente, nella quotidianità del lavoro, un atteggiamento positivo e collaborativo e un pensare differente. Diventano così alleati di chi persegue il cambiamento, vero mantra per uscire dal pantano di una crisi tanto reale quanto percepita.

Così – in barba all'arretratezza di pensiero della società e spesso delle politiche sociali - si scopre nei fatti un nuovo profilo delle organizzazioni, tolleranti verso le differenze di genere, intese come arricchimento e creazione di valore. Per la propria cultura di impresa e - soprattutto – per il business.

_Mamma mia!_ Ecco il lato b delle imprese, che si rivela nei migliori ambienti di lavoro dove le persone sentono di poter essere autentiche, raccontandosi e mettendosi in gioco.

_Abbabusiness_ è un tratto identitario delle persone GLBT, che hanno molto spesso anticipato mode, tendenze, estetiche. Ma è anche il requisito di tutta quella parte della popolazione aziendale “etero” protagonista di una visione interculturale, interessata ad un approccio positivo verso tutto ciò che è multietnico, interreligioso e – ovviamente – multigenere.

In questo saggio gli autori - due esperti di management e cultura di impresa – inquadrano il fenomeno e spiegano perché le organizzazioni più competitive scelgono di accogliere e di rispettare i collaboratori di tutti gli orientamenti sessuali: un approccio interessato alle diversità genera valore per l’azienda che ha l’urgenza di cambiare ed è sintomo di una cultura fertile e aperta.

Abbabusiness è una fotografia dell'impresa moderna. Ed è anche un invito che i due autori rivolgono con forza a ciascun collaboratore: “Fai del tuo business un _abbabusiness_. Esci allo scoperto e colora la tua azienda”.

_Scopri l'abbabusiness anche online, su_ [www.abbabusiness.it](http://www.abbabusiness.it/)

Il testo è stato scritto da Giampaolo Colletti e Andrea Notarnicola.

**_Giampaolo Colletti_** _è un trentenne con la passione per le nuove tecnologie applicate alla cultura di impresa. Si occupa di community digitali e comunicazione interna integrata._

_Dall'età di ventidue anni lavora in aziende d'eccellenza. E' co-Founder dell'Osservatorio sull'Enteriprise Generated Content dell'Università Bocconi, network che coinvolge le maggiori imprese italiane che studiano i nuovi trend della comunicazione digitale aziendale. Ha scritto e condotto format tv per Rai Educational e per alcuni circuiti privati nazionali. Giornalista pubblicista, ha scritto per .Com, Italia Oggi e Sole24Ore nell'inserto sul mondo del lavoro Job24. Attualmente scrive per Nòva24 e collabora con Wired. E' Founder della prima wikipedia sulle nuove forme di micro web tv Altratv.tv. Pescarese di nascita e milanese di adozione, da due anni vive in un ridente paesello nel cuore della Romagna, a pochi chilometri dal mare._

**_Andrea Notarnicola_**_, partner di Newton Management Innovation - Gruppo 24 ORE, è consulente di direzione di primarie imprese globali caratterizzate da intense diversità culturali, etniche, religiose, di genere e orientamento sessuale. Formatore e coordinatore dei format di MASTER 24, si occupa di innovazione nei linguaggi d’impresa attraverso il teatro e i linguaggi tv e di edutainment (education+entertainment). Ha scritto il primo volume sulla corporate tv in Italia, uno dei primi volumi sul coaching e un manuale per la formazione dei leader del cambiamento. E’ stato uno dei promotori del Manifesto dello Humanistic Management. E’ editorialista de L’IMPRESA – Rivista italiana di management e membro del collettivo Jack O. Selz, autore di “Zzzoot – Fulminati in azienda”. E’ stato autore e conduttore di programmi educational della RAI e coordinatore dell’area sviluppo manageriale del MIB School of Management._

_Entrambi gli autori hanno scoperto l'abbabusiness. E ve lo vogliono raccontare._