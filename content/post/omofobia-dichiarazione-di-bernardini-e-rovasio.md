---
title: 'OMOFOBIA: DICHIARAZIONE DI BERNARDINI E ROVASIO'
date: Mon, 12 Oct 2009 16:18:43 +0000
draft: false
tags: [Comunicati stampa]
---

**OMOFOBIA: IL PROVVEDIMENTO OGGI IN DISCUSSIONE ALLA CAMERA DEVE ESSERE MIGLIORATO. GRAVE ESCLUDERE LA TRANSFOBIA.**

**DEPOSITATI DAI DEPUTATI RADICALI, PRIMA FIRMATARIA RITA BERNARDINI, SETTE EMENDAMENTI MIGLIORATIVI.**

I deputati radicali, prima firmataria Rita Bernardini, hanno oggi depositato sette emendamenti migliorati del testo che verrà oggi discusso alla Camera dei deputati prima della sua votazione, prevista per domani, martedì 13 ottobre.

**Gli emendamenti prevedono interventi anche in ambito sociale, educativo e formativo e propongono la conversione dell’Unar, l’Ufficio Nazionale Antidiscriminazione Razziale, costituito presso il Ministero per le Pari Opportunità, in Agenzia nazionale contro le discriminazioni”.**

**Dichiarazione di Rita Bernardini, deputata radicale Pd e Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

“Il provvedimento contro l’omofobia oggi in discussione alla Camera dei deputati necessita di miglioramenti e deve includere anche la transfobia. Escludere la lotta alla violenza contro le persone transessuali è grave perché si tratta di persone che necessitano di maggiore aiuto e sostegno e sono le più esposte al pregiudizio e alla violenza. In questi giorni ci sono stati appelli e manifestazioni che chiedono alla classe politica e al Parlamento di avere maggiore attenzione ai diversi aspetti che caratterizzano la lotta all’omofobia e alla transfobia. Ci auguriamo che le proposte migliorative vengano sostenute".

Gli emendamenti dei deputati radicali – Pd sono consultabili al seguente link:

**[http://www.certidiritti.it/tutte-le-notizie/473-gli-emendamenti-antiomofobia-e-transfobia-dei-radicali.html](tutte-le-notizie/473-gli-emendamenti-antiomofobia-e-transfobia-dei-radicali.html)**