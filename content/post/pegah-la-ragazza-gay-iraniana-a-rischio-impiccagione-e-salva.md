---
title: 'PEGAH, LA RAGAZZA GAY IRANIANA A RISCHIO IMPICCAGIONE E'' SALVA'
date: Thu, 12 Feb 2009 09:12:37 +0000
draft: false
tags: [Comunicati stampa, EVERYONE, IRAN, PEGAH]
---

RICONOSCIUTO DEFINITIVAMENTE LO STATUS DI RIFUGIATO POLITICO ALLA RAGAZZA GAY IRANIANA PEGAH EMAMBAKHSH CHE RISCHIAVA, SE RIMPATRIATA, DI ESSERE IMPICCATA.

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

"Ci è giunta oggi notizia dalla sede di Londra dell'Iranian Queer Railroad che alla ragazza lesbica iraniana, Pegah Emambakhsh, è stato riconosciuto definitivamente, dalle autorità inglesi, lo status di rifugiato politico a causa del grave rischio di morte cui sarebbe stata condannata in caso di rimpatrio forzato in Iran a causa della sua omosessualità. In Iran è infatti prevista la pena di morte per le persone omosessuali.

Per Pegah si erano mobilitate l'autunno scorso in tutta Europa migliaia di persone e decine di Associazioni che si battono per i diritti umani, in primis la nostra Associazione Radicale Certi Diritti che, insieme ai parlamentari radicali del Pd, al Gruppo EveryOne e all'impegno al Parlamento Europeo dei deputati radicali Marco Cappato e Marco Pannella, si erano attivati in tutte le sedi istituzionali per salvare la vita di Pegah.

La prima reazione di Pegah Emambakhsh alla notizia è stata: "Da questo momento sono libera. Spero che tutte le persone possano realizzare i loro sogni".