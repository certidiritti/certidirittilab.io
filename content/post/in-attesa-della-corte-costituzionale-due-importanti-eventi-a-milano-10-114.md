---
title: 'IN ATTESA DELLA CORTE COSTITUZIONALE: DUE IMPORTANTI EVENTI A MILANO 10-11/4'
date: Wed, 07 Apr 2010 13:20:37 +0000
draft: false
tags: [Comunicati stampa]
---

**UGUAGLIANZA E DIRITTI:  IN ATTESA DELLA DECISIONE DELLA CONSULTA SUI RICORSI DELLE COPPIE GAY, SABATO 10 E DOMENICA 11 APRILE L’ASSOCIAZIONE RADICALE CERTI DIRITTI PROMUOVE DUE IMPORTANTI EVENTI A MILANO.**

Dopo il 12 aprile la Corte Costituzionale si esprimerà in merito alla questione posta da alcune coppie di persone dello stesso sesso che, nell'ambito dell'iniziativa di[Affermazione Civile](http://www.affermazionecivile.it/), hanno fatto ricorso contro il rifiuto (ricevuto dai loro Comuni di residenza) alla pubblicazione degli Atti matrimoniali con rito civile. Alcuni Tribunali e Corti d'Appello hanno rinviato la questione alla Consulta.

 In vista del pronunciamento della Corte Costituzionale l’Associazione Radicale Certi Diritti, che insieme ad Avvocatura lgbt – Rete Lenford ha lanciato in Italia la campagna di Affermazione Civile, terrà a Milano nei giorni sabato 10 e domenica 11 aprile due importanti eventi:

**Sabato 10 aprile Happening-Assemblea su**:  **"La battaglia per il diritto all'eguaglianza: scenari e prospettive del dopo sentenza_"_** dalle ore 15.00 alle ore 18.00  presso  il Teatro d’arte contemporanea Elfo Puccini – Milano (sala Bausch, Corso Buenos Aires 33 - fermata MM1 - LIMA).

Assemblea pubblica per discutere e comprendere gli scenari possibili dopo la prossima sentenza della Corte Costituzionale. E per iniziare a pensare al come proseguire la battaglia.

Parteciperanno tutti i soggetti finora coinvolti  nella battaglia sul matrimonio, per il diritto all'eguaglianza: tra gli altri, gli Avvocati **Vittorio Angiolini,** **Massimo Clara e Ileana Alesso,** membri del collegio di difesa presso la Corte Costituzionale;  le **coppie** milanesi protagoniste di **Affermazione civile**; **Ivan Scalfarotto**, Vice Presidente del'Assemblea nazionale del Partito Democratico; **Sergio Rovasio**, Segretario dell'Associazione radicale Certi Diritti; **Luca Trentini**, segretario nazionale di Arcigay; il rappresentante di Avvocatura lgbg, Rete Lenford, Avvocato Matteo Winkler,  esponenti della comunità LGBT(E),  alcuni dei firmatari dell’appello.

**Domenica 11 aprile  riunione del Direttivo** **dell’Associazione Radicale Certi Diritti (**allargata ad Assemblea), dalle ore 10,30 alle ore 17, presso  la **Sala Bauer del Centro Umanitaria,** Via **San Barbara**, 48 - Milano. Tra  i punti che verranno discussi vi sono: la campagna di Affermazione Civile;  possibile riforma statuaria dell’Associazione;  nuove idee e proposte organizzative dell’Associazione.