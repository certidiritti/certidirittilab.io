---
title: 'Mozioni particolari e raccomandazioni 2010'
date: Sat, 28 Feb 2009 13:04:50 +0000
draft: false
tags: [Politica]
---

**III Congresso dell'Associazione Radicale Certi Diritti. Firenze, 30 e 31 gennaio 2010**
-----------------------------------------------------------------------------------------

### **Mozione particolare sul censimento ISTAT 2010**

Il congresso dell'Associazione radicale Certi Diritti, considerato l'intervento di Carlo D'Ippoliti [_I gay che non contano_](http://www.lavoce.info/articoli/pagina1001518.html) pubblicato su [www.lavoce.info](http://www.lavoce.info) riguardante la grave discriminazione dell'ultimo censimento verso le unioni gay, chiede agli organi dirigenti di impegnarsi affinche l'ISTAT inserisca nel regolamento di esecuzione del prossimo censimento i parametri necessari ad una reale fotografia di una realta italiana delle unini civili omosessuali.

(a prima firma Sergio Rovasio)

### **Mozione particolare per la creazione del coordinamento "Giovani per i Diritti"**

**Per la memoria del futuro.** "_Abbiamo durato, rifiutando di sopravvivere, ricominciando sempre, facendo anche delle sconfitte materia buona per dar volto e corpo alle nostre testarde, ed alla fine semplici e antiche, speranze._" **Marco Pannella**

Noi giovani omo-affettivi, etero-affettivi, trans-affettivi rifiutiamo di  "avviarci nel futuro" senza il riconoscimento di importanti diritti, senza le libertà, senza una sessualità finalmente libera. Certo, rivendichiamo questi diritti solo dopo aver compiuto i nostri doveri di cittadini, nel pieno rispetto di ogni diversità, pensiero, modo di vivere. Sentiamo il bisogno di una vera e propria Rivolta in nome di un ideale che si voglia giovane come quello che illuminò il secolo della ragione e che ritrovi nella storia i punti qualificanti del proprio programma. Vogliamo e sentiamo la necessità di continuare le battaglie di Edward Carpenter, Harvey Milk, Sylvia Rivera, Mario Mieli, Massimo Consoli e del nostro Enzo Francone. È questa tradizione nobile che ci impone il dovere di "fare qualcosa".   Per questo presentiamo a questo Congresso una mozione per la creazione all'interno dell'Associazione Radicale Certi Diritti del coordinamento "Giovani per i Diritti". Questo sarà un vero e proprio "investimento per il futuro", non soltanto dei giovani, ma che da loro partirà per aiutare tutti noi a vivere meglio. Un coordinamento che ha l'ambizione di unire tutti i "giovani liberi" che vogliono far qualcosa per i propri diritti e li farà sentire non più semplici iscritti/sudditi come accade in molte realtà associative (e non la nostra) ma "azionisti". L'azione (e non l´iscrizione, per carità!), è la condizione necessaria per essere soci di una prospettiva nuova, diversa, altra, per convogliare le forze "giovani" in un contenitore di stampo libertario, analogo al movimento radicale. Vi chiediamo quindi di dare supporto alla speranza e sostenere il nostro "futuro".

(a prima firma Matteo Di Grande)

### **Mozione particolare sulla situazione in Uganda**

Il congresso dell'Associazione Radicale Certi Diritti ringrazia il segretario dell'associazione [Non C'è Pace Senza Giustizia](http://www.npwj.org/pages/Versione+italiana) per la relazione sulla situazione in Uganda e si impegna a collaborare su questo fronte e su altri che protebbero emergere in difesa dei diritti umani delle persone LGBT nel mondo.

(a prima firma Francesco Poirè)

### **Raccomandazione per la riduzione dei costi associativi per le fasce di reddito più deboli**

In un momento congiunturale così difficile, nel mezzo di una crisi epocale che solo la miopia di chi ci governa fa sì che venga ritenuta ormai alle spalle, con migliaia di lavoratori che perdono il posto di lavoro e per questo spesso sono costretti ad azioni di protesta drammaticamente eclatanti, con un precariato così diffuso che, nel nome del dio della mobilità, rende impossibile qualsiasi progettualità della propria esistenza, in una fase così delicata della vita economica e sociale del nostro paese, riteniamo che l'Associazione Radicale Certi Diritti debba dare un segnale molto forte. Ecco perché presentiamo a questo Congresso una mozione particolare affinché venga stabilito un prezzo politico per le quote associative, con una maggiore differenziazione per status sociale. Facendo salva l'iscrizione ordinaria a 50 Euro, chiediamo che:

1.La quota associativa ridotta sia riportata a 25 Euro;

2.Essa sia riservata non solo a studenti e disoccupati, ma anche alle fasce di reddito più basse, ovvero ai pensionati, agli incapienti e a coloro che dichiarino di non superare i 15.000 Euro di reddito annui;

3.Comunque sia concesso lo stesso beneficio a tutti coloro che dichiarino di avere contratti di lavoro atipici o a tempo determinato.

Questo darebbe un segnale forte a quelle persone, militanti e non, che criticando apertamente la scelta fatta a suo tempo di quote di iscrizione molto alte rispetto alla media delle altre associazioni LGBT, non comprendono la particolarità di Certi Dirirtti che SOLO di quote associative vive, oltre al contributo volontario di alcuni sostenitori benemeriti, con la speranza che tale decisione avvicini ancora di più la gente comune alla nostra associazione.

(a prima firma Luca "Lucky" Amato)