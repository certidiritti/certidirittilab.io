---
title: 'COMMEMORAZIONE XX SETTEMBRE: DOMENICA 19 SETTEMBRE TUTTI A PORTA PIA!'
date: Mon, 13 Sep 2010 08:40:04 +0000
draft: false
tags: [certi diritti, Comunicati stampa, papa re, PORTA PIA, RADICALI, XX settembre]
---

**LA VERA CERIMONIA** **SI TERRA’ DOMENICA 19 SETTEMBRE ALLE ORE 12,30 A ROMA ALLA BRECCIA DI PORTA PIA DA OLTRE 50 TRA ASSOCIAZIONI, GRUPPI E MOVIMENTI. L’ELENCO DEGLI ADERENTI  E DEI PARTECIPANTI.**

**ANCHE ARCIGAY, CERTI DIRITTI, MARIO MIELI E GAYLIB TRA I PARTECIPANTI.**

La vera commemorazione della Breccia di Porta Pia verrà fatta da oltre 50 tra associazioni, movimenti e forze politiche, Domenica 19 settembre, alle ore 12,30, a Roma, nel luogo dove il XX Settembre 1870 fu aperto  l’acccesso alla liberazione di Roma dal potere del Papa Re.

I Radicali giungeranno a Porta Pia su un pullman scoperto dopo un tour laico che farà tappa nei luoghi simbolo della storia della laicità di Roma (Castel Sant’Angelo, P.za Risorgimento, Corso Vittorio, Piazza Giuditta Tavani Arquati, Ponte Garibaldi, Campo dè Fiori, P.za Navona, Via del Plebiscito, Via XX Settembre, Porta Pia).

Qui di seguito trasmettiamo il testo della convocazione delle organizzazioni promotrici.

Questa sarà la vera manifestazione commemorativa del XX settembre, alla presenza di cittadini, studiosi, parlamentari ed esponenti della società civile italiana.

**150° anniversario dell’Unità d’Italia**

**200° anniversario nascita di Cavour**

**140° anniversario della Breccia di Porta Pia (XX Settembre 1870)**

**Manifestazione, alla Breccia di Porta Pia, Domenica 19 settembre, ore 12,30, contro ogni fondamentalismo dell’illibertà, della frattura tra la coscienza e la scienza. Per un nuovo Risorgimento laico e liberale.**

**Per la vera commemorazione del XX Settembre!**

\*\*\*

Il 20 settembre 1870, data della Breccia di Porta Pia e della liberazione di Roma dal potere pontificio,  segna l’inizio di una nuova libertà di pensiero, di coscienza e di religione che si offre al popolo italiano ed europeo.

Ridurlo, come in passato hanno provato il fascismo e la democrazia cristiana, ad un fatto d’armi con cui una nazione si conquistò una più prestigiosa capitale, è un falso storico non tollerabile da chi si professa democratico.

Ad essere battuta, il 20 settembre, fu l'ultima trincea della più assolutista e forcaiola concezione del potere e della società, che coagulava intorno a sé ogni sorta di ostilità alla civiltà moderna.

In occasione del 140° anniversario, a fronte delle tentazioni revisioniste portate avanti dalle autorità “ufficiali”,  le stesse che per decenni hanno operato una sistematica rimozione culturale della Roma laica e risorgimentale, vogliamo ricordarne il significato e il valore epocale nella storia italiana  ed internazionale, sul piano istituzionale, politico, laico e religioso.

Contro ogni fondamentalismo dell'illibertà, della violenza, della frattura tra la coscienza e la scienza.

Per un nuovo Risorgimento laico e  liberale.

**PROMOTORI, ADERENTI, PARTECIPANTI:**

\- Radicali Italiani

\- Associazione Radicale Certi Diritti

\- Anticlericale.net

\- Gruppo Consiliare Lista Bonino Pannella, Federalisti Europei alla Regione Lazio

\- Consulta Romana per la Laicità delle Istituzioni

\- Associazione Luca Coscioni

\- Associazione Radicali Roma

\- Arcigay Nazionale

\- Uaar Roma

\- Uaar nazionale

\- Critica Liberale

\- Radio Radicale

\- Associazione nazionale del Libero Pensiero “Giordano Bruno”

\- Psi, Federazione Romana

\- Federazione dei Giovani Socialisti

\- Lettera Internazionale, Rivista europea

\- Associazione Libera Uscita

\- Lega Italiana Divorzio Breve

\- Cgil - Nuovi Diritti

\- Forum Donne Socialiste

\- Rete Laica Bologna

\- ItaliaLaica – giornale dei laici italiani

\- Associazione Democrazia Laica

\- Associazione democratica Giuditta Tavani Arquati

\- Federazione dei Verdi del Lazio

\- Fondazione Massimo Consoli

\- Gajamente Critical Forum Lgbtq

\- PeaceWaves International Network

\- Quaderni Radicali, bimestrale politico Radicale

\- Democrazia Atea

\- CRIDES- Centro Romano di Iniziativa per la Difesa dei Diritti nella Scuola

\- Circolo di cultura omosessuale Mario Mieli di Roma

\- Associazione Sabina Radicale

\- Lega Arcobaleno, Federazione di Associazioni impegnate sui problemi della disabilità e dell'handicap

\- Era, Associazione Radicale Esperantista

\- Associazione “I Garibaldini del terzo millennio”

\- European Consumer

\- Coordinamento Radicali Maremma

\- Associazione Radicale LiberaPisa

\- Gay Lib

\- Liberacittadinanza - Rete dei Girotondi e Movimenti

\- Rivista “Confronti”

\- Direzione Nazionale del Nuovo Partito d’Azione

-Democrazia Atea - partito politico

**_Organizzazione, ufficio stampa, adesioni:_**

**_[info@radicali.it](mailto:info@radicali.it) Tel. 06-65937036_**

**_Sergio Rovasio_** **_– Diego Sabatinelli_**

* * *

**_IL 20 SETTEMBRE CONVEGNO STORICO PRESSO LA SEDE DELLA PROVINCIA DI ROMA - PALAZZO VALENTINI_**

20 settembre 2010 -   ore 16.30 - 19.30

Sala di Liegro, Palazzo Valentini , Via IV Novembre 119/a, Roma

Saluto del Coordinatore della Consulta Romana

**Carlo Cosmelli**

_Dipartimento di Fisica, Sapienza Università di Roma_

Saluto del Vicepresidente della Provincia di Roma

**_Cecilia d’Elia_**

**Marcello Vigli**

_Consulta Romana per la Laicità delle Istituzioni_

   Coordinamento degli interventi e del dibattito

**Giuseppe Monsagrati**

_Storia del Risorgimento, Sapienza Università di Roma_

  Il mito di Roma: dalla Repubblica Romana del 1849 al 1870

**Anna Maria Isastia**

_Storia Contemporanea, Sapienza Università di Roma_

  Il XX Settembre tra polemiche e celebrazioni

**Anna Foa**

_Storia Moderna, Sapienza Università di Roma_

 Gli  Ebrei di Roma dal Ghetto all’emancipazione

**Nicola Tranfaglia**

_Storia dell’Europa e del giornalismo, Università di Torino_

 Il significato del XX settembre oggi.

**Dibattito**

Consulta Romana per la Laicità delle Istituzioni
================================================