---
title: 'CONVEGNO A ROMA, 30 MAGGIO ALLE 17.30: "I FIGLI FANTASMA DELLE COPPIE FANTASMA" '
date: Tue, 27 May 2008 19:43:01 +0000
draft: false
tags: [Comunicati stampa, Famiglia Fantasma, Famiglie Arcobaleno, Famiglie Fantasma, Figli Fantasma]
---

Convegno: **"I figli fantasma delle coppie fantasma. Assenza di tutela e welfare nelle famiglie omogenitoriali."** A Roma, 30 Maggio alle 17.30,  Sala del Consiglio dell'[XI Municipio in Via Benedetto Croce, 50](http://maps.google.it/maps?f=q&hl=it&geocode=&q=via+benedetto+croce,+50+roma&sll=41.841787,12.484455&sspn=0.011254,0.028667&ie=UTF8&ll=41.841464,12.483902&spn=0.022508,0.057335&z=15&iwloc=addr)  
  
Interventi:  
  
**Giuseppina La Delfa** (presidente delle Famiglie Arcobaleno);  
**Chiara Lalli** (docente di Logica e Filosofia della Scienza presso la Facoltà di Medicina e Chirurgia dell’Università “La Sapienza” di Roma; sta scrivendo un libro sulle FA);  
**Susanna Lollini** (avvocato delle FA che illustrerà una proposta di legge sulle famiglie omogenitoriali);  
**Paola Concia** (che spiegherà come intende presentarla);  
  
_coordinatore: **Tommaso Giartosio**_