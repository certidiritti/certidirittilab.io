---
title: 'Newsletter 14 Maggio'
date: Wed, 31 Aug 2011 10:03:16 +0000
draft: false
tags: [Politica]
---

**![](http://old.radicalparty.org/pub/certidiritti_logo.jpg)**

Paradossalmente la Corte di Giustizia europea rafforzando i diritti delle coppie dello stesso sesso in alcuni Stati (vedi la sentenza che sancisce il diritto alla pensione anche per le coppie non sposate) e non intervenendo in quelli, dove le ingiustizie di chi è discriminato in base all’orientamento sessuale sono più evidenti come in Italia, riesce ad aumentare e consolidare quel “discrimination gap” che è sempre più critico tra gli stati omofobi a rischio di integralismo e gli stati più laici e socialmente avanzati.

Intanto il Parlamento ugandese ha rinviato il voto sulla legge antigay dopo le pressioni internazionali. Ma non possiamo fermarci ora. Continua a sostenere la nostra associazione e il fondo David Kato Kisule.

Il 15 e 16 maggio vota per i candidati iscritti a Certi Diritti. Sotto tutti i nomi.

Buona lettura

**IN MEMORIA DI DAVID KATO KISULE**
===================================

**Parlamento ugandese rinvia voto su legge antigay.   
[Dedichiamo questa giornata a David Kato Kisule >](tutte-le-notizie/1125-parlamento-ugandese-rinvia-voto-su-legge-antigay.html)**

**[SOSTIENI IL FONDO DAVID KATO KISULE necessario a sostenere le spese legali per assicurare un processo equo e veritiero sulla sua morte >](campagne/in-memoria-di-david-kato/1071-fondo-in-memoria-di-david-kato-kisule.html)**

  
**All’ugandese gay Kasha Jacqueline Nabagesera il Premio Martin Ennals.   
[Leggi >](http://www.notiziegay.com/?p=74708)**

ASSOCIAZIONE
============

**Candidati di Certi Diritti**

**a Trieste: [Clara Comelli](http://www.claracomelli.it/wp/tag/trieste-cambia/), candidata per Trieste Cambia**

**a [Milano](http://milano.boninopannella.it/candidatilistaboninopannellacomune): Emma Bonino, Marco Cappato, ****Carlo Daniel Cargnel, Giulia Crivellini Yuri Guaiana, Leonardo Monaco, Francesco Poirè, Mina Welby**

**Lista Bonino - Pannella: presentazione del programma LGBT alle elezioni comunali di Milano.   
[Ascolta su Radio Radicale >](http://www.radioradicale.it/scheda/326466/lista-bonino-pannella-presentazione-del-programma-lgbt-alle-elezioni-comunali-di-milano)**

**Lista Bonino Pannella - Guaiana: Cresce l'omofobia nel milanese: come sempre la Moratti sta a guardare.****   
[Leggi >](http://www.radicali.it/20110507)**

**Mattia Feltri su la Stampa: il PD all'Ikea coi radicali...  
[Leggi >](tutte-le-notizie/1110.html)**

_**Certi Diritti**_**: la curia di Palermo è fuori dalla grazie di dio.   
[Leggi >](tutte-le-notizie/1111.html)**

**Coppie gay attaccate da Giovanardi? Il commento di Certi Diritti.   
[Leggi >](http://www.gaywave.it/articolo/coppie-gay-attaccate-da-giovanardi-il-commento-di-certi-diritti/30789/)**

**Omosessualità: intervista a Valerio Di Bussolo, a Giuseppina La Delfa ed a Sergio Rovasio sull'ultima campagna pubblicitaria di IKEA.  
[Ascolta >](http://www.radioradicale.it/scheda/326477)**

**16 maggio a Roma serata contro l'omofobia e la trans fobia.  
[Leggi >](tutte-le-notizie/1118.html)**

**Ivan Scalfarotto distribuisce la patente di ‘omofobo’. **Il primo a riceverla è Beppe Severgnini e fa un pastrocchio …**   
[Leggi >](http://www.notiziegay.com/?p=74953&)**

**Commissione Europea conferma sostegno per diritti Trans.  
[Leggi >](tutte-le-notizie/1113.html)**

**Interrogazione al Parlamento Europeo su omofobia in Italia.**  
[**Leggi >**](tutte-le-notizie/1119.html)

**[Spes contra spem, per una stagione dei diritti. Di Domenico Naso >](http://www.fareitalia.com/180_spes_contra_spem__per_una_stagione_dei_diritti)**

**GLI EVENTI DI MAGGIO CHE CI COINVOLGONO COME CERTIDIRITTI:**
==============================================================

**Giovedì 12 e venerdì 13 **Convegno **a Firenze **su Eguaglianza e giustizia I diritti LGBTI (di lesbiche, gay, bisessuali, trans, intersessuati) nel XXI secolo;

**domenica 15 **presentazione libro di Angelo Pezzana al salone del libro di **Torino** ore 12. Con Sergio Rovasio.

**lunedì 16 evento di CD a Roma **documentario sulla storia del Fuori,presentazione libro e confronto con altri esponenti della Comunità LGBT italiana, in occasione della giornata internazionale contro l' omofobia;

**martedì 17 **alle ore 10 **a Roma** incontro tra il Presidente della Camera dei deputati Gianfranco Fini alla Camera e le associazioni LGBT italiane;

**martedì 17 **pomeriggio **a Parma** evento con il Comune e Certi Diritti su lotta omofobia;

**venerdì 20 a Palermo **con Rita Bernardini, dibattito partiti su diritti LGBT;

**sabato 21 a Palermo, **visita al Carcere dell'Ucciardone con Rita Beranrdini e, nel pomeriggio, Pride Palermo

**giovedì 26 e venerdì' 27 a Roma **evento di Cd con altre 3 organizzazioni all'Acrobax in vista dell'Europride

**venerdì 10 giugno convengo di Certi Diritti presso sede del Pe a Roma**

**sabato 11 giugno a Roma Europride**

RASSEGNA STAMPA
===============

‎**Stuart Milk: “L’Italia approvi la legge contro l’omofobia”****   
[Leggi >](http://www.ilfattoquotidiano.it/2011/05/06/stuart-milk-litalia-approvi-la-legge-contro-lomofobia/109442/)**

**Con Stuart Milk anche la Bindi si converte alla causa omosessuale.  
[Leggi >](http://www.notiziegay.com/?p=74753)**

**In risposta a Giovanardi. Enrico Vesco (Regione Liguria): “Chi attacca le coppie omosessuali non rappresenta l’Italia”.   
[Leggi >](http://gaynews24.com/?p=20733)**

**Effetto Giovanardi a Palermo. Scritte omofobe sui manifesti del pride.  
[Leggi >](http://www.notiziegay.com/?p=74650)**

**Gay, la protesta è di famiglia.   
[Leggi >](http://espresso.repubblica.it/dettaglio/gay-la-protesta-e-di-famiglia/2150449/8)**

**Legge sempre più lontana. L’omofobia? Può attendere …  
[Leggi >](http://www.notiziegay.com/?p=74686)**

**Legge sull’omofobia. Pierluigi Mantini (Udc): il mio partito è contrario al reato di omofobia.  
[Leggi >](http://gaynews24.com/?p=20715)**

**La7 Exit: Intervista a don Giovanni Bellò. AVI.  
[Guarda il video >](http://www.youtube.com/watch?v=cFo2dDdHIFI)**

**Elezioni, i vescovi in campo:non votate pro aborto e gay.  
[Leggi >](http://www.ilsecoloxix.it/p/italia/2011/05/07/AOWFfLS-elezioni_vescovi_votate.shtml)**

**Borghezio: troppi figli per gli islamici , una tassa su nascite.  
[Leggi >](http://tg.la7.it/politica/video-i415598)**

**Due matrimoni gay in Italia il 21 maggio con rito anglicano vetero -cattolico.   
[Leggi >](http://www.queerblog.it/post/11130/due-matrimoni-gay-in-italia-il-21-maggio-con-rito-anglicano-vetero-cattolico)**

**Bacio gay al Colosseo multa da 2mila euro.   
[Leggi >](http://roma.repubblica.it/cronaca/2011/05/04/news/gay-15785305/)**

**Roma. Bacio omo al Colosseo, Gaycenter annuncia che ricorrerà in appello.   
[Leggi >](http://gaynews24.com/?p=20729)**

**"A Bruzzano una ronda omofoba aggredisce coppie omosessuali"  
[Leggi >](http://milano.repubblica.it/cronaca/2011/05/06/news/a_bruzzano_una_ronda_omofoba_aggredisce_coppie_omosessuali-15842552/)**

**Palermo: Chiesa vietata ai gay cristiani "Pregheremo davanti al portone"   
[Leggi](http://palermo.repubblica.it/cronaca/2011/05/07/news/chiesa_vietata_ai_gay_cristiani_pregheremo_davanti_al_portone-15897845/)****[>](http://palermo.repubblica.it/cronaca/2011/05/07/news/chiesa_vietata_ai_gay_cristiani_pregheremo_davanti_al_portone-15897845/)**

**Palermo:Giovane Italia, da associazioni gay nuovo attacco alla chiesa.   
[Leggi >](http://www.libero-news.it/news/731675/Palermo__Giovane_Italia__da_associazioni_gay_nuovo_attacco_a_chiesa.html)**

**Gay e felici: la doppia vita dei preti omosessuali che la Chiesa vorrebbe “curare”.   
[Leggi e guarda i video >](http://www.siciliainformazioni.com/giornale/societa/123833/felici-doppia-vita-preti-omosessuali-chiesa-omofoba-vorrebbe-curare.htm)**

**Gay, la protesta è di famiglia.   
[Leggi >](http://espresso.repubblica.it/dettaglio/gay-la-protesta-e-di-famiglia/2150449/8)**

**Elezioni al Viagra… “Più Viagra per tutti”, l’ultima battaglia del PdL.  
[Leggi >](http://www.notiziegay.com/?p=74757)**

**Battuta su Bindi e Concia, Pisapia: «Mantovani offende e la Moratti tace» **Anche Manfredi Palmeri attacca: penose battute da avanspettacolo contro le donne e i gay. **  
[Leggi](http://milano.corriere.it/milano/politica/speciali/2011/elezioni-amministrative/notizie/cena-mantovani-donne-moratti-bindi-pisapia-190569708863.shtml)****[>](http://milano.corriere.it/milano/politica/speciali/2011/elezioni-amministrative/notizie/cena-mantovani-donne-moratti-bindi-pisapia-190569708863.shtml)**

**“Sei gay, non puoi insegnare religione cattolica”   
[Leggi >](http://www.giornalettismo.com/archives/124232/se-sei-gay-non-puoi-insegnare-la-religione-cattolica/)**

**Lega Nord. A Bologna c’è un candidato omosessuale ed ex porno attore.   
[Leggi >](http://gaynews24.com/?p=20839)**

**Il j’accuse dai toni omofobi dell’ ‘agente Betulla’: l’Europa dei cetrioli vuole rifilarci i matrimoni gay.  
[Leggi >](http://www.notiziegay.com/?p=74640)**

**Inghilterra: Leeds, Charity cattolica perde appello contro adozioni gay.****  
[Leggi >](http://www.toscanaoggi.it/news.php?IDNews=22805&IDCategoria=206)**

**GIUSTIZIA. Corte Ue: unione civile omosessuale, stesso diritto alla pensione di coppia sposata.   
[Leggi >](http://www.helpconsumatori.it/news.php?id=32755)**

**Siria/ Ragazza lesbica di Damasco diventa un'eroina della rivolta.  
[Leggi >](http://notizie.virgilio.it/notizie/esteri/2011/05_maggio/06/siria_ragazza_lesbica_di_damasco_diventa_un_eroina_della_rivolta,29501057.html)**

**D****al blog di Amina, ragazza lesbica diventata eroina della rivolta in Siria.  
[Leggi >](rassegna-stampa/1115.html)**

**Sudafrica, stuprata perché lesbica. Ragazzina di 13 anni violentata per ‘rieducazione’.  
[Leggi >](http://www.blitzquotidiano.it/agenzie/lesbica-stuprata-a-13-anni-va-rieducata-choc-in-sudafrica-848016/)**

**Sudafrica: il governo creerà un gruppo spec****iale per combattere le violenze contro le persone omosessuali.   
[Leggi >](http://www.queerblog.it/post/11152/sudafrica-il-governo-creera-un-gruppo-speciale-per-combattere-le-violenze-contro-le-persone-omosessuali)**

**Omofobia In Messico: Ucciso Attivista Gay.  
[Leggi >](http://www.paid2write.org/attualita_gossip/omofobia_in_messico_ucciso_attivista_gay_14737.html)**

**Minnesota: i repubblicani chiedono che la Costituzione vieti le nozze gay.   
[Leggi >](http://www.queerblog.it/post/11128/minnesota-i-repubblicani-chiedono-che-la-costituzione-vieti-le-nozze-gay)**

**USA La coppia gay che ha adottato dodici bambini.   
[Leggi >](http://www.giornalettismo.com/archives/124196/la-coppia-gay-che-ha-adottato-dodici-bambini/)**

**Louisiana**. **Lo Stato dove il sesso orale è un crimine contro natura**. In Louisiana una legge punisce tutti i rapporti non vaginali. Con pene fino all’ergastolo.**   
[Leggi >](http://www.giornalettismo.com/archives/124300/sesso-orale-crimine-contro-natura/)**

**Stato di New York. ‘Santa’ alleanza. Le religioni unite contro i matrimoni gay.   
[Leggi >](http://www.notiziegay.com/?p=74934)**

**USA. ****Che "genere" di giustizia. ****  
[Leggi >](http://www.linkiesta.it/blogs/diario-americano/genere-giustizia)**

**Richard e John: “fidanzati” da 61 anni, sperano di potersi sposare.  
[Leggi >](http://www.queerblog.it/post/11122/richard-e-john-fidanzati-da-61-anni-sperano-di-potersi-sposare)**

**Da Cuba Mariela Castro dice sì ai matrimoni gay.  
[Leggi >](http://gaynews24.com/?p=20681)**

**Brasile. La Corte Suprema riconosce i diritti legali alle coppie omosessuali.  
[Leggi >](http://www.rainews24.rai.it/it/news.php?newsid=152535)**

**Elezioni in Canada: la vittoria della destra minaccia i diritti gay?  
[Leggi >](http://www.queerblog.it/post/11138/elezioni-in-canada-la-vittoria-della-destra-minaccia-i-diritti-gay)**

STUDI, RICERCHE E MATERIALI INFORMATIVI
=======================================

**Il brutto delle unioni moderne: le coppie di fatto? Scoppiano in tribunale come quelle sposate.   
[Leggi >](http://www.ilgiornale.it/cronache/il_brutto_unioni_moderne_coppie_fatto__scoppiano_tribunale_come_quelle_sposate/04-05-2011/articolo-id=520948-page=0-comments=1)**

**Belgrado, capitale della Queeroslavia.   
[Leggi >](http://www.balcanicaucaso.org/ita/Tutte-le-notizie/Belgrado-capitale-della-Queeroslavia-93286)**

**Una riflessione sulle unioni civili.  
[Leggi >](http://www.generazioneitalia.it/2011/05/03/una-riflessione-sulle-unioni-civili/)**

**Il paese chiama, ma il legislatore non risponde.   
[Leggi >](http://www.viasarfatti25.unibocconi.it/notizia.php?idArt=7874)**

LIBRI E CULTURA
===============

**In Italia il film L'amour fou su Ysl e Pierre Bergé.   
[Leggi >](http://www.queerblog.it/post/11142/in-italia-il-film-lamour-fou-su-ysl-e-pierre-berge)**

**Torino: Tomboy di Celine Sciamma vince il Festival del Cinema Gay.   
[Leggi >](http://www.g-friendly.com/?p=1153)**

**Va in scena il bacio gay. Polemica su Romeo e Giulietto.   
[Leggi >](http://corrieredelveneto.corriere.it/veneto/notizie/cultura_e_tempolibero/2011/6-maggio-2011/va-scena-bacio-gay-polemica-romeo-giulietto-190580068620.shtml)**

**Lo stilista Marc Jacobs a favore dei matrimoni gay.   
[Leggi >](http://www.queerblog.it/post/11121/marc-jacobs-a-favore-dei-matrimoni-gay)**

**Femministe di sinistra sedotte dallo scientismo.  
[Leggi >](http://archiviostorico.corriere.it/2011/maggio/03/Femministe_sinistra_sedotte_dallo_scientismo_co_9_110503018.shtml)**

**Video gay collaborativo contro l'omofobia.   
[Guarda il video >](http://www.youtube.com/watch?v=ex6QEhRSwcg)**

**Modern Family - Il Re Leone.   
[Guarda il video >](http://www.youtube.com/watch?v=Ci6oRmKYniQ&feature=related)**

**Scusa se ti chiamo frocio … Da Zappa a Bukowski, gli artisti censurabili per omofobia. [Leggi >](http://www.notiziegay.com/?p=74818)**

**Omosessuali di prima penna.  
[Leggi >](http://www.gay.it/channel/libri/31656/Omosessuali-di-prima-penna.html)**

**Scaraffia L. (a cura di)****, _[Bioetica come storia](http://www.lindau.it/schedaLibro.asp?idLibro=1276), Come cambia il modo di affrontare le questioni bioetiche nel tempo_, Ed. Lindau, ****€****23,00**

**Ivan Scalfarotto, _[In nessun paese](http://www.edizpiemme.it/libri/in-nessun-paese-9788856614442)_, Piemme editore (2010), € 17.50**

**Luca Scarlini, _[Un paese in ginocchio](http://www.guanda.it/scheda.asp?editore=Guanda&idlibro=7163&titolo=UN+PAESE+IN+GINOCCHIO)_, Guanda, € 13.00****0**

[www.certidiritti.it](http://www.certidiritti.it/)
==================================================