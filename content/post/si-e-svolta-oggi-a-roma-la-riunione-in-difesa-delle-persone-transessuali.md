---
title: 'SI E'' SVOLTA OGGI A ROMA LA RIUNIONE IN DIFESA DELLE PERSONE TRANSESSUALI'
date: Thu, 07 Jan 2010 19:33:13 +0000
draft: false
tags: [Comunicati stampa]
---

EMERGENZA VIOLENZA CONTRO TRANSESSUALI: SI SONO RIUNITE OGGI  A ROMA LE ASSOCIAIZONI PER LA DIFESA DEI DIRITTI E DELLA DIGNITA’ DELLE PERSONE TRANSESSUALI. DECISE AZIONI E INIZIATIVE CONCRETE CONTRO LA VIOLENZA E IL PREGIUDIZIO.  
  
Roma, 7 gennaio 2009

  
Su proposta dell’Associazione Radicale Certi Diritti si sono riunite oggi a Roma, presso la sede dei Radicali, le principali associazioni italiane che si occupano della difesa dei diritti e della dignità delle persone transessuali. La riunione è nata dall’esigenza di studiare azioni concrete con l’obiettivo di sensibilizzare il Ministro degli Interni, i media e la società civile, riguardo il grave fenomeno della violenza di cui sono state vittime le persone transessuali in questi ultimi mesi. Tra i punti che verranno elaborati in proposte concrete, con l’obiettivo di aiutare, sostenere e proteggere le persone transessuali, vi sono temi che riguardano l’ambito del lavoro, dei media, dell’assistenza sanitaria e sociale, anche con iniziative legislative.   
  
Tali proposte verranno  definite da commissioni di lavoro che si sono costituite su specifici temi. Sabato 23 gennaio si svolgerà a Roma un’altra riunione nazionale che dovrà definire termini e modalità degli incontri con il Ministro degli Interni, con la Presidente della Commissione Giustizia della Camera dei deputati e  con altre istituzioni.  
  
All’incontro, coordinato da Fabianna Tozzi,  Presidente Associazione Transgenere e Sergio Rovasio, Segretario dell’Associazione Radicale Certi Diritti, hanno partecipato, tra le altre e gli altri, Imma  Battaglia, Presidente di DiGayProject, Paola Concia, deputata del Pd; Helena Velena; Darianna Saccomani, co-promotrice Congresso italiano Transessuali; Leila Deianis, Presidente Ass.ne Libellula; Maria Gigliola Toniollo, Cgil Nuovi Diritti; Sergio Stanzani, Presidente del Partito Radicale Nonviolento;  Mirella Izzo, Presidente Crisalide PanGender; l’Avv. Federica Pezzoli; Deborah Orlandini, Circolo Pasolini di Lecce; Francesca Eugenia Busdraghi, Presidente nazionale Azione Trans; Porpora Marcasciano, Presidente del Movimento Identità Transessuali; Gruppo Ireos Firenze; Gruppo Every One; Cristiana Alicata, tavolo lgbt del Pd; Alba Montori e Claudio Mori, Fondazione Massimo Consoli; Andrea Maccarrone, Guido Allegrezza, esponenti del movimento lgbt.