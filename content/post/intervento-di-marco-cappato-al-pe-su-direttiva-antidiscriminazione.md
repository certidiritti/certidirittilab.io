---
title: 'INTERVENTO DI MARCO CAPPATO AL PE SU DIRETTIVA ANTIDISCRIMINAZIONE'
date: Fri, 23 May 2008 14:27:28 +0000
draft: false
tags: [Comunicati stampa]
---

TESTO INTEGRALE DELL’INTERVENTO DEL DEPUTATO EUROPEO RADICALE MARCO CAPPATO ALL’ASSEMBLEA PLENARIA DEL PARLAMENTO EUROPEO SVOLTO A STRASBURGO IL 20-5-20068 SULLA NORMA ANTIDISCRIMINAZIONE DELLA COMMISSIONE EUROPEA  
  

Marco Cappato (ALDE). – Signor Presidente, onorevoli colleghi, grazie al lavoro della collega Lynne, questo Parlamento si accinge a chiedere di nuovo una direttiva orizzontale ed è la nona volta, Commissario Špidla, che il Parlamento chiede questo.

Allora di fronte a un Parlamento che per la nona volta fa una richiesta così precisa, ci si attenderebbe una maggiore chiarezza da parte della Commissione in quest'Aula, anche perché corrisponde a un impegno del Presidente Barroso, crediamo anche al lavoro del cosiddetto impact assessment, che è in corso di elaborazione.

Francamente non si capisce, da parte di alcuni colleghi del gruppo popolare, questa idea di opporsi a una direttiva orizzontale. Si rimprovera giustamente a una certa sinistra il vizio di voler dividere la società per corporazioni e per rappresentanza di corporazioni. Mi sembra però che qui ora lo volete fare voi: i disabili sì perché magari è più politicamente corretto, altre forme di discriminazione: no aspettiamo...  

La collega Oomen-Ruijten non c'è, ma è un altro vizio di una certa sinistra dire: ma più che le leggi serve cambiare la mentalità e questa volta lo sentiamo da parte dei colleghi del gruppo popolare. Parliamo di diritti individuali e parliamo della necessità di non giocare una corporazione contro un'altra, una minoranza contro un'altra, ma di assicurare la non discriminazione per il 100% dei cittadini. Questo ha senso, questo vi chiediamo! Altri provvedimenti un po' alla volta per una categoria e non per l'altra, francamente non ne abbiamo bisogno, non ne hanno bisogno i cittadini dell'Unione.