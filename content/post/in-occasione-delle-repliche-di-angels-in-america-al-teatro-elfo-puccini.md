---
title: 'A Milano il 25 maggio ''L''omosessualità? Non può essere reato'' in occasione delle ultime repliche del cult ''Angels in America'''
date: Sat, 19 May 2012 11:15:29 +0000
draft: false
tags: [Africa]
---

Venerdì 25 maggio ore 17:00 | **INCONTRO**  
Teatro Elfo Puccini | Milano |  
C.so Buenos Aires, 33 MM Lima  
  
  

L'OMOSESSUALITÀ? NON PUÒ ESSERE REATO

_In memoria di _[_David Kato Kisule_](http://www.elfo.org/persone/guests/davidkatokisule.html)

Interventi dal vivo e video messaggi per fare il punto sulla campagna globale per depenalizzare l'omosessualità in decine di paesi in tutto il mondo.  
Intervengono Elio Polizzoto (Centro per la difesa e promozione dei diritti umani), Yuri Guaiana (Associazione Radicale Certi Diritti), Alessandro Demarie (Amnesty International)  
**Evento organizzato in collaborazione con [Associazione Radicale Certi Diritti](http://www.certidiritti.it/)**

_Ingresso libero_

* * *

* * *

Dal 22 maggio al 3 giugno al Teatro Elfo Puccini ultima occasione per rivedere lo spettacolo culto degli ultimi 10 anni di storia dell'Elfo, vincitore di tutti i maggiori premi italiani e campioni di incassi e di pubblico. 

SALA SHAKESPEARE | **22 MAGGIO - 3 GIUGNO 2012  **  
MAR-SAB: 20:30 / DOM: 15:30 

**[**ANGELS IN AMERICA Fantasia gay su temi nazionali**  
Si avvicina il millennio e Perestroika](http://www.elfo.org/stagioni/20112012/angelsinamerica.html)  
**di Tony Kushner  
  
uno spettacolo di Ferdinando Bruni e Elio De Capitani  
con Elio De Capitani, Elena Russo Arman, Cristina Crippa, Ida Marinelli, Cristian Giammarini, Edoardo Ribatto, Fabrizio Matteini, Umberto Petranca, Sara Borsarelli  
produzione Teatridithalia/ERT Emila Romagna Teatro Fondazione

_A David Kato Kisule, attivista ugandese e difensore dei diritti umani, ucciso dall'odio e dal pregiudizio dedichiamo la gaiezza travolgente e gioiosa, lo splendore ironico e barocco,_ _la tenerezza struggente delle ultime repliche di Angels in America a Milano e a Madrid.  __Questa “Divina Commedia dei nostri anni laici e tormentati" è divisa in due parti, che andranno in scena,_ _sia autonomamente, in due serate diverse, sia in un'unica fluviale maratona di sette ore. __Abbiamo rappresentato per la prima volta la versione integrale in occasione dell’inaugurazione dell’Elfo Puccini nel 2010_ _e gli spettatori la ricordano come una grande e indimenticabile emozione. __Inevitabile, dunque, riproporre la maratona a Milano per l'ultima volta._

**PROMOZIONE**

mercoledì 23 maggio: PRIMA PARTE - Si avvicina il millennio - ORE 20:30

venerdì 25 maggio: SECONDA PARTE - Perestroika - ORE 20:30

**BIGLIETTI RIDOTTI a ****11,50 EURO cad.** (fino esaurimento posti disponibili)

**Prenotazioni: clicca su rispondi e sostituisci l’indirizzo del destinatario con [promozione@elfo.org](mailto:%3Ca%20href=) e indicando come oggetto **PROMOANGELS****

**Domenica 3 giugno: MARATONA ANGELS - **Si avvicina il millennio+Perestroika** \- ORE 15:30 (con un'ora di intervallo tra le due parti)**

****BIGLIETTI RIDOTTI a 21****,50 EURO cad.** (fino esaurimento posti disponibili)**

**Prenotazioni: clicca su rispondi e sostituisci l’indirizzo del destinatario con [promozione@elfo.org](mailto:promozione@elfo.org) e indicando come oggetto **PROMO MARATONA****