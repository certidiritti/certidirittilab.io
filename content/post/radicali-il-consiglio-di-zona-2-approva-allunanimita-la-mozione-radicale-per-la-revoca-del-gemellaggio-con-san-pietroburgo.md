---
title: 'Radicali: il consiglio di zona 2 approva all''unanimità la mozione radicale per la revoca del gemellaggio con San Pietroburgo'
date: Fri, 01 Jun 2012 08:16:26 +0000
draft: false
tags: [Russia]
---

dichiarazione di Yuri Guaiana, Segretario dell'associazione radicale Certi Diritti e vicepresidente della Zona 2 di Milano.

Milano, 1 giugno 2012

Il Consiglio di Zona 2 ha approvato nella seduta del 31 maggio 2012 la mozione presentata dal vicepresidente della Zona Yuri Guaiana, del Gruppo Radicale-Federalista Europeo, per la revoca del patto di gemellaggio tra Milano e San Pietroburgo.

La mozione, approvata con 21 voti a favore e 3 astenuti, chiede la revoca del gemellaggio con l'ex capitale zarista in ragione della legge approvata, il 29 febbraio scorso, dal parlamento di San Pietroburgo che sanziona la cosiddetta "propaganda dell'omosessualità" criminalizzando di fatto qualunque attività o informazione relativa alle persone LGBTI (Lesbiche, Gay, Bisessuali, Transessuali e Intersessuali) e alle relazioni tra persone dello stesso sesso, in patente violazione delle libertà di espressione e associazione, nonché degli impegni presi dalla Russia ratificando la Convenzione europea per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali.

**La stessa mozione è stata presentata anche al Consiglio Comunale dal consigliere radicale Marco Cappato ed è stata firmata da tutti i capigruppo della maggioranza.** L’iniziativa ha inoltre avuto l'appoggio di Elio De Capitani e Ferdinando Bruni che prima delle repliche del loro Angels in America leggono un testo per chiedere a Milano di dare un segno tangibile a favore dei diritti umani e delle libertà fondamentali revocando il patto di gemellaggio con San Pietroburgo. Gli spettacoli dell’Elfo come Angels in America e History Boys, infatti, rischiano di non poter più essere messi in scena nella ex capitale zarista.

Yuri Guaiana, che è anche segretario dell'Associazione Radicale Certi Diritti, ringrazia Elio De Capitani e Ferdinando Bruni, il Consiglio di Zona 2 per il voto espresso e auspica che il consiglio comunale segua al più presto il suo esempio.

**Il 7 giugno 2012 alle 18:00 il Teatro dell'Elfo ospiterà un incontro, organizzato dall'Associazione Radicale Certi Diritti e Amnesty International**, di tre militanti dei diritti LGBTI - oltre a Sevval Kilic, esponente della Istanbul LGBTT Solidarity Association della Turchia e Altin Hasizaj, del Pink Embassy con sede in Albania, anche Olga Lenkova, Communications manager dell'associazione Lgbt Coming Out, che ha sede proprio a San Pietroburgo - con i cittadini, le associazioni milanesi e i consiglieri comunali che accetteranno l'invito. Sarà un'occasione importante per meglio comprendere lo stato dei diritti delle persone LGBTI ai confini dell'Unione Europea e la situazione di San Pietroburgo in particolare, alla presenza di Elio De Capitani, Ferdinando Bruni e della presidente della Commissione Pari Opportunità del Comune di Milano Anita Sonego.

Sarà anche un'occasione per **firmare le 5 proposte di delibera di iniziativa popolare sostenute da varie associazioni tra cui Arcigay, l'Associazione Luca Coscioni e l'Associazione Radicale Certi Diritti** (milanoradicalmentenuova.it) che, tra l’altro, chiedono che Milano riconosca le coppie di fatto e adotti politiche contro le discriminazioni e per le pari opportunità per tutti tornando ad essere una città laica ed europea, più simile a Berlino, Londra e Parigi che a San Pietroburgo.