---
title: 'MUTUO RICONOSCIMENTO DEI DOCUMENTI DI STATO CIVILE E DEI LORO EFFETTI'
date: Mon, 22 Nov 2010 23:00:00 +0000
draft: false
tags: [Comunicati stampa]
---

**PARLAMENTO EUROPEO CHIEDE MUTUO RICONOSCIMENTO DEI DOCUMENTI DI STATO CIVILE E DEI LORO EFFETTI, FONDAMENTALE PER LE UNIONI LGBT.  
CITTADINI IN EUROPA, FANTASMI IN ITALIA!**

**Comunicato Stampa dell'Associazioen Radicale Certi Diritti**

Bruelles - Roma, 23 novembre 2010

Oggi il Parlamento europeo ha approvato la relazione Berlinguer elaborata dalla commissione giuridica, "sugli aspetti relativi al diritto civile, al diritto commerciale, al diritto di famiglia e al diritto internazionale privato del Piano d'azione per l'attuazione del programma di Stoccolma". La relazione "_sottolinea l'esigenza di garantire il riconoscimento reciproco dei documenti ufficiali delle pubbliche amministrazioni nazionali"_ e _"accoglie con favore lo sforzo della Commissione di mettere in grado i cittadini di esercitare i loro **diritti di libera circolazione e appoggia con vigore i piani volti a permettere il riconoscimento reciproco degli effetti degli atti di stato civile**"._ La Commissione europea ha infatti annunciato il lancio di una consultazione sulla "libera circolazione" dei documenti civili nella UE alla quale seguirà una proposta legislativa nel 2013 al riguardo, che dovrebbe coprire anche il mutuo riconoscimento di tali documenti. Sebbene la risoluzione non menzioni esplicitamente le coppie dello stesso sesso, la pronuncia del PE spinge la Commissione a fare quanto già annunciato da Vivane Reding, Commissaria Ue per i Diritti Fondamentali, in merito al mutuo riconoscimento delle unioni tra le persone dello stesso sesso: "_assicurare che i cittadini europei possano portare con loro il loro status civile, "dallo Stato A allo Stato B"._

Tre emendamenti del gruppo conservatore di destra ECR sono stati rigettati, che riaffermavano il principio della sussidiarietà in materia di diritto di famiglia e di stato civile, in particolare per quanto riguarda il mutuo riconoscimento delle famiglie tra Stati membri, e che volevano cancellare l'approvazione del PE rispetto al mutuo riconoscimento degli effetti dei documenti sullo stato civile, riaffermando la sovranità degli Stati membri in materia di diritto di famiglia.

Certi Diritti si felicita per questa ulteriore promozione dei diritti delle persone, senza discriminazioni basate sull'orientamento sessuale, che arriva dall'Europa: ancora una volta, _cittadini in Europa, fantasmi in Italia!_