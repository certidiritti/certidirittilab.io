---
title: 'Matrimonio: due coppie gay ricorrono alla Cedu contro l''Italia'
date: Fri, 10 Jun 2011 13:01:10 +0000
draft: false
tags: [Matrimonio egualitario]
---

**Due coppie omosessuali ricorrono alla Corte Europea dei Diritti dell'Uomo per vedersi riconosciuto il loro diritto al matrimonio.** **L'annuncio fatto durante il convegno presso la sede del Parlamento Europeo a Roma.**

**Roma, 10 giugno 2011**

**Comunicato Stampa dell’Associazione Radicale Certi Diritti:**

Durante il convegno ‘Costruire e difendere l’Europa dei Diritti’ in corso a Roma, presso la sede del Parlamento Europeo, l’Avvocato Massimo Clara di Milano ha annunciato che stamane sono stati  depositati i ricorsi di due coppie omosessuali di Milano contro lo Stato italiano per discriminazione a causa del loro orientamento sessuale. Il ricorso è stato presentato a causa dell’impossibilità delle coppie di potersi sposare in Italia.

La sentenza 138/2010 della Corte Costituzionale, ha espressamente dichiarato che all’unione tra due persone dello stesso sesso, intesa come stabile convivenza, spetta il diritto fondamentale di vivere liberamente una condizione di coppia e che tale  formazione sociale – nel contesto di un sistema pluralistico- deve ottenere per legge il riconoscimento giuridico con i conseguenti diritti e doveri.

Finora il Parlamento italiano non ha dato ascolto a questo monito della Corte Costituzionale, e per questo le coppie hanno agito davanti alla Corte Europea dei Diritti dell’Uomo con l’assistenza legale della Prof. Marilisa D’Amico, Professore di Diritto Costituzionale all’Università di Milano e degli Avvocati Massimo Clara e Cesare Pitea di Milano.

L’Associazione Radicale Certi Diritti ringrazia la Costituzionalista Marilisa D’Amico e gli Avvocati che stanno assistendo le due coppie di Milano e annuncia che la campagna giudiziaria di Affermazione Civile continua in Italia e in Europa. Altre coppie sono pronte a far partire i loro ricorsi contro l’Italia per la continua negazione, da parte della classe politica, di quanto indicato dalla stessa Corte Costituzionale e per quanto previsto dagli stessi Trattati di Nizza e di Lisbona sulla libera circolazione dei cittadini membri dell’Unione Europea e per quanto previsto in materia di lotta alle discriminazioni anche per orientamento sessuale.