---
title: 'Interrogazione Ue su campagna europea contro prostituzione'
date: Fri, 01 Jul 2011 13:43:34 +0000
draft: false
tags: [Lavoro sessuale]
---

**La campagna europea di stampo integralista-ideologico contro la prostituzione nega dignità e autodeterminazione per le sex workers.  Interrogazione alla Commissione europea su eventuale utilizzo di fondi europei.**

**Comunicato Stampa del Comitato per il Diritti Civili delle Prostitute e dell’Associazione Radicale Certi Diritti**

Roma, 1 luglio 2011

Sulla campagna europea lanciata da una fantomatica lobby europea “European Women” contro la prostituzione, che nega il diritto all’autodeterminazione e alla libera scelta delle persone che scelgono di svolgere questa professione, è stata oggi presentata al Parlamento Europeo una interrogazione urgente, presentata dal Deputato Europeo Gianni Vattimo, che chiede alla Commissione Europea se sono stati utilizzati fondi europei per la sua promozione.  La libertà di scegliere cosa fare del proprio corpo, nel pieno della propria responsabilità e autodeterminazione, è un basilare diritto umano e nessuno si può arrogare il diritto di decidere per gli altri cosa è buono oppure no.

Risulterebbe infatti che il programma ‘Progress’ dell’Unione Europea abbia finanziato questa campagna che di fatto nega  ogni forma di dignità e rispetto delle persone che si prostituiscono in Europa con l’intento di imporre una propria visione che stabilisce, tout court, che la prostituzione è equiparabile alla violenza. Viene così falsificata una realtà diffusissima ovunque in Europa e nel mondo, con specifiche legislazioni nazionali di regolamentazione di questa professione, senza distinguere tra libera scelta di chi la pratica e la violenza causata invece dalla tratta della prostituzione e dal proibizionismo ipocrita, crimonoegno e inutile che i demagoghi di turno ogni tanto cercano di promuovere.  Una campagna che può produrre solo stigma e discriminazioni verso chi lavora nel mercato del sesso per libera scelta o anche per bisogno. Mercato che in parte dell'Europa è legale e dove lavoratrici e lavoratori sono a tutti gli effetti legali, rispettabili e socialmente inclusi.

Vogliamo anche sottolineare che con questa campagna viene offesa anche la dignità di ogni cliente che si rivolge al sesso commerciale, non tenendo in conto che esistono in Europa persone che sono assistite, anche dalle pubbliche istituzioni, per accedere al sesso, avendo ostacoli fisici e psichici che non consentono loro di soddisfare un bisogno primario del quale hanno legittimo e umano diritto.  

Le Sex Workers italiane, la Tampep International Foundation,  il Comitato per i diritti civili delle prostitute e l’Associazione Radicale Certi Diritti, ringraziano il deputato europeo Gianni Vattimo per aver chiesto chiarimenti su questa iniziativa fallimentare, inutile e, forse, addirittura fatta a carico dei contribuenti.

**Qui di seguito il testo integrale dell’interrogaizone parlamentare:**

INTERROGAZIONE URGENTE ALLA COMMISSIONE EUROPEA

Bruxelles, 1 luglio 2011

Può la Commissione europea spiegare se  fondi europei sono stati utilizzati, direttamente o indirettamente per finanziare “la campagna per porre fine alla prostituzione in Europa” e "Insieme per un'Europa senza prostituzione" finalizzate alla promozione di una "Europa libera dalla prostituzione" e invitando " persone, i governi nazionali e l'Unione europea a intraprendere azioni concrete", sulla base del modello legislativo svedese sul tema e con l'obiettivo di abolire la prostituzione, che si presenta come una forma di violenza contro le donne?  Sono stati utilizzati per questo anche fondi ingenti del Progress? Se così fosse,  invitiamo Progress ad illustrarci come i fondi europei possono essere utilizzati per promuovere un certo modello normativo, in particolare su un tema su cui gli Stati membri hanno politiche e sensibilità diverse a riguardo? Nel caso in cui fondi europei siano stati utilizzati direttamente o indirettamente, verrà lanciata una campagna per legalizzare la  prostituzione e il lavoro sessuale, per promuovere modelli legislativi differenti? Saranno idonei gli stessi fondi? Se no, perché? La Commissione chiederà la restituzione dei fondi europei? Le campagne sono finanziate all’insaputa della Commissione?

On. Gianni Vattimo