---
title: 'Gemellaggio Milano - San Pietroburgo: domani il Consiglio comunale vota sulla revoca. Certi Diritti conferma il sit-in'
date: Wed, 17 Oct 2012 06:28:39 +0000
draft: false
tags: [Russia]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti

Milano, 17 ottobre 2012  

Apprendiamo con soddisfazione che, a distanza di sei mesi dalla presentazione, la mozione per la revoca o la sospensione del gemellaggio tra Milano e San Pietroburgo è stata messa in discussione proprio giovedì 18, data in cui abbiamo convocato un sit-in davanti a Palazzo Marino.  
  
**[SIT-IN GIOVEDI' ORE 18 PALAZZO MARINO >>>](http://www.facebook.com/events/487206151300865/)**

Si tratta di un indiscutibile successo della campagna lanciata dall'Associazione Radicale Certi Diritti per la sospensione del gemellaggio tra Milano e San Pietroburgo in occasione del 45° anniversario della firma del patto. In 15 giorni 1.222 persone hanno premuto "mi piace" sulla pagina Facebook della campagna dove stiamo raccogliendo i video di personalità milanesi del mondo della cultura e dello spettacolo che chiedono di sospendere il gemellaggio per dare un segno tangibile a favore dei diritti umani e delle libertà fondamentali dopo l'approvazione, il 29 febbraio scorso, di una legge che sanziona la cosiddetta “propaganda dell’omosessualità” criminalizzando di fatto qualunque attività o informazione relativa alle persone LGBTI (Lesbiche, Gay, Bisessuali, Transessuali e Intersessuali)  in patente violazione delle libertà di espressione e associazione, nonché degli impegni presi dalla Russia ratificando la Convenzione europea per la salvaguardia dei diritti dell’uomo e delle libertà fondamentali.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: "Siamo felici che la voce di Moni Ovadia, Gad Lerner, Lella Costa, Alessandro Cecchi Paone, della Compagnia dell'Elfo, della Consulta per la Laicità delle Istituzioni e degli altri intellettuali che hanno aderito alla nostra campagna, stia iniziando a essere udita anche in Consiglio Comunale. Saremo comunque di fronte a Palazzo Marino dalle 18:30 di giovedì 18 per attendere l'esito della votazione. La campagna continuerà finché il patto di gemellaggio non verrà effettivamente sospeso con una vera e propria delibera del Consiglio Comunale".  
  
**[LA CAMPAGNA SU FACEBOOK >>>](http://www.facebook.com/Stopalgemellaggio)**

**[TUTTI I VIDEO >>>](http://www.youtube.com/playlist?list=UUVynmgs09tImbr1kxXCUR3g&feature=plcp)**  
  
**[SOSTIENI CERTI DIRITTI >>>](partecipa/iscriviti)**