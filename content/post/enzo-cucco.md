---
title: 'Enzo Cucco'
date: Sun, 24 Jan 2016 22:58:31 +0000
draft: false
---

[![enzocucco](http://www.certidiritti.org/wp-content/uploads/2016/01/enzocucco1.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/01/enzocucco1.jpg)Enzo Cucco, nato a Salerno nel 1960 e vive a Torino da sempre. Militante del Fuori dal 1976 ha ricoperto in quella associazione molti incarichi tra cui quella di direttore della rivista. Ha partecipato alla nascita della Fondazione Fuori! nel 1980, di Informagay nel 1987, del Gruppo Solidarietà Aids nel 1989, del Forum Aids Italia nel 1990. E’ stato tra i fondatori dell’associazione Lambda (assistenza alle persone di terza età lgbti) nel 2005 e nello stesso anno del Comitato Torino Pride 2006, essendone il coordinatore fino al 2007. Tra i fondatori del Coordinamento Torino Pride lgbti. Tra i fondatori dell’Associazione radicale certi diritti nel 2008, radicale (a volte iscritto a volte no) da sempre. Attualmente ricopre la carica di presidente dell’Associazione Radicale Certi Diritti e dell’Associazione Lambda.