---
title: 'COMMISSIONE DEL IV CONGRESSO DEDICATA AL TRANSESSUALISMO'
date: Tue, 30 Nov 2010 15:41:20 +0000
draft: false
tags: [Comunicati stampa]
---

IV CONGRESSO DI CERTI DIRITTI HA AFFRONTATO CON UNA COMMISSIONE DI LAVORO AD HOC IL TEMA DEL TRANSESSUALISMO

La Commissione è stata presieduta da Maria Gigliola Toniollo

In merito all’affermazione civile delle persone trans in Italia, i congressisti hanno posto numerose sollecitazioni, in particolare hanno chiesto all’associazione Certi Diritti e ai soggetti costituenti il PRT il pieno riconoscimento delle istanze dell’International Network for Trans’ Despathologization.

Cio’ significa sostenere la cancellazione della transessualità dai manuali dei disordini mentali (DSM-IV-R e ICD-10), il diritto di decidere liberamente se si vuole o no modificare chirurgicamente il proprio corpo, il diritto di proseguire nella decisione con la debita assistenza medica, senza impedimenti burocratici, politici o economici, ne’ alcun tipo di coercizione e il diritto di cambiare nome e indicativo di genere sui documenti ufficiali senza doversi sottoporre a monitoraggio medico o psichiatrico, ne’ a intervento chirurgico di riattribuzione dei genitali.  
E’ parso particolarmente necessario siano tutelate le relazioni familiari, sostenendo la non obbligatorietà del divorzio in caso di percorso di transizione del partner e che sia combattuto il pregiudizio e ogni provvedimento contro il rapporto con i figli componenti il nucleo familiare anche in caso di adozione e di tecniche riproduttive.  
E’ richiesta la rimozione della voce “sesso” dai documenti ufficiali e dalla modulistica.Si chiede inoltre l’abolizione della normalizzazione binaria per i bambini intersessuati e il rispetto dell’integrità sessuale del bambino sino a una sua evoluzione, ponendo fine agli interventi chirurgici sugli infanti intersessuati, vere e proprie mutilazioni genitali infantili.