---
title: 'TRATTA PROSTITUZIONE: IL GOVERNO OPERA TAGLI CHE ALIMENTERANNO LE ORGANIZZAZIONI CRIMINALI. QUESTA SAREBBE LA LOTTA ALLA PROSTITUZIONE? IL GOVERNI TAGLI PIUTTOSTO I SOLDI AI PARTITI'
date: Sun, 25 Jul 2010 11:12:13 +0000
draft: false
tags: [Comunicati stampa]
---

**TRATTA PROSTITUZIONE: IL GOVERNO OPERA TAGLI CHE ALIMENTERANNO LE ORGANIZZAZIONI CRIMINALI. QUESTA SAREBBE LA LOTTA ALLA PROSTITUZIONE? IL GOVERNO TAGLI DAL FINANZIAMENTO PUBBLICO AI PARTITI SE PROPRIO HA BISOGNO DI DENARO**

Dichiarazione di **Sergio Rovasio**, Segretario Associazione Radicale Certi Diritti

“La gravissima decisione del Governo di operare tagli su specifiche iniziative di lotta alla tratta della prostituzione produrrà effetti devastanti riguardo la difesa delle persone sfruttate obbligate a prostituirsi nelle strade italiane. Oltre all’incremento di leggi e ordinanze in chiave proibizionista contro la prostituzione, che finora hanno prodotto in molte città miseri risultati che alimentano ancora di più il mercato nero e l’aumento di violenze senza che nemmeno vengano denunciate, si operano tagli che ledono la dignità delle persone ridotte in schiavitù da bande criminali.

La decisione di chiudere le postazioni locali del Numero Verde Antitratta, causata dai tagli della manovra economica,  segue altri due atti altrettanto gravi: il primo è quello dell’azzeramento dei fondi destinati all'attività di primo contatto, in strada e indoor, per far emergere i fenomeni della tratta e del grave sfruttamento e alla pronta assistenza di tre mesi per le vittime che decidono di uscire dalla loro condizione di assoggettamento (secondo quanto previsto dall’art. 13 della legge 226/2003, “Misure contro la tratta di persone”).

Il Dipartimento per le Pari Opportunità ha assicurato che i soldi – 2,5 milioni di euro – verranno, alla fine, trovati. Ma, al momento, manca una conferma ufficiale. La seconda decisione è la riduzione di 800mila euro dei fondi destinati, invece, ai progetti di inserimento sociale a favore delle vittime finanziati con l’art. 18 del T.U. sull’immigrazione. Se si considera che l’ammontare totale dei fondi stanziati è stato, negli ultimi anni, pari a circa 4,5 milioni di euro, siamo in presenza di un taglio di quasi il 18%.

Occorre che il Governo reperisca, almeno, i 600mila euro necessari per assicurare il funzionamento delle postazioni locali del Numero Verde Antitratta per tutto l’anno 2010 e che venga convocato al più presto il tavolo tecnico sulla tratta composto da istituzioni centrali e locali e dal terzo settore – istituito formalmente, ma mai realmente attivato – per ridefinire insieme l’assetto complessivo del sistema di aiuto alle vittime.

La lotta alla tratta, grazie all’opera di associazioni e volontari in tutta Italia ha assicurato assistenza e integrazione sociale a oltre 14mila persone e prodotto un congruo numero di denunce, arresti e condanne di criminali e sfruttatori.

Questa lotta non può essere liquidata in questo modo, il Governo pensi piuttosto a operare tagli sul furto del finanziamento pubblico ai partiti che ancora oggi garantisce un fiume di denaro  ai partiti”.

**Ufficio Stampa**: 06-68979250