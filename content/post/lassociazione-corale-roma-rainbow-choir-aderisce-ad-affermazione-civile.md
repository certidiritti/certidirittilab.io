---
title: 'L''ASSOCIAZIONE CORALE "ROMA RAINBOW CHOIR" ADERISCE AD "AFFERMAZIONE CIVILE"'
date: Thu, 22 May 2008 21:16:21 +0000
draft: false
tags: [Adesione ufficiale a Affermazione Civile, Comunicati stampa, Roma Rainbow Choir]
---

L'associazione corale "Roma Rainbow Choir" aderisce formalmente all'Iniziativa di Affermazione Civile promossa dall'Associazione Radicale Certi Diritti.  
Spettabile Clara Comelli  
Presidente Ass.ne Radicale Certi Diritti  
  
Con la presente l'associazione corale "Roma Rainbow Choir" aderisce formalmente all'Iniziativa di Affermazione Civile promossa dall'Associazione Radicale Certi Diritti.  
  
Ci impegnamo per quanto a noi possibile e con la nostra caratteristica specifica a poromuovere e sostenere sia l'iniziativa che l'associazione.  
  
Nel ringraziarvi per l'attenzione che ci avete riservato e per il lavoro svolto e che svolgerete porgiamo i nostri  
  
Distinti saluti  
  
Sonia Cirone  
presidente dell'associazione  
  
Giuseppe Pecce  
Maestro