---
title: 'La svolta di Giovanardi: proporre ciò che è già consentito ed equiparare le unioni di carattere affettivo a qualunque convivenza basata su ragioni di convenienza'
date: Wed, 05 Sep 2012 08:29:13 +0000
draft: false
tags: [Politica]
---

Non ci stiamo! Coi diritti umani non si scherza.

Comunicato dell’Associazione Radicale Certi Diritti

Roma, 5 settembre 2012

Dichiarazione di Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti:  
  
“Erano passati troppi giorni senza che l’onorevole Giovanardi facesse una dichiarazione omofoba. Ci si doveva aspettare che qualcosa bollisse in pentola e, ovviamente, la sorpresa è arrivata: il Senatore ha depositato un disegno di legge per introdurre nel Codice Civile il Contratto di convivenza e solidarietà. Uno strumento giuridico tanto innovativo, come si legge nella relazione, da riportarci addirittura ai Di.Do.Re del 2008. Un balzo in avanti da vertigini, non c’è dubbio.  
Il Senatore s’improvvisa anche raffinato costituzionalista: “La Corte costituzionale ha ribadito in una recente sentenza \[quale, di grazia?\] che la famiglia, come scolpita \[!?!\] nell’articolo 29 della Costituzione, è «società naturale fondata sul matrimonio» fra un uomo ed una donna, come la stessa Corte \[ancora?\] ha più volte avuto modo di precisare \[ci riproviamo: quando, di grazia?\]”. Questa menzogna sta diventando stucchevole: l’articolo 29 non parla affatto del genere dei nubendi, come lo stesso Senatore è costretto ad ammettere non virgolettandone il riferimento.

Inoltre, la sentenza 138/2010 dice testualmente che “si deve escludere \[che il riconoscimento delle unioni omosessuali\] possa essere realizzato soltanto attraverso una equiparazione delle unioni omosessuali al matrimonio”. “Soltanto”, caro Giovanardi e cara Bindi \[!\], significa che tale equiparazione possa avvenire anche, ma non solo, attraverso un’equiparazione al matrimonio, se l’italiano non è un’opinione. Il Senatore propone poi una soluzione meramente contrattuale, mentre per la Corte Costituzionale il riconoscimento delle unioni omosessuali “necessariamente postula una disciplina di carattere generale”.

Giovanardi infine si erge a grande pragmatico avulso da intenti ideologici, come se non fosse ideologico ridurre le unioni di carattere affettivo a una qualunque convivenza basata su “ragioni di convenienza \[!\] e solidarietà”. I contratti di convivenza, da stipularsi per atto pubblico o scrittura privata con firma autenticata, sono già possibili nel nostro ordinamento giuridico, caro Giovanardi, ma non risolvono il problema della discriminazione ai danni di tutte quelle cittadine e tutti quei cittadini che non possono accedere all’istituto del matrimonio civile per via del genere del partner che vorrebbero sposare.

Finché un istituto dello Stato sarà riservato solo a una fascia della popolazione, finché il diritto umano fondamentale di vedere rispettata, e quindi riconosciuta, la propria vita familiare sarà negato ad alcuni cittadini italiani per via del loro orientamento sessuale, non verrà sanata alcuna discriminazione, non vi sarà piena uguaglianza, la Costituzione italiana e i diritti umani universali verranno gravemente violati”.