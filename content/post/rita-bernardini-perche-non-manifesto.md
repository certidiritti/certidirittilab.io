---
title: 'Rita Bernardini: perché non manifesto'
date: Sun, 13 Feb 2011 08:57:51 +0000
draft: false
tags: [Politica]
---

  
La deputata radicale: chi se ne frega di B., lottiamo per le donne.

da Lettera43 del 12 febbraio 2011

Il 13 febbraio 2011 le piazze italiane si coloreranno di rosa. Generazioni diverse mosse da un moto di indignazione si incontreranno, cartelli alla mano, per dire no a un modello di donna che, a partire dagli scandali sessuali nei quali è coinvolto il presidente del Consiglio Silvio Berlusconi, ha «disonorato» il Paese.  
«Se non ora quando?», si chiedono le organizzatrici della manifestazione che con questo motto hanno deciso di chiamare tutte le donne a una risposta decisa e simbolica. «Sempre», ha risposto **Rita Bernardini, già segretaria dei Radicali italiani e attuale deputata della delegazione radicale nel Partito democratico. Che però di scendere in piazza domenica non vuole proprio saperne**.  
Lei che nel 1975 seguì Marco Pannella nella lotta per la depenalizzazione dell'aborto e la difesa delle legge sul divorzio. Che nel 1977, davanti al divieto di manifestare imposto dall'allora ministro dell'Interno, Francesco Cossiga, scese in piazza con gli invalidi in carrozzella e costruì scivoli in cemento davanti agli uffici pubblici.  
 

**Domanda. È stanca di lottare per i diritti?**  
**Risposta.** No, semplicemente non mi rispecchio con quel manifesto intriso di richiami alla dignità della donna come se fosse stata violata solo in questo periodo. I problemi della donna non sono quelli si stanno raccontando in questi giorni.  
  
**D. E quali sono?**  
**R.** Le possibilità di lavoro che non ci sono. Siamo agli ultimi posti in Europa per livelli di occupazione femminile e non perché siamo disoccupate ma inoccupate. Le donne in Italia non cercano lavoro non perché vogliono fare le mantenute, ma perché sono costrette a svolgere lavori non retribuiti, quelli domestici. Senza asili e strutture di assistenza per anziani e disabili, loro lavorano, ma gratis. E purtroppo per questi problemi non si scende in piazza.  
  
**D. Non critica quindi un femminismo stradaiolo anacronistico, ma i motivi della protesta.**  
**R.** Ci sono ancora tante battaglie da fare, ma non il sexgate. In piazza io scenderei per il miglioramento della legge sull'aborto, per il divorzio breve, per la fecondazione medicalmente assistita, per la riforma del diritto di famiglia che comprenda anche le coppie di fatto. E invece queste cose spariscono davanti alla morale.  
  
**D. Donne che snobbano le donne?**  
**R.** Ricordo quando con Emma Bonino facemmo una battaglia per l'equiparazione dell'età pensionabile delle donne in modo da destinare al welfare femminile quello che si risparmiava. Molte la interpretarono come un'offesa, ma noi volevamo solo rendere le leggi italiane uguali a quelle europee. Il nostro slogan era 'Proteggimi di meno, includimi di più'. Emma per questo fu sbeffeggiata.  
  
**D. Non l'avevano capita, come forse ora lei non sta capendo loro.**  
**R.** In piazza domenica 13 febbraio ci saranno sicuramente anche persone in buona fede che ci credono, che manifestano perché vogliono più diritti, ma mi preoccupa chi vuole strumentalizzare questa protesta.  
  
**D. Si riferisce alla politica?**  
**R.** All'abilità politica di chi ha organizzato tutto questo. Si presentano con il volto di chi vuole purificare tutto al grido 'più dignità per le donne', ma è un'ipocrisia. Fingono che sia per noi ma vogliono solo far cadere il governo.  
  
**D. L'opposizione scende in piazza per se stessa?**  
**R.** Mi chiedo perché non si sia scesi in piazza per l'adeguamento del diritto di famiglia che è fermo al 1975. Perché la proposta di legge fatta dai radicali (sei all'interno del gruppo del Pd, ndr) non è ancora stata calendarizzata dal Pd che ha il compito di fare le proposte?  
  
**D. Perché è troppo impegnato a organizzare questa protesta?**  
**R.** Il Pd si sta spendendo molto in questa battaglia e non è la prima volta. Già nel luglio 2009 ai tempi del caso Noemi (la ragazza di Casoria, prima 'papi girl', ndr) ci fu un appello delle deputate del Pd che chiesero a Berlusconi di venire in aula per chiedere scusa alle donne. Non ho aderito allora e non lo farò ora. Non è certo questo ciò che dovremmo rimproverare al governo.  
  
**D. E che cos'è?**  
**R.** Che non c'è alcuna corrispondenza tra le leggi che propone e i suoi comportamenti fuori dal Parlamento. E a dargli una mano c'è il Pd che certo non si azzarda a dire una parola su temi come le coppie di fatto e il testamento biologico. In Italia quando si parla di sesso perdono tutti la testa.  
  
**D. In che senso?**  
**R.** C'è un atteggiamento sessuofobico. Io legalizzerei la prostituzione per smascherare questo moralismo. Sembra che ci siano donne angeli e pudiche, che si mettono a giudicare altre donne che fanno il mestiere più antico del mondo. Io dico regolamentare, che è esattamente il contrario di liberalizzare.  
  
**D. Ma oggi ci si indigna perché le escort arrivano in Parlamento.**  
**R.** Saranno problemi loro come fanno carriera. Io sono per una società liberale, dove c'è un senso di responsabilità individuale e ognuno fa quello che vuole, basta che non danneggi l'altro.  
  
**D. Berlusconi non ha danneggiato l'immagine della donna italiana?**  
**R.** Che male c'è se ci sono persone che fanno sesso a pagamento? Se poi ci sono reati che la magistratura agisca, ma senza doppi fini.  
  
**D. Si spieghi meglio.**  
**R.** Ho un fondato sospetto che la magistratura entri a piè pari nelle vicende politiche italiane per determinarne l'esito e usi due diversi principi di legalità.  
  
**D. Quali?**  
**R.** La magistratura milanese ha chiuso gli occhi davanti a episodi gravissimi come le firme false presentate dalla lista Formigoni alle elezioni regionali. E quando Edmondo Bruti Liberati, che ha reso questo servizio al presidente della Regione Lombardia, viene promosso procuratore capo, avalla una mega inchiesta sul sexgate.  
  
**D. Che cosa si aspetta da questa manifestazione?**  
**R.** Vediamo se alla fine l'opposizione che lotta per i diritti delle donne continuerà davvero questa battaglia e quante ne farà entrare in politica. Le donne non vogliono né la carità né le quote rosa, ma hanno solo bisogno di essere messe nella condizione di lavorare.

Sabato, 12 Febbraio 2011  
[www.lettera43.it](http://www.lettera43.it)