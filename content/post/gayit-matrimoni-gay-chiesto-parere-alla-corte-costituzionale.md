---
title: 'GAY.IT - MATRIMONI GAY: CHIESTO PARERE ALLA CORTE COSTITUZIONALE'
date: Tue, 21 Apr 2009 08:45:47 +0000
draft: false
tags: [Senza categoria]
---

**Per la prima volta i giudici dell'Alta Corte saranno chiamati a pronunciarsi sull'estensione del matrimonio alle coppie gay. Due uomini avevano visto rifiutarsi la pubblicazione degli atti in Comune.  
**

**Come in California, anche la Corte Costituzionale italiana** è stata chiamata a pronunciarsi sui matrimoni gay. In particolare, il Tribunale di Venezia ha rinviato ai giudici dell'"Alta Corte" il ricorso di una [coppia](http://www.gay.it/keyword/610/Coppia.html) di uomini che aveva chiesto al  Comune di appartenenza la pubblicazione degli atti di matrimonio, proposta chiaramente rifiutata dal funzionario di stato civile.  
  
[![Gay.it - Matrimoni gay: chiesto parere alla Corte Costituzionale](http://images.gay.it/foto_articoli/135x135/co/cortecostmatirmonio1.jpg "Gay.it - Matrimoni gay: chiesto parere alla Corte Costituzionale")](http://www.gay.it/channel/attualita/26416/Matrimoni-gay-chiesto-parere-alla-Corte-Costituzionale.html#)**I due aspiranti sposi erano comunque** consapevoli di ricevere il "no" del Comune e già avevano intenzione di ricorrere al tribunale. L'operazione, infatti, rientra in un'iniziativa chiamata di "Affermazione civile" promossa sin dal maggio 2008 dall'associazione radicale Certi Diritti, con l'assistenza legale gratutita degli avvocati della "Rete Lenford", che ha come obiettivo quello di incardinare il maggior numero di iniziative legali volte al superamento delle [discriminazioni](http://www.gay.it/keyword/82/Discriminazioni.html) che per legge impediscono il matrimonio alle coppie dello stesso [sesso](http://www.gay.it/keyword/241/Sesso.html). La campagna prevede come primo passo quello di richiedere le pubblicazioni per il matrimonio civile nel Comune di residenza della [coppia](http://www.gay.it/keyword/610/Coppia.html) e quando viene opposto il diniego presentare ricorso presso il Tribunale competente.  
  
**Quello che è giunto inaspettato**, piuttosto, è stato proprio il rinvio del Tribunale di Venezia alla Corte Costituzionale del procedimento per verificare se il rifiuto del funzionario comunale alla pubblicazione degli atti matrimoniali abbia un fondamento nella carta costituzionale o meno.  
  
**Negli U.S.A. la legalizzazione del matrimonio** gay è passata in più stati grazie, non tanto al voto favorevole del parlamento federale, ma proprio per una decisione della Corte Costituzionale, la stessa che nello stato del governatore Schwarzenegger si pronuncerà a giorni sulla validità o meno della Prop8 che i matrimoni li aveva invece aboliti.  
  
**L'Italia potrebbe avere presto una decisione favorevole** ai due ricorrenti che sarebbe di portata storica o, al contrario, l'ennesima voce, dopo quella della politica, sfavorevole all'estensione dell'istituto matrimoniale alle coppie gay.  
  
[![Gay.it - Matrimoni gay: chiesto parere alla Corte Costituzionale](http://images.gay.it/foto_articoli/135x135/co/cortecostmatirmonioF2.jpg "Gay.it - Matrimoni gay: chiesto parere alla Corte Costituzionale")](http://www.gay.it/channel/attualita/26416/Matrimoni-gay-chiesto-parere-alla-Corte-Costituzionale.html#)**Sergio Rovasio, Segretario dell'Associazione Radicale** Certi Diritti da noi sentito ha dichiarato: "Per noi e per la comunità lgbt è un momento storico. Il fatto che un Tribunale abbia rinviato alla Consulta la decisione già di per sè è un grande passo avanti. Ci auguriamo che la Corte riconosca la grave discriminazione di cui sono vittime le coppie gay che in Italia  desiderano sposarsi. Dobbiamo rafforzare la campagna di Affermazione civile, speriamo che tante altre coppie gay partecipino a questa iniziativa promossa insieme agli avvocati di Rete Lenford, le invitiamo da subito a contattarci su [www.certidiritti.it](http://www.gay.it/channels/click.php?url=http://www.certidiritti.it)".

http://www.gay.it/channel/attualita/26416/Matrimoni-gay-chiesto-parere-alla-Corte-Costituzionale.html