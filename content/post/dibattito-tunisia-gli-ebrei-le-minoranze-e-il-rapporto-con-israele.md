---
title: 'Dibattito: “Tunisia: gli ebrei, le minoranze e il rapporto con Israele”.'
date: Fri, 23 Nov 2018 11:30:50 +0000
draft: false
tags: [Africa, Medio oriente, Movimento LGBTI, Transnazionale]
---

**[![45572496_10156476652015973_4779659917336248320_n](http://www.certidiritti.org/wp-content/uploads/2018/11/45572496_10156476652015973_4779659917336248320_n-300x200.jpg)](http://www.certidiritti.org/2018/11/23/dibattito-tunisia-gli-ebrei-le-minoranze-e-il-rapporto-con-israele/45572496_10156476652015973_4779659917336248320_n/)Dibattito: “Tunisia: gli ebrei, le minoranze e il rapporto con Israele”.**

**Comunicato Stampa dell’Associazione Radicale Certi Diritti, Associazione Milanese Pro Israele e Amici di Israele**

Milano, 23 novembre 2018

**Sabato 24 novembre, alle 20.45, presso la** [**Sinagoga Beth Shlomo**](http://www.bethshlomo.it/3skl/vortal/bth/index.jsp?id_lingua=0&id_struttura=113&opzione=home&subopzione=home&visPrimo=0&titoloPagina=Home%20page), in Corso Lodi 8/c, Milano (MM 3 Linea gialla, fermata Romana) si terrà il dibattito “**Tunisia: gli ebrei, le minoranze e il rapporto con Israele**”.

Ospite d’eccezione è **Mounir Baatour**, presidente del Partito Liberale Tunisino, difensore dei diritti LGBTI e del dialogo con Israele che si confronterà con **Davide Romano** (portavoce della [**sinagoga Beth Shlomo**](http://www.bethshlomo.it/3skl/vortal/bth/index.jsp?id_lingua=0&id_struttura=113&opzione=home&subopzione=home&visPrimo=0&titoloPagina=Home%20page)), **Yuri Guaiana** (presidente dell’[Associazione Radicale Certi Diritti](http://www.certidiritti.org/)) e **Alessandro Litta Modignani** (presidente [AMPI](https://www.facebook.com/AssMilaneseProIsraele/))