---
title: 'Avvenire contro diritti gay ma un giorno Chiesa chiederà scusa'
date: Wed, 25 May 2011 12:39:15 +0000
draft: false
tags: [Politica]
---

Roma, 22 maggio 2011  
   
Dichiarazione di Sergio Rovasio, Segretario Associaizone Radicale Certi Diritti

  
   
Quelli  di Avvenire, quotidiano della Conferenze Episcopale Italiana, entità religiosa che un giorno sì e l’altro pure dice a tutti noi cosa è bene e cosa è male, senza mai limitare giudizi, sentenze e condanne ai loro seguaci, oggi ci spiegano che non è giusto occuparsi di lotta all’omofobia e che non bisogna riconoscere diritti alle persone lesbiche e gay.

E’ bene ricordare a questi signori, che mai hanno chiesto scusa per le gravi responsabilità di cui si sono macchiati nel corso dei secoli per le persecuzioni contro le persone omosessuali -pur avendolo fatto in questi ultimi decenni per tutte le altre persecuzioni di cui sono stati promotori- che l’odio e il pregiudizio di oggi è certamente frutto della loro visione fondamentalista per la quale dovranno un giorno chiedere scusa e perdono.

Siamo certi che lo faranno, non sappiamo se tra un anno o un secolo. Certo, fino a quando i loro privilegi, di casta clericale, che vive soprattutto di ipocrisia e denari, continuerà ad alimentare lo strapotere dei loro gerarchi, sarà difficile far prevalere il messaggio di attenzione e rispetto verso il prossimo. Ma noi attenidamo fiduciosi.