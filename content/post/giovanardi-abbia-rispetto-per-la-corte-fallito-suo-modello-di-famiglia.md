---
title: 'GIOVANARDI ABBIA RISPETTO PER LA CORTE: FALLITO SUO MODELLO DI FAMIGLIA'
date: Wed, 24 Mar 2010 07:38:07 +0000
draft: false
tags: [Comunicati stampa]
---

**ROVASIO: GIOVANARDI PENSI AL FALLIMENTO DEL SUO MODELLO DI FAMIGLIA PIUTTOSTO CHE TENTARE DI INFLUENZARE LA CORTE COSTITUZIONALE**

23 marzo 2010

_**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti e candidato tra i capilista della Lista Bonino-Pannella alla Regione Lazio**_

L'ineffabile molto Sottosegretario Giovanardi farebbe bene a studiare le ragioni per le quali da quando ha l'incarico con delega alle politiche per la famiglia di questo Governo si sono verificati aumenti dei divorzi, calo dei matrimoni e natalità sempre più vicina al numero ‘zero'. Ora si è messo a dare indicazioni ai Giudici della Corte Costituzionale che tra poche ore dovranno decidere sui ricorsi di alcune coppie gay che chiedono di accedere all'istituto del matrimonio. Se ci fosse un maggiore rispetto per le istituzioni anche il Sottosegretario Giovanardi attenderebbe come noi che i Giudici della Corte decidano nel pieno rispetto del loro mandato. Tentare di dettare con un comunicato stampa la sentenza, come ha fatto oggi pomeriggio l'ineffabile Sottosegretario, dimostra totale mancanza di rispetto per la Corte costituzionale.