---
title: 'Forze dell''ordine, con supporto guardie private armate, si accaniscono contro chi pratica naturismo. Governo chiarisca'
date: Tue, 03 Jul 2012 11:12:07 +0000
draft: false
tags: [Politica]
---

Il Parlamento discuta il ddl dei radicali. Al Senato interrogazione urgente su apportunità di tali azioni che danneggiano innanzitutto il turismo.

Roma, 3 luglio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Quanto sta accadendo in questi giorni in alcune spiagge dove si pratica il naturismo ha dell’incredibile. In almeno tre spiagge italiane, dove da decenni si pratica il naturismo, le forze dell’ordine svolgono azioni che nulla hanno a che vedere con la prevenzione e la repressione del crimine.

Tra i casi segnalare vi è quello della Spiaggia dell’Arenauta di Gaeta, frequentata anche dalla comunità Lgbte, dove da alcuni giorni agenti della Polizia Municipale e Guardia Costiera, accompagnati da Vigilantes armati di agenzie private, stazionano nel tratto di spiaggia frequentata da naturisti, imponendo ai bagnanti di mettersi il costume. Alcuni anni fa il Governo rispose ad una interrogazione parlamentare dei deputati della Rosa nel Pugno, prendendo una posizione netta e chiara e dichiarando che era stata sollecitata la Prefettura di Latina affinchè verificasse se la concessione delle licenze  date alle società di vigilanza private armate  prevedevano di andare per le spiagge a intimidire i bagnanti. Durante una Conferenza Stampa svoltasi venerdì scorso a Gaeta dal Comitato per la difesa e la tutela della spiaggia dell’Arenauta è stato evidenziata anche la disponibilità dell’Assessore all’Ambiente del Comune di Gaeta a individuare in quell’area la destinazione della pratica del naturismo.

Altre vicende simili segnalate riguardano le spiagge di Lido Pizzo e Punta della Suina di Gallipoli (Lecce) e quella di lido di Dante (Ravenna) dove  forze dell’ordine in borghese, da alcuni giorni, presidiano le aree dove si pratica il naturismo.

I Parlamentari Radicali Donatella Poretti e Marco Perduca, insieme ad altri Senatori del Pd, hanno depositato ad inizio Legislatura un Disegno di Legge che chiede la regolamentazione della pratica del naturismo in Italia.

Non essendoci una legislazione nazionale sul tema del naturismo nel corso degli anni la giurisprudenza ha più volte ribadito che la pratica del naturismo non è perseguibile perché l' esposizione del corpo nudo su una spiaggia, quando viene effettuata senza esibizioni, senza platealità e senza scopi provocatori ma con naturalezza e riservatezza, non può vedere applicato l'art. 726 del c. p. (atti contrari alla pubblica decenza) perché il naturista "sta" nudo e trovandosi in uno "stato" non compie alcun "atto" ed è agli "atti" cui tale articolo di legge si riferisce.  
  
La Corte di Cassazione ha ripetutamente stabilito, obiter dictum, che 'il nudo integrale riguarda l’espressione della libertà individuale o derivare da convinzioni salutiste o da un costume particolarmente disinibito. Esso, se praticato in una spiaggia appartata, frequentata da soli naturisti, è penalmente irrilevante'; e, ancora obiter, che 'non può considerarsi indecente la nudità integrale (…) di un naturista in una spiaggia riservata ai nudisti o da essi solitamente frequentata': tale comportamento non costituisce quindi 'atto contrario alla pubblica decenza' ai sensi dell’articolo 726 del codice penale (così Cass. pen., III sez., n.8959/1997, 1765/2000 e la n.3557/2000).  
  
Su quanto sta accadendo nelle spiagge naturiste italiane, i Senatori Radicali eletti nel Pd, Donatella Poretti e Marco Perduca hanno oggi depositato una interrogazione urgente al Ministro degli Interni, della Difesa e del Turismo per conoscere i costi degli interventi delle forze dell’ordine e delle società di vigilanza privata e se non ritengano che tali azioni non siano un grave disincentivo al turismo nazionale  e internazionale.

**[I senatori Perduca e Poretti nudi contro la sessuofobia nella campagna di Certi Diritti 'Dai corpo ai tuoi diritti' >>>](http://www.certidiritti.org/2012/06/26/dai-corpo-ai-tuoi-diritti-parlamentari-artisti-ex-porno-divi-disabili-studenti-rifugiati-e-attivisti-politici-nudi-contro-la-sessuofobia/)**