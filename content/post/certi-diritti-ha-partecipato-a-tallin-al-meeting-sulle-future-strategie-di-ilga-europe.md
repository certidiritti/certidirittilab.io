---
title: 'CERTI DIRITTI HA PARTECIPATO A TALLIN AL MEETING SULLE FUTURE STRATEGIE DI ILGA EUROPE'
date: Wed, 16 Jun 2010 05:35:25 +0000
draft: false
tags: [Comunicati stampa]
---

Sabato 12 e domenica 13 giugno L'Associazione Radicale Certi Diritti ha partecipato al meeting di "Pianificazione Strategia Triennale 2011-2013" organizzata da ILGA Europe ([www.ilga-europe.org](http://www.ilga-europe.org/)) \- di cui Certi Diritti è membro - che si è tenuta a Tallinn, Estonia.

Alcuni membri di ILGA Europe, insieme al direttivo e allo staff, hanno condiviso i punti di riferimento per le iniziative che ILGA Europe vorrà intraprendere i prossimi anni, dando risonanza presso le istituzioni della Unione Europea a specifici temi dell'attivismo LGBTI.

Certi Diritti ha voluto essere presente, per supportare la promozione, anche a livello europeo, del diritto al matrimonio come Principio di Uguaglianza, insieme al riconoscimento di tutte le forme di famiglie, a prescindere dal sesso e dall'orientamento sessuale di chi le costituisce.

Inoltre, grazie alla collaborazione di Certi Diritti con il Partito Radicale Nonviolento Transnazionale Transpartito, abbiamo potuto offrire ad ILGA Europe la possibilità di difendere i diritti umani delle persone omosessuali, transessuali e intersessuali, intervenendo direttamente presso il Consiglio dei Diritti Umani di Ginevra.

Non per ultimo, questa è stata un'ottima occasione per rinforzare la collaborazione e la conoscenza reciproca con le altre realtà europee e italiane, quali Arcigay e Comitato Torino Pride.

In attesa che riprendano iniziative concrete nell'ambito di Affermazione Civile, il nostro impegno dunque continua a 360 gradi, anche rafforzando e diffondendo all'estero la cultura del diritto al matrimonio come principio di uguaglianza.