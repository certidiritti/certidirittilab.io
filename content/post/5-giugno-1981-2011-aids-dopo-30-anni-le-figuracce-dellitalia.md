---
title: '5 giugno 1981-2011: Aids, dopo 30 anni le figuracce dell''Italia'
date: Sun, 05 Jun 2011 06:32:18 +0000
draft: false
tags: [Salute sessuale]
---

**Trent'anni di Aids: il 5 giugno 1981 veniva pubblicato il primo studio.  
**

**L'Italia nel 2011 tra mancati pagamenti, studi fermi al 2008 e l'invio di Giovanardi alla conferenza Onu, fa la sua pessima figura e quasi niente contro l'Aids.**

**Interrogazione urgente dei deputati radicali-Pd al governo italiano.  
**

**Roma, 5 giugno 2011**

Il 5 giugno 1981 negli Stati Uniti viene pubblicato il primo studio relativo ad una patologia infettiva che provoca un deficit del sistema immunitario. E’ l’atto di nascita dell’Aids. Il Governo Usa nel periodo compreso tra l’ottobre 1980 e il maggio 1981 annunciò i primi cinque casi della Sindrome da Immuno-deficienza acquisita.

A distanza di  trent’anni l’unica iniziativa promossa  dal Governo italiano, in occasione dell’ultima giornata contro l’Aids, è stata quella di invitare le persone tra i 30 e 40 anni a fare il test Hiv; in Italia non esistono campagne di informazione e prevenzione sulla diffusione dell’Hiv, nemmeno rivolte ai migranti con apposite iniziative, così come suggerito dalla Commissione nazionale di lotta contro l'Aids. Non si può nemmeno nominare pubblicamente la parola preservativo e alcune farmacie non li vendono in nome del diritto all'obiezione di coscienza. 

L’ultimo studio sulla diffusione dell’Aids pubblicato nel sito del Governo italiano risale al 2008. Alla Conferenza Onu dell' 8-10 giugno prossimi, su Hiv/Aids, il Governo italiano non sarà presente con Ministri, a differenza di molti altri paesi democratici, e ridurrà la sua partecipazione mandando il Sottosegretario Giovanardi che ha già annunciato che chiederà la cancellazione delle politiche sulla ‘riduzione del danno’. 

Tra i 40 paesi donatori per la lotta internazionale contro l’Aids, l’Italia è l’unico paese a non aver ancora versato la sua parte-quota del 2009 e anche per questo è stata esclusa dal Consiglio di Amministrazione del Fondo globale per la lotta contro l’Aids, malaria e tubercolosi. E questo nonostante sia stata la promotrice del Fondo durante i lavori del G8 di Genova.

Fra due mesi l’Italia ospiterà la IAS, uno dei più importanti eventi medico-scientifici dedicati alla lotta all'AIDS, con queste premesse.

**I deputati Radicali eletti nel Pd, prima firmataria Rita Bernardini, hanno depositato una interrogazione urgente al Governo. Qui di seguito il testo integrale:**

Interrogazione urgente a risposta scritta

Al Presidente del Consiglio dei Ministri

Al Ministro della Salute

Per sapere - Premesso che:

Tra l’ottobre 1980 e il maggio 1981 il Governo degli Stati Uniti annunciò quelli che poi vennero scoperti essere i primi cinque casi della Sindrome da immuno-deficienza acquisita.  

Il 5 giugno 1981 negli Stati Uniti viene pubblicato il primo studio relativo a una patologia infettiva che provoca un deficit del sistema immunitario. E' l'atto di nascita dell'Aids;

Sono passati trent’anni e la pandemia ha certamente insegnato molte cose al mondo contemporaneo. Oggi una sola pillola può tenere l’infezione sotto controllo pur non potendola curare e nuove ricerche mostrano che gli anti-retrovirali non sono solo terapeutici, ma possono anche ridurre il rischio d’infezione; la pandemia ha mostrato di quanto sia necessario fare massicce e mirate campagne d’informazione sulle malattie a trasmissione sessuale;

Il Governo italiano, in occasione della giornata mondiale della lotta all’AIDS, si è limitatoa  promuovere una campagna  che ha come obiettivo quello di “incentivare i giovani adulti (30-40 anni), di qualunque orientamento sessuale, italiani e stranieri, ad effettuare il test HIV” ([http://www.governo.it/GovernoInforma/campagne_comunicazione/aids_2010/index.html](http://www.governo.it/GovernoInforma/campagne_comunicazione/aids_2010/index.html)).

Secondo l’esperienza acquisita in tutto il mondo, la lotta all’AIDS non si fa solo nell’ambulatorio delle analisi, ma si concretizza nel momento in cui le persone scelgono di proteggersi durante i rapporti sessuali; è certamente quello il momento della scelta, della decisione da prendere, è lì che si decide se si vuole combattere la battaglia o lasciarla sopraffare;

Secondo le più importanti organizzazioni che si occupano di lotta all’Aids occorrono campagne, innanzitutto non sporadiche ma sistematiche, anche rivolte alle diverse comunità: migranti (quindi in diverse lingue), popolazione carceraria, eterosessuali, omosessuali, bisessuali, transessuali, coppie siero divergenti (come si fa in Svizzera). La necessità di un messaggio più mirato è d’altronde riconosciuto anche dalla Commissione Nazionale per la Lotta contro l'AIDS che sin dal giugno 1987 ha istituito presso il Dipartimento di Malattie Infettive, Parassitarie e Immunomediate dell’Istituto Superiore della Sanità il Servizio Nazionale Telefono Verde AIDS (TVA) che prevede il contributo di mediatori linguistico culturali per rivolgersi ai cittadini stranieri ([http://www.governo.it/GovernoInforma/Dossier/aids/telefono_verde.html](http://www.governo.it/GovernoInforma/Dossier/aids/telefono_verde.html)).

Studi e ricerche dovrebbero essere specifiche per le diverse comunità. Mentre negli Stati Uniti e nel Regno Unito ci sono per esempio dati aggiornati sul comportamento sessuale della comunità LGBT e la diffusione della malattia nella stessa, in Italia cifre e analisi sulla salute della popolazione omosessuale sono limitate ad alcune realtà locali;

Al Meeting Onu su Hiv/Aids che si svolgerà dall’8 al 10 giugno 2011° New York, il Governo italiano ha affidato la lotta all'Aids al sottosegretario Giovanardi e al suo Dipartimento Antidroga; finora risulta soltanto che le proposte italiane saranno quelle della richiesta della cancellazione delle politiche ‘sulla riduzione del danno’ che sono proprio quelle ritenute più efficaci;

l’ultimo dossier sull’AIDS rintracciabile sul sito del Governo risale al 2008;

L’Italia è stata esclusa dal consiglio di amministrazione del Fondo globale per la lotta contro Aids, malaria e tubercolosi perché indietro con i pagamenti di ben due anni e questo nonostante sia stata la promotrice del Fondo durante i lavori del G8 di Genova;

L’ Italia  ospiterà tra appena due mesi la IAS, uno dei più importanti eventi medico-scientifici dedicati alla lotta all'AIDS.

Degli oltre 40 paesi donatori (a cui vanno aggiunte associazioni come quelle che fanno capo a Bill Gates e a Bono Vox) l’Italia è l’unico a non aver ancora versato la quota del 2009”;

Per sapere:

\- Quali altri iniziative ha messo in campo il Governo riguardo la lotta all’Aids;

\- per quale motivo gli ultimi dati aggiornati nel sito del Governo sulla lotta all’Aids risalgono al 2008;

\- se il Governo non ritenga troppo riduttivo inviare alla Conferenza Onu su Hiv/Aids dell’8 – 10 giugno il Sottosegretario Giovanardi che solitamente esprime posizioni politiche di evidente pregiudizio nei confronti delle persone omosessuali; e se le proposte da lui annunciate sul proporre la cancellazione delle politiche ‘sulla riduzione del danno’ siano rappresentative delle posizioni del Governo e su quali basi scientifiche e sociali si basa tale decisione;

-  se non ritenga il Governo un grave errore lasciare alle sole Associazioni l’iniziativa della lotta all’Aids che la considerano una priorità nazionale e che hanno promosso il Forum della società civile italiana sull'Hiv/Aids ([http://www.forumhivaids.it/](http://www.forumhivaids.it/)).

\- quale sarà l’atteggiamento del Governo italiano alla prossima riunione IAS;

\- se non ritiene il Governo che tale comportamento metta in una cattiva luce il nostro paese sia rispetto agli impegni presi, sia rispetto alla necessità del nostro paese di contribuire in modo determinante alla lotta all’Aids in tutto il mondo;

Rita Bernardini

Marco Beltrandi

Maria Antonietta Farina Coscioni

Matteo Mecacci

Maurizio Turco

Elisabetta Zamparutti