---
title: 'Certi Diritti lancia la campagna ‘Bologna chiAMA i diritti umani’: stop al gemellaggio con la città di Kharkov'
date: Mon, 18 Feb 2013 11:48:38 +0000
draft: false
tags: [Europa]
---

Chiediamo al comune di Bologna di sospendere i due protocolli di intesa con la città ucraina di Kharkov e con la città di Nizhny Novgorod (Federazione Russa).

Comunicato stampa dell’associazione radicale Certi Diritti.  
  
Bologna, 18 febbraio 2013  

Lo scorso 25 gennaio in Russia è stata approvata alla Duma, in prima lettura, una norma che sancisce il reato di “propaganda omosessuale”.  
Una legge che renderà illegali i gay-pride e per la quale il semplice scambio di baci o lo sventolamento di bandiere arcobaleno costituiranno gravi illeciti punibili anche con la galera.  
Provvedimenti analoghi a quelli adottati alla Duma, in realtà, erano già in vigore nella primavera 2012 sia nel Distretto Federale Nordoccidentale – dove 17 attivisti del movimento in difesa dei diritti degli omosessuali a San Pietroburgo il primo maggio 2012 vennero arrestati – sia nel Distretto Federale del Volga – dove la città di Nizhny Novgorod rese nota la propria approvazione nell'aprile 2012.  
  
Nel mese di ottobre 2012, anche il Parlamento Ucraino inaugura la medesima prassi approvando in prima lettura, con 289 voti favorevoli su 450,  il disegno di legge 8711, successivamente registrato come legge 0945.  Volto a punire qualsiasi tipo di azione atta a “promuovere l'omosessualità”, tale disegno di legge, in virtù della tutela della pubblica morale, apporterà considerevoli alterazioni alle norme che regolano l’informazione, modificando addirittura il codice penale e dispensando fino a 5 anni di galera ai contravventori.  
Ci auguriamo che le suddette norme non superino le successive letture in Parlamento divenendo così definitive.  
  
Poiché, sia quella approvata nella città Nizhny Novgorod, sia quella presentata alla Duma, che quella avanzata al Parlamento Ucraino, con l'annichilente oscurantismo censorio che le contraddistingue, rappresentano, e di fatto sono, fra le massime violazioni alle libertà di parola, di espressione, di riunione e di associazione ai sensi della Convenzione europea dei diritti dell'uomo e del Patto internazionale sui diritti civili e politici. Ritraendo l'omosessualità come riprovevole, oltraggiosa e criminale altresì incentivano sentimenti di odio contro le comunità LGBT, legittimano la violenza omofoba e sopprimono ogni possibilità per gli attivisti di qualsivoglia tentativo, anche il più innocuo, di rivendicare i propri diritti civili.

Il comune di Bologna ha sottoscritto in data 5 agosto 1966 - poi rinnovandolo il 16 novembre 2007- un gemellaggio con la città di Kharkov (Ucraina). E allo stesso modo, in data 8 ottobre 1998, ha siglato un protocollo di cooperazione con la città di Nizhny Novgorod (Federazione Russa), rinnovandolo successivamente il 7 febbraio del 2000 e il 27 gennaio del 2001.

**Chiediamo al comune di Bologna e a tutta l'amministrazione che questi due protocolli di intesa vengano sospesi immediatamente, per trasmettere un segnale di vicinanza a tutte le comunità GLBTI e di monito al parlamento Russo ( affinché nella città Nizhny Novgorod venga abrogata la legge già approvata e in vigore) e a quello Ucraino, in difesa dei diritti umani e della libertà di espressione.**