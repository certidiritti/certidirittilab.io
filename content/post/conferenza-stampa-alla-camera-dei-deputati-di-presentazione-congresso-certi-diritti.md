---
title: 'CONFERENZA STAMPA ALLA CAMERA DEI DEPUTATI DI PRESENTAZIONE CONGRESSO CERTI DIRITTI'
date: Thu, 25 Nov 2010 20:34:03 +0000
draft: false
tags: [Comunicati stampa]
---

**Venerdì 26 novembre, alle ore 12, presso la Sala Stampa della Camera dei deputati (via della Missione, 4 – Roma) Conferenza Stampa sui Diritti Civili e Umani in Uganda, Russia e Italia.** 

**Presentazione del IV Congresso dell’Associazione Radicale Certi Diritti**

**Con:**

**\- Marco Pannella**, Partito Radicale Nonviolento Transnazionale e Transpartito;

**\- Kato David Kisule**, rappresentante gay per i diritti civili in Uganda;

**\- Nikolaj Alexiev**, organizzatore del Pride di Mosca;

**\- Matteo Mecacci**, deputato radicale – Pd, Rapporteur Assemblea Parlamentare dell'OSCE per Democrazia, Diritti Umani e Questioni Umanitarie

**\- Didier Eribon**, docente universitario – Estensore Manifesto Uguaglianza per i diritti in Francia

**\- Sergio Rovasio**, Segretario dell’Associazione Radicale Certi Diritti;

**Kato David Kisule** è una delle quattro persone che ha denunciato alle autorità ugandesi la rivista locale ‘Roling Stones’ che aveva pubblicato le foto di 100 persone omosessuali (tra cui la sua) con la scritta ‘Wanted’.

**Didier Eribon** è l'estensore del "Manifesto per l'uguaglianza dei diritti" (Francia, 2004)

**Nikolaj Alexiev** è stato aggredito, denunciato e arrestato nel corso degli ultimi 4 anni per aver tentato di organizzare l’annuale Gay Pride di Mosca.

**Durante la Conferenza Stampa verrà illustrato il programma dei lavori del IV Congresso dell’Associazione Radicale Certi Diritti che si svolgerà a Roma nei giorni sabato 27 e domenica 28 novembre 2010.**