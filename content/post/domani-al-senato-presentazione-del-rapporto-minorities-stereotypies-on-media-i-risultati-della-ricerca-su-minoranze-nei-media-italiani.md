---
title: 'Domani al Senato presentazione del rapporto Minorities Stereotypies on Media: i risultati della ricerca su minoranze nei media italiani'
date: Wed, 22 Feb 2012 22:19:55 +0000
draft: false
tags: [Politica]
---

Giovedì 23 febbraio 2012 ore 9.30 - Sala Conferenze ex hotel Bologna, Via di S. Chiara, 5 - Roma. Partecipano Emma Bonino e Sergio Rovasio.

Minorities Stereotypes on Media, progetto nato dalla collaborazione tra il Centro D’Ascolto dell’Informazione Radiotelevisiva e il Dipartimento di Comunicazione e Ricerca Sociale della Sapienza, presenta i dati del monitoraggio della rappresentazione delle minoranze sui mezzi di informazione italiani.

Roma, 22 febbraio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Domani con la Commissione straordinaria per la tutela e la promozione dei Diritti Umani del Senato, presso la Sala Conferenze ex Hotel Santa Chiara (Via di S.Chiara, 5 – Roma) verranno presentati i risultati della ricerca sulla rappresentazione delle minoranze nei media italiani. ‘Minorities Stereotypes on Media è un progetto di ricerca nato dalla collaborazione tra il Centro d’Ascolto dell’Informazione Radiotelevisiva e il Dipartimento di Comunicazione e Ricerca Sociale dell’Università Sapienza di Roma, con il supporto dell’Open Society Foundations.

Interverranno, tra gli altri:

-        Gianni Betto, Direttore del Centro d’Ascolto dell’Informazione Radiotelevisiva

-        Mario Morcellini, Direttore del Dipartimento di Comunicazione e Ricerca Sociale Università Sapienza di Roma

-        Pietro Marcenaro, Presidente Commissione straordinaria per la tutela e la promozione dei Diritti Umani

-        Emma Bonino, Vice Presidente del Senato

-        Sergio Rovasio, Associazione Radicale Certi Diritti

La ricerca si è svolta nel corso di tutto il 2011. Verranno illustrati i metodi di lavoro utilizzati per la ricerca a partire dal concetto di minoranza dell’indagine Mister Media, le tipologie di minoranze analizzate, lo sguardo d’insieme sul ritratto televisivo delle minoranze. Un capitolo della ricerca riguarderà il tema: Uguali e diversi, tipi di minoranze e immagini mediali: gli immigrati e i rifugiati; i Rom, i Sinti i nomadi, gli zingari e le altre minoranze etno-culturali e linguistiche; le minoranze per orientamento sessuale, le minoranze religiose.

“(…) Sulle reti televisive e radiofoniche sono stati monitorati 24 ore su 24 tutti i notiziari informativi e tutte le trasmissioni di approfondimento: 168 ore al giorno di programmazione televisiva e 360 ore al giorno di programmazione radiofonica (Rai, Mediaset, La7, e i canali radiofonici Rai, Radio 101, Rmc, Radio Capital, Radio Deejay, Radio Kiss Kiss, Rtl 102,5 network, Radio Dimensione Suono, Radio Italia solo musica italiana, Radio 24-Il Sole 24 ore, M20, Virgin Radio)….”