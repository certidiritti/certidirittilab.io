---
title: '2008-0140-Discrimination-FI-ST14548.EN10'
date: Wed, 06 Oct 2010 10:36:07 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 6 October 2010

Interinstitutional File:

2008/0140 (CNS)

14548/10

LIMITE

SOC 616

JAI 810

MI 359

  

  

  

  

  

NOTE

from :

General Secretariat

to :

The Working Party on Social Questions

on :

19 October 2010

No. prev. doc. :

13883/10 SOC 561 JAI 759 MI 317

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

In preparation of the next meeting of the Working Party, which is provisionally scheduled for 19 October 2010, delegations will find attached a note from the FI delegation.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Questions for SQWP debate on housing**

**1\.** **Situation in Member States**

Both the Racial Equality Directive (2000/43/EC) and the Gender Goods and Services Equality Directive (2004/113/EC) cover housing available to the public in their scope. Several MS have gone beyond these two grounds in their protection against discrimination.

a)         To what extent does your national legislation against discrimination apply to housing?

_There is a patchwork of legal provisions that are relevant in the field of housing when it comes to prohibiting discrimination. To begin with, section 6 of the Constitution (731/1999) prohibits discrimination on a wide variety of grounds, including all the grounds covered by Article 19 of TFEU. The Constitution implicitly prohibits unequal treatment, without an acceptable reason, in the provision of housing services by the public authorities. The Act on Equality between Women and Men (609/1986) prohibits gender discrimination in the provision of goods and services that are provided to the public. The Equal Treatment Act (21/2004) prohibits discrimination on the grounds of ethnic origin in the provision of goods and services to the public. Neither of these two acts apply to housing that is provided within the context of private and family life. Finally, section 11:11 of the Criminal Code (39/1889) penalizes discrimination on a number of grounds (again, including all the grounds mentioned in Article 19 of TFEU) when the offence takes place within a public office or in a private sector business._

_Moreover, a number of other acts and regulations govern different aspects of housing, and are to some extent relevant in this context even if they do not incorporate an express anti-discrimination provision. For instance, section 6 of the Tenancy Act (481/1995) provides that a stipulation of a tenancy agreement that is contrary to sound practice or that is otherwise unreasonable, may be amended or declared null and void._

  

b)         Do you distinguish (for this purpose) between private and public/social housing?

_As of now, the distinction between private and public/social housing is of some relevance. This is particularly relevant from the point of view of section 6 of the Constitution, in relation to which municipalities and other public authorities, such as the Housing Finance and Development Centre of Finland (ARA), oversee and provide guidance with regard to non-discriminatory provision of public housing._

c)       Do you have statistics on the number of cases of discrimination in the area of housing, and if so, what is the most prevalent ground?

_Precise and comprehensive statistics on housing discrimination is currently not available. However, according to a report by the Finnish League for Human Rights, Finnish courts of first instance dealt with altogether 45 discrimination cases in the time period 1.1.2007-31.8.2008; one of these cases dealt with discrimination in housing. The Ombudsman for Minorities, who deals exclusively with complaints about ethnic discrimination, received 921 communications in 2009, and 23 % of these concerned housing. Also the Parliamentary Ombudsman and the Housing Finance and Development Centre of Finland (ARA) receive complaints every now and then, but precise figures are not available._

**2\.** **Social housing**

a)         Do you consider social housing to be a service (for the purposes of EU law)?

_We understand services in this context to be those that are defined in Article 57 of TFEU, meaning services “normally provided for remuneration”. Accordingly, some forms of social housing would fall under this category. For instance, some forms of municipal and public interest housing that consists of subsidised or otherwise affordable housing that are provided on social grounds would be considered a service._

_Housing services that are provided to particular groups in accordance with social and health sector legislation, for example the Social Welfare Act (719/1982), the Act on Services and Support based on Disability (380/1987) and Mental Health Act (1116/1990), are not to be considered a service for the purposes of EU law, but are part of the social protection system. This could entail living, on a short or long  time basis, in an apartment rented from the private sector or in a small community -like or institution-like setting._

b)      If not, do you consider it to be a part of the social protection system?

_See above. Living in social- and health care units, as well as in housing arranged by the social welfare services in accordance with the aforementioned acts and other comparable acts in the social and health care sector, fall into this category._

**3\.** **Private life**

In some cases, the principle of equality might be in conflict with the right to private life, in particular in the area of housing (e.g. renting out a room in your own apartment).

a)       Is this better taken into account by an exclusion of transactions in the area of private life from the scope of the Directive, or by an exclusion of activities which are not commercial or professional?

_As things stand, the Act on Equality between Women and Men and the Equal Treatment Act exclude from their respective scopes of application matters that fall into the sphere of private and family life. We consider this to be a good approach, in particular because there is a need to ensure respect of fundamental rights in this context anyway._

_In our view, activities that are of commercial or professional nature would not fall under the exception, whereas for instance subrentals would be._

  

b)         How is commercial/professional housing defined in your national legislation?

_The concepts of commercial or professional housing are not **as such** used let alone defined in the national legislation. For instance, those provisions that deal with the accessibility of buildings, make distinctions **only** with respect to the purpose for which the building is used (administrative buildings/commercial and service buildings). Also the definition of a “public interest body” is relevant from the point of view of housing legislation, as such bodies are eligible for certain state subsidies._

**4\.** **Disability - reasonable accommodation**

a)       In your view, what should the obligation to provide reasonable accommodation mean for providers of housing?

_Such an obligation would refer to measures that do not entail costs that are unreasonable. A typical reasonable accommodation measure would be the instalment of a ramp or a hand rail. It is not entirely clear what is meant by ‘providers of housing’ in this context. It is of essence for Finland, that housing corporations (a specific type of housing co-operative prevalent in Finland but not elsewhere in the EU) are given the obligation to bear responsibility for the common parts of a residential building only insofar as similar obligation is placed upon comparable forms of housing co-operatives. It is also of essence for us that any responsibilities that pertain to providers of housing are clearly spelled out._

b)      Is there legislation on the accessibility of buildings (private as well as public/social) in your country?

_Yes there is, primarily with respect to new buildings. The most important provision in this respect is section 117, subsection 3, of the Land Use and Buildings Act (132/1999), which provides as follows:_

_A building must conform with its purpose and be capable of being repaired, maintained and altered, and, in so far as its use requires, also be suitable for people whose capacity to move or function is limited._

_Accessibility in buildings is regulated moreover by the Land Use and Building Decree, section 53 of which provides as follows:_

_Administrative and service buildings, commercial and service premises in other buildings to which everyone must have access for reasons of equality, and their building sites shall also be suitable for use by persons with restricted ability to move around or function otherwise._

_Taking into account its design and the number of storeys and other circumstances, a residential building and associated spaces shall meet the requirements for accessibility in building._

_For purposes of equality, buildings with work space shall be designed and built so that they provide the persons referred to in paragraph 1 with sufficient opportunity to work, taking into account the nature of the work._

_More detailed provisions on ensuring accessibility in building are laid down in the National Building Code of Finland._

_The National Building Code specifies in greater detail the requirements for accessible building. These codes are applied practically with respect to all new buildings. The requirements are somewhat more lenient with respect to residential buildings that are less than three storeys high._

**5.         Disability - accessibility**

a)       What should the obligation to provide for accessibility measures encompass in the area of housing?

_At least the entrance and the access route from the parking lot to the building should be made accessible. Also access to the common areas of the building should be ensured. Everyone should be able to move freely inside an apartment. All buildings with three or more storeys should have a lift._

b)         Should there be a difference between old and new buildings?

_The making of a difference between old and new buildings is very important to us. In our view, relatively strict accessibility requirements should apply to new buildings, whereas the requirements should be different with respect to old buildings. Without taking a final stance on these questions, we propose that EU member states would be given a long enough implementation time period (e.g. ten or twenty years) with respect to old buildings, whereas we support immediate applicability with respect to new buildings. Moreover, we consider that with respect to old buildings the Member States should perhaps simply be imposed a general obligation to develop their legislation and policies with a view to increasing accessibility, whereas obligations should be more precise and demanding with respect to new buildings. One argument in favour of this approach is that over time, new and duly accessible housing would replace older and less accessible housing._

c)         Do you support immediate applicability with respect to new buildings?

_Yes._

d)      Should there be a difference between the obligations for private and public/social housing respectively?

_In Finland accessibility requirements under the National Building Code are tied to the purpose for which a particular building is used (administrative, residential etc), not to its type as private, social or commercial housing. Therefore the housing standards including accessibility standards are similar to all providers and residential building owners._

_As reported previously The Equal Treatment Act prohibits discrimination on the grounds of the ethnic origin in the provision of social/public and commercial housing. This same principle could be applied to this Directive, for instance in the assessment of reasonable accommodation._

_It could be national policy if stricter requirements will later be applied in housing subsidized or supported from public funds in connection with the Directive._

e)  Are there public funds available in your country for the adaptation of housing (private as well as public/social)?

_Yes. Section 9, subsection 2, of the_ _Act on Services and Support based on Disability provides that a municipality shall reimburse to a severely disabled person reasonable costs arising from accommodation measures made to an apartment as well as reasonable costs arising from the purchase of necessary equipment. Similarly, sections 2 and 6 of the Act on Housing Renovation, Energy and Health Detriment Subsidies provides that funds can be allocated to the renovation of apartments that are used by persons with disabilities._

_The most important institution providing public funding for these purposes is the Housing Finance and Development Centre of Finland (ARA)._

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_