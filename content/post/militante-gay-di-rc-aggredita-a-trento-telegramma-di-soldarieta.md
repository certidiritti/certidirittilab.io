---
title: 'MILITANTE GAY DI RC AGGREDITA A TRENTO: TELEGRAMMA DI SOLDARIETA'''
date: Mon, 11 May 2009 08:06:27 +0000
draft: false
tags: [Comunicati stampa]
---

GAY AGGREDITA A TRENTO: SOLIDARIETA' DI CERTI DIRITTI PER L'ENNESIMO ATTO DI VIOLENZA CONTRO UNA PERSONA GAY.

"L'Associazone Radicale Certi Diritti ha oggi inviato un telegramma di solidarietà ai dirigenti di Rifondazione Comunista di Trento per l'atto squadrista e violento accaduto ieri ad una militante gay. Di seguito il testo del telegramma inviato: "Vi siamo vicini per questo ennesimo atto violento avvenuto ai danni di una militante gay sola e indifesa nella vostra sede di Trento. Il ripetersi su tutto il territorio nazionale di atti violenti di squadrismo omofobico deve preoccupare l'opinione pubblica totalmente distratta e influenzata da demagogia, populismo e falsità, anche dell'informazione, contro minoranze politiche e sociali".