---
title: 'IX Congresso dell''Associazione Radicale Certi Diritti'
date: Tue, 29 Sep 2015 22:59:11 +0000
draft: false
tags: [Politica]
---

  ![unnamed](http://www.certidiritti.org/wp-content/uploads/2015/10/unnamed.png)   E' convocato per il 6, 7 e 8 novembre 2015 ad Arezzo il IX Congresso dell'Associazione Radicale Certi Diritti, il massimo organo deliberativo dell’Associazione, di cui stabilisce annualmente gli obiettivi d’azione. La partecipazione è aperta a chiunque. Gli [iscritti](http://www.certidiritti.org/iscriviti/) in regola con la quota di iscrizione possono votare i documenti congressuali e le cariche statutarie. Questa pagina verrà progressivamente aggiornata con il programma definitivo e con tutte le informazioni per organizzare la vostra permanenza ad Arezzo durante i tre giorni di lavori congressuali. Preannuncia la tua partecipazione attraverso l'[evento Facebook](https://www.facebook.com/events/160322227644837/).  [![Convoca](http://www.certidiritti.org/wp-content/uploads/2015/09/Convoca.jpg)](http://www.certidiritti.org/donazioni/)  

**ORDINE DEI LAVORI**

 

\*\*\*

**Venerdì 6 novembre 2015**

**Sala dei grandi, Palazzo della Provincia, P.zza Libertà, Arezzo**

[![unnamed](http://www.certidiritti.org/wp-content/uploads/2015/09/unnamed.gif)](http://www.certidiritti.org/wp-content/uploads/2015/09/unnamed.gif)

[![logo-Provincia-arezzo](http://www.certidiritti.org/wp-content/uploads/2015/09/logo-Provincia-arezzo.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/09/unnamed.gif)

GENERI DI PRIMA NECESSITA’ Contro il bullismo sessista, omofobo e transfobico nelle scuole, ore 14 – 18

  Saluti istituzionali e Introduzione L’importanza deli interventi contro il sessismo e il bullismo nelle scuole o Marco Rossi Doria, Assessore alle Politiche educative, scolastiche e giovanili del Comune di Roma * o Vincenzo Spadafora, Garante per l’Infanzia e l’adolescenza* Cosa è il bullismo o Un rappresentante dell’Università di Trieste/Regione Friuli Venzia Giulia* Verità e manipolazione sulla cosiddetta teoria del gender o Lorenzo Bernini, Università degli Studi di Verona Il Parlamento europeo e l’uguaglianza di genere nelle scuole • Liliana Rodrigues, europarlamentare* Educare alle differenze o Giulia Selmi, Associazione Scosse e Rete nazionale di Educare alle differenze Esperienze nelle scuole toscane o Daniela Volpi, Regione Toscana o Eleonora Ducci, Provincia di Arezzo o Alice Troise o Federico Batini* Conclusioni o Un rappresentante dell’Associazione radicale certi diritti

\*\*\*

**Sabato e domenica 7 e 8 novembre**

_Hotel Continentale, Piazza Guido Monaco n.7 - 52100 Arezzo www.hotelcontinentale.com_

IX Congresso nazionale

Associazione radicale Certi Diritti

-

_«Comizi d’Amore» per una riforma del diritto di famiglia._

_Ricerca, proposta, dibattito, attivismo_

-

    **Sabato**   Ore 9,00 Insediamento presidenza e prime procedure congressuali a seguire Saluti istituzionali e delle associazioni invitate   Ore 10,00 Relazione di Segretario, Tesoriere, Presidente e Revisore dei conti.  

Ore 11,30

Estratto dal film "La Donna Pipistrello". Regia di Francesco Belais e Matteo Tortora. Anno 2015

Stefania Voli, Universtà Milano-Bicocca, (Trans)sessualità, genere e politica nel dibattito parlamentare verso la legge 164/1982

  Ore 12,00 Interverranno:

Bruno De Filippis, Tribunale di Salerno, Riforma del diritto di Famiglia

Monica Cirinnà, Senatrice del Partito Democratico prima firmataria del ddl 2081 sulle unioni civili e le convivenze di fatto

Sergio Lo Giudice, senatore PD, aggiornamento sul ddl Unioni Civili

Mark Seymour, University of Otago, Il discorso pubblico italiano intorno alla riforma del diritto di famiglia   Ore 14,00 Enzo Cucco, Fondo per le cause pilota e Litigation Strategy in Italia

Alexander Schuster, Università di Trento

Maria Grazia Sangalli, Rete Lenford

Tito Flagella, avvocato.

  Ore 15,00 Estratto dal film "Ucraina Paralipomenon. Cronaca delle omissioni". Regia di Yulia Gaia Matsiy. Anno 2015 Antonio Stango, direttivo Certi Diritti, La pena di morte per omosessualità nel mondo. Patrick McDonagh, PhD Researcher, European University Institute, Un approccio storico al referendum irlandese. Frank van Dalen, Liberal International, La strategia LGBTI dei liberali internazionali   Ore 16,00 Francesca Russo, Amnesty International, La decriminalizzazione del lavoro sessuale Pia Covre, Comitato per i diritti civili delle prostitute Isabella Eretica Gerini*, blogger e attivista   Ore 17,00 Miriam Van Der Have, co-presidente Oii Europe, presidente NNID Alessandro Comeni, attivista intersex, cofondatore del collettivo Intersexioni, cofondatore di OII Europe, Presidente onorario dell’associazione radicale Certi Diritti Michela Balocchi, ricercatrice Marie Curie, American University (in videoconferenza da Washington DC) Eleonora, mamma e volontaria di intersexioni   Ore 18,00 Dibattito generale   **Domenica, Intersex Solidarity Day**   Ore 9,00 Dibattito generale Benedetto Della Vedova, senatore del Gruppo Misto, Sottosegretario agli Affari Esteri Mozioni e votazioni     *=in attesa di conferma