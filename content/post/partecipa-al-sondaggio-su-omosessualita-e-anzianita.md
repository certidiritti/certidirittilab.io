---
title: 'Partecipa al sondaggio su omosessualità e anzianità'
date: Mon, 12 Nov 2012 14:50:17 +0000
draft: false
tags: [Movimento LGBTI]
---

Il sondaggio è rivolto alle persone LGBT (lesbiche, gay, bisessuali, trans), di tutte le età.  
E’ la prima volta che, in Italia, viene realizzato uno studio di questo tipo.

I risultati dell’indagine verranno presentati il 28 novembre 2012 durante il convegno nazionale “Omosessualità e Anzianità: tra invisibilità e nuovi diritti”, organizzato da Equality Italia e SPI CGIL.  
Verranno anche pubblicati sul sito web www.lelleri.it.

Scadenza per la raccolta dei questionari: 23 novembre 2012.

**[VAI AL SONDAGGIO >>>](http://www.lelleri.it/sondaggio-anziani/)**