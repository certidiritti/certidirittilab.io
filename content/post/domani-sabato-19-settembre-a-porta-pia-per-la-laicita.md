---
title: 'DOMANI, SABATO 19 SETTEMBRE A PORTA PIA, PER LA LAICITA'''
date: Fri, 18 Sep 2009 12:49:32 +0000
draft: false
tags: [Comunicati stampa]
---

Comunicato Stampa delle associazioni radicali Anticlericale.net e Certi Diritti

Anche in onore ai caduti di Kabul, perché con il tempo il loro sacrificio non sia dimenticato come è stato dimenticato, cancellato, quello dei bersaglieri impegnati a combattere l’usurpatore, confermiamo che domani, sabato 19 settembre alle ore 14.30 saremo a Porta Pia, per la commemorazione del XX Settembre 1870.

La manifestazione è promossa dalle Associazioni radicali Anticlericale.net e Certi Diritti, con l'adesione e la partecipazione di Radicali Italiani, Associazione Luca Coscioni, Nessuno Tocchi Caino, Radicali Roma, Comitato Nathan, Agorà Digitale, Ass. Radicale Antiproibizionisti,  Esperanto Radikala Asocio, Circolo di Cultura omosessuale Mario Mieli e Arcigay.  
  

Facciamo appello ai cittadini che hanno a cuore la laicità dello Stato a partecipare a questo importante appuntamento che ogni anno ricorda la liberazione dal potere clericale.  
  

Avremmo voluto, come sempre, manifestare per le vie della città, con una marcia che voleva toccare  i luoghi simbolo della lotta anticlericale di Roma. Per questo avevamo presentato formale preavviso alle autorità, ma il Questore di Roma, quest’anno per la prima volta, ha vietato l’iniziativa, cosicché la memoria di quei bersaglieri caduti per compiere quella liberazione rischia di essere meglio e definitivamente dimenticata.  
  

Saremo a Porta Pia con la bandiera italiana abbrunata e quella del Partito Radicale Nonviolento, Transnazionale e Transpartito: per i morti di Kabul e per quelli di Porta Pia, gli uni e gli altri impegnati a sconfiggere gli estremismi politici e religiosi, antidemocratici.  Saremo a Porta Pia per denunciare lo scempio di legalità e democrazia contro la Costituzione

repubblicana.