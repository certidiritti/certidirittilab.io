---
title: 'Denuncia della Lila: Ministero della Difesa chiede test Hiv per suonare nella banda o per accedere alle scuole dell''Esercito. Esposto all''Unar e all''Oscad'
date: Fri, 06 Jul 2012 10:22:49 +0000
draft: false
tags: [Salute sessuale]
---

Violata la legge 135/1990 e altre norme internazionali.

Roma, 6 luglio 2012

  
Comunicato Stampa dell’Associazione Radicale Certi Diritti

La Lega Italiana per la Lotta contro l’Aids ha oggi denunciato una grave forma di discriminazione nei confronti delle persone con l’Hiv. Tutti i Bandi del Ministero della Difesa chiedono esplicitamente ai candidati di presentare un test Hiv negativo pena l’esclusione. Sia che si tratti di suonare nella Banda dell’Arma dei Carabinieri, sia si tratti di tirare con l’arco in un centro agonistico della Marina Militare. Tra i Bandi elencati dalla Lila vi è anche quello riguardante i 248 posti disponibili per l’accesso ai licei annessi alle Scuole militari.

Il tutto in violazione della Legge 135/1990, e altre norme sul mondo del lavoro anche quelle dell’Onu che escludono la possibilità di richiedere il test dell’Hiv ai lavoratori e aspiranti tali.

L’Associazione Radicale Certi Diritti ha inviato oggi un esposto all’Unar e all’Oscad segnalando questa evidentissima forma di discriminazione nei confronti delle persone con Hiv. La Lila segnala anche l’interrogazione presentata dai Parlamentari Radicali che non ha mai ricevuto risposta nemmeno da questo Governo, nonostante tutti i solleciti.  
  
**sotto in allegato l'interrogazione dell'on. Maurizio Turco.**

  
[interrogazione\_525\_-\_test\_HIV\_concorsi\_accesso\_forze\_armate.doc](http://www.certidiritti.org/wp-content/uploads/2012/07/interrogazione_525_-_test_HIV_concorsi_accesso_forze_armate.doc)