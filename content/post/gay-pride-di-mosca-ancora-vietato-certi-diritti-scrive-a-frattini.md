---
title: 'Gay Pride di Mosca ancora vietato Certi Diritti scrive a Frattini'
date: Thu, 26 May 2011 08:38:12 +0000
draft: false
tags: [Russia]
---

 Per il sesto anni consecutivo il Pride russo vietato dal sindaco, su richiesta dei gruppi fanatici religiosi, nonostante la sentenza di condanna della Corte Europea dei dirtti dell'Uomo.  
Certi Diritti scrive al minsitro Frattini chiedendo di allertare ambasciata italiana per invio osservatori e per vigilare sul rispetto diritti umani.

Roma, 26 maggio 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Il sindaco di Mosca, dopo un lungo tira e molla, ha definitivamente deciso di vietare il Gay Pride a Mosca che si sarebbe dovuto tenere il prossimo 28 maggio. L’appuntamento era stato in un primo momento autorizzato dal Vice Sindaco, Liubov Chevtsova, che aveva posto alcune condizioni ai manifestanti, tra le altre quello che alla manifestazione non dovevano partecipare più di 500 persone.  Il raduno era previsto in piazza Bolotnaia, nel centro della capitale russa.

Secondo quanto dichiarato da Nikolay Alexeiev, organizzatore del Pride moscovita, che era stato ospite a Roma al IV congresso di Certi Diritti, il nuovo Sindaco di Mosca, “Sergei Sobianin, è il degno erede omofobo del vecchio Sindaco,   Yuri Luzhkov”, padrone della città per 18 anni, che era stato condannato dalla Corte Europea dei Diritti dell’Uomo per aver deliberato il divieto allo svolgimento del gay pride nella città. La sentenza della Cedu  aveva stabilito che “il divieto del  Gay Pride di Mosca, è una discriminazione, una illegale violazione del diritto alla protesta pacifica e alla libertà di riunione , in contrasto con la Convenzione europea dei diritti dell'uomo” e per questo aveva condannato la Russia a risarcire gli organizzatori del Pride. La Russia aveva addirittura presentato ricorso in Tribunale a questa sentenza che ha poi perso.

E’ il sesto anno consecutivo che il Gay Pride moscovita viene vietato dalle autorità in violazione dei più elementari diritti democratici e costituzionali, il Comune di Mosca ha invece autorizzato una “manifestazione rally  contro le perversioni sessuali” alla quale hanno partecipato oltre 3.000 persone.

Gli attivisti lgbt hanno preannunciato che si recheranno ugualmente a manifestare sfidando il divieto. Alcuni osservatori occidentali si recheranno a Mosca, così come avevano già fatto nel 2008 i parlamentari Marco Cappato e Vladimir Luxuria, che, insieme al funzionario del Parlamento Europeo Ottavio Marzocchi, vennero poi fermati e arrestati dalle forze speciali di sicurezza russe.

Anche i gruppi del fanatismo religioso, insieme ai gruppi di nazinskin, hanno preannunciato che saranno presenti sul luogo.

**L’Associazione Radicale Certi Diritti ha mandato una lettera al Ministro degli Esteri Franco Frattini chiedendo, così come hanno fatto altre organizzazioni lgbt(e) europee, di allertare la nostra Ambasciata italiana a Mosca affinchè invii propri osservatori sul luogo della manifestazione e affinchè vigili sul rispetto dei diritti civili e umani.**

Alcune Ambasciate occidentali, tra queste la Gran Bretagna, hanno assicurato la presenza di loro osservatori durante il Gay Pride.