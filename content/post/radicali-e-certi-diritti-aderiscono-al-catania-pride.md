---
title: 'RADICALI E CERTI DIRITTI  ADERISCONO AL CATANIA PRIDE'
date: Fri, 20 Jun 2008 16:48:27 +0000
draft: false
tags: [Catania Pride, Certi Diritti Catania, Comunicati stampa, Radicali Catania]
---

Catania, 20 giugno 2008  
Adesione al Catania Pride  
Comunicato congiunto dell’associazione Radicali Catania e di Certi Diritti di Catania L’associazione Radicali Catania e l’associazione radicale Certi Diritti aderiscono al Catania Pride 2008, con l’obiettivo di contribuire alla lotta per il pieno riconoscimento dei diritti delle persone gay, lesbiche, bisessuali e transessuali.  
  
Riteniamo che l’orientamento sessuale e l’identità di genere non possano costituire la base per negare alcuni diritti fondamentali della persona e per questo, come radicali, ci battiamo, in Parlamento e nel paese, per il superamento di odiose discriminazioni e per una completa eguaglianza sotto il profilo giuridico, compresa la possibilità per le persone omosessuali di accedere agli istituti del matrimonio civile e dell’adozione di minori, come già avviene in numerose democrazie avanzate.  
  
Parteciperemo al corteo del prossimo 5 luglio, e invitiamo i cittadini di Catania a fare altrettanto: essere presenti al Pride è la migliore risposta al pesante clima omofobico e al gravissimo ritardo del nostro paese in materia di diritti civili.  
  
  
www.radicalicatania.org  
info@radicalicatania.org  
certidirittict@live.it