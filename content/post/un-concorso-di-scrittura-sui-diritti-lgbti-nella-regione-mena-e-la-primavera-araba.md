---
title: 'Un concorso di scrittura sui diritti LGBTI nella regione MENA e la Primavera Araba'
date: Sat, 14 Apr 2012 07:32:20 +0000
draft: false
tags: [Transnazionale]
---

Un'iniziativa di 'Non c'è Pace Senza Giustizia' in collaborazione con l'associazione radicale Certi Diritti ed il Partito Radicale Non-violento, Transnazionale e Transpartito (PRNTT).

Bruxelles - Roma - New York, 6 aprile 2012

Non c'è Pace Senza Giustizia, in collaborazione con l'associazione radicale Certi Diritti ed il Partito Radicale Non-violento, Transnazionale e Transpartito (PRNTT) lancia un concorso di scrittura sul tema dei diritti LGBTI (lesbiche, gay, bisessuali, transessuali ed intersex) nella regione del Medio oriente e Nord africa (MENA) e la Primavera Araba/ Rivoluzione del gelsomino: “The people demand the end of discrimination”.  
   
La competizione è aperta a singoli attivisti e associazioni che operano nella regione, con interesse per i diritti e le tematiche LGBTI.  
   
Ai partecipanti viene richiesto di scrivere un saggio breve o un articolo, che descriva se e come la Primavera Araba / Rivoluzione del Gelsomino abbia avuto effetti sui diritti LGBTI e/o in che misura gli attivisti per i diritti LGBTI abbiano contribuito ad alimentare questo movimento in favore della democrazia.  
   
In particolare, dovrebbero essere in grado di evidenziare se la Primavera Araba abbia favorito gli sviluppi, siano essi positivi o negativi, nel campo dei diritti LGBTI all'interno del loro paese.  
La selezione e la classifica dei migliori elaborati sarà effetuata da una giuria internazionale, composta da eminenti personalità nel campo dei diritti umani.  
   
I migliori elaborati saranno pubblicati e pubblicizzati da Non c'è Pace senza Giustizia. I primi tre classificati riceveranno inoltre un premio in denaro di 700 dollari ciascuno.  
   
Tempistiche  
I partecipanti devono registrarsi entro lunedi 30 aprile 2012 inviando una mail all'indirizzo [lgbti@npwj.org](mailto:lgbti@npwj.org), con una breve descrizione (tra 100 e 300 parole) dell'elaborato, e indicando nome (dell'individuo o dell'organizzazione), età, email, città e paese. Solo i partecipanti registratisi entro il 30 aprile potranno partecipare alla competizione.  
   
Gli elaborati potranno contenere tra le 2000 e le 3500 parole e dovranno essere consegnati entro il 30 Giugno 2012.  
   
Le informazioni personali dei partecipanti saranno trattate nel pieno rispetto della legislazione sulla privacy e gli elaborati vincitori potranno essere pubblicati sotto pseudonimo, se necesssario. Tuttavia, NPSG avrà bisogno dei dati effettivi (nome ed indirizzo) dei partecipanti per trasferire gli eventuali premi in denaro e per garantire i diritti di copyright.  
   
**[Per maggiori info sul concorso di scrittura e per scaricare il volantino in francese, inglese e arabo clicca qui >](http://www.npwj.org/it/LGBTI/NPSG-lancia-un-concorso-di-scrittura-sui-diritti-LGBTI-nella-regione-MENA-e-la-Primavera-Araba)**