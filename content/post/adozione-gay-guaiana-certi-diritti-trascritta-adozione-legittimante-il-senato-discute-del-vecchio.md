---
title: 'ADOZIONE GAY. GUAIANA (CERTI DIRITTI): TRASCRITTA ADOZIONE LEGITTIMANTE, IL SENATO DISCUTE DEL VECCHIO'
date: Thu, 10 Dec 2015 22:29:15 +0000
draft: false
tags: [adozione, adozione gay, Affermazione Civile, corte d'appello di milano, Diritto di Famiglia, yuri guaiana]
---

[![2015](http://www.certidiritti.org/wp-content/uploads/2015/05/2015-300x111.png)](http://www.certidiritti.org/wp-content/uploads/2015/05/2015.png)"Importante ed innovativa sentenza di merito attraverso la quale la Corte d'Appello di Milano ha ordinato la trascrizione dell'adozione per la seconda mamma di una bambina nata in spagna grazie alla procreazione medicalmente assistitia. Si tratta di una pietra miliare  n ella giurisprudenza relativa a questa materia.Non  si  parl a  più dell'adozione in casi particolari, ma di adozione che parifica completamente i diritti-doveri della genitrice sociale a quelli della genitrice biologica.  Benchè al giurisprudenza abbia fatto significativi passi avanti nel frattempo in parlamento si discute  di  un disegno di legge che per la timidezza e le continue procrastinazioni si è già fatto vecchio.  Appare anacronistico  pensare che c'è chi vuole sostituire la stepchild adoption con assurdi artifici legislativi come l'affido rafforzato".

 E' quanto dichiara Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, commentando la notizia della sentenza emessa dalla corte d'appello di milano.

"Si conferma un'Italia a doppia velocità: quando il Parlamento andrà al passo dei cittadini e dell e   sempre più frequenti pronunce giurisprudenziali in tal senso? Non è abbastanza ricordare ai nostri onorevoli che l'Italia deve fare i conti con una condanna definitiva da parte della CEDU per la totale assenza di una legislazione per le famiglie e le coppie dello stesso sesso?"