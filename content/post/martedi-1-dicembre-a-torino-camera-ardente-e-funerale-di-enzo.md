---
title: 'MARTEDI'' 1 DICEMBRE A TORINO CAMERA ARDENTE E FUNERALE DI ENZO'
date: Mon, 30 Nov 2009 13:14:07 +0000
draft: false
tags: [Comunicati stampa]
---

**MARTEDI’ 1 DICEMBRE,  A TORINO CAMERA ARDENTE E FUNERALE DI ENZO FRANCONE, ESPONENTE STORICO DEL MOVIMENTO LGBT, TESORIERE DI CERTI DIRITTI.**

Martedì 1 dicembre 2009,  dalle 11 alle 14,30 sarà allestita la camera ardente di Enzo Francone., esponente storico del movimento lgbt italiano, Tesoriere dell'Associazione Radicale Certi Diritti. La camera ardente sarà allestita presso la Fondazione Sandro Penna, Via Santa Chiara 1 a Torino.

Alle 15,30 cerimonia funebre presso il Tempio crematorio del Cimitero monumentale, ingresso principale.