---
title: 'CERTI DIRITTI AL PARLAMENTO EUROPEO IL 17 APRILE'
date: Tue, 08 Apr 2008 08:51:14 +0000
draft: false
tags: [Comunicati stampa]
---

Il 17 aprile a Bruxelles presso la sede del Parlamento Europeo, l'Associazione Radicale Certi Diritti parteciperà alla Conferenza promossa dai deputati Sophie In't Veld e Marco Cappato del Gruppo liberale Alde, organizzata da Ottavio Marzocchi, dirigente della nostra Associazione. I lavori saranno registrati da Radio Radicale e verranno al più presto messi a disposizione sul nostro sito. Per l'Associazione, oltre a Ottavio Marzocchi, parteciperanno Matteo Pegoraro e Sergio Rovasio.