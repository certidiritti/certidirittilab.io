---
title: 'CONGRESSO ILGA ASIA INTERROTTO PER COLPA DEI FONDAMENTALISTI RELIGIOSI'
date: Fri, 26 Mar 2010 16:36:42 +0000
draft: false
tags: [Comunicati stampa]
---

**CONFERENZA ILGA IN ASIA. LA POLIZIA CEDE AL RICATTO DELLA FOLLA DEI FONDAMENTALISTI E LA CONFERENZA VIENE ANNULLATA. LA VIOLENZA DEL FONDAMENTALISMO RELIGIOSO COLPISCE OVUNQUE.**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

“La decisione della polizia indonesiana di far annullare una importante riunione asiatica dell’International Lesbian and Gay Association, è gravissima e si basa sul ricatto violento della cultura del fondamentalismo religioso. Per diversi giorni è stato impossibile avere la certezza che la riunione si potesse fare pur avendo gli organizzatori pagato le spese dell’albergo, tra i più importanti di Sarabaya, con partecipanti giunti da tutta l’Asia e alcuni dall’Europa. La polizia ha ceduto al fanatismo religioso di un centinaio di fondamentalisti islamici che avevano invaso alle ore 13 locali la hall dell’albergo.

L’Associazione Radicale Certi Diritti esprime tutta la sua solidarietà all’Interntation Lesbian and Gay Association di cui è membro e al co-Segretario Generale Renato Sabbadini. Quanto avvenuto è gravissimo e dimostra di quanto il fanatismo religioso esprima soltanto violenza e odio”.