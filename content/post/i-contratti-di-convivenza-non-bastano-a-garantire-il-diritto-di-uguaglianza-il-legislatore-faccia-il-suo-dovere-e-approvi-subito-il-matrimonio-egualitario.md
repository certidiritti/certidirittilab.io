---
title: 'I contratti di convivenza non bastano a garantire il diritto di uguaglianza. Il Legislatore faccia il suo dovere e approvi subito il matrimonio egualitario.'
date: Thu, 28 Nov 2013 12:18:17 +0000
draft: false
tags: [Matrimonio egualitario]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 28 novembre 2013

Il 30 novembre il Consiglio Nazionale del Notariato ha indetto la giornata dei Contratti di Convivenza: molto bene, i notai danno prova, civile e professionale, di sensibilità sociale per la tematica delle coppie (anche) del medesimo sesso e decidono di evidenziare le questioni economiche e patrimoniali della loro vita che le coppie possono definire già oggi con contratti privati. Non soltanto, però, non potranno essere risolti con questo strumento tutti i problemi sul tavolo (eredità, previdenza, ecc.), e i notai giustamente sottolineano che per questo occorre una legge, ma risalta ancora una volta la discriminazione della legislazione negata.

Iniziative come questa, così come le tante decisioni dei giudici che – applicando il criterio indicato dalla sentenza 138 della Corte Costituzionale – riconoscono diritti a coppie che vorrebbero ma non possono sposarsi, mettono ancora più in risalto come il legislatore, con la sua latitanza, continui a negare il diritto di uguaglianza alle coppie dello stesso sesso che può essere riconosciuto, lo ribadiamo ancora, solo attraverso il matrimonio egualitario: l'apertura a tutti i cittadini, a prescindere dal loro orientamento sessuale, dell'istituto principe della vita di coppia.