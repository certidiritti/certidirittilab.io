---
title: 'Lavoratrice del sesso trovata morta a Roma: Certi Diritti denuncia il fallimento delle politiche proibizioniste e chiede con forza la decriminalizzazione della prostituzione'
date: Thu, 11 Oct 2012 06:53:10 +0000
draft: false
tags: [Lavoro sessuale]
---

Firma l'appello per la decriminalizzazione e la regolamentazione della prostituzione

Comunicato stampa dell’Associazioni Radicale Certi Diritti.

Roma, 10 ottobre 2012

Ieri una lavoratrice del sesso nigeriana è stata trovata morta a Roma e meno di un mese fa una sua collega rumena è stata picchiata data alle fiamme nella periferia della capitale. Una violenza inaccettabile contro delle donne, a poco più di un mese dalla Giornata internazionale per l'eliminazione della violenza contro le donne, e contro delle lavoratrici che va condannata con forza. È evidente che le multe e il pugno di ferro di Alemanno sanno produrre solo tragedie. Quante altre vittime dovremo contare prima che ci si renda conto del fallimento delle politiche proibizioniste sul lavoro sessuale?  
  
Queste politiche vanno superate come chiedono l’Associazione Radicale Certi Diritti, il Comitato per i Diritti Civili delle Prostitute e la CGIL-Nuovi Diritti in un **[Manifesto-Appello per la decriminalizzazione della prostituzione.](campagne-certi-diritti/itemlist/category/85-legalizzazione-prostituzione)**

Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti, afferma: “Al posto delle multe, peraltro ritenute illegittime dal Consiglio di Stato, e delle parate in motocicletta Alemanno dovrebbe pensare a una seria politica di zonizzazione per consentire alle lavoratrici e ai lavoratori del sesso di offrire i loro servizi in piena sicurezza. Ma il problema non può essere risolto solo dagli enti locali,  è tempo che la legge Merlin venga riformata nel senso di un superamento del divieto di prostituirsi al chiuso e del reato di favoreggiamento della prostituzione. Solo così si potrà garantire la necessaria sicurezza alle lavoratrici e ai lavoratori del sesso che scelgono questa professione e, al contempo, rendere più efficace la lotta contro lo sfruttamento e contro la tratta”.