---
title: 'Newsletter 6 Aprile'
date: Sun, 08 Apr 2012 12:59:43 +0000
draft: false
tags: [Politica]
---

![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)

Il prossimo 21 aprile l’Associazione Radicale Certi Diritti, il Comitato per i Diritti Civili delle Prostitute e la Cgil-Nuovi Diritti, terranno a Roma una Conferenza Nazionale sulla Legalizzazione della Prostituzione alla quale ti invitiamo a partecipare.** [Qui trovi il programma quasi definitivo >](conferenza-nazionale-sulla-legalizzazione-della-prostituzione)**

**[Il Parlamento Europeo riaffermando il diritto alla libera circolazione per le coppie dello stesso sesso conferma le motivazioni della sentenza di Reggio Emilia >   ](parlamento-europeo-sostiene-sentenza-reggio-emilia-riaffermando-diritto-alla-libera-circolazione-delle-coppie-dello-stesso-sesso)  
**“Oggi in Italia ci sono due battaglie civili che si stanno combattendo nei tribunali per le coppie omosessuali: la prima è quella per il riconoscimento del diritto al matrimonio o, in alternativa, a un’unione registrata; la seconda è quella del riconoscimento di singoli diritti garantiti ai coniugi, come nel caso di questa sentenza”: **[intervista a Giulia Perin](http://www.justicetv.it/index.php/news/2201-reggio-emilia-sposi-gay-allestero-si-a-permesso-soggiorno) **che ha assistito il ricorrente della sentenza di Reggio Emilia per conto dell'associazione radicale Certi Diritti.

**[Coppia gay ricorre contro copertura sanitaria negata e vince >](http://www.gay.it/channel/attualita/33397/Coppia-gay-ricorre-contro-negata-copertura-sanitaria-e-vince.html)          
**E' ancora un tribunale a stabilire che nessuna discriminazione può essere fatta tra coppie eterosessuali e coppie omosessuali. [A pronunciarsi è la Corte d'Appello di Milano](http://www.justicetv.it/index.php/news/2212-milano-gay-convivente-ottiene-copertura-sanitaria) ma il**Ministro Fornero **sostiene che i diritti per [l e coppie gay necessitano di una … strada lunga](http://www.vanityfair.it/news/italia/2012/04/03/coppie-gay-elsa-fornero-nocoppiediserieb). Noi quando si tratta dei diritti dei cittadini crediamo che non ci possano essere priorità e per questo continueremo a chiedere alla classe politica il riconoscimento di quei diritti riaffermati con forza dalle sentenze della Corte costituzionale e di Cassazione.

**[L'unione fa la forza! Cosa manca alla strategia italiana per il diritto al matrimonio ](lunione-fa-la-forza-cosa-manca-alla-strategia-italiana-per-il-diritto-al-matrimonio) di Enzo Cucco**, presidente associazione radicale Certi Diritti.

[Quale riforma per il diritto di famiglia e la giustizia familiare? ](http://www.radioradicale.it/scheda/348686/quale-riforma-per-il-diritto-di-famiglia-e-la-giustizia-familiare) Ascolta su Radio Radicale, tra gli altri,  l’intervento di **Yuri Guaiana**

**Da [Berlino arriva la notizia che le unioni registrate saranno trattate fiscalmente come il matrimonio.](a-berlino-le-unioni-registrate-saranno-trattate-fiscalmente-alla-stessa-maniera-delle-coppie-sposate)**

E ancora:

[**Roma. Contro lo spread dei diritti civili**](http://www.radicali.it/rassegna-stampa/contro-spread-dei-diritti-civili)

**[Carceri: radicali  e Certi Diritti visitano Regina Coeli](http://www.agi.it/roma/notizie/201203311913-pol-rrm1019-carceri_radicali_visitano_regina_coeli).     
25 aprile marcia per l’amnistia. [Partecipa >](http://www.radicalparty.org/it/seconda-marcia-amnistia)         **

Mercoledì 11 aprile 2012 ore 21:00 presso il CIG di via Bezzecca 3 a Milano**, " Certi Diritti che le coppie conviventi non sanno di avere"** con Gabriella Friso del Direttivo e Yuri Guaiana, segretario dell’associazione radicale Certi Diritti. Si discuterà delle recenti sentenze della Corte di Cassazione e di Reggio Emilia che hanno visto il riconoscimento delle coppie dello stesso sesso e **[verrà presentato il nostro libro](certi-diritti-che-le-coppie-conviventi-non-sanno-di-avere). **  
L’Unione Europea desidera conoscere le tue esperienze – fa in modo che la tua esperienza conti. Al fine di sostenere la legislazione e il processo decisionale in materia di diritti LGBT, l’Agenzia dell’Unione europea per i diritti fondamentali (FRA) ha lanciato un sondaggio LGBT in tutta l’Unione con [un questionario che è disponibile on-line](https://www.lgbtsurvey.eu/html/lgbt2t/startpage.php?lang=IT) per raccogliere i pareri di persone gay, lesbiche, bisessuali e transgender (maggiorenni) che vivono nell’Unione Europea e in Croazia.

**Importante:** cerchiamo uno sviluppatore Apple iOS a Milano che possa aiutarci per la nostra App!

**Sostenere l’associazione radicale Certi Diritti costa meno di un caffè a settimana!  
[Scopri le modalità per farlo >](iscriviti)**

  
[www.certidiritti.it](http://www.certidiritti.it/)