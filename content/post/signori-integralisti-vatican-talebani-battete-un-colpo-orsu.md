---
title: 'Signori integralisti, vatican-talebani, battete un colpo, orsù!'
date: Sun, 16 Jan 2011 15:57:22 +0000
draft: false
tags: [Comunicati stampa]
---

**FAMILY DAY BATTI UN COLPO: NELLE DRAMMATICHE ORE IN CUI IL PRESIDENTE DEL CONSIGLIO E' SOTTO ACCUSA SU ESCORT E ALTRO, RIMANIAMO IN ATTESA DI UN CENNO DAI MORALISTI INTEGRALISTI CLERICALI.**

_Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:_

I grandi moralisti, benpensanti, catto-integralisti, i vandeani del berlusconismo più estremista e durista, nordico e clericale, che un giorno si e l'altro pure ci spiegano come dobbiamo vivere nel rispetto delle norme dell'integralismo moral-religioso-tradizionalista o vatican-talebano, oggi sono tutti zitti, silenti, ammutoliti. Nessuno fiata!

Che succede? Che è questo silenzio?  Dove siete tutti quanti? In gita fuori-porta?

Sempre tutti pronti a dirci come e con chi si deve vivere, come si deve morire, con chi si deve come e se si può fare all'amore, che i single devono pagare più tasse, che non bisogna usare il preservativo perchè non serve a combattere l'Aids, che tutti si devono rimettere al modello di famiglia del mulino bianco, all'uopo ben spiegato da una conferenza governativa sulla famiglia durata ben tre giorni e 'gestita' da cardinali, preti e giornalisti dell'Avvenire, il quotidiano della Cei dell'8 per mille da un miliardo di euro ogni anno.

E mai e poi mai, ci dite, passerà il matrimonio gay, e ci confermate il no alle unioni civili, il no alle persone lesbiche e gay, che non devono essere discriminate! Ma mai, mai, mai, devono essere riconosciuti  loro diritti, tutele, garanzie! Perché ciò sarebbe addirittura incostituzionale!

Insomma, Signori Ministri, Signore e Signori Sottosegretari (molto Sottosegretari), umili servi delle vigne del Vaticano di destra, centro, sinistra, sopra e sotto, organizzatori, promotori e partecipanti al family day, tutti colpiti e trafitti dal dogma della verità, della purezza dell'integralismo religioso e della invasa pulizia interiore che vi rende molto, ma molto, angelici e buoni, voi che siete sempre pronti a strombettare le vostre  lezioni di moralismo e pseudo-perbenismo, perché state zitti? Perché non ci date la vostra dose quotidiana di pensiero, le vostre indicazioni su quanto sta accadendo? Perché questo assordante silenzio in queste drammatiche ore di smarrimento? Fateci ingerire la pillola quotidiana del vostro pensiero puro, indefesso e salvifico!

Mica vorrete far credere al popolo che le vostre idee, le vostre convinzioni, la vostra coerenza, il vostro credo, siano tutte una messa in scena? Non sarà così? Nevvero? Non c'è nessuna ipocrisia nelle vostre idee, nevvero? Allora dateci un cenno di riscontro, fate sentire la vostra voce, urlate la vostra rabbia, fateci sentire le trombe del vostro sconcerto, manifestate la vostra disperazione, rendeteci partecipi del vostro smarrimento, in un momento così difficile. Ma non state zitti, orsù!   Come dice un vecchio saggio, "sennò vien da pensar male, e in tal caso si fa peccato, anche se spesso  si indovina".