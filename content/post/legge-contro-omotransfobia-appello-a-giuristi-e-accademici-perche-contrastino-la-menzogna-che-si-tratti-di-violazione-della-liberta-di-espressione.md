---
title: 'Legge contro omotransfobia: appello a giuristi e accademici perché contrastino la menzogna che si tratti di violazione della libertà di espressione'
date: Thu, 18 Jul 2013 21:28:57 +0000
draft: false
tags: [Politica]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti

Roma, 19 luglio 2013

**[GLI EMENDAMENTI CHE PROPONE L'ASSOCIAZIONE RADICALE CERTI DIRITTI](notizie/comunicati-stampa/item/download/41_4940b191244af303ce3bf32c1e4b9e7e)**

Assistiamo, con un misto di preoccupazione e disincanto, al dibattito presso la Commissione Giustizia della Camera dei Deputati sul testo unitario in materia di lotta alla omofobia e transfobia a firma Scalfarotto (PD) e Lepri (PDL). Il testo necessita di emendamenti per fugare ogni ombra in merito alla estensione della Legge Mancino anche ai reati ed alle discriminazioni commessi per motivi legati all'orientamento sessuale, all'identità o all'espressione di genere della vittima. Ma appare chiaramente come la presentazione di centinaia di emendamenti abbia un altro scopo, quello di fare ostruzionismo.

Il destino di questa legge è anche legato agli equilibri di questa maggioranza, perché è noto a tutti che una maggioranza per approvare il testo base, in teoria, ci sarebbe, ma sarebbe costituita da PD, SEL, M5S, quindi non dall'attuale maggioranza di governo. Ma queste vicende dovrebbero essere in secondo piano rispetto al nodo del problema.

L'Associazione radicale Certi Diritti ha chiesto, dal primo momento, che si procedesse ad una riforma organica della materia, garantendo all'Italia una legge contro i crimini d'odio ed i discorsi d'odio secondo le numerose indicazioni degli organismi internazionali e delle leggi più avanzate in Europa. Agendo sulla leva penale (senza aggravare più di tanto le pene) ma soprattutto su quella della prevenzione.

Così non è stato. E si continuerà a lavorare in Italia su questi temi senza coordinare gli interventi e solo sulla base della buona volontà delle istituzioni e sulla loro capacità di ritagliarsi risorse sempre più scarse.

Ma se grazie alla poderosa macchina lobbystica in atto passasse pure la menzogna che l'estensione della legge Mancino ai crimini basati su orientamento sessuale, identità ed espressione di genere fosse lesiva della libertà di espressione, e questo producesse l'approvazione di emendamenti conseguenti, il danno sarebbe molto più evidente, e l'efficacia di una norma già parziale, potrebbe essere nulla.

Invitiamo i giuristi italiani a prendere pubblicamente posizione contro queste che sono menzogne belle e buone. La fonte di tutto questo è, e non può che essere, l'omofobia, ovvero la libertà di insulto e di pregiudizio, non la libertà di pensiero. Se no per essere coerenti bisognerebbe chiedere la cancellazione della norma per tutti.

Invitiamo le associazioni e le forze politiche italiane a prendere posizione, e fare campagna per una legge che interessa anche loro, perché stiamo parlando di diritti umani. E sarebbe grave se a difendere una legge utile ci fossero solo le persone lgbt e le loro associazioni.

Nei giorni passati l'Associazioen radicale ceti diritti ha proposto ai relatori della proposta unitaria alcuni emendamenti al testo che riassumendo chiedono:

\- l'inserimento della definizione di "espressione di genere", che completare tutte le potenziali aree di persone che sono vittime di discriminazioni e violenze e che non sono ricompresse tra quelle definite dai concetti di "orientamento sessuale" e "identità di genere"

\- il ripristino della multa, e l'aumento delle stesse

\- l'estensione "esplicita" di tutta la legge Mancino anche ai casi commessi a causa dell'orientamento sessuale, identità o espressione di genere;

\- il lancio di un Piano nazionale per il contrasto e la prevenzione dei crimini e dei discorsi d'odio, finanziato con 20 milioni di euro.

  
[Emendamenti\_Certi\_Diritti\_pdl\_omofobia.pdf](http://www.certidiritti.org/wp-content/uploads/2013/07/Emendamenti_Certi_Diritti_pdl_omofobia.pdf)