---
title: 'Newsletter 24 Marzo 2012'
date: Sun, 25 Mar 2012 09:52:14 +0000
draft: false
tags: [Politica]
---

![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)

E' uscito in libreria il 21 marzo il nostro nuovo libro dal titolo ['Certi Diritti che le coppie conviventi non sanno di avere'](certi-diritti-che-le-coppie-conviventi-non-sanno-di-avere). Concepito come un manuale di sopravvivenza, il volume indica gli strumenti legalmente utilizzabili per vedere riconosciuti, almeno in parte, i diritti di chi convive e non vuole o **non può** ancora sposarsi. [Puoi acquistare il libro anche online >](http://www.stampalternativa.it/libri/978-88-6222-281-5/bruno-de-filippis-/certi-diritti-che-le-coppie.html)

Ascolta su Radio Radicale [l’intervista a Bruno de Filippis sul libro di cui è co-autore >](http://www.radioradicale.it/scheda/348582)  
**'Certi Diritti che le coppie conviventi non sanno di avere'** esce proprio mentre in Italia si riapre il dibattito sui diritti delle coppie dello stesso sesso, grazie alla sentenza della Cassazione che ha riconosciuto il 'diritto alla vita famigliare' di una coppia che aveva chiesto la trascrizione del matrimonio celebrato all'estero (su Radio Radicale il commento alla sentenza dell'[avv. Massimo Clara](http://www.radioradicale.it/scheda/348262/sentenza-storica-della-cassazione-la-coppia-omosessuale-e-titolare-del-diritto-alla-vita-famigliare-come-q) e di [Sergio Rovasio](http://www.radioradicale.it/scheda/348267/sentenza-storica-della-cassazione-la-coppia-omosessuale-e-titolare-del-diritto-alla-vita-famigliare-come-q) entrambi del Direttivo di Certi Diritti).

Il ministro Fornero afferma: [“sono tradizionalista, ma è giusto dare diritti”](http://www.gay.it/channel/attualita/33313/Fornero-su-gay-io-tradizionalista-ma-e-giusto-dare-diritti.html) , [Vanity Fair lancia una petizione sulle unioni civili](grazie-vanity-fair-per-petizione-per-unioni-civili-il-ministro-fornero-tenga-conto-delle-iniziative-in-corso-a-roma-e-milano) e si parla di [prove di accordo bipartisan](http://ricerca.repubblica.it/repubblica/archivio/repubblica/2012/03/17/piu-diritti-agli-omosessuali-prove-di-accordo.html) per riconoscere diritti agli omosess uali. Ma **i politici continuano ad ignorare volutamente la sentenza 138/10 della Corte costituzionale,** che ormai due anni fa ha sollecitato il Parlamento a varare una disciplina di carattere generale in materia. [E c'è chi come Gasparri e Giovanardi non perde occasione di fare propaganda politica.](coppie-gay-gasparri-e-giovanardi-con-lettera-a-napolitano-su-sentenza-cassazione-fanno-propaganda-politica-e-ignorano-volutamente-sentenza-della-consulta) 

Il **matrimonio tra persone dello stesso sesso e la riforma del diritto di famiglia**restano gli obiettivi dell'associazione radicale Certi Diritti e anche quello dei parlamentari radicali.  
**[Ecco tutte le proposte radicali in Parlamento >](diritto-di-famiglia-matrimonio-unioni-civili-e-adozioni-tutte-le-proposte-radicali-in-parlamento)**

Il 23 marzo il segretario Yuri Guaiana introduce a Milano l'incontro-dibattito [“Quale riforma per il diritto di famiglia e la giustizia famigliare?”](http://www.divorziobreve.org/node/1470) e continua la presentazione nelle città italiane del libro **'Dal cuore delle coppie al cuore del diritto. L'udienza alla Corte costituzionale per il diritto al matrimonio tra persone dello stesso sesso’**. Dopo Berlino, Vicenza ([video >](http://www.youtube.com/watch?v=VpQXm_lJfbw)), Torino, Milano ([audio >](http://www.radioradicale.it/scheda/348793/presentazione-del-libro-di-yuri-guaiana-dal-cuore-delle-coppie-al-cuore-del-d%20iritto)), ad aprile sarà la volta di Grosseto e Bassano Del Grappa. 

Si è conclusa l’iniziativa **‘Adotta Piccolo Uovo’**: abbiamo raccolto 387,90 € donati da 22 cittadini, compreso il contributo di 120€ raccolto dall'associazione studentesca BESt – Bocconi Equal Students, e abbiamo regalato 4 libri a tutte le biblioteche rionali di Milano. [Il video >](lassociazione-radicale-certi-diritti-porta-piccolo-uovo-in-regalo-a-tutte-le-24-biblioteche-rionali-di-milano)

**L'Italia continua però ad essere il paese dell'omofobia istituzionalizzata.**Romano La Russa (PDL): ["I gay sono malati ma possono essere curati"](http://www.youtube.com/watch?v=UUiC55mz9Og&feature=player_embedded) . La Parlamentare radicale Farina Coscioni considera  gravissime e offensive le sue dichiarazioni e [presenta una interrogazione parlamentare](http://www.radicali.it/comunicati/20120321/gay-farina-coscioni-gravissime-offensive-dichiarazioni-del-consigliere-alla-regi). E [dall’omofobia non si salva neppure Tabacci](http://www.tempi.it/tabacci-no-alle-nozze-e-alle-adozioni-gay-video-di-tempiit), assessore al Bilancio del comune di Milano. Rispondi firmando le proposte di legge di iniziativa popolare per [l’istituzione del registro delle unioni civili e per le pari opportunità a Milano](http://metticilafirma.it/), insieme a quella per la [regolamentazione della prostituzione](http://notizie.radicali.it/articolo/2012-03-15/editoriale/regolamentare-la-prostituzione-la-ragionevole-proposta-radicale). Anche [chi vive Roma può firmare per la proposta di delibera sulle unioni civili](http://teniamofamiglia.blogspot.com/) e presto potrà farlo anche chi vive a Pescara.

Certi Diritti aderisce all'iniziativa **['Alziamo le mani ma per ballare'](certi-diritti-aderisce-alliniziativa-alziamo-le-mani-ma-per-ballare)** in programma domenica 25 marzo davanti alla discoteca Just In di Germignaga-Luino (VA), dove sabato 17 marzo sono stati pestati sette ragazzi gay. [Intervenga la SILB >](ancora-una-volta-violenza-contro-persone-omosessuali-solo-perche-sono-omosessuali-intervenga-la-silb-e-subito)

**[Certi diritti aderisce alla seconda marcia di Pasqua per l'amnistia, la legalità, la libertà >](certi-diritti-aderisce-alla-seconda-marcia-per-lamnistia-la-giustizia-la-liberta)**

Inoltre:  
[Dal mutuo alle visite in ospedale così per lo Stato i gay sono invisibili >](http://www.repubblica.it/cronaca/2012/03/16/news/gay_diritti_negati-31619505/?ref=HREC2-3)

Sono cento le coppie "diverse" iscritte nel registro comunale di Torino**: **[Enzo Cucco Presidente di Certi Diritti](http://www3.lastampa.it/torino/sezioni/cronaca/articolo/lstp/447230/)fa il punto della situazione**.**

[Dal Montenegro uno slancio verso l’uguaglianza dei diritti >](dal-montenegro-uno-slancio-verso-leguaglianza-nei-balcani)

[Uganda: la crociata di Bahati contro gli omosessuali. Nuovo disegno di legge](http://www.gaynews.it/articoli/Mondo/87864/Uganda-la-crociata-di-Bahati-contro-gli-omosessuali-Nuovo-disegno-di-legge.html)** . **[Firma la petizione >](http://www.isavelives.be/fr/node/8779)

**Leggendo questa lunga email (scusaci) puoi conoscere tutto ciò che fa l’associazione radicale Certi Diritti. Tutto questo è possibile solo grazie a chi ha deciso di rinnovare la sua iscrizione o versare un contributo alla nostra associazione.**   
**[Se non lo hai ancora fatto, puoi iscriverti ora >](iscriviti)**

Grazie per quello che potrai fare  
[www.certidiritti.it](http://www.certidiritti.it/)