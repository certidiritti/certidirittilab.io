---
title: 'INTERROGAZIONE DEPUTATI RADICALI SU SUICIDIO TRANSESSUALE'
date: Tue, 29 Dec 2009 07:48:21 +0000
draft: false
tags: [Comunicati stampa]
---

**TRANSESSUALE SUICIDA GIORNO DI NATALE NEL CIE DI MILANO: AVEVA DENUNCIATO IL SUO SFRUTTATORE. ERA CONTINUAMENTE MINACCIATA. IN ITALIA PER QUESTO SI E’ ESPULSI!**

**Interrogazione parlamentare dei deputati radicali – pd INTERROGAZIONE A RISPOSTA SCRITTA Al Ministro dell’Interno**

INTERROGAZIONE A RISPOSTA SCRITTA Al Ministro dell’Interno

Per sapere - premesso che:

secondo quanto riportato dalle agenzie di stampa, una transessuale brasiliana, Santos Da Costa, nome d’arte Leona, di anni 24, si è impiccata nel Centro di Identificazione ed Espulsione di Via Corelli (Milano) nel primo pomeriggio del giorno di Natale;

Leona aveva denunciato, alcuni mesi fa, il suo sfruttatore ai poliziotti, facendolo arrestare dagli agenti senza però ottenere alcun permesso di soggiorno per motivi di giustizia, sicché da allora viveva costantemente minacciata dal fratello dello sfruttatore, il quale le aveva promesso la morte non appena la stessa avesse fatto ritorno in Brasile;

secondo quanto riferito da alcune prostitute brasiliane, Leona avrebbe ricevuto l’ultima minaccia di morte sul telefonino il 20 dicembre u.s., proprio pochi istanti dopo essere stata condotta nel CIE di Via Corelli;

sembra che la presenza di Leona in Italia non fosse ritenuta strettamente necessaria ai fini dell’indagine, sostenuta da altre numerose prove;

un'altra transessuale brasiliana trattenuta in Via Corelli ha dichiarato che “l’idea di tornare in Brasile sconvolgeva Leona. Era giovane, era la prima volta che veniva arrestata. Era terrorizzata dall’idea di venire ammazzata di botte da quello che la sta aspettando in Brasile. Prima di ammazzarsi ha chiesto alle guardie un tranquillante, ma non glielo hanno dato. Alcuni piangono. Sembrano terrorizzati. Dicono tutti di aver visto il corpo di Leona morta impiccata al termosifone della cella, il cappio fatto con un copriletto di cotone. Dicono pure di aver visto le guardie scherzare davanti al suo cadavere, senza alcun rispetto”;

la morte di Leona è il secondo suicidio avvenuto in un Centro di Identificazione ed Espulsione dall’inizio dell’anno e un’altra morte sospetta è stata registrata nel CIE di Roma;

sulla vicenda è intervenuto anche l’Osservatorio sulle morti in carcere (sostenuto da Radicali Italiani, associazione “Il detenuto Ignoto”; “Antigone”; “A Buon Diritto”, “RadioCarcere” e “Ristretti Orizzonti” che lancia l’allarme sui decessi nei CIE, dove, si ribadisce, non si è formalmente detenuti senza però godere delle garanzie vigenti nelle carceri”:-

se il Ministro sia a conoscenza dei fatti descritti in premessa;

per quali motivi non sia stato concesso a Santos da Costa il permesso di soggiorno per motivi di giustizia;

per quali motivi la transessuale Leona sia stata espulsa dall’Italia nonostante la stessa fosse continuamente minacciata di morte dal fratello del suo sfruttatore che lei stessa aveva denunciato e fatto arrestare;

se intenda avviare una indagine amministrativa interna al fine di appurare se nei confronti della transessuale morta suicida nel CIE di Via Corelli siano state messe in atto tutte le misure di sorveglianza previste e necessarie e quindi se non vi siano responsabilità di omessa vigilanza da parte del personale del Centro;

quali iniziative, più in generale, il Governo intenda assumere per contenere e ridurre i decessi, i suicidi e gli atti di autolesionismo che annualmente si consumano all’interno dei Centri di Identificazione ed Espulsione.

**I deputati radicali - Pd:**

**Rita Bernardini, Marco Beltrandi, Maria Antonietta Farina Coscioni, Matteo Mecacci, Maurizio Turco, Elisabetta Zamparutti**