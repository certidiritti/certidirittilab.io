---
title: 'Europride Roma 2011: tutti gli eventi con Certi Diritti'
date: Sat, 28 May 2011 12:40:17 +0000
draft: false
tags: [Europa]
---

**EUROPRIDE ROMA 2011**

**1 - 12 GIUGNO,** **TUTTI GLI EVENTI CON** **LA** **PARTECIPAZIONE DELL’ASSOCIAZIONE RADICALE CERTI DIRITTI**

**Lunedì 30 maggio**:

**Ore 12:** EUROPRIDE ROMA 2011. Conferenza Stampa del Comitato Europride, presentazione degli eventi, del Pride Park e della parata dell’11 giugno. A cura del Comitato Europride 2011 - Roma, Hotel Es Radisson (Via Filippo Turati, 171)

**Giovedì 2 giugno**:

**Ore 18 – 19,30:** Conferenza pre-torneo Yellow LGBITE su Transessualismo e sport. Con Porpora Marcasciano – Mit e Sergio Rovasio, Segretario Associazione Certi Diritti. Presso la sede dell’Arcigay di Roma. Via di San Giovanni in Laterano n.10.

**Lunedì 6 giugno**:

**Ore 18,30:**  FUTURO GAY - Raccontiamoci la comunità LGBTIQ che verrà. A cura del Coordinamento Roma Pride in collaborazione con la Casa Internazionale delle Donne e la partecipazione del Roma Rainbow Choir. Presso la Casa Internazionale delle Donne, Via della Lungara, 19 – Roma.

**Martedì 7 giugno**:

**Ore 11:** probabile Sit-in/Maratona oratoria in Piazza Montecitorio – Roma, promossa dall’Associazione Radicale Certi Diritti insieme a Comitato EuroPrideRoma2011 e alle principali assoiciazioni lgbt(e) italiane.

**Ore 21**: QueerInAction, la cultura cinematografica a tematica LGBTQI nella capitale.

Nell’ambito della rassegna sul tema "Religioni e Omofobia" dibattito su 4 aree tematiche : religiosa, psicologica, sociologica e legislativa. C/O Pride Park, Piazza Vittorio, Roma.

**Mercoledì 8 giugno**

**Ore 11:** Ilga Europe incontro con con la Commissione Diritti Umani del Senato della Repubblica (da confermare).

**Ore 18:** HUMAN RIGHTS ARE MY PRIDE   La lunga marcia per i diritti LGBT in Europa. A cura della Sezione Italiana di Amnesty International e il Circolo Mario Mieli convegno.  Introduce e modera  Giusy D’Alconzo Direttrice Ufficio Campagne Amnesty International.

Interventi  di Rossana Praitano Presidente Mario Mieli; Vladimir Simonko Presidente Lithuanian Gay League (Lituania) su “La battaglia contro la propaganda omofoba in Lituania: leggi e limiti alla libertà di espressione”. Şevval Kilic  Attivista Istanbul LGBTT Solidarity Association (Turchia) su: “Le violazioni dei diritti umani delle persone transgender in Turchia: transfobia, discriminazioni e violenza della polizia”; Sergio Rovasio  Segretario Associazione Radicale Certi Diritti su: “La direttiva UE antidiscriminazione: ostacoli e prospettive” –  “Affermazione Civile 2: I ricorsi alla Corte Europea dei Diritti dell’Uomo”; Alberto Emiletti, Coordinamento LGBT della Sezione italiana di Amnesty International, Franko Dota, Attivista e coordinatore dello Zagreb Pride (Croazia)  Diritti Lgbt dopo dieci anni di Pride in Croazia.

Presso Pride Park – Piazza Vittorio, Roma

**Giovedì 9 giugno 2011**

**Ore 21**: Dinner 4° International Business Leader Forum inizia in meno di due settimana, con il Welcome Dinner giovedì 9 giugno alle 19.30 e la Conferenza venerdì 10 giugno alle 9.00.

**Venerdì 10 giugno**:

**Ore 9:** 4° International Business Leader Forum a cura dell’Egma – European Gay and Lesbian Managers Association. Es Hotel Radisson – Via Filippo Turati, 171 – Roma.  

**Ore 9,30 – 17:** "Europa 2020: crescita inclusiva e diritto all'uguaglianza. Orientamento sessuale e identità di genere: sindacato e contrattazione dei diritti per la parità". A cura di Cgil in collaborazione con Etuc (Confederazione Europea dei Sindacati) e Ilga. Presso Cgil Nazionale C/o Sala Sinti. Corso d’Italia, 25 Roma.

**Ore 15,30 – 18,30** Costruire (e difendere) l’Europa dei Diritti. I Diritti non sono un optional ma una parte fondamentale della strategia di sviluppo europea. E l’Italia? **Incontro promosso da Associazione radicale Certi Diritti presso la sede di Roma del Parlamento Europeo**.          Con il patrocinio di: Comitato organizzatore Europride Roma 2011 e Comitato organizzatore ILGA-Europe Torino 2011.

Saluto introduttivo: Sergio Rovasio, Segretario Associazione radicale Certi Diritti            Sessione introduttiva: Irene Tinagli: in che rapporto stanno sviluppo e lotta alle discriminazioni? Pier Virginio Dastoli: “Quale spazio possono avere le politiche antidiscriminatorie nella Strategia 2020 dell’Unione Europea?”; Marilisa D’Amico: “E’ possibile costruire una  strategia giudiziaria per il raggiungimento dei diritti laddove sono negati?”; Ottavio Marzocchi: “Quanto è libera la circolazione delle coppie e delle famiglie in Europa?”. A seguire Tavola rotonda: Silvan Agius, ILGA-Europe; Luciano Scagliotti, ENAR (European Network against racism); Pietro Barbieri, FISH (membro del Disability European Forum); Una rappresentante di European Women’s Lobby. Conclusioni: Emma Bonino, Vice Presidente del Senato; Michael Cashman: Presidente dell’Intergruppo lgbt del Parlamento europeo. Co-chair: Valeria Manieri (Associazione Pari o Dispare); Enzo Cucco (Associazione radicale Certi Diritti).

Presso la sede del Parlamento Europeo, Via IV Novembre 119 – Roma.

**Ore 20,30** Riunione del Direttivo dell’Associazione Radicale Certi Diritti. Via di Torre Argentina, 76 – Roma.

**Sabato 11 giugno:**

**ore 15** Parata dell’Europride Roma 2011. Carro delle Associazioni Certi Diritti, Luca Coscioni, Radicali Italiani, Nonviolent Radical Party, Nessuno Tocchi Caino, Non c’è Pace Senza Giustizia.