---
title: 'COMUNICATO DI CERTI DIRITTI E DEL COLLEGIO DI DIFESA SU MOTIVAZIONI SENTENZA'
date: Thu, 15 Apr 2010 14:46:01 +0000
draft: false
tags: [Comunicati stampa]
---

**MATRIMONIO GAY - CORTE COSTITUZIONALE: NELLE MOTIVAZIONI DELLA SENTENZA TRE IMPORTANTI PUNTI: NECESSARIO RICONOSCIMENTO DELLE UNIONI GAY; SI PUO’ LEGIFERARE SUL MATRIMONIO GAY;  SE LEGISLATORE NON INTERVIENE LA CORTE PUO’ INTERVENIRE PER  RICONOSCERE I DIRITTI DELLE COPPIE.**

****Comunicato Stampa dell’Associazione Radicale Certi Diritti. A seguire **dichiarazioni dei Professori Vittorio Angiolini, Marilisa D’Amico e dell’Avvocato Massimo Clara:******

L’Associazione Radicale Certi Diritti, che insieme ad Avvocatura lgbt Rete Lenford ha lanciato due anni fa la campagna di ‘Affermazione Civile’ per il riconoscimento del matrimonio gay, esprime il suo dispiacere per non aver raggiunto subito il risultato di ottenere dalla Corte costituzionale il pieno accoglimento delle questioni sollevate dai ricorsi delle coppie gay per vedersi riconosciuto il matrimonio. Al contempo esprimiamo profonda soddisfazione riguardo il riconoscimento del principio espresso dalla decisione del fondamento costituzionale delle unioni omosessuali. La Corte, infatti, ritiene che non sia possibile oggi estendere semplicemente l’istituto del matrimonio anche alle coppie omosessuali ma riconosce esplicitamente la loro rilevanza costituzionale, il loro diritto ad avere una normativa giuridica appropriata, ritenendo che sia compito del legislatore scegliere la disciplina più confacente.

**Di seguito sintesi dei tre punti rilevanti favorevoli alle coppie gay:**

1)  riconoscimento che l’unione omosessuale, come stabile convivenza, è una formazione sociale degna di garanzia costituzionale perché espressione del diritto fondamentale di vivere liberamente una condizione di coppia.

2) neppure il concetto di matrimonio è cristallizzato dall’Art. 29 della Costituzione e quindi non è precluso alla legge disciplinare il matrimonio tra gay, anche se restano possibili per il legislatore soluzioni diverse.

3) il legislatore deve intervenire e se non interviene la Corte potrà intervenire per ipotesi particolari, in cui sia necessario costituzionalmente un trattamento omogeneo tra la coppia coniugata e la coppia omosessuale.

**Dichiarazione del Prof. Vittorio Angiolini, Ordinario di Diritto Costituzionale alla Università Statale di Milano:** “Nelle motivazioni della sentenza della Corte costituzionale viene esplicitata la necessità di un riconoscimento costituzionale delle unioni omosessuali, resta al legislatore di provvedere senza discriminare le persone gay dalle persone eterosessuali”.

**Dichiarazione di Marilisa D’Amico, Ordinario di Diritto Costituzionale all’Università Statale di Milano e dell’Avvocato Massimo Clara:** “Questa è una prima tappa in cui la Corte indica la strada costituzionale per il riconoscimento della piena uguaglianza fra coppie omosessuali ed eterosessuali e se il Parlamento non intervenisse sarebbe evidente il vulnus costituzionale per il riconoscimento delle unioni tra coppie gay”.

Le motivazioni della sentenza sono al seguente link:

[http://www.certidiritti.it/tutte-le-notizie/685-le-motivazioni-della-sentenza-della-corte-costituzionale.html](tutte-le-notizie/685-le-motivazioni-della-sentenza-della-corte-costituzionale.html)