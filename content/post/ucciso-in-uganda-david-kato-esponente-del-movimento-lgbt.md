---
title: 'Ucciso in Uganda David Kato, esponente del movimento lgbt'
date: Thu, 27 Jan 2011 08:17:28 +0000
draft: false
tags: [certi diritti, Comunicati stampa, Kato David Kisule, lgbt, movimento, rolling stone, Uganda]
---

**UCCISO IN UGANDA DAVID KATO KISULE, ESPONENTE AFRICANO DEL MOVIMENTO LGBT. ERA STATO OSPITE A ROMA DEL CONGRESSO DI CERTI DIRITTI.**

**LA SUA FOTO ERA STATA PUBBLICATA, TRA LE ALTRE, DAL QUOTIDIANO UGANDESE ROLLING STONE NEL CORSO UNA CAMPAGNA PERSECUTORIA CONTRO LE PERSONE OMOSESSUALI.**

**ERA TRA LE PERSONALITA’  INVITATE AI LAVORI DEL 39° CONGRESSO DEL PARTITO RADICALE NONVIOLENTO, PREVISTI A CHIANCIANO DAL 17 AL 20 FEBBRAIO 2011.**

**Roma – Bruxelles, 27 gennaio 2011**

L’Associazione Radicale Certi Diritti e l'Ong radicale Non c'è Pace Senza Giustizia piangono la morte di David Kato Kisule. esponente di rilievo dell'organizzazione SMUG (Sexual Minorities Uganda), principale rappresentate del movimento gay ugandese.  Secondo le prime informazioni raccolte, David è stato ucciso ieri pomeriggio, presso la sua abitazione in circostanze poco chiare. La polizia di Kampala ha avviato delle indagini che sono ancora in corso.

Grazie all’impegno di Elio Polizzotto e dell’Ong Non c’è Pace Senza Giustizia, David era stato ospite lo scorso novembre ai lavori del IV Congresso dell’Associazione Radicale Certi Diritti, iscrivendosi, dove aveva raccontato delle persecuzioni  e di veri e propri linciaggi, di cui sono vittime le persone lesbiche e gay in Uganda, promosse da organizzazioni del fondamentalismo religioso.

Il 16 ottobre 2010 la rivista ugandese Rolling Stones pubblicò in prima pagina le foto di 100 attivisti omosessuali (o presunti tali) ugandesi chiedendone l’arresto. Tra le 100 foto vi era anche quella di David Kato Kisule, l’esponente più noto del movimento. Il clima di odio contro le persone omosessuali è alimentato dal fondamentalismo religioso dei predicatori evangelisti che trovano terreno molto fertile tra la popolazione che vive nella miseria e nella disperazione.

Molte Ong internazionali si erano mobilitate in diversi paesi del mondo contro questa barbarie. Il Parlamento Europeo, grazie alla campagna internazionale di Non c'è Pace Senza Giustizia, aveva approvato una Risoluzione di condanna nei confronti dell'Uganda. David era stato anche audito dalla Sottocommissione Diritti Umani del Parlamento Europeo dopo aver partecipato  a Roma al Congresso di Certi Diritti.

David, con molto coraggio e determinazione, aveva avviato una iniziativa legale contro la rivista Rolling Stones e lo scorso 7 gennaio l’Alta Corte ugandese aveva condannato la rivista per violazione della legge sulla privacy, difendendo le persone gay perseguitate. L’Alta Corte aveva dichiarato che nessuna delle persone la cui foto era stata pubblicata, aveva commesso reati, previsti dal codice penale ugandese per le persone omosessuali.

David Kato, nel suo soggiorno a Roma, aveva raccontato di come la situazione in Uganda fosse divenuta per gli attivisti omosessuali molto pericolosa. Durante le udienze in Tribunale era protetto da volontari delle Ong internazionali che seguivano il processo e difeso da diplomatici di ambasciate occidentali che lo avevano salvato da diversi tentativi di linciaggio perché riconosciuto dalla folla inferocita.

L’Associazione Radicale Certi Diritti, si unisce alla richiesta promossa da Smug (Sexual Minorities Uganda), Human Rights Watch, Global Rights, Global LGBT Advocacy, Npwj, e altre Ong, affinchè il Governo  si assuma le proprie responsabilita’ per non essere sino ad ora intervenuto per fermare la campagna di odio e di violenza contro la comunita LGBTI  e protegga gli attivisti in pericolo in Uganda adoperandosi quanto prima ad avviare un clima di tolleranza e di dialogo tra le autorità e le organizzazioni politiche e religiose.

Nei prossimi giorni l’Associazione Radicale Certi Diritti, promuoverà una iniziativa per ricordare e conoscere la figura del suo iscritto David Kato Kisule. Ricorderemo inoltre il suo impegno durante i lavori del XXXIX Congresso del Partito Radicale Nonviolento, transnazionale e transpartito che si svolgeranno a Chianciano dal 17 al 21 febbraio 2011 al quale David avrebbe dovuto partecipare.

info@certidiritti.it