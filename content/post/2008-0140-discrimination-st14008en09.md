---
title: '2008-0140-Discrimination-ST14008.EN09'
date: Wed, 21 Oct 2009 12:09:08 +0000
draft: false
tags: [Senza categoria]
---

  

  

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 21 October 2009

Interinstitutional File:

2008/0140 (CNS)

14008/09

LIMITE

SOC 566

JAI 644

MI 361

  

  

  

  

  

OUTCOME OF PROCEEDINGS

from :

The Working Party on Social Questions

No. prev. doc. :

13740/09 SOC 504 JAI 564 MI 326

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

-    Consolidated text

**I.         INTRODUCTION**

Delegations will find attached a consolidated text of the draft Directive, as revised during the course of the discussions in the Working Party on Social Questions, up to and including the meeting that took place on 22 September 2009[\[1\]](#_ftn1).

Delegations' general positions and the major outstanding issues are briefly summarised below, and further information is contained in the footnotes to the text.

**II.        DELEGATIONS' GENERAL POSITIONS**

A large majority of delegations (BE, BG, DK, EE, EL, ES, FR, IE, LU, HU, MT, NL, AT, PL, PT, SI, SK, FI, SE, UK) have welcomed the proposal in principle, many endorsing the fact that it aims to complete the existing legal framework by addressing all four grounds of discrimination through a horizontal approach.

Most delegations have affirmed the importance of promoting equal treatment as a shared social value within the EU. In particular, several delegations have underlined the significance of the proposal in the context of the UN Convention on the Rights of Persons with Disabilities[\[2\]](#_ftn2). However, some delegations (ES, AT, PT, SI) would have preferred more ambitious provisions in regard to disability.

While emphasising the importance of the fight against discrimination, DE has put forward the view that more experience with the implementation of existing Community law is needed before further legislation is adopted at the Community level. This delegation has questioned the timeliness of and the need for the proposal, which it sees as infringing on national competence for certain issues[\[3\]](#_ftn3).

CZ, LT, MT and IT, similarly, have expressed doubts regarding the need for the proposal, as they believe that it encroaches on national competences and conflicts with the principles of subsidiarity and proportionality.

EL has voiced concern in regard to the provisions on education; this delegation has requested that education be deleted from the scope of the proposed Directive.

NL has underlined the need for greater legal clarity, the Netherlands Parliament having requested clarification of the scope and the envisaged practical, legal and financial impact of the proposal. IT and LT have also seen a need for deeper impact assessments.

  

**III.      MAJOR OUSTANDING ISSUES AND STATE OF PLAY**

The Working Party has recognised the need for extensive further discussion, with a view to resolving the numerous outstanding questions, which include the following:

1) The scope, the division of competences and the issue of subsidiarity;

2) The financial and practical implications of the provisions, especially those concerning disability;

3) The implementation calendar; and

4) The need to ensure legal certainty in the Directive as a whole.

For the time being, all delegations have therefore maintained general scrutiny reservations on the proposal. CZ, DK, FR, MT and UK have maintained parliamentary scrutiny reservations, CY and PL maintaining linguistic scrutiny reservations. The Commission has meanwhile affirmed its original proposal at this stage and has maintained a scrutiny reservation on any changes thereto.

**IV.      CONCLUSION**

The European Parliament having adopted its Opinion under the Consultation Procedure on 2 April 2009[\[4\]](#_ftn4), the Working Party is continuing to examine the text. The Presidency has announced that at the next meeting, scheduled for 22 October 2009, the discussion will mainly focus on the disability provisions.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

Proposal for a

COUNCIL DIRECTIVE

on implementing the principle[\[5\]](#_ftn5) of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

THE COUNCIL OF THE EUROPEAN UNION,

Having regard to the Treaty establishing the European Community, and in particular Article 13(1) thereof,

Having regard to the proposal from the Commission[\[6\]](#_ftn6),

Having regard to the opinion of the European Parliament[\[7\]](#_ftn7),

Whereas:

  

(1)          In accordance with Article 6 of the Treaty on European Union, the European Union is founded on the principles of liberty, democracy, respect for human rights and fundamental freedoms, and the rule of law, principles which are common to all Member States and it respects fundamental rights, as guaranteed by the European Convention for the Protection of Human Rights and Fundamental Freedoms and as they result from the constitutional traditions common to the Member States, as general principles of Community law.

(2)          The right to equality before the law and protection against discrimination for all persons constitutes a universal right recognised by the Universal Declaration of Human Rights, the United Nations Convention on the Elimination of all forms of Discrimination Against Women, the International Convention on the Elimination of all forms of Racial Discrimination, the United Nations Covenants on Civil and Political Rights and on Economic, Social and Cultural Rights, the UN Convention on the Rights of Persons with Disabilities, the European Convention for the Protection of Human Rights and Fundamental Freedoms and the European Social Charter, to which \[all\] Member States are signatories. In particular, the UN Convention on the Rights of Persons with Disabilities includes the denial of reasonable accommodation in its definition of discrimination.

(3)          This Directive respects the fundamental rights and observes the fundamental principles recognised in particular by the Charter of Fundamental Rights of the European Union. Article 10 of the Charter recognises the right to freedom of thought, conscience and religion; Article 21 prohibits discrimination, including on grounds of religion or belief, disability, age or sexual orientation; and Article 26 acknowledges the right of persons with disabilities to benefit from measures designed to ensure their independence.

(4)          The European Years of Persons with Disabilities in 2003, of Equal Opportunities for All in 2007, and of Intercultural Dialogue in 2008 have highlighted the persistence of discrimination but also the benefits of diversity.

  

(5)          The European Council, in Brussels on 14 December 2007, invited Member States to strengthen efforts to prevent and combat discrimination inside and outside the labour market[\[8\]](#_ftn8).

(6)          The European Parliament has called for the extension of the protection of discrimination in European Union law[\[9\]](#_ftn9).

(7)          The European Commission has affirmed in its Communication ‘Renewed social agenda: Opportunities, access and solidarity in 21st century Europe’[\[10\]](#_ftn10) that, in societies where each individual is regarded as being of equal worth, no artificial barriers or discrimination of any kind should hold people back in exploiting these opportunities. Discrimination based on religion or belief, disability, age or sexual orientation may undermine the achievement of the objectives of the EC Treaty, in particular the attainment of a high level of employment and of social protection, the raising of the standard of living, and quality of life, economic and social cohesion and solidarity. It may also undermine the objective of abolishing of obstacles to the free movement of persons, goods and services between Member States.

(8)          The Community has adopted three legal instruments[\[11\]](#_ftn11) on the basis of article 13(1) of the EC Treaty to prevent and combat discrimination on grounds of sex, racial and ethnic origin, religion or belief, disability, age and sexual orientation. These instruments have demonstrated the value of legislation in the fight against discrimination_._ In particular, Directive 2000/78/EC establishes a general framework for equal treatment in employment and occupation on the grounds of religion or belief, disability, age and sexual orientation. However, variations remain between Member States on the degree and the form of protection from discrimination on these grounds beyond the areas of employment.

(9)     Therefore, legislation should prohibit discrimination based on religion or belief, disability, age or sexual orientation in a range of areas outside the labour market, including social protection, education and access to and supply of goods and services, including housing. Services should be taken to be those within the meaning of Article 50 of the EC Treaty.

  

(10)   Directive 2000/78/EC prohibits discrimination in access to vocational training; it is necessary to complete this protection by extending the prohibition of discrimination to education which is not considered vocational training.

(11)     Deleted.

(12)   Discrimination is understood to include direct and indirect discrimination, harassment, instructions to discriminate and denial of reasonable accommodation.

(12a) (new)  In accordance with the judgment of the Court of Justice in Case C-303/06[\[12\]](#_ftn12), it is appropriate to provide explicitly for protection from discrimination by association on all grounds[\[13\]](#_ftn13) covered by this Directive. Such discrimination occurs, _inter alia_, when a person is treated less favourably, or harassed, because, in the view of the discriminator, he or she is associated with persons of a particular religion or belief, disability, age or sexual orientation, for instance through his or her family, friendships, employment or occupation[\[14\]](#_ftn14)**.** Moreover, discrimination within the meaning of this Directive also includes direct discrimination or harassment based on assumptions[\[15\]](#_ftn15) about a person's religion or belief, disability, age or sexual orientation.

  

(12b)[\[16\]](#_ftn16) (new)   Harassment is contrary to the principle of equal treatment, since victims of harassment cannot enjoy[\[17\]](#_ftn17) access to social protection, education and goods and services on an equal basis with others[\[18\]](#_ftn18). Harassment can take different forms, including unwanted verbal, physical, or other non-verbal conduct. Such conduct may be deemed harassment in the meaning of this Directive when it is either repeated or otherwise so serious in nature that it has the purpose or effect of violating the dignity of a person and of creating an intimidating, hostile, degrading, humiliating or offensive environment. In this context, the mere expression of a personal opinion or the display of religious symbols or messages[\[19\]](#_ftn19) are presumed as not constituting harassment[\[20\]](#_ftn20).

(13)   In implementing the principle of equal treatment irrespective of religion or belief, disability, age or sexual orientation, the Community should, in accordance with Article 3(2) of the EC Treaty, aim to eliminate inequalities, and to promote equality between men and women, especially since women are often the victims of multiple discrimination.

(14)   The appreciation of the facts from which it may be presumed that there has been direct or indirect discrimination should remain a matter for the national judicial or other competent bodies in accordance with rules of national law or practice. Such rules may provide, in particular, for indirect discrimination to be established by any means including on the basis of statistical evidence.

(14a) Differences in treatment in connection with age may be permitted under certain circumstances if they are objectively justified by a legitimate aim and the means of achieving that aim are appropriate and necessary. Such differences of treatment may include, for example, special age conditions regarding access to certain goods or services such as alcoholic drinks, arms, or driving licences.

  

(15)[\[21\]](#_ftn21) Actuarial and risk factors related to disability[\[22\]](#_ftn22) and to age are used in the provision of insurance, banking and other financial services[\[23\]](#_ftn23). These should not be regarded as constituting discrimination where service providers have shown, by relevant actuarial principles, accurate statistical data or medical knowledge[\[24\]](#_ftn24), that  such factors are determining factors for the assessment of risk.

(16)     Deleted.

(17)   While prohibiting discrimination, it is important to respect other fundamental rights and freedoms, including the protection of private and family life and transactions carried out in that context, the freedom of religion, the freedom of association, the freedom of expression and the freedom of the press[\[25\]](#_ftn25).

  

(17b)[\[26\]](#_ftn26) (new)   This Directive covers access to social protection[\[27\]](#_ftn27), which includes social security, social assistance, and health care, thereby providing comprehensive protection against discrimination in this field. Consequently, the Directive applies with regard to access to rights and benefits which are derived from general or special social security,  social assistance and healthcare schemes, which are provided either directly by the State, or by private parties in so far as the provision of those benefits by the latter is funded by the State[\[28\]](#_ftn28). In this context, the Directive applies with regard to benefits  in cash, benefits in kind and services, irrespective of whether the schemes involved are contributory or non-contributory. The abovementioned schemes include, for example, access to the branches of social security defined by Regulation 883/2004/EC on the coordination of social security systems[\[29\]](#_ftn29), as well as schemes providing for benefits or services granted for reasons related to the lack of financial resources or risk of social exclusion.

(17c) Deleted.

(17d) (new)     All individuals enjoy the freedom to contract, including the freedom to choose a contractual partner for a transaction. This Directive should not apply to economic transactions undertaken by individuals for whom these transactions do not constitute a professional or commercial activity. In this context, the concept of professional or commercial activity may be defined in accordance with the national laws and practice of the Member States.

  

(17e) [\[30\]](#_ftn30) (new)  This directive does not alter the division of competences between the European Community and the Member States in the areas of education and social protection, including social security, social assistance and health care. It is also without prejudice to the essential role and wide discretion of the Member States in providing, commissioning and organising services of general economic interest.

(17f) (new)      The exclusive competence of Member States with regard to the organisation of their social protection systems includes decisions on the setting up, financing and management of such systems and related institutions as well as on the substance and delivery  of benefits and health services and the conditions of eligibility. In particular Member States retain the possibility to reserve certain benefits or services to certain age groups or persons with disabilities. Moreover, this Directive is without prejudice to the powers of the Member States to organise their social protection systems in such a way as to guarantee their sustainability[\[31\]](#_ftn31).

(17g) (new)     The exclusive competence of Member States with regard to the content of teaching or activities and the organisation of national educational systems, including the provision of special needs education,  includes the setting up and management of educational institutions, the development of curricula and other educational activities and the definition of examination processes. In particular Member States retain the possibility to set age limits for certain education activities. However, there may be no discrimination in the access to educational activities, including the admission to and participation in classes or programmes and the evaluation of students' performance.

(17h) (new)     This Directive does not apply to matters covered by family law[\[32\]](#_ftn32) including marital status and adoption, and laws on reproductive rights[\[33\]](#_ftn33). It is also without prejudice to the secular nature of the State, state institutions or bodies, or education.

(18)     Deleted.

  

(19a)[\[34\]](#_ftn34) Persons with disabilities include those who have long‑term physical, mental, intellectual or sensory impairments which, in interaction with various barriers, may hinder their full and effective participation in society on an equal basis with others.

(19b)[\[35\]](#_ftn35) (new)   Measures to enable persons with disabilities to have access, on an equal basis with others, to the areas covered by this Directive play an important part in ensuring full equality in practice. Such measures shall include appropriate modifications or adjustments and the identification and elimination of obstacles and barriers to accessibility. An example of modifications and adjustments which may amount to a disproportionate burden could be where significant structural changes are required to buildings or infrastructure which are protected under national rules on account of their historical, cultural or architectural value. In addition, individual measures of reasonable accommodation may be required in some cases to ensure access. In neither case are measures required that would impose a disproportionate burden. In assessing whether the burden is disproportionate, account should be taken of a number of factors including the size, resources and nature of the organisation. The principles of reasonable accommodation and disproportionate burden are established in Directive 2000/78/EC and the UN Convention on Rights of Persons with Disabilities.

  

(19c)[\[36\]](#_ftn36) (new)   Measures to ensure access to persons with disabilities, on an equal basis with others, should not impose a disproportionate burden, nor require fundamental alteration[\[37\]](#_ftn37). An alteration should be considered fundamental if it would change the nature of the social protection, social advantage, education, good, or service to such an extent that a different kind of social protection, social advantage, education, good or service was effectively provided or that access for persons without a disability was reduced.

(19d) (new)     Improvement of access can be provided by a variety of means, including application of the "universal design" principle. “Universal design” means the design of products, environments, programmes and services to be usable by all people, to the greatest possible extent, without the need for adaptation or specialised design. “Universal design” should not exclude[\[38\]](#_ftn38) assistive devices for particular groups of persons with disabilities where this is needed.[\[39\]](#_ftn39)

(20)   Legal requirements[\[40\]](#_ftn40) and standards on accessibility have been established at European level in some areas while Article 16 of Council Regulation 1083/2006 of 11 July 2006 on the European Regional Development Fund, the European Social Fund and the Cohesion Fund and repealing Regulation (EC) No 1260/1999[\[41\]](#_ftn41) requires that accessibility for disabled persons is one of the criteria to be observed in defining operations co-financed by the Funds. The Council has also emphasised the need for measures to secure the accessibility of cultural infrastructure and cultural activities for people with disabilities[\[42\]](#_ftn42).

  

(21)   The prohibition of discrimination should be without prejudice to the maintenance or adoption by Member States of measures intended to prevent or compensate for disadvantages suffered by a group of persons of a particular religion or belief, disability, age or sexual orientation. Such measures may permit organisations of persons of a particular religion or belief, disability, age or sexual orientation where their main object is the promotion of the special needs of those persons.

(22)   This Directive lays down minimum requirements, thus giving the Member States the option of introducing or maintaining more favourable provisions. The implementation of this Directive should not serve to justify any regression in relation to the situation which already prevails in each Member State.

(23)   Persons who have been subject to discrimination based on religion or belief, disability, age or sexual orientation should have adequate means of legal protection. To provide a more effective level of protection, associations, organisations and other legal entities should be empowered to engage in proceedings, including on behalf of or in support of any victim, without prejudice to national rules of procedure concerning representation and defence before the courts.

(24)   The rules on the burden of proof must be adapted when there is a prima facie case of discrimination and, for the principle of equal treatment to be applied effectively, the burden of proof must shift back to the respondent when evidence of such discrimination is brought. However, it is not for the respondent to prove that the plaintiff adheres to a particular religion or belief, has a particular disability, is of a particular age or has a particular sexual orientation.

(25)   The effective implementation of the principle of equal treatment requires adequate judicial protection against victimisation.

(26)   In its resolution on the Follow-up of the European Year of Equal Opportunities for All (2007), the Council called for the full association of civil society, including organisations representing people at risk of discrimination, the social partners and stakeholders in the design of policies and programmes aimed at preventing discrimination and promoting equality and equal opportunities, both at European and national levels.

  

(27)   Experience in applying Directives 2000/43/EC and 2004/113/EC show that protection from discrimination on the grounds covered by this Directive would be strengthened by the existence of a body or bodies in each Member State, with competence to analyse the problems involved, to study possible solutions and to provide concrete assistance for the victims.

(28)   Deleted.

(29)   Member States should provide for effective, proportionate and dissuasive sanctions in case of breaches of the obligations under this Directive.

(30)   In accordance with the principles of subsidiarity and proportionality as set out in Article 5 of the EC Treaty, the objective of this Directive, namely ensuring a common level of protection against discrimination in all the Member States, cannot be sufficiently achieved by the Member States and can therefore, by reason of the scale and impact of the proposed action, be better achieved by the Community. This Directive does not go beyond what is necessary in order to achieve those objectives.

(31)   In accordance with paragraph 34 of the interinstitutional agreement on better law-making, Member States are encouraged to draw up, for themselves and in the interest of the Community, their own tables, which will, as far as possible, illustrate the correlation between the Directive and the transposition measures and to make them public.

Article 1  
Purpose[\[43\]](#_ftn43)

This Directive lays down a framework for combating discrimination on the grounds of religion or belief, disability, age, or sexual orientation, with a view to putting into effect in the Member States the principle[\[44\]](#_ftn44) of equal treatment within the scope of Article 3.

Article 2[\[45\]](#_ftn45)  
Concept of discrimination

1.       For the purposes of this Directive, the “principle[\[46\]](#_ftn46) of equal treatment” shall mean that  there shall be no direct or indirect discrimination on any of the grounds referred to in Article 1.

2.       For the purposes of paragraph 1, the following definitions apply[\[47\]](#_ftn47):

(a)     direct discrimination shall be taken to occur where one person is treated less favourably than another is, has been or would be treated in a comparable situation, on any of the grounds referred to in Article 1;

(b)[\[48\]](#_ftn48) indirect discrimination shall be taken to occur where an apparently neutral provision, criterion or practice would put persons of a particular religion or belief, a particular disability, a particular age, or a particular sexual orientation at a particular disadvantage compared with other persons, unless that provision, criterion or practice is objectively justified by a legitimate aim and the means of achieving that aim are appropriate and necessary.

  

3.[\[49\]](#_ftn49) Harassment shall be deemed to be a form of discrimination within the meaning of paragraph 1, when unwanted[\[50\]](#_ftn50) conduct related to any of the grounds referred to in Article 1[\[51\]](#_ftn51) takes place with the purpose or effect of violating the dignity of a person and of creating an intimidating, hostile, degrading, humiliating or offensive environment. In this context, the concept of harassment may be defined in accordance with the national laws and practice of the Member States[\[52\]](#_ftn52).

3a.[\[53\]](#_ftn53) Discrimination includes direct discrimination or harassment due to a person's association[\[54\]](#_ftn54) with persons of a certain religion or belief, persons with disabilities, persons of a given age or of a certain sexual orientation; or based on assumptions[\[55\]](#_ftn55) about a person's religion or belief, disability, age or sexual orientation.

4.       An instruction to discriminate against persons on any of the grounds referred to in Article 1 shall be deemed to be discrimination within the meaning of paragraph 1.

5.       Denial of reasonable accommodation in a particular case as provided for by Article 4 (1)(a) of the present Directive as regards persons with disabilities shall be deemed to be discrimination within the meaning of paragraph 1.

  

6.[\[56\]](#_ftn56) Notwithstanding paragraph 2, differences of treatment on grounds of age shall not constitute discrimination, if they are  objectively[\[57\]](#_ftn57) justified by a legitimate aim, and if the means of achieving that aim are appropriate and necessary. In this context Member States may specify[\[58\]](#_ftn58) aims which can be considered to be legitimate.

Such differences of treatment may include the fixing of a specific age[\[59\]](#_ftn59) for access to social protection, including social security, social assistance and healthcare; education; and certain goods or services which are available to the public.

6a.     (new) Notwithstanding paragraph 2, differences of treatment of persons with a disability[\[60\]](#_ftn60) shall not constitute discrimination, if they are aimed at protecting their health and safety and if the means of achieving that aim are appropriate and necessary.

  

7.[\[61\]](#_ftn61) Notwithstanding paragraph 2, in the provision of financial services[\[62\]](#_ftn62), proportionate differences in treatment where, for the service in question, the use of age or disability is a determining factor in the assessment of risk based on relevant actuarial principles, accurate statistical data or  medical knowledge[\[63\]](#_ftn63) shall not be considered discrimination for the purposes of this Directive[\[64\]](#_ftn64).

8.       This Directive shall be without prejudice to measures laid down in national law[\[65\]](#_ftn65) which, in a democratic society, are necessary for public security, for the maintenance of public order and the prevention of criminal offences, for the protection of health and the protection of the rights and freedoms of others.

Article 3[\[66\]](#_ftn66)  
Scope

1.[\[67\]](#_ftn67) Within the limits of the powers conferred upon the Community, the prohibition of discrimination shall apply to all persons, as regards both the public and private sectors, including public bodies, in relation to access to[\[68\]](#_ftn68):

(a)          Social protection, including social security, social assistance and healthcare;

(b)          \[Deleted.\][\[69\]](#_ftn69)

(c)          Education;

(d)     and the supply of, goods and other services which are available to the public, including housing.

Subparagraph (d) shall apply to natural persons only[\[70\]](#_ftn70) insofar as they are performing a professional or commercial activity defined  in accordance with national laws and practice.

2.       This Directive does not alter the division of competences between the European Community and the Member States. In particular[\[71\]](#_ftn71) it does not apply to:

  

(a)     matters covered by family law[\[72\]](#_ftn72), including marital status and adoption, and laws on reproductive rights[\[73\]](#_ftn73);

(b)     the organisation of  Member States' social protection    systems, including decisions on the setting up, financing and management of such systems and related institutions as well as on the substance and delivery of benefits and services and the conditions of eligibility[\[74\]](#_ftn74);

(c)     the powers[\[75\]](#_ftn75) of Member States to determine the type of health services provided and the conditions of eligibility; and

(d)     the content[\[76\]](#_ftn76) of teaching or activities and the organisation of Member States' educational systems, including the provision of special needs education.

3.       Member States may provide that differences of treatment  based on religion or belief in respect of admission[\[77\]](#_ftn77) to educational institutions, the ethos of which is based on religion or belief, in accordance with national laws, traditions and practice, shall not constitute discrimination.

  

3a.     This Directive is without prejudice to national measures authorising or prohibiting the wearing of religious symbols[\[78\]](#_ftn78).

4.[\[79\]](#_ftn79) This Directive is without prejudice to national legislation ensuring the secular nature of the State, State institutions or bodies, or education, or concerning the status and activities of churches and other organisations based on religion or belief.

5.       This Directive does not cover differences of treatment based on nationality and is without prejudice to provisions and conditions relating to the entry into and residence of third-country nationals and stateless persons in the territory of Member States, and to any treatment which arises from the legal status of the third-country nationals and stateless persons concerned.

**_Article 4_**[\[80\]](#_ftn80)

**_Equal treatment of persons with disabilities_**

1.[\[81\]](#_ftn81) In compliance with the principle of equal treatment[\[82\]](#_ftn82), Member States shall take the necessary and appropriate measures to ensure to persons with disabilities access[\[83\]](#_ftn83), on an equal basis with others, within the areas[\[84\]](#_ftn84) set out in Article 3.

Such measures shall include appropriate modifications[\[85\]](#_ftn85) or adjustments and the identification and elimination of obstacles and barriers[\[86\]](#_ftn86) to accessibility.

Such measures should not impose a disproportionate burden[\[87\]](#_ftn87), nor require fundamental alteration[\[88\]](#_ftn88) or the provision of alternatives.

  

2.       Without prejudice to  the obligation to ensure to persons with disabilities access, on an equal basis with others, reasonable accommodation[\[89\]](#_ftn89) shall be provided.[\[90\]](#_ftn90) For the purpose of this provision, “reasonable accommodation” means necessary and appropriate modifications and adjustments not imposing a disproportionate[\[91\]](#_ftn91) burden[\[92\]](#_ftn92), where needed in a particular case, to ensure that persons with disabilities can enjoy or exercise, on an equal basis with others**,** rights concerning social protection, including social security and health care, social advantages, education and access to goods and services[\[93\]](#_ftn93) in the meaning of Article 3, paragraph 1.

3.       For the purposes of assessing whether measures necessary to comply with paragraphs 1 and 2  would impose a disproportionate burden, account shall be taken, in particular[\[94\]](#_ftn94), of the size and resources of the organisation or enterprise[\[95\]](#_ftn95), its nature, the estimated cost, the life cycle of the goods[\[96\]](#_ftn96) and services, and the possible benefits of increased access for persons with disabilities. The burden shall not be deemed disproportionate when it is sufficiently remedied by measures existing within the framework of the equal treatment policy[\[97\]](#_ftn97) of the Member State concerned.

Paragraphs 1 and 2 shall not apply[\[98\]](#_ftn98) to the design and manufacture of goods.

  

4.[\[99\]](#_ftn99) In order to take account of particular conditions[\[100\]](#_ftn100), the Member States may, if necessary:

a)             have an additional period of \[X\] years from \[the deadline for transposition\] to comply with paragraphs 1 and 2**;**

b)             have an additional period of \[Y\] years \[from the deadline for transposition\] to implement the provisions set out in  paragraph 1[\[101\]](#_ftn101) progressively when adaptation of existing buildings[\[102\]](#_ftn102) or infrastructures[\[103\]](#_ftn103) is required.

Member States wishing to use the additional period laid down in paragraphs 4(a) and (b) shall inform the Commission accordingly and provide reasons by the deadline set in Article 15 \[date of transposition\].

5.[\[104\]](#_ftn104) This Directive shall be without prejudice to the provisions of Community law or national rules providing for detailed standards or specifications on the  accessibility of particular goods or services, including public transport[\[105\]](#_ftn105), as long as such rules do not restrict the application of paragraphs 1 and 2.

Article 5  
Positive action

1.  With a view to ensuring full equality in practice, the principle of equal treatment shall not prevent any Member State from maintaining or adopting specific measures to prevent or compensate for disadvantages linked to religion or belief, disability, age, or sexual orientation.

2**.**[\[106\]](#_ftn106)  (new)       In particular, the principle of equal treatment shall be without prejudice to the right of Member States to maintain or adopt more favourable[\[107\]](#_ftn107) provisions for persons of a given age or for persons with disabilities as regards conditions for access to social protection, including social security, social assistance and healthcare; education; and certain goods or services which are available to the public, in order to promote their economic, cultural or social integration.

Article 6  
Minimum requirements

1.       Member States may introduce or maintain provisions which are more favourable to the protection of the principle of equal treatment than those laid down in this Directive.

2.       The implementation of this Directive shall under no circumstances constitute grounds for a reduction in the level of protection against discrimination already afforded by Member States in the fields covered by this Directive.

CHAPTER II  
REMEDIES AND ENFORCEMENT

Article 7  
Defence of rights

1.       Member States shall ensure that judicial and/or administrative procedures, including where they deem it appropriate conciliation procedures, for the enforcement of obligations under this Directive are available to all persons who consider themselves wronged by failure to apply the principle of equal treatment to them, even after the relationship in which the discrimination is alleged to have occurred has ended.

2.       Member States shall ensure that associations, organisations or other legal entities, which have, in accordance with the criteria laid down by their national law, a legitimate interest in ensuring that the provisions of this Directive are complied with, may engage[\[108\]](#_ftn108), either on behalf or in support of the complainant, with his or her approval, in any judicial and/or administrative procedure provided for the enforcement of obligations under this Directive.

3.       Paragraphs 1 and 2 shall be without prejudice to national rules relating to time limits for bringing actions as regards the principle of equality of treatment.

Article 8  
Burden of proof

1.       Member States shall take such measures as are necessary, in accordance with their national judicial systems, to ensure that, when persons who consider themselves wronged because the principle of equal treatment has not been applied to them establish, before a court or other competent authority[\[109\]](#_ftn109), facts from which it may be presumed that there has been direct or indirect discrimination, it shall be for the respondent to prove[\[110\]](#_ftn110) that there has been no breach of the prohibition of discrimination.

2.       Paragraph 1 shall not prevent Member States from introducing rules of evidence which are more favourable to plaintiffs.

3.         Paragraph 1 shall not apply to criminal procedures.

4.         Member States need not apply paragraph 1 to proceedings in which the court or other competent body investigates the facts of the case.

5.       Paragraphs 1, 2, 3 and 4 shall also apply to any legal proceedings commenced in accordance with Article 7(2).

Article 9  
Victimisation

Member States shall introduce into their national legal systems such measures as are necessary to protect individuals from any adverse treatment or adverse consequence as a reaction to a complaint or to proceedings[\[111\]](#_ftn111) aimed at enforcing compliance with the principle of equal treatment

Article 10  
Dissemination of information

Member States shall ensure that the provisions adopted pursuant to this Directive, together with the relevant provisions already in force, are brought to the attention of the persons concerned by appropriate means throughout their territory.

Article 11  
Dialogue with relevant stakeholders

With a view to promoting the principle of equal treatment, Member States shall encourage dialogue with relevant stakeholders, which have, in accordance with their national law and practice, a legitimate interest in contributing to the fight against discrimination on the grounds and in the areas covered by this Directive.

Article 12  
Bodies for the Promotion of Equal treatment

1.       Member States shall designate a body or bodies for the promotion of equal treatment of all persons irrespective of their religion or belief, disability, age, or sexual orientation. These bodies may form part of agencies charged at national level with the defence of human rights or the safeguard of individuals' rights.

  

2.       Member States shall ensure that the competences of these bodies include:

**(a)** without prejudice to the right of victims and of associations, organizations or other legal entities referred to in Article 7(2), providing independent assistance to victims of discrimination in pursuing their complaints about discrimination,

**(b)** conducting independent surveys concerning discrimination, **and**

**(c)** publishing independent reports and making recommendations on any issue relating to such discrimination.

CHAPTER III  
FINAL PROVISIONS

Article 13  
Compliance

Member States shall take the necessary measures to ensure that the principle of equal treatment is respected within the scope of this Directive and in particular that:

(a)     any laws, regulations and administrative provisions contrary to the principle of equal treatment are abolished;

(b)     any contractual provisions, internal rules of undertakings, and rules governing profit-making or non-profit-making associations contrary to the principle of equal treatment are, or may be, declared null and void or are amended.

Article 14  
Sanctions[\[112\]](#_ftn112)

Member States shall lay down the rules on sanctions applicable to infringements of national provisions adopted pursuant to this Directive, and shall take all measures necessary to ensure that they are applied. Sanctions may comprise the payment of compensation, which may not be restricted by the fixing of a prior upper limit, and must be effective, proportionate and dissuasive.

Article 14a (new)[\[113\]](#_ftn113)  
Gender mainstreaming

In accordance with  the objective of  Article 3(2) of the EC Treaty, Member States shall, when implementing this Directive, take into account the objective of equality between men and women.

Article 15  
Implementation

1.       Member States shall adopt the laws, regulations and administrative provisions necessary to comply with this Directive by …. at the latest \[X years after adoption\][\[114\]](#_ftn114). They shall forthwith inform the Commission thereof and shall communicate to the Commission the text of those provisions.[\[115\]](#_ftn115)

When Member States adopt these measures, they shall contain a reference to this Directive or be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.

  

2.[\[116\]](#_ftn116) In order to take account of particular conditions, Member States may, if necessary, establish that the obligation to provide effective access as set out in Article 4 has to be complied with by … at the latest \[Y years after adoption\].

Member States wishing to use this additional period shall inform the Commission at the latest by the date set down in paragraph 1 giving reasons.

Article 16  
Report

1.       Member States shall communicate to the Commission, by …. at the latest and every five years thereafter, all the information necessary for the Commission to draw up a report to the European Parliament and the Council on the application of this Directive.

2.       The Commission's report shall take into account, as appropriate, the viewpoints of national equality bodies and relevant stakeholders, as well as the EU Fundamental Rights Agency. In accordance with the principle of gender mainstreaming, this report shall, _inter alia_, provide an assessment of the impact of the measures taken on women and men. In the light of the information received, this report shall include, if necessary, proposals to revise and update this Directive.

Article 17  
Entry into force

This Directive shall enter into force on the day of its publication in the Official Journal of the European Union.

Article 18  
Addressees

This Directive is addressed to the Member States.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) This document is based on docs. 13740/09, 13049/09 + COR 1, 12096/09 + COR 1, 10073/1/09 REV 1, 10072/09 + COR 1 and 16594/08 + ADD 1.

[\[2\]](#_ftnref2) See docs. 12892/2/08 REV 2, 12892/08 ADD 1 REV 1 and 8321/09.

[\[3\]](#_ftnref3) See also doc. DS 823/08.

[\[4\]](#_ftnref4) See doc. A6-0149/2009. Kathalijne Maria Buitenweg (Group of the Greens / European Free Alliance) served as Rapporteur.

[\[5\]](#_ftnref5) IE maintained a reservation. See Article 1.

[\[6\]](#_ftnref6) OJ C , , p. .

[\[7\]](#_ftnref7) OJ C , , p. .

[\[8\]](#_ftnref8) Presidency conclusions of the Brussels European Council of 14 December 2007, point 50.

[\[9\]](#_ftnref9) Resolution of 20 May 2008 P6_TA-PROV(2008)0212

[\[10\]](#_ftnref10) COM (2008) 412

[\[11\]](#_ftnref11) Directive 2000/43/EC, Directive 2000/78/EC and Directive 2004/113/EC

[\[12\]](#_ftnref12) Case C-303/06, Coleman v. Attridge, judgment of 17 July 2008, nyr.

[\[13\]](#_ftnref13) CZ, DK, DE, IT and NL questioned the expansion of the concept of discrimination by association to cover "all grounds". See also Article 2(3a).

[\[14\]](#_ftnref14) LV suggested deleting "for instance through his or her family, friendships, employment or occupation".

[\[15\]](#_ftnref15) EL, FR and IT suggested deleting the separate mention of discrimination based on assumptions; IT supported by EE also preferred defining discrimination based on association in relation to its seriousness, as was done for harassment. See also Article 2(3a) and doc. 13495/09. Cion was unable to accept this approach.

[\[16\]](#_ftnref16) DK and MT maintained scrutiny reservations.

[\[17\]](#_ftnref17) NL suggested moving "on an equal basis with others" here. Cion welcomed this suggestion.

[\[18\]](#_ftnref18) CZ supported by BG suggested deleting the second part of the sentence: "since… others."

[\[19\]](#_ftnref19) CZ, LT and AT asked for the meaning of the reference to "religious symbols or messages" to be clarified.

[\[20\]](#_ftnref20) EE, PT, SK and FI suggested deleting the last sentence. FR, similarly, entered a reservation on the last sentence. IE questioned the consistency between the last sentence and Article 2(3), whereby harassment is to be defined nationally. IT, for its part,  suggested moving the words to Article 2(3) (see doc. 13495/09).

[\[21\]](#_ftnref21) AT maintained a reservation. FI made a written suggestion (doc. 13496/09) and also suggested replacing "product in question" with "service in question" therein. MT suggested the following new wording: "…where **ensuing proportionate differences in treatment for the product in question occur provided that** the service providers in question have shown by relevant and accurate actuarial **and** statistical data…" SK supported this wording.

[\[22\]](#_ftnref22) CZ entered a scrutiny reservation on the inclusion of disability as a risk-assessment factor. BE and HU also expressed doubts, especially in the context of the UN Convention on the Rights of Persons with Disabilities. AT suggested replacing "disability" with "state of health". BE also suggested that insurance companies should disclose the ways in which they used age and disability to calculate risk.

[\[23\]](#_ftnref23) SK preferred "products".

[\[24\]](#_ftnref24) IT suggested replacing "medical knowledge" with "scientifically proved medical knowledge and evidence" (see doc. 13495). BG, supporting, suggested "…_or_ evidence". Cion pointed out that many insurers lacked statistics on disability.

[\[25\]](#_ftnref25) BG suggested deleting the reference to the freedom of the press in this context.

[\[26\]](#_ftnref26) All delegations maintain scrutiny reservations on Article 3 and Recital 17b.

[\[27\]](#_ftnref27) NL strongly urged the need to clarify the distinction between "access to" and "the content of" social protection.

[\[28\]](#_ftnref28) FI maintained a scrutiny reservation. This delegation preferred "statutory schemes of general or specific social security" (see doc. 13496/09). FR supported this approach. Agreeing that there was a need to improve the text, Cion pointed out that "statutory schemes" had a narrow meaning.

[\[29\]](#_ftnref29) OJ L 166, 30.4.2004, p. 1. FI expressed the view that the reference to Regulation 883/2004/EC did not improve clarity, as the branches of social security covered by the said Regulation did not fully correspond to the national social security classifications, which fell under the Member States' competence.

[\[30\]](#_ftnref30) See delegations' remarks concerning "access" in Article 3.

[\[31\]](#_ftnref31) BE suggested replacing the last two words with "for instance, their adequacy and sustainability".

[\[32\]](#_ftnref32) MT supported by IT and PL suggested adding "and public policy".

[\[33\]](#_ftnref33) PL suggested adding "and the benefits dependent thereon". PL maintained a reservation.

[\[34\]](#_ftnref34) FI suggested placing Recital 19a in the articles. AT entered a reservation on Recital 19a on the grounds that it weakened the text unduly.

[\[35\]](#_ftnref35) FI suggested placing Recital 19b in the articles. PT informed delegations that it was studying the question as to whether Recital 19b was compatible with the Universal Declaration of Human Rights as well as the UN Convention. IE and FR entered reservations on Recital 19b, pending more precise wording. AT also entered a reservation on the grounds that Recital 19b weakened the text unduly. LU, agreeing, pointed out that the issue of significant structural changes was already included in Article 4(3), and stated that the key distinction to be made was between buildings that were open to the public and those that were not.

[\[36\]](#_ftnref36) IE maintained a reservation.

[\[37\]](#_ftnref37) DE called for the provisions on "fundamental alteration" to be spelled out in the articles, so as to ensure legal certainty.

[\[38\]](#_ftnref38) UK suggested replacing "should not exclude assistive devices" with "should not prevent the design and manufacture of assistive devices for particular groups". The Chair pointed out that rewording the definition would mean that the reference to the UN Convention would need to be removed.

[\[39\]](#_ftnref39) Article 2 of the UN Convention on the Rights of Persons with Disabilities.

[\[40\]](#_ftnref40) Regulation (EC) No. 1107/2006 and Regulation (EC) No 1371/2007

[\[41\]](#_ftnref41) OJ L 210, 31.7.2006, p.25. Regulation as last amended by Regulation (EC) No 1989/2006 (OJ L 411, 30.12.2006, p.6).

[\[42\]](#_ftnref42) OJ C 134, 7.6.2003, p.7

[\[43\]](#_ftnref43) FR suggested specifying that the Directive was without prejudice to Directive 2000/78/EC.

[\[44\]](#_ftnref44) IE maintained a reservation on the notion of a general principle of equal treatment. Cion favoured the inclusion of the term in the text.

[\[45\]](#_ftnref45) LT, MT and UK maintained scrutiny reservations on Article 2.

[\[46\]](#_ftnref46) IE, DE and UK have questioned the inclusion of "the principle of equal treatment" here.

[\[47\]](#_ftnref47) IE suggested incorporating "discrimination by imputation" into the definition of discrimination (see doc. 13046/09).

[\[48\]](#_ftnref48) IE maintained a reservation on the definition of "indirect discrimination" on the grounds that a _potential_ disadvantage should not be viewed as constituting indirect discrimination. See also ES note in doc. DS 1215/1/08 REV 1.

[\[49\]](#_ftnref49) DK and MT entered scrutiny reservations. BG, BE, LV, LT and AT also had certain doubts about the inclusion of "harassment". (See DK note supported by PL in doc. 12893/09.)

[\[50\]](#_ftnref50) ES and PT suggested deleting "unwanted".

[\[51\]](#_ftnref51) CZ, DK, DE, IT and NL questioned the expansion of the concept of discrimination by association to cover "all grounds". (See also Recital 12a.) DE, EL and IT maintained reservations. UK suggested replacing "any of the grounds referred to in Article 1" with "disability or age". See doc. 13636/09. Cion was unable to accept this approach.  BE, AT and UK also underlined the importance of ensuring that the provisions did not unduly interfere with the exercise of freedom of speech. MT suggested adding a reference to "national laws and practice".

[\[52\]](#_ftnref52) PT maintained a reservation on the grounds that this wording would bring legal uncertainty.

[\[53\]](#_ftnref53) LV and NL asked for "association" and "assumptions" to be clarified. ES supported by DK and LV suggested clarifying the text by means of separate recitals for these two concepts. BG entered a reservation on Article 2(3a). DE, AT and MT entered scrutiny reservations. (See also earlier suggestion by IE in doc. 13046/09.)

[\[54\]](#_ftnref54) IT supported by EE suggested defining discrimination based on association in relation to its seriousness, as was done for harassment (see doc. 13495/09).

[\[55\]](#_ftnref55) UK and Cion felt that discrimination based on assumptions was covered by the phrase "on the grounds of", AT also supported a simple approach. EL, FR and IT similarly suggested removing any separate mention of discrimination "based on assumptions" and MT also expressed concern in respect of the potential extension of the scope. DK called for further justification of the inclusion of discrimination "based on assumptions".

[\[56\]](#_ftnref56) BG, BE, CZ, DK, FR, IE, ES, LT, NL, AT and UK variously affirmed the competences of the Member States for setting age limits. BE, DK and FR supported the written suggestion by NL (see doc. 13651/09), DK also suggesting the deletion of "presumed to be". Cion also found some merit in the NL suggestion. AT, however, supported the earlier written suggestion by DK (12893/09). UK suggested exempting persons under eighteen (doc. 13636/09). NL, HU, RO and UK also stressed the non-legislative nature of many age limits. Cion entered a reservation on any blanket exclusion of persons under eighteen.  IE, having earlier also tabled a written suggestion for exempting minors (doc. 13046/09), asked whether current Irish legislation permitting differences of treatment of persons under eighteen in respect of access to goods and services would conflict with the draft Directive; Cion undertook to study this question. FI supported expressed the view that minors should not be excluded from the Directive outright. Concurring with this view, CZ and SK affirmed the general justification of exemptions already contained in Article 2(6). FI undertook to table examples of discrimination affecting minors (see also doc. 13496/09).     

[\[57\]](#_ftnref57) CZ and FI preferred "objectively _and reasonably_".

[\[58\]](#_ftnref58) Responding to IE, AT, RO and UK, Cion explained that the draft Directive contained no obligation for the Member States to spell out the "legitimate aims". IE also saw a need to address legitimate differences of treatment in contexts such as sport and drama (see doc. 13046/09).

[\[59\]](#_ftnref59) PL suggested referring to "specific rules" rather than "a specific age". (See also doc. DS 1173/08.)

[\[60\]](#_ftnref60) CZ, IE, FR, FI, UK have expressed hesitation in respect of the disability exemption and the way it is formulated (see note from IE in doc. 13046/09). AT and BE have called for all the disability provisions to be grouped in Article 4.

[\[61\]](#_ftnref61) AT maintained a reservation on Article 2(7). BE, CZ, NL, IT and UK maintained scrutiny reservations on Article 2(7). See also note from IE (doc. 13740/09). FR affirmed the drafting suggestions contained in doc. 16594/08 ADD 1.

[\[62\]](#_ftnref62) LT suggested "_insurance, banking and other_ financial services" (the wording used in Recital 15). AT suggested aligning the wording on Article 5 of Directive 2004/113/EC. SK preferred "products" to "services". CY suggested adding "the Member States may allow". UK maintained a scrutiny reservation.

[\[63\]](#_ftnref63) IT suggested replacing "medical knowledge" with "scientifically proved medical knowledge and evidence" (see doc. 13495/09). BG, supporting, suggested "…_or_ evidence". Cion pointed out that many insurers lacked statistics on disability. BE, IT and NL have also underlined the importance of publishing risk assessment data; BE has suggested placing responsibilities on insurance companies to disclose the ways in which they used age and disability to calculate risk (e.g. annual reporting).

[\[64\]](#_ftnref64) CY suggested ending the sentence with "and shall not consider such differences as discrimination".

[\[65\]](#_ftnref65) IE suggested adding "and practice"; MT suggested adding "and public policy". NL asked for the meaning of the term "national law" to be clarified.

[\[66\]](#_ftnref66) All delegations maintain scrutiny reservations on Article 3 and Recital 17b. DE has expressed particular concern in respect of the financial implications arising from the scope. EL has asked for "education" to be deleted from the scope. MT maintained a reservation on Article 3.

[\[67\]](#_ftnref67) EL maintained its reservation in respect of the inclusion of education in the scope of the draft Directive. DE and IE have also expressed doubts in respect of the inclusion of education. ES and FR maintained scrutiny reservations on Article 3(1).

[\[68\]](#_ftnref68) Many delegations (including BE, CZ, DK, DE, EL, IE, IT, LV, LT, MT, PL, NL, AT, SK, FI and UK) have seen a need to clarify the meaning of the term "access to", which was added with a view to clarifying the scope of the Directive (see also doc. 14896/08, p. 29). IT and UK maintained reservations.

[\[69\]](#_ftnref69) FR and FI questioned the deletion of "social advantages", FR alluding to certain concepts that were not covered by the concept of "social assistance". CZ asked whether "social advantages" constituted a sub-category of "social protection" or "social assistance".

[\[70\]](#_ftnref70) SE suggested deleting "only".

[\[71\]](#_ftnref71) MT suggested replacing "in particular" with "_inter alia_".

[\[72\]](#_ftnref72) MT reiterated its call, supported by IT and PL, for "public policy" to be mentioned alongside "family law" as an area of national competence in Article 3(2)(a). MT asked for clarification in regard to the removal of the term "family status", which appeared alongside "marital status" in the Commission's original proposal.

[\[73\]](#_ftnref73) PL suggested adding "and the benefits dependent thereon". PL maintained a reservation. See doc. 813/08.

[\[74\]](#_ftnref74) BE, CZ and DK saw a need to clarify the difference between "access" and "conditions of eligibility".

[\[75\]](#_ftnref75) Responding to RO, Cion explained that no change in meaning was implied by the use of the term "powers" as a synonym for "competences" in Article 3(2)(c).

[\[76\]](#_ftnref76) NL called for clarification of the link between this provision and Recital 7g.

[\[77\]](#_ftnref77) AT made the point that refusing a child "_admission_" to an educational institution on any of the protected grounds would still be discrimination. UK also expressed hesitation, as the provision could wrongly give the impression that faith-based schools were being given a licence to discriminate. UK entered a scrutiny reservation.

[\[78\]](#_ftnref78) IE wished to see "goods and services produced for a religious purpose" exempted in Article 3(3a). See doc. 13049/09, p. 14.

[\[79\]](#_ftnref79) CZ, IE and MT shared the view that Article 3(4) was redundant on the grounds that the European Community in any case had no competences in the matter of national legislation ensuring the secular nature of the state. MT maintained a scrutiny reservation.

[\[80\]](#_ftnref80) All delegations maintain scrutiny reservations on Article 4. Several delegations (DE, ES, FR, IE, LU, MT, PT, RO, UK) called for the obligations stemming from Article 4 to be spelled out more clearly, so as to avoid legal uncertainty, BG, DE, IE, MT, SE and UK and others raising concerns in regard to the potential costs involved, particularly in respect of housing. Certain delegations also warned of the cost implications for SMEs (MT, IT, PT). NL reiterated its concern in respect of the legal and financial implications of the proposal.

[\[81\]](#_ftnref81) DE and AT requested that definitions for the terms used in Article 4(1) be provided.

[\[82\]](#_ftnref82) DE raised the question as to whether this reference to the principle of equal treatment was useful. See also Article 1.

[\[83\]](#_ftnref83) DE and MT called for the term "access" to be clarified (see Article 4(3)).

[\[84\]](#_ftnref84) Several delegations (BG, DE, IE, MT, SE, UK) requested clarification of the provisions on housing, UK reiterating its question as to whether, under the suggested provisions, individual landlords would be obliged to ensure that all their properties were accessible to persons with disabilities. DE requested clarification of the practical meaning of the provisions concerning transport and raised the question as to whether traffic safety and the interests of users other than persons with disabilities could be taken into consideration when defining effective and non-discriminatory access.

[\[85\]](#_ftnref85) ES and RO suggested clarifying the term "appropriate modification". See also Recital 19b.

[\[86\]](#_ftnref86) FR, PL and RO saw a need to clarify the reference to eliminating "obstacles and barriers".

[\[87\]](#_ftnref87) ES, IE and MT asked for the term "disproportionate burden" to be clarified.

[\[88\]](#_ftnref88) ES, IT and RO asked for the term "fundamental alteration" to be clarified; AT suggested deleting "nor require fundamental alteration…" FR reaffirmed the wording suggested under the French Presidency ("…nor require fundamental alteration of the nature of the social protection…"). See also Recital 19c.

[\[89\]](#_ftnref89) See DK suggestion in doc. 12893/09.

[\[90\]](#_ftnref90) UK reiterated its call, supported by BG, DK, IT, MT and SE, for this provision to be elucidated in a recital.

[\[91\]](#_ftnref91) The word "undue" is deleted.

[\[92\]](#_ftnref92) UK suggested adding "not requiring a fundamental alteration".

[\[93\]](#_ftnref93) SK suggested adding "available to the public, including housing".

[\[94\]](#_ftnref94) CZ asked whether safety and health considerations could also be taken into account when assessing whether measures would impose a disproportionate burden.

[\[95\]](#_ftnref95) DE suggested that, in the interest of greater legal certainty, SMEs of a particular size (e.g. those with 20 or fewer employees) be exempted.

[\[96\]](#_ftnref96) EE and RO asked for the meaning of this provision to be clarified. NL and SE also called for the scope to be delineated as precisely as possible. Cion affirmed the inclusion of "the design and manufacture of goods" in the scope and explained that the aim was to provide access to persons with the most common forms of disability.

[\[97\]](#_ftnref97) IE suggested "disability policy".

[\[98\]](#_ftnref98) AT was unable to accept this restriction to the scope. Giving a preliminary view, NL suggested that product redesign be included _provided that it did not constitute a disproportionate burden_. EE, NL, RO and FI also pointed out that this exclusion appeared peculiar in the light of the reference earlier in the paragraph to "the life cycle of the goods"; in addition, FI recalled the reference to "universal design" in Recital 19d.

[\[99\]](#_ftnref99) RO suggested moving the provisions concerning the implementation calendar to Article 15. PT and LT maintained scrutiny reservations on the implementation dates; see also Article 15.

[\[100\]](#_ftnref100) DK and FI asked for the term "particular conditions" to be clarified.

[\[101\]](#_ftnref101) SI made the observation that this cross-reference blurred the distinction between immediate rights and rights which do not require immediate action, including those involving significant structural changes to buildings and infrastructures.

[\[102\]](#_ftnref102) DE and LV underlined the Member States' competences for deciding how to improve access to existing buildings of infrastructures. CZ, LV and MT suggested that adaptation be required only for new buildings or when a building was renovated, or when its use was changed. UK also endorsed this approach. MT maintained a scrutiny reservation. LV entered a reservation linked to the implementation calendar. BG and PL also stressed the need for sufficiently long implementation periods.

[\[103\]](#_ftnref103) Responding to FI, the Chair explained that "infrastructures" did not include vehicles.

[\[104\]](#_ftnref104) MT, NL and UK asked for the provisions in Article 4(5) to be clarified, UK raising the issue of whether the Directive applied to modes of transport not covered by national or Community legislation.

[\[105\]](#_ftnref105) DE reiterated its view that traffic safety issues and the interests of users other than persons with disabilities, including financial considerations, should be taken into consideration, and that rail and air transport should be excluded from the scope, detailed Community rules on accessibility having recently been adopted. DE and MT maintained scrutiny reservations.

[\[106\]](#_ftnref106) FI reiterated its concerns in respect of the deletion of "social advantages".

[\[107\]](#_ftnref107) UK raised the question as to whether this provision on more favourable treatment was in conflict with the provision concerning _less_ favourable treatment in Article 2(6), given that treatment that was more favourable to a particular person was automatically less favourable to someone else.

[\[108\]](#_ftnref108) IE entered a reservation on Article 7. This delegation suggested adding here: "as the Member States so determine and in accordance with the criteria laid down by their national law". DE, EL, IT and AT, similarly, favoured retaining a reference to the criteria laid down by national law (cf. Directives 2000/43/EC and 2000/78/EC).

[\[109\]](#_ftnref109) Responding to BG, Cion explained that no difference of meaning was intended between "competent authority" and "competent body" (see Article 8(4)).

[\[110\]](#_ftnref110) CZ, DE, IE, IT, LU, NL expressed concern in respect of the reversal of the burden of proof in the context of the draft Directive. IT maintained a scrutiny reservation. Cion explained and affirmed the inclusion of this provision.

[\[111\]](#_ftnref111) IE pointed out that "legal proceedings" were referred to in Directives 2000/78/EC and 2004/113/EC.

[\[112\]](#_ftnref112) The Chair suggested "penalties" instead of "sanctions".

[\[113\]](#_ftnref113) CZ entered a reservation on the inclusion of a gender mainstreaming article. BG, IE, NL and UK have also asked for clarification.

[\[114\]](#_ftnref114) BG, ES, EL, IT, LV, MT, PL, RO, FI, SE and UK underlined the need for a sufficiently long transposition period. Cion acknowledged the need for a longer transposition period (e.g. four years).

[\[115\]](#_ftnref115) Cion entered a reservation on the deletion of the provision obliging the Member States to send the Commission tables illustrating the correlation between the provisions of the Directive and the instruments adopted nationally for its implementation.

[\[116\]](#_ftnref116) EL, LV, MT, NL, SE and UK underlined the need for a sufficiently long transposition period. LT, PL and PL entered scrutiny reservations on the implementation calendar for the disability provisions (Article 15(2)), pending finalisation of Article 4. LV and AT favoured an incremental approach (progressive realisation) instead of the single deadline currently provided for in Article 15(2).