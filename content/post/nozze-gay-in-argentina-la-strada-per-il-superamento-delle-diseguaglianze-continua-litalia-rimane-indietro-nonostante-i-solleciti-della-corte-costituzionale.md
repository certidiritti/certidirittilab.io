---
title: 'NOZZE GAY IN ARGENTINA: LA STRADA PER IL SUPERAMENTO DELLE DISEGUAGLIANZE CONTINUA. L’ITALIA RIMANE INDIETRO NONOSTANTE I SOLLECITI DELLA CORTE COSTITUZIONALE'
date: Thu, 15 Jul 2010 14:04:45 +0000
draft: false
tags: [Comunicati stampa]
---

Dichiarazione di **Sergio Rovasio**, Segretario dell’Associazione Radicale Certi Diritti

_"_Mentre anche l’Argentina approva in via definitiva l’accesso all’istituto del matrimonio per le coppie omosessuali, sul piano dei diritti civili e umani l’Italia rimane alla finestra".

"La Corte Costituzionale italiana lo scorso aprile aveva respinto il ricorso di alcune coppie gay per vedersi riconosciuto il matrimonio ma aveva dato tre precise indicazioni anche alla classe politica riguardo il riconoscimento  dell’unione omosessuale: 1) come stabile convivenza la coppia gay è una formazione sociale degna di garanzia costituzionale perché espressione del diritto fondamentale di vivere liberamente una condizione di coppia; 2)  neppure il concetto di matrimonio è cristallizzato dall’Art. 29 della Costituzione e quindi non è precluso alla legge disciplinare il matrimonio tra gay, anche se restano possibili per il legislatore soluzioni diverse.  Infine (punto 3) la Corte aveva sostenuto che  “il legislatore deve intervenire e che,  in caso contrario, la stessa Corte potrà esprimersi per ipotesi particolari, in cui sia necessario costituzionalmente un trattamento omogeneo tra la coppia coniugata e la coppia omosessuale.

L’Italia dovrà prima o poi adeguarsi e ci auguriamo che il Parlamento si muova presto, così come hanno fatto molti paesi anche cattolicissimi, senza che accadesse al loro interno alcuna tragedia nonostante tutti gli allarmismi ispirati dal fondamentalismo religioso dai vertici ecclesiastici.

Anche su questo tema ci sono diverse proposte di legge depositate, anche di parlamentari radicali, che aspettano di essere calendarizzate”.