---
title: 'Terribile suicidarsi perché si è omosessuali. Si devono attivare iniziative per la prevenzione dei suicidi, ma anche delle violenze e dei maltrattamenti delle persone LGBT'
date: Mon, 12 Aug 2013 11:50:12 +0000
draft: false
tags: [Politica]
---

Comunicato stampa dell'Associazione radicale certi diritti

Roma, 12 agosto 2013

Anche nel caso dei suicidi, proprio come accade nel caso di violenze e maltrattamenti nei confronti delle persone lgbt, non ci si può arrendere di fronte alla realtà che viviamo. Le Istituzioni sono responsabili di qaunto non si fa per la prevenzione di questi fenomeni. Invocare l'approvazione della legge contro omofobia e transfobia (quella all'esame del parlamento non quella che chiedono le associazioni) pensando che in questo modo lo Stato intervenga in modo posotivo sulla questione è falso.

Gli interventi in materia penale non affrontano il problema della prevenzione e dell'assistenza delle vittime, e soprattutto non intervengono in quel retroterra sociale e culturale che crea gli ambienti ostili e discriminatori che generano violenza e discriminazione.

Solo servizi pubblici e privati, preparati e accoglienti, possono offrire aiuto ai ragazzi e alle ragazze adolescenti e alle loro famiglie in situazioni di difficoltà e sofferenza così acute da provocare tentativi di suicidio o di autoemarginazione.

[L'Associazione Radicale Certi Diritti ha proposto ai relatori della proposta di legge su omofobia e transfobia alcuni emendamenti, uno dei quali va nella direzione di un Piano nazionale per la prevenzioen di questi fenomeni.](notizie/comunicati-stampa/item/download/41_4940b191244af303ce3bf32c1e4b9e7e)

In Italia non mancano esempi di iniziative, sia di singole associazioni che di amministrazioni locali, anche molto importanti, ma manca un Piano nazionale, manca un intervento forte del Governo e dei Ministeri più direttamente coivnolti (Istruzione, Politiche Sociali, Sport e Associazionismo giovanile) per legittimare e moltiplicare questi interventi.

Il Governo è andato proprio in questa direzione nel propio decreto legge contro la violenza alle donne, prevedendo la realizzazione di un nuovo grande Piano nazionale per la prevenzione e il contrasto. La legge contro l'omofobia e la transfobia deve essere ancora approvata, siamo, anzi siete, ancora in tempo a inserire questa misura che, sola, va nella direzione di prevenire altri suicidi.