---
title: 'NO ALL''OMOFOBIA VATICANA: SABATO 6-12 FIACCOLATA SIT-IN IN P.ZA SAN PIETRO'
date: Wed, 03 Dec 2008 12:00:59 +0000
draft: false
tags: [Comunicati stampa, fiaccolata, OMOFOBIA, vaticano]
---

**depenalizzazione omosessualità all'Onu: sabato 6 dicembre, fiaccolata sit-in dei Radicali, arcigay, arcilesbica e altre associazioni, davanti p.zza San Pietro contro l'omofobia vaticana.**

  
  

Roma, 3 dicembre 2008

Sabato 6 dicembre, dalle ore 17, Radicali Italiani, l'Associazione Radicale Certi Diritti, l'Associazione Luca Coscioni e Nessuno Tocchi Caino promuovono, insieme ad Arcigay e Arcilesbica con altre associazioni che si battono per la tolleranza, la laicità, i diritti civili e umani, una fiaccolata sit-in davanti San Pietro, in Piazza Pio IX, contro la grave decisione del Vaticano di non sostegno alla proposta francese all'Onu di depenalizzazione dell'omosessualità. All’iniziativa nonviolenta partecipano dirigenti, parlamentari e militanti radicali insiemi ad esponenti del movimento lgbt.

A metà dicembre l'Assemblea Generale dell'Onu sarà chiamata a votare una dichiarazione politica promossa dal Governo francese e sostenuta da tutti i paesi dell'Unione Europea, che invita gli Stati membri a depenalizzare l'omosessualità laddove è considerata ancora reato. Difatti, in oltre 80 paesi del mondo l'omosessualità è considerato un crimine, in 9 di questi è prevista la pena di morte.

Il Vaticano, pur sostenendo a parole, che occorre avere rispetto per le persone omosessuali, ha deciso di allearsi con i paesi teocratici e dittatoriali che prevedono durissime condanne, torture e pena di morte per le persone gay. Molti paesi tra questi, in particolare quelli democratici, pur prevedendo l'omosessualità come reato non applicano la legge e hanno un atteggiamento di tolleranza verso gli omosessuali.

La decisione del Vaticano è gravissima perché alimenta l'omofobia e la violenza verso persone che non hanno colpe e che desiderano vivere la loro vita con rispetto, dignità e riconoscimento dei propri diritti.

Invitiamo quanti hanno a cuore la tolleranza, il rispetto dei diritti civili e umani ad aderire e partecipare a questo importante evento, inviando una e-mail a

[certidiritti@radicali.it](mailto:certidiritti@radicali.it).