---
title: 'ENNESIMO ATTO DI OMOFOBIA: CONTRO WWW.GAY.IT, LA NOSTRA SOLIDARIETA'''
date: Wed, 05 Aug 2009 10:46:32 +0000
draft: false
tags: [Comunicati stampa]
---

#### MINACCE DI MORTE AL SITO [WWW.GAY.IT](http://www.gay.it/):  SOLIDARIETA’ DELL’ASSOCIAZIONE RADICALE CERTI DIRITTI. CHIEDIAMO ALLE AUTORITA’ DI GARANTIRE L’INCOLUMITA’ E LA SICUREZZA DELLA COMUNITA’ LGBT E COMBATTERE CON DECISIONE I CASI DI OMOFOBIA.

#### DEPOSITATA SULLA VICENDA UN’INTERROGAZIONE PARLAMENTARE DEI DEPUTATI RADICALI, PRIMA FIRMATARIA RITA BERNARDINI.

#### Dichiarazione di Sergio Rovasio, Segretario Associaizone Radicale Certi Diritti:

#### 

“La grave vicenda delle minacce, anche di morte, avvenute la notte scorsa presso la sede del portale [www.gay.it](http://www.gay.it/) dimostra, se ancora ve ne fosse bisogno,  la situazione di tensione e violenza che sempre più colpisce  la comunità gay italiana. Esprimiamo tutta la nostra convinta solidarietà agli amici di [gay.it](http://gay.it/) e chiediamo alle autorità di garantire  l’incolumità dei redattori di uno dei più importanti network gay italiani, colpito non a caso dall’odio razzista e omofobico.

Ci auguriamo che il nostro Governo - che continua a far finta di niente sull’aumento dei casi di omofobia avvenuti in Italia nell’ultimo anno - intervenga contro questi ignobili attacchi di terrore e paura che avvengono in una sempre più crescente e preoccupante indifferenza”.

Di seguito il testo dell’interrogazione parlamentare depositata oggi dai deputati radicali, prima firmataria Rita Bernardini:

Al Ministro degli Interni, Al Ministro per le Pari Opportunità,

Per sapere – Premesso che:

- La notte tra il 4 e il 5 agosto, a Ospedaletto, zona artigianale di Pisa, davanti alla sede operativa del portale [www.gay.it](http://www.gay.it/) , uno dei più importanti network della comunità gay italiana, con una media di 700.000 contatti/mese, ignoti hanno imbrattato con scritte minacciose sui muri, tra le altre,  le seguenti scritte: “gli uffici bruceranno e voi morirete tutti oggi” e “froci a morte”;

- Presso la redazione di [www.gay.it](http://www.gay.it/) lavorano circa 15 persone, impegnate in un importante lavoro di diffusione di informazioni e di aggregazione per la comunità lgbt italiana;

- Tra le scadenze diffuse recentemente dal sito [gay.it](http://gay.it/) vi è anche quella dell’annuncio delle iniziative che si svolgeranno in occasione del ‘Mardi gras’ che inizieranno il 7 agosto a Torre del  Lago e coinvolgeranno molte migliaia di persone in uno degli eventi più attesi della comunità lgbt italiana:

Per sapere:

- Quali iniziative ha attivato il Governo per contrastare i casi di violenza omofobica, in forte aumento in tutta Italia nell’ultimo anno;

- In particolare quali interventi intende attivare il Ministro affinchè venga assicurata la massima sicurezza e incolumità operativa alle persone che lavorano presso il portale [www.gay.it](http://www.gay.it/) e in occasione degli eventi promossi per il ‘Mardi gras’ a Torre del Lago a partire dal prossimo 7 agosto 2009 ;

- Se il Minisitro per le Pari Opportunità alla luce di quanto esposto in premessa non ritenga di intervenire con specifiche campagne di informazione ed educazione volte a constratare il grave fenomeno dell’omofobia.

I deputati radicali:

Rita Bernardini

Marco Beltrandi

Maria Antonietta Farina Coscioni

Matteo Mecacci

Maurizio Turco

Elisabetta Zamparutti