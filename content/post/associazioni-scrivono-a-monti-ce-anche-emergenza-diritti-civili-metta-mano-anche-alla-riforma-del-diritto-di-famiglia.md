---
title: 'Associazioni scrivono a Monti: c''è anche emergenza diritti civili, metta mano anche alla Riforma del diritto di famiglia'
date: Sat, 19 Nov 2011 11:40:02 +0000
draft: false
tags: [Diritto di Famiglia]
---

Certi Diritti insieme ad altre associazioni scrivono al nuovo Presidente del Consiglio: metta mano anche alla Riforma del Diritto di Famiglia, è già pronta.

Roma, 19 novembre 2011

Comunicato Stampa dell'Associazione Radicale Certi Diritti:

Oggi è stata inviata al Presidente del Consiglio, Sen. Mario Monti, una lettera-appello, firmata da molte Associazioni, nella quale si chiede di considerare tra i temi dell' emergenza italiana anche il fronte dei diritti civili. La lettera, firmata da Associazione Radicale Certi Diritti, Associazione Luca Coscioni, Arcigay, Associazione Divorzio Breve, Equality Italia, Famiglie Arcobaleno, Associazione GayLib, Agedo, GayNet, Fondazione Massimo Consoli, Associazione ‘Nuova Proposta, donne e uomini omosessuali cristiani', Gaynews.it, chiede  di intervenire sul tema della Riforma del diritto di famiglia, proposta che ha visto coinvolte negli ultimi anni, per la sua preparazione, giuristi, esperti e associazioni italiane, poi  pubblicata nel libro ‘Amore Civile' e depositata in Parlamento come proposta di legge.

Allegati alla lettera sono stati inviati al Presidente del Consiglio il libro ‘Amore civile - dal Diritto della tradizione al Diritto della ragione' curato dai giuristi Bruno de Filippis e Francesco Bilotta e il testo della proposta di legge n. 3607 ‘Norme Modifiche al codice civile in materia di testamento biologico, di disciplina del diritto di famiglia e della fecondazione assistita, al codice penale in materia di omicidio del consenziente e di atti di violenza o di persecuzione psicologica all'interno della famiglia, nonché al codice di procedura civile in materia di disciplina della domanda di divorzio (presentata il 6 luglio 2010,  prima firmataria la deputata Radicale-Pd On. Rita Bernardini).

**Qui di seguito il testo integrale della lettera e il link al testo della proposta di legge n. 3607:**

Roma, 15 novembre 2011

**Al Presidente del Consiglio dei Ministri**

**On. Sen. Mario Monti**

**Palazzo Chigi**

**00186 Roma**

Signor Presidente del Consiglio,

 il Governo al quale lei sta lavorando avrà un compito immenso, e urgente, per il nostro Paese: riportare l'Italia agli standard sociali, culturali e normativi, non solo economici, comuni ai paesi dell'Unione Europea.

Come Lei ben saprà la Strategia 2020 adottata nel giugno 2010 dal Consiglio Europeo  ha fissato nell'interdipendenza tra politiche di sviluppo e politiche per l'inclusione sociale uno dei cardini delle strategie di sviluppo europeo, individuando tra gli interventi prioritari della Piattaforma contro l'emarginazione la lotta contro le discriminazioni. Operare contro ogni forma di discriminazione, quindi, non è solo obiettivo di giustizia e di uguaglianza, ma condizione per lo sviluppo.

            Tradurre questi principi in interventi normativi e politiche specifiche è un compito arduo per chi si accinge ad operare in un Paese, come il nostro, ove il velo dell'ideologia nasconde una realtà  in trasformazione al pari con il resto dell'Europa. In particolare riteniamo che negli ultimi anni il pregiudizio ideologico di una parte della nostra classe dirigente si sia accanito contro le famiglie, in particolare contro la responsabilità e la libertà su cui si sono costruite nuove forme di famiglia in Italia al pari che nel resto dell'Europa.

            Le segnaliamo quindi, un progetto integrato e completo di Riforma del Diritto di famiglia che comprende specifici progetti di intervento su:

-      **la regolamentazione delle coppie di fatto;**

-      **la separazione, la mediazione familiare;**

-      **il divorzio breve;**

-      **il matrimonio tra persone dello stesso sesso;**

-      **le norme sulla procreazione e sulla genitorialità responsabile;**

-      **la norme in materia di filiazione legittima e naturale;**

-      **nuove norme contro la violenza ed i maltrattamenti in famiglia**

Noi siamo convinti che l'Italia ha bisogno urgentemente di un intervento normativo, e crediamo che il Suo Governo possa e debba contribuire a colmare il divario tra noi e il resto dell'Europa su queste materie.

Le alleghiamo la pubblicazione ‘Amore Civile. Dal diritto della tradizione al diritto della ragione' che raccoglie l'insieme del progetto di Riforma del Diritto di Famiglia e l'adattamento dello stesso testo come proposta di legge (n. 3067, prima firmataria l'On. Rita Bernardini, "Modifiche al codice civile in materia di testamento biologico, di disciplina del diritto di famiglia e della fecondazione assistita, al codice penale in materia di omicidio del consenziente e di atti di violenza o di persecuzione psicologica all'interno della famiglia, nonché al codice di procedura civile in materia di disciplina della domanda di divorzio").

Siamo certi che La sua sensibilità e La sua attenzione ai temi dei diritti civili saranno considerati prioritari per il suo Governo.

Cordiali saluti,

**_Associazione Radicale Certi Diritti_**

**_Associazione Luca Coscioni_**

**_Associazione Divorzio Breve_**

**_Equality Italia_**

**_Arcigay_**

**_Famiglie Arcobaleno_**

**_Associazione GayLib_**

**_Agedo (Associazione dei genitori di persone omosessuali);_**

**_GayNet (Associazione giornalisti gay)_**

**_Fondazione Massimo Consoli_**

**_Associazione Nuova Proposta, donne e uomini omosessuali cristiani;_**

**_Gaynews.it (quotidiano di informazione sui sull'omosessualità);_**

Link al testo della pdl n. 3607:

[http://www.camera.it/126?pdl=3607&ns=2](http://www.camera.it/126?pdl=3607&ns=2)