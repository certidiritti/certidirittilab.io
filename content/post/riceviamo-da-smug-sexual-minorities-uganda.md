---
title: 'RICEVIAMO DA SMUG (SEXUAL MINORITIES UGANDA)'
date: Sat, 05 Feb 2011 11:09:43 +0000
draft: false
tags: [Comunicati stampa]
---

**Come i nostri amici dovrebbero  rispondere all'assassinio di David Kato**

**29 gennaio 2011**

_Cari amici e colleghi, David Kato è stato brutalmente assassinato mercoledì 26 gennaio 2011. David era il lobbista dell’associazione Sexual Minoritis Uganda (SMUG) e un attivista, di lunga data, per i diritti umani di lesbiche, gay, bisessuali e transgender (LGBT)._

_Il cordoglio e le offerte di sostegno della comunità internazionale sono state straordinarie. A nome della famiglia di David, colleghi e amici, vi ringraziamo._

_Comprendiamo che molti di voi sono pieni di tristezza e rabbia  e vorrebbero intraprendere azioni in nome di David. Tuttavia, crediamo che, prima di tutto, la società civile ugandese deve essere rispettata nel suo ruolo di leader e coordinatore di eventi e azioni che saranno portate avanti nelle prossime settimane e nei prossimi mesi. Crediamo, inoltre, che è fondamentale che noi ugandesi veniamo messi nelle condizioni di documentare la risposta nazionale e internazionale all’assassinio brutale di David, e questo richiede regolari comunicazioni con noi._

**Che azione intraprendere:**

·_Inviare lettere chiedendo al Governo dell’Uganda (informazioni sui contatti in fondo alla pagina) di intraprendere i seguenti passi:_

-_condannare pubblicamente l’assassinio di David;_

-_portare avanti una completa e chiara indagine sul’assassinio di David;_

-_perseguire il colpevole (o i colpevoli) nei termini e nel rispetto della legge;_

-_investigare sulle intrusioni dell’account email di David nei giorni precedenti la sua morte;_

-_presumere che, finché non verrà provato il contrario, che la morte di David è stata motivata da omofobia e non da violenza abituale o arbitraria;_

-_comunicare frequentemente con i leader LGBT per tutto il periodo delle indagini sulla morte di David;_

-_assicurare che i membri della comunità LGBT ugandese abbiano adeguata protezione dalla violenza;_

-_intraprendere tempestive azioni contro tutte le minacce o i discorsi di odio che incitano alla violenza;_

-_eliminare ogni possibilità di presa in esame o approvazione della legge anti omosessualità (Anti-Homosexuality Bill)._

·   _Contattare le proprie autorità governative e fare pressioni affinché comunichino le loro preoccupazioni alle autorità ugandesi in maniera diretta o privata._

·   _Continuare a mettere in luce e denunciare pubblicamente gli evangelici conservatori Statunitensi che diffondono omofobia in Uganda._

·   _Organizzare veglie, rispettose e nonviolente, di fronte alle ambasciate o ai consolati ugandesi del proprio Paese._

**Come agire:**

·   _Informare SMUG  di tutte le azioni intraprese per l’assassinio di David, in modo che possano essere monitorati tutti gli sviluppi. Mandare copia dei propri comunicati stampa, delle dichiarazioni, delle registrazioni audio/video delle veglie, delle foto, e dei piani d’azione su questo argomento, via email a :_ [justicefordavidkato@gmail.com](mailto:justicefordavidkato@gmail.com)

·   _Assicurarsi di non diffondere “informazioni sbagliate” o di fare disinformazione. Un’indagine di alto vertice politico e delicata è in corso nel pericoloso ambiente dell’Uganda, e pertanto la disinformazione potrebbe seriamente danneggiarla._

_Invitiamo a una risposta rispettosa verso l’assassinio di David e a NON usare questo tragico evento per campagne di raccolta fondi. Ringraziamo e incoraggiamo chiunque ha sostenuto il lavoro dello SMUG e continuerà con noi la battaglia per i diritti dei LGBT._

**Contatti del Governo ugandese:**

Presidente della Repubblica dell’Uganda . H.E Yoweri Museveni

Parliament Building

PO Box 7168Kampala, Uganda

Email: info@govexecutive.net

Fax: + 256 414 346 102

Appellativo: Sua Eccellenza

Ispettore Generale della Polizia Major Kale Kayihura

Police Headquarters

PO Box 7055Kampala, Uganda

Fax: + 256 414 255 630

Salutation: Dear Major

Ministro della Giustizia Hon. Makubuya Kiddu

Parliament Building

PO Box 7183Kampala, Uganda

Email: info@justice.go.ug

Fax: + 256 414 234 453

Appellativo: Caro Ministro

**Contatti smug:**

Frank Mugisha: +1 646 436 1858; fmugisha@sexualminoritiesuganda.org

Val Kalende: +1 857-247-1184;  kalendenator@ gmail.com

Pepe Julian: +256 772 370 674; jpepe@sexualminoritiesuganda.org

In solidarity together as one

Frank Mugisha –

Executive Director : SMUG