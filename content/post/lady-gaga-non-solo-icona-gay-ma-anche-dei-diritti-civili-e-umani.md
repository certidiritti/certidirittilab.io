---
title: 'Lady Gaga non solo icona gay ma anche dei diritti civili e umani'
date: Mon, 13 Jun 2011 11:38:59 +0000
draft: false
tags: [Politica]
---

**Il suo intervento di impegno civile sia di insegnamento ai politicanti fondamentalisti che campano di ipocrisie, privilegi e visioni integraliste della società.**

**Intervento di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti, che sarà pubblicato domani 14 giugno da Notizie Radicali.**

Lady Gaga non è solo la Pop star del momento, è la paladina dei diritti civili e umani che già avevamo avuto modo di conoscere il 20 settembre 2010 nel suo famoso intervento in Usa, alla Equality March, contro l’ipocrita disposizione dell’esercito Usa ‘Don’t ask, don’t tell” riguardante il riconoscimento delle persone lesbiche e gay per l’accesso al servizio militare e agli Mtv Awards di Los Angeles in un intervento contro l’omofobia dell'esercito Usa che aveva cacciato diversi ufficiali perché dichiaratamente gay.

Il suo intervento di sabato scorso, a Roma, ha sorpreso tutti per le parole di forza e speranza che ha voluto esprimere alla comunità lgbt(e) europea. I messaggi “vogliamo un mondo fatto di uguaglianze”, “ricordate che non siete soli a combattere, lo siamo insieme”, ”oggi e ogni giorno lottiamo per la libertà e la giustizia”,”vogliamo totale uguaglianza! Ora!”,  “oggi noi difendiamo l’amore” con l’appello finale del “chiedo ai Governi: aiutateci a combatere pacificamente, non divideteci”, sono i titoli di un lungo e appassionato intervento che entrerà certamente nella storia del movimento lgbt(e) italiano ed europeo. Non possiamo che ringraziare la venticinquenne Lady Gaga per questo suo impegno e per come lo ha saputo urlare, con forza, determinazione e convinzione al Circo Massimo.

I soliti grandi pensatori del fondamentalismo politico del regime italico, che campano di ipocrisie, privilegi e visioni integraliste della società, dovrebbero imparare ad essere più umili e meno arroganti e dare ascolto a quanto Lady Gaga ha detto ieri, anziché già stare lì pronti a sparare veleni e odio, un giorno contro una multinazionale di grandi magazzini perché fa una pubblicità con due uomini che si tengono per mano e, oggi, contro di lei. Per loro l’omosessualità è diventata una fissa, una roba che dovrebbe diventare oggetto di studio per gli esperti di psico-politica. Stanno lì, ad aspettare che accada qualcosa in favore dei gay e poi, subito, in pochissimi minuti, con le loro pseudo-autorevolezze di sottopotere di governo, a borbottare, lamentare, giudicare, che queste sono cose contro la chiesa, contro la famiglia, contro la tradizione.  Accade semplicemente che questi signori hanno capito che il terreno sotto i loro piedi sta franando perché la conquista dei diritti civili e umani, anche in Italia, arriverà. E loro lo sanno bene, e urlano istericamente, sempre più soli e poveri, le loro contumelie contro le persone omosessuali.  
  
Questi impolverati e grigi signori  dovrebbero comprendere cosa sono i colori dell’amore, l’armonia della giustizia e l’uguaglianza dei diritti e la smettano di rilasciare interviste e dichiarazioni infarcite di squallide e becere tesi secondo le quali i diritti delle persone gay ‘si possono anche riconoscere’ ma senza che le coppie gay possano adottare o senza che i gay si possano esibire ai Pride o senza che si possano sposare o senza che dicano niente contro la chiesa o senza che possano rivendicare cose a loro non gradite. Insomma, 'potete respirare ma non fare altro', “allora si che stiamo dalla vostra parte” hanno il coraggio di dire!  
Non possiamo altro che dirvi che il continuare nel vostro cammino di ipocrisia e privilegi vuol dire prendere in giro i cittadini e noi continueremo a denunciare questa vostra falsità. E rimarrete infelici e tristi anche quando andrete nel vostro fantasioso paradiso (o all’inferno, più probabile).

Applaudiamo e festeggiamo Lady Gaga, pop-star, icona gay e delle libertà e dei diritti, di tutti.