---
title: 'Lettera del professore cacciato perchè "gay e di sinistra"'
date: Wed, 09 Mar 2011 12:34:03 +0000
draft: false
tags: [Comunicati stampa, GAY, genesio petrucci, keplero, OMOSESSUALITA', preservativi, RELIGIONE, sinistra, vicariato]
---

Roma, 8 marzo 2011  
  
Carissimi colleghi ed amici,  
sento il bisogno di rivolgermi anche a voi direttamente considerato che la vicenda che mi vede protagonista sta avendo un consistente impatto sul Liceo stesso. Di questo vi chiedo sinceramente scusa: la serenità del vostro lavoro mi sta assolutamente a cuore.  
  
Vi scrivo anche per dissipare alcuni dubbi, possibili cause di turbamento.  
  
Immagino che la vicenda nuda e cruda del mio non rinnovo contrattuale vi sia ormai nota nella sua interezza, ma forse non in tutta la sua chiarezza. Per questo motivo, cogliendo gli umori di questi giorni, ho sentito la necessità di precisare alcuni “passaggi”.  
  
Innanzitutto l’interrogativo più diffuso: perché questa storia esce fuori solo ora? La risposta è semplicissima: perché solo ora ho trovato il coraggio. Solo ora ho deciso di mettere da parte timori e remore e dare dimostrazione, innanzitutto a me stesso, di un urgente desiderio di chiarezza. Solo ora, superata la profonda amarezza dell’inizio e trasformata la rabbia in volontà costruttiva, ho deciso di guardare oltre la mera convenienza.  
  
C’è un altro motivo, lo confesso. La cronaca delle ultime settimane e il deprimente quadro che si traccia del senso etico comune hanno smosso dentro di me una certa indignazione: ho perso il lavoro perché ho creduto in un progetto educativo, di certo discutibile, ma sincero e trasparente nei presupposti e nelle finalità. In un Paese dove purtroppo spesso si confonde la valorizzazione del corpo con la sua "prezzificazione", l’accaduto che mi riguarda credo sia, quantomeno, paradossale.  
  
In ultimo, ma primo per valore,  lo schiaffo morale ricevuto nelle scorse settimane dai miei ex alunni: nonostante il mio silenzio, decidono di uscire allo scoperto chiedendo loro al Vicariato spiegazioni in merito a quanto accaduto alla mia carriera di docente. Di fronte a tale testimonianza di stima e di impegno, ho sentito forte il bisogno di fare altrettanto anche io: chiedere e denunciare. Finalmente in coerenza con quanto loro insegnato in questi anni.  
  
Posso dire in tutta tranquillità che a me non risultano altre motivazioni se non quella, peraltro comunicatami esclusivamente a voce e su mia sollecitazione, del voto favorevole, in sede di consiglio di istituto, al progetto di educazione alla salute e alla sessualità in questione. Questo è quanto mi è stato riferito in occasione del colloquio col direttore dell’Ufficio scuola del Vicariato.  
  
Nel periodo precedente alla votazione di quel progetto, non avevo avuto notizia alcuna sul mio “non reincarico”. Mi erano solo state riferite  “segnalazioni” ricevute sul mio conto. Tali “segnalazioni” riferivano semplicemente la mia presenza ad uno sciopero generale della CGIL e ad una manifestazione per i diritti delle coppie di fatto (etero o gay che fossero). Fui io stesso in quella sede a ribadire di non cogliere nessuna anomalia in tali partecipazioni e di non sentirmi fuori posto né da laico credente né da insegnante di religione cattolica. Ammesso, e non assolutamente concesso, che già in quel momento mi fosse stato comunicato il non rinnovo dell’incarico, ritengo, forse addirittura in misura maggiore, che, viste le motivazioni, si sarebbe trattato di un abuso non accettabile.  
  
Per quanto concerne poi l’ipotizzata incompatibilità dell’essere omosessuale con l’essere docente di religione, faccio riferimento a quanto la Chiesa stessa afferma nei confronti dell’omosessualità e, cioè, la non condannabilità della condizione in quanto tale. E la necessità della non discriminazione. Questo per limitarmi ad un approccio “formale” alla questione, essendo serenamente consapevole che si tratti di una semplice possibilità del declinarsi dell’identità.   
  
E’ vero: faccio “tutto questo” perché non ho nulla da perdere. Più esattamente faccio tutto questo perché non voglio perdere ciò che ancora mi è rimasto: il dovere della coerenza e il rispetto di me stesso.  
  
Ci tengo poi a chiarire, qualora ve ne fosse bisogno, che  “dietro” alla mobilitazione degli studenti non ci sono affatto io. Non sono dietro e non sono avanti: sono solo soggetto/oggetto del loro interesse. Naturalmente sarei un ipocrita se dicessi di non esserne lusingato: da loro sto solo ricevendo gratuite attestazioni di stima e di sostegno.  
  
Sono intimamente convinto che la scuola sia palestra insostituibile nell’esercizio della presa di coscienza, della consapevolizzazione. Ho insegnato religione cattolica e sono cattolico per formazione e per scelta.  
  
Proprio in virtù del mio percorso ho maturato la persuasione che la coscienza personale (come il Concilio Ecumenico Vaticano II ha luminosamente insegnato) sia luogo inviolabile per ogni decisione e che ascolto e rispetto debbano guidare ogni interrelazione: è per questo che non mi sono sentito e non mi sento affatto anomalo come docente di religione. Dopo 10 anni di insegnamento ho chiara una cosa: il lavoro di docente richiede una forma urgente di obbedienza (nel senso etimologico di “ascolto previo”) che è dovuta ai ragazzi, a coloro che danno senso al nostro lavoro. Questi ragazzi ci chiedono di essere loro compagni di viaggio. Ho provo a farlo quotidianamente guardando sì alle mie stelle polari, senza mai tuttavia distogliere lo sguardo dalle loro, quali che fossero. E mai cederò alla tentazione di considerare le mie più giuste, perché adulte, e le loro meno giuste, perché “immature”.  
  
Banalizzare la sessualità è colpevole, tuttavia colpevolizzarla ad ogni costo è non solo banale ma soprattutto grave.  
  
Credo nel valore sacrosanto della laicità, in obbedienza sincera al principio evangelico della distinzione degli ambiti: “Date a Cesare quel che è di Cesare, a Dio quel che è di Dio”( Marco 12,13-17). Credo nel valore assoluto della libertà, chiaramente declinata nell’articolo 21 della nostra Carta Costituzionale: “Tutti hanno diritto di manifestare liberamente il proprio pensiero con la parola, lo scritto e ogni altro mezzo di diffusione”.  
  
Non ho intenzione di tediarvi ulteriormente. Sarebbe troppo lungo spiegarvi perché davvero non credo alla contraddizione di essere insieme gay, credente, di sinistra, insegnante di religione. Tuttavia disponibilissimo a parlarne con chi vorrà, in privato o in pubblico.  
  
Un’ultima considerazione però concedetemela: per otto anni ho vissuto e sentito il Keplero come casa mia. Un trasloco è sempre doloroso, soprattutto quando forzato. Non ho mai avuto la minima intenzione di gettare discredito sul Liceo e sui suoi docenti, anzi sarò sempre orgoglioso della mia esperienza “keplerina”.  
  
Ho ribadito in tutte le sedi mediatiche opportune l’assoluta estraneità del Keplero e della sua dirigenza  alla mia vicenda.  
  
Grato a chi vorrà sostenermi, grato a chi in coscienza, con lealtà e trasparenza, sentirà di non poterlo o volerlo fare.  
  
Grazie a tutti per la solidarietà più volte espressa. Perdonate l’intrusione.  
  
Un saluto  
Genesio Petrucci