---
title: 'Blogger siriana scomparsa: venerdì 10 giugno sit-in a Roma'
date: Wed, 08 Jun 2011 09:07:35 +0000
draft: false
tags: [Medio oriente]
---

**L'Associazione Radicale Certi Diritti, il Partito Radicale Nonviolento, transnazionale e transpartito e Radicali Roma promuovono un sit-in davanti all’Ambasciata di Siria venerdì 10 giugno ore 13 davanti all'ambasciata siriana per chiedere la liberazione della blogger lesbica Amina Araf.**  
Presentata un'interrogazione urgente al Senato a prima firma Senatore Marco Perduca.  
  
Roma, 8 giugno 2011

Sulla grave vicenda della scomparsa a Damasco della Blogger siriana lesbica Amina Araf, il Partito Radicale Nonviolento, transnazionale e transpartito, l’Associazione Radicale Certi Diritti e Radicali Roma, promuovono un sit-in davanti all’Ambasciata di Siria (lato Piazza San Marco  -Roma) per venerdì 10 giugno alle ore 13 per chiederne l’immediata liberazione. Amina, attivista gay e dissidente, con  cittadinanza americana, è la curatrice di  un Blog dichiaratamente omosessuale e di denuncia contro il regime autoritario di Siria.  
Alla manifestazione parteciperanno parlamentari e militanti Radicali.  
   
**Qui di seguito il testo integrale dell’interrogazione urgente presentata oggi dal Senatore Radicale – Pd Marco Perduca:**  
  
Interrogazione urgente a risposta scritta  
Al Presidente del Consiglio dei Ministri  
Al Ministro degli Esteri  
   
il 7 giugno 011 la blogger siro-americana Amina Arraf è scomparsa, notizie raccolte nella Rete lasciano pensare che sia stata prelevata da uomini armati a Damasco e condotta in un luogo segreto.  
Amina Arraf gestiva un blog dichiratamente omosessuale ([http://damascusgaygirl.blogspot.com/](http://damascusgaygirl.blogspot.com/)) dal quale scriveva dei vari aspetti di chi vive in Siria in condizioni di fortissime limitazioni delle libertà individuali tanto che l'omosessualità viene considerato un reato da perseguire penalmente;  
   
secondo quanto riportato anche da repubblica.it, dallo scoppio delle proteste anti-regime nel marzo scorso e con la conseguente espulsione di gran parte dei giornalisti stanieri dalla Siria, il blog di Amina era diventata una delle fonti di notizie per la stampa internazionale. I suo post si erano fatti via via più diretti e aggressivi e domenica ad esempio scriveva: "Devono andarsene, devono andarsene subito. Non c'è altro da dire".  
   
Amina Arraff era costretta a vivere nascosta anche a seguito di numerose minacce subite dovendosi spostare continuamente per paura di esser arrestata;  
   
il permanenere di una reazione spropositata da parte delle autorità siriane nei confronti del dissenso con un'escalation che nel giorno in cui Amina Arraff è stata arrestata l'esercito aveva aperto il fuoco sulla folla in molte città siriane dove per la prima volta si svolgevano manifestazioni.  
   
Si chiede di sapere:  
   
quali siano le informazioni in mano a codesto Ministero anche perché, secondo quanto affermato dalla compagna della Arraff, la coppia aveva intenzione di passare un periodo di riposo a Roma;  
   
quali siano in contatti colle autorità statunitensi volte alla liberazione della blogger;  
   
quali iniziative intenda prendere l'Italia per chiedere l'immediato rilascio di Amina Arraff;  
   
più in generale quale siano le decisioni relative al mantenimento di contatti formali con un regime che negli ultimi due mesi ha represso colla forza brutale un pacifico dissenso che si è manifestato con manifestazioni non-violente in tutto il paese per chiedere libertà, diritti e democrazia.  
   
se l'Italia si unirà alla richiesta di indagine internazionale per le violazioni del diritto umanitario internazionale che gli Stati uniti e i membri europei del Consiglio di sicurezza stanno preparando.  
   
Marco Perduca  
Donatella Poretti