---
title: 'Enzo Cucco: Torino capitale dei diritti? Lo dimostri anche in Russia.'
date: Fri, 29 Jun 2018 11:16:21 +0000
draft: false
tags: [Russia]
---

[![GEMELLAGGIO TO-SPG CARD 1X1-2](http://www.certidiritti.org/wp-content/uploads/2018/06/GEMELLAGGIO-TO-SPG-CARD-1X1-2.png)](http://www.certidiritti.org/torinosanpietroburgo/)"Abbiamo appreso dai giornali che il Comune di Torino starebbe definendo un accordo con la Città di San Pietroburgo e altre istituzioni russe per lo scambio culturale e commerciale tra le Città. In assenza di iniziative concrete del Comune, si tratterebbe di instaurare un rapporto con una Città, e uno Stato, chiaramente e platealmente omofobi. Cosa che contrasterebbe con la storia di questa Città e con le dichiarazioni e atti della sua amministrazione. Offendendo inoltre chi, da quasi 50 anni si batte per i diritti e la dignità delle persone omosessuali, lesbiche e transessuali." E' quanto afferma Enzo Cucco, storico esponente del movimento LGBTI italiano e componente del direttivo dell'Associazione Radicale Certi Diritti, rivolgendosi attraverso una lettera aperta alla Sindaca di Torino Chiara Appendino. "Dalla legge contro la cosiddetta propaganda dell'omosessualità trai minori - approvata per la prima volta proprio dal Parlamento di San Pietroburgo nel 2012 - fino all'insabbiamento del pogrom antigay Ceceno, passando per l'oppressione delle opposizioni e delle minoranze attraverso la negazione delle libertà fondamentali, sarebbe un errore imperdonabile legittimare queste istituzioni liberticide non cogliendo l'occasione di rianimare il dibattito sulla sitematica strage di Diritto che ha luogo nella Federazione Russa" sostengono il primo firmatario Enzo Cucco e il segretario dell'associazione radicale Leonardo Monaco, presentando la lettera da oggi aperta alle sottoscrizioni di tutti sul sito di Certi Diritti. "Torino è davvero la Capitale dei diritti? Lo Dimostri anche in Russia."

[FIRMA ANCHE TU LA LETTERA APERTA ALLA SINDACA APPENDINO](http://www.certidiritti.org/torinosanpietroburgo/)

Tra le richieste di Cucco e di Certi Diritti alla Sindaca 5stelle: una chiara e pubblica presa di posizione contro la repressione delle persone lgbti e delle associazioni, l'inserimento nella sua agenda ufficiale di incontri con rappresentanti di associazioni lgbt locali; la promozione, insieme al Museo del Cinema ed al Festival Lovers, di un ciclo di incontri e proiezioni sui diritti delle persone lgbt ed in generale sui diritti e sulla libertà di espressione.

[FIRMA ADESSO](http://www.certidiritti.org/torinosanpietroburgo/)