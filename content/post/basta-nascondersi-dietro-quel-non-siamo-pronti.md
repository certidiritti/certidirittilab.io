---
title: 'Basta nascondersi dietro quel "non siamo pronti"'
date: Tue, 05 Jul 2011 09:24:55 +0000
draft: false
tags: [Matrimonio egualitario]
---

**Dalla sua rubrica su Vanity Fair un articolo di Daria Bignardi sul matrimonio tra persone dello stesso sesso.**

Sinistra italiana batti un colpo. Fai quel che non hai fatto fino a ora, e che ti è costato la palude dove ti sei ficcata. Svegliati, ora che è cambiato il vento. Tira fuori il coraggio, prima che sia troppo tardi, e inizia da qui, da una campagna per i diritti. Hai presente New York? Il posto più democratico, liberale e moderno del mondo? Non ho detto la Svezia o l’Olanda, ho detto New York.

Dal 24 luglio  anche a New York le coppie omosessuali avranno diritto di sposarsi, come già in Massachusetts, Vermont, New Hampshire, Iowa, Connecticut, District of Columbia, Spagna, Belgio, Norvegia, Portogallo, Islanda, Canada, Sudafrica, Svezia, Paesi Bassi, Argentina e  Città del Messico. In Francia, in Israele, in Aruba e nelle Antille olandesi sono riconosciuti i matrimoni tra persone dello stesso sesso contratti altrove. E in quasi tutta Europa sono previste le unioni civili tra omosessuali.  Insomma:  il  mondo si sta pian piano (ma neanche tanto piano) dividendo tra Paesi democratici e Paesi arretrati. E tu da che parte vuoi che stia l’Italia? Te la senti di rischiare per un’idea, di combattere per i diritti civili, finalmente? Se non lo fai tu, chi lo deve fare?

Basta nascondersi dietro un vigliacco «In Italia non siamo ancora pronti». Anche Milano non sembrava pronta. Sembrava che la destra dovesse vincere per sempre, poi arriva uno per bene che si rimbocca le maniche, ci crede, ci prova e – oplà – la destra perde. La vogliamo alzare l’asticella dei progetti, dei sogni, del rinnovamento, della democrazia e dei diritti civili o vuoi continuare ad aver paura per sempre?

In Italia c’è il Vaticano. Bene. Anche in Spagna c’erano ancora i franchisti, eppure. Suor Chiara, amica mia, lo so che ti stai facendo il segno della croce e ti si spacca il cuore a leggere queste parole, ma pensaci un momento prima di scrivermi  una email di dolore. Sono sicura che anche Gesù, se capitasse da queste parti, avrebbe rispetto per l’amore.

Mi stanno simpatici quelli che dicono, come il povero Vittorio Sgarbi, che sono contrari al matrimonio anche per gli eterosessuali. È un’affermazione divertente e in parte condivisibile: ma allora  facciamo o tutti o nessuno. Perché io sì e tu no? Perché io no e tu sì? In cosa siamo diversi? Giuro che non riesco a capirlo, ci ho provato, davvero, ma proprio non mi entra in testa. Perché se Giovanna vuole sposare Rosa non può? Chi lo decide? E perché? Per i cattolici è un peccato? Benissimo, massimo rispetto. Omosessuali cattolici, nessuno vi costringe a sposarvi. Ma perché una coppia omosessuale non può sposarsi civilmente come ho fatto io? È ora che la sinistra italiana, se esiste, ricominci a parlare di diritti civili, e a fare delle scelte.

[http://barbablog.vanityfair.it/](http://barbablog.vanityfair.it/)