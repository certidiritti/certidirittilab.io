---
title: 'OMOSESSUALI E INVISIBILI -  A PALERMO SENZA DIRITTI'
date: Mon, 14 Apr 2008 09:37:57 +0000
draft: false
tags: [Comunicati stampa]
---

  
(AGI) - Palermo, 11 apr. - Andrea e Cristian, coppia di ragazzi omosessuali ripudiati dai genitori, senza tetto e a rischio di cancellazione dall'anagrafe. A Palermo.

E' "inaccettabile" che Andrea e Cristian, la coppia di omosessuali che dorme in una tenda al Foro Italico di Palermo, "venga considerata una coppia di invisibili dalle leggi dello Stato italiano e da tutti quegli enti locali che non adottano misure per i diritti dei gay". E' l'accusa di Vladimir Luxuria, parlamentare di Rifondazione comunista, in merito alla storia di Cristian Tagliavia 21 anni, e Andrea Colletti, due storie di porte chiuse in faccia e di abbandoni, anche da parte dei genitori una volta conosciuta la loro condizione, e di licenziamenti su due piedi per lo stesso motivo.  
L'Ufficio servizi demografici del Comune gli ha comunicato la cancellazione dall'anagrafe se non dichiareranno il loro indirizzo di residenza. Ma vivono da senzatetto in una tenda, spostandosi continuamente, "e una casa - spiegano - il Comune ci ha detto che non l'avremo mai, perche' non siamo una famiglia e non abbiamo dei figli.  Per loro, non abbiamo diritti".