---
title: 'Nasce ''Fuor di pagina'', la rassegna stampa di Certi Diritti. Scarica il podcast'
date: Wed, 02 May 2012 09:51:21 +0000
draft: false
tags: [Politica]
---

'Fuor di pagina' è la neonata rassegna stampa audio dell'Associazione Radicale Certi Diritti che si propone di dare settimanalmente un quadro delle notizie sui diritti civili e la libertà sessuale delle persone.

Il titolo richiama il fatto che spesso e volentieri queste notizie non arrivano sulle pagine politiche o di cronaca dei quotidiani nazionali né di quelli locali, si dovrà andare, appunto, fuor di pagina per trovare le notizie che riguardano più da vicino la vita delle persone. Si dovrà scandagliare i siti web informativi e delle associazioni che si occupano di queste cose per restituire a tutti noi il polso di ciò che più ci tocca da vicino.

Il titolo si rifà però anche alla prima associazione omosessuale italiana, il Fuori!, che richiamava nel suo nome quel percorso di liberazione personale che consiste tutt'oggi nell'accettazione del proprio orientamento sessuale e della conseguente acquisizione della capacità di viverlo alla luce del sole in piena serenità.  
Ugualmente "Fuor di pagina" vuole dare visibilità ai temi che tratta nel tentativo di aiutare il dibattito pubblico, e quindi la politica, a riavvicinarsi alla vita delle persone, alle loro domande più profonde e alla loro emotività.

Infine si è voluta offrire una suggestione shakespeariana richiamandosi alla locuzione "out of joint", "fuor di sesto", contenuta nell'Amleto, Atto I Scena V, che indica qualcosa fuori squadra, di sbagliato, di non normale, appunto la negazione dei diritti e delle libertà civili e individuali che troppo spesso vengono conculcate in Italia e nel mondo.

**[Scarica il podcast e ascolta la prima puntata >](http://www.radioradicale.it/podcasting.php?e=1004)**