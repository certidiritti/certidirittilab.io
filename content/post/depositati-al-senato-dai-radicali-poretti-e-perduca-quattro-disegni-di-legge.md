---
title: 'DEPOSITATI AL SENATO DAI RADICALI PORETTI E PERDUCA, QUATTRO DISEGNI DI LEGGE'
date: Wed, 21 May 2008 13:01:01 +0000
draft: false
tags: [Comunicati stampa]
---

DEPOSITATI AL SENATO DAI RADICALI PORETTI E PERDUCA, I DISEGNI DI LEGGE SU MATRIMONIO GAY, DIVORZIO BREVE, TUTELA PERSONE TRANSESSUALI, LOTTA ALL'OMOFOBIA E UNIONI CIVILI. Roma, 21 maggio 2008  
Sono stati depositati oggi, al Senato, quattro Disegni di Legge, firmati dai Senatori radicali del Pd, Donatella Poretti e Marco Perduca, sui temi dei diritti civili e della riforma del diritto di famiglia. I provvedimenti, sui quali è in corso una raccolta firme tra tutti i Senatori, riguardano l'estensione dell'istituto del matrimonio per le coppie lesbiche e gay, maggior tutela per le persone transessuali e transgender, iniziative per la lotta contro l'omofobia, regolamentazione delle Unioni civili e accorciamento dei tempi per il divorzio.  
Tali proposte, che in molti paesi europei sono già divenute realtà che estendono e difendono i diritti di milioni di cittadini, rispondono alle sollecitazioni delle Direttive europee e, in Italia,  anche a sentenze della Corte Costituzionale che chiede adeguamenti legislativi all'evoluzione della società.  
Nell'ambito delle proposte finalizzate alla Riforma del diritto di famiglia erano già stati depositati, dagli stessi Senatori, i Disegni di Legge sulle adozioni per i single, doppio cognome e diritti dei figli naturali.  
  
Info: sergio.rovasio@gmail.com