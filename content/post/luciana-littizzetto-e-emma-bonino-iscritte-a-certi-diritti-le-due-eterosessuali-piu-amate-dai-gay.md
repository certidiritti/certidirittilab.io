---
title: 'Luciana Littizzetto e Emma Bonino, iscritte a Certi Diritti, "le due eterosessuali più amate dai gay"'
date: Thu, 08 Sep 2011 07:40:09 +0000
draft: false
tags: [Politica]
---

Tra i 20.000 votanti sul sito del GayVillage per i premi Awards, vincono Luciana Littizzetto e Emma Bonino, entrambe iscritte all'associazione radicale Certi Diritti, considerate le "eterosessuali più amate dai gay".  
  

Comunicato Stampa dell’Associazione Radicale Certi Diritti

In occasione della serata GayVillage Awards, che si svolgerà a Roma presso il GayVillage all’Eur, venerdì 9 settembre, tra i 20.000 votanti del sito [gayvillage.it](http://gayvillage.it/), le più votate sono state, per lo spettacolo, Luciana Littizzetto e, per la politica, Emma Bonino, entrambe iscritte per il 2011 all’Associazione Radicale Certi Diritti, considerate "le eterosessuali più amate dai gay".

L’Associazione Radicale Certi Diritti ringrazia tutti coloro che hanno votato per queste due donne straordinarie che con la loro grinta, forza e impegno quotidiano riescono ad affermare principi e valori di libertà, laicità e impegno civile nel nostro paese.

La loro iscrizione alla nostra Associazione ha un valore immenso ed è anche un gesto di riconoscenza per l’impegno quotidiano che ci vede tutti impegnati per il superamento delle diseguaglianze.  

Speriamo che molti si affianchino alle due ‘eterosessuali più amate dai gay’ e vogliano  sostenerci per continuare insieme a lottare per il riconoscimento dei diritti e per la lotta all’ipocrisia clerical-partitocratica che sempre più invade il nostro paese. 

Per ricevere la NewsLetter dell'Associazione Radicale Certi Diritti:  

[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)