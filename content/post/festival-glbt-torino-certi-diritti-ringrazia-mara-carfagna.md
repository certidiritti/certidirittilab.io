---
title: 'Festival Glbt Torino: Certi Diritti ringrazia Mara Carfagna'
date: Fri, 01 Apr 2011 22:43:28 +0000
draft: false
tags: [Politica]
---

La ministra interviene con competenza e serietà e rimedia ad una vicenda ridicola. I torinesi ne terranno conto alle prossime elezioni europee.

Torino, 1 aprile 2011

L’Associazione Radicale Certi Diritti ringrazia il Ministro per le Pari Opportunità per essere intervenuta sulla vicenda relativa alla cancellazione, da parte della Regione Piemonte, del Patrocinio alla rassegna internazionale del cinema lgbt di Torino. Il Ministro ha dimostrato attenzione e serietà rispetto ad una vicenda assurda e ridicola che fa tornare indietro di decenni la cultura e la politica.

La rassegna internazionale del cinema lgbt di Torino, tra le più importanti in Europa e in assoluto la più prestigiosa in Italia, e che coinvolge decine di migliaia di persone, provenienti da varie parti del mondo, ha subìto la ‘censura politica’ di rappresentati politici retrogradi  e con una visione miope della realtà. Saranno gli stessi torinesi ad esprimere il loro giudizio in occasione delle prossime elezioni.

Negare un evento culturale di questa portata è il segno dei brutti tempi che stiamo attraversando. Bene ha fatto quindi il Ministro Mara Carfagna ad intervenire immediatamente con serietà e competenza.