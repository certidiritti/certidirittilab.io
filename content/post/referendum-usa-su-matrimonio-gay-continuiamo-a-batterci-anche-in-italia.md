---
title: 'REFERENDUM USA SU MATRIMONIO GAY: CONTINUIAMO A BATTERCI ANCHE IN ITALIA'
date: Thu, 06 Nov 2008 15:05:01 +0000
draft: false
tags: [AFFERMAZIONE, CIVILE, Comunicati stampa, diritti, GAY, MATRIMONIO, PARKS, ROSA, UGUAGLIANZA]
---

MATRIMONIO GAY: ANCHE IN ITALIA, COME IN CALIFORNIA, INIZIATIVE LEGALI PER FAR VALERE I DIRITTI DI UGUAGLIANZA DI TUTTI.

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

“Estendere i diritti a persone che non ne hanno è uno degli obiettivi che l’Associazione Radicale Certi Diritti si è data da quando ha lanciato la campagna di Affermazione civile finalizzata al riconoscimento del matrimonio per le persone gay in Italia come già avvenuto in Spagna, Olanda e Belgio. Sono oltre 30 le coppie di lesbiche e gay che in Italia hanno incardinato in queste settimane iniziative legali con questo obiettivo.

La democrazia americana riguardo l’accesso all’istituto del matrimonio per le persone omosessuali ha dato un segnale che non è una soluzione di superamento delle diseguaglianze e di questo ne prendiamo atto. Non permettere l’accesso all’istituto del matrimonio per le persone lesbiche e gay significa voler mantenere una distinzione sociale, culturale e politica tra eterosessuali ed omosessuali.

In California è stato il 2% a ribaltare una decisione della Corte Suprema sul matrimonio gay; in Italia, grazie al potere clericale e alla genuflessione della classe politica ai voleri del Vaticano la percentuale sarebbe stata certamente maggiore perché manca totalmente il dibattito, la discussione e la necessaria cultura laica che è alla base delle leggi di civiltà e di giustizia.

Quando a Montgomery, l’1 dicembre 1955, la donna di colore Rosa Parks si sedette in un posto riservato ai bianchi e si rifiutò di obbedire all’ordine del conducente dell’autobus di spostarsi nella parte posteriore riservata ai neri, poneva all’attenzione della storia un fatto di disuguaglianza di trattamento tra persone. Noi continuiamo questa lotta perché riteniamo che le persone lesbiche e gay siano persone come tutte le altre e fino a quando il matrimonio (civile) sarà loro vietato questa differenza non verrà meno”.