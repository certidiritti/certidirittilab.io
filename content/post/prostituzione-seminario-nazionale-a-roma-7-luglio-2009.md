---
title: 'PROSTITUZIONE: SEMINARIO NAZIONALE A ROMA 7 LUGLIO 2009'
date: Fri, 26 Jun 2009 06:12:19 +0000
draft: false
tags: [Comunicati stampa]
---

Seminario nazionale “PROSTITUZIONE - quali politiche e quali risposte alla cittadinanza, alle persone che si prostituiscono, alle vittime di sfruttamento e tratta di esseri umani”,

7 luglio 2009 Roma. dalle ore 9.00 alle ore 18.00 c/o l’ex Hotel Bologna - via S. Chiara n. 4, Roma.  
   
Il fenomeno della prostituzione riveste caratteristiche di complessità rispetto alle quali è necessario rispondere con politiche ed interventi adeguati.

Nel luglio del 2008 il documento “Prostituzione e Tratta, Diritti e Cittadinanza – le proposte di chi opera sul campo”, promosso da Asgi, Ass. Gruppo Abele, Ass. On the Road, Caritas Italiana, Coordinamento Nazionale Comunità di Accoglienza (Cnca), Comitato per i Diritti Civili delle Prostitute, Comune di Venezia, Consorzio Nova, Coop. Sociale Dedalus, Save the Children Italia e cui hanno aderito 116 enti pubblici e non profit, ha avuto come scopo quello di evidenziare le problematiche e le possibili linee di intervento in merito alla prostituzione e alla tratta di esseri umani nelle sue diverse forme.  
   
Alla luce delle nuove evoluzioni legislative sul tema della prostituzione a livello nazionale e delle ordinanze emesse da molti sindaci italiani, alcuni degli enti promotori del documento “Prostituzione e Tratta” hanno ritenuto importante cercare di analizzarne contenuti ed effetti. Da qualche tempo, infatti, si sta lavorando ad una ricognizione nazionale sulle cosiddette ordinanze anti-prostituzione proprio allo scopo di tentare di capire quale sia l’impatto di tali provvedimenti sulle condizioni di vita di chi si prostituisce e delle vittime di tratta e sfruttamento sessuale, quali le conseguenze sul lavoro degli operatori di strada e quali le prospettive future nel caso di approvazione del Disegno di Legge n. 1079.  
   
Il lavoro di monitoraggio svolto in questi mesi è confluito in un report finale nel quale sono stati rielaborati i dati raccolti a livello nazionale, grazie anche alla collaborazione di enti ed associazioni che a vario titolo realizzano interventi sociali nel campo della prostituzione.

Associazione On the Road Onlus  
Via delle Lancette 27/27A  
64014 Martinsicuro (TE)  
tel. 0861-796666/0861-762327  
fax 0861-765112  
[www.ontheroadonlus.it](http://www.ontheroadonlus.it)  
e-mail: [romina@ontheroadonlus.it](mailto:romina@ontheroadonlus.it)