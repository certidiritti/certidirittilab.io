---
title: 'PARLAMENTO EUROPEO APPROVA NUOVA RISOLUZIONE SUI DIRITTI LGBT IN LITUANIA'
date: Wed, 19 Jan 2011 14:22:32 +0000
draft: false
tags: [Comunicati stampa]
---

La risoluzione chiede anche alla commissaria Reding di istituire una road map per i diritti lgbt(e) in Europa.

Roma-Bruxelles, 19 gennaio 2011

Comunicato stampa dell’Associazione Radicale Certi Diritti

Il Parlamento Europeo ha approvato oggi a larghissima maggioranza - anche grazie all'appoggio di molte delegazioni moderate del PPE - la risoluzione dei gruppi ALDE, Pse, Verdi e Gue sulla "violazione della libertà di espressione e sulle discriminazioni basate sull'orientamento sessuale in Lituania".

Il Parlamento Europeo chiede al Parlamento lituano di bocciare i progetti di emendamento al codice amministrativo che puniscono la **"pubblica promozione delle relazioni omosessuali"** con una multa compresa tra i 2 000 e i 10 000 LTL (580 - 2 900 euro); di consentire ai minori di avere libero accesso alle informazioni sull'orientamento sessuale e di tutelarli contro le discriminazioni basate sull'orientamento sessuale, modificando la legge sulla "difesa dei minori rispetto alle informazioni pubbliche" ed il progetto di legge sull'istruzione; di chiarire il significato del divieto previsto dalla legge sulla pubblicità, che proibisce la "manifestazione o promozione dell'orientamento sessuale". Nella risoluzione il PE si congratula anche con la Presidente della Lituania Dalia Grybauskaitė e la Commissiaria per i diritti fondamentali Viviane Reding per la loro azione in difesa dei diritti fondamentali delle persone LGBT e chiede alla Presidente di opporre il veto agli emendamenti se fossero approvati.

La Risoluzione, inoltre, chiede alla Commissaria Reding di lanciare una **"Tabella di marcia (Roadmap) dell'Unione Europea contenente misure concrete contro l'omofobia e le discriminazioni basate sull'orientamento sessuale"**.

L’Associazione Radicale Certi Diritti ringrazia il Parlamento Europeo per l'ulteriore appoggio ai diritti delle persone LGBT(E) in Lituania ed in Europa e chiede alla Commissione di lanciare la Roadmap per i diritti LGBT(E) al fine di assicurare che leggi come quella votata in Lituania non possano trovare spazio nei paesi membri dell’Ue, visto l’evidente intento discriminatorio nei confronti di una parte dei cittadini.