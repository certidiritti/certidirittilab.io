---
title: 'LA «SOCIETA'' NATURALE» E I SUOI "NEMICI"'
date: Fri, 15 Jan 2010 23:45:27 +0000
draft: false
tags: [Comunicati stampa]
---

UNIVERSITA' DEGLI STUDI DI FERRARA  
DIPARTIMENTO DI SCIENZE GIURIDICHE

Ferrara, 11 gennaio 2010

  
Carissima/o,

_anche quest'anno organizzeremo il tradizionale **Seminario "preventivo" ferrarese**, tredicesima edizione della nostra artigianale filiera. L'appuntamento è fissato per venerdì **26 febbraio 2010** con inizio alle **ore 10.30**, presso l'Aula Magna del Dipartimento di Scienze giuridiche, Corso Ercole I d'Este n. 37, a Ferrara._  
_Questo il suo titolo:_

### LA «SOCIETA' NATURALE» E I SUOI "NEMICI"  
Sul paradigma eterosessuale del matrimonio  

Maggiori informazioni su programma dei lavori, partecipanti e relazioni al seguente link:

www.amicuscuriae.it

_Il Seminario prende le mosse da **quattro ordinanze di rinvio** dalla diversa provenienza geografica: Tribunale di Venezia, 3 aprile 2009 e Corte d'Appello di Trento, 29 luglio 2009 (entrambe già pubblicate in G.U., rispettivamente n. 117 e n. 248 del 2009), Corte d'Appello di Firenze, 3 dicembre 2009 e Tribunale di Ferrara, 14 dicembre 2009 (ad oggi inedite). Tutte chiedono che il Giudice delle leggi si pronunci in ordine alla riconoscibilità costituzionale del diritto dell'individuo omosessuale di contrarre matrimonio con persona del proprio sesso._

_La **dinamica dei casi**, che ora approdano per via incidentale a Palazzo della Consulta, è seriale: una coppia di omosessuali chiede di procedere alle pubblicazioni di matrimonio; l'ufficiale di stato civile oppone rifiuto alla richiesta considerata illegittima; la coppia ricorre in sede civile avverso il provvedimento di diniego, sollecitando il giudice - in via principale - ad ordinare le pubblicazioni rifiutate e - in via subordinata - di sollevare la relativa questione di legittimità costituzionale; il giudice civile, esclusa la praticabilità di una interpretazione del dato normativo difforme da quella seguita dall'ufficiale di stato civile, promuove la quaestio perché rilevante e non manifestamente infondata.  
  
I thema decidenda prospettati non sono perfettamente sovrapponibili. Non lo sono nei **parametri invocati**: gli artt. 2, 3 e 29 Cost., cui solo il Tribunale di Venezia aggiunge anche l'art. 117, comma 1, Cost. Non lo sono anche per le **disposizioni legislative impugnate** che, variamente combinate, risultano essere gli artt. 93, 96, 98, 107, 108, 143, 143 bis, 156 bis, 231 codice civile, «nella parte in cui, complessivamente valutati, non consentono agli individui di contrarre matrimonio con persone dello stesso sesso» (così la Corte d'Appello di Trento, ma analoghe formule sono adoperate anche dagli altri remittenti). Alcuni giudici a quibus, peraltro, ricorrono alla **clausola di salvaguardia** a tenore della quale «valuterà la Corte, qualora ritenesse la questione fondata, se vi sia la necessità di estendere la pronuncia anche ad altre disposizioni legislative interessate in via di consequenzialità ai sensi dell'art. 27 della legge n. 87 del 1953» (così i Tribunali di Venezia e di Ferrara).  
La lettura degli atti di promovimento permette agevolmente di individuare in quella di Venezia l'ordinanza-pilota: ad essa fanno espresso rinvio i giudici di Trento e di Firenze, mentre quello di Ferrara ne ricalca la lettera per larga parte della propria parte motiva. Anche le altre ordinanze, tuttavia, introducono inediti profili argomentativi rispetto alla capofila._

_Comune è la **ricostruzione della disciplina complessiva dell'istituto matrimoniale**, di cui si esclude un'interpretazione evolutiva e costituzionalmente orientata. Il silenzio dell'ordinamento - che non prevede né vieta espressamente il matrimonio omosessuale - viene inteso come norma implicita di esclusione, in considerazione delle numerose disposizioni che presuppongono la differenza di sesso nel rapporto coniugale. L'interpretazione assunta fotografa così «una consolidata e ultramillenaria nozione di matrimonio come unione di un uomo e di una donna» (Tribunale di Venezia e di Ferrara) che «trova il suo fondamento nel sentimento, nella cultura, nella storia della nostra comunità nazionale» (così l'ufficiale di stato civile nella citazione del Tribunale di Ferrara). Interpretazione, peraltro, confermata dalle determinazioni del Ministero degli Interni in materia: vengono citate la circolare n. 2 del 26 marzo 2001; il parere del 28 luglio 2004, prot. 04006451-15100/15952; la circolare n. 55 del 18 ottobre 2007, prot. n. 15100/397/0009861.  
E' proprio **l'ipotesi di partenza della giuridica inesistenza del matrimonio omosessuale** a rivelarsi costituzionalmente dubbia, anche alla luce di un'evoluzione sociale e - altrove, ma non in Italia - ordinamentale che impone «un'attenta meditazione sulla persistente compatibilità dell'interpretazione tradizionale con i principi costituzionali» (Tribunali di Venezia e di Ferrara)._

_Ad entrare in tensione è, innanzitutto, l'**art. 2 Cost.** laddove riconosce i diritti inviolabili dell'uomo anche all'interno della famiglia quale primaria formazione sociale che conferisce uno status (quello di persona coniugata e come tale titolare di diritti ed obblighi peculiari) non surrogabile attraverso gli strumenti dell'autonomia negoziale privata. Alla sfera dell'art. 2 Cost. è riconducibile anche il diritto di sposarsi quale libertà fondamentale - positiva e negativa - della persona, inclusiva della scelta autonoma del proprio coniuge secondo il proprio individuale orientamento sessuale «né patologico, né illegale» (Tribunali di Venezia e Ferrara)._

_Il secondo parametro invocato è **l'art. 3 Cost.**: se nel diritto di contrarre matrimonio trova espressione la dignità della persona, esso non può trovare preclusione in discriminazioni derivanti dal sesso o dall'orientamento sessuale, «allo stesso modo in cui il principio d'uguaglianza assicura la libertà di scegliere un coniuge di una razza piuttosto che di un'altra, di una religione piuttosto che di un'altra, di una condizione personale piuttosto che di un'altra» (Corte d'Appello di Firenze). E', questa, una libertà di scelta che non travalica limiti posti a tutela di beni costituzionalmente rilevanti, pur evocati nel dibattito dottrinale ma - secondo i giudici a quibus - cedevoli o giuridicamente ultronei: il rispetto della natura umana, la sicurezza e l'ordine pubblico, l'integrità etica della Nazione, la tradizione, la funzione procreativa come finalità propria della famiglia matrimoniale quale «granaio dello Stato», il diritto dei figli a crescere in un idoneo contesto familiare.  
Particolare rilievo viene dato alla **legge n. 164 del 1982** (ed alla relativa sentenza costituzionale n. 161/1985) in materia di rettificazione di attribuzione di sesso che, già oggi, consente la celebrazione o la conservazione del vincolo matrimoniale anche se il partner abbia nel frattempo mutato il proprio sesso originario: allo stato del giure, dunque, si registra una disparità irragionevole tra la condizione dell'omosessuale e l'analoga situazione del transessuale (cfr. le ordinanze dei Tribunali di Venezia e Ferrara)._

_Anche **l'art. 29, comma 1, Cost.** entra in gioco: non come ostacolo giuridico da superare, semmai come «ulteriore stimolo ad ammettere il matrimonio omosessuale» (Corte d'Appello di Firenze). Dell'invocata disposizione costituzionale, infatti, i giudici a quibus mettono a valore la formulazione neutrale, la sua genealogia ricostruita attraverso una lettura non banalizzata dei lavori in Assemblea Costituente; ne declinano l'attributo «naturale» in senso fenomenico e non giusnaturalistico, valorizzandone la funzione giuridica di limite alle ingerenze dello Stato legislatore nell'autonomia familiare. Così riletto, si arriva ad escludere che rientri nelle finalità dell'art. 29 Cost. la tutela di un modello familiare tradizionale, peraltro da tempo oggetto di inarrestabile trasformazione nei costumi sociali e nella stessa disciplina legislativa, anche sulla spinta di "storiche" sentenze della Corte costituzionale (come tutti gli atti di promovimento argomentano)._

_E' il solo Tribunale di Venezia ad invocare anche il parametro dell'art. 117, comma 1, Cost., inquadrando il problema "nazionale" all'interno del più dinamico quadro transnazionale e comparato. Entrano così in gioco quali **norme interposte** gli artt. 8, 12, 14 della CEDU (nell'interpretazione datane dalla Corte di Strasburgo) e gli artt. 7, 9, 21 della Carta di Nizza (successivamente all'ordinanza di rinvio entrata in vigore a seguito del rinvio materiale ad essa contenuto nel Trattato di Lisbona). Si richiamano i numerosi atti delle Istituzioni europee favorevoli a rimuovere gli ostacoli che si frappongono al matrimonio di coppie omosessuali. Si segnala, in materia, l'evoluzione «nel diritto di molte nazioni di civiltà giuridica affine alla nostra» che hanno accolto «una nozione di relazioni familiari tale da includere le coppie omosessuali»._

_Non è escluso che, da qui alla data del Seminario ferrarese, **altre ordinanze** veicolino analoghe questioni di legittimità: in tal caso, verranno acquisite in corsa. Così come, sul piano più strettamente processuale, è da mettere certamente in conto la **richiesta di intervento davanti alla Corte costituzionale** da parte di Associazioni che operano a tutela e per l'affermazione dei diritti delle minoranze "LGBT": profilo, questo, che dovrà entrare nella nostra riflessione preventiva alla luce di una giurisprudenza costituzionale, in tema di integrazione del contraddittorio, non lineare._

_Il Seminario, come da tradizione, è aperto a tutti, senza preclusioni scientifiche, accademiche, generazionali.  
La discussione sarà governata secondo le regole oramai consolidate dei nostri precedenti appuntamenti: una comune traccia per la discussione, che ripercorrerà gli interrogativi ricavabili dall'intera vicenda costituzionale. Una relazione introduttiva, affidata quest'anno alla **Prof.ssa Barbara Pezzini (Ordinaria di Diritto costituzionale nell'Università di Bergamo)**. La discussione, come sempre, sarà franca ed informale, caratterizzata da brevi (dunque numerosi) interventi di 7 minuti ciascuno.  
L'intera documentazione dei "casi" e tutte le necessarie informazioni logistiche saranno reperibili nell'oramai ricco **sito** dedicato ai seminari "preventivi" ferraresi, **attualmente in allestimento**, del quale segnaliamo il **nuovo link**: [www.amicuscuriae.it](http://www.amicuscuriae.it). Ivi sarà fruibile anche la registrazione audiovideo integrale dei nostri lavori.  
Come già accaduto per tutti i precedenti seminari, gli atti dell'incontro verranno posti **nella disponibilità della Corte costituzionale** in tempi brevi (e comunque utili per il suo giudizio), per essere poi pubblicati nella Collana amicus curiae dell'editore Giappichelli.  
  
Se - come speriamo davvero - intenderai partecipare al Seminario, ti chiediamo, per ragioni organizzative, di comunicare preventivamente la tua **adesione** (nome e cognome, qualifica, e-mail) ad uno di noi, utilizzando il relativo indirizzo di posta elettronica.  
  
Un cordiale saluto,_

  
Prof. Roberto Bin \[bnb@unife.it\]  
Prof.ssa Giuditta Brunelli \[bug@unife.it\]  
Prof. Andrea Guazzarotti \[gzzndr@unife.it\]  
Prof. Andrea Pugiotto \[pua@unife.it\]  
Prof. Paolo Veronesi \[vrp@unife.it\]