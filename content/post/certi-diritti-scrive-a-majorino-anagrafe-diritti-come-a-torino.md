---
title: 'Certi Diritti scrive a Majorino: anagrafe diritti come a Torino'
date: Mon, 27 Jun 2011 13:30:23 +0000
draft: false
tags: [Politica]
---

**Dopo l’intervento dell’Assessore alle Politiche sociali e Servizi per la salute davanti alle migliaia di persone che hanno affollato il Pride di Milano, l’Associazione Radicale Certi Diritti - Milano gli rivolge la seguente lettera aperta chiedendogli un incontro per discutere insieme dell’istituzione a Milano dell’anagrafe dei diritti, che dovrebbe avere come modello Torino e non Padova.**

L’Associazione Radicale Certi Diritti - Milano  si augura che gli impegni elettorali si traducano presto in fatti concreti per segnare un cambio di passo decisivo dalle scorse amministrazioni e ridare speranza alla nostra città e al nostro paese tutto.

  
Carlo Cargnel e Yuri Guaiana

Associazione Radicale Certi Diritti - Milano

  
**Ecco la lettera:**

Gentile assessore Majorino,

L’abbiamo ascoltata con grande interesse sabato scorso al Gay Pride di Milano e abbiamo molto apprezzato il suo discorso, soprattutto nella parte in cui dice: “Iniziamo col dire che il registro delle unioni civili lo faremo - ha detto -, lo do' per scontato perché e' nel programma, poi ragioneremo su cos'altro fare. Sul terreno delle politiche sociali per il riconoscimento dei diritti civili Milano deve fare tutto perché non e' stato fatto nulla. Ci confronteremo ma ci auguriamo che anche il Parlamento si muova”.

Ha ragione, tante sono le cose da fare e bisogna iniziare con ciò che è già compreso nel programma elettorale votato da migliaia di cittadini milanesi. Come lei stesso ricorda in un intervista ad Alessia Gallione su Repubblica ([http://milano.repubblica.it/cronaca/2011/06/24/news/coppie\_di\_fatto\_piano\_del\_comune\_sar\_un\_cantiere\_per\_i\_diritti\_civili-18144266/](http://milano.repubblica.it/cronaca/2011/06/24/news/coppie_di_fatto_piano_del_comune_sar_un_cantiere_per_i_diritti_civili-18144266/)), in quel programma si dice testualmente che “il registro delle unioni civili che il Comune intende istituire non è un atto simbolico, ma funzionale all’adozione di politiche e di atti non discriminatori” e si aggiunge che servirà a “riconoscere la pluralità delle forme di comunione di vita, con l’impegno dell’amministrazione a promuovere la parità e contrastando ogni discriminazione in tutti i settori dell’attività del Comune”.

  
Come Associazione Radicale Certi Diritti abbiamo, unici tra le associazioni LGBT milanesi, partecipato al workshop “La città dei diritti” dell’Officina Pisapia portando varie proposte tra le quali è stata accolta proprio l’Anagrafe dei diritti, che prevede appunto la piena parificazione con la coppia sposata (nell'ambito delle competenze comunali, ovviamente) di ogni comunità di affetto e di stile di vita che desideri registrarsi.

  
Nella medesima intervista a Repubblica lei prende a modello “quanto fatto a Padova da Flavio Zanonato che ha formalizzato una sorta di anagrafe dove si possono registrare le coppie di fatto”. Il modello padovano però non è assolutamente sufficiente per ottemperare agli impegni presi in campagna elettorale e inseriti nel programma.

  
Per quanto riguarda il registro, nel quale non si esaurisce il tema dell’anagrafe dei diritti, occorre invece riferirsi al modello torinese. Per questo le inviamo la delibera del Comune di Torino e le chiediamo al più presto un incontro per discutere insieme dell’istituzione a Milano dell’anagrafe dei diritti e di cos’altro fare per, come ha ben detto lei, “far uscire Milano dallo scantinato dei diritti civili”.