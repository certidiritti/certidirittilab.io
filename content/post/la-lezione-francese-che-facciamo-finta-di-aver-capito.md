---
title: 'La lezione francese che facciamo finta di aver capito'
date: Wed, 24 Apr 2013 19:36:27 +0000
draft: false
tags: [Europa]
---

Nota di Enzo Cucco, Presidente dell'Associazione Radicale Certi Diritti

Roma, 24 Aprile 2013

Mi sembra già di sentirle le geremiadi di quanti osservano atterriti la straordinaria organizzazione del movimento per il no al matrimonio egualitario francese. Come se le parole spese in questi anni sul significato profondo delle battaglie per l'estensione del matrimonio civile a tutti e tutte non avessero fatto alcuna breccia. Come se non avessimo letto e visto le reazioni non solo furibonde ma astute, innovative e meravigliosamente retoriche delle campagne per il no negli USA.(1)

Ora è finalmente arrivato anche agli occhi della maggioranza degli italiani cosa è la nuova, vera opposizione al principio di uguaglianza nei paesi occidentali. Dietro qualche sparuta faccia da trombone, o clericale (spesso i due termini si accompagnano) ci sono le facce alla Frigide Barjot, alle migliaia e migliaia di manifestanti, giovani e donne in prima linea, all'uso massiccio dell'ironia, come massiccio è stato l'uso dei social network, per tentare di ridicolizzare prima che di demonizzare, la richiesta del matrimonio egualitario.

La vera lezione francese sta soprattutto in questa duplice domanda: come si è costruito il fronte del NO, così moderno e aggressivo e come la maggioranza di governo sia riuscita a tener testa alla manipolazione mediatica ed alla retorica conservatrice mascherata da difesa della democrazia.

Se proiettate queste due domande alla situazione italiana descriviamo esattamente un bel programma di lavoro, urgente e necessario, per far si che le richieste che facciamo sia supportate da campagne serie di supporto e promozione.

La seconda domanda non credo abbia bisogno di risposte, purtroppo. Se i massimi leader della sinistra italiana ripetono all'unisono che le famiglie composte da coppie dello stesso sesso non sono famglie (cosa che ha detto tra gli altri, anche il forse prossimo presidente del consiglio Letta) di cosa vogliamo parlare? Urge un lavoro dentro la sinistra, altrò che......

Quello che mi sembra più grave, ed urgente, è la sottovalutazione dell'italico fronte del NO. Quanti di noi, per esempio, hanno derubricato la manifestazione della famiglia ad epifenomeno del clericalismo italiano? Quanti si ostinano a classificare i tre cavalieri dell'apocalisse ferrara-roccella-dellaloggia come frutto intellettuale dell'italico eclettismo curiale finto laico e non come avanguardia culturale fintamente modernizzante di argomenti, e forze, vecchie e stantie? In altre parole: quanti di noi continuano a perdersi dietro le stronzate di Giovanardi senza capire la gravità profonda di quel che dice ("sono per i diritti umani ma ....") invece che organizzarsi, ed organizzare, il fronte del SI a partire dallo studio sistematico ed alla confutazione degli argomenti del fronte del NO?

Oltre ai tanti validi motivi che stanno alla base di questo serissimo ritardo culturale di chi ha a cuore i diritti in italia (dentro cui metto anche la stragrande maggioranza del cosiddetto movimento lgbt) ne riconosco uno tanto pernicioso quanto poco visibile. E parlo di quella pseudo-machiavellica considerazione di chi ritiene che tutti i cambiamenti sociali siano solo frutto di equilibri di potere. E quindi l'obiettivo è quello di abbattere lo strapotere vaticano in Italia per ottenere quelle riforme di cui abbiamo bisogno. La storia italiana è tutta lì a dimostrare quanto sia falsa questa impostazione, a volte inconscia nella classe dirigente del paese. Falsa e deresponsabilizzante rispetto al qui e ora, e al come si costruiscono le battaglie politico sociali. Con quali strumenti, con quali argomenti, con quali analisi affronteremo la tempesta che si profila anche nel nostro Paese? E sarà tempesta, spero ne siate consapevoli, anche sulle unioni civili a dispetto di tutti i soloni che negli anni han provato a convincerci che non si poteva chiedere il matrimonio, che non era tempo, che bisognava aspettare, che l'obiettivo era un altro....

Non abbiamo più tempo da perdere: la lezione francese è chiarissima. Si tratta di mettersi a lavorare insieme. O perlomeno insieme a chi ci sta.

Enzo Cucco

Associazione radicale certi diritti

http://gayindependent.blogspot.it/

(1) andate a rivedervi l'interessantissimo intervento di Joel al congresso di certi diritti di qualche giorno fa http://joel-lederoff.blogspot.be/2013/04/il-matrimonio-per-tutti-alla-francese.html