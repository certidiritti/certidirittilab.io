---
title: 'AAA Cercansi Coppie'
date: Fri, 14 Feb 2014 14:12:20 +0000
draft: false
---

UGUALI FAMIGLIE UGUALI DIRITTI
------------------------------

[![ICON_2](http://www.certidiritti.org/wp-content/uploads/2014/02/ICON_2.png)](http://www.certidiritti.org/wp-content/uploads/2014/02/ICON_2.png)Noi coppie che in Italia non possiamo unirci in matrimonio e/o accedere ad altre forme di convivenza riconosciute, e che per questo siamo privi dei diritti e dei doveri fondamentali per ciascuna famiglia. Noi famiglie i cui figli non hanno gli stessi diritti che hanno i figli nati e cresciuti in famiglie matrimoniali.

**Noi non ci arrendiamo di fronte ad un Parlamento ed a forze politiche che ignorano volutamente la nostra esistenza  e non riescono a riformare il diritto di famiglia riconoscendo il matrimonio egualitario e regolamentando diritti e doveri delle nostre famiglie.**

Noi che siamo costretti ad andare all’estero per poterci sposare o unire in patti di solidarietà, senza che questi vengano riconosciuti e comportino una assunzione piena di diritti e doveri anche in Italia.

**Condividiamo l’idea che insieme alla nostra felicità si debba combattere per la felicità di tutti e tutte, soprattutto nei Paesi come l’Italia ove questa possibilità è limitata da leggi cieche e ostili a riconoscere le diverse forme familiari che negli anni si sono moltiplicate.**

**Uniamo le nostre forze, insieme a quelle di tutte le associazioni e ai singoli che condivideranno le nostre richieste, per combattere, in Italia, una battaglia di civiltà, di dignità e di riconoscimento pieno dei diritti umani per noi e le nostre famiglie, con ogni mezzo e iniziativa non violenta.** **Riteniamo che, in attesa che il Parlamento italiano prenda seriamente e concretamente in considerazione questa realtà e legiferi, la strada delle cause pilota sia la via principale affinchè la nostra richiesta non resti inascoltata ma faccia passi avanti, anche se piccoli e parziali.** **Chiediamo alle coppie ed alle famiglie che si trovano nella nostra situazione di unirsi a noi nello sforzo di moltiplicare le cause pilota per rendere più incisiva e pressante l’unica strategia che, ad oggi, ha offerto qualche risultato concreto.** **Chiediamo alle associazioni di appoggiarci in questa battaglia, agli avvocati di sostenerci e al mondo dell’informazione di raccontare la nostra realtà, senza pietismo o approssimazioni.** **Chiediamo il sostegno di tutti i cittadini che condividono la necessità di riforma complessiva del diritto di famiglia che preveda la possibilità di celebrare il matrimonio civile per le coppie dello stesso sesso;la libertà per tutti/e di accedere a istituti più flessibili come le unioni libere, le intese di solidarietà e le comunità intenzionali, l’opportunità per tutti/e di accedere alle adozioni nazionali, internazionali e all’adozione del secondo genitore.** _**Noi, che non ci vogliamo arrendere all’inerzia del Parlamento, chiediamo a tutti e tutte di aiutarci, e di aiutare l’intera società italiana, ad uscire dalla menzogna dell’ideologia sessuofobica, omofobica e transfobica, qui ed ora a cominciare dalle nostre vite, dalle nostre richieste, dal nostro impegno pubblico.**_  

ADERISCI AL GRUPPO COPPIE
-------------------------

Inserisci i tuoi dati nel modulo sottostante per essere ricontattato al più presto

\[wufoo username="associazionelucacoscioni" formhash="ravexxv0uks28b" autoresize="true" height="974" header="show" ssl="true"\] \[gallery columns="5" link="file" ids="6025,6026,6027,6028,6030,6031,6032,6033,6034,6035,6036,6037,6038,6039,6040,6041,6042" orderby="rand"\]