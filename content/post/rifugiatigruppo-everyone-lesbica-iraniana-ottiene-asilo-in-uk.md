---
title: 'RIFUGIATI/GRUPPO EVERYONE: LESBICA IRANIANA OTTIENE ASILO IN UK'
date: Thu, 17 Jun 2010 07:26:51 +0000
draft: false
tags: [asilo politico, Comunicati stampa, Everyone Group, Gran Bretagna, IRAN, OMOFOBIA]
---

**17 giugno 2010**

È di questa notte la conferma ufficiale che **Kiana Firouz**, attrice e attivista lesbica fuggita dall'**Iran** nel 2008 e rifugiatasi nel Regno Unito, **ha ottenuto asilo definitivo in Gran Bretagna**.

Il [**Gruppo EveryOne**](http://www.everyonegroup.com/EveryOne/MainPage/MainPage.html) aveva lanciato l'allarme [meno di un mese fa](http://www.everyonegroup.com/it/EveryOne/MainPage/Entries/2010/5/6_Campagna_per_la_vita_diKiana,_lesbica_iraniana_a_rischio_deportazione_del_Regno_Unito.html) a livello internazionale, mobilitando alcuni membri del Parlamento europeo e inoltrando alla Border Immigration Agency e al Governo britannico un appello a seguito del rigetto, da parte dell'Home Office e del giudice di primo grado, della richiesta di asilo come rifugiata.

Kiana, che recentemente ha realizzato un cortometraggio sulla condizione degli omosessuali in Iran, prendendo parte alle scene del film "[**Cul de sac**](http://www.washingtonbanglaradio.com/content/5862810-ramin-goudarzinejad-mahshad-torkans-cul-de-sac-2010-film-about-lesbian-iran)" in uscita nelle sale dal 20 maggio scorso, rischiava infatti la deportazione in Iran, dove quasi certamente sarebbe stata condannata a morte per la propria condizione.

"In seguito al nostro appello a istituzioni e autorità europee, altre organizzazioni, tra cui **Amnesty International, Certi Diritti e Arcigay**, avevano amplificato la nostra voce, contribuendo a divulgare il caso" dichiarano i co-presidenti di EveryOne, **Roberto Malini, Matteo Pegoraro e Dario Picciau**.

"Ancora una volta, grazie all'intervento dell'attivismo, **trionfa la politica dei Diritti Umani** e un'altra vita è salva dalla persecuzione in Patria. Nonostante il difficile clima intimidatorio che, soprattutto in Italia, ci ostacola nel nostro lavoro quotodiano," aggiungono gli attivisti, "campagne vittoriose come questa ci inducono ad andare avanti a testa alta, certi che la via dei Diritti Umani, del dialogo e della salvaguardia di ogni essere umano siano superiori a ogni violenza, ogni minaccia, ogni persecuzione".

"Noi, iraniani LGBT, siamo orgogliosi di non essere soli" ha dichiarato, non appena appresa la notizia, Arsham Parsi, direttore dell'organizzazione [**Iranian Queer Railroad for Refugees**](http://www.irqr.net/). "Siamo felici che vi siano milioni di persone che sono seriamente preoccupati per la nostra situazione e che ci supportano ogniqualvolta ne abbiamo bisogno".

"Ringraziamo tutte le realtà che si sono mobilitate in difesa della vita di Kiana" concludono Malini, Pegoraro e Picciau, "e ci auguriamo che il Regno Unito, nonché gli altri stati Membri dell'Unione, d'ora in avanti prestino maggiore attenzione alle esigenze di protezione umanitaria delle minoranze perseguitate, mettendo al primo posto, davanti alla politica interna e alla burocrazia, la salvaguardia della vita umana".  
**  
Per ulteriori informazioni:  
**Gruppo EveryOne  
+39 393 4010237 :: +39 331 3585406  
[info@everyonegroup.com](mailto:info@everyonegroup.com) :: [www.everyonegroup.com](http://www.everyonegroup.com/)