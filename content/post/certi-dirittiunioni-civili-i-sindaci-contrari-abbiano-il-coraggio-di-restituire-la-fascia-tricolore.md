---
title: 'CERTI DIRITTI/UNIONI CIVILI: I SINDACI CONTRARI ABBIANO IL CORAGGIO DI RESTITUIRE LA FASCIA TRICOLORE.'
date: Thu, 06 Oct 2016 20:06:35 +0000
draft: false
tags: [Diritto di Famiglia]
---

[![mani-1024x683](http://www.certidiritti.org/wp-content/uploads/2016/07/mani-1024x683-300x200.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/07/mani-1024x683.jpg)Si stanno moltIplicando le dichiarazioni di alcuni Sindaci che si dicono contrari alla Legge sulle Unioni Civili e che vorrebbero non applicarla. Alcuni mettendo i bastoni tra le ruote di chi sceglie di unirsi (scegliendo luoghi e ore improbabili per le registrazioni e amenità del genere), altri si trincerano ancora dietro un generico e insopportabile «mancano i decreti attuativi», quando invece il comma 35 della Legge dice espressamente: «_Le disposizioni di cui ai commi da 1 a 34 acquistano efficacia a decorrere dalla data di entrata in vigore della presente legge»_. È chiaro, tra l’altro, come questa disposizione renda passibile di denuncia chiunque si opponga all’effettiva efficacia della Legge stessa.

Altri Sindaci ancora, e questo è il caso più grave e ridicolo insieme, hanno addirittura dichiarato che non rilasceranno alcuna delega agli ufficiali di stato civile affinché facciano le registrazioni.

Su tutti quei comportamenti che impediscono, direttamente o indirettamente, l'applicazione della Legge chiediamo che i Prefetti, così come erano super vigili nei confronti dei Sindaci che volevano trascrivere, siano altrettanto solerti ora e intervengano affinché la Legge sia applicata _in toto_.

Sulle dichiarazioni in merito al rilascio della delega chiediamo invece agli operatori e alle operatrici dei media di riflettere qualche minuto in più: la Legge sulle Unioni civili parla espressamente di  “ufficiale di stato civile” e non di Sindaci. I Sindaci, come noto, sono ufficiali, ma delegano questa funzione ad altra persona o persone, dell’amministrazione. E questa delega non è specifica per ciascuna delle attività che un ufficiale deve svolgere, bensì generica, perché fa riferimento alla funzione e non alle singole attività.Quindi non esiste nessuna delega alle unioni civili, e mai potrà esistere. Quei Sindaci che dicono il contrario mentono sapendo di mentire.

Se invece i Sindaci fossero coerenti fino in fondo dovrebbero adottare un provvedimento che escluda la registrazione delle unioni civili dai compiti dell’ufficiale. Cosa che non sta né in cielo né in terra e sarebbe immediatamente fonte di intervento sostitutivo del Prefetto e di denuncia.

Inoltre, come ha chiarito il Consiglio di Stato e chiunque legga correttamente la legge e si intende minimamente di giurisprudenza, **non esiste un diritto all’obiezione di coscienza** sulla Legge Cirinnà. Se qualcuno lo pensa davvero dovrebbe fare come i veri obiettori che andavano in galera per sostenere le proprie idee. Ci sono quindi Sindaci disponibili a portare le proprie dichiarazioni alle logiche conseguenze? Ci sono quindi Sindaci disposti a restituire la fascia tricolore?

Non è qui in gioco la libertà di pensiero, tanto a sproposito richiamata in questi casi, ma la coerenza, e questi Sindaci dimostrano di non averla. Hanno bensì un fiuto certo per la comunicazione. Ormai è noto: straparlando contro le persone omosessuali e transessuali e contro la  Legge sulle Unioni Civili si finisce sui giornali.

_Roma, 7 ottobre 2016_