---
title: 'GIORNATA MONDIALE CONTRO OMOFOBIA: CERTI DIRITTI  RICEVUTA AL QUIRINALE'
date: Thu, 14 May 2009 11:37:12 +0000
draft: false
tags: [Comunicati stampa]
---

**GIORNATA MONDIALE CONTRO OMOFOBIA: L’ASSOCIAZIONE RADICALE CERTI DIRITTI LUNEDI’ 18-5 SARA’ RICEVUTA AL QUIRINALE. MOBILITAZIONE IN TUTTA ITALIA CON INIZIATIVE ED EVENTI A MILANO, TORINO, ROMA, GENOVA, BOLOGNA, PARMA, LECCE, CATANIA....  
**

In occasione della giornata mondiale contro l’omofobia che si terrà domenica 17 maggio, l’Associazione Radicale Certi Diritti, anche nello spirito dello slogan del Pride di Mosca "Parità senza compromessi" che si svolge lo stesso giorno, ha promosso in tutta Italia eventi, manifestazioni e iniziative volte a sensibilizzare l’opinione pubblica sul tema della violenza e delle persecuzioni contro le persone transessuali, omosessuali.

**Lunedì 18 maggio, alle ore 18, una delegazione di Certi Diritti sarà ricevuta al Quirinale.** Durante l’incontro verranno consegnati dossier e documenti sulle gravi condizioni di vita e sulle persecuzioni delle persone lesbiche, gay e transessuali, ancora oggetto di violenza nel nostro paese e la grave situazione dei migranti che richiedono lo status di rifugiato in Italia perché omosessuali perseguitati nei loro paesi di origine.

**Gli eventi in programma a Roma:**

**\- Venerdì 15 maggio, dalle ore 19** Certi Diritti Roma, al Caffè Letterario (Via Ostiense, 65) Homo, Homini, Homophobiucs, serata con drag queen, documentari e interventi sui diritti civili con esponenti della comunità lgbt;  
**\- Sabato 16 maggio, alle ore 11.30,** Conferenza Stampa di Certi Diritti in Piazza San Giovanni con performance del coordinamento Boarosa (riconsegna della Piazza ai cittadini italiani); durante la iniziative verranno illustrate le iniziative in corso contro l’omofobia e la transfobia. Parteciperanno Lucky Amato, coordinatore radicale Certi Diritti e Sergio Rovasio, Segretario Ass.ne Certi Diritti e candidato circ.ne centro Lista Bonino-Pannella insieme ai parlamentari Pasqualina Napolitano (deputata europea e candidata per Sinistra e Libertà) e Marco Perduca (Senatore radicale nel Pd).

**Cos’è l’omofobia e la transfobia e perché il 17 maggio:**

L'omofobia e la transfobia sono un insieme di emozioni e sentimenti come ansia , disgusto, avversione, paura e disagio, che taluni provano in maniera conscia o inconscia nei confronti di gay e lesbiche".

La diffusione dell' omofobia è molto variabile , in quanto essa dipende da fattori quali l'atteggiamento dei mass media, , le leggi persecutorie, l'incidenza delle credenze religiose integraliste, il grado di scolarizzazione, il tasso di maschilismo, il sesso (gli uomini sono più omofobici delle donne), ecc. In Italia l' omofobia è molto diffusa.

Dal 17 maggio 1990, giorno in cui l' Assemblea generale dell' Organizzazione mondiale della sanità (Oms) ha eliminato l' omosesualità dalla lista delle malattie mentali, in tutto il mondo si celebra la giornata contro l'omofobia. Il 17 maggio è diventato un momento di riflessione e azioni allo scopo di lottare contro ogni violenza fisica, morale o simbolica legata all' orientamento sessuale.