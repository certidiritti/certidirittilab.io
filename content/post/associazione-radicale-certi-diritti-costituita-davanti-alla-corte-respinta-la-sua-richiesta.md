---
title: 'ASSOCIAZIONE RADICALE CERTI DIRITTI COSTITUITA DAVANTI ALLA CORTE, RESPINTA LA SUA RICHIESTA'
date: Wed, 24 Mar 2010 07:41:04 +0000
draft: false
tags: [Comunicati stampa]
---

9CO304605 4 POL ITA R01

MATRIMONIO GAY, CERTI DIRITTI COSTITUITA DAVANTI CONSULTA

(9Colonne) Roma, 23 mar - L'Associazione Radicale Certi diritti si è

costituita davanti alla Corte Costituzionale per sostenere le ragioni delle

illegittimità costituzionale del divieto di contrarre il matrimoniale per le

persone dello stesso sesso. Questa mattina la prima udienza. La Corte

Costituzionale dovrà decidere se i Comuni che hanno impedito le pubblicazioni

di matrimonio di coppie dello stesso sesso hanno agito in contrasto con l'art.

2 (che tutela la persone), l'art. 3 (che garantisce l'uguaglianza e vieta le

discriminazioni), l'art. 29 (cha tutela la famiglia, da interpretare in senso

evolutivo), l'art. 117, 1 comma (che rende operanti le norme del diritto

europeo e comunitario). Se la corte deciderà in senso positivo, le coppie

omosessuali potranno sposarsi. Il collegio di difesa dell'Associazione

Radicale Certi Diritti è composto da Marilisa D'Amico, che insegna Diritto

costituzionale alla Statale di Milano e dagli avvocati Massimo Clara,

Francesco Bilotta, Alessandro Giadrossi ed Ileana Alesso.

ADN0119 5 CRO 0 ADN CRO NAZ

GAY: ASSOCIAZIONE RADICALE 'CERTI DIRITTI' SI E' COSTITUITA DAVANTI A CORTE COSTITUZIONALE =

PRESENTE OGGI IN UDIENZA IL SEGRETARIO SERGIO ROVASIO

Roma, 23 mar. - (Adnkronos) - L'Associazione Radicale Certi

diritti si e' costituita davanti alla Corte Costituzionale per

sostenere le ragioni dell'illegittimita' costituzionale del divieto di

contrarre il matrimonio per le persone dello stesso sesso. Questa

mattina la prima udienza.

''La Corte Costituzionale -si legge in una nota- dovra' decidere

se i Comuni che hanno impedito le pubblicazioni di matrimonio di

coppie dello stesso sesso hanno agito in contrasto con l'art.2 (che

tutela la persona), l'art. 3 (che garantisce l'uguaglianza e vieta le

discriminazioni), l'art. 29 (cha tutela la famiglia, da interpretare

in senso evolutivo), l'art. 117, 1 comma (che rende operanti le norme

del diritto europeo e comunitario)''.

Se la corte decidera' in senso positivo, come ricorda una nota

dell'Associazione, ''le coppie omosessuali potranno sposarsi.Il

collegio di difesa dell'Associazione Radicale Certi Diritti e'

composto dall'avv. Prof. Marilisa D'Amico, che insegna Diritto

Costituzionale alla Statale di Milano, e dagli avvocati Massimo Clara,

Francesco Bilotta, Alessandro Giadrossi e Ileana Alesso''.

MAW9087 4 cro gn00,rg00 427 ITA0087;

Apc-Gay/ Matrimonio, conclusa udienza Corte costituzionale

\[1\]Decisione potrebbe arrivare in giornata

Roma, 23 mar. (Apcom) - E' terminata intorno alle 11 l'udienza

presso la Corte costituzionale sul procedimento nel quale tre

coppie omosessuali contestano la impossibilità di sposarsi come

illegittima e in contrasto con la Carta fondamentale. I giudici,

che potrebbero esprimersi già oggi, stanno valutando se ammettere

nel procedimento l'associazione radicale Certi diritti, che si è

costituita come interveniente.

ADN0484 5 CRO 0 ADN CRO NAZ

GAY: CONSULTA, CONCLUSA UDIENZA SUI MATRIMONI OMOSEX =

'NEL POMERIGGIO CAMERA DI CONSIGLIO, SENTENZA ATTESA IN

GIORNATA'

Roma, 23 mar. - (Adnkronos) - Si e' conclusa al Palazzo della

Consulta l'udienza dei giudici costituzionali presieduta da Francesco

Amirante, dedicata al tema dell'ammissibilita' dei matrimoni fra

individui dello stesso sesso. In particolare, i giudici della Consulta

sono chiamati a pronunciarsi sul ricorso presentato da tre coppie gay

contro le decisioni prese dal Tribunale di Venezia e dalla Corte

d'Appello di Trento di giudicare legittimo il rifiuto di procedere

alla pubblicazione di matrimonio per coppie formate da individui dello

stesso sesso, dopo la richiesta presentata nei comuni di Venezia e di

Trento.

L'udienza mattutina ha permesso ai giudici della Corte

Costituzionale di ascoltare le posizioni espresse dai legali delle

coppie gay e dall'Avvocatura generale dello Stato: i primi in favore

di un intervento della Consulta che cancelli quella che definiscono

coma una discriminazione sessuale; la seconda a ribadire la competenza

del legislatore, ovvero del Parlamento, in materia di matrimonio per

il quale secondo quanto scritto nella stessa Costituzione resta

elemento necessario la diversita' di sesso fra i due individui.

Per l'Avvocatura dello Stato, e' intervenuta l'avvocato

Gabriella Palmieri mentre i legali Vittorio Angiolini, Vincenzo Zeno

Zencovich, Marilisa D'Amico e Massimo Clara hanno parlato in

rappresentanza delle coppie gay che hanno presentato i ricorsi. Non e'

stata accettata come 'interveniente', invece, l'Assocazione Radicale

Certi Diritti, rappresentata peraltro dagli stessi avvocati scelti

dalle parti. La camera di consiglio e' convocata per il primo

pomeriggio e' la sentenza e' attesa presumibilmente entro la stessa

giornata.

(Bon/Ct/Adnkronos)

23-MAR-10 13:39