---
title: 'Unioni civili: Alemanno si inventa che Comuni non hanno competenza. Pronti a raccogliere firme'
date: Sun, 22 Jan 2012 11:06:30 +0000
draft: false
tags: [Diritto di Famiglia]
---

Il sindaco di Roma si inventa che non spetta ai comuni intervenire. Il mulino bianco è un cartone animato, la realtà è un'altra. Pronti a raccogliere firme su una proposta come quelle di Torino e Napoli.

Roma, 22 gennaio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti e Radicali Roma

Il Sindaco di Roma Gianni Alemanno si è inventato che il riconoscimento dei diritti delle coppie conviventi ‘è un tema tipicamente nazionale e che è il Parlamento che deve decidere se è ammissibile un’idea del genere’, poi, non contento ha aggiunto che: ‘è sbagliato che i comuni si intromettano’. Non sappiamo se la sua è ignoranza o semplicemente obbedienza ai voleri della casta clericale, sappiamo però che egli mente sapendo di mentire, e questo è certamente ancora più grave per un Sindaco che fa finta di non conoscere i campi di intervento della sua amministrazione.

I Comuni hanno molte competenze sulle coppie conviventi, sia quelle eterosessuali che quelle gay, sposate e non, a partire dagli aiuti per coloro che hanno bambini e che si trovano in difficoltà economiche e/o che vivono in situazioni di difficoltà.

Al Sindaco di Roma basterebbe poco per conoscere quello che hanno già fatto altri Comuni sulle unioni civili: da Torino a Napoli, ad esempio, che recentemente hanno approvato atti amministrativi per offrire maggiori tutele e garanzie avendo come riferimento la legge sulla famiglia anagrafica del 1989.

Annunciamo sin d’ora che nei prossimi giorni partirà a Roma una proposta di Delibera di iniziativa popolare che verrà proposta anche alle associazioni cittadine che si battono per i diritti civili, per avere anche nella Capitale d’Italia una legge adeguata ai cambiamenti socio-familiari e che estenda il riconoscimento dei diritti alle coppie conviventi gay ed eterosessuali, che includerà anche la lotta ad ogni forma di discriminazione. La famiglia del mulino bianco è un cartone animato e il Sindaco Alemanno dovrà farsene una ragione visto che la realtà della città è un’altra.