---
title: 'UNIVERSITA'' DEGLI STUDI STATALE, COLLETTIVO GAY NEL MIRINO'
date: Mon, 15 Mar 2010 18:49:45 +0000
draft: false
tags: [Senza categoria]
---

MILANO. STATALE, COLLETTIVO GAY NEL MIRINO "PUNIREMO CHI VIOLA I LORO DIRITTI"  
Militante aggredito mentre attacca volantini, insulti durante le riunioni: il caso va al Senato accademico  
sabato 13 marzo 2010 , La Repubblica-Milano

di FRANCO VANNI

Prima i manifesti strappati dalle pareti, poi gli insulti urlati in pubblico: «Voi froci siete la feccia dell´umanità». È successo al polo di Biologia dell´università Statale, in via Celoria. Giacomo, studente di 19 anni, stava attaccando in bacheca volantini che promuovono una rassegna di cinema gay, quando un giovane lo ha avvicinato: «Attaccane un altro e ti ammazzo di botte - ha gridato - per voi malati qui non c´è posto». Era l´ora di pranzo di giovedì. Alla scena, durata diversi minuti, hanno assistito alcuni studenti, che hanno espresso solidarietà a Giacomo. Lo stesso hanno fatto ieri professori e ricercatori. Lo studente ora si dice «scosso e allibito». Della questione, di cui è informato il comitato Pari opportunità dell´università, si discuterà martedì nella riunione del Senato accademico.

Per la studentessa Angelica Vasile, nel comitato, «purtroppo le segnalazioni simili in università non sono così rare: le riunioni del collettivo gay sono disturbate da insulti, e i volantini vengono strappati». Gli episodi di omofobia sono stati raccolti in una relazione inviata al comitato di cui fa parte la preside di Agraria, Claudia Sorlini: «Simili atti sono inaccettabili - dice - serve un´indagine rapida che individui i responsabili e, se ci sono gli estremi, decida provvedimenti esemplari». Il regolamento disciplinare della Statale prevede come massime punizioni la sospensione dagli studi fino a un anno o l´espulsione. A esporre la questione in Senato sarà la studentessa Alessandra Brambati: «Qualunque sarà la decisione - dice - manderemo una mail a tutti gli studenti per informarli».

Il collettivo Gay Statale riunisce 220 fra studenti e studentesse e propone iniziative finanziate in parte dall´università. «Si deve garantire che la civile convivenza non sia compromessa - dice Enrico, il portavoce - anche perché la Statale si è sempre dimostrata attenta». Dallo scorso anno, l´ateneo consente agli studenti che cambiano sesso di registrare sul libretto il nuovo nome