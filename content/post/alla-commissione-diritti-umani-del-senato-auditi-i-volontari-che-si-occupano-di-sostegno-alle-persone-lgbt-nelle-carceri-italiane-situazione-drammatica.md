---
title: 'Alla Commissione Diritti Umani del Senato auditi i volontari che si occupano di sostegno alle persone lgbt nelle carceri italiane. Situazione drammatica'
date: Wed, 28 Sep 2011 19:39:15 +0000
draft: false
tags: [Politica]
---

Audizione in Commissione Diritti Umani del Senato su condizione omosessuali e transessuali nelle carceriu italiane: situazione estremamente grave.

Associazione radicale Certi Diritti chiede interventi mirati per evitare casi di violenze, assistenza adeguata e protezione.

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

“La Commissione Diritti Umani del Senato ha oggi audito alcuni esperti che si occupano di assistenza alle persone omosessuali e transessuali detenute nelle carceri italiane. Gli ultimi dati diffusi oggi in Commissione riferiscono di 104 persone transessuali e omosessuali che si trovano in reparti  ‘protetti’. Ovviamente i detenuti omosessuali sono molti di più ma per ragioni di sicurezza non si dichiarano. Purtroppo molti di loro vivono in condizioni pessime perché devono tenere nascosta la loro condizione e anche perché le strutture carcerarie non garantiscono la protezione e l’assistenza necessaria.

Durante le visite ispettive svolte dai parlamentari Radicali, in particolare della deputata Rita Bernardini e Marco Perduca, in questi ultimi mesi sono state riscontrate carenze molto gravi sul piano dell’assistenza sanitaria del tutto insufficiente per le persone transessuali. Alcune strutture carcerarie non hanno alcun reparto per loro.  Occorre dare atto che grazie al lavoro di volontariato di alcune Associazioni come il Mario Mieli di Roma, il Mit, Movimento identità transessuali e l’Associazione Libellula di Roma, vengono promosse dentro il carcere alcune attività di aiuto e sostegno.

In questi ultimi mesi con la deputata Rita Bernardini, Presidente di Certi Diritti, abbiamo visitato alcuni dei 17 reparti ‘protetti’ per le persone transessuali. Nelle prossime settimane abbiamo previste le visite nei reparti transessuali di  Napoli, Belluno e Milano, tra i più numerosi. Occorre che le Asl si attivino con la massima urgenza per garantire alcuni tra i più elementari diritti umani".