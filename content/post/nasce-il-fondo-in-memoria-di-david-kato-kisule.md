---
title: 'Nasce il fondo in memoria di David Kato Kisule'
date: Wed, 13 Apr 2011 17:09:58 +0000
draft: false
tags: [Africa]
---

Arcigay, Certi Diritti e Non c'è Pace senza Giustizia hanno creato un fondo in memoria dell'attivista gay ugandese, iscritto di Certi Diritti, per garantire un processo giusto sulla sua morte e sostenere il movimento lgbti in Uganda.

Roma, 11 aprile 2011

Il 26 gennaio del 2011, l’attivista ugandese e difensore dei diritti umani, David Kato Kisule, esponente della comunità LGBTI e iscritto a Certi Diritti, è stato trovato assassinato nella sua residenza a Kyetume, nel distretto di Mukono, Uganda.  
E’ dedicato a lui il Fondo che servirà a sostenere le spese per garantire un processo equo, efficace, imparziale e che permetta di stabilire la verità sulla sua morte.  
In Uganda le autorità governative e i sostenitori del fondamentalismo religioso,sostenuti da alcuni settori della destra americana, continuano a istigare all’odio contro coloro che appartengono alla comunità LGBTI. In questo scenario il Governo ugandese non ha accantonato la proposta di legge che propone la pena di morte per gli omosessuali “recidivi”.  
Nonostante la situazione di pericolo per tutti gli attivisti LGBTI ugandesi, l’associazione Sexual Minorities Uganda (SMUG), di cui faceva parte David Kato Kisule, continua il suo impegno.  Arcigay, l'Associazione Radicale Certi Diritti, Non c'è Pace Senza Giustizia con il consenso di SMUG hanno creato un fondo in memoria dell’attivista barbaramente ucciso e che aveva partecipato, prima di morire, al Congresso di Certi diritti, chiedendo aiuto per il suo Paese.  
In questo modo le tre associazioni si impegnano a non lasciare soli gli amici e compagni ugandesi e a contribuire concretamente  a questa battaglia di civiltà.  
  
**[FAI UNA DONAZIONE ONLINE >](campagne/in-memoria-di-david-kato/1071-fondo-in-memoria-di-david-kato-kisule.html)**  
  
Nei siti di Arcigay, Certi Diritti e Non c'è Pace Senza Giustizia è possibile tramite il sistema Paypal fare i versamenti con Carta di Credito, specificando nella causale: Fondo David Kato Kisule.  
Il conto bancario del Fondo è il seguente:  
**Con bonifico bancario sul c/c n 12691/24  
IBAN: IT52 V030 6905 0310 0000 1269 124  
ABI: 03069 - CAB:05031  Cariplo Banca Intesa BCI  
Intestato al Comitato Non c’e Pace Senza Giustizia  
Piazza Colonna Agenzia 1 (Roma) Italia**