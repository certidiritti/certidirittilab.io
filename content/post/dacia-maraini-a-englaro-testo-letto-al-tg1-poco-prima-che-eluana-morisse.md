---
title: 'DACIA MARAINI A ENGLARO: TESTO LETTO AL TG1 POCO PRIMA CHE ELUANA MORISSE'
date: Tue, 10 Feb 2009 15:12:23 +0000
draft: false
tags: [Comunicati stampa, dacia maraini, englaro, libertà]
---

Il Tg1 delle ore 20, di lunedì 9 febbraio, poco prima di dare l'annuncio della morte di Eluana Englaro, con Dacia Maraini ospite in studio, ha dato lettura di una sua lettera a Beppino Englaro, padre di Eluana.

Di seguito il testo:

> Caro Englaro,
> 
> fino a ieri mi chiedevo perché non mostrasse una foto, anche una sola foto di sua figlia come è oggi.
> 
> Sono sicura che la gente capirebbe meglio. A costo di perdere in credibilità, lei evita di rendere pubblica un'immagine dolorosa e infelice di sua figlia. Lei avrebbe potuto mostrare quel corpo, e non l'ha fatto.  
> 
> Avrebbe potuto portare all'estero la figlia amata, e non l'ha fatto. Avrebbe potuto acconsentire alla pratica comune in tutti i nostri ospedali dove i parenti, in accordo con i medici, lasciano che i loro cari ormai perduti sino sepolti in pace, ma non l'ha fatto. E questo garantisce della sua buona fede.
> 
> Eppure c'è chi in prepotenza vuole decidere per gli altri in base a principi astratti, ed è pronto a denigrarla e falsificare la realtà per affermare le proprie idee. Per questo lei oggi viene perseguitato e accusato della peggiore delle ignominie: quella di voler assassinare sua figlia. Chiunque a questo punto si sarebbe arreso, lei no. anzi Decide con più fermezza di agire secondo giustizia e secondo pietà. Di questo la ringrazio, perché la sua risolutezza costituisce un esempio di grande civiltà e coerenza.
> 
> Mi auguro che dopo questo caso la scelta di una morte dignitosa sarà considerato un diritto e non un colpo del boia, qualcosa che le persone disperate possano desiderare per i loro cari, quando non c'è più niente da fare per riportarle al mondo.
> 
> Dacia Maraini