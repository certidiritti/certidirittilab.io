---
title: 'La Commissione del Parlamento Europeo sulle libertà civili ha adottato una raccomandazione per una roadmap europea contro omofobia e transfobia'
date: Tue, 17 Dec 2013 15:12:54 +0000
draft: false
tags: [Europa]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti.

Roma, 17 dicembre 2013

Stamane la Commissione del Parlamento Europeo per le Libertà Cvili, la Giustizia e gli Affari Interni ha adottato una raccomandazione per una futura roadmap europea contro le discriminazioni basate sull'orientamento sessuale e l'identità di genere. L'auspicio è che l'UE decida di integrare le strategie già esistenti (integrazione dei Rom, discriminazioni basate sulla disabilità e eguaglianza di genere) una strategia complessiva a favore dell'eguaglianza dei cittadini europei LGBTI. Più precisamente la Commissione parlamentare chiede alla Commissione Europea di fare proposte concrete per attuare politiche coerenti, nel pieno rispetto del principio di sussidiarietà per il quale l'Unione e gli Stati membri devono rispettare le reciproche competenze, contro la discriminazione sul lavoro, nel campo dell'educazione, della salute, dell'accesso a beni e servizi, nel campo della cittadinanza, delle famiglie e della libertà di movimento, per la libertà di assemblea e d'espressione, contro i discorsi e i crimini d'odio, nel campo dell'asilo e della migrazione, monche nel campo degli affari esteri. Il voto odierno segue altre decisioni simili prese dal Parlamento europeo negli ultimi tre anni e dalla persa di posizione, nel maggio di quest'anno, di undici Stati Membri, tra cui l'Italia, a favore di una roadmap.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: «Mentre l'Italia si avvita attorno al surreale dibattito sulla Legge mancino-Reale, la Commissione del Parlamento Europeo per le Libertà Cvili, la Giustizia e gli Affari Interni ci ha dato oggi una grande lezione con un voto ampio e trasversale, che speriamo venga confermato nel 2014 in plenaria, a favore di un approccio coerente a favore dei diritti umani delle persone LGBTI che attraversi le varie aree di competenza politica dei commissari europei. Questo è l'approccio giusto, non una mera ingegneria penalistica. Il Parlamento italiano è ancora in tempo a inserire un vero piano contro le discriminazioni basate sull'orientamento sessuale, l'identità e l'espressione di genere, nella Legge Mancino-Reale in discussione al Senato. Cerchiamo di essere coerenti con le posizioni prese dall'Italia in Europa e dai parlamentari italiani al Parlamento Europeo!».