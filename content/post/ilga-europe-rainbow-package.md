---
title: 'ILGA-Europe Rainbow Package'
date: Thu, 29 May 2014 14:40:32 +0000
draft: false
---

[![logo](http://www.certidiritti.org/wp-content/uploads/2014/05/logo-300x173.png)](http://www.certidiritti.org/wp-content/uploads/2014/05/logo.png) Come ogni anno, ILGA-Europe ha presentato il **_"Rainbow Package"_**, un rapporto annuale che fotografa la situazione europea per quel che riguarda i diritti delle persone LGBTI nel continente. Il "pacchetto" comprende:

*   la famosa **Mappa Rainbow**, che rappresenta graficamente le condizioni dei vari Paaesi dell'area europea rispetto alle politiche LGBTI;
*   **[Il Rapporto annuale ILGA-Europe 2014 (inglese)](http://www.certidiritti.org/wp-content/uploads/2014/05/Annual-Review-2014-web-version.pdf)** che offre una lettura più ampia e ragionata dei dati della Mappa Rainbow (qui il **[Rapporto annuale 2014 sull'Italia in italiano](http://www.certidiritti.org/wp-content/uploads/2014/05/annual-review-2014-Italia.pdf)**)

[![Side A - Rainbow Europe Map May 2014](http://www.certidiritti.org/wp-content/uploads/2014/05/Side-A-Rainbow-Europe-Map-May-2014-1024x739.png)](http://www.certidiritti.org/wp-content/uploads/2014/05/Side-A-Rainbow-Europe-Map-May-2014.png) [![Side B - Rainbow Europe Index may 2014](http://www.certidiritti.org/wp-content/uploads/2014/05/Side-B-Rainbow-Europe-Index-may-2014-1024x739.png)](http://www.certidiritti.org/wp-content/uploads/2014/05/Side-B-Rainbow-Europe-Index-may-2014.png)