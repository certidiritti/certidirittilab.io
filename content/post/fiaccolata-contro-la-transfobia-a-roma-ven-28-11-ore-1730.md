---
title: 'FIACCOLATA CONTRO LA TRANSFOBIA A ROMA, VEN 28-11 ORE 17.30'
date: Wed, 26 Nov 2008 08:32:56 +0000
draft: false
tags: [Comunicati stampa]
---

INIZIATIVA CONTRO LA TRANSFOBIA A ROMA  
   
Dopo l'ennesimo omicidio di una persona transessuale, avvenuto a Roma, il 25 novembre, abbiamo promosso una fiaccolata contro il pregiudizio, l'ignoranza e la drammatica condizione di molte persone transessuali e della violenza di cui sono vittime.  
   
Venerdì 28 novembre, alle ore 17.30, faremo una fiaccolata sulla scalinata del Campidoglio. Invitiamo ciascuno a partecipare, aderire e diffondere la notizia. Hanno già aderito all'iniziativa alcune associaizoni, parlamentari, personalità politiche e della società civile.  
   
Per adesione e preannunci di partecipazione scriveteci:  
[presidenza@libellula2001.it](mailto:presidenza@libellula2001.it)  
   
Con preghiera di divulgare a tutti i Vostri contatti ed a quanti possano essere interessati.  
   
A presto. Un caro saluto,  
   
Sergio Rovasio  
   
   
   
   
TESTO DIFFUSO SUGLI OBIETTIVI DELLA FIACCOLATA:  
   
STOP alla violenza contro le persone transessuali  
STOP alla discriminazione  
STOP all'ignoranza dei Media  
   
Essere transessuali oggi in Italia vuol dire essere oggetto di derisione, di persecuzione, di discriminazione e persino di omicidio!  
Le istituzioni ci puniscono e ignorano i nostri problemi.  
Nessuna risorsa in aiuto delle persone transessuali in campo giuridico, sanitario, sociale. Solo multe alle trans MtF perché si presume che siano prostitute.  
I media ci descrivono come malati pericolosi.  
Gli articoli di cronaca si riferiscono sempre alla persona transessuale ignorandone l'identità di genere, solo per alimentare pregiudizi morbosi ed infondati.  
La gente ci odia, fino ad ammazzarci.  
Si moltiplicano sempre più drammaticamente le aggressioni violente: l'Italia è seconda nel mondo per i crimini contro le persone transessuali.  
   
Una società sana, civile, umana deve dire basta all'ipocrisia, alla disinformazione, all'odio.  
   
Il 28 novembre alle 17,30 fiaccolata davanti alla scalinata del Campidoglio.  
Per dire basta a questa vergogna.  
Invitiamo le Associazioni ad aderire ed a dare ampia diffusione.  
   
Organizzano:  
Associazione Libellula Roma e Napoli  
Coordinamento Sylvia Rivera  
CGIL Nuovi Diritti  
Associazione Radicale Certi Diritti  
Associazione Streghe da Bruciare