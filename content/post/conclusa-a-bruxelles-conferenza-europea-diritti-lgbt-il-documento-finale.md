---
title: 'CONCLUSA A BRUXELLES CONFERENZA EUROPEA DIRITTI LGBT, IL DOCUMENTO FINALE'
date: Fri, 18 Apr 2008 17:08:06 +0000
draft: false
tags: [Comunicati stampa]
---

AI LAVORI HANNO PARTECIPATO PARLAMENTARI EUROPEI DI DIVERSI GRUPPI POLITICI, RAPPRESENTATI DI HUMAN RIGHTS WATCH, ILGA EUROPE, ILGA WORLD, ESPERTI, STUDIOSI, RICERCATORI, DOCENTI UNIVERSITARI, L’AGENZIA ONU PER I RIFUGIATI UNCHR, COMMISSIONE EUROPEA, PARTITO RADICALE NONVIOLENTO, ASSOCIAZIONI ED ESPONENTI DEL MONDO LGBT DI BELGIO, OLANDA, ITALIA, POLONIA, GRAN BRETAGNA, SPAGNA, L’INTERGRUPPO LGBT DEL PARLAMENTO EUROPEO.

L’Associazione radicale Certi Diritti con Sergio Rovasio (Segretario), Matteo Pegoraro (membro del Direttivo) e Marco Perduca (Senatore Radicale del PD e membro del Direttivo) sono intervenuti alla Conferenza ALDE, sui diritti LGBT svoltasi a Bruxelles nella sede del Parlamento europeo il 17 Aprile 2008.

La Conferenza, organizzata da Marco Cappato (Eurodeputato radicale) ed Ottavio Marzocchi (membro del Direttivo certi diritti), ha analizzato, con esperti e rappresentati di Ong provenienti da tutta Europa, l'affermazione dei diritti LGBT nei paesi membri dell’Ue e in altri paesi del mondo. Sono state documentate te le forme di repressione dei Gay Pride in alcuni paesi. Durante i lavori sono state oggetto di dibattito e confronto la futura direttiva sulle discriminazioni, le norme e prassi nazionali ed europee sul diritto d'asilo per le persone LGBT, la libertà di circolazione delle coppie dello stesso sesso nell'UE e l'applicazione del principio del mutuo riconoscimento dei matrimoni e delle unioni civili tra gli Stati membri. Un'azione per assicurare la protezione dei diritti delle persone LGBT é particolarmente necessaria a livello internazionale: ben sette Stati prevedono ancora la pena di morte per atti omosessuali mentre in altri ottanta è criminalizzato l’orientamento sessuale delle persone. Hanno aperto la conferenza il Presidente del gruppo liberale Graham Watson ed il Presidente della Commissione Libertà Pubbliche Gerard Deprez.  
Nel corso della Conferenza sui diritti Lgbt, numerosi partecipanti si sono uniti alla preoccupazione espressa nei giorni scorsi da Watson e Deprez e dal gruppo Liberale rispetto alla possibile nomina a Commissario Europeo per l’Italia, con delega alla giustizia e gli affari interni competente anche per i diritti umani, di esponenti ultra-cattolici o di persone senza un profilo rispettoso dei diritti delle minoranze.L’Associazione radicale Certi Diritti chiede al Governo italiano che il nuovo Commissario sia un difensore dei diritti umani e che l'Italia eviti l'ennesima figuraccia dopo quella fatta con Buttiglione.  
  

**AL SEGUENTE [LINK](http://www.alde.eu/index.php?id=42&tx_ttnews%5Btt_news%5D=9371&cHash=e81d4a8f54) E’ POSSIBILE SCARICARE I DOCUMENTI DI LAVORO DELLA CONFERENZA (IN INGLESE). DI SEGUITO IL TESTO DELLA DICHIARAZIONE FINALE VOTATA ALL’UNANIMITA’ DI TUTTI I PARTECIPANTI AI LAVORI:  
  
  
**

**Dichiarazione della Conferenza ALDE sui Diritti LGBT,  
Parlamento Europeo, 17 Aprile 2008  
  
**

Gli organizzatori ed i partecipanti alla Conferenza ALDE sui diritti LGBT tenutasi al Parlamento europeo il 17 Aprile 2008 e sponsorizzata dal gruppo ALDE nel quadro della campagna "ALDE 4 Equality", hanno adottato la seguente dichiarazione: _"Gli organizzatori e partecipanti alla Conferenza ALDE sui diritti LGBT,  
 \- ringraziano tutti gli speakers ed i partecipanti alla Conferenza, che ha toccato un'ampia gamma di temi di rilevanza per i diritti delle persone LGBT;  
\- chiedono agli Stati membri dell'Unione Europea, alle istituzioni UE ed a quelle internazionali, in particolare nell'ambito ONU, di agire al fine di:  
\- riconoscere pienamente che i diritti di LGBT sono diritti dell'uomo e che le esecuzioni, la criminalizzazione e la discriminazione basate sull'orientamento sessuale sono assolutamente contrarie a tutti i diritti dell'uomo ed al principio di uguaglianza;  
\- sostenere pienamente la promozione e la diffusione dei principi di Yogyakarta a livello internazionale;  
\- deplorare il fatto che in 7 Stati gli atti omosessuali ancora sono puniti con la pena di morte, in 77 sono criminalizzati e puniti con pene carcerarie, mentre alcuni Stati attuano campagne d'odio contro le persone LGBT, spesso condotte da figure pubbliche o religiose, ed attuate dai governi stessi o da terzi;  
\- sostenere l'istituzione dell' "IDAHO" (giorno internazionale contro l'omofobia), come il PE ha fatto, tra l'altro organizzando un convegno il 17 maggio 2006;  
\- sollevare le questioni legate ai diritti LGBT nel quadro dei rapporti con i paesi terzi;  
\- invitare con urgenza le autorità locali, regionali e nazionali ad assicurare che la libertà d'espressione, di dimostrazione e d'assemblea siano rispettate pienamente in occasione dei Gay Prides e delle Marce per l'Eguaglianza e che una protezione adeguata sia assicurata a partecipanti ed organizzatori;  
\- rafforzare la politica anti-discriminazione, lanciando ed adottando una direttiva UE orizzontale che riguardi tutti i settori (oltre all'occupazione anche l'educazione, la formazione, la previdenza sociale, l'assistenza sanitaria, l'accesso ai beni ed ai servizi, l'alloggio, ecc) e tutte le cause di discriminazione, compreso l'orientamento sessuale;  
\- assicurare che i principi della libertà di circolazione e del riconoscimento reciproco nell'UE siano applicati e garantiti anche per le persone che hanno una relazione con persone dello stesso sesso, in modo da assicurare che i cittadini di paesi terzi che abbiano una relazione di lungo termine con cittadini comunitari abbiano il diritto di seguirli in altri Stati membri;  
\- prendere le misure necessarie al fine di sormontare le discriminazioni e le disuguaglianze vissute dalle coppie dello stesso sesso e riconoscere tali relazioni, sotto le forme della coabitazione, unioni civili, matrimoni omosessuali, compreso il diritto all'adozione, come pure assicurare che le legislazioni e la prassi prevedano il riconoscimento di questi contratti anche quando sono celebrati in un altro stato membro della UE;  
\- riconoscere l'asilo politico alle persone che fuggono persecuzioni e morte a causa del loro orientamento sessuale;  
\- promuovere l'eguaglianza per le persone di LGBT nella sfera pubblica con le misure e programmi di anti-discriminazione, formazione e campagne d'informazione;"_