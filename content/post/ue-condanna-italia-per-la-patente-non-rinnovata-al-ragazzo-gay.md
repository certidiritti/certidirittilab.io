---
title: 'UE condanna l''Italia per la patente non rinnovata al ragazzo gay'
date: Mon, 05 Sep 2011 14:39:47 +0000
draft: false
tags: [Politica]
---

La Commissione europea condanna le discriminazioni in materia di negato rilascio o rinnovo della patente di guida sulla base dell'orientamento sessuale.

La Commissione europea, rispondendo ad una interrogazione depositata dagli eurodeputati liberali Marietje Schaake, Gianni Vattimo e Sophia in 't Veld [su sollecitazione dell'Associazione Radicale Certi Diritti](index.php?option=com_k2&view=item&id=1124&Itemid=227 "Patente negata a gay: interrogazione anche al Parlamento europeo "), ha condannato senza mezzi termini le ripetute discriminazioni avvenute in Italia in merito ai dinieghi di concessione e rinnovo della patente a persone LGBT, ultima quella contro Cristian Friscina.

Essa ha affermato che "in primo luogo, non vede come l'orientamento sessuale possa rientrare tra le categorie di disturbi menzionate" nelle direttive europee "e, in secondo luogo, non riesce a capire il collegamento tra l'orientamento sessuale e un'eventuale incapacità funzionale di guidare un veicolo. Inviterà pertanto le autorità italiane a chiarire la loro posizione sull'argomento".

Certi Diritti si felicita per la posizione presa dalla Commissione europea al riguardo e si augura che le autorità italiane vogliano finalmente smettere ogni discriminazioni contro le persone LGBT sulla base del loro orientamento sessuale, che sono costrette a vivere simili umiliazioni, a fare ricorsi ai tribunali ed alle istituzioni nazionali ed europee per vedere i loro più elementari diritti rispettati.

[Segue il testo dell'interrogazione e della risposta della Commissione](http://www.europarl.europa.eu/sides/getDoc.do?type=WQ&reference=E-2011-005015&language=IT)