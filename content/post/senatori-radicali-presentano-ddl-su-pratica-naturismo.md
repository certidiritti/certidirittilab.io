---
title: 'SENATORI RADICALI PRESENTANO DDL SU PRATICA NATURISMO'
date: Tue, 09 Dec 2008 17:20:35 +0000
draft: false
tags: [Comunicati stampa]
---

Presentato oggi in Senato dagli onorevoli Poretti e Perduca un Disegno di Legge per regolamentare la pratica del naturismo nel nostro paese

L'uni Lazio esprime la propria soddisfazione.

Roma, 9 dicembre 2008

• Donatella Poretti e Marco Perduca, radicali eletti nelle liste del PD, hanno presentato oggi in Senato un disegno di legge per la depenalizzazione della pratica del naturismo

Il disegno di legge ripropone il testo già presentato nella scorsa legislatura dall'Onorevole Franco Grillini modificando peraltro l'articolo 2 ove è prevista una più drastica soluzione di abrogazione dell'art. 726 del codice penale e non la sua sospensione, come proposto in passato, nelle aree dedicate o frequentate dai naturisti.

Tale articolo 726  punisce – come è detto nella relazione che illustra il disegno di legge – con sanzioni amministrative pecuniarie chiunque, in luogo pubblico o aperto o esposto al pubblico, compie atti contrari alla pubblica decenza. Una definizione questa  – sottolinea la Senatrice Poretti - così vaga che risulta opportuno abrogare vista la sussistenza del reato di atti osceni molto più gravi e puniti dall'art. 527 del codice penale e che certo non riguardano la pratica del naturismo.

Nel disegno di legge – prosegue la Senatrice Poretti - è previsto che le regioni e le province autonome di Trento e Bolzano debbano definire i criteri secondo cui i comuni possono individuare le aree pubbliche o private da destinare alla pratica del naturismo, anche su richiesta di organizzazioni, associazioni, società o soggetti privati interessati a progettare e gestire le relative strutture. Ove i comuni non provvedano ad individuare tali aree i proprietari o i gestori delle aree turistico-ricettive potranno chiedere alle amministrazioni comunali competenti l'autorizzazione ad adibire tali aree alla pratica del naturismo.

Le aree destinate – conclude la Senatrice Poretti - dovranno essere segnalate e provviste di una adeguata identificazione che le distingua dagli spazi frequentati da cittadini che non praticano il naturismo.

L'UNI Lazio, nel ringraziare i presentatori del disegno di legge, rammenta come ogni anno in Italia siano stati numerosissimi i naturisti incappati nella violazione dell'art. 726 per aver preso il sole nudi in spiagge isolate e frequentate da naturisti da oltre trent'anni.

Il Presidente dell'UNI Lazio, Prof. Carlo Consiglio, sottolinea come sia oramai urgente l'approvazione da parte del Parlamento di una legge che regolamenti la pratica del naturismo. In argomento rammenta come gli oltre 500.000 naturisti italiani siano costretti ogni anno a recarsi presso le migliaia di spiagge e strutture esistenti in tutto il mondo. L'incertezza legislativa sull'argomento ha anche comportato notevoli perdite economiche, perché gli oltre 20 milioni di naturisti europei si sono sempre ben guardati dallo scegliere il nostro Paese come meta vacanziera.

Qui il testo del disegno di legge: [http://blog.donatellaporetti.it/?p=385](http://blog.donatellaporetti.it/?p=385)