---
title: '2008-0140-Discrimination-OP-SOC-160710-ST12107.EN10'
date: Tue, 03 Aug 2010 08:00:00 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 3 August 2010

Interinstitutional File:

2008/0140 (CNS)

12107/10

LIMITE

SOC 456

JAI 623

MI 242

  

  

  

  

  

OUTCOME OF PROCEEDINGS

from :

The Working Party on Social Questions

on :

16 July 2010

No. prev. doc. :

9312/10 SOC 314 JAI 387 MI 128 + COR 1

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

**I.         INTRODUCTION**

At its meeting on 16 July 2010, the Working Party continued its examination of the above proposal. The discussion focused on the question of equal treatment in the area of _financial services_, based on a questionnaire prepared by the Presidency[\[1\]](#_ftn1). Delegations broadly welcomed the Presidency's approach, which will allow in-depth discussions to take place on specific issues.

**II.        INDEPENDENT STUDY**

The Commission representative presented the outline of a forthcoming independent study produced by CIVIC Consulting; the full text of the report is available online[\[2\]](#_ftn2).

**III.      THE PRESIDENCY'S QUESTIONNAIRE**

Delegations gave their first reactions to the questions tabled by the Presidency, some (CZ, EL, ES, IE, IT, LT, HU, PL, PT, SK, SE, FI) indicating that they required more time to study the issues.

**1\.** **Situation in Member States**

_How do you deal with the use of age and disability as factors in risk assessment in financial services (insurance and banking) at national level?_

_-        Age_

UK supported the use of age as a risk assessment factor and explained that a national regulation would be introduced in 2012. EE, IT, HU, MT, PL, RO and SI explained that there were currently no binding rules in their respective countries restricting the use of age as a risk assessment factor. CZ, DK, FR, IE, LV, LT, NL and FI also explained that age was used as a risk assessment factor in their respective countries. CY responded that age was used in the insurance field in Cyprus.

  

SK reported legislation restricting the use of age as a risk assessment factor.

AT described the forthcoming national equal treatment law that would prohibit the use of age as a risk assessment factor in Austria.

_\-        Disability_

UK explained that using disability as a risk assessment factor was prohibited in the United Kingdom, but an exception was made for insurance, provided that it was based on relevant and reliable information. EE, HU, LV, PL, RO and SI explained that there were currently no binding rules in their respective countries restricting the use of disability as a risk assessment factor. CY, DK, IE, FR, IT, NL and FI also explained that disability was used as a risk assessment factor in their respective countries, particularly for certain types of insurance.

CZ, MT and LT explained that _health_ could be used as a risk assessment factor in their respective countries, LT also mentioning the use of disability as a proxy factor.

EL and SK also reported legislation in their respective countries restricting the use of disability as a risk assessment factor.

ES informed delegations of forthcoming legislation in Spain based on the United Nations Convention on the Rights of Persons with Disabilities (UNCRPD).

AT explained that no exceptions on the grounds of disability were allowed in Austria.

DE saw a need to clarify the term "financial services" in the interest of legal certainty.

**2\.** **Disability / health conditions**

_It has been suggested that the exception for disability can be limited to the cases where the underlying health condition is a relevant risk factor. What is your view?_

CZ, EL, IT, HU, LT, NL, PT, AT and UK agreed that the exception for disability could be restricted to an underlying health condition only. IT, HU and UK expressed the concern that defining the link between disability and health could prove difficult. Recalling the new legislation being introduced in Spain, ES, supported by LU, affirmed the need to justify any differences in treatment and to limit the exceptions to the minimum.

SI and FI agreed that health should be the factor where it (rather than disability) was the relevant consideration, although disability was also still used in some cases. While affirming the need to distinguish between health conditions and disabilities, PL explained that all relevant factors could be taken into account by insurers in Poland. DK, similarly, felt that disability was a legitimate risk assessment factor where relevant.

LV agreed that health was the primary consideration, but observed that insurers often adopted a generalised approach, instead of examining individual cases.

CY expressed the view that the exception for disability could not be restricted to an underlying health condition only.

**3\.** **Scope of the exception**

_Currently the exception provides for a three-step test (similar to Directive 2004/113/EC regarding gender):_

_1\._ _Is age / disability a determining factor in the assessment of risk for the service in question?_

_2\._ _Is the risk assessment based on relevant actuarial principles, accurate statistical data or medical knowledge? (evidence base)_

_3\._ _Is the difference of treatment proportionate?_

_Is this test workable in your view?_

CZ, FR, IT, CY, HU, LU, NL, PL and FI agreed that the three-step test was indeed workable. ES and AT also provisionally welcomed it.

Regarding Test Step 1, IE, IT and UK underlined that age and disability had to be determining factors rather than "decisive" or "key" factors.

UK supported by IE and IT felt that Test Step 2 did not capture all evidence sources; instead of aiming for exhaustiveness, they preferred stipulating that the evidence had to be relevant and reliable. UK and IE also felt that "reliable medical knowledge" was a more appropriate term than "accurate medical knowledge". MT also stressed the importance of reasonable and relevant data, as well as other factors.

DK, LU, AT and FI similarly preferred a cumulative formulation ("and"). IT, however, stressed that the evidence sources were used in different combinations and that a flexible formulation was appropriate. LU raised the question of the potential multiple discrimination of the old and disabled.

  

CY, FR and LU stressed that the test should be the same for age and disability. AT felt that medical knowledge should only be used if no statistical data were available, while DK had doubts concerning the use of medical knowledge without any statistical data. ES stated that the term "medical knowledge" was rather vague.

See also Questions 4 ("Evidence") and 5 ("Proportionality") below.

**4\.** **Evidence to justify differential treatment**

_The current text on the possible evidence base is slightly different for age and disability. 1) Should risk assessment always be based on relevant actuarial principles? 2) Should the use of medical knowledge only be possible, if statistical data are not available? 3) Should there be restrictions on the kind of data that can be used?_

Regarding Question 4(1), CZ, AT and FI felt that risk assessment should indeed always be based on relevant actuarial principles. DK favoured deleting the reference to relevant actuarial principles. UK and IT made the point that risk assessment was not necessarily entirely based on relevant actuarial principles, as matters of judgment and experience also came into play. HU stated that, while relevant actuarial principles were important, correct data and medical knowledge could also be considered as relevant data. The Commission representative affirmed the importance of actuarial principles. MT called for clarification.

Regarding Question 4(2), FI supported by HU felt that medical knowledge should only be used in respect of age if no statistical data were available. FI agreed that medical knowledge could be used in respect of disability. DK stated that the risk assessment should always be based on statistical data, possibly backed by medical knowledge, while AT took the view that both were required..

  

NL affirmed the use of medical knowledge, especially where no statistical data were available. CZ also affirmed the use of expert judgments if no other information was available. NL felt it was important to refer to _the latest_ medical knowledge. IT and UK opined that the use of medical knowledge should not be subject to a qualification, and that what mattered most was the relevance and reliability of the data. MT similarly called for a flexible formulation.

Regarding Question 4(3), UK and IT did not support restrictions, preferring to leave the assessments to the insurer. HU expressed hesitation and called for clarification. AT, for its part, stressed the need for relevant and meaningful data, and the need to take data protection issues into account.

**5.           Proportionality of differential treatment**

_What do you understand by "proportionate differences"? Mathematical proportionality or a wider concept of proportionality, taking into account issues such as legitimacy, objectivity, relevance and the existence of less restrictive alternatives ("ultima ratio" principle)?_

A large number of delegations (DK, EE, EL, ES, FR, CY, HU, MT, LV, NL, SI, FI, UK) affirmed a wider concept of proportionality. LU stressed the paramount importance of objectivity. See also Question 4(1).

**6.       Age limits and age bands**

_Would the exception allow financial service providers to apply age limits or age bands? How could proportionality be ensured in that context?_

  

CZ, DK, CY, LV, LT, HU, MT, FI and UK affirmed financial service providers' right to apply age limits and bands, UK underscoring the need to provide legal certainty on this matter, and CZ stressing the importance of ensuring a level playing field across the internal market.

IT expressed the provisional view that, while age limits and bands were needed for voluntary insurance products, they may not be as relevant to compulsory products, such as motor insurance.

IE explained that age limits were not used in IE, where insurers were obliged to offer quotes to prospective customers regardless of their age.

EL was unable to accept age limits and age bands.

Reminding delegations of the serious practical consequences faced by individuals who are refused essential insurance products because of their age, the Commission representative warned against any carte blanche approach; thus proportionality and age limits should be reconciled in a reasonable fashion.

**7.           Transparency towards customer**

_Is there a need in the exception for a requirement to publish data, similar to the one in Directive 2004/113/EC? If not, should there be other forms of transparency towards the customer (e.g. obligation to explain the reasons for the differential treatment / to advise the customer on possible alternatives)?_

  

Regarding Question 7(1), CZ, EL, CY, LV, HU and AT saw merit in the requirement to publish data.

Regarding Question 7(2), FR and IT stressed the importance of transparency with respect to the _criteria or rationale_ used for calculating premiums; DK, MT, FI and UK suggested that it was enough to explain the _reason for refusing a service_, FI stressing the role of control bodies in monitoring insurers.

FR, IT, CY, FI and UK also affirmed the need to respect confidentiality in regard to the specific techniques used by operators, for whom they were legitimate business secrets.

DK, EL, CY, HU, MT and AT stressed the need to provide consumers with information that was useful and understandable, including advice on alternatives.

**IV.      CONCLUSION**

A number of delegations (DK, EL, ES, IT, MT, NL, FI, SE, UK) undertook to provide replies in writing[\[3\]](#_ftn3). The Presidency invited delegations to submit written contributions no later than 20 August and announced that the Working Party would continue its examination of the proposal in due course.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) See doc. 11903/10 + COR 1.

[\[2\]](#_ftnref2) Study on the use of age, disability, sex, religion or belief, racial or ethnic origin and sexual orientation in financial services;  
Main report: [http://ec.europa.eu/social/BlobServlet?docId=5599&langId=en](http://ec.europa.eu/social/BlobServlet?docId=5599&langId=en),  
Country reports: [http://ec.europa.eu/social/BlobServlet?docId=5600&langId=en](http://ec.europa.eu/social/BlobServlet?docId=5600&langId=en),  
Annexes: [http://ec.europa.eu/social/BlobServlet?docId=5601&langId=en](http://ec.europa.eu/social/BlobServlet?docId=5601&langId=en)

[\[3\]](#_ftnref3) Written comments from SK have been circulated in doc. 12461/10.