---
title: 'BILANCIO 2010'
date: Fri, 09 Dec 2011 07:32:12 +0000
draft: false
tags: [Politica]
---

**Situazione finanziaria al 26 novembre 2010**

**AVANZO DI BILANCIO 2009**

**€ 1.657,71**

**RICAVI 2010**

ISCRIZIONI e CONTRIBUTI

€ 15.327,18

INTERESSI BANCARI ATTIVI

€ 8,37

**Totale**

**€ 15.335,55**

**COSTI 2010**

**SPESE GENERALI**

_Spese Bancarie_

€ 197,47

_Sito web_

€ 76,25

_Stampati_

€ 300,00

_Spedizioni_

€ 14,25

_Telefono_

€ 29,00

_Spedizione e stampa tessere_

€ 534,40

**Subtotale**

**€ 1.151,37**

**CONGRESSI E RIUNIONI**

_Congresso Firenze_

**€ 583,60**

_Congresso Roma_

Affitto sala

€ 1.300,00

Inserzioni pubblicitarie

€ 540,00

Spese di viaggio e soggiorno

€ 1.450,89

Accantonamento costi presunti

€ 1.000,00

Totale

**€ 4.290,89**

_Riunioni varie_

**€ 332,50**

**Subtotale**

**€ 5.206,99**

**CONTRIBUTI EROGATI**

_Ilga Europe_

€ 125,00

_Consulta laica di Milano_

€ 100,00

_Coordinamento Arcobaleno Milano_

€ 120,00

**Subtotale**

**€ 345,00**

**INIZIATIVE POLITICHE**

_Affermazione civile_

€ 1.436,60

_Comunicazione (volantini, gadgets, bandiere)_

€ 1.111,40

_Pride Milano_

€ 172,36

_Pride Napoli_

€ 2.311,91

_Seminario Milano_

€ 663,10

_Varie_

€ 2.829,57

**Subtotale**

**€ 8.524,94**

**TOTALE COSTI**

**€ 15.228,30**

**AVANZO DI BILANCIO 2010**

**€ 107,25**

**TOTALE ATTIVO**

**€ 1.764,96**