---
title: 'PILLOLA GIORNO DOPO: LA PROVA DELLA ILLEGALITÀ NEGLI OSPEDALI DI ROMA'
date: Sat, 13 Sep 2008 17:38:06 +0000
draft: false
tags: [Comunicati stampa]
---

**Ospedali romani e la “pillola del giorno dopo”, Iervolino: “porteremo il filmato ai magistrati, intanto Marrazzo intervenga!”**

**Roma, 11 settembre 2008**

Questa mattina, presso la sede del Partito Radicale, è stata presentata la video inchiesta Gli ospedali romani e la “pillola del giorno dopo”, realizzata dall’Associazione Radicali Roma, della quale ieri Repubblica Tv ha pubblicato alcune sequenze. Alla conferenza stampa hanno preso parte Massimiliano Iervolino, Segretario di Radicali Roma; Rita Bernardini, deputata Radicali/PD; Michele De Lucia, Tesoriere di Radicali Italiani; Alessandro Gerardi, avvocato e Marco Pannella in collegamento telefonico.

• Dichiarazione di Massimiliano Iervolino, Segretario dell’Associazione Radicali Roma

Mi urge dare tre “piccole” notizie:

1) La prossima settimana ci recheremo dai magistrati per consegnare il Dvd inerente all’inchiesta sugli ospedali romani e la “pillola del giorno dopo”, auspichiamo che costoro abbiano la volontà di far luce su quanto accaduto, e su quanto accade, nelle strutture romane.

2) Voglio sperare che nelle prossime ore, il Presidente della Regione Lazio, nonché Commissario alla Sanità, Piero Marrazzo, voglia contattarci personalmente per recepire questo filmato, lo diciamo da tempo: o in Regione Lazio ci sarà un cambio di direzione su molti temi o la sconfitta oltre ad essere ad oggi probabile tra un anno e mezzo diventerà realtà. Qualora Marrazzo non ritenesse opportuno acquisire questo filmato ci sentiremmo in obbligo, durante la prossima settimana, di fargli visita, bussando alla sua porta fin quando non saremo ricevuti. L’apertura all’Udc per le prossime elezioni regionali ci interessa poco, a noi quello che interessa è far finire il calvario che le donne subiscono negli ospedali romani durante i week end. Quindi ribadisco: o il presedente Marrazzo ci invita o saremo noi a far di tutto per incontrarlo e consegnargli il filmato.

3) Ci prendiamo la responsabilità politica di annunciare che le nostre video inchieste negli ospedali e consultori romani continueranno, ci sentiamo in obbligo nei confronti di tutte quelle donne che soffrono la condizione di illegalità diffusa in cui versano le strutture ospedaliere, di continuare a monitorare queste cose e di far conoscere a più persone possibili la verità che troppe volte viene nascosta da paure o interessi di parte.”

Il filmato integrale della video inchiesta sugli ospedali romani e la “pillola del giorno dopo” può essere visto sul sito www.radicaliroma.com