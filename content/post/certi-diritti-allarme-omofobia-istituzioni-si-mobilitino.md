---
title: 'Certi Diritti: allarme omofobia, istituzioni si mobilitino'
date: Wed, 18 May 2011 11:58:44 +0000
draft: false
tags: [Politica]
---

**Dopo la Giornata mondiale contro l'omofobia (IDAHO) l'associazione radicale Certi Diritti lancia allarme sulle discriminazioni, le diseguaglianze e le persecuzioni che in Italia colpiscono le persone lgbt e chiede alle istituzioni misure urgenti al riguardo.**

Bruxelles-Roma, 18 maggio 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

L’Associazione Radicale Certi Diritti il giorno dopo la giornata mondiale contro l'omofobia e la transfobia (International Day Against Homophobia – IDAHO), che si celebra ogni anno il 17 maggio, fa un bilancio della situazione dei diritti LGBT in Italia, in Europa e nel mondo.  
Come rilevato da ILGA ed ILGA-Europe, che hanno elaborato la "Mappa e Indice arcobaleno" (Rainbow Map e Index, disponibili su  [www.ilga.org](http://www.ilga.org/) e [http://www.ilga-europe.org](http://www.ilga-europe.org/) ), la situazione rimane problematica in molte aree del mondo ed anche in Europa.  
La situazione a livello mondiale rimane estremamente preoccupante, come dimostrato dal recente tentativo - fallito anche grazie alla mobilitazione internazionale, europea e italiana - da parte del Parlamento ugandese di adottare una legge che avrebbe condannato le persone LGBT alla pena di morte.

**Per quanto riguarda l'Europa:**  
\- nessuno Stato europeo puo' affermare di avere dato piena eguaglianza per le persone LGBT, dato che in tutti gli Stati permangono problemi;  
\- 14 Stati, dei quali uno Stato membro UE, sono nella "zona rossa" a causa delle gravi violazioni dei diritti umani e delle discriminazioni;  
\- ci sono variazioni significative tra gli Stati membri UE: se alcuni hanno fatto progressi nel corso dell'anno passato (Germania, Portogallo), di altri non si puo' dire lo stesso (Italia, Cipro, Lettonia, Malta, Turchia, Ucrania), mentre in altri vi é un rischio di peggioramento (Ungheria, Lituania);  
\- numerosi Stati UE sono al limite o sotto la media in merito al rispetto dei diritti umani e l'assicurazione di eguaglianza legale delle persone LGBT.

**Per quanto riguarda l'Italia:**  
Certi diritti stigmatizza il fatto che l'Italia é rimasto uno dei paesi UE che sono più arretrati in materia di diritti LGBT, in compagnia di Malta e Lettonia, e seguita da Cipro e non distante dal Vaticano, la Russia, la Turchia, l'Ucraina e la Bielorussia. Insomma, se l'Europa civilizzata avanza, l'Italia é immobile o arretra verso gli Stati che non rispettano i diritti umani, le libertà individuali, la democrazia e lo Stato di diritto. 

L’Associazione Radicale Certi Diritti suona il campanello d'allarme, anche in ragione dei recenti episodi di violenze ed aggressioni verbali e fisiche di stampo omofobo, private e pubbliche, e chiede alle istituzioni italiane di prendere misure urgenti per riportare l'Italia sulla strada dei diritti umani, delle libertà e della democrazia basata sull'eguaglianza e sulla non-discriminazione. Una prima occasione è il provvedimento anti-omofobia dell’on. Paola Concia calendarizzato alla Camera dei deputati il prossimo 23 maggio.

Nota: come rivelato dal Rainbow Index, **l'Italia non proibisce le discriminazioni basate sull'orientamento sessuale e l'identità di genere nella Costituzione, non ha una legge che proibisce le discriminazioni basate sull'orientamento sessuale e l'identità di genere nell'accesso ai beni ed ai servizi, non riconosce il cambiamento di sesso senza che vi sia una operazione chirurgica, non riconosce la coabitazione, la partnership, il matrimonio tra persone dello stesso sesso, non riconosce alcun diritto genitoriale alle coppie LGBT, non ha una legge sull'omofobia e la transfobia, non riconosce le coppie dello stesso sesso sposate all'estero, impone il divorzio e la sterilizzazione alle persone trans prima che il cambiamento di sesso sia riconosciuto. **