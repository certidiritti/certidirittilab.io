---
title: 'LEONA DIMENTICATA'
date: Sun, 10 Jan 2010 05:56:01 +0000
draft: false
tags: [Senza categoria]
---

Milano. Trans suicida in CIE: per Gruppo EveryOne è dramma umanitario causato da politiche intolleranti. “Intervengano Alto Commissario per i Diritti Umani e per i rifugiati”

Ieri, 25 dicembre, intorno alle 15 una persona transessuale di nazionalità brasiliana, entrata domenica scorsa nel Centro di Identificazione ed Espulsione di via Corelli a Milano, si è impiccata usando un lenzuolo. L'allarme, dato da un detenuto intorno alle 15,30, non è però servito a rianimarla in tempo, dopo l'arrivo dei soccorsi.

“Quella di ieri è l'ennesima vittima delle politiche dettate dalla Lega Nord e del pacchetto sicurezza,” affermano Roberto Malini, Matteo Pegoraro e Dario Picciau, co-presidenti dell'organizzazione per i diritti umani EveryOne, “una legge crudele che ha introdotto in Italia il reato di clandestinità, ponendo i migranti senza permesso di soggiorno nella condizione di rischiare ogni giorno l'arresto, la detenzione nei Cie fino a sei mesi, trattamenti inumani e infine la deportazione”.  
  
  
"Quando accadono tragedie come questa," prosegue EveryOne, "diventa necessario riflettere sulla realtà dei Cie, che sono vere e proprie carceri dove ogni diritto viene violato, tanto che lo stesso premier Silvio Berlusconi - nel corso di una recente conferenza stampa a L'Aquila con il commissario europeo Jacques Barrot - li ha definiti 'simili a campi di concentramento'.  
  
Ricordiamo che l'Italia è il primo Paese europeo per discriminazioni, morti e violenze transfobiche: un terribile primato che rende le persone transessuali e transgender cittadini vulnerabili ed esclusi,” continuano i rappresentanti del Gruppo EveryOne, “su cui stampa, autorità e istituzioni riversano pregiudizi e trattamenti lesivi della dignità di esseri umani. E se questo non bastasse a sollevare lo sdegno delle persone civili, ricordiamo che l'Italia ha anche il primato dei suicidi, delle violenze e degli stupri nelle prigioni”.  
  
  
Il Gruppo EveryOne, che da febbraio 2009 porta avanti, assieme ai Radicali e alla rete antirazzista, un monitoraggio sui CIE di tutta Italia, ha rilevato che suicidi (tentati o riusciti), specie tra immigrati transessuali e transgender, sono in costante aumento e che ciò che si sta consumando nei Centri di Identificazione ed Espulsione altro non è che un dramma umanitario.  
  
“Le condizioni igienico-sanitarie dei centri” spiegano gli attivisti, “sono terribilmente precarie, con mancanza spesso di acqua corrente e servizi igienici agibili; per non parlare delle violenze e delle umiliazioni, più volte documentate anche dal nostro Gruppo, che si consumano tra le loro mura: intimidazioni, pestaggi, spedizioni punitive, atrocità.  
  
Abbiamo sollecitato” concludono Malini, Pegoraro e Picciau, “gli uffici a Ginevra dell'Alto Commissario ONU per i Diritti Umani e dell'Alto Commissario ONU per i Rifugiati, con cui EveryOne collabora a stretto contatto, affinché la discussione sulle gravi violazioni dei diritti fondamentali perpetrate dal sistema carcerario e dalle politiche anti-immigrazione del nostro Paese venga portata all'attenzione della Corte Internazionale di Giustizia, principale organo giudiziale delle Nazioni Unite”.  
  
Radio radicale: Intervista a Roberto Malini sul suicidio del transessuale nel Cie di Milano.  
  
Sergio Rovasio:"aveva la sola colpa di essere una transessuale brasiliana."  
La notizia ovviamente, nella serenità e nella gioia del Natale è una di quelle che dà tanto fastidio. I parlamentari radicali, sicuramente, presenteranno su questa drammatica vicenda un’interrogazione parlamentare, che probabilmente si aggiungerà alle tante altre presentate e rimaste senza risposta. Come inascoltata, evidentemente, è rimasta ogni sua richiesta d’aiuto.