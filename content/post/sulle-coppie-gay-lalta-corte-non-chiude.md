---
title: 'SULLE COPPIE GAY L''ALTA CORTE NON CHIUDE'
date: Mon, 19 Apr 2010 06:05:57 +0000
draft: false
tags: [Senza categoria]
---

di **Vittorio Angiolini*,** su [_**L'Unità**_](http://cerca.unita.it/data/PDF0115/PDF0115/text2/fork/ref/101063k4.HTM?key=VIttorio+Angiolini&first=1&orderby=1&f=fir), 16 Aprile 2010

La decisione della Corte costituzionale sul matrimonio gay, letta la motivazione, non si esaurisce affatto nel rinviare il problema alla "discrezionalità" del legislatore.

Anzitutto, la Corte – e non è poco – riconosce che, nell'art. 2 della Costituzione: "per formazione sociale deve intendersi ogni forma di comunità, semplice o complessa, idonea a consentire e favorire il libero sviluppo della persona nella vita di relazione, nel contesto di una valorizzazione del modello pluralistico. In tale nozione è da annoverare anche l'unione omosessuale, intesa come stabile convivenza tra due persone dello stesso sesso, cui spetta il diritto fondamentale di vivere liberamente una condizione di coppia, ottenendone – nei tempi, nei modi e nei limiti stabiliti dalla legge – il riconoscimento giuridico con i connessi diritti e doveri".

Il legislatore ha dunque da intervenire, ma solo per scegliere tra diverse modalità della protezione da accordare alla coppia omosessuale in applicazione di un diritto che è direttamente garantito dall'art. 2 della Costituzione. Inoltre, tra le soluzioni praticabili per la protezione della coppia gay non è precluso al legislatore scegliere quella del matrimonio. Poiché, sempre secondo la Corte, la nozione di matrimonio e quella di famiglia dell'art. 29 della Costituzione non sono "cristallizzate", ma da adeguare alla "evoluzione della società e dei costumi". Anche se un tale adeguamento resta compito del legislatore e non può essere opera della Corte con una "interpretazione creativa".

Infine, il legislatore non è neanche libero di rimanere inerte. Poiché, in assenza di una legge adeguata, per così dire chiudendo il cerchio, la Corte si riserva "la possibilità d'intervenire a tutela di specifiche situazioni (come è avvenuto per le convivenze more uxorio: sentenze n. 559 del 1989 e n. 404 del 1988). Può accadere, infatti, che, in relazione ad ipotesi particolari, sia riscontrabile la necessità di un trattamento omogeneo tra la condizione della coppia coniugata e quella della coppia omosessuale, trattamento che questa Corte può garantire con il controllo di ragionevolezza".

L'intervento del legislatore sul riconoscimento delle unioni omosessuali, pur non essendo a "rime obbligate", è dunque costituzionalmente dovuto, per l'art. 2 della Costituzione, e da svolgersi senza dar adito a discriminazioni ingiustificate rispetto ai diritti già spettanti alle coppie sposate eterosessuali, poiché eventuali scelte discriminatorie, penalizzanti le coppie gay, sarebbero censurabili costituzionalmente per "irragionevolezza".

Speriamo che, finalmente, il Parlamento faccia la sua parte, lasciando indietro pregiudizi vecchi e nuovi, di ogni risma e colore.

\* **Vittorio Angiolini**,  Professore di Diritto Costituzionale, Avvocato delle coppie omosessuali in giudizio presso la Corte Costituzionale