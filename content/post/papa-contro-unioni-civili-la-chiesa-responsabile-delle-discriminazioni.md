---
title: 'PAPA CONTRO UNIONI CIVILI: LA CHIESA RESPONSABILE DELLE DISCRIMINAZIONI'
date: Sun, 14 Sep 2008 15:04:12 +0000
draft: false
tags: [Comunicati stampa]
---

**ESTERNAZIONI PAPA: ORA CI DICE CHE LE LEGGI SULLE UNIONI CIVILI VANNO CONTRO IL BENE COMUNE DELLA SOCIETA’. STRANO, FINORA DOVE SONO STATE APPROVATE HANNO AIUTATO MILIONI DI PERSONE. LA CHIESA CONTINUA AD ESSERE RESPONSABILE DELLE DISCRIMINAZIONI.**

**Roma, 14 settembre 2008**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

“Il Papa oggi ha detto che la Chiesa deve opporsi alle leggi che favoriscono le unioni civili. Spesso – ha detto oggi a Lourdes il Papa – le leggi cercano più di adattarsi ai costumi e alle rivendicazioni di particolari individui o gruppi, che non di promuovere il bene comune della società. Francamente questa tesi ci appare fuori luogo, a maggior ragione se detta da un ‘fine intellettuale’ quale egli sarebbe. Infatti, tutti i paesi europei, e non solo, dove sono state approvate leggi sulle unioni civili, si è andati incontro alle esigenze di milioni di cittadini che sono esattamente uguali a tutti gli altri,e che non devono più sentirsi cittadini di ‘serie B’. Sostenere che le unioni civili vanno contro il bene comune della società non è una tesi credibile, dimostra semmai mancanza di argomentazioni e alimenta le discriminazioni di cui la chiesa ha la maggior parte di responsabilità”.