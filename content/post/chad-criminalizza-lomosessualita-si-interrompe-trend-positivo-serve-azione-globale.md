---
title: 'Chad criminalizza l''omosessualità. Si interrompe trend positivo: serve azione globale.'
date: Fri, 16 Dec 2016 15:53:34 +0000
draft: false
tags: [Africa]
---

![chad-mmap-md](http://www.certidiritti.org/wp-content/uploads/2016/12/chad-MMAP-md-300x212.png)Con 111 voti a favore, 4 astenuti e uno solo contrario, il Chad introduce il reato di omosessualità. «Con il voto del Chad a favore della criminalizzazione dell’omosessualità, salgono a 72 i paesi dove il proprio amore è considerato un crimine. Il fatto che la punizione prevista sia solo una multa e non 20 anni di carcere come proposto nel 2014, non è proprio una consolazione. Il 2017 deve vedere l’impegno di tutte e tutti per arginare questa nuova tendenza alla criminalizzazione», così Leonardo Monaco, segretario dell’associazione Radicale Certi Diritti.