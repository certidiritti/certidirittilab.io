---
title: 'Associazione radicale Certi Diritti: sit-in davanti a Palazzo Marino all''indomani dell''incontro mondiale delle famiglie'
date: Mon, 04 Jun 2012 16:14:28 +0000
draft: false
tags: [Diritto di Famiglia]
---

Nota dell'Associazione Radicale Certi Diritti

Milano, 4 giugno 2012

L'Associazione Radicale Certi Diritti annuncia un presidio che si terrà lunedì 4 giugno dalle 17:00 davanti alla sede del Consiglio comunale di Milano, il giorno della prima seduta dopo l'Incontro Mondiale delle Famiglie.

L'Associazione denuncia l'immobilismo trasversale del Consiglio in materia di diritti civili delle famiglie di fatto da sei mesi a questa parte, proprio in ottica della manifestazione tenutasi dal 30 maggio al 3 di giugno che – ricorda – ha celebrato solo e unicamente la “famiglia matrimoniale”, escludendo così tutte le altre forme familiari “non tradizionali” originate da coppie etero e omosessuali, con o senza figli.

“Certi Diritti” denota l'incompatibilità con il principio di laicità delle istituzioni di questa presa di posizione assunta da una certa classe politica milanese, ed evidenzia come ancora una volta siano stati i cittadini e le realtà associative a mobilitarsi e ad avvalersi degli strumenti di democrazia diretta per sopperire a questa intollerabile lacuna di diritti. L'Associazione radicale è pertanto tra le promotrici del comitato Milano Radicalmente Nuova che, oltre alle proposte in materia di testamento biologico, sale da iniezione e regolamentazione della prostituzione, propone ai milanesi altre due iniziative popolari comunali sul riconoscimento delle unioni civili e sull'istituzione di un Settore anti discriminazioni.

Nel corso del presidio tutti i cittadini residenti a Milano maggiori di 16 anni avranno modo di sottoscrivere le cinque proposte di Milano Radicalmente Nuova.

Parteciperanno alla manifestazione Yuri Guaiana e Giacomo Cellottini (segretario e tesoriere di “Certi Diritti”) e Leonardo Monaco (membro della Giunta di “Certi Diritti” e presidente dell'associazione “Comitati iniziative popolari Milano”)