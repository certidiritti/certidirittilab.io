---
title: 'Registro unioni civili Roma, Mainardi: maggioranza rinvia testo in Commissione. Temiamo mancanza di convinzione sul tema'
date: Thu, 26 Jun 2014 13:54:03 +0000
draft: false
tags: [Diritto di Famiglia]
---

[![proprietà www.romaincamper.it](http://www.certidiritti.org/wp-content/uploads/2014/06/piazza_campidoglio-300x198.jpg)](http://www.certidiritti.org/wp-content/uploads/2014/06/piazza_campidoglio.jpg)Proposta popolare sulle unioni civili calendarizzata in piena violazione dei termini temporali statutariamente previsti solo a seguito della segnalazione al Prefetto di Roma da parte del consigliere Radicale Riccardo Magi; 8mila firmatari in attesa di un riscontro dal consiglio. Questo è stato lo scenario di apertura dell'Assemblea Capitolina odierna. La maggioranza (Riccardo Magi escluso) non si è tuttavia voluta prendere la responsabilità nemmeno di bocciare, come già fece nel 2007, la proposta di delibera popolare. Il rinvio odora troppo di mancanza di convinzione all'interno del Partito Democratico e di paura di non poter intestarsi il merito sul tema da parte di alcuni partiti. Confidiamo in un'immediata calendarizzazione della stessa proposta di iniziativa consiliare per non prendere in giro 8mila cittadini sottoscrittori che si devono sommare ai 44mila firmatari del quesito referendario contenuto nell'iniziativa RomaSìMuove.

Dichiarazione di Matteo Mainardi, Associazione Radicale Certi Diritti