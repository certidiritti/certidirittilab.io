---
title: 'Certi Diritti: Presentazione del libro «Il lungo “inverno democratico” nella Russia di Putin»'
date: Wed, 21 Nov 2018 10:06:32 +0000
draft: false
tags: [Senza categoria]
---

[![Locandina_Il lungo inverno imp ai loghi ..](http://www.certidiritti.org/wp-content/uploads/2018/11/Locandina_Il-lungo-inverno-imp-ai-loghi-..-214x300.jpg)](http://www.certidiritti.org/2018/11/21/certi-diritti-presentazione-del-libro-il-lungo-inverno-democratico-nella-russia-di-putin/locandina_il-lungo-inverno-imp-ai-loghi/)Saranno **Anna Zafesova**, giornalista de «**La Stampa**» e **Lorena Villa**, della **Fondazione Luigi Einaudi**, a inaugurare il XII Congresso dell’Associazione Radicale Certi Diritti.

**Venerdì 23 novembre 2018, alle ore 19, presso la Libreria Open di Milano**, in Viale Monte Nero, 6 – Metro Linea 3 (gialla) fermata Porta Romana – verrà presentato **«Il lungo “inverno democratico” nella Russia di Putin» edito da Diderotiana Editrice**. Interverrà anche il curatore del libro **Yuri Guaiana**, presidente di Certi Diritti.

A un anno e mezzo dal suo arresto a Mosca mentre tentava di consegnare più di 2 milioni di firme per chiedere un’inchiesta sugli arresti illegali, le torture e le uccisioni di omosessuali in Cecenia, **Guaiana** ha radunato esperti come la stessa **Anna** **Zafesova**, e attivisti, come **Marco** **Cappato**, per meglio comprendere i rischi che la Russia di Putin pone, non solo ai suoi cittadini, ma anche a noi europei.

Come scrive **Emma Bonino** nella prefazione « Questo è un libro ben pensato, scritto in modo gradevole ed elegante \[…\] Sono pagine che ricostruiscono un pezzo di storia radicale e della nostra costante attenzione per i diritti umani in Russia, come in qualunque altra parte del mondo, ma anche una vicenda storica, politica e umana tanto tragica quanto cruciale per comprendere il momento politico che stiamo vivendo. Questo libro dovrebbero leggerlo tutti, ma soprattutto coloro i quali ritengono che il nostro futuro sia a Est, tra Visegrád e Mosca. Il prezzo da pagare per i loro sogni (e gli incubi di tutti gli altri) lo scopriamo proprio tra queste pagine.»

Milano, 21 novembre 2018