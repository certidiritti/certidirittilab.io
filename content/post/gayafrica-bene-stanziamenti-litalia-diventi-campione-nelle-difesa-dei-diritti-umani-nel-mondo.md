---
title: 'Gay/Africa: Bene Stanziamenti, l''Italia diventi campione nelle difesa dei diritti umani nel Mondo'
date: Thu, 04 Dec 2014 12:04:50 +0000
draft: false
tags: [AFRICA, certi diritti, ciad, criminalizzazione, diritti umani, gambia, GAY, lgbti, OMOSESSUALITA', penalizzazione, Transnazionale, Uganda]
---

[![malawi-gay-rights](http://www.certidiritti.org/wp-content/uploads/2014/12/malawi-gay-rights-300x200.jpg)](http://www.certidiritti.org/wp-content/uploads/2014/12/malawi-gay-rights.jpg)"E' incoraggiante la notizia dello stanziamento a favore di un progetto per l'esportazione di buone pratiche tra gli attivisti per i diritti umani ugandesi riferita da Lapo Pistelli nella risposta ad una interrogazione promossa tra gli altri dal senatore Lo Giudice, iscritto all'Associazione Radicale Certi Diritti. Chiederemo al Ministero continui aggiornamenti sul progetto in questione e intensificheremo i nostri contatti con i partner locali."

Lo dichiara Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti. L'organizzazione è da tempo impegnata nella promozione e nella difesa dei diritti umani delle perone LGBTI in italia e nel mondo. Nel 2009, nel corso del suo congresso annuale l'Associazione portò a conoscenza dell'opinione pubblica la situazione delle persone LGBTI ugandesi con la testimonianza di David Kato Kisule, leader del movimento locale. Kato venne ucciso pochi mesi dopo il suo viaggio in Italia da un fanatico omofobo.

"Ricordiamo come la criminalizzazione dell'omosessualità sia un fenomeno globale e, in questa fase di transizione verso un nuovo ordine mondiale, siano necessarie prese di posizione categoriche da parte dei governi e della politica estera europea: la scelta della corte costituzionale ugandese di fare retromarcia rispetto alla legge antigay firmata da Museveni un anno fa è la prova della centralità che la comunità internazionale può avere in questi casi".

Comunicato stampa dell'Associazione Radicale Certi Diritti