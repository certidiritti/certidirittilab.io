---
title: 'CERTI DIRITTI RACCONTA A RUTELLI COME VORREBBE ROMA'
date: Thu, 27 Mar 2008 20:20:22 +0000
draft: false
tags: [certi diritti, Comunicati stampa, rutelli, sindaco]
---

27/03/08

Il sito di Rutelli [www.rutelliroma.it](http://www.rutelliroma.it/ "www.rutelliroma.it") invita in home page tutti i Romani e gli italiani a raccontare una storia su come vorrebbero Roma.

CertiDiritti non perde l'occasione e racconta a Rutelli la proria Capitale: città laica, e promotrice di uguaglianza dei diritti.

Vi invitiamo a fare altrettanto. Intanto potete leggere le seguenti storie:

[http://www.rutelliroma.it/adon.pl?act=doc&doc=618&sid=8](http://www.rutelliroma.it/adon.pl?act=doc&doc=618&sid=8 "http://www.rutelliroma.it/adon.pl?act=doc&doc=618&sid=8")