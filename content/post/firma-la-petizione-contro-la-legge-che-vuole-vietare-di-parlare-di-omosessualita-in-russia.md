---
title: 'Firma la petizione contro la legge che vuole vietare di parlare di omosessualità in Russia'
date: Wed, 23 Nov 2011 08:09:46 +0000
draft: false
tags: [Russia]
---

Entro 48 ore la Russia voterà una legge discriminatoria, voluta dal partito di Medvedev e Putin, contro le persone lesbiche, gay e trans che vuole eliminare la libertà di parlare pubblicamente e manifestare. Firma >

Il disegno di legge vieta manifestazioni come i Gay Pride e qualsiasi altra manifestazione in pubblico del proprio orientamento sessuale, equiparando tali comportamenti alla propaganda della pedofilia.

**FIRMA LA PETIZIONE**[  
www.allout.org/en/russia_silenced](http://www.allout.org/en/russia_silenced)