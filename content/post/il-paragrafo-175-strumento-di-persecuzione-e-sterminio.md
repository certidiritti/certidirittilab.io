---
title: 'IL PARAGRAFO 175: STRUMENTO DI PERSECUZIONE E STERMINIO'
date: Sun, 25 Jan 2009 11:15:34 +0000
draft: false
tags: [Comunicati stampa]
---

Allo scoppio della prima guerra mondiale, Berlino aveva una quarantina di locali gay, la vita per la popolazione lesbica e gay era tra le più floride di tutto il mondo. Tuttavia, nella Germania dell'epoca, a causa dell'eredità del regime prussiano, gli atteggiamenti e le inclinazioni omosessuali erano regolate dal Paragrafo 175 del Codice penale che recitava testualmente: "**Un atto sessuale innaturale commesso tra persone di sesso maschile o da esseri umani con animali è punibile con la prigione. Può essere imposta la pena accessoria della perdita dei diritti civili**".

Tale articolo del codice penale era sostanzialmente inapplicato ma dopo un mese dall'avvento del nazismo, fine gennaio 1933, vennero subito chiuse le riviste e i locali per gay e quando nel 1934 venne messa fuori legge l'ala sinistra del partito nazista, guidata dall'omosessuale Rohm, la repressione si fece ancora più violenta. Gli omosessuali furono i primi ad essere perseguitati con la forza della legge che fino ad allora non era applicata. Il famigerato Paragrafo 175 rimase in vigore fino al 1968 e le persecuzioni di varia natura delle persone omosessuali continuarono fino a quella data con oltre centomila processi anche dopo la fine della seconda guerra mondiale.

La tragedia dello sterminio di milioni di ebrei fu accompagnata dallo sterminio di zingari, omosessuali, malati di mente, tutte minoranze che in qualche modo 'disturbavano' la formazione della 'pura razza ariana'.

Gli omosessuali furono tra i primi ad essere perseguiti grazie ad una legge che era già scritta quando arrivò il nazismo, anzi, fu rafforzata in chiave più repressiva con integrazioni al Paragrafo 175 nel 1935.

Agli omosessuali internati nei campi di concentramento veniva assegnato il triangolo rosa, alle lesbiche, considerate malate di mente, veniva invece assegnato il triangolo nero, quello delle prostitute.

L'omofobia, oggi ancora diffusa in ambito legislativo, sociale e culturale in molte parti del mondo ha, nel corso degli ultimi decenni, rimosso quasi totalmente il ricordo delle vittime della ferocia del nazismo e del famigerato Paragrafo 175.  Così la vita di decine di migliaia di persone si è conclusa in terribili sofferenze e nell'oblio. Occorre accendere i fari su questo lato oscuro della storia affinché la violenza dell'intolleranza, dell'ignoranza, del buio della civiltà non si ripetano. E' quello che con i radicali tentiamo di fare ogni giorno con la nostra lotta nonviolenta, per estendere i diritti a chi non ne ha, per proteggere le persone più deboli, per permettere a ognuno di essere felice.

Ringraziamo coloro che hanno promosso questo giorno di commemorazione che onora ogni persona perseguitata e sterminata solo perché diversa.

Sergio Rovasio

Segretario Associazione Radicale Certi Diritti

[www.certidiritti.it](http://www.certidiritti.it/)     [segretario@certidiritti.it](mailto:segretario@certidiritti.it)