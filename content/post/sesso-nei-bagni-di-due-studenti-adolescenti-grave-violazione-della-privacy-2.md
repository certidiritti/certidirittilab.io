---
title: 'SESSO NEI BAGNI DI DUE STUDENTI ADOLESCENTI; GRAVE VIOLAZIONE DELLA PRIVACY'
date: Tue, 26 Jan 2010 07:25:27 +0000
draft: false
tags: [Senza categoria]
---

**SOSPESI  DUE ADOLESCENTI GAY COLTI DA UN INSEGNANTE A FAR SESSO NEL BAGNO DI UNA SCUOLA A MILANO. L'ASSOCIAZIONE RADICALE CERTI DIRITTI DENUNCIA LA PALESE DISCRIMINAZIONE E LA VIOLAZIONE DELLE NORME SULLA PRIVACY.**

**Dichiarazione di Giovanni Bastianelli, coordinatore dei "Gruppi Studenteschi" dell'Associazione Radicale Certi Diritti, e di Matteo Di Grande, del gruppo "Giovani per i Diritti" di Certi Diritti Roma:  
**

Esprimiamo tutta la nostra solidarietà ai due ragazzi che sono stati sospesi dalle lezioni poiché colti in flagrante da un professore mentre facevano sesso orale nel bagno del loro liceo. Il professore, insospettito dalle frequenti richieste di uscire dall'aula di uno dei due, aveva "pensato bene" di seguirlo fin dentro i bagni della scuola, per poi poco dopo irrompervi dentro e sorprendere sul fatto i due giovani amanti. Tutto questo avveniva senza bussare, come buona educazione insegnerebbe e come il rispetto della privacy suggerirebbe, comportamento più che scorretto che a nostro avviso si potrebbe configurare come un vero e proprio "abuso di potere", reato molto grave per un dipendente pubblico.

Noi, giovani studenti dell'Associazione Radicale Certi Diritti, siamo molto preoccupati per la fase di arretramento culturale cui assistiamo in questo paese, sempre più sessuofobo, in cui "si fa ma non si deve dire", soprattutto qualora si tratti di sesso gay. Del resto, mentre di nascosto su internet o nei luoghi di ritrovo gay frequentati dai più giovani si può trovare di tutto, in pubblico la società dei così detti "grandi", invece di capire ed informare adeguatamente sul valore del sesso e, ad esempio, sui rischi delle malattie a trasmissione sessuale, è capace soltanto di bacchettare e reprimere. La sessualità degli adolescenti, come ampiamente dimostrato, più viene repressa e più si traduce in perversione.

Noi, ragazze e ragazzi dei "Gruppi Studenteschi" dell'associazione radicale Certi Diritti e dei "Giovani per i Diritti" di Certi Diritti Roma crediamo in una "cultura del sesso" finalmente libera, che ci piace chiamare "cultura dell'amore".

E' per questo che continuiamo a batterci, con i nostri mezzi nonviolenti e referendari, affinché anche nelle scuole italiane si cominci seriamente a parlare di informazione sessuale quale materia di studio fondamentale, con personale idoneo, scientificamente preparato e psicologicamente addestrato, come avviene del resto nella stragrande maggioranza delle scuole europee; chissà che questo non serva anche a guarire l'omofobia e la sessuofobia di qualche insegnante "benpensante".

Esprimiamo quindi piena solidarietà ai due amanti, impegnandoci fin da ora ad offrir loro tutto il supporto necessario, qualora ci venga richiesto, affinché vergognosi episodi come quello messo in atto dal docente di Milano non si ripetano mai più.

Giovanni Bastianelli  
Matteo Di Grande