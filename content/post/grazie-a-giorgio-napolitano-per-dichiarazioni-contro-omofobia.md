---
title: 'GRAZIE A GIORGIO NAPOLITANO PER DICHIARAZIONI CONTRO OMOFOBIA'
date: Wed, 09 Sep 2009 12:17:21 +0000
draft: false
tags: [Comunicati stampa]
---

**LOTTA ALLA VIOLENZA OMOFOBICA: GRAZIE AL PRESIDENTE GIORGIO NAPOLITANO PER LE SUE PAROLE. LE STESSE RAGIONI DA LUI INDICATE SONO ALLA BASE DELLA NOSTRA INIZIATIVA POLITICA PER L’EGUAGLIANZA DEI DIRITTI.**

**Comunicato Stampa dell’Associazione Radicale Certi Diritti.**

**Roma, 9 settembre 2009**

**L'Associazione Radicale Certi Diritti ringrazia il Presidente della Repubblica Giorgio Napolitano per le importanti dichiarazioni rilasciate oggi alla Conferenza Internazionale sulla violenza contro le donne a Roma. Che il Presidente della Repubblica citi assieme le emergenze omofobia, xenofobia e violenza sulle donne parlando di tutela dei diritti senza allontanarsi dalla Costituzione e indicandole come un tutt'uno con la causa del rifiuto dell'intolleranza e della violenza, conferma della necessità che ha questo paese di crescere riguardo la difesa e la promozione dei diritti civili su molti fronti e che, unitamente, eterosessuali ed omosessuali devono spendersi per una societa' piu' inclusiva e democratica che sappia difendere i deboli e chi non ha diritti.  
  
E' proprio sulla base di cio' che ricorda Napolitano – laddove la stessa Costituzione parla di eguaglianza tra i cittadini, cosi' come la Carta  Europea dei diritti umani vincola alla non discriminazione - che nasce la rivendicazione dell'Associazione Radicale Certi Diritti assieme a Rete Lenford - Avvocatura per i diritti lgbt.**

**

  
Infatti le due associazioni hanno avviato da tempo una battaglia per il riconoscimento del matrimonio tra persone dello stesso sesso. E a breve la Corte Costituzionale dovra' esprimersi su questa questione.  
Nel frattempo nel nostro Paese bisogna lavorare per una cultura dell'accettazione e dell'inclusione e tutte le nostre istituzioni in questo senso devono farsi carico di questo impegno.  
  
L'input odierno della piu' alta carica dello Stato ci ricorda il principio di autodeterminazione sancito dalla nostra Costituzione , sia che esso riguardi il testamento biologico, la fecondazione assistita, la pillola Ru486 o appunto il matrimonio tra persone dello stesso sesso.  
  
Una crescita effettiva e allargata dei diritti civili favorira' sicuramente , e' nelle nostre idee, il benessere di tutta la popolazione senza distinzione di sesso, razza o religione

**