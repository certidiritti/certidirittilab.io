---
title: 'Anche le coppie dello stesso sesso che hanno un''unione civile celebrata all''estero possono ottenere il permesso di soggiorno in Italia'
date: Fri, 28 Sep 2012 10:13:09 +0000
draft: false
tags: [Diritto di Famiglia]
---

Comunicato stampa dell’Associazione Radicale Certi Diritti

Milano, 28 settembre 2012

Per la prima volta in Italia una ragazza che ha sottoscritto un PACS in Francia con un’italiana, ha ottenuto il permesso di soggiorno come famigliare di cittadina europea dalla Questura di Milano.

Le leggi sulla libera circolazione in Europa restano l’unica possibilità per riconoscere in Italia, almeno per quanto riguarda il soggiorno, il diritto per le coppie miste dello stesso sesso a non essere discriminate.

Fino a oggi le istanze sono state presentate da coppie omosessuali che, sposate all’estero, hanno deciso di vivere in Italia, ma la normativa europea è chiara e comprende l’inclusione di ogni tipo di unione  debitamente attestata  dallo  Stato  del  cittadino dell'Unione.  È questo il caso di una ragazza proveniente dalle Seychelles “pacsata” con un’italiana che da oggi ufficialmente può vivere con la partner nel nostro Paese.

Questo è un esempio evidente di come la normativa europea sempre più entri a far parte del nostro ordinamento per quanto riguarda la tutela dei diritti fondamentali del cittadino.

Fatima e Manuela, che si sono rivolte alla rete del Progetto IO (Immigrazione e omosessualità) di Milano, con il supporto dell’Associazione radicale Certi Diritti, sono entrate a far parte a pieno titolo del cammino che le persone LGBTI hanno intrapreso per giungere all’approvazione del matrimonio civile egualitario in Italia, che rimane l’unico modo rimuovere definitivamente tutte le discriminazioni che ancora colpiscono tutte le coppie omosessuali. È incredibile che, ancora nel 2012, il desiderio di queste persone di vedere riconosciute le proprie coppie e la loro felicità nel vivere liberamente la propria vita familiare scateni le ire di politici codini o baciapile.