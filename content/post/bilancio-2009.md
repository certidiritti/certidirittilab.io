---
title: 'BILANCIO 2009'
date: Sun, 30 May 2010 13:51:02 +0000
draft: false
tags: [Politica]
---

**INTROITI**

Quote d'iscrizione  

€ 8.844,12+

Interessi bancari

€ 4,25

**TOTALE  
**

**=€ 8.848,37**

**SPESE SOSTENUTE**

Viaggi

€ 297,60

Oneri e servizi bancari

€ 212,08

Commissioni su carte di credito

€ 3,74

Varie

€ 12,00

**Servizi comuni**

**€ 525,42 +  
**

Spedizioni

€ 10,40

Stampa e spedizione tessere

€ 414,00

Sito web

€ 170,06

**Campagne di informazione e autofinanziamento**

**€ 594,46+**

Riunione di Direttivo del 20 settembre 2009

€ 307,00

**Riunioni**

**€ 307,00+**

Contributi a Partito Radicale

€ 300,00

Iscrizione ILGA Europe

€ 125,00

Contributo ad Arcigay

€ 300,00

**Contributi erogati**

**€ 725,00+**

Gay Pride Genova

€ 2.578,00

Iniziative politiche

€ 1.228,00

Convegno del 09/10/2010

€ 390,00

Partecipazione a Conferenza ILGA

€ 840,66

**Iniziative politiche**

**€ 5.036,66+**

Congresso di Firenze del 30/01/10

€ 300,00

**Congressi**

**€ 300,00+**

  

  

**TOTALE**

**=€ 7.902,54**

**ECCEDENZE BILANCIO 2009**

**€ 1.359,83**

  

[**Bilancio 2008**](chi-siamo/bilancio/bilancio-2008.html)