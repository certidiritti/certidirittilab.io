---
title: 'Buone notizie dall''Uganda. La Corte Costituzionale ha bocciato le leggi antigay. Un primo passo.'
date: Fri, 01 Aug 2014 12:05:05 +0000
draft: false
tags: [Africa]
---

[![9cbbd9b599ba873184b5e4ff80ef41e2-k60-U10302301442919Y0E-640x320@LaStampa.it](http://www.certidiritti.org/wp-content/uploads/2014/08/9cbbd9b599ba873184b5e4ff80ef41e2-k60-U10302301442919Y0E-640x320@LaStampa.it_-150x150.jpg)](http://www.certidiritti.org/wp-content/uploads/2014/08/9cbbd9b599ba873184b5e4ff80ef41e2-k60-U10302301442919Y0E-640x320@LaStampa.it_.jpg)Venerdì scorso la Corte costituzionale ugandese, su ricorso di alcune associazioni di difesa dei diritti umani, hanno bocciato la legge ugandese che inaspriva moltissimo le norme contor le persone omosessuali e transessuali, arrivando fino alla pena dell’ergastolo. La Corte ha bocciato la legge per motivi procedurali e non è intervenuta su questioni connesse ala difesa dei diritti umani. “Crediamo sia un passo avanti, ha dichiarato l’Associazione radicale Certi Diritti, che ha dato ragione a tutti coloro che nel mondo, e soprattutto in Uganda, hanno lottato e stanno lottando contro questa palese violazione del diritto e dei diritti umani di tutti e tutte. Si deve continuare su questa strada per combattere ogni forma di violenza e integralismo criminoso. In tutta l’Africa e non solo in Uganda.”

Comunicato stampa dell'Associazione Radicale Certi Diritti