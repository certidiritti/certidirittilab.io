---
title: 'APPELLO AI MEDIA PER MAGGIOR RISPETTO VERSO LE  PERSONE TRANSESSUALI'
date: Sat, 24 Oct 2009 09:10:56 +0000
draft: false
tags: [Comunicati stampa]
---

**TRANSESSUALE AL MASCHILE O AL FEMMINILE: APPELLO AI MEDIA AD AVERE MAGGIOR RISPETTO PER LE PERSONE TRANSESSUALI. E’ VERO CHE POSSONO ESSERE DEFINITE ANCHE Al MASCHILE MA NON IN QUESTO CASO.**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

“In questi giorni e ore sui media si torna a parlare di transessuali per vicende di cronaca (e politica) che non intendiamo qui commentare. Ma v’è un fatto che merita invece una nostra segnalazione-appello ai media.

Definire al maschile la persona transessuale protagonista dei fatti di questi giorni è un errore. Difatti la persona di cui si parla è una transessuale, non ‘un’ transessuale.

Con la parola **transessuale** si indica generalmente una persona che persistentemente sente di appartenere al sesso opposto a quello anagrafico e fisiologico. E questo è il caso oggi della persona transessuale protagonista dell’attenzione dei media.

Vero è che la realtà transessuale riguarda entrambe le direzioni di ‘transizione’: esistono persone transessuali maschi transizionanti femmina e transessuali femmine transizionanti maschio. La persona transessuale di cui si parla oggi riguarda il primo caso”.