---
title: 'MINISTERO INTERNI DICE: "GARANTE PRIVACY AUTORIZZA SCHEDATURE ROM", NON CI RISULTA!'
date: Thu, 24 Jul 2008 11:05:23 +0000
draft: false
tags: [Comunicati stampa]
---

Secondo informazioni diffuse nei giorni scorsi dal Ministero degli Interni, il Garante per la privacy avrebbe autorizzato le linee guida riguardo il censimento dei Rom in alcune città italiane; perchè  non c'è traccia di questa autorizzazione da nessuna parte?  Nel sito del Garante sono  pubblicati comunicati che dicono esattamente il contrario. Quale la verità?

  

Sulla vicenda è stata scritta una lettera al Garante per la Privacy firmata da Marco Cappato, deputato europeo radicale, Ottavio  Marzocchi, responsabile per le questioni europee di Certi Diritti e Sergio Rovasio, Segretario Ass. radicale Certi Diritti hanno scritto

  

  

Roma - Bruxelles, 24 luglio 2008

  

Al Signor Garante per la Privacy.

Oggetto: _Approvazione da parte del garante delle linee guida del Ministero dell'Interno in merito al censimento dei residenti nei campi nomadi_  

Signor Garante per la privacy,

il Ministro dell'Interno ha in questi giorni affermato che il suo ufficio ha approvato le linee guida del Ministero in merito al censimento dei residenti nei campi nomadi, come riportato dalla stampa.

Controllando il sito del Garante per la Privacy abbiamo visto che gli ultimi vostri comunicati in merito al censimento dei Rom sostengono esattamente il contrario, e  non vi sono comunicazioni più recenti.

Potreste confermare se le notizie diffuse  dal Ministero degli Interni sono vere?  Nnel caso in cui abbiate elaborato una opinione scritta, é possibile averla al fine di diffonderla presso istituzione e Ong europee e italiane che, come sa, si stanno occupando dell'argomento con attenzione e preoccupazione?

Grazie, cordiali saluti,

**Marco Cappato, Deputato europeo radicale**

**Ottavio Marzocchi, responsabile per le questioni europee dell'Associazione Radicale Certi Diritti**

**Sergio Rovasioi, Segretario Ass. Radicale Certi Diritti**