---
title: '25 maggio convegno "questo matrimonio, s''ha da fare?"'
date: Mon, 24 May 2010 08:39:45 +0000
draft: false
tags: [Senza categoria]
---

[![30075_125288234156890_100000271423188_248497_2516450_n](http://www.certidiritti.org/wp-content/uploads/2010/05/30075_125288234156890_100000271423188_248497_2516450_n.jpg)](http://www.certidiritti.org/wp-content/uploads/2010/05/30075_125288234156890_100000271423188_248497_2516450_n.jpg)Nell'ambito degli eventi per il Sicilia Pride 2010, il primo lgbt pride mai realizzato a Palermo e delle contestuali celebrazioni per il trentennale della nascita di Arcigay (fondata nel 1980 a Palermo), Arcigay Palermo sta organizzando un convegno sulle prospettive di tutela delle coppie formate da persone dello stesso sesso alla luce della recente sentenza della corte costituzionale.

Il convegno, dal titolo "Questo matrimonio s'ha da fare?" avrà luogo il 25 maggio presso la FACOLTA' DI SCIENZE POLITICHE dell'Università di Palermo alle ore 16 e con la partecipazione di:  
\- Rita Borsellino, deputata al Parlamento Europeo  
\- Lino Leanza, assessore regionale della Famiglia, delle Politiche sociali e del Lavoro (da confermare)  
\- Antonello Cracolici deputato all'Assemblea Regionale Siciliana (da confermare)  
\- Alessandro Aricò, deputato all'Assemblea Regionale Siciliana  
\- Stefania Munafò, consigliere comunale Comune di Palermo  
\- Sergio Rovasio, segretario di Associazione Radicale Certi Diritti  
\- Paolo Patanè, presidente Arcigay nazionale  
\- Valerio Barrale, coordinatore regionale dei giovani UDC Sicilia  
\- Marco Carnabuci, responsabile giuridico Arcigay Palermo, rete Lenford  
\- Guido Galipò, dottore di ricerca in Diritti fondamentali, Università di Catania