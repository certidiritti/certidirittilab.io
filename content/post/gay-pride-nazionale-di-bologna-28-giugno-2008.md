---
title: 'GAY PRIDE NAZIONALE DI BOLOGNA 28 GIUGNO 2008'
date: Thu, 26 Jun 2008 14:10:24 +0000
draft: false
tags: [Comunicati stampa]
---

**GAY PRIDE NAZIONALE DI BOLOGNA 28 GIUGNO 2008:  GLI APPUNTAMENTI DI CERTI DIRITTI IN OCCASIONE DELLA GIORNATA MONDIALE DELL'ORGOGLIO GAY.**

Hanno aderito al Gay Pride di Bologna, che si svolgerà il pomeriggio di sabato 28 giugno, l'Associazione Luca Coscioni, Radicali Italiani, Nessuno Tocchi Caino e l' Associazione Radicale Certi Diritti.  ** Nell'ambito del Gay Pride nazionale, Certi Diritti, insieme ai radicali di Bologna, ha programmato la seguenti iniziative:**

**Venerdì 27 giugno, alle ore 20.30,** presso la Sala dell'Angelo, Via San Mamolo, 24 – Bologna:  
Dibattito – Presentazione Associazione Radicale Certi Diritti  
con: -  **Zeno Gobetti**, Segretario Ass. Radicale Giorgiana Masi, Moderatore;; **Sergio Rovasio**, Segretario Certi Diritti; **Michelangelo Stanzanti, Associazione Rosa Arcobaleno; Sergio Lo Giudice**, Consigliere Comunale, Presidente Commissione  Pari Opportunità lgbt;

**Sabato 28 giugno, alle ore 11**, Presso la saletta del bar di Corte Isolani – Bologna,  
dalle ore 11:00 alle ore 12:00, Conferenza stampa dell' Associazione radicale "Certi Diritti"  
con: **Marco Beltrandi, deputato radicale – Pd; Sergio Rovasio, Segretario Associazione Radicale Certi Diritti; Zeno Gobetti, Segretario Ass. Radicale Giorgiana Masi di Bologna; Monica Mischiatti, Segretario Radicali Bologna, membro Comitato Radicali Italiani.**

**Sabato 28 giugno, alle ore 15.30**: al Gay Pride,  
le Associazioni radicali Certi Diritti,  Giorgiana Masi e Radicali Bologna, sfileranno con un carro in comune con l'Associazione Rosa Arcobaleno. Alla fine del corteo, dal palco, in rappresentanza dei radicali, interverrà Enzo Cucco, esponente storico del movimento lgbt italiano, membro del Comitato di Radicali Italiani.

Hanno aderito individualmente, anche se non tutti potranno partecipare agli eventi programmati a Bologna per il concomitante Comitato nazionale di Radicali Italiani in corso a Roma:

**Rita Bernardini, deputata radicale - Pd - Segretaria di Radicali Italiani  
Marco Cappato, deputato europeo radicale - Segretario Associazione Luca Coscioni  
Elisabetta Zamparutti, deputata radicale - Pd, Tesoriera Ri e Ntc;  
Marco Beltrandi, deputato radicale - Pd;  
Sergio D'Elia, Segretario Assocazione Nessuno Tocchi Caino  
Matteo Mecacci, deputato radicale - Pd  
Marco Perduca, Senatore radicale - Pd**

**Maria Antonietta Farina Concioni, deputata radicale – Pd.**

**Per info: Enzo Gobetti 3462295481 – Monica Mischiatti 339-8150231  -   Associazione Radicale Certi Diritti [www.certiditti.it](http://www.certidiritti.it) oppure vai nel [nostro forum](index.php?option=com_fireboard&Itemid=103&func=view&catid=2&id=7#7)  
  
**