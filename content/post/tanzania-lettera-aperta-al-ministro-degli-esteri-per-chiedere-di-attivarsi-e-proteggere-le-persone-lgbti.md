---
title: 'Tanzania: lettera aperta al Ministro degli Esteri per chiedere di attivarsi e proteggere le persone LGBTI'
date: Mon, 12 Nov 2018 11:21:16 +0000
draft: false
tags: [Africa]
---

![LGBT in Tanzania](http://www.certidiritti.org/wp-content/uploads/2018/11/LGBT-in-Tanzania.jpg)
------------------------------------------------------------------------------------------------

Di seguito la lettera aperta promossa dall'Associazione Radicale Certi Diritti e sottoscritta dalle maggiori associazioni LGBTI italiane, Radicali Italiani e Associazione Luca Coscioni per chiedere al ministro Moavero Milanesi di attivarsi per proteggere i diritti delle persone LGBTI in Tanzania.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Egr. Ministro,

lo scorso 31 ottobre, il commissario della regione Dar es Salaam, Paul Makonda, ha tenuto una conferenza stampa **annunciando un piano per arrestare tutti coloro che siano sospettabili di omosessualità**, sottoporli a test anali e offrire loro un'**alternativa tra il sottoporsi a terapie riparative o l'ergastolo**. Il commissario ha anche incitato la cittadinanza a denunciare alla polizia tutte le persone omosessuali o percepite tali e ha affermato di essere già in possesso di centinaia di nomi. L'attuazione del piano era prevista per il 5 novembre.

Paul Makonda non è nuovo a queste minacce, nel luglio 2016, durante una manifestazione politica, aveva **minacciato arrestare tutte le persone omosessuali e coloro che le seguivano sui socia media**, oltre a minacciare di vietare le associazioni che promuovono l'omosessualità.

Il 4 novembre, il Ministro degli Affari Esteri della Tanzania ha affermato pubblicamente che la campagna anti-gay proposta dal Commissario regionale rappresentava solo "la sua opinione personale e non la posizione del governo" e che la Tanzania avrebbe "continuato a rispettare e proteggere" i diritti umani riconosciuti internazionalmente.

Lo stesso giorno, tuttavia, Amnesty International ha rivelato che la polizia ha **arrestato delle persone a Zanzibar** con l'accusa di essere omosessuali.

In Tanzania, i rapporti sessuali tra persone adulte e consenzienti dello stesso sesso sono **puniti con 30 anni di reclusione**. Una delle leggi più dure al mondo.

Dall'elezione del presidente John Magufuli nel dicembre del 2015, la Tanzania ha ripetutamente **violato le libertà d'espressione e associazione** intimidendo e arrestando ripetutamente giornalisti, oppositori politici e critici, secondo varie ONG internazionali, inclusa Human Rights Watch.

Inoltre, le **persone LGBTI sono regolarmente arrestate arbitrariamente e sottoposte a test anali**, un metodo screditato per provare l'orientamento omosessuale che le Nazioni Unite e la Commissione Africana per i diritti umani e dei popoli hanno denunciato come "tortura". Le autorità hanno **chiuso cliniche _gay-friendly _e limitato l'accesso ai lubrificanti a base acquosa, essenziali per la prevenzione dell'AIDS**. L'anno scorso, sempre nella regione di Dar es Salaam, un gruppo di legali sud africani è stato arrestato e deportato per aver partecipato a una conferenza sulla salute e i diritti delle persone LGBTI.

Se è incoraggiante che il governo abbia dichiarato di voler rispettare gli obblighi internazionali sui diritti umani, è assai preoccupante che non sia stata fatta menzione dei passi che il governo intenda intraprendere, considerate le marchiane violazioni sopra riportate.

**Ministro, le associazioni firmatarie si appellano a Lei affinché intraprenda i passi diplomatici necessari per ottenere dalla Tanzania:**

*   il rispetto della Convenzione internazionale sui diritti civili e politici e della Convenzione sui diritti economici, sociali e culturali che la Tanzania ha sottoscritto;
*   l'abrogazione della legge che criminalizza i rapporti sessuali tra persone adulte e consenzienti dello stesso sesso;
*   la liberazione delle persone arrestate per omosessualità e la cessazione immediata degli arresti basati sull'orientamento sessuale o l'identità di genere;
*   il divieto di somministrare test anali;
*   il pieno accesso alle cure mediche di tutti i cittadini a prescindere dall'orientamento sessuale e l'identità di genere;
*   la piena libertà di operare per le associazioni LGBTI e le cliniche _LGBTI-friendly_.

In attesa di un Suo cortese riscontro, poniamo distinti saluti.

_Associazione radicale Certi Diritti, AGEDO Nazionale, Arcigay, Associazione Luca Coscioni, Avvocatura per i Diritti LGBTI - Rete Lenford, Circolo di Cultura Omosessuale Mario Mieli, Famiglie Arcobaleno, GayNet, Globe-MAE, Futura Lgbtqi, Radicali Italiani._