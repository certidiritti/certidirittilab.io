---
title: '2008-0140-Discrimination-SK-ST12461.EN10'
date: Tue, 20 Jul 2010 11:05:29 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 20 July 2010

Interinstitutional File:

2008/0140 (CNS)

12461/10

LIMITE

SOC 470

JAI 651

MI 256

  

  

  

  

  

NOTE

from :

Council General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

11903/10 SOC 451 JAI 611 MI 233 + COR 1

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Further to the meeting of the Working Party on Social Questions on 16 July 2010, delegations will find attached a note from the SK delegation.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**Answers to the questionnaire in document 11903/10 – Slovakia**

1.

Age and disability represent key factors of risk-assessment for insurance system in Slovakia. Insurance companies assess mainly these two elements in case of life, accident and individual health insurance. Assessment and acceptance of the risk is the basis for the provision of insurance product. Insurance companies are obliged to price objectively particular financial service in dependence on the risk of the insured person concerned.

2.

We agree with the opinion that exception for disability should be restricted to the cases where health condition of the client represents relevant risk factor. Nowadays the situation in Slovakia is very similar. Any kind of factor is taken into consideration only when this is regarded as relevant and determining.

3.

The system implemented by the Directive 2004/113/EC (three-step test) is applicable. There is no evidence of any complication on the insurance markets that might be caused by applying the factors of age or disability.

4.

Risk assessment based on particular actuarial principles refers only to insurance area and therefore it might not cover all data sources used by other financial institutions. We think that there is a need to use all accessible data (not only actuarial principles but also medical knowledge and others) which means that there should not be any limitations on specific data which might be used in this respect.

5.

Question is not exactly defined but we are tending towards broader concept of proportionality in consideration of the issues of legitimacy, relevancy and existence of less restrictive alternatives. It is not always possible to define proportionality on pure mathematical basis.

  

6.

It is likely that the possibility to apply the age limits or age bands exists; however at the financial market the individual approach to the client is favored and the risk-assessment related to it.

7.

There is a possibility to include the publication of the data into the Slovak legislation; however we support the open transparency towards the customers. The importance lays on the equal treatment towards all the clients. Especially in case of increased risk that is related to the price of the financial service it is important to explain all the reasons for higher price of the financial service.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_