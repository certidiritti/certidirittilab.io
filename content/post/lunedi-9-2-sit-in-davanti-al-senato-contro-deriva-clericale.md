---
title: 'LUNEDI'' 9-2 SIT-IN DAVANTI AL SENATO CONTRO DERIVA CLERICALE'
date: Sun, 08 Feb 2009 12:44:35 +0000
draft: false
tags: [Comunicati stampa]
---

**L’ASSOCIAZIONE RADICALE CERTI DIRITTI LUNEDI’ 9 FEBBRAIO DAVANTI AL SENATO.**

L’Associazione Radicale Certi Diritti, domani, lunedì 9 febbraio, partecipa al sit-in davanti alla sede del Senato insieme all’Associazione Luca Concioni, Radicali Italiani, Verdi, Sinistra Democratica e Socialisti e altre decine di Associazioni.

L’iniziativa è stata promossa per il rispetto della legge, per la difesa della Costituzione, per il rispetto della dignità, per la laicità e contro le ingerenze vaticane.