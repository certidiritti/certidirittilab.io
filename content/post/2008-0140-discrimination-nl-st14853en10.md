---
title: '2008-0140-Discrimination-NL-ST14853.EN10'
date: Wed, 13 Oct 2010 10:45:24 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 13 October 2010

Interinstitutional File:

2008/0140 (CNS)

14853/10

LIMITE

SOC 646

JAI 836

MI 373

  

  

  

  

  

NOTE

from :

General Secretariat

to :

The Working Party on Social Questions

on :

19 October 2010

No. prev. doc. :

13883/10 SOC 561 JAI 759 MI 317

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

In preparation of the next meeting of the Working Party, which is provisionally scheduled for 19 October 2010, delegations will find attached a note from the NL delegation.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Presidency questionnaire concerning housing**

**\[Document 13883/10\]**

**NL replies**

**1\.** **Situation in Member States**

_1a)       To what extent does your national legislation against discrimination apply to housing?_

*   The **General Equal Treatment Act** has a provision that forbids discrimination in the area of goods and services on the grounds religion or belief, political belief, race, gender, nationality, sexual orientation and civil status. Housing is considered to be a service within the meaning of EU law.

*   The Dutch policy in the field of equal treatment on the ground of disability and chronic illness follows two tracks. The first track is the general policy of encouraging and stimulating; the second track offers legal protection to individuals. In the Netherlands the technical building regulations are laid down in the **Building Code** (Bouwbesluit). The **Social Support Act** (Wet maatschappelijke ondersteuning) arranges funding for constructional and technical accommodations when a disabled person encounters ergonomic obstacles in the general use of his house. This act gives the municipalities the responsibility to compensate people with a handicap and leaves a margin of discretion to the municipalities.

*   The **Equal Treatment Act on the ground of Disability and Chronic Illness** focuses on legal protection for people with a chronic illness or any form of disability. This legal protection is a supplement to the accessibility regulation in the Building Code (Bouwbesluit) and the provisions in the Social Support Act (Wet maatschappelijke ondersteuning). Article 6b of the Dutch Equal Treatment Act on the ground of Disability and Chronic Illness forbids discrimination in offering housing. The obligation for the provider to make adjustments is limited to reasonable adjustments in procedures, agreements, regulations or user instructions and proportionate non-constructional and non-technical accommodations where needed in a particular case.

*   According to The Netherlands (see also doc. 6563/10), the proposed Directive only covers goods and services which are available to the public and for which remuneration is normally required. Properties such as buildings only fall under the scope of this directive if a good or service as covered by this directive is supplied from this property. This also means that the public space such as a square or pavements in front of or on the side of these buildings, do not fall within the scope of the Directive. Only objects that provide direct access to, and are functionally attached to, these buildings, fall under the scope of the Directive. These may include ramps, for example.

_1b)       Do you distinguish (for purpose) between private and public/social housing?_

There is no distinction (for this purpose) between private and public/social housing.

_1c)     Do you have statistics on the number of cases of discrimination in the area of housing, and if so, what is the most prevalent ground?_

_Number of complaints submitted to anti-discrimination bureaus concerning discrimination in the area of housing._

_2004_

_2005_

_2006_

_2007_

_2008_

_2009_

**_72_**

**_91_**

**_82_**

**_80_**

**_107_**

**_163_**

An anti-discrimination bureau is an organization that helps people who _feel_ discriminated. These bureaus register complaints, help people to evaluate their complaint and guide them in possible further action (as stipulated in the Municipal Anti-Discrimination Services Act). They are not equal treatment bodies. The numbers therefore do not reflect actual cases of discrimination.

  

In the area of housing, in the year 2006, two cases of discrimination were brought before the Equal Treatment Committee (Commissie Gelijke Behandeling); both related to race/nationality. The Committee accepted these complaints. In 2010 the Committee accepted one complaint in the area of housing related to the ground disability.[\[1\]](#_ftn1)

**2.         Social housing**

_2a)       Do you consider social housing to be a service (for the purpose of EU law)?_

Yes. Social housing is seen as a service of general economic interest. In the General Equal Treatment Act, housing (not only social housing) is placed under the article that deals with the supply of goods and services. It states explicitly that organisations dealing with housing (volkshuisvesting) fall within the scope of the article.

_2b)       If not, do you consider it to be a part of the social protection system?_

**3.         Private life**

_3a)    Is this better taken into account by an exclusion of transactions in the area of private life from the scope of the directive, or by an exclusion of activities which are not commercial or professional_

The Dutch delegation feels that both elements are important in order to avoid creating a situation where professional/commercial activities in the private sphere (a person's own home) would have to be made accessible to persons with a disability.

_3b)_ _How is commercial/professional housing defined in your national legislation?_

There is no definition of commercial/professional housing in the Dutch legislation.

The regulation regarding rent and rent prices is laid down in the Civil Code. In addition to this there is, among others, legislation regarding:

  

*   Housing allocation in the Accommodation Act (Huisvestingswet);

*   Rent-benefit

*   Social housing organisations in the Housing Act and the Code on Management of the Social Rental Sector (Besluit beheer sociale-huursector)

In article 7 of the General Equal Treatment Act housing is marked as a good or service.

**4.         Disability – reasonable accommodation**

_4a)_ _In your view, what should the obligation to provide reasonable accommodation mean for providers of housing?_

The aim of the directive is to prohibit the refusal of housing to individuals on discriminatory grounds. The Dutch delegation feels this aim cannot be attained by making all buildings from which housing is offered accessible to disabled persons. This is better attained by adapting a building upon request. Under Dutch law the obligation for the provider to make adjustments is limited to reasonable adjustments in procedures, agreements, regulations or user instructions and proportionate non-constructional and non-technical accommodations where needed in a particular case. This could entail for example, the registration of persons seeking housing, engaging housing agents, and providing information about the supply of housing or about restrictive provisions in contracts (deeds or leases) with regard to user instructions. The **Social Support Act** arranges funding for constructional and technical accommodations when a disabled person  encounters ergonomic obstacles in the general use of his or her house. This act gives the municipalities the responsibility to compensate people with a handicap and leaves a margin of discretion to the municipalities. All of the above must be maintained.

_4b)    Is there legislation on accessibility of buildings (private as well as public/social) in your country?_

In the Netherlands the technical building regulations are laid down in the **Building Code** (Bouwbesluit). These are uniform and performance-based regulations at the national level, which all structures must comply with. Based on the Housing Act, which itself does not contain technical rules, the Building Code is a general administrative order, issued by the central government. New regulations for the construction industry officially came into force on 1 January 2003. They include a revised Building Code and the amended Housing Act.

The Code contains the minimal technical building requirements which are deemed necessary from a public viewpoint and have to be complied with by anyone undertaking construction activities in the Netherlands. One of these requirements concerns accessibility for disabled people.

Article 7 of the General Equal Treatment Act forbids housing organisations to make a distinction in the offering of services.

It is also relevant to mention the Housing Act and the Code on Management of the Social Rental Sector (Besluit beheer sociale-huursector). According to this act and code, housing associations have a legal task to house target groups such as persons with a low income, elderly people, the disabled and people who are in need of physical and mental care and sheltered housing.

**5.         Disability - accessibility**

_5a)    What should the obligation to provide for accessibility measures encompass in the area of housing?_

This obligation should entail generic adjustments by anticipation in procedures mentioned under 4a). The obligation should also entail simple adjustments of a physical nature. The entrance to new houses must be accessible for disabled persons (e.g. wheelchairs).

  

New houses also have to be adaptable for use by disabled persons. Blocks of flats must have an accessible entrance and are obliged to have a lift from a certain minimum height. These requirements only apply to new buildings and in case of complete renovations of existing buildings.

The obligation should also entail the accessible supply of specific houses that have been adjusted or purpose-built for persons with a disability.

The above mentioned are the current legal and policy requirements in the Netherlands and should not be altered.

Accessibility does not mean making all buildings from which housing is offered accessible to disabled persons. The Dutch delegation believes that it would not be proportionate  to require all houses that are rented out to be adjusted beforehand. This is not practical since different handicaps ask for different adjustments. The Dutch delegation is highly concerned that, if all houses were to be adjusted, they would have to be adjusted all over again when a person without a handicap or a person with a different handicap moved in. This can cause unnecessary remodelling of, for instance, apartments. Also, it is not necessary to adjust all houses in advance, because an adjustment can easily be made the moment a disabled person wants to rent a particular house.

This is without prejudice to general measures ensuring the accessibility of apartment blocks, e.g. with respect to common premises or the presence of an elevator.

The Dutch delegation feels there should be no obligation for owners/occupants to make their house or apartment accessible for disabled persons when they enter into the (commercial) activity of selling their property.

_5b)_ _Should there be a difference between old and new buildings_?

Yes, for the implementation of the articles 4 – 4b a distinction must be made.

_5c)_ _Do you support immediate applicability with respect to new buildings?_

No, even for new buildings it can be disproportionate to alter the design or plans. This depends on what exactly ‘new’ means and in which stage of construction or delivery the building is.

_5d)_ _Should there be a difference between the obligations for private and public/social housing respectively?_

No, see the answer to question 1b.

_5e)  Are there public funds available in your country for the adaptation of housing (private as well as public/social)?_

The Social Support Act arranges funding for constructional and technical accommodations. This act gives municipalities the responsibility to compensate people with a handicap for the barriers they encounter in and around the house. The Act leaves a margin of discretion to the municipalities.

Housing associations have a legal task to house target groups such as persons with a low income,  elderly people, the disabled and people who are in need of physical and mental care and sheltered housing. They are not directly subsidized by the state, but they receive some forms of compensation from the state for their legal task.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) The Equal Treatment Committee is not a court and its decisions are not legally binding.