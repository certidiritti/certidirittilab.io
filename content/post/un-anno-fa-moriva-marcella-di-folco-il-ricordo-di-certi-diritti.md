---
title: 'Un anno fa moriva Marcella di Folco. Il ricordo di Certi Diritti'
date: Fri, 09 Sep 2011 07:26:39 +0000
draft: false
tags: [Transessualità]
---

Mercoledì 7 settembre, dopo la proposta in comune di Sel e Pd di intitolarle via Polese, gli amici, la politica e le associazioni hanno ricordato al Cassero di Bologna Marcella di Folco. Il messaggio del Segretario di Certi Diritti Sergio Rovasio.   

7 settembre 2011  
   
In ricordo di Marcella Di Folco

Marcella Di Folco è stata una grande personalità di lotta e di impegno per i diritti civili e umani, di tutti. La sua forza, il suo carattere, la sua grinta, non sono mai passate inosservate ed è anche per questo che il ricordo che ognuno ha di lei è molto forte, bello e non ipocrita.

Ai tempi dell’approvazione della legge del 1982 sul cambio del sesso, quando di fatto occupò gli uffici del Gruppo Parlamentare Radicale, al sesto piano di Via di Uffici del Vicario 21  a Roma, lei aveva l’incarico di telefonare e incontrare i politici dei diversi gruppi parlamentari per cercare di convincerli ad approvare la legge. Ogni volta che passava dalle porte delle sicurezza del Palazzo nasceva una discussione e le sue urla si sentivano dalla piazza; ovviamente aveva ragione, qualcuno non voleva farla salire in quanto sui documenti risultava il nome maschile.  Quando i commessi e gli addetti alla sicurezza la conobbero bene avevano tutti paura di lei e la facevano entrare senza problemi, così come le altre compagne del Mit che insieme a lei lavoravano per l’approvazione della legge.

Marcella è stata una vera politica piena di passione e determinazione. Insieme alle storiche fondatrici dell’allora Movimento Italiano Transessuali, da Pina Bonanno a Gianna Parenti e fino a Roberta Franciolini, il giovedì sera venivano fatte le riunioni settimanali nella sede di Via di Torre Argentina, 18, al terzo piano. Spesso volavano sedie e parrucche, tra urla e discussioni di ogni genere. Il risultato è che ogni giorno c’era un sit-in, una manifestazione, una presenza costante e decisa del Mit alle varie iniziative promosse in diverse città italiane.

Al Congresso dell’Associazione Radicale Certi Diritti fatto a Bologna nel 2009 si iscrisse e partecipo’ ai lavori sempre con la sua solita passione e impegno, urlando e amandoci.

Il più bel ricordo che abbiamo di lei è molto recente. E’ del 17 maggio 2010 quando fummo ricevuti al Quirinale dal Presidente della Repubblica in occasione della giornata internazionale contro l’omofobia e la transfobia. Partecipavano alcune decine di persone, rappresentanti delle principali associazioni lgbt italiane. Io stavo vicino a Marcella perché ci dovevamo raccontare alcune cose.

Appena entrati nel Palazzo del Quirinale ci accorgemmo che nel grande corridoio c’erano i corazzieri in alta divisa, con il pennacchio,  ne segnalai uno a Marcella per quanto era bello, aitante, alto, spettacolare. La Marcella si fermò, lo guardò dall’alto al basso e dal basso all’alto, si tolse il grande cappello e disse a voce alta: “quando ero un uomo sapessi quanti me ne sono fatti di soldati come te!”.  Scoppiammo tutti a ridere e il povero corazziere non ce la fece a trattenere la risata in un evidente imbarazzo generale dei suoi colleghi. Qualcuno la richiamò all’ordine visto ‘il luogo sacro’ in cui ci si trovava e lei ribattè incazzata: “macchè luogo sacro, mica è una chiesa questa, sai???” Arrivammo finalmente a destinazione in una sala meravigliosa e quando entrammo il Presidente della Repubblica si avvicinò subito a Marcella, la salutò con un abbraccio e lei gli ricordò di quando, certamente quando era più giovane, a Capri, si facevano feste e spettacoli. Il Presidente non sembrò ricordare. Marcella poi intervenne per dire di come questo paese continui a discriminare gravemente le persone transessuali.

Questo è il più bel ricordo che abbiamo di Marcella, una donna di lotta che, tra tutti noi, era quella che meritava di più di entrare nel Palazzo del Quirinale, dove tra una risata, una litigata e un intervento politico, davanti al Capo dello Stato, continuava ad essere lei. Il più grande onore glielo rese, crediamo, La Stampa, quando le dedicò una pagina intera sui suoi trascorsi di attrice felliniana e donna d’arte.

All’Europride di Roma dello scorso giugno avevamo sul carro una gigantografia di lei, con un grande cappello e un grande sorriso. Quello che continuerà ad avere sempre, tra un’incazzatura e un’altra.

Ciao Marcella.

Sergio Rovasio, Segretario Associazione Radicale Certi Diritti