---
title: 'Radicale attua disobbedienza civile: a Roma multato per aver parlato con una prostituta. Subito ricorso per violazione diritti costituzionali'
date: Mon, 21 Nov 2011 13:10:23 +0000
draft: false
tags: [Lavoro sessuale]
---

Membro dell’Associazione Radicale Certi Diritti e del Comitato nazionale di Radicali Italiani multato per aver parlato con una prostituta, dopo che gli era stato intimato di non farlo. Immadiato ricorso contro il provvedimento che viola le più elementarti libertà costituzionali.

Roma, 21 novembre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti e di Radicali Roma

Nella notte tra domenica 20 e lunedì 21 novembre, Enrico Salvatori, membro dell’Associazione Radicale Certi Diritti e del Comitato nazionale di Radicali Italiani è stato fermato e multato dai Carabinieri per aver violato l’ordinanza proibizionista e fallimentare sulla prostituzione del Sindaco Gianni Alemanno perché parlava con una prostituta sulla Via Tiburtina (altezza Settecamini) a Roma. Enrico Salvatori ha attuato un’azione di disobbedienza civile in quanto i Carabinieri gli avevano intimato di non rivolgere la parola alla prostituta, cosa che invece ha subito fatto.

Nel verbale contestato al nostro iscritto i Carabinieri hanno dichiarato che Enrico Salvatori “contattava soggetto di sesso femminile dedito alla prostituzione” e che la prostituta “stazionava sul ciglio della strada in luogo abitualmente frequentato da prostitute con atteggiamenti e modalità comportamentali, ovvero indossava capi di abbigliamento atti all’adescamento di persone ivi transitanti, manifestando l’incontrovertibile volontà ad esercitare l’attività di meretricio previo corrispettivo in denaro, creando turbativa ed intralcio alla circolazione stradale”.

L’Associazione Radicale Certi Diritti sosterrà in tutte le sedi il ricorso che l’Avvocato Alessandro Gerardi presenterà al Giudice di Pace anche riguardo la legittimità costituzionale dell’Ordinanza anti-prostituzione della Giunta Alemanno, in quanto, la stessa, limita i diritti fondamentali dei cittadini, dalla libertà di circolazione, a quella di espressione e comportamentale anche delle prostitute.