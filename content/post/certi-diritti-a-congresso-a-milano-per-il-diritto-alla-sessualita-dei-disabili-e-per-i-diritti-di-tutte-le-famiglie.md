---
title: 'Certi Diritti a Congresso a Milano per il diritto alla sessualità dei disabili e per i diritti di tutte le famiglie'
date: Wed, 08 Jan 2014 11:36:40 +0000
draft: false
tags: [Politica]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti.

Roma, 8 gennaio 2014

L'Associazione Radicale Certi Diritti terrà il suo VII Congresso a Milano, presso la Ex Chiesetta del Parco Trotter in via Mosso 7 dal 10 al 12 gennaio 2014. Per la prima volta si discuterà di assistenza sessuale allo scopo di elaborare una proposta nonviolenta per l'interruzione del proibizionismo sessuofobico sui corpi delle persone con disabilità. Ne discuteranno l'11 gennaio Max Ulivieri di Sexabiliti, il sessuologo Fabrizio Quattrini, l'avvocato Massimo Clara e Marco Cappato, consigliere comunale radicale e tesoriere dell'Associazione Luca Coscioni.

Si parlerà naturalmente di diritti umani in Italia e nel mondo anche grazie all'ambasciatore Gian Ludovico de Martino, presidente del Comitato Interministeriale per i Diritti Umani e a Samuel Opio, Executive Director Queer Youth Uganda.

Ma il filo rosso del congresso sarà naturalmente il tema del matrimonio egualitario e della riforma del diritto di famiglia sul quale prenderanno la parola le coppie di persone dello stesso sesso, la prof.ssa Marilisa D'Amico e le altre associazioni LGBTI nazionali (Arcigay, Famiglie Arcobaleno, Rete genitori Rainbow, AGEDO ed Equality Italia) che il 12 alle 11.30 discuteranno insieme di come raggiungere l'obiettivo comune del matrimonio egualitario. Sono stati invitati anche Matteo Renzi e Giancarlo Galan.

Ci porteranno un saluto anche Alessandro Cecchi Paone, Anita Sonego, presidente della Commissione Pari Opportunità del Comune di Mialno, Rita Bernardini e Valerio Federico, rispettivamente segretaria e tesoriere di Radicali Italiani, Nicoletta Paci, Vicesindaco di Parma e Pia Covre, del Comitato per i Diritti delle Prostitute, che ci parlerà dei diritti delle lavoratrici e dei lavoratori del sesso.

Il tutto sarà preceduto venerdì 10 dalle 16 alle 19 sempre a Milano, presso la Ex Chiesetta del Parco Trotter in via Mosso 7, dal seminario La libertà d'espressione in un'ottica liberale: i casi delle proposte di legge contro omo-transfobia e negazionismo. Interverranno:

Valerio Pocar, già titolare dell'insegnamento di Sociologia del Diritto presso l'Università degli studi di Milano-Bicocca

Nicola Riva, assegnista di ricerca in Filosofia del Diritto presso l'Università degli Studi di Milano

Andrew Smith, Article 19

Andrea Bitetto, avvocato

Andrea Pugiotto, ordinario di Diritto Costituzionale presso l'Università degli Studi di Ferrara

Marco Gattuso, Giudice presso il Tribunale di Bologna

Sergio Lo Giudice, senatore della Repubblica