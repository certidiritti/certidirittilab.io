---
title: 'IL PRIMO MATRIMONIO TRA DUE DONNE'
date: Wed, 23 Dec 2009 07:30:21 +0000
draft: false
tags: [Comunicati stampa]
---

Il Parlamento italiano  continua ad eludere la realtà dei fatti ed inevitabilmente si trova ad affrontare paradossi legislativi oltre che infliggere discriminazioni ai propri cittadini ed allontanarsi sempre più dal diritto dell’Unione Europea.

Così la cronica reticenza a varare una legge che tuteli le coppie formate da persone dello stesso sesso  si scontra oggi con un fatto di cronaca che denuncia tutta l’inadeguatezza dei nostri politici incapaci di rappresentare  le varie realtà di questo nostro paese.

Una coppia di donne di Bologna risulta infatti regolarmente sposata. Il matrimonio, contratto inizialmente tra un uomo e una donna, dopo il cambio di sesso del coniuge , è , di fatto, la prima unione, regolarmente registrata, tra due donne.

Si assiste ora all’imbarazzo dei nostri amministratori che non sanno come gestire questa “delicata” situazione che in altri Paesi, al passo con i tempi, configurerebbe un normale atto giuridico .

Sarà difficile però chiedere alla coppia di separarsi alla luce dell’attuale legge sul divorzio. Infatti la stessa prevede , nel caso in cui passi in giudicato una sentenza di rettificazione di attribuzione di sesso, la facoltà e non l’obbligo o l’automatismo di divorziarsi.

Sarebbe quindi auspicabile che il Parlamento emanasse una legge che prevede l’unione tra persone dello stesso sesso per ripristinare di fatto  una condizione di eguaglianza tra i cittadini che già la nostra Costituzione prevede.

Il caso di Bologna rappresenta una anomalia giuridica che deve trovare una soluzione nell’estensione del matrimonio alle coppie di donne e di uomini.