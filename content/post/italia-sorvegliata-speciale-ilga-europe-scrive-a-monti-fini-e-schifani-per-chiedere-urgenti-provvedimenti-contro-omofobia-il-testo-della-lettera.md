---
title: 'Italia sorvegliata speciale. Ilga-Europe scrive a Monti, Fini e Schifani per chiedere urgenti provvedimenti contro omofobia. Il testo della lettera'
date: Tue, 26 Jun 2012 06:15:53 +0000
draft: false
tags: [Politica]
---

Allarmante il livello raggiunto dalle aggressioni omofobe e transfobiche.

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Roma, 26 giugno 2012

Dopo i numerosi e recenti casi di aggressioni omofobe a Roma culminate in quella a Guido Allegrezza e altre persone gay, e la manifestazione “Basta intolleranza”, contro l'omofobia, organizzata a Roma, in piazza Farnese il 22 giugno scorso, ILGA-Europe, l’Associazione Internazionale di Lesbiche, Gay, Bisessuali, Transessuali e intersessuali - ha deciso di scrivere al Presidente del Consiglio, ai presidenti di Camera e Senato, e al prefetto Cirillo, presidente dell’Osservatorio per la sicurezza contro gli atti discriminatori (Oscad) per esprimere la propria preoccupazione a causa dell’allarmante livello raggiunto da omofobia e transfobia in Italia, la quale è evidentemente una sorvegliata speciale in Europa.

ILGA-Europe fa notare che l’Italia è tenuta a prevenire i crimini d’odio e a combattere le discriminazioni basate sull’orientamento sessuale e l’identità di genere dalla Decisione N° 9/09 del dicembre 2009 del Consiglio ministeriale dell’OSCE e dalla Raccomandazione CM/Rec(2010)5 del Comitato dei Ministri del Consiglio d’Europa. Inoltre il diritto a viveri liberi dalla discriminazione e di venire protetti dalla violenza, inclusa quella motivata dall’odio, è riconosciuto da numerosi trattati internazionali ed europei siglati dall’Italia e quindi vincolanti. Anche la Corte Europea dei Diritti dell’Uomo ha recentemente stabilito che “la discriminazione basata sull’orientamento sessuale è altrettanto grave di quella basata sulla razza, l’origine etnica o il colore della pelle” (Vejdeland and others v. Sweden, 2012).

Nella lettera si chiede pertanto esplicitamente l’estensione della legge Mancino del 1993 in modo da prevedere un’aggravante anche per tutte le violenze, minacce di violenza e incitamenti alla violenza basata sull’orientamento sessuale e l’identità di genere. Omofobia e transfobia sono considerate aggravanti di reati già in 16 paesi dell’Unione Europea. La lettera chiede inoltre che i dati sui crimini d’odio siano raccolti, archiviati e pubblicizzati e sottolinea l’importanza di costruire un rapporto di fiducia tra le forze dell’ordine e la comunità LGBTI (lesbica, gay, bisessuale, transessuale e intersessuale) anche con una formazione ad hoc delle forze di pubblica sicurezza.

L’Associazione Radicale Certi Diritti ringrazia vivamente ILGA-Europe per la decisa presa di posizione e spera di sapere presto dal Governo e dal Parlamento italiani come intendano rispettare gli impegni presi a livello internazionale. Non è più possibile aspettare ulteriormente!

Al seguente link il testo integrale della lettera di Ilga-Europe:

[http://www.certidiritti.it/notizie/comunicati-stampa/item/1509-violenza-contro-le-persone-lgbti-in-italia-ilga-europe-scrive-a-monti-ai-presidenti-di-camera-e-senato-e-alloscad](http://www.certidiritti.org/2012/06/25/violenza-contro-le-persone-lgbti-in-italia-ilga-europe-scrive-a-monti-ai-presidenti-di-camera-e-senato-e-alloscad/)