---
title: 'INCONTRO DELLE ASSOCIAZIONI LGBT CON IL MINISTRO CARFAGNA'
date: Thu, 08 Oct 2009 14:15:41 +0000
draft: false
tags: [Comunicati stampa]
---

ROMA, 8 OTTOBRE 2009

**INCONTRO DELLE ASSOCIAZIONI LGBT CON IL MINISTRO PER LE PARI OPPORTUNITA’ ON. MARA CARFAGNA.**

**L’Associazione Radicale Certi Diritti esprime soddisfazione per l’incontro che si è svolto oggi pomeriggio con il Ministro per le Pari Opportunità Mara Carfagna e le Associazioni lgbt, durante il quale sono stati presi impegni da parte del Ministro per azioni concrete di lotta contro l’omofobia. **

****Apprezziamo inoltre l’impegno assunto dal Ministro di far reinserire nel provvedimento in discussione in Parlamento il riferimento all’identità di genere, nella consapevolezza che le persone transessuali, sono quelle che subiscono più di tutte, la discriminazione e la violenza.****

**

Il primo banco di prova sarà quello della prossima settimana nell’aula di Montecitorio.

**Di seguito il testo del documento dell’Associazione Radicale Certi Diritti consegnato al Ministro Carfagna.**

La lotta contro l’omofobia e la transfobia è parte del lavoro necessario per una società più giusta, responsabile e inclusiva, per tutti e tutte.

Noi crediamo che le parole, anche quelle scritte, non debbano essere sprecate. Sul tema della lotta all’omofobia e alla trasfobia sono state prodotte in Europa e dall’Europa leggi, regolamenti, indirizzi generali e programmi specifici che devono solo essere applicati nel nostro Paese, con quel minimo di attività di adeguamento alla realtà italiana di cui hanno bisogno. Il punto di partenza è, quindi, definito. Si tratta di tirar fuori la volontà politica e le risorse necessarie affinchè si passi dalle parole ai fatti.

Ci limitiamo a ribadire per punti alcuni concetti e iniziative che noi consideriamo prioritari.

No alle leggi o agli interventi speciali: un cambio di prospettiva.

Omofobia e transfobia sono aspetti diversi di patologie sociali generali. Lottare contro queste forme di razzismo significa intervenire a favore di una società più giusta, non solo per difendere i diritti di una minoranza. Approvare leggi o programmi o azioni specifici contro l’omofobia, senza che siano inseriti nel quadro complessivo di lotta alle discriminazioni significa ridurre l’impatto delle azioni che verranno prodotte e ingenerare la falsa idea che omofobia e transfobia siano problemi che riguardano solo le persone omosessuali e transessuali invece che l’intera società.

Il codice penale non è nè l’unico né il miglior strumento di intervento.

Visto che nessuno pensa realmente che lo strumento penale possa essere veramente risolutivo per le questioni connesse all’omofobia e alla transfobia, è bene che si passi dalle parole agli atti e si concretizzi quanto previsto (a livello comunitario) dall’articolo 13 del Trattato CE, ovvero programmi di intervento contro ogni forma di discriminazione, come strumento per prevenire gli atti di odio e violenza, e combattere alla radice pregiudizio e ignoranza che sono il nutrimento di questi atti.

Guardiamo all’Europa.

In tutti i paesi europei con i quali noi solitamente ci confrontiamo esistono Agenzie indipendenti per la lotta contro ogni forma di discriminazione. Chiediamo al Governo che ne costruisca una anche in Italia, con risorse e competenze adeguate.

Guardiamo in Italia.

Non è vero che in Italia non si è fatto nulla. Ci sono realtà (Torino, Bologna, Venezia, Roma, Firenze, Agrigento e altre ancora) ed una rete nazionale delle amministrazioni comunali, provinciali e regionali contro le discriminazioni per orientamento sessuale e identità di genere (Rete Ready) che possono essere un punto di partenza. Il Governo quindi coinvolga le regioni italiane, ed il sistema delle autonomie locali, chiamandole a partecipare anche alla individuazione di risorse da dedicare a queste attività.

Azioni trasparenti e valutate.

Il Governo scelga i criteri per individuare chi coinvolgere e come lavorare con i partner individuati nella lotta contro la transfobia e l’omofobia. E stabilisca poche regole, trasparenti e pubbliche, su come si utilizzeranno i fondi individuati e quali metodi di valutazione (effettuata da soggetti esterni al circuito di coloro che utilizzeranno i fondi) dovranno essere utilizzati. Per far si che i controllori non siano anche i controllati.

Infine presentiamo alla Ministro per le Pari Opportunità copia degli emendamenti al pdl contro l’omofobia in questi giorni in discussione alla camera dei Deputati. Al di là del tentativo di migliorare una legge che, se approvata, segnerà un momento importante per la storia del nostro paese, essi sono la testimonianza di proposte concrete sulel quali chiediamo a lei, Signora Ministro, e al Governo, di non chiudere la porta del confronto e della riflessione.

  


**