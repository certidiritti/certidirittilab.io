---
title: 'DETENUTE TRANSESSUALI: CHIESTE MAGGIORI GARANZIE RIGUARDO TRATTAMENTO'
date: Mon, 17 Aug 2009 13:40:09 +0000
draft: false
tags: [Comunicati stampa]
---

**DETENUTE TRANSESSUALI: L’ASSOCIAZIONE RADICALE CERTI DIRITTI CHIEDE AL DAP MAGGIORI GARANZIE E TUTELE RIGUARDO IL LORO TRATTAMENTO DETENTIVO.**

**Roma, 17 agosto 2009**

**Dichiarazione di Sergio Rovasio, Segretario dell’Associazione Radicale Certi Diritti:**

“In occasione dell’iniziativa ‘Ferragosto nelle carceri’ lo scorso 14 agosto ci siamo recati, nel carcere di Rebibbia dove, insieme al deputato radicale del Pd Matteo Mecacci, e al Tesoriere di Radicali Italiani, Michele de Lucia, abbiamo fatto visita al reparto delle persone transessuali. Nel reparto vi sono una decina di detenute per lo più sudamericane. Nel reparto, distinto e separato da quello degli uomini, è garantita l’assistenza medica e la somministrazione delle cure ormonali anche se mancano totalmente i servizi di assistenza e intervento specialistico di cui le persone transessuali necessitano periodicamente riguardo il loro equilibrio psico-fisico. In particolare ciò che preoccupa è la non totale separazione dall’area dei detenuti comuni, ciò perché evidentemente lo Stato non riconosce di sesso femminile le persone che non hanno ancora avuto il cambio anagrafico sui documenti a seguito dell’intervento chirurgico.

L’Associazione Radicale Certi Diritti ha scritto oggi una lettera al Dottor Franco Ionta, Capo del Dipartimento dell’Amministrazione Penitenziaria, nella quale si chiede che per le persone transessuali vengano rispettate alcune basilari regole riguardo il regime detentivo. In particolare nella lettera si fa riferimento allo stato psico-fisico che le condizioni carcerarie rendono alterato e ancora più difficilmente raggiungibile.  Il fatto che non sia possibile, ad esempio, avere accesso a sedute di elettro o laser-depilazione, così come non poter svolgere “l’ora d’aria” con le donne, ma anzi, in alcuni casi con i detenuti comuni, rende certamente la vita quotidiana nel carcere estremamente difficile. Le persone transessuali si trovano in condizioni di maggiori vulnerabilità proprio per la condizione di estrema difficoltà ad essere socialmente accettate, per questo occorre avere un approccio particolare nei loro confronti, oggi del tutto inesistente nelle carceri italiane”.