---
title: 'Circolare del ministero Interni spiega alle questure perchè è possibile rilasciare carta di soggiorno per familiare straniero di cittadino Ue anche a partner dello stesso sesso'
date: Thu, 08 Nov 2012 12:13:58 +0000
draft: false
tags: [Diritto di Famiglia]
---

La circolare cita espressamente la sentenza del Tribunale di Reggio Emilia, frutto della campagna di Affermazione civile 2.0 promossa dall’Associazione Radicale Certi Diritti.

Comunicato Stampa dell’Associazione Radicale Certi Diritti.

Roma, 8 novembre 2012.

Apprendiamo che il 26 ottobre scorso il Ministero dell’Interno ha emanato una [Circolare](notizie/comunicati-stampa/item/download/17_5be4ab65cd064d3530bd45b010ddb1af) avente ad oggetto l’Unione fra persone dello stesso sesso. Nella circolare si spiegano le ragioni per le quali le Questure sono tenute a rilasciare la Carta di soggiorno per familiare straniero di cittadino UE, al partner straniero dello stesso sesso.

La [circolare](notizie/comunicati-stampa/item/download/17_5be4ab65cd064d3530bd45b010ddb1af) dice esplicitamente che, benché “la legislazione italiana non prevede alcuna legge che riconosca le unioni civili e tuteli i diritti delle coppie omosessuali”, nella prassi “si registrano casi di segno opposto in seguito a decisioni della magistratura la quale, nell’esercizio della sua funzione, è chiamata comunque a riempire il vuoto normativo in materia”.

La [circolare](notizie/comunicati-stampa/item/download/17_5be4ab65cd064d3530bd45b010ddb1af) cita espressamente la sentenza del Tribunale di Reggio Emilia, frutto della campagna di Affermazione civile 2.0 promossa dall’Associazione Radicale Certi Diritti, “che ha riconosciuto il diritto ad ottenere un permesso di soggiorno per motivi di famiglia ad un cittadino straniero, che aveva contratto, in un altro Stato dell’Unione, un matrimonio valido, con cittadino italiano dello stesso sesso”. Si evidenzia, inoltre, che tale decisione si inserisce nel solco della sentenza della Corte Costituzionale 138/2010, anch’essa frutto della campagna di Affermazione civile promossa dall’Associazione Radicale Certi Diritti e dall’Avvocatura per i Diritti LGBTI – Rete Lenford, e di quella della Corte di Cassazione 1328/2011.

Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti, commenta: ≪si tratta di un riconoscimento dell’efficacia della via giudiziaria intrapresa dall’Associazione Radicale Certi Diritti dal 2008 di fronte a un Parlamento completamente paralizzato dai veti vaticani e inadempiente rispetto alle richieste europee e, quel che è ancora peggio, indifferente al monito della Corte Costituzionale di riconoscere anche alle persone omosessuali “il diritto fondamentale di vivere liberamente una condizione di coppia”. E’ molto importante anche che la [circolare](notizie/comunicati-stampa/item/download/17_5be4ab65cd064d3530bd45b010ddb1af) citi il passaggio della sentenza 138/2010 per il quale il “diritto all’unità della famiglia che si esprime nella garanzia della convivenza del nucleo familiare costituisce espressione di un diritto umano fondamentale della persona umana”. Che i diritti LGBTI siano diritti umani, come ha affermato il Segretario di Stato USA, lo hanno capito i giudici Italiani e anche il Ministero dell’Interno, quando lo capirà anche il Parlamento, legiferando per garantirne il rispetto?≫.

**[IL TESTO DELLA CIRCOLARE >](notizie/comunicati-stampa/item/download/17_5be4ab65cd064d3530bd45b010ddb1af)**

  
[Circolare\_Ministero\_Interno_-\_Titolo\_di\_soggiorno\_familiare\_gay\_Straniero_-\_26\_ottobre\_2012\_n._8996.pdf](http://www.certidiritti.org/wp-content/uploads/2012/11/Circolare_Ministero_Interno_-_Titolo_di_soggiorno_familiare_gay_Straniero_-_26_ottobre_2012_n._8996.pdf)