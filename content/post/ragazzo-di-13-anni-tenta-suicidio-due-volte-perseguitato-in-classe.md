---
title: 'RAGAZZO DI 13 ANNI TENTA SUICIDIO DUE VOLTE, PERSEGUITATO IN CLASSE'
date: Thu, 09 Jul 2009 06:01:28 +0000
draft: false
tags: [Comunicati stampa]
---

**OMOBULLISMO: RAGAZZO DI 13 TENTA DUE VOLTE SUICIDIO ED E' COSTRETTO A CAMBIARE SCUOLA. DERISO E PERSEGUITATO DAI COMPAGNI PERCHE' ACCUSATO DI ESSERE GAY. INTERROGAZIONE PARLAMENTARE URGENTE DEI DEPUTATI RADICALI AI MINISTRI GELMINI, CARFAGNA E MARONI.**

ROMA, 9 LUGLIO 2009

Di seguito il testo dell'interrogazione oggi depositata a prima firma dell'On. Elisabetta Zamparutti:

**Al Ministro dell'Istruzione**

**Al Ministro per le Pari Opportunità**

**Al Ministro degli Interni**

**Per sapere – Premesso che:**

secondo alcune notizie di stampa, tra tutte il quotidiano La Repubblica di domenica 5 luglio 2009, un ragazzo di 13 anni, di una scuola in Provincia di Pavia, è stato deriso e perseguitato dai compagni che lo ritenevano gay; il ragazzo ha tentato due volte il suicidio, prima con i farmaci e poi tagliandosi le vene;

questo caso di vero e proprio “omobullismo”, sfociato anche in atti di violenza contro il ragazzo, ha avuto delle conseguenze che hanno imposto al ragazzo il cambio della scuola per poter proseguire gli studi;

secondo quanto verificato dall’Avvocato Francesco Bilotta, tra i fondatori di Rete Lenford - Avvocatura per i diritti di lesbiche, gay, bisessuali e transessuali, il caso è stato sottovalutato dal corpo docente e dai dirigenti scolastici della scuola; solo il trasferimento presso una scuola di Pavia ha consentito al ragazzo di potersi diplomare;

**Per sapere:**

quanti sono stati negli ultimi due anni i casi di violenza contro studenti a causa del loro orientamento sessuale;

se non ritenga il Governo urgente attivare corsi di informazione ed educazione specifici per prevenire casi come quello indicato in premessa;

se non ritengano urgente i Ministri verificare con le Associazioni lgbt e Rete Lenford, avvocatura per i diritti lgbt, quanti casi di bullismo sono stati denunciati da studenti nelle loro scuole a causa dell’orientamento sessuale;

quali azioni concrete sono state attivate dal Governo verso il corpo docente e presso i dirigenti scolastici in Italia affinchè vengano monitorati in modo dettagliato i casi di violenza e la causa che li ha determinati.

**Elisabetta Zamparutti**

**Marco Beltrandi**

**Rita Bernardini**

**Maria Antonietta Farina Coscioni**

**Matteo Mecacci**

**Maurizio Turco**