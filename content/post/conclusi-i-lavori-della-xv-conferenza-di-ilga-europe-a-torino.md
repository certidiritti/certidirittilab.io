---
title: 'Conclusi i lavori della XV Conferenza di Ilga-Europe a Torino'
date: Wed, 02 Nov 2011 11:09:16 +0000
draft: false
tags: [Movimento LGBTI]
---

La Conferenza più partecipata della storia dell'organizzazione. Il messaggio del Presidente della Repubblica e la situazione italia, l'emergenza Cipro Nord, l'elezione dei nuovi membri del Board, i prossimi appuntamenti.

Si sono conclusi domenica 30 ottobre 2011 a Torino i lavori della XV Conferenza di ILGA-Europe. I partecipanti sono stati 343, provenienti da 43 paesi diversi, rendendo la Conferenza di Torino la più partecipata della storia dell’organizzazione, anche grazie al contributo della Città di Torino che ha consentito di offrire molte borse di studio per facilitare la partecipazione di delegati e associazioni provenienti da tutta Europa, soprattutto dai paesi dell’Est e dell’area balcanica.

La Conferenza ha approvato, tra gli altri, un documento relativo alla grave situazione della parte Nord di Cipro, che è rimasto l’unico paese in Europa dove i rapporti tra adulti consenzienti dello stesso sesso sono ancora un reato.

La Conferenza, inoltre, attraverso un breve documento approvato all’unanimità ed una dichiarazione di Martin Christensen (co-chair dell’organizzazione) ha ringraziato il Presidente della Repubblica e tutte le Istituzioni italiane per le importanti parole ed i patrocini indirizzati alla Conferenza, sottolineando come gli stessi possano e debbano trasformarsi in un nuovo periodo e nuove concrete iniziative per la piena uguaglianza delle persone omosessuali, transessuali e intersessuali in Italia. Anche in considerazione del ruolo di membro fondatore dell’Unione Europea e del Consiglio d’Europa che l’Italia riveste.  
Anche il Comitato organizzatore italiano ha espresso gratitudine per i messaggi ed i patrocini ricevuti, con particolare riferimento alle parole del Presidente della Repubblica: si tratta della prima volta in Italia che tutte le Istituzioni nazionali concedono il patrocinio ad un evento politico e sociale relativo alla comunità lgbti italiana. Proprio per questo è assolutamente necessario che il Parlamento e il Governo italiani assumano quanto prima quei provvedimenti necessari per la piena salvaguardia della dignità e dei diritti delle persone gay, lesbiche, transessuali e intersessuali, ed il pieno recepimento dei principi e delle norme comunitarie in materia. E’ urgente, in altre parole, passare dalle parole ai fatti.

Il titolo della Conferenza (“Valori tradizionali e diritti umani: scontro o dialogo”) è stato al centro di una Conferenza che si è occupata di molti temi, comprese alcune iniziative in merito alla storia del movimento in Europa e nel mondo. A questo proposito durante la conferenza sono stati ricordati:  
Axel Axgil, morto proporio durante i giorni della Conferenza ILGA-Europe (a cui ha partecipato con un suo messaggio video pre-registrato) tra i fondatori del movimento lgbt danese nel 1948 che insieme al suo partner ottenne, per primo in Mondo, il certificato di partnership da parte dello stato danese; Enzo Francone, morto nel 2009, tra i fondatori di ILGA nel 1978, che ha sviluppato numerose attività internazionali tra cui le proteste a teheran nel 1979 e a Mosca nel 1980 contro la persecuzione delle persone omosessuali in quei paesi.

Alla Conferenza hanno partecipato, tra gli italiani, con il loro intervento Emma Bonino, Anna Paola Concia, Angelo Pezzana, Mariacristina Spinosa (Assessore alle Pari Opportunità dela Città di Torino), Mariagiuseppina Puglisi (Assessore alle pari Opportunità della Provincia di Torino), Massimiliano Monnanni (direttore UNAR-DPO-Presidenza del Consiglio dei Ministri).

**Durante la Conferenza sono stati eletti 5 membri del board di ILGA-Europe, che ad oggi risulta così composto:**  
Gabriela Calleja (Malta) co-chair  
Martin K.I. Christensen (Denmark) co-chair  
Paulo Côrte-Real (Portugal)  
Sanja Juras (Croatia)  
Tanja Lehtoranta (Finlad)  
Gregory Czarnecki (Poland)  
Linda Freimane (Latvia)  
Kristian Randjelovič (Serbia)  
Pierre Serne (France).  
Louise Ashworth (UK)

**Il Comitato organizzatore italiano ha inoltre deciso di trasformarsi nella rete italiana delle associazioni e delle persone convolte nelle attività europee e internazionali, al fine di promuovere maggiormente l’Italia in Europa e nel Mondo, e viceversa.**

La XV Conferenza annuale di ILGA-Europe:  
ha ricevuto il saluto del Presidente della Repubblica, On. Giorgio Napolitano,  
ha ottenuto il patrocinio di:  
Senato della Repubblica  
Camera dei Deputati  
Ministro delle Pari Opportunità  
Città di Torino  
Provincia di Torino  
Esperienza italiana 150, Comitato organizzatore

**Hanno aderito al Comitato organizzatore della XV Conferenza annuale di ILGA Europe Torino 2011:**  
Città di Torino,  
che ha nominato l’Assessore Mariacristina Spinosa quale rappresentante nel Comitato d’onore  
Provincia di Torino  
Che ha nominato l’Assessore Mariagiuseppina Puglisi quale rappresentante nel Comitato d’onore  
Regione Piemonte  
Il Comitato organizzatore della XV Conferenza annuale di ILGA Europe Torino 2011 ha ottenuto contributi e supporto da:  
Città di Torino  
Ambasciata Olandese a Roma  
Presidenza del Consiglio regionale del Piemonte  
Camera di commercio industria artigianato e agricoltura di Torino  
Gruppo Consiglio regionale del Piemonte “Uniti per Bresso”  
Gruppo Consiglio Regionale del Piemonte “Sinistra Ecologia Libertà con Vendola”  
Gruppo Consiglio Regionale del Piemonte “Per la Federazione - Sinistra Europea”  
Coordinamento Torino Pride LGBT  
Arcigay Nazionale  
Arcigay Bologna - Cassero  
Ise Bosch  
Il Comitato organizzatore della XV Conferenza annuale di ILGA Europe Torino 2011 è composto da:  
3D – Democratici per pari diritti e dignità di lesbiche, gay, bisessuali e transessuali  
Arcigay  
Arcigay Bologna – Il Cassero  
Arcigay Milano – Centro d’iniziativa gay  
Arcilesbica  
Associazione radicale Certi Diritti  
Coordinamento Torino Pride lgbt  
Famiglie Arcobaleno – Associazione genitori omosessuali  
La Fenice Torino – Gruppo omosessuali felici e credenti  
Gruppo Lambda  
Associazione Quore  
Associazione Ask  
Maurice – Circolo arci di cultura gay, lesbica, bisessuale trangender queer