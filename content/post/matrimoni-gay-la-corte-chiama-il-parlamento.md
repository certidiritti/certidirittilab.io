---
title: 'Matrimoni gay, la Corte «chiama» il Parlamento'
date: Sun, 18 Apr 2010 21:45:37 +0000
draft: false
tags: [Senza categoria]
---

di **Luigi Manconi** su **_[L'](http://cerca.unita.it/data/PDF0115/PDF0115/text2/fork/ref/101083t3.HTM?key=manconi&first=1&orderby=1) Unità_** del 18 aprile 2010, pag. 30

La motivazione, depositata ieri, della sentenza n. 138/2010 della Corte Costituzionale sui matrimoni tra omosessuali conferma che si tratta di una **decisione assai importante per la promozione dei diritti civili in Italia**.

La spensierata soddisfazione, espressa alcuni giorni fa dai più giulivi sottosegretari del governo Berlusconi, Eugenia Roccella e Carlo Giovanardi, si è rivelata fallace. La Consulta, infatti, non ha precluso in alcun modo un intervento legislativo in materia (in base a un’asserita carenza di copertura costituzionale o addirittura di conflitto con essa).

E piuttosto sembra lanciare un **monito al legislatore** affinché disciplini le unioni tra persone dello stesso sesso, in quanto riconducibili alle formazioni sociali dove si svolge la personalità umana, richiamate dall’**articolo 2** della Costituzione. Articolo che ha rappresentato la base per sancire i «nuovi diritti» e, in primo luogo, la dignità della persona, quale diritto «ad avere diritti» (Hannah Arendt).

La sentenza nasce dalle ordinanze di rinvio di alcuni tribunali, chiamati a decidere dell’impugnazione del diniego, opposto dall’ufficiale di stato civile, alla richiesta di pubblicazioni relative a matrimoni omosessuali. Quelle ordinanze hanno censurato alcune norme del codice civile in relazione agli articoli della Costituzione che tutelano i diritti inviolabili dell’uomo, sia come singolo, sia nelle formazioni sociali, l’eguaglianza tra gli individui, la famiglia come «società naturale fondata sul matrimonio», e l’articolo 117 (obbligo di conformità delle leggi al diritto internazionale e comunitario).

La Consulta ha dovuto affrontare, pertanto, un nodo assai complesso e l’ha sciolto assai ragionevolmente. I**l percorso adottato è complesso e, dunque, va seguito con pazienza**.

Le ordinanze presuppongono innanzitutto che le norme del codice civile sul matrimonio, sistematicamente interpretate, si applichino unicamente alle unioni eterosessuali e pertanto siano incostituzionali. Sebbene infatti manchino norme definitorie in tal senso e d’altro canto non sia previsto alcun divieto di matrimonio tra persone dello stesso sesso, il complesso delle norme del codice, così come interpretate secondo una «consolidata e ultramillenaria nozione di matrimonio come unione di un uomo e di una donna», non potrebbero essere dal giudice applicate estensivamente alle coppie omosessuali.

La Corte non smentisce questa interpretazione e **nega che l’impossibilità di applicare le norme sul matrimonio alle coppie omosessuali violi il principio di eguaglianza**: esso presuppone infatti che siano trattate nello stesso modo situazioni uguali, ma non possono ritenersi tali quelle delle coppie eterosessuali da un lato e quelle delle coppie omosessuali, dall’altro. **Né è fondata l’eccezione riferita all’art. 29**, in quanto non è immaginabile che le unioni omosessuali fossero considerate all’interno dell’orizzonte concettuale dei costituenti, che facevano riferimento al matrimonio (eterosessuale) disciplinato dal codice del 1942. Nonostante che quella norma fosse stata concepita come **limite all’intervento statuale nella «società naturale»** generatasi con il matrimonio, e nonostante che **i concetti di famiglia e matrimonio debbano interpretarsi in chiave evolutiva** e non come nozioni cristallizzate.

D’altra parte secondo la Corte, **non sarebbe violato nemmeno l’art. 117** \- per la difformità della legislazione nazionale rispetto alle fonti internazionali e comunitarie - in quanto tali norme non vietano né impongono il riconoscimento del matrimonio omosessuale, lasciando **al legislatore nazionale piena discrezionalità** sul punto.

Quella stessa discrezionalità che la Corte chiama in causa nel dichiarare inammissibile l’eccezione riferita all’art. 2, in quanto si sarebbe pretesa dalla Corte una sentenza che estendesse tout court alle coppie omosessuali l’istituto del matrimonio come disciplinato dal codice civile. Ma la Corte risponde di non poterlo fare, perché in questo caso la pronuncia non è a «rime obbligate»: **la tutela che dall’articolo 2 della Carta deriva alle coppie omosessuali non impone**, insomma, **un’unica soluzione**.

Infatti - come dimostra anche la legislazione degli altri Paesi - ben diverse possono essere le scelte che il legislatore (e solo lui in quanto organo direttamente rappresentativo della sovranità popolare) può fare per garantire alle persone dello stesso sesso quel complesso di diritti sanciti dall’art. 2 e in particolare «il diritto fondamentale di vivere liberamente una condizione di coppia, ottenendone - nei tempi, nei modi e nei limiti stabiliti dalla legge - il riconoscimento giuridico con i connessi diritti e doveri».