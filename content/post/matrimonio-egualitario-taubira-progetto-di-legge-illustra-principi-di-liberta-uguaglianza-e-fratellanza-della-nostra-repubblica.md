---
title: 'Matrimonio egualitario, Taubira: progetto di legge illustra principi di libertà, uguaglianza e fratellanza della nostra Repubblica'
date: Wed, 30 Jan 2013 13:13:19 +0000
draft: false
tags: [Europa]
---

Traduzione in italiano del discorso che il ministro della Giustizia francese ha tenuto in occasione dell'inizio del dibattito sul progetto di legge sul matrimonio per tutti. Traduzione di Leonardo Monaco.

Signor Presidente, Signor Presidente della Commissione, Signora Presidentessa della Commissione degli affari sociali, Signor relatore, Signora relatrice, Signore e Signori deputati,

abbiamo l'onore e il privilegio Dominique Bertinotti, Ministra delegata per la famiglia, e io di presentarvi al nome del Governo un progetto di legge che traduce l'impegno del Presidente della Repubblica ad aprire il matrimonio e l'adozione alle coppie dello stesso sesso.

Trattandosi dello stato civile delle persone, saranno principalmente adattate le norme del codice civile in materia di matrimonio, adozione e attribuzione del cognome.

Dominique Bertinotti e io abbiamo avuto cura di partecipare attivamente, nel rispetto delle prerogative dei parlamentari, alle due sedute della commissione; poiché in seguito alle modifiche del regolamento della Assemblée Nationale è sul testo uscito dai lavori della commissione che dibatteremo queste due settimane, week-end compresi.

Non abbiamo mai sottovalutato l'importanza di questa riforma, questo è il motivo per il quale abbiamo deciso di accogliere tutti coloro che hanno chiesto di essere auditi. Sappiamo quanto possano essere utili i lavori della commissione: hanno migliorato il testo e le disposizioni che vi sono state introdotte saranno presentate dai vostri relatori.

Volevo soffermarmi un attimo sull'evoluzione del matrimonio per comprendere meglio quello che siamo in procinto di fare.

Nel 1989, in occasione dei lavori di riflessione sul bicentenario della Rivoluzione francese, Jean Carbonnier definiva il matrimonio civile la “gloria nascosta” della Rivoluzione. Faceva evidentemente allusione ai vivi dibattiti che hanno accompagnato l'instaurazione del matrimonio civile: la sua dimensione contrattuale, la sua durata e quindi la possibilità di divorziare. Ad oggi, due religioni riconoscono il divorzio: la religione protestate e la religione ebraica; mentre la religione cattolica dichiara il matrimonio indissolubile. Il decano Carbonnier considera dunque che la costituente del 1791 abbia compiuto una vera e propria rivoluzione instaurando il matrimonio civile. La secolarizzazione di questo matrimonio è così consacrata nella Costituzione del 1791.

Il matrimonio civile ha l'impronta dell'uguaglianza. Si tratta di una vera e propria conquista della Repubblica in un movimento generale di laicizzazione della società.

Una tale conquista divenne importate per coloro che dal matrimonio erano esclusi fino ad allora. Dopo la revoca dell'editto di tolleranza, detto “editto di Nantes”, nel 1685 i protestanti potevano sposarsi solo segretamente con i loro pastori. Non potevano fare una famiglia e i loro bambini erano considerati alla pari dei bastardi. Dal 1787 l'editto di tolleranza autorizza di nuovo i preti e i giudici a ufficiare questi matrimoni come ufficiali dello stato civile.

C'è dunque una prima apertura, a due anni dalla Rivoluzione, con il riconoscimento del pluralismo religioso e la possibilità di includere nel matrimonio coloro che ne erano tenuti fuori: i protestanti e gli ebrei. Ma il matrimonio rimaneva ancora una cosa per credenti.

Escludeva anche alcuni tipi di professionisti, ad esempio gli attori, perché la religione proclamava di non riconoscere le pratiche infami degli attori di teatro. E' l'attore Talma che si rivolgerà alla costituente dopo che il curato di Saint Sulpice si rifiutò di pubblicare i bandi del suo matrimonio con una “mondana”, come si diceva all'epoca.

I costituenti decidono allora di instaurare un matrimonio civile e inseriscono nell'articolo 7 del titolo II° della Costituzione del settembre 1791 che il matrimonio non è che contrattuale e che il potere legislativo stabilirà per tutti gli abitanti, senza distinzione alcuna, il modo atraverso il quale le nascite, i matrimoni e i decessi saranno constatati e designerà gli ufficiali incaricati di registrare questi atti.

Il matrimonio civile ora permette di includere non solo i credenti non cattolici: si allarga a tutti. Tutti coloro che desiderano sposarsi posono disporre degli stessi diritti e devono rispondere agli stessi doveri.

Questa concezione del matrimonio civile che ha l'impronta dell'uguaglianza è essenzialmente una libertà, poiché dall'instaurazione stessa del matrimonio anche il divorzio sarà automaticamente riconosciuto. E' scritto nella presentazione dei motivi della legge del 1792 che il divorzio è il risultato di una libertà individuale, ragion per cui un impegno indissolubile ne vorrebbe dire la perdita. Poiché il matrimonio consiste nella libertà delle parti e non nella sacralizzazione di una volontà divina, questa libertà di sposarsi non si concepisce senza la libertà di divorziare, e visto che il matrimonio si stacca dal sacramento che lo aveva preceduto potrà rappresentare i valori repubblicani e integrare progressivamente i valori della società.

La migliore manifestazione di questa libertà si esprime nell'articolo 146 del codice civile che è rimasto alla sua versione originale, secondo il quale non c'è matrimonio senza consenso. Questo articolo stabilisce dunque la piena libertà delle due parti unite in matrimonio.

Se ci ricordiamo che il matrimonio è stato prima di tutto un'unione del patrimonio, delle eredità, dei lignaggi, e che si doveva andare dal notaio prima ancora che davanti al prete; il fatto di riconoscere la libertà dei congiunti è un progresso considerevole, ad oggi ancora impresso nel codice civile.

Il divorzio accompagnerà molto presto il matrimonio. Sarà proibito nel 1816, in un contesto dove i conservatori sono dominanti e dove le libertà – quelle delle donne specialmente – sono in difficoltà. Sarà ristabilito nel 1884 dalla legge Naquet, ancora durante un movimento generale di laicizzazione della società. L'evoluzione del matrimonio porta in effetti il segno della laicità, dell'uguaglianza e della libertà; valori che sono mutati nel nostro Diritto e nella nostra società nell'ambito di un contrasto che ha visto anche episodi di forte tensione.

E' dunque in un momento di laicizzazione dello stato civile, delle libertà individuali, della società in generale che il divorzio sarà di nuovo consentito nel 1884. E' nel corso di questo decennio che altre leggi sulle libertà individuali come la legge sulla stampa, sulla libertà di associazione o la libertà sindacale, e presto la legge di separazione tra Stato e chiese interverranno.

Il divorzio sarà consolidato nel 1975 con il mutuo consenso (già riconosciuto nel 1792), come anche con l'incompatibilità caratteriale.

Il matrimonio accompagnato dal divorzio riconosce dunque la libertà di non sposarsi, ed è il motivo per il quale la legge riconosce anche le famiglie non matrimoniali e per il quale riconoscerà anche i figli di queste famiglie. Lo stesso matrimonio che si distaccò dal sacramento si separerà anche dalla concezione patriarcale della società, concezione che fa del marito e padre il “proprietario”, il “possessore” del patrimonio ma anche dalla moglie e dei figli.

Questa evoluzione del matrimonio e del divorzio, che permetterà d'ora in poi alle coppie di scegliere liberamente l'organizzazione delle loro vite, sarà inserita nella legge perché dopo due secoli l'istituto del matrimonio conosce una evoluzione verso l'uguaglianza. E' proprio quello che noi stiamo facendo qui oggi: perfezionare l'evoluzione verso l'uguaglianza di questo istituto nato con la laicizzazione della società e del matrimonio.

  
Questa evoluzione riguarderà prima di tutto le donne, con la soppressione della referenza al padre di famiglia e con le leggi del 1970 e del 1975 sul mutuo consenso.

Il riconoscimento dei diritti delle donne sarà inserito nella legge progressivamente.

Era l'anno 1970, a malapena 40 anni fa; e ciò vuol dire che ancora oggi vivono donne che hanno avuto bisogno dell'autorizzazione del loro sposo per aprire un conto in banca, sottoscrivere un contratto o disporre del loro salario e dunque essere riconosciute come soggetto di diritto.

Questa evoluzione verso l'uguaglianza, che andrà a modernizzare il nostro istituto del matrimonio riconoscendo la donna come soggetto di diritto riconoscerà anche progressivamente i diritti del bambino. Dalla legge del 1972, il legislatore cesserà di stabilire una differenza tra figli legittimi e figli naturali. Procederà dunque a una fusione della filiazione, in maniera da riconoscere un'uguaglianza di diritti per i bambini, indipendentemente dal fatto che la loro filiazione sia stata legittima o naturale.

Nel 2000 è una sentenza della CEDU, la sentenza Mazurek, che costringerà la Francia a mettere fine alle discriminazioni imposte ai figli nati da adulterio; ed è solo da un'ordinanza del 2005 ratificata da una legge del 2009, che le nozioni di figlio legittimo e naturale spariranno dal nostro codice. Il bambino diventa dunque un soggetto di diritto.

Presentandovi oggi questo progetto di legge che contiene delle disposizioni che aprono ugualmente il matrimonio e l'adozione alle coppie omosessuali, il Governo sceglie di permettere alle coppie dello stesso sesso di entrare in questo istituto e di comporre una famiglia come vale per le coppie eterosessuali: sia per unione di fatto (che chiamiamo concubinato), sia per contratto (il PACS), sia per matrimonio.

E' quest'ultimo l'istituto che il Governo ha deciso di allargare alle coppie dello stesso sesso.

E' un atto di uguaglianza.

Si tratta esattamente del matrimonio che è stabilito dal nostro codice civile, non di un matrimonio riadattato, di un artificio, di uno stratagemma. Si tratta di un matrimonio come contratto tra due persone, come istituto che produce regole di ordine pubblico.

Sì, è proprio il matrimonio, con tutto il suo significato simbolico che il Governo apre alle coppie dello stesso sesso, nelle stesse condizioni di età e di consenso dalla parte di ciascuno dei congiunti; con gli stessi divieti sull'incesto, sulla poligamia e con le stesse obbligazioni di assistenza, di fedeltà e di rispetto della legge del 2006. Con gli stessi obblighi coniuge-coniuge, figli-coniuge e coniuge-figli.

Sì, è proprio il matrimonio che apriamo alle coppie dello stesso sesso. Che ci venga spiegato il perché due persone che si sono conosciute, che si sono amate e che sono invecchiate insieme debbano subire la precarietà dell'ingiustizia, che per il solo fatto di non essere riconosciute dalla legge non venga loro riconosciuto il diritto di un'altra coppia altrettanto stabile che ha deciso di costruire una vita insieme.

Che cosa toglierà il matrimonio omosessuale alle coppie eterosessuali? Se non leva nulla ci permettiamo solo di fare delle obiezioni sui sentimenti e sui comportamenti. Parleremo allora delle bugie che sono state dette in occasione di questa “campagna del panico”, sulla fantomatica soppressione delle parole “padre” e “madre” dal codice civile e dal livret de famille.

Noi rispondiamo, e parliamo di ipocrisia da parte di coloro che si rifiutano di vedere le famiglie omogenitoriali e i loro figli esposti alla casualità della vita. Noi rispondiamo, e parliamo di egoismo da parte di coloro che credono che un istituto della Repubblica possa essere riservato a una sola categoria di cittadini.

Noi diciamo che il matrimonio allargato alle coppie dello stesso sesso illustra bene i principi della nostra Repubblica. Illustra la libertà di scegliersi, la libertà di decidere di vivere insieme.

Noi proclamiamo attraverso questo testo l'uguaglianza di tutte le coppie, di tutte le famiglie.

Infine diciamo che in questo testo c'è anche un passo avanti di fratellanza, perché nessuna diversità può servire da pretesto per una discriminazione di Stato.

In nome di un “diritto del bambino” che non esiste protestate perché matrimonio e adozione saranno aperti alle coppie dello stesso sesso nelle stesse condizioni in cui sono disponibili per le coppie eterosessuali. Detto in altro modo o affermate che le coppie eterosessuali hanno un diritto al bambino scritto nel codice civile, o che questo diritto al bambino non esiste (e in effetti non esiste) e che le coppie omosessuali avranno il diritto di adottare nelle stesse condizioni delle coppie eterosessuali.

In nome di un finto diritto al bambino, negate dei diritti a dei bambini che scegliete di non avere. Il testo che noi presentiamo non ha nulla di distorsivo della Convenzione internazionale dei diritti dell'infante. Al contrario: protegge dei bambini che vi rifiutate di vedere.

Le coppie omosessuali portranno adottare come le coppie etrosessuali e secondo le stesse procedure: il via libera sarà concesso alle stesse condizioni dai consigli generali, l'adozione accordata alle stesse condizioni da parte del giudice in maniera conforme all'articolo 353 del codice civile che dispone che l'adozione è accordata se conforme ai diritti del bambino. Di conseguenza le vostre obiezioni sono infondate se non è una vera difficoltà includere nelle vostre rappresentazioni la legittimità di queste coppie dello stesso sesso. Oggi i vostri figli e nipoti le includono già e le includeranno sempre di più. E non sarete di certo a vostro agio quando per loro curiosità leggeranno i resoconti delle nostre sedute!

Abbiamo dunque deciso di aprire il matrimonio e l'adozione alle coppie dello stesso sesso. Il matrimonio, come ho mostrato con riferimenti storici e giuridici alla mano, è stato un istituto di proprietà visto che è servito prima di tutto a sposare i patrimoni, le eredità e i lignaggi.

E' stato un istituto di proprietà visto che il padre aveva autorità assoluta sulla moglie e sui figli. E' stato un istituto di esclusione, lo abbiamo visto: il matrimonio civile ha messo fine all'esclusione dei credenti non cattolici e di una intera serie di cittadini. Questo matrimonio che è stato un istituto di esclusione, diventerà alla fine un istituto universale!

Potete continuare a rifiutarvi di vedere, a rifiutarvi di vedere tutto ciò che vi è intorno, a rifiutarvi di tollerare la presenza vicino a voi e, probabilmente, nella vostra famiglia di coppie omosessuali. Potete mantenere lo sguardo ostinatamente rivolto al passato.

Avete deciso di protestare contro il riconoscimento dei diritti a queste coppie: è affar vostro. Noi siamo fieri di quello che facciamo.

Noi ne siamo talmente fieri che vorrei definirlo citando le parole del poeta Léon-Gontran Damas: l'atto che stiamo per compiere è “bello come una rosa della quale la torre Eiffel assediata vede alla fine sbocciare i petali”. E' grande come “il bisogno di cambiare aria”.

E' “forte come il grido acuto nella lunga notte”.