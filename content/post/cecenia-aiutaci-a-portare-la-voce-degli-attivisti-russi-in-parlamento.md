---
title: 'Cecenia: aiutaci a portare la voce degli attivisti russi in Parlamento'
date: Wed, 12 Jul 2017 22:36:54 +0000
draft: false
---

* * *

![CECENIA](http://www.certidiritti.org/wp-content/uploads/2017/07/CECENIA-300x251.png) Certi Diritti e AllOut hanno ottenuto dalla Commissione diritti umani del Senato e dal Comitato diritti umani della Camera [**due incontri di approfondimento sui pogrom anti-gay in Cecenia**](http://www.certidiritti.org/2017/07/13/commissioni-camera-e-senato-ascolteranno-attivista-russo-su-persecuzione-gay-in-cecenia/). Mentre proseguono i rapimenti di omosessuali da parte dei paramilitari e mentre su altri fronti la situazione dei diritti umani sembra precipitare sempre di più, **porteremo in Italia Igor Kochetkov del Russian LGBTI network**: il nostro Parlamento sarà così la prima assemblea nazionale ad ascoltare un attivista locale a proposito del pogrom anti-gay in Cecenia. L'incontro in Senato avrà luogo martedì 18 luglio alle 13.30, mentre quello alla Camera dei Deputati mercoledì 19 alle 14.00.

**Per portare Igor a Roma abbiamo preventivato tra viaggio, sistemazione ed interpretariato, una spesa di 1500€**

**Aiutaci a sostenere le spese per questa iniziativa**

\[wufoo username="associazionelucacoscioni" formhash="x1f0dpnc05e3rt5" autoresize="true" height="313" header="show" ssl="true"\]