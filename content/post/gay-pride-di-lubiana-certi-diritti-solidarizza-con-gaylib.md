---
title: 'GAY PRIDE DI LUBIANA:  CERTI DIRITTI SOLIDARIZZA CON GAYLIB'
date: Sun, 22 Jun 2008 13:30:35 +0000
draft: false
tags: [Comunicati stampa]
---

L'Associazione radicale Certi Diritti esprime piena solidarietà all'Associazione Gaylib (gay liberali di centro destra) in seguito alla loro "esclusione" dal Gay Pride di Lubiana al quale la stessa associazione di omosessuali di centro destra aveva dato la propria adesione accettando in toto la piattaforma politica della manifestazione slovena.

La comunicazione di non essere graditi al gay pride, ricevuta dal presidente di Gay Lib, è stata riferita all'Associazione Radicale  Certi Diritti che pure ha aderito e preso parte alla commemorazione del politico olandese Pim Fortuyn celebrata questa mattina  a Provesano. La presidente di Certi Diritti, Clara Comelli, ha voluto sentire gli organizzatori  del gay pride sloveno per avere conferma di questa esclusione. Mitja Blasich, dell'Associazione Slovena Dih e membro del comitato organizzatore, ha confermato alla stessa le motivazioni espresse ad Oliari, riferendo anche che al gay pride di Lubiana sono state invitate tutte le autorità politiche slovene senza esclusione di sorta. Non si capisce perciò - sostiene Clara Comelli - che criterio politico sia stato adottato nell'escludere un'associazione gay italiana anche se schierata nel centro destra. L'essere filoberlusconiani e l'aver commemorato una figura come quella di Pim Fortuyn (sulla cui storia forse ci si dovrebbe documentare di più, prima di bollarlo come xenofobo), non rappresenta certo, secondo Certi Diritti, motivo valido per un'esclusione tanto grave. Difendere i diritti civili delle persone in generale deve restare una priorità cara ad ogni persona libera e democratica, a prescindere dal contesto politico  in cui si sceglie di agire le proprie rivendicazioni. Certi Diritti , che pure doveva essere presente al gay pride di Lubiana, ha deciso di non presenziare alla manifestazione ritenendo che fosse venuto a mancare lo spirito di libertà e tolleranza necessario.

 

Clara Comelli

presidente Associazione Radicale Certi Diritti

www.certidiritti.it