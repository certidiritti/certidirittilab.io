---
title: 'Newsletter 10 Settembre'
date: Sat, 17 Sep 2011 11:17:30 +0000
draft: false
tags: [Politica]
---

**![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)**

oggi apriamo con una bella notizia: la Commissione europea ha condannato le discriminazioni in materia di negato rilascio o rinnovo della patente di guida sulla base dell'orientamento sessuale. Sembra paradossale ma Certi Diritti è dovuta intervenire pochi mesi fa proprio per un caso di questo genere …

Aosta e Pesaro (grazie al lavoro di Radicali Marche) hanno dato la possibilità per le coppie dello stesso sesso di accedere ai bandi per le case popolari. Con il vostro aiuto e le vostre segnalazioni possiamo chiedere la stessa possibilità in tutte le altre città italiane che non lo dovessero prevedere. 

La lotta alle discriminazioni inizia dalla convinzione  di non volerle subire.

Sostieni la nostra lotta contro la sessuofobia e per l'uguaglianza dei diritti  
**[www.certidiritti.it/iscriviti](iscriviti)**

Buona lettura

ASSOCIAZIONE
============

**La campagna di Affermazione Civile per il riconoscimento del matrimonio tra persone dello stesso sesso. **  
[Cos'è e come nasce >](la-campagna-di-affermazione-civile)

**Mostra del Cinema di Venezia: presentazione del documentario di Barbara Cupisti 'Io sono – storie di schiavitù' con Certi Diritti e Libellula. **  
[Leggi >](venezia-presentazone-del-docu-film-io-sono-storie-di-schiavitu-con-le-associazioni-certi-diritti-e-libellula)

**Un anno fa moriva Marcella di Folco. **  
[Il ricordo di Sergio Rovasio >](un-anno-fa-moriva-marcella-di-folco-il-ricordo-di-certi-diritti)

**Luciana Littizzetto e Emma Bonino, iscritte di Certi Diritti, **  
[le più amate dai gay >](luciana-littizzetto-e-emma-bonino-iscritte-a-certi-diritti-le-due-eterosessuali-piu-amate-dai-gay)

**Certi Diritti all'incontro di OSCAD e UNAR su lotta alle discriminazioni. **  
[Leggi >](certi-diritti-allincontro-di-oscad-e-unar-su-lotta-alle-discriminazioni)

**A Pesaro case popolari anche a coppie gay conviventi . **  
[Leggi >](a-pesaro-case-popolari-anche-a-coppie-gay-conviventi)

**UE condanna l'Italia per la patente non rinnovata al ragazzo gay. **  
[Leggi >](condanna-dell-ue-contro-le-discriminazioni-italiane-in-base-allorientamento-sessuale)

RASSEGNA STAMPA
===============

[Lesbica aggredita a Milano >](http://milano.repubblica.it/cronaca/2011/09/08/news/milano_era_al_ristorante_con_la_compagna_un_uomo_l_ha_aggredita_perch_lesbica-21395776/)

**La Rai censura la puntata di una fiction solo perchè due uomini si sposano.**[Leggi >](http://metilparaben.blogspot.com/2011/09/la-cancellazione-degli-omosessuali.html)

**Napoli. 1° Conferenza Internazionale per contrastare le discriminazioni nei riguardi del genere e dell’orientamento sessuale. **  
[Leggi>](http://www.casertanews.it/public/articoli/2011/08/15/075230_attualita-napoli-1-conferenza-internazionale-contrastare-discriminazioni-nei-riguardi-genere-orientamento-sessuale.htm)

**Milano, 16 settembre 2011, Convegno internazionale "Stalking, maltrattamenti, abusi:strategie e modelli di intervento" **  
[Leggi> ](http://www.cipm.it/)

**Milano gay friendly? Per l'assessore Majorino si all'emergere del mondo Lgbt, no ai quartieri ghetto. **  
[Leggi>](http://www.queerblog.it/post/11981/milano-gay-friendly-per-lassessore-majorino-si-allemergere-del-mondo-lgbt-no-ai-quartieri-ghetto)

**Aosta sostiene le giovani coppie. Anche quelle gay. **  
[Leggi>](http://www.giornalettismo.com/archives/143387/aosta-sostiene-le-giovani-coppie-anche-quelle-gay/)

**Festa di nozze per Lo Giudice. Merola: “Ora il Gay Pride a Bologna” **  
[Leggi e guarda il video>](http://www.ilfattoquotidiano.it/2011/09/03/festa-di-nozze-per-lo-giudice-video-merola-ora-il-gay-pride-a-bologna/155092/)

**Bollini 'gay friendly' su negozi, hotel e bar? Giovanardi li boccia: "Così si auto-ghettizzano"  **  
[Leggi>](http://qn.quotidiano.net/cronaca/2011/09/02/573826-bollini_friendly_negozi_hotel.shtml)

**Bollino «gay friendly» a Padova Arriva il no della Curia. **  
[Leggi>](http://corrieredelveneto.corriere.it/veneto/notizie/cronaca/2011/31-agosto-2011/bollino-gay-friendly-padova-arriva-no-curia-1901401310728.shtml)

**Torino. ****"Un bollino per le imprese avversarie di chi discrimina"**  
[Leggi>](http://www3.lastampa.it/torino/sezioni/politica/articolo/lstp/418626/)

**Lucca, il tg 'omofobo': ''Censuriamo Mister Gay e Miss Trans'' **  
[Leggi e guarda il video>](http://tv.repubblica.it/italia/lucca-il-tg-omofobo-censuriamo-mister-gay-e-miss-trans/75125?video)

**Padova. ****Amore gay, due donne sposate dal prete del dissenso. **  
[Leggi>](http://mattinopadova.gelocal.it/cronaca/2011/09/06/news/amore-gay-due-donne-sposate-dal-prete-del-dissenso-4913317)

**Daniele Vanni si giustifica per il video del Tg: "Non sono omofobo, era una battuta infelice!"  **  
[Leggi>](http://www.queerblog.it/post/11967/daniele-vanni-si-giustifica-per-il-video-del-tg-non-sono-omofobo-era-una-battuta-infelice)

**Le confessioni di Marrazzo "Perché andavo in via Gradoli" **  
[Leggi>](http://www.repubblica.it/politica/2011/08/15/news/intervista_marrazzo-20450866/)

**I Trans «Donne all' ennesima Potenza» perché lasciano in Pace gli Uomini? **  
[Leggi>](http://archiviostorico.corriere.it/2011/agosto/17/Trans_Donne_all_ennesima_Potenza_co_9_110817060.shtml)

**Vladimir Luxuria contestata da Forza Nuova : pervertiti. **  
[Leggi>](http://articolotrepalermo.blogspot.com/2011/09/vladimir-luxuria-contestata-da-forza_04.html)

**In Italia un premier gay? Si, ma tenga a freno la sua natura! La gaffe di Laura Allegrin. **  
[Leggi>](http://www.gaywave.it/articolo/in-italia-un-premier-gay-si-ma-tenga-a-freno-la-sua-natura-la-gaffe-di-laura-allegrini/33345/)

**Manovra: Gaylib, riconoscere diritti e rinunciamo alla reversibilità della pensione. **  
[Leggi>](http://www.digayproject.org/Archivio-notizie/manovra_gaylib.php?c=4684&m=15&l=it)

**Partnership fra associazione italiana turismo gay & lesbian e Iglta. **  
[Leggi>](http://www.travelquotidiano.com/parliamo_di/mercato_e_tecnologie/partnership_fra_associazione_italiana_turismo_gay_lesbian_e_iglta/%28tqid%29/29668)

**Ohio. Jackie, il bambino che cambia sesso a dieci anni. **  
[Leggi>](http://affaritaliani.libero.it/cronache/bambino_cambia_sesso_a_dieci_anni020911.html)

**New York: 1400 le coppie gay sposate in un mese. **  
[Leggi>](http://www.queerblog.it/post/11953/new-york-1400-le-coppie-gay-sposate-in-un-mese)

**USA. ****Se i veterani gay licenziati vogliono tornare in divisa****. **  
[Leggi>](http://www.giornalettismo.com/archives/144755/se-i-veterani-gay-licenziati-vogliono-tornare-in-divisa/)

**Cile: sette senatori si sono impegnati a lavorare a favore delle unioni gay. **  
[Leggi>](http://www.queerblog.it/post/11995/cile-sette-senatori-si-sono-impegnati-a-lavorare-a-favore-delle-unioni-gay)

**Messico: è lesbica la prima coppia dello stesso sesso che ha adottato un bimbo. **  
[Leggi>](http://www.queerblog.it/categoria/famiglie)

**Tahilandia. Congedato perché trans. Ma non più "pazzo"  **  
[Leggi>](http://www.lastampa.it/_web/cmstp/tmplRubriche/giornalisti/grubrica.asp?ID_blog=175&ID_articolo=121&ID_sezione=358)

STUDI, RICERCHE E MATERIALI INFORMATIVI
=======================================

**Comune di Pesaro**.** Avviso pubblico per la formazione della graduatoria degli aspiranti all'assegnazione di un alloggio di edilizia residenziale pubblica sovvenzionata . **  
[Scarica >](http://bacheca-atti.comune.pesaro.pu.it/Atti/allegati/405325_graffetta.pdf.pdf)

**Aosta. Coppie gay, arriva il contributo per l’affitto. **  
[Scarica la Delibera n.40 e il regolamento >](http://www.west-info.eu/it/coppie-gay-arriva-il-contributo-per-laffitto-omosessuali-coppie-di-fatto-unioni-civili/)

**Bologna città “gay friendly”, a svelarlo una ricerca. **  
[Leggi >](http://www.telesanterno.com/bologna-citta-gay-friendly-a-svelarlo-una-ricerca-0808.html)

**Come avere successo? Sei studentesse su 10 lo ottengono con il sesso. **  
[Leggi >](http://www.ilgiornale.it/cronache/come_avere_successo_sei_studentesse_10_lo_ottengono_sesso/25-08-2011/articolo-id=541864-page=0-comments=1)

**Stati Uniti. ****Il fanatismo religioso della destra americana****. **  
[Leggi >](http://notizie.radicali.it/articolo/2011-09-06/intervento/stati-uniti-il-fanatismo-religioso-della-destra-americana)

**Texas, Kansas, Montana, Oklahoma: states hold onto laws defining gay ‘conduct’ illegal. **  
[Leggi in inglese >](http://www.lgbtqnation.com/2011/03/texas-kansas-montana-oklahoma-states-hold-onto-laws-defining-gay-conduct-illegal/)

**Africa e omosessualità: i cristiani fomentano l'odio.**  
[Leggi>](http://www.ilgrandecolibri.com/2011/09/africa-e-omosessualita-i-cristiani.html)

**Sessuologia oggi.**   
[Leggi>](http://gazzettadireggio.gelocal.it/cronaca/2011/08/31/news/sessuologia-oggi-1.794950)

LIBRI E CULTURA
===============

**Spot di un lubrificante per lesbiche sulla tv USA. **  
[Leggi e guarda il video>](http://www.queerblog.it/post/11965/spot-di-un-lubrificante-per-lesbiche-sulla-tv-usa)

**Cold Star: video sul bullismo e sulle varie sfumature dell'amore. **  
[ Leggi e guarda il video>](http://www.queerblog.it/post/11939/cold-star-video-sul-bullismo-e-sulle-varie-sfumature-dellamore)

**Lesbiche e gay nuovi supereroi della DC Comics. **  
[Leggi>](http://www.queerblog.it/post/11937/lesbiche-e-gay-nuovi-supereroi-della-dc-comics)

**Il compleanno di liberi tutti . **  
[Leggi>](http://liberitutti.blog.unita.it/il-compleanno-di-liberi-tutti-1.326953)

**La storia del porno _gay_in un volume****. **  
[Leggi>](http://www.gay.it/channel/libri/32349/La-storia-del-porno-gay-in-un-volume.html)

**Il piccolo uovo, la fiaba sulle famiglie gay. **  
[Leggi>](http://affaritaliani.libero.it/milano/piccolo_uovo_fiaba_famiglie_gay060911.html)

**Martha C. Nussbaum, **  
[Disgusto e umanità](http://torino2011.saggiatore.it/disgusto-e-umanita/)**, Il Saggiatore, € 21,00**

**Da rileggere:**  
**Christopher Isherwood, **  
[Christopher e il suo mondo](http://www.lafeltrinelli.it/products/9788877103291/Christopher_e_il_suo_mondo/Christopher_Isherwood.html)**, Feltrinelli,****€ 20,66**

[www.certidiritti.it](http://www.certidiritti.it/)
==================================================