---
title: 'Lettera dei parlamentari europei ai colleghi ugandesi'
date: Wed, 25 May 2011 12:51:40 +0000
draft: false
tags: [Africa]
---

**La lettera è stata inviata l'11 maggio, a poche ore dal voto sulla legge antigay per convincere i parlamentari ugandesi a non votarla. Grazie alle pressioni internazionali il voto è stato poi rimandato.**  
  
Strasbourgo, 11 maggio 2011

  
   
Onorevole portavoce, Onorevoli membri del Parlamento d'Uganda,

Il mondo ha cominciato a reagire all'imminente esame della legge Anti-Omosessualità di Mr Bahati, e la sua possibile adozione prima della fine di questa legislatura. Se la legge non sarà votata entro questa settimana potrebbe essere adottata durante la prossima sessione parlamentare. Noi ci appelliamo a voi perchè non adottite questa legge inumana. . .

  
Noi ci appelliamo a voi perchè non adottite questa legge inumana, non come Europa, non come coloro che vi danno aiuti, non come "polizia morale globale", ma come compagni che fanno leggi e esseri umani. Noi condividiamo i vosttri valori di dignità umana, libertà, pace e libertà dall'oppressione; e la legge Anti-Omosessualità va completamente contro questi valori.  
   
Si è sollevata la preoccupazione che l'omosessualità sia non-Africana; tuttavia l'Africa non è unita contro l'omosessualità.  
Mentre si revisionava il Codice Penale un anno fa, il  Rwanda ha rifiutato di criminalizzare l'omosessualità e ha fatto una forte dichiarazione alle Nazioni Unite nel dicembre 2010 contro la persecuzione di tutte le minoranze,  lesbiche e gay inclusi.

Il Rwanda è non-Africano? Il Sud Africa ha custodito la non discriminazione su basi di orientamento sessuale nella sua costituzione e permette a coppie dello stesso sesso di sposarsi. Il Sud Africa è non Africano? Il Gabon, la Repubblica centrale Africana, il Guinea-Bissau, Capo Verde, il Rwanda,  la Sierra Leone, le Seychelles, São Tomé e Principe, le Mauritius e il Sud Africa uniti hanno fatto dichiarazioni contro la criminalizzazione dell'omosessualità. Queste nazioni sono non-Africane?  
Siamo perfettamente consapevoli di altre violazioni dei diritti umani non meno gravi in Uganda. Abbiamo saputo di autorità che hanno fatto usare gas lacrimogeni sui manifestanti nelle dimostrazioni di massa del 29 Aprile; rappressentanti dell'opposizione hanno subito intimidazioni, sono stati imprigionati o intenzionalmente accecati da scioccanti esibizioni di inumanità commesse da autorità nazionali; e le autorità nazionali opprimono in maniera crescente il popolo dell'Uganda attraverso la repressione fisica, l'intimidazione o l'imprigionamento.

Siamo completamente consapevoli che l'esame all'ultimo minuto di questa legge è un modo eccellente per distrarre la nostra attenzione e con essa l'attenzione del popolo ugandese da questi seri problemi.

La nostra Camera, eletta da 500milioni di cittadini, ha formalmente richiamato l'Uganda perchè non adotti la legge Anti-Omosessualità in due occasioni.  
Siamo convinti che l'adozione di qualunque futura legge che criminalizza il sesso consensuale tra adulti (inclusa l'adozione di questa legge, sia nella forma corrente che in una versione modificata) avrà un grave impatto negativo sulle nostre relazioni bilaterali, sia nella dimensione degli aiuti sia in quella diplomatica.

Ci appelliamo a voi perchè vi asteniate dal criminalizzare ulteriormente l'omosessualità. La popolazione dell'Uganda merita che la vostra attenzione sia posta completamente su problemi più pressanti, incluso un governo libero dalla corruzione, i prezzi per i prodotti base tenuti sotto controllo,la libertà di parola e assemblea e le condizioni di vita decenti.  
Non adottate questa legge.

Heidi Hautala MEP (Membro del Parlamento Europeo)  
Sottocommissario dei Diritti Umani  
Gruppo dei Verdi/Libera alleanza Europea

Michael Cashman MEP  
Co-Presidentedell'intergruppo LGBT Rights  
Gruppo di Alleanza di Socialisti e Democratici  
   
Ulrike Lunacek MEP  
Co-Presidentedell'intergruppo  LGBT Rights  
Gruppo dei Verdi/Libera alleanza Europea  
   
Sirpa Pietikäinen MEP  
Vice-Presidente of the Intergruppo  LGBT Rights  
Gruppo del Partito popolare Europeo  
   
Raül Romeva i Rueda MEP  
Vice-Presidente dell'intergruppodi LGBT Rights  
Gruppo dei Verdi/Libera alleanza Europea  
   
Rui Tavares MEP  
Vice-Presidente dell'intergruppo LGBT Rights  
Gruppo della Sinistra Unita Europe/ Sinistra Verde Nordica  
   
Sophie in 't Veld MEP  
Vice-Presidente dell'intergruppodi LGBT Rights  
Gruppo dell'Alleanza dei Liberali e Democratici per l'Europa

Cecilia Wikström MEP  
Gruppodell'Alleanza dei Liberali e Democratici per l'Europa

Baroness Sarah Ludford MEP  
Gruppo dell'Alleanza dei Liberali e Democratici per l'Europa

Gianni Vattimo MEP  
Gruppo dell'Alleanza dei Liberali e Democratici per l'Europa

Renate Weber MEP  
Gruppo dell'Alleanza dei Liberali e Democratici per l'Europa

Sonia Alfano MEP  
Gruppo dell'Alleanza dei Liberali e Democratici per l'Europa

Marietje Schaake MEP  
Gruppo dell'Alleanza dei Liberali e Democratici per l'Europa