---
title: 'Anniversario della sentenza 138/2010 della Corte Costituzionale: Bernardini, Concia e Associazioni LGBTI scrivono al Governo e ai Capigruppo di Camera e Senato'
date: Fri, 13 Apr 2012 13:36:11 +0000
draft: false
tags: [Movimento LGBTI]
---

Comunicato stampa dell'associazione radicale Certi Diritti

13 aprile 2012

In occasione del secondo anniversario della sentenza 138/2010 della Corte Costituzionale, **l'Associazione Radicale Certi Diritti regalerà a tutti i capigruppo di Camera e Senato ai Presidenti e Vicepresidenti di Camera e Senato, a tutti i ministri e al Presidente del Consiglio una copia del libro "Dal cuore delle coppie al cuore del diritto. L'udienza alla Corte Costituzionale per il diritto al matrimonio tra persone dello stesso sesso"**. Il libro spiega come come la sentenza sia frutto della campagna di Affermazione civile che ha visto molte coppie dello stesso sesso mettersi in gioco direttamente per chiedere di vedere riconosciuti i propri diritti in dialogo con le istituzioni.

Il libro sarà accompagnato da una **lettera firmata da Rita Bernardini, deputata radicale eletta nelle liste del PD, Anna Paola Concia, deputata PD, Yuri Guaiana, Segretario Associazione Radicale Certi Diritti, Paolo Patanè, presidente Arcigay, Paola Brandolini, presidente Arcilesbica, Luca Possenti, vice presidente Famiglie Arcobaleno e Rita De Santis, presidente AGEDO**. Nella lettera si sottolinea la gravità dell'inerzia del Parlamento di fronte al monito del più alto consesso giuridico italiano dietro al quale vi sono le importanti considerazioni giuridiche avanzate dagli avvocati difensori delle coppie durante l’udienza, ma anche migliaia di cittadini discriminati in un aspetto fondamentale della loro esistenza che attendono da troppo tempo giustizia e pari dignità di fronte allo Stato. I firmatari ribadiscono che giustizia sarà fatta solo consentendo a tutti di accedere al matrimonio civile a prescindere dal sesso della persona con cui decidono d'intraprendere un percorso di vita comune.

**Ai Gruppi parlamentari si chiede quindi di rispettare quanto scritto dalla Corte Costituzionale, mentre al Governo si chiede di ritirare la Circolare Amato n.55 del 18 ottobre 2007** (Protocollo n. 15100/397/0009861), con oggetto «Matrimoni contratti all’estero tra persone dello stesso sesso. Estratti plurilingue di atti dello stato civile» per la quale i Comuni non possono trascrivere i matrimoni delle coppie omosessuali sposate all’estero per ragioni di ‘ordine pubblico’, materia questa non applicabile al diritto di famiglia, come già ribadito in più occasioni dalla Corte di Cassazione.

**[COMPRA IL LIBRO >](e-uscito-il-libro-dal-cuore-delle-coppie-al-cuore-del-diritto-ludienza-138/2010-alla-corte-costituzionale-per-il-diritto-al-matrimonio-tra-persone-dello-stesso-sesso)**