---
title: 'POLITICI DI DESTRA NON SANNO DI CHE PARLANO. DIDORE'' PORTA-SFIGA'
date: Tue, 12 May 2009 12:15:29 +0000
draft: false
tags: [Comunicati stampa]
---

UNIONI CIVILI: L'ATTEGGIAMENTO PIETISTICO DI ALCUNI ESPONENTI DEL CENTRO DESTRA E' PREOCCUPANTE. CHI CI GOVERNA ABBIA MAGGIOR RISPETTO PER LE PERSONE. IL PRESIDENTE FINI VOLI PIU' ALTO DELLA PDL PORTA-SFIGA DIDORE’.  
  
Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti, candidato per la Lista Bonino/Pannella nella Circoscrizione Centro:

“L’atteggiamento pietistico e caritatevole che caratterizza alcuni esponenti del centro-destra riguardo le varie proposte sulle unioni civili dovrebbe far riflettere il Presidente Fini che si è detto d’accordo nel sostenerle.  C’è chi dice, a vanvera, che una legge per le unioni civili non è oggi necessaria (Mantovano) perché “i rapporti tra questo genere di coppie, omosessuali o eterosessuali che siano, sono tutelati attraverso disposizioni di volontà e accordi privati”, oppure (Ronchi) che “non si possono negare i diritti fondamentali della persona a chi non si sposa; non è giusto, per esempio, che un convivente, omosessuale o eterosessuale, non possa andare a trovare la propria compagna in rianimazione o in carcere”; e poi ancora (Quagliariello): “però queste cose sono già state risolte da alcune sentenze della magistratura. Una legge non serve! Anzi, sarebbe meglio che gli esponenti di governo se ne tenessero ben lontano” e poi ancora che “sarebbe strano che il centrodestra intervenisse sulle coppie di fatto ancora prima di aver garantito quegli aiuti promessi alle famiglie in difficoltà economica”.  
Insomma, ci par di capire che il problema di quasi un milione di coppie conviventi, omosessuali o eterosessuali, sia quello del carcere, del reparto di rianimazione di un ospedale o che tutti i problemi sociali legati ad una convivenza “sono già stati risolti dalla magistratura”.   
Ma questi dove vivono?  
Ma possibile che a nessuno di loro venga in mente che forse basterebbe convocare, audire, sentire, ascoltare una di queste coppie o una delle Associazioni che si battono per i diritti civili, prima di parlare così a vanvera? Ci auguriamo che il Presidente Fini vada oltre, voli alto. Non si fermi alla pdl porta-sfiga denominata Didorè  (si parla anche lì solo di malattia e morte) e che studi una soluzione così come hanno fatto i suoi colleghi di destra di Spagna, Francia, Svezia, Danimarca, Germania e Gran Bretagna. Solo così possiamo sperare di raggiungere l’Europa”.