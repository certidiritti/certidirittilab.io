---
title: 'BOLOGNA  13-6 CONVEGNO "TUTELA  PERSONA E ORIENTAMENTO SESSUALE."'
date: Tue, 10 Jun 2008 17:40:50 +0000
draft: false
tags: [Comunicati stampa]
---

Convegno “Tutela della persona e orientamento sessuale. Strumenti, tecniche e strategie contro la discriminazione” a Bologna, 13 giugno 2008 , ore 14:00.  
Facoltà di Giurisprudenza – Via Zamboni 22 Patrocinato dalla Regione Emilia-Romagna e dalla Facoltà di Giurisprudenza dell’Università di Bologna e attributivo di 5 crediti formativi per avvocati e patrocinatori, il convegno organizzato dal Centro Europeo Studi sulla Discriminazione e Avvocatura per il diritti LGBT – Rete Lenford, ospiterà le riflessioni di costituzionalisti (Andrea Morrone e Diletta Tega), esperti di diritti umani (Robert Wintemute), giuslavoristi (Laura Calafà), magistrati (Maria Acierno), e organismi di parità (significativa la presenza dell’Ufficio nazionale antidiscriminazioni razziali e dell’Ombudsman svedese contro la discriminazione basata sull’orientamento sessuale).  
  
Esso costituirà l’occasione per continuare a riflettere sulle coordinate teoriche della discriminazione basata sull’orientamento sessuale, le sue manifestazioni concrete, le strategie di contrasto sulla base della normativa esistente. Costituirà inoltre momento di scambio di buone pratiche e di aggiornamento sulle novità giurisprudenziali e legislative, in particolare per quanto concerne le recenti proposte di legge contro l’istigazione a discriminare e la violenza omofoba.  
  
Programma dell’iniziativa, informazioni e iscrizioni: www.cesd.eu  
Registrazione: gratuita per il pubblico, 40€ per gli avvocati che intendano ottenere i crediti formativi.