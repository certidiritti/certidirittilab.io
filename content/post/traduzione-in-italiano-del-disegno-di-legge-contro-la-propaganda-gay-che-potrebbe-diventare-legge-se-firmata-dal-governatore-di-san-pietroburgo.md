---
title: 'Il testo in italiano del disegno di legge contro "la propaganda gay" che potrebbe diventare legge se firmata dal governatore di San Pietroburgo'
date: Thu, 08 Mar 2012 11:20:17 +0000
draft: false
tags: [Russia]
---

Ecco cosa prevede il disegno di legge che limita il diritto alla libertà di espressione delle persone lgbtqi, che è stato approvato in terza lettura lo scorso 29 febbraio e che diventerà legge se sarà firmato dal governatore di San Pietroburgo.

**Закон Санкт-Петербурга от 12 мая 2010 года N 273-70 (dal 12 maggio 2010, N 273-70)**  
Modifiche sulla legge in merito ai reati amministrativi a San Pietroburgo.

Articolo 1:  
I seguenti cambiamenti sono a supplemento degli articoli 7.1 e 7.2 di cui si riporta il testo:

7.1) Il pubblico intervento finalizzato a promuovere la propaganda LGBT ai giovani e ai minori è punibile con una sanzione amministrativa per un importo di rubli cinquemila per i cittadini, di rubli cinquantamila per i funzionari-pubblici ufficiali, e per un importo variabile da duecentocinquantamila a cinquecentomila rubli per i soggetti giuridici e le attività commerciali.  
Avviso. Con azioni pubbliche volte a promuovere la propaganda LGBT ai minori si intende attività intenzionale e la diffusione incontrollata di informazioni al pubblico con modalità in grado di nuocere alla salute e allo sviluppo morale e spirituale dei minori, dando loro una visione distorta circa l’equivalenza  
sociale dei rapporti tradizionali e non tradizionali.

7.2) Il pubblico intervento finalizzato a promuovere la pedofilia è punibile con una sanzione amministrativa per un importo di rubli cinquemila per i cittadini, di rubli cinquantamila per i funzionari e pubblici ufficiali, e per un importo variabile da cinquecentomila a un milione di rubli per i soggetti giuridici.  
Avviso. Con azioni pubbliche volte a promuovere la pedofilia si intende attività intenzionale e la diffusione incontrollata di informazioni al pubblico, nonché tutte le attività svolte a creare e diffondere le nozioni distorte circa il rapporto adulto-minore.

Articolo 2:  
La qui presente legge entra in vigore trascorsi dieci giorni dalla data della sua pubblicazione ufficiale.  
Il governatore di San Pietroburgo  
Georgy Poltavchenko

\_\_\_\_\_\_\_\_\_\_  
NOTA ESPLICATIVA  
Nota esplicativa al disegno di legge "sulle modifiche alla legge di San Pietroburgo" sui reati amministrativi a San Pietroburgo.  
Il disegno di legge mira a combattere la propaganda LGBT ai minori così come la pedofilia. Il presente disegno di legge è stato progettato sulla base del ricorso collettivo firmato da diverse centinaia di abitanti della città che protestano contro queste tipologie di propaganda.  
Secondo gli esperti la sempre crescente propaganda di perversioni sessuali ha avuto un effetto demoralizzante specialmente sui giovani ed è uno dei fattori che ostacolano il tasso di crescita delle nascite.  
In presenza del Presidente della Federazione Russa sui diritti dell’infanzia è stato fatto presente che la propaganda della pedofilia è aumentata e che tale propaganda è socialmente pericolosa e dovrebbe essere vietata.

Ai sensi del paragrafo 2 dell'articolo 5 della legge federale del 24 luglio 1998 N 124-F3 “Garanzie fondamentali dei diritti dell’infanzia nella Federazione Russa” uno dei poteri delle autorità pubbliche della Federazione Russa per l'attuazione delle garanzie dei diritti del bambino nella Federazione Russa è l'implementazione delle politiche pubbliche per i bambini, al fine di proteggere i bambini dai fattori che influenzano negativamente il loro fisico, intellettuale, mentale, spirituale e morale.  
In conformità al disegno di legge presentato si intende introdurre la responsabilità amministrativa per le azioni pubbliche volte alla promozione di pratiche LGBT tra i minori, nonché azioni pubbliche volte alla diffusione della pedofilia, pena una sanzione amministrativa per un importo da mille a tremila rubli per i cittadini, da tremila a cinquemila per i pubblici ufficiali e da diecimila a cinquantamila per i soggetti giuridici e attività commerciali.  
  
Capo della fazione “Russia Unita”.  
V.S. Makarov.

**iscriviti alla newsletter >[  
](newsletter/newsletter)[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)  
**