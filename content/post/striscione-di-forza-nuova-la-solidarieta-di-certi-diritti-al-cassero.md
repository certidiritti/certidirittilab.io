---
title: 'Striscione di Forza Nuova, la solidarietà di Certi Diritti al Cassero'
date: Sat, 27 Oct 2012 18:44:01 +0000
draft: false
tags: [Movimento LGBTI]
---

Associazione radicale Certi Diritti

26 ottobre 2012

L'Associazione Radicale Certi Diritti esprime tutta la sua solidarietà al Cassero Arcigay Bologna e al festival Gender Bender per il grave attacco omofobo subito. Frasi come quelle appese fuori dal Cassero: "le perversioni vanno curate", sono di una violenza inaudita e sono ancora più gravi se si pensa alle molte persone gay e lesbiche che sono davvero costrette a sottoporsi a terapie riparative tanto inutili quanto crudeli. D'altronde la violenza è un'arma non estranea alla cultura politica di Forza Nuova che ha appeso quel cartello e ha dato fuoco a due motorini davanti al cinema Lumière dove si svolgerà il festival Gender Bender.

Quanto ancora dovremo aspettare prima che lo Stato italiano agisca in maniera sistematica per prevenire questi atti? Occorre un'azione capillare di lotta all'omofobia, non solo in occasione del 17 maggio, ma tutti i giorni dell'anno con corsi di formazione per tutti i funzionari pubblici, con programmi contro il bullismo da adottare nelle scuole, con campagne di sensibilizzazione e riconoscendo pari dignità alle persone LGBTI difronte alla legge.