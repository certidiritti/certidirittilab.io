---
title: 'ALTRO RINVIO CORTE COSTITUZIONALE: COPPIE GAY CHIEDONO MATRIMONIO'
date: Wed, 05 Aug 2009 07:45:36 +0000
draft: false
tags: [Comunicati stampa]
---

MATRIMONIO GAY:  DUE COPPIE GAY DI TRENTO FANNO RICORSO IN TRIBUNALE DOPO IL NO DEL COMUNE, LA CORTE D’APPELLO SOLLEVA LA QUESTIONE ALLA CORTE COSTITUZIONALE. 

LA CAMPAGNA DI AFFERMAZIONE CIVILE DELL’ASSOCIAZIONE RADICALE CERTI DIRITTI E RETE LENFORD CONTINUA, PER I DIRITTI E L’UGUAGLIANZA DELLE COPPIE GAY.

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

Roma, 4 agosto 2009

“Due coppie gay di Trento, una composta da due donne e l’altra da due uomini, che avevano aderito alla campagna di Affermazione Civile promossa dall’Associazione Radicale Certi Diritti e da Avvocatura lgbt Rete Lenford, si erano viste negare dal Comune di Trento le pubblicazioni matrimoniali e per questo avevano fatto ricorso al Tribunale. Il Giudice di primo grado aveva dato alle due coppie un parere negativo e per questo l’avvocato Alexander Schuster aveva presentato ricorso, il 9 luglio scorso, davanti alla Corte d’Appello di Trento. La memoria difensiva era incentrata sul fatto che il matrimonio civile deve essere un diritto garantito a tutti i cittadini, indipendentemente dal loro orientamento sessuale.  
   
Dopo il Tribunale di Venezia anche quello di Trento considera fondate le ragioni delle coppie gay che chiedono di accedere all’istituto del matrimonio e per questo ha deciso il rinvio alla Corte Costiuzionale. Consideriamo questo passo una grande vittoria per tutto il movimento lgbt italiano.  La campagna di Affermazione Civile continua. In Italia sono quasi 30 le coppie gay che hanno aderito a questa battaglia di civiltà che  persegue le via legali vista la totale indifferenza e paralisi di quasi tutta la classe politica sul tema delle unioni civili, del matrimonio gay, dei diritti civili e umani delle persone”.  
   
   
Di seguito alcuni estratti dell’ordinanza dei giudici di Trento:  
   
Il Collegio dei giudici della Corte d’Appello di Trento, il 2 agosto scorso, hanno rimesso alla Corte Costituzionale la decisione in quanto “si tratta di questione rilevante e non manifestamente infondata. Non vi è dubbio infatti – continua il documento – che rispetto all’epoca in cui sono state incardinate le norme disciplinanti il matrimonio si è verificata un’inarrestabile trasformazione della società e dei costumi che ha portato al superamento del monopolio del modello di famiglia tradizionale ed al contestuale sorgere di forme diverse di convivenza che chiedono (talora a gran voce) di essere tutelate e disciplinate”, Nell’ordinanza si legge tra l’altro: “…quanto sopra osservato non può essere superato da un’interpretazione secondo cui il matrimonio deve e può essere consentito solo a coppie eterosessuali a ragione della sua funzione sociale, principio secondo taluni ricavabile dall’art. 29 Cost. (norma che riconosce i diritti della famiglia come società naturale fondata sul matrimonio). Detto principio infatti si limita a riconoscere alla famiglia un suo ruolo naturale, nel senso che da un lato lo Stato non può prescindere da tale realtà sociale a cui tende per natura la stragrande maggioranza degli individui e, dall’altro, afferma che la famiglia è fondata sul matrimonio; ma certo esso non giunge ad escludere la tutela della famiglia di fatto (che prescinde dal matrimonio) o ad affermare la funzione della famiglia come granaio dello Stato”.  
   
La sentenza ribadisce i sensi dei primi articoli della Costituzione, della Dichiarazione di Nizza e della Convenzione europea dei Diritti dell’Uomo che sanciscono la parità di accesso ai medesimi istituti giuridici per tutti i cittadini, al di là delle esasperazioni moralistiche che caratterizzano una politica italiana di sovente poco laica e poco proiettata al bonum comune.