---
title: 'ILGA fa il punto della situazione in Europa'
date: Thu, 30 May 2013 07:24:49 +0000
draft: false
tags: [Europa]
---

Ecco il nuovo rapporto annuale di ILGA per i diritti della popolazione LGBTI in Europa. 

Come ogni anno ILGA tasta il polso delle varie nazioni per controllare i progressi in materia di diritti tracciando una mappa europea che tiene in considerazione tutti i casi di violenza, odio e discriminazione contro la popolazione LGBTI, sottolineando sviluppi e tendenze nelle maggiori nazioni europee.

Qui sotto potete trovare tutti i link ai documenti:

**[Mappa Rainbow europea](documenti/download/doc_download/70-sidearainboweuropemap-2013may)  **

[**Mappa Rainbow: indici di valutazione per nazione**](documenti/download/doc_download/71-sideb-rainboweuropeindexmay2013)

[**Rapporto ILGA sull'Italia**](documenti/download/doc_download/68-ilga-annual-rewiev-2013-italy)

**[Infografica sulle statistiche italiane per i diritti LGBTI](documenti/download/doc_download/72-italyen)**

[**Rapporto ILGA sull'Europa**](documenti/download/doc_download/69-ilga-annual-rewiev-2013-europe)