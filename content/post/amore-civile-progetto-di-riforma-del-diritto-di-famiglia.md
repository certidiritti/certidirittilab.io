---
title: 'AMORE CIVILE - PROGETTO DI RIFORMA DEL DIRITTO DI FAMIGLIA'
date: Fri, 02 Oct 2009 08:03:32 +0000
draft: false
tags: [Comunicati stampa]
---

**AMORE CIVILE - PROGETTO DI RIFORMA DEL DIRITTO DI FAMIGLIA  
**

**Roma, 8 ottobre 2009 ore 9-14****  
Camera dei Deputati - Sala del Refettorio, Palazzo San Macuto  
Via del Seminario, 76**

_Segreteria organizzativa: Giacomo Cellottini Tel. 06 67609021 E-mail [diritto.famiglia@gmail.com](mailto:diritto.famiglia@gmail.com)_

Giovedì 8 ottobre, presso la Camera dei Deputati (Sala del Refettorio, Palazzo San Macuto, Via del Seminario, 76 - Roma) dalle ore ore 9 alle ore 14 sarà presentato il volume '**Amore civile, progetto di riforma del Diritto di Famiglia**', edito dalla Mimesis edizioni, nell'ambito della Collana Quaderni Loris Fortuna, promossa dalla Fondazione omonima.

Il libro raccoglie il lavoro condotto nell'ultimo anno da diversi giuristi, sociologi, psicologi, membri di associazioni legate alle problematiche famigliari. Risultato di tale lavoro è una proposta di Riforma, che per la prima volta dopo il 1975 affronta le questioni più urgenti in materia famigliare: dal riconoscimento delle unioni di fatto, alla procreazione medicalmente assistita, dal matrimonio tra persone dello stesso sesso, al divorzio breve.L'auspicio è che la proposta di Riforma del Diritto di Famiglia divenga un Disegno di Legge per avviare un dibattito approfondito nella Aule parlamentari, in un'ottica non ideologica e con una forte attenzione ai problemi reali delle persone.

I curatori del volume unitamente a tutti i componenti della Commissione permanente per la Riforma del Diritto di Famiglia sono lieti di invitare la S. V. alla presentazione del volume. Il Programma e i dettagli sono qui sotto.

  
**Per qualsiasi maggiore informazione, contattare:**

**Coordinatore Commissione per la Riforma del Diritto di Famiglia:** **Sergio Rovasio** **Segreteria organizzativa:** **Giacomo Cellottini Tel. 06 67609021  
E-mail** [**diritto.famiglia@gmail.com**](mailto:diritto.famiglia@gmail.com)

On. Rita Bernardini

Sen. Donatella Poretti

Avv. Francesco Bilotta

**AMORE CIVILE - PROGETTO DI RIFORMA DEL DIRITTO DI FAMIGLIA**

  
Roma, 8 ottobre 2009

Camera dei Deputati - Sala del Refettorio, Palazzo San Macuto ore 9 – 14

Via del Seminario, 76 - Roma

Programma:

Presiede Sen. **Emma BONINO,** Vice Presidente del Senato

Modera **Concita DE GREGORIO**, direttrice de L’UNITA’

Ore 9.00 Sen. Emma BONINO - Saluto ai partecipanti ed apertura del Convegno

Ore 9.15 Bruno DE FILIPPIS* - Presentazione del Progetto

**LA REGOLAMENTAZIONE DELLE COPPIE DI FATTO**

Ore 9.30 On. Fabio EVANGELISTI (da confermare)

Ore 10.00 Guido ALLEGREZZA*

**LA SEPARAZIONE E LA MEDIAZIONE FAMILIARE**

Ore 10.05 Sen. Giuseppe SARO (da confermare)

Ore 10.20 Maurizio QUILICI* e Giuliano GRAMEGNA*

**IL MATRIMONIO TRA PERSONE DELLO STESSO SESSO**

Ore 10.25 On. Anna Paola CONCIA

Ore 10.35 Sen. Donatella PORETTI  
  
Ore 10.45 Francesco Bilotta*

  
**LE NORME SULLA PROCREAZIONE**

Ore 10.50 Sen. Antonio PARAVIA – Norme sulla procreazione assistita

Ore 11.00 On. Benedetto DELLA VEDOVA – Norme sull’interruzione della gravidanza

Ore 11.10 Filomena GALLO*

**LA FILIAZIONE LEGITTIMA E NATURALE**

Ore 11.15 On. Jole SANTELLI

Ore 11.30 Anna Laura ZANATTA e Maria Teresa PAOLI*  

**IL DIVORZIO BREVE**

Ore 11.40 On. Vittoria FRANCO

Ore 11.50 On. Rita BERNARDINI

Ore 12.00 Diego SABATINELLI* e Alessandro GERARDI*

**LA VIOLENZA SULLE DONNE IN FAMIGLIA**

Ore 12.05 On. Fiamma NIRENSTEIN (da confermare)

Ore 12.15 Enrichetta BUCHLI*

**COMMENTO SUL PROGETTO DI RIFORMA DEL DIRITTO DI FAMIGLIA**

Ore 12.20 Marco CAPPATO, Segretario Associazione Luca Coscioni

Ore 12.35 Giulia RODANO, Assessore alla Cultura della Regione Lazio

Ore 12.45 Sen. Maria Elisabetta ALBERTI CASELLATI, Sottosegretario alla Giustizia (da confermare)

Ore 13.00 Conclusioni di Bruno DE FILIPPIS

Ore 13.15 Dibattito con Marco PANNELLA

Ore 14.00 Chiusura dei lavori

  
\* componente della Commissione di Studio per la Riforma del Diritto di Famiglia

**Il volume, disponibile al convegno, "Progetto di riforma del diritto di famiglia. Amore civile: dal diritto della tradizione al diritto della ragione" (Collana Quaderni Loris Fortuna, Mimesis edizioni, 2009)**

Il Convegno è anche Commissione del [VII congresso online dell’Associazione Luca Coscioni per la libertà di ricerca scientifica](http://www.lucacoscioni.it/).

* * *

**Confermare entro e non oltre il 7 ottobre 2009 la propria partecipazione al seguente indirizzo e-mail:** [diritto.famiglia@gmail.com](mailto:diritto.famiglia@gmail.com)

Per accedere alla Sala del Refettorio occorre avere un documento d’identità e, per gli uomini, la giacca.

**Radicali Italiani - Associazione radicale Certi diritti - Associazione Luca Coscioni - Lega Italiana per il divorzio breve**

  
Coordinatore Commissione per la Riforma del Diritto di Famiglia: Sergio Rovasio  
Segreteria organizzativa: Giacomo Cellottini Tel. 06 67609021  
E-mail [diritto.famiglia@gmail.com](mailto:diritto.famiglia@gmail.com)