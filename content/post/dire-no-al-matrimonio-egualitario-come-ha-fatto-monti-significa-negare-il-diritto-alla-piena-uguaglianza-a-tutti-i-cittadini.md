---
title: 'Dire no al matrimonio egualitario, come ha fatto Monti, significa negare il diritto alla piena uguaglianza a tutti i cittadini'
date: Thu, 17 Jan 2013 13:03:00 +0000
draft: false
tags: [Matrimonio egualitario]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 17 gennaio 2013

Apprendiamo che il prof. Monti si è dichiarato contrario al matrimonio egualitario su SkyNews24 aggiungendo che per lui "la famiglia è formata da un uomo e da una donna, ed è giusto che i figli crescano con un padre e una madre". Spiace vedere come un uomo eletto alla Presidenza del Consiglio con un'agenda europeista, una volta salito (sic!) in politica, si riveli solo un politico italiano qualunque.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, afferma: "Dire no al matrimonio egualitario vuol dire negare il diritto alla piena eguaglianza a tutti i cittadini italiani, violando un principio costituzionale che il Professore ben conosce. Fintanto che un cittadino non potrà liberamente scegliere chi sposare, "senza distinzione di sesso, di razza, di lingua, di religione, di opinioni politiche, di condizioni personali e sociali", l'articolo 3 della Costituzione non potrà dirsi pienamente rispettato".

Sophie Lannefranque, drammaturga francese, ha recentemente scritto una lettera prendendo parte al dibattito francese sul mariage pour tous. Facciamo nostre alcune delle sue parole e le rivolgiamo a tutti coloro che si oppongono al matrimonio egualitario:" Sonia non deve avere il diritto di sposare Charlotte? Stéphane non sarà mai il marito di Bernard? Vi unirete in corteo per le starde contro il loro amore? Per il vostro? Perché il vostro, e soltanto il vostro, è legale? È rispettabile? Vergogna. Perché nulla vi dà il diritto di vietare qualunque cosa non vi faccia del male, leggete la Costituzione!".