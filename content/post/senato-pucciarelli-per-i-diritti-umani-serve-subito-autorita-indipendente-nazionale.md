---
title: 'Senato. Pucciarelli? Per i diritti umani serve subito autorità indipendente nazionale'
date: Wed, 14 Nov 2018 16:39:21 +0000
draft: false
tags: [Politica]
---

I diritti umani storicamente acquisiti non possono cambiare colore e forma a seconda dei parlamenti e delle compagini governative che si susseguono.
----------------------------------------------------------------------------------------------------------------------------------------------------

![Stefania Pucciarelli, la nuova presidente della commissione diritti umani del Senato](http://www.certidiritti.org/wp-content/uploads/2018/11/fd7f86a96d78d9f8ae22d60e2caabca7_XL-300x231.jpg)"L'Italia continua ad essere uno degli ultimi paesi europei a non disporre di un'**autorità indipendente per la tutela e la promozione dei diritti umani**. Eppure saremmo tenuti a farlo, come ci chiede la risoluzione 48/134 dell’Assemblea generale dell’Onu, sottoscritta dal nostro Paese nel lontano 1993. **Le commissioni parlamentari e le agenzie governative non sono all'altezza** delle sfide cui siamo chiamati a rispondere e i diritti umani storicamente acquisiti non possono cambiare colore e forma a seconda dei parlamenti e delle compagini governative che si susseguono. Le polemiche attorno all'**elezione come presidente della commissione diritti umani di Palazzo Madama della senatrice Pucciarelli** sono sintomo e allarme di una grave inadempienza italiana che dura ormai da 26 anni" Così **Leonardo Monaco**, segretario dell'Associazione Radicale Certi Diritti. "Istituire un’agenzia nazionale autonoma e indipendente per la tutela dei diritti umani che favorisca la prevenzione delle discriminazioni e la difesa delle libertà fondamentali: è questo l'appello urgente che rivolgiamo prima di tutto alla nuova commissione diritti umani e a **tutti i parlamentari della XVIII Legislatura**", conclude Monaco.