---
title: 'TRIESTE MARTEDI 27-5 "MATRIMONIO GAY ALL''ITALIANA"'
date: Fri, 23 May 2008 14:00:41 +0000
draft: false
tags: [certi diritti, Comunicati stampa, Conferenza Stampa Certi Diritti trieste, Presentazione Trieste]
---

Trieste, Martedì 27 maggio 2008, presso il caffè Tommaseo alle 11.15 l'Associazione Radicale Certi Diritti Vi invita alla conferenza stampa.  
Sarà l'occasione per parlare del "Matrimonio gay all'italiana" oltre che riflettere sul clima omofobico del nostro Paese. Contestualmente verrano presentate alla stampa l'iniziativa nazionale di Affermazione Civile e le proposte di legge depositate alla Camera e al Senato per le unioni civili, il matrimonio gay e per il contrasto all'omofobia.  
  
Saranno presenti  
L'astrofisica prof.ssa Margherita Hack  
Fabio Omero capogruppo Pd in consiglio comunale (Ts) Il prof. Francesco Bilotta Avvocatura Glbt - Rete Lenford Modera Clara Comelli presidente Associazione Radicale Certi Diritti