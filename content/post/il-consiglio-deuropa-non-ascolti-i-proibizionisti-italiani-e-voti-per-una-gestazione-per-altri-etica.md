---
title: 'IL CONSIGLIO D’EUROPA NON ASCOLTI I PROIBIZIONISTI ITALIANI E VOTI PER UNA GESTAZIONE PER ALTRI ETICA.'
date: Tue, 11 Oct 2016 15:21:10 +0000
draft: false
tags: [Gestazione per altri]
---

[![utero-in-affitto](http://www.certidiritti.org/wp-content/uploads/2016/09/utero-in-affitto-300x193.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/09/utero-in-affitto.jpg)Dopo il voto in settembre del rapporto sulla gestazione per altri alla Commissione Affari Sociali dell’assemblea parlamentare del Consiglio d’Europa, oggi l’Assemblea Parlamentare del Consiglio d’Europa si esprimerà in plenaria.

Le associazioni firmatarie di questo comunicato hanno sottoscritto un [documento](http://www.certidiritti.org/wp-content/uploads/2016/09/323161648-MHB-Ethical-Surrogacy-Italian-Statement-of-Principles.pdf) di sintesi dei parametri etici per il ricorso alla gestazione per altri poiché ritengono che la soluzione più idonea a rispettare i diritti umani delle parti coinvolte sia la regolamentazione del fenomeno, non la sua proibizione come proposto da un gruppo di parlamentari italiani.

I parlamentari italiani che  ideologicamente si stanno battendo per la proibizione della gestazione per altri, in realtà finiscono per adoperarsi affinché si perpetui il cosiddetto turismo procreativo, anche in Paesi che non garantiscono le tutele e i diritti necessari delle gestanti e dei nascituri.

Auspichiamo che l’Assemblea Parlamentare del Consiglio d’Europa ribalti la decisione della Commissione Affari Sociali e inviti gli Stati membri ad adottare l’unica soluzione possibile: una salda normativa che regolamenti la gestazione per altri garantendo l’autodeterminazione della donna e delle scelte riproduttive dei cittadini europei, così come i diritti dei bambini nati dalla gestazione per altri, che, ricordiamo, per la Corte europea dei Diritti dell’Uomo rischierebbero di essere violati proprio dal divieto della gestazione per altri (sentenze del 26 giugno 2014 contro la Francia).

_Roma, 11 ottobre 2016_