---
title: '27 GIUGNO 2009: GENOVA PRIDE NAZIONALE'
date: Thu, 02 Apr 2009 12:17:43 +0000
draft: false
tags: [Senza categoria]
---

27 GIUGNO 2009: GENOVA PRIDE NAZIONALE   

==========================================

Un pride “per” prima ancora che “contro”   

--------------------------------------------

Il Pride del 2009 è per noi l’occasione di sottolineare la piena cittadinanza delle persone lgbtqi (lesbiche, gay, bisessuali, transgender, queer, intersessuali) italiane. La nostra presenza sociale è oggi considerata dalla gran parte della società italiana un fatto positivo e arricchente dei differenti vissuti e punti di vista.

Per questo riteniamo che nel pieno della crisi economica e sociale del Paese sia nostro dovere sottolineare come le persone lgbtqi condividano la preoccupazione per la perdita del posto di lavoro, per la restrizione della disponibilità di reddito, per la precarizzazione esasperata della propria esistenza. Ribadiamo con forza che l’Italia cambierà solamente quando diritti civili e diritti sociali saranno collegati insieme in un progetto di mutamento delle condizioni concrete delle persone. La nostra piattaforma rivendicativa, è rimasta sostanzialmente immutata dal Pride nazionale di Roma del 2007 passando per il Pride nazionale di Bologna del 2008. Questo dimostra  l’arretratezza e l’immobilismo della politica italiana.

Nel 2009 vogliamo aggiungere alle nostre specifiche e legittime aspirazioni un messaggio politico di aggregazione di tutte le forze sociali e culturali italiane affinché dalla crisi economica, dall’assenza di diritti civili, dalla incertezza del lavoro sia possibile uscire costruendo una nuova idea di società plurale e solidale.

Per tutte queste ragioni, il 27 giugno, vigilia del quarantennale del movimento mondiale, il Genova Pride 2009, sarà la manifestazione nazionale dei diritti, delle libertà, della solidarietà, del lavoro e della sua sicurezza, un’iniziativa aperta, di dialogo tra differenti culture, orientamenti sessuali, identità di genere, provenienze etniche, politiche e religiose. In particolare siamo impegnati per la tutela e aiuto nei confronti delle persone lgbtqi appartenenti a etnie e religioni provenienti da paesi ove sono perseguitate.

E’ per noi essenziale ribadire la difesa della Costituzione e della laicità dello Stato, insieme alla  difesa e promozione della vita e dignità delle persone e delle famiglie lgbtqi in particolare contrastando l’ondata di omofobia, lesbofobia, transfobia che si è abbattuta negli ultimi anni nel nostro Paese.

Un laboratorio per sperimentare nuovi percorsi anche dentro il movimento lgbtqi, che deve saper interpretare l’impetuoso cambiamento economico e sociale determinato dalla crisi.

Tutte le info logisitche: [www.genovapride.it](http://www.genovapride.it)