---
title: 'Radical Faeries: gli elfi gay dei boschi'
date: Thu, 21 Oct 2010 11:43:46 +0000
draft: false
tags: [Comunicati stampa]
---

![](http://www.gay.tv/gaytv/GayTV_tbMedia/b770c0abd7cc3392548fe753408f2de1c28e9c38154d387a)Il sesso come strumento di conoscenza e la nudità come filosofia: sono i Radical Faeries, comunità di elfi e fate lgbt. E stanno arrivando in Europa.

di [www.gay.tv](http://www.gay.tv)

**Abitano in remote zone rurali**, lontano dalla civiltà urbana di cui rifiutano il progresso. **Come gli hippy, cercano il contatto con la natura** e percepiscono come sacra la Madre Terra. Usano **il sesso e la corporeità per intrecciare contatti** e relazioni, vivono **in comunità** collettive in cui "il tuo è mio", **credono nella nudità**, nel naturismo e nella magia. **Sono le fate, i maghi, gli elfi e le ninfe dei boschi.**  
**_E sono tutti gay._**  
  
Il loro nome ufficiale e **Radical Faeries**: sono comunità lgbt che abbracciano la filosofia della natura, riconoscendosi nella **spiritualità pagana e nelle derivazioni sociali hippy**.  
"_Si vive in comunità, che possono essere piccoli gruppi di amici che si ritrovano a casa di qualcuno o intere comunità che affittano o comprano grandi terreni per trasformarli in aree sacre permanenti_" racconta **a L'Espresso un Faerie originario di Roma**, di 47 anni, che si fa chiamare Isildur.

![](http://www.gay.tv/gaytv/GayTV_tbMedia/b770c0abd7cc3392548fe753408f2de1f71ec83e767aaed6)

Nelle comunità dei Radical Faeries **il travestimento e il corpo ricoprono ruoli centrali**: la maggior parte dei membri di questi gruppi adotta un nome "magico" e fo**rme di trucco e performance estetica**. **Fate, maghi, creature mitologiche: ognuno "diventa" un essere incantato**. A questi gruppi aderiscono sempre più spesso persone che nel contesto sociale quotidiano si trovano ai margini, e che invece qui possono **vivere l'identità queer** in modo unico. "_**Ci conosciamo attraverso il sesso, camminiamo nudi nel bosco o ci travestiamo**. Tutti i vestiti sono travestimenti, dallo smoking al costume sciamanico. Indossiamo spesso maschere o ci coloriamo il volto. **Per noi il trucco ha un valore sacro, l'ornamento del viso e del corpo sono forme di comunicazione con il divino che ci circonda**_".

![](http://www.gay.tv/gaytv/GayTV_tbMedia/b770c0abd7cc3392548fe753408f2de14d6f08f4b82b515d)

L'idea alla base della scelta di vita dei Radical Fearies ha natura **sia spirituale che politica**. Da un lato implica un ritorno alla natura e alla terra, dall'altra rappresenta **una chiara rivendicazione dell'identità lgbt**. "_Siamo **una rete di omosessuali, artisti, lavoratori, abitanti delle città in fuga che sentono la cultura gay, lesbica e transessuale come una comunità distinta**, separata dalla società di massa, con una nostra spiritualità, un nostro modo di essere. **Dobbiamo unirci e diventare un grande popolo**, recuperare l'equilibrio perduto della più ampia comunità umana del pianeta_" scrive Joey Cain in un manifesto della comunità.

I Radical Faeries sono **diffusi soprattutto in Nord America**, ma il fascino di questo stile di vita **sta contagiando anche l'Europa**, tanto che i primi gruppi si stanno formando in Francia e Inghilterra. **Ci sarà spazio per loro anche nei boschi italiani?**

[Gay.tv](http://www.gay.tv/articolo/3/13090/Radical-Faeries--gli-elfi-gay-dei-boschi)