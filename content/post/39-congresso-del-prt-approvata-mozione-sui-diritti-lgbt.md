---
title: '39° congresso del PRT: approvata mozione sui diritti lgbt'
date: Mon, 21 Feb 2011 22:36:30 +0000
draft: false
tags: [bonino, certi diritti, Comunicati stampa, david kato, IRAN, libia, pannella, RADICALI, rovasio, smug, Uganda]
---

Mozione particolare

Il 39° Congresso del Partito Radicale Nonviolento, transnazionale e transpartito, riunito a Chianciano nei giorni 17-20 febbraio 2011...

Considerato quanto è avvenuto e sta avvenendo in Uganda e altri paesi dell’Africa contro le persone omosessuali e transessuali, dove l’odio e il pregiudizio fomentati sistematicamente ed intenzionalmente da organizzazioni religiose fondamentaliste e politiche allo scopo di spargere paura e creare un nemico contro cui aggregare consensi e acquisire potere hanno alimentato azioni sempre più violente;

Considerato che il 27 e 28 novembre 2010 David Kato Kisule, leader del movimento per i diritti civili ugandese e militante di Non c’è pace senza giustizia, era stato invitato ai lavori del IV° Congresso dell’Associazione Radicale Certi Diritti, dove aveva raccontato del dramma che vivono nel suo paese i gay, le lesbiche e le persone transessuali, vittime di linciaggi e di persecuzioni violente di ogni genere e che si era iscritto all’Associazione Radicale Certi Diritti;

Considerato che lo scorso 26 gennaio David Kato Kisule è stato brutalmente ucciso nella sua abitazione e che, a seguito del suo omicidio, in tutto il mondo vi sono state reazioni unanimi di condanna con la richiesta alle autorità ugandesi di promuovere rispetto e sicurezza per lesbiche, gay e trans ugandesi;

Considerato che in Uganda l’omosessualità è perseguita penalmente e che da lungo tempo è in discussione un disegno di legge ancora più illiberale, che prevede pene ancora più severe, compresa la pena di morte, contro le persone omosessuali, transessuali, le associazioni e le organizzazioni che ne sostengono i diritti;

Considerato che gli attivisti ugandesi devono sostenere ingenti costi per sostenere le spese legali dei diversi processi in corso, anche da loro intentati per difendere la loro dignità, il loro onore e la loro credibilità;

Preso atto che ai lavori del 39° Congresso hanno partecipatoPepe Julian e Auf, rappresentanti dell’organizzazione SMUG (Sexual Minorities Uganda) e Francis Onyango, il legale che ha rappresentato David Kato davanti l’Alta Corte ugandese nel caso contro l’infima pubblicazione locale Rolling Stone, che aveva incitato al linciaggio degli omosessuali, pubblicando cento nomi, foto ed indirizzi; causa poi vinta da David;

Invita i suoi organi dirigenti a promuovere in ambito transnazionale,azioni di sostegno per i diritti civili e le libertà in Uganda e in tutti quei paesi dove l’omosessualità e il transessualismo sono perseguiti penalmente, e a sostenere la proposta francese di depenalizzazione universale presentata all’Onu nel dicembre 2009;

Impegna gli organi del Prntt a promuovere un sostegno politico per i parlamentari liberali e i difensori dei diritti civili in Uganda ed in altri paesi dove, al momento, è estremamente rischioso opporsi alle draconiane leggi omofobiche e transfobiche attualmente in discussione.<