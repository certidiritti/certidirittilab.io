---
title: 'Parlamento europeo approva direttiva su vittime di reati, inclusi quelli omofobici e transfobici'
date: Thu, 13 Sep 2012 12:47:44 +0000
draft: false
tags: [Europa]
---

Associazione radicale Certi Diritti: di fronte all'inerzia del Parlamento europeo iniziativa ancora più importante per la comunità lgbti italiana.

Comunicato stampa dell'Associazione Radicale Certi Diritti

Roma, 13 settembre 2012

Il Parlamento europeo ha dato oggi il suo ok alla quasi unanimità (611 favorevoli, inclusi tutti i deputati italiani presenti al voto; 9 contrari e 13 astenuti) alla proposta di direttiva che istituisce norme minime riguardanti "i diritti, l'assistenza e la protezione delle vittime di reato". La direttiva é il primo atto di diritto internazionale che fa riferimento all'"espressione di genere". Le persone vittime di un crimine a causa della loro "espressione di genere, identità di genere, orientamento sessuale" avranno diritto ad un'assistenza e protezione speciale: tali discriminazioni  dovranno essere prese in piena considerazione nel quadro della valutazione del caso individuale. In particolare, gli Stati membri dell'UE dovranno prevedere che le vittime di crimini abbiano accesso a servizi di supporto appropriati, siano interrogate da professionisti che hanno seguito un training specifico, siano supportate nel corso della procedura giudiziaria, nonché protette da ulteriori intimidazioni. In sostanza, la direttiva permetterà l'assistenza e la protezione delle persone vittime di crimini transfobici ed omofobici. Una volta approvata dal Consiglio nelle prossime settimane, la direttiva entrerà in vigore e dovrà essere applicata entro 3 anni.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti si felicita col PE per l'approvazione della direttiva, si augura che l'Italia applichi la direttiva il prima possibile e dice: "Come al solito su questi temi l'Italia va al traino dell'Europa. Di fronte all'inerzia del Parlamento italiano risulta ancora più importante per la comunità LGBTI italiana l'iniziativa del Parlamento Europeo. Gli attacchi omo e transfobici si ripetono con una frequenza quasi settimanale in Italia determinando una situazione particolarmente drammatica per le persone LGBTI che finalmente trovano un'adeguata risposta in questa direttiva, la quale stabilisce un'utile cornice legislativa su come gli Stati membri dovranno aiutare le vittime di crimini commessi sulla base dell'orientamento sessuale, dell'identità di genere e dell'espressione di genere".

[Il testo della direttiva del Parlamento europeo e del Consiglio che istituisce norme minime riguardanti i diritti, l'assistenza e la protezione delle vittime di reato >](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+REPORT+A7-2012-0244+0+DOC+XML+V0//IT)

[Il comunicato stampa del Parlamento Europeo >](http://www.europarl.europa.eu/news/it/headlines/content/20120831FCS50275/10/html/Il-Parlamento-sostiene-i-diritti-minimi-in-tutta-l%27UE-per-le-vittime-di-reati)