---
title: 'Iscriviti'
date: Tue, 11 Mar 2014 10:01:12 +0000
draft: false
---

È solo grazie al canale dell'autofinanziamento che possiamo portare avanti le nostre iniziative. Per **investire** in un anno di lotte nonviolente per la libertà e la responsabilità sessuale delle persone, puoi versare la tua quota di iscrizione iniziando a compilare il modulo sottostante scegliendo il metodo di pagamento che preferisci. Una volta inseriti tutti i tuoi dati riceverai una mail con le istruzioni per finalizzare la tua iscrizione.

#### La quota di iscrizione è **50 Euro** La quota di iscritto sostenitore è **150 Euro** La quota di iscrizione per persone senza reddito è di **25 Euro**

\[wufoo username="associazionelucacoscioni" formhash="qq2a8jl1utwb2a" autoresize="true" height="313" header="show" ssl="true"\]