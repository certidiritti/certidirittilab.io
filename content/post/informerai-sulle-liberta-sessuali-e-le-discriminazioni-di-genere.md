---
title: 'InformeRai sulle libertà sessuali e le discriminazioni di genere'
date: Tue, 25 Oct 2011 04:03:36 +0000
draft: false
tags: [Politica]
---

Reclama il tuo diritto all'informazione su libertà sessuali e discriminazione di genere!

Chiedi con noi all'autorità garante delle comunicazioni, l'Agcom, che la Rai finalmente apra un dibattito pubblico sulle libertà sessuali e le discriminazioni di genere!

**FIRMA LA DENUNCIA >**  
[](http://www.radicali.it/informerai-liberta-discriminazioni-sessuali)[http://www.radicali.it/informerai-liberta-discriminazioni-sessuali](http://www.radicali.it/informerai-liberta-discriminazioni-sessuali)

  
Informano sulla struttura della società italiana che comprende centinaia di migliaia di famiglie non tradizionali e fra queste le coppie omosessuali con e senza figli, che risultano attualmente prive di riconoscimento giuridico? O che la riforma del diritto di famiglia è una necessità avvertita da milioni di cittadini mentre in Parlamento giacciono numerose proposte di legge in tal senso?

Ancora una volta l'informazione del servizio pubblico lascia a desiderare.

Che dire poi della prostituzione, un fenomeno sociale mai discusso se non in chiave moralistica o di ordine pubblico; delle malattie sessualmente trasmesse, che sono in drammatico aumento e dell’informazione sull’Aids, per fare un esempio, che a trent’anni dall’inizio della pandemia è relegata a spazi sporadici.

E le persone transessuali e intersessuali particolarmente bersaglio di disinformazione ed ignoranza che hanno difficoltà nel far valere i propri diritti nel campo del lavoro, della scuola, della salute e nella vita sociale?

Se sei un telespettatore Rai, probabilmente non ne sai nulla! Allora denuncia la Rai per questa informazione che ti nega!

Chiedi con noi all'autorità garante delle comunicazioni, l'Agcom che la Rai finalmente apra un dibattito pubblico sulle libertà sessuali e le discriminazioni di genere!

**Leggi il testo completo (pdf) della denuncia >**  
[http://www.radicali.it/sites/default/files/denuncia-liberta-discriminazioni-sessuali.pdf](http://www.radicali.it/sites/default/files/denuncia-liberta-discriminazioni-sessuali.pdf)

**FIRMA LA DENUNCIA >**  
[http://www.radicali.it/informerai-liberta-discriminazioni-sessuali](http://www.radicali.it/informerai-liberta-discriminazioni-sessuali)