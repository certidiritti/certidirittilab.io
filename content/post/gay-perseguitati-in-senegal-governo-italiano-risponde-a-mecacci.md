---
title: 'GAY PERSEGUITATI IN SENEGAL: GOVERNO ITALIANO RISPONDE A MECACCI'
date: Thu, 22 Jan 2009 14:15:59 +0000
draft: false
tags: [Comunicati stampa]
---

OMOSESSUALI PERSEGUITATI IN SENEGAL,  GOVERNO RISPONDE A INTERROGAZIONE MATTEO MECACCI DEPUTATO RADICALE –PD: ‘CI SIAMO ATTIVATI CON UNIONE EUROPEA PER DEPLORARE SENEGAL CHE VIOLA DICHIARAZIONE UNIVERSALE DIRITTI DELL’UOMO’.

Sulla vicenda degli omosessuali arrestati e condannati in Senegal, il Governo italiano ha risposto all’interrogazione parlamentare del deputato radicale del Pd Matteo Mecacci. Nella risposta del Governo si legge tra l’altro: “La questione è stata oggetto di immediata consultazione nel quadro della cooperazione politica europea. Ne è scaturita la decisione, assunta all’unanimità, di effettuare un passo presso le Autorità di Dakar per deplorare la condanna, evidentemente pronunciata sulla base dell’orientamento sessuale delle persone in spregio ai principi della Dichiarazione Universale dei Diritti dell’Uomo, e per chiedere l’immediata liberazione degli accusati”. Nella risposta il Governo italiano si è riservato ulteriori interventi e ha ricordato il suo impegno in sede Onu per la depenalizzazione dell’omosessualità sottoscritta da 66 paesi e depositata lo scorso 18 dicembre.

Matteo Mecacci, deputato radicale del Pd ha dichiarato: “Nell’interrogazione avevamo chiesto al Governo italiano di attivarsi anche attraverso i canali diplomatici, convocando con urgenza l’ambasciatore del Senegal. Pur apprezzando l’impegno del nostro Governo in sede europea, riteniamo necessario e più incisivo un intervento bilaterale nei confronti del Governo senegalese, anche riguardo il tema della depenalizzazione dell’omosessualità in sede Onu, la cui leadership liberale negli anni passati si era dimostrata attenta al rispetto dei diritti umani, mentre invece ora si assiste a una pericolosa regressione”.