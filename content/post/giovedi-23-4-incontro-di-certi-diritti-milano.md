---
title: 'GIOVEDI'' 23-4 INCONTRO DI CERTI DIRITTI - MILANO'
date: Sun, 19 Apr 2009 17:10:36 +0000
draft: false
tags: [Comunicati stampa]
---

A Milano, il prossimo **giovedì 23** aprile, alle ore **21**, si terrà in [via Borsieri 12](http://www.tuttocitta.it/tcol/mappe/milano?cb=0&cx=9.18865&cy=45.48683&dv=Milano%20%28MI%29&ind=Via%20Pietro%20Borsieri,%2012&op=mc&ldv=Milano%20%28MI%29&lpr=MI&lre=Lombardia&lcn=Milano&ccd=51557&cre=7&lty=C&lcd=51557&z=1&isciv=1) il **primo incontro milanese di iscritti**, sostenitori e simpatizzanti dell'associazione radicale Certi Diritti.  
  
Sarà l'occasione per conoscersi, per presentare l'associazione e per parlare delle iniziative in atto (tra cui [affermazione civile](index.php?option=com_content&view=article&id=41&Itemid=72) per l'accesso al matrimonio anche alle coppie omosessuali e il [silence day](http://www.dayofsilence.org/index.cfm), la manifestazione che stiamo organizzando, insieme ad altre associazioni lgbt milanesi, per il prossimo 17 maggio, giornata mondiale contro l'omofobia) e di quelle a venire.  
  
Invitiamo tutti a partecipare.

  
  
Per informazioni: Luca Piva (349.8717146) o Francesco Poiré (348.3883413)