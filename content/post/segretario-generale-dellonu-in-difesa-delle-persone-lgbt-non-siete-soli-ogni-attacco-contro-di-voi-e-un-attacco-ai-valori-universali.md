---
title: 'Segretario Generale dell''Onu in difesa delle persone Lgbt: Non siete soli. Ogni attacco contro di voi è un attacco ai valori universali'
date: Fri, 09 Mar 2012 10:14:24 +0000
draft: false
tags: [Transnazionale]
---

Storica presa di posizione del Segretario generale dell'Onu in favore delle persone lgbt. L’Associazione radicale Certi Diritti collaborerà con il Partito Radicale, membro Ecosoc dell'Onu.

Roma, 8 marzo 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Ieri, mercoledì 7 marzo, per la prima volta il Consiglio dei Diritti Umani delle Nazioni Unite ha dedicato una sua sessione ad un dibattito sulla violenza e le discriminazioni contro le persone Lesbiche, Gay, Bisessuali e Transgender al quale è intervenuto il Segretario Generale Ban Ki-moon. Nel suo intervento il Segretario Generale, rivolto ai paesi membri, ha detto di  vedere  una serie di violenze e discriminazioni dirette contro persone sulla base del loro orientamento sessuale e, rivolto alle persone Lgbt, ha aggiunto che: “Non siete soli. La vostra lotta per porre fine alla violenza e alla discriminazione è una lotta condivisa. Ogni attacco contro di voi è un attacco ai valori universali delle Nazioni Unite e io ho giurato di difenderli e sostenerli. Oggi, sto con voi ... e mi appello a tutti i paesi e a tutte le persone a stare con voi”.

Il Consiglio dei Diritti Umani è stato istituito nel 2006 ed è un organo sussidiario dell’Assemblea Generale delle Nazioni Unite. Ha il compito di promuovere e garantire alle popolazioni la piena realizzazione di tutti i diritti stabiliti nella Carta delle Nazioni Unite e contenuti nei trattati e nelle leggi internazionali sui Diritti Umani. Il suo mandato include, inoltre, la prevenzione delle violazioni dei Diritti Umani e la promozione della cooperazione internazionale nell’ambito dei Diritti Umani.

Lo scorso febbraio,  l’Oic, l’ organismo che raggruppa i paesi islamici alle Nazioni Unite,  aveva inviato una lettera a tutti i paesi dell’Onu, dopo che era stata approvata da parte del Consiglio per i Diritti Umani una Risoluzione in difesa dei diritti Lgbti(e) e per boicottare la riunione svoltasi ieri.  
Secondo quanto scritto nella lettera dell’Oic “ I Diritti Umani non sarebbero per nulla universali, ma il loro rispetto dipenderebbe dalle "peculiarità nazionali e regionali e dai diversi contesti storici, culturali e religiosi. Siamo ancor più preoccupati dal tentativo di concentrarsi su determinate persone in base al loro comportamento sessuale anormale, mentre non ci si concentra su esempi evidenti di intolleranza e discriminazione, in diverse parti del mondo, basati su colore, razza, genere o religione, solo per citare alcune cause".

L’Associazione Radicale Certi Diritti ringrazia il Segretario Generale dell’Onu per queste sue coraggiose dichiarazioni e si rende disponibile a collaborare, insieme al Partito Radicale Nonviolento, transnazionale e transpartito, -che ha lo  Status consultivo all’Ecosoc dell’Onu- sul fronte della promozione dei diritti civili e umani delle persone Lgbti(e) che in ambito Onu si stanno affermando sempre di più.

Qui di seguito alcuni link ai documenti contro le discriminazioni del Consiglio Diritti Umani dell’Onu:

Report of the United Nations High Commissioner for Human Rights: [Discriminatory laws and practices and acts of violence against individuals based on their sexual orientation and gender identity (PDF)](http://bit.ly/x0ZFdC%20)

[Overview from the UN News Centre](http://www.un.org/apps/news/story.asp?NewsID=41477&Cr=gay&Cr1=lesbia)

[More on combatting discrimination based on sexual orientation and gender identity](http://www.ohchr.org/EN/Issues/Discrimination/Pages/LGBT.aspx)

**[ISCRIVITI ALLA NEWSLETTER >](newsletter/newsletter)**