---
title: 'Diritti gay: la rivoluzione civile di Obama sia un esempio'
date: Fri, 24 Jun 2011 08:35:05 +0000
draft: false
tags: [Americhe]
---

**Dopo un incontro con la comunità lgbt in cui ha dichiarato che "le coppie gay meritano gli stessi diritti civili di tutte le altre coppie", il 29 giugno il presidente americano riceverà il Gay Pride alla Casa Bianca. L'Italia impari dagli Usa e non dal vaticano.**

**Comunicato Stampa dell’Associazione Radicale Certi Diritti**

Roma, 24 giugno 2011

Le notizie che arrivano oggi dagli Usa sono di portata storica, così come lo furono quelle che arrivarono quando fu abrogata la segregazione razziale negli anni ’50. Il Presidente Barack Obama ha oggi partecipato ad un importante incontro della comunità Lgbt newyorkese dove ha dichiarato che “le coppie gay meritano gli stessi diritti civili di tutte le altre coppie”. Proprio mentre il Parlamento dello Stato di New York si accinge a votare in favore del matrimonio tra persone dello stesso sesso, forse già domani. Diverrebbe così il sesto Stato Usa ad approvare il matrimonio anche per  le coppie gay dopo Iowa, New Hampshire, Massachusettes, Connecticut e Vermont.

Nei giorni scorsi il Presidente Usa Barack Obama aveva detto che la sua posizione di contrarietà al matrimonio tra persone dello stesso sesso ‘sta evolvendo’. Appare quindi evidente che si fa sempre più strada l’idea che il superamento delle diseguaglianze sarà raggiunto quando anche alle coppie dello stesso sesso sarà consentito l’accesso all’istituto del matrimonio civile.  Ciò che fa ben sperare è la frase detta dal Presidente durante l’incontro di New York: "Noi continueremo a combattere fino a quando la legge considerera'  partner, coloro che sono stati insieme per decenni, e che oggi sono  tra di loro come degli estranei".

Il Presidente Obama aveva già sostenuto, nei mesi scorsi, che il Governo Federale Usa non avrebbe più sostenuto e difeso il ‘Defense of Marriage act’ del 1993, che blocca il riconoscimento federale dei matrimoni tra persone dello stesso sesso.  

Va inoltre ricordato che la prossima settimana il Presidente degli Stati Uniti riceverà per la prima volta alla Casa Bianca il Gay Pride che si svolgerà a Washington.

Oggi l’Italia ha certamente qualcosa da imparare dagli Usa.