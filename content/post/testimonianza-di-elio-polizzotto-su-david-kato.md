---
title: 'Testimonianza di Elio Polizzotto su David Kato'
date: Wed, 08 Jun 2011 05:04:35 +0000
draft: false
tags: [Politica]
---

Caro Rovasio, Cari Amici di Certi Diritti,

scrivo per ringraziarvi della pagina che avete dedicato a David sul vostro sito internet, il sito di un’associazione che appartiene ai propri membri, come i nostri amici ugandesi recentemente iscritti. Grazie per essere un loro punto di riferimento, per tutelarli, informali e onorarne il ricordo. David Kato Kisule, attivista LGBTI e difensore dei diritti umani di tutti gli ugandesi, apparteneva alla vostra associazione: come ogni iscritto, David si vedeva e si rifletteva in Certi Diritti. Grazie per non averlo mai dimenticato nonostante le lunghe e intense attività nelle quali siete impegnati.

Insieme a David stavo lavorando a un progetto in Uganda che ci ha tenuto spesso occupati e vicini. Un progetto che non era un progetto, era un sogno che entrambi volevamo realizzare: un’Uganda libera dai pregiudizi religiosi, politici e sociali; Un’ Uganda in cui tutte le persone indipendentemente dal colore della propria pelle, dalla propria etnia, dalla propria vita sentimentale, potessero sentirsi libere, sicure e felici. Quel sogno per David rappresentava un sentimento, un amore sviscerale. **Uganda, ovunque si trovi, David continuerà ad amarti.**

Grazie David per avermi dato l'opportunità di essere tuo fratello e amico, di avermi regalato momenti intensi di condivisione e di impegno politico e sociale.

Non dimenticherò mai quel giorno di maggio in cui ci incontrammo in un albergo fuori Kampala e decidesti di partecipare, su invito di Non c’e’ Pace senza Giustizia, ad una discussione sulla Corte penale internazionale e i crimini di natura sessuale. Non dimenticherò mai la nostra lunga discussione mentre ti accompagnavo a prendere un mezzo per ritornare in città. Non dimenticherò mai i dialoghi infiniti che nei giorni seguenti ci hanno fatto stare seduti per ore e ci hanno fatto conoscere. Non dimenticherò mai quei momenti in cui cominciavamo ad impostare le basi di quel sogno comune: lottare per un movimento LGBTI ugandese libero. Non dimenticherò mai i giorni passati insieme a Roma, quando accettasti l’invito a partecipare al congresso annuale di Certi Diritti dove portasti la voce di migliaia di ugandesi e africani che non potevano urlare la loro voglia di vivere, amare e amarsi, dove portasti la tua richiesta di solidarietà e di aiuto, ripetuta ancora una volta sia al Senato Italiano che al Parlamento Europeo, in un’audizione alla sottocommissione parlamentare sui diritti umani. Non dimenticherò mai i momenti più intimi a casa mia a Bruxelles dove, stanchi delle nostre giornate di lavoro, passavamo la serata seduti sul divano continuando a parlare, a conoscersi, a progettare e a sognare.

**Non dimenticherò mai te, David.**

Invece di ignorare la complessa politica di transazione culturale nel tuo paese e l’omofobia, molto radicata nella società ugandese specialmente negli ultimi decenni, ed il ruolo fondamentale delle religioni e dei suoi capi, tu, David, hai deciso di contestarla, di combatterla e di farla diventare fonte di ispirazione per il tuo attivismo. Non ti sei mai abbassato ad una mera demonizzazione dell'Uganda, non l’hai mai né visto né trattato come un paese di natura “omofobica”, non hai mai idealizzato l"Occidente" come unico posto di liberazione, di speranza e di vita per gli attivisti ugandesi. Ti sei invece preoccupato di contestare la politica governativa, il complesso mondo religioso, nazionale e neo-coloniale, denunciandone gli effetti che scaturivano.

A chi ti chiedeva perché non andavi a vivere in un altro paese, tu rispondevi sempre: “Perché? Perché dovrei farlo? Perché dovrei lasciare il paese che amo, i miei fratelli, le mie sorelle? Quale messaggio e quale esempio sarebbe per tutti gli altri miei fratelli e sorelle ugandesi, per i giovani?”

David aveva fatto la sua scelta e con estremo coraggio e orgoglio aveva deciso di combattere in prima persona l’omofobia e la sua istituzionalizzazione, per abbattere quella legge contro la sodomia, per demolire quel codice penale che sancisce il carcere per chi si “macchia” di amare una persona del proprio sesso. David voleva aiutare il suo paese e lo voleva fare dal suo stesso paese.

Una volta mi disse “I wanted to be a good human rights defender, not a dead one, but an alive one”, “voglio essere un buon difensore dei diritti umani, ma da vivo no da morto”. Per questo lavorava duramente e non amava perdere tempo, non gli piaceva girare intorno ai problemi, andava dritto al punto sembrando anche rude e poco tollerante.

David non era un “eroe” ma semplicemente un persona che con la sua innata determinazione era riuscito a superare ostacoli e paure di chi si fa voce di tutti quelli che, giornalmente, vedono i propri diritti calpestati, le proprie vite distrutte da pregiudizi, odio e violenza, e per questa ragione apriva la sua vita così come la sua porta di casa a chiunque gli chiedesse aiuto.

Ciao David, ovunque tu sia riposa in pace e che “dio ti benedica”, però quel dio in cui tu credevi e che ti dava forza, coraggio e fede, quel dio che non ti ha mai abbandonato, quel dio per cui hai duramente lottato, quel tuo dio che parlava di pace amore e fratellanza tra tutti gli individui. No, non quell’altro “dio”, quello che per bocca di individui come David Bahati, Martin Ssempra, James Buturo e di altri capi religiosi ed istituzionali incita alla violenza e alla non accettazione. Quel dio da loro invocato a giustificazione dell’incapacità di tollerare e accettare le diversità, come copertura per il loro odio, piccolezza d'animo e inadempienza delle proprie responsabilità e delle conseguenze del loro credo di morte e delle loro azioni sporche di sangue, sporghe anche del tuo sangue.

Fin quando noi saremo presenti, tu sarai con noi. Vivrai nella nostra lotta e nelle nostre battaglie affinché quel progetto non resti solo un sogno. Gioiremo di nuovo insieme, magari non sul divano di casa, a Kampala o a Roma.  Ma lo faremo e tutti insieme, perché te lo dobbiamo. Ciao David da tutti noi che ti hanno amato e che continueranno ad amarti.