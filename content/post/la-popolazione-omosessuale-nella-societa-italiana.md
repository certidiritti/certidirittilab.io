---
title: 'La popolazione omosessuale nella società italiana'
date: Fri, 18 May 2012 07:06:15 +0000
draft: false
tags: [Politica]
---

Rapporto Istat presentato alla Camera dei Deputati il 17 maggio 2012 in occasione della Giornata Internazionale contro l'omofobia e la transfobia. Scarica il report completo >

Il 61,3% dei cittadini tra i 18 e i 74 anni ritiene che in Italia gli omosessuali sono molto o abbastanza discriminati, l’80,3% che lo sono le persone transessuali.

Generalizzata appare la condanna di comportamenti discriminatori: il 73% è in totale disaccordo con il fatto che non si assuma una persona perché omosessuale o non si affitti un appartamento per lo stesso motivo.

D’altra parte, che persone omosessuali rivestano alcuni ruoli crea problemi ad una parte della popolazione: per il 41,4% non è accettabile un insegnante di scuola elementare omosessuale, per il 28,1% un medico, per il 24,8% un politico.

Il 74,8% della popolazione non è d’accordo con l’affermazione “l’omosessualità è una malattia”, il 73% con “l’omosessualità è immorale”, il 74,8% con “l’omosessualità è una minaccia per la famiglia”. Al contrario, Il 65,8% è d’accordo con l’affermazione “si può amare una persona dell’altro sesso oppure una dello stesso sesso: l’importante è amare”.

La maggioranza dei rispondenti ritiene accettabile che un uomo abbia una relazione affettiva e sessuale con un altro uomo (59,1%) o che una donna abbia una relazione affettiva e sessuale con un’altra donna (59,5%).

Ciononostante, il 55,9% si dichiara d’accordo con l’affermazione “se gli omosessuali fossero più discreti sarebbero meglio accettati”, mentre per il 29,7% “la cosa migliore per un omosessuale è non dire agli altri di esserlo”.

La maggioranza dei rispondenti (62,8%) è d’accordo con l’affermazione “è giusto che una coppia di omosessuali che convive possa avere per legge gli stessi diritti di una coppia sposata”. Il 43,9% con l’affermazione “è giusto che una coppia omosessuale si sposi se lo desidera”.

Maggiore è la contrarietà nei confronti dell’adozione dei figli (solo circa il 20% è molto o abbastanza d’accordo con la possibilità di adottare un bambino).

Le donne, i giovani e i residenti nel Centro Italia mostrano una maggiore apertura nei confronti degli omosessuali.

Circa un milione di persone si è dichiarato omosessuale o bisessuale, più tra gli uomini, i giovani e nell’Italia Centrale. Altri due milioni circa hanno dichiarato di aver sperimentato nella propria vita l’innamoramento o i rapporti sessuali o l’attrazione sessuale per persone dello stesso sesso.

Forti difficoltà emergono per gli omosessuali/ bisessuali in famiglia. Circa il 20% dei genitori sa che i loro figli vivono una tale condizione. Il dato è più alto per i fratelli (45,9%), i colleghi (55,7%) e soprattutto gli amici (77,4%).

Gli omosessuali/bisessuali dichiarano di aver subito discriminazioni a scuola o all’università, più degli eterosessuali (24% contro 14,2%) e così anche nel lavoro (22,1% contro il 12,7%). Un altro 29,5% si è sentito discriminato nella ricerca di lavoro (31,3% per gli eterosessuali).

Considerando tutti e tre questi ambiti, il 40,3% degli omosessuali/bisessuali dichiara di essere stato discriminato, contro il 27,9% degli eterosessuali. Si arriva al 53,7% aggiungendo le discriminazioni subite (e dichiaratamente riconducibili all’omosessualità/bises-sualità degli intervistati) nella ricerca di una casa (10,2%), nei rapporti con i vicini (14,3%), nell’accesso a servizi sanitari (10,2%) oppure in locali, uffici pubblici o mezzi di trasporto (12,4%).

**SOTTO IN ALLEGATO IL REPORT ISTAT COMPLETO.**

  
[report-omofobia.doc](http://www.certidiritti.org/wp-content/uploads/2012/05/report-omofobia.doc)