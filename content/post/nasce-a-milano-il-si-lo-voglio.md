---
title: 'Nasce a Milano il ''Sì lo voglio'''
date: Sat, 05 Mar 2011 10:37:56 +0000
draft: false
tags: [Comunicati stampa]
---

Sabato 5 marzo a Milano, in Via Bezzecca, 3, presso la sede del Cig - Arcigay, dalle ore 9,30 alle ore 17, riunione costitutiva del Comitato Si lo Voglio, insieme alle Associazioni promotrici e aderenti.

L'Associazione Radicale Certi Diritti, tra i promotori del Comitato Si lo Voglio, che si batterà principalmente sul tema del riconoscimento in Italia del matrimonio tra persone dello stesso sesso, partecipa alla riunione con una delegazione composta dal Segretario Sergio Rovasio, dai membri del Direttivo Enzo Cucco e Maurizio Cecconi e dal coordinatore del Gruppo di Certi Diritti Milano Carlo D. Cargnel.