---
title: 'Russia/Gay: per la Corte Costituzionale russa le leggi antigay non sono discriminatorie'
date: Thu, 25 Sep 2014 10:04:25 +0000
draft: false
tags: [CORTE COSTITUZIONALE, discriminazione, GAY, russia, Transnazionale]
---

[![gay-ban-2](http://www.certidiritti.org/wp-content/uploads/2014/09/gay-ban-2-300x168.jpg)](http://www.certidiritti.org/wp-content/uploads/2014/09/gay-ban-2.jpg)Mentre a San Pietroburgo il Queerfest ha dovuto essere cancellato all'ultimo momento a causa di minacce di aggressioni e bombe, nonché per le pressioni delle autorità, la Corte costituzionale russa ha deciso oggi che "il divieto di propaganda gay tra i minori non puo' essere considerata un limite ai diritti delle minoranze sessuali​". Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: "la lottizzazione delle istituzioni da parte del presidente Vladimir Putin ha ormai trasformato anche la Corte Costituzionale russa nella Suprema Cupola dell'omofobia di Stato. Se questa è l'interpretazione che la Russia dà della Convenzione europea per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali, occorre uno scatto da parte della comunità internazionale e dell'UE. In attesa di sapere come si pronuncerà la Corte Europea dei Diritti dell'Uomo, chiediamo a Federica Mogherini, Alto rappresentante dell'Unione per gli affari esteri e la politica di sicurezza, sulla violazione del diritto fondamentale alla libertà d'espressione in Russia".

Comunicato Stampa dell'Associazione Radicale Certi Diritti