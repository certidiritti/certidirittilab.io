---
title: 'GIORNALE UGANDESE DIFFONDE 100 FOTO DI PERSONE GAY  E NE CHIEDE L’IMPICCAGIONE.'
date: Thu, 14 Oct 2010 16:28:38 +0000
draft: false
tags: [Comunicati stampa]
---

**PARLAMENTARI RADICALI DEPOSITERANNO INTERROGAZIONE ALLA CAMERA E AL SENATO. CHIESTO L’INTERVENTO DEL PARLAMENTO EUROPEO E DELLE ORGANIZZAZIONI UMANITARIE.**

Bruxelles-Kampala-Roma, 12 ottobre 2010

**COMUNICATO STAMPA DI NON C’E’ PACE SENZA GIUSTIZIA, SEXUAL MINORITIES UGANDA  E DELL’ASSOCIAZIONE RADICALE CERTI DIRITTI:**

Il giornale ugandese Rolling Stone nel suo numero di Ottobre 02-09, Vol.1 n. 5 del 2010, ha pubblicato un articolo intitolato " **Divulgate 100 Foto Dei Leader Gay Ugandesi**" sollecitando "l'impiccagione degli omosessuali" in Uganda, e mostrando alcune delle 100 immagini di presunti omosessuali, lesbiche e altri attivisti di diritti umani, correlati da: nomi, posizione professionale, descrizione della vita privata, ubicazione dell’abitazione sia privata che lavorativa. L'articolo chiede inoltre che il governo dell'Uganda intervenga pesantemente nei confronti degli omosessuali.

La pubblicazione della lista dei nomi in questo contesto è francamente incompatibile con il principio dello Stato di diritto. Il principio della libertà di stampa non può essere tradotto nella mancanza di ogni forma di restrizione nelle scelte editoriali.

L'articolo che incita all'impiccagione di una minoranza sessuale in Uganda, alla violenza contro un gruppo particolare di cittadini, stigmatizzati da una proposta di legge che rende un determinato comportamento sessuale, un reato secondo il diritto ugandese, è un tentativo di intimidire non solo le minoranze sessuali, ma anche le autorità incaricate di vigilare e fare rispettare il principio dello Stato di diritto.

Non c'è Pace Senza Giustizia (NPSG), Sexual Minorities Uganda (SMUG) e l’Associazione Radicale Certi Diritti invitano tutti gli attivisti per i diritti umani e le organizzazioni internazionali della società civile di opporsi con forza a questa infame campagna orchestrata dal giornale ugandese Rolling Stone e di esprimere pubblicamente il loro sostegno al rispetto dei diritti umani e fondamentali di tutta la comunità LGBT in Uganda, prima che si possano verificare atti di violenza fisica, inoltre chiedono che il  giornale Rolling Stone immediatamente porga pubbliche scuse e che si impegni a non proseguire con la pubblicazione di questi articoli. Chiediamo che anche il Parlamento Europeo intervenga contro questa vergognosa e criminale campagna mediatica.

Non c'è Pace Senza Giustizia, Sexual Minorieties Uganda e l’Associazione Radicale Certi Diritti, chiedono al governo dell'Uganda di intervenire immediatamente a prendere tutte le misure appropriate per porre fine a questo inaccettabile incitamento alla violenza pubblica contro un gruppo specifico di cittadini. Il governo ugandese dovrebbe riconoscere e cogliere l'opportunità di esercitare la tutela dello stato di  diritto, il cui rispetto è affidato alla sua autorità, e dalla Costituzione ugandese, nonché i trattati internazionali e regionali sui diritti umani, di cui l'Uganda è firmataria.