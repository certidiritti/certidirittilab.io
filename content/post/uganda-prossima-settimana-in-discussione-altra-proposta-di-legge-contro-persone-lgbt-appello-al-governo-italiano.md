---
title: 'Uganda: prossima settimana in discussione altra proposta di legge contro persone lgbt. Appello al Governo italiano'
date: Wed, 08 Feb 2012 13:23:05 +0000
draft: false
tags: [Africa]
---

L'Associazione radicale Certi Diritti chiede al ministro degli esteri italiano Terzi un intervento presso i rappresentanti dell'Unione Europea.

\* nella foto il funerale di David Kato Kisule, ucciso il 26 gennaio 2011 a Kampala. Attivista per i diritti gay con l'associazione SMUG, David era intervenuto al IV Congresso di Certi Diritti a Roma per denunciare la terribile situazione per le persone lgbti in Uganda.

Roma, 8 febbraio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Dall’Uganda  giungono brutte notizie riguardo la proposta di legge che inasprisce le pene per gli atti omosessuali. Il deputato David Bahati, con il supporto delle chiese evangeliste-pentecostali,  espressione del fondamentalismo religioso locale, ha ripresentato una proposta di legge omofoba, che quasi sicuramente andrà in discussione la prossima settimana. Lo scorso anno, durante la sessione inaugurale del nuovo Parlamento,  grazie alle pressioni internazionali di Usa, Europa e altri paesi democratici, un’altra proposta di legge che prevedeva la pena di morte contro le persone gay, era stata accantonata. L’assassinio di David Kato Kisule aveva spinto le Ong e i Governi di molti paesi a intervenire su quanto stava accadendo in Uganda.

L’Associazione Radicale Certi Diritti chiede al Ministro degli Esteri del Governo italiano di intervenire con la massima urgenza con i paesi membri dell’ Unione Europea e in ambito di rapporti bilaterali Ue-Uganda, affinchè il paese africano non inasprisca le pene, già molto gravi, e che prevedono in alcuni casi l’ergastolo.

Una coppia gay ugandese, nei giorni scorsi è dovuta fuggire in Kenia a causa delle aggressioni subite da alcune persone a Kampala.