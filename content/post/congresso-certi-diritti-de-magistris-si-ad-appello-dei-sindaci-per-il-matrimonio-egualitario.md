---
title: 'Congresso Certi Diritti: De Magistris, sì ad appello dei sindaci per il matrimonio egualitario'
date: Mon, 08 Apr 2013 11:04:58 +0000
draft: false
tags: [Matrimonio egualitario]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 8 aprile 2013

Il VI Congresso dell'Associazione Radicale Certi Diritti si è concluso dopo tre giorni di lavori - nei quali importanti personalità internazionali e nazionali hanno riflettuto sulla legalizzazione della prostituzione, il matrimonio egualitario e la dicotomia tra valori tradizionali e diritti umani - con la conferma di Enzo Cucco e Yuri Guaiana nelle cariche di presidente e segretario dell'Associazione. Leonardo Monaco è stato eletto Tesoriere e Giacomo Cellottini, già tesoriere dell'Associazione, è stato eletto revisore dei conti.

Il sindaco di Napoli si è impegnato a sottoscrivere e promuovere, con l'Associazione Radicale Certi Diritti, un appello a favore del matrimonio egualitario da rivolgere a tutti i sindaci italiani. Luigi De Magistris ha anche scelto il nostro congresso per annunciare che il 27 aprile assegnerà ad Abu Mazen, presidente dell'Autorità Nazionale Palestinese, la cittadinanza onoraria. Ringraziamo molto il Sindaco e l'amministrazione partenopea per il sostegno dato al nostro congresso e per l'impegno preso con noi sul l'appello a favore del matrimonio egualitario, ma ricordiamo a De Magistris che nei territori controllati dall'Autorità Nazionale Palestinese, benché l'omosessualità non sia più illegale, le persone LGBTI subiscono una fortissima persecuzione di natura familiare e sociale che spesso mette direttamente a rischio le loro vite. 

Oltre al sindaco di Napoli, hanno partecipato ai lavori congressuali anche Ivan Scalfarotto (deputato PD), Sergio Lo Giudice, (Senatore PD), Benedetto Della Vedova (senatore Scelta Civica) e Luis Alberto Orellana (senatore M5S).

Tra i relatori internazionali si segnalano: Charles Radclife, Chief, Global Issues Section Office of the UN High Commissioner for Human Rights (OHCHR), Ryan J. Davis (consulente del Presidente Obama per la campagna sul tema della marriage equality), Renato Sabbadini (ILGA), Kemal Ordek, Attivista europeo per i diritti dei e delle sex workers, Joël Le Déroff (ILGA-Europe), Adrian Trett (LGBT+ Liberal Democrats), Altin Hazizaj (1st Ambassador, Pink Embassy); Olga Lenkova (Communications manager dell'associazione LGBT "Coming Out" di San Pietroburgo).

La mozione generale impegna l'associazione a concentrarsi su affermazione civile, sulla costruzione e il lancio di una campagna nazionale per il matrimonio egualitario e sullo sviluppo di iniziative transnazionali. Il congresso ha poi approvato tre ordini del giorno: uno "da mandato specifico agli organi dell’associazione di provare ancora una volta nel tentativo di trovare unità di obiettivi e di strategia con le altre associazioni lgbti italiane, nelle forme che verranno condivise secondo l’unità laica delle forze, che come radicali ci ha guidato nei nostri rapporti con le altre associazioni"; uno per chiedere al ministro Elsa Fornero "di adottare la Strategia con un atto formale, di livello governativo, che consenta al documento di avere la definizione amministrativa che il Consiglio d’Europa si aspetta e la realtà italiana necessita"; uno per aderire "alla campagna di raccolta firme promossa dall’Associazione Luca Coscioni, insieme ad altre realtà italiane, per l’eutanasia legale".

Il congresso si era aperto venerdì scorso con una una visita ispettiva al carcere di Poggioreale dell'onorevole Sandro Gozi accompagnato dal segretario dell'Associazione Radicale Certi Diritti, Yuri Guaiana, e dal tesoriere dell'Associazione Radicale per la Grande Napoli, Roberto Gaudioso. L'istituto di pena, uno dei più sovraffollati d'Europa, dovrebbe ospitare 1400 (1130 per via di lavori nel padiglione Genova) detenuti, mentre ne ospita circa 2800. Gli agenti di polizia penitenziaria dovrebbero essere 950, ma sono 737 effettivamente in servizio. Le condizioni igieniche sono drammatiche: l'amministrazione può permettersi di fornire carta igienica e sapone solo ogni mese e mezzo; vi è una costante promiscuità tra bagno e cucina, mentre in alcuni casi i sanitari sono separati dal resto della cella solo da un muretto non più alto di un metro. Il carcere ha dei reparti distinti e dedicati a transessuali e omosessuali, per nulla scontati in altri carceri italiani e meno afflitti dal sovraffollamento. È la prima volta che vediamo applicate le indicazioni, annunciate a livello di dipartimento, di misure straordinarie di protezione e sicurezza per le persone detenute che  possono vivere situazioni di pericolo a causa del loro orientamento sessuale oltre che di identità di genere. Il Capo Dipartimento, dott.sa Matone, aveva annunciato iniziative in questo senso qualche tempo fa. Problemi specifici dei detenuti LGBTI sono: l'accesso alle terapie ormonali per le persone transessuali e l'accesso al l'assistenza psicologica per i detenuti omosessuali che sembra ridursi alla disponibilità di una suora e di una volontaria da essa indicata, le cui opinioni sul l'omosessualità non ci sono note. 

  

**La registrazione integrale del congresso: [Prima giornata ("Regolamentare la Prostituzione")](http://www.radioradicale.it/scheda/376966), [Seconda giornata](http://www.radioradicale.it/scheda/376967), [Terza Giornata](http://www.radioradicale.it/scheda/376968)  
**

**[La Mozione Generale e gli Ordini del Giorno approvati dal VI° Congresso](chi-siamo/mozione-congressuale)**

**[Il bilancio approvato](chi-siamo/bilancio)  
**

**[La relazione delle attività svolte dall'Associazione nell'anno radicale 2011-2012](notizie/comunicati-stampa/item/download/37_e28c2afce9dbb5e7b4139b29e1a21ec7)  
**

**[L'aggiornamento sulla situazione di Affermazione Civile](notizie/comunicati-stampa/item/download/38_65e357cb3f0c11acd0e3d40d7c7be567)**

**[Il saluto del Ministro Elsa Fornero](notizie/comunicati-stampa/item/download/40_a89eb4b319e84c93621694904d914a33)**

**[Il saluto di Ottavio Marzocchi](notizie/comunicati-stampa/item/download/39_414e7312e380bf1a1c83b6daa4b94ff2)  
**

**I video-interventi degli ospiti internazionali:**

L'Intervento di Alejandro Nasif Salum al VI° Congresso dell'Associazione Radicale Certi Diritti [http://youtu.be/j0KctmYxNcQ](http://youtu.be/j0KctmYxNcQ)

L'Intervento di Altin Hazizaj al VI° Congresso dell'Associazione Radicale Certi Diritti [](http://youtu.be/I1bvXMe_0NI)[http://youtu.be/I1bvXMe_0NI](http://youtu.be/I1bvXMe_0NI)

Il saluto di Federico e Stefano di LegalizeLove [http://youtu.be/T4UW8Rp33Yc](http://youtu.be/T4UW8Rp33Yc)

L’Intervento di Charles Radclife al VI Congresso dell'Associazione Radicale Certi Diritti [http://www.youtube.com/watch?v=LEPh9m4ncXI](http://www.youtube.com/watch?v=LEPh9m4ncXI) ([traduzione](notizie/comunicati-stampa/item/download/35_95b102dce91b207bdc18293ed51cfca7))

L'Intervento di Ryan Davis al VI° Congresso dell'Associazione Radicale Certi Diritti [http://youtu.be/sU29XErGgb8](http://youtu.be/sU29XErGgb8) ([traduzione](notizie/comunicati-stampa/item/download/34_8c165ebc70fe20daa024bfc1b2b8ebe4))

L'Intervento di Olga Lenkova al VI° Congresso dell'Associazione Radicale Certi Diritti [http://youtu.be/30DbKiM7iKM](http://youtu.be/30DbKiM7iKM) ([traduzione](notizie/comunicati-stampa/item/download/33_ec633122ed9b8a4ea5fb6faa7e32b315))

  
[Olga_Lenkova.pdf](http://www.certidiritti.org/wp-content/uploads/2013/04/Olga_Lenkova.pdf)  
[Ryan\_J.\_Davis.pdf](http://www.certidiritti.org/wp-content/uploads/2013/04/Ryan_J._Davis.pdf)  
[Charles_Radclife.pdf](http://www.certidiritti.org/wp-content/uploads/2013/04/Charles_Radclife.pdf)  
[Ordine dei Lavori](http://www.certidiritti.org/wp-content/uploads/2013/04/Ordine_dei_lavori.pdf)  
[relazione\_attivityZ\_certi_diritti.pdf](http://www.certidiritti.org/wp-content/uploads/2013/04/relazione_attivityZ_certi_diritti.pdf)  
[affermazione_civile.pdf](http://www.certidiritti.org/wp-content/uploads/2013/04/affermazione_civile.pdf)  
[Ottavio_Marzocchi.pdf](http://www.certidiritti.org/wp-content/uploads/2013/04/Ottavio_Marzocchi.pdf)  
[Messaggio\_di\_saluto\_Ministro\_Fornero\_CERTI\_\_DIRITTI.pdf](http://www.certidiritti.org/wp-content/uploads/2013/04/Messaggio_di_saluto_Ministro_Fornero_CERTI__DIRITTI.pdf)