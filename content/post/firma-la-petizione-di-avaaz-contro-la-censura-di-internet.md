---
title: 'Firma la petizione di Avaaz contro la censura di internet'
date: Mon, 21 Mar 2011 17:24:56 +0000
draft: false
tags: [Politica]
---

Il governo italiano ha lanciato un nuovo attacco alla libertà di accesso all'informazione, e già questa settimana un organo amministrativo sconosciuto ai più potrebbe ricevere poteri enormi per censurare internet.

L’Autorità per le comunicazioni, un organo di nomina politica, sta per votare un meccanismo che potrebbe perfino portare alla chiusura di qualunque sito internet straniero - da Wikileaks a Youtube ad Avaaz! - in modo arbitrario e senza alcun controllo giudiziario. Gli esperti hanno già denunciato l’incostituzionalità della regolamentazione, ma soltanto una valanga di proteste dell’opinione pubblica può fermare questo nuovo assalto alle nostre libertà democratiche.  
  
Non c'è tempo da perdere, il voto potrebbe essere già questa settimana. Sappiamo che nell’Autorità ci sono membri contrari, e se ci uniremo con forza a loro nell'opporci a questo provvedimento potremmo fare la differenza. Inondiamo i membri dell'Autorità di messaggi per chiedere di respingere la regolamentazione e preservare così il nostro diritto ad accedere all’informazione su internet. Agisci ora e inoltra l'appello a tutti!  
  
**[FIRMA >](http://www.avaaz.org/it/it_internet_bavaglio/?vl)**

Campagna di Agorà Digitale, Altroconsumo e altre associazioni contro la delibera AGCOM sulla rimozione automatica dei contenuti su internet: http://sitononraggiungibile.e-policy.it/