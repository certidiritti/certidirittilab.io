---
title: 'Parlamento cieco sordo e muto sull’omofobia'
date: Tue, 26 Jul 2011 15:08:06 +0000
draft: false
tags: [Politica]
---

In quale Paese vivono i deputati che hanno presentato ed approvato le vergognose eccezioni di costituzionalità che hanno di nuovo affossato la proposta di legge contro l'omofobia?

Dichiarazione di Sergio Rovasio, Associazione radicale certi diritti

I signori e le signore della casta, così attenti a non toccare alcuno dei privilegi a loro spettanti, sono solertissimi nell’affossare anche i minimi tentativi di inserire nel nostro ordinamento una norma che censuri il comportamento omofobo e transfobico in quanto comportamento antisociale e lesivo della libertà personale dei singoli individui omosessuali e transessuali, delle loro famiglie e dei più basilari principi di rispetto e dignità che dovrebbero essere alla base della nostra convivenza civile.  
  
Per la seconda volta la Camera ha affossato la proposta di legge che, anche se timidamente, andava in questa direzione.

L’Associazione radicale Certi Diritti ribadisce che l’Italia ha bisogno di una moderna legge contro tutti i reati d’odio, che la sola via penale alla lotta contro omofobia e transfobia non basta, e che servono misure concrete, sostenute economicamente e politicamente, per combattere ignoranza e pregiudizio (le fonti di ogni atto omofobico e transfobico) così come in tutt’Europa accade da ormai molti anni.  Il lavoro meritorio avviato dal Ministero delle Pari Opportunità non basta, e deve essere potenziato anche attraverso un provvedimento legislativo.  
  
Siamo offesi e disgustati da un Parlamento capace solo di bocciare ogni pur minima proposta su questa materia, e stupisce che i deputati che hanno cancellato questo tentativo non siano altrettanto solerti nel colmare il vuoto normativo nella direzione da essi auspicata, ovvero nella direzione dell’approvazione di una norma più generale contro i reati d’odio.  
  
Un Parlamento che non vuol vedere la realtà della nostra società, che parla solo attraverso censure e beceri pregiudizi, che non vuol ascoltare chi sostiene la necessità di intervento su questa materia, come il Commissario dei diritti umani del Consiglio d’Europa, il Parlamento e la Commissione europea, l’ILGA-Europe.