---
title: 'GIORNATA MONDIALE CONTRO OMOFOBIA: OGGI AL QUIRINALE'
date: Mon, 18 May 2009 12:58:55 +0000
draft: false
tags: [Comunicati stampa]
---

**GIORNATA MONDIALE OMOFOBIA: OGGI ALLE ORE 18, L’ASSOCIAZIONE RADICALE CERTI DIRITTI SARA’ RICEVUTA AL QUIRINALE.**

Oggi, lunedì 18 maggio, alle ore 18, l’Associazione Radicale Certi Diritti sarà ricevuta al Quirinale dove verranno illustrati i principali episodi di violenza omofobica avvenuti in Italia in questi ultimi tre anni.

L’incontro si svolge a conclusione delle iniziative svolte in tuta Italia dall’Associazione Radicale Certi Diritti, insieme alle principali associazioni lgbt italiane, in occasione della giornata mondiale contro l’omofobia che si celebra ogni anno il 17 maggio.

La delegazione che si recherà al Qurinale è composta da:

\- Rita Bernardini, deputata radicale del Pd;

\- Sergio Rovasio, Segretario Associazione Radicale Certi Diritti;

\- Leila Deianis, rappresentante del Coordinamento Transgender Sylvia Rivera;

\- Guido Allegrezza, coordinatore campagna di Affermazione Civile di Certi Diritti.

**Cos’è l’omofobia e la transfobia e perché il 17 maggio:**

L'omofobia e la transfobia sono un insieme di emozioni e sentimenti come ansia , disgusto, avversione, paura e disagio, che taluni provano in maniera conscia o inconscia nei confronti di gay , lesbiche e transessuali".

La diffusione dell' omofobia è molto variabile , in quanto essa dipende da fattori quali l'atteggiamento dei mass media, , le leggi persecutorie, l'incidenza delle credenze religiose integraliste, il grado di scolarizzazione, il tasso di maschilismo, il sesso (gli uomini sono più omofobici delle donne), ecc. In Italia l' omofobia è molto diffusa.

Dal 17 maggio 1990, giorno in cui l' Assemblea generale dell' Organizzazione mondiale della sanità (Oms) ha eliminato l' omosesualità dalla lista delle malattie mentali, in tutto il mondo si celebra la giornata contro l'omofobia. Il 17 maggio è diventato un momento di riflessione e azioni allo scopo di lottare contro ogni violenza fisica, morale o simbolica legata all' orientamento sessuale.