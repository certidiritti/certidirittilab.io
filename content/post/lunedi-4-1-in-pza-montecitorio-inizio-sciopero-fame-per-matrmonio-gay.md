---
title: 'LUNEDI'' 4-1 IN P.ZA MONTECITORIO INIZIO SCIOPERO FAME PER MATRMONIO GAY'
date: Wed, 23 Dec 2009 11:56:44 +0000
draft: false
tags: [Comunicati stampa]
---

**Lunedì 4 gennaio, a Roma – Piazza Montecitorio, alle ore 15 Conferenza Stampa**

**dI Francesco Zanardi e Manuel Incorvaia**

**Il movimento spontaneo Gay Italiani promosso e fondato da Francesco Zanardi e Manuel Incorvaia, terrà lunedì 4 gennaio 2010, alle ore 15, una conferenza stampa sulle ragioni dello sciopero della fame ad oltranza finalizzato al riconoscimento della propria unione da parte delle istituzioni.**

**Aderiscono e/o partecipano:  
\- Don Franco barbero, esponente delle comunità cristiane di base;  
\- Sergio Rovasio, segretario associazione Radicale Certi Diritti;  
\- Donatella Poretti, Senatrice radicale - Pd;  
\- Anna Paola Concia, deputata Pd;  
\- Vladimir Luxuria, già parlamentare;  
\- Franco Grillini già parlamentare, Direttore di Gaynet;  
\- Alessandro Cecchi Paone, giornalista;  
\- Intergroup LGBT e ILGA del Parlamento Europeo;  
\- I rappresentanti regionali del movimento Gay Italiani;  
\- Tutte le coppie che in Italia hanno aderito all’iniziativa di Affermazione Civile;  
\- I rappresentanti delle associazioni nazionali che partecipano al progetto e al riconoscimento dei diritti della comunità LGBT italiana.  
  
Sono stati invitati all’iniziativa:  
  
\- Tutte le associazioni nazionali che intendono intervenire;  
\- Rita Levi Montalcini;  
\- Sergio Chiamparino, Sindaco di Torino;  
\- Federico Berruti, Sindaco di Savona;  
\- Tutte le coppie che in Italia hanno aderito all’iniziativa di Affermazione Civile;  
\- Imma Battaglia;  
\- Tutti gli organi di stampa nazionali e le principali agenzie estere;  
Intervista rilasciata a al canale LGBT EUSTACCHIO79 rilasciata a Roma in data 21/12/2009 disponibile su YouTube [http://www.youtube.com/watch?v=R2VcSAifT5M](http://www.youtube.com/watch?v=R2VcSAifT5M)**