---
title: 'Diritti gay, sul palco e alla Consulta'
date: Sun, 04 Apr 2010 16:48:30 +0000
draft: false
tags: [Senza categoria]
---

http://www.radicalifvg.it/piccolo_020410.pdf

Mentre al di là dell’oceano, nei teatri di Broadway, l'omosessualità non è mai andata così tanto in scena, in drammi, commedie e musical che si concentrano sul privato e sulle problematiche quotidiane delle coppie gay, da questa parte dell’Atlantico, al Teatro Rossetti di Trieste, è arrivata “Angels in America. Fantasia gay su temi nazionali”, pluripremiata opera teatrale scritta da Tony Kushner nei primi anni '90 e presentata nella versione italiana con la regia di Ferdinando Bruni ed Elio De Capitani.

Proprio quest’ultimo, regista e attore del teatro dell’Elfo, ha partecipato mercoledì, insieme all’avvocato Francesco Bilotta e a Clara Comelli, presidente dell’associazione radicale Certi Diritti, a un incontro alla Libreria Lovat dedicato al tema ”I gay, attori sociali al di là e al di qua dell'oceano”. Un incontro in cui si è parlato di lotte politiche, di diritti negati e naturalmente di “Angels in America”, opera teatrale dall'accento fortemente politico. «Si tratta di un testo – spiega De Capitani – in cui si parla d’amore, di Aids, e del rifiuto dell’amministrazione Reagan di fare qualcosa per questa malattia quando si palesò: un rifiuto che durò per ben due anni».

Se nei teatri newyorchesi quindi, e lo ammette lo stesso autore di “Angels in America” Tony Kushner, le storie personali delle coppie gay hanno rubato la scena alle loro lotte politiche, è perché oggi gli omosessuali, soprattutto nelle grandi città americane, sono finalmente integrati in società: il matrimonio gay è una realtà in cinque stati dell' Unione, le coppie gay possono ricorrere all' adozione e, grazie all’amministrazione Obama, anche le omofobiche forze armate statunitensi hanno dovuto cambiare rotta.

E la storia personale di Kushner ne è la dimostrazione: «Si è sposato - racconta Elio De Capitani - con il suo compagno, con tanto di pubblicazione dell’annuncio di nozze sul New York Times, facendo così anche della sua storia personale una narrazione di come si coniughi la parola libertà negli Stati Uniti d’America».

In Italia invece c'è ancora bisogno di pièce come “Angels in America”: il matrimonio gay non esiste, anche se pare che ora qualcosa si stia finalmente muovendo. «Il 23 marzo – racconta Francesco Bilotta – per la prima volta alla Corte Costituzionale si sono presentate, sfilandosi l’etichetta della sessualità, che fa tanto “uomo a una dimensione”, delle persone con le loro storie d’amore: tre coppie, due di uomini e una di donne, che hanno richiesto il rispetto di un loro diritto, quello all’eguaglianza. Hanno chiesto di potersi sposare in Comune, come possono fare tutti i cittadini italiani. E la Consulta, per la prima volta, ha deciso di discutere l’argomento».

«Avremmo voluto in questa sede raccontarvi di un risultato raggiunto - conclude Comelli - ma la decisione della Corte Costituzionale è stata rimandata al 12 aprile».

Fino a quel giorno la comunità omosessuale italiana rimarrà con il fiato sospeso, ma con la soddisfazione di essere riuscita a non farsi schiacciare dal potere costituito: che gli omosessuali abbiano zero potere, come recita De Capitani in “Angels in America”, potrebbe presto divenire un falso stereotipo.

**Giulia Basso**