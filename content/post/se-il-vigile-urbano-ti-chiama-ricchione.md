---
title: 'Se il vigile urbano di Milano ti chiama “ricchione”'
date: Fri, 15 Jun 2012 10:27:10 +0000
draft: false
tags: [Politica]
---

Lettera aperta al Sindaco di Milano

di Leonardo Monaco - Segreteria dell'Associazione Radicale Certi Diritti

Al Sindaco di Milano,

Ieri da San Siro è partito un messaggio molto forte. Durante l'esecuzione del brano “Nobody knows”, [](http://www.youtube.com/watch?feature=player_embedded&)[il led wall del palco di Madonna ha mostrato le immagini di un video](http://www.youtube.com/watch?feature=player_embedded&v=sP0-b01zWXA) che alternava ai volti dei giovani suicidi vittime di bullismo omofobico, le immagini dei leader politici e degli estremisti religiosi simbolo dell'intolleranza e della repressione delle libertà sessuali.

Dopo la splendida serata, la buona musica e la sensazione di aver ben speso i soldi del biglietto, mai mi sarei aspettato che durante la strada del ritorno, attraversando un incrocio, avrei sentito dire da un vigile urbano intento a dirigere il traffico: “Questi sono uno più ricchione dell'altro”, riferendosi al mio gruppo di amici.

Sarà stato per la frustrazione o per la voglia di tornare a casa e mettersi sotto le coperte che abbiamo percorso qualche metro senza dare sufficiente peso all'accaduto, ma ad un tratto – forse incuriositi dalla reazione della Municipale alla nostra lamentela, forse spinti da quello spirito polemico che accomuna me e il mio amico Claudio – siamo tornati indietro per manifestare il nostro malcontento in seguito all'accaduto.

La reazione del vigile che ha pronunciato la pittoresca e infelice frase è stata di totale indifferenza, ma quanto hanno detto i suoi due colleghi (un uomo e una donna) è stato ancor più disarmante: “avete qualche problema”, “andate a farvi un sonno” e per finire la minaccia di denunciarci per interruzione di pubblico servizio (ma non chiedo forse che venga fatto “pubblico servizio”?)

In questa mia lettera scritta di getto, da Liberale Radicale mi riservo di non riportare le coordinate del luogo dove è accaduto il fatto; non sono un giustizialista e non farò reclamo alla polizia Municipale con alla mano i numeri di matricola degli agenti (che comunque non ho, visto che sulle uniformi non c'è numero o nome che mi permetta di risalire all'identità del vigile).

Non lo faccio perché non cerco provvedimenti diversi nei confronti dei tanti italiani ignoranti e – per fortuna – di quei pochissimi infelici che prestano servizio nel corpo della Polizia Municipale di Milano, il cui lavoro è già abbastanza difficile per colpa del sottodimensionamento e dei tagli.

  
Non voglio aggravanti anti-omofobia, perché non posso tollerare che il fattore di discriminazione che non permette a gay e lesbiche di accedere ai diritti per i quali combattono e chiedono pari opportunità diventi la ragione per chiedere pene esemplari a chi si macchia di violenza verbale o fisica, sempre grave indipendentemente dal bersaglio. Non voglio che diventi una vigliacca e vittimistica affermazione di diversità.

Ritengo di far parte di quelli che non si accontentano di “toppe” estemporanee che le istituzioni propongono di tanto in tanto per tappare i buchi del colabrodo italiano dei diritti delle minoranze. Faccio parte di quei “visionari” che vedono come unica panacea il Diritto e i diritti, l'incontrastata possibilità per tutti di realizzarsi umanamente.

Sono tra quelli che credono fermamente in chi dice che “laddove c'è strage di Diritto c'è strage di popoli”, motivo per il quale chiedo responsabilità ai dipendenti dell'amministrazione pubblica e a una certa classe politica prima di attaccare gratuitamente o di pontificare su ciò che la loro soggettività li porta a credere giusto o sbagliato, morale o immorale, degno o indegno. Lo dico per le mie coetanee e i miei coetanei, per quelli che resistono e per quelli che non ce l'hanno fatta. Per i ragazzi del video di Madonna e per i giovani italiani che hanno provato a farla finita, troppo spesso riuscendoci. Lo dico per denunciare chi, con le sue infelici uscite, può e deve ritenersi in parte responsabile (materialmente, non moralmente) di ogni suicidio, di ogni pestaggio.

A nome dell'Associazione Radicale “Certi Diritti” propongo al Sindaco e agli assessori a cui è indirizzata questa lettera di contattarci e di organizzare corsi di aggiornamento per i dipendenti pubblici sull'educazione all'uguaglianza (e ripeto “uguaglianza”, non diversità)

Con un pizzico di ironia mi permetto di aggiungere che in occasione del prossimo concerto della pop star italo-americana a Milano, il Comune potrebbe valutare l'idea di non regalare come suo solito biglietti omaggio ai suoi consiglieri, ma di distribuirli a tutti gli agenti della Polizia Municipale al fine di offrire agli intolleranti una lezione di civiltà e di rispetto tenuta dalla signora Ciccone, e ai tanti – tanti! – altri che non ne hanno bisogno uno spettacolo sensazionale come quello di ieri.

  
Distinti saluti

  
Leonardo Monaco

Associazione Radicale “Certi Diritti”

Tesoriere dell'Associazione Enzo Tortora – Radicali Milano