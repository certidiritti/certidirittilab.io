---
title: 'MONSIGNOR BABINI FREQUENTI DI PIU'' BISONTI E SCIMPANZE'' ANZICHE''...'
date: Mon, 12 Apr 2010 10:26:29 +0000
draft: false
tags: [Comunicati stampa]
---

**GAY E CHIESA OMOFOBA: MONSIGNOR BABINI FAREBBE MEGLIO A FREQUENTARE BISONTI E SCIMPANZE’ ANZICHE’ LANCIARE SAETTE E ANATEMI OFFENSIVI CONTRO LE PERSONE OMOSESSUALI.**

Roma, 12 aprile 2010

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

"Per evitare, se siamo ancora in tempo, che Monsignor Giacomo Babini, Vescovo emerito di Grosseto, diventi anche emerito di qualcos’altro, gli consigliamo vivamente di frequentare di più bisonti e scimpanzé anziché lanciare anatemi e offese contro le persone omosessuali.  Sostenere, come fa lui, che occorre che gli omosessuali ”accettino serenemante la loro croce e la malattia con santa rassegnazione” e dire agli omosessuali che si vantano della loro condizione “che persino gli animali rispettano l'ordine della natura e loro no” e, ancora, che “da questo punto di vista è meglio la regolarità degli animali”, dimostra una miserabile ignoranza dell’emerito Monsignore sul fatto che molti animali praticano l’omosessualità. Ci auguriamo quindi che l’emerito si faccia uno zoo safari  per capire che in natura l’omosessualità è molto diffusa a cominciare dai bisonti e dagli scimpanzé”.