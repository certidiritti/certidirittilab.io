---
title: 'Ad un anno dalla sentenza sul matrimonio gay lettera ai sindaci'
date: Wed, 23 Mar 2011 16:20:48 +0000
draft: false
tags: [Matrimonio egualitario]
---

Il 23 marzo 2010 cade il primo anniversario dell'udienza che si è tenuta presso la Corte costituzionale per valutare la costituzionalità del rifiuto al matrimonio tra persone dello stesso sesso. Ad un anno di distanza Certi Diritti, che insieme a Rete Lenford ha promosso la campagna di Affermazione Civile, lancia l'iniziativa 'Giornate di dialogo tra le coppie delle stesso sesso e le Pubbliche Amminsitrazioni'.

OGGETTO 12 Aprile 2011 - ad un anno dalla sentenza 138/2010 della Corte Costituzionale, le coppie dello stesso sesso dialogano con le Pubbliche Amministrazioni

Il 23 marzo 2010 cade il primo anniversario dell’udienza che si è tenuta presso la Corte costituzionale per valutare la costituzionalità del rifiuto al matrimonio tra persone dello stesso sesso.

Dopo alcune settimane, il 12 aprile 2010, la relativa sentenza, 138/2010 ha segnato un passo fondamentale. Da quel giorno, nella giurisprudenza italiana sono sanciti inequivocabilmente i seguenti principi:  
• Le coppie dello stesso sesso hanno rilevanza costituzionale e non sono un mero fatto privato  
• Il matrimonio tra persone dello stesso sesso è compatibile con l’attuale Costituzione  
• Le coppie dello stesso sesso non sposate sono su un piano giuridico differente, perché più meritevole di riconoscimenti di diritti e doveri rispetto alle coppie eterosessuali non sposate  
• Il parlamento ha il dovere di legiferare in materia con una regolamentazione organica e generale, dunque non privatistica.  
  
L’udienza del 23 marzo 2010 e la sentenza 138/2010 del 12 Aprile non sono avvenute “per caso”, ma sono state il frutto di un movimento di consapevolezza civica chiamato “Affermazione Civile”. Già dal 2007, infatti, diverse decine di coppie dello stesso sesso si sono recate nel comune di residenza, per chiedere la pubblicazione degli atti del matrimonio e impugnare il diniego presso i tribunali competenti. Questo è avvenuto con il sostegno dell’associazione radicale Certi Diritti e della rete di avvocatura LGBT Rete Lenford.

Il 23 marzo segna un momento fondamentale poiché, per la prima volta in Italia, dei cittadini omosessuali, senza celare il loro orientamento sessuale, né la loro condizione affettiva e famigliare, si sono posti apertamente nei confronti delle istituzioni, giungendo fino al massimo livello, rappresentato appunto dalla Corte costituzionale. Ma, occorre ricordare che quel dialogo tra cittadini omosessuali e istituzioni della Repubblica era partito qualche anno prima dal basso, e proprio dagli uffici dello Stato Civile di tutta Italia.

Il 23 marzo 2010 è il giorno in cui, irreversibilmente, le persone omosessuali e le famiglie da loro costituite sono entrate nelle istituzioni, con la stessa dignità di ogni altro cittadino italiano e di ogni altra famiglia italiana.

Questa presenza va incoraggiata e sostenuta. E l’associazione radicale Certi Diritti intende farlo, invitando le coppie dello stesso sesso italiane a presentarsi – in particolare tra il 23 Marzo e il 12 Aprile – presso i propri comuni di residenza per richiedere le pubblicazioni degli atti di matrimonio.

In virtù dell’articolo 98 del Codice Civile, queste coppie sapranno già di attendersi un documento di diniego scritto, che ci auguriamo sarà preceduto da un confronto e un dialogo responsabile tra cittadini e istituzioni.

E’ evidente che le coppie non si presenteranno per “averla vinta” sui comuni italiani, bensì per instaurare un rapporto rispettoso e aperto con le istituzioni della città in cui vivono. L’invito che rivolgiamo a voi, è quello di prepararvi a questo dialogo, e di affrontare senza pregiudizi tutte le buone novità che la società odierna porta con sé.

Per una coppia alla quale viene negato di sposarsi, quello di richiedere le pubblicazioni degli atti di matrimonio è un momento familiare importantissimo, che merita il massimo rispetto interpersonale e istituzionale.  
Per gli uffici di Stato Civile, potersi confrontare apertamente con le coppie dello stesso sesso del proprio comune è un’occasione unica di instaurare un dialogo che, nel tempo, speriamo porterà al riconoscimento pieno di tutte le famiglie italiane.

  
Per ogni altra informazione, vi preghiamo di non esitare a contattarci. Saremo ben lieti di spiegarvi a fondo la storia e i fondamenti giuridici di questa iniziativa civica e giuridica di dialogo e di apertura alle istituzioni.

Cordiali saluti

 Sergio Rovasio     Gian Mario Felicetti  
Segretario Associazione Radicale Certi Diritti         Coordinatore campagna Affermazione Civile