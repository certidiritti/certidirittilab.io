---
title: 'Italia: no a riconoscimento matrimoni gay fatti all''estero'
date: Sun, 29 May 2011 12:17:18 +0000
draft: false
tags: [Diritto di Famiglia]
---

**Coppie gay sposate o registrate in altri paesi Eu non devono essere riconosciute. Commissione Politiche Europee della Camera vota contro la proposta della Commissione europea per riconoscimento principio di sussidarietà.**

**La via legale sempre più necessaria per applicare Trattati di Nizza e Lisbona.**

Roma, 29 maggio 2011

“La Commissione Europea sta tentando di rispondere al vuoto che si determina tra i 27 paesi membri dell’Unione Europea riguardo la mancanza di garanzie in cui si trovano le coppie ‘internazionali’ (quelle che hanno uno dei due partner di un altro paese Ue o quelle che si sono sposate in un altro Stato membro), indipendemente dal loro orientamento sessuale.

Tra le iniziative promosse in ambito europeo vi è il tentativo di dare riconoscimento al principio di sussidiarietà per rimuovere le difficoltà che le ‘coppie internazionali’ devono ancora affrontare nella gestione quotidiana dei loro beni o al momento della divisione del loro patrimonio a causa delle eccessive diversità tra le norme applicabili nei vari stati membri.  Sono gli stessi Trattati di Nizza e di Lisbona, sulla libera circolazione e contro le discriminazioni a imporlo. E’ evidente che tale mancato riconoscimento vìola i Trattati, ragione per la quale l’Associazione Radicale Certi Diritti intende promuovere anche ricorsi alla Corte di Giustizia Europea (Lussemburgo) oltre a quelli già programmati alla Corte Europea dei Diritti dell’Uomo  (Strasburgo).

Giovedì 26 maggio la ‘Commissione Politiche dell’Unione Europea’ della Camera dei deputati ha discusso su un documento dell’on. Sandro Gozi del Pd, che aveva proprio l’obiettivo di accogliere quanto proposto dalla Commissione Europea su cui poi il Consiglio Europeo (l’organismo che raccoglie tutti i Governi dell’Ue) è chiamato a decidere per risolvere questo grave problema di discriminazioni. La  maggioranza della Commissione, come al solito ispirata dal peggior pregiudizio, da motivazioni ridicole, patetiche e di indifferenza alle problematiche sempre più diffuse in ambito europeo sul mancato riconoscimento del principio di sussidiarietà, ha  respinto il documento presentato dall’On. Sandro Gozi del Pd che aveva proprio l’obiettivo di dare un’indicazione precisa e chiara per risolvere questo problema.

L’Associazione Radicale Certi Diritti, ringrazia l’on. Sandro Gozi per l’impegno avuto in Commissione e per aver proposto un documento che proponeva di accogliere le richieste degli organismi europei. Non c’è dubbio che la strada dei ricorsi alla Corte di Giustizia Europea e alla Corte Europea dei Diritti dell’Uomo è, in Italia,  l’unica ormai praticabile, grazie all’ottusità e al pregiudizio di parte della nostra classe politica, sempre più asservita agli interessi vaticani e ed ispirata ad una concezione fondamentalista della società”.