---
title: 'LA LETTERA DI OSCAR WILDE LETTA DA ROBERTO BENIGNI AL FESTIVAL DI SANREMO'
date: Thu, 19 Feb 2009 12:37:36 +0000
draft: false
tags: [Comunicati stampa]
---

**DI SEGUITO LA LETTERA SCRITTA IN CARCERE DA OSCAR WILDE PER L'AMATO LORD ALFRED DOUGLAS.**

Onoriamo la sua memoria e ringraziamo Roberto Benigni per averla letta lunedì 16 febbraio in apertura del Festiva di Sanremo.

 

Mio carissimo ragazzo,

questo è per assicurarti del mio amore immortale, eterno per te. Domani sarà tutto finito. Se la prigione e il disonore saranno il mio destino, pensa che il mio amore per te e questa idea, questa convinzione ancora più divina, che tu a tua volta mi ami, mi sosterranno nella mia infelicità e mi renderanno capace, spero, di sopportare il mio dolore con ogni pazienza. Poiché la speranza, anzi, la certezza, di incontrarti di nuovo in un altro mondo è la meta e l' incoraggiamento della mia vita attuale, ah! debbo continuare a vivere in questo mondo, per questa ragione.

Il caro *** mi è venuto a trovare oggi. Gli ho dato parecchi messaggi per te. Mi ha detto una cosa che mi rassicurato: che a mia madre non mancherà mia niente. Ho sempre provveduto io al suo mantenimento, e il pensiero che avrebbe potuto soffrire delle privazioni mi rendeva infelice.

Quanto a te (grazioso ragazzo dal cuore degno di un Cristo), quanto a te, ti prego, non appena avrai fatto tutto quello che puoi fare, parti per l'Italia e riconquista la tua calma, e componi quelle belle poesie che sai fare tu, con quella grazia così strana. Non esporti all' Inghilterra per nessuna ragione al mondo. Se un giorno, a Corfù o in qualche isola incantata, ci fosse una casetta dove potessimo vivere insieme, oh! la vita sarebbe più dolce di quanto sia stata mai.

Il tuo amore ha ali larghe ed è forte, il tuo amore mi giunge attraverso le sbarre della mia prigione e mi conforta, il tuo amore è la luce di tutte le mie ore. Se il fato ci sarà avverso, coloro che non sanno cos'è l'amore scriveranno, lo so, che ho avuto una cattiva influenza sulla tua vita. Se ciò avverrà, tu scriverai, tu dirai a tua volta che non è vero. Il nostro amore è sempre stato bello e nobile, e se io sono stato il bersaglio di una terribile tragedia, è perchè la natura di quell' amore non è stata compresa.

Nella tua lettera di stamattina tu dici una cosa che mi dà coraggio. Debbo ricordarla. Scrivi che è mio dovere verso di te e verso me stesso vivere, malgrado tutto. Credo sia vero. Ci proverò e lo farò. Voglio che tu tenga informato Mr Humphreys dei tuoi spostamenti così che quando viene mi possa dire cosa fai. Credo che gli avvocati possano vedere i detenuti con una certa frequenza. Così potrò comunicare con te.

Sono così felice che tu sia partito! So cosa deve esserti costato. Per me sarebbe stato un tormento pensarti in Inghilterra mentre il tuo nome veniva fatto in tribunale. Spero tu abbia copie di tutti i miei libri. I miei sono stati tutti venduti. Tendo le mani verso di te. Oh! possa io vivere per toccare i tuoi capelli e le tue mani. Credo che il tuo amore veglierà sulla mia vita. Se dovessi morire, voglio che tu viva una vita dolce e pacifica in qualche luogo fra fiori, quadri, libri, e moltissimo lavoro. Cerca di farmi avere tue notizie.

Ti scrivo questa lettera in mezzo a grandi sofferenze ; la lunga giornata in tribunale mi ha spossato. Carissimo ragazzo, dolcissimo fra tutti i giovani, amatissimo e più amabile. Oh! aspettami! aspetta mi! io sono ora, come sempre dal giorno in cui ci siamo conosciuti, devotamente il tuo, con un amore immortale

Oscar

Lunedì sera \[29 aprile 1895\]

Carcere di S.M., Holloway