---
title: 'Corte Strasburgo/Associazioni: Il Governo non impugni la sentenza. Il Parlamento approvi il matrimonio per tutti'
date: Thu, 23 Jul 2015 13:49:46 +0000
draft: false
tags: [Matrimonio egualitario]
---

[![lostessosi](http://www.certidiritti.org/wp-content/uploads/2015/07/lostessosi-300x286.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/07/lostessosi.jpg)

**Corte Strasburgo / Associazioni: Il Governo non impugni la sentenza. Il Parlamento approvi il matrimonio per tutti** Roma, 23 luglio 2015 Le associazioni italiane LGBTI e tutte le associazioni impegnate sul terreno delle libertà civili, che aderiscono alla Campagna #LoStessoSì, all’indomani della sentenza della Corte Edu che condanna lo Stato italiano per la violazione dell’articolo 8 – il diritto al rispetto per la vita privata e familiare – della Convenzione europea dei diritti umani, esprimono preoccupazione per l’inerzia di un legislatore che colpevolmente, al di là del colore politico, non ha saputo sino ad oggi riconoscere dignità sociale alle relazioni familiari delle persone omosessuali. Chiediamo che il Governo italiano si esprima, in modo inequivoco e coerentemente con gli impegni assunti dai propri esponenti nel corso di questi mesi, dichiarando di rinunciare alla impugnazione della decisione, mediante acquiescenza. Chiediamo altresì che il Parlamento italiano introduca senza ritardi una legge che garantisca alle coppie dello stesso sesso la protezione della vita familiare mediante l’estensione del matrimonio civile, l’unico istituto che garantirebbe realmente la piena uguaglianza tra tutti i cittadini e le cittadine. Lo Stato italiano sarà da oggi sotto osservazione da parte dell'Assemblea dei Ministri del Consiglio d’Europa, ma anche le associazioni continueranno a vigilare, perché non vengano introdotte forme di discriminazione in materia di diritto di famiglia. D'ora in avanti il colpevole immobilismo delle istituzioni provocherà per lo Stato l'obbligo di risarcire ogni singola persona o coppia omosessuale vittima di violazione del diritto fondamentale alla vita familiare. Risarcimenti che peseranno sulle casse dello Stato e sulle tasche di ogni singolo cittadino costretto a pagare per l'inerzia della classe politica. Noi ci impegneremo a promuovere e sostenere tutte le relative azioni giudiziarie.

Agedo

Antéros LGBTI Padova

Arci

Arcigay

Arcilesbica

Associazione Radicale Certi Diritti

Associazione Attivisti Gay Harvey Milk Onlus Ascoli Piceno

Avvocatura per i Diritti LGBTI - Rete Lenford

Azione Gay e Lesbica

CILD - Coalizione Italiana per le Libertà e Diritti Civili

Circolo Tondelli  Bassano del Grappa

Circolo di Cultura Omosessuale Mario Mieli

CondividiLove

Cooperativa Befree

Coordinamento Torino Pride

Diversity

Edge - Excellence & Diversity by GLBT Executives.

Equality Italia

Famiglie Arcobaleno

Harvey Milk Onlus

I Mondi Diversi

La Fenice Gay

L.Arcobaleno - LUISS Students Association

LoveOutLaw

Polis Aperta

Progetto Prisma

Rete Genitori Rainbow