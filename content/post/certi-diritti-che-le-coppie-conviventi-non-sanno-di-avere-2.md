---
title: '''Certi Diritti che le coppie conviventi non sanno di avere'''
date: Sun, 15 Apr 2012 13:35:32 +0000
draft: false
tags: [Diritto di Famiglia]
---

Fino a quando le coppie conviventi, etero o gay, continueranno ad essere discriminate, questo libro edito da Stampa Alternativa è importante sia per le coppie sia per chi, ignorando ciò che già la Costituzione ed alcune leggi dicono, rischia di collocarsi tra i discriminatori.

Per trovare esempi di leggi o sentenze che parlano di diritti delle coppie conviventi e, in particolare, delle coppie composte da persone dello stesso sesso, siamo abituati a guardare l’Europa.

I singoli Stati o il Parlamento europeo costituiscono una “miniera” di principi, raccomandazioni, esempi, cui di solito ci rivolgiamo quando siamo un po’ depressi per come vanno le cose in Italia.

Ora sembra, tuttavia, che il vento stia cambiando.

La “nostra” Corte di Cassazione, nel 2011, ha affermato che la convivenza è una vera e propria famiglia, portatrice di valori di “stretta solidarietà, arricchimento e sviluppo della personalità di ogni componente e di educazione ed istruzione della prole” e, nel 2012, la medesima Corte ha detto che è stata radicalmente superata la concezione secondo cui la diversità di sesso tra gli sposi è presupposto indispensabile per l’esistenza del matrimonio.

Possiamo quindi pensare che si stia avvicinando il momento in cui un libro come quello che abbiamo scritto non servirà più.

Per ora, tuttavia, fino a che il messaggio non giunga alle “orecchie” dei politici e gli stessi non riescano a mettersi d’accordo su di un testo e poi a farne addirittura una legge, le coppie conviventi, le famigerate “coppie di fatto”, etero o gay, continuano ad essere discriminate, a non avere successioni e pensioni, trasferimenti e tutele, per se stesse e per i figli, e possono incontrare difficoltà anche solo per assistere il partner in ospedale, in caso di malattia.

Per sopravvivere in questa fase provvisoria, che ci auguriamo non sia lunga come quella che occorse all’epoca per assicurare il diritto di voto alle donne o per altre storiche battaglie di civiltà del passato, il libro è ancora necessario.

Esso dice quali sono i diritti che le coppie comunque hanno e come possono farli valere, come possono servirsi delle leggi e degli strumenti dell’autonomia privata che già esistono, maneggiandoli con l’abilità di un fiorettista, per essere, se non “non più discriminati”, almeno discriminati il meno possibile.

Se ne consiglia la lettura a tutti, sia a coloro che rischiano la discriminazione, cui anzi si suggerisce di tenerlo sempre con sé, sull’autobus e sul comodino, per avere sempre la risposta pronta, sia a coloro che, ignorando ciò che già la Costituzione ed alcune leggi dicono, rischiano di collocarsi tra i discriminatori.

Dal 21 marzo 2012 il libro è disponibile in tutte le librerie e online qui.