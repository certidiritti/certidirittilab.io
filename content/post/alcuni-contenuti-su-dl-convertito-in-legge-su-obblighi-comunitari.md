---
title: 'ALCUNI CONTENUTI SU DL CONVERTITO IN LEGGE SU OBBLIGHI COMUNITARI'
date: Tue, 10 Jun 2008 17:38:15 +0000
draft: false
tags: [Comunicati stampa]
---

Il Parlamento approva definitivamente le modifiche ai decreti legislativi nn. 215 e 216 del 2003 contro le discriminazioni. In risposta alla procedura di infrazione avviata dalla Commissione europea contro l’Italia, [il Parlamento ha modificato entrambi i decreti di recepimento](http://www.senato.it/japp/bgt/showdoc/frame.jsp?tipodoc=Ddlmess&leg=16&id=302496) delle direttive anti-discriminazione del 2000.  
Le novità approvate definitivamente nella seduta di ieri 4 giugno, sono molteplici:  
**•    MOLESTIE:** all’articolo 2, comma 3, del decr. 215 e parole: “umiliante e offensivo“ sono sostituite dalle seguenti: “umiliante od offensivo;  
**•    ONERE DELLA PROVA:** il comma 3 del decr. 215 è sostituito dal seguente:  
        “3\. Quando il ricorrente fornisce elementi di fatto, desunti anche da dati di carattere statistico, idonei a fondare, in termini precisi e concordanti, la presunzione dell’esistenza di atti, patti o comportamenti discriminatori, spetta al convenuto l’onere di provare l’insussistenza della discriminazione“;  
**•    PROTEZIONE DELLE VITTIME:** nuovo articolo Art. 4-bis aggiunto al decr. 215: “1. La tutela giurisdizionale di cui all’articolo 4 si applica altresì nei casi di comportamenti, trattamenti o altre conseguenze pregiudizievoli posti in essere o determinate, nei confronti della persona lesa da una discriminazione diretta o indiretta o di qualunque altra persona, quale reazione ad una qualsiasi attività diretta ad ottenere la parità di trattamento“;  
  
Più nutrite ancora le modifiche introdotte al decr. 216, tra cui si segnalano:  
**•    REQUISITI ESSENZIALI E DETERIMINANTI:** aggiunto il riferimento alla finalità legittima  
**•    FORZE ARMATE, SERVIZI DI POLIZIA, PENITENZIARI O DI SOCCORSO:** abrogato il riferimento all’idoneità allo svolgimento delle funzioni  
**•    ETA’:** specificati i casi in cui è ammessa la disparità di trattamento  
**•    ONERE DELLA PROVA:** nuova formulazione dell’art. 4, c. 4:     “4. Quando il ricorrente fornisce elementi di fatto idonei a fondare, in termini gravi, precisi e concordanti, la presunzione dell’esistenza di atti, patti o comportamenti discriminatori, spetta al convenuto l’onere di provare l’insussistenza della discriminazione“ – SI NOTI LA DIFFERENTE FORMULAZIONE RISPETTO AL TESTO DEL 215  
•    PROTEZIONE DELLE VITTIME: idem come sopra  
•    LEGITTIMAZIONE AD AGIRE: superato la restrizione posta alle sole organizzazioni sindacali, possono agire anche “le associazioni e le organizzazioni rappresentative del diritto o dell’interesse leso”  
  
Testo:  ed in allegato  
[Resoconti stenografici (1)](http://www.senato.it/japp/bgt/showdoc/frame.jsp?tipodoc=Resaula&leg=16&id=302472)  
  
[Resoconti stenografici (2)](http://www.senato.it/japp/bgt/showdoc/frame.jsp?tipodoc=Resaula&leg=16&id=302538)  
   
[Documento del Servizio studi del Senato, che riporta le censure della Commissione europea](http://www.senato.it/documenti/repository/dossier/studi/2008/Doss_010.pdf) (in particolare da p. 117 a p. 128)