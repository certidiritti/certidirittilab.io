---
title: 'IL GOVERNO INTERVENGA SULLA CRESCENTE OMOFOBIA VIOLENTA IN ITALIA'
date: Wed, 02 Sep 2009 08:53:28 +0000
draft: false
tags: [Comunicati stampa]
---

**BOMBE GAY STREET DI ROMA: IL GOVERNO AFFRONTI LA QUESTIONE DOMANI, ALCONSIGLIO DEI MINISTRI E RIFERISCA IN PARLAMENTO SU GRAVE FENOMENO OMOFOBIA A ROMA E IN ITALIA**

Dichiarazione di Sergio Rovasio, Segretario dell'Associazione Radicale Certi Diritti ed Enzo Cucco, Direttore della Fondazione Sandro Penna

"Il grave episodio avvenuto ieri notte alla gay street di Roma conferma quanto abbiamo purtroppo previsto riguardo l'aumento delle azioni violente di omofobia a Roma e in Italia. Dopo quanto avvenuto ieri notte, a pochi metri dal Colosseo, non è più possibile far finta di nulla. Il fatto che nel giro di pochi giorni si comincino a contare i feriti di questi atti violenti dovrebbe porre seri interrogativi a chi finora, per motivi clericali o ideologici, ha volutamente ignorato il grave fenomeno dell'omofobia.

Occorre che il Governo dia rassicurazioni urgenti alla Comunità lgbt italiana. La gravità del fenomeno, che rischia di alimentare paura e terrore, non solo nella comunità gay, merita un'adeguata risposta da parte delle autorità di Governo nazionale e locale oltre che delle forze dell'ordine e della magistratura. Chiediamo che il Governo affronti con rigore la questione al Consiglio dei Ministri previsto domani, giovedì 3 settembre, e riferisca in Parlamento sul grave fenomeno dell'omofobia che ha caratterizzato l'estate italiana e in particolare su quanto avvenuto a Roma in queste settimane.  
La questione del grave incremento della violenza omofobica non riguarda soltanto la sicurezza dei cittadini ma richiede urgenti interventi sul piano dell'informazione, dell'educazione e dell'aiuto alla comunità lgbt italiana".