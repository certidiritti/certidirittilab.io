---
title: 'Newsletter 13 Aprile'
date: Sat, 14 Apr 2012 07:09:59 +0000
draft: false
tags: [Politica]
---

![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)

Care e cari amici e sostenitori dell'Associazione Radicale Certi Diritti,

uno dei nostri impegni sul tema della lotta alla sessuofobia e a tutti i proibizionismi è la campagna per** la legalizzazione della prostituzione**. 

Sabato 21 aprile terremo a Roma, presso la Sala Di Liegro della Provincia di Roma (Via IV Novembre - P.za Venezia) una Conferenza Nazionale sulla Legalizzazione della prostituzione, promossa insieme al **Comitato per i Diritti Civili delle Prostitute** e alla **Cgil Nuovi Diritti**. Sarà una importante occasione di confronto con esperti e personalità non solo politiche, che affronterà i diversi aspetti del tema della prostituzione.  Sarà anche un'occasione per capire come meglio organizzare anche questo fronte di lotta e di impegno.

Ti invitiamo a partecipare e a estendere l'invito alle persone che possono essere interessate a questo argomento. 

Di seguito il programma dei lavori.

Facci sapere se potrai partecipare a [info@certidiritti.it](mailto:<a href=)" target="_blank" style="color: rgb(255, 102, 0); ">[info@certidiritti.it](mailto:info@certidiritti.it) 

**Yuri Guaiana, Segretario **  
**Enzo Cucco, Presidente**  
**Giacomo Cellottini, Tesoriere**

\- \- \- \- \- \- \- \- \- \- \- \- \- \- \-

**La Legalizzazione della Prostituzione**  
 Conferenza Nazionale   
**Programma**  
  

Ore 9,15        _Registrazione dei Partecipanti _

_Ore 9,30__Apertura dei Lavori_  

**Diritti e Rispetto delle Libertà Personali**  
_Presiede:_**Pia Covre****_, _**_Comitato per i Diritti Civili delle Prostitute_

**I Diritti Umani nella prostituzione**  
**            Matteo Mecacci**  
_                Presidente della Commissione Diritti Umani Osce, Deputato Radicale - Pd_  
              
**Prostituzione - Diritti Civili, Libertà, Autodeterminazione**  
**            Porpora Marcasciano**  
_                Presidente del Movimento Identità Transessuale_

**Immigrazione, Traffico di Esseri Umani, Impatto delle Politiche Migratorie**  
**            Diritti Umani, scenari delle prostituzioni e politiche pubbliche**  
**            Paola Degani**  
_                Dipartimento di Scienze Politiche, Giuridiche e Studi Internazionali,Università di Padova_  
  
**Prostituzione, una Storia infinita**  
**            Lasse Braun**  
_                Scrittore, regista_

**Costi Economici, Aspetti Legali e Fiscali nella Situazione Attuale**  
**e in una Prospettiva di Cambiamento**  
_Presiede:_** Maria Gigliola Toniollo, **_Nuovi Diritti Cgil Nazionale_        

**I costi Economici del Proibizionismo nella Prostituzione**   
**            Lilly Chiaromonte **  
_Federconsumatori_  
              
**I** C**osti Economici della Repressione nella Prostituzione **  
**            Gianni Meuti**  
_            Sindacato di Polizia Silp – Cgil Nazionale_  
           
**Aspetti Legali e Fiscali nella Prostituzione – Profili Penali**  
            **Alessandro Gerardi**  
_            Avvocato - Radicali Roma_  
**            Francesca Re**  
_            Avvocato - Phd Candidate Università di Roma Tor Vergata_

**Presentazione di uno studio economico nel Veneto**  
**Claudio Donadel**  
**_Coordinatore dell'Unità operativa per la Protezione Sociale e Umanitaria del Comune di Venezia_**

**_Dibattito_**  
13,30 - 14,30 Pausa Pranzo

_Proiezione di:_  
**"Ni Coupables ni Victimes" **  
_Videobox a cura di _[Betty@sexworkeurope](mailto:<a href=)" target="_blank" style="color: rgb(255, 102, 0); ">[Betty@sexworkeurope>  
_Creazione by Sexyshock in collaborazione con Icrse - International Committee on the Rights of Sex Workers in Europe_  
](mailto:Betty@sexworkeurope</a)[www.sexworkeurope.org](http://www.sexworkeurope.org/)[www.ecn.ong/sexyshock](http://www.ecn.ong/sexyshock)  
**_Special guests Scarlot Hariot & Wonder Bra_**

**Le Politiche Locali a Confronto **  
_Presiede: _**Enzo Cucco**_, Presidente dell'Associazione Radicale Certi Diritti_

**Progetto "Città e Prostituzione" del Comune di Venezia **  
**            Sandro Simionato**  
_            Vicesindaco del Comune di Venezia_      

**Le esperienze di Roma e di Milano**  
**            Riccardo Magi**  
_Segretario Radicali Roma_  
**            Giulia Crivellini**  
        _Comitato per la Delibera di Iniziativa Popolare al Comune di Milano su Legalizzazione_

**La parola a Sex-Worker, Clienti e Organizzazioni dei Servizi**  
**_Presiede: _****Sergio Rovasio****_, Associazione Radicale Certi Diritti_**

**La Parola alle e ai Sex-Worker – Tavola Rotonda**  
_             Rappresentanti dei collettivi e attivist*_      
**            Antonia Monopoli**  
_Peer Educator di Ala Milano Onlus_

**Testimonianze di Clienti di Prostitute e di Prostituti**  
**            Enrico Salvatori**  
            _Rappresentante Clienti _   

**Gli annunci via web**  
**            Antonio Nigrelli**  
_Piccoletrasgressioni_  
**            Maya Checchi**  
**_            Cybercore_**                    
** Dibattito**

_Parlamentari: _**Rita Bernardini, Vittoria Franco, Livia Turco**  
_Tutti i Parlamentari sono stati invitati_

**Verso il Cambiamento: Iniziative per la Legalizzazione e Proposte di Legge**

**Manifesto Appello per la Legalizzazione della Prostituzione**  
**            Enzo Cucco**  
_                Presidente Associazione Radicale Certi Diritti_  
                   
**E ora la Legge**  
_Concludono: _**Carla Corso e Marco Pannella**  
               
**  Sabato 21 aprile 2012 **  
**Palazzo Valentini - Sala Di Liegro **  
**via IV Novembre, 119/a**  
**Roma**