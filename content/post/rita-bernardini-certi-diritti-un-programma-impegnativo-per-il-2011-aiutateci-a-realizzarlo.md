---
title: 'Rita Bernardini. Certi Diritti: un programma impegnativo per il 2011. Aiutateci a realizzarlo!'
date: Thu, 06 Jan 2011 23:31:19 +0000
draft: false
tags: [Comunicati stampa]
---

Lo scorso 28 novembre sono stata eletta - e ne sono felicissima! _-_ Presidente dell'_Associazione Radicale Certi Diritti_.

Promuovere i diritti civili, per la responsabilità e la libertà sessuale delle persone, non è cosa facile in un Paese sessuofobico come il nostro. Le gerarchie vaticane (e politici compiacenti) sono arruolate in servizio permanente effettivo a spiare i cittadini dal buco della serratura per scoprirli peccatori da condannare e redimere, magari con qualche legge che faccia coincidere ciò che le loro gerarchie considerano “peccati” con reati puniti con la galera.

Vi scrivo per chiedervi di far parte, con la vostra iscrizione, all’_Associazione_. Ci aiuterete così a sostenere gli obiettivi del nostro “programma”, deciso e votato all'ultimo congresso: dal progetto di Riforma del Diritto di Famiglia alle unioni civili e al matrimonio gay; dalla lotta all’omofobia e alla transfobia alla prevenzione delle malattie sessualmente trasmissibili; dalla legalizzazione della prostituzione all’affermazione del diritto alla sessualità nei luoghi di privazione della libertà.

Vi chiedo di dare fiducia al lavoro importante e straordinario che molte persone svolgono volontariamente in tutta Italia per migliorare sul piano dei diritti questo nostro malandato paese. L’unica risorsa su cui possiamo contare, oltre alla passione e alle idee, è il contributo che ognuno di noi, con un piccolo sforzo, può dare.

Grazie per quanto potrete fare.

**Rita Bernardini**

deputata radicale (delegazione nel Gruppo parlamentare del _PD_ alla Camera)

Presidente _Associazione Radicale_ _**Certi Diritti**_

[**Qui**](iscrizioni-e-contributi) per iscrizioni e contributi