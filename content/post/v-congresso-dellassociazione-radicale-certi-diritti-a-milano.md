---
title: 'V Congresso dell''associazione radicale Certi Diritti a Milano'
date: Thu, 20 Oct 2011 04:49:55 +0000
draft: false
tags: [Politica]
---

 **PIU’ DIRITTI, PIU’ DEMOCRAZIA, PIU’ SVILUPPO**  
**Nuove alleanze contro vecchie discriminazioni  
  
**V Congresso dell’associazione radicale Certi Diritti  
2-4 dicembre 2011 MILANO

  
  
**I NUOVI ORGANI ELETTI >[  
](v-congresso-dellassociazione-radicale-certi-diritti-a-milano)**

**[http://www.certidiritti.it/conclusi-a-milano-i-lavori-del-v-congresso-dellassociazione-radicale-certi-diritti-eletti-i-nuovi-organi-statutari](conclusi-a-milano-i-lavori-del-v-congresso-dellassociazione-radicale-certi-diritti-eletti-i-nuovi-organi-statutari)**

**I LAVORI SONO APERTI A TUTTI (VOTANO GLI ISCRITTI)  
  
NELLA SESSIONE DI SABATO POMERIGGIO  
SARA' PRESENTE UN INTERPRETE LIS  
**

**2 dicembre Università degli Studi di Milano**

**facoltà di Giurisprudenza – aula 410, via Festa del Perdono, 7**  
  
**3-4 dicembre Residence San Vittore 49 - via San Vittore, 49**

**_il programma potrebbe subire modifiche  
  
_**

**Venerdì 2 dicembre**

**Ore 15.00: Università degli Studi di Milano - Via Festa del Perdono 7  
settore aule di Giurisprudenza - aula 410**

**La via giudiziaria delle coppie omosessuali che chiedono  
il riconoscimento dei loro diritti: un approccio di diritto comparato  
  
sullo stesso tema:  
[Scarica il numero monografico della rivista giuridica 'Ianus'  
sul same-sex marriage >   
 ](download/doc_download/56-monografia-sul-matrimonio-tra-persone-dello-stesso-sesso)**

**Interventi: **

**Prof.ssa Marilisa D’Amico, "Le coppie omosessuali nello spazio comune europeo"**

**Prof. Carlo Rimini, "La disciplina della convivenza omosessuale: questioni di metodo fra modello francese e modello tedesco"**

**Dott. Cesare Pitea, “La giurisprudenza della Corte europea dei diritti umani”**

**Avv. Ugo Millul, "Diritti fondamentali in Europa e negati in Italia"**

**Avv. Livio Scaffidi, "Il riconoscimento delle coppie same-sex: aspetti transfrontalieri"**

**Ore 18.00: tavola rotonda**

**La parola alle coppie  
Con Paola Concia, Sergio Lo Giudice, Enrico Oliari, Claudia Mameli e Gian Mario Felicetti.  
Modera Sergio Rovasio.  
**

**Ore 19.30: riunione del direttivo - via Marino 7, sala riunioni n. 321 al 3° piano.  
**

**Sabato 3 dicembre**

  
  
**Residence San Vittore** (Via San Vittore, 49 - Milano)

Ore 9.30Apertura del congresso**relazioni organi statutari:  
Rita Bernardini:** Presidente**; Sergio Rovasio** - Segretario; **Giacomo Cellottini** - Tesoriere

**Saluti istituzionali:**  
**Anita Sonego**, Presidente Commissione Pari Opportunità del comune di Milano  
**Marilisa D'Amico**, Presidente Commissione Affari Istituzionali del comune di Milano  
**Marco Cappato**, dirigente Radicale, consigliere comunale Milano  
**Fabio Arrigoni**, Presidente del Consiglio di zona 1 del comune di Milano  
**Stefano Chirico,** Vice Questore aggiunto, rappresentante OSCAD - Osservatorio per la Sicurezza contro gli atti Discriminatori della Polizia di Stato.

Invitati: **tutti i dirigenti e parlamentari radicali. **  
  
Hanno confermato la loro partecipazione:

**Mario Staderini**, Segretario di Radicali Italiani;**Ivan Scalfarotto**, Vicepresidente PD - **Paolo Patanè**, Presidente Arcigay - **Paola Concia**, deputata PD \- **Franco Grillini**, responsabile nazionale Diritti Civili IDV - **Massimiliano Monnanni**, Direttore Generale dell'Unar, **Enrico Oliari**, Presidente GayLib - **Aurelio Mancuso**, Presidente Equality - **Rita De Santis**, Presidente Agedo - **Alexander Schuster**, staff legale di Famiglie Arcobaleno - Coordinamento Arcobaleno Milano - **Mina Welby;** Associazione Luca Coscioni; **Flavio Koea**, Presidente di ASSOSEX; **Augusto Pistilli**, Presidente Federsex Italia.

  
   
Ore 11.00  
Relazioni  
  
**Matrimonio tra persone dello stesso sesso: le cause pilota**  
Massimo Clara – Gian Mario Felicetti  
  
**Via legale e Soccorso Civile**  
Avv. Filomena Gallo, Segretaria Associazione Coscioni  
  
  
**La difesa del diritto all’orientamento sessuale in Europa e nel mondo  
**Renato Sabbadini, Segretario genereale Ilga - Bruxelles;  
Ottavio Marzocchi, funzionario al Parlamento Europeo - Bruxelles;

**Riforma del diritto di famiglia e convivenze  
**Bruno De Filippis, giurista

**Uganda, la situazione nel paese: dall'assassinio di David Kato Kisule  
all'odio fondamentalista di Stato  
John Francis Onyango**, avvocato di David Kato Kisule - Kampala, Uganda;  
Stefano Fabeni, Heartland Alliance - Washington.  
  
**Proibizionismo sulla prostituzione: i danni al paese, all’economia  
e alle lavoratrici e lavoratori del sesso  
**Pia Covre, Presidente del comitato per i diritti civili delle prostitute  
Sergio Rovasio, lotta al proibizionismo sulla prostituzione, idee e proposte  
  
**Intersex e diritti negati: quali battaglie comuni con il movimento GLBT(E) in Italia?**  
Michela Balocchi, docente Università di Firenze e Consultorio TransGenere

Ore 12,45: Commemorazione di **David Kato Kisule** e consegna all'Associazione Radicale Certi Diritti del premio a lui dedicato (On. Rita Bernardini, Paolo Patanè, Jhon Francis Onyango, Elio Polizzotto). 

Ore 13.00  
pausa pranzo  
   
Ore 14.00  
**Alleanza per i diritti e contro le discriminazioni: perché e come**

Introduzione

**Rita Bernardini**, Presidente;  
**Massimiliano Monnanni**, direttore UNAR

Partecipano

**CGIL - Settore Nuovi Diritti** (Gigliola Toniollo)  
**ALA MILANO ONLUS** (Vincenzo Cristiano e Antonia Monopoli)  
**ASGI** (Livio Neri)  
**ASS. VITA INDIPENDENTE** (Ida Sala)

Coordina **Enzo Cucco**

Ore 16.00  
Dibattito generale

Ore 19  
**Quando i diritti si fanno cultura**  
Elio De Capitani e Ida Marinelli

Ore 19.30  
Proiezione **'Fuori! - Storia del primo movimento omosessuale in Italia (1971-2011)'  
**introdotto da Angelo Pezzana.a seguire **"Fuori dal video e sulle Ali del Pegaso"** di Enrico Salvatori (13')

  
**Domenica 4 dicembre**

Ore 9.30  
ripresa dibattito generale

Ore 11.00    
**'Milano deve essere il cambiamento che vogliamo per tutto il paese:**  
**Diritti e sviluppo nelle aree metropolitane':**

Confronto tra, **Stefano Boeri, Pierfrancesco Majorino, Marco Cappato, Ivan Scalfarotto**

Ore 13.00  
Pausa pranzo (buffet all’interno della sede del congresso)

Ore 14.00  
Votazione documenti (mozioni, mozioni particolari) e organi del Congresso

**INFORMAZIONI LOGISTICHE**  
Per gli studenti e i senza reddito stiamo organizzando sistemazioni presso amici e compagni di Milano. Invitiamo coloro che vivono a Milano a darci disponibilità ad ospitare i congressisti e chi è di fuori a segnalarci tale necessità all’indirizzo e-mail: **[leonardo.monaco@myself.com](mailto:leonardo.monaco@myself.com)**

Per il pernottamento a Milano Certi Diritti consiglia:

\- **Hotel San Guido** (3 stelle) - convenzione per congressisti di Certi Diritti   
Via Carlo Farini, 1 – Milano         Tel: 02 6552261  
costo singola: 40 euro costo doppia: 60 euro

\- **Ostello Bello**  
Via Medici, 4 – Milano        Tel: 02 36582720  
posto letto a partire da 28 euro

per informazioni scrivi a **info@certidiritti.it  
 **

[**COSA FARE A MILANO LA SERA DOPO IL CONGRESSO? ECCO ALCUNE PROPOSTE:**](cosa-fare-a-milano-la-sera-dopo-il-congresso-ecco-alcune-proposte)