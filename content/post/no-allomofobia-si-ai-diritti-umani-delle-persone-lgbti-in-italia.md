---
title: 'No all''omofobia, sì ai diritti umani delle persone lgbti in Italia'
date: Sat, 09 Jun 2012 08:25:29 +0000
draft: false
tags: [Politica]
---

###   
Data di pubblicazione dell'appello: 07.06.2012

#### Status dell'appello: aperto

Amnesty International Italia prenderà parte al **Bologna Pride 2012 di sabato 9 giugno**, aderendo alla modalità di svolgimento, solidale **con le popolazioni colpite dal terremo**to, proposta dal comitato organizzatore della manifestazione. **[PER SAPERNE DI PIU'](http://www.amnesty.it/bolognapride)**

Come accade purtroppo in diverse parti del mondo e dell’Europa, anche in Italia i diritti delle persone lesbiche, gay, bisessuali, transgender e intersessuate (Lgbti) sono messi a rischio dalla discriminazione e da altri fattori, legislativi, sociali e culturali. Le istituzioni hanno l’obbligo di prevenire e contrastare la discriminazione, perché qualsiasi eccezione all’universalità dei diritti umani è inaccettabile.

È necessario che le autorità italiane assicurino alle persone Lgbti il diritto a esprimere la loro identità di genere e il loro orientamento sessuale, il diritto a una vita affettiva e familiare libera da interferenze e un equo accesso a tutti i diritti umani riconosciuti dalle convenzioni e dagli standard internazionali.

Negli ultimi anni, attacchi verbali e fisici nei confronti delle persone Lgbti si sono verificati con preoccupante frequenza, mentre diversi esponenti politici e istituzionali hanno continuato a fomentare un clima di intolleranza e di odio verso le persone Lgbti con dichiarazioni palesemente discriminatorie.

La legge penale italiana antidiscriminazione prevede pene aggravate per crimini di odio basati sull’etnia, razza, nazionalità, lingua o religione, ma non tratta allo stesso modo quelli motivati da finalità di discriminazione per l’orientamento sessuale e l’identità di genere (c.d. legge Mancino-Reale, n. 654 del 1975, come modificata e integrata dal decreto legge n. 122 del 1993 e successive modificazioni). A causa di questa lacuna, le persone che subiscono discriminazione, odio e violenza a causa del loro orientamento sessuale e della loro identità di genere non hanno la stessa tutela garantita alle vittime di reati motivati da altri fattori che la legge identifica come discriminatori; per lo stesso motivo, l’incitamento verbale all’omofobia e alla transfobia, i cui casi purtroppo non sono mancati in questi anni, non è perseguibile come crimine motivato da odio.

Questa lacuna è in contrasto con la legislazione internazionale ed europea sulla discriminazione e rischia di favorire l’aumento di atteggiamenti di intolleranza e violenza verso le persone Lgbti. Il principio di non discriminazione, sancito dall’art. 21 della Carta dei diritti fondamentali dell’Unione europea, trattato vincolante per l’Italia, garantisce parità di trattamento tra le persone e stabilisce il divieto di qualsiasi forma di discriminazione basata su sesso, razza o origine etnica, lingua, religione o credo, disabilità, età e orientamento sessuale.

Ciononostante, nel luglio 2011, come già accaduto nel 2009, il parlamento italiano ha respinto proposte di legge tese a colmare questa lacuna.

Inoltre, nella legislazione italiana manca qualsiasi riconoscimento della rilevanza sociale delle famiglie costituite da persone dello stesso sesso - alle quali non viene consentito di sposarsi - e dai loro figli. In tutto il mondo, Amnesty International si oppone alla discriminazione fondata sull'orientamento sessuale o sull'identità di genere nell’accesso al matrimonio civile e chiede agli stati che riconoscano diritti anche alle famiglie di fatto e alle unioni formate all’estero sulla base delle leggi locali. Negare il riconoscimento alle coppie omosessuali impedisce a molte persone di godere di tutta una serie di diritti, necessari per l’autorealizzazione, e alimenta ulteriormente la stigmatizzazione, la discriminazione e gli abusi nei confronti delle persone Lgbti. 

Le autorità italiane hanno la responsabilità di proteggere e garantire la realizzazione dei diritti umani delle persone Lgbti affinché esse non siano vittime di discriminazione, possano godere degli stessi diritti di ogni individuo e possano esprimere liberamente il loro orientamento sessuale e identità di genere senza il rischio di subire altre violazioni e abusi dei loro diritti umani.

[![](http://www.amnesty.it/flex/TemplatesUSR/Site/IT/TemplatesUSR-Site-img/Oggetti/firma-subito-appello.png "Firma subito l'appello")](http://www.amnesty.it/flex/FixedPages/IT/appelliForm.php/L/IT/ca/229)