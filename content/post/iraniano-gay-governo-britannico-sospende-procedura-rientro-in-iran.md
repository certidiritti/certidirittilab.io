---
title: 'IRANIANO GAY: GOVERNO BRITANNICO SOSPENDE PROCEDURA RIENTRO IN IRAN'
date: Thu, 27 Mar 2008 20:14:03 +0000
draft: false
tags: [Comunicati stampa, GAY, GOVERNO BRITANNICO, IRAN, IRANIANO]
---

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

“ Il Ministro degli Interni britannico, Jacqui Smith, ha deciso oggi di sospendere la procedura che prevede la deportazione in Iran del ragazzo gay Medhi Kazemi dove lo attendeva la pena di morte. Quanto avvenuto oggi è il risultato della mobilitazione internazionale che ha visto in prima linea le Associazioni Nessuno Tocchi Caino, il Gruppo EveryOne, il Partito Radicale Nonviolento e l’Associaizone radicale Certi Diritti insieme a molte associazioni lgbt europee, media e network internazionali, in particolare la stampa di Spagna, Italia, Olanda e Gran Bretagna, che lottano per la libertà e la democrazia.

Non possiamo che esprimere al Governo britannico i nostri più sentiti ringraziamenti per la scelta di salvare la vita a Medhi. Occorre ora garantire, e su questo vigileremo, che in tutti i paesi dell’Unione Europea venga applicata la Direttiva 2004/83/CE che impone il riconoscimento dello status di rifugiato anche alle persone perseguitate nel loro paese di origine a causa del loro orientamento sessuale”.