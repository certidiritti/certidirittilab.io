---
title: 'DIRITTI DELLA PERSONA E LAICITA'', A ROMA IL 3 FEBBRAIO'
date: Tue, 27 Jan 2009 07:59:01 +0000
draft: false
tags: [Comunicati stampa]
---

Diritti della persona e laicità
-------------------------------

**Martedì 3 febbraio 2009, ore 15.00**

_Diritti fondamentali: dalla Carta di Nizza alla realtà italiana_

Stefano Rodotà

_La natura ambigua del naturale_

Chiara Lalli

_Cittadinanza, legge, affetti_

Vittorio Lingiardi

Interventi programmati

Dibattito

Visto il recente dibattito sviluppatosi dopo la sentenza Englaro e la pubblicazione recente del libro “_Le unioni tra persone dello stesso sesso. Profili di diritto civile, comunitario e comparato_” (a cura di Francesco Bilotta, Mimesis edizioni, Milano 2008) si rende necessario un approfondimento sul tema della libertà e dell’autodeterminazione. Evidentemente i due ambiti sono molto diversi tra loro ma hanno un elemento comune, ovvero il rapporto tra libertà della persona e laicità dello Stato. Da qui la necessità di indagare quali sono i limiti e quali sono i doveri del legislatore al riguardo, anche alla luce del recentissimo richiamo del Presidente della Corte costituzionale.

Sala conferenze Fondazione Basso

Via della Dogana Vecchia, 5 – Roma

Tel. 066879953 – fax 0668307516

[basso@fondazionebasso.it](mailto:basso@fondazionebasso.it) – [www.fondazionebasso.it](http://www.fondazionebasso.it/)