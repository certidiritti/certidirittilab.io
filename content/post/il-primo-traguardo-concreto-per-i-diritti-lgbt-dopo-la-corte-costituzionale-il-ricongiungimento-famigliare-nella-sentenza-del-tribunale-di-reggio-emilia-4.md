---
title: 'IL PRIMO TRAGUARDO CONCRETO PER I DIRITTI LGBT DOPO LA CORTE COSTITUZIONALE: IL RICONGIUNGIMENTO FAMIGLIARE  NELLA SENTENZA DEL TRIBUNALE DI REGGIO EMILIA'
date: Sun, 25 Mar 2012 09:28:52 +0000
draft: false
tags: [Diritto di Famiglia]
---

Grazie alle iniziative di Affermazione Civile è stata ottenuta una conquista concreta per i diritti delle coppie conviventi omosessuali, la prima dopo la sentenza della Corte Costituzionale.

E’ il caso di una coppia italo uruguayana residente a Reggio Emilia, che si era sposata in Spagna. Il loro matrimonio non è stato nè trascritto, nè riconosciuto in Italia. Tuttavia, al ricorrente, cittadino Uruguaiano, è stato riconosciuto il diritto a godere di un permesso di soggiorno per motivi famigliari, quindi è stato riconosciuto il suo status di coniuge, per quanto ai soli fini del diritto di libera circolazione (e non nel contesto del diritto di famiglia).

Alcune considerazioni importanti:

1)**Questa sentenza è esecutiva**, e ad oggi il ricorrente ha già ottenuto dalla questura il permesso di soggiorno CE per soggiornanti di lungo periodo, poiché ha potuto presentare l’ordinanza del giudice.

2) Per ora è una sentenza di primo grado.**Se non verrà impugnata dall'Avvocatura dello Stato, diverrà definitiva**, e il cittadino Uruguayano sarà libero di vivere in Italia insieme al coniuge italiano. Se invece verrà impugnata, dovrà essere affrontato il secondo grado di giudizio.

3)**L'ordinanza ha efficacia solo per la coppia ricorrente**, quindi non ne beneficieranno automaticamente tutte le coppie che si trovano nella situazione analoga . Tuttavia costituisce un precedente favorevole a cui le Questure, titolate al rilascio dei Permessi di soggiorno, potrebbero adeguarsi e nel caso non lo facessero, si potrebbero comunque impugnare i dinieghi e ricorrere di nuovo al tribunale

4) In questa occasione il Giudice di Reggio Emilia ha nelle motivazioni date all'accoglimento del ricorso, fatto ampiamente riferimento alla sentenza 138/2010 della Corte Costituzionale che ha affermato il diritto per la coppia dello stesso sesso ad avere una vita famigliare, tale principio è stato ribadito nella recentissima sentenza 4184 della Cassazione.

Purtroppo, quindi, siamo ancora nella condizione in cui ogni coppia potrebbe aver bisogno di rivolgersi al giudice per ottenere giustizia.

Noi ci auguriamo che molte coppie dello stesso sesso sposate o unite civilmente all'estero presentino alle Questure la richiesta di permesso di soggiorno per i/le compagni/e di origine extra UE e che siano titolari di un soggiorno nel Paese dove si è svolto il matrimonio o stipulata l'unione civile, queste richieste se accolte, consolideranno questo risultato.

Naturalmente l'associazione radicale Certi Diritti è disponibile a seguire le coppie nel loro percorso e a supportarle in caso di nuovi ricorsi.  

Chiediamo anche di avere notizia rispetto i risultati ottenuti da coppie che non desiderano avere il nostro aiuto perchè è importantissimo aver un quadro delle risposte date dalle Questure.

  
[Sentenza Tribuanale Reggio Emilia](http://www.certidiritti.org/wp-content/uploads/2012/03/sentenza_senza_dati__1.pdf)