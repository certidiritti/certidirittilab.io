---
title: 'CONFERENZA STAMPA DEL COMITATO SI LO VOGLIO'
date: Wed, 24 Mar 2010 10:59:11 +0000
draft: false
tags: [Comunicati stampa]
---

Il Comitato nazionale "Sì lo voglio" incontra i giornalisti per commentare la decisione della Corte costituzionale sui matrimoni tra persone dello stesso genere, domani mercoledì 24 marzo alle ore 12:00 presso Hotel Nazionale, Piazza di Montecitorio.

I coordinatori del Comitato nazionale:

Imma Battaglia (335.5845313)  
Maurizio Cecconi ( 349.8084899)  
Enzo Cucco (347.0431401)  
Paolo Patanè ( 349.8845809)  
Francesca Polo ( 339. 5765311)