---
title: 'UGANDA: ATTIVISTI LGBTI DENUNCIANO IL GIORNALE ROLLING STONE. L’ALTA CORTE UGANDESE HA EMESSO UN ORDINE PROVVISORIO DI CESSAZIONE DELLE PUBBLICAZIONI'
date: Wed, 10 Nov 2010 15:16:21 +0000
draft: false
tags: [Comunicati stampa]
---

Comunicato stampa congiunto delle associazioni radicali Non c’è Pace Senza Giustizia (NPWJ/NPSG) e Certi Diritti, del Partito Radicale Nonviolento Transnazionale (PRNTT) e di Sexual Minorities Uganda (SMUG)

Bruxelles-Roma-Kampala, 10 novembre 2010

Il primo Novembre 2010, il settimanale ugandese “Rolling Stone” ha pubblicato  un articolo dal titolo “Gli Uomini della Vergogna parte II” in cui sono contenuti nomi e foto di 10 presunti gay. L'articolo è il seguito dell’infame articolo pubblicato il 2 Ottobre 2010, in cui si chiedeva ai lettori di “Impiccare gli omosessuali” in Uganda. L’uscita del primo articolo del settimanale ha causato l'aggressione di 8 persone i cui nomi e foto erano contenuti nell'articolo, mentre una donna è stata costretta ad abbandonare la propria casa dopo che i vicini l'avevano colpita con una raffica di pietre.

In seguito ad una causa civile intentata contro il giornale da organizzazioni della società civile in Uganda, la Corte Suprema dell'Uganda ha emesso, il primo Novembre 2010, un ordine provvisorio che intima al “Rolling Stone” di cessare qualsiasi pubblicazione che identifichi con nomi, foto o altri dati rilevanti, persone riconosciute come gay, lesbiche o omosessuali in generale, un'ulteriore udienza è stata fissata per il 23 Novembre 2010.

Il 13 Ottobre 2010, la Commissione delle Nazioni Unite sull'Eliminazione della Discriminazione contro le Donne (CEDAW) ha espresso gravi perplessità riguardo all’omofobica campagna mediatica e alle diffuse violazioni dei diritti umani che devono affrontare le persone LGBTI in Uganda, da quando,un anno fa, al Parlamento ugandese è stato proposto un progetto di legge Anti-omosessualità. Se approvata, quella legge introdurrà l'ergastolo e la pena di morte per atti omosessuali.

La delegazione ugandese con a capo Rukia Isanga Nakadama, Ministro dello Stato per gli affari Culturali e di Genere, ha tentato di respingere le responsabilità del Governo ugandese, in quanto il disegno di legge è stato proposto da un singolo parlamentare e non è stato sostenuto da nessun esponente del Governo. Allo stesso modo, l'Uganda ha declinato qualsiasi responsabilità per gli articoli pubblicati dal giornale “Rolling Stone”, sostenendo che si tratta di una questione di libertà di espressione e che la polizia sarà responsabile nell'occuparsi delle conseguenze che questo fatto potrà suscitare. Tuttavia,la Commissione delle Nazioni Unite non ha considerato convincenti queste affermazioni,sostenendo che questa sia una situazione inaccettabile e un caso di incitamento alla violenza.

Le associazioni radicali Non c’è Pace Senza Giustizia (NPWJ/NPSG) e Certi Diritti, il Partito Radicale Nonviolento Transnazionale (PRNTT) e Sexual Minorities Uganda (SMUG) accogliendo in maniera favorevole la decisione della Corte Suprema dell'Uganda di emettere un ordine provvisorio di cessazione di pubblicazioni da parte del giornale “Rolling Stones” in attesa dell'udienza del 23 Novembre 2010, invitano la Corte Suprema a difendere lo stato di diritto ed a rimarcare il fatto che esiste una netta differenza tra libertà di espressione e il crimine di incitamento all'odio.

NPWJ/NPSG, L'Associazione radicale Certi Diritti, PRNTT e SMUG invitano il Governo ugandese a prendere adeguati provvedimenti per assicurare il pieno rispetto dei diritti umani fondamentali ed un'adeguata protezione da atti di violenza alle persone che subiscono discriminazioni basate sull'orientamento sessuale. Ciò è particolarmente urgente per coloro che hanno deciso di non rimanere in silenzio di fronte a questo nuovo episodio di persecuzione e discriminazione e che hanno preso parte all'azione civile nelle corti contro il giornale “Rolling Stone”. Questi difensori dei diritti civili dovrebbero essere considerati dei campioni dello stato di diritto, che hanno preso questa decisione con grosso rischio personale ma per il bene dei cittadini ugandesi.

Per ulteriori informazioni si prega di contattare Elio Polizzotto (NPWJ/NPSG), email:[epolizzotto@npwj.org](http://us.mc308.mail.yahoo.com/mc/compose?to=epolizzotto@npwj.org)  telefono:+32 2 548 39 21 e l'Advocacy Litigation Officer (SMUG) telefono:+ 256 773104971.