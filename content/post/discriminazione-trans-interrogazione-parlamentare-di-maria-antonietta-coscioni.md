---
title: 'DISCRIMINAZIONE TRANS: INTERROGAZIONE PARLAMENTARE DI MARIA ANTONIETTA COSCIONI'
date: Fri, 15 Oct 2010 06:36:10 +0000
draft: false
tags: [Comunicati stampa]
---

La deputata radicale **Maria Antonietta Farina Coscioni**, iscritta a Certi Diritti, ha presentato alla Camera un'interrogazione parlamentare sul un caso di discriminazione nei confronti di una persona transessuale.

Dal settimanale "**L'Espresso**" del 14 ottobre 2010.

**MOBBING COL TRUCCO**

La deputata radicale Maria Antonietta Coscioni, iscritta a Certi Diritti, ha denunciato in Parlamento la discriminazione subita da una truccatrice televisiva, allontanata dal suo posto di lavoro perchè transessuale.

La trasmissione "Io canto", in onda su Canale 5, ha ritenuto infatti, secondo la parlamentare, che la sua presenza potesse turbare i bambini che dovevano partecipare ed esibirsi. L'accaduto, per la Coscioni, riafferma la necessità di maggiori tutele per i lavoratori vittime di discriminazioni. Da quì la sua richiesta al governo di verificare l'accaduto e di intervenire con un sistema di norme che garantiscano sul lavoro un effettiva parità di trattamento fra le persone.

> Atto Camera  
>   
> Interrogazione a risposta scritta 4-08646 presentata da
> 
> MARIA ANTONIETTA FARINA COSCIONI  
> lunedì 20 settembre 2010, seduta n.370
> 
>   
> FARINA COSCIONI, MAURIZIO TURCO, BELTRANDI, BERNARDINI, MECACCI e ZAMPARUTTI. -
> 
> Al Ministro del lavoro e delle politiche sociali, al Ministro per le pari opportunità.
> 
> \- Per sapere - premesso che:  
>   
> numerosi organi di stampa e in particolare dall'emittente radio «RMC» si è riferito del caso di una truccatrice, Andrea D., milanese, 38 anni, spostata dalla trasmissione «Io canto», in onda su Canale 5, condotta da Alfonso Signorini, perché transessuale;  
>   
> secondo il racconto riferito dallo stesso Signorini, Andrea D. avrebbe confidato: «Siamo professionisti come tutti gli altri e meritiamo rispetto», ha detto la truccatrice trans, «faccio questo lavoro da tanti anni e non mi era mai capitato un episodio del genere. Queste sono cattiverie gratuite, che calpestano la dignità delle persone. E importante capire che noi siamo gente che desidera avere una vita normale e che si impegna e lavora tanto quanto gli altri. Ho già truccato bambini per altre trasmissioni e nessuno di loro mi ha mai fatto domande imbarazzanti, anche perché alla loro età non hanno la malizia per pensare che una persona abbia intrapreso un diverso percorso sessuale. Mi sono sempre trovata bene con loro»;  
>   
> l'episodio sembra configurare un inaccettabile e odioso caso di discriminazione -:  
>   
> se quanto sopra esposto corrisponda a verità, e in particolare se la signora Andrea D, sia effettivamente stata allontanata dalla trasmissione «Io canto» perché transessuale e in tal caso quali iniziative di competenza intendano assumere. (4-08646)