---
title: 'Logistica VIII Congresso'
date: Thu, 04 Sep 2014 17:38:16 +0000
draft: false
---

L’ottavo Congresso dell’Associazione Radicale Certi Diritti si svolgerà presso il **Grand Hotel Lamezia**

Piazza Lamezia – Sant’Eufemia di Lamezia Terme (Cz)

http://www.grandhotellamezia.it/

Tel: 0968.53021

L’hotel è situato **di fronte la stazione ferroviaria centrale di Lamezia Terme** a 5 minuti dallo svincolo autostradale di Lamezia Terme sull’A3 Salerno-Reggio Calabria e **a 5 minuti dall’aeroporto internazionale di Lamezia Terme**. [![map](http://www.certidiritti.org/wp-content/uploads/2014/07/map1.png)](http://www.certidiritti.org/wp-content/uploads/2014/07/map1.png) Dall’aeroporto c’è un **servizio di S[huttle](http://www.certidiritti.org/wp-content/uploads/2014/07/Shuttle-aeroporto-stazione-FS-Lamezia-Terme.pdf) dalle ore 5.45 alle ore 23.30 con una corsa ogni mezz’ora che collega l’aeroporto alla stazione FFSS**. Le compagnie che volano da e per l’aeroporto di Lamezia Terme sono: **Alitalia, Ryanair e Easyjet**; che **collegano direttamente Roma Fiumicino, Milano Linate e Malpensa, Torino, Pisa, Bologna**. Per chi intendesse noleggiare un'automobile consigliamo [**autoeurope.it**](http://www.autoeurope.it/) [![linea separazione](http://www.certidiritti.org/wp-content/uploads/2014/07/linea-separazione.png)](http://www.certidiritti.org/wp-content/uploads/2014/07/linea-separazione.png)

**Hotel**

**\*\*\*\***

**Grand Hotel Lamezia** (sede congressuale)

70,00 € per camera doppia per notte con prima colazione, 60,00 € per la doppia ad uso singolo (al momento della prenotazione specificare la partecipazione al Congresso dell'Associazione Radicale Certi Diritti per usufruire della convenzione)

\*\*\*

**Hotel Morgan** (a 50 metri dalla sede del congresso) 3 stelle Via M. L. King Sant'Eufemia Lamezia Terme (Cz) 0968/53094 [info@hotelmorgan.it](mailto:info@hotelmorgan.it) [http://www.hotelmorgan.it/](http://www.hotelmorgan.it/) Camera singola per notte con prima colazione € 55,00 Camera doppia per notte con prima colazione € 75,00 Camera tripla per notte con prima colazione € 90,00

**

**Piccolo Hotel** (a 200 metri dalla sede del congresso) 2 stelle Via del Mare Sant'Eufemia Lamezia Terme (Cz) 0968/411212 [piccolohotel@hotmail.com](mailto:piccolohotel@hotmail.com) Camera singola per notte con prima colazione € 40,00 Camera doppia per notte con prima colazione € 55,00 Camera tripla per notte con prima colazione € 70,00 Camera quadrupla per notte con prima colazione € 90,00