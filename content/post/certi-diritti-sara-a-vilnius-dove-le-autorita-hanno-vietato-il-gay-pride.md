---
title: 'Certi Diritti sarà a Vilnius dove le autorità hanno vietato il Gay Pride'
date: Thu, 06 May 2010 10:18:54 +0000
draft: false
tags: [Comunicati stampa]
---

**ASSOCIAZIONE RADICALE CERTI DIRITTI MANIFESTERA' A VILNIUS IN LITUANIA PER I DIRITTI FONDAMENTALI E CON CARTELLI FAVOREVOLI AL MATRIMONIONO GAY - NOSTANTE LA PRETESTUOSA REVOCA DELL'AUTORIZZAZIONE PER LA MARCIA DELL'8 MAGGIO ED I DIVIETI DELLA LEGGE LITUANA SULLA PROTEZIONE DEI MINORI**

"Nonostante la sospensione dell'autorizzazione per il Pride Baltico di Vilnius previsto per l'8 maggio, decisa ieri da un Tribunale amministrativo su istanza del Procuratore Generale e sulla base di presunti rischi di turbativa dell'ordine pubblico, i militanti di Certi Diritti manifesteranno a Vilnius per la libertà di espressione, d'assemblea, di manifestazione e per l'uguaglianza e la lotta alle discriminazioni. L'Associazione Radicale Certi Diritti esporrà anche cartelloni favorevoli al matrimonio gay in Lituania, Italia ed in Europa, atto potenzialmente punito dalla contestata "legge sulla protezione dei minori dalle informazioni nocive", che ricomprende anche la promozione di informazioni pubbliche favorevoli ad un concetto di famiglia diverso da quello (eterosessuale) previsto dalla Costituzione lituana. Lo annuncia la delegazione radicale di Certi Diritti, composta da Ottavio Marzocchi, responsabile per le questioni europee, e da Joaquin Nogueroles Garcia, membro spagnolo di Certi Diritti".