---
title: 'FIACCOLATA A ROMA CONTRO VIOLENZA E INTOLLERANZA: ADESIONI RADICALI'
date: Thu, 24 Sep 2009 08:34:47 +0000
draft: false
tags: [Senza categoria]
---

FIACCOLATA A ROMA CONTRO VIOLENZA E INTOLLERANZA: L’ADESIONE E PARTECIPAZIONE DI CERTI DIRITTI, RADICALI ROMA, COMITATO ERNESTO NATHAN.

OCCORRONO CAMPAGNE DI INFORMAZIONE E DI EDUCAZIONE.  

Roma, 24 settembre 2009

La Regione Lazio, la Provincia e il Comune di Roma promuovono congiuntamente la fiaccolata che oggi, giovedì 24 settembre, a partire dalle ore 19.00, vedrà sfilare migliaia di cittadini da Piazza SS. Apostoli, fino al Campidoglio. Alla sfilata parteciperanno, insieme ai presidenti Marrazzo e Zingaretti, al sindaco Alemanno e a molti rappresentanti della società civile, le associazioni lesbiche, gay e transessuali, chiamate a testimoniare i tremendi episodi di violenza omo e transfobica di questi giorni.

L’Associazione radicale Certi Diritti, Radicali Roma e il Comitato Ernesto Nathan aderiscono e partecipano alla manifestazione con le proprie bandiere. Sappiamo bene che il semplice aggravamento delle pene per i reati di omo e transfobia non bastano. Non può essere infatti sufficiente inasprire le condanne in un paese come questo, dove non vi è certezza della pena, dove comunque dimostrare l'aggravante omofobica sarebbe difficilissimo, anche alla luce di quanto avvenuto di recente, e dove difficilmente si arriva, in caso di querela, al “coming out” della parte lesa.

Sarebbe molto più importante, accanto alle iniziative legali, promuovere campagne serie di informazione rivolte ai cittadini, ai giovani, nelle scuole di ogni livello, con iniziative mirate, come si sta già facendo nel caso dello stalking.

Soltanto così potremmo educare i cittadini di oggi e di domani al vero rispetto delle differenze e delle diversità tutte, e non a quella ipocrita tolleranza che spesso è in realtà vera intolleranza.

Negare e opporsi al riconoscimento di diritti civili e spazi di libertà per le persone e le coppie del mondo lesbico, gay, bisex, trans ed intersessuale è già di per sé un atto che induce all’omofobia e alla transfobia. Invitiamo le istituzioni a riflettere e ad adeguarsi a quegli standard minimi di diritti civili ormai raggiunti da quasi tutti paesi europei.

Luca (LUCKY) Amato, _Certi Diritti Roma_

Demetrio Bacaro, _Radicali Roma_

Massimiliano Iervolino, _Comitato Ernesto Nathan_