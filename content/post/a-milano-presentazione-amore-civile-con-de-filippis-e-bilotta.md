---
title: 'A MILANO PRESENTAZIONE ''AMORE CIVILE'' CON DE FILIPPIS E BILOTTA'
date: Thu, 31 Dec 2009 09:03:19 +0000
draft: false
tags: [Comunicati stampa]
---

**Organizzatore:** CERTI DIRITTI Milano  
**Data:** venerdì 15 gennaio 2010  
**Ora:** 16.30 - 19.30  
**Luogo:** Università degli Studi di Milano - Facoltà di Scienze Politiche  
**Indirizzo:** via Conservatorio, 7 - Aula 13  
**Città/Paese:** Milan, Italy

L’Associazione radicale Certi Diritti organizza la presentazione del progetto di riforma del diritto di famiglia:

  
«Amore civile. Dal diritto della tradizione al diritto della ragione»  
a cura di Bruno De Filippis e Francesco Bilotta  
  
Venerdì 15 gennaio 2010 alle ore 16.30  
Aula 13, Facoltà di Scienze politiche di Milano, via Conservatorio, 7.  
  
  
Interverranno Elio De Capitani, attore e regista, e Ida Marinelli, attrice, che fin dalla fondazione del Teatro dell’Elfo si sono distinti per la passione con cui raccontano gli amori “altri” e “fuorilegge” ai quali hanno saputo dare visibilità artistica e dignità narrativa e, quindi, civile.  
  
  
L’ordine dei lavori prevede gli interventi di:  
\- Domenico Rizzo, storico delle donne e dell’identità di genere, Università “L’Orientale” di Napoli.  
\- Marilisa D’Amico, costituzionalista, Università degli studi di Milano.  
\- Paola Ronfani, sociologa giuridica, Università degli studi di Milano.  
\- Antonella Besussi, Teorica della politica, Università degli studi di Milano.  
  
Coordina i lavori Sergio Rovasio, segretario nazionale di Certi Diritti.  
  
Sarà presente uno dei due curatori, Francesco Bilotta, avvocato e fondatore di Rete Lenford.  
  
[http://www.mimesisedizioni.it/archives/001130.html](http://www.mimesisedizioni.it/archives/001130.html)  
[http://www.certidiritti.it/](http://www.certidiritti.it/)  
[http://www.retelenford.it/](http://www.retelenford.it/)  
[http://www.unimi.it/](http://www.unimi.it/)  
[http://www.elfo.org/](http://www.elfo.org/)