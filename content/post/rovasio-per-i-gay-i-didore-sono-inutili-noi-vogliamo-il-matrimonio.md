---
title: 'Rovasio: per i gay i DiDoRe sono inutili. Noi vogliamo il matrimonio'
date: Wed, 24 Mar 2010 10:56:12 +0000
draft: false
tags: [Comunicati stampa]
---

_**Dichiarazione di SERGIO ROVASIO, candidato Lista Bonino-Pannella nel Lazio e Segretario Certi Diritti**_  
  
L'associazione radicale Certi Diritti ringrazia l'Onorevole Alessandra Mussolini per essere favorevole alla regolarizzazione delle coppie gay attraverso i DiDoRe. Peccato però che i DiDoRe servano soltanto a confermare che l'unica famiglia è quella fondata sul matrimonio eterosessuale, così come si afferma nella relazione accompagnatrice al Progetto di Legge. I DiDoRe riconoscono diritti non ai conviventi, ma a coloro che ormai non sono più tali perché uno dei due è morto o perché la convivenza non c'è più.  
  
L'onorevole Mussolini dovrebbe piuttosto spiegarci se secondo lei due persone dello stesso sesso sono capaci di formare una famiglia oppure no. Nel caso siano capaci, perché non concedergli di contrarre matrimonio?  
Sicuramente domani la Corte costituzionale, che è chiamata ad esprimersi sulla legittimità del matrimonio gay, ci aiuterà ad uscire dall'ideologia e ad affrontare con razionalità i problemi delle persone. L'associazione radicale Certi Diritti attende con fiducia la decisione della Corte e qualunque sarà il risultato continuerà con forza la sua lotta per il superamento delle diseguaglianze.