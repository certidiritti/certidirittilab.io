---
title: 'VIDEO E NEWS MANIFESTAZIONE A TORINO CONTRO ARCIDIOCESI'
date: Fri, 05 Dec 2008 08:02:28 +0000
draft: false
tags: [Comunicati stampa]
---

Ai seguenti link si può visionare il video della manifestazione di Certi Diritti svoltasi a Torino lo scorso 2 dicembre contro decisione del Vaticano di non sostenere la depenalizzazione dell'omosessualità all'Onu.

[http://www.youtube.com/user/TorinoPride1](http://www.youtube.com/user/TorinoPride1)

Al seguente link [foto e news dell'iniziativa](http://torino.repubblica.it/multimedia/home/3933025/1/1).)