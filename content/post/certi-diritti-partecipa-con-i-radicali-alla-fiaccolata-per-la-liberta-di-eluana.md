---
title: 'CERTI DIRITTI PARTECIPA CON I RADICALI ALLA FIACCOLATA PER LA LIBERTA'' DI ELUANA'
date: Fri, 16 Jan 2009 18:53:28 +0000
draft: false
tags: [Comunicati stampa, Coscioni, Eluana Englaro, Enzo Tortora, fiaccolata, Lecco, Milano, Renzo e Lucio]
---

Domenica 18 Gennaio, alle ore 17:30, a Lecco, Certi Diritti parteciperà alla

**FIACCOLATA PER LA LIBERTA' DI ELUANA**
----------------------------------------

“Radicali Italiani” ha deciso di partecipare ufficialmente ed attivamente all’iniziativa, organizzata dalle Cellule Luca Coscioni di Lecco e di Milano e co-promossa dalle Associazioni “Radicali Lecco” e  “Enzo Tortora - Radicali Milano”.

L’iniziativa si svolge la prossima:

### **DOMENICA 18 GENNAIO, ORE 17.30**  
**PIAZZA DIAZ (di fronte al Comune), LECCO**

**Il recente Comitato nazionale, svoltosi nello scorso fine settimana, ha approvato, all’unanimità, un formale e caloroso invito rivolto, in primo luogo, ai** parlamentari, ai dirigenti e ai militanti radicali ad “esprimere la propria solidarietà alla famiglia Englaro”, partecipando alla fiaccolata in occasione del diciassettesimo anniversario dell’incidente che ha causato il coma irreversibile e la lunga agonia di Eluana.

Noi saremo presenti.

Contiamo davvero di vederti, perché crediamo sia necessario e doveroso, di fronte alla subdola e vergognosa campagna di pressione e di vero e proprio linciaggio cui viene sottoposto il padre, Beppino, stringersi attorno alla famiglia di Eluana e rivendicarne la libertà.

Sarà un momento importante dal punto di vista umano e significativo dal punto di vista  politico: nessuna presenza può essere considerata scontata, dunque arrivederci a domenica a Lecco, per Eluana, con Beppino e la famiglia Englaro.

Roma, 13 gennaio 2009

**Antonella Casu** \- segretaria  
**Michele De Lucia** – tesoriere  
**Bruno Mellano** \- presidente

_**Il programma:**_  
h 17.30 - ritrovo in piazza Diaz, inizio maratona oratoria, distribuzione delle fiaccole  
h 18.30 - partenza "Fiaccolata per la libertà di Eluana"  
h 19.15 - arrivo presso la Clinica Telamone e consegna delle 17 rose e scioglimento del corteo

_**Il percorso:**_  
Lecco - piazza Diaz, via Cavour, via Roma, piazza XX Settembre, piazza Cermenati, via Canonica, via San Nicolò.