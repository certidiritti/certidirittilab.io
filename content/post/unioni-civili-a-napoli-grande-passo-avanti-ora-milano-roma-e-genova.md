---
title: 'Unioni civili a Napoli: grande passo avanti. Ora Milano, Roma e Genova'
date: Mon, 13 Feb 2012 18:04:43 +0000
draft: false
tags: [Diritto di Famiglia]
---

Il comune di Napoli ha dato la migliore risposta al mulinaro bianco Giovanardi. Ora il Parlamento discuta Riforma Diritto di famiglia e matrimonio tra persone stesso sesso.

Roma, 13 febbraio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

L’Associazione Radicale Certi Diritti ringrazia i consiglieri comunali e la Giunta del Comune di Napoli per la storica decisione sul riconoscimento delle unioni civili. E’ un passo in avanti che sollecita la politica nazionale a muoversi, così come la stessa Corte Costituzionale ha chiesto  da quasi due anni con la sentenza 138/2010.  
Dopo Torino e Napoli ora è la volta di Milano, Roma e Genova. L’Associazione Radicale Certi Diritti, insieme a molte altre associazioni che si battono per i diritti civili e per il superamento delle diseguaglianze, ha promosso una serie di iniziative con l’obiettivo di far approvare nei Comuni italiani il riconoscimento delle unioni civili così come previsto dalla legge anagrafica del 1989.

La classe politica è ora che la smetta di far finta di niente e di comportarsi in modo ipocrita per obbedienza al potere della casta clerical-fondamentalista. Tra gli obiettivi della nostra lotta rimane centrale il matrimonio civile per le coppie dello stesso sesso nell’ambito della Riforma complessiva del Diritto di famiglia che i parlamentari Radicali hanno depositato come proposta di legge in Parlamento.

Il Comune di Napoli ha dato la più bella risposta al mulinaro bianco Carlo Giovanardi.  
  

**iscriviti alla newsletter >[  
](newsletter/newsletter)[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**