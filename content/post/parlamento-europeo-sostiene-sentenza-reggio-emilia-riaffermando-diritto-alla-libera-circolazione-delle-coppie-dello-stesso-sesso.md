---
title: 'Parlamento Europeo sostiene sentenza Reggio Emilia riaffermando diritto alla libera circolazione delle coppie dello stesso sesso'
date: Fri, 30 Mar 2012 20:32:38 +0000
draft: false
tags: [Diritto di Famiglia]
---

Bruxelles-Roma, 30 marzo 2012  
  
Comunicato Stampa dell'Associazione Radicale Certi Diritti  
   
Il PE ha approvato ieri la relazione di Adina Valean (Eurodeputata rumena del Gruppo Liberale) sulla cittadinanza europea, approvando anche un emendamento depositato da più di 100 deputati che riafferma la posizione del PE contro le discriminazioni basate sull'orientamento sessuale, in particolare nell'esercizio libera circolazione dell'UE.  
   
L'emendamento riaffermava, come recentemente ribadito anche dalla sentenza del Tribunale di Reggio Emilia, che la direttiva UE sulla libera circolazione garantisce il pieno diritto di entrata, di soggiorno e di residenza al coniuge non-UE dello stesso sesso del cittadino UE sposato in uno Stato terzo. Il PE afferma anche che tale libertà di circolazione e diritto alla residenza e soggiorno si applica anche alle coppie dello stesso sesso e di sesso diverso che siano in una partnership registrata o in una relazione stabile ed attestata. Il gruppo PPE ha votato contro l'emendamento depositato dalla relatrice Valean, che é stato comunque approvato con un voto in due parti.  
   
La prima parte é stata approvata con 352 voti a favore, 239 contrari e 14 astenuti (voto sulla frase " in accordo con i principi di reciproco riconoscimento, uguaglianza, non discriminazione, dignità e rispetto della vita privata e familiare"); la seconda con 351 a favore, 241 contrari e 11 astenuti (voto sul resto del paragrafo). Certi Diritti si felicita per l'approvazione dell'emendamento e della relazione, che rappresentano il pieno appoggio da parte del PE della sentenza di Reggio Emilia. Certi Diritti ribadisce la richiesta di fare un passo ulteriore al governo ritirando la circolare Amato che impedisce il riconoscimento dei matrimoni tra persone dello stesso sesso celebrati all'estero, costituendo una discriminazione basata sull'orientamento sessuale proibita dalla UE.  
   
**Testo dell'emendamento (nr 1) approvato dal PE:**  
   
7 bis. ribadisce le sue precedenti richieste agli Stati membri di garantire la libera circolazione per tutti i cittadini dell'UE e le loro famiglie, senza discriminazioni sulla base dell'orientamento sessuale o della nazionalità; ribadisce la sua richiesta agli Stati membri di dare piena attuazione ai diritti sanciti dall'articolo 2 e dall'articolo 3 della direttiva 2004/38/CE1 e di riconoscere tali diritti non soltanto ai coniugi di sesso diverso, ma anche ai partner legati da un'unione registrata, ai membri del nucleo familiare e ai partner con cui un cittadino dell'UE abbia una relazione stabile e debitamente attestata, ivi compresi i membri di coppie dello stesso sesso, in accordo con i principi di reciproco riconoscimento, uguaglianza, non discriminazione, dignità e rispetto della vita privata e familiare; invita la Commissione, in tale contesto, a garantire che la direttiva sia applicata rigorosamente.