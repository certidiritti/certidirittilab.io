---
title: 'La Regione Sicilia e le persone intersex/dsd'
date: Sun, 09 Feb 2014 11:28:55 +0000
draft: false
tags: [Intersex]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti e del collettivo Intersexioni

Roma, 7 febbraio 2014

Dopo il caso del bimb* nat* a Gela con un certo grado di atipicità genitale, l'Associazione Radicale Certi Diritti e il collettivo Intersexioni hanno scritto una lettera aperta al presidente della Regione Sicilia Rosario Crocetta, che si è espresso con sensibilità e apertura sulla vicenda, per chiedergli che la Sicilia diventi un esempio di buone prassi sulla questione intersex/dsd per tutta l'Italia.

Le richieste avanzate vanno dall'emanazione di un decreto o legge regionale che vieti il ricorso a trattamenti medico-farmacologici non necessari o trattamenti chirurgici cosmetici e non vitali su genitali sani solo perché atipici al monitoraggio del fenomeno, dalla creazione di centri qualificati all'interno delle strutture sanitarie per la tutela della salute e del benessere delle persone intersex/dsd e dei loro familiari all'organizzazione di corsi di aggiornamento e formazione.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: «Il diritto del bambino all'integrità fisica è un diritto umano fondamentale riconosciuto dalle Nazioni Unite, dall'Unione Europea e dal Consiglio d'Europa, ma che troppo spesso viene violato, in Italia, ai danni dei bambini intersex/dsd. È ora che la politica se ne occupi in maniera seria sollevando il velo di silenzio che copre questa realtà. La sensibilità dimostrata dal Presidente Crocetta, fa ben sperare che la Sicilia possa diventare un esempio di buone prassi per tutta l'Italia».

Michela Balocchi, ricercatrice e tra le fondatrici di intersexioni, dichiara: «mi auguro che anche attraverso queste comunicazioni pubbliche i genitori del neonato, così come qualsiasi altro genitore che si trovi in una situazione simile, possano avere accesso a fonti d'informazione alternative e non si sentano costretti a prendere decisioni affrettate, come interventi irreversibili e potenzialmente dannosi per la salute psico-fisica del bimbo, e che sappiano che ci sono gruppi di volontari/e, attiviste/i, associazioni di pazienti ed ex pazienti, persone adulte intersex/dsd con cui possono parlare e confrontarsi, pronte ad accoglierli e a condividere esperienze senza pressioni».