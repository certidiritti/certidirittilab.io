---
title: 'Lettera-appello al Presidente Zingaretti'
date: Tue, 10 Feb 2015 15:25:09 +0000
draft: false
tags: [Politica]
---

![regionelazio](http://www.certidiritti.org/wp-content/uploads/2015/02/regionelazio.jpg)Caro Presidente Nicola Zingaretti,

il 28 gennaio abbiamo festeggiato insieme in Campidoglio la fine del lungo percorso che ha portato all'approvazione del registro delle Unioni civili a Roma. Un giusto riconoscimento a tutte le famiglie che “di fatto” già vivono nella nostra città. Ma prima di tutto un messaggio chiaro a tutti i cittadini: non esistono amori e unioni di serie A e B.

Quella per le unioni civili è stata una lunga battaglia partita 8 anni fa, quando il movimento LGBTQI  e i Radicali lanciarono la campagna per una prima delibera popolare. Il testo venne bocciato dalla maggioranza di centro sinistra. Nel 2012 una nuova iniziativa popolare provò a riproporre il tema, senza successo: la delibera infatti non venne mai neppure calendarizzata.

Questa approvazione, dunque, rappresenta un cambio di rotta rispetto agli errori del passato.

Il cammino è ancora lungo: in Parlamento così come nella Regione che Lei si è assunto la responsabilità di amministrare. Nel Lazio infatti, è ancora vigente la “Legge Storace sulla famiglia” del 2001, espressione di una cultura fortemente discriminatoria sui servizi essenziali, opposta a quella che ha ispirato la vittoria in Campidoglio. La legge regionale per gli interventi a sostegno della famiglia, riguarda infatti esclusivamente le famiglie unite in matrimonio, subordinando la concessione di prestiti a tasso agevolato, l’edilizia residenziale pubblica e il rimborso delle spese relative a utenze e imposte, all'"esibizione dell'atto matrimoniale".

Le rivolgiamo dunque un appello affinché ci indichi la strada che la Sua Giunta intende seguire per abrogare la legge Storace del 2001, aprendo un confronto concreto anche con le associazioni, per approdare alla redazione di un nuovo testo che possa integrare e migliorare la normativa precedente in materia, la legge 34 del 1999, che già apriva al pluralismo delle forme familiari.  Dopo Roma, solo con il Suo impegno potremo aprire un nuovo capitolo di lotta alle discriminazioni anche per tutti i cittadini e le cittadine del Lazio.

**Associazione radicale Certi Diritti, GayNet, Circolo di cultura omosessuale Mario Mieli, Di'Gay Project, I Mondi Diversi, Rete Genitori Rainbow, Famiglie Arcobaleno, BallorOmo, Fondazione Luciano Massimo Consoli, La Fenice Gay, Gruppo Pesce Roma, UAAR Roma, CondividiLove, Anddos**

Pubblicato su Il Manifesto del 10/2/2015