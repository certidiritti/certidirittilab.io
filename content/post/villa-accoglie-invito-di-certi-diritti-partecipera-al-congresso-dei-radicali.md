---
title: 'VILLA ACCOGLIE INVITO DI CERTI DIRITTI, PARTECIPERA'' AL CONGRESSO DEI RADICALI'
date: Tue, 28 Oct 2008 08:02:53 +0000
draft: false
tags: [Comunicati stampa, CONGRESSO, RADICALI, UDC, VILLA]
---

**DIRIGENTE DELL'UDC DISCRIMINATO PERCHE' GAY ACCOGLIE L’INVITO DI CERTI DIRITTI E PARTECIPERA’ AI LAVORI DEL CONGRESSO DI RADICALI ITALIANI CHE SI TERRANNO DAL 30 OTTOBRE AL 2 NOVEMBRE A CHIANCIANO TERME.**

Roma, 29 ottobre 2008

**_Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:_**

“Alberto Villa, l’esponente dell’Udc che secondo quanto da lui stesso dichiarato era stato emarginato e discriminato dall’Udc perchè dichiaratamente gay, ha accolto l’invito dell’Associazione Radicale Certi Diritti e parteciperà ai lavori del VII Congresso nazionale di Radicali Italiani che si svolgerà da giovedì 30 ottobre a domenica 2 novembre a Cianciano Termne.

Alberto Villa ha annunciato che seguirà e interverrà ai lavori di una delle Commissioni, in particolare quella che ha per titolo. “Ipoteca contemporanea contro la civiltà: contro tutti i proibizionismi, per la libertà e la responsabilità delle persone”. Villa ha voluto anche precisare che la sua partecipazione non vuol dire, per il momento, adesione a Radicali Italiani.”.