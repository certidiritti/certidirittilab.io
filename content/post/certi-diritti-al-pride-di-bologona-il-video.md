---
title: 'CERTI DIRITTI AL PRIDE DI BOLOGONA - IL VIDEO'
date: Wed, 30 Jul 2008 09:20:12 +0000
draft: false
tags: [Affermazione Civile, Bologna Pride 2008, certi diritti, Comunicati stampa, Pubblicazione degli atti di Matrimonio]
---

Finalmente [è possibile vedere il video di Certi Diritti al Pride di Bologna 2008](index.php?option=com_content&view=article&id=164:certi-diritti-al-pride-di-bologona-il-video&catid=1:ultime&Itemid=55). Sul nostro carro molte coppie hanno potuto "sperimentare" l'emozione che si può provare [aderendo alla nostra iniziativa di affermazione civile](index.php?option=com_content&view=article&id=41&Itemid=72): recarsi nel proprio comune per chiedere la pubblicazione degli atti di matrimonio. Certi Diritti vi aiuterà ad impugnare il rifiuto ricevuto dall'ufficiale di stato civile.

[Guarda il video!](index.php?option=com_content&view=article&id=164:certi-diritti-al-pride-di-bologona-il-video&catid=1:ultime&Itemid=55)