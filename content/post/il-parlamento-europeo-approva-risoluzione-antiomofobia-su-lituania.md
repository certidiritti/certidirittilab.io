---
title: 'IL PARLAMENTO EUROPEO APPROVA RISOLUZIONE ANTIOMOFOBIA SU LITUANIA'
date: Fri, 18 Sep 2009 12:47:21 +0000
draft: false
tags: [Comunicati stampa]
---

Certi Diritti si felicita per l'approvazione della risoluzione del PE sull'omofobia in Lituania

Il Parlamento europeo continua anche nella nuova legislatura a difendere i diritti LGBT, con l'appoggio di tante delegazioni nazionali del PPE - ma senza quello della PDL italiana.

17 settembre 2009

Dichiarazione di Sergio Rovasio, segretario dell'Associazione Radicale Certi Diritti:

"Il PE ha approvato oggi la risoluzione, co-firmata da tutti i principali gruppi politici del PE, che condanna la Lituania per avere approvato una legge omofoba che criminalizza l'informazione pubblica sull'omosessualità, se questa é accessibile ai minori. La legge mira a proibire l'accesso da parte dei minori di 18 anni ad ogni informazione, film, libro, che tratti dell'omosessualità in modo positivo, acomunando l'omosessualità alla necrofilia, alle immagini di corpi mutilati e morti o che incoraggiano al suicidio ed all'auto-mutilazione.

Il PE chiede alle autorità lituane di cambiare la legge prima che entri in vigore nel marzo 2010, incarica l'Agenzia dei diritti fondamentali di fornire un parere giuridico e la commissione libertà pubbliche di seguire la questione.

La risoluzione é basata sul progetto redatto da Ottavio Marzocchi, responsabile per gli affari europei di Certi Diritti, funzionario del gruppo ALDE e membro dell'intergruppo LGBT e successivamente negoziato con gli altri gruppi politici.

Certi Diritti si felicita per l'approvazione della risoluzione ed in particolare per l'appoggio alla risoluzione da parte di una gran parte dei deputati del PPE - mentre la delegazione Pdl al PE votava contro la risoluzione per coprire l'omofobia in Lituania, come lo fa anche in Italia non condannando con prese di posizioni pubbliche da parte di alcun Ministro del governo ed in alcun modo gli attacchi omofobici avvenuti in Italia.

Il testo della risoluzione approvata é disponibile nella versione provvisoria su:

[http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+MOTION+P7-RC-2009-0026+0+DOC+XML+V0//IT](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+MOTION+P7-RC-2009-0026+0+DOC+XML+V0//IT)

Gli appelli nominali saranno disponibili su:

[http://www.europarl.europa.eu/sed/votingResults.do](http://www.europarl.europa.eu/sed/votingResults.do)