---
title: 'Gruppi locali'
date: Mon, 24 Jan 2011 20:03:03 +0000
draft: false
tags: [Politica]
---

Per contattare le sedi locali di Certi Diritti:

**sede nazionale**:    via di Torre Argentina, 76 00186 Roma  
Telefono: 06-68979215

**Piemonte e Valle d'Aosta:**

Enzo Cucco

[torino@certidiritti.it](mailto:torino@certidiritti.it)

**Lombardia:**

Gian Mario Felicetti

[milano@certidiritti.it](mailto:milano@certidiritti.it)

**Triveneto:**

Clara Comelli

[trieste@certidiritti.it](mailto:trieste@certidiritti.it)

**Gorizia**

Luca Cenni

[l.cenni@activeweb.it  
](mailto:l.cenni@activeweb.it)348-3237935[](mailto:l.cenni@activeweb.it)

**Liguria:**

Alessandro Mazzone

[genova@certidiritti.it](mailto:genova@certidiritti.it)

**Emilia Romagna**

Maurizio Cecconi

[bologna@certidiritti.it](mailto:bologna@certidiritti.it)

**Marche**

Giacomo Celottini

marche@certidiritti.it

**Lazio**

Sergio Rovasio

[roma@certidiritti.it](mailto:roma@certidiritti.it)

**Campania**

Andrea Furgiuele

[napoli@certidiritti.it](mailto:napoli@certidiritti.it)

**Puglia**

Dario Vese

[lecce@certidiritti.it](mailto:lecce@certidiritti.it)

**Calabria**

Riccardo Cristiano

[catanzaro@certidiritti.it](mailto:catanzaro@certidiritti.it)

**Sicilia**

Eduardo Melfi

[catania@certidiritti.it](mailto:catania@certidiritti.it)