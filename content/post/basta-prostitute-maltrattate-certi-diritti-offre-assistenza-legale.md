---
title: 'BASTA PROSTITUTE MALTRATTATE: CERTI DIRITTI OFFRE ASSISTENZA LEGALE'
date: Thu, 14 Aug 2008 10:31:58 +0000
draft: false
tags: [Comunicati stampa]
---

**BASTA CON I MALTRATTAMENTI ALLE PROSTITUTE, CERTI DIRITTI OFFRE ASSISTENZA LEGALE IN ATTESA DI REGOLAMENTAZIONE DELLA PROSTITUZIONE:**

**Dichiarazione di Marco Perduca, Senatore radicale - Pd e Guido Allegrezza, membro del Direttivo dell'Associazione radicale Certi Diritti:**

.  
"l'Associazione Radicale Certi Diritti, e i parlamentari Radicali eletti nelle liste del PD stanno valutando coi suoi legali eventuali iniziative in sostegno ai recenti casi di maltrattamento di cittadini non italiani, per prestare assistenza a coloro che, incorrendo in incidenti di quel tipo, non sappiano a chi rivolgersi.  
  
Il recente caso della prostituta rinchiusa a Parma in condizioni inaccettabili per un paese civile trova una drammatica eco (ormai anche internazionale) in quanto è accaduto il 12 agosto a Roma, dove una ragazza peruviana, seduta sui gradini di Santa Maria della Vittoria per aspettare un'amica, apostrofata da una volante di passaggio: "Ma che fai ti metti a lavorare proprio qui, davanti a una chiesa?". Al sopraggiungere dell'amica, stesso trattamento anche per lei: "Bella, diglielo pure alla tua amica, questa è una chiesa, non potete mettervi a lavorare qui".  
  
In questura, dove sono andate per denunciare il comportamento degli agenti, la ragazza che non aveva con sé i documenti è stata condotta all'ufficio immigrati e rinchiusa in "una cella sporca di immondizia", dalla quale sarà rilasciata solo la mattina del 13.  
  
Secondo quanto riportato dalla stampa le ragazze avrebbero lamentato il fatto che "Roma è diventata invivibile per gli stranieri: siamo regolari, parliamo italiano, abbiamo amici italiani eppure veniamo trattate così. Siamo qui da tanti anni, continuiamo ad amare questa città, ma facciamo fatica a viverci". All'idea di denunciare il trattamento subito: "Volevamo farlo ieri, ma poi è andata come è andata. Ora abbiamo paura, chi ci torna in questura?".  
  
In una nota dell'Associazione Radicale Certi Diritti, Guido Allegrezza e il Senatore Radicale Marco Perduca hanno denunciato il comportamento tenuto "dalle forze dell'ordine, sia per la brutalità nelle parole degli agenti sia nella volontà di impedire che il loro trattamento fosse denunciato dalle interessate, applicando una misura repressiva sproporzionata tale da condurre in fermo la ragazza che non aveva con sé i documenti."  
  
Tra l'altro, sottolinea Allegrezza e Perduca, i fatti si sono svolti in pieno centro, in una zona frequentatissima da turisti, in cui è noto a tutti che non esiste giro di prostituzione femminile in strada. Appare quindi singolare che gli agenti della volante si siano comportati in quel modo. C'è da dire che, se essi fossero stati veramente intenzionati ad intervenire sul tema, avrebbero trovato abbondanza di casi pochi passi più oltre. Tutti a Roma sanno che nella piazzetta antistante le terme di Diocleziano fra le baracchette dei librai che si trovano fra la stazione Termini e Piazza Repubblica - se ne trova traccia anche nella letteratura di Pasolini, quindi un fenomeno tutt'altro che nuovo - opera quotidianamente un folto gruppo di prostituti maschili, in una situazione di degrado urbano e sociale che ha veramente pochi riscontri in città.  
  
La reazione presso i gradini della chiesa avrebbe dovuto essere sufficiente a far comprendere l'abbaglio: non si era in presenza di prostitute. Un comportamento semplicemente educato e civile, a prescindere dalla nazionalità delle interessate, avrebbe rimesso tutto a posto, senza destare il clamore delle cronache. Questo preoccupante comportamento è da attribuire al clima che si respira fatto di un miscuglio di moralismo, disprezzo per gli stranieri che vengono dai luoghi del mondo che consideriamo meno evoluti, sbandierata attenzione alla sicurezza e un preoccupante autoritarismo di ritorno.  
  
Allegrezza e Perduca concludono denunciando che "questi fatti recenti si scatenano contro i più deboli, ovvero il punto finale di un fenomeno che si continua a voler combattere reprimendo chi si prostituisce e i clienti piuttosto che le organizzazioni criminali che, spesso, ne costituiscono l'elemento forte. Se veramente si volesse intervenire decisamente contro gli aspetti criminali della prostituzione, basterebbe decidersi a regolarla facendone una professione. In attesa di cio' l'Associazione Radicale Certi Diritti interverra' con un sostegno anche legale nei confronti di chi subisce soprusi come quelli di questi giorni".  
  
Appena insediati, i Senatori Radicali Marco Perduca e Donatella Poretti hanno presentato un disegno di legge per legalizzare e regolamentare la prostituzione".