---
title: 'Giovanardi non ficchi il naso in vicende che non lo riguardano e lasci stare l''unione di Paola Concia'
date: Sat, 06 Aug 2011 11:34:24 +0000
draft: false
tags: [Politica]
---

Il sottosegretario Giovanardi ha perso l'ennesima occasione per starsene zitto e farsi gli affari suoi. Grave l'intromissione sull'unione di Paola Concia con la sua compagna.  
Avviate decine di iniziative legali contro l'Italia per il superamento delle diseguaglianze e per il matrimonio gay anche in Italia.

Comunicato Stampa dell’Associazione Radicale Certi Diritti:

Il (molto) Sottosegretario Carlo Giovanardi ha perso un’altra occasione per starsene zitto e non ficcare il naso in vicende umane e personali che riguardano questa volta la parlamentare Paola Concia, che ha appena coronato legalmente la sua unione con la sua compagna in Germania. E’ grave che un rappresentante del Governo trovi sempre occasione per ficcare il naso, in modo ossessivo -e certamente anche da studiare sul piano psico-comportamentale-, in una vicenda che non lo riguarda, né tantomeno riguarda quello che dovrebbe fare in qualità di Sottosegretario.

Su quanto l’esponente clerical-pseudo-costituzional-familista Giovanardi va ripetendo, con un mantra ossessivo,  che 'la nostra Carta prevede, quali requisiti per il matrimonio, un rapporto tra un uomo e una donna, per realizzare quella 'societa' naturale' che e' il luogo in cui nascono i figli'', non possiamo che consigliargli di guardare cosa è avvenuto in Belgio, Olanda, Spagna, Sud-Africa, Argentina, e molti altri paesi democratici, ovvero che il matrimonio è un istituto al quale oramai accedono anche le coppie omosessuali e che prima o poi anche l’Italia raggiungerà questo obiettivo. La nostra lotta ha questo come obiettivo: il superamento delle diseguaglianze anche grazie ai ricorsi alla Corte Europea dei Diritti dell’Uomo e alla Corte di Giustizia Europea di molte coppie gay conviventi che partecipano alla campagna di Affermazione Civile dell’Associazione Radicale Certi Diritti.