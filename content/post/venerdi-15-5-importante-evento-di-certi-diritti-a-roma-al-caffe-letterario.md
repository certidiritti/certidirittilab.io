---
title: 'VENERDI'' 15-5 IMPORTANTE EVENTO DI CERTI DIRITTI A ROMA, AL CAFFE'' LETTERARIO'
date: Tue, 05 May 2009 13:47:13 +0000
draft: false
tags: [Comunicati stampa]
---

Venerdì 15 maggio, dalle ore 19, in occasione della giornata mondiale contro l'omofobia (che è il 17-5),  Certi Diritti Roma terrà al Caffè Letterario, in Via Ostiense 95, un Happy Hours con spettacoli, drink, show, drag queen (e molto altro)

Non mancare... l'ingresso è aperto a tutti!!!