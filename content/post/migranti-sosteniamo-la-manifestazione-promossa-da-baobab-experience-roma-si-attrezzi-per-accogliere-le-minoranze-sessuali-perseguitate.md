---
title: 'Migranti: sosteniamo la manifestazione promossa da Baobab Experience. Roma si attrezzi per accogliere le minoranze sessuali perseguitate.'
date: Sat, 17 Dec 2016 11:30:28 +0000
draft: false
tags: [Politica]
---

![15621928_1189764011100349_159162924920712110_n](http://www.certidiritti.org/wp-content/uploads/2016/12/15621928_1189764011100349_159162924920712110_n-300x108.jpg)"L'Associazione Radicale Certi Diritti non può non sostenere la mobilitazione di oggi promossa da Baobab Experience, così come appoggia il grande lavoro delle volontarie e dei volontari che hanno colmato le carenze di un'amministrazione impassibile di fronte ai sintomi di un mondo che sta cambiando. Alle richieste di Baobab Experience di istituire un Centro di Primissima Accoglienza per i migranti, un tavolo permanente di confronto per garantire e promuovere i diritti fondamentali dei migranti, il monitoraggio attento delle attività dell’ufficio immigrazione della questura, Certi Diritti sollecita la città di Roma a dotarsi anche di strutture e conoscenze per accogliere le vittime di persecuzioni nei confronti di lesbiche, gay, bisessuali, transessuali e intersex." Lo dice Leonardo Monaco, segretario dell'Associazione Radicale Certi Diritti, annunciando il sostegno dell'organizzazione alla manifestazione 'Proteggiamo le persone non i confini'.