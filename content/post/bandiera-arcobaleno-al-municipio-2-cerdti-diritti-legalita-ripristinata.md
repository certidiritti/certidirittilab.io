---
title: 'BANDIERA ARCOBALENO AL MUNICIPIO 2. CERTI DIRITTI: LEGALITÀ RIPRISTINATA.'
date: Thu, 23 Jun 2016 22:48:51 +0000
draft: false
tags: [Politica]
---

[![10385508_10152224776610488_7261283092756996601_n-225x300](http://www.certidiritti.org/wp-content/uploads/2016/06/10385508_10152224776610488_7261283092756996601_n-225x300.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/06/10385508_10152224776610488_7261283092756996601_n-225x300.jpg)“Alla luce degli sviluppi che la vicenda ha assunto negli ultimi giorni e dei  possibili scenari che potrebbero profilarsi anche per l'Amministrazione municipale, ho ritenuto di disporre che la bandiera venga esposta - a partire da domani mattina ovvero quando sarà presente il personale del settore incaricato e per i giorni in cui si svolgerà la Pride Week e il Festival MIX \[…\]”, così il Direttore del Settore Municipio 2 in una comunicazione al avv. Andrea Bullo che ha assistito legalmente l’Associazione Radicale Certi Diritti.

“La certezza del diritto è stata ripristinata al Municipio 2. È surreale che nella Milano del 2016 occorrano campagne online, manifestazioni e persino una diffida formale per far rispettare una delibera votata da un organo democraticamente eletto. È una vittoria di tutte le cittadine e tutti i cittadini del Municipio 2 che hanno dimostrato cosa significa davvero inclusione e rispetto e hanno restituito dignità all’istituzione municipale”, dichiara Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti.

“Ringrazio l’avvocato Andrea Bullo per l’ottimo lavoro svolto e tutte le cittadine e i cittadini del Municipio 2 che hanno appeso le bandiere arcobaleno alla finestra, che le hanno postate sulla pagina Facebook del Municipio 2 e che le hanno portate in viale Zara 100”, conclude Guaiana.