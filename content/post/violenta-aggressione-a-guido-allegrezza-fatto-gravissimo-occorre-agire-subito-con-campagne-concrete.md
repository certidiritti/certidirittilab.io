---
title: 'Violenta aggressione a Guido Allegrezza fatto gravissimo. Occorre agire subito con campagne concrete'
date: Fri, 15 Jun 2012 08:25:29 +0000
draft: false
tags: [Politica]
---

Non basta più la solidarietà, occorre passare ai fatti, superando gli steccati ideologici.

Roma, 15 giugno 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti  
  
La notizia della violenta aggressione subita da Guido Allegrezza, militante della comunità lgbt di Roma, già iscritto e sostenitore dell’Associazione Radicale Certi Diritti, impegnato da molti anni per il superamento delle diseguaglianze, ci riempie di sgomento ed esprimiamo a lui la nostra più sentita vicinanza e solidarietà. Questo episodio è l'inequivocabile segnale di quanto siano urgenti provvedimenti efficaci, nel nostro Paese, per la prevenzione e il contrasto degli atti di violenza e maltrattamento per motivi omofobi e transfobici. Provvedimenti che, a nostro avviso, devono soprattutto caratterizzarsi per un impegno davvero capillare e forte nell'ambito della prevenzione con campagne di informazione mirate, nelle scuole e nei vari ambiti sociali. In questo senso il Comune di Roma si deve attivare subito con iniziative concrete. Non basta più la solidarietà, occorre passare ai fatti, superando gli steccati ideologici.