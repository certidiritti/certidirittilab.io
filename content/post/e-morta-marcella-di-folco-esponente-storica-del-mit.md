---
title: 'E'' MORTA MARCELLA DI FOLCO, ESPONENTE STORICA DEL MIT'
date: Tue, 07 Sep 2010 13:02:33 +0000
draft: false
tags: [Comunicati stampa]
---

**MORTE MARCELLA DI FOLCO, STORICA ESPONENTE DEL MOVIMENTO ITALIANO TRANSESSUALI. IL RICORDO DELL’ASSOCIAZIONE CERTI DIRITTI.  
   
Roma, 8 settembre 2010  
   
Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

  “Oggi è deceduta per un tumore Marcella Di Folco, storica esponente del Movimento Italiano Transessuali.  Marcella è stata iscritta e contribuente radicale e lo scorso anno si era iscritta a Certi Diritti.  
   
   Marcella l’abbiamo conosciuta quando, nel 1982, in Piazza Montecitorio, si facevano continuamente sit-in per sollecitare l’approvazione della proposta di legge dei radicali: ‘Legge 14 aprile 1982, n. 164 ‘norme in materia di rettificazione di attribuzione di sesso’.  
   
   Il Gruppo Parlamentare Radicale, nei giorni precedenti l’approvazione della Proposta di Legge, era diventato, all’epoca, il quartier generale del movimento delle persone transessuali. Ogni giorno si facevano comunicati stampa per sensibilizzare i media e la classe politica della necessità di un provvedimento che potesse dare maggiori garanzie, tutele e dignità alle persone transessuali. Si inviavano lettere ai parlamentari, si facevano telefonate ai Capigruppo dell’epoca, si chiedevano continuamente incontri con i membri delle Commissioni Affari sociali e Sanità dove la Proposta di Legge era  in discussione.  
   
   I commessi della Camera dei deputati, dell’ingresso di Via Uffici del Vicario, a volte non facevano salire qualcuna/o agli uffici dei radicali ubicati al sesto piano del Palazzo, in quel caso era sufficiente chiedere a Marcella di intervenire e il tutto si risolveva con qualche urlo e qualche risata. Compagna di lotta era con lei anche Gianna Parenti, storica amica e militante, esponente del Movimento Italiano Transessuali che tanto si adoperò per l’approvazione di quella legge.  
   
   I giorni e i mesi passati insieme furono di grande aiuto per molti di noi per comprendere quanto fosse importante sostenere le rivendicazioni delle persone transessuali, sempre denigrate, offese, calpestate ancora oggi nella loro dignità.  
   
    Abbiamo incontrato l’ultima volta Marcella lo scorso 17 maggio, in occasione della VI giornata internazionale contro l’omofobia, a Roma, in Piazza del Quirinale. In quell’occasione, grazie ad un importante intervento di Paola Concia,  il Presidente della Repubblica ricevette le principali organizzazioni lgbt italiane. Era bellissimo vedere Marcella camminare per quei corridoi, con in testa un grande cappello rosso, davanti a tutti i Corazzieri schierati in fila sull’attenti. Disse qualcosa anche a loro…  
   
   Ti abbracciamo forte cara Marcella”.