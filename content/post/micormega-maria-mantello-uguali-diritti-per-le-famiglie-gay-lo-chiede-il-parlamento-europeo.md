---
title: '(Micromega, Maria Mantello) Uguali diritti per le famiglie gay. Lo chiede il Parlamento europeo.'
date: Wed, 24 Jun 2015 11:16:14 +0000
draft: false
tags: [Europa]
---

[![797](http://www.certidiritti.org/wp-content/uploads/2015/06/797-199x300.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/06/797.jpg)Nessuna disparità per le famiglie gay. È l’importante risoluzione approvata dal Parlamento europeo  il 9 giugno 2015 contro le discriminazioni per nazionalità, condizioni economiche, convinzioni personali, nonché pregiudizi sessisti e di orientamento sessuale, con cui ancora oggi si escludono le persone Lgbt dal diritto al pubblico riconoscimento del loro legittimo desiderio di essere famiglia. Ma, si legge nella risoluzione di Strasburgo, «la composizione e la definizione delle famiglie si evolvono nel tempo», pertanto, anche gli stati membri dell’Unione europea devono impegnarsi affinché «le legislazioni sulla famiglia e sul lavoro siano più complete per ciò che riguarda le famiglie monoparentali e i genitori Lgbt». Un’apertura di civiltà questa, in cui ha avuto un ruolo di fondamentale sollecitazione l’Associazione **_Certi Diritti_** di cui **Sergio Rovasio**, che qui intervistiamo, è stato socio fondatore, segretario, ed ancora oggi valido esempio di militanza attiva. [**Continua a leggere l'intervista sul sito web di Micormega >>>**](http://blog-micromega.blogautore.espresso.repubblica.it/2015/06/15/maria-mantello-uguali-diritti-per-le-famiglie-gay-lo-chiede-il-parlamento-europeo/)

Ti interessano le nostre battaglie? Sostienici con la tua [**iscrizione**](http://www.certidiritti.org/iscriviti/) o con un [**contributo libero**](http://www.certidiritti.org/donazioni/)!