---
title: 'LE NOZZE DI SODOMA'
date: Mon, 26 Apr 2010 17:53:27 +0000
draft: false
tags: [Senza categoria]
---

di **Persio Tincani**, _[l'Ornitorinco](http://edizioniornitorinco.it/tincani_nozze.html)_ edizioni 

Il matrimonio gay da Los Angeles a Madrid è una delle questioni centrali del dibattito politico.

La legislazione italiana non pone ostacoli alla celebrazione di validi matrimoni civili tra persone dello stesso sesso, e il rifiuto di celebrarli dipende da un’interpretazione delle norme vigenti condizionata dal pregiudizio in base al quale il matrimonio civile è inteso come una sorta di facsimile del matrimonio religioso cattolico.

Quale che sia l’opinione che si ha sul problema, non si può negare che il matrimonio omosessuale è una questione in cui convergono problemi di carattere ben più vasto e generale, come i diritti individuali e l’estensione del potere statale, che riguardano da vicino tutti. Una norma esplicita, in un senso o nell’altro, non esiste. Ma, a parere dell'autore, le ragioni a favore dell’ammissibilità sono più consistenti di quelle per la negazione.

Per la prima volta un libro prende in considerazione gli aspetti storici, quelli giuridici ma anche e soprattutto le case history della questione in Italia e nel mondo.

Un libro di parte, ma non un libro militante: la semplice e argomentata descrizione di una grande battaglia di civiltà, dei suoi presupposti giuridico-filosofici della sua pratica attuazione.