---
title: 'GLI ISCRITTI 2010 ALL''ASSOCIAZIONE RADICALE CERTI DIRITTI'
date: Wed, 14 Apr 2010 16:10:31 +0000
draft: false
tags: [Senza categoria]
---

[![t](http://www.certidiritti.org/wp-content/uploads/2010/04/t.jpg)](chi-siamo/iscritti-2010/694-come-iscriversi-o-versare-un-contributo.html)

Si sono iscritti per l'anno 2010, tra gli altri (in aggiornamento):

**Guido ALLEGREZZA**, Manager (**SOCIO SOSTENITORE**)

**Matteo ANGIOLI**, Dirigente di _Radicali italiani_

**Laura ARCONTI**, Radicale storica, **TESSERA N° 1**

**Alessandra BAGHDIGHIAN**, Impiegata 

**Angiolo BANDINELLI**, Giornalista, Radicale storico

**Marco** **BELTRANDI**, Deputato radicale

**Rocco BERARDO**, Consigliere regionale radicale del Lazio,

Tesoriere dell'_Associazione Luca Coscioni_

**Rita BERNARDINI**, Deputata radicale

**Emma BONINO**, Vicepresidente del Senato

**Mario BRAMBILLA**, Infermiere

**Aldo BRANCACCI**, Professore ordinario di Storia della filosofia antica

**Enrica CAFERRI**, Insegnante

**Francesco CAMPEDELLI**, Medico

**Fabio CANINO**, Showman

**Marco CAPPATO**, Segretario dell'_Associazione Luca Coscioni  
_

**Carla CARACCIO**, Dirigente di _Non c'è Pace Senza Giustizia_

**Maurizio CECCONI**, Portavoce della _Rete Laica Bologna_

**Simone CASADEI**, Libero professionista

**Claudio CIPELLETTI**, Regista di "Nessuno Uguale" e "[Due volte genitori](http://www.duevoltegenitori.com/)"  

**Mario CIRRITO**, Giornalista, tra gli autori di [queerblog.it](http://www.queerblog.it/)

**Clara COMELLI**, La Presidente  

**Pia COVRE**, Segretaria del [Comitato per i diritti civili delle prostitute](http://www.lucciole.org/)

**Danilo CRESCINI**, Studente universitario

**Cristina CRIPPA**, Attrice [www.elfo.org](http://www.elfo.org)

**Marilisa D'AMICO**, Professore ordinario di Diritto Costituzionale

**Elio DE CAPITANI**, Attore e regista  [www.elfo.org](http://www.elfo.org/) (**SOCIO SOSTENITORE**)

**Alessio DE GIORGI**, Direttore di [www.gay.it](http://www.gay.it/)

**Sergio D'ELIA**, Segretario di [Nessuno Tocchi Caino](http://www.nessunotocchicaino.it/)

**Marco DI SALVO**, Giornalista, Dirigente di Radicali Italiani

**Lelio DONÀ**, Operatore di cooperativa

**Maria Antonietta FARINA COSCIONI**, Deputata radicale

**Gian Mario FELICETTI**, Consulente

**Niccolò FIGÀ-TALAMANCA**, Segretario Generale di [Non c'è Pace Senza Giustizia](http://www.npwj.org/)

**Virginia FIUME**, Scrittirice, Dirigente di Radicali italiani

**Francesco FRONGIA**, Regista e Video Designer  

**Cristian Maria GIAMMARINI**, Attore

**Giovanna GRA**, Illustratrice e sceneggiatrice

**Yuri GUAIANA**, Storico 

**Aldo GUFFANTI**, Segretario dell'_Associazione radicali Como_

**Alessandro LITTA MODIGNANI**, Dirigente di _Radicali italiani_

**Vittorio LINGIARDI**, Psichiatra e psicoanalista, Prof. Ordinario alla Sapienza di Roma

**Alessandro LICCARDO**, Impiegato

**Lorenzo LIPPARINI**, Dirigente di _Radicali italiani_

**DARIO LUPI**, Coordinatore ufficio acquisti e appalti

**Luigi MANCONI**_,_ Presidente di [A buon diritto. Associazione per le libertà](http://www.abuondiritto.it/)

**Gabriele MARIANI**, Ingegnere (**SOCIO SOSTENITORE con 150 €**)

**Pierfrancesco MAJORINO**, Capogruppo **PD** nel Consiglio Comunale di Milano

**Ida MARINELLI**, Attrice [www.elfo.org](http://www.elfo.org/)

**Marcello Tito MANGANELLI**, Attore

**Alessandro MAZZONE**, ingegnere

**Matteo MECACCI**, Deputato radicale

**Alba MONTORI**, Segretaria dell'[Associazione Fondazione Luciano Massimo Consoli](http://fondazionemassimoconsoli.com/)

**Claudio MORI**, Socio fondatore della [Fondazione Massimo Consoli](http://fondazionemassimoconsoli.com/)

**Gianluca PACCHIANI**, Interprete

**Francesco PAIANO**, ingegnere

**Marco PANNELLA**, Presidente del Senato del _Nonviolent‌ Radical‌ Party‌ transnational‌ and‌ transparty_  (**SOCIO SOSTENITORE**)

**Lorenzo PASSINI**, Impiegato

**Marco PERDUCA**, Senatore radicale

**Luca PERILLI**, Cameramen e regista

**Umberto PETRANCA**, Attore

**Angelo PEZZANA**, tra i fondatori del FUORI!

**Luca PIVA**, Il Tesoriere (**SOCIO SOSTENITORE**)

**Davide PIZZAGALLI**, Studente

**Francesco POIRÉ**, Segretario dell'_Associazione Enzo Tortora_

**Michele Elio POLIZZOTTO**, Dirigente di _Non c'è Pace Senza Giustizia_

**Donatella** **PORETTI**, Senatrice radicale

**Giuseppe ROSSODIVITA**, Consigliere regionale radicale del Lazio

**Sergio ROVASIO**, Il Segretario (**SOCIO SOSTENITORE**)

**Elena RUSSO ARMAN**, Attrice

**Ivan SCALFAROTTO**, Vicepresidente del [Partito Democratico](http://www.partitodemocratico.it/prehome.htm) (**SOCIO SOSTENITORE**)

**Giancarlo SCHEGGI**, Segretario dell'[Associazione radicale Andrea Tamburi](http://www.radicalifirenze.it/wordpress/)

**Francesca SIVORI**, Counselor

**Mario STADERINI**, Segretario di _Radicali Italiani_

**Antonio STANGO**,  Dirigente di [Non c'è Pace Senza Giustizia](http://www.npwj.org/)

**                                      Sergio STANZANI**, Presidente del Senato                             del [Nonviolent‌ Radical‌ Party‌ transnational‌ and‌ transparty](http://www.radicalparty.org/)

**Alfredo STOPPA**, Avvocato

**Laura TERNI**, radicale storica

**Maria Gigliola TONIOLLO**_,_ [Cgil Nuovi Diritti](http://www.cgil.it/organizzazione/dipartimenti/Welfare.aspx)

**Maurizio TURCO**, Deputato radicale 

**Dario Vese**, [Studenti Coscioni](http://studenticoscioni.it/)

**Riccardo ZAPPA**, Artigiano (**SOCIO SOSTENITORE**)

**Franco ZAPPETTINI**, Mediatore interculturale

**Mina WELBY**, Associazione Luca Coscioni