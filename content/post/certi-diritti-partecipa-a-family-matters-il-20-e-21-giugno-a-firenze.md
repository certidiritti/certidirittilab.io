---
title: 'CERTI DIRITTI PARTECIPA A FAMILY MATTERS IL 20 E 21 GIUGNO A FIRENZE'
date: Sun, 01 Jun 2008 12:41:15 +0000
draft: false
tags: [Comunicati stampa]
---

Il 20 e 21 giugno si svogerà a Firenze il Convegno di chiusura del [progetto europeo **Family Matters**](http://www.daphnefamilymatters.net) , sostenere le famiglie per prevenire la violenza verso giovani gay e lesbiche,  
Family Matters è contenuto nel più ampio progetto Daphne; questo progetto ha avuto come capofila l'Università del Piemonte Orientale, altri partners sono stati [**ampgil**](http://www.ampgil.org/) (Spagna) e [**fflag**](http://www.fflag.org.uk/) (Regno Unito). Le due giornate vedranno la presenza di illustri ospiti internazionali  **la presentazione del video "Due volte Genitori" di [Claudio Cipeletti](http://www.agedo.org/video.html)** , sarà inoltre presentata l'ass. europea dei genitori di omosessuali [**Euroflag**](http://www.euroflag.be/).