---
title: 'Prostituzione: Alemanno ha fallito, basta demagogia'
date: Mon, 11 Jul 2011 20:00:00 +0000
draft: false
tags: [Politica]
---

**Il sindaco di Roma Alemanno dichiari di aver fallito nella lotta contro la prostituzione anzichè continuare ad usare la demagogia e chiedere una legge per vietare la porstituzione in strada. Le sue politiche sono ridicole di fronte al dramma di donne violentate e derubate a causa del proibizionismo sulla prostituzione.**

Roma, 11 luglio 2011

  
Dichiarazone di Mario Staderini e Sergio Rovasio, rispettivamente Segretario di Radicali Italiani e dell’Associazione Radicale Certi Diritti

“Il Sindaco di Roma dopo aver fatto quasi due anni fa un’ordinanza antiprostituzione, gia’ prorogata almeno 2 volte, con risultati fallimentari e costosissimi, e dopo aver dichiarato alcuni mesi fa di aver fallito perche’ il Governo non e’ intervenuto a sostegno della sua politica proibizionista, ha deciso di lanciare un’altra crociata contro la prostituzione. Questa volta girando come una ronda nella citta’. Anziche’ rendersi conto e ammettere del miserabile fallimento della sua politica proibizionista, ipocrita e fallimentare, questa volta chiede una legge che risolva l’irrisolvibile.

Vorremmo ricordare al Sindaco che  esiste gia’ un disegno di legge governativo, stranamente -e chissa' perche'-  congelato al Senato da alcuni mesi, su cui e’ calato un silenzio ipocrita e che e’ la brutta copia dell’ordinanza del Sindaco di Roma. Approvarlo equivarrebbe a moltiplicare ancora di piu’ questo fenomeno che verrebbe cosi’ sempre piu’ marginalizzato e sempre piu’ clandestinizzato con l’aumento delle violenze, degli stupri e dell’illegalita’ criminale.

L’unica soluzione sarebbe quella di approvare le proposte di legge dei Radicali che hanno l’obiettivo di regolamentare il fenomeno e rendere cosi’ l’Italia un paese piu’ civile, cosi’ come gia’ avviene in Olanda, Germania, Spagna, Belgio e molti altri paesi democratici, ridando cosi’ dignita’ alle persone che si prostituiscono, denigrate, offese e vittime di ogni tipo di violenza”.

**Il sindaco Alemanno aveva dichiarato:** "in vista dell'incontro che avrò con il ministro dell'interno Roberto Maroni ho voluto prendere visione direttamente della situazione in città. Ho preso la moto, sono andato da solo, ho girato per le strade per circa due ore".

"Ci sono molte strade che sono in ordine, dove non c'è nessun segnale inquietante, - spiega Gianni Alemanno - ci sono però dei punti, variabile a seconda dei giorni, dove c'è un addensamento di prostituzione. Credo - sottolinea - che sia assolutamente necessario approvare una legge che dichiari finalmente che la prostituzione in strada è un reato". E accusa: non c'è polizia, lo Stato faccia la sua parte

**Su Repubblica.it il video della sua ronda antiprostituzione in moto:**

[http://tv.repubblica.it/edizione/roma/le-ronde-anti-prostituzione-di-alemanno/72484?pagefrom=1&ref=HREC1-7](http://tv.repubblica.it/edizione/roma/le-ronde-anti-prostituzione-di-alemanno/72484?pagefrom=1&ref=HREC1-7)