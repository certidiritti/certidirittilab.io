---
title: 'Europride. Radicali: delegazione a Stoccolma'
date: Fri, 03 Aug 2018 13:24:25 +0000
draft: false
tags: [Europa]
---

[![IMG-20180803-WA0066](http://www.certidiritti.org/wp-content/uploads/2018/08/IMG-20180803-WA0066-225x300.jpg)](http://www.certidiritti.org/wp-content/uploads/2018/08/IMG-20180803-WA0066.jpg)Una delegazione radicale si trova in questi giorni a **Stoccolma** in occasione dell'**Europride 2018**. La delegazione è composta da **Leonardo Monaco**, segretario dell'Associazione Radicale Certi Diritti e membro di Giunta di Radicali Italiani; **Yuri Guaiana**, presidente di Certi Diritti; **Matteo Mainardi**, membro di Giunta dell'Associazione Luca Coscioni; **Giulia Crivellini**, direzione di Radicali Italiani; **Claudio Uberti**, direttivo di Certi Diritti e **Carlo Maresca** dell'Associszione Enzo Tortora.

Gli esponenti radicali, a margine della grande parata di liberazione sessuale e dell'orgoglio lgbti, parteciperanno alla **presentazione del network dei Liberali Lgbti in Europa**, di cui l'Associazione Radicale Certi Diritti è cofondatore.

Per **Leonardo Monaco** e **Yuri Guaiana**, «un'occasione di rilancio delle battaglie e delle conquiste di liberazione sessuale, troppo spesso messe in discussione dalla crescente ondata populista e autoritaria che sta investendo l'Europa. Certi Diritti e Radicali Italiani, la prima con il suo network transnazionale e la seconda con la sua partecipazione proattiva nell'Alde Party, sostengono l'iniziativa di questo nuovo strumento in mano alla comunità liberale, che non poteva non scegliere di nascere durante un Europride, che rivendica apertamente "un'Europa unita e aperta al mondo"».

[![IMG-20180803-WA0061](http://www.certidiritti.org/wp-content/uploads/2018/08/IMG-20180803-WA0061-300x225.jpg)](http://www.certidiritti.org/wp-content/uploads/2018/08/IMG-20180803-WA0061.jpg)