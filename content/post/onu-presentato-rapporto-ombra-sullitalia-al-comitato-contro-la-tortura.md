---
title: 'Onu: presentato rapporto ombra sull''Italia al Comitato contro la tortura'
date: Fri, 10 Nov 2017 13:39:00 +0000
draft: false
tags: [Transnazionale]
---

![phpn8LvhW8584](http://www.certidiritti.org/wp-content/uploads/2017/11/phpn8LvhW8584-300x191.jpg)"In vista della 62a sessione del Comitato contro la tortura delle Nazioni Unite, l'Associazione Radicale Certi Diritti ha coordinato la realizzazione di un rapporto ombra (_[Considerations on the LGBTI’s condition in Italy](http://www.certidiritti.org/wp-content/uploads/2017/11/Considerations-on-the-LGBTI’s-condition-in-Italy.docx.pdf)_) sottoscritto da OII-Italia, Associazione Luca Coscioni e Radicali Italiani sulla condizione delle persone LGBTI in Italia. Il documento in tre parti affronta il capitolo dei diritti umani delle persone intersex, in prospettiva di superare la pratica delle mutilazioni genitali neonatali e di realizzare un monitoraggio nazionale delle nascite intersex, la proibizione delle cosiddette 'terapie riparative' sui minori e il miglioramento delle condizioni di detenzione delle persone omosessuali, bisessuali e transessuali nonché l'affermazione del diritto all'affettività dietro le sbarre"

Così Leonardo Monaco e Yuri Guaiana, rispettivamente segretario e responsabile delle questioni trasnazionali dell'Associazione Radicale Certi Diritti.

"Il 13, 14 e 15 novembre il Governo Italiano sarà quindi chiamato a dare risposta a queste e ad altre considerazioni poste dalle Ong in sede di revisone. Certi Diritti, che confida nello strumento delle revisioni per alimentare un dibattito transnazionale sulle libertà sessuali e i diritti civili, ha voluto rimarcare come gli approcci sessuofobici e antiscientifici si concretizzino troppo spesso in vera e propria violenza sui corpi delle persone", concludono.