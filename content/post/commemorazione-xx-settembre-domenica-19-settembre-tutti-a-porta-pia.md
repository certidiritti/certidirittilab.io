---
title: 'COMMEMORAZIONE XX SETTEMBRE: DOMENICA 19 SETTEMBRE TUTTI A PORTA PIA!'
date: Wed, 30 Nov -0001 00:00:00 +0000
draft: false
tags: [Comunicati stampa]
---

**COMMEMORAZIONE XX SETTEMBRE:**

**LA CERIMONIA SI** **TERRA’ DOMENICA 19 SETTEMBRE ALLE ORE 12,30 A ROMA ALLA BRECCIA DI PORTA PIA DA OLTRE 20 ASSOCIAZIONI.**

La vera commemorazione della Breccia di Porta Pia verrà fatta da oltre 20 Associazioni e forze politiche Domenica 19 settembre, alle ore 12,30, a Roma, nel luogo dove il XX Settembre 1870 fu aperto  l’acccesso alla liberazione di Roma dal potere del Papa Re.

Qui di seguito trasmettiamo il testo della convocazione delle organizzazioni promotrici. Segnaliamo alla stampa che questa sarà la vera manifestazione commemorativa del XX settembre, alla presenza di cittadini, studiosi, parlamentari ed esponenti della società civile italiana.  

**150° anniversario dell’Unità d’Italia**

**200° anniversario nascita di Cavour**

**140° anniversario della Breccia di Porta Pia (XX Settembre 1870)**

**Manifestazione, alla Breccia di Porta Pia, Domenica 19 settembre, ore 12,30, contro ogni fondamentalismo dell’illibertà, della frattura tra la coscienza e la scienza. Per un nuovo Risorgimento laico e liberale.**

**Per la vera commemorazione del XX Settembre!**

\*\*\*

Il 20 settembre 1870, data della Breccia di Porta Pia e della liberazione di Roma dal potere pontificio,  segna l’inizio di una nuova libertà di pensiero, di coscienza e di religione che si offre al popolo italiano ed europeo.

Ridurlo, come in passato hanno provato il fascismo e la democrazia cristiana, ad un fatto d’armi con cui una nazione si conquistò una più prestigiosa capitale, è un falso storico non tollerabile da chi si professa democratico.

Ad essere battuta, il 20 settembre, fu l'ultima trincea della più assolutista e forcaiola concezione del potere e della società, che coagulava intorno a sé ogni sorta di ostilità alla civiltà moderna.

In occasione del 140° anniversario, a fronte delle tentazioni revisioniste portate avanti dalle autorità “ufficiali”,  le stesse che per decenni hanno operato una sistematica rimozione culturale della Roma laica e risorgimentale, vogliamo ricordarne il significato e il valore epocale nella storia italiana  ed internazionale, sul piano istituzionale, politico, laico e religioso.

Contro ogni fondamentalismo dell'illibertà, della violenza, della frattura tra la coscienza e la scienza.

Per un nuovo Risorgimento laico e  liberale.

** PROMOTORI, ADERENTI, PARTECIPANTI:**

\- Radicali Italiani

\- Associazione Radicale Certi Diritti

\- Anticlericale.net

\- Gruppo Consiliare Lista Bonino Pannella, Federalisti Europei alla Regione Lazio

\- Consulta Romana per la Laicità delle Istituzioni

\- Associazione Luca Coscioni

\- Associazione Radicali Roma

\- Arcigay Nazionale

\- Uaar Roma

\- Critica Liberale

\- Radio Radicale

\- Associazione nazionale del Libero Pensiero “Giordano Bruno”

\- Psi, Federazione Romana

\- Federazione dei Giovani Socialisti

\- Lettera Internazionale, Rivista europea

\- Associazione Libera Uscita

\- Lega Italiana Divorzio Breve

\- Cgil - Nuovi Diritti

\- Forum Donne Socialiste

\- Rete Laica Bologna

\- ItaliaLaica – giornale dei laici italiani

\- Associazione Democrazia Laica

\- Associazione democratica Giuditta Tavani Arquati

\- Federazione dei Verdi del Lazio

**_Organizzazione, ufficio stampa, adesioni:_**

**_[info@radicali.it](mailto:info@radicali.it)     Tel. 06-65937036_**

**_Sergio Rovasio_** **_– Diego Sabatinelli_**