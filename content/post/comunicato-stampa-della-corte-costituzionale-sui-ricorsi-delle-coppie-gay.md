---
title: 'COMUNICATO STAMPA DELLA CORTE COSTITUZIONALE SUI RICORSI DELLE COPPIE GAY'
date: Wed, 14 Apr 2010 11:15:07 +0000
draft: false
tags: [Comunicati stampa]
---

### **Corte Costituzionale**

### **Ufficio Stampa**

_Matrimonio tra persone dello stesso sesso_

_______La Corte costituzionale, decidendo sulle questioni poste con ordinanze del Tribunale di Venezia e della Corte d’appello di Trento, in relazione alle unioni omosessuali, ha dichiarato inammissibili le questioni stesse in riferimento agli artt. 2 e 117, I° comma, della Costituzione e infondate in relazione agli artt. 3 e 29 della Costituzione._______