---
title: 'CATANIA  SABATO 24-5 INIZIATIVA DI CERTI DIRITTI   '
date: Fri, 23 May 2008 14:35:09 +0000
draft: false
tags: [Comunicati stampa]
---

Domani 24 maggio in via Etnea presso bar Spinella dalle 16,00 alle 20,00 Certi  
Diritti Catania organizza tavolo informativo sulla battaglia del matrimonio gay in  
Italia . Sarà possibile firmare una petizione sulle unioni civili da presentare al  
comune di Catania.