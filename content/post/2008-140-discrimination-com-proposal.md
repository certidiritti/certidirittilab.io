---
title: '2008-140-Discrimination-COM-Proposal'
date: Wed, 02 Jul 2008 08:42:55 +0000
draft: false
tags: [Senza categoria]
---

FR
--

COMMISSION DES COMMUNAUTÉS EUROPÉENNES

Bruxelles, le 2.7.2008

COM(2008) 426 final

2008/0140 (CNS)

Proposition de

DIRECTIVE DU CONSEIL

relative à la mise en œuvre du principe de l'égalité de traitement entre les personnes sans distinction de religion ou de convictions, de handicap, d'âge ou d'orientation sexuelle  
  
  
{SEC(2008) 2180}  
{SEC(2008) 2181}

(présentée par la Commission)

  

EXPOSÉ DES MOTIFS

1.           Contexte de la proposition

**Motivation et objectifs de la proposition**

La présente proposition vise la mise en œuvre du principe de l'égalité de traitement entre les personnes sans distinction de religion ou de convictions, de handicap, d'âge ou d'orientation sexuelle, en dehors du marché du travail. Elle définit un cadre pour l'interdiction de toute discrimination fondée sur ces motifs et établit un niveau de protection minimal uniforme à l’intérieur de l’Union européenne pour les personnes victimes de telles discriminations.

La présente proposition complète le cadre juridique communautaire existant, qui ne prohibe la discrimination fondée sur la religion ou les convictions, un handicap, l'âge ou l'orientation sexuelle qu'en ce qui concerne l'emploi, le travail et la formation professionnelle[\[1\]](#_ftn1).

**Contexte général**

Dans son programme législatif et de travail adopté le 23 octobre 2007[\[2\]](#_ftn2), la Commission a annoncé qu'elle allait proposer de nouvelles initiatives afin de compléter le cadre juridique anti-discrimination de l'Union européenne.

La présente proposition est soumise dans le cadre de l’«Agenda social renouvelé: opportunités, accès et solidarité dans l'Europe du XXIe siècle»[\[3\]](#_ftn3) et accompagne la communication intitulée «Non-discrimination et égalité des chances: un engagement renouvelé»[\[4\]](#_ftn4).

La convention des Nations unies relative aux droits des personnes handicapées a été signée par les États membres et la Communauté européenne. Elle est fondée sur les principes de non-discrimination, de participation, d’intégration dans la société, d’égalité des chances et d’accessibilité. Une proposition de conclusion de la convention par la Communauté européenne a été soumise au Conseil[\[5\]](#_ftn5).

**Dispositions en vigueur dans le domaine de la proposition**

Cette proposition se situe dans le prolongement des directives 2000/43/CE, 2000/78/CE et 2004/113/CE[\[6\]](#_ftn6), qui interdisent la discrimination fondée sur le sexe, la race ou l’origine ethnique, un handicap, l'orientation sexuelle, la religion ou les convictions[\[7\]](#_ftn7). La discrimination fondée sur la race ou l’origine ethnique est interdite dans le cadre de l'emploi, du travail et de la formation professionnelle, ainsi que dans des domaines extérieurs à l’emploi tels que la protection sociale, les soins de santé, l’éducation et l’accès aux biens et services à la disposition du public, y compris le logement. La discrimination fondée sur le sexe est prohibée dans les mêmes domaines, à l'exception de l'éducation, des médias et de la publicité. La discrimination en fonction de l’âge, de la religion ou des convictions, de l’orientation sexuelle et du handicap est toutefois interdite uniquement dans le contexte de l'emploi, du travail et de la formation professionnelle.

Les directives 2000/43/CE et 2000/78/CE devaient être transposées en droit interne en 2003 au plus tard, à l’exception des dispositions relatives à la discrimination fondée sur l’âge et le handicap, pour lesquelles un délai supplémentaire de trois ans était accordé. Un rapport sur l'application de la directive 2000/43/CE a été adopté par la Commission en 2006[\[8\]](#_ftn8). L'adoption du rapport sur la mise en œuvre de la directive 2000/78/CE a eu lieu le 19 juin 2008[\[9\]](#_ftn9).  Tous les États membres sauf un ont transposé ces deux directives. Quant à la directive 2004/113/CE, elle devait être transposée d’ici fin 2007.

Dans la mesure du possible, les concepts et règles contenus dans la présente proposition s'appuient sur ceux des directives relevant de l'article 13 CE déjà existantes.

**Cohérence avec les autres politiques et les objectifs de l'Union**

Cette proposition s'inscrit dans la stratégie développée depuis le traité d'Amsterdam en vue de lutter contre la discrimination et concorde avec les objectifs horizontaux de l’Union européenne, notamment la stratégie de Lisbonne pour la croissance et l’emploi et les objectifs du processus de protection sociale et d’inclusion sociale de l’Union européenne. Elle aidera à renforcer les droits fondamentaux des citoyens, dans l’esprit de la Charte des droits fondamentaux de l’Union européenne.

2.           Consultation des parties intéressées et analyse d'impact

**Consultation**

Lors de la préparation de cette initiative, la Commission s'est efforcée d'associer toutes les parties potentiellement intéressées et a veillé à ce que celles qui souhaitaient formuler des observations aient la possibilité et le temps de répondre. L’Année européenne de l’égalité des chances pour tous a fourni une occasion unique de mettre l’accent sur ces questions et d’encourager la participation au débat.

Il convient tout particulièrement de mentionner la consultation publique en ligne[\[10\]](#_ftn10), une enquête sur le monde de l’entreprise[\[11\]](#_ftn11), ainsi qu’une consultation écrite et des réunions avec les partenaires sociaux et des ONG européennes actives dans le domaine de la lutte anti-discrimination[\[12\]](#_ftn12). Les résultats de la consultation publique et du sondage auprès des ONG en appelaient à la mise en place d'une législation au niveau communautaire afin de renforcer le niveau de protection contre la discrimination, tandis que certaines personnes interrogées se sont prononcées en faveur de directives spécifiques dans les domaines du handicap et de la discrimination fondée sur le sexe. La consultation du panel d’entreprises européennes a montré que ces dernières estiment qu’il serait utile d'avoir le même niveau de protection contre la discrimination dans toute l’Union européenne. Les partenaires sociaux représentant les employeurs étaient en principe hostiles à une nouvelle législation, estimant qu’elle entraînerait une augmentation de la bureaucratie et des coûts, tandis que les syndicats lui étaient favorables.

Les réponses à la consultation ont fait apparaître des inquiétudes sur le traitement qu'une nouvelle directive réserverait à un certain nombre de domaines sensibles et a également révélé des méprises sur les limites ou l'étendue des compétences de la Communauté. La directive proposée répond à ces inquiétudes et explicite les limites des compétences communautaires. À l'intérieur de ces limites, la Communauté a le pouvoir de statuer (article 13 du traité CE), et elle estime qu’une action au niveau de l’Union européenne constitue la meilleure manière de procéder.

Les réponses ont également mis l'accent sur le caractère spécifique de la discrimination liée au handicap et sur les mesures nécessaires pour y faire face. Celles-ci font l’objet d’un article spécifique.

D’aucuns ont indiqué qu’ils craignaient qu’une nouvelle directive n’entraîne des coûts pour les entreprises, mais il convient de souligner que la présente proposition s’appuie largement sur des concepts utilisés dans les directives existantes et qui sont bien connus des opérateurs économiques. Quant aux mesures visant la discrimination fondée sur le handicap, le concept d'aménagement raisonnable est familier aux entreprises depuis qu’il a été établi par la directive 2000/78/CE. La proposition de la Commission précise les facteurs à prendre en compte pour déterminer ce qui est «raisonnable».

Il a été relevé que, contrairement aux deux autres directives, la directive 2000/78/CE n’oblige pas les États membres à mettre en place des organismes chargés des questions d’égalité. La nécessité de lutter contre la discrimination multiple, par exemple en la définissant en tant que discrimination et en garantissant des voies de recours efficaces, a également été soulignée. Ces questions dépassent le cadre de la présente directive, mais rien n'empêche les États membres de prendre des mesures dans ces domaines.

Enfin, il a été signalé que la directive 2004/113/CE assurait une protection moins étendue contre la discrimination fondée sur le sexe que la directive 2000/43/CE, et qu’une nouvelle législation était donc nécessaire sur ce point. La Commission ne retient pas cette suggestion pour l’instant, étant donné que le délai de transposition de la directive 2004/113/CE vient tout juste d’expirer. Elle rendra toutefois compte de l’application de la directive en 2010 et pourra alors proposer des modifications, le cas échéant.

**Obtention et utilisation d'expertise**

Une étude[\[13\]](#_ftn13) réalisée en 2006 a montré que, d’un côté, la plupart des pays garantissent une forme ou une autre de protection juridique qui va au delà des exigences communautaires actuelles dans la plupart des domaines examinés mais que, d’un autre côté, le degré et la nature de cette protection varient fortement entre les pays. Cette étude a également montré que très peu de pays réalisaient des analyses d’impact ex ante en matière de législation anti-discrimination.

Une autre étude[\[14\]](#_ftn14) a examiné la nature et l’étendue de la discrimination en dehors de la sphère de l’emploi dans l'Union européenne et son coût potentiel (direct et indirect) pour les personnes et la société.

La Commission a en outre utilisé les rapports du Réseau européen des experts indépendants en matière de non-discrimination, notamment le document sur «Le développement de la législation contre les discriminations en Europe»[\[15\]](#_ftn15), ainsi qu’une étude intitulée «Lutte contre la discrimination multiple: pratiques, politiques et lois»[\[16\]](#_ftn16). Les résultats d’un Eurobaromètre spécial[\[17\]](#_ftn17) et d’un Flash Eurobaromètre réalisé en février 2008[\[18\]](#_ftn18) ont également fourni des informations utiles.

**Analyse d'impact**

Le rapport d’analyse d’impact[\[19\]](#_ftn19) a examiné l'existence de discriminations en dehors du marché du travail. Il est parvenu à la conclusion que, si la non-discrimination est reconnue comme l’une des valeurs fondamentales de l'Union européenne, le niveau de protection juridique en place pour défendre ces valeurs diffère dans la pratique en fonction des États membres et des motifs de discrimination. Il en résulte que les personnes exposées à la discrimination sont souvent moins à même de participer pleinement à la vie sociale et économique, ce qui a des répercussions négatives tant pour les personnes que pour la société en général.

Le rapport définit trois objectifs que toute initiative devrait atteindre:

*   améliorer la protection contre la discrimination;

*   garantir la sécurité juridique aux acteurs économiques et aux victimes potentielles dans l’ensemble des États membres;

*   renforcer l’inclusion sociale et encourager la participation pleine et entière de toutes les catégories de la population à la vie sociale et économique.

Parmi les nombreuses mesures recensées susceptibles d’aider à atteindre ces objectifs, six options ont été soumises à une analyse plus approfondie, consistant notamment dans l'absence de nouvelles mesures au niveau communautaire, l’autorégulation, la formulation de recommandations ou l'adoption d’une ou plusieurs directives interdisant la discrimination en dehors de la sphère de l’emploi.

En tout état de cause, les États membres devront appliquer la convention des Nations unies relative aux droits des personnes handicapées, qui définit le refus de procéder à des aménagements raisonnables comme une discrimination. Toute mesure juridiquement contraignante interdisant la discrimination fondée sur le handicap comporte un coût financier en raison des adaptations nécessaires mais présente également des avantages liés à une meilleure inclusion économique et sociale des catégories faisant actuellement l’objet d’une discrimination.

Le rapport conclut qu’une directive couvrant l'ensemble des motifs, conçue de manière à respecter les principes de subsidiarité et de proportionnalité, constituerait une réponse appropriée. Si un petit nombre d’États membres disposent déjà d'un arsenal législatif assez complet, la plupart n'offrent qu'une certaine protection, moins étendue. L’adaptation législative requise par de nouvelles règles communautaires serait donc variable.

La Commission a reçu de nombreuses plaintes faisant état de discriminations dans le secteur de l’assurance et de la banque. Mais le fait que les assureurs et les banques utilisent l'âge ou le handicap pour évaluer le profil de risque des clients ne constitue pas nécessairement une discrimination: tout est fonction du produit. La Commission va engager un dialogue avec les secteurs de la banque et de l'assurance et d'autres parties intéressées afin de parvenir à une meilleure compréhension commune des domaines dans lesquels l'âge ou le handicap constituent des facteurs pertinents à prendre en compte dans la conception et le coût des produits proposés par ces secteurs.

3.           aspects juridiques

**Base juridique**

La présente proposition est fondée sur l'article 13, paragraphe 1, du traité CE.

**Subsidiarité et proportionnalité**

Le principe de subsidiarité s’applique dans la mesure où la proposition ne relève pas de la compétence exclusive de la Communauté. Les objectifs de la proposition ne peuvent pas être réalisés de manière suffisante par les États membres agissant seuls, car seule une mesure à l’échelle communautaire peut garantir un niveau minimal de protection standard contre la discrimination fondée sur la religion ou les convictions, le handicap, l'âge ou l'orientation sexuelle dans tous les États membres. Un acte juridique communautaire assure la sécurité juridique en ce qui concerne les droits et obligations des acteurs économiques et des citoyens, y compris ceux qui se déplacent entre les États membres. L’expérience a montré que les directives précédemment adoptées en vertu de l’article 13, paragraphe 1, du traité CE avaient eu un effet positif en permettant de parvenir une meilleure protection contre la discrimination. Conformément au principe de proportionnalité, la directive proposée ne va pas au-delà de ce qui est nécessaire pour atteindre les objectifs définis.

De plus, les traditions et les approches nationales dans des domaines tels que la santé, la protection sociale et l'éducation ont tendance à différer davantage qu'en matière d'emploi. Ces domaines sont marqués par des choix de société légitimes dans des secteurs qui relèvent de la compétence nationale.

La diversité de ses sociétés est l'une des forces de l'Europe et doit être respectée conformément au principe de subsidiarité. Il est préférable que l'organisation et le contenu de l'éducation, la reconnaissance du statut marital ou familial, l'adoption, les droits de procréation et d'autres questions similaires soient tranchées au niveau national. La directive n'exige donc d'aucun État membre qu'il modifie ses lois et pratiques actuelles sur ces questions. Elle n'affecte pas non plus les règles nationales relatives aux activités des églises et autres organisations religieuses, ou les relations de celles-ci avec l'État. Les États membres conserveront ainsi leur liberté de décision sur des questions telles que l'autorisation de la sélection à l'entrée des établissements scolaires, l'interdiction ou le droit de porter ou d'exhiber des symboles religieux à l'école, la reconnaissance des mariages entre personnes du même sexe, et la nature de toute relation entre les religions constituées et l'État.

**Choix de l’instrument**

Une directive est l’instrument qui assure le mieux un niveau minimal et cohérent de protection contre la discrimination dans toute l’Union européenne, tout en permettant aux États membres désireux d’aller au-delà de ces normes minimales de le faire. Elle leur permet également de choisir les moyens de coercition et les sanctions les plus appropriés. L’expérience passée dans le domaine de la lutte contre la discrimination a montré qu'une directive était l'instrument le plus approprié.

**Tableau de correspondance**

Les États membres sont tenus de communiquer à la Commission le texte des dispositions nationales transposant la directive ainsi qu'un tableau de correspondance entre ces dispositions et la directive.

**Espace économique européen**

Ce texte présente de l'intérêt pour l'Espace économique européen et sera applicable aux États tiers membres de cet Espace à la suite d'une décision du comité mixte de l'EEE.

4.           Incidence budgétaire

La proposition n’a pas d’incidence sur le budget de la Communauté.

5.           Explication détaillée des différentes dispositions

Article premier: Objet

L’objectif principal de la directive est de lutter contre la discrimination fondée sur la religion ou les convictions, l’âge ou l’orientation sexuelle et de mettre en œuvre le principe de l’égalité de traitement dans d'autres domaines que l'emploi. La directive n’interdit pas les différences de traitement fondées sur le sexe, qui sont couvertes par les articles 13 et 141 du traité CE et la législation dérivée connexe.

Article 2: Concept de discrimination

La définition du principe de l’égalité de traitement est fondée sur celle contenue dans les directives précédemment adoptées en vertu de l’article 13, paragraphe 1, du traité CE \[ainsi que dans la jurisprudence de la Cour de justice des Communautés européennes en la matière\].

La discrimination directe consiste à réserver un traitement différent à une personne uniquement en raison de son âge, d’un handicap, de sa religion ou de ses convictions ou de son orientation sexuelle. La discrimination indirecte est plus complexe en ceci qu'une règle ou une pratique apparemment neutre désavantage en fait particulièrement une personne ou une catégorie de personnes possédant une caractéristique spécifique. L’auteur de la règle ou de la pratique n'a peut-être aucune idée des conséquences pratiques, et l'intention de discriminer n’entre donc pas en ligne de compte. Tout comme dans les directives 2000/43/CE, 2000/78/CE et 2002/73/CE[\[20\]](#_ftn20), il est possible de justifier la discrimination indirecte (si «cette disposition, ce critère ou cette pratique \[est\] objectivement justifié par un objectif légitime et que les moyens de réaliser cet objectif \[sont\] appropriés et nécessaires»).

Le harcèlement est une forme de discrimination. Le comportement non désiré peut prendre différentes formes allant de réflexions orales ou écrites à des gestes ou un comportement, mais il doit être suffisamment grave pour créer un environnement intimidant, humiliant ou offensant. Cette définition est identique à celles contenues dans les autres directives adoptées en vertu de l’article 13.

Le refus de réaliser des aménagements raisonnables est considéré comme une forme de discrimination, ce qui concorde avec la convention des Nations unies relative au droit des personnes handicapées et avec la directive 2000/78/CE. Certaines différences de traitement fondées sur l’âge peuvent être légales si elles sont justifiées par un but légitime et que les moyens de parvenir à ce but sont appropriés et nécessaires (critère de proportionnalité).

Dans les directives existantes au titre de l’article 13, des exceptions à l'interdiction de la discrimination directe ont été autorisées en cas d’«exigence professionnelle essentielle et déterminante» en ce qui concerne les différences de traitement fondées sur l’âge, la discrimination en fonction du sexe et l’accès aux biens et services. Bien que la proposition actuelle ne couvre pas le domaine de l’emploi, des différences de traitement dans les domaines mentionnés à l'article 3 devront être autorisées. Toutefois, étant donné que les exceptions au principe général d’égalité doivent être étroitement circonscrites, le double critère exigeant à la fois l'existence d'un objectif justifié et des moyens proportionnés d’y parvenir (c'est-à-dire aussi peu discriminatoires que possible) est nécessaire.

Une disposition spéciale est ajoutée à l'intention des services d’assurance et de banque, compte tenu du fait que l’âge et le handicap peuvent constituer un élément essentiel de l’évaluation du risque pour certains produits, et donc du prix. S'il était totalement interdit aux assureurs de tenir compte de l’âge et du handicap, les coûts additionnels seraient entièrement à la charge des autres assurés, ce qui conduirait à des coûts globaux plus élevés et à une offre de couverture plus réduite pour les consommateurs. L’utilisation de l’âge et du handicap dans l’évaluation des risques doit cependant être fondée sur des données et des statistiques précises.

La directive n’affecte pas les mesures nationales concernant la sécurité publique, l’ordre public, la prévention des infractions pénales, la protection de la santé et les droits et libertés d’autrui.

Article 3: Champ d’application

La discrimination en fonction de la religion ou des convictions, du handicap, de l’âge ou de l’orientation sexuelle est interdite tant dans le secteur public que dans le secteur privé, dans les domaines suivants:

*   la protection sociale, y compris la sécurité sociale et les soins de santé;

*   les avantages sociaux;

*   l’éducation;

*   l'accès aux biens et services et la fourniture de biens et services à la disposition du public, y compris en matière de logement.

En termes d’accès aux biens et services, seules les activités professionnelles ou commerciales sont couvertes. En d’autres termes, les transactions entre les personnes privées agissant à titre privé ne sont pas couvertes: il n’y a pas lieu de traiter la location d’une chambre dans une habitation privée de la même manière que la location de chambres dans un hôtel. Les différents domaines sont couverts uniquement dans la mesure où une question relève des compétences de la Communauté. Ainsi, l’organisation du système et des activités scolaires et le contenu de l'enseignement, y compris la manière d'organiser l'enseignement pour les personnes handicapées, relèvent de la compétence des États membres et ces derniers peuvent prévoir des différences de traitement dans l’accès aux établissements d'enseignement religieux. Une école peut par exemple organiser une présentation spécialement réservée aux enfants d'un certain âge, tandis qu'un établissement fondé sur la foi peut être autorisé à organiser des excursions scolaires sur un thème religieux.

Le texte indique clairement que les questions concernant l’état matrimonial ou familial, y compris l’adoption, ne relèvent pas du champ d'application de la directive. Cela inclut les droits en matière de procréation. Les États membres demeurent libres de décider ou non de l’institution et de la reconnaissance de partenariats _officiellement_ enregistrés. Toutefois, dès lors que le droit interne reconnaît de telles relations comme étant comparables au mariage, le principe de l'égalité de traitement s'applique[\[21\]](#_ftn21).

L’article 3 précise que la directive ne couvre ni les lois nationales relatives au caractère laïque de l’État et de ses institutions, ni le statut des organisations religieuses. Les États membres sont donc libres d'autoriser ou d'interdire le port de symboles religieux à l'école. La directive ne couvre pas non plus les différences de traitement fondées sur la nationalité.

_Article 4: Égalité de traitement des personnes handicapées_

L’accès effectif des personnes handicapées à la protection sociale, aux avantages sociaux, aux soins de santé et à l’éducation, ainsi que l'accès aux biens et services et la fourniture de biens et services à la disposition du public pour ces personnes, y compris en matière de logement, seront prévus à l’avance. Une clause d’exception prévoit l'exemption de cette obligation si celle-ci impose une charge disproportionnée ou des modifications majeures du produit ou du service.

Dans certains cas, des mesures individuelles d’aménagement raisonnable peuvent s'avérer nécessaires pour garantir l'accès effectif d'une personne handicapée en particulier. Là encore, uniquement à condition qu’une telle mesure n'impose pas une charge disproportionnée. Une liste non exhaustive de facteurs susceptibles d'être pris en compte pour déterminer si la charge est disproportionnée est dressée, permettant ainsi de tenir compte de la situation des petites et moyennes entreprises et des micro-entreprises.

Le concept d'aménagement raisonnable existe déjà dans le domaine de l'emploi, au titre de la directive 2000/78/CE, et les États membres et les entreprises ont donc déjà l'expérience de son application. Une mesure appropriée pour une grande compagnie ou un organisme public ne l'est pas forcément pour une petite ou moyenne entreprise. L'obligation de procéder à des aménagements raisonnables ne suppose pas uniquement des modifications matérielles, mais peut également signifier une manière différente de fournir un service.

_Article 5: Action positive_

Cette disposition est commune à toutes les directives fondées sur l'article 13. Il est clair que dans de nombreux cas, une égalité formelle n'entraîne pas une égalité dans la pratique. Il peut être nécessaire d'adopter des mesures spécifiques pour prévenir ou corriger des situations d'inégalité. Les États membres ont des traditions et des pratiques différentes en matière d'action positive, et cet article leur permet de prendre des mesures dans ce sens sans toutefois en faire une obligation.

Article 6: Prescriptions minimales

Cette disposition est commune à toutes les directives fondées sur l'article 13. Elle autorise les États membres à assurer un niveau de protection plus élevé que celui garanti par la directive, et confirme qu'il ne doit pas y avoir d'abaissement du niveau de protection contre la discrimination déjà accordé par les États membres lors de l'application de la directive.

Article 7: Défense des droits

Cette disposition est commune à toutes les directives fondées sur l'article 13. Les personnes doivent pouvoir faire respecter leur droit de ne pas être discriminées. Cet article dispose donc que les personnes qui estiment avoir été victimes de discrimination doivent pouvoir avoir recours à des procédures administratives ou judiciaires, même après la cessation de la relation dans le cadre de laquelle la discrimination est censée s'être produite, conformément à la décision de la Cour de justice dans l'affaire Coote[\[22\]](#_ftn22).

Le droit à une protection juridique efficace est renforcé par le fait d'autoriser les organisations qui ont un intérêt légitime à combattre la discrimination à aider les victimes de discrimination dans le cadre de procédures judiciaires ou administratives. Cette disposition est sans préjudice des règles nationales relatives aux délais impartis pour former un recours.

Article 8: Charge de la preuve

Cette disposition est commune à toutes les directives fondées sur l'article 13. Dans les procédures judiciaires, la règle générale est que toute allégation doit être prouvée. Dans les affaires de discrimination, il est toutefois souvent extrêmement difficile d'obtenir les preuves nécessaires, étant donné que celles-ci se trouvent souvent entre les mains du défendeur. Ce problème a été reconnu par la Cour de justice des Communautés européennes[\[23\]](#_ftn23) et par le législateur communautaire dans la directive 97/80/CE[\[24\]](#_ftn24).

Le renversement de la charge de la preuve s'applique à toutes les affaires dans lesquelles une violation du principe de l'égalité de traitement est alléguée, y compris celles qui impliquent des associations et des organisations au sens de l'article 7, paragraphe 2. Tout comme dans les directives précédentes, ce renversement de la charge de la preuve ne s'applique pas aux situations dans lesquelles une discrimination supposée est poursuivie au pénal.

Article 9: Protection contre les rétorsions

Cette disposition est commune à toutes les directives fondées sur l'article 13. Une protection juridique efficace doit inclure une protection contre les représailles. Une victime peut être dissuadée d'exercer ses droits en raison du risque de représailles, il est donc nécessaire de protéger les personnes contre tout traitement défavorable résultant de l'exercice des droits conférés par la directive. Cet article est identique dans les directives 2000/43/CE et 2000/78/CE.

Article 10: Diffusion de l’information

Cette disposition est commune à toutes les directives fondées sur l'article 13. L'expérience et les sondages montrent que les personnes sont mal ou insuffisamment informées de leurs droits. Plus le système d'information publique et de prévention est efficace, moins les recours individuels seront nécessaires. Cet article reproduit les dispositions correspondantes des directives 2000/43/CE, 2000/78/CE et 2004/113/CE.

Article 11: Dialogue avec les parties intéressées

Cette disposition est commune à toutes les directives fondées sur l'article 13. Elle vise à encourager le dialogue entre les pouvoirs publics concernés et des organismes tels que les organisations non gouvernementales qui ont un intérêt légitime à contribuer à la lutte contre la discrimination fondée sur la religion ou les convictions, le handicap, l'âge ou l'orientation sexuelle. Une disposition similaire est contenue dans les directives anti-discrimination précédentes.

Article 12: Organismes de promotion de l'égalité de traitement

Cette disposition est commune à deux directives fondées sur l'article 13. Elle prévoit que les États membres doivent disposer au niveau national d'un ou plusieurs organismes («Organisme chargé des questions d'égalité») responsables de promouvoir l'égalité de traitement entre toutes les personnes, sans discrimination fondée sur la religion ou les convictions, le handicap, l’âge ou l’orientation sexuelle.

Cet article reproduit les dispositions de la directive 2000/43/CE dans la mesure où celles-ci concernent l'accès aux biens et services et la fourniture de biens et services, et s'appuie sur des dispositions équivalentes contenues dans les directives 2002/73/CE[\[25\]](#_ftn25) et 2004/113/CE. Il définit les compétences minimales d'organismes au niveau national devant agir de manière indépendante afin de promouvoir le principe de l'égalité de traitement. Les États membres peuvent décider que ces organismes seront les mêmes que ceux déjà établis en application des directives précédentes.

Il est à la fois difficile et coûteux pour les particuliers d'intenter un recours en justice s'ils estiment avoir fait l'objet d'une discrimination. Un des rôles clés des organismes chargés des questions d'égalité est d'apporter une aide indépendante aux victimes de discrimination. Ils doivent également être en mesure de procéder à des études indépendantes sur la discrimination et de publier des rapports et des recommandations sur des questions liées à la discrimination.

Article 13: Conformité

Cette disposition est commune à toutes les directives fondées sur l'article 13. L'égalité de traitement suppose l'élimination de toute discrimination résultant de dispositions législatives, réglementaires ou administratives, et la directive impose donc aux États membres de supprimer toute disposition de ce type. Comme dans la législation antérieure, la directive exige également que toute disposition contraire au principe de l'égalité de traitement soit déclarée nulle et non avenue ou modifiée, ou qu'elle puisse l'être en cas de contestation.

Article 14: Sanctions

Cette disposition est commune à toutes les directives fondées sur l'article 13. Conformément à la jurisprudence de la Cour de justice[\[26\]](#_ftn26), le texte exclut la fixation de tout plafond maximal pour les dédommagements dus en cas de violation du principe de l'égalité de traitement. Cette disposition ne requiert pas l'introduction de sanctions pénales.

Article 15: Mise en œuvre

Cette disposition est commune à toutes les directives fondées sur l'article 13. Elle accorde aux États membres un délai de deux ans pour transposer la directive en droit interne et communiquer les textes de leur législation nationale à la Commission. Les États membres peuvent disposer que l'obligation d'assurer un accès effectif aux personnes handicapées ne s'appliquera que quatre ans après l'adoption de la directive.

_Article 16: Rapport_

Cette disposition est commune à toutes les directives fondées sur l'article 13. Elle prévoit que la Commission doit soumettre au Parlement européen et au Conseil un rapport sur l'application de la directive, sur la base des informations reçues des États membres. Ce rapport tiendra compte de l'opinion des partenaires sociaux, des ONG concernées et de l'Agence des droits fondamentaux de l'Union européenne.

_Article 17: Entrée en vigueur_

Cette disposition est commune à toutes les directives fondées sur l'article 13. La directive entrera en vigueur le jour de sa publication au Journal officiel.

_Article 18: Destinataires_

Cette disposition, commune à toutes les directives fondées sur l'article 13, précise que la directive est destinée aux États membres.

  

2008/0140 (CNS)

Proposition de

DIRECTIVE DU CONSEIL

relative à la mise en œuvre du principe de l'égalité de traitement entre les personnes sans distinction de religion ou de convictions, de handicap, d'âge ou d'orientation sexuelle

LE CONSEIL DE L’UNION EUROPÉENNE,

vu le traité instituant la Communauté européenne, et notamment son article 13, paragraphe 1,

vu la proposition de la Commission[\[27\]](#_ftn27),

vu l'avis du Parlement européen[\[28\]](#_ftn28),

vu l'avis du Comité économique et social européen[\[29\]](#_ftn29),

vu l'avis du Comité des régions[\[30\]](#_ftn30),

considérant ce qui suit:

(1)               Conformément à l'article 6 du traité sur l'Union européenne, l'Union européenne est fondée sur les principes de liberté, de démocratie, de respect des droits de l'homme et des libertés fondamentales ainsi que de l'État de droit – principes qui sont communs à tous les États membres – et respecte les droits fondamentaux tels qu'ils sont garantis par la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et tels qu'ils résultent des traditions constitutionnelles communes aux États membres, en tant que principes généraux du droit communautaire.

(2)               Le droit de tout individu à l'égalité devant la loi et à la protection contre la discrimination constitue un droit universel reconnu par la Déclaration universelle des droits de l'homme, par la Convention des Nations unies sur l'élimination de toutes les formes de discrimination à l'égard des femmes, par la Convention internationale sur l'élimination de toutes les formes de discrimination raciale, par les pactes des Nations unies relatifs aux droits civils et politiques et aux droits économiques, sociaux et culturels, par la Convention des Nations unies relative aux droits des personnes handicapées, par la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et par la Charte sociale européenne, signés par \[tous\] les États membres. En particulier, la Convention des Nations unies relative aux droits des personnes handicapées inclut le refus d’aménagement raisonnable dans sa définition de la discrimination.

(3)               La présente directive respecte les droits fondamentaux et observe les principes fondamentaux reconnus, en particulier, par la Charte des droits fondamentaux de l'Union européenne. L’article 10 de cette charte reconnaît le droit à la liberté de pensée, de conscience et de religion;  l’article 21 exclut toute discrimination, y compris la discrimination fondée sur la religion ou les convictions, un handicap, l'âge ou l'orientation sexuelle, et l’article 26 reconnaît le droit des personnes handicapées à bénéficier de mesures visant à assurer leur autonomie.

(4)               L’Année européenne des personnes handicapées en 2003, l'Année européenne de l'égalité des chances pour tous en 2007 et l'Année européenne du dialogue interculturel en 2008 ont mis en évidence la persistance des discriminations, mais aussi les bienfaits de la diversité.

(5)               Le Conseil européen de Bruxelles du 14 décembre 2007 a appelé les États membres à intensifier leurs efforts pour prévenir et combattre les discriminations sur le marché du travail et en dehors[\[31\]](#_ftn31).

(6)               Le Parlement européen a appelé à ce que la législation de l’Union européenne élargisse la protection contre les discriminations[\[32\]](#_ftn32).

(7)               La Commission européenne a affirmé, dans sa communication intitulée «Un Agenda social renouvelé: opportunités, accès et solidarité dans l'Europe du XXIe siècle»[\[33\]](#_ftn33), que dans des sociétés où tous les individus sont considérés comme égaux, aucune barrière artificielle ni discrimination d'aucune sorte ne devrait empêcher les individus d'exploiter les occasions qui s’offrent à eux.

(8)               La Communauté a adopté trois instruments juridiques[\[34\]](#_ftn34) sur la base de l’article 13, paragraphe 1, du traité CE, afin de prévenir et de combattre la discrimination fondée sur le sexe, la race et l’origine ethnique, la religion ou les convictions, le handicap, l’âge et l'orientation sexuelle. Ces instruments ont prouvé l’utilité de la législation dans la lutte contre la discrimination. Plus particulièrement, la directive 2000/78/CE établit un cadre général pour l’égalité de traitement en matière d’emploi et de travail au regard de la religion ou des convictions, du handicap, de l'âge et de l'orientation sexuelle. Toutefois, au-delà du domaine de l'emploi, des différences subsistent entre les États membres en ce qui concerne le degré et la forme de protection contre les discriminations fondées sur ces motifs.

(9)               La législation devrait donc interdire la discrimination fondée sur la religion ou les convictions, le handicap, l’âge ou l’orientation sexuelle dans une série de domaines au-delà du marché du travail, tels que la protection sociale, l’éducation, l’accessibilité et la fourniture des biens et services, y compris le logement. Elle devrait prévoir des mesures permettant de garantir l’égalité d’accès des personnes handicapées aux domaines couverts.

(10)           La directive 2000/78/CE interdit la discrimination dans l'accès à la formation professionnelle; il est nécessaire de compléter cette protection en étendant l'interdiction de discrimination aux formes d'éducation qui ne sont pas considérées comme de la formation professionnelle.

(11)           La présente directive devrait être sans préjudice des compétences des États membres en matière d’éducation, de sécurité sociale et de soins de santé. Elle devrait également être sans préjudice du rôle essentiel et du large pouvoir discrétionnaire des États membres pour ce qui est de la fourniture, de la commande et de l’organisation de services d’intérêt économique général.

(12)           La discrimination s’entend comme incluant les formes directes et indirectes de discrimination, le harcèlement, les comportements consistant à enjoindre de pratiquer une discrimination ainsi que le refus de procéder à des aménagements raisonnables.

(13)           Dans la mise en œuvre du principe de l'égalité de traitement sans distinction de religion ou de convictions, d’âge ou d’orientation sexuelle, la Communauté devrait, conformément à l'article 3, paragraphe 2, du traité CE, tendre à éliminer les inégalités et à promouvoir l'égalité entre les hommes et les femmes, en particulier du fait que les femmes sont souvent victimes de discrimination multiple.

(14)           L'appréciation des faits qui permettent de présumer l'existence d'une discrimination directe ou indirecte devrait revenir à l'instance judiciaire nationale ou à une autre instance compétente, conformément au droit national ou aux pratiques nationales, qui peuvent prévoir, en particulier, que la discrimination indirecte peut être établie par tous moyens, y compris les données statistiques.

(15)           Des facteurs actuariels et des facteurs de risque liés au handicap et à l’âge sont utilisés dans le cadre des services d'assurance, de banque et d’autres services financiers. Ces facteurs ne devraient pas être considérés comme des discriminations lorsqu’ils s’avèrent déterminants pour l'évaluation du risque.

(16)           Toute personne jouit de la liberté contractuelle, y compris de la liberté de choisir un cocontractant pour une transaction. La présente directive ne devrait pas s’appliquer aux transactions économiques réalisées par des particuliers pour lesquels ces transactions ne constituent pas une activité professionnelle ou commerciale.

(17)           Tout en interdisant la discrimination, il est important de respecter les autres libertés et droits fondamentaux, notamment la protection de la vie privée et familiale ainsi que les transactions qui se déroulent dans ce cadre, la liberté de religion et la liberté d’association. Cette directive est sans préjudice des législations nationales relatives à l’état matrimonial ou familial, et notamment aux droits en matière de procréation. Elle est également sans préjudice du caractère laïque de l’État, des institutions et organismes publics ou de l’éducation.

(18)           Les États membres sont responsables de l’organisation et du contenu de l’éducation. La communication de la Commission intitulée «Améliorer les compétences pour le XXIe siècle: un programme de coopération européenne en matière scolaire» souligne la nécessité d’accorder une attention particulière aux enfants défavorisés et aux enfants présentant des besoins particuliers en matière d’éducation. En particulier, les législations nationales peuvent permettre des différences s’agissant de l’accès aux établissements d’enseignement fondés sur la religion ou les convictions. Les États membres peuvent également autoriser ou interdire le port ou l'exhibition de symboles religieux dans les établissements scolaires.

(19)           L'Union européenne a reconnu explicitement dans sa déclaration n° 11 relative au statut des Églises et des organisations non confessionnelles, annexée à l'acte final du traité d'Amsterdam, qu'elle respecte et ne préjuge pas le statut dont bénéficient, en vertu du droit national, les Églises et les associations ou communautés religieuses dans les États membres et qu'elle respecte également le statut des organisations philosophiques et non confessionnelles. Les mesures destinées à permettre aux personnes handicapées de jouir d’un accès effectif et non discriminatoire aux secteurs couverts par la présente directive contribuent largement à assurer la pleine égalité en pratique. Par ailleurs, des mesures individuelles d’aménagement raisonnable peuvent être requises dans certains cas pour garantir un tel accès aux personnes handicapées. En tout état de cause, aucune mesure qui représenterait une charge disproportionnée n’est requise. Afin d’évaluer si la charge est disproportionnée, il convient de tenir compte de plusieurs facteurs, dont la taille, les ressources et la nature de l’organisation. Le principe d’aménagement raisonnable et de charge disproportionnée est établi dans la directive 2000/78/CE et dans la Convention des Nations unies relative aux droits des personnes handicapées.

(20)           Des exigences et des normes légales d'accessibilité[\[35\]](#_ftn35) ont été définies au niveau européen pour certains domaines, tandis que l'article 16 du règlement (CE) n° 1083/2006 du 11 juillet 2006 sur le Fonds européen de développement régional, le Fonds social européen et le Fonds de cohésion, abrogeant le règlement (CE) n° 1260/1999[\[36\]](#_ftn36), dispose que l'accessibilité pour les personnes handicapées est l'un des critères à respecter lors de la définition d'opérations cofinancées par les Fonds. Le Conseil a également souligné la nécessité d’adopter des mesures pour garantir l'accès des personnes handicapées aux infrastructures et activités culturelles[\[37\]](#_ftn37).

(21)           L'interdiction de discrimination devrait être sans préjudice du maintien ou de l'adoption, par les États membres, de mesures destinées à prévenir ou à compenser les désavantages que connaissent certains groupes du fait de leur religion ou de leurs convictions, de leur handicap, de leur âge ou de leur orientation sexuelle. Ces mesures peuvent autoriser l'existence d'organisations de personnes d'une religion ou de convictions, d'un handicap, d'un âge ou d'une orientation sexuelle donnés lorsque leur objet principal est la promotion des besoins spécifiques de ces personnes.

(22)           La présente directive fixe des exigences minimales, laissant aux États membres la liberté d’adopter ou de maintenir des dispositions plus favorables. La mise en œuvre de la présente directive ne devrait pas justifier une régression par rapport à la situation existant dans chaque État membre.

(23)           Les personnes qui ont fait l'objet d'une discrimination fondée sur la religion ou les convictions, un handicap, l'âge ou l'orientation sexuelle devraient disposer de moyens de protection juridique adéquats. Pour assurer un niveau de protection plus efficace, les associations, les organisations et les autres entités juridiques devraient être habilitées à engager une procédure, y compris au nom ou à l'appui d'une victime, sans préjudice des règles de procédure nationales relatives à la représentation et à la défense devant les juridictions.

(24)           L'aménagement des règles concernant la charge de la preuve s'impose dès qu'il existe une présomption de discrimination et, dans les cas où cette situation se vérifie, l’application effective du principe de l'égalité de traitement requiert que la charge de la preuve revienne à la partie défenderesse. Toutefois, il n'incombe pas à la partie défenderesse de prouver que la partie demanderesse adhère à une religion donnée ou possède des convictions données, présente un certain handicap, a un âge spécifique ou une orientation sexuelle donnée.

(25)           L’application effective du principe de l'égalité de traitement requiert une protection judiciaire adéquate contre les rétorsions.

(26)           Dans sa résolution sur le suivi de l'Année européenne de l'égalité des chances pour tous (2007), le Conseil a appelé à associer pleinement la société civile, notamment les organisations qui représentent les groupes de population exposés à la discrimination, les partenaires sociaux et les parties prenantes, à l'élaboration des politiques et des programmes visant à prévenir la discrimination et à promouvoir l'égalité de traitement et l'égalité des chances, tant au niveau européen qu'au niveau national.

(27)           L’expérience acquise dans l’application des directives 2000/43/CE et 2004/113/CE montre que la protection contre la discrimination fondée sur les motifs visés à la présente directive serait renforcée par l'existence, dans chaque État membre, d'un ou de plusieurs organismes ayant compétence pour analyser les problèmes en cause, étudier les solutions possibles et apporter une assistance concrète aux victimes.

(28)           En exerçant leurs pouvoirs et en assumant leurs responsabilités au titre de la présente directive, ces organismes devraient agir de manière compatible avec les principes de Paris définis par les Nations unies en ce qui concerne le statut et le fonctionnement des institutions nationales pour la protection et la promotion des droits de l'homme.

(29)           Les États membres devraient mettre en place des sanctions effectives, proportionnées et dissuasives applicables en cas de non-respect des obligations découlant de la présente directive.

(30)           Conformément au principe de subsidiarité et au principe de proportionnalité tels qu'énoncés à l'article 5 du traité CE, l'objectif de la présente directive, à savoir assurer un niveau commun de protection contre la discrimination dans tous les États membres, ne peut pas être réalisé de manière suffisante par les États membres et peut donc, en raison des dimensions et des effets de l'action envisagée, être mieux réalisé au niveau communautaire. La présente directive n'excède pas ce qui est nécessaire pour atteindre ces objectifs.

(31)           Conformément au paragraphe 34 de l'accord interinstitutionnel «Mieux légiférer», les États membres sont encouragés à établir, pour eux-mêmes et dans l'intérêt de la Communauté, leurs propres tableaux illustrant, dans la mesure du possible, la concordance entre la directive et les mesures de transposition, et à les rendre publics.

A ARRÊTÉ LA PRÉSENTE DIRECTIVE:

Chapitre 1  
DISPOSITIONS GÉNÉRALES

Article premier  
Objet

La présente directive instaure un cadre pour lutter contre la discrimination fondée sur la religion ou les convictions, le handicap, l'âge ou l'orientation sexuelle, en vue de mettre en œuvre, dans les États membres, le principe de l'égalité de traitement dans d’autres domaines que l'emploi et le travail.

Article 2  
Concept de discrimination

1\. Aux fins de la présente directive, on entend par «principe de l'égalité de traitement» l'absence de toute discrimination directe ou indirecte fondée sur un des motifs visés à l'article 1er.

2\. Aux fins du paragraphe 1:

a) une discrimination directe est réputée se produire lorsqu'une personne est traitée de manière moins favorable qu'une autre ne l'est, ne l'a été ou ne le serait dans une situation comparable, sur la base de l'un des motifs visés à l'article 1er;

b) une discrimination indirecte est réputée se produire lorsqu'une disposition, un critère ou une pratique apparemment neutre est susceptible d'entraîner, pour des personnes ayant une religion ou des convictions, un handicap, un âge ou une orientation sexuelle donnés, un désavantage particulier par rapport à d'autres personnes, à moins que cette disposition, ce critère ou cette pratique ne soit objectivement justifié par un objectif légitime et que les moyens de réaliser cet objectif ne soient appropriés et nécessaires.

3\. Le harcèlement est considéré comme une forme de discrimination au sens du paragraphe 1 lorsqu'un comportement indésirable lié à l'un des motifs visés à l'article 1er se manifeste, qui a pour objet ou pour effet de porter atteinte à la dignité d'une personne et de créer un environnement intimidant, hostile, dégradant, humiliant ou offensant.

4\. Tout comportement consistant à enjoindre à quiconque de pratiquer une discrimination à l'encontre de certaines personnes pour l'un des motifs visés à l'article 1er est considéré comme une discrimination au sens du paragraphe 1.

5\. Le refus de réaliser un aménagement raisonnable dans une situation donnée comme le prévoit l’article 4, paragraphe 1, point b), de la présente directive, au bénéfice de personnes handicapées, est considéré comme une discrimination au sens du paragraphe 1.

6\. Nonobstant le paragraphe 2, les États membres peuvent prévoir que les différences de traitement fondées sur l'âge ne constituent pas une discrimination lorsqu'elles sont justifiées, dans le cadre du droit national, par un objectif légitime et que les moyens de réaliser cet objectif sont appropriés et nécessaires. En particulier, la présente directive n’exclut pas la fixation d’un âge spécifique pour accéder aux prestations sociales, à l’éducation et à certains biens ou services.

7\. Nonobstant le paragraphe 2, en ce qui concerne la fourniture de services financiers, les États membres peuvent être autorisés à instaurer des différences proportionnées de traitement lorsque, pour le produit en question, l’utilisation de l’âge ou d'un handicap constitue un facteur déterminant pour l’évaluation du risque, sur la base de données actuarielles ou statistiques précises et pertinentes.

8\. La présente directive est sans préjudice des mesures générales prévues par la législation nationale qui, dans une société démocratique, sont nécessaires à la sécurité publique, au maintien de l’ordre public et à la prévention des infractions pénales, à la protection de la santé et à la protection des droits et libertés d'autrui.

Article 3  
Champ d’application

1\. Dans les limites des compétences conférées à la Communauté, l’interdiction de discrimination s'applique à toutes les personnes, tant dans le secteur public que dans le secteur privé, y compris dans les organismes publics, en ce qui concerne:

a) la protection sociale, y compris la sécurité sociale et les soins de santé;

b) les avantages sociaux;

c) l’éducation;

d) l'accès aux biens et aux services et la fourniture de biens et services mis à la disposition du public, y compris en matière de logement.

Le point d) s'applique aux particuliers uniquement dans la mesure où ceux-ci exercent une activité professionnelle ou commerciale.

2\. La présente directive est sans préjudice des législations nationales relatives à l’état matrimonial ou familial et aux droits en matière de procréation.

3\. La présente directive est sans préjudice des responsabilités des États membres en ce qui concerne le contenu, les activités et l’organisation de leurs systèmes d’éducation, y compris en matière d'éducation répondant à des besoins spécifiques. Les États membres peuvent permettre des différences de traitement s'agissant de l'accès aux établissements d’enseignement fondés sur la religion ou les convictions.

4\. La présente directive est sans préjudice de la législation nationale qui garantit la laïcité de l’État, des institutions et organismes publics ou de l’éducation, ou qui concerne le statut et les activités des Églises et autres organisations fondées sur la religion ou sur certaines convictions. Elle est également sans préjudice de la législation nationale qui promeut l’égalité entre hommes et femmes.

5\. La présente directive ne couvre pas les différences de traitement fondées sur la nationalité et s'entend sans préjudice des dispositions et conditions relatives à l'admission et au séjour des ressortissants de pays tiers et des apatrides sur le territoire des États membres et de tout traitement lié au statut juridique des ressortissants de pays tiers et des apatrides concernés.

Article 4  
Égalité de traitement des personnes handicapées

1\. Afin de garantir le respect du principe de l'égalité de traitement à l'égard des personnes handicapées:

a) sont prévues de manière anticipative, entre autres par des modifications et des ajustements appropriés, les mesures nécessaires pour permettre aux personnes handicapées de jouir d’un accès effectif et non discriminatoire à la protection sociale, aux avantages sociaux, aux soins de santé et à l’éducation, ainsi que de l'accès aux biens et services et la fourniture des biens et services mis à la disposition du public, y compris en matière de logement et de transports. Ces mesures ne devraient pas imposer de charge disproportionnée ou nécessiter de modification fondamentale de la protection sociale, des avantages sociaux, des soins de santé, de l’éducation ou des biens et services concernés, ni de substitution de ces biens et services;

b) sans préjudice de l’obligation d’assurer un accès effectif et non discriminatoire et si un cas particulier le requiert, des aménagements raisonnables devront être effectués à moins que cette obligation ne représente une charge disproportionnée.

2\. Afin d’évaluer si les mesures nécessaires à l'application du paragraphe 1 représentent une charge disproportionnée, il est en particulier tenu compte de la taille et des ressources de l’organisation, de sa nature, du coût estimé, du cycle de vie des biens et services et des avantages potentiels d’une meilleure accessibilité pour les personnes handicapées. La charge n'est pas disproportionnée lorsqu'elle est compensée de façon suffisante par des mesures s’inscrivant dans le cadre de la politique d’égalité de traitement menée par l'État membre concerné.

3\. La présente directive est sans préjudice des dispositions communautaires ou nationales portant sur l’accessibilité de biens ou services spécifiques.

Article 5  
Action positive

En vue d’assurer la pleine égalité en pratique, le principe de l'égalité de traitement n'empêche pas un État membre de maintenir ou d'adopter des mesures spécifiques pour prévenir ou compenser des désavantages liés à la religion ou aux convictions, au handicap, à l’âge ou à l’orientation sexuelle.

Article 6  
Prescriptions minimales

1\. Les États membres peuvent adopter ou maintenir des dispositions plus favorables à la protection du principe de l'égalité de traitement que celles prévues par la présente directive.

2\. La mise en œuvre de la présente directive ne peut en aucun cas constituer un motif d'abaissement du niveau de protection contre la discrimination déjà assuré par les États membres dans les domaines régis par la présente directive.

CHAPITRE II  
VOIES DE RECOURS ET APPLICATION DU DROIT

Article 7  
Défense des droits

1\. Les États membres veillent à ce que des procédures judiciaires et/ou administratives, y compris, lorsqu'ils l'estiment approprié, des procédures de conciliation, visant à faire respecter les obligations découlant de la présente directive soient accessibles à toutes les personnes qui s'estiment lésées par le non-respect à leur égard du principe de l'égalité de traitement, même après la cessation de la relation dans laquelle la discrimination est présumée s'être produite.

2\. Les États membres veillent à ce que les associations, les organisations ou les autres entités juridiques qui ont un intérêt légitime à assurer le respect des dispositions de la présente directive puissent, pour le compte ou à l'appui du plaignant, avec son approbation, engager toute procédure judiciaire et/ou administrative prévue pour faire respecter les obligations découlant de la présente directive.

3\. Les paragraphes 1 et 2 sont sans préjudice des règles nationales relatives aux délais impartis pour former un recours en ce qui concerne le principe de l'égalité de traitement.

Article 8  
Charge de la preuve

1\. Les États membres prennent les mesures nécessaires, conformément à leur système judiciaire, afin que, dès lors qu’une personne s’estime lésée par le non-respect à son égard du principe de l’égalité de traitement et établit, devant une juridiction ou une autre instance compétente, des faits qui permettent de présumer l’existence d’une discrimination directe ou indirecte, il incombe à la partie défenderesse de prouver qu’il n’y a pas eu violation de l’interdiction de discrimination.

2\. Le paragraphe 1 n'empêche pas les États membres d'adopter des règles de la preuve plus favorables aux plaignants.

3\. Le paragraphe 1 ne s’applique pas aux procédures pénales.

4\. Les États membres peuvent ne pas appliquer le paragraphe 1 aux procédures dans lesquelles la juridiction ou l'instance compétente procède à l'instruction des faits.

5\. Les paragraphes 1, 2, 3 et 4 s'appliquent également à toute procédure engagée conformément à l'article 7, paragraphe 2.

Article 9  
Protection contre les rétorsions

Les États membres introduisent dans leur système juridique interne les mesures nécessaires pour protéger les personnes contre tout traitement ou toute conséquence défavorable faisant suite à une plainte ou à une action en justice destinée à faire respecter le principe de l'égalité de traitement.

Article 10  
Diffusion de l’information

Les États membres veillent à ce que les dispositions adoptées en application de la présente directive ainsi que celles qui sont déjà en vigueur dans ce domaine soient portées à la connaissance des personnes concernées par des moyens appropriés sur l'ensemble de leur territoire.

Article 11  
Dialogue avec les parties intéressées

Afin de promouvoir le principe de l’égalité de traitement, les États membres encouragent le dialogue avec les parties intéressées, en particulier les organisations non gouvernementales qui ont, conformément aux pratiques et législations nationales, un intérêt légitime à contribuer à la lutte contre la discrimination fondée sur les motifs couverts par la présente directive et dans les domaines couverts par celle-ci.

Article 12  
Organismes de promotion de l'égalité de traitement

1\. Les États membres désignent un ou plusieurs organismes chargés de promouvoir l'égalité de traitement entre toutes les personnes, sans distinction de religion ou de convictions, de handicap, d'âge ou d'orientation sexuelle. Ces organismes peuvent faire partie d'agences chargées, à l'échelon national, de défendre les droits de l'homme ou de protéger les droits des personnes, y compris les droits découlant d’autres actes communautaires, comme les directives 2000/43/CE et 2004/113/CE.

2\. Les États membres veillent à ce que ces organismes aient notamment pour compétence:

–      d'apporter aux personnes victimes d'une discrimination une aide indépendante pour engager une procédure pour discrimination, sans préjudice des droits des victimes et des associations, organisations ou autres entités juridiques visées à l'article 7, paragraphe 2;

–      de mener des études indépendantes sur les discriminations;

–      de publier des rapports indépendants et de formuler des recommandations sur toutes les questions liées à ces discriminations.

CHAPITRE III  
Dispositions finales

Article 13  
Conformité

Les États membres prennent les mesures nécessaires pour faire en sorte que le principe de l’égalité de traitement soit respecté, et en particulier:

a) que les dispositions législatives, réglementaires et administratives contraires au principe de l’égalité de traitement soient supprimées;

b) que les dispositions contractuelles, les règlements intérieurs des entreprises et les règles régissant les associations à but lucratif ou sans but lucratif qui sont contraires au principe de l’égalité de traitement soient ou puissent être déclarés nuls et non avenus ou soient modifiés.

Article 14  
Sanctions

Les États membres déterminent le régime des sanctions applicables aux violations des dispositions nationales adoptées en exécution de la présente directive et prennent toute mesure nécessaire pour assurer l'application de ces sanctions. Celles-ci peuvent comprendre le versement d’indemnités, qui ne peuvent pas être limitées à priori par un plafond et doivent être effectives, proportionnées et dissuasives.

Article 15  
Mise en œuvre

1\. Les États membres adoptent les dispositions législatives, réglementaires et administratives nécessaires pour se conformer à la présente directive au plus tard le … \[deux ans après l’adoption\]. Ils en informent immédiatement la Commission et lui communiquent le texte de ces dispositions ainsi qu’un tableau de correspondance entre ces dispositions et la présente directive.

Lorsque les États membres adoptent ces dispositions, celles-ci contiennent une référence à la présente directive ou sont accompagnées d'une telle référence lors de leur publication officielle. Les modalités de cette référence sont arrêtées par les États membres.

2\. Afin de tenir compte de conditions particulières, les États membres peuvent, s’il y a lieu, disposer que l’obligation d’assurer un accès effectif telle que prévue à l’article 4 doit être respectée au plus tard le... \[au plus tard\] quatre \[ans après l'adoption\].

Les États membres qui souhaitent faire usage de ce délai additionnel en informent la Commission au plus tard à la date mentionnée au paragraphe 1, en motivant leur décision.

Article 16  
Rapport

1\. Les États membres et les organismes nationaux chargés des questions d’égalité communiquent à la Commission, au plus tard le … et ensuite tous les cinq ans, toutes les informations nécessaires à la Commission pour établir un rapport à l’intention du Parlement européen et du Conseil sur l'application de la présente directive.

2\. Le cas échéant, le rapport de la Commission tient compte du point de vue des partenaires sociaux et des organisations non gouvernementales concernées, ainsi que de l'Agence des droits fondamentaux de l'Union européenne. Conformément au principe de prise en considération systématique des questions d’égalité entre hommes et femmes, ce rapport fournit, entre autres, une évaluation de l'incidence des mesures adoptées sur les hommes et les femmes. À la lumière des informations reçues, ce rapport inclut, si nécessaire, des propositions visant à réviser et à actualiser la présente directive.

Article 17  
Entrée en vigueur

La présente directive entre en vigueur le jour de sa publication au Journal officiel de l’Union européenne.

Article 18  
Destinataires

Les États membres sont destinataires de la présente directive.

Fait à Bruxelles, le

Par le Conseil

Le Président

  

* * *

[\[1\]](#_ftnref1) Directive 2000/43/CE du 29 juin 2000 relative à la mise en œuvre du principe de l'égalité de traitement entre les personnes sans distinction de race ou d'origine ethnique, JO L 180 du 19.7.2000, p. 22 et directive 2000/78/CE du 27 novembre 2000 portant création d'un cadre général en faveur de l'égalité de traitement en matière d'emploi et de travail, JO L 303 du 2.12.2000, p. 16.

[\[2\]](#_ftnref2) COM (2007) 640

[\[3\]](#_ftnref3) COM (2008) 412

[\[4\]](#_ftnref4) COM (2008) 420

[\[5\]](#_ftnref5) \[COM (2008) XXX\]

[\[6\]](#_ftnref6) Directive 2004/113/CE du 13 décembre 2004 mettant en œuvre le principe de l’égalité de traitement entre les femmes et les hommes dans l’accès à des biens et services et la fourniture de biens et services, JO L 373 du 21.12.2004, p. 37.

[\[7\]](#_ftnref7) Directive 2000/43/CE du 29 juin 2000 relative à la mise en œuvre du principe de l'égalité de traitement entre les personnes sans distinction de race ou d'origine ethnique (JO L 180 du 19.7.2000); directive 2000/78/CE du 27 novembre 2000 portant création d'un cadre général en faveur de l'égalité de traitement en matière d'emploi et de travail (JO L 303 du 2.12.2000).

[\[8\]](#_ftnref8) COM (2006) 643 final.

[\[9\]](#_ftnref9) COM (2008) 225.

[\[10\]](#_ftnref10) Les résultats complets de la consultation sont disponibles à l'adresse suivante: [http://ec.europa.eu/employment\_social/fundamental\_rights/news/news_fr.htm](http://ec.europa.eu/employment_social/fundamental_rights/news/news_fr.htm)

[\[11\]](#_ftnref11) [http://ec.europa.eu/yourvoice/ebtp/consultations/index_fr.htm](http://ec.europa.eu/yourvoice/ebtp/consultations/index_fr.htm)

[\[12\]](#_ftnref12) [http://ec.europa.eu/employment\_social/fundamental\_rights/org/imass_fr.htm](http://ec.europa.eu/employment_social/fundamental_rights/org/imass_fr.htm)

[\[13\]](#_ftnref13) [http://ec.europa.eu/employment\_social/fundamental\_rights/pdf/pubst/stud/mapstrand1_fr.pdf](http://ec.europa.eu/employment_social/fundamental_rights/pdf/pubst/stud/mapstrand1_fr.pdf)

[\[14\]](#_ftnref14) Sera disponible sur [http://ec.europa.eu/employment\_social/fundamental\_rights/org/imass_fr.htm](http://ec.europa.eu/employment_social/fundamental_rights/org/imass_fr.htm)

[\[15\]](#_ftnref15) [http://ec.europa.eu/employment\_social/fundamental\_rights/pdf/legnet/07compan_fr.pdf](http://ec.europa.eu/employment_social/fundamental_rights/pdf/legnet/07compan_fr.pdf)

[\[16\]](#_ftnref16) [http://ec.europa.eu/employment\_social/fundamental\_rights/pdf/pubst/stud/multdis_fr.pdf](http://ec.europa.eu/employment_social/fundamental_rights/pdf/pubst/stud/multdis_fr.pdf)

[\[17\]](#_ftnref17) Étude Eurobaromètre spéciale n° 296 sur la discrimination dans l’Union européenne: [http://ec.europa.eu/employment\_social/fundamental\_rights/public/pubst_fr.htm](http://ec.europa.eu/employment_social/fundamental_rights/public/pubst_fr.htm) et [http://ec.europa.eu/public\_opinion/archives/eb\_special_fr.htm](http://ec.europa.eu/public_opinion/archives/eb_special_fr.htm)

[\[18\]](#_ftnref18) Flash Eurobaromètre n° 232; [http://ec.europa.eu/public\_opinion/flash/fl\_232_fr.pdf](http://ec.europa.eu/public_opinion/flash/fl_232_fr.pdf)

[\[19\]](#_ftnref19) Sera disponible sur [http://ec.europa.eu/employment\_social/fundamental\_rights/org/imass_fr.htm](http://ec.europa.eu/employment_social/fundamental_rights/org/imass_fr.htm)

[\[20\]](#_ftnref20) JO L 269 du 5.10.2002.

[\[21\]](#_ftnref21) Arrêt de la CJCE du 1.4.2008 dans l’affaire C-267/06, _Tadao Maruko_.

[\[22\]](#_ftnref22) Affaire C-185/97, Rec. 1998, p. I-5199.

[\[23\]](#_ftnref23) Affaire  **109/88, _Danfoss_, Rec. 1989, p.**  _03199._

[\[24\]](#_ftnref24) JO L 14 du 20.1.1998.

[\[25\]](#_ftnref25) Directive 2002/73/CE modifiant la directive 76/207/CEE du Conseil relative à la mise en œuvre du principe de l'égalité de traitement entre hommes et femmes en ce qui concerne l'accès à l'emploi, à la formation et à la promotion professionnelles, et les conditions de travail, JO L 269 du 5.10.2002, p. 15.

[\[26\]](#_ftnref26) Affaires Draehmpaehl (C-180/95, Rec.1997, p. I-2195) et Marshall (C-271/91, Rec. 1993, p. I-4367).

[\[27\]](#_ftnref27) JO C , , p. .

[\[28\]](#_ftnref28) JO C , , p. .

[\[29\]](#_ftnref29) JO C , , p. .

[\[30\]](#_ftnref30) JO C , , p. .

[\[31\]](#_ftnref31) Conclusions de la présidence du Conseil européen de Bruxelles du 14 décembre 2007, point 50.

[\[32\]](#_ftnref32) Résolution du 20 mai 2008 P6_TA-PROV(2008)0212.

[\[33\]](#_ftnref33) COM(2008) 412.

[\[34\]](#_ftnref34) Directives 2000/43/CE, 2000/78/CE et 2004/113/CE.

[\[35\]](#_ftnref35) Règlement (CE) n° 1107/2006 et règlement (CE) n° 1371/2007.

[\[36\]](#_ftnref36) JO L 210 du 31.7.2006, p. 25. Règlement modifié en dernier lieu par le règlement (CE) n° 1989/2006 (JO L 411 du 30.12.2006, p. 6).

[\[37\]](#_ftnref37) JO C 134 du 7.6.2003, p. 7.