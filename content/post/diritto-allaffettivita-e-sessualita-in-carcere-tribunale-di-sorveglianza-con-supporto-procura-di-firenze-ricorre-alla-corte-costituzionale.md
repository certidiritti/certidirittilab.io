---
title: 'Diritto all''affettività e sessualità in carcere: tribunale di Sorveglianza con supporto Procura di Firenze ricorre alla Corte Costituzionale'
date: Wed, 23 May 2012 10:55:27 +0000
draft: false
tags: [Salute sessuale]
---

Solo in Italia i detenuti non hanno diritto a relazioni intime.

Roma, 23 maggio 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

La decisione del Tribunale di Sorveglianza di Firenze, con il sostegno della Procura della Repubblica di Firenze, di ricorrere alla Corte Costituzionale contro il secondo comma dell’articolo 18 della Legge 345 del 1975 che non consente ai detenuti di avere rapporti intimi con i familiari e il/la partner, è un atto di grande coraggio e civiltà. Quella norma, infatti, impone che i colloqui si svolgano in appositi spazi sotto la diretta sorveglianza del personale penitenziario, impedendo di fatto ogni tipo di rapporto intimo.   Il ricorso è nato da un detenuto del carcere di Sollicciano che chiedeva di incontrare la moglie al di fuori dalla vista della polizia penitenziaria. In quasi tutta Europa è consentito ai detenuti di incontrare intimamente, in appositi spazi, i familiari e/o partner così come  lo stesso Consiglio d’Europa e il Parlamento Europeo hanno chiesto ripetutamente ai paesi membri con diverse Risoluzioni.

Sin dalla XIII Legislatura i Radicali hanno depositato proposte riguardanti il diritto alla sessualità dei detenuti. Nel 1987 il Consiglio d’Europa ha dato indicazioni ben precise sulla necessità di adeguare le regole relative ai colloqui dei familiari con i detenuti adottando alcune Regole minime europee in materia penitenziaria, un complesso di disposizioni specificamente rivolte al trattamento della popolazione detenuta, oggetto di una raccomandazione del Comitato dei Ministri del Consiglio d’Europa che  sancisce un obbligo morale al loro rispetto da parte dei paesi membri.

I Radicali in questa Legislatura hanno depositato la Proposta di Legge n. 1310, prima firmataria Rita Bernardini: “Modifiche alla legge 26 luglio 1975, n. 354, e altre disposizioni in materia di relazioni affettive e familiari dei detenuti” che prevede spazi e tempi per relazioni affettive,  indipendentemente dall’orientamento sessuale dei partners.

Impedire ai detenuti il diritto all’affettività non può che configurarsi come una punizione aggiuntiva particolarmente afflittiva che degrada ancora di più la loro condizione. Nella castità forzata dei detenuti vi è una violenza istituzionale che nessuna legge ha formalmente autorizzato. Ci auguriamo che il Parlamento voglia concretamente far rispettare quanto previsto dagli articoli 2,3, 27, 29, 31 e 32 della Costituzione,  dal diritto europeo in materia di non discriminazione, in particolare per quanto indicato dalla Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali.