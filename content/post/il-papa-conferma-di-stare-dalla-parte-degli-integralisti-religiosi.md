---
title: 'Il Papa conferma di stare dalla parte degli integralisti religiosi'
date: Tue, 10 Jan 2012 21:21:42 +0000
draft: false
tags: [Politica]
---

Il Papa conferma di stare dalla parte degli integralisti religiosi anzichè dalla parte degli organismi dell’Onu, del consiglio d’Europa e dell’amministrazione usa per i quali i diritti delle persone lgbti(e) sono diritti umani

Dichiarazione del Segretario dell’Associazione Radicale Certi Diritti, Yuri Guaiana

Benedetto XVI continua a fare il suo lavoro di ultraconservatore integralista e denuncia «le politiche lesive della famiglia» che «minacciano la dignità umana e il futuro stesso dell'umanità» esortando invece a difendere «la famiglia, fondata sul matrimonio di un uomo con una donna». Evidentemente il Papa preferisce mettersi al fianco di tutti gli integralismi religiosi che, dall'Africa all'Asia alle Americhe e purtroppo ancora oggi all'Europa, condannano i diritti umani piuttosto che dell'ufficio ONU per l’Alto Commissario dei diritti umani (OHCHR), del Consiglio d'Europa, del Segretario di Stato Americano  per i quali i diritti gay sono diritti umani.

Se davvero Ratzinger volesse, come dice,«politiche che lo valorizzino e aiutino la coesione sociale e il dialogo» dovrebbe essere a favore della formazione di nuove famiglie dove ci si possa aprire al mondo e alla vita, come dice lui, nell'amore e cioè ad estendere a tutti (cioè anche alle persone dello stesso sesso) la possibilità di contrarre matrimonio come ha compreso persino il primo ministro britannico conservatore David Cameron.

Il Papa può naturalmente continuare a giocare a fare il talebano trascinando il cattolicesimo su posizioni medievali e inconciliabili con la contemporaneità, ma i governi devono occuparsi seriamente della laicità delle loro istituzioni e di garantire uguali diritti a tutti i loro cittadini.