---
title: 'Fondi Strutturali Europei: il Ministro non ha risposto alle associazioni LGBTI, chiederemo alla Commissione Europea di vigilare sul caso italiano'
date: Mon, 28 Jul 2014 12:47:55 +0000
draft: false
tags: [Europa]
---

[![GIULIANO-POLETTI_08_resize](http://www.certidiritti.org/wp-content/uploads/2014/07/GIULIANO-POLETTI_08_resize-300x199.jpg)](http://www.certidiritti.org/wp-content/uploads/2014/07/GIULIANO-POLETTI_08_resize.jpg)In data 24 luglio il Ministero del Lavoro ha affermato dell’avveduta formale trasmissione agli uffici della Commissione Europea il PON Inclusione sociale dal quale è stato deciso di eliminare l’obiettivo “Lotta contro tutte le forme di discriminazione e per la promozione di pari opportunità” e ogni riferimento all’orientamento sessuale e all’identità di genere tra le cause di potenziale discriminazione o alla Strategia nazionale LGBTI. Prendiamo atto della mancata risposta da parte del Ministro del Lavoro e delle Politiche Sociali alla nostra richiesta d’incontro urgente per verificare la possibilità di inserire il riferimento esplicito alle azioni antidiscriminatorie in ambito lavorativo, con particolare riferimento a orientamento sessuale e identità di genere, sia nella bozza di Accordi di Partenariato che nella bozza di PON Inclusione sociale. Agiremo in sede europea per far sì che l’Accordo di Partenariato tra Italia e Unione Europea e il PON Inclusione sociale dell’Italia rispettino gli standard europei in tema di antidiscriminazione e chiediamo sin d’ora alla Commissione Europea di vigilare sul caso italiano.

AGEDO, Arcigay, ArciLesbica, Associazione Radicale Certi Diritti, Equality Italia, Famiglie Arcobaleno