---
title: 'Comunicato Stampa: Presentazione del libro Amore civile. Università degli Studi. Milano, 15/01/2010.'
date: Mon, 04 Jan 2010 12:41:06 +0000
draft: false
tags: [Senza categoria]
---

Presentazione del libro
-----------------------

  
[**_Amore civile. Dal diritto della tradizione al diritto della ragione_**](http://www.mimesisedizioni.it/archives/001130.html)  
a cura di **Bruno De Filippis e Francesco Bilotta**, Mimesis, 2009  
venerdì 15 gennaio 2010 alle ore 16.30  
Aula 13  
Facoltà di Scienze politiche dell'Università degli Studi di Milano  
[via Conservatorio, 7](http://maps.google.it/maps?hl=it&q=via+Conservatorio,+7&sourceid=navclient-ff&rlz=1B3WZPB_itIT338IT338&um=1&ie=UTF-8&hq=&hnear=Via+Conservatorio,+7,+20122+Milano&gl=it&ei=gMc7S5zLBpT6_AaD45HDDw&sa=X&oi=geocode_result&ct=title&resnum=1&ved=0CAoQ8gEwAA)

\_\_\_

L’[Associazione radicale Certi Diritti](undefined) organizza la presentazione del progetto di riforma del diritto di famiglia [Amore civile. Dal diritto della](http://www.mimesisedizioni.it/archives/001130.html)

[tradizione al diritto della ragione](http://www.mimesisedizioni.it/archives/001130.html) a cura di **Bruno De Filippis** e F**rancesco Bilotta**, venerdì 15 gennaio 2010 alle ore **16.30**, Aula 13,

Facoltà di Scienze politiche dell'**Università degli Studi di Milano**, [via Conservatorio, 7](http://maps.google.it/maps?hl=it&q=via+Conservatorio,+7&sourceid=navclient-ff&rlz=1B3WZPB_itIT338IT338&um=1&ie=UTF-8&hq=&hnear=Via+Conservatorio,+7,+20122+Milano&gl=it&ei=gMc7S5zLBpT6_AaD45HDDw&sa=X&oi=geocode_result&ct=title&resnum=1&ved=0CAoQ8gEwAA).

Interverranno **Elio De Capitani**, attore e regista, e **Ida Marinelli**, attrice, che fin dalla fondazione del [Teatro dell’Elfo](http://www.elfo.org/) si sono distinti per la

passione con cui raccontano gli amori “altri” e “fuorilegge” ai quali hanno saputo dare visibilità artistica e dignità narrativa e, quindi, civile.

L’**ordine dei lavori** prevede gli interventi di:  
  
**Domenico Rizzo**, Storico delle donne e dell’identità di genere, _Università “L’Orientale” di Napoli_, che introdurrà il tema dal punto di vista storico.  
  
**Marilisa D’Amico**, Costituzionalista, _Università degli studi di Milano_, che illustrerà la coerenza costituzionale della riforma proposta.  
  
**Paola Ronfani**, Sociologa giuridica, _Università degli studi di Milano_, che metterà in luce i mutamenti sociali recentemente intervenuti nel contesto delle famiglie italiane.  
  
**Antonella Besussi**, Teorica della politica, _Università degli studi di Milano_, che discuterà le ragioni filosofiche e culturali alla base della riforma proposta.

Coordina i lavori **Sergio Rovasio**, segretario nazionale dell’[Associazione radicale Certi Diritti](undefined), che promuove iniziative politiche per

l’affermazione dei diritti civili e l’attuazione di una riforma inclusiva del diritto di famiglia.

Sarà presente uno dei due curatori, **Francesco Bilotta**, avvocato e fondatore di [Rete Lenford](http://www.retelenford.it/), l’Avvocatura per i diritti LGBT, impegnata nella tutela giudiziaria delle persone omosessuali.

Segreteria organizzativa: **Yuri Guaiana** tel. 3404694701 e-mail [yuri.guaiana@unimi.it](mailto:yuri.guaiana@unimi.it)