---
title: '2008-0140-Discrimination-ST15838.EN10'
date: Thu, 04 Nov 2010 13:30:34 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 4 November 2010

Interinstitutional File:

2008/0140 (CNS)

15838/10

LIMITE

SOC 728

JAI 913

MI 425

  

  

  

  

  

NOTE

from :

General Secretariat

to :

The Working Party on Social Questions

on :

4 November 2010

No. prev. doc. :

13883/10 SOC 561 JAI 759 MI 317

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

In preparation of the next meeting of the Working Party, on 4 November 2010, delegations will find attached a note from the IE delegation.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Presidency questionnaire concerning housing**

**\[Document 13883/10\]**

**IE replies**

**1\.** **Situation in Member States**

Both the Racial Equality Directive (2000/43/EC) and the Gender Goods and Services Equality Directive (2004/113/EC) cover housing available to the public in their scope. Several MS have gone beyond these two grounds in their protection against discrimination.

a)       To what extent does your national legislation against discrimination apply to housing?

Housing in Ireland

Other than self-build, the main options available in Ireland in respect of housing are

*   buying a house or apartment, either new or existing from a private owner, developer or local authority.
*   renting on the private rental market, and
*   local authority housing.

Since 2004, landlords have to register all tenancies with the Private Residential Tenancies Board, which operates a national tenancy registration system and provides a dispute resolution service between landlords, tenants and third parties. Owner-occupied accommodation and property in which the spouse, parent or child of the landlord lives are excluded.

The provision of housing by local authorities is governed by the Housing (Miscellaneous Provisions) Acts 1966 to 2009. Local authorities in Ireland are the main providers of social housing, which is allocated according to housing need on the basis of each Authority's scheme of letting priorities, with rents based on ability to pay. Voluntary housing associations and housing co-operatives also provide social housing for people who cannot afford to buy their own homes, on a somewhat similar basis to local authority housing and are financed to a significant extent by government. Local authorities and housing associations have specific powers to evict tenants engaging in anti-social behaviour.

Discriminatory treatment & access to housing

Discrimination, harassment, sexual harassment and victimisation are generally prohibited in the disposal of premises and provision of accommodation, under the Equal Status Acts 2000 to 2008, on nine grounds which include age, disability, sexual orientation and religion. The scope of this legislation extends to the disposal of premises (other than by will or gift), termination of tenancies and provision of accommodation or any services or amenities related to accommodation.

The legislation provides a number of exceptions to the general prohibition on differences in treatment. For example, accommodation and services or amenities relating to accommodation which are not available to the public generally or a section of the public are not included. Also excluded is accommodation offered by a person in a part (other than a separate and self-contained part) of the person's home, or where the provision of the accommodation affects the person's private or family life or that of any other person residing in the home.

The legislation also permits premises or accommodation to be reserved for the use of persons in a particular category of persons for a religious purpose or as a refuge, nursing home, retirement home, home for persons with a disability or hostel for homeless persons or for a similar purpose.

Housing authorities are permitted to take account of family size, family status, marital status, disability, age or membership of the Traveller community in allocating housing. For example, in setting out its scheme of letting priorities, a local authority could give priority to applicants with children and larger properties could be reserved for applicants with larger families.

b)      Do you distinguish (for this purpose) between private and public/social housing?

No.

The equality legislation applies to all housing accommodation irrespective of its ownership, having due regard to the exclusions mentioned in (1)(a) above in regard to housing authorities.

c)       Do you have statistics on the number of cases of discrimination in the area of housing, and if so, what is the most prevalent ground?

Most disputes concerning private rental accommodation would be brought to the Private Residential Tenancies Board (PRTB). In 2008, when the PRTB had over 200,000 tenancies registered, it received over 1,600 applications for adjudication or mediation.

In 2008, local authority lettings exceeded 118,000 dwellings. The public service Ombudsman has the power to examine complaints from people who feel they have been unfairly treated by a local authority in the matter of housing and dealt with 297 such complaints in 2008.

By contrast, the number of complaints referred to the Equality Tribunal alleging discrimination in access to accommodation is low. The table below summarises the 84 cases decided by the Equality Tribunal between 2001 and 2009 on this topic, with a breakdown by type of accommodation and the discriminatory grounds considered in the complaint.

**Table: Number of cases decided by the Equality Tribunal concerning housing accommodation**

**YEAR**

**Private Rental Accommodation**

**Local Authority housing**

**Housing Association**

**Total**

**Discriminatory Grounds**

**2001**

0

0

0

0

n/a

**2002**

0

0

0

0

n/a

**2003**

1

0

0

1

1 gender/family status (private rental).

**2004**

2

1

0

3

1 disability (local authority); 2 age/marital status (private rental).

**2005**

2

1

0

3

1 disability & 1 traveller/family status (private rental); 1 gender (local authority).

**2006**

0

5

1

6

1 disability & 4 traveller (local authority); 1 race* (housing assoc).

**2007**

2

3

0

5

1 disability & 1 race* (private rented); 3 disability (local authority).

**2008**

7

46

0

53

6 gender & 1 traveller private rented); 1 disability/age & 45 traveller (local authority).

**2009**

0

12

1

13

1 disability/gender, 1 disability, 1 marital status, 9 traveller (local authority); 1 traveller (housing assoc).

14

68

2

**84**

_Note *: The definition of race for the purpose of national equality legislation is inclusive of nationality and national origin._

**2\.** **Social housing**

a)       Do you consider social housing to be a service (for the purposes of EU law)?

Ireland considers that social housing is a service of general economic interest.

b)      If not, do you consider it to be a part of the social protection system?

Not applicable.

**3\.** **Private life**

In some cases, the principle of equality might be in conflict with the right to private life, in particular in the area of housing (e.g. renting out a room in your own apartment).

a)       Is this better taken into account by an exclusion of transactions in the area of private life from the scope of the Directive, or by an exclusion of activities which are not commercial or professional?

As outlined at (1)(a) above, it was decided to provide in Irish equality law a balance between respecting private and family life and combating discriminatory treatment by specifying areas in which private and family life would be affected. Thus, in addition to the exclusion of accommodation and services or amenities relating to accommodation which are not available to the public generally or to a section of the public, also excluded is accommodation

*   offered by a person in a part (other than a separate and self-contained part) of the person's home, or
*   where the provision of the accommodation affects the person's private or family life or that of any other person residing in the home.

We feel that this is a distinction which is easily understood and accepted by both suppliers and customers in our national context.

We feel a distinction between activities which are commercial or professional and those which are not is not helpful in this instance, due to difficulties in definition. For example, where rent is accepted for housing accommodation, even to share a room in a person's home, is there a question as to whether a commercial transaction has occurred?

b)      How is commercial/professional housing defined in your national legislation?

No such distinction is drawn.

**4\.** **Disability - reasonable accommodation**

a)       In your view, what should the obligation to provide reasonable accommodation mean for providers of housing?

  

Irish equality law currently provides that discrimination includes a refusal or failure by the provider of a service (e.g. the person disposing of premises, or for the provision of accommodation or any related services or amenities) to do all that is reasonable to accommodate the needs of a person with a disability by providing special treatment or facilities, if without such special treatment or facilities it would be impossible or unduly difficult for the person to avail himself or herself of the service.

A refusal or failure to provide such special treatment or facilities is considered reasonable only if such provision would give rise to a cost, other than a nominal cost, to the provider of the service in question. However, where a person has a disability that, in the circumstances, could cause harm to the person or to others, it is permitted to treat the person differently to the extent reasonably necessary to prevent such harm.

Reasonable accommodation is the response required by law from a service provider in response to the circumstances of a particular person with a disability, at a particular time. The law does not require the service provider to make assumptions or anticipate what that individual person's needs could possibly be.

These provisions in equality law are in addition to general provisions, for example in building regulations, requiring greater accessibility of buildings.

b)      Is there legislation on the accessibility of buildings (private as well as public/social) in your country?

Yes.

General accessibility

Minimum requirements necessary to ensure dwellings are visitable to persons with disabilities (i.e. that adequate provision is made to enable people with disabilities to safely and independently access and use a building) are set out in Part M of the Building Regulations and the associated Technical Guidance Document M[\[1\]](#_ftn1). These requirements aim to provide that persons with disabilities can safely and conveniently approach and gain access to a dwelling, have access to the main habitable rooms at entry level and gain access to and use of a toilet.

However, the underlying philosophy on which those requirements are based is that buildings should be accessible and usable by everyone, including people with disabilities. Those involved in the design and construction of buildings are encouraged to have regard to this philosophy of universal access and consider making additional provision, where practicable and appropriate.

In general, these Building Regulations apply to the construction of new buildings and to extensions and material alterations to existing buildings. In addition, certain parts of the Regulations apply to existing buildings where a material change of use takes place. Otherwise, the present Building Regulations do not apply to buildings constructed prior to 1 June, 1992.

Following a review, these Building Regulations are being revised and, pending Government approval, updated regulations are expected to be published by end 2010.

On the general matter of accessibility of public spaces, where a road authority constructs or alters, or consents to the construction or alteration of, any public footway or other public pavement, it is required under the Equal Status Acts, for the purpose of facilitating the mobility of persons with a disability, to provide, or require the provision of, ramps, dished kerbs or other sloped areas at appropriate places at or in the vicinity of any pedestrian crossing or intersection used by pedestrians in that part of the footway or pavement so constructed or altered.

Such general provisions to ensure the greater accessibility of buildings and services are in addition to the requirement to provide reasonable accommodation under equality law.

**5\.** **Disability - accessibility**

a)       What should the obligation to provide for accessibility measures encompass in the area of housing?

As outlined above, the underlying philosophy on which national building regulations are based is that buildings should be accessible and usable by everyone, including people with disabilities. Those involved in the design and construction of buildings are encouraged to have regard to this philosophy of universal access and consider making additional provision, where practicable and appropriate.

At a minimum, it is felt that such general measures to provide for accessibility should aim to provide that persons with disabilities can safely and conveniently approach and gain access to a dwelling, have access to the main habitable rooms at entry level and gain access to and use of a toilet. However, due consideration should be given to the

Ireland is currently developing a **new national housing strategy for people with a disability**. Work on the strategy is at an advanced stage and the strategy is expected to be published shortly. One of the key issues which the strategy is examining is in relation to the physical accessibility of housing, including the Irish provisions in this regard, the concept of retrofitting and options in relation to lifetime homes policy.  It is envisaged that the strategy will propose that an examination of lifetime homes policy be undertaken which will encompass

*   a review of best practice,
*   cost benefit analysis,
*   the requirement for the development of standards, etc.

b)      Should there be a difference between old and new buildings?

  

Yes.

In our view, as is currently the case in national building regulations referred to above, some distinction must be drawn between existing buildings, existing buildings undergoing renovation or change of use and new properties being built.

For example, the relevant building regulations apply to the construction of new buildings and to extensions and material alterations to existing buildings. In addition, certain parts of the Regulations apply to existing buildings where a material change of use takes place. Otherwise, the present Building Regulations do not apply to buildings constructed prior to 1 June, 1992.

Such a distinction is also made in determining the level of grant made under the scheme of Housing Adaptation Grants for People with a Disability, through which State funding is made available for defraying the cost of adaptations. The amount of funding available for adaptation of houses which are at the planning stage is half that for adaptation of existing buildings.

Reasonable accommodation

The need to provide reasonable accommodation only applies to existing property, not to property which has yet to be built. The cost of adapting older properties is generally higher than for recently built properties. Therefore, this distinction is built into the definition of "reasonable accommodation".

c)       Do you support immediate applicability with respect to new buildings?

In our view, it would be appropriate for transitional arrangements to be in place.

d)      Should there be a difference between the obligations for private and public/social housing respectively?

This issue is still being considered in the context of development of the new national housing strategy for people with a disability, and the associated implementation plan.

e)       Are there public funds available in your country for the adaptation of housing (private as well as public/social)?

  

The physical accessibility of housing to meet the needs of people with disabilities are currently met, to varying degrees, through the delivery of individualised units, provision of funding for adaptation works and through the effective enforcement of Part M of the Building Regulations.

There are a number of people with disabilities for whom the provisions of lifetime housing would not be sufficient and who will continue to require higher access design standards and, sometimes, specifically designed housing to meet their needs. Housing authorities currently develop accommodation to meet these specific needs on a case by case basis for people who qualify for social housing.

Where it is identified in the housing assessment process that an applicant has particular needs, the housing authority will establish design requirements to address these needs, and plan for these in the design and construction / adaptation of housing.  Any funding implications i.e. identified cost implications or issues which are likely to give rise to increased costs, are taken into consideration at the planning stage and sufficient resources allocated by housing authorities.

As noted above, the Housing Adaptation Grant for People with a Disability Scheme, is administered by local authorities and provides grant aid to applicants living in private accommodation to assist in the carrying out of works that are reasonably necessary for the purposes of rendering a house more suitable for the accommodation needs of a person with a disability.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[http://www.environ.ie/en/Publications/DevelopmentandHousing/BuildingStandards/](http://www.environ.ie/en/Publications/DevelopmentandHousing/BuildingStandards/)