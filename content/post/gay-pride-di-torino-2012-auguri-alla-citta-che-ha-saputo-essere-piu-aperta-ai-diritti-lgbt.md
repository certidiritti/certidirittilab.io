---
title: 'Gay Pride di Torino 2012: auguri alla Città che ha saputo essere più aperta ai diritti Lgbt'
date: Mon, 18 Jun 2012 21:16:09 +0000
draft: false
tags: [Politica]
---

Torino continui ad essere esempio per il resto dell'Italia.

Torino, 16 giugno 2012 Gay Pride di Torino 2012: auguri alla Città che ha saputo essere più aperta ai diritti Lgbt

Comunicato Stampa dell’Associazione Radicale Certi Diritti

E' il settimo anno consecutivo che a Torino si celebra un Gay  Pride, come quello che sfilerà oggi per le vie della città al quale l’Associaizone Radicale Certi Diritti ha aderito e parteciperà con iscritti e sostenitori, guidati dal Presidente dell’Associazione Enzo Cucco.

Dopo i primi tre storici organizati dal Fuori!, il primo movimento gay italiano, nel 1979, 1980 e 1981, è dal 2006, anno del Gay Pride nazionale, che in modo continuativo Torino conferma questo importante appuntamento. Questa continuità rappresenta un grande valore, per tutti e tutte.

Ha un valore perchè significa che il coordinamento Torino Pride ha saputo in questi anni trovare un modo per collaborare con tutta la città, le associazioni,le istituzioni locali che hanno trasformato il Gay Pride in un autentico momento collettivo di lotta per i diritti e le libertà di tutti, perchè i diritti lgbt sono i diritti di tutti i cittadini.

Il Comune di Torino ha saputo in questi anni sostenere attività significative  riconosciute a livello nazionale e internazionale: il Festival del Cinema lgbt, il servizio comunale Lgbt e la delibera di riconoscimento delle unioni civili sono gli esempi più conosciuti e concreti.

E' molto importante che il Comune di Torino non solo continui a sostenere queste ed altre esperienze significative ma continui a rappresentare un modello per tutte le altre città italiane. Non c'è quindi solo onore nell'essere attivi sui diritti lgbt, ma anche onere e responsabilità per tutti coloro che sono impegnati per la promozione e la difesa dei diritti civili.

Auguriamo un Pride di festa e di impegno per tutte e tutti i torinesi.