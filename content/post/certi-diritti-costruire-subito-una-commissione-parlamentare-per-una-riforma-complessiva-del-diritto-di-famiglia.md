---
title: 'Certi Diritti: costruire subito una commissione parlamentare per una riforma complessiva del diritto di famiglia'
date: Fri, 01 Mar 2013 14:40:35 +0000
draft: false
tags: [Diritto di Famiglia]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 1 marzo 2013  

L'Associazione Radicale Certi Diritti ha scritto una lettera aperta ai neo eletti Deputati e Senatori per chiedere "di costituire, subito, una Commissione Parlamentare per una complessiva riforma del diritto delle persone e della famiglia, coinvolgendo studiosi ed associazioni, seguendo il metodo che, sia pure limitatamente ad un solo tema, ha portato all'approvazione della Legge 219/2012 (abolitrice di buona parte delle differenze esistenti tra figli legittimi e naturali)".

La lettera pubblicata sul blog del segretario Yuri Guaiana, ospitato dall'Huffington Post, "richiama l'attenzione sul Progetto “Amore civile”, riassunto in una Conferenza che si svolse presso la Sala delle Colonne della Camera dei Deputati il 18 maggio 2008, un libro (“Amore civile, dal diritto della tradizione al diritto della ragione”, Mimesis 2009) e in due progetti di legge di riforma del libro primo del codice civile “ Delle persone e della famiglia” che sono stati presentati al Senato (n. 2263) e alla Camera (n. 3607) dei deputati nella scorsa Legislatura".  
  
Consapevole dell'importanza della continuazione delle lotte per la conquista di maggiori spazi di dignità e libertà di scelta per i cittadini, per il consolidamento della laicità dello Stato e per il progresso civile, l'Associazione Radicale Certi Diritti ritiene che le riforme indicate in quelle proposte siano "non solo necessarie, ma urgenti. Tra esse il matrimonio tra persone dello stesso sesso, l’adozione da parte di single, la moltiplicazione dei modelli di unione matrimoniale, l’unificazione dei procedimenti di separazione e divorzio, il testamento biologico, il riconoscimento delle comunità intenzionali, l’ammissione della procreazione eterologa ed altre modifiche alla legge 40, la parità tra uomo e donna nella trasmissione del cognome, la modifica, nel regime successorio, dell’istituto della legittima, la dichiarazione dei “diritti dei minori”, la valorizzazione di forme di solidarietà sociale extra familiari".

[**Il testo della lettera >**](http://www.huffingtonpost.it/yuri-guaiana/costituire-subito-una-commissione-parlamentare-per-una-riforma-complessiva-del-diritto-di-famiglia_b_2784796.html)