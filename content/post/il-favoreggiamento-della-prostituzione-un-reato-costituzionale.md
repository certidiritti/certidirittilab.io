---
title: 'Il favoreggiamento della prostituzione: un reato costituzionale?'
date: Thu, 28 Jun 2018 09:41:52 +0000
draft: false
tags: [Lavoro sessuale]
---

[![IMG_20180628_113303](http://www.certidiritti.org/wp-content/uploads/2018/06/IMG_20180628_113303-300x169.png)](http://www.certidiritti.org/wp-content/uploads/2018/06/IMG_20180628_113303.png)

Milano 28 giugno 2018 – A breve la **Corte Costituzionale** si pronuncerà sulla legittimità costituzionale del **reato di favoreggiamento della prostituzione** nell'ambito del cosiddetto **processo "Escort"** dove si dibatte delle accuse, a vario titolo, di associazione a delinquere e induzione, favoreggiamento e sfruttamento della prostituzione.

Ne abbiamo parlato ieri sera a Milano. Per riascoltare il dibattito, registrato da Radio Radicale, clicca [qui](https://www.radioradicale.it/scheda/545394/il-favoreggiamento-della-prostituzione-un-reato-costituzionale). Di seguito, alcuni estratti degli interventi:

**Yuri Guaiana**, presidente dell'Associazione Radicale Certi Diritti, e **Pia Covre**, dell’Associazione Radicale Certi Diritti e co-fondatrice del Comitato per i Diritti Civili delle Prostitute ONLUS: «Lavoratori e lavoratrici sessuali si battono da anni per la **decriminalizzazione del lavoro sessuale** che passa anche per l’abrogazione del reato di favoreggiamento della prostituzione che non solo non protegge lavoratori e lavoratrici del sesso, ma costringe a condizioni di lavoro molto meno sicure».

**Avv. Massimo Clara**: «Quando la prostituzione è esercitata liberamente, non appare costituzionalmente legittimo sanzionare condotte di organizzazione o ausilio: se la persona fa la propria scelta senza sfruttamento e senza condizionamento, e non commette comunque un reato, non vi è ragione per imporre limiti criminalizzanti, considerando in particolare che la legge non deve occuparsi della morale dei cittadini, ma al contrario consentire le condizioni più opportune per l’ esercizio di una libertà».

**Avv. Giulia Crivellini**: «La corte costituzionale ha ora davanti a sé un'occasione storica: difendere la separazione tra diritto e morale. Compito di uno Stato democratico è tutelare tutti i soggetti, non solo contrastando i fenomeni criminali legali alla prostituzione, ma anche garantendo la piena libertà di autodeterminarsi per chi esercita liberamente».