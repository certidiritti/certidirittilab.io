---
title: '12 febbraio a Roma al Teatro Valle, commemorazione di David Kato'
date: Tue, 08 Feb 2011 15:18:40 +0000
draft: false
tags: [arcigay, certi diritti, Comunicati stampa, david kato kisule, GAY, luxuria, OMOFOBIA, RADICALI, rita bernardini, teatro dell'elfo, Uganda]
---

Sabato 12 febbraio a Roma commemorazione di David Kato Kisule promossa dall'Associazione radicale Certi Diritti e dalla compagnia Teatro dell'Elfo. Con la partecipazione di:

**NON C’E’ PACE SENZA GIUSTIZIA -  AGEDO - PARTITO RADICALE NONVIOLENTO - CIRCOLO MARIO MIELI -  RADICALI ROMA - ASSOCIAZIONE LUCA COSCIONI – ARCIGAY -  NESSUNO TOCCHI CAINO – ASSOCIAZIONE DIGAYPROJECT -  RADICALI ITALIANI - NUOVA PROPOSTA, DONNE E UOMINI OMOSESSUALI CRISTIANI.  
**

**  
In memoria di David Kato Kisule**

**Roma, sabato 12 febbraio 2011 – ore 16  al Teatro Valle**

L’Associazione Radicale **Certi Diritti** e la compagnia **Teatro dell’ Elfo**, in scena con **[‘Angels in America’ (fantasia gay su temi nazionali)](http://www.elfo.org/stagioni/20102011/tournee/angelsinamericamaratona.html)**, terranno al Teatro Valle di Roma (Via del Teatro Valle, 21) una commemorazione – ricordo di **David Kato Kisule**, attivista gay ucciso in Uganda dall’odio e dal pregiudizio.

L’invito a partecipare al ricordo della figura di David Kato Kisule, membro **iscritto** dell’Associazione Radicale Certi Diritti,  è rivolto a personalità e rappresentanti di Associazioni con l’obiettivo di valorizzare l’impegno per la promozione e la difesa dei diritti civili e umani.  
I testi e gli interventi verranno raccolti e condivisi con la **Smug** (Sexual Minorities Uganda), di cui David era uno dei più importanti esponenti.

Saranno presenti tra gli altri:  
**On. Rita Bernardini** – presidente Certi Diritti, **Vladimir Luxuria**, **Mario Staderini**, segretario Radicali Italiani, **Paolo Patanè** – presidente nazionale Arcigay, **Imma Battaglia** – presidente Di Gay Project e rappresentanti di associazioni per i diritti civili e umani.  
  
--------------------   
  
Grazie all’impegno di Elio Polizzotto e dell’Ong **Non c’è Pace Senza Giustizia**, David era stato ospite lo scorso novembre ai lavori del IV Congresso dell’Associazione Radicale Certi Diritti, dove aveva raccontato delle persecuzioni e di veri e propri linciaggi, di cui sono vittime le persone lesbiche e gay in Uganda, promosse da organizzazioni evangeliche del fondamentalismo religioso.

Il 16 ottobre 2010 la rivista ugandese **Rolling Stones** pubblicò in prima pagina le foto di 100 attivisti omosessuali (o presunti tali) ugandesi chiedendone l’arresto. Tra le 100 foto vi era anche quella di David Kato Kisule, l’esponente più noto del movimento. Il clima di odio contro le persone omosessuali è alimentato dal fondamentalismo religioso dei predicatori evangelisti che trovano terreno molto fertile tra la popolazione che vive nella miseria e nella disperazione.

Molte Ong internazionali si erano mobilitate in diversi paesi del mondo contro questa barbarie. Il Parlamento Europeo, grazie alla campagna internazionale di Non c'è Pace Senza Giustizia, aveva approvato una Risoluzione di condanna nei confronti dell'Uganda. David era stato anche audito dalla Sottocommissione Diritti Umani del Parlamento Europeo dopo aver partecipato  a Roma al Congresso di Certi Diritti.

David con molto coraggio e determinazione aveva avviato una iniziativa legale contro la rivista Rolling Stones e **lo scorso 7 gennaio l’Alta Corte ugandese aveva condannato la rivista per violazione della legge sulla privacy**, difendendo le persone gay perseguitate. L’Alta Corte aveva dichiarato che nessuna delle persone la cui foto era stata pubblicata, aveva commesso reati, previsti dal codice penale ugandese per le persone omosessuali.  In quell’occasione è stata anche ordinata la chiusura immediata della rivista.

David Kato, nel suo soggiorno a Roma, aveva raccontato di come la situazione in Uganda fosse divenuta per gli attivisti omosessuali molto pericolosa. Durante le udienze in Tribunale era protetto da volontari delle Ong internazionali che seguivano il processo e difeso da diplomatici di ambasciate occidentali che lo avevano salvato da diversi tentativi di linciaggio perché riconosciuto dalla folla inferocita.

L’Associazione Radicale Certi Diritti, insieme alle Associazioni e Ong Smug (Sexual Minorities Uganda), Human Rights Watch, Global Rights, Global LGBT Advocacy, Npwj, **ha chiesto che il Governo si assuma le proprie gravi responsabilita’ per non essere intervenuto per fermare la campagna di odio e di violenza contro la comunita LGBTI ** e per proteggere gli attivisti in pericolo in Uganda, adoperandosi quanto prima ad avviare un clima di tolleranza e di dialogo tra le autorità e le organizzazioni politiche e religiose.

Ricorderemo inoltre il suo impegno anche durante i lavori del [XXXIX Congresso del Partito Radicale Nonviolento, transnazionale e transpartito](http://www.radicali.it/primopiano/39-congresso-partito-radicale-nonviolento-transnazionale-transpartito-chianciano-17-20-fe) che si svolgeranno a Chianciano dal 17 al 21 febbraio 2011 al quale David avrebbe dovuto partecipare.

[www.certidiritti.it](undefined/) -  [segretario@certidiritti.it](mailto:segretario@certidiritti.it)