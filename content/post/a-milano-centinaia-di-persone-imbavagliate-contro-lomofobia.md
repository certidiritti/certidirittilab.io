---
title: 'A MILANO CENTINAIA DI PERSONE IMBAVAGLIATE CONTRO L''OMOFOBIA'
date: Mon, 18 May 2009 12:56:38 +0000
draft: false
tags: [Comunicati stampa]
---

### Milano: Giornata Mondiale contro l’omofobia

Ieri, domenica 17 Maggio 2009, si è svolta per le vie del centro di Milano una manifestazione in occasione della **Giornata Mondiale contro l’Omofobia**.