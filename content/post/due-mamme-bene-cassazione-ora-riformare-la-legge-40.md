---
title: 'Due mamme: bene Cassazione, ora riformare la Legge 40'
date: Thu, 15 Jun 2017 16:41:19 +0000
draft: false
tags: [Diritto di Famiglia]
---

![1422511144-0-fecondazione-eterologa-al-via-in-sicilia-ancora-non-decise-le-tariffe-ecco-i-centri](http://www.certidiritti.org/wp-content/uploads/2017/06/1422511144-0-fecondazione-eterologa-al-via-in-sicilia-ancora-non-decise-le-tariffe-ecco-i-centri-300x168.jpg)"Una sentenza, quella della Corte di Cassazione resa nota oggi, che per ironia della sorte casca a dodici anni esatti dal referendum sulla legge 40: il figlio di una coppia di donne omosessuali nato all'estero con fecondazione eterologa deve essere iscritto all'anagrafe italiana con i cognomi di entrambe le madri. Una nuova doccia di realtà per un Parlamento che farebbe bene a superare di fretta gli ultimi divieti della legge 40 - come quello imposto a donne single e lesbiche - prima che ci pensi la Corte Costituzionale a finire di demolire una delle più brutte leggi della storia repubblicana" Lo dichiarano Filomena Gallo e Leonardo Monaco, segretari rispettivamente dell'Associazione Luca Coscioni e dell'Associazione Radicale Certi Diritti.