---
title: 'SABATO 4 LUGLIO A CATANIA, CONFERENZA STAMPA E GAY PRIDE SICILIA'
date: Fri, 03 Jul 2009 06:36:42 +0000
draft: false
tags: [Comunicati stampa]
---

**GAY PRIDE SICILIA: SABATO 4 LUGLIO, A CATANIA, CONFERENZA STAMPA DEI RADICALI, ORE 11 HOTEL EXCELSIOR.**

Domani, sabato 4 luglio, alle ore 11, presso l’Hotel Excelsior (Piazza Verga - Catania) i radicali terranno una Conferenza Stampa nell’ambito del Gay Pride Sicilia, che si svolgerà nel pomeriggio per le strade del centro.

Alla Conferenza Stampa parteciperanno:

\- Sergio Rovasio, Segretario Associazione Radicale Certi Diritti;-

\- Gianmarco Ciccarelli, Segretario Associazione Radicali Catania;-

\- Eduardo Melfi, Responsabile Certi Diritti Catania;

Nel corso della Conferenza Stampa verrà illustrata la proposta di Certi Diritti di tenere il gay pride nazionale del 2010 a Catania.

Verranno anche distribuiti documenti relativi alla campagna di Affermazione Civile per il matrimonio gay, l’ordinanza del Tribunale di Venezia e il testo dell’interrogazione parlamentare dei Senatori radicali eletti nel Pd, Marco Perduca e Donatella Poretti, sull’intervento dei Carabinieri lunedì scorso al Gazebo di Certi Diritti Catania.