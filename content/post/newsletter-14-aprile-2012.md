---
title: 'Newsletter 14 Aprile 2012'
date: Sat, 14 Apr 2012 11:10:38 +0000
draft: false
tags: [Politica]
---

![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)

Oggi è il secondo anniversario della **sentenza 138/2010 della Corte Costituzionale**, che ha affermato per la prima volta la dignità costituzionale delle coppie formate da persone dello stesso sesso. Ma in questi due anni, in cui la nostra associazione ha continuato la battaglia per l’uguaglianza dei diritti depositando anche due ricorsi alla **Corte Europea dei diritti dell’uomo**, il Parlamento non ha fatto nulla su questa materia che la Corte costituzionale ha chiesto espressamente di regolare.

Così ieri l'Associazione Radicale Certi Diritti ha inviato a tutti i capigruppo di Camera e Senato, ai Presidenti e Vicepresidenti di Camera e Senato, a tutti i ministri e al Presidente del Consiglio una** [**copia del libro**](e-uscito-il-libro-dal-cuore-delle-coppie-al-cuore-del-diritto-ludienza-138/2010-alla-corte-costituzionale-per-il-diritto-al-matrimonio-tra-persone-dello-stesso-sesso)** "Dal cuore delle coppie al cuore del diritto. L'udienza alla Corte Costituzionale per il diritto al matrimonio tra persone dello stesso sesso". [**Il libro è stato accompagnato da una lettera firmata dall’on. Rita Bernardini e dall’on. Anna Paola Concia, insieme a Certi Diritti, Arcigay, Arcilesbica, Agedo e Famiglie Arcobaleno**](anniversario-della-sentenza-138-2010-della-corte-c%20ostituzionale-bernardini-concia-e-associazioni-lgbti-scrivono-al-governo-e-ai-capigruppo-di-camera-e-senato)**. **I firmatari ribadiscono che giustizia sarà fatta solo consentendo a tutti di accedere al **matrimonio civile a prescindere dal sesso della persona con cui decidono d'intraprendere un percorso di vita comune.**

Qualcosa però sembra muoversi. [**Giovedì 19 aprile sono infatti all'ordine del giorno della commissione Giustizia di Montecitorio le nove proposte di legge**](http://www.tmnews.it/web/sezioni/news/PN_20120413_00245.shtml) su unioni civili e matrimonio tra persone dello stesso sesso depositate alla Camera che Giulia Bongiorno, presidente di Commissione e relatrice, illustrerà.  
Tra le nove proposte, **tre sono dell’on. radicale Rita Bernardini**, membro della commissione Giustizia della Camera  iscritta a Certi Diritti: una sulle unioni civili, una sul matrimonio tra persone dello stesso sesso e quella sulla riforma complessiva del diritto di famiglia, cha abbiamo voluto chiamare **‘Amore Civile’**.

Questo non significa ovviamente che una legge sui diritti delle coppie gay è vicina, ma sicuramente sarà l’occasione per riaprire il dibattito all’interno delle istituzioni e soprattutto nel Paese.   
[Per questo abbiamo bisogno anche del tuo aiuto.  ](iscriviti) 

Intanto il 21 aprile organizzeremo a Roma, insieme al Comitato per i Diritti Civili delle Prostitute e alla Cgil-Nuovi Diritti, **una Conferenza Nazionale sulla Legalizzazione della Prostituzione** durante la quale sarà lanciato un manifesto-appello. [Scarica il programma sul nostro sito](conferenza-nazionale-sulla-legalizzazione-della-prostituzione) e comunicaci la tua partecipazione a [](https://mail.google.com/mail/mu/mp/472/?mui=ca)[info@certidiritti.ti](mailto:info@certidiritti.ti)

[**Rimuovere le discriminazioni contro le famiglie non matrimoniali si può. Cominciamo dai congedi parentali**.](rimuovere-le-discriminazioni-contro-le-famiglie-non-matrimoniali-si-puo-cominciamo-dai-congedi-parentali) Questa la richiesta che l’Associazione radicale Certi Diritti ha indirizzato ai ministri Riccardi e Fornero.

[**“Io, gay, in regola con il permesso di soggiorno grazie al matrimonio con un uomo”**](http://blog.panorama.it/italia/2012/03/27/sposa-un-italiano-ed-uruguayano-gay-ottiene-il-permesso-di-soggiorno-intervista/) intervista a Rafael il ricorrente a Reggio Emilia e a Sergio Rovasio di Certi Diritti.

Con  un’altra sentenza [il convivente di una coppia omosessuale ottiene la copertura sanitaria.](http://www.gaynews.it/articoli/Vita-di-coppia/87953/Milano-anche-convivente-coppia-omosessuale-ottiene-copertura-sanitaria.html) Un commento dell’**Avv. Massimo Clara** del nostro Direttivo: [**la coppia more uxorio è sia etero sia omo >**](la-coppia-more-uxorio-e-sia-etero-sia-omo-un-nuovo-passo-della-giurisprudenza)

**Certi diritti che le coppie conviventi non sanno di avere**: **[un commento al libro>](http://www.tellusfolio.it/index.php?prec=%2Findex.php&cmd=v&id=14293)**

**[Firma per chiedere l’immediato rilascio di Jean-Claude Roger Mbede in Cameroon>](nuova-attivazione-per-jean-claude-roger-mbede-cameroon)**

**[uno studio inglese conferma: l’omofobia più marcata è quella dei ‘gay repressi’>](http://www.gaynews.it/articoli/Salute/87966/Universita-inglese-di-psicologia-omofobia-maschera-omosessualita-latente.html)**

**[Diritti lgbti e primavera araba: NPWJ, Certi Diritti e Radical Party lanciano un concorso di scrittura >](un-concorso-di-scrittura-sui-diritti-lgbti-nella-regione-mena-e-la-primavera-araba)**

  
**[www.certidiritti.it](http://www.certidiritti.it/)**