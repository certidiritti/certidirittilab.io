---
title: 'Unioni civili a Milano: Certi Diritti ringrazia Marilisa D''Amico e chiede l’immediata discussione della delibera sul registro delle coppie di fatto'
date: Thu, 23 Feb 2012 22:50:44 +0000
draft: false
tags: [Diritto di Famiglia]
---

L' Associazione Radicale Certi Diritti ringrazia la consigliera comunale del Pd Marilisa D' Amico per la proposta di delibera sul registro delle coppie di fatto da lei presentata al Consiglio Comunale di Milano e ne chiede l’immediata discussione senza ulteriori attese.

L' Associazione Radicale Certi Diritti ringrazia Marilisa D' Amico per la proposta di delibera da lei presentata al Consiglio Comunale di Milano per l' istituzione del registro delle coppie di fatto. E' una doverosa concretizzazione del programma Pisapia che non si limita all’istituzione formale di un registro ma impegna anche Assessorati e Uffici competenti, “a tutelare e sostenere le unioni civili, al fine di superare situazioni di discriminazione e favorirne l'integrazione e lo sviluppo nel contesto sociale, culturale ed economico del territorio”.

L’Associazione Radicale Certi Diritti sostiene con convinzione tutte le iniziative che vanno in questa direzione e chiede con forza che la delibera venga immediatamente messa all’ordine del giorno per la discussione in Consiglio Comunale senza ulteriori attese. L’agenda politica di una città laica non può essere condizionata coloro che decidono di organizzarvi eventi o di farvi visita. Milano su questo tema è fanalino di coda non solo dell’Europa, ma anche dell’Italia, dove città come Torino sono all’avanguardia da anni. I diritti civili di migliaia di cittadini non possono più aspettare.  
  
*nella foto Marilisa D'amico insieme a Beppino Englaro e Barbara Pollastrini

**iscriviti alla newsletter >[  
http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**