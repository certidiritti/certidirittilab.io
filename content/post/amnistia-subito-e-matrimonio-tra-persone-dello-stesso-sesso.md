---
title: 'Amnistia subito! E matrimonio tra persone dello stesso sesso'
date: Sat, 25 Aug 2012 14:03:45 +0000
draft: false
tags: [Matrimonio egualitario]
---

![LogoCD](http://www.radicalparty.org/file/Logo_Cd_2012.JPG)

l’associazione radicale Certi Diritti partecipa e aderisce alla quattro giorni di mobilitazione nonviolenta sulla drammatica situazione della Giustizia e delle Carceri nel nostro paese.  
[**Firma l’appello al Presidente Napolitano su Amnistiasubito.it >**](http://www.certidiritti.org/2012/07/19/mobilitazione-nonviolenta-per-la-giustizia-lassociazione-radicale-certi-diritti-partecipa-alle-iniziative-e-sottoscrive-la-lettera-al-presidente-della-repubblica/)

Con l’assemblea nazionale del PD si chiude un breve ciclo evolutivo del dibattito pubblico sul**matrimonio civile tra persone dello stesso sesso** in Italia. Ciclo partito con la presentazione della proposta di legge di iniziativa popolare “Una volta per tutti” (Zan & C.) e concluso proprio con laformalizzazione in sede assembleare del documento sui diritti del PD che, nella sostanza, ripropone una istituzione simile ai DICO. [**Leggi l’intervento di Enzo Cucco, Presidente dell’associazione radicale Certi Diritti >**](http://gayindependent.blogspot.it/2012/07/piccole-buone-notizie.html)

L’Assemblea nazionale del PD ha riacceso il dibattito politico dopo le dichiarazioni di Obama e Hollande e la proposta di legge depositata alla Camera da Di Pietro, che si va ad aggiungere a quelle dei Radicali e di Paola Concia. Così anche Grillo e Vendola si sono espressi a favore del same-sex marriage, mentre Bersani pensa alle unioni civili modello tedesco.     
Non deve pensarla allo stesso modo la presidente del PD** Rosy Bindi che alla Festa dell’Unità di Roma non solo ha dichiarato il falso sostenendo che “una sentenza della Corte costituzionale ha vietato i matrimoni gay”** ma ha affermato anche: “rimpiangerete le unioni civili” e che il PD è chiamato “a governare il Paese e non una minoranza”. [Qui il video >](http://tv.ilfattoquotidiano.it/2012/07/19/matrimoni-bindi-costituzione-vieta-scoppia-bagarre-alla-festa-dellunita/201955/)        

L’associazione radicale ha iniziato la battaglia per l’uguaglianza dei diritti nel 2008 con la campagna di[**Affermazione Civile**](campagne-certi-diritti/itemlist/category/86-affermazione-civile), che ha visto oltre 30 coppie dello stesso sesso presentarsi in comune e chiedere le pubblicazioni di matrimonio.             
I ricorsi presentati in tribunale contro il prevedibile rifiuto in forma di documento ufficiale da parte degli uffici comunali hanno portato alla sentenza 138/10.       
La battaglia continua, anche in Europa con i due ricorsi alla CEDU.       
Qui riproponiamo [**l’intervento della costituzionalista Marilisa D'Amico sul nostro sito**,](http://www.certidiritti.org/2012/07/16/matrimonio-tra-persone-dello-stesso-sesso-e-principi-costituzionali/)quello di [**Stefano Rodotà su Repubblica**](http://www.spetteguless.it/2012/07/le-nozze-gay-secondo-stefano-rodota-e-filippo-ceccarelli/) e la [**Piccola Posta di Adriano Sofri su Ilfoglio.it**](http://www.certidiritti.org/2012/07/18/adriano-sofri-sul-matrimonio-tra-persone-dello-stesso-sesso/).

Intanto al consiglio comunale di Milano si sta arrivando alla discussione sul registro delle unioni civili.Certi Diritti, che sta raccogliendo le firme insieme ad altre associazioni, [**regala il libro ‘Dal cuore delle coppie al cuore del diritto’ sulla sentenza 138/10**](http://www.certidiritti.org/2012/07/20/registro-unioni-civili-a-milano-lassociazione-radicale-certi-diritti-regala-una-copia-del-libro-dal-cuore-delle-coppie-al-cuore-del-diritto-a-tutti-i-consiglieri-comunali/).  
Anche a Roma continua la raccolta firme sugli 8 referendum di [**Romasimuove**](http://www.romasimuove.it/attivati/), tra cui quello che chiede di garantire pari servizi e condizioni alle famiglie di fatto.

[**Manifesti con donna e codice a barre: ignobile campagna di politici demagogici**](http://www.certidiritti.org/2012/07/18/manifesti-con-donna-seminuda-e-codice-a-barre-contro-prostituzione-e-azione-ignobile-e-lesiva-delle-donne-e-delle-prostitute-firma-lappello-per-la-legalizzazione/) che non sanno distinguere tra prestazioni sessuali e vendita del corpo.               
[**Firma il nostro appello per la legalizzazione >**](campagne-certi-diritti/itemlist/category/85-legalizzazione-prostituzione)   

[Italia sempre più debitrice del Fondo mondiale contro l'Aids. Sostegno alla richiesa di chiarimenti al Governo italiano su lotta all'Aids >](http://www.certidiritti.org/2012/07/20/italia-sempre-piu-debitrice-del-fondo-mondiale-contro-laids-sostegno-alla-richiesa-di-chiarimenti-al-governo-italiano-su-lotta-allaids/)

Intervento del Senatore Marco Perduca del Direttivo di Certi Diritti [**sulla parità di presenza dei generi in Parlamento >**](http://perdukistan.blogspot.it/2012/07/sulla-parita-di-presenza-dei-generi-in.html)

[**La Senatrice radicale Donatella Poretti sollecita la risposta del Ministro sulle spiagge naturiste >**](http://www.youtube.com/watch?feature=player_embedded&v=oje_DsCByYU#!) 

La nostra rassegna stampa “**Fuor di pagina**” che avrebbe dovuto essere trasmessa da Radio Radicale a partire dal 21 luglio a seguito dei “quattro giorni di nonviolenza, di sciopero della fame e di silenzio” a cui la Radio aderisce, slitta di una settimana. **L’appuntamento inizierà da sabato 28 luglio alle 24,30. Ascoltala in podcast su [iTunes](http://itunes.apple.com/it/podcast/radioradicale-fuor-di-pagina/id530466683?mt=2.) o [su radio radicale >](http://www.radioradicale.it/rubrica/1004)**

**Contributi**:

Dal blog **[ilgrandecolibri.com](http://ilgrandecolibri.com/) [Carabinieri contro il feticismo: ai gay va bene così? ](http://www.certidiritti.org/2012/07/16/carabinieri-contro-il-feticismo-ai-gay-va-bene-cosi/) **

Da Liberi.tv nello Spazio Certi Diritti: **[Ama il prossimo tuo come te stesso, i gay credenti in Italia. Intervista ad Andrea Rubera (Nuova Proposta)](http://www.liberi.tv/webtv/2012/07/13/video/ama-prossimo-tuo-come-te-stesso-i-gay-credenti-italia) **di** Riccardo Cristiano**

_Partecipa anche tu alla campagna ‘Dai corpo ai tuoi Diritti’ inviando la tua foto a **[info@certidiritti.it](mailto:info@certidiritti.it)** e sostieni le nostre battaglie. Tutto quello che facciamo è solo grazie ai nostri iscritti: **[www.certidiritti.it/partecipa/iscriviti](partecipa/iscriviti)**_