---
title: 'Lettera appello al Governo e ai partiti sulla legge anti gay in Russia'
date: Tue, 05 Feb 2013 23:26:06 +0000
draft: false
tags: [Russia]
---

Chiediamo al Governo italiano e a tutte le forze politiche, di attivarsi immediatamente nei confronti del Federazione Russa affinché vi sia un ripensamento rispetto a una legge che reintroduce in Europa provvedimenti che si sperava, viste le persecuzioni dei decenni passati, non riemergessero più.

5 febbraio 2013

Al Presidente del Consiglio  
Alle forze politiche italiane

La Duma Federale Russa ha approvato in prima lettura una legge che renderà illegale scrivere un articolo, organizzare eventi o parlare in pubblico di omosessualità. Analoghe leggi sono già  state approvate nelle regioni di Rjazan' nel 2006, Archangel'sk nel 2011 e Kostroma e San Pietroburgo nel 2012, e anche le regioni di Novosibirsk, Samara, Kirov, Krasnojarsk e Kaliningrad stanno considerando l'adozione di norme simili.

L’omosessualità è stata depenalizzata nella Federazione Russa nel 1999 ora ritorna ad essere una condizione perseguita, e purtroppo le opposizioni pubbliche sono sistematicamente represse attraverso fermi e licenziamenti.

Un forte segnale di protesta è giunto negli scorsi giorni dal Consiglio Comunale di Venezia, che all’unanimità ha denunciato questi fatti e impegnato la Giunta a sospendere ogni accordo di cooperazione con la città di San Pietroburgo. Il Consiglio Comunale di Milano ha deciso nei mesi scorsi la sospensione del gemellaggio con la stessa città.

L’Unione Europa attraverso una dichiarazione di Catherine Ashton, rappresentante per la politica estera europea, ha sottolineato come ''la messa in atto di questa legge può rafforzare la discriminazione contro lesbiche, gay, bisessuali e transessuali così come contro chi difende le loro scelte, in particolare limitando le loro libertà d'espressione, di associazione e di riunione''. Nella dichiarazione inoltre si fa specifico riferimento agli impegni internazionali della Russia ''nel quadro del Consiglio d'Europa, come firmataria della Convenzione dei diritti umani''

Non stiamo parlando di un Paese che non ha mai conosciuto l'alfabeto dei diritti civili ma di una pericolosa regressione che a dispetto di scienza e cultura si attesta su crinali di pericolosità a noi storicamente noti. Alla stato di fatto non valgono le migliaia di studi scientifici in materia di orientamento sessuale e identità di genere e nemmeno l'attestarsi della battaglia per i diritti civili in tutti i programmi politici di rilevanza internazionale. E dal prossimo voto, quello definitivo, a cui seguirà solo la firma del Presidente Putin, in una parte dell'Europa gli omosessuali non potranno più vivere e parlare liberamente.

Chiediamo al Governo italiano e a tutte le forze politiche, di attivarsi immediatamente nei confronti del Federazione Russa affinché vi sia un ripensamento rispetto a una legge che reintroduce in Europa provvedimenti che si sperava, viste le persecuzioni dei decenni passati, non riemergessero più, e di agire nelle istituzioni Europee al fine di costruire un’azione politico diplomatica adatta a far sentire la voce di tutti i Paesi del nostro Continente.

Primi firmatari:

Camilla Seibessi, consigliera comunale di Venezia  
Ritanna Armeni, giornalista scrittrice  
Angela Azzaro, vice direttore settimanale gli Altri  
Laura Balbo, sociologa  
Paola Brandolini, presidente nazionale Arcilesbica  
Lella Costa, attrice  
Franco Grillini, presidente nazionale Gaynet  
Yuri Guaiana, segretario nazionale Associazione Radicale Certi Diritti  
Rita De Santis, presidente Agedo  
Giuseppina La Delfa, presidente nazionale Famiglie Arcobaleno  
Vladimir Luxuria, artista  
Andrea Maccarone, presidente Circolo di Cultura Omosessuale Mario Mieli  
Aurelio Mancuso, presidente nazionale Equality Italia  
Fabrizio Marrazzo, portavoce Gay Center  
Flavio Romani, presidente nazionale Arcigay  
Piero Sansonetti, direttore Calabria Ora e gli Altri  
Chiara Saraceno, sociologa