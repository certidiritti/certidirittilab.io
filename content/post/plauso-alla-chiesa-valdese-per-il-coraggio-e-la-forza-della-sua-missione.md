---
title: 'Plauso alla Chiesa Valdese, per il coraggio e la forza della sua missione'
date: Fri, 27 Aug 2010 08:00:19 +0000
draft: false
tags: [certi diritti, chiesa valdese, Comunicati stampa, coppie gay, Sergio Rovasio, valdesi]
---

**PLAUSO ALLA CHIESA VALDESE PER IL CORAGGIO E LA FORZA DELLA SUA MISSIONE RELIGIOSA E PASTORALE.**

**Comunicato Stampa dell’Associazione Radicale Certi Diritti:**

“La Chiesa Valdese, approvando l’odg in favore sulle benedizioni delle coppie gay e lesbiche, ha dimostrato quanto la tolleranza e l’amore siano il vero cammino che dovrebbe ispirare i valori del cristianesimo.

La Chiesa Valdese dimostra con questo gesto di rimanere aperta alle realtà sociali che chiedono di non essere escluse e messe ai margini come purtroppo in Italia ancora accade nei confronti delle persone lesbiche e gay.

L’Associazione Radicale Certi Diritti ha invitato nei mesi scorsi i propri sostenitori e iscritti a devolvere l’8 per mille alla Chiesa Valdese perché il messaggio di attenzione e tolleranza verso i più deboli  è  un dato concreto e forte nelle loro azioni.