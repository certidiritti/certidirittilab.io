---
title: 'RIUNIONE DI CERTI DIRITTI ROMA'
date: Sat, 28 Nov 2009 07:10:39 +0000
draft: false
tags: [Senza categoria]
---

Carissime e carissimi,  
è un bel po’ che non ci incontriamo, anche a causa dei molteplici impegni di ciascuno di noi. Che ne dite di una bella riunione, giusto per ricordarci i nostri volti?  
Lunedì prossimo, 30 novembre, abbiamo pensato di prendere tre piccioni con una fava; l’occasione è un direttivo della nostra associazione, via Skype, che si terrà nel pomeriggio dalle ore 18.00 alle ore 20.00.  
L’idea sarebbe quella di riunire i membri romani del direttivo per partecipare tutti assieme alla riunione. A questo punto è venuta spontanea la proposta di fare, nelle due ore precedenti, anche una riunione di CD Roma, i cui partecipanti potranno poi fermarsi ad assistere al successivo Direttivo Nazionale, così da vedere come viene gestita la nostra associazione e conoscerne, almeno in voce, i dirigenti nazionali.  
L’appuntamento è quindi fissato alle ore 16.00 alla sede del Partito Radicale, Via di Torre Argentina 76; all’ordine del giorno ci saranno le istanze da portare alla prossima riunione del coordinamento nazionale delle associazioni lesbo, trans e gay (19 dicembre), i preparativi per il congresso nazionale di Certi Diritti (30-31 gennaio) e lo stato delle iniziative che stiamo mettendo in atto per la primavera.  
Dopo le due riunioni, chi vuole, può venire con noi alla chiesa anglicana di All Saints di via del Babuino 153, dove il Roma Rainbow Choir si esibirà in occasione della XXII giornata mondiale per la lotta all'AIDS. Finito il concerto andiamo tutti a mangiare una pizza…  
Allora, che ne dite? A lunedì prossimo?  
  
CERTI DIRITTI  
Coordinamento di Roma  
Luca "Lucky" Amato  
roma@certidiritti.it