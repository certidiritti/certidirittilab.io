---
title: 'Certi Diritti aderisce alla Festa delle Famiglie 2018'
date: Thu, 03 May 2018 18:15:02 +0000
draft: false
tags: [Movimento LGBTI]
---

**Certi Diritti** aderisce alla **Festa delle Famiglie 2018** organizzata da **Famiglie Arcobaleno** a **Milano** in via Licata (Parco Lambro). Appuntamento a sabato 6 maggio per un pomeriggio di giochi e attività per bambini a partire dalle 15. [![festa famiglie 2018](http://www.certidiritti.org/wp-content/uploads/2018/05/festa-famiglie-2018-212x300.jpg)](http://www.certidiritti.org/wp-content/uploads/2018/05/festa-famiglie-2018.jpg)