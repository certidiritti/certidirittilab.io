---
title: 'DOMENICA 23-1 BERNARDINI E ROVASIO NEL CARCERE DI SULMONA'
date: Sat, 22 Jan 2011 19:41:51 +0000
draft: false
tags: [Comunicati stampa]
---

##### Suicidio nel carcere di Sulmona: domenica 23 gennaio delegazione Radicale in visita nell’istituto che detiene il drammatico record dei suicidi. Ore 15,30 incontro con la Stampa fuori dal carcere

**SULMONA (AQ), CARCERE SULMONA**, CON RITA BERNARDINI, SERGIO ROVASIO - 23/01/2011 - 12:00

Domenica 23 Gennaio Rita Bernardini, deputata radicale eletta nel Pd, Sergio Rovasio, Segretario dell’Associazione Radicale Certi Diritti, e Giulio Petrilli, responsabile Diritti Umani del Pd de L’Aquila, visiteranno, a partire dalle ore 12 circa, il carcere di Sulmona. Nel reparto "internati" dell'istituto, il 19 gennaio si è tolto la vita, impiccandosi, un egiziano di 66 anni. Il Carcere di Sulmona detiene il drammatico record dei suicidi di detenuti.  
**Alle ore 15,30 la delegazione radicale incontrerà la Stampa fuori dal Carcere di Sulmona**.  
Negli ultimi 2 giorni tre detenuti , in poche ore, si sono tolti la vita nelle carceri di Sulmona, Prato e Caltagirone.