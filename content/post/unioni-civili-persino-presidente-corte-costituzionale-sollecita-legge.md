---
title: 'UNIONI CIVILI: PERSINO PRESIDENTE CORTE COSTITUZIONALE SOLLECITA LEGGE'
date: Tue, 13 Jan 2009 08:35:48 +0000
draft: false
tags: [Affermazione Civile, Comunicati stampa, CORTE COSTITUZIONALE, unioni civili]
---

UNIONI GAY: IL PRESIDENTE DELLA CORTE COSTITUZIONALE HA COLTO NEL SEGNO. SE NON CI SARANNO INTERVENTI LEGISLATIVI SI OCCUPERA’ LA GIUSTIZIA DELL’INGIUSTIZIA.

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

 “Tutte le più importanti battaglie sui diritti civili, dalla marcia del sale di Gandhi alla lotta alla segregazione razziale americana, hanno sempre visto la classe politica un passo indietro rispetto alle richieste di giustizia e di libertà dei cittadini. Nelle democrazie la mediazione di queste istanze di lotta delle ingiustizie l’ha sempre dovuta affrontare per prima la Giustizia.

 In Italia il caso Englaro è quello che più di tutti vale come esempio: la Corte di Cassazione, dopo vari passaggi legali, ha dato il via libera alle richieste della famiglia che non poteva avvalersi di una legge di regolamentazione, richiesta da tutti ma bloccata dal fondamentalismo religioso.

 Sul tema del riconoscimento delle unioni civili in Italia siamo due passi indietro, rispetto a quasi tutti gli altri paesi dell’Unione Europea, e se persino il Presidente della Corte Costituzionale lo ha sottolineato vuol dire che il problema esiste e va superato con la legge. Intanto l’Associazione Radicale Certi Diritti ha avviato da alcuni mesi una campagna di ‘Affermazione civile’ che ha l’obiettivo di incardinare iniziative legali in tutta Italia per il riconoscimento delle unioni tra persone gay. Vedremo quanto tempo ci metterà la classe politica a comprendere che anche l’ingiustizia di questo mancato riconoscimento va superata, intanto del tema se ne occuperà la Giustizia”.