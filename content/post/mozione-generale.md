---
title: 'Mozione Generale approvata dal VII Congresso'
date: Mon, 13 Jan 2014 09:32:11 +0000
draft: false
tags: [Politica]
---

L’Italia vive una nuova stagione di sessuofobia, che nel corso degli ultimi anni è diventata la causa principale dell’impossibilità a raggiungere le riforme di cui abbiamo bisogno, sia in materia di lotta ai crimini d’odio e ai discorsi d’odio, che in materia di lotta alle discriminazioni, alla riforma del diritto di famiglia ed al riconoscimento del matrimonio egualitario, alla difesa della autodeterminazione sui corpi e sugli affetti, alla lotta contro le malattie sessualmente trasmesse, alla regolamentazione della prostituzione, alla legalizzazione della professione di assistente sessuale e ad una efficace lotta contro ogni forma di bullismo.

Le forze che in Italia combattono questa realtà sono poche, e non alleate tra loro. Mancano della consapevolezza dei pericoli che questa situazione provoca, e spesso pericolosamente attratte dalla demagogia dell’affidarsi alla leva penale per reprimere fenomeni discriminatori così come dalla falsa speranza nelle nuove generazioni che, se non allenate alla libertà responsabile ed alla conoscenza consapevole sono pronte nel ripetere fobie ed errori del passato.

Nuova sessuofobia, nuovo maschilismo, nuovi falsi moralismi sono quindi i nemici contro i quali, consapevoli della loro pervasività ed influenza dobbiamo attrezzarci.

L’Associazione Radicale Certi Diritti, pur nell’eccezionalità dei risultati già ottenuti, non è, in questa fase della sua vita, all’altezza della sfida, né singolarmente come associazione, né nelle sue alleanze. Gli sforzi compiuti in questi anni, nel merito e nel metodo di iniziative come Affermazione civile ed SOS Russia, hanno costituito esempi di attivismo consapevole, efficace e condiviso, ma rimangono lontani dai bisogni che in materia di piena uguaglianza e riconoscimento di diritti e doveri di tutti sono ormai chiari.

L’associazione, consapevole di questa realtà, comincia dal 2014 un percorso di rinnovamento e rilancio delle proprie attività, sia dal punto di vista del riesame delle proprie priorità, che devono diventare sempre più coerenti con la necessità di combattere le nuove forme di sessismo, sia dal punto di vista organizzativo.

Questo percorso non comporta l’interruzione, e anzi richiede il rilancio, di battaglie fondamentali per l’uguaglianza che abbiamo intrapreso come quelle di Affermazione civile, per il matrimonio egualitario, ma necessita di uno sforzo condiviso e costante per rilanciare conoscenza ed attivismo sulle nuove emergenze che abbiamo di fronte.