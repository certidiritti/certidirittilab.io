---
title: 'APPELLO ALLA FRANCIA PER SALVARE RAGAZZO GAY IRANIANO'
date: Tue, 23 Jun 2009 15:16:58 +0000
draft: false
tags: [Comunicati stampa]
---

**LIBERTA’ E ASILO PER IL GAY IRANIANO VAHID, DETENUTO A LIONE**

**Mercoledì 24 giugno, alle 22, performance alla Gay Street di Roma per chiedere la salvezza del profugo iraniano gay**

L’Associazione Radicale Certi Diritti si unisce all’appello del Gruppo EveryOne e della Fondazione Massimo Consoli per chiedere l’immediata scarcerazione e sospensione del provvedimento di deportazione in Iran di Vahid Kiani Motlagh, 32enne omosessuale iraniano rinchiuso dal 25 maggio – data in cui è stato fermato mentre transitava clandestinamente in Francia per raggiungere il Belgio con il compagno – nel centro di detenzione dell’aeroporto Saint-Exupery di Lione.

“Se rimandato in Patria, come vorrebbe il giudice del tribunale amministrativo lionense,” dichiara Sergio Rovasio, segretario dell’Associazione Radicale Certi Diritti “Vahid verrebbe giustiziato con l’accusa di lavat (sodomia) a causa del proprio orientamento sessuale, così come è stato per il giovane Makwan, cui la nostra Associazione Radicale è dedicata, impiccato nel carcere di Kermanshah nel dicembre 2007. Da Bruxelles” aggiunge Rovasio “il nostro responsabile per le questioni europee, Ottavio Marzocchi, ha mobilitato tutti i membri dell’intergruppo LGBT al Parlamento Europeo, tra cui l’eurodeputato Michael Cashman, affinché si occupino quanto prima del caso e intercedano presso il Governo francese per far sì che Vahid si veda riconosciuto, come sarebbe legittimo in un Paese civile e membro dell’Unione,   il diritto fondamentale all’asilo come rifugiato, in linea con le norme europee e internazionali”.

Domani sera, a partire dalle 22, in via San Giovanni Laterano, Certi Diritti, assieme al Gruppo EveryOne, alla Fondazione Massimo Consoli, ad Azione Trans, GayNet e ad Arcigay e Arcilesbica Roma, parteciperà a una performance dove verranno distribuiti ‘green ribbons’, fiocchi verdi, ai passanti e verrà affisso un cartello "VAHID LIBERO".

**Per ulteriori informazioni:**

[www.certidiritti.it](http://www.certidiritti.it/) :: [certidiritti@radicali.it](mailto:certidiritti@radicali.it)