---
title: 'LETTERA DI AMNESTY INTERNATIONAL SU FIACCOLATA SAN PIETRO'
date: Fri, 05 Dec 2008 17:54:52 +0000
draft: false
tags: [Comunicati stampa]
---

Spettabili organizzazioni,  
  
in occasione della vostra manifestazione, prevista per domani pomeriggio,  
desidero  
porgervi il saluto e la solidarietà della Sezione Italiana di Amnesty  
International a favore dell’importante obiettivo di rimuovere in  
ogni Paese del mondo la criminalizzazione degli atti omosessuali e,  
più in generale, a sostegno della comune lotta contro la  
discriminazione subita dalle persone lesbiche, gay, bisessuali e  
transgender.

Amnesty International considera prigionieri di coscienza coloro che  
vengono detenuti a causa del loro reale o presunto orientamento  
sessuale o identità di genere, e ne chiede il rilascio immediato e  
incondizionato. Inoltre, chiede a tutti i Paesi di rimuovere ogni  
forma di discriminazione, inclusa la criminalizzazione  
dell'omosessualità.  
  
Le leggi che criminalizzano la comunità LGBT rafforzano la condizione  
di ingiusto svantaggio in cui si trovano le persone lesbiche, gay,  
bisessuali e transgender, costituendo un incitamento e spesso una  
giustificazione alla discriminazione e alla violenza: in strada, a  
casa, in prigione e in ogni altra sfera sociale.  
  
Le norme che criminalizzano gli atti omosessuali, ovvero  
l’orientamento sessuale reale o presunto, permettono ad esempio alle  
forze dell’ordine di invadere le residenze e la vita privata delle  
persone, e spesso queste norme garantiscono l’impunità anche a fronte  
di torture, maltrattamenti e arresti arbitrari fino a giungere alla  
pena di morte.  
  
Senza la protezione della legalità, è impossibile per gli attivisti  
dei diritti umani costituire organizzazioni e attivarsi per la tutela  
dei diritti privando le vittime delle violazioni dell’accesso alla  
giusitizia.  
  
Amnesty International conferma, dunque, il proprio impegno nella lotta  
a ogni forma di discriminazione ribadendo come l’orientamento  
sessuale e l'identità di genere, al pari di origine etnica,  
religione, genere, nazionalità, facciano parte dei caratteri  
fondamentali dell’identità umana e in quanto tali vadano tutelati  
come diritti umani nel senso più completo del termine.  
  
Nel salutarvi, vi auguro una buona riuscita per la manifestazione di  
domani.  
  
Daniela Carboni  
  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
Daniela Carboni  
Direttrice Ufficio Campagne e Ricerca  
Amnesty International - Sezione Italiana  
tel: +39 06 4490228, +39 348 2894276, fax: +39 06 4490222  
  
Partecipa su [www.amnesty.it](http://www.amnesty.it/) alla campagna di Amnesty International  
per fermare le violazioni dei diritti umani nella "guerra al terrore"  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
  
Le comunicazioni effettuate per mezzo di Internet non sono affidabili e  
pertanto Amnesty International non si assume responsabilità legale per i  
contenuti di questa mail e di eventuali allegati. L'attuale infrastruttura  
tecnologica non può garantire l'autenticità del mittente né dei contenuti  
di questa mail.  
Se lei ha ricevuto questa mail per errore, è pregato di non utilizzare le  
informazioni in essa riportate e di non portarle a conoscenza di alcuno.  
Opinioni, conclusioni e altre informazioni contenute in questa mail  
rappresentano punti di vista personali e non, salvo quando espressamente  
indicato, quelli di Amnesty International.