---
title: 'Uganda e Nigeria: l''omofobia e la transfobia di Stato dilagano in Africa'
date: Sat, 21 Dec 2013 09:42:40 +0000
draft: false
tags: [Africa]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti.

Roma, 20 dicembre 2013

Il macabro regalo di Natale promesso l'anno scorso dalla speaker del Parlamento ugandese, Rebecca Kadaga, è arrivato con un anno di ritardo e depotenziato, ma è arrivato. Il famigerato 'Anti-homosexuality bill' è stato approvato oggi dal governo ugandese e adesso attende solo la firma del presidente Yoweri Museveni per entrare in vigore. Rispetto al testo originario non è o prevista la pena capitale, fortunatamente, ma l'omosessualità «recidiva» viene punita più severamente di prima: la pena massima è addirittura l'ergastolo. 7 anni di prigione sono invece previsti per chi si macchia del reato di «promozione di omosessualità», rendendo così assai difficile il lavoro delle ONG ugandesi e internazionali che lavorano sui diritti umani.

Martedì 17 dicembre, invece, il Senato nigeriano ha adottato un provvedimento che sanziona con 5 anni di carcere «chiunque s'iscriva, partecipi o operi in associazioni o club gay o faccia, direttamente o indirettamente, mostra in pubblico di una relazione amorosa tra persone dello stesso sesso». Inoltre si punisce con la reclusione a 5 anni chiunque (anche i turisti) sia sposato a una persona dello stesso sesso o sia stato un testimone di nozze. Anche in questo caso si tratta di un aggravamento della situazione precedente che puniva le attività omosessuali con 14 anni di carcere e, in 12 Stati del Nord, addirittura con la pena di morte. Anche in Nigeria le legge attende ancora la firma presidenziale per entrare in vigore.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: «quest'ondata di omo-transfobia di Stato che sta spazzando l'Africa Sub-Sahariana è estremamente preoccupante per l'incolumità delle persone LGBTI e per le intollerabili limitazioni alle attività degli attivisti per i diritti umani anche internazionali. Esprimo la mia vicinanza e solidarietà agli amici Francis John Onyang, già Presidente onorario dell'Associazione Radicale Certi Diritti, Frank Mugisha, direttore dello Smug (Sexual Minorities Uganda), Pepe Julian Onziema, Kasha Jacqueline Nabagesera e a tutti gli attivisti ugandesi che si battono strenuamente per i diritti umani delle persone LGBTI. Continueremo a essere al loro fianco nella lotta comune per i diritti umani con ben fissa nella memoria la figura di David Kato Kisule, già iscritto all'Associazione Radicale Certi Diritti. Il primo passo è quello di chiedere all'Italia, all'Unione Europea e a tutta la comunità internazionale di condannare fermamente ed esplicitamente queste leggi e di fare pressioni sui Presidenti nigeriano e ugandese affinché non firmino».