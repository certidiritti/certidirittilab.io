---
title: '2008-0140-Discrimination-ST05568.EN10'
date: Wed, 20 Jan 2010 12:14:21 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 20 January 2010

Interinstitutional File:

2008/0140 (CNS)

5568/10

LIMITE

SOC 35

JAI 68

MI 21

  

  

  

  

  

NOTE

from :

The General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

16063/09 SOC 706 JAI 836 MI 434

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached a note from the UK Delegation with a view to the meeting of the Working Party on Social Questions on 22 January 2010.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Note from the UK delegation**

**Subject: Art. 2(7), proportionate differences in treatment on the grounds of age and the provision of Financial Services**

The UK Government recently completed a thorough analysis of the potential issues that arise in prohibiting age discrimination in the context of the provision of financial services. This involved issuing a public consultation, commissioning independent research[\[1\]](#_ftn1) and convening a working group of financial services experts. Our intention was and remains to ensure that the financial services sector could continue to treat people differently on the grounds of their age and disability where this is fair and reasonable – but that harmful and unjustifiable age discrimination should be eradicated.

The following practices were assessed and concluded to have the potential to be beneficial to consumers. We would be interested in views from the Commission, Council Legal Service and other Member States on how Art. 2(7) of the proposed Directive impacts on these practices.

**1.         The use of age bands**

Some insurance products are priced to reflect the typical risk posed by individuals in a particular age band.

Removing age bands will make insurance more expensive to sell. These costs will be passed on to consumers, with the result that the cost (particularly over the course of a lifetime) will be much higher than consumers currently pay. We believe it is therefore counter-productive to prohibit age bands.

Narrowing of age bands would result in a smoother price structure by way of a redistribution of prices. However, this is likely to result in higher prices at the older ends of the bands, which at present receive a subsidy from younger ages within the same band. In addition firms would have increased cost with regards to calculating premiums and there would be a need for more detailed underwriting, costs which would be passed on to consumers.

  

Where detailed underwriting procedures are involved, age bands are more likely to be relatively narrow – in motor insurance, for example. For other products the bands might be wider to reflect a simpler underwriting process by grouping people into age bands, and charging similar premiums to everyone in that band. This keeps the prices down and allows for easier distribution. This is relevant to travel insurance, where relatively few risk factors are considered, and where the level of risk posed by individuals within wide age bands does not vary significantly. The independent research acknowledges that whilst such a pricing structure in travel insurance does not fully reflect the underlying risk for each individual, it keeps the pricing simple and cheap.

The UK has therefore concluded that the use of age bands is desirable.

**_Would the current text permit the use of different age bands for different products?_**

**2.         The benefits of market specialisation**

Some insurers specialise in particular age groups (for example older people), and this expert knowledge allows them to deliver a better service more cheaply to their age group. Provided the market is adequately covered it is to the general benefit to consumers to allow market specialisation. Specialisation also means that insurers are also more likely to be able to cover the extremes of the market, where data about risks can be very thin.

We therefore think it is important to allow financial service providers to specialise.

**_Would the current text permit this?_**

**3.         The use of age limits**

Unlike gender, there will be some age groups about whom there is insufficient data to assess risk. This is generally at the extremes. For example, there are so few drivers over the age of 90 that it is statistically very difficult to assess risk.

In such circumstances, we want to ensure that insurers have the freedom to choose not to offer their services, where they feel that it exposes them to an unquantifiable risk. This means that the industry must be able to set reasonable age limits for their services. But it would mean that in some cases this will mean refusing to provide a quotation or service.

We believe that the best way of ensuring that market extremes are covered is through market specialisation. By allowing an insurer to specialise in a particular market, they will acquire the data and experience necessary to assess risk more accurately.

Age limits also allow diversity of product features. If insurers were obliged to offer the same product features to all consumers regardless of age, it is likely that they would reduce the variety and quality of those features to the detriment of all. For example, insurers may choose to withdraw benefits for all drivers in order to avoid having to offer them to some (for example young) drivers.

**_Would the current text allow financial service providers to set age limits for their products?_**

**4.         Measures avoiding over-exposure to unexpected risks**

Financial services providers need to be able to manage their risk portfolio as a whole. Financial services providers, in effect, pool risks across society, using the returns from good risks to bear the costs of bad risks.

It is valid for providers to be concerned about having too many of a certain type of risk on their books – for example, if their assumptions about longevity in the cohort born in the 1940s prove wrong, providers need to ensure that they are not disproportionately affected. If a provider is significantly exposed to one particular age group, it may be valid to seek to attract more people from other age groups, and to deter customers from the age group that is already over-represented.

  

**_Would the Directive provide service providers with the freedom to decide how to manage their portfolios, including by changing the profile of those it chooses to insure, in order to control their financial risk?_**

**\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_**

  

* * *

Available at [http://www.equalities.gov.uk/news/age_consultation.aspx](http://www.equalities.gov.uk/news/age_consultation.aspx)