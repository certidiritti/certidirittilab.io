---
title: 'BERLUSCONI NON APRIRA'' CONFERENZA SU FAMIGLIA: ALLORA VENGA DA NOI'
date: Sat, 06 Nov 2010 09:30:07 +0000
draft: false
tags: [Comunicati stampa]
---

**CONFERENZA GOVERNATIVA SULLA FAMIGLIA,  LETTERA-INVITO  AL PRESIDENTE DEL CONSIGLIO:  “SE NON LA APRE PER EVITARE STRUMENTALIZZAZIONI, VENGA ALLA NOSTRA DOVE ABBIAMO DA FARE PROPOSTE DI GOVERNO SULLE DIVERSE TIPOLOGIE DI FAMIGLIE ESISTENTI E CHE CHIEDONO DIRITTI, NON PROPAGANDA”.**

**Comunicato stampa dell'Associazione Radicale Certi Diritti**

Roma, 6 novembre 2010

Onorevole Presidente del Consiglio,

dopo il diktat di Francesco Belletti, presidente del Forum delle associazioni familiari, che aveva dichiarato che la sua presenza alla Conferenza nazionale della famiglia li avrebbe "imbarazzati", Lei ha deciso di non inaugurarla e di lasciare questo compito al Sottosegretario Giovanardi.

Dobbiamo dedurre che l'agenda politica del Presidente del Consiglio è dettata dal Presidente del Forum delle associazioni familiari o che vuole prendere le distanze da un convegno che ha la pretesa di «sviluppare opportuni criteri di adeguatezza famigliare» in un’irrefrenabile corsa verso un modello ideologico e inesistente di famiglia.

In ogni caso, l’Associazione Radicale Certi Diritti e Radicali Italiani non provano il minimo imbarazzo ad invitarLa alla vera Conferenza sulle famiglie (al plurale) che si terrà Lunedì 8 novembre 2010 dalle 10:30 alle 19:00 presso la Sala Lauree dell’Università degli Studi di Milano - Facoltà di Scienze Politiche, via Conservatorio, 7, dove - insieme a giuristi, economisti, antropologi, sociologi e psicologi - cercheremo di trovare i modi migliori e più concreti per governare la realtà delle famiglie italiane con proposte.

Al nostro seminario si parlerà di temi che potrebbero riguardare direttamente Lei e molti membri della sua maggioranza come il divorzio breve, le unioni civili, lo stereotipo maschile tra potere e libertà, il mito della famiglia naturale e la malattia che affligge la famiglia dal punto di vista psicoanalitico.

Si parlerà anche di famiglie omogenitoriali, di matrimonio tra persone dello stesso sesso, di comunità intenzionali e di filiazione tra scienza e diritti, temi che le permetterebbero di comprendere molte cose che riguardano milioni di cittadini italiani finora ignorati dal suo Governo.

Speriamo allora che voglia accettare il nostro invito e partecipare ai lavori del nostro seminario per una Riforma del Diritto di Famiglia inclusiva, non di esclusiva proprietà della chiesa cattolica.

Associazione Radicale Certi Diritti

[www.certidiritti.it](http://www.certidiritti.it/)