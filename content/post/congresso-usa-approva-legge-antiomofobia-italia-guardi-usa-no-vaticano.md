---
title: 'CONGRESSO USA APPROVA LEGGE ANTIOMOFOBIA: ITALIA GUARDI USA NO VATICANO'
date: Sat, 24 Oct 2009 09:05:52 +0000
draft: false
tags: [Comunicati stampa]
---

**CONGRESSO USA APPROVA LEGGE ANTIOMOFOBIA: L’ITALIA FAREBBE BENE A GUARDARE AGLI USA PIUTTOSTO CHE ALLO STATO TEOCRATICO VATICANO**.

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

“Il Congresso Usa, nell’approvare oggi la legge contro l’omofobia, dimostra che alcune forme di violenza, come gli ‘hate crimes’, devono essere combattuti con specifiche iniziative legislative. Il Congresso Usa ha inserito i crimini provocati dal sesso e dall’orientamento sessuale delle vittime tra quelli provocati a causa della razza, del colore della pelle, e della religione o la provenienza nazionale delle vittime. Gli Stati Uniti con l’approvazione di questa legge, dedicata alla memoria di Mattew Shepard, studente assassinato 11 anni fa perche' omosessuale, insegnano ancora una volta ai paesi democratici che non sono né gli integralismi ideologici né quelli religiosi a poter dettare e influenzare l’agenda politica così come avviene in Italia dove la classe politica è succube, genuflessa e prostrata ai voleri dello Stato teocratico Vaticano”.