---
title: 'Parlamento Europeo approva Risoluzione di condanna nei confronti della Russia per le leggi omofobe'
date: Thu, 16 Feb 2012 13:17:30 +0000
draft: false
tags: [Russia]
---

Il Parlamento europeo ha votato una risoluzione contro la Russia per la legge che se approvata vieterà di pubblicare testi su omosessualità e transgender. Russia rispetti le convenzioni internazionali che ha sottoscritto.

Roma – Bruxelles, 16 febbraio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Oggi il Parlamento Europeo ha votato una Risoluzione presentata da cinque diversi gruppi politici dove si esprime una forte condanna nei confronti della Russia per il divieto di pubblicare testi su omosessualità e identità di genere.  Il testo richiama la Russia al rispetto delle diverse convenzioni sui diritti umani che ha sottoscritto inclusa la Convenzione Europea sui Diritti Umani e l’ International  Covenant on Civil and Political Rights.

Nel testo della Risoluzione approvata dal Parlamento Europeo viene duramente condannata l’adozione, da parte dell’Assemblea Legislativa di San Pietroburgo, della legge appena votata contro la ‘propaganda sull’orientamento sessuale’  e le leggi simili approvate nelle regioni  Ryazan, Arkhangelsk e Kostroma. Il Parlamento Europeo chiede alla autorità russe di sospendere le restrizioni riguardanti la libertà di  espressione in relazione all’orientamento sessuale e l’identità di genere delle persone.  Nel testo inoltre si chiede alla Vice Presidente della Commissione Europea  e Alto rappresentante per gli Affari Esteri e la politica di sicurezza dell'Unione Europea di opporsi a queste gravi leggi omofobe.

L’Associazione Radicale Certi Diritti ringrazia il Parlamento Europeo per questo importante voto di condanna e i promotori di questa iniziativa: Michael Cashman e Sophie in’ t Veld, Parlamentari  europei e  Ottavio Marzocchi, funzionario della Commissione Libertà Pubbliche del Parlamento Europeo,  membri dell’Intergruppo Lgbt al Parlamento Europeo.

*nella foto Sophie in’ t Veld