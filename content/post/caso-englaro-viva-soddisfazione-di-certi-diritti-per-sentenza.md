---
title: 'CASO ENGLARO: VIVA SODDISFAZIONE DI CERTI DIRITTI PER SENTENZA'
date: Fri, 14 Nov 2008 14:09:55 +0000
draft: false
tags: [Comunicati stampa]
---

**CASO ENGLARO: CERTI DIRITTI ESPRIME VIVA SODDISFAZIONE PER LA SENTENZA DELLA CASSAZIONE SULLA VICENDA. HA VINTO LA LAICITA', NON CERTO IL FONDAMENTALISMO.**

 

Trieste, 14 novembre 2008

L'Associazione Radicale Certi Diritti esprime la più viva soddisfazione per  la sentenza emessa dalla Cassazione relativamente al caso di Eluana Englaro. La decisione della Suprema Corte avversa al ricorso della Procura di Milano, pone la parola fine alla penosa vicenda  della donna in coma irreversibile  da quasi 17 anni e sottolinea con maggior vigore un vuoto legislativo sulle questioni di fine vita che a tutt'oggi rimangono tristemente relegate ad una sfera di decisionismo privato. E' stata infatti la forza e la caparbietà di una persona come Beppino Englaro, padre di Eluana, a far si che il testamento biologico espresso dalla figlia in vita fosse riconosciuto come legalmente valido.  
E la riconoscenza dell'Associazione va a queste persone che, private di diritti, hanno il coraggio di affrontare un percorso giudiziario, pagando lo scotto di vivere in un Paese non laico e non incline a sancire il diritto all'autoderminazione dei cittadini così come previsto dalla Costituzione. Certi Diritti è vicina a Beppino Englaro e si congratula per la sua tenacia con cui ha affrontato una vicenda così dolorosa, lunga e faragginosa.

L'Associazione Certi diritti, che pure ha deciso di intraprendere le vie legali per il riconoscimento dei diritti civili alle coppie omosessuali, trae forza dalle parole rilasciate da Englaro: "Questa sentenza è la conferma che viviamo in uno stato di diritto" e si augura che pure la politica si svegli dallo stato di torpore in cui è avvolta e che ascolti e dia risposte ai bisogni delle persone contro ogni fondamentalismo religioso e ideologico.