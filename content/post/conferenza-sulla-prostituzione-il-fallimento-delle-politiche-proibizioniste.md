---
title: 'CONFERENZA SULLA PROSTITUZIONE: IL FALLIMENTO DELLE POLITICHE PROIBIZIONISTE'
date: Sat, 10 Jul 2010 11:43:39 +0000
draft: false
tags: [Comunicati stampa, conferenza, convegno, diritti civili, Prostituzione, roma, sex workers]
---

**Prostituzione: il fallimento delle politiche proibizioniste. _Le proposte di regolamentazione. Lotta alla tratta e allo sfruttamento della prostituzione_**

ROMA, Mercoledì 14 luglio, ore 15/18.30, presso il Partito Radicale, Via di Torre Argentina 76, **ROMA**

* * *

Il fenomeno della prostituzione in Italia coinvolge **centinaia di migliaia di persone** tra prostitute/i, clienti, bande criminali, schiave sfruttate, avvocati, forze dell’ordine, politici, cittadini comuni, famiglie, ecc. Il **mercato nero** della prostituzione produce  un mercato di proporzioni enormi che si alimenta  grazie alle politiche proibizioniste e criminogene promosse in ambito locale in ogni parte del paese grazie a iniziative demagogiche e populiste inutili, ridicole e spesso stupide oltre che gravemente lesive e violente nei confronti delle persone.

L’**antiproibizionsimo**, la **regolamentazione** della prostituzione equivale a governare il fenomeno, contro i tabù, le ipocrisie  e l’illegalità che sempre più si affermano nel nostro paese.

La **lotta alla tratta e allo sfruttamento** della prostituzione necessita di maggiori risorse e di un approccio politico nuovo e innovativo.

* * *

Presiede e modera: **Sergio Rovasio**, Segretario dell’Associazione Radicale Certi Diritti

Interventi di:

**Donatella Poretti**,  Senatrice Radicale-PD

**Giuseppe Rossodivita**,  Avvocato, Presidente Gruppo consiliare Lista Bonino Pannella, Federalisti Europei alla Regione Lazio

**Marco Bufo**, Responsabile interventi nazionali e transnazionali sulla tratta e sulla prostituzione - Associazione On the Road **Emanuela Costa**, Associazione Le Graziose, Comitato per i diritti civili delle prostitute,  Genova

**Lucio Malan**, Senatore PDL

**Mario Staderini**, Segretario di Radicali Italiani

**Monica Del Bono**, Presidente ‘La Strega da Bruciare’, Comitato per i diritti civili delle prostitute, Roma

**Jean Baptiste Poquelin**, Studente, Coordinatore  Gruppo di Roma di Certi Diritti

**Leila Deianis**, Presidente Associazione Libellula, Roma

**Alessandro Gerardi**, Avvocato, membro della Direzione di Radicali Italiani, Tesoriere dell'Associazione Radicale Divorzio breve