---
title: '2008-0140-Discrimination-ST14456.EN10'
date: Mon, 04 Oct 2010 12:15:22 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 4 October 2010

Interinstitutional File:

2008/0140 (CNS)

14456/10

LIMITE

SOC 608

JAI 803

MI 352

  

  

  

  

  

NOTE

from :

General Secretariat

to :

The Working Party on Social Questions

on :

19 October 2010

No. prev. doc. :

13883/10 SOC 561 JAI 759 MI 317

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

In preparation of the next meeting of the Working Party, which is provisionally scheduled for 19 October 2010, delegations will find attached a note from the SE delegation.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Presidency questionnaire concerning housing**

**\[Document 13883/10\]**

**SE replies**

**1.         Situation in Member States**

Both the Racial Equality Directive (2000/43/EC) and the Gender Goods and Services Equality Directive (2004/113/EC) cover housing available to the public in their scope. Several MS have gone beyond these two grounds in their protection against discrimination.

a)       To what extent does your national legislation against discrimination apply to housing?

b)      Do you distinguish (for this purpose) between private and public/social housing?

c)       Do you have statistics on the number of cases of discrimination in the area of housing, and if so, what is the most prevalent ground?

a)       According to the Discrimination Act, discrimination that is associated with sex, transgender identity or expression, ethnicity, religion or other belief, disability or sexual orientation is prohibited for a natural or legal person who supplies goods, services or housing to the general public, outside the private and family sphere. The prohibition does not apply to discrimination that is associated with age. As regards denial of reasonable accommodation as a form of discrimination, see answer to question 4 a). A Committee has recently proposed that discrimination associated with age shall be prohibited for a natural or legal person who supplies goods, services or housing to the general public, outside the private and family sphere (“An enlarged protection against age discrimination” (SOU 2010:60). The Committee suggests an exception to the prohibition so that it doesn’t prevent differential treatment on grounds of age, if there is a legitimate purpose and the means that are used are appropriate and necessary to achieve that purpose. The Committee report is referred for consideration until 20th December 2010.

  

Housing includes all forms of housing, be it permanent or temporary. It comprises both immovable and movable property. The manner in which housing is made available is of no consequence. Both commercial/professional housing and transactions made by natural persons are covered by the prohibition. Outside the scope falls situations when someone offers housing to a small circle of people, such as family or other relatives, friends or co-workers. Transactions made by natural persons on a single occasion are not covered by the prohibition. Some examples are letting out or selling of your own home on a single occasion. How often and in which way a natural person rents/lets out property or sells property is of great importance as to whether the transaction is deemed to fall within the private and family sphere or not. Offers of housing on more than a single occasion or on a frequent basis are covered. Anyone offering for example a flat or a room to rent on a frequent or recurrent basis cannot take advantage of the exception to the rule.

b)      No such distinction is made in the Discrimination Act. “Social housing” doesn’t exist in Sweden.

c)       The Equality Ombudsman has on behalf of the Government made a survey concerning discrimination in the housing market. The survey was reported to the Government in July 2010. The survey is based on a study made by the Institute for Housing and Urban Research at Uppsala University. The method of situation testing was used in the study in order to investigate whether discrimination that is associated with sex, ethnicity, religion or other belief, disability or sexual orientation occurred in the housing market. The results of the study shows that discrimination tends to be more common when it comes to renting flats etc. than is the case when it comes to selling and/or buying flats etc. and that discrimination occurs in both large and small cities. Ethnicity was the most prevalent ground of discrimination. Lack of accessibility affected persons with disabilities when it came to looking at the flats or houses in question.

  

In 2009 the Equality Ombudsman received a total of 2 537 complaints regarding discrimination. 79 of those dealt with housing, mainly discrimination on grounds of ethnicity or disability. In 2009 two verdicts regarding housing were passed in Courts of law, both dealt with discrimination on grounds of ethnicity. In 2009 the Equality Ombudsman reached two settlements as regards housing, both dealt with discrimination on grounds of ethnicity.

**2.         Social housing**

a)       Do you consider social housing to be a service (for the purposes of EU law)?

b)      If not, do you consider it to be a part of the social protection system?

“Social housing” doesn’t exist in Sweden. Against that background we don’t have any strong opinion. It seems to us though that “social housing” is rather a form of social protection than a service.

**3.         Private life**

In some cases, the principle of equality might be in conflict with the right to private life, in particular in the area of housing (e.g. renting out a room in your own apartment).

a)       Is this better taken into account by an exclusion of transactions in the area of private life from the scope of the Directive, or by an exclusion of activities which are not commercial or professional?

b)      How is commercial/professional housing defined in your national legislation?

a)       It should be in line with Directives 2004/113/EC and 2000/43/EC. Against that background an exception should preferably be taken into account by an exclusion of transactions in the area of private life from the scope of the Directive.

  

b)      See answer to question 1 a). Discrimination that is associated with sex, transgender identity or expression, ethnicity, religion or other belief, disability or sexual orientation is prohibited for natural and legal persons who supply goods, services or housing to the general public, outside the private and family sphere. Discrimination outside the private and family sphere is thus forbidden. This includes commercial /professional housing. A definition of commercial/professional housing is not necessary as all discrimination outside the private and family sphere is prohibited.

**4.         Disability – reasonable accommodation**

a)       In your view, what should the obligation to provide reasonable accommodation mean for providers of housing?

b)      Is there legislation on the accessibility of buildings (private as well as public/social) in your country?

a)       For employers and some education providers in higher education the Discrimination Act prescribes an obligation to provide reasonable support and adaptation measures to ensure that persons with disabilities are put in a comparable situation to persons without disabilities. Education providers are obliged to take reasonable measures regarding the accessibility and usability of the premises. Examples of measures are removal of high thresholds, installation of electronic door openers, shaping of toilets, ventilation and good acoustics.

There is no prohibition of discrimination in the form of inadequate accessibility for people with disabilities as regards housing. The matter has been looked into on a non-political level (“Beyond fair words – Inadequate accessibility as discrimination”) (Ds 2010:20). In the Memorandum the introduction into the Discrimination Act of a new provision prohibiting discrimination in the form of inadequate accessibility for people with disabilities, is proposed.

  

The Memorandum is referred for consideration until 19th November 2010. Parallel with this work, the Swedish Agency for Public Management has been commissioned to make an impact assessment that among other things will include an assessment of the costs for companies and other private actors and also an analysis of the effects on the public expenditure. The answers from the referral will then be considered together with the impact assessment before the Government decides whether and, if that is the case, in what way the proposals of the Memorandum will be carried out.

b)      The Planning and Building Act contains rules about accessibility to buildings. Buildings containing dwellings, working premises or premises to which the general public has access, must be designed and built in such a way that the dwellings and the premises are accessible to, and can be used by, people with limited mobility or orientation capacity. The rules are applicable when a building is constructed or altered.

In already existing buildings with premises to which the public has access “easily eliminated obstacles” to the accessibility shall be removed. By “easily eliminated obstacles” is meant such obstacles that with regard to the benefit of the measure being taken and the premises as such can be considered reasonable to eliminate. The costs of the measures mustn’t be unreasonably for the real estate owner.*

**5.         Disability – accessibility**

a)       What should the obligation to provide for accessibility measures encompass in the area of housing?

b)      Should there be a difference between old and new buildings?

c)       Do you support immediate applicability with respect to new buildings?

d)      Should there be a difference between the obligations for private and public/social housing respectively?

e)  Are there public funds available in your country for the adaptation of housing (private as well as public/social)?

  

a)      In Sweden there are general rules regarding accessibility to buildings in the Planning and Building Act (see answer on question 4 b). Reasonable accommodation as a form of discrimination is prescribed in the Discrimination Act regarding working life and higher education (see answer on question 4 a).

b)      In Sweden there are different rules on accessibility regarding new buildings and already existing buildings (see answer on question 4 b).

c)       There are rules on accessibility when constructing new buildings (see answer on question 4 b).

d)      “Social housing” doesn’t exist in Sweden. Sweden therefore has no opinion on the matter.

e)      Economic funds (subsidies) can be given to natural persons according to Swedish law. The purpose is to give persons with disabilities a possibility to live an independent life in their own homes. Economic funds can be given for the adaption of permanent functions in the home and in connection with the home.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_