---
title: 'L''AMORE SPIAZZA-IN PIAZZA PER ESSERE E PER AMARE'
date: Fri, 16 Apr 2010 02:34:06 +0000
draft: false
tags: [Senza categoria]
---

BERGAMO - 18 aprile 2010, Piazza Pontida ore 15.00

Dopo la prima tappa a Magenta, un pullman arcobaleno sarà a Bergamo il 18 aprile 2010, seconda tappa itinerante di una maratona contro l’omofobia che toccherà diverse realtà della provincia.

Il coordinamento Arcobaleno riunisce le realtà trans, lesbiche e gay di Milano e provincia e partirà in pullman per sbarcare nelle piazze e incontrare i cittadini e le cittadine della Lombardia.

Nel cuore della Padania celta e longobarda, dove l’omofobia è una piaga radicata e dove fioriscono i gruppi per curare l’omosessualità, vogliamo esserci con dignità e fierezza, per affermare che la differenza è un valore e che la vera malattia è l’omofobia.

In un momento in cui in Italia assistiamo a fenomeni d’odio e di violenza verso coloro che sono portatori di diversità, noi visiteremo le città lombarde raccontando di persona le nostre storie.

Vogliamo parlare delle nostre. vite e dei nostri amori per farci conoscere e per diminuire le distanze che creano un muro di pregiudizi.

In piazza Pontida a Bergamo va in piazza l’amore che non ha diritti e la dignità delle proprie scelte di vita.