---
title: 'LA COMMISSIONE EUROPEA SOSTIENE UFFICIALMENTE PROPOSTA FRANCESE ALL''ONU'
date: Wed, 17 Dec 2008 17:04:09 +0000
draft: false
tags: [Comunicati stampa]
---

Depenalizzazione omosessualità/Cappato:

Da Commissione europea importante sostegno ad iniziativa francese   
   
Su iniziativa del Gruppo dell'Alleanza dei Liberali e Democratici per l'Europa (ALDE), si é svolto oggi al parlamento europeo a Strasburgo un dibattito sull'iniziativa ONU della Presidenza francese sulla depenalizzazione dell'omosessualità in tutto il mondo.    
 

"Ci auguriamo che il fronte ampio e trasversale che la Presidenza francese è giá riuscita a raccogliere su questa proposta possa ottenere lo stesso successo raggiunto dall'iniziativa che ha portato all'approvazione di una moratoria ONU sulla pena di morte", ha dichiarato l'eurodeputato radicale Marco Cappato, esprimendo pieno sostegno alla proposta francese a nome dell'ALDE e del Partito radicale nonviolento.  
   
 "Ci rallegriamo - ha aggiunto Cappato - del sostegno espresso oggi in Aula a Strasburgo dalla Commissaria europea Ferrero Waldner e facciamo appello alle istituzioni europee e a tutti gli Stati membri affinché respingano in maniera ferma le manovre che il Vaticano sta realizzando in sede di Nazioni Unite per sabotare questa iniziativa".  
   
   
Strasburgo, 17 dicembre 2008