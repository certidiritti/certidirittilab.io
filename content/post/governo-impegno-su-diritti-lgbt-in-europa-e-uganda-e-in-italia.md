---
title: 'Governo: impegno su diritti lgbt in Europa e Uganda. E in Italia?'
date: Thu, 23 Jun 2011 11:40:20 +0000
draft: false
tags: [Transnazionale]
---

**Il governo italiano risponde all'interrogazione del deputato radicale-Pd Matteo Mecacci su gravi discriminazioni in Uganda contro le persone gay.**

**La risposta è ineccepibile riguardo l'impegno in ambito Ue e Onu. Ma sulla politica interna?**

  
**Roma, 22 giugno 2011**

![Read More](http://www.certidiritti.org/wp-content/uploads/2011/06/trans.gif "Read More")

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Il Governo italiano ha risposto, tramite il Sottosegretario agli Esteri, On. Enzo Scotti, ad una interrogazione urgente a risposta scritta, presentata dal deputato Radicale – Pd, Matteo Mecacci, che ringraziamo per il suo impegno, sulla grave situazione dei diritti umani in Uganda, in particolare riguardo le persone omosessuali e sulla proposta di legge presentata al Parlamento ugandese, ora ‘congelata’, che prevede la pena di morte per le persone omosessuali.  
Le informazioni date dal Governo sono molto importanti e positive. **Per quale motivo però l’Italia si impegna in ambito Ue e Onu e non sulla sua politica interna?**  
  
**Il Governo, difatti, ha così risposto all’interrogazione:**  
“Lo scorso 17 maggio, in occasione della gioranta internazionale contro l’omofobia, l’Alto Rappresentante Catherine Ashton ha esortato tutti gli Sati a porre fine ad ogni tipo di discriminazione basata sull’orientamento sessuale e sull’identità di genere, riaffermando altresì, a nome dell’Unione Europea, il comune impegno anche attraverso misure specifiche di sostegno ai diritti delle persone lesbiche, gay, bisessuali e transgender.  
Nella stessa occasione è stato poi rivolto un particolare omaggio a David Kato e Jhon Edison Ramirez, attivisti di spicco dei movimenti per i diritti delle persone LGBT, assassinati lo scorso anno, ed ha encomiato la scelta dell’attivista LGBT ugandese Kasha Jaqueline Nabagesera quale candidata per il prestigioso Premio Martin Ennals per i difensori dei Diritti Umani  
(…)  
  
Il Governo italiano ha seguito da vicino, con tutti gli altri partner comunitari, le vicende relative ai fenomeni di intolleranza e di discriminazione fondate sull’orientamento sessuale che purtroppo si sono verificate e ancora si verificano in Uganda, anche al fine di valutare, d’intesa con l’Unione Europea, eventuali azioni da intraprendere. Lo stesso Presidente Museveni, a seguito di tali pressioni, aveva detto al suo partito che occorreva ‘rallentare’ l’iter della legge discriminatoria contro gli omosessuali, in quanto stava diventando “un problema di politica estera”. Nonostante si sia tentato di riportare la bozza all’attenzione del Parlamento, prima del suo scioglimento a seguito delle elezioni generali dello scorso febbraio, il Vice Ministro degli Esteri ugandese aveva rassicurato il Capo Delegazione europea di Kampala che la proposta di legge non sarebbe stata discussa neanche dalla futura Assemblea.

Nell’ambito del suo impegno volto a promuovere e tutelare i diritti umani, l’Unione Europea ha accolto con particolare favore la dichiarazione comune formulata al Consiglio dei Diritti dell’Uomo delle Nazioni Unite dello scorso 22 marzo, a nome di 85 paesi di ogni continente, dal titolo: “Porre fine agli atti di violenza e alle violazioni dei diritti umani basate sull’orientamento sessuale e l’identità di genere”. Quindici Stati membri dell’UE hanno fatto parte del gruppo incaricato dell’elaborazione di tale dichiarazione e tutti gli Stati membri dell’UE hanno espresso pieno appoggio a questa iniziativa. Tale dichiarazione comune dimostra il crescente impegno della comunità internazionale nei confronti della promozione e tutela dei Diritti Umani, a prescindere dall’orientamento sessuale o dall’identità di genere”.