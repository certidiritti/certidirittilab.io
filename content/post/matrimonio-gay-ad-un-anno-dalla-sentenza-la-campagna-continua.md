---
title: 'Matrimonio gay: ad un anno dalla sentenza la campagna continua'
date: Wed, 23 Mar 2011 17:08:48 +0000
draft: false
tags: [Matrimonio egualitario]
---

Il 23 marzo 2011 cade il primo anniversario dell'udienza presso la Corte costituzionale per valutare la costituzionalità del rifiuto al matrimonio tra persone dello stesso sesso. Certi Diritti, che insieme a Rete Lenford ha promosso la campagna di Affermazione Civile, scrive ai sindaci di tutti i capoluoghi di provincia e invita le coppie gay e lesbiche a richiedere le pubblicazioni degli atti di matrimonio presso il proprio comune.

Roma, 23 marzo 2011  
  
Nel 1° anniversario dell’udienza della Corte Costituzionale (sentenza 138/2010) sulla decisione relativa alla costituzionalità del rifiuto al matrimonio per le coppie dello stesso sesso, i Sindaci e i dirigenti degli uffici di Stato Civile di oltre 140 Comuni italiani, riceveranno una lettera dell’Associazione Radicale Certi Diritti.  
  
          Nella lettera viene ricordato, grazie alla sentenza della Consulta, come nella giurisprudenza italiana sono stati sanciti inequivocabilmente i seguenti principi:  
   
•         Le coppie omosessuali hanno rilevanza costituzionale e non sono un mero fatto privato;  
•         Il matrimonio tra persone dello stesso sesso è compatibile con l’attuale Costituzione;  
•         Le coppie non sposate omosessuali sono su un piano giuridico differente, perché più meritevole di riconoscimenti di diritti e doveri rispetto alle coppie eterosessuali non sposate;  
•         Il parlamento ha il dovere di legiferare in materia con una regolamentazione organica e generale, dunque non privatistica.  
   
          **L’udienza del 23 marzo 2010 e la sentenza 138/2010 del 12 Aprile (motivazione rese pubbliche il 15 aprile) non sono avvenute “per caso”, ma sono state il frutto di un movimento di consapevolezza civica chiamato “Affermazione Civile”.** Già dal 2007, infatti, diverse decine di coppie dello stesso sesso si sono recate nel comune di residenza, per chiedere la pubblicazione degli atti del matrimonio e impugnare il diniego presso i tribunali competenti. Questo è avvenuto con il sostegno dell’associazione radicale Certi Diritti e della rete di avvocatura LGBT Rete Lenford.  
   
Questa presenza va incoraggiata e sostenuta e Certi Diritti intende farlo portando avanti la Campagna di Affermazione Civile. L'associazione invita le coppie omosessuali italiane a presentarsi – in particolare tra il 23 Marzo e il 12 Aprile – presso i propri comuni di residenza per richiedere le pubblicazioni degli atti di matrimonio, come gli scorsi anni hanno già fatto quelle coppie coraggiose che hanno permesso di arrivare alla sentenza della Corte costituzionale.  
  
Fino a quando quasi tutta la classe politica rimane paralizzata sui suoi interessi di casta e vive anche nella commistione di privilegi con il potere clericale Vaticano, **non rimane che agire sul piano dell’iniziativa legale in Italia e in Europa**.  
   
E’ evidente che le coppie non si presenteranno per “averla vinta” sui Comuni italiani, bensì per instaurare un rapporto rispettoso e aperto con le istituzioni della città in cui vivono. L’invito che rivolgiamo ai Sindaci, è quello di prepararsi a questo dialogo, e di affrontare senza pregiudizi le richieste di parte della cittadinanza.  
   
Il testo integrale della lettera inviata ai 140 Sindaci e dirigenti dei Comuni capoluogo di Provincia di tutta Italia al seguente link:  
   
[http://www.certidiritti.it/tutte-le-notizie/1073-ad-un-anno-dalla-sentenza-sul-matrimonio-gay-lettera-ai-sindaci.html](tutte-le-notizie/1073-ad-un-anno-dalla-sentenza-sul-matrimonio-gay-lettera-ai-sindaci.html)

LA CAMPAGNA PER IL MATRIMONIO TRA PERSONE DELLO STESSO SESSO CONTINUA.  
**[ECCO COME >](campagne/diritto-al-matrimonio/affermazione-civile-2011.html)**