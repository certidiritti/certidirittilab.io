---
title: 'Rapporto annuale ILGA-Europe sui diritti umani LGBTI in Europa. Italia fanalino di coda'
date: Wed, 16 May 2012 09:12:16 +0000
draft: false
tags: [Europa]
---

ILGA-Europe lancia il suo primo rapporto annuale sulla situazione dei diritti umani LGBTI in Europa e aggiorna la sua mappa arcobaleno. Italia fanalino di coda.

Roma, 15 maggio 2012

comunicato stampa Associazione radicale Certi Diritti

Oggi ILGA-Europe lancia il suo primo rapporto annuale sulla situazione dei diritti umani di Lesbiche, Gay, bisessuali, persone Transessuali e Intersessuali in 49 paesi europei e aggiorna la sua mappa arcobaleno. L’occasione è la celebrazione della Giornata internazionale contro l’omofobia alla presenza del Commissario Europeo agli Interni Cecilia Malmström.

Dal documento emerge tristemente che nessuno Stato europeo da piena uguaglianza giuridica alle persone LGBTI. I paesi che si comportano meglio, in una scala da -12 a 30 sono:

    Gran Bretagna (21 punti)  
    Germania e Spagna (20 punti ciascuno)  
    Svezia (18 punti)  
    Belgio (17 punti)

10 paesi si collocano addirittura nell’area negativa non rispettando nemmeno gli standard base in materia di diritti umani:

    Moldavia e Russia (-4,5 punti ciascuno)  
    Armenia, Azerbaijan, Macedonia e Ucraina (-4 punti ciascuno)  
    Monaco, San Marino e Turchia (-3 punti ciascuno)  
    Bielorussia e Liechtenstein (-1 punto ciascuno).

L’Italia - senza una legge contro l’omofobia e le discriminazioni, senza alcun riconoscimento giuridico delle coppie gay e con l’obbligo di sterilizzazione chirurgica per cambiare legalmente nome e genere - ottiene solo 2,5 punti collocandosi al di sotto di Andorra e Lituania e appena al di sopra di Estonia, Grecia, Kossovo e Polonia.

Elementi positivi che emergono sono i progressi raggiunti nel 2011 a livello internazionale ed Europeo nei campi dell’asilo e della protezione dalla violenza. Vari paesi continuano ad andare nella direzione del riconoscimento del diritto all’eguaglianza per le famiglie arcobaleno e si registra un certo numero di proposte di legge volte a introdurre delle norme più umane riguardanti il cambio del nome e del genere per le persone trans.

Elementi negativi sono, invece, la stasi di alcuni paesi e, peggio, i sempre più diffusi tentativi di adottare leggi che criminalizzino la cosiddetta “propaganda dell’omosessualità”.

**SOTTO IN ALLEGATO IL REPORT**

  
[report\_ILGA\_2012.doc](http://www.certidiritti.org/wp-content/uploads/2012/05/report_ILGA_2012.doc)