---
title: 'Wowereit rieletto sindaco di Berlino. Certi Diritti: accolga il Papa insieme al suo compagno'
date: Mon, 19 Sep 2011 14:03:38 +0000
draft: false
tags: [Europa]
---

Il sindaco di Berlino, gay dichiarato, eletto per la terza volta alla guida della città. Il segretario di Certi Diritti in una lettera aperta dopo gli auguri gli chiede di accogliere il Papa insieme al suo compagno. Anche Certi Diritti sarà presente.

Roma, 19 settembre 2011

L’Associazione Radicale Certi Diritti ha oggi inviato una lettera aperta al Sindaco di Berlino, Klaus Wowereit, gay dichiarato,  appena rieletto alla guida della città, per esprimergli i migliori auguri e per chiedergli di accogliere il papa con il suo compagno al fianco.

Il Sindaco di Berlino dell’Spd, rieletto per la terza volta alla guida della città, accoglierà il papa il prossimo 22 settembre durante la sua visita al Parlamento tedesco. Alcuni gruppi di laici, anticlericali e associazioni lgbt(e) hanno organizzato una manifestazione che vedrà la presenza, secondo gli organizzatori, di 20.000 persone e alla quale parteciperà la delegazione berlinese di Certi Diritti. Oltre 100 parlamentari tedeschi hanno annunciato che diserteranno la seduta parlamentare quando il papa prenderà la parola.  
  
 

L’Associazione Radicale Certi Diritti ha inviato oggi una lettera aperta al Sindaco di Berlino invitandolo ad accogliere il papa con il suo compagno convivente al fianco. Qui di seguito il testo integrale della lettera aperta:  
  

Roma, 19 settembre 2011

Signor Sindaco di Berlino,

nel farle i complimenti per la sua terza elezione alla carica di Sindaco di una delle città più gayfriendly d’Europa, la invitiamo a fare un gesto molto importante per le comunità lgbt(e) europee. Il prossimo 22 settembre, i suoi doveri di rappresentanza le impongono di accogliere il papa che sarà in visita nella sua città; in passato il papa  si era ben guardato dal visitare la città di Berlino ma ora vi è costretto per recarsi nella sede del Parlamento. La invitiamo ad accogliere il papa con il suo compagno di vita al fianco perché, ne siamo certi, questo gesto sarebbe di grande  aiuto  alle comunità Lgbt(e) europee e determinerebbe un effetto mediatico molto importante in tutta Europa. Questo gesto simbolico aiuterebbe centinaia di migliaia di persone lesbiche e gay che, anche a causa della politica omofoba della chiesa cattolica, non hanno il coraggio di uscire allo scoperto e vivere con serenità la loro condizione.

Cordiali saluti,

Sergio Rovasio  
Segretario Associazione Radicale Certi Diritti  
  
**  
lettera in tedesco**

Sehr geehrter Regierender Bürgermeister von Berlin,

wir gratulieren Ihnen ganz herzlich zu Ihrer dritten Wahl in das Amt des Regierenden Bürgermeisters in einer der schwulenfreundlichsten Städte Europas und bitten Sie um eine Geste, die für die LGBT-Gemeinschaft in ganz Europa sehr wichtig wäre.

Am 22. September erfordern Ihre Pflichten, dass Sie den Papst in Berlin willkommen heissen. In der Vergangenheit hat der Papst es bewusst vermieden Berlin zu besuchen, doch dieses Mal kann er keinen Bogen um die Hauptstadt machen, da er eine Rede im Bundestag halten soll.

Wir bitten Sie, den Papst mit Ihrem Lebenspartner an Ihrer Seite zu begrüßen, weil wir uns sicher sind, dass diese Geste eine große Hilfe für die europäische LGBT-Gemeinschaft wäre und eine große mediale Wirkung haben würde.  
  
Es wäre eine Geste für Millionen von Menschen, besonders für Lesben und Schwule, die sich auch aufgrund der Homophobie der katholischen Kirche nicht trauen, sich zu outen und ihre Sexualität frei und öffentlich zu leben.

Mit freundlichen Grüßen,

Sergio Rovasio  
Vorsitzender von  
Associazione Radicale Certi Diritti

(traduzione di Davide Miraglia)

**SOSTIENI CERTI DIRITTI >**  
**www.certidiritti.it/iscriviti**