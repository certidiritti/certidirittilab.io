---
title: 'DOMENICA ANTIPROIBIZIONISTA A ROMA: LE NON DROGHE, USCIRE DAL MEDIOEVO'
date: Sat, 20 Feb 2010 10:33:51 +0000
draft: false
tags: [Comunicati stampa]
---

Domenica 21 febbraio 2010 ore 15.00 / 24.00 Roma, Domus Talenti, Via delle Quattro Fontane 113  
Pomeriggio antiproibizionista e raccolta firme per la presentazione della Lista Bonino Pannella nel Lazio. La sera proiezione in anteprima del film candidato all'Oscar "Il Profeta".   
  
Le non droghe, uscire dal medioevo...

Introduzione e saluti  
Interventi in diretta o in video di candidati delle Liste Bonino Pannella, movimenti e gruppi dell' antiproibizionismo sulla cannabis, esperti  
  
Controllami di meno curami di più  
Il punto sulle iniziative su test antidroga e cannabis terapeutica  
Rovigo - Firenze: un progetto stupefacente  
  
Giù le mani dalla canapa!  
La libera coltivazione domestica come opposizione alla criminalità organizzata  
Coltivazione indoor e outdoor / diritti e facoltà di canapai, coltivatori, consumatori  
  
  
Durante la giornata, 15.00 / 24.00 sarà possibile firmare per la presentazione della Lista Bonino Pannella nel Lazio   
  
Serata dedicata agli interventi dei candidati della lista Bonino Pannella nel Lazio.  
  
Alle 19.30, nella sede del Partito Radicale, Via di Torre Argentina 76, Roma, secondo incontro del cineforum Radicale.  
Anche questa volta un film di sensibilità radicale, “Il profeta” di Jacques Audiard è infatti una denuncia sulle condizioni di vita nelle carceri francesi.  
  
Il film è candidato al Premio Oscar 2010 come Miglior Film Straniero, ha vinto il Gran Premio della Giuria Cannes 2009, il Premio European Film Awards 2009 al protagonista Tahar Rahim ed è candidato in Francia a 13 Premi César 2010.