---
title: 'CONFERENZA SU AMORE CIVILE, ROMA, 10-11-12 MAGGIO - INVITO'
date: Fri, 02 May 2008 21:04:48 +0000
draft: false
tags: [Comunicati stampa]
---

function mxclightup(imageobject, opacity){ if (navigator.appName.indexOf("Netscape")!=-1 &&parseInt(navigator.appVersion)>=5) imageobject.style.MozOpacity=opacity/100 else if (navigator.appName.indexOf("Microsoft")!= -1 &&parseInt(navigator.appVersion)>=4) imageobject.filters.alpha.opacity=opacity }   

**Conferenza su Amore Civile, Sala delle Conferenze; Piazza Montecitorio, 123 A –** [(scarica il programma)](index.php?option=com_docman&task=doc_download&gid=1&Itemid=79 "Programma Amore Civile")  
  
**Introduzione:** Il 12 maggio del 1974 gli italiani approvavano con un referendum la legge sul divorzio, sugellando con il proprio voto il principio per cui la famiglia non rappresentava più l'imposizione di un ordine naturale, figlio della tradizione o del sacramento, ma una libera scelta d'amore. Creazione degli uomini e delle donne, poteva da loro essere conclusa.

Il voto del 12 maggio accellerò anche in Italia quella rivoluzione culturale che attraversava tutta l'Europa e il mondo occidentale. Da allora sono aumentati costantemente i matrimoni civili, le convivenze di fatto eterosessuali e omosessuali, i figli nati fuori dal matrimonio, le famiglie ricostituite a seguito del divorzio, le famiglie con un solo genitore, e insieme le adozioni, gli affidi, i figli nati grazie alla fecondazione assistita. Socialmente e culturalmente la famiglia non è più uno strumento funzionale alla riproduzione della specie, ma un luogo in cui si arricchisce, mettendola in comune con chi si ama, la propria intimità e la propria vita.  
  
La famiglia tradizionale non rappresenta l'ordine naturale della società. L'uomo è per natura un essere di cultura e la famiglia ha accompagnato le sue trasformazioni adattandosi in un'infinità di modi. Il progresso della società è sempre stato guidato dall'affermazione di valori in grado di unire le persone oltre i ristretti legami di sangue. Nella tragedia greca le Erinni, dee della vendetta, si trasformano in benevole Eumenidi quando le nuove leggi della democrazia si sostituiscono all'ordine arcaico legato alle leggi di sangue della famiglia. Anche il cristianesimo ha proposto valori in contrasto con una visione morale confinata all'interno della famiglia. E' stato Cristo a dire «sono venuto a dividere il figlio da suo padre, la figlia da sua madre, la nuora dalla suocera; e i nemici dell'uomo saranno quelli stessi di casa sua. Chi ama padre o madre più di me, non è degno di me; e chi ama figlio o figlia più di me, non è degno di me» (Mt 10, 34-37).  
  
Tra le battaglie che,  nella storia italiana recente, sono state vinte dalle forze del progresso,  vi è quella per la riforma del diritto di famiglia, avvenuta nel 1975. Quella lotta, indissolubilmente  legata all'introduzione del divorzio, consentì di affermare il principio  di parità tra uomo e donna nel matrimonio e di allargare gli spazi  di libertà, e conseguente possibile ricerca della felicità, a disposizione  di ogni individuo.  
Quella riforma è tuttavia rimasta a metà. L'opera del 1975  deve essere completata con completa parificazione dei diritti di uomini  e donne e dei diritti dei figli nati nel matrimonio e dei figli nati  fuori di esso. Il procedimento di  divorzio deve essere modificato, sostituendo la burocraticità e l'etero-direttività,  che ancora lo caratterizzano, con gli strumenti moderni della mediazione  e dell'auto-determinazione. Il procedimento di  separazione deve essere radicalmente trasformato, se non abolito. Gli spazi di libertà  individuale devono essere ampliati, nel rispetto dei diritti del partner  e, in modo particolare, dei figli.  
  
Al di là del completamento  della riforma del 1975, non si deve indugiare ulteriormente nel garantire quelli che appaiono veri e propri diritti negati, riconosciuti nella maggior parte degli altri Paesi del contesto occidentale, come il diritto di ciascuno di avere un compagno e di costituire con lui una famiglia, senza discriminazioni di sesso o di comportamenti sessuali, il diritto delle coppie che desiderano avere un figlio con l'aiuto della procreazione assistita di valersi di essa, senza limitazioni che non abbiano razionale giustificazione, il diritto dei single di adottare un bambino, senza che questa adozione sia svalutata o minimizzata dalla legge, il diritto delle coppie di fatto di vedersi riconosciuta dignità e tutela giuridica, il diritto di ciascun componente di una comunità familiare di non subire violenza e di realizzare pienamente la propria personalità.  
  
Amore civile significa mettere al centro non la forma, ma la qualità delle relazioni affettive. Amore civile è l'antidoto all'amore fatale come passione travolgente assoluta incapace di risconoscere l'autonomia dell'altro. Amore civile è convivenza basata sui criteri della democrazia, del rispetto e del dialogo. Amore civile è anche accoglienza delle diversità, riconoscere che oggi la famiglia è composta da tante diverse normalità e che in questo è rintracciabile la sua vitalità e ricchezza. Significa rivendicare che amore non è sinonimo di riproduzione, e che la civiltà dell'amor richiede sforzo, consapevolezza e anche aiuto, non il baratto di tutele in cambio di imposizioni di una morale di stato.  
  
L'Italia cattolica e del Family day è il paese caratterizzato da record europei per assenza di asili nido, iniqua distribuzione del lavoro domestico tra uomo e donna, bassa partecipazione femminile al mercato del lavoro, numero di figli per donna, tempi e costi per l'adozione. E' anche caratterizzata per un record di proibizioni.  
  
Il 10, 11 e 12 maggio 2008 "Amore civile" si propone di tornare a unire studiosi della famiglia con i diretti interessati a una modifica della legislazione vigente.  
  
  **Programma** **Sabato 10 maggio – Convegno  
**_Sala delle Conferenze di piazza di Monte Citorio 123/A_**ore 9,30-10,30  
  
 PRESENTAZIONE DELL'INIZIATIVA**  
  

1.   _Introduce_Diego Galli, RadioRadicale.it
2.   _L'amore civile_ Enrichetta Buchli, psicanalista, autrice del libro “Il mito dell'amore fatale”
3.   _Il diritto di famiglia tra Europa e Kandahar: quale modello per l'Italia?_ Bruno de Filippis, giurista esperto di diritto di famiglia

  
   **ore 10,30-13,30 RELAZIONI**  

1.   _Titolo_ Piergiorgio Donatelli, docente di bioetica Università La Sapienza
2.   _La tutela giuridica della famiglia omosessuale_ Francesco Bilotta, docente di Diritto privato Università di Udine
3.   _Asimmetrie di genere e bilanci del tempo delle famiglie_ Letizia Mencarini, Professore Associato di Demografia Università di Torino
4.   _Violenze sulle donne, femminicidio, cultura della sopraffazione: il peso delle religioni e la debolezza della laicità_ Monica Lanfranco, giornalista
5.   _Predica evangelica sulla famiglia_ Anna Maffei, presidente dell’Unione cristiana evangelica battista d'Italia
6.   _Unioni di fatto: dati di realtà e orientamenti di opinione_ Anna Laura Zanatta, Docente di Sociologia della famiglia Università La Sapienza
7.   _Conclusioni_Marco Cappato, segretario Associazione Luca Coscioni

  
**ore 15-19 INTERVENTI DELLE ASSOCIAZIONI**  

1.   _Modera_ Enzo Cucco, Certi diritti
2.   _“Il divorzio breve”_Diego Sabatinelli, segretario della Lega Italiana per il Divorzio breve
3.   _“Adozioni senza discriminazioni”_Chiara Lalli, docente di Logica e Filosofia della Scienza Università “La Sapienza” di Roma
4.   _“Nuovi padri. Per nuovi diritti e nuovi doveri”_Maurizio Quilici, Presidente dell'Istituto di studi sulla paternità
5.   Alessandro Capriccioli, Soccorso civile – Associazione Luca Coscioni
6.   Antonella Sapio, presidente Istituto Nazionale per lo Studio e la Promozione del Cohousing
7.   Gian Ettore Gassani, presidente nazionale dell'Associazione matrimonialisti italiani
8.   Monica Soldano, Presidente dell'Associazione Madre Provetta
9.   Marino Maglietta, Presidente della Fondazione Crescere Insieme 
10.   Filomena Gallo, Presidente Associazione Nazionale Amica Cicogna onlus
11.   Rossella Bartolucci, Sos infertilità
12.   Gianni Geraci, presidente Gruppo del Guado
13.   Antonio Bernini, presidente Coordinamento Associazioni e Comunità di Ricerca Etica Interiore e Spirituale
14.   Giuseppe Pio Torcicollo, presidente Associazione Convoglio valori

  
  **ore 21 PROIEZIONE DEL FILM “TI DO I MIEI OCCHI”**  
 Salone dell'Associazione Luca Coscioni  
Via di Torre Argentina 76, 3° piano_  
  
A seguire dibattito condotto da Enrichetta Buchli, psicanalista_Perché una donna resta per dieci anni con un uomo, fisicamente e psicologicamente violento, che la picchia? A Toledo la bella Pilar, spinta dalla paura, fugge da casa e dal marito Antonio col figlio Juan, rifugiandosi dalla sorella, ma qualche tempo dopo, ancora innamorata del marito e fiduciosa nelle sue promesse di ravvedimento, ritorna con lui. Il secondo distacco sarà definitivo. Scritto con Alicia Luna, il 3° lungometraggio dell'attrice madrilena I. Bollaín affronta il tema della violenza domestica sulle donne, riuscendo a subordinare i suoi espliciti intenti didattici alla complessità di un dolorante rapporto umano, a un ammirevole scavo psicologico dei personaggi. 7 premi Goya, gli Oscar spagnoli.  
  
**Domenica 11 maggio – Contenuti e strategia per una riforma globale del diritto di famiglia**Salone dell'Associazione Luca CoscioniVia di Torre Argentina 76, 3° piano  
  
** ORE 9-14 DIBATTITO CON RELATORI E PARTECIPANTI**

1.  Mattinata di confronto aperto con i relatori, i rappresentanti delle associazioni e i partecipanti alla conferenza e istituzione delle commissioni di lavoro permanente.Di seguito i titoli delle tematiche che verranno affrontate:
2.   Matrimonio, separazione e divorzio, mediazione familiare (storia del rapporto tra matrimonio civile e religioso, patti lateranensi, convenzioni matrimoniali, regime patrimoniale del matrimonio, prenuptial agreements, divorzio breve, condizione e affidamento dei minori nella separazione e nel divorzio, potestà e responsabilità genitoriale, diritti e doveri nei rapporti tra genitori e figli, riforma della solidaritetà post coniugale, introduzione della mediazione familiare);
3.   Coppie di fatto e coppie omosessuali (diritti individuali e diritti di coppia, dati statistici, legislazione europea, giurisprudenza italiana, psicologia della coppia, capacità genitoriale, discriminazioni storiche ed attuali, evoluzioni del costume e della mentalità sociale);
4.   Procreazione assistita, aborto e clonazione terapeutica (superamento della legge 40, ricerca scientifica, la condizione del padre del concepito, aggiornamento della legge 194 alla luce delle evoluzioni scientifiche, diritto costituzionale alla salute e diritti del nascituro, definizione di persona secondo la legge vigente, accanimento terapeutico ed eutanasia);
5.   Parità tra uomo e donna e tra figli nati fuori e dentro il matrimonio (cognome della moglie, cognome dei figli, differenze esistenti tra figli legittimi, naturali, incestuosi, la condizione della donna nel medioevo e nell'Italia post unitaria, la condizione del "bastardo", profili psicologici e sociologici, modelli tradizionali di divisione del lavoro domestico e dei compiti di cura dei figli, servizi ed aiuti per le coppie);
6.   Riforma successioni ed adozioni (adozione da parte del single, abrogazione adozioni "di serie B", riforma dell'istituto della legittima, riforma del testamento, testamento biologico o testamento in vita);
7.   Violenza in famiglia, violenza contro le donne (indagini statistiche, storiche e psicologiche, modifica della legge 154/2001, tipizzazione dei reati familiari e contro la persona, mobbing familiare).

  
**Lunedì 12maggio – Conferenza stampa**** ORE 12 PRESENTAZIONE ALLA STAMPA DEI RISULTATI DELLA CONFERENZA**  
Parlamentari e partecipanti alla conferenza presenteranno gli obiettivi della conferenza e alcune proposte di legge campione come esempio della riforma complessiva del diritto di famiglia che emergerà alla conclusione dei lavori delle commissioni.