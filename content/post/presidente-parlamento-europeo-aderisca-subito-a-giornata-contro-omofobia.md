---
title: 'PRESIDENTE PARLAMENTO EUROPEO ADERISCA SUBITO A GIORNATA CONTRO OMOFOBIA'
date: Sat, 20 Mar 2010 09:01:43 +0000
draft: false
tags: [Comunicati stampa]
---

**CERTI DIRITTI CHIEDE AL PRESIDENTE DEL PARLAMENTO EUROPEO DI ASSICURARE LA PARTECIPAZIONE DEL PE ALLE CELEBRAZIONI INTERISTITUZIONALI DELLA GIORNATA MONDIALE CONTRO L'OMOFOBIA**

**Bruxelles, 20 marzo 2010**

Dichiarazione di Ottavio Marzocchi, responsabile delle questioni europee dell'Associazione Radicale Certi diritti:

"Secondo alcune indiscrezioni, il Presidente del Parlamento europeo - il polacco conservatore Jerzy Buzek - sarebbe intenzionato a rispondere negativamente all'invito della Commissaria europea per la giustizia, l'eguaglianza ed i diritti umani - la conservatrice lussemburghese Viviane Reding - a celebrare congiuntamente la giornata internazionale contro l'omofobia il 17 maggio prossimo (IDAHO - International Day Against Homophobia).  
  
Se questa indiscrezione si avverasse, il Presidente non solamente si rivelerebbe omofobo e non più capace di rappresentare l'amplissima maggioranza parlamentare che vota per lottare contro l'omofobia, ma smentirebbe anche una precedente decisione del PE che prevedeva di celebrare tale giornata ogni anno.  
  
Ci auguriamo che tale indiscrezione sia scorretta; se fosse corretta, invitiamo il Presidente a cambiare idea. L'alternativa di dovere condurre una campagna per chiedere le sue dimissioni non ci entusiasmerebbe in alcun modo".