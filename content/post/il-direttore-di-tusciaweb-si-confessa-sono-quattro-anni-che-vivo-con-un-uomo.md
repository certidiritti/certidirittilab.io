---
title: 'Il direttore di Tusciaweb si confessa: sono quattro anni che vivo con un uomo…'
date: Mon, 18 Jun 2012 22:34:13 +0000
draft: false
tags: [Politica]
---

Un contributo di Carlo Galeotti al dibattito sul matrimonio tra persone dello stesso sesso.

\- Ebbene sì. Ho deciso di fare coming out: da anni vivo con un uomo. Lo confesso!

Un uomo a cui voglio molto bene. Anzi è l’uomo a cui voglio più bene al mondo.

Non basta. Si tratta di un uomo molto giovane. Non so se la cosa è ancora più grave. E poi è un uomo molto intelligente si occupa di logica e matematica. Forse anche per questo gli voglio bene.

Certo la mia è una confessione pesante. Ma mi rincuora che anche Alan Turing voleva molto bene a un matematico. Spero solo di non fare la stessa fine di Alan Turing. Spero che la mia confessione non porti alle stesse conseguenze subite da un genio assoluto come Turing.

Ormai è fatta

. Ho confessato e basta. Vada come vada.

Le polemiche di questi giorni sul concetto di famiglia e di matrimonio mi hanno fatto riflettere e ho deciso: faccio anche io coming out. Vivo da qualche anno con un uomo… Ed è bene che tutti lo sappiano.

Molti si domanderanno chi questo uomo. Ovviamente prima di rivelare chi è ho chiesto al mio convivente se potevo rendere pubblica la cosa. Mi ha dato l’ok.

Vi svelo tutto. L’uomo con cui vivo da oltre quattro anni è… mio figlio.

E allora direte: che c’è di strano? Beh, se certi ragionamenti dei giorni scorsi sono validi molti sono i dubbi che mi assalgono.

La nostra sarà una famiglia? Mi domando angosciato, ormai da tempo. E poi non abbiamo né l’intenzione né la potenzialità di avere figli. E, come ci spiegano certi sedicenti cattolici e cristiani, non dovremmo essere classificati come famiglia.

E poi ciò che mi angoscia di più è che l’uomo con cui convivo è un matematico e un logico. I logici potranno far parte di una famiglia?

Magari in questo stato bizantino ci sarà una legge che proibisce di far parte di una famiglia agli ostrogoti e ai matematici, come avrebbe detto Benigni. Chissà se la chiesa cattolica ce l’ha con gli ostrogoti o con i matematici.

Caro Fioroni, non si sa mai, si informi, prima di prendere posizione politica, la prossima volta. Sì perché con qualche matematico la sua chiesa se l’è presa veramente in passato. Un filosofo visionario l’ha pure bruciato sul rogo.

Come dire i dubbi mi assalgono, e non mi fanno dormire. Capite bene che se mi tolgono l’etichetta di famiglia non saprei dove sbattere la testa. E proprio per questo ho deciso: vado all’anagrafe e chiedo lo stato di famiglia. Sì. Lo stato di famiglia. Avete capito bene.

E, meraviglia delle meraviglie, mi danno lo “stato di famiglia”. Siamo una famiglia quindi. E allora sto più tranquillo. Sì, sul certificato ci sono i nomi di due uomini eppure lo stato italiano ci considera una famiglia. Non ci hanno chiesto neppure se vogliamo procreare o se siamo potenzialmente in grado di farlo. Insomma qui la minchiata che una famiglia significhi un uomo e una donna che debbano, possano o vogliano fare figli non incide.

Per un momento penso di stare in un paese del Nord Europa. Ma poi leggo le dichiarazioni di Fioroni e capisco… sono ancora nello stato della chiesa.

E allora al di là delle battute e dell’ironia, capisco che qui bisogna ancora battersi per l’avvento di quella che Gobetti chiamava rivoluzione liberale. Dello stato di diritto, che, anche grazie alla cultura cattolica, non sembra voler nascere. Come dire che uno stato figlio di culture religiose e a volte totalitarie (fascista e comunista) non può che produrre legislazioni che non hanno al centro i diritti dell’individuo. Ma enti intermedi di difficile individuazione e definizione. E di dubbia esistenza.

Per quanto riguarda il matrimonio, poi, mi sono fatto una opinione per così dire originale. Non c’è nulla di più innaturale. E’ solo, come dimostra l’antropologia, una costruzione culturale e non naturale per l’animale uomo.

Qualunque cosa significhi “naturale”. Per la destrutturazione del concetto di “natura”, che così tanto piace oggi a certi cattolici, rimando al primo dei saggi religiosi di John Stuart Mill, quello sulla natura, appunto.

Se insomma qualcuno ci voleva fedeli per la vita e finalizzati alla convivenza e la procreazione in spazi più o meno ristretti, ci avrebbe creati piccioni o pinguini. E invece siamo uomini e la nostra vista è un sistema aperto, le nostre società devono essere aperte, la nostra storia si apre a ventaglio.

E allora tutte le misere e scolastiche definizioni di “matrimonio” e “famiglia”, su cui si basa la retorica di certo cattolicesimo che “tiene la propria fede per i denti”, appaiono ridicole. Vuoto nominalismo più degno della scolastica medioevale che di una società in movimento che vuole sopravvivere. E una società che vuole sopravvivere non può, senza nessun relativismo, che basarsi sullo stato di diritto. E di fronte al diritto tutti devono essere assolutamente uguali.

Anzi proprio i diritti di lesbiche, gay, bisessuali e transgender (lgbt) sono la cartina al tornasole che ci dice se si vive in una democrazia. Per essere precisi in uno stato di diritto. Per questo Fioroni dice una corbelleria quando straparla di democrazia e demagogia e afferma: “Sì a diritti civili, no a matrimoni gay. La differenza tra democrazia e demagogia”. Io mi sono sempre occupato dei problemi dello stato di diritto. E sinceramente mi sembra che l’ex ministro abbia un po’ le idee confuse.

Peccato! Perché un uomo politico del suo livello non se lo può permettere. E dovrebbe ricordare che è proprio degli stati totalitari non rispettare i diritti degli omosessuali e non solo degli omosessuali ovviamente, ma di chiunque non rispetti i canoni stabiliti arbitrariamente.

Per questo, al contrario di quanto si possa pensare, il diritti lgbt sono fondamentali. Perché ci dicono se siamo in una democrazia liberale o in qualche altro sistema che vorremmo evitare di sperimentare. Come dire… nel Novecento abbiamo già dato.

Ogni persona è una persona e non può che avere che gli stessi diritti e doveri degli altri. E il termine persona dovrebbe dire qualcosa al buon Fioroni che in gioventù amava leggere Mounier e Maritain.

Ultimo argomento per molti versi fondamentale. Il confronto sul matrimonio gay è un confronto asimmetrico.

Chi è per il matrimonio gay non vuole imporre nulla a nessuno. E un matrimonio gay non lede le famiglie e le coppie canoniche. Chi invece si oppone impone le sue scelte a una bella fetta di popolazione.

Insomma. Nessuno impone a Fioroni di sposare un uomo. Lui invece vuole impedire a due uomini, o due donne, di unirsi in matrimonio. Perché? Boh.

Insomma basta con le mimchiate sugli omosessuali, basta con la demagogia mirata a quello che si presume sia il popolo cattolico. Il rischio è che il popolo cattolico non vi segua più come accade col divorzio. Ricordate Fanfani…

Carlo Galeotti