---
title: 'MATRIMONIO GAY: LETTERA DI UN 16ENNE A SENATORI ARGENTINI'
date: Tue, 29 Jun 2010 13:23:31 +0000
draft: false
tags: [Comunicati stampa]
---

In Argentina la legge per il matrimonio tra persone dello stesso sesso – approvata lo scorso 5 maggio dalla Camera dei Deputati – [è attualmente in discussione da parte della Commissione Legislativa del Senato](http://www.dosmanzanas.com/2010/06/emotivo-testimonio-de-un-adolescente-argentino-adoptado-por-un-padre-gay.html). Un ragazzo adottato da un omosessuale scrive ai Senatori: “Voi capite con il cuore che significa essere gay?”

dal sito [QueerBlog.it](http://www.queerblog.it/post/8356/argentina-ragazzo-adottato-da-un-gay-scrive-ai-senatori-voi-capite-con-il-cuore-che-significa-essere-gay)

C’è molta tensione e gli omofobi stanno facendo di tutto per portare la maggior parte dei senatori dalla loro parte. Uno degli argomenti preferiti dagli omofobi è che l’approvazione del matrimonio aprirà le porte all’adozione da parte di coppie gay.

In tale contesto ha rivestito molta importanza una lettera scritta ai senatori argentini da Daniel Lezana, ragazzo sedicenne adottato da un padre gay.

Dopo il salto trovate la (bellissima, secondo me) [lettera di Daniel](http://www.lanacion.com.ar/nota.asp?nota_id=1278183), in una nostra traduzione.

> Signori Senatori,
> 
> mi chiamo Daniel Lezana, sono figlio di Luis Lezana, ho sedici anni e sono stato adottato sei anni fa, motivo per cui posso assumere il cognome di mio padre.
> 
> Martedì 8 giugno sono stato con mio padre in Senato, ho ascoltato le diverse opinioni e ora anche io voglio dire la mia.
> 
> Non divido le persone in base alla loro sessualità: etero, omo, trans e via dicendo.
> 
> I miei genitori biologici erano eterosessuali e, per le circostanze della vita, il mio fratellino e io siamo andati a finire in un orfanotrofio (non mi va di spiegarne le ragioni).
> 
> Per quattro volte, famiglie eterosessuali hanno provato ad adottarmi per poi rispedirmi all’orfanotrofio, perché dicevano che ero irrequieto; una volta mi hanno portato indietro perché avevo dato troppo cibo ai pesciolini fino a farli morire. Delle altre volte ricordo poco, dal momento che avevo più o meno otto anni.
> 
> Non voglio dire che tutti gli eterosessuali siano cattivi: io sono eterosessuale, mi piacciono le ragazze e sono una buona persona.
> 
> Quando avevo dieci anni si è presentato all’orfanotrofio Luigi, padre della mia anima, come diciamo noi. Il giudice mi disse: “Dani, c’è un single che ha un grande cane che si chiama Carolo e vuole adottarti”. Io non riuscivo a crederci: c’era una nuova speranza per me che pensavo di rimanere nell’orfanotrofio al pari di molti altri ragazzi grandi. Il mio fratellino era stato adottato perché piccolino: lui sì che aveva avuto fortuna… Io ero grande, perché nessuno mi voleva? Tutte le notti me lo chiedevo, fino ad addormentarmi senza trovare risposta…
> 
> Fu così che venni a Buenos Aires. Gli inizi non furono facili. Luis è un architetto e la casa è quasi sempre un caos: sempre modifica qualcosa, non si riposa mai… ah, ah, ah. Luis è un bel rompino: tutto il giorno mi chiede: “Hai studiato? Ti sei lavato? Hai lavato i denti?” “Uffa – rispondo – mi sono stufato…!” Però, quando la sera vado a dormire, lui sempre viene a rimboccarmi le coperte e a darmi un bacio sulla fronte… proprio cattivo, vero? Ah, ah, ah
> 
> Passato un po’ di tempo mi feci coraggio e parlai con il mio vecchio dell’omosessualità: all’inizio non mi piaceva, perché non lo capivo. Anche a voi deve succedere lo stesso, no? Voi capite con il cuore che significa essere gay?
> 
> ![Argentina. Eagazzo adottato da un gay scrive ai Senatori: â��Voi capite con il cuore che significa essere gay?â��](http://static.blogo.it/queerblog/torta_nuziale_gay.jpg)Con il tempo ho iniziato a vedere con gli occhi del cuore sia Luis che Gustavo (il suo ex, ora si sono lasciati)… sono anche figlio di genitori separati… occhio con i miei traumi… ah, ah, ah. Mi sarebbe piaciuto se Luis e Gustavo si fossero sposati, così avrei avuto due papà.
> 
> Quando vivevamo in cinque (c’erano anche due cani), eravamo contenti: Luis (il mio vecchio) era il cattivo e noi eravamo le sue vittime… ah, ah, ah… era bello: tutti eravamo contro di lui che doveva sempre difendersi.
> 
> Secondo Luis (e io rido molto di questo) lui deve essere madre e padre contemporaneamente… è proprio un personaggio! Succede lo stesso a tutti i figli che vengono cresciuti solo da un padre o da una madre, no? I loro genitori svolgono due ruoli e anche il mio lo fa.
> 
> Noi siamo una famiglia, piaccia o meno a molti, questa è la mia famiglia.
> 
> Per quanti pensano o credono che il mio vecchio mi inculchi l’essere gay o che mi possa contagiare, si sbagliano! A me piacciono le ragazze e molto! E se anche fossi gay? Voi credete che siccome mi ha cresciuto un gay… mmm… non ci credo. Ora che vi sto scrivendo sia per i diritti del mio vecchio che miei, desidererei che lui si sposasse. Come io mi sposerò un domani.
> 
> Quando lui si sposerà lo farà con un altro gay, una persona come lui. Non si sposerà con un etero: cosa temete? Che i gay sono una piaga che ci colpirà? Se si sposerà mio padre, le pagelle scolastiche le potranno firmare in due e alle riunioni della scuola potrò venire l’uno o l’altro. Voglio avere gli stessi diritti che hanno i miei compagni di classe e se loro (i miei padri) si separano, avere gli stessi diritti che hanno i figli di genitori separati… i loro figli li hanno e io no, perché?
> 
> Infine, vi dico che sono orgoglioso del padre che ho e da lui imparo che nella vita bisogna lottare per le cose alle quali teniamo e io, amato vecchio mio, starò sempre al tuo fianco.
> 
> Per favore signori senatori: i gay si sposeranno tra di loro, non temete che non si sposeranno con voi.
> 
> Grazie mille.
> 
> Daniel Lezana