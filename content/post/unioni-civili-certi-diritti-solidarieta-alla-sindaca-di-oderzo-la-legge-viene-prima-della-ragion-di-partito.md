---
title: 'UNIONI CIVILI. CERTI DIRITTI: SOLIDARIETÀ ALLA SINDACA DI ODERZO. LA LEGGE VIENE PRIMA DELLA RAGION DI PARTITO.'
date: Tue, 11 Oct 2016 17:00:18 +0000
draft: false
tags: [Diritto di Famiglia]
---

[![mani-1024x683](http://www.certidiritti.org/wp-content/uploads/2016/07/mani-1024x683-300x200.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/07/mani-1024x683.jpg)Abbiamo letto che la Sindaca di Oderzo, Maria Scardellato, è stata minacciata di espulsione dal Segretario provinciale della Lega, Dimitri Coin. La sua colpa sarebbe quella di aver rispettato la legge e aver unito civilmente due uomini.

“Si tratta di una minaccia bella e buona, perfino passibile di querela in quanto finalizzata a impedire a un Sindaco di svolgere il suo dovere di ufficiale di stato civile. Se si continuasse nelle dichiarazioni contro la Sindaca provvederemo noi ad esporre alla magistratura e al Prefetto questo caso”, dichiara Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti.

“Qui è in gioco il ruolo stesso delle istituzioni e il loro rapporto con la legalità. La legge viene prima della ragion di partito, che deve stare alla larga dagli amministratori e dalle vite di noi cittadini”, ha aggiunto.

“Tutta la nostra solidarietà alla Sindaca di Oderzo che ha fatto il suo dovere. E solo quello”, conclude Guaiana.

_Roma, 11 ottobre 2016_