---
title: 'Shooting pubblico per la campagna ''Dai corpo ai tuoi diritti'' a margine del direttivo dell''associazione radicale Certi Diritti che si apre sabato 30 giugno a Milano'
date: Fri, 29 Jun 2012 13:51:10 +0000
draft: false
tags: [Politica]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti

Milano, 29 giugno 2012

Il 30 giugno e il 1 luglio si terranno a Milano, in via Borsieri 12 (MM2 Garibaldi), i lavori del direttivo dell'Associazione Radicale Certi Diritti. Tra i partecipanti, il senatore radicale Marco Perduca, il segretario, Yuri Guaiana; il presidente, Enzo Cucco; il tesoriere, Giacomo Cellottini; Gigliola Toniollo, segretaria nazionale dell'ufficio Nuovi Diritti - CGIL e l'avvocato Massimo Clara. Sono stati invitati tutti i dirigenti e gli eletti radicali tra cui il consigliere comunale di Milano Marco Cappato. Tra l'altro si discuterà delle campagne per l'accesso delle coppie omosessuali all'istituto del matrimonio civile e per la legalizzazione della prostituzione, nonché delle prossime iniziative transnazionali e sulla transessualità.

**Il 1 luglio, a conclusione dei lavori del Direttivo, si terrà uno shooting pubblico per la campagna iscrizioni dell'Associazione Radicale Certi Diritti a sostegno dei diritti civili e della libertà sessuale: "Dai corpo ai tuoi diritti". Tutti coloro che vorranno farsi fotografare potranno venire in via Borsieri 12 (MM2 Garibaldi) a partire dalle ore 16:00.  
  
[TUTTO SULLA CAMPAGNA DAI CORPO AI TUOI DIRITTI >>>](http://www.certidiritti.org/2012/06/26/dai-corpo-ai-tuoi-diritti-parlamentari-artisti-ex-porno-divi-disabili-studenti-rifugiati-e-attivisti-politici-nudi-contro-la-sessuofobia/)  
**