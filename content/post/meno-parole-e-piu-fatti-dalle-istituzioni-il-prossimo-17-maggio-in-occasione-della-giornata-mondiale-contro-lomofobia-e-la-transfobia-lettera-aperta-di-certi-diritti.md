---
title: 'Meno parole e più fatti dalle istituzioni il prossimo 17 maggio in occasione della giornata mondiale contro l''omofobia e la transfobia. Lettera aperta di Certi Diritti.'
date: Tue, 07 May 2013 07:39:29 +0000
draft: false
tags: [Politica]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 7 maggio 2013

Il prossimo 17 maggio si celebra la Giornata Internazionale contro l'Omofobia e la Transfobia. Nel passato alcuni rappresentanti delle Istituzioni italiane hanno espresso parole importanti, che ancora sono attualissime su questa materia. In particolare tutti e tutte noi siamo ancora profondamente grati per le parole che il Presidente Napolitano ha speso durante gli incontri con i rappresentanti delle associazioni ed in tutte le occasioni in cui si è espresso pubblicamente su questa materia. Con chiarezza e senza alcuna ambiguità ha rilanciato quella che la nostra costituzione e le leggi europee dicono da tempo: i diritti delle persone lgbt sono diritti umani, quindi riguardano tutti. E bisogna procedere con al rimozione delle cause che provocano pregiudizio, discriminazione e violenza, passando dalle parole ai fatti.

Gli anni, però, son passati, ed alle forti parole del Presidente, e di molti altri rappresentanti istituzionali, non sono seguiti i fatti. Ecco perchè l'Associazione radicale certi diritti, facendo proprio il principio che è meglio prevenire che curare, ha scritto a tutti i rappresentanti istituzionali del nostro Paese ed ai ministri interessati per materia, chiedendo che quest'anno in occasioen del 17 maggio si limitino, se possibile, le parole che verranno spese solo per annunciare impegni concreti, fattivi, su questa materia.

Magari guardando davvero all'Europa, così come il Presidente Letta ha sottolineato più volte in questi giorni, e procedendo speditamente per definire per il nostro paese i crimini d'odio, compresi i discorsi d'odio, e non limitarsi unicamente alla repressione penale dei reati stessi. Ma puntando prioritariamente alla prevenzione ed alla educazione su questa materia, soprattutto nelle scuole e tra l'associazionismo giovanile.

**[SCARICA IL TESTO DELLA LETTERA>>>](documenti/download/doc_download/64-lettera-aperta-giornata-mondiale-omofobia-2013)**