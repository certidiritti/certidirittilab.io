---
title: 'Mozione Particolare sulla gestazione per altri approvata dal VIII Congresso'
date: Wed, 23 Nov 2011 22:27:07 +0000
draft: false
tags: [Gestazione per altri]
---

**POSIZIONE SULLA GESTAZIONE PER ALTRI DELL’ASSOCIAZIONE RADICALE CERTI DIRITTI E DELL’ASSOCIAZIONE FAMIGLIE ARCOBALENO.**

1 PREMESSA GENERALE

Le associazioni qui elencate si oppongono a ogni tipo di discriminazione relativa all’accesso alle tecniche di PMA sulla base dell’orientamento sessuale o dell’identità di genere di singoli o di coppie sposate o non sposate.

1 GESTAZIONE PER ALTRI

a)       Sui contratti

Alcuni pensano che, per tutelare le gestanti (o “portatrici”), i contratti tra genitori intenzionali e gestante debbano prevedere per quest’ultima la possibilità di un ripensamento, che potrebbe verificarsi dopo l’esperienza della gravidanza.

Noi rifiutiamo di accogliere l’idea di donne incapaci di fare delle scelte consapevoli e di vivere nel proprio corpo esperienze coerenti con il proprio pensiero.

Troviamo pericoloso il recupero di concetti che abbiamo respinto da decenni. Noi donne lesbiche e madri, che abbiamo vissuto sulla nostra pelle gravidanze ardentemente desiderate, affermiamo che lo stato di gravidanza non trasforma una donna in un essere debole e vittima dei suoi ormoni al punto di cambiare idea su un atto voluto, capito, introiettato. E’ ovvio che, come in ogni contratto, c’è la possibilità (anche se rarissima nelle storie di GPA) di cambiare idea, di voler rompere i vincoli contrattuali. Crediamo sia giusto in questi casi l’intervento di un tribunale che decida del futuro del bambino nel suo migliore interesse. Non crediamo che la volontà della portatrice debba prevalere a priori. Se così fosse, anche in casi privi di controversie alla fine del percorso le portatrici avrebbero per sempre la sensazione di avere avuto la possibilità di tenere un bambino come proprio e aver invece deciso di “abbandonarlo” ad altri, come se non fosse ben chiaro al legislatore e a tutte le parti coinvolte chi fossero i genitori del bambino che sta per nascere.

La decisione, presa consapevolmente accettando l’accordo, deve essere vincolante. Ciò è rassicurante per tutte le parti in causa, ma soprattutto per le gestanti stesse che sanno senza ombra di dubbio prima, durante e dopo, che il bambino che portano in grembo non è e non sarà mai loro. La responsabilità di aver “abbandonato” il proprio “figlio” sarebbe schiacciante e insopportabile.

Sui limiti da imporre alla pratica della GPA i legislatori possono solo dare regole generali. Devono assicurarsi che le donne non siano sottoposte a costrizioni economiche o famigliari, che conoscano i diversi aspetti della GPA e che si siano sottoposte a un controllo psicologico. Gli aspiranti genitori devono assicurarsi che le donne diano prova di consapevolezza, lucidità, autodeterminazione. Che siano in grado di raccontare e valorizzare la loro storia e soprattutto che la loro sia una scelta consapevole.

Quello che noi auspichiamo è che (se e solo se lo desiderano tutte le parti coinvolte) le donne gestanti possano avere relazioni coi nascituri e che si crei un legame basato sull’affetto e l’amicizia tra le due famiglie. Questo avviene quasi sempre nelle famiglie omogenitoriali, quando, grazie alla GPA, nasce una rete di relazioni forti che coinvolge due famiglie e due culture e che al tempo stesso ridefinisce i legami familiari, proponendo scenari bellissimi di famiglie allargate oltre gli oceani.

La maternità si basa su tre pilastri: i geni, la gestazione, e l’adozione. Ma tutti i figli del mondo per essere figli davvero devono essere adottati psichicamente dai loro genitori: nessuna genetica e nessuna gravidanza farà di te una madre se non hai adottato il bimbo che porti in grembo e che nasce da te. Le madri che desiderano avere un figlio lo adottano in testa ben prima che sia concepito, ben prima che si muova in pancia, ben prima che nasca. Una portatrice non ha mai adottato il piccolo che altri hanno adottato anni prima. E tante madri lesbiche sono madri incontestabili dei piccoli che non hanno partorito e ai quali non hanno trasmesso nemmeno un gene.

Perché la genitorialità è prima di tutto, e sempre, un atto d’amore e di riconoscimento.

b)       Sullo scambio di denaro

Noi pensiamo che la compensazione in denaro sia necessaria e auspicabile e che debba essere fatta all’interno di un sistema trasparente e legale, cioè tutelato dalle leggi dello Stato nell’interesse della donna stessa. Pensiamo che questo compenso debba essere equilibrato e proporzionato all’investimento psico-fisico della gestante.

Portare in grembo un bambino vuole dire non solo nove mesi di gravidanza, ma almeno altri tre mesi di recupero fisico. Cioè un anno intero dedicato a questo progetto: vuole dire visite mediche, attenzione continua all’alimentazione e all’attività fisica, spese per i vestiti adatti, spostamenti presso cliniche per i controlli, rallentamento o sospensione alla vita lavorativa abituale, rischi per la salute, ecc… Merita dunque secondo noi un compenso adeguato, che comunque non sarà mai un saldo per la generosità e l’importanza dell’atto compiuto. Le portatrici in America e in Canada ricevono un compenso di circa 19.000 euro, che ci sembra una somma adeguata e non tale da indurre donne in buone condizioni economiche a scegliere la GPA come un mestiere e basta.

Le portatrici sono persone consapevoli che fanno questa scelta per i più svariati motivi, molto spesso altruistici, ma che molto probabilmente non la farebbero senza un compenso adeguato. Questo vuol dire che queste donne sono altamente consapevoli della loro potenza e che sono capaci di capire che un tale investimento merita una compensazione monetaria.

Riteniamo che per troppo tempo le donne abbiano fornito gratuitamente aiuti e sostegno a figli, mariti, genitori e altre persone senza ricevere nessun compenso: ancora oggi, anche se si parla di “lavori domestici”, nessun parlamentare si sogna davvero di stipendiare le donne che si occupano della casa e della famiglia. Che alcune lesbiche e femministe, pur riconoscendo l’onnipotenza procreativa delle donne, pretendano che questo potere non vada risarcito, ci appare difficilmente comprensibile.

Certo sappiamo che anche nella GPA esistono (soprattutto in alcuni paesi) situazioni di sfruttamento. Le condanniamo senza mezzi termini. Ma rifiutiamo anche l’equazione: scambio di denaro = sfruttamento. Il risarcimento finanziario è il giusto compenso e un riconoscimento legittimo per le responsabilità assunte, il tempo, il rischio e gli inconvenienti legati al progetto.

E’ vero che la necessità di andare all’estero e pagare anche somme ingenti ad agenzie private per i servizi e il sostegno legale e sanitario può far dubitare della bontà dell’operazione in sé. Ma quello che noi auspichiamo è l’inquadramento della GPA nelle leggi dello Stato, per impedire appunto che qualcuno realizzi affari sostanziosi sulla pelle delle persone che desiderano diventare genitori o aiutare altri a diventarlo.

L’anonimato, la vergogna, la mancata condivisione dell’esperienza sono spesso indice di un contesto che non rispetta e non tutela la dignità delle donne. Quando una donna gestante per altri racconta con fierezza che porta in grembo il figlio di un altro e lo fa anche perché ne ha un compenso, non può essere giudicata da altre donne né come una persona sfruttata e incapace di avere un giudizio personale, né come una persona senza valori etici che vende il proprio utero per denaro.

Infine ricordiamo che, in tutti i casi di GPA consapevoli e correttamente impostate, i genitori intenzionali e la gestante si scelgono a vicenda. Nessuna gestante è costretta ad accettare di portare un bambino in grembo per una famiglia che non “accoglie”.

Sono ormai numerosi gli studi che hanno descritto le donne americane o canadesi o britanniche che hanno portato in grembo figli di altri, ricevendo un compenso. Sono in maggioranza bianche, sostenute nella loro scelta dai mariti o compagni, hanno già avuto almeno un figlio in gravidanze senza complicazioni, amano essere in stato di gravidanza, hanno un livello di studi medio.

Promuoviamo con forza la libertà delle donne di scegliere cosa fare del proprio corpo e del proprio utero senza controlli né paternalistici, né tantomeno maternalistici. Lo promuoviamo con determinazione, perché abbiamo sentito le parole di queste donne che si organizzano sempre di più, che raccontano in blog e siti la loro esperienza, e che lo fanno da donne immensamente libere da schemi e da miti ancora profondamente radicati nelle donne europee (specie quelle mediterranee, che hanno a volte una visione idilliaca della maternità). Troppe madonne nell’immaginario collettivo, e ancora troppo poche donne che osano davvero liberare il loro corpo e la loro mente dalla maternità-mito idealizzata, da immagini ideologiche che cambiano a secondo delle necessità storiche, del luogo, delle necessità economiche. Ancora oggi, per esempio, troppe donne subiscono pressioni psicologiche e sociali forti su questioni che devono riguardare soltanto loro, relative alla IVG e alla contraccezione ma anche al tipo di allattamento, al modo di partorire, al ricorso all’epidurale, ecc. Queste pressioni pesano spesso su delle scelte che non solo devono rimanere appannaggio della donna, ma devono venire sempre rispettate e mai forzate o derise.

Gli studi mostrano come la GPA rimanga la più antica delle pratiche per permettere a coppie infertili di avere dei figli. Sicuramente la GPA di oggi, legalizzata e visibile, separa in modo indiscutibile ed evidente la funzione riproduttiva da quella della madre sociale, portando scompiglio ancora una volta nel paradigma che alcun* vorrebbero imporre: l’immutabilità del concetto di famiglia e delle funzioni sociali della madre e del padre.

Per noi la GPA rimane un’esperienza cruciale di autocoscienza femminile; quando aiuta dei padri gay o dei padri single a diventare genitori, è un ulteriore strumento di liberazione dei maschi e uno straordinario modo per fare saltare in aria rappresentazioni sociali, ruoli di genere, imposizioni storiche fatte alle donne.

Il compenso monetario è per noi indispensabile anche

\- per limitare situazioni in cui membri della famiglia prendano in carico una gestazione per un parente stretto. In alcuni paesi l’impossibilità di accedere a portatrici esterne alla famiglia crea forti pressioni in questo senso, che reputiamo nocive e deleterie per la salute delle donne e l’equilibrio delle famiglie stesse. Il Rapporto Hancock (relazione sulla GPA presentata nel 2005 al Consiglio d’Europa) mette in effetti in guardia riguardo alle GPA “in famiglia”, quelle in cui la portatrice è la sorella o cugina o cognata di chi vorrebbe un figlio. Dietro questi accordi, dice il documento, ci possono essere tacite, violentissime forme di pressione psicologica e di ricatto emotivo. (Quando a offrirsi per la GPA sono le madri non più giovani di giovani aspiranti madri infertili, a queste pressioni si aggiungono gravissimi rischi per la salute, nel caso di gravidanze gemellari o plurigemellari.) La GPA “famigliare” di solito è quella più facile da immaginare e giustificare, ma non tiene conto di questi aspetti e anche del debito psicologico dei genitori verso la parente, che non potrà mai essere saldato.

\- soprattutto per evitare e contrastare GPA clandestine in cui le donne sono potenziali vittime di accordi non mantenuti.

c)       Altre considerazioni

Auspichiamo che la GPA venga effettuata in certe situazioni e contesti:

\- pensiamo che le donne che hanno già vissuto una gravidanza per loro stesse siano maggiormente consapevoli del gesto che compiono portando in grembo il figlio di altri;; pensiamo anche che l’essere già divenute madri fornisca loro la maturità necessaria per farlo serenamente.

\- pensiamo anche che, per la tutela della donna gestante, è auspicabile che la GPA venga fatta sotto il controllo pubblico piuttosto che in clandestinità o con contratti privati non sostenuti da leggi dello Stato.

\- la GPA può e deve essere raccontata, e deve essere motivo d’orgoglio per chi la porta a termine. Occorre che le donne possano con orgoglio raccontare il “regalo” (perché nessun compenso potrà mai risarcire adeguatamente o sciogliere il debito del dono fra lei e la famiglia ricevente) di una vita messa al mondo per altri; occorre che le coppie infertili possano raccontare con orgoglio il viaggio verso l’altro che ha portato alla loro famiglia.

Riteniamo perciò che il lavoro delle famiglie omogenitoriali sia fondamentale, perché toglie il velo gettato su una pratica ampiamente utilizzata da coppie infertili eterosessuali e omosessuali, anche in contesti in cui la dignità delle donne non è chiaramente garantita o in situazioni di clandestinità.

I padri gay delle FA rivendicano con orgoglio il loro percorso e lo condividono affinché cadano zone d’ombra, ipocrisie, sfruttamento, vergogna.

d)      Conclusioni

Desideriamo sottolineare che siamo coscienti che l’accordo di GPA non è sempre un innamoramento e che può contenere anche dei conflitti, che esso in fin dei conti non può mai essere ridotto a qualcosa di meno di una relazione umana (anche se c’è chi preferisce nasconderlo dietro la facciata del patto d’affari). Chi sceglie la GPA non deve illudersi di poter aggirare questo legame, che coinvolge due famiglie e che perdura nel tempo in modalità varie. Se la corda si tende tra regioni molto lontane (economicamente e culturalmente), occorre fare più strada per entrare in contatto. Desideriamo anche mettere in evidenza il fatto che i genitori gay non hanno la possibilità di barare con i loro figli e con la loro comunità famigliare e sociale. In fin dei conti la loro visibilità obbligata è per tutti una grande opportunità, poiché ognuno di loro ha e avrà dei conti da rendere ai propri figli e alla comunità. Devono poter sempre parlare della loro storia con orgoglio, specie ai propri figli.

Perciò :

\- diffidiamo dei paesi in cui contratti di tipo privato sostituiscono leggi dello Stato.

\- diffidiamo dei paesi in cui coppie di uomini o coppie di donne o single maschi o femmine non hanno accesso alla GPA.

\- diffidiamo dei contesti in cui le gestanti non possono serenamente raccontare la loro storia e condividerla con la comunità.

\- diffidiamo dei paesi in cui le gestanti non hanno la possibilità, anche quando ne hanno espresso il desiderio, di mantenere un contatto con i bambini nati da loro e con le loro famiglie.

\- disapproviamo fortemente la pratica della GPA nei contesti in cui l’autodeterminazione delle donne non è sufficientemente garantita.

Firmato in originale a Lamezia Terme, 23 novembre 2014