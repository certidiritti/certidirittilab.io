---
title: 'NATO IL PRIMO MAGAZINE GAY NEL MONDO ARABO'
date: Wed, 22 Sep 2010 10:31:26 +0000
draft: false
tags: [arabia, Comunicati stampa, GAY, marocco, mithly]
---

_Riproponiamo l'intervento di Roberto Barducci, pubblicato su l’[Unione Sarda](http://www.unionesarda.it/) nel mese di Luglio 2010_

**Rabat**. È nato in Marocco il primo magazine gay del mondo arabo. Coraggioso, anti-conformista e rivoluzionario, la rivista Mithly (ovvero gay) vuole rompere il tabù dell’omosessualità, in un paese in cui le relazioni tra persone dello stesso sesso sono punibili dai sei mesi ai tre anni di carcere.

Per questo motivo, il magazine è stato redatto nella clandestinità, grazie al lavoro redazionale di cinque ragazzi al di sotto dei trent’anni e a Samir Bergachi, instancabile 23enne già coordinatore dell’Associazione Kif Kif (Uguaglianza), promotrice del mensile.

Il primo numero di 19 pagine, finanziato in parte dall’Unione Europea (5 mila euro) e in parte dai membri dall’Ass. Kif KIf (10 mila euro), è stato lanciato con grande clamore e temi shock. Sull’indice si leggono: attualità sulla comunità gay, testimonianze, riflessioni sull’omosessualità, dossier sul tasso di suicidi nella comunità LGBT (Lesbiche, Gay, Bisessuali e Transgender), e articoli di Abdellah Taia, noto scrittore marocchino apertamente omosessuale, autore dell’Esercito della salvezza, romanzo di formazione gay. In copertina, una foto di due paia di gambe di uomini in tacchi a spillo e la chiara scritta provocatoria in inglese: Moroccan Queer Magazine, queer è un termine controverso per indicare la comunità LGBT. E poi, non manca il sito internet, [www.mithly.net](http://www.mithly.net/), che ha già avuto 10 mila lettori, di cui l’80 per cento marocchini.

La comunità gay del paese ha dichiarato che Mithly “rappresenta un grande passo in avanti” e che offre un “soffio di speranza”. Il settimanale liberale Tel Quel ha dedicato numerosi servizi, applaudendo l’iniziativa ed elogiando il fatto che il nuovo magazine è riuscito a riaprire il dibattito sull’omosessualità e sulle libertà individuali. Anche dalle clandestine associazioni gay del Medio Oriente arrivano messaggi di supporto per il mensile, che ha l’ambizione di diventare la pubblicazione di riferimento non soltanto per gli omosessuali in Marocco, ma per tutto il mondo arabo. Mithly è infatto unica nel suo genere, nonostante in Libano esista un sito internet, Bekhsoos, dedicato a tematiche LGBT. Libano e Marocco sono infatti i due paesi più tolleranti nei confronti degli omosessuali.

I movimenti islamisti però non hanno mancato di farsi sentire. Il quotidiano Attajdid, vicino al partito marocchino islamista PJD, ha scritto che le autorità devono subito proibire la pubblicazione, che “attenta ai valori islamici”, spiegando che “l’omosessualità è contro l’avvenire dell’umanità”. Altri ancora sono addirittura arrivati a paragonare i gay a dei terroristi.

Gli attacchi contro la comunità LGBT non arrivano soltanto dagli islamisti. Nel 2007, il popolare quotidiano Al-Massae aveva incitato alla lapidazione, dopo un reportage su un presunto matrimonio di due uomini nella città povera di Ksar Kebir, iniziando una vera e propria caccia all’omosessuale stile Inquisizione. Nonostante si trattasse di una serata privata e non di un festa nuziale. Inoltre, di conta che dall’indipendenza del Marocco, siano 5 mila i gay che sono stati arrestati e incarcerati per “atti licensiosi o contro natura con individui dello stesso sesso”.

Sembra però che in questo oscurantismo, il paese si stia comunque aprendo lentamente alla modernità. Lo scorso maggio al festival musicale Mawazine World Rhythms, nonostante le proteste del PJD, che non avrebbe voluto l’esibizione di Elton John in quanto omosessuale, il governo e l’organizzazione dell’evento si sono imposti decidendo in altro modo. Il concerto del cantante pop inglese infatti non solo si è tenuto regolarmente, ma l'artista ha suonato per circa tre ore. Sono stati 40 mila i fan ad osannare Elton John e nonostante la vistosa la presenza forze dell'ordine, l’evento non è stato disturbato.

Certo, questo non è abbastanza, ma i redattori di Mithly sembrano guardare al fututo con ottimismo, ma anche con un po’ di amarezza. “L’esistenza di questa rivista mostra che le cose cominciano a muoversi – ha dichiarato Mourad, membro del team nella capitale Rabat – E forse arriveremo un giorno a fare ammettere che anche noi siamo essere esseri umani come gli altri”.