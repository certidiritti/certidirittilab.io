---
title: 'Consiglio d''Europa pubblica rapporto su rispetto diritti lgbt'
date: Thu, 23 Jun 2011 09:29:32 +0000
draft: false
tags: [Europa]
---

**Consiglio d'Europa pubblica rapporto sui diritti lgbt in 47 Paesi europei. L'Ue e gli altri paesi dovranno rispettare le 36 raccomandazioni indicate.**

**Comunicato Stampa dell’Associazione Radicale Certi Diritti**

Strasburgo – Roma, 23 giugno 2011

L’Associazione Radicale Certi Diritti ringrazia il Consiglio d'Europa per la pubblicazione del rapporto sul rispetto dei diritti Lgbt all’interno dei suoi 47 paesi membri (inclusi Russia e Turchia). Tale rapporto è di fondamentale importanza per monitorare la situazione in Europa ed intervenire laddove i più elementari diritti e i parametri democratici vengono violati.  

I 27  Stati membri  dell'Unione Europea dovranno applicare le 36 raccomandazioni contenute nel rapporto e promuovere azioni legislative e politiche di  lotta contro l'omofobia e per l'uguaglianza di tutti i cittadini. Tali raccomandazioni valgono per tutti i 47 paesi membri del Consiglio d’Europa.

Il rapporto, si sofferma, tra gli altri, sui temi della protezione legale o della discriminazione su basi giuridiche, sulla libertà di espressione, riunione e associazione, accesso all’assistenza sanitaria, all’educazione e al lavoro, alle garanzie del diritto di asilo per le persone Lgbt.

Al seguente link il testo della pubblicazione del rapporto:

 [http://www.coe.int/t/Commissioner/Source/LGBT/LGBTStudy2011_en.pdf](http://www.coe.int/t/Commissioner/Source/LGBT/LGBTStudy2011_en.pdf)