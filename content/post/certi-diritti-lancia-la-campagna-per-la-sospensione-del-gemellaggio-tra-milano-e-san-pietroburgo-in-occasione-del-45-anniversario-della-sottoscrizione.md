---
title: 'Certi Diritti lancia la campagna per la sospensione del gemellaggio tra Milano e San Pietroburgo in occasione del 45° anniversario della sottoscrizione'
date: Mon, 01 Oct 2012 11:08:36 +0000
draft: false
tags: [Russia]
---

Comunicato dell’Associazione Radicale Certi Diritti

Milano, 1 ottobre 2012

L'Associazione Radicale Certi Diritti, in occasione del 45° anniversario della firma del gemellaggio tra Milano e San Pietroburgo, lancia una campagna in cui varie personalità milanesi ne chiedono la sospensione con un video.

Il 29 febbraio scorso, infatti, **il parlamento di San Pietroburgo ha approvato una legge che sanziona la cosiddetta “propaganda dell’omosessualità” criminalizzando di fatto qualunque attività o informazione relativa alle persone LGBTI** (Lesbiche, Gay, Bisessuali, Transessuali e Intersessuali) e alle relazioni tra persone dello stesso sesso, in patente violazione delle libertà di espressione e associazione, nonché degli impegni presi dalla Russia ratificando la Convenzione europea per la salvaguardia dei diritti dell’uomo e delle libertà fondamentali. Questa legge assurda, condannata anche dal Parlamento Europeo, ha già colpito molti cittadini russi che hanno tentato di manifestare per i diritti delle persone LGBTI e per questo sono stati arrestati e multati.

Di fronte a questa patente violazione dei più elementari diritti umani, **l’Associazione Radicale Certi Diritti ha promosso una mozione al Consiglio Comunale**, firmata da tutti i capigruppo della maggioranza, per chiedere la revoca o la sospensione del patto di gemellaggio tra Milano e l’ex capitale zarista. La stessa mozione è già stata approvata all'unanimità dal Consiglio di Zona 2.

Yuri Guaiana, segretario dell’Associazione Radicale Certi Diritti, dichiara: “**La campagna inizia con una lettera a tutti i membri della Commissione Diritti Umani del Senato e ai parlamentari italiani del Consiglio d'Europa, nonché con la pubblicazione su una pagina Facebook dedicata dei video di Moni Ovadia, Lella Costa, Elio De Capitani e di tutta la compagnia dell’Elfo.** Nei giorni successivi ne pubblicheremo altri tra i quali quelli di Federica Fracassi, Danilo De Biasio, ex direttore di Radio Popolare, Ferruccio Capelli, direttore della Casa della cultura, lo storico Aldo Sabino Giannuli e Serena Sinigaglia del Teatro Ringhiera. Speriamo che il Consiglio Comunale ascolti l’appello di queste personalità della cultura milanese e decida finalmente di discutere la mozione che da sei mesi giace nei cassetti municipali, dando così un segno tangibile a favore dei diritti umani e delle libertà fondamentali. **La cosa assume oggi una particolare rilevanza, non solo per via dell’anniversario della sottoscrizione del patto, ma anche in ragione della recente approvazione al Consiglio per i diritti umani delle Nazioni Unite di una risoluzione russa per la quale i diritti umani vanno interpretati in linea con le tradizioni culturali dei singoli paesi**”.  
  
In allegato la mozioni presentate in Consiglio comunale da Marco Cappato e da Yuri Guaiana nella zona 2 per la revoca del gemellaggio tra Milano e San Pietroburgo.  
  
**[I VIDEO DELLA CAMPAGNA >>>](http://www.youtube.com/watch?v=6cgpqaGqOwM)**

**[LA PAGINA FACEBOOK >>>](http://www.facebook.com/Stopalgemellaggio)**

  
[Mozione\_revoca\_Gemellaggio\_Milano\_-\_San\_Pietroburgo.pdf](http://www.certidiritti.org/wp-content/uploads/2012/10/Mozione_revoca_Gemellaggio_Milano_-_San_Pietroburgo.pdf)  
[MOZIONE\_SULLA\_REVOCA\_DEL\_GEMELLAGGIO\_TRA\_MILANO\_E\_.pdf](http://www.certidiritti.org/wp-content/uploads/2012/10/MOZIONE_SULLA_REVOCA_DEL_GEMELLAGGIO_TRA_MILANO_E_.pdf)