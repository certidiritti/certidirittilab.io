---
title: 'L''emerito Babini alimenta odio e violenza. Intervenga la Procura della Repubblica'
date: Wed, 28 Jul 2010 22:35:43 +0000
draft: false
tags: [babioni, Comunicati stampa, glbt, lgbt, odio, OMOFOBIA, pontifex, repubblica, vaticano, VIOLENZA]
---

**LE AFFERMAZIONI DI MONSIGNOR BABINI ALIMENTANO ODIO E VIOLENZA OMOFOBE. SULL'EMERITO INTERVENGA LA PROCURA DELLE REPUBBLICA.**

_Comunicato Stampa di Famiglie Arcobaleno, Certi Diritti, AzioneTrans, Psicologia gay.com e degli esponenti del movimento lgbt italiano: Paola Biondi, Psicologiagay.com, Giuseppina Petrucci, Aurelio Mancuso, Luca Valeriani, Guido Allegrezza, Lucky Amato._

"La gravità delle affermazioni di Monsignor Babini meriterebbero un intervento immediato delle gerarchie ecclesiastiche. Sappiamo delle difficoltà della chiesa cattolica, delle gerarchie ecclesiastiche e del Vaticano a far fronte a tutti gli scandali che avvengono al loro interno riguardo le violenze pedofile che sono state tenute nascoste per molti anni alle autorità. Ciò che non compendiamo è l'indifferenza, per non parlare di grave complicità, di fronte a dichiarazioni gravi e omofobe che dimostrano del pregiudizio e dell'odio che spesso alimenta la violenza fisica contro le persone omosessuali. Ci auguriamo che quanto prima i vertici ecclesiastici intervengano su Monsignor Babini per chiarire una volta per tutte se quanto egli sostiene è frutto di opinioni personali o se le sue dichiarazioni rappresentano il pensiero della chiesa cattolica.

Siamo oltremodo scandalizzati per come Monsignore Babini sminuisca con le sue dichiarazioni l'orrendo crimine della pedofilia prendendosi beffa dei minori cosi doppiamente violentati, una prima volta fisicamente e ora con queste parole indegne.

L'Emerito farebbe bene a sapere che la comunità scientifica psicologica e psichiatrica distingue nettamente l'omosessualità che è una variante naturale della sessualità umana dalla pedofilia che è una parafilia inserita nella categoria dei Disturbi sessuali e della Identità di Genere

Invieremo la dichiarazione di Monsignor Babini alla Procura della Repubblica di Grosseto affinchè si possa valutare se nelle dichiarazioni del Monsignore si ravvisano forme di reato, in particolare per quanto prevede la legge Mancino.

Giuseppina La Delfa

Presidente Famiglie Arcobaleno

Sergio Rovasio

Segretario Associazione Radicale Certi Diritti

Mirella Izzo

Presidente di AzioneTrans

Paola Biondi di Psicologiagay.com

E gli esponenti del movimento lgbt italiano:

Giuseppina Petrucci,

Aurelio Mancuso,

Luca Valeriani,

Guido Allegrezza,

Lucky Amato,