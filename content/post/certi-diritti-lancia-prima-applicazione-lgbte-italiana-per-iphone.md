---
title: 'Certi Diritti lancia prima applicazione LGBTE italiana per Iphone'
date: Thu, 19 May 2011 07:30:10 +0000
draft: false
tags: [Politica]
---

**Informazioni e idee per un'italia più civile e al passo con l'Europa. Per scaricarla digitare 'Certi Diritti' nello spazio search dell'App store sul proprio Iphone. L'App è gratuita.**

Comunicato Stampa dell'Associazione Radicale Certi Diritti

Roma, 20 maggio 2011

L’Associazione Radicale Certi Diritti, grazie al suo esperto informatico Gian Mario Felicetti, Coordinatore del Direttivo e della campagna di Affermazione Civile, ha pubblicato la prima ‘App’ italiana per I-Phone pensata per la comunità LGBTE.

Tutti ora avranno una possibilità in più per stringersi attorno alla comunità che si batte per la promozione e la difesa dei diritti civili contro la sessuofobia, tenersi informati e capire cosa è possibile fare per costruire un'italia più civile, rispettosa delle persone prima che dei valori teorici ed astratti. Siamo convinti che l'omofobia si combatte con l'informazione, la diffusione di messaggi positivi nei confronti delle persone omosessuali e transessuali, e infine, con un’ educazione alla responsabilità civica, una sensibilità che noi chiamiamo "Affermazione Civile" e che spinge le persone ad essere cittadini responsabili, protagonisti di un processo democratico inclusivo. Tutto questo sarà possibile trovarlo nella App Certi Diritti che offrirà informazioni, idee e proposte.

Oggi c'è uno strumento in più per fare tutto questo. Siamo convinti che l'App Certi Diritti sarà uno strumento utilissimo per aiutare il passaparola nella lotta comune di tutti gli italiani contro questo male oscuro della società e della politica italiana, che è l'omofobia.  

Questa App è la migliore risposta alla odiosa App che pubblicizzava le teorie riparative e che giustamente la Apple ha poi deciso prontamente di togliere dal mercato. Oggi più che mai è importante ribadire che le pratiche delle teorie riparative è da condannare senza se e senza ma, e che questo malcostume deve essere sradicato dall'Italia. Certi Diritti dà il suo contributo, perché è convinta che questa App non solo non verrà tolta dal mercato, ma avrà un grande successo e sarà amata da migliaia di italiani e di italiane di qualunque orientamento sessuale.