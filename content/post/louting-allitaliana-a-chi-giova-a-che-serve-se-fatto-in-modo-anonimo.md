---
title: 'L''outing all''italiana a chi giova? A che serve se fatto in modo anonimo?'
date: Mon, 19 Sep 2011 13:31:45 +0000
draft: false
tags: [Politica]
---

L'outing diventa azione senza responsabilità e non di rivendicazione, dove accusatori anonimi fanno i nomi di persone che andranno a chiedere scusa al Papa. Lo strumento è contraddittorio e non aiuterebbe nessuno. I precedenti dimostrano che in un alcuni casi si è avuto come esito il **rafforzamento della vergogna e dello stigma di un comportamento che invece meriterebbe di emergere con trasparenza alla luce del sole.**

Comunicato Stampa dell'Associazione Radicale Certi Diritti  
Roma, 19 settembre 2011

Da alcuni giorni girano notizie di una prossima diffusione di elenchi di persone, politici e religiosi, gay non dichiarati, su un sito anonimo, depositato all'estero.  Si dice che usciranno i nomi di 10 politici (o forse più) e personalità omofobe. Le reazioni a questa iniziativa sono la più varie, sia all'interno della comunità LGBT che in tutta la società.

L'outing è uno strumento di per sè contraddittorio, utilizzato solo in alcune delle nazioni che sono all'avanguardia nella lotta dei diritti civili di gay e lesbiche, e sempre con risultati ambigui.

Siamo certi che se l'Italia fosse stata una nazione capace di trattare con dignità le persone omosessuali (in politica come nelle religioni, nelle istituzioni laiche così come nei media) ci potremmo risparmiare questo episodio che sicuramente non sarà ricordato come la punta di diamante della conquista dei diritti civili in Italia per le persone gay e lesbiche. **Quali sarebbero gli effetti? Per alcune/i della comunità lgbt si soddisferebbe un bisogno di vendetta che nulla porterebbe né agli accusatori (rigorosamente anonimi) né tantomeno alle vittime (cambierebbero?)**.

In particolare colpisce l'assoluta mancanza di responsabilità (e addirittura di identità) da parte di chi accusa. Cosa che inoltre fa pensare che le accuse non potranno essere corredate delle necessarie prove, oltre che del contraddittorio che ci si aspetta in simili contesti. **Messa così, tutta questa operazione potrà apparire esclusivamente pretestuosa, come già fu per il famoso caso Boffo.**

Nessuno potrà poi dimenticare che le persone lesbiche, gay, bisessuali e transgender sono state esse stesse, per millenni, vittime di delazioni e comportamenti vili che avevano l’obiettivo di denigrare e mettere all’indice la loro esistenza. E’ mai possibile che una riflessione su questo i responsabili anonimi non riescano a farla? Almeno una riflessione.

**In italia, poi, ci sono altri aberranti precedenti che lasciano ipotizzare che operazioni di outing saranno inclini al fallimento. In alcuni casi si è avuto come esito il rafforzamento della vergogna e dello stigma di un comportamento che invece meriterebbe di emergere con trasparenza alla luce del sole. All'outing non è mai seguito, almeno qui da noi, un sereno esame di coscienza nè la valutazione coraggiosa e limpida dei propri comportamenti sessuali. Al contrario abbiamo assistitio addirittura a pubbliche scuse davanti alla persona del papa, e chissà poi perché? Come se del proprio orientamento sessuale si debba dare conto ad una autorità religiosa. Ma, appunto, siamo in Italia.**

Secondo noi, chi già sospetta di essere nominato in quella lista, sta già preparando la "trafila burocratica" per un pubblico (quanto falso) mea culpa di fronte ai vertici della Chiesa. Perché si sa, una genuflessione val bene la poltrona, almeno così è qui da noi, priva ormai dei minimi valori istituzionali, che semmai fa rendere un’esenzione Ici in cambio dei propri peccati.

Come dice il nostro nome e la tradizione Radicale che ci caratterizza, anche di fronte alle volgarità, e alle violenze detestabili dei politici omofobi italiani, promuoviamo la cultura e il metodo del diritto, del dialogo aperto e della trasparenza.

Non ci resta che fare gli auguri a tutti i politici gay e omofobi in Italia: se magari, prima di andare dal papa, aveste un barlume di amor proprio e risolutezza, **l'Associazione Radicale Certi Diritti è pronta ad accogliervi e sostenervi in questa nuova fase della vostra vita in cui avrete l'opportunità di gustare fino in fondo un'esistenza non più legata alla vergogna di ciò che si è. Se volete smettere di tradire voi stessi e il vostro elettorato, noi ci siamo per darvi una mano, qualunque sia il vostro partito**. Perché anche in politica, così come nel mondo del lavoro, l'essere gay non è necessariamente uno svantaggio, ma può essere la chiave di volta di un nuovo modo responsabile di vivere la propria condizione e anche di servire il paese.

**Iscriviti alla newsletter[  
www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**

**Sostieni Certi Diritti**[  
www.certidiritti.it/iscriviti](iscriviti)