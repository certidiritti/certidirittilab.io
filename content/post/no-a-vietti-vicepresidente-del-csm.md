---
title: 'NO A VIETTI VICEPRESIDENTE DEL CSM'
date: Sun, 01 Aug 2010 19:43:34 +0000
draft: false
tags: [Comunicati stampa, consiglio superiore della magistratura, csm, glbt, lgbt, orientamento sessuale, vietti]
---

**Appello ai componenti del Consiglio Superiore della Magistratura**

Le associazioni lgbt italiane e rappresentanti dei partiti e della società civile invitano l'opinione pubblica a sottoscrivere una petizione on-line contro la nomima a vicepresidente del CSM dell' avv. Michele Vietti, primo firmatario della pregiudiziale di costituzionalità che ha affossato la legge Concia contro l'omofobia.

Nella pregiudiziale l'orientamento sessuale viene esplicitamente confuso con pratiche sessuali quali l’incesto, la pedofilia, la zoofilia, il sadismo, la necrofilia e il masochismo. FIRMA >

Illustrissimi componenti del Consiglio Superiore della Magistratura,  
  
lunedì prossimo vi accingete ad eleggere il vostro Vice-Presidente, carica importante che deve garantire quella separazione e indipendenza dei tre poteri (legislativo, esecutivo e giudiziario) posta a fondamento della nostra Costituzione e a salvaguardia della nostra Repubblica.  
  
Per questo, come cittadini, ci sentiamo in dovere di porre alla Vostra attenzione la richiesta di non eleggere l’Avv. Michele Vietti.  
  
Il neo consigliere - al quale va la stima e il rispetto che si deve portare per tutti i componenti di un organo sì fondamentale – non possiede le necessarie caratteristiche istituzionali per rivestire il suddetto ruolo.  
  
Michele Vietti è stato sottosegretario alla Giustizia nel governo Berlusconi II e sottosegretario all’Economia nel Berlusconi III. E’ uno dei padri della depenalizzazione del falso in bilancio, legge grazie a cui il premier ha evitato una condanna per i processi “All Iberian” e “Consolidato Fininvest”, in quanto “il fatto non costituisce più reato”. E’ stato altresì il promotore del ripristino dell’immunità parlamentare, nel giorno in cui Marcello Dell’Utri veniva condannato a 9 anni per concorso esterno in associazione mafiosa. E’ l’ideatore del legittimo impedimento, norma che blocca i processi per il Presidente del Consiglio dei Ministri e per i Ministri tutti.  
  
Il Michele Vietti deputato è stato il primo firmatario della pregiudiziale di costituzionalità che ha affossato la legge Concia contro l’omofobia. Nella pregiudiziale, l’orientamento sessuale viene esplicitamente confuso con pratiche sessuali quali l’incesto, la pedofilia, la zoofilia, il sadismo, la necrofilia e il masochismo. In base a tale illegittimo accostamento, l’introduzione di un’aggravante per i reati motivati dall’orientamento sessuale della vittima avrebbe significato, secondo Vietti, dare il via libera ad una protezione speciale delle suddette pratiche (incesto, pedofilia, etc). A qualunque persona, anche priva di nozioni giuridiche, non sfugge la falsità e l’offensività verso milioni di cittadini italiani di questa posizione. Essere omosessuali è una condizione personale; commettere un abuso sessuale su un minore è un crimine giustamente punito dalla legge.

FIRMA LA PETIZIONE >  [http://urlin.it/197cc](http://urlin.it/197cc)