---
title: '2008-0140-Discrimination-ST08999-RE01.EN09'
date: Wed, 29 Apr 2009 21:00:00 +0000
draft: false
tags: [Senza categoria]
---

  

  

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 30 April 2009

8999/1/09

REV 1

LIMITE

SOC 271

JAI 230

MI 170

  

  

  

  

  

NOTE

from :

The Presidency

to :

The Working Party on Social Questions

on :

5 May 2009

No. prev. doc. :

16594/08 ADD 1 SOC 749 JAI 682 MI 509

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached Presidency drafting suggestions concerning Article 4 of the above proposal. Changes in relation to the previous version (doc. 16594/08 ADD 1) are set out as follows: new text is in **bold** and deletions are marked **"\[…\]"**.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Presidency drafting suggestion**

**_Article 4_**

**_Equal treatment of persons with disabilities_**

1.       In compliance with the principle of equal treatment in relation to persons with disabilities, Member States shall take the necessary measures to ensure that persons with disabilities have:

–          effective and non-discriminatory access to social protection, social advantages, health care **and** education, and

–          effective and non-discriminatory access to and supply of goods and services which are available to the public, including housing and transport.

Such measures shall include appropriate modifications or adjustments by anticipation**.** Such measures should not impose a disproportionate burden, nor require fundamental alteration of the social protection, social advantages, health care, education, or goods and services in question or require the provision of alternatives thereto.

2.       Without prejudice to  the obligation to ensure effective non-discriminatory access and where needed in a particular case, reasonable accommodation shall be provided unless this would impose a disproportionate burden. For the purposes of this provision, “reasonable accommodation” means **necessary and appropriate modifications and adjustments not imposing a disproportionate or undue burden, where needed in a particular case, to ensure that persons with disabilities can enjoy or exercise,** on an equal basis with others**,** rights concerning social protection, including social security and health care, social advantages, education and access to goods and services in the meaning of Article 3, paragraph 1.

  

3.       For the purposes of assessing whether measures necessary to comply with paragraphs 1 and 2  would impose a disproportionate burden, account shall be taken, in particular, of the size and resources of the organisation or enterprise, its nature, the estimated cost, the life cycle of the goods and services, and the possible benefits of increased access for persons with disabilities. The burden shall not be deemed disproportionate when it is sufficiently remedied by measures existing within the framework of the equal treatment policy of the Member State concerned.

**3a.     Effective non-discriminatory access can be provided by a variety of means, including application of the universal design principle.  “Universal design” means the design of products, environments, programmes and services to be usable by all people, to the greatest possible extent, without the need for adaptation or specialised design. “Universal design” shall not exclude assistive devices for particular groups of persons with disabilities where this is needed.**[\[1\]](#_ftn1)

**4\.** **Paragraphs 1 and 2 shall not apply to the design and manufacture of goods.**

**5\.** **Paragraphs 1 and 2 shall not require significant structural changes to buildings or infrastructures which are protected under national rules on account of their historical, cultural or architectural value.**

6.       In order to take account of particular conditions, the Member States may, if necessary:

a)             have an additional period of \[X\] years from \[the deadline for transposition\] to comply with paragraphs 1 and 2**;**

b)             **implement the provisions set out in** paragraph 1 **in a gradual manner** when **\[…\]** adaptation of existing buildings or infrastructures **is required.**

  

Member States wishing to use the additional period laid down in paragraph 6(a) shall inform the Commission and provide reasons for the request for additional time by the deadline set in Article 15 \[date of transposition\].

7.       This Directive shall be without prejudice to the provisions of Community law or national rules providing for detailed standards or specifications on **the** accessibility **of** particular goods or services, **including public transport**, as long as such rules do not restrict the application of paragraphs 1 and 2.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) Article 2 of the UN Convention on the Rights of Persons with Disabilities.