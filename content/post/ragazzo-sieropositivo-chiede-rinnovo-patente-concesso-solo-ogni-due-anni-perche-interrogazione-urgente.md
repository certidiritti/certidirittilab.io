---
title: 'Ragazzo sieropositivo chiede rinnovo patente, concesso solo ogni due anni. Perchè? Interrogazione urgente'
date: Thu, 01 Dec 2011 16:42:57 +0000
draft: false
tags: [Salute sessuale]
---

Secondo la ASL di Mestre le persone sieropositive devono cheidere il rinnovo della patente ogni due anni, anche se al momento della visita viene fatto solo il controllo medico-oculistico. Quale logica scientifico-medica vi è in questa disposizione che appare del tutto discriminatoria?

Roma 1 dicembre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Secondo quanto segnalato dal  sito www.gay.it , uno dei più importanti portali di informazione lgbt italiano, un ragazzo sieropositivo che doveva rinnovare la patente si è visto rispondere dalla Commissione medica della Ussl di Mestre che il rinnovo può valere solo due anni, anziché dieci.  Ciò che  appare assurdo, in questa disposione, è che al momento della visita medica viene fatta soltanto la visita oculistica e nemmeno vengono richiesti certificati o fatte visite riguardo le condizioni di salute per chi chiede il rinnovo. Peraltro il ragazzo aveva con sè anche il certificato che confermava le sue ottime condizioni di salute. Quale logica scientifico-medica vi è in questa disposizione che appare del tutto discriminatoria?

**I deputati Radicali-Pd, prima firmataria Rita Bernardini, hanno oggi depositato una interrogazione urgente ai Minsitri dei Trasporti, della Salute e delle Pari Opportunità. Qui di seguito il testo integrale:**  
  

Interrogazione urgente a risposta scritta

Al Ministro dei Trasporti

Al Ministro della Sanità

Al Ministero per le Pari Opportunità

Per sapere – premesso che:

Secondo quanto riportato dal sito internet www.gay.it, il più importante portale di informazione gay italiano, un lettore ha segnalato che nel 2007, allo scadere della sua patente di guida, si è recato davanti alla commissione medica della Ussl di Mestre  per una visita oculistica per il rinnovo della patente di guida; i medici presenti  gli hanno chiesto quali farmaci assumesse , il Signor C. ha risposto che stava sotto terapia perché sieropositivo;  il Signor C aveva anche  un certificato del suo medico infettivologo che dichiarava del suo “ottimo stato di salute”.

A seguito di questa risposta i medici gli hanno prescritto che sarebbe dovuto tornare per il rinnovo della patente di guida dopo due anni, anziché dieci; nel 2009 gli è stata rinnovata la patente per altri due anni;

Ogni visita, tra costi di bolli, certificati e visite, ha un costo di circa 50 Euro;

La redazione del portale www.gay.it  ha contattato il Direttore del dipartimento di prevenzione della Ussl 12, Dottor Rocco Sciarrone, il quale ha confermato la prassi in vigore per chi afferma di assumere la terapia anti-Aids;

Alla redazione del portale www.gay.it è stato anche detto dai rappresentanti della Ussl di Mestre che avverso la decisione della Commissione è possibile fare ricorso;

per sapere:

-        se la prassi seguita dalla Ussl di Mestre sia quella obbligatoria prevista dalle norme che regolano rilascio e il rinnovo della patente di guida;

-        se tale prassi fosse confermata dalle norme per il rinnovo della patente di guida, quali sono i criteri medico-scientifici  seguiti se al momento della visita medica per il rinnovo si effettua solo ed esclusivamente una visita oculistica e, per le persone sieropositive, non è nemmeno necessario produrre un certificato medico che documenti la carica virale e la situazione relativa alle difese immunitarie e/o lo stato generale di salute;

-        Se non ritengano i Ministeri interrogati che le eventuali disposizioni che obbligano le persone affette da Hiv al rinnovo della patente ogni due anni, anziché dieci, rientri tra quelle norme discriminatorie che andrebbero eliminate.

Rita Bernardini

Marco Beltrandi

Maria Antonietta Farina Coscioni

Matteo Mecacci

Maurizio Turco

Elisabetta Zamparutti