---
title: 'PUBBLICAZIONE DEGLI ATTI PER MATRIMONIO'
date: Tue, 15 Apr 2008 18:22:13 +0000
draft: false
tags: [Affermazione Civile, PUbblicazione degli atti, Senza categoria]
---

**_LA PUBBLICAZIONE_  
_Cosa è e cosa occorre sapere._**

**Cosa è?**  

La pubblicazione ([articoli 93 e seguenti del codice civile](http://it.wikisource.org/wiki/Codice_civile_-_Libro_Primo/Titolo_VI#Art._93_Pubblicazione)) è il primo passo che ogni coppia compie amministrativamente per potersi sposare. La pubblicazione consiste nell'affissione alla porta della casa comunale di un atto dove si indicano il nome, il cognome, la professione, il luogo di nascita e la residenza degli sposi, se sono maggiori o minori di età, il luogo dove si sposeranno, il nome del padre e il nome e il cognome della madre degli sposi (salvo eccezioni).

**Chi la fa?**

La richiesta della pubblicazione viene fatta da tutti e due gli sposi (o da una persona che essi hanno incaricato) all'ufficiale dello stato civile del comune dove uno dei due ha la residenza ed è fatta presso i rispettivi comuni di residenza degli sposi.

L'ufficiale di stato civile può negare la pubblicazione e deve rilasciare un certificato coi motivi del rifiuto. Questo è l'atto contro il quale gli sposi possono ricorrere in tribunale per ottenere la pubblicazione e quindi potersi poi sposare.

**Partecipando ad Affermazione Civile...**  

1.  Fisseremo un **incontro** o un colloquio per dare maggiori informazioni e valutare bene il percorso da compiere
2.  La coppia prenderà a**ppuntamento con l'ufficio dello Stato Civile** e si presenterà per richiedere le pubblicazione (basta la carta di identità)
3.  L'**ufficiale di Stato Civile rifiuterà la richiesta** e dovrà emettere e consegnare alla coppia richiedente l'atto di diniego, con l'esplicitazione dei motivi (prevalentemente si tratterà di un generico richiamo alle disposizioni sulla turbativa dell'ordine pubblico). Da notare che questo documento è un obbligo al quale l'ufficiale di Stato Civile non può sottrarsi, sotto pena di denuncia per omissione di atti di ufficio (in generale, se egli si dovesse ostinare a non consegnare l'atto può bastare richiedere l'intervento dei Carabinieri, invocando l'articolo 98 del codice civile). Sottolineaimo che questo documento è essenziale, perché è sulla base del suo contenuto che può essere fatto ricorso presso il Tribunale.  
    
4.  Con l'**assistenza gratuita** dei legali di Certi Diritti e di Rete Lenfors, il rifiuto verrà impugnato presso i tribunali competenti. Nel corso del procedimento, sarà necessario sostenere alcune spese, che di norma - a parte di casi eccezionali - sono dell'ordine di poche decine di euro. Queste spesesono a carico della coppia ricorrente ([visita le Domande Frequenti](affermazione-civile/faq.html#D4) per informazioni più dettagliate).
  
  
  

[![cosa_sono_le_pubblicazioni_degli_atti.png](http://www.certidiritti.org/wp-content/uploads/2008/04/cosa_sono_le_pubblicazioni_degli_atti.png "cosa_sono_le_pubblicazioni_degli_atti.png")](affermazione-civile/pubblicazione-degli-atti.html?task=view)

[![domande frequenti](http://www.certidiritti.org/wp-content/uploads/2008/04/faq.png "domande frequenti")](affermazione-civile/faq.html?task=view) [![i_fondamenti_giuridici.png](http://www.certidiritti.org/wp-content/uploads/2008/04/i_fondamenti_giuridici.png "i_fondamenti_giuridici.png")](affermazione-civile/fondamenti-giuridici.html)