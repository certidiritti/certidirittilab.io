---
title: 'AGENZIE DELL''UE: GOVERNO ITALIANO SI MUOVA CONTRO DISCRIMINAZIONI'
date: Tue, 31 Mar 2009 09:38:06 +0000
draft: false
tags: [Comunicati stampa]
---

CERTI DIRITTI: RAPPORTO SULL'OMOFOBIA DELL'AGENZIA DEI DIRITTI FONDAMENTALI DELL'UE: GOVERNO ITALIANO SI MUOVA PER LOTTARE CONTRO OMOFOBIA  
   
Dichiarazione di Sergio Rovasio, Segretario dell'Associazione Certi Diritti e di Ottavio Marzocchi, responsabile per le questioni europee: "L'Agenzia per i diritti fondamentali dell'Unione europea ha pubblicato oggi la seconda parte della relazione sull'omofobia che il PE le aveva richiesto e dal rapporto emerge una immagine dell'Italia "schizofrenica": se da una parte la società italiana é relativamente aperta nei confronti delle persone omosessuali come emerge dai dati dell'Eurobarometro, dall'altra lo Stato ed il governo non attuano o promuovono politiche attive contro le discriminazioni, anzi si piegano alle campagne pro-discriminazione promosse in primo luogo dalla Chiesa cattolica e da esponenti politici estremisti.  
   
L'Italia é tra la minoranza dei paesi che in Europa non hanno alcuna forma di riconoscimento delle unioni tra persone dello stesso sesso; non ha un organismo competente per le discriminazioni basate sull'orientamento sessuale; non prevede la criminalizzazione né alcuna aggravante per crimini legati all'odio o all'incitazione all'odio sulla base dell'orientamento sessuale; i gruppi di estrema destra compiono attacchi contro i gay; personaggi politici e religiosi rilasciano dichiarazioni che incitano all'odio ed alla discriminazione contro le persone omosessuali.  
   
La Ministra Carfagna é esplicitamente richiamata nella relazione in merito alle sue dichiarazioni sull'inesistenza delle discriminazioni contro le persone omosessuali e sul non patrocinio del gay pride: "vi sono numerosi esempi di politici che si rifiutano di appoggiare i Prides, ma quando questi politici sono direttamente responsabili per l'eguaglianza il messaggio politico é particolarmente forte...é chiaro che gli estremisti non solo i soli attori che danneggiano il diritto alla libertà di assemblea per le organizzazioni LGBT", afferma la relazione. La chiesa cattolica é richiamata per partecipare attivamente nel dibattito politico anti-LGBT e per mobilizzare e fare lobbying contro ogni avanzamento dei diritti umani al riguardo.  
   
Certi Diritti ringrazia l'Agenzia ed il suo direttore, il danese Kjaerum, per la relazione e per la sua dichiarazioni in commissione libertà pubbliche del PE "la lotta contro le discriminazioni richiede che i governi prendano la leadership per promuovere ciò che é giusto: l'eguaglianza ed i diritti umani per tutti", e per avere illustrato l'ennesimo capitolo del "caso Italia". E' ora che il governo italiano si muova"