---
title: 'Rinnovata la patente al ragazzo a cui era stata negata perchè gay'
date: Mon, 13 Jun 2011 20:27:57 +0000
draft: false
tags: [Politica]
---

**Ora Cristian Friscina potrà lavorare. Grazie all'avv. Antonio Rotelli, all'Unar e a quanti hanno aiutato Cristian. Ma quanti Cristian ci sono in Italia?**

**Roma, 6 giugno 2011**

**Comunicato stampa dell’Associazione Radicale Certi Diritti**

Nella settimana dell’EuroPrideRoma 2011’ ci giunge da Brindisi la notizia che a Cristian Friscina è finalmente arrivato per posta il rinnovo della patente che gli era stato negato perché gay.

La vicenda risale al 2009 quando Cristian si era rivolto alla motorizzazione perché gli era scaduta la patente. La risposta fu che a causa dell'essersi dichiarato omosessuale, gli era stata tolta la possibilità di guidare e quindi il rinnovo non gli poteva essere dato a causa di una incompatibilità psico-fisica.

Nelle scorse settimane avevamo denunciato pubblicamente la vicenda di Cristian e lui stesso, il 17 maggio scorso, in occasione della Giornata internazionale contro l’omofobia,  aveva partecipato ad un incontro delle Associazioni  lgbt italiane con il Presidente della Camera dei deputati, On. Gianfranco Fini  e il Ministro per le Pari Opportunità, Mara Carfagna che conoscevano la sua vicenda.

Cristian è stato aiutato e difeso dall’Avvocato Antonio Rotelli, Presidente di Rete Lenford, il quale, sulla vicenda, aveva presentato ricorso, poi vinto. L’Unar, l’Ufficio centrale Antidiscriminazione del Ministero delle Pari Opportunità era subito intervenuto per verificare quanto stava succedendo negli uffici della Motorizzazione di Brindisi.

Non abbiamo idea di quanti sono i Cristian Friscina in Italia, perché molte persone non hanno il coraggio di denunciare questo vero e proprio sopruso, forse non solo di tipo burocratico. Ci auguriamo che si faccia presso gli uffici della Motorizzazione civile di tutta Italia una verifica ministeriale. Dopo il caso di Catania pensavamo che queste vicende non si sarebbero più verificate, invece sbagliavamo.

Ringraziamo l’Avvocato Antonio Rotelli, l’Unar, il Ministro delle Pari Opportunità, il Presidente della Camera dei deputati e i media per aver aiutato e dato sostegno a Cristian Friscina che ora potrà trovarsi un lavoro grazie al fatto che può guidare.