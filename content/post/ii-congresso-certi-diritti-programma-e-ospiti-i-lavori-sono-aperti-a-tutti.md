---
title: 'II° CONGRESSO CERTI DIRITTI: PROGRAMMA E OSPITI.    I LAVORI SONO APERTI A TUTTI'
date: Wed, 11 Mar 2009 15:17:21 +0000
draft: false
tags: [Comunicati stampa]
---

2° CONGRESSO DI CERTI DIRITTI A BOLOGNA SABATO 14 MARZO. TRA I PARTECIPANTI PARLAMENTARI, DIRIGENTI RADICALI E GLI ESPONENTI DEL MOVIMENTO LGBT ITALIANO.  IL PROGRAMMA DEI LAVORI.  

Sabato 14 marzo per tutta la giornata si terrà a Bologna, presso l’Hotel Europa (Via Boldrini) il 2° Congresso nazionale dell’Associazione Radicale Certi Diritti.

Al Congresso parteciperanno, tra gli altri: **Marco Beltrandi**, deputato radicale del Pd; **Marco Perduca**, Senatore radicale del Pd; **Paola Concia**, deputata del Pd; **Aurelio Mancuso**, Segretario Nazionale di Arcigay; **Franco Grillini**, già deputato, Direttore di Gaynews; **Pia Covre**, Presidente del Comitato per i diritti civili delle prostitute; **Leila Deianis**, Presidente Libellula; **Fabiana Tozzi**, Coordinamento Sylvia Rivera; **Marcella Di Folco**, Presdiente del Movimento italiano transessuali; **Daniele Gosti**, Segretario Associazione Rosa Arcobaleno; **Alessio De Giorgi**, Direttore di gay.it; **Felix Cossolo**, Direttore di Gayclubbing; **Mario Cirrito**, Direttore editoriale di Babilonia; **Enrico Oliari**, Presidente di GayLib; **Giuliano Federico**, Direttore di Gay.tv; **Renato Sabbadini**, Segretario Generale dell’Ilga World (International Lesbian and Gay Association); **Alba Montori** e **Claudio Mori**, Fondazione Massimo Consoli; Ilaria Trivellato, Famiglie Arcobaleno; **Rita De Santis**, Presidente Agedo.

I lavori, presieduti da Enzo Cucco, esponente storico del movimento lgbt, inizieranno alle ore 9.30 con le relazioni della Presidente Clara Comelli, del Segretario, Sergio Rovasio, e della Tesoriera Roni Guetta. A seguire si svolgeranno le seguenti relazioni tematiche su:

\- Campagna di Affermazione Civile finalizzata al matrimonio gay, **Prof. Francesco Bilotta**;

\- Terapie Riparative: le false illusioni dei fondamentalisti,  **Gianmario Felicetti**;

\- Diritti Civili  in Europa: la grave situazione italiana, **Ottavio Marzocchi**;

\- La Riforma del Diritto di famiglia: la nostra proposta, **Guido Allegrezza**;

\- Iniziative universitarie di Certi Diritti, **Giovanni Bastianelli**;

\- Riconoscimento dello status di rifugiato a vittime di persecuzioni causa orientamento

sessuale, **Dott. Andrea Mimeci**;

Ore 13 a cura di Guido Allegrezza: Cerimonia di consegna del **Premio BoaRosa**, al Presidente della Repubblica **Giorgio Napolitano** e al Ministro degli Esteri **Franco Frattini** per il loro impegno contro le discriminazioni e per aver sostenuto la proposta francese di depenalizzazione dell’omosessualità all’Onu.

Ore 13.30 – 15 pausa pranzo.

Ore 15-19 dibattito generale; a seguire votazione mozione congressuale, **elezione nuovi organi** dell’Associazione Radicale Certi Diritti.

Ore 19 Anteprima proiezione estratto del documentario **‘****Over the Rainbow’**, di Maria Martinelli e Simona Cocuzza.  Conflitti, delusioni, e speranze di una coppia lesbica italiana che non si nasconde e che rivendica il desiderio della maternità, in viaggio obbligato all’estero per raggiungere i propri sogni. Saranno presenti le protagoniste e la regista del documentario.

 I LAVORI CONGRESSUALI SONO APERTI A TUTTI