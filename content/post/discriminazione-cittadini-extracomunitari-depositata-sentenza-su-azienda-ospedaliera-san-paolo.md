---
title: 'DISCRIMINAZIONE CITTADINI EXTRACOMUNITARI - DEPOSITATA SENTENZA SU AZIENDA OSPEDALIERA SAN PAOLO '
date: Thu, 19 Jun 2008 18:04:25 +0000
draft: false
tags: [funzione pubblica, azione collettiva, accesso alle procedure di stabilizzazione, esclusione dei cittadini extracomunitari, discriminazione sulla base della cittadinanza, Comunicati stampa, sentenza, Tribunale di Milano]
---

Il Tribunale di Milano, sezione Lavoro, ha depositato il 30 maggio 2008 nella cancelleria del tribunale ordinario di Milano [la sentenza riguardante il comportamento discriminatorio dell'Azienda Ospedaliera San Paolo](http://www.cgil.it/lavorosocieta/Attualità/SAN%20PAOLO.pdf)...  
  
...nei confronti dei dipendenti extracomunitari già assunti con contatto a termine o con contratto di collaborazione coordinata e continuativa, relativamente alle procedure di stabilizzazione previste dalle norme di legge e dalle norme contrattuali.  
  
Si segnala una confusa interpretazione della discriminazione indiretta, che sussisterebbe in caso di “comportamenti che per quanto privi da parte del soggetto agente di un intento discriminatorio vengono comunque ad assumere tale connotato”.  
  
[Testo della sentenza](http://www.cgil.it/lavorosocieta/Attualità/SAN%20PAOLO.pdf)