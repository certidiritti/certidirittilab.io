---
title: 'Certi Diritti consegna al Presidente della Camera una lettera aperta: ''Contro l''omfobia ci vogliono diritti''. Dopo le parole di Gianfranco Fini si facciano atti concreti'
date: Thu, 17 May 2012 11:53:30 +0000
draft: false
tags: [Politica]
---

Roma, 17 maggio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Questa mattina alla Camera dei deputati, in occasione della presentazione del Rapporto Istat sulla popolazione omosessuale nella società italiana, alla quale era presente una delegazione dell’Associazione Radicale Certi Diritti, composta da Yuri Guaiana, Segretario, Matteo Mainardi e Sergio Rovasio, membri della Segreteria, è stata consegnata al Presidente della Camera, On. Gianfranco Fini il testo di una lettera aperta dal titotolo “Contro l’omofobia e la transfobia ci vogliono i diritti”.

Il testo della lettera, che ha l’obiettivo di sollecitare attenzione al tema dei diritti per le persone Lgbt(e), è rivolto alle Istituzioni ed alla classe dirigente del paese affinchè al più presto le parole si tramutino in leggi e programmi, unico modo per debellare ogni forma di omofobia e transfobia nel paese che determinano violenze e discriminazioni. Proprio come la Costituzione italiana recita dal 1948. Proprio come il Presidente Napolitano ha affermato ormai in molte occasioni.

Nella lettera aperta viene ricordato che “quando si parla di dignità e diritti delle persone omosessuali e transessuali tutte le scuse diventano utili per negare una semplice constatazione, che il diritto internazionale ha acquisito da tempo, ovvero i diritti delle persone lgbti (lesbiche. gay, bisessuali, transessuali e intersessuali) sono diritti umani.  Per questo riteniamo che si debba lavorare senza indugi e sulla scorta di quanto nei principali paesi europei già accade, verso una definizione chiara delle competenze e degli strumenti di intervento innanzitutto sulla Riforma del Diritto di famiglia che  includa innanzitutto il matrimonio tra persone dello stesso sesso, una legge sulle unioni civili così come già sollecitato da diverse sentenze della Corte Costituzionale, della Corte di Cassazione e di diversi Tribunali, l’omogenitorialità e il superamento di odiose discriminazioni tra figli legittimi/naturali e illegittimi.

Nella lettera si sollecita la classe politica anche ad intervenire sulla Riforma della Legge 164/1982 per semplificare il procedimento di rettifica anagrafica, anche chiarendo che gli interventi chirurgici non sono obbligatori per lo stesso.

Il Presidente della Camera dei deputati nel suo intervento di questa mattina, richiamandosi al libro bianco europeo contro l’omofobia, ha fatto un importante richiamo all’esigenza che le amministrazioni locali, i Comuni, intervengano per rimuovere le barriere discriminatorie nei confronti delle persone omosessuali. E’ anche per questo che siamo impegnati a Milano sulla raccolta firme contro le discriminazioni e a Roma consegneremo oggi, insieme a Radicali Roma e a decine di altre associazioni, oltre 7.000 firme per una proposta di Delibera di Iniziativa popolare per il riconoscimento delle Unioni civili. Ci auguriamo che questa sia una grande occasione per dare seguito concreto alle parole del Presidente della Camera.

**Al seguente link il testo integrale della Lettera Aperta ‘Contro l’omofobia ci vogliono i diritti’:**

[http://www.certidiritti.it/notizie/comunicati-stampa/item/1474-letta-aperta-che-certi-diritti-ha-consegnato-al-presidente-fini-nella-giornata-mondiale-contro-lomotransfobia](http://www.certidiritti.org/2012/05/17/lettera-aperta-che-certi-diritti-ha-consegnato-al-presidente-fini-nella-giornata-mondiale-contro-lomotransfobia/)