---
title: 'CONSULTA SU MATRIMONIO GAY: APERTA LA STRADA CONTRO DISEGUAGLIANZE'
date: Wed, 24 Mar 2010 10:57:41 +0000
draft: false
tags: [Comunicati stampa]
---

**_Dichiarazione di Sergio Rovasio, Segretario Ass. radicale Certi Diritti e candidato tra i capilista della Lista Bonino-Pannella alla Regione Lazio_**

"Qualsiasi sarà la decisione della Corte costituzionale sui ricorsi delle coppie gay e lesbiche che chiedono il matrimonio, riteniamo che la strada per il superamento delle disuguaglianze e contro le discriminazioni sia aperta. Siamo tutti chiamati oramai a marciare insieme per raggiungere questi obiettivi.

A coloro che sono già pronti a scagliarsi contro i protagonisti eroici di questa battaglia legale e ispirati ai principi della lotta nonviolenta, diciamo sin da ora, mentre ancora ci troviamo dentro l'aula della Corte in attesa che i giudici prendano una decisione, che stare alla finestra in attesa del verdetto per poi dare giudizi a seconda delle convenienze, è più comportamento da sciacalli che da persone protagoniste delle lotte per la libertà".