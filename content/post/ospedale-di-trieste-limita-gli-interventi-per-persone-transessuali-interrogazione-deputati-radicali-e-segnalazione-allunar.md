---
title: 'Ospedale di Trieste limita gli interventi per persone transessuali: interrogazione deputati Radicali e segnalazione all''Unar'
date: Fri, 17 Feb 2012 17:22:26 +0000
draft: false
tags: [Transessualità]
---

Roma, 17 febbraio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Su proposta dell’Associazione Radicale Certi Diritti è stata oggi depositata alla Camera dei deputati una interrogazione urgente a risposta scritta, prima firmataria Rita Bernardini, deputata Radicale-Pd, sulla situazione venutasi a creare all’Ospedale di Trieste riguardo l’assistenza e l’aiuto sanitario alle persone transessuali e transgender.

Il  Cedig (Centro per il supporto al disturbo delle identità di genere) di Trieste  ha denunciato nei giorni scorsi la chiusura di una sala operatoria  determinando così una grave limitazione agli interventi chirurgici di riassegnazione del sesso in uno dei centri di eccellenza più importanti d'Italia.

  
Qui di seguito il testo dell’Interrogazione parlamentare urgente

**Interrogazione urgente a risposta scritta**

  
Al Ministro della Salute

Al Ministro del Lavoro e delle Politiche Sociali

Per sapere, premesso che

  
\- Secondo quanto denunciato dai medici del Cedig di Trieste, Centro per il supporto al disturbo delle identità di genere, la Regione Friuli Venezia Giulia avrebbe negato la disponibilità all’utilizzo di una sala operatoria per il cambio di sesso;

\- Il Professor Carlo Trombetta, specialista in Urologia e coordinatore del Cedig (Centro per il supporto al disturbo delle identità di genere) ha recentemente dichiarato che: «In questi anni  abbiamo creato con il Cedig un'équipe di endocrinologi, psichiatri, ginecologi e chirurghi plastici. Sono stati organizzati dei corsi di formazione per gli infermieri. Ma non abbiamo più la disponibilità delle sale operatorie»;  Se un tempo le sale a disposizione erano due, ora il Cedig ne può utilizzare una sola al mese con 47 interventi in lista d'attesa: «Da quando ci sono gli attuali responsabili non possiamo più fare questi interventi. I quattro che vengono effettuati in un anno li inserisco all'interno del mio master e lo stesso viene fatto dal collega Zoran Arnez che ne riesce a fare altri quattro. Ma non si può far aspettare quattro anni per un intervento quando ogni giovedì riceviamo in ambulatorio persone che scelgono di cambiare sesso e che vengono da tutta Italia. Inoltre è stata anche abolita l'Unità semplice identità di genere».

\- Nel solo capoluogo del Friuli Venezia Giulia gli interventi per il cambio di genere praticati negli ultimi 18 anni sono stati 450;  Trieste fin dai primi anni Novanta è stato un centro di riferimento per le persone transessuali. Dal 1994 sono stati praticati 350 interventi di cambio sesso da uomo a donna e 100 viceversa da donna a uomo; Gli interventi chirurgici per il cambio di sesso sono poco frequenti e considerati meno urgenti degli altri. Per queste ragioni le sale operatorie per simili interventi spesso vengono negate;

\- a giudizio dell’interrogante fatti come quelli esposti aumentano le difficoltà e il disagio che invece occorrerebbe combattere promuovendo  maggiore  aiuto e sostegno alle persone transessuali e transgender:-

Per sapere:

\- Se quanto denunciato dal Professor Carlo Trombetta corrisponda al vero e quali iniziative urgenti intendano prendere i Ministri interrogati;

\- Quali iniziative intendano prendere i Ministri interrogati riguardo la messa in campo di azioni che tengano conto della necessaria tempestività che occorre per questa fattispecie di interventi chirurgici e per combattere le lunghe liste di attesa che si protraggono per anni in centri di eccellenza come il Cedig di Trieste;

\- quanti interventi di cambiamento di sesso sono stati eseguiti in Italia, anno per anno, negli ultimi 5 anni e in quali regioni.

Rita Bernardini  
Marco Beltrandi  
Maria Antonietta Farina Coscioni  
Matteo Mecacci  
Maurizio Turco  
Elisabetta Zamparutti