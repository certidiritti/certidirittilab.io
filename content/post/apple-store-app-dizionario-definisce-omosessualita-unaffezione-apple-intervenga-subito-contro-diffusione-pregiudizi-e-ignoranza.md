---
title: 'Apple Store: App dizionario definisce omosessualità un''affezione. Apple intervenga subito contro diffusione pregiudizi e ignoranza!'
date: Mon, 02 Jan 2012 15:57:31 +0000
draft: false
tags: [Politica]
---

**APPLE STORE: DIZIONARIO DEFINISCE L'OMOSESSUALITA' UNA AFFEZIONE, CERTI DIRITTI CHIEDE ALLA APPLE E ITUNES ITALIA MAGGIORE ATTENZIONE E PIU' RISPETTO. [DOPO LA CENSURA DELLA PAROLA "OMOSESSUALI"](certi-diritti-apple-censura-la-parola-omosessuali-su-itunes "Apple censura la parola "), E' ORA DI INTERVENTI PIU' STRUTTURALI PER EVITARE LA DIFFUSIONE DEL PREGIUDIZIO E DELL'IGNORANZA.**

Certi Diritti si offre per aiutare Itunes Italia verso una formazione e sensibilizzazione sul tema lgbt.

Roma, 2 gennaio 2011

Comunicato Stampa dell'Associazione Radicale Certi Dirtti

L'associazione radicale Certi Diritti ha scritto oggi ad ITunes Italia a causa di una nuova grave forma di offesa delle persone Lgbt(e). Infatti nel vocabolario "s4m" (link: [http://itunes.apple.com/it/](http://itunes.apple.com/it/)app/dizionario-italiano-completo/id417834221?mt=8#) si definisce la persona omosessuale "affetta" da omosessualità, rimandando quindi ad un concetto di malattia oppure di status di sofferenza intrinseco per il fatto stesso dell'essere omosessuali. Una tale definizione è del tutto in sintonia con le idee professate dalle cosiddette "teorie riparative" dei fondamentalisti religiosi, dalle quali la Apple si è in passato nettamente distaccata.

Inoltre, il vocabolario presenta una accezione errata del termine "bisessuale" che viene platealmente confuso con il termine "intersessuale".

Le persone transessuali vengono definite tali esclusivamente nel caso accedessero alla riassegnazione chirurgica del sesso; ciò è errato perché non necessariamente le persone transessuali devono raggiungere quell'obiettivo per migliorare la loro condizione. Purtroppo tale 'imposizione di legge' è, in alcuni casi, fonte di grande disagio.

Itunes Italia non è nuova a queste (ci auguriamo involontarie) offese alle persone Lgbt(e) italiane. Ad esempio, pochi mesi fa, nella presentazione della App di Certi Diritti (prima App italiana di informazione sui diritti delle persone LGBT) la parola "omosessuali" era stata censurata con degli asterischi.

In questo caso non è ravvisabile una responsabilità diretta da parte di ITunes Italia, dato che si tratta di una App creata da altri, questo però non vuol dire che non possano intervenire in caso di diffusione di gravi pregiudizi. La Apple (e per associazione ITunes) è famosa per essere tra le aziende più gayfriendly al mondo. Un tale buon nome va onorato e conquistato sul campo, specialmente in un contesto come quello italiano dove l'omofobia è di casa tra molti 'decision-makers'. Per una azienda italiana, essere gayfriendly non è solo un vanto, ma diventa anche un obbligo alla responsabilità e all'impegno serio, visto il grave deficit di diritti civili in cui si trova il nostro paese.

L'associazione radicale Certi Diritti chiede alla Apple di contattare urgentemente i proprietari del dizionario al fine di correggere le definizioni errate ed offensive, fino a che esse non saranno effettivamente corrette. O che, al contrario, prenda le decisioni necessarie, affinché ITunes Italia non sia associabile in nessuno modo alla promozione di contenuti omofobici e perfino in linea con le teorie riparative. Proponiamo inoltre a ITunes Italia alcune idee per rendere effettiva e concreta la natura gay friendly dell'azienda. Ci offriamo a dare contributi e idee per la formazione dei dirigenti di ITunes Italia sul tema della cultura e dei diritti civili delle persone gay, lesbiche, bisessuali, transessuali, intersessuali (e anche eterosessuali).

Qui di seguito il link dell'App Store di ITunes Italia dove è possibile scaricare il vocabolario della lingua italiana, chiamato s4m (link: [http://itunes.apple.com/it/](http://itunes.apple.com/it/)app/dizionario-italiano-completo/id417834221?mt=8#) con le gravi definizioni segnalate nel Comunicato Stampa.

**_eterosessuale_**

1 (sm. e sf.) e sf. Che, chi presenta eterosessualità.

**_omosessuale_**

1 (sm. e sf.) e sf. Che, o chi, è affetto da omosessualità. ~ donna lesbica; uomo invertito.

Sinonimi: finocchio, gay, invertito, omofilo

Contrari: eterosessuale

**_bisessuale_**

1 (agg.) Che ha i caratteri di entrambi i sessi. ~ ermafrodito.

Sinonimi: bisessuato, ermafrodito, monoclino

**_transessuale m e f_** (pl: transessuali)

Persona che sente di appartenere al sesso opposto, rispetto a quello anagrafico, assumendone chirurgicamente le sembianze fisiologiche.