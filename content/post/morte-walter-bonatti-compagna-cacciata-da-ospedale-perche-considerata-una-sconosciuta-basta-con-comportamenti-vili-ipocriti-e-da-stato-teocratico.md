---
title: 'Morte Walter Bonatti: compagna cacciata da ospedale perchè considerata una sconosciuta. Basta con comportamenti vili, ipocriti e da stato teocratico'
date: Wed, 21 Sep 2011 15:10:00 +0000
draft: false
tags: [Diritto di Famiglia]
---

Rossana Potestà, la convivente del noto alpinista, cacciata dall'ospedale in quando considerata sconosciuta dall'ospedale e dalla legge. L'Italia si muova.

Roma, 21 settembre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Apprendiamo dalle cronache di oggi che la compagna di vita di Walter Bonatti, Rossana Potesta', è stata allontanata in modo umanamente inqualificabile dalla struttura ospedaliera di terapia intensiva che curava il noto alpinista.

Purtroppo in Italia le persone non sposate non hanno nè diritti civili, nè riconoscimenti pubblici e le strutture sanitarie pubbliche sono in diritto (e a volte in dovere, quando gestite dagli ambienti clericali) di denigrare e umiliare la dignità di chi ha osato fare progetti, nutrire affetti e servirsi in mutua solidarietà senza sposarsi.

Alla signora Rossana Potestà, compagna di vita di Walter Bonatti, va tutta la nostra solidarietà umana e civile. La sua storia è tragicamente in piena sintonia con le storie delle tantissime coppie italiane dello stesso sesso che al contrario  non possono nemmeno sposarsi se lo vogliono, perché non è certo il coniugio che dà la dignità alle persone. La dignità sta nella libertà di scelta, e per questo lo Stato ha il dovere di riconoscere le famiglie italiane in tutte le loro forme. Queste famiglie, tutte insieme, costituiscono quella "società naturale" che la nostra Costituzione invita a riconoscere, così come anche indicato dalla stessa Corte Costituzionale con la sentenza 138/2010.

Al contrario, l'unica possibilità che l'Italia lascia alle coppie non sposate è quella di organizzarsi autonomie private o spilucchiare il codice civile alla ricerca di sparuti riconoscimenti ai conviventi sparsi qua e là, quasi nascosti e senza alcuna organicità. In aggiunta queste poche, miserabili possibilità vengono taciute: non sia mai che poi la gente comune ne faccia uso!

Ad esempio pochi sanno che l'articolo 82 del DECRETO LEGISLATIVO 30 giugno 2003, n. 196 (CODICE IN MATERIA DI PROTEZIONE DEI DATI PERSONALI) riconosce al convivente - seppure in modo contraddittorio e lacunoso - la possibilità di essere informato sulla condizione di salute del convivente. In alternativa, anche la mutua designazione dell'amministratore di sostegno avrebbe permesso alla signora Potesta' di seguire il compagno nel suo percorso finale. Paradossalmente, un semplicissimo atto anagrafico o notarile avrebbe potuto evitare alla Signora Potestà, ma anche a molti altri conviventi,questa umiliazione.Questo episodio che i più troveranno disumano è la riprova che, al di là delle apparenze, l'affermazione dei propri diritti è una questione di profonda umanità.

Per questo l'associazione radicale Certi Diritti è pronta a sostenere Rossana Potestà, e chiunque si sia trovato in condizioni simili, a far valere il proprio stato di diritto con un ricorso nei confronti della giustizia ordinaria, non per rivalersi nei confronti delle strutture (che hanno stupidamente ma lecitamente applicato la legge italiana) ma per evidenziare ai giudici il vulnus che ancora esiste nella legislazione e promuovere attraverso la via giudiziaria quella riforma del diritto di famiglia che gli italiani aspettano da decenni.

Testo dell'Articolo 82 del DECRETO LEGISLATIVO 30 giugno 2003, n. 196 (CODICE IN MATERIA DI PROTEZIONE DEI DATI PERSONALI)

Emergenze e tutela della salute e dell’incolumità fisica

a) 1. L’informativa e il consenso al trattamento dei dati personali possono intervenire senza ritardo, successivamente alla prestazione, nel caso di emergenza sanitaria o di igiene pubblica per la quale la competente autorità ha adottato un’ordinanza contingibile ed urgente ai sensi dell'articolo 117 del decreto legislativo 31 marzo 1998, n. 112.

2\. L’informativa e il consenso al trattamento dei dati personali possono altresì intervenire senza ritardo, successivamente alla prestazione, in caso di:

a) impossibilità fisica, incapacità di agire o incapacità di intendere o di volere dell’interessato, quando non è possibile acquisire il consenso da chi esercita legalmente la potestà, ovvero da un prossimo congiunto, da un familiare, da un convivente o, in loro assenza, dal responsabile della struttura presso cui dimora l’interessato;

b) rischio grave, imminente ed irreparabile per la salute o l’incolumità fisica dell’interessato.

3\. L’informativa e il consenso al trattamento dei dati personali possono intervenire senza ritardo, successivamente alla prestazione, anche in caso di prestazione medica che può essere pregiudicata dall'acquisizione preventiva del consenso, in termini di tempestività o efficacia.

4\. Dopo il raggiungimento della maggiore età l’informativa è fornita all'interessato anche ai fini della acquisizione di una nuova manifestazione del consenso quando questo è necessario.