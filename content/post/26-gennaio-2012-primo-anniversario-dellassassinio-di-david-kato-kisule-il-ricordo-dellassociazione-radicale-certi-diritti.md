---
title: '26 gennaio 2012: primo anniversario dell''assassinio di David Kato Kisule. Il ricordo dell''Associazione Radicale Certi Diritti'
date: Wed, 25 Jan 2012 15:02:59 +0000
draft: false
tags: [Africa]
---

L'associazione radicale Certi Diritti ricorda la sua figura con uno speciale su Radio Radicale

Roma, 26 gennaio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Il 26 gennaio 2012 si celebra il primo anniversario dell’uccisione di **David Kato Kisule**, vittima dell’odio del fondamentalismo religioso. David, militante a attivista gay, membro di Smug, Sexual Minorities Uganda, venne barbaramente assassinato presso la sua abitazione dopo una campagna di odio che in Uganda è stata alimentata dai predicatori evangelici nelle piazze delle periferie causando una versa e propria caccia agli omosessuali e da una proposta di legge che chiedeva la pena di morte per le persone Lgbt. La rivista ugandese ‘Rolling Stone’ pubblicò in copertina, nel mese di ottobre del 2010, le foto di 100 attivisti gay con la richiesta immediata di arresto e impiccagione.

**Francis Onyango**, il suo avvocato, intentò una causa, poi vinta, contro la rivista e il giudice ne impose la chiusura. Durante la varie fasi del dibattimento David venne aggredito e malmenato dal pubblico che assisteva alle udienze e venne aiutato e protetto dalle delegazioni diplomatiche presenti grazie alle richieste di Ong internazionali.

David Kato Kisule fu invitato a Roma nel novembre 2010  al IV Congresso dell’Associazione Radicale Certi Diritti dove raccontò il dramma della situazione ugandese chiedendo aiuto e sostegno. David si iscrisse in quell’occasione a Certi Diritti e decise di tornare nel suo paese, nonostante i gravi rischi per la sua incolumità.

A Kampala oggi è prevista una giornata di commemorazione della sua figura presso il City  Center dove sono previsti interventi di rappresentati di Ong, amici  e attivisti ugandesi.  A nome della famiglia parlerà il fratello.

Alla notizia della sua morte Il presidente Usa Barack Obama disse: “Sono profondamente rattristato di sapere del suo omicidio. David ha dimostrato grande coraggio nello schierarsi apertamente contro l'odio. Era un grande testimone dei valori di uguaglianza e libertà".

L’Unione Europea, il Dipartimento di Stato Usa, esponenti politici europei e americani,  espressero frasi di cordoglio per la scomparsa di David Kato Kisule. Marco Pannella disse: “Spero che le massime autorità della nostra Repubblica dicano una loro parola sulla morte di David Kato. Ne onorerebbero non solo la memoria  ma la Repubblica stessa…”.

Francis Onyango, il suo Avvocato, è stato eletto lo scorso dicembre Presidente onorario dell’Associazione Radicale Certi Diritti. Negli ultimi mesi, grazie al supporto di Ong e Associazioni di diversi paesi del mondo, Francis e altri esperti sono riusciti  a fare una  contro-inchiesta, consegnata alle autorità, che smentisce quanto dichiarato dalla polizia sul fatto che l’assassino di David Kato Kisule è una persona estranea alla campagna di odio promossa nel paese.

Giovedì 26 gennaio, Radio Radicale trasmetterà uno speciale su David Kato Kisule curato da Sergio Rovasio e Dino Marafioti con le testimonianze di Elio Polizzoto, amico di David; Paolo Patanè, Presidente di Arcigay, Yuri Guaiana, Segretario dell’ Associazione Radicale Certi Diritti; Matteo Pegoraro, Gruppo EveryOne; Elio De Capitani, Direttore Artistico del Teatro dell’Elfo di Milano, Marco Pannella, leader dei Radicali.

Al seguente link le modalità per contribuire al **Fondo David Kato Kisule** promosso dalle Associazioni Arcigay, Associazione Radicale Certi Diritti e Non c'è Pace Senza Giustizia, in accordo con Smug (quanto raccolto finora è stato consegnato all'Avvocato Francis Onyango):[  
**http://www.certidiritti.it/fondo-in-memoria-di-david-kato-kisule**](fondo-in-memoria-di-david-kato-kisule)

Al seguente link il testo tradotto in italiano dell’**intervento di David Kato Kisule** al IV congresso dell’Associazione Radicale Certi Diritti:  
**[http://www.certidiritti.it/ultime/intervento-di-kato-david-kisule-al-iv-congresso-di-certi-diritti](ultime/intervento-di-kato-david-kisule-al-iv-congresso-di-certi-diritti)**

Al seguente link gli interventi di RR su David Kato Kisule e l’**audio video integrale del suo intervento a Roma del novembre 2010**:[  
**http://www.cuturl.co.uk/bx**](http://www.cuturl.co.uk/bx)

**iscriviti alla newsletter >[  
](newsletter/newsletter)[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**