---
title: 'Registro Unioni Civili a Milano: l’associazione radicale Certi Diritti regala una copia del libro ''Dal cuore delle coppie al cuore del diritto'' a tutti i consiglieri comunali'
date: Fri, 20 Jul 2012 09:39:55 +0000
draft: false
tags: [Diritto di Famiglia]
---

Comunicato stampa dell’Associazione Radicale Certi Diritti

Milano, 20 luglio 2012

In occasione del prossimo dibattito in Consiglio Comunale a Milano, l’Associazione Radicale Certi Diritti regalerà a tutti i consiglieri comunali una copia del libro "Dal cuore delle coppie al cuore del diritto. L'udienza alla Corte Costituzionale per il diritto al matrimonio tra persone dello stesso sesso". Il libro spiega come la sentenza sia frutto della campagna di Affermazione civile che ha visto molte coppie dello stesso sesso mettersi in gioco direttamente per chiedere di vedere riconosciuti i propri diritti in dialogo con le istituzioni.

Il volume sarà accompagnato da una lettera firmata da Yuri Guaiana, Segretario Associazione Radicale Certi Diritti e curatore del testo. La lettera sottolinea come dietro la sentenza della Corte Costituzionale vi siano le importanti considerazioni giuridiche avanzate dai nostri avvocati durante l’udienza, ma anche migliaia di cittadini discriminati in un aspetto fondamentale della loro esistenza che attendono da troppo tempo giustizia e pari dignità di fronte allo Stato. L’Associazione Radicale Certi Diritti ribadisce che il regolamento delle unioni civili in discussione, se approvato, darà la possibilità a tutte le coppie non matrimoniali, etero e omosessuali, di fare un piccolo passo avanti nell’equiparazione di diritti e doveri per quanto riguarda le materie di competenza comunale e che gli enti locali hanno un ruolo cruciale nel combattere sul territorio tutte quelle discriminazioni che si annidano persino nella macchina comunale. La missiva sottolinea quindi l’importanza del comma del regolamento che impegna il Comune a provvedere, “attraverso singoli atti e disposizioni degli Assessorati e degli Uffici competenti, a tutelare e sostenere le unioni civili, al fine di superare situazioni di discriminazione, favorire pari opportunità, integrazione e lo sviluppo nel contesto sociale, culturale ed economico del territorio”.

Guaiana ricorda, inoltre, che l’Associazione Radicale Certi Diritti, insieme ad altre associazioni milanesi tra cui il CIG - Arcigay Milano, sta portando a termine la raccolta di 5000 firme per 5 proposte di delibera di iniziativa popolare tra cui una sul riconoscimento delle unioni civili e il sostegno alle nuove forme famigliari e una sulla prevenzione, contrasto e assistenza alle vittime di discriminazione nonché per la promozione di pari opportunità per tutti.

Oltre al volume regalato si consiglia infine anche la lettura del manuale “Certi Diritti che le coppie non sanno di avere. Per vivere consapevolmente in coppia o in attesa di giuste nozze” di Bruno de Filippis et altri, edito da StampAlternativa, dove bene si chiariscono gli ambiti nazionali in cui la certificazione anagrafica può aiutare le famiglie non matrimoniali nell’attuale contesto di vuoto legislativo.

La lettera si conclude con la richiesta al Consiglio Comunale di far fare a Milano e a tutto il Paese un passo avanti verso il pieno rispetto della Costituzione e dei trattati internazionali ed europei sottoscritti dall’Italia, i quali prevedono l’eliminazione di tutte le discriminazioni basate sul sesso e l’orientamento sessuale.