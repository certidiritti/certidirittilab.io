---
title: 'DA OGGI AL VIA LA SEZIONE “MILANO” DI CERTI DIRITTI'
date: Tue, 26 Jan 2010 18:52:45 +0000
draft: false
tags: [Comunicati stampa]
---

Dopo due anni di vita, parte ufficialmente la sezione milanese del sito di Certi Diritti.

A [questo link](index.php?Itemid=135) toverete tutte le notizie riguardanti la città di Milano e, possibilmente, la Lombardia: oltre ai nostri comunicati, le news, le leggi e i regolamenti approvati o in discussione presso comune, provincia e regione, le dichiarazioni dei politici sul tema dei diritti civili, informazioni sugli incontri di Certi Diritti a Milano, sulle iniziative nostre e di tutte le altre associazioni LGBT.

> **Se vivete a Milano o in Lombardia, mettete [questo link](index.php?Itemid=135) tra i vostri preferiti e seguiteci!**

Se volete suggerci gli argomenti da trattare, o volete altre informazioni su Certi Diritti a Milano, scriveteci a [milano@certidiritti.it](mailto:milano@certidiritti.it) Gireremo i vostri suggerimenti a **Emanuele Sebastio**, che cura questa sezione del sito.

Vi aspettiamo numerosi a visitare le nostre pagine!