---
title: 'BUTTIGLIONE LANCI ANATEMI DA ALTRI PULPITI, NOI NON ANTICATTOLICI'
date: Fri, 19 Jun 2009 10:03:23 +0000
draft: false
tags: [Comunicati stampa]
---

#### CANDIDATURA PRESIDENZA PE: NESSUN PREGIUDIZIO ANTICATTOLICO PER MAURO, PERO’ OGNUNO PREDICHI DAL PROPRIO PULPITO, NO AI FONDAMENTALISMI RELIGIOSI IN EUROPA.

#### Roma, 19 giugno 2009

#### Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

#### “Il Presidente dell’Udc Rocco Buttiglione dovrebbe ben sapere che i fondamentalismi religiosi o ideologici non hanno posto per cariche di responsabilità in Europa. Lui, meglio di tutti, lo dovrebbe sapere dopo che è stato cacciato dal Parlamento Europeo per avere avuto delle posizioni integraliste, innanzitutto verso le persone omosessuali. Nessuno di noi, nell’esprimere la propria contrarietà all’onorevole Mauro Presidente del Parlamento Europeo, si ispira a ‘pregiudiziali anticattoliche’ o intende promuovere ‘attacchi personali’ a chicchessia. Abbiamo già detto che L’on. Mario Mauro è persona corretta e lavoratrice (altro che attacco personale dunque), rispetto ad altri deputati italiani del centro-destra che spiccano per le loro assenza ai lavori del Pe.

#### Ciò che non condividiamo, pur dispiacendo a Buttiglione, è che se un politico europeo considera la bandiera europea un ‘simbolo cristiano della Vergine Maria’ o si richiama ad un’ Europa ‘nata cristiana altrimenti non è’ o, ancora, sostiene la necessità di un Concordato tra Ue e chiesa cattolica sul modello di quello italiano, bhè, caro Presidente Buttiglione, il pulpito di questi proclami si fa in altri luoghi ma non nel ruolo di Presidente del Parlamento Europeo, istituzione di civiltà, progresso e laicità. Questo abbiamo detto e confermiamo. Le scomuniche e i proclami  potete continuare a farli a Roma dove il Vaticano vi indica la linea, non a Bruxelles”.