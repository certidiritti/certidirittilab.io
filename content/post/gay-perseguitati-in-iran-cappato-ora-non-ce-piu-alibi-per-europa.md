---
title: 'GAY PERSEGUITATI IN IRAN, CAPPATO: ORA NON C''E PIU'' ALIBI PER EUROPA'
date: Tue, 29 Apr 2008 17:16:07 +0000
draft: false
tags: [Comunicati stampa]
---

Bruxelles, 29 aprile 2008 Rispondendo ad un'interrogazione scritta di Marco Pannella, la Commissione europea ha espresso una dura condanna contro il regime teocratico iraniano e dichiarando per la prima volta in modo inequivocabile di essere "consapevole del fatto che in Iran l'omosessualità consensuale tra adulti è perseguibile a termini di legge.

Parimenti, il diritto alla privacy risulta regolarmente violato dai guardiani religiosi, i quali effettuano incursioni nelle abitazioni e in altri luoghi privati alla ricerca di comportamenti "devianti", tra cui la condotta omosessuale. Inoltre, sono stati denunciati anche casi di condanna a morte e maltrattamenti per omosessualità consensuale in Iran"."Finalmente - ha dichiarato l'eurodeputato radicale Marco Cappato - viene sancita in modo ineccepibile la linea di condotta alla quale gli Stati membri dell'Unione europea devono attenenersi nel valutare richieste di asilo o provvedimenti di espulsione di cittadini iraniani omosessuali. D'ora in poi, ha aggiunto, nessun Paese europeo potrá piú dire di 'non sapere' quali quali siano i rischi per una persona omosessuale in Iran". Secondo il commissario ad interim, Jacques Barrot, infatti, "le autorità degli Stati membri devono tenere conto di tali circostanze nel decidere, in conformità al diritto comunitario, riguardo a casi nei quali sussiste il timore di persecuzioni per motivi di orientamento sessuale in Iran, in particolare ove l'omosessualità di un richiedente asilo sia stata portata a conoscenza delle autorità iraniane".

Link all'interrogazione scritta:

[http://www.europarl.europa.eu/...](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+WQ+P-2008-1330+0+DOC+XML+V0//IT&language=IT)