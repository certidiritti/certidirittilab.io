---
title: 'AGEDO E FAMILY MATTERS: IL TRAILER DI "DUE VOLTE GENITORI" DI CIPELLETTI'
date: Sun, 22 Jun 2008 15:41:47 +0000
draft: false
tags: [certi diritti, Claudio Cipelletti, Comunicati stampa, Due volte Genitori, Familiy Matters]
---

Durante il convegno ["Family Matters"](index.php?option=com_content&task=view&id=92&Itemid=56) tenutosi in questi giorni a Firenze, è è stato ufficialmente presentato il film documentario "Due Volte Genitori" di Claudio Cipelletti.  
[Clicca qui per vedere il trailer.](index.php?option=com_content&task=view&id=126&Itemid=55)  
[vedi il trailer](http://www.youtube.com/watch?v=aRRE_tyfC2U)