---
title: 'LETTERA ROVASIO E AVERSA A COMITATO CATANIA PRIDE: DIALOGO CON LA BERTOZZO'
date: Wed, 02 Jul 2008 12:35:17 +0000
draft: false
tags: [Comunicati stampa]
---

Roma, 2 luglio 2008

  
**Al Comitato Catania Pride  
Arcigay Catania, Open Mind GLBT, Pegasos Club  
**  
 

Cari amici del Comitato Catania Pride,

quanto è accaduto la sera di sabato 28 giugno nei pressi del palco del Pride di Bologna è un fatto grave e segna le difficoltà del movimento gay, lesbico e transessuale nell'ottenere giusti riconoscimenti legislativi, sociali, politici. Il movimento per i diritti civili è da sempre composto da molte associazioni ognuna con una propria specificità e con progetti ed idee in autonomia: questo fatto non può diventare un ulteriore ostacolo alle rivendicazioni e alle varie forme di protesta, Pride in testa, ma deve contribuire al pluralismo e alla forza del movimento lgbtq tutto.

La violenza e le prevaricazioni vanno combattute da qualsiasi parte provengano ma nello stesso tempo bisogna impegnarsi per la libera espressione di tutti. Il Pride non appartiene solo agli organizzatori ma a tutti quelli che vi partecipano anche con posizioni critiche rispetto alla piattaforma politica della manifestazione. La memoria storica del movimento non deve dimenticare l'impegno di figure come quella di Graziella Bertozzo che, come molti altri esponenti del movimento, ha speso parte della sua vita per il riconoscimento dei diritti di libertà e di civiltà di lesbiche, gay e trans. I Pride 2008, da quello di Roma, a quello di Biella fino a quello di sabato 5 a Catania, sono stati messi in seria discussione da divieti e ostacoli posti in atto dalle istituzioni e dai vari esponenti omofobi della nostra società: questo ci deve far riflettere e ricordare che i principali avversari sono esterni al movimento lgbtq. <!\-\- D(\["mb","u003c/divu003ennu003cdivu003e u003c/divu003enu003cdivu003eProponiamo e chiediamo al Comitato Pride di Catania di far intervenire Graziella Bertozzo dal palco di piazza dellu0026#39;Università, per un tempo uguale a quello dato a tutti gli altri esponenti del movimento, per chiarire definitivamente lu0026#39;episodio increscioso e riprendere serenamente e rafforzati la difficile e lunga battaglia per i diritti ancora negati. Ci auguriamo che questo possa contribuire a superare le tensioni e anche gli equivoci nati dallu0026#39;episodio di Bologna.u003cspanu003eu003c/spanu003e u003c/divu003ennu003cdivu003eu003cbru003eu003cbru003eu003cstrongu003eSaverio Aversau003cbru003eu003c/strongu003eResp. Naz.u003cbru003eDiritti e Culture delle Differenzeu003cbru003ePRC-SEu003cbru003eu003cbru003eu003cstrongu003eSergio Rovasiou003c/strongu003eu003cbru003eSegretario Associazione Radicale Certi Dirittiu003c/divu003enu003cdivu003e u003c/divu003enu003cdivu003e u003c/divu003enu003cdivu003enu003cp styleu003d"margin:0cm 0cm 0pt"u003eu003cspan styleu003d"font-size:13pt"u003eu003cfont faceu003d"Times New Roman"u003e u003c/fontu003eu003c/spanu003eu003c/pu003enu003cp styleu003d"margin:0cm 0cm 0pt"u003eu003cspan styleu003d"font-size:13pt"u003eu003cfont faceu003d"Times New Roman"u003e u003c/fontu003eu003c/spanu003eu003c/pu003enu003cp styleu003d"margin:0cm 0cm 0pt"u003eu003cspanu003e u003c/spanu003eu003c/pu003enu003cp styleu003d"margin:0cm 0cm 0pt"u003eu003cspanu003e u003c/spanu003eu003c/pu003enu003cp styleu003d"margin:0cm 0cm 0pt"u003eu003cstrongu003eu003cspanu003eInterrogazione a risposta scritta al Ministro degli Interniu003c/spanu003eu003cspanu003eu003c/spanu003eu003c/strongu003eu003c/pu003ennu003cp styleu003d"margin:0cm 0cm 0pt"u003eu003cspanu003eu003cstrongu003e u003c/strongu003eu003c/spanu003eu003c/pu003enu003cp styleu003d"margin:0cm 0cm 0pt"u003eu003cstrongu003eu003cspanu003ePer Sapere  Premesso che:u003c/spanu003eu003cspanu003eu003c/spanu003eu003c/strongu003eu003c/pu003ennu003cp styleu003d"margin:0cm 0cm 0pt"u003eu003cspanu003e u003c/spanu003eu003c/pu003enu003cp styleu003d"margin:0cm 0cm 12pt"u003eu003cspanu003eLo scorso 28 giugno, durante la fase conclusiva della manifestazione del Gay Pride nazionale di Bologna, alcuni oratori intervenivano dal palco allestito in Piazza VIII maggio. Mentre dal palco intervenivano alcuni degli esponenti del movimento lgbt alcuni esponenti del Movimento Facciamo Breccia si avvicinavano per sostenere uno striscione scenografico. Tra questo gruppo di sostenitori vu0026#39;era anche Graziella Bertozzo, esponente storica del movimento Lgbt e di Facciamo Breccia che, per un ritardo, era rimasta al di fuori del perimetro di delimitazione del palco. Secondo quanto ricostruito anche da testimoni oculari Graziella Bertozzo nel tentativo di raggiungere il palco, senza il passi di accesso, viene spintonata a terra anche da un agente in borghese oltre che da altri in divisa, ",1\] ); //-->

Proponiamo e chiediamo al Comitato Pride di Catania di far intervenire Graziella Bertozzo dal palco di piazza dell'Università, per un tempo uguale a quello dato a tutti gli altri esponenti del movimento, per chiarire definitivamente l'episodio increscioso e riprendere serenamente e rafforzati la difficile e lunga battaglia per i diritti ancora negati. Ci auguriamo che questo possa contribuire a superare le tensioni e anche gli equivoci nati dall'episodio di Bologna.

  
  
**Saverio Aversa  
**Resp. Naz.  
Diritti e Culture delle Differenze  
PRC-SE  
  
**Sergio Rovasio**  
Segretario Associazione Radicale Certi Diritti