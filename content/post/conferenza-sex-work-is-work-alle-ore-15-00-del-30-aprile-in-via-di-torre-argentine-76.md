---
title: 'Conferenza SEX WORK IS WORK, alle ore 15.00 del 30 Aprile in via di Torre Argentine 76'
date: Wed, 29 Apr 2015 09:03:19 +0000
draft: false
tags: [Lavoro sessuale]
---

[![11150253_10205021826359229_9001238621387071889_n](http://www.certidiritti.org/wp-content/uploads/2015/04/11150253_10205021826359229_9001238621387071889_n.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/04/11150253_10205021826359229_9001238621387071889_n.jpg) Conferenza “SEX WORK IS WORK “ alle ore 15.00 del 30 Aprile in via di Torre Argentine 76 (sede dei Radicali)-ROMA Le lavoratrici e i lavoratori del sesso sono riuniti a Roma per confrontarsi con i/le rappresentanti di Camera e Senato, sostenuti da un cartello di associazioni che difendono e promuovono i Diritti Civili e da rappresentanze dei Consumatori. Come lavoratrici e i lavoratori del sesso chiediamo di essere ascoltati dai politici che vogliono impegnarsi per mettere a riposo la Legge Merlin e fare una legge più adatta alla realtà che viviamo. Le proposte di legge presentate in Parlamento per ora non ci soddisfano: molte contegono norme che sono contro i nostri diritti, la nostra dignita' e la nostra libertà personale. Abbiamo l’occasione di una nuova normativa che risponda alle esigenze di cittadine e cittadini che da tempo chiedono di superare disagi e problemi nelle proprie aree residenziali e, soprattutto, di rendere pienamente legale la condizione delle lavoratrici e dei lavoratori del sesso, il che offrirebbe maggiori garanzie anche per i consumatori. Per questo la giornata del 30 aprile in attesa del 1 maggio sarà dedicata ad una Conferenza alle ore 15.00, con conferenza stampa finale dove sex worker, escort, gigolò risponderanno alle domande dei giornalisti. Alle ore 22.30 ci sarà a via dei Fori Imperiali una sfilata di ombrelli rossi simbolo dei sex worker nel mondo con artistiche performance perché Il 1 Maggio è anche la nostra festa! NIENTE SU DI NOI SENZA DI NOI Il 1 Maggio è anche nostro Telefono CDCP 3355400982  

Comitato per i Diritti Civili delle Prostitute

Associazione Radicale Certi Diritti

CODACONS