---
title: 'Parlamento Europeo pubblica studio per una tabella di marcia europea per l''eguaglianza per le persone lgbti'
date: Sat, 27 Oct 2012 09:57:42 +0000
draft: false
tags: [Europa]
---

Comunicato Stampa dell'Associazione Radicale Certi Diritti

Roma, 27 ottobre 2012

Il Parlamento europeo ha pubblicato oggi lo studio "Verso una tabella di marcia dell'UE per l'eguaglianza sulla base dell'orientamento sessuale e l'identità di genere", che sostiene la necessità e l'urgenza di lanciare tale "roadmap" al fine di lottare contro le discriminazioni delle persone LGBTI nell'Unione europea.

Lo studio, richiesto su iniziativa del gruppo liberale e democratico al Parlamento europeo su impulso di Ottavio Marzocchi, funzionario nonché membro del direttivo dell'Associazione Radicale Certi Diritti, è stato commissionato dalla commissione libertà pubbliche, giustizia ed affari interni del PE, che ha ripetutamente richiesto alla Commissione europea di elaborare tale tabella di marcia. La Commissione ha fino ad oggi ignorato tali richieste, mentre la Commissaria per i diritti fondamentali e la giustizia Vivane Reding ha evitato di rispondere alle domande dei deputati europei nel corso della sua ultima audizione in commissione.

Lo studio si concentra sulle aree dell'eguaglianza (impiego, salute, educazione, accesso ai beni ed ai servizi ed alla casa), le questioni trans ed intersex, le famiglie e la libertà di circolazione, la libertà di assemblea e di espressione, la violenza, il crimine e l'incitazione all'odio, la protezione delle persone che fuggono dall'omofobia e dalla transfobia in Stati terzi. Infine sono proposte una serie di raccomandazioni ed azioni da intraprendere con un calendario, come base di lavoro per definire tale "roadmap".  

La commissione libertà pubbliche si baserà su tale studio per elaborare nel corso del 2013 una relazione, come proposto dal gruppo liberale su proposta di Marzocchi dell'Associazione Radicale Certi Diritti. Allo stesso tempo, verrà utilizzato dai deputati per verificare le credenziali del nuovo commissario maltese Mario Borg che sostituirà Dalli per il portafoglio salute, conosciuto per essere anti-divorzio, anti-aborto ed anti-LGBTI.

L'Associazione Radicale Certi Diritti si felicita per l'iniziativa del Parlamento europeo di commissionare tale studio e di elaborare nel 2013 una relazione per rafforzare ulteriormente la lotta per una tabella di marcia europea per i diritti LGBTI e per spingere la Commissione a proporla, al fine di cancellare le discriminazioni basate sull'orientamento sessuale in Europa.  
  
**sotto in allegato il documento del Parlamento Europeo in PDF.**

  
[mappauguaglianza_pe462482.pdf](http://www.certidiritti.org/wp-content/uploads/2012/10/mappauguaglianza_pe462482.pdf)