---
title: 'CERTI DIRITTI CHIEDE ALL''UNIONE EUROPEA DI DIFENDERE I DIRITTI UMANI DELLE PERSONE LGBT NELLE RELAZIONI CON I PAESI AFRICANI, DEL PACIFICO E DEI CARAIBI (ACP)'
date: Wed, 10 Nov 2010 15:17:59 +0000
draft: false
tags: [Comunicati stampa]
---

Nel corso della 21esima sessione dell'Assemblea Parlamentare dei paesi Africani, del Pacifico e dei Caraibi (paesi ACP) che si é tenuta il 28 settembre 2010, é stata adottata una dichiarazione unilaterale sulla " coesistenza pacifica delle religioni e sull'importanza data al fenomeno dell'omosessualità nelle relazioni di partnership tra l'Unione europea ed i paesi ACP"

La dichiarazione, dopo avere condannato l'intolleranza religiosa, il risorgere di conflitti tra le diverse comunità religiose e le dichiarazioni politiche che incitano all'odio inter-religioso, affronta il "fenomeno dell'omosessualità" richiamando le "profonde divergenze che si sono create nel quadro della seconda revisione dell'Accordo di Cotonou".

L'assemblea ACP "rimane fermamente convinta che la partnership ACP-EU può funzionare sulla base del dovuto rispetto per le differenze culturali e diversità sociali delle due Parti", "appellandosi urgentemente all'UE perché si trattenga da qualunque tentativo si imporre i suoi valori che non sono liberamente condivisi nel quadro della partnership ACP-UE". 

Certi Diritti chiede all'Unione Europea di non sottostare ai diktat degli estensori di tale dichiarazione che, in violazione dei diritti umani fondamentali dei loro stessi cittadini, utilizzano strumentalmente pretesti di diversità culturale e sociale per negare il diritto alla vita, alla sicurezza, all'eguaglianza ed alla non discriminazione alle persone LGBT, nonostante questi siano previsti da tutte le convenzioni internazionali sui diritti umani, firmate e ratificate dagli stati ACP. Agli Stati UE ed ACP, Certi Diritti chiede anche di interrompere ogni persecuzione contro le persone LGBT e di indagare sull'operato dei rappresentanti religiosi, inclusi quelli cattolici ed evangelisti, che diffondono in Africa un messaggio radicale edestremista incitando la violenza, l'odio e la discriminazione contro le persone LGBT".

[Scarica il documento](http://www.certidiritti.org/wp-content/uploads/2010/11/Documento_ACP.pdf)