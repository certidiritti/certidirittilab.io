---
title: 'LETTERA SPEDITA ALLA SIGNORA SCHLESINGER'
date: Thu, 13 May 2010 12:35:54 +0000
draft: false
tags: [Comunicati stampa]
---

La dottoressa Laura Schlesinger e' una famosa giornalista della radio americana; nella sua trasmissione dispensa consigli alle persone che telefonano. Qualche tempo fa, Laura ha affermato che l'Omosessualità, secondo la Bibbia (Lev.18:22) e' un abominio, e non può essere tollerata in alcun caso.

**La seguente e' una lettera spedita alla signora Schlesinger.**

"Cara Dottoressa Schlesinger, le scrivo per ringraziarla del suo lavoro educativo sulle leggi del Signore. Ho imparato davvero molto dal suo programma, ed ho cercato di dividere tale conoscenza con piu' persone possibile. Adesso, quando qualcuno tenta di difendere lo stile di vita omosessuale, gli ricordo semplicemente che nel Levitico 18:22 si afferma che cio' e' un abominio. Fine della discussione. Però, avrei bisogno di alcun consigli da lei, a riguardo di altre leggi specifiche e come applicarle.

1\. Vorrei vendere mia figlia come schiava, come sancisce Esodo 21:7. Quale pensa sarebbe un buon prezzo di vendita?

2\. Quando do fuoco ad un toro sull'altare sacrificale, so dalle scritture che ciò produce un piacevole profumo per il Signore. (Lev. 1.9) Il problema e' con i miei vicini. I blasfemi sostengono che l'odore non e' piacevole per loro. Devo forse percuoterli?

3\. So che posso avere contatti con una donna quando non ha le mestruazioni. (Lev.15: 19-24.) Il problema e': come faccio a chiederle questa cosa? Molte donne s'offendono..

4\. Lev. 25:44 afferma che potrei possedere degli schiavi, sia maschi che femmine, a patto che essi siano acquistati in nazioni straniere. Un mio amico afferma che questo si puo' fare con i filippini, ma non con i francesi. Può farmi capire meglio? Perche' non posso possedere schiavi francesi?

5\. Un mio vicino insiste per lavorare di Sabato. Esodo 35:2 dice chiaramente che dovrebbe essere messo a morte. Sono moralmente obbligato ad ucciderlo personalmente?

6\. Un mio amico ha la sensazione che anche se mangiare crostacei e' un abominio (Lev. 11:10), lo e' meno dell'omosessualita'. Non sono d'accordo. Può illuminarci sulla questione?

7\. Lev. 21:20 afferma che non posso avvicinarmi all'altare di dio se ho difetti di vista. Devo effettivamente ammettere che uso occhiali per leggere.... La mia vista deve per forza essere 10 decimi o c'e' qualche scappatoia alla questione?

8\. Molti dei miei amici maschi usano rasarsi i capelli, compresi quelli vicino alle tempie, anche se questo e' espressamente vietato dalla Bibbia (Lev 19:27). In che modo devono esser messi a morte?

9\. In Lev 11:6-8 viene detto che toccare la pelle di maiale morto rende impuri. Per giocare a pallone debbo quindi indossare dei guanti?

10\. Mio zio possiede una fattoria. E' andato contro Lev. 19:19, poiché ha piantato due diversi tipi di ortaggi nello stesso campo; anche sua moglie ha violato lo stesso passo, perche' usa indossare vesti di due tipi diversi di tessuto (cotone/acrilico). Non solo: mio zio bestemmia a tutto andare. E' proprio necessario che mi prenda la briga di radunare tutti gli abitanti della citta' per lapidarli come prescrivono le scritture? Non potrei, piu' semplicemente, dargli fuoco mentre dormono, come simpaticamente consiglia Lev 20:14 per le persone che giacciono con consanguinei?

So che Lei ha studiato approfonditamente questi argomenti, per cui sono sicuro che potrà rispondermi a queste semplici domande. Nell'occasione, la ringrazio ancora per ricordare a tutti noi che la parola di Dio e' eterna e immutabile. Sempre suo ammiratore devoto."