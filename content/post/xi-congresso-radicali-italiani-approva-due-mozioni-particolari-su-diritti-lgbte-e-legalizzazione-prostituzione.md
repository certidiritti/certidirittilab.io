---
title: 'XI Congresso Radicali Italiani approva due mozioni particolari su diritti lgbte e legalizzazione prostituzione'
date: Fri, 09 Nov 2012 13:30:34 +0000
draft: false
tags: [Politica]
---

Anche quest'anno il congresso di Radicali Italiani, che si è svolto a Roma dal 1 al 4 novembre, ha approvato quasi all'unanimità due mozioni particolari presentate dall'associazione radicale Certi Diritti.  
  
Roma, 4 novembre 2011  

Segue il testo delle due mozioni

  
**Mozione per la decriminalizzazione/legalizzazione della prostituzione**

L'XI Congresso di Radicali italiani riunito a Roma dal 1 al 4 novembre 2012

Premesso che  
In molti comuni italiani sono state fatte in questi ultimi anni ordinanze antiprostituzione ispirate a demagogia, populismo e visioni proibizioniste che hanno prodotto risultati deleteri sul piano del diritto e contro le lavoratrici e i lavoratori del sesso;

In Italia la prostituzione è costretta in un limbo senza regole da normative e ipocrisie che hanno aumentato violenza, maltrattamenti e discriminazioni contro prostitute e prostituti, frenando allo stesso tempo la lotta contro lo sfruttamento e contro la tratta;

Le politiche proibizioniste incrementano l'economia illegale, alimentando le organizzazioni criminali e gli sfruttatori che sulla vita di chi si prostituisce creano potere e guadagni illeciti con dispendio di imponenti risorse economiche a danno di tutta la società, esponendo le persone che si prostituiscono e i loro clienti a una denigrazione e a una criminalizzazione di fatto, contrarie alle leggi, alla cultura, ai principi ormai ampiamente condivisi dalle Istituzioni internazionali e comunitarie;

I pareri dell'UNAIDS, dell'Organizzazione Mondiale della Sanità e di diversi organismi istituzionali europei, hanno fatto emergere che le norme repressive limitano i diritti delle/dei sex workers, ne aumentano la vulnerabilità alla violenza e impediscono l'accesso alle cure e al welfare, ne negano la dignità di persone;

Tenuto conto del fallimento delle politiche proibizioniste derivato da leggi nazionali e da provvedimenti amministrativi locali nel tempo hanno prodotto:

    l'aumento della criminalizzazione e dello stigma contro sex worker e i loro clienti e l'incremento della violenza fisica e di altre forme di sfruttamento contro le/i sex workers;  
    la violazione dei diritti fondamentali della persona;  
    l'incremento di ignoranza e disinformazione riguardo i temi della sessualità;  
    la mancanza di politiche di responsabilizzazione delle persone riguardo la prevenzione delle malattie sessualmente trasmissibili;

Considerato che l'immissione di denaro nei canali dell'illegalità determina un'inevitabile connessione con le altre tipologie di mercati illegali come la tratta delle persone, il commercio della droga e delle armi, il riciclo del denaro sporco e la diffusione di bande criminali;

Impegna Radicali italiani a promuovere e sostenere proposte legislative ed eventuali iniziative referendarie, finalizzate alla decriminalizzazione/legalizzazione della prostituzione che tengano conto delle richieste delle e dei sex workers a partire dal Manifesto di Bruxelles del 2005, dal manifesto promosso dall'Associazione Radicale Certi Diritti elaborato durante la conferenza nazionale sulla prostituzione svoltosi nell'aprile 2012 a Roma e da quanto espressamnte richiesto dal Comitato Internazionale per i Diritti  dei/delle Sex Workers in Europa e dal Comitato per i diritti civili delle prostitute Onlus e dal Movimento Identità Transessuale e dalle esperienze legislative già esistenti in diversi stati europei.

Prima firma: Maria Gigliola Toniollo  
  
  
  
**Mozione particolare per una nuova stagione di libertà civili per l'eguaglianza contro le discriminazioni**

L'XI Congresso di Radicali Italiani,

    considerata l’urgente necessità di superare attraverso una nuova stagione di libertà civili la gravissima situazione di blocco, se non di degrado progressivo, della situazione delle libertà individuali e dei diritti fondamentali in Italia, come pure nell'Unione europea ed all'ONU;

    considerato che in Italia continuano ad avere luogo gravissime discriminazioni basate sull’identità di genere, l'origine etnica, la religione o le convinzioni personali, la disabilità, l'età e l'orientamento sessuale, in violazione del principio di eguaglianza, nella totale assenza di qualunque iniziativa e modifica legislative per assicurare diritti, libertà e dignità;

    considerato che all’Unione europea, a causa dei veti degli Stati membri, è impedita persino l'attuazione della lettera e dello spirito del Trattato di Lisbona e della Carta del Diritti fondamentali, che prevedono tra l'altro l'adesione dell'UE alla Convenzione europea dei Diritti umani, la lotta alle discriminazioni attraverso l'adozione di una direttiva orizzontale al riguardo, il rafforzamento dell’Agenzia dei diritti fondamentali dell’UE, la libera circolazione dei cittadini europei ed il mutuo riconoscimento dello status civile delle persone e delle loro famiglie;  
    considerato che alle Nazioni Unite si rafforzano le iniziative sponsorizzate congiuntamente dai regimi totalitari e dittatoriali, dagli stati fondamentalisti e dal Vaticano per subordinare ai cosiddetti “valori e culture tradizionali" sui diritti umani e le libertà fondamentali, colpendo così al cuore i diritti fondamentali stessi, ed in particolare i diritti delle donne sia in materia di eguaglianza che di salute sessuale e riproduttiva, che quelli delle persone omosessuali.

Impegna Radicali Italiani a rafforzare e rilanciare la lotta in Italia, in Europa e a livello internazionale per assicurare la promozione e l'attuazione del principio di eguaglianza contro tutte le discriminazioni:

    Sollecitando l'attuazione della lettera e dello spirito del Trattato di Lisbona e della Carta dei Diritti fondamentali attraverso l’adesione dell’UE alla Convenzione europea dei diritti umani, l'approvazione della Direttiva Europea contro le discriminazioni e la sua attuazione in Italia, la libera circolazione ed il mutuo riconoscimento dello status civile delle persone nell’UE

    rafforzando, assieme all’Associazione Radicale Certi Diritti, non solo la lotta ad ogni livello contro l'omofobia e per l'approvazione delle unioni civili, ma anche l’accesso al matrimonio civile alle coppie formate da persone dello stesso sesso, mettendo quest’obiettivo al centro del dibattito politico;

    Rafforzando assieme al Partito Radicale Nonviolento, Transnazionale e Transpartito, la lotta per assicurare che gli organismi internazionali per la difesa dei diritti fondamentali individuali rafforzino le loro iniziative ed azioni contro le violazioni di tali diritti attuate dagli Stati e dalle loro autorità, nonché contrastando i tentativi di subordinare i diritti umani ai cosiddetti “valori tradizionali”, e chiedendo alla Commissione europea di assumere con Coraggio le proprie responsabilità nel garantire, proteggere e promuovere i diritti fondamentali nell’UE.

Prima firma: Ottavio Marzocchi