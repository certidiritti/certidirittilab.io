---
title: 'E'' uscita l''edizione 2012 della mappa ILGA sui diritti LGBTI nel mondo. Scarica la versione italiana curata dall''associazione radicale Certi Diritti >'
date: Thu, 21 Jun 2012 13:25:37 +0000
draft: false
tags: [Transnazionale]
---

Anche quest'anno ILGA ha prodotto la mappa mondiale dei diritti LGBTI come importante strumento volto ad accrescere la consapevolezza sulla situazione delle persone LGBTI nel mondo.

La mappa è accompagnata dal rapporto annuale sull'omofobia istituzionale nel mondo. Quest'anno, l'Associazione Radicale Certi Diritti ha curato la versione italiana della mappa, per garantirne la piena fruizione anche nel nostro paese.

www.ilga.org

**sotto in allegato la versione scaricabile.**

  
[ILGA\_WorldMap\_Certi_Diritti.pdf](http://www.certidiritti.org/wp-content/uploads/2012/06/ILGA_WorldMap_Certi_Diritti.pdf)