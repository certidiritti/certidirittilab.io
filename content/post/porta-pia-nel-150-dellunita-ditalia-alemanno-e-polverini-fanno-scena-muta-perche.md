---
title: 'Porta Pia: nel 150° dell''Unità d''Italia Alemanno e Polverini fanno scena muta. Perchè?'
date: Tue, 20 Sep 2011 11:26:48 +0000
draft: false
tags: [Politica]
---

La vera commemorazione di Porta Pia oggi 20 settembre dalle ore 16.30 sul luogo della breccia in Corso d'Italia con decine di associazioni, movimenti e personalità.

Comunicato stampa di Sergio Rovasio, Segretario dell’Associazione Radicale Certi Diritti

Roma, 20 Settembre 2011

“Ormai, ad ogni anniversario di Porta Pia, che si celebra il 20 Settembre, siamo abituati ad assistere ad ogni tipo di cerimonia da parte delle istituzioni che, alle ore 10, di ogni anno, si riservano il luogo della Breccia tutto per loro: dal deposito di corone d’alloro, fatto per molti anni senza nemmeno la presenza di autorità, a quanto avvenne due anni fa, allorchè l’inviato del Sindaco, anziché ricordare i bersaglieri caduti, lesse i nomi degli zuavi pontifici.

Lo scorso anno, poi, fu addirittura chiamato il Cardinale Bertone, Segretario di Stato Vaticano,  nel tentativo di nascondere le  malefatte dei mercenari zuavi pontifici il XX Settembre 1870 contro l’esercito italiano che entrava a Roma uccidendo 49 bersaglieri.

Anche questa mattina, alle ore 10, le autorità non hanno mancato di stupire il pubblico, ben contento di vedere insieme, finalmente, sul luogo della Breccia, il Sindaco di Roma, Gianni Alemanno, la Presidente della Regione, Renata Polverini e la rappresentante della Provincia di Roma. Dopo aver depositato le corone, però, hanno fatto scena muta,  silenzio totale! Qualcuno ha chiesto il perché e la Presidente della Regione Lazio ha risposto: ‘non era previsto’; ecco, non era previsto, e quindi silenzio assoluto, nel 150° anniversario dell’Unità d’Italia.

La vera commemorazione la faranno i Radicali e le decine di Associazioni, movimenti, partiti, personalità, parlamentari, oggi pomeriggio a partire dalle ore 16,30 sul luogo della Breccia (Corso d’Italia), oggi pomeriggio,  a partire dalle ore 16,30 per ricordare la Liberazione di Roma dal potere temporale dello Stato teocratico pontificio.  Per la laicità e la libertà, contro ogni forma di  fondamentalismo ideologico e religioso.

Hanno finora aderito alla manifestazione:

Radicali Italiani;

Associazione Radicale Certi Diritti;

Moderatori della pagina Facebook ‘Vaticano paga tu la manovra’;

Partito Socialista Italiano – Federazione di Roma;

Gruppo consiliare Lista Bonino Pannella, Federalisti Europei - Regione Lazio;

Radicali Roma;

Anticlericale.net;

Federazione dei Giovani Socialisti italiani:

Forum delle donne Socialiste;

Associazione Nazionale del Libero Pensiero "Giordano Bruno";

Associazione Luca Coscioni;

Nessuno Tocchi Caino;

Associazione Italialaica.it;

Radio Radicale;

www.gay.it;

Associazione democratica Giuditta Tavani Arquati;

Uaar – Roma;

Lega italiana per il Divorzio Breve;

Gaycs – Aics  Coordinamento Lgbt;

Associazione Famiglie Arcobaleno;

Movimento Identità Transessuale;

Libera Uscita, Associazione laica e apartitica;

Sinistra Ecolologia e Libertà – Lazio;

Sinistra Ecologia Libertà – Area metropolitana Roma;

Arcigay;

Fondazione Religionfree;

CRIDES – Centro romano d’iniziativa per la difesa dei diritti nella Scuola;

Fondazione Massimo Consoli;

Maria Gigliola Toniollo – Reponsabile Cgil Nuovi Diritti

Axteismo -  No alla chiesa, no alle religioni - Movimento Internazionale di Libero Pensiero

Democrazia Laica

Circolo Mario Mieli di Roma;

Rete Genitori Rainbow;

Associazione Liberacittadinanza- Rete dei girotondi e movimenti;

Consulta Romana per la Laicità delle istituzioni;

Cristiana Alicata – Pd Roma;

Luca Valeriani – esponente della comunità lgbt;