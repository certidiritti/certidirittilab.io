---
title: 'BILANCIO 2008'
date: Mon, 11 Aug 2008 10:10:15 +0000
draft: false
tags: [Politica]
---

Quote Associative

€ 5.780,00

**ENTRATE**

**€ 5.780,00**

  

  

Spese per il congresso costitutivO

€ 607,00

Spese per il carro del Pride di Bologna

€ 550,00

Spese di cartoleria/volantini/pubblicazioni

€ 789,00

Spese per il Pride di Roma

€ 477,30

Rimborsi trasferte/spostamenti/hotel

€ 433,00

Spese di spedizione postale

€ 71,40

Stampa tessere 2008

€ 165,00

Spese per manifestazioni/eventi vari

€ 456,00

**USCITE**

**€ 3.548,40**

  

  

  

  

**ECCEDENZE DI BILANCIO**

**€ 2.231,60**