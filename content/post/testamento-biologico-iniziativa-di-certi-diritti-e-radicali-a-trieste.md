---
title: 'TESTAMENTO BIOLOGICO: INIZIATIVA DI CERTI DIRITTI E RADICALI A TRIESTE'
date: Sat, 20 Dec 2008 11:28:33 +0000
draft: false
tags: [Comunicati stampa]
---

**Testamento Biologico - Conferenza stampa a Trieste**

**s**abato 20 dicembre 2008 in piazza Cavana a Trieste alle ore 16,00 conferenza stampa sulle questioni del testamento biologico.

Saranno presenti:

**Clara Comelli** Presidente dell'associazione radicale "Certi Diritti", che presenterà la petizione sul testamento biologico, 

**Maria Grazia Cogliati** (PD) consigliere comunale, che illustrerà le iniziative per istituire al comune di Trieste di un registro pubblico  dei testamenti biologici che i cittadini vorranno depositare,

**Andrea Bellavite**, prete, giornalista e consigliere comunale a Gorizia per il Forum, che spiegherà  il senso della lettera "Natale 2008: nella complessità con ragionevole speranza e rinnovato impegno", pubblicata oggi sulla stampa regionale.

Contemporaneamente, al tavolo, si distribuiranno e  raccoglieranno testamenti biologici (sulla base di un formulario preparato  con l'associazione "A buon diritto" di Luigi Manconi), si registreranno testimonianze audiovideo da mettere su Internet, si raccoglieranno  firme sulla petizione al Parlamento.

  

Marco Gentili