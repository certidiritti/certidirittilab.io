---
title: 'Conferenza dei paesi islamici all''Onu scrive a Consiglio Diritti Umani dell''Onu: no ai diritti Lgbt'
date: Mon, 20 Feb 2012 16:33:27 +0000
draft: false
tags: [Transnazionale]
---

Allarme lanciato da UN Wacht quasi del tutto ignorato.

Roma, 20 febbraio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

\* nella foto opera tratta da Adam and Ewald by Sooreh Hera

Zamir Akram, il Coordinatore dell’Oic, l’organismo che raggruppa i paesi islamici alle Nazioni Unite lo scorso 14 febbraio ha scritto una lettera all’Onu nella quale, tra l’altro, si legge che "Gli Stati dell'Organizzazione della Conferenza Islamica (OIC) sono profondamente preoccupati per l'introduzione nel Consiglio per i Diritti Umani dell'ONU di nozioni controverse come l'orientamento sessuale e l'identità di genere". Lo ha reso noto UN Watch una importante Ong di promozione dei Diritti Umani che svolge attività di monitoraggio sull’attività delle Nazioni Unite.

La lettera è stata scritta dopo l’approvazione di una Risoluzione in difesa dei diritti Lgbti(e)  da parte del Consiglio per i Diritti Umani dell’Onu durante la sua ultima Sessione svoltasi a Ginevra e in vista di un importante incontro sul tema previsto il prossimo 7 marzo.

Secondo quanto scritto nella lettera dell’Oic “ I Diritti Umani non sarebbero per nulla universali, ma il loro rispetto dipenderebbe dalle "peculiarità nazionali e regionali e dai diversi contesti storici, culturali e religiosi".  Insomma, una evidente contraddizione risolvibile solo proclamando o la fine di tutti i diritti umani o la "non-umanità" degli omosessuali e delle persone transessuali. La lettera dell'OIC rimane ambigua e cerca di confondere le acque: "Siamo ancor più preoccupati dal tentativo di concentrarsi su determinate persone in base al loro comportamento sessuale anormale, mentre non ci si concentra su esempi evidenti di intolleranza e discriminazione, in diverse parti del mondo, basati su colore, razza, genere o religione, solo per citare alcune cause".

L’Associazione Radicale Certi Diritti ringrazia l’Ong UN Watch e, in Italia, il Blogger il grande colibrì [http://www.ilgrandecolibri.com](http://www.ilgrandecolibri.com)  per aver fatto conoscere questa lettera e si augura che i paesi islamici democratici non aderiscano a questa preoccupante iniziativa che cerca di limitare l’azione della Consiglio per i Diritti Umani delle Nazioni Unite.  
  

**iscriviti alla newsletter >[  
](newsletter/newsletter)[http://www.certidiritti.it/newsletter/newsletter](newsletter/newsletter)**