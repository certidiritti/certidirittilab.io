---
title: 'LA VICENDA VATICANO OMOFOBO VERRA'' DISCUSSA AL PARLAMENTO EUROPEO'
date: Wed, 10 Dec 2008 20:15:11 +0000
draft: false
tags: [Comunicati stampa]
---

PARLAMENTO EUROPEO DISCUTERA IN SESSIONE PLENARIA DI STRASBURGO DELLA PROSSIMA SETTIMANA L'INIZIATIVA FRANCESE SULLA DECRIMINALIZZAZIONE UNIVERSALE DELL'OMOSESSUALITA E DELLA POSIZIONE DEL VATICANO  
   
Dichiarazione di Marco CAPPATO, Eurodeputato Radicale:  
  "Il PE, su iniziativa dei Radicali al PE e del gruppo Liberale e Democratico (ALDE), ha deciso statera di inserire nell'agenda della sessione parlamentare di Strasburgo della prossima settimana una dichiarazione della Commissione europea sulla depenalizzazione universale dell'omosessualità, sulla base dell'interrogazione depositata dal gruppo ALDE al riguardo.  
   
La Commissione sarà chiamata anche a prendere posizione sulle dichiarazioni del Vaticano, come richiesto nelle interrogazioni depositate al Parlamento europeo.  
   
La decisione della maggioranza dei gruppi politici del PE di portare la questione in plenaria al fine di assicurare maggiore forza all'iniziativa francese all'ONU sulla depenalizzazione universale dell'omosessualità é la migliore risposta alle dichiarazioni odierne di Bertone sulla posizione del Vaticano sui diritti umani, della quale troviamo un chiaro esempio nell'alleanza del Vaticano con Stati che violano i diritti umani costantemente"  
   
Testo dell'interrogazione alla Commissione  
di Marco Cappato, Sophie In't Veld, Andrew Duff, Olle Schmidt, a nome del gruppo ALDE

  
Iniziativa francese all'ONU sulla depenalizzazione dell'omosessualità  
La Francia ha lanciato un'iniziativa in sede ONU sulla depenalizzazione universale dell'omosessualità, raccogliendo l'adesione di più di 50 Stati tra cui tutti gli Stati Membri dell'UE, volta al deposito di una dichiarazione comune all'Assemblea Generale. La dichiarazione afferma che le violazioni, persecuzioni, torture, trattamenti inumani, crudeli e degradanti, arresti e detenzioni arbitrarie, esecuzioni e discriminazioni compiute sulla base dell'orientamento sessuale e dell'identità di genere sono una violazione dei diritti umani e chiedendo misure legislative o amministrative per assicurare che orientamento sessuale e identità di genere non siano alla base di sanzioni criminali, in particolare esecuzioni, arresti e detenzioni. Rispetto a tale dichiarazione, il Vaticano ha espresso la sua opposizione attraverso l'osservatore permanente della Santa Sede presso le Nazioni Unite, monsignor Celestino Migliore, il quale ha affermato che "con una dichiarazione di valore politico, sottoscritta da un gruppo di paesi, si chiede agli Stati ed ai meccanismi internazionali di attuazione e controllo dei diritti umani di aggiungere nuove categorie protette dalla discriminazione, senza tener conto che, se adottate, esse creeranno nuove e implacabili discriminazioni...Per esempio gli Stati che non riconoscono l'unione tra persone dello stesso sesso come "matrimonio" verranno messi alla gogna e fatti oggetto di pressioni".  
Quali iniziative la Commissione europea ha intrapreso o intende intraprendere al fine di assicurare che la dichiarazione francese ottenga il più ampio sostegno da parte degli Stati all'ONU? Con quali Stati extra-europei ha preso contatto al riguardo? Cosa intende fare la Commissione per scongiurare l'azione diplomatica del Vaticano e dei 91 Stati totalitari o integralisti che nel mondo prevedono sanzioni, torture, pene e persino l'esecuzione capitale (in 10 paesi islamici) contro le persone omosessuali? Non ritiene la Commissione che sia urgente sollevare tale questione nell'ambito delle relazioni internazionali e del cosiddetto dialogo interculturale - interreligioso che l'UE mantiene con il Vaticano e con i "rappresentanti" della religione cattolica ed adottare le misure a tal riguardo come accadrebbe con Stati terzi?