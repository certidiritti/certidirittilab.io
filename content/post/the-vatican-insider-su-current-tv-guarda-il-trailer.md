---
title: '''The Vatican Insider'' su Current TV. Guarda il trailer >'
date: Tue, 12 Jul 2011 18:00:00 +0000
draft: false
tags: [Politica]
---

**L'ultima produzione targata Current Italia prima dell'imminente chiusura del canale è una serie di docu-film dal respiro internazionale che si propongono di raccontare la realtà della Chiesa cattolica in maniera esaustiva, chiara, indipendente. **Un viaggio senza precedenti tra i misteri dello Stato più piccolo e segreto del mondo.****

**Il primo episodio 'IL DENARO IN NOME DI DIO' andrà in onda giovedì 14 luglio 2011 alle ore 21.00 su Sky 130.  
**

Il canale italiano dell'Informazione Indipendente, a ridosso dell'imminente chiusura del 31 luglio, si congeda dal suo pubblico con l'anteprima di una produzione interna targata Current Italia: The Vatican Insider (2011).  
Si tratta di una serie di docu-film dal respiro internazionale che si propongono di raccontare, attraverso una grammatica più cinematografica, la realtà della Chiesa cattolica in maniera esaustiva, chiara, indipendente. Senza pregiudizi e forzature.

Con IL DENARO IN NOME DI DIO, il 14 luglio - a circa 10 giorni dall'avvenuta pubblicazione del Bilancio Consuntivo Consolidato della Santa Sede - alle ore 21.00, Current programma sul canale 130 Sky il primo episodio di The Vatican Insider. I successivi avranno seguito su un'eventuale futura base di emittenza. Un modo per dare appuntamento, nelle modalità che saranno comunicate a tempo debito, a tutti gli affezionati del canale e ai tanti che fin qui hanno sostenuto e reso possibile il progetto di Current.

La serie The Vatican Insider è scritta dalla giornalista Silvia Luzi con la regia di Luca Bellino (autori del controverso documentario su Chavez bloccato dalla Rai La Minaccia) e sviluppata grazie al contributo di Andrea Cairola, regista di Citizen Berlusconi. La colonna sonora originale di The Vatican Insider è composta, orchestrata e diretta da Andrea Morricone, figlio del Premio Oscar Ennio Morricone.

Il trailer: