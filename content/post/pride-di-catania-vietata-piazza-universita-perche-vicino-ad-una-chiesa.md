---
title: 'PRIDE DI CATANIA: VIETATA PIAZZA UNIVERSITA'' PERCHE'' VICINO AD UNA CHIESA'
date: Sun, 29 Jun 2008 23:22:08 +0000
draft: false
tags: [Comunicati stampa]
---

GAY PRIDE DI CATANIA DEL 4 LUGLIO: ANCHE A CATANIA, COSI’ COME A BIELLA E A ROMA LE AUTORITA’ VIETANO UNA PIAZZA PERCHE’ TROPPO VICINA A UNA CHIESA. L’ENNESIMO ESEMPIO DI ASSERVIMENTO AL POTERE CLERICALE

  

===

Dichiarazione di Sergio Rovasio, Segretario dell’Associazione Radicale Certi Diritti
====================================================================================

Così come è avvenuto a Roma e a Biella il 7 giugno scorso, anche a Catania le autorità, per compiacere il potere clericale, hanno vietato l’accesso a Piazza dell’Università come luogo di arrivo del Gay Pride che si svolgerà il prossimo 4 luglio. E’ la prima volta che ciò accade, perché da anni il corteo del Gay Pride passa o raggiunge tale piazza a Catania. Il divieto sarebbe motivato con il fatto che la piazza sarebbe troppo vicina al Duomo. Si tratta dell’ennesimo esempio di asservimento della classe politica al potere clericale cittadino in palese violazione di garanzie costituzionali e di norme amministrative. L’Associazione Radicale Certi diritti insieme con i dirigenti e parlamentari Radicali si mobiliterà, così come è già avvenuto il 7 giugno scorso in Piazza San Giovanni a Roma, se il divieto verrà confermato, il 4 luglio ci imbavaglieremo anche di fronte al Duomo di Catania.