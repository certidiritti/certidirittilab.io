---
title: 'Onu. Sventato nuovo attacco ad Esperto indipendente sui diritti LGBTI'
date: Tue, 20 Dec 2016 11:04:07 +0000
draft: false
tags: [Transnazionale]
---

![onu_05](http://www.certidiritti.org/wp-content/uploads/2016/12/onu_05-300x197.jpg)Dichiarazione di Leonardo Monaco, segretario dell'Associazione Radicale Certi Diritti "Ieri, nel corso della presentazione dei report della terza Commissione all'Assemblea Generale Onu, abbiamo assistito all'ennesimo attacco nei confronti della figura dell'Esperto Indipendente Onu. L'emendamento che avrebbe compromesso l'operato di Vitit Muntarbhorn è stato respinto con 84 voti contrari, 77 favorevoli e 16 astenuti. Dietro l'attacco ci sarebbero nuovamente alcuni paesi Africani, in compagnia di Russia e altre potenze campionesse nella violazione sistematica dei diritti umani. Sono una piacevole conferma alcune astensioni e non partecipazioni al voto, alcune delle quali da parte di Paesi che stanno affrontando lenti percorsi verso la decriminalizzazione dell'orientamento sessuale e dell'identità di genere. Sta adesso alla società civile, all'Italia e ai suoi partner di politica estera ripartire da loro attraverso il dialogo per invertire il trend delle omofobie di stato e consolidare il fronte transnazionale per la difesa delle libertà"

**Il voto per Paese** ![screen-shot-2016-12-19-at-12-50-42-pm](http://www.certidiritti.org/wp-content/uploads/2016/12/Screen-Shot-2016-12-19-at-12.50.42-PM.png)