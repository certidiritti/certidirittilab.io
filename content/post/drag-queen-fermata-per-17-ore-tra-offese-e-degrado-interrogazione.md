---
title: 'DRAG QUEEN FERMATA PER 17 ORE TRA OFFESE E DEGRADO: INTERROGAZIONE'
date: Fri, 24 Jul 2009 10:31:15 +0000
draft: false
tags: [Comunicati stampa]
---

#### DRAG QUEEN RINCHIUSA IN CELLA PER 17 ORE A SASSARI, TRA OFFESE, INGIURIE E DEGRADO, POI CACCIATA CON FOGLIO DI VIA. INTERROGAZIONE DEI DEPUTATI RADICALI – PD.

Roma, 24 luglio 2009

**“La scorsa settimana un ragazzo incensurato, che fa spettacoli in Drag Queen, è stato fermato a Sassari e rinchiuso in una cella della Questura per 17 ore. Il tutto è avvenuto tra offese, ingiurie e degrado. Dopo 17 ore di fermo gli è stato dato un ‘foglio di via’. Di seguito il testo dell’interrogazione parlamentare depositata oggi dai deputati radicali – pd, prima firmataria Rita Bernardini**

**Al Ministro delgi Interni; Al Ministro della Giustizia; Al Ministro per le Pari Opportunità**

Per sapere – premesso che:

la notte tra mercoledì 15 e giovedì 16 luglio 2009, il signor Michele Cicogna di Prato, persona che svolge spettacoli in alcuni locali come ‘Drag Queen’, è stato fermato dalla Polizia in una zona periferica della città di Sassari mentre si trovava all’interno della sua auto in attesa di un amico che abita poco distante;

Michele Cicogna, che doveva recarsi in alcuni locali per concordare i dettagli di alcune presenze relative alla sua attività artisitica, era truccato e vestito con abiti femminili; gli agenti di Polizia che stavano svolgendo un’azione finalizzata al contrasto del fenomeno dello sfruttamento della prostituzione e dell’immigrazine clandestina hanno invitato il Signor Cicogna a seguirli in Questura per alcuni minuti;

In Questura al Signor Cicogna è stato tolto il telefonino, gli sono state prese le impronte digitali, è stato rinchiuso in una cella insieme ad alcune persone straniere, presumibilmente fermate per prostituzione;  nella cella v’erano strutture in cemento, senza brandine con l’aria irrespirabile per un bagno confinante da un separè, utilizzato da tutte le persone rinchiuse nella cella;

per tutta la durata del fermo il Signor Cicogna, sarebbe stato ripetutamente offeso con frasi e battute offensive sulla sua condizione; alla sua richiesta di poter avere dell’acqua gli sarebbe stato risposto che non si trovava in un agriturismo; solo alle ore 7 del mattino, una cittadina straniera gli avrebbe dato un euro per poter acquistare una bottiglietta di acqua presso la macchinetta distributrice;

Quando alle ore 16 circa è giunto negli uffici il Questore gli è stato consegnato un ‘foglio di via’, con sopra scritto che il Signor Cicogna per tre anni non potrà entrare nel Comune di Sassari; alle ore 18, dopo 17 ore di fermo, il Signor Cicogna ha potuto lasciare gli uffici della Questura;

Per sapere:

-  in base a quali elementi investigativi e di pericolosità il Signor Cicogna è stato fermato e trattenuto per circa 17 ore in Questura e rinchiuso in cella insieme a persone straniere;

-  se il fenomeno delle offese e delle denigrazioni nei confronti delle persone fermate, indipendentemente dall’orientamento e condizione sessuale, è diffuso e nel caso se tale fenomeno è adeguatamente contrastato anche tra coloro che operano nell’ambito delle iniziative di polizia di contrasto dell’illegalità e della lotta allo sfruttamento della prostituzione;

-  quale nesso ravvisano i Ministri riguardo l’episodio descritto in premessa e quanto scritto nel Foglio di Via redatto dal Questore di Sassari nei confronti del Signor Michele Cicogna relativamente all’iniziativa finalizzata ‘al contrasto del fenomeno dello sfruttamento della prostituzione e dell’immigrazione clandestina’;

-  se non ritenga il Governo che questo fenomeno si inquadri in uno dei tanti episodi di omofobia che avvengono sempre più nel nostro paese;

Rita Bernardini

Marco Beltrandi

Maria Antonietta Farina Coscioni

Matteo Mecacci

Maurizio Turco

Elisabetta Zamparutti