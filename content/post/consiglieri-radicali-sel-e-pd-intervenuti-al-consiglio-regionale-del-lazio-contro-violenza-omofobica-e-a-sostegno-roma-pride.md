---
title: 'Consiglieri radicali, Sel e Pd intervenuti al consiglio regionale del Lazio contro violenza omofobica e a sostegno Roma Pride'
date: Thu, 21 Jun 2012 09:36:05 +0000
draft: false
tags: [Politica]
---

Chiesta la calendarizzazione delle proposte di legge contro l'omofobia depositate da quasi due anni alla regione Lazio.

Roma, 20 giugno 2012

Questa mattina, nel corso della seduta del Consiglio Regionale del Lazio i Capigruppo dei Radicali Giuseppe Rossodivita, di Sel Luigi Nieri e il Consigliere del Pd Enzo Foschi sono intervenuti in aula per condannare gli atti di violenza subiti nei  giorni scorsi da Guido Allegrezza, militante Lgbt di Roma e altre persone omosessuali. Durante gli interventi in aula i Consiglieri hanno ricordato che la città di Roma ha il più alto tasso di atti di violenza omofobica in Italia. I Capigruppo di Sel e dei Radicali hanno chiesto che vengano calendarizzate quanto prima le proposte di legge depositate quasi due anni fa che hanno l’obiettivo di prevenire ogni forma di violenza con apposite campagne di informazione e di aiuto alle persone Lgbt della Regione Lazio, così come già avviene nella Regione Liguria che ha approvato  un’ottima legge su questo tema.

I Gruppi della Lista Bonino Pannella, Federalisti Europei e di Sel al Consiglio Regionale del Lazio hanno aderito alla manifestazione contro l'omofobia prevista venerdì sera dalle ore 19 in Piazza Farnese a Roma e al Gay Pride che sfilerà per le strade di Roma sabato 23 giugno.