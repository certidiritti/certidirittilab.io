---
title: 'POLIZIOTTA LESBICA SOSPESA  DA QUESTORE: INTERROGAZIONE DEI RADICALI'
date: Fri, 03 Jul 2009 10:33:19 +0000
draft: false
tags: [Comunicati stampa]
---

**POLIZIOTTA LESBICA SOSPESA DAL SERVIZIO PERCHE’ PERICOLOSA, AVEVA DENUNCIATO VESSAZIONI E CONFERMATO LA SUA FELICITA’ PER LA SUA CONDIZIONE.**

**INTERROGAZIONE AL GOVERNO DEI PARLAMENTARI RADICALI.**

I parlamentari radicali, al Senato primo firmatario Marco Perduca, alla Camera prima firmataria  Elisabetta Zamparutti, hanno oggi depositato un’interrogazione urgente a risposta scritta nella quale si chiede conto al Governo sull’operato del Questore di Rovigo che ha inviato alla poliziotta Luana Zanaga una lettera di contestazione e dove viene definita pericolsa per aver dichiarato la sua omosessualità.

L’Italia è ormai uno degli ultimi paesi in Europa a discriminare le persone omosessuali all’interno delle Forze dell’ordine, a differenza di quanto avviene in molti paesi democratici dove gli stessi sfilano ufficialmente ai gay pride nazionali.

Di seguito il testo dell’interrogazione parlamentare.

**Interrogazione a risposta scritta**

**Al Presidente del Consiglio;**

**Al Ministro degli Interni;**

**Al Ministro per le Pari Opportunità**

-  Luana Zanaga, poliziotta di 39 anni di Rovigo, nell’ottobre 2008, ha dichiarato pubblicamente di essere omosessuale;

-  a seguito del ‘coming out’ Luana Zanaga è stata convocata da una commissione di disciplina che ha stabilito una punizione con una sospensione dalle sue mansioni di poliziotta “fino a sei mesi”;

-  di ritorno dal Gay Pride nazionale di Genova, svoltosi sabato 27 giugno, Luana Zanaga ha trovato una lettera del Questore di Rovigo dove sembra evidente la decisione di destituirla dal servizio;

-  Il Questore Savina nega che nel provvedimento contro la poliziotta si parli di licenziamento ma precisa che, con questo atto formale, si è avviata la pratica che le contesta gli addebiti; tra gli altri, quello che la indica come pericolosa per avere, nel maggio scorso, fatto dichiarazioni al settimanale L’Espresso nell’articolo ‘Agenge gay a rapporto’;

-  il funzionario incaricato di seguire il caso per conto del Questore parla invece apertamente di destituzione dall’incarico;

-  Secondo quanto riportato dal Settimanale L’Espresso in edicola venerdì 3 luglio, nell’articolo l’agente gay diceva di vivere in un ambiente omofobico, di aver subito il mobbing e di essere stata sottoposta a vessazioni. Come successe anni fa, quando la costrinsero ad andare dal medico per attestarne l’idoneità visto che era omosessuale. «Mi chiedevano se stavo bene con la mia omosessualità e io rispondevo che stavo benissimo», accennava nell’articolo. Per il capo della Questura di Padova queste accuse sono fortemente denigratorie e portano discredito alla Polizia. Nessun cenno invece, nella lettera, agli altri giornali, riviste e tv che hanno riferito della poliziotta. O alla solidarietà  manifestatale dal presidente della Camera, Gianfranco Fini. Pubblicazioni uscite nello stesso periodo e anche successivamente.

-  Peraltro nello stesso articolo ci si interroga anche del perché nessun provvedimento disciplinare è stato preso nei confronti dei due colleghi poliziotti che scrissero a Laura Zanaga che doveva ‘bruciare in un lager’;

Per sapere:

-  se le informazioni su riportate corrispondono al vero;

-  quali iniziative il Governo intende prendere affinchè fatti così gravi ed evidentemente discriminatori non abbiano più a ripetersi;

-  se il Governo è a conoscenza delle Direttive europee e delle Raccomandazioni del Parlamento Europeo contro ogni forma di discriminazione delle persone anche in base al loro orientamento sessuale nei luoghi di lavoro e che episodi simili espongono l’Italia a iniziative legali anche in ambito internazionale;

-  come intende il Governo rispettare le Direttive europee e le Raccomandazioni del Parlamento Europeo contro ogni forma di discriminazione delle persone anche in base al loro orientamento sessuale nei luoghi di lavoro affinche' episodi simili non espongano piu' l’Italia a iniziative legali anche in ambito internazionale;

-  di chiarire una volta per tutte se tra le competenze di un Questore vi sia anche quella di decidere se un agente debba essere eterosessuale per svolgere bene il suo dovere e se il giudizio di pericolosità rivolto alla poliziotta e' da rapportarsi al suo orientamento sessuale o alle sue dichiarazioni alla stampa riguardo le vessazioni e le violenze subite;

-  se il Governo è a conoscenza di quanto accade ai gay pride di molti paesi democratici, tra questi l’Australia, la Gran Bretagna, la Francia, la Spagna e Satati Uniti dove poliziotti gay manifestano e invitano i cittadini a entrare loro stessi nelle forze dell’ordine;

-  infine se non ritenga il Governo necessario e urgente porre fine a quanto descritto in premessa per recuperare l’arretratezza italiana sul tema della lotta alle discriminazioni.