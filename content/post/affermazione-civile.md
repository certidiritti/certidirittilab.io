---
title: 'AFFERMAZIONE CIVILE'
date: Sun, 27 Jun 2010 12:45:10 +0000
draft: false
tags: [138/2010 Corte costituzionale, Affermazione Civile, Associazione radicale Certi Diritti, diritto al matrimonio, matrimonio tra persone dello stesso sesso, matrmonio civile, Sentenza, Senza categoria]
---

**AFFERMAZIONE CIVILE**
=======================

#### ****

### **Dopo la sentenza della Corte costituzionale, l'impegno per il diritto al matrimonio come traguardo di uguaglianza continua. Il nostro obiettivo è tornare in autunno con nuove iniziative concrete, questa volta anche per ottenere il riconoscimento dei singoli diritti, come ha suggerito la Corte.**

****

Nel 2008, in collaborazione con Avvocatura LGBT – Rete Lenford, abbiamo lanciato la campagna di Affermazione civile per il riconoscimento del diritto al matrimonio tra persone dello stesso sesso.

Questa battaglia ha condotto, due anni dopo, alla sentenza della Corte Costituzionale 138/2010. Questa sentenza: 

1) ha riconosciuto la dignità costituzionale delle unioni omosessuali

2) sollecita il Parlamento a varare una disciplina di carattere generale in materia

3) parifica i diritti delle coppie omosessuali ai diritti già riconosciuti alla coppie more uxorio eterosessuali

4) si riserva d’intervenire per garantire un trattamento omogeneo tra le coppie coniugate e quelle omosessuali

5) bandisce il comportamento omofobico.

**[La sentenza 138/2010](http://www.cortecostituzionale.it/giurisprudenza/pronunce/scheda_ultimo_deposito.asp?sez=ultimodep&Comando=LET&NoDec=138&AnnoDec=2010&TrmD=&TrmM)** rappresenta in sè un traguardo importante, poiché finalmente la Repubblica Italiana si è espressa in merito al diritto di famiglia per le coppie dello stesso sesso. Grazie ad essa, i soggetti democratici avranno finalmente a disposizione un punto di riferimento certo per pianificare le future azioni politiche e giudiziarie.

Per questo è fondamentale, prima di agire, **favorire un periodo di riflessione a approfondimento**, così da individuare con precisione le nuove opportunità che la sentenza ci offre, e perseguire con convinzione le azioni giuridiche ed eventualmente anche politiche da intraprendere per ottenere effetti tangibili sulla vita di gay e lesbiche in Italia.

**La calma che ha seguito il fermento del dopo sentenza è in qualche modo voluto.** E' l'unico modo per essere sicuri che, quando ripartiremo, saremo in grado - come abbiamo fatto in passato - di proporre iniziative serie, affidabili, portate avanti con competenza e una visione d'insieme efficace. Agire adesso, subito, senza una opportuna riflessione, significherebbe esporci tutti ad una maggiore vulnerabilità da tutti i punti di vista.

Nel frattempo siamo prima di tutto **impegnati a diffondere in ogni contesto**, compresi i contesti accademici e giuridico/legalistici, **una lettura realisticamente positiva e propositiva della sentenza.** Questo perché è importantissimo consolidare un substrato di professionisti e attivisti capaci di promuovere - ognuno nei loro contesti - una cultura che motivi all'iniziativa civile, pacifica e non vioenta, e allo stesso tempo convinta ed incisiva.

Inoltre stiamo continuando a fornire un **contributo importante per la messa in moto del Comitato "Sì, lo voglio"**, perché siamo sempre più convinti che l'impegno per il diritto al matrimonio è un patrimonio e una conquista di tutta la società, non solamente di gay e lesbiche. 

Inoltre, stiamo riformulando **un rinnovato e solido pensiero su cosa è Affermazione Civile:   
 - **in che modo affiancare le iniziative per il matrimonio alle iniziative per ottenere specifici diritti di coppia  
 \- in che modo affiancare alle iniziative giuridiche eventuali iniziative giuridiche in sintonia con lo spirito del diritto al matrimonio come traguardo di uguaglianza  
 \- in che modo contribuire al dibattito politico contro l'omofobia anche attraverso queste iniziative che rafforzano lo stato di diritto.

**Affermazione Civile è, in generale, un modo di essere cittadini responsabili** (non sudditi) **e fare cittadinanza attiva. **

In concreto, siamo fiduciosi, già per questo autunno, di individuare e lanciare nuove iniziative giuridiche concrete.  Perciò tenetevi pronti: **come è avvenuto in passato, tutto quello che si potrà fare dipenderà proprio da voi. **Questa volta ci aspettiamo che saranno tante le persone e le coppie che vorranno ripetere, magari con altre sfumature, quanto già hanno fatto quelle coppie che si sono presentate in comune per chiedere le pubblicazioni degli atti di matrimonio.

E al di là di ogni altra cosa, vogliamo rassicurarvi su questo punto importante: **per Certi Diritti l'obiettivo resta quello della piena uguaglianza dei cittadini di fronte alla legge e del diritto al matrimonio.**

Su questa strada continueremo, insieme a decine di associazioni LGBT(E) con le quali abbiamo dato vita al Comitato Sì, lo voglio.