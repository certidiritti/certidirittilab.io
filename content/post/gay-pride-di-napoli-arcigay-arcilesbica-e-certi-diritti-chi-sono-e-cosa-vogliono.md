---
title: 'GAY PRIDE DI NAPOLI : Arcigay, Arcilesbica e Certi Diritti... Chi sono e cosa vogliono?'
date: Sat, 03 Jul 2010 19:07:16 +0000
draft: false
tags: [Senza categoria]
---

[Levanteonline](http://www.levanteonline.net/italia/regione/1895-gay-pride-di-napoli--arcigay-arcilesbica-e-certi-diritti-chi-sono-e-cosa-vogliono.html)

Di Chiara De Capitani

Fra le tante associazioni presenti al gay pride di Napoli notabile la presenza in particolare dei grandi carri dell'**Arcilesbica**, **Arcigay** e **Certi Diritti**. Per la coerenza della lotta di queste associazioni alla protezione e rispetto dei diritti LGBT e per l'impossibilità di approfondire la conoscenza con altre associazioni in quel tran tran di colori e festività ecco una breve intervista a tre rappresentanti di queste ultime.

Intervista per strada a **Giordana Curati**, Presidente **Arcilesbica Napoli** ([www.arcilesbica.it](http://www.arcilesbica.it/) )

**Levante:** **Volevamo sapere da lei quali azioni avete intrapreso a livello regionale e come siete accolti in generale sul territorio?**

Siamo innanzitutto un circolo situato su napoli periferiale e recentemente siamo riusciti a realizzare questo pride anche grazie a un percorso costruito in rete con altre realtà associative sul territorio ed anche le istituzioni; abbiamo in particolare costruito insieme al Comune di Napoli un tavolo di concertazione lgbt che possa promuovere varie iniziative e attività per il territorio sui nostri temi, ed é proprio attraverso questo tavolo di concertazione che siamo arrivati a proporre Napoli come luogo del pride: la collaborazione col comune é stata essenziale sia dal punto di vista politico che dal punto di vista del sostegno materiale.

**Personalmente quali sono le sue speranze a breve termine, per il prossimo anno per la comunità LGBT a Napoli in particolare?**

Mi auguro che questo pride apra una stagione di tante iniziative, anche al livello della alta politica - che bisognerebbe smuovere rispetto ai nostri temi e alle nostre rivendicazioni – ma anche nel terreno più basso dell'opinione pubblica, della cultura e dell'informazione. Noi ci aspettiamo di uscire rafforzati da questo pride e che, in quanto movimento locale riusciamo a promuovere iniziative culturali in contatto diretto con i cittadini... penso in particolare a progetti all'interno delle scuole, ad attività sul territorio che possono essere di partecipazione ma anche di informazione per le persone perchè – infatti - molto spesso il problema si genera dall'ignoranza , dalle nostre realtà sconosciute da alcuni, dalla nostra condizione e quindi si vive di pregiudizi e quindi di discriminazione. Andare a toccare la sensibilizzazione della cittadinanza napoletana é il nostro progetto, lo é sempre stato e continuerà in questa direzione.

**Rispetto a cittadini di altre regioni, lei come trova risponda in generale un cittadino eterosessuale napoletano alla situazione gay (sapendo che nella cultura napoletana é radicata una cultura aperta riguardo ai femminielli da ormai secoli)?**

Napoli è una città molto strana, molto imprevedibile. Esistono sicuramente delle forme molto radicate della cultura dei “femminielli” anche nella cultura più generale dell'accoglienza all'altro...E' vero che però é una città in grandi sofferenze soprattutto rispetto a varie problematiche legate ai diritti sociali per cui la sofferenza di una realtà territoriale inasprisce probabilmente anche il comportamento sociale ed é molto facile che generi atti di omofobia o un comportamento-pensiero omofobo in città. Tale comportamento esiste pure ma non é predominante: a Napoli ci sono sacche del tessuto sociale cittadino che hanno assolutamente scardinato i pregiudizi e le discriminazioni ed esistono invece altre sacche di cittadinanza che basterebbe informare e sensibilizzare (alla tolleranza). Quindi Napoli é imprevedibile, terremo conto delle risposte di oggi e continueremo a lavorare per cercare di trovare una sinergia all'interno della cittadinanza eterosessuale, omosessuale e transessuale napoletana.

Intervista a **Domenico Panico**, coordinatore del settore scuola di **Arcigay Napoli** ([http://www.arcigaynapoli.org](http://www.arcigaynapoli.org/)).

**Levante:** **ci parli un po' dei progetti dell'Arcigay napoletana...**

Per quanto riguarda le nostre aspettative regionali cerchiamo semplicemente di integrarci , la nostra richiesta é sempre integrazione - cerchiamo come baluardo della nostra idea semplicemente quella di riuscire a lottare insieme (eterosessuali ed omosessuali), volendo semplicemente che la libertà diventi legale. Ovviamente utilizziamo mezzi che cambiano in base alle situazioni: nelle scuole, con conferenze, in mezzo alla strada facendo manifestazioni, cerchiamo di fare sit in di fronte agli organi importanti del nostro caro stato. Fino a quando ci sarà possibile lotteremo.

**Quali sono i migliori mezzi per convincere il napoletano ignorante che non siete dei pedofili mandati dal diavolo?**

La luce del sole. Lottare, farsi vedere di fronte a tutti. Vivere alla luce del sole perché le persone si devono abituare, finché le cose non le vedono ne hanno paura . Devono solo abituarsi.

Infine, intervista a **Sergio Rovasio**, segretario dell'associazione radicale “**Certi Diritti**” ([www.certidiritti.it](http://www.certidiritti.it/) )

**Levante:** **In cosa consiste l'associazione “Certi Diritti”?**

E' un'associazione radicale che si batte per i diritti e per il superamento delle disuguaglianze molto pesanti in questo paese disgraziato (non solo sul piano economico e sociale ma anche sul piano dei diritti civili e dei diritti umani), d'altronde, nonostante i richiami dall'Europa e nonostante i paesi membri dell'unione Europea seguano tutti in una direzione l'Italia é forse l'unico paese che va nella direzione opposta. Queste sono occasioni di visibilità per la comunità LGBT-E (aggiungo sempre la “E” finale perché é anche grazie gli eterosessuali che noi riusciremo a fare grandi battaglie di civiltà per il superamento di queste ingiustizie vergognose) di battersi per non essere considerati come in secondo piano, nell'offesa, nel degrado, nell'insulto e nel calpestare tutti i diritti basilari - basta pensare proprio alle coppie che convivono da decenni che non possono vedersi riconosciuti degli elementarissimi diritti che invece alle coppie eterosessuali sono garantiti.

**Quali sono i vostri piani d'azione passati, presenti e o futuri per garantire i diritti della comunità LGBT?**

Noi dell'associazione Certi Diritti lavoriamo su diversi fronti, principalmente ci occupiamo del superamento delle disuguaglianze sul piano - per esempio - del matrimonio (impedito alle coppie omosessuali), e contro ogni forma di omofobia, del fenomeno della denigrazione delle persone transessuali per le quali cerchiamo di promuovere iniziative per la difesa della dignità e della necessità di nuove leggi (l'ultima legge riguardante i transessuali, di iniziativa radicale risale infatti all'82 e permise il cambio di sesso in strutture pubbliche). La dimostrazione che l'Italia sta andando indietro é che dall'82 ai giorni nostri su questi temi non é stato fatto assolutamente nessun passo avanti anzi son stati fatti soltanto passi indietro. Inoltre lottiamo anche per quanto riguarda la questione delicata della prostituzione, fenomeno considerato dall'Italia come peccaminoso, grave, offensivo mentre esiste probabilmente da quando esiste l'uomo (infatti si dice sia la professione più antica di questo mondo) e nonostante questo esistono ancora demagoghi politici che “ce la menano e ce la cantano” dicendo che si inventeranno qualcosa per fermare il fenomeno della prostituzione; la questione é ridicola e patetica e fa vedere come l'Italia sia un pessimo concentrato su tutti i livelli: politico-sociale-culturale-scolastico, infestato di ipocrisie e demagogie nel dare il contentino alla gente dicendo che da oggi a domani si cancellerà la prostituzione nelle città mentre le prostitute continuano ad essere offese, denigrate, calpestate nei loro diritti e sono soggette a stupri senza poterli denunciare per evitare una multa di 500 euro prevista dall'ordinanza del sindaco Alemanno. Come vedete in Italia c'é molto lavoro da fare, con la nostra associazione cerchiamo di frenare questa valanga di politica demagogica populista e fascista che impedisce alla formazione e alla responsabilità della dignità delle persone.