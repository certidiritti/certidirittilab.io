---
title: 'Certi Diritti scrive a Renzi: vieni al nostro congresso dove le coppie dello stesso sesso ti convinceranno che le unioni civili non bastano'
date: Sat, 04 Jan 2014 20:15:40 +0000
draft: false
tags: [Diritto di Famiglia]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti

Roma, 4 gennaio 2014

Il Segretario dell'Associazione Radicale Certi Diritti ha scritto una lettera al Segretario del Partito Democratico per invitarlo al VII Congresso dell'Associazione che si svolgerà a Milano, presso l'Ex Chiesetta del Parco Trotter in via Mosso 7, dal 10 al 12 gennaio.

[Nella lettera aperta, inviata al segretario PD e pubblicata sul blog ospitato dall'Huffington Post di Guaiana](http://www.huffingtonpost.it/yuri-guaiana/renzi-vieni-al-nostro-con_b_4541058.html), si legge, tra l'altro: «Sono convinto che le nostre copie e i nostri giuristi la potranno convincere che le unioni civili non bastano e, se fossero riservate solo agli omosessuali sarebbero persino un passo falso. Sono convinto che le nostre copie e i nostri giuristi la potranno convincere che per mettere davvero tutti i cittadini su un piede di parità occorre riconoscere anche in Italia il matrimonio egualitario. Sono convinto che le nostre copie e i nostri giuristi la potranno convincere che i figli delle coppie dello stesso sesso devono avere gli stessi diritti di tutti gli altri».

Secondo Guaiana, infatti, «solo con [una riforma complessiva del diritto di famiglia capace di soddisfare le esigenze di tutti, unificare finalmente le norme già esistenti in tema di diritto di famiglia e dare così luogo a un'unica e organica legislazione](http://www.huffingtonpost.it/yuri-guaiana/per-un-amore-civile_b_2282585.html)» si potrebbe «volgere la situazione di spaventosa arretratezza italiana persino in un vantaggio».