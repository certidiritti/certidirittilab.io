---
title: 'PARTE LA PETIZIONE "NESSUNO CI PUO'' RIPARARE"'
date: Fri, 05 Feb 2010 09:11:35 +0000
draft: false
tags: [Comunicati stampa]
---

Carissim* da oggi è possibile sottoscrivere la petizione "Nessuno ci può riparare" promossa da Psicologiagay.com.

L'obiettivo è sollecitare gli Ordini nazionali dei medici e degli psicologi perchè prendano una posizione chiara, pubblica e definita in merito al concetto di orientamento sessuale (che ha sollecitato la pregiudiziale di incostituzionalità della cd. legge Concia), in merito alla non scientificità delle terapie riparative, perchè intervengano qualora loro iscritti scrivano/dicano pubblicamente affermazione assolutamente contrarie a quanto la comunità scientifica asserisce e verifica.

Vi chiedo un enorme aiuto perchè sia pubblicizzata l'iniziativa presso le vostre associazioni, le vostre reti sociali, i vostri siti, amici, colleghi, conoscenti.

La petizione si trova qui

http://www.psicologiagay.com/petitio_nessuno.php

Sono a vostra disposizione per qualunque informazione in merito a questa e altre iniziative di Psicologigay.com e vi ringrazio per la collaborazione.

Dr Paola Biondi