---
title: 'ANCHE  FERRARA RIMETTE A CONSULTA RICORSO COPPIA GAY MATRIMONIO'
date: Fri, 18 Dec 2009 08:22:06 +0000
draft: false
tags: [Comunicati stampa]
---

**FERRARA, MATRIMONI GAY: SALGONO A QUATTRO LE ORDINANZE DI REMISSIONE ALLA CORTE COSTITUZIONALE. LA CAMPAGNA DI AFFERMAZIONE CIVILE CONTINUA**

**Dichiarazione congiunta di Antonio Rotelli, Presidente di Avvocatura lgbt – Rete Lenford e Sergio Rovasio, Segretario nazionale dell’Associazione Radicale Certi**

**"Anche il Tribunale di Ferrara, in data 14 dicembre 2009, ha depositato un’ordinanza di remissione alla Corte costituzionale sul caso di una coppia di persone dello stesso sesso che si era vista rifiutare dal Comune di Ferrara la richiesta di pubblicazioni matrimoniali.**

**Dopo la Corte d’appello di Firenze e prima ancora il Tribunale di Venezia e la Corte d’appello di Trento, anche il Tribunale di Ferrara ha ritenuto contrario alla Costituzione il non consentire alle coppie di persone dello stesso sesso di contrarre matrimonio: “il diritto di contrarre matrimonio – si legge nell’ordinanza - è un momento essenziale di espressione della dignità umana, esso deve essere garantito a tutti senza discriminazioni derivanti dal sesso o dalle condizioni personali (quali l’orientamento sessuale), con conseguente obbligo dello Stato di intervenire in caso di impedimenti all’esercizio”. Privare qualcuno della possibilità di fondare una famiglia in ragione dell’orientamento sessuale lede la sua dignità…”.**

La campagna di Affermazione Civile, che prevede di fare ricorso al Tribunale quando il Comune oppone il diniego alle pubblicazioni di matrimonio richieste da una coppia di persone dello stesso sesso, raggiunge così un altro importante risultato.

Facciamo appello a tutte le coppie lesbiche e gay di attivarsi come hanno fatto le coppie di Ferrara, Firenze, Trento e Venezia presentando presso il loro Comune di residenza una richiesta di pubblicazioni matrimoniali per poterla impugnare dinanzi ai tribunali in caso di rifiuto. È un’opportunità storica per far comprendere a tutto il Paese che le famiglie composte da persone dello stesso sesso hanno pari dignità come ormai è riconosciuto in tutti i Paesi europei".

[www.retelenford.it](http://www.retelenford.it)

[www.certidiritti.it](../../undefined/)