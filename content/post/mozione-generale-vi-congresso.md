---
title: 'Mozione generale VI° Congresso'
date: Tue, 09 Apr 2013 08:21:13 +0000
draft: false
tags: [Politica]
---

Il VI Congresso dell'Associazione radicale certi diritti ringrazia per il loro importante contributo i rappresentanti dei movimenti lgbt di Francia, Gran Bretagna, Argentina, Turchia, Russia, Albania e USA, ed i rappresentanti di ILGA e OHCHR, i cui interventi sono stati non solo utili per lo sviluppo delle iniziative transnazionali dell'Associazione ma anche per quelle nazionali.

Il Congresso ringrazia inoltre, tutti coloro che hanno deciso di partecipare ai suoi lavori , in particolare Pia Covre, Bruno De Filippis, Marilisa D'Amico, Antonio Stango, Elisabetta Zamparutti e Filomena Gallo confermando una disponibilità tanto più preziosa per i tempi confusi e pericolosi che viviamo.

Il Congresso, infine, ringrazia i rappresentanti dei Gruppi parlamentari che hanno scelto di confrontarsi nel nostro Congresso sulle "riforme possibili" e ritiene che la loro disponibilità a continuare il dialogo (tra di loro e con le associazioni) sia un elemento importante su cui contare per la costruzione di una strategia efficace per il raggiungimento delle riforme di cui questo paese ha bisogno, anche nell'ambito della difesa dei diritti umani delle persone lgbt.

Il Congresso, ascoltate ed accolte le relazioni degli organi associativi, non può che sottolineare l'insostenibile distanza tra ricchezza di temi ed iniziative attivate e l'estrema limitatezza di risorse (personali e finanziarie) a disposizione, sottolineando che la questione centrale dell'Associazione, oggi più che mai, sia quella di dotarsi di una organizzazione capace di essere all'altezza degli obiettivi che si da.

Il Congresso, quindi, decide di dedicare i mesi che ci separano dal prossimo congresso annuale, da convocarsi entro e non oltre il prossimo 31 dicembre 2013, alla riorganizzazione dell'associazione, ed alla attivazione di iniziative che consentano di fornire nuova forza alle battaglie che intendiamo sviluppare.

A tal fine ritiene che l'Associazione debba concentrare le sue attività e i suoi sforzi fino al prossimo Congresso soprattutto su due ambiti di intervento:

• oltre a confermare la continuazione di Affermazione civile e la validità della strategia adottata, l'Associazione deve concentrarsi sulla costruzione e il lancio di una campagna nazionale per il matrimonio egualitario, perché questo è il tema che rappresenta il cardine del confronto oggi in atto sulla piena uguaglianza sostanziale e non solo formale delle persone lgbt nel Paese. Campagna che deve essere capace di coinvolgere non solo il maggior numero possibile di cittadini e cittadine, membri del parlamento e della classe dirigente del paese e associazioni;

• lo sviluppo di iniziative transnazionali, in particolare nelle istituzioni internazionali ed europee, perché il diritto non si trasformi ulteriormente in lettera morta ma sia garanzia della vita, della dignità, dei diritti e dei doveri delle persone a prescindere da orientamento sessuale e identità di genere.

Il Congresso, infine, rinnova il suo impegno come soggetto costituente del Partito Radicale Nonviolento Transnazionale e Transpartito, perché ritiene che questa sia la sua naturale collocazione politica, l'orizzonte culturale entro il quale è nato e si è sviluppato, ed il futuro delle sue iniziative le quali, tutte, rientrano appieno nel solco dell'esperienza radicale.

 Napoli 7 aprile 2013