---
title: 'Giacomo Cellottini'
date: Tue, 26 Jan 2016 06:56:11 +0000
draft: false
---

[![giacomocellottini](http://www.certidiritti.org/wp-content/uploads/2016/01/giacomocellottini-300x300.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/01/giacomocellottini.jpg)Giacomo Cellottini nasce il 6 settembre 1984 nelle Marche, dove attualmente vive e lavora. Laureato in Sociologia all'Università di Urbino con una tesi sulle nuove famiglie, nel 2006 si trasferisce a Milano per lavorare come web editor a Gay.tv. Al congresso fondativo di Roma, nel marzo 2008, si iscrive all'Associazione Radicale Certi Diritti e l'anno successivo con il segretario Sergio Rovasio organizza alla Camera dei Deputati i convegni sulla riforma del diritto di famiglia e sul matrimonio egualitario. Dal 2010 al 2012 ha ricoperto la carica di tesoriere dell'associazione.