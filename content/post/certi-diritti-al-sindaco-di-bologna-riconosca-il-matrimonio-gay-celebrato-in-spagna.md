---
title: 'Certi Diritti al sindaco di Bologna: riconosca il matrimonio gay celebrato in Spagna'
date: Tue, 08 Nov 2011 17:10:37 +0000
draft: false
tags: [Diritto di Famiglia]
---

Appello dell'associazione radicale Certi Diritti affinchè Merola compia un atto di affermazione di coscienza per l'eguaglianza e contro le discriminazioni riconoscendo il matrimonio tra Ottavio e Joaquin.

Roma-Bruxelles, 8 novembre 2011  
  
Comunicato Stampa dell'Associazione Radicale Certi Diritti  
  
L'Associazione Radicale Certi Diritti ed Ottavio Marzocchi e Joaquin Nogueroles Garcia si appellano al Sindaco di Bologna Valerio Merola per il riconoscimento del loro matrimonio celebrato in Spagna il 20 agosto scorso. Marzocchi, cittadino italiano nato a Bologna e membro della direzione di Certi Diritti, e Nogueroles Garcia, cittadino spagnolo ed iscritto a Certi Diritti, hanno depositato lo scorso 4 novembre, presso gli uffici anagrafici del Comune di Bologna, la domanda per il riconoscimento in Italia del loro matrimonio, corredato dai documenti necessari. E' molto probabile che il Comune di Bologna dia una risposta standard di diniego di tale riconoscimento.  
  
Marzocchi e Nogueroles si appellano pero' al Sindaco di Bologna perché compia un atto di affermazione di coscienza per l'eguaglianza e contro le discriminazioni basate sull'orientamento sessuale e per garantire tale riconoscimento sulla base della Costituzione italiana, della Carta dei diritti fondamentali dell'UE, dei Trattati e delle direttive europee, disapplicando quanto la stessa Europa chiede con i Trattati, e che il Governo ignora, sul riconoscimento in Italia delle unioni tra persone dello stesso sesso,. L'Italia considera le unioni contratte all'estero come "contrarie all'ordine pubblico" - come se le coppie dello stesso sesso fossero un pericolo per l'ordine pubblico quali le organizzazioni criminali o terroristiche - e quindi nulle, inesistenti, illegali, proibite. La stessa Corte di Cassazione con alcune importanti sentenze ha dichiarato che nell'ambito del diritto di famiglia non si possono applicare norme sull'ordine pubblico.  
  
Nogueroles e Marzocchi chiedono anche alle autorità spagnole di difendere il riconoscimento dei diritti acquisiti sulla base della legge spagnola negli altri Stati membri dell'Unione europea, Italia inclusa. Infine chiedono di interrompere la politica di discriminazione basata sull'orientamento sessuale che nega il riconoscimento degli effetti della legge sul matrimonio o sull'unione civile di uno stato estero quando riguarda le coppie dello stesso sesso, mentre accetta e riconosce gli effetti della stessa legge quando riguarda coppie di sesso diverso.  
  
Per ottenere l'eguaglianza sono necessarie azioni e persone che abbiano il "coraggio" di interrompere le prassi italiane discriminatorie e di applicare i principi di base della democrazia e dei diritti umani fondamentali, e Marzocchi e Nogueroles chiedono al sindaco di Bologna di compiere tali azioni.  
  
Marzocchi e Nogueroles continueranno in ogni caso la battaglia legale contro le discriminazioni e per l'eguaglianza, e preannunciano che la loro vicenda verrà portata presso le giurisdizioni nazionali ed europee.  
  
L'iniziativa si inquadra nella campagna di Affermazione Civile di Certi Diritti che ha portato davanti alle corti nazionali ed europee le situazioni di discriminazione vissute dalle coppie dello stesso sesso in Italia.