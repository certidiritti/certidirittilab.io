---
title: 'CERTI DIRITTI PARTECIPA ATTIVAMENTE AI GAY PRIDE: DA TEL AVIV A ROMA A CATANIA...'
date: Fri, 12 Jun 2009 18:02:42 +0000
draft: false
tags: [Comunicati stampa]
---

Nel 40` anniversario della rivolta di Stonewall, il mese di giugno e` diventato in tutto il mondo il mese della straordinaria mobilitazione del movimento lgbtq(e).

Sergio Rovasio e` stato invitato a Tel Aviv a rappresentare l\`Italia in qualita\` di Segretario dell\`Associazione Radicale Certi Diritti in occasione del Gay Pride cittadino che si e\` svolto oggi, venerdi` 12 giugno.

A Torino si e` gia` svolto il Gay Pride cittadino lo scorso 26 maggio con la partecipazione di decine di migliaia di persone.

In Italia si tengono in questi giorni i seguenti gay pride:

A Roma sabato 13 giugno, alle ore 15 inizio del Gay pride che vede coinvolte decine e decine di Associazioni provenienti da tutta Italia. Certi Diritti fa parte del comitato promotore e l\`intervento del coordinatore di Roma, Lucky Amato, e\` previsto in Piazza Navona alle ore 18 circa.

A Genova si terra` il prossimo sabato 27 giugno il Gay pride nazionale che ha come Portavoce il Coordinatore cittadino di Genova di Certi Diritti, Alberto Villa. Nell\`ambito del Gay Pride sono previste nella cittadini ligure decine di iniziative, compresa l\`inaugurazione di un villaggio gay.

A Catania si terra` sabato 4 luglio il Gay pride cittadino che vedra` tra i partecipanti l`Associazione Certi Diritti.