---
title: 'DUE GAY CONDANNATI IN MALAWI A 14 ANNI DI LAVORI FORZATI. INTERROGAZIONE URGENTE DEI DEPUTATI RADICALI AL GOVERNO ITALIANO.'
date: Thu, 20 May 2010 16:16:22 +0000
draft: false
tags: [Comunicati stampa]
---

I deputati radicali – Pd, hanno oggi depositato una interrogazione parlamentare urgente al Ministro degli Esteri sulla coppia di omosessuali condannati da un Tribunale del Malawi a 14 anni di lavori forzati perché avrebbero violato “l’ordine della natura”. Di seguito il testo integrale dell’interrogazione parlamentare urgente, primo firmatario il deputato radicale Matteo Mecacci:

**Interrogazione urgente a risposta scritta**

Al Ministro degli Esteri

-         Un tribunale del Malawi ha condannato una coppia di omosessuali a 14 anni di lavori forzati perché riconosciuta colpevole di aver violato «l'ordine della natura»;

-         Steven Monjeza, 26 anni, e Tiwonge Chimbalanga, 20, furono arrestati il 27 dicembre scorso a Blantyre con l'accusa di oltraggio al pudore. Due giorni prima si erano uniti in matrimonio con una cerimonia simbolica. Nel motivare la condanna, il giudice Nyakwawa Usiwa Usiwaha ha accusato la coppia di «esempio orribile». «Vi ho inflitto una pena spaventosa in modo che i figli e le figlie del Malawi siano protetti da gente come voi e che nessuno sia tentato di emulare quell'orribile esempio, contrario alla cultura e ai valori religiosi di questo Paese», ha affermato il magistrato;

-         L'arresto della coppia ha suscitato l'indignazione della comunità a difesa dei diritti degli omosessuali e delle organizzazioni impegnate nella lotta all'Aids;

-         Esponenti religiosi del Malawi, tuttavia, hanno approvato l’azione del governo e della magistratura, sottolineando come l’omosessualità costituisca un peccato e l’Occidente non debba utilizzare il suo potere economico per costringere il Paese ad accettarla;

-         L’omosessualità è attualmente illegale in almeno 37 Paesi africani. L'Uganda sta valutando un incremento delle pene che comprenderebbe condanne fino all’ergastolo e condanna a morte per i recidivi.

Per sapere:

-         quali iniziative il nostro Governo intende prendere nei confronti del Malawi affinché vengano rispettati i più elementari diritti civili e umani anche delle persone lesbiche e gay;

-         quali iniziative anche in ambito europeo e Onu intende avviare il nostro Governo per sostenere la depenalizzazione dell’omosessualità almeno in quei paesi dove è prevista la pena di morte;

-         se non ritenga infine necessario promuovere una iniziativa diplomatica per chiedere di poter far visita ai due detenuti da parte del nostro Vice Console onorario di Lilongwe.

I deputati radicali - Pd:

Matteo Mecacci, Rita Bernardini, Marco Beltrandi, Maria Antonietta Farina Coscioni, Maurizio Turco, Elisabetta Zamparutti