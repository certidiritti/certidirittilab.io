---
title: 'Prendiamo atto blocco trascrizioni a Bologna, adesso serve urgentemente una legge'
date: Mon, 15 Sep 2014 12:48:18 +0000
draft: false
tags: [Diritto di Famiglia]
---

[![registro-firma-bologna-merola-nozze-gay-estero-15-settembre](http://www.certidiritti.org/wp-content/uploads/2014/09/registro-firma-bologna-merola-nozze-gay-estero-15-settembre.jpg)](http://www.certidiritti.org/wp-content/uploads/2014/09/registro-firma-bologna-merola-nozze-gay-estero-15-settembre.jpg)L'Associazione Radicale Certi Diritti prende atto della decisione del Prefetto di Bologna di bloccare il registro delle trascrizioni  e punta il dito su Parlamento e Governo che persistono nel non voler riconoscere il diritto umano alla vita famigliare delle persone lesbiche e gay creando anche un evidente problema di coerenza con gli altri paesi europei. Così il segretario dell'Associazione Radicale Certi Diritti, Yuri Guaiana, che prosegue: "Tra delibere comunali, annunci dei sindaci e sentenze dei tribunali che affermano tutto e il contrario di tutto - ultimo il "no" del Tribunale di Milano in netto contrasto con la debole ordinanza del tribunale di Grosseto - è necessario che un intervento legislativo colmi una volta per tutte questa lacuna. Noi siamo per il matrimonio egualitario che evidentemente risolverebbe tutti i problemi. Non c'è più tempo. Smettetela di giocare sulle nostre vite e sui nostri affetti!!"

Comunicato stampa dell'Associazione Radicale Certi Diritti