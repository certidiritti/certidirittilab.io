---
title: 'A Sochi Letta sollevi la questione dei diritti umani e non si dimentichi dell''Italia'
date: Sun, 02 Feb 2014 21:25:16 +0000
draft: false
tags: [Politica]
---

Comunicato dell'Associazione Radicale Certi Diritti.

Roma, 3 febbraio 2014

Mentre il presidente del Consiglio Letta sembra aver deciso di andare a Sochi per le Olimpiadi, il presidente del CONI, dott. Giovanni Malagò, ha risposto alla nostra richiesta d'inserire il divieto di discriminazione basato su orientamento sessuale, identità ed espressione di genere all'art. 6 del Codice di comportamento sportivo con una generica rassicurazione sulla sua attenzione e sensibilità al tema delle discriminazioni nello sport, che peraltro non mettiamo in discussione. Se così è, però, occorre prendere un impegno pubblico e preciso per inserire il divieto di discriminazione basato anche su orientamento sessuale, identità ed espressione di genere all'art. 6 del Codice di comportamento sportivo, cosa di cui nella risposta di Malagò non c'è traccia.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: «visto che il presidente del Consiglio Letta si dimostra così sensibile allo spirito olimpico da recarsi sino a Sochi, vorrà fare in modo che anche in Italia lo sport sia un'esperienza nel segno del rispetto per tutti e operare affinché il Codice di comportamento sportivo del Coni rifiuti esplicitamente le discriminazioni basate su orientamento sessuale, identità ed espressione di genere. Già che si trova in Russia, potrebbe anche cogliere l'occasione per sollevare la questione dei diritti umani o almeno ricordare il principio 6 della Carta Olimpica, per il quale qualunque forma di discriminazione è incompatibile con il Movimento Olimpico. Così potrebbe restituire un po' di voce e dignità ai milioni di cittadini russi LGBTI che il governo russo vorrebbe silenziare».

**[IL TESTO DELLA LETTERA DELL'ASSOCIAZIONE RADICALE CERTI DIRITTI](notizie/comunicati-stampa/item/download/45_34786fb38e0175d2740c9ee6c0b6bc3d)**

**[LA REPLICA DEL CONI](notizie/comunicati-stampa/item/download/44_8601b7eddeafea376471483d5ca9af64)**

**[LA RISPOSTA DI CERTI DIRITTI](notizie/comunicati-stampa/item/download/46_d7ba19c838280be17ed0f33efd724e59)**

  
[risposta\_del\_Coni.jpg](http://www.certidiritti.org/wp-content/uploads/2014/02/risposta_del_Coni.jpg)  
[richiesta\_di\_iniziative\_contro\_le\_discriminazioni\_basate\_su\_orientamento\_sessuale\_e\_identita\_di\_genere\_anche\_in\_vista\_di\_SOCHI.pdf](http://www.certidiritti.org/wp-content/uploads/2014/02/richiesta_di_iniziative_contro_le_discriminazioni_basate_su_orientamento_sessuale_e_identita_di_genere_anche_in_vista_di_SOCHI.pdf)  
[Modifica\_dellYart.\_6\_del\_Codice\_di\_comportamento_sportivo.pdf](http://www.certidiritti.org/wp-content/uploads/2014/02/Modifica_dellYart._6_del_Codice_di_comportamento_sportivo.pdf)