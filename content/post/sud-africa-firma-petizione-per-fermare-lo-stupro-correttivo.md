---
title: 'SUD AFRICA: FIRMA PETIZIONE PER FERMARE LO ''STUPRO CORRETTIVO'''
date: Tue, 25 Jan 2011 15:39:48 +0000
draft: false
tags: [Comunicati stampa]
---

Lo "stupro correttivo", l'orrenda pratica di stupro delle lesbiche nel tentativo di "curare" la loro omosessualità, sta diventando una vera e propria emergenza in Sud Africa. **Firma anche tu la petizione per fermarlo.**

Millicent Gaika (nella foto) è stata legata, strangolata, torturata e stuprata ripetutamente in un attacco lo scorso anno. Ma alcuni coraggiosi attivisti sudafricani stanno rischiando la loro vita per far sì che il caso di Millicent sia la miccia per il cambiamento. Il loro appello al Ministro della giustizia è esploso fino a raggiungere le 130.000 firme, costringendolo così a difendersi in tv.

  
[Firma la petizione sul sito di Avaaz >](https://secure.avaaz.org/it/stop_corrective_rape/?cl=921352696&v=8242)