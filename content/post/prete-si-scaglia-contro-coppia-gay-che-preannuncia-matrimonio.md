---
title: 'PRETE SI SCAGLIA CONTRO COPPIA GAY CHE PREANNUNCIA MATRIMONIO'
date: Thu, 31 Jul 2008 13:43:14 +0000
draft: false
tags: [Comunicati stampa]
---

Dichiarazione di Sergio Rovasio su prete che umilia coppia gay che aveva preannunciato di sposarsi in Olanda:

Sabato scorso, intorno alle 17, a Fano, a quanto riferisce un articolo apparso su Il Messaggero di ieri, il parroco della chiesa di San Giuseppe al Porto, nel corso della celebrazione che vedeva unirsi in matrimonio una coppia fanese, avrebbe lanciato anatemi contro una coppia di omosessuali – Fausto ed Elvin – ben nota nella cittadina, che nei giorni scorsi aveva annunciato, attraverso una radio locale, di voler convolare civilmente a nozze il prossimo 16 agosto in Olanda, a Schiedam, paese originario di uno dei due. Il parroco avrebbe più volte fatto i nomi e cognomi dei due promessi sposi omosessuali, nonché rivelate le loro qualifiche professionali: dj animatore il primo, dirigente dei servizi educativi del Comune di Fano il secondo. “E a costui dovremmo affidare l'educazione dei nostri figli?” ha sentenziato il sacerdote, riferendosi proprio a quest’ultimo, davanti ai due sposi eterosessuali e alla platea incredula di invitati.

“Ciò che è avvenuto a Fano è ovviamente sintomatico del clima di omofobia e ignoranza che offusca la Chiesa cattolica,” dichiara Sergio Rovasio, segretario dell’associazione radicale Certi Diritti “e il voler alzare il dito da un altare contro due persone che hanno il diritto come chiunque altro di amarsi – e di vedere riconosciuta la loro unione in un Paese più civile dell’Italia – dimostra come sia ormai inesistente negli uomini di Chiesa lo spirito dell’accoglienza e della fratellanza nei confronti di tutti gli individui, indipendentemente dalla loro condizione sociale o personale. A Fausto ed Evin” continua Rovasio “manifesto tutta la solidarietà dell'associazione radicale Certi Diritti, nonché la nostra piena disponibilità a essere al loro fianco se i due ragazzi vorranno chiedere la trascrizione del loro matrimonio in Italia, avviando una procedura giudiziaria per il riconoscimento di un diritto legittimo e fondamentale, sancito dalla nostra Costituzione”.