---
title: 'San Pietroburgo approva legge omofoba. Certi Diritti chiede intervento Ministro Esteri e Consiglio d''Europa'
date: Sun, 12 Feb 2012 15:15:13 +0000
draft: false
tags: [Russia]
---

Pene molto pesanti con multe di 500.000 rubli al pari degli atti di pedofilia. La legge rischia di essere approvata in via definitiva il prossimo 16 febbraio.

Roma, 12 febbraio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Il Parlamento locale di San Pietroburgo ha approvato ieri in seconda lettura una gravissima legge omofoba che nemmeno ai tempi di quando la città si chiamava Leningrado era stata immaginata. La nuova legge oltre a vietare i gay pride nella città vieta anche che vengano pubblicati testi dove di parla di omosessualità e promuove diverse fome di censure sui comportamenti delle organizzazioni Lgbt(e) che difendono i diritti civili e umani. Le pene sono molto pesanti con multe di 500.000 rubli al pari degli atti di pedofilia.  Alcuni organizzazioni e militanti dei diritti civili hanno manifestato in questi giorni contro questo atto di inciviltà e antidemocratico.

Il Segretario dell’Associazione Radicale Certi Diritti, Yuri Guaiana e il Senatore Radicale-Pd Marco Perduca hanno scritto una lettera al Ministro degli Esteri italiano e chiesto al Segretario Generale del Consiglio d’Europa di intervenire urgentemente contro questo atto di barbarie che limita la libertà di espressione e di manifestazione e che rischia di essere approvato in via definitiva il prossimo 16 febbraio.  
   
La Russia fa parte del Consiglio d’Europa, organismo che lo scorso giugno aveva dato delle precise indicazioni ai suoi Stati membri riguardo il diritto alla libertà di manifestare delle organizzazioni Lgbt(e) e sollecitando  l’approvazione di leggi per il riconoscimento di diritti oggi negati.