---
title: 'UGANDA: FIRMA LA PETIZIONE CONTRO LA LEGGE ANTI-GAY'
date: Fri, 28 Jan 2011 15:02:57 +0000
draft: false
tags: [Comunicati stampa]
---

Gli omosessuali dell'Uganda potrebbero essere condannati a morte se la proposta di legge anti-omossessualità di cui si sta discutendo in questi giorni verrà approvata. **Firma la petizione su Avaaz.org**

Una forte condanna internazionale ha costretto il Presidente a sottoporre questa proposta di legge ad un'ulteriore revisione, ma i nostri sostenitori ugandesi dicono che solo una richiesta a gran voce proveniente da tutto il mondo potrebbe allontanare i Parlamentari da questa idea di discriminzione dal momento che si sentirebbero isolati a livello internazionale.

**David Kato Kisule, attivista gay ugandese ammazzato il 26 gennaio 2011**, aveva denunciato la terribile situazione del suo Paese al IV Congresso di Certi Diritti e invitato a firmare la petizione:

_"Chiedo a tutti voi di aderire a un’iniziativa che stiamo preparando: una petizione per chiedere giustizia e per scoraggiare il parlamento ugandese ad approvare il disegno di legge, anche in considerazione del fatto che il Ministro dell’Etica e dell’Integrità ha dichiarato che tale testo dovrà essere approvato prima che le Camere vengano sciolte, cioè prima di maggio 2011."_

NON LASCIARE CHE LA VIOLENZA SPENGA LA SUA VOCE.

**[FIRMA LA PETIZIONE SUL SITO DI AVAAZ 'DIRITTI NO REPRESSIONE'>  
](http://www.avaaz.org/it/uganda_rights/)**