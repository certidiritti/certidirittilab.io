---
title: 'T-DOR 2010 TRANSGENDER DAY OF REMEMBRANCE'
date: Tue, 16 Nov 2010 20:55:26 +0000
draft: false
tags: [Comunicati stampa]
---

**TRANSGENDER DAY OF REMEMBRANCE**

**A ROMA LA CERIMONIA DEL T-DOR 2010 SI SVOLGERA’ IN PIAZZA DEL CAMPIDOGLIO VENERDI’ 19 NOVEMBRE DALLE 17,30.**

**L’Associazione Libellula, Associazione Radicale Certi Diritti, Cgil Nuovi Diritti, Arcigay Roma, Circolo di Cultura omosessuale Mario Mieli, Radicali Roma e la Fondazione Massimo Consoli, celebreranno insieme il T-Dor 2010 a Roma in Piazza del Campidoglio, venerdì 19 novembre a partire dalle ore 17,30.**

**Invitiamo le persone che hanno a cuore la difesa dei diritti civili  e umani delle persone transessuali e transgender a partecipare a questa importante cerimonia promossa anche in molte altre città italiane.**

Il 20 Novembre ricorre in tutto il mondo il  “Transgender day of Remembrance” (Giornata mondiale in ricordo delle vittime dell'odio e del pregiudizio contro le persone transessuali).

La cerimonia commemorativa prevede che tutti i partecipanti, in fila, leggano a turno i nomi, qualche dettaglio di vita e le modalità dell'assassinio delle vittime transessuali dei crimini d'odio. Solitamente si accende una candela corrispondente al nome di ogni vittima della violenza.

.

In questo giorno si organizzano delle veglie in ricordo delle vittime dell'ultimo anno, i siti internet restano oscurati per lutto, l’immagine sostituita con una comune in tutto il mondo, scelta dal comitato americano e si cerca di sensibilizzare l'opinione pubblica su questo silenzioso massacro con presidi, fiaccolate, reading, performance teatrali, mostre fotografiche, eventi musicali e proiezioni cinematografiche.

Il primo evento risale alla fine degli anni ’90 e si deve a Gwendolyn Ann Smith in ricordo di Rita Hester, il cui assassinio nel 1998 diede avvio al progetto web "Remembering Our Dead" e nel 1999 a una veglia a lume di candela a San Francisco.

In Italia il numero degli omicidi di persone transessuali e’ incredibilmente alto e, in particolare, vittime sacrificali di un pregiudizio insensato sono come al solito le persone transgender immigrate. Sia a livello nazionale che internazionale i dati ufficiali sugli omicidi trans non descrivono che la punta di un iceberg, basandosi esclusivamente su denunce presentate, su rassegne stampa e su quanto le associazioni riescono a raccogliere direttamente.

In molti Paesi la morte violenta di persone transgender passa sotto silenzio, o perche’ la transessualità è ancora considerata un reato, o a causa di disinteresse e censure della rete informativa o perche’ i familiari nascondono la condizione transgender delle vittime per paura del giudizio sociale.