---
title: 'Monaco (Certi Diritti): negata adozione a coppia lesbica, serve certezza del Diritto'
date: Thu, 24 Nov 2016 15:57:41 +0000
draft: false
tags: [Diritto di Famiglia]
---

![due-mamme](http://www.certidiritti.org/wp-content/uploads/2016/11/Due-mamme-300x161.jpg)"La sentenza del Tribunale di Milano che ha negato l'adozione coparentale a una coppia lesbica, distinguendosi da altri precedenti, è l'assaggio di quello che sarà dopo la decisione del Parlamento di delegare alle corti e all'intraprendenza delle coppie la difesa di un diritto fondamentale, in questo caso quello del minore. E' questo il momento di una riforma del diritto di famiglia che comprenda matrimonio egualitario e adozioni per tutti al fine di garantire diritti e certezza del Diritto; sta al Legislatore arrivarci prima che l'Italia ricominci a collezionare sentenze dalle corti nazionali e internazionali" Così Leonardo Monaco, segretario dell'Associazione Radicale Certi Diritti