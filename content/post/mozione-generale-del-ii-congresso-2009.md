---
title: 'Mozione generale del II Congresso 2009'
date: Sat, 28 Mar 2009 13:31:54 +0000
draft: false
tags: [Politica]
---

#### Mozione generale approvata all'unanimità il 14 marzo 2009

  
Le ragioni che ci portarono un anno fa alla costituzione della Associazione radicale Certi Diritti sono, purtroppo, più che mai attuali.

La classe dirigente del nostro Paese ha dimostrato una capacità di sudditanza al pensiero ed ai voleri delle gerarchie cattoliche in materia di etica, e di etica sessuale in particolare, che lascia stupiti anche chi, come noi, non ha mai avuto soverchie aspettative, né da maggioranze di destra né da maggioranze di sinistra. Ogni tentativo di introdurre anche in Italia garanzie di legge e pari opportunità per le persone gay lesbiche e transessuali si scontra con un ideologico e granitico rifiuto di aprire gli occhi sulla realtà della vita di milioni di uomini e donne. Rifiuto che è superbamente esemplificato in sede ONU dalla crimonogena posizione del Governo dello Stato Vaticano che, insieme ai rappresentanti dei peggiori Stati sessuofobi, che purtroppo monopolizzano le Istituzioni internazionali, si oppone alla richiesta di depenalizzazione del comportamento omosessuale ed a qualsivoglia riconoscimento dei diritti individuali e di coppia per le persone lgbt.

Il movimento lgbt italiano sembra anestetizzato sotto i colpi congiunti dei risultati delle scorse elezioni politiche e delle laceranti polemiche interne che ormai dividono le organizzazioni praticamente su tutto. Invece di investire sovrumane energie in competizioni inutili non si è riusciti a fare quello che, a seconda di molti e non solo della nostra associazione, doveva essere fatto subito: una analisi delle ragioni della sconfitta di 10 anni buoni di politica del movimento (obiettivi, strategie, iniziative) non con l'obiettivo di colpevolizzare alcuno, ma perché potesse ripartire una nuova stagione del movimento, con nuovi obiettivi e soprattutto con nuovi strumenti di intervento.

Di fronte a questa stagnate situazione, notevolmente depressiva per chi ha a cuore i diritti delle persone lgbt nel nostro Paese, i risultati politici ottenuti dall'Associazione radicale Certi Diritti sono stati importanti:

\- si è aperto un fronte concreto di rivendicazione dei diritti delle coppie dello stesso sesso, che è quello della Campagna per l'affermazione civile, con la partnership preziosa e lungimirante della Rete Lenford. Campagna che tenta di utilizzare fino in fondo la via giudiziaria al riconoscimento dei diritti attraverso l'impegno e la testimonianza di persone e coppie, nel solco delle battaglie per i diritti civili degli anni '60 e '70;

\- si è avviata la costituzione di un nucleo forte di organizzazione nazionale per i diritti delle persone lgbt votata esclusivamente all'iniziativa politica (sociale e culturale laddove necessario) in questo settore, secondo la tradizione radicale italiana e pienamente calata nel contesto europeo;

\- si è cercato di interrompere la tradizione di perenne conflitto tra le diverse esperienze del movimento lgbt italiano almeno negli ultimi dieci anni, sottraendo deliberatamente l'Associazione da ogni tipo di polemica e cercando di costruire i rapporti con le altre organizzazioni esclusivamente su proposte concrete e iniziative politiche puntuali. E praticando la doppia tessera come forma di positiva contaminazione tra le diverse realtà del movimento;

In questo anno il supporto dei parlamentari italiani ed europei radicali (e di pochi altri) è stato fondamentale per esercitare il necessario controllo sulle attività delle Istituzioni e spronarle verso un maggiore impegno nella difesa dei diritti lgbt. In particolare l'azione dei deputati radicali per assicurare l'impegno del Governo italiano nell'appoggio alla proposta franco-europea di depenalizzazione dell'omosessualità in sede ONU e l'azione di sindacato ispettivo esercitata dagli stessi deputati sui fatti, anche gravi, che si sono verificati nel nostro paese a danno delle persone gay lesbiche e transessuali, oltre alle proposte di legge presentate all'inizio della legislatura;

Tutto questo non basta, ed i risultati fino ad ora raggiunti sono importanti ma fragili se non saranno supportati da nuove iniziative e nuovo impegno da parte di tutti e di tutte.

  
Il II Congresso dell'Associazione radicale Certi Diritti, quindi, anche alla luce della qualità del dibattito svolto e delle proposte avanzate dai rappresentanti delle organizzazioni che sono intervenute, individua per il prossimo anno le seguenti priorità:

• una grande campagna di iscrizione all'associazione che preveda come obiettivo minimo il raggiungimento dei 1000 iscritti e la costituzione di almeno un nucleo associativo in ciascuna regione italiana;

  
• un forte impegno per la diffusione e il sostegno alla campagna di affermazione civile per il riconoscimento del diritto a contrarre matrimonio per le persone dello stesso sesso, aumentando il numero di coppie che decidono di sostenere l'iniziativa con la loro scelta pubblica e promuovendola presso la comunità lgbt italiana;

• un impegno forte per il raggiungimento di una piattaforma comune di tutto il movimento lgbt unito, laicamente, nel perseguire obiettivi comuni, oltre le singole, legittime ma parziali, posizioni e idealità;

• rafforzare l'impegno in sede di Parlamento italiano ed europeo, in collaborazione con i parlamentari radicali e tutti coloro che vorranno aderire su alcuni obiettivi minimi:

a) l'adozione da parte dell'Unione Europea della nuova Direttiva contro ogni forma di discriminazione proposta dalla Commissione europea, che estenda le garanzie previste con le precedenti Direttive a tutte le persone soggette a potenziale discriminazione, battendo le resistenze dei Governi nazionali e sulla base delle proposte del Parlamento europeo;  
b) l'approvazione da parte del Parlamento italiano di norme contro l'omofobia e la transfobia;

c) l'approvazione da parte del Parlamento italiano della riforma della legge 164/1982, per una maggiore tutela delle persone transessuali

• Promuovere nel Paese e verso la classe politica iniziative per scongiurare il rischio che alle prossime elezioni europee venga a mancare una rappresentanza radicale, che è stata presente e attiva anche sui temi cari alla comunità lgbt, dal 1979 ad oggi. La cui continuazione, oggi più che mai, è necessaria.

Bologna, il 14 marzo 2009