---
title: 'IL VATICANO PREDICA  BENE E PRATICA MALE, GUARDA CASO, CONTRO TRANS E GAY'
date: Thu, 19 Feb 2009 10:33:38 +0000
draft: false
tags: [Comunicati stampa, DISCRIMINAZIONI, DURBAN, lgbt, ONU, vaticano]
---

**IL VATICANO COME SEMPRE PREDICA BENE E PRATICA MALE, ALL’ONU NUOVAMENTE ALLEATO CON I PEGGIORI PAESI DITTATORIALI: CONTRO LE PERSONE LGBT, GUARDA CASO.**

_Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:_

“Il Vaticano, che predica sempre che le persone vanno rispettate e che bisogna combattere in ogni dove le discriminazioni, alle Nazioni Unite sta facendo ‘blocco comune’ con le peggiori dittature contro la lotta alle discriminazioni verso le persone omosessuali e transgender, opponendosi alla definizione di ‘orientamento sessuale’ in un progetto di documento in corso di stesura.

In vista della Conferenza ‘Durban II” sul razzismo, che si svolgerà a Ginevra dal 20 al 24 aprile, dove peraltro alcuni paesi democratici (Canada e Israele) hanno già deciso di non partecipare, alcuni paesi africani insieme ai paesi teocratici e dittatoriali, Vaticano in testa, si sono opposti al documento contro le discriminazioni che dovrà essere adottato alla fine dei lavori.

ll documento è fortemente sostenuto da Olanda, Stati Uniti e paesi latino-americani, in particolare dove si esplicita la frase: "condannare tutte le forme di discriminazione e tutte le altre forme di violazione fondate sull'orientamento sessuale”.

Insomma, nessuna novità, nulla di nuovo riguardo l’omofobia del cupolone che predica bene, anzi sempre meglio contro gli omosessuali”.