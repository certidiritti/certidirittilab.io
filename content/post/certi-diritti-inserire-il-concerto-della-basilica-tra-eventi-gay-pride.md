---
title: 'CERTI DIRITTI: INSERIRE IL CONCERTO DELLA BASILICA TRA EVENTI GAY PRIDE'
date: Thu, 29 May 2008 13:19:40 +0000
draft: false
tags: [Comunicati stampa]
---

[http://www.gay.it/channel/attualita......](http://www.gay.it/channel/attualita/24750/Roma-Pride-Negata-piazza-SGiovanni.html)

[![Gay.it - Roma Pride: Negata piazza S.Giovanni](http://images.gay.it/foto_articoli/135x135/sa/sangiovanniprideF1.jpg "Gay.it - Roma Pride: Negata piazza S.Giovanni")](#)Manca poco, pochissimo, al corteo del [Gay Pride](keyword/117/Gay-Pride.html) romano previsto per il 7 giugno. Ma solo nella serata di ieri i rappresentanti del Circolo di Cultura Omosessuale **Mario Mieli, organizzatori dell'evento**, sono venuti a conoscenza che il percorso non potrà essere quello concordato in precedenza.

**La questura di Roma**, infatti, «ha ritirato l'autorizzazione, concessa originariamente in data 11 aprile, a concludere la parata a Piazza San Giovanni con la motivazione di un concomitante convegno e concerto corale all'interno dei Palazzi Lateranensi» fanno sapere dal Mieli.

«Del problema sul percorso - continuano gli organizzatori - siamo venuti a conoscenza soltanto oggi, durante un incontro tecnico al Comune di [Roma](keyword/650/Roma.html) e nel conseguente incontro in Questura, senza che nessuna autorità competente l'abbia comunicato prima, nonostante siano passati quasi due mesi dall'autorizzazione originaria e dall'ampia notorietà pubblica data all'evento e al percorso»

Per **Aurelio Mancuso**, presidente di Arcigay, si tratta di una «provocazione politica». «Se com’è stato riferito la modifica la percorso è dovuta al fatto che all’interno dei palazzi Lateranensi si tiene durante la giornata un Convegno internazionale, con un concerto conclusivo dentro la Basilica, ciò si prefigurerebbe come un’inutile ed incomprensibile provocazione politica.» ha detto Mancuso che si chiede: «Cosa c’entra il Pride con un Convegno clericale? Quali problemi d’ordine pubblco, potrebbero sorgere tra una parata che sfila nelle vie di [Roma](keyword/650/Roma.html) e si conclude nella serata nella storica piazza e un’iniziativa della gerarchia cattolica dentro le mura della Basilica?»

**Sergio Rovasio**, Segretario dell'Associazione Radicale Certi Diritti, annuncia che, qualora la Questura confermasse il divieto all'utilizzo di Piazza San Giovanni, proporrà agli organizzatori - e agli iscritti a Certi Diritti - di partecipare tutti al concerto previsto nella Basilica di San Giovanni la sera del 7 giugno. «Chiederemo che il concerto venga  inserito tra gli eventi del [Gay Pride](keyword/117/Gay-Pride.html) di [Roma](keyword/650/Roma.html).»