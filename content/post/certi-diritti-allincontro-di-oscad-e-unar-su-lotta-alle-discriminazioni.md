---
title: 'Certi Diritti all''incontro di OSCAD e UNAR su lotta alle discriminazioni'
date: Tue, 06 Sep 2011 08:38:23 +0000
draft: false
tags: [Politica]
---

L'associazione radicale Certi Diritti ha partecipato all'incontro di presentazione del lavoro delle forze dell'ordine. Di seguito una sintesi dei lavori.

Roma, 6 settembre 2011  
   
Comunicato Stampa dell'Associazione Radicale Certi Diritti  
   
Ieri, lunedì 5 setttembre, si è svolto a Roma, presso la Scuola Superiore di Polizia, un importante incontro promosso dall'Oscad, Osservatorio per la sicurezza contro gli atti discriminatori, alla presenza dei vertici delle forze dell'ordine, l'Unar, Ufficio Nazionale antidiscriminazioni razziali e le principali Associazioni nazionali che si battono sui temi della lotta agli atti discriminatori.  
   
L'Associazione Radicale Certi Diritti intende contribuire all'importante lavoro avviato dall'Oscad un anno fa, in stretta collaborazione con l'Unar, e ringazia tutti coloro che alla riunione di ieri hanno proposto idee e suggerimenti per migliorare le condizioni di vita delle persone più deboli, vittime di odio e violenza, nel nostro paese.  
   
Qui di seguito il link al sito della Polizia di Stato con una sintesi scritta dei lavori svolti:  
[http://www.poliziadistato.it/articolo/view/23641/](http://www.poliziadistato.it/articolo/view/23641/)