---
title: 'Comitato per i diritti umani dell''Onu all''Iran: agisca contro gravi discriminazioni e detenzione persone lgbt'
date: Tue, 29 Nov 2011 15:04:23 +0000
draft: false
tags: [Medio oriente]
---

Il coordinatore regionale per il Medio Oriente dell’International Gay and Lesbian Human Rights Commission ha scritto al governo iraniano affinchè agisca immediatamente per eliminare le discriminazioni contro le persone lgbt e per la scarcerazione delle persone detenuto a causa del loro orientamento sessuale.

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Roma, 29 novembre 2011

Il 3 novembre il Comitato delle Nazioni Unite per i diritti umani, l'unica autorità all'interno del sistema delle Nazioni Unite per valutare e monitorare gli Stati riguardo al rispetto della Convenzione Internazionale sui Diritti Civili e Politici (ICCPR), ha informato il governo iraniano che deve agire immediatamente per eliminare le discriminazioni sistematiche contro lesbiche, i gay, i bisessuali e transgender (LGBT).  
Il Comitato per i diritti umani ha sollecitato il governo iraniano ad abrogare o modificare le leggi che "potrebbero comportare l'accusa di discriminazione, persecuzione e punizione di persone a causa del loro orientamento sessuale o identità di genere”. C'è una serie di leggi discriminatorie in Iran e, tra queste, alcune criminalizzano il rapporto omosessuale e lo puniscono con la morte.  
Il Comitato per i diritti umani ha chiesto al governo iraniano di liberare senza condizioni "chiunque sia detenuto esclusivamente in conseguenza delle sue attività sessuali o tendenze sessuali attuate liberamente e di comune accordo ." A causa del sistema giudiziario opaco e corrotto dell'Iran, il numero esatto delle persone trattenute in stato di detenzione sulla base di atti omosessuali è sconosciuta, tuttavia anche una sola persona incarcerata su questa base costituisce una violazione dei diritti fondamentali alla riservatezza e alla non discriminazione.

Il Comitato dei diritti dell'uomo ha invitato il governo iraniano a  "prendere tutte le misure legislative, amministrative e di altro tipo per eliminare e vietare la discriminazione sulla base dell'orientamento sessuale". I riferimenti del Comitato sono alle diverse sfere della vita, tra cui "l'occupazione, l'alloggio,l' istruzione e l'assistenza sanitaria "e lo Stato è invitato a garantire che le persone GLBT siano" protette dalla violenza e dall'esclusione sociale ".  
  

**Qui di seguito il testo della dichiarazione di Hossein Alizadeh, Coordinatore Regionale per il Medio Oriente dell’International Gay and Lesbian Human Rights Commission**

New York, 4 novembre 2011

"Per anni, le autorità iraniane hanno commesso atrocità contro persone lesbiche, gay, bisessuali e transgender, hanno incitato gli altri alla violenza, e hanno rifiutato di ammettere che le persone LGBT iraniane esistono", ha detto Hossein Alizadeh, Coordinatore Regionale per il Medio Oriente e l'Africa del Nord presso l'International Gay and Lesbian Human Rights Commission (IGLHRC).

Nelle Osservazioni conclusive dalla sua terza revisione periodica della Repubblica islamica dell'Iran, il Comitato delle Nazioni Unite per i diritti umani  ha affermato l'importanza della condotta del governo nella  violazione delle leggi internazionali che ha accettato di sostenere. " Uno Stato che si vanta come l'Iran di tradizioni e di moralità, deve agire ora immediatamente per garantire che le sue definizioni di tradizione e moralità sono in conformità con i principi fondamentali del diritto internazionale dei diritti umani".

"Il Comitato delle Nazioni Unite per i diritti umani ha inviato un forte messaggio al governo iraniano perché il suo trattamento delle persone lesbiche, gay, bisessuali e transessuali rappresenta una sistematica violazione dei diritti umani attuata col mancato rispetto dei suoi obblighi previsti dal trattato. Il Comitato ha chiesto al governo iraniano di far circolare l'informazione  sulle sue osservazioni conclusive al potere giudiziario iraniano, al governo e alla società civile con ampiezza. Previa consultazione con la società civile, il governo dovrà presentare una relazione sull'attuazione delle raccomandazioni contenute nelle Osservazioni Conclusive del Comitato. Il Comitato ha espressamente chiesto al governo iraniano di includere nella sua prossima revisione periodica informazioni dettagliate sul godimento da parte dei membri della comunità lesbica, gay, bisessuali e transgender dei diritti sanciti nel Trattato ".

“Noi della Commissione International Gay and Lesbian Human Rights siamo fiduciosi e pieni di speranza per i continui progressi nel campo dei diritti umani per tutti, ovunque. Siamo orgogliosi del lavoro in collaborazione di IGLHRC e dei nostri partner iraniani, l'Organizzazione iraniana Queer (IRQO), nel contribuire a questo importante passo avanti attraverso il nostro Shadow Report congiunto dal titolo: <Violazioni dei diritti umani sulla base dell'orientamento sessuale, dell'identità di genere e omosessualità nella Repubblica islamica dell'Iran e testimonianza davanti al Comitato delle Nazioni Unite per i diritti umani>".