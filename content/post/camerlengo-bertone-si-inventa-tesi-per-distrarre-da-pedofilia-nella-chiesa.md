---
title: 'CAMERLENGO BERTONE SI INVENTA TESI PER DISTRARRE DA PEDOFILIA NELLA CHIESA'
date: Tue, 13 Apr 2010 12:21:01 +0000
draft: false
tags: [Comunicati stampa]
---

**PEDOFILIA CHIESA CATTOLICA: TARCISIO BERTONE FAREBBE MEGLIO A OCCUPARSI DELLE MIGLIAIA DI CASI DI PEDOFILIA DENTRO LA CHIESA CATTOLICA ANZICHE’ DENIGRARE LE PERSONE OMOSESSUALI.**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

“La tesi del Cardinale Tarcisio Bertone, Segretario di Stato Vaticano e Camerlengo di Santa Romana Chiesa, secondo la quale ‘esiste un legame tra omosessualità e pedofilia’ è un tentativo maldestro e volgare di denigrare le persone omosessuali, per giunta senza citare le fonti che evidentemente si è inventato.

Il dirigente dello Stato teocratico Vaticano sostiene tesi che non hanno alcuna base scientifica. Egli cerca di distrarre l’attenzione dell’opinione pubblica dai gravissimi casi di pedofilia avvenuti dentro la chiesa cattolica e nascosti con veri e propri atteggiamenti criminali alle autorità giudiziarie di molti paesi del mondo.

In Italia, la stragrande maggioranza degli abusi sui minori avviene da parte di padri, nonni, zii, conviventi della madre, in contesti di vita familiare fatta di casa, lavoro, chiesa, relazioni sociali.

Consigliamo al Camerlengo Bertone di leggersi i Rapporti nazionali sulla condizione dell’infanzia e dell’adolescenza curati dal Telefono Azzurro e da Eurispes, anziché dire parole a vanvera sull’associazione pedofilia-omosessualità. Infatti, nella stragrande maggioranza dei casi, le vittime di pedofilia sono minori di sesso femminile (4 su 5) e le percentuali di questo fenomeno, tra il 65% e il 90%, avviene all’interno della famiglia. Addirittura sono molti i casi di padri separati e/o conviventi della madre ad essere responsabili di abusi sessuali sui minori. Per non parlare appunto dei casi che avvengono dentro la chiesa.

Il Camerlengo Bertone farebbe bene a citare le sue fonti altrimenti le sue tesi diventano pura calunnia, promozione di odio e violenza omofobica".