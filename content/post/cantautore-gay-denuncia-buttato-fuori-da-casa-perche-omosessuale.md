---
title: 'Cantautore gay denuncia: «Buttato fuori da casa perchè omosessuale»'
date: Sun, 13 Jun 2010 15:26:48 +0000
draft: false
tags: [Senza categoria]
---

[http://roma.corriere.it/roma/n...5.shtml](http://roma.corriere.it/roma/notizie/cronaca/10_giugno_13/gay-sfrattato-casa-1703192172205.shtml)

### I CARABINIERI PROCEDONO PER VIOLAZIONE DI DOMICILIO

Viveva al Pigneto in una casa in affitto: «Hanno buttato le mie cose nei sacchi e cambiato serratura»
-----------------------------------------------------------------------------------------------------

![Emilio Rez (dal web)](http://images.roma.corriereobjects.it/media/foto/2010/06/13/reznew--140x180.jpg "Emilio Rez (dal web)")

Emilio Rez (dal web)

ROMA - Il 19 agosto del 2009 era stato insultato ed aggredito nella Capitale perchè gay. Ora denuncia di essere stato sfrattato da casa. Secondo le associazioni per i diritti degli omosessuali si tratterebbe di un nuovo episodio di discriminazione, ma la storia ha contorni poco chiari.  
Tre giorni fa Emilio Rez - un giovane 25enne, che fa il cantautore - è tornato nel suo appartamento in affitto al Pigneto, periferia sud di Roma, ed ha trovato parte delle sue cose sulle scale in sei sacchi neri. La serratura dell'abitazione era stata cambiata.

AFFITTO DI 40 METRI - Tutto ciò, sostiene il protagonista, perchè è omosessuale. Eppure i proprietari dell'immobile sapevano delle sue inclinazioni fin dall'inizio del rapporto di locazione. Secondo l'associazione radicale «Certi diritti», da ottobre scorso Rez era in affitto in un appartamento di 40 metri quadrati a 600 euro al mese. Il 10 giugno ne sarebbe stato cacciato.

«Avevo chiesto di registrare il contratto, ma la proprietaria, una donna di 70 anni, mi aveva detto che se lo volevo registrare avrei dovuto pagarne 900 - racconta ora Rez -. Ho sempre pagato regolarmente, qualche tempo fa ho chiesto di poter prendere visione del regolamento condominiale e la proprietaria si è infastidita».

![La copertina del singolo «Tremenda» di Emilio Rez (dal  web)](http://images.roma.corriereobjects.it/media/foto/2009/08/28/rez--180x140.jpg "La copertina del singolo «Tremenda» di Emilio Rez (dal  web)")

La copertina del singolo «Tremenda» di Emilio Rez (dal web)

GIOIELLI E COMPUTER - Quanto all'omosessualità, il giovane sostiene: «Poi deve aver saputo che ero gay, lei e la sua famiglia hanno cominciato a farmi 20-30 telefonate al giorno di minacce ed offese sulla mia omosessualità dicendo frasi come 'Sei un frocio di m.... con noi caschi malè». Fino a quando una delle figlie, ha raccontato il cantautore, «Mi ha detto: 'un giorno di questi imbocco dentro casa tua e cambio la serratura... e così di fatto è stato».  
«Solo che hanno preso i miei abiti di scena, i miei gioielli, i miei Pc - prosegue Rez -, i televisori ed i microfoni per un valore che il maresciallo ha quantificato sui 40mila euro. Ed ora sono senza casa e senza le mie cose e non ho più nemmeno gli strumenti per fare il mio lavoro». L'associazione «Certi diritti» ha reso noto che è stata «sporta una denuncia ai carabinieri per i reati di violazione di domicilio, appropriazione indebita e minacce».