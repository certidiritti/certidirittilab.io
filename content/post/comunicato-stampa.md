---
title: 'COMUNICATO STAMPA'
date: Tue, 23 Mar 2010 19:42:33 +0000
draft: false
tags: [Senza categoria]
---

CORTE COSTITUZIONALE: STUDENTI ISTITUTO TECNICO DI CALTANISSETTA ASSISTONO A UDIENZA SU MATRIMONIO GAY. PLAUSO ALLA SCUOLA DA PARTE DELL'ASSOCIAZIONE RADICALE CERTI DIRITTI

L'Associazione Radicale Certi Diritti, che sta assistendo con una sua rappresentanza all'udienza in Corte Costituzionale relativamente alla questione di legittimità del matrimonio tra persone dello stesso sesso, plaude all'Istituto Tecnico Mottura di Caltanissetta, che con 41 dei suoi studenti sta assistendo al dibattimento. "Plaudiamo all'iniziativa dell'Istituto Tecnico Mottura," affermano i rappresentanti di Certi Diritti, "che ha dato l'opportunità ai suoi studenti di presenziare a una sentenza storica per l'Italia e per i suoi cittadini; è con questo senso civico che un istituto scolastico dovrebbe rapportarsi alla società, mettendo in primo piano il confronto, l'informazione e la sensibilizzazione su tematiche che riguardano il principio di eguaglianza e di non discriminazione tra tutte le persone. Un messaggio positivo e incoraggiante" continuano, "rispetto a quello del liceo Don Milani di Romano di Lombardia (BG), che qualche giorno fa ha bloccato, per opera dei genitori, un'incontro propedeutico alla non discriminazione delle persone omosessuali.