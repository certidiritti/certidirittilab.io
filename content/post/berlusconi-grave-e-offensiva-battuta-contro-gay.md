---
title: 'BERLUSCONI, GRAVE E OFFENSIVA BATTUTA CONTRO GAY'
date: Tue, 02 Nov 2010 11:17:07 +0000
draft: false
tags: [Comunicati stampa]
---

### BERLUSCONI, GRAVE E OFFENSIVA BATTUTA CONTRO GAY. SIT-IN ORE 18 DAVANTI A PALAZZO CHIGI

**BERLUSCONI: LA SUA DISPERAZIONE POLITICA DIVENTA DEMAGOGIA E OFFESA AI CITTADINI. E’ VERGOGNOSO QUANTO DICHIARATO,  SEGNO DI FORTE DEBOLEZZA.**  
**SIT-IN DAVANTI A PALAZZO CHIGI, OGGI, DALLE ORE 18.**  
  
Comunicato stampa dell’Associazione Radicale Certi Diritti:

Roma, 2 novembre 2010

  
   
È molto grave quanto dichiarato oggi da Berlusconi, che denigra e offende le persone gay. La sua disperazione politica, causata dalla sua incapacità di governare impulsi e comportamenti, non certo degni di un Presidente del Consiglio, non può giustificare battute da osteria, che necessariamente alimentano la violenza e la discriminazione verso le persone gay nel nostro paese. 

Troviamo inaccettabile e gravissimo quanto da lui dichiarato oggi: battuta idiota che si aggiunge ai già molto gravi comportamenti politici di totale indifferenza alle richieste di leggi di riforma del diritto di famiglia riguardo i diritti civili e umani delle persone lgbt(e) nel nostro paese.

  
Alle ore 18 di oggi, martedì 2 novembre, saremo davanti a Palazzo Chigi per un sit-in insieme a parlamentari,  associazioni e cittadini che si battono per il superamento delle diseguaglianze nel nostro paese.