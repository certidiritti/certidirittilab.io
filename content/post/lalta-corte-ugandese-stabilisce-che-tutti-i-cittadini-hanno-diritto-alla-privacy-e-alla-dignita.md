---
title: 'L''ALTA CORTE UGANDESE STABILISCE CHE TUTTI I CITTADINI HANNO DIRITTO ALLA PRIVACY E ALLA DIGNITÀ'
date: Wed, 12 Jan 2011 23:19:28 +0000
draft: false
tags: [Comunicati stampa]
---

Bruxelles-Kampala-Roma, 6 gennaio 2011

comunicato stampa di **Non C’è Pace Senza Giustizia** (NPSG), Associazione Radicale **Certi Diritti**, **Partito Radicale Nonviolento, Transnazionale e Transpartito** (PRNTT) e **Sexual Minorities Uganda** (SMUG).

Accogliamo con entusiasmo la sentenza dell’Alta Corte ugandese che stabilisce che  la pubblicazione da parte del giornale ugandese “The Rolling Stone” di nomi, indirizzi e luoghi di ritrovo di persone accusate di essere LGBTI, e l’incitamento al loro linciaggio, costituiscono una violazione della Costituzione ugandese.

ell’Alta Corte che sottolinea come il diritto alla vita e alla dignità delle minoranze sessuali siano stati violati. Questa sentenza, infatti, garantisce la protezione ad altri cittadini ugandesi omosessuali, o percepiti come tali; oltre a fornire un importante precedente per ogni altro tentativo, da parte dei media, di attaccare o minacciare i cittadini per la loro presunta identità di genere o per il loro orientamento sessuale.

La sentenza dell’Alta Corte ugandese, insieme alla recente decisione di mantenere il riferimento all’orientamento sessuale nella risoluzione dell’Assemblea Generale dell’ONU contro le esecuzioni extragiudiziali e alla risoluzione del Parlamento Europeo sui diritti LGBTI in Uganda dello scorso dicembre, costituiscono un chiaro messaggio secondo il quale la negazione dei diritti delle persone LGBTI come strumento politico non sarà tollerato né a livello nazionale né a livello internazionale.

NPSG, Certi Diritti, il PRNTT e la SMUG denunciano l’aumento delle molestie, della violenza e delle minacce che le persone LGBTI stanno vivendo in Uganda dalla pubblicazione del primo articolo sul giornale “The Rolling Stone” e si congratulano con la Coalizione della Società Civile per i Diritti Umani e la Legge Costituzionale per la loro vittoria di fronte all’Alta Corte ugandese. Credono, infine, che sia arrivato il momento per il Governo dell’Uganda di dare chiare rassicurazioni alla gente ugandese, da qualsiasi parte essa venga, sul fatto che verrà loro garantita la protezione da parte dello Stato e della legge contro ogni tipo di minaccia e violenza relativa al loro reale o presunto orientamento sessuale.