---
title: 'Pubblicità sessista a Milano? Paranoia, non si insulti libertà sessuale cittadini'
date: Thu, 22 Feb 2018 14:19:40 +0000
draft: false
tags: [Politica]
---

![28059269_10215006562905113_5835093217487674223_n](http://www.certidiritti.org/wp-content/uploads/2018/02/28059269_10215006562905113_5835093217487674223_n-300x169.jpg)“Un uomo che prende da dietro una donna tenendola per i capelli: agli occhi di alcuni certo sesso è diventato ‘sessista’ e la polemica, dopo aver travolto Napoli ed essere costata la rimozione dei cartelloni pubblicitari, adesso sembra aver toccato Milano. Grazie per la fatica non richiesta a censori di ogni genere, ma i giudizi sulla “dignità” delle persone in base a posizioni e pratiche sessuali se li tengano per la loro di vita.” Così **Leonardo Monaco** e **Barbara Bonvicini**, rispettivamente segretari dell’**Associazione Radicale Certi Diritti** e dell'**Associazione Enzo Tortora Radicali Milano**, candidati nelle liste di **Più Europa con Emma Bonino**. “In un periodo caratterizzato da un dibattito complesso attorno al tema delle molestie sessuali – concludono i due – la sessualità libera e consapevole delle persone non deve essere messa sotto attacco dalle paranoie sessuofobiche del moralista di turno”