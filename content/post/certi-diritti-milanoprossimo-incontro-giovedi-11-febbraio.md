---
title: 'CERTI DIRITTI MILANO:PROSSIMO INCONTRO GIOVEDì 11 FEBBRAIO'
date: Sat, 06 Feb 2010 06:23:16 +0000
draft: false
tags: [Senza categoria]
---

Sarà una serata importante, una vera e propria "chiamata alle armi" (le armi della nonviolenza, beninteso), perché il 23 Marzo ci sarà la tanto sospirata sentenza della Corte Costituzionale sulle coppie di Affermazione Civile e sul loro diritto a contrarre matrimonio civile.

Un momento di civiltà e democrazia che possiamo vivere in modo corale: tutti noi possiamo dare un contributo importante, poco importa se siamo eterosessuali o omosessuali, sposati, fidanzati o single, avvocati o panettieri.

Prepariamoci insieme a questo giorno e godiamocelo!

L'appuntamento perciò è sempre alle 21, via Malachia de' Taddei 10 (MM1), presso la sede dei radicali a Milano.

Oltre alla sentenza della Corte Costituzionale, Marco ci parlerà di "L'Amore Spiazza", il pomeriggio a Magenta organizzato per il 14 Febbraio da tutte le associazioni LGBT Milanesi.

Infine, Francesco e Luca ci aggiorneranno brevemente sulle prossime votazioni regionali.

Invitate tutte le persone di buona volontà!

Gian Mario, Luca e tutte le coppie Milanesi di Affermazione Civile!  
(per info 329-9045945)