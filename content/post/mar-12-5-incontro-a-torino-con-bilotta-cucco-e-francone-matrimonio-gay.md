---
title: 'MAR 12-5 INCONTRO A TORINO CON BILOTTA, CUCCO E FRANCONE : MATRIMONIO GAY'
date: Sat, 09 May 2009 14:09:09 +0000
draft: false
tags: [Comunicati stampa]
---

**martedì 12 maggio 2009, ore 20,30**

**Sala dell'antico Macello di Po**

**Via Matteo Pescatore 7, Torino**   

**Incontro sul tema:**   

**Il matrimonio**

**tra persone dello stesso sesso:**

**POSSIBILE?!?** 

_partecipano_ 

**Francesco Bilotta**

curatore del volume "Le unioni tra persone dello stesso sesso", Mimesis Edizioni, 2009 

**Enzo Cucco ed Enzo Francone,**

Associazione radicale Certi Diritti 

**Una occasione per discutere di diritti delle persone e delle coppie lgbt nel nostro Paese e dei modi che abbiamo per rivendicarli. In particolare della iniziativa di affermazione civile per il riconoscimento al matrimonio civile per le coppie di persone dello stesso sesso.** 

_L’incontro fa parte del Programma di attività in occasione della Giornata mondiale contro l’omofobia 2009 ed in occasione del Torino Pride 2009._   
 

**Ingresso libero e raccomandato per tutti e tutte!**

**Per informazioni 011.5212033 – [torino@certidiritti.it](mailto:torino@certidiritti.it)**