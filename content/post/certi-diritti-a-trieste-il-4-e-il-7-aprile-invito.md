---
title: 'CERTI DIRITTI A TRIESTE IL 4 E IL 7 APRILE: INVITO'
date: Wed, 30 Nov -0001 00:00:00 +0000
draft: false
tags: [certi diritti, Comunicati stampa, elezioni, presentazione, trieste]
---

INVITO DI CERTI DIRITTI PER DUE IMPORTANTI EVENTI A TRIESTE IL 4 E IL 7 APRILE

Venerdì 4 aprile alle ore 20 al Caffè San Marco di via Battisti a Trieste. l’Associazione radicale Certi Diritti organizza un incontro dal titolo NUOVI DIRITTI. PER UN’ITALIA LAICA ED EUROPEA.

Saranno presenti i candidati al Partito Democratico Maria Antonietta Farina Coscioni per la Camera e Fabio Omero per il Senato. Ospite il prof. Francesco Bilotta giurista  
Presenta e modera l’incontro Clara Comelli presidente dell’Associazione Certi Diritti

Lunedì 7 aprile alle 17.30 presso la Libreria Feltrinelli di Via Mazzini a Trieste,presentazione dell’Associazione radicale Certi Diritti.  
Saranno presenti: Prof. Francesco Remotti, antropologo , autore del libro “Contronatura – Una lettera al Papa” ed. Laterza; la prof.ssa Chiara Lalli, autrice del libro”Dilemmi della bioetica” ed.Liguori e il prof. Francesco Bilotta docente di diritto privato all’università di Udine  
Interverrà Maria Antonietta Farina Coscioni candidata alla Camera per il Partito Democratico e  
Copresidente dell’Associazione Luca Coscioni  
Modera gli interventi la Presidente dell’Associazione Certi Diritti Clara Comelli