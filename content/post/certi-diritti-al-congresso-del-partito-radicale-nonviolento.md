---
title: 'CERTI DIRITTI AL CONGRESSO DEL PARTITO RADICALE NONVIOLENTO'
date: Thu, 17 Feb 2011 11:15:47 +0000
draft: false
tags: [Comunicati stampa]
---

Parteciperanno ai lavori una delegazione di esponenti del movimento lgbt ugandese ed altri esponenti impegnati sui temi della promozione e della difesa dei diritti civili e umani delle persone lgbt(e).

Roma, 17 febbraio 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

i

In occasione del 39° Congresso del **Partito Radicale Nonviolento**, che si svolgerà a Chianciano dal 17 al 20 febbraio 2011, l’Associazione Radicale **Certi Diritti**, insieme  all’Ong **Non c’è Pace Senza Giustizia**, ha invitato ai lavori una delegazione di esponenti del movimento lgbt ugandese insieme ad altri esponenti impegnati sui temi della promozione e della difesa dei diritti civili e umani delle persone lgbt(e).

Durante i lavori verranno presentate le attività dell’Associazione ed elaborate alcune proposte che si svolgeranno in occasione dell’Europride di Roma del giugno 2011. Si terranno anche relazioni riguardo l’attività svolta dall’Intergruppo lgbt al Parlamento Europeo e delle campagne svolte in altri paesi dove le persone lesbiche e gay sono perseguitate e criminalizzate.  
  
Ai lavori parteciperanno, tra gli altri: Pepe Onziema, - Human Rights Defender - Program Coordinator di SMUG, **Sexual Minorities Uganda**; John Francis Onyango, Avvocato difensore di David Kato Kisule; Usaam Auf Mukwaya, esponente della comunità Lgbt ugandese che ha chiesto e ottenuto asilo politico in Francia;  Elio Polizzotto, LGBTI Program Coordinator, dirigente dell’Ong ‘No Peace Without Justice’; **Stefano Fabeni**,  Director, Global LGBT Advocacy Heartland Alliance for Human Needs and Human Rights - Washington; **Paolo Patanè**, Presidente nazionale di Arcigay; **Ottavio Marzocchi**, funzionario della Commissione Libertà Pubbliche e membro dell’Intergruppo Lgbt al Parlamento Europeo, Sergio Rovasio, Segretario Associazione Radicale Certi Diritti, Giacomo Cellottini, Tesoriere dell’Associazione Radicale Certi Diritti; Maria Gigliola Toniollo, Cgil Nuovi Diritti.  
  
Molti, come di consueto, anche temi e ospiti protagonisti della politica internazionale. Tra gli interventi previsti sin da giovedì 17 febbraio è previsto quello del dirigente Radicale e leader antischiavista mauritano **Biram Dah Sbeid**. Nei giorni scorsi è stato graziato dal Presidente della Repubblica e l'altro ieri è uscito dal carcere dove era stato rinchiuso lo scorso dicembre, insieme ad altri militanti dell'IRA (Initiative de Résurgence Abolitionniste), per aver denunciato la condizione di schiavitù di due bambine prigioniere di un'alta dirigente di banca, in Mauritania. E per la cui liberazione i Radicali si sono battuti a lungo, con missione dello stesso Pannella in Mauritania.  
  
Al 39° Congresso dei Radicali, inoltre prenderanno parte oltre ad Emma Bonino, Marco Pannella, anche parlamentari e militanti dei diritti umani provenienti da tutto il mondo, appartenenti a diversi orientamenti politici, culturali e religiosi, tra i quali Rebiya KADEER, la "guerriera gentile" leader del popolo Uighuro, che vive in esilio negli Stati Uniti; il Presidente del Parlamento tibetano in esilio Penpa TSERING, il leader dei Montagnard dell\`altopiano indocinese Kok KSOR, l'ex direttore dell\`ufficio del Rabbino Capo di Israele Dov HALBERTAL, l\`ex ministro degli esteri croato Tonino PICULA, parlamentari ed esponenti politici provenienti da tutta Europa, dai paesi arabi dell\`area mediterranea, dai Balcani, dal Caucaso, dall\`Asia centrale e dall\`Africa. Per l\`Italia, è previsto l\`intervento del sottosegretario agli Esteri, con delega ai diritti umani, Vincenzo Scotti.