---
title: 'A che punto siamo?'
date: Fri, 14 Feb 2014 14:10:45 +0000
draft: false
---

#### [![logo-affermazione-civile](http://www.certidiritti.org/wp-content/uploads/2014/02/logo-affermazione-civile.jpg)](http://www.certidiritti.org/wp-content/uploads/2014/02/logo-affermazione-civile.jpg)AFFERMAZIONE CIVILE – IL CONTESTO GIURIDICO ATTUALE

Dopo anni di rodaggio, possiamo dire che le campagne giuridiche avviate in Italia per raggiungere obiettivi di uguaglianza stanno avendo gli effetti previsti, nello stato di diritto e anche nella vita sociale e politica del paese. Questo è stato possibile principalmente per un motivo: **quello giuridico, è un processo che fino ad oggi si è mostrato essere graduale, ma che nondimeno è sempre stato percorso consolidando senza compromessi il principio di uguaglianza:** il solo che può garantire il pieno riconoscimento della dignità di ogni cittadino davanti alla legge e alla società.

Ma procediamo con ordine. La sentenza 138/2010 della Corte costituzionale, resta l’esito più importante, ed è il risultato della fase iniziale di Affermazione Civile, condotta da Certi Diritti insieme agli avvocati di Rete Lenford. Sono tutte le sentenze che si sono susseguite nel 2012 ad averla confermata una pietra miliare e un punto di riferimento solido. Specialmente quando afferma _“la necessità di un trattamento omogeneo tra la condizione della coppia coniugata e quella della coppia omosessuale, trattamento che questa Corte può garantire con il controllo di ragionevolezza”._

E così, il Tribunale di Reggio Emilia cita questa sentenza nell’ordinare alla prefettura il rilascio del permesso di soggiorno (previsto per i coniugi di cittadini di  Paesi membri dell’Unione europea) ad una coppia dello stesso sesso italo-uruguayana sposata in Spagna (sentenza del 13/02/2012).

La Cassazione, allo stesso modo, cita la stessa sentenza per riaffermare che _“I componenti della coppia omosessuale, conviventi in stabile relazione di fatto \[…\] possono adire i giudici comuni per far valere, in presenza di specifiche situazioni, il diritto ad un trattamento omogeneo a quello assicurato dalla legge alla coppia coniugata.”_ (sentenza 4184 del 15 Marzo 2012).

Infine, la Corte di Appello di Milano, cita entrambe le sentenze della Corte costituzionale e della Cassazione per affermare che la cassa mutua di categoria non può negare il diritto alle prestazioni assistenziali alla coppia dello stesso sesso stabilmente convivente. (sentenza 7176 del 31/08/2012)[![1](http://www.certidiritti.org/wp-content/uploads/2014/02/1.png)](http://www.certidiritti.org/wp-content/uploads/2014/02/1.png)

**Anche la politica segna il passo** e ormai, pure continuando a riproporre il falso principio secondo il quale i matrimoni gay non sarebbero costituzionali, non riesce più a darsi credibilità agli occhi di una **opinione pubblica** che è sempre meno disposta a subire acriticamente le istanze elettoralistiche della discriminazione omofobica.

#### AFFERMAZIONE CIVILE – LA NOSTRA STRATEGIA

E’ in questo contesto che si focalizzano i nuovi obiettivi dell’Associazione Radicale Certi Diritti, all’interno della campagna di Affermazione Civile.

Da un lato, sarà importante continuare a **fare in modo che le Corti si esprimano sugli specifici diritti** per i quali le coppie dello stesso sesso stabilmente conviventi abbiano diritto **ad un trattamento uguale a quello delle coppie sposate**: la pensione, il lavoro, le tasse, la salute ecc... Su tutti questi diritti dovrà essere vagliato e spiegato un principio di ragionevolezza che rafforzerà inevitabilmente lo stato di diritto di tutte le coppie dello stesso sesso.

In secondo luogo, occorrerà **scardinare i (sempre più fragili) paradigmi discriminatori che ancora resistono nei giudizi delle corti.** In riferimento alla sentenza della Cassazione, noi crediamo fermamente che le persone sposate all’estero abbiano il diritto di vedere trascritto in Italia il proprio matrimonio contratto all’estero, e di vedersi riconosciuto lo stato civile di “Coniugato” o “Coniugata”. Anche per questo, continueremo a batterci (in questo caso fuori dalle aule dei tribunali) anche affinché venga ritirata la circolare Amato che vieta la trascrizione dei matrimoni contratti all’estero da persone dello stesso sesso.

In riferimento alla sentenza della Corte costituzionale, **noi crediamo fermamente che vietare a due persone di sposarsi solo perché sono dello stesso sesso sia un atto discriminatorio.** E siamo soprattutto convinti che sia possibile arrivare ad una evidenza giuridica di queste nostre convinzioni.

Forse, cosa più importante di tutte, crediamo **che i traguardi raggiunti e i metodi impiegati** in questa campagna giudiziaria che chiamiamo “Affermazione Civile” costituisce un vero tesoro di civiltà al quale la politica, volente o nolente non può non attingere e un faro di giustizia civile e sociale verso il quale anche le posizioni più omofobe e resistenti non possono non orientarsi se vogliono evitare il suicidio.[![2](http://www.certidiritti.org/wp-content/uploads/2014/02/2.png)](http://www.certidiritti.org/wp-content/uploads/2014/02/2.png)

#### UN CASO PARTICOLARE - LA LIBERA CIRCOLAZIONE E LA CARTA DI SOGGIORNO

In tutto questo, meritano un approfondimento particolare gli sviluppi attuali a seguito della sentenza del Tribunale di Reggio Emilia sulla libera circolazione.

Prima di tutto questa sentenza è importante perché è **la prima sentenza definitiva che è stata tecnicamente vinta sul tema delle discriminazioni delle famiglie LGBT**. Inoltre, a seguito della sentenza, per la prima volta le istituzioni dello Stato hanno rilasciato un documento ufficiale che riconosce l’esistenza di una famiglia composta da due persone dello stesso sesso.

I passi successivi a questa vittoria sono quelli di **consolidare la sentenza** in modo tale che i principi espressi dal giudici possano essere, quanto prima possibile, fruibili non necessariamente attraverso un ricorso al giudice (prassi certamente eccezionale e inadeguata ad un diritto) ma semplicemente attraverso procedure di ordinaria amministrazione.

Fortunatamente, dopo la sentenza, siamo stati contattati da tantissime coppie dello stesso sesso (e anche eterosessuali!) che adesso sono maggiormente in grado di riconoscere quei casi in cui il rifiuto di un permesso di soggiorno può essere una discriminazione.

Per questo ci è stato possibile aiutare queste coppie a formulare richieste di permessi di soggiorno in varie Questure italiane, valorizzando anche quelle coppie che si sono costituite in modo leggermente differente dalla coppia pilota: magari perché hanno contratto un matrimonio fuori dall’UE, o perché si sono uniti tramite una Unione Civile ecc...

E i primi risultati già ci sono stati: ci sono Questure, come quella di Milano, Roma, Treviso, Rimini  che – a seguito della sentenza – hanno rilasciato i permessi di soggiorno a coppie dello stesso sesso, senza rendere necessario il ricorso al giudice.

Il Ministero dell’Interno ha anche emanato una Circolare il 26 ottobre 2012 in cui, facendo riferimento alla Sentenza della Corte Costituzionale 138/10 e all’ordinanza del Tribunale di Reggio Emilia, legittima il rilascio della **Carta di soggiorno per familiari di cittadini comunitari** al componente extra EU di famiglie dello stesso sesso. E’ questo un importante riconoscimento sebbene soltanto ai fini del soggiorno regolare in Italia.

Dopo la sentenza della Corte costituzionale 138/10 e della Cassazione 4184/2012 la campagna di Affermazione civile si muove in due direzioni: da una parte continuare ad operare per ottenere  il riconoscimento del matrimonio tra persone dello stesso sesso rivolgendosi anche a Corti europee (due coppie si sono rivolte alla CEDU), e dall’altra, quella di riuscire ad ottenere sempre più diritti in Italia facendo in modo che i Tribunali si esprimano su questioni specifiche legate alla  discriminazione delle coppie dello stesso sesso.

E’ proprio in questo secondo filone che l’Associazione, quest’anno, ha ottenuto i risultati migliori.

Il 13.2.12 il Tribunale di Reggio Emilia si è espresso a favore della concessione della **Carta di Soggiorno come familiare di cittadino comunitario** per un ragazzo uruguayano sposato in Spagna con un italiano. I due ragazzi sono stati seguiti da noi e dall’Avv. Giulia Perin.

In questo caso le normative di riferimento sono quelle sulla libera circolazione in Europa dei cittadini comunitari e dei loro familiari **(Decreto Legislativo n.30/2007)**. Dopo questa sentenza divenuta definitiva, circa 70 coppie ci hanno contattato per ottenere informazioni e/o iniziare il percorso che consente, al partner non italiano, di ottenere  il titolo di soggiorno di 5 anni (a cui seguirà quello a tempo indeterminato).

Fino ad ora sono 25 le coppie che abbiamo seguito e che hanno ottenuto la carta di soggiorno per familiare di cittadino europeo. Tra queste sono diverse le tipologie: coppie con matrimonio celebrato in Europa, in un Paese extraeuropeo e unione civile. Le Questure che l’hanno concessa sono: Milano, Roma, Rimini, Treviso, Cagliari, Firenze, Varese, Verona, Lucca, Palermo. Alcune Questure hanno sollevato delle obiezioni, ma le argomentazioni contenute nelle Memorie ex art. 10 bis L. n. 241/1990  che abbiamo  presentato sono sempre state accolte e le carte rilasciate.

A fronte di un quesito posto dalle Questure di Firenze - Pordenone, **il Ministero dell’Interno il 26.10.12 ha emanato una Circolare** in cui, facendo riferimento alla Sentenza della Corte Costituzionale 138/10 e all’ordinanza del Tribunale di Reggio Emilia, ha legittimato il rilascio della **Carta di soggiorno per familiari di cittadini comunitari** al componente extra EU ( o comunitario a  carico) di coppie dello stesso sesso. **E’ questo un importante riconoscimento, sebbene soltanto ai fini del soggiorno regolare in Italia, che ha costretto le istituzioni a concedere per la prima volta un documento che riconosce le coppie dello stesso sesso.**

Per rendere accessibili a tutti/e  i risultati che abbiamo ottenuto è stata realizzata e diffusa  la guida “**Informazioni per ottenere la “Carta di soggiorno per familiari di cittadini europei” (Decreto  Legislativo 30/2007) per coppie miste dello stesso sesso”** e una seconda guida che fornisce le **indicazioni pratiche per poter celebrare il matrimonio in Portogallo,** unico Paese europeo (con la Norvegia) che non richiede la residenza per celebrare il matrimonio**.**

Premettendo che non sempre è facile trovare delle coppie disponibili ad iniziare un percorso di Affermazione civile ( o che subiscono e non accettino una discriminazione rispetto ad una coppia eterosessuale) questi sono le azioni legali in itinere:

**Tribunale di Milano**, il 17 maggio ci sarà la prima udienza a fronte del diniego di concessione ad una coppia dello stesso sesso degli assegni familiari. Sempre a Milano verrà depositato in questi giorni un ricorso al diniego dell’ASL di iscrivere al SSN la partner comunitaria **a carico** della compagna italiana (coppia che ha sottoscritto in Inghilterra una unione civile). Un istituto bancario in Lombardia ha concesso la licenza matrimoniale ad un ragazzo che prossimamente celebrerà il matrimonio in Portogallo.

Sono in corso di definizione e istruzione altre cause pilota su specifici diritti di coppie dello stesso sesso conviventi e/o che abbiano celebrato il matrimonio all’estero.

Nell’attesa ormai biblica che anche l’Italia si allinei con i Paesi europei, l’aprile scorso abbiamo pubblicato il libro **“Certi diritti che le coppie conviventi non sanno di avere”** un vero e proprio manuale su come tutelare le famiglie che non vogliono o non possono accedere all’istituto matrimoniale. Questo libro si aggiunge a quello editato nel novembre 2011 “**Dal cuore delle coppie al cuore del diritto. L'udienza 138/2010 alla Corte costituzionale per il diritto al matrimonio tra persone dello stesso sesso”.**

Infine è stato lanciato **“Sos famiglie”**  un sito di informazione sui diritti e i doveri delle coppie non matrimoniali.

Dopo la sentenza della Corte costituzionale 138/10 e della Cassazione 4184/2012 la campagna di Affermazione civile si è mossa in due direzioni: da una parte continuare ad operare per ottenere  il riconoscimento del matrimonio tra persone dello stesso sesso rivolgendosi anche a Corti europee (due coppie si sono rivolte alla CEDU), e dall’altra, quella di riuscire ad ottenere sempre più diritti in Italia facendo in modo che i Tribunali si esprimano su questioni specifiche legate alla  discriminazione delle coppie dello stesso sesso.

E’ proprio in questo secondo filone che l’Associazione, nel 2012, ha ottenuto i risultati migliori.

Il 13.2.12 il Tribunale di Reggio Emilia si è espresso a favore della concessione della **Carta di Soggiorno come familiare di cittadino comunitario** per un ragazzo uruguayano sposato in Spagna con un italiano. I due ragazzi sono stati seguiti da noi e dall’Avv. Giulia Perin.

In questo caso le normative di riferimento sono quelle sulla libera circolazione in Europa dei cittadini comunitari e dei loro familiari **(Decreto Legislativo n.30/2007)**. Dopo questa sentenza divenuta definitiva, circa 130 coppie ci hanno contattato per ottenere informazioni e/o iniziare il percorso che consente, al partner non italiano, di ottenere  il titolo di soggiorno di 5 anni (a cui seguirà quello a tempo indeterminato).

Al 6 gennaio 2014 sono 27 le carte di soggiorno per familiare di cittadino/a comunitario/a rilasciate alle coppie da noi seguite dalle Questure di: **Reggio Emilia, Rimini, Milano, Roma, Lucca, Treviso,**  **Varese, Cagliari, Firenze, Venezia, Vicenza,**  **Genova, Palermo, Brindisi  e Bolzano.** La **Questura di Verona,** ha concesso il titolo di soggiorno ad una coppia sposata all’estero ma lo ha negato ad  una che ha sottoscritto una unione civile in Germania. A questo diniego è in corso di deposito il ricorso al Tribunale

 I documenti di soggiorno rilasciati riguardano diverse tipologie: coppie con matrimonio celebrato in Europa, in un Paese extraeuropeo e unione civile. Alcune Questure hanno sollevato delle obiezioni, ma le argomentazioni contenute nelle Memorie ex art. 10 bis L. n. 241/1990  che abbiamo  presentato sono sempre state accolte (tranne nel caso di Verona sopra citato) e le carte rilasciate.

A fronte di un quesito posto dalle Questure di Firenze - Pordenone, **il Ministero dell’Interno il 26.10.12 ha emanato una Circolare** in cui, facendo riferimento alla Sentenza della Corte Costituzionale 138/10 e all’ordinanza del Tribunale di Reggio Emilia, ha legittimato il rilascio della **Carta di soggiorno per familiari di cittadini comunitari** al componente extra EU ( o comunitario a  carico) di coppie dello stesso sesso.                                                                                                                                **E’ questo un importante riconoscimento, sebbene soltanto ai fini del soggiorno regolare in Italia, che ha costretto le istituzioni a concedere per la prima volta un documento che riconosce le coppie dello stesso sesso.**

Per rendere accessibili a tutti/e  i risultati che abbiamo ottenuto è stata realizzata e diffusa  la guida “**Informazioni per ottenere la “Carta di soggiorno per familiari di cittadini europei” (Decreto  Legislativo 30/2007) per coppie miste dello stesso sesso”,** guida  che è stata ulteriormente aggiornata, dopo l’entrata in vigore nel settembre 2014, degli adeguamenti normativi richiesti dall’Europa all’Italia al D. leg.30/2007.

Sempre con lo scopo informativo abbiamo diffuso due altre guide che forniscono le **indicazioni pratiche per poter celebrare il matrimonio in Portogallo,** unico Paese europeo (con la Norvegia) che non richiede la residenza per celebrare il matrimonio e **in Brasile.**

Premettendo che non sempre è facile trovare delle coppie disponibili ad iniziare un percorso di Affermazione civile ( o che subiscono e non accettino una discriminazione rispetto ad una coppia eterosessuale) questi sono le azioni legali intraprese o in itinere:

**Tribunale di Milano**, a fronte del diniego di concessione ad una coppia dello stesso sesso degli assegni familiari è stato presentato un ricorso bocciato dal Giudice  in primo grado.  A seguito di deposito del ricorso l’appello è stato fissato nel …maggio 2015.

Sempre **a Milano** un ricorso al diniego dell’ASL di iscrivere al SSN la partner comunitaria **a carico** della compagna italiana (coppia che ha sottoscritto in Inghilterra una unione civile) è stato bocciato in primo grado nel dicembre 2013. In questo caso non abbiamo proceduto con il ricorso in appello perché la coppia, soprattutto a causa delle difficoltà oggettive dovute a questa situazione, ha deciso di ritornare in Inghilterra.

Un **istituto bancario in Lombardia** ha concesso la licenza matrimoniale ad un ragazzo che ha celebrato il matrimonio in Portogallo.

Nel luglio 2012 alla **Prefettura di Roma** è stata depositata la richiesta di cittadinanza italiana per il coniuge dello stesso sesso di un italiano sposati all’estero. La normativa infatti prevede che dopo 2 anni dal matrimonio in caso di convivenza in Italia, o dopo 3 se la residenza è all’estero, il/la cittadino/a italiano/a possa trasmettere la cittadinanza italiana al coniuge. Attualmente è stata presentata una memoria ex art. 10 bis L. n. 241/1990 e siamo in attesa della decisione della Prefettura.

Nell’attesa ormai biblica che anche l’Italia si allinei con i Paesi europei, nell’aprile 2012 abbiamo pubblicato il libro **“Certi diritti che le coppie conviventi non sanno di avere”** un vero e proprio manuale su come tutelare le famiglie che non vogliono o non possono accedere all’istituto matrimoniale. Questo libro si aggiunge a quello editato nel novembre 2011 “**Dal cuore delle coppie al cuore del diritto. L'udienza 138/2010 alla Corte costituzionale per il diritto al matrimonio tra persone dello stesso sesso”.**

#### PER FINIRE

C’è un’ultima cosa da dire, forse la più importante: per poter fare tutto questo c’è stato bisogno, e ce ne sarà bisogno sempre di più, dell’ingrediente più importante: coppie che siano consapevoli dei propri diritti.

Ne è la prova il fatto che due delle sentenze del 2012 non sono avvenute per l’impegno diretto di una associazione LGBT, quanto per l’iniziativa autonoma di cittadini, coppie capaci di vedersi protagoniste del percorso che è necessario per vedere riconosciuti i diritti di cui la legge priva una parte dei suoi cittadini.