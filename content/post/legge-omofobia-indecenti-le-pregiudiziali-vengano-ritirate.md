---
title: 'Legge Omofobia: indecenti le pregiudiziali, vengano ritirate'
date: Wed, 15 Jun 2011 14:05:51 +0000
draft: false
tags: [Politica]
---

**Governo irresponsabile. Comunicato delle associazioni: AGEDO, Arcigay, Arcilesbica, Certi diritti, Comitato Roma Europride 2011, Famiglie Arcobaleno, Mario Mieli, MIT, Rete Lenford.**

  
                 
Il voto sulle pregiudiziali costituzionali sulla Legge contro l'omofobia slitta a dopo il 22 giugno. Le Associazioni lgbt italiane,  AGEDO, Arcigay, Arcilesbica, Certi diritti (organizzatore di una maratona oratoria nel giorno del voto), Comitato Roma Europride 2011, Famiglie Arcobaleno, Mario Mieli , MIT, Rete Lenford chiedono che le indecenti e false pregiudiziali vengano ritirate dai presentatori e che sia ascoltata la richiesta di riconoscimento di dignità, diritti ed uguaglianza proveniente dal formidabile Europride dell'11 giugno.

La domanda che la politica non perda altro tempo, sollevata dal milione di persone che hanno marciato per le vie di Roma, prevalga sulla falsificazione della realtà e del diritto e sulle offese alle persone lgbt contenute nelle pregiudiziali stesse.

La maggioranza e il Governo non possono continuare ad insultare e ledere la dignità di milioni di persone con tanta cieca e cinica superficialità: da qui a fine giugno si apre dunque una fase di severa battaglia politica e chiediamo a tutte le forze politiche, sociali e ai mass media di sostenere il dibattito che stiamo animando.  
Se le pregiudiziali non verranno ritirate, si configurerà una evidente complicità morale con chi esercita discriminazione e violenza: gli elettori  puniranno certamente i loro rappresentanti che si ostinano a legiferare con iniqua disumanità contro le persone lgbt.

Il Ministro Carfagna aveva annunciato nell'aula della Camera dei Deputati , a novembre 2009, un disegno di legge del Governo per la lotta contro l'omofobia e la transfobia, che non c'è mai stato.  
Comprendiamo che la sensibilità del Ministro non si è tradotta in decisione politica del Governo che così conferma  la sua totale irresponsabilità ed incapacità che denunciamo all'opinione pubblica italiana ed europea.