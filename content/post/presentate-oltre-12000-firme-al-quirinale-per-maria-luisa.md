---
title: 'PRESENTATE OLTRE 12.000 FIRME AL QUIRINALE PER MARIA LUISA'
date: Fri, 17 Jul 2009 11:29:28 +0000
draft: false
tags: [Comunicati stampa]
---

PRESENTATE AL QUIRINALE OLTRE 12.000 FIRME PER LA MEDAGLIA AL VALOR CIVILE A MARIA LUISA MAZZARELLA. IL GOVERNO SI ATTIVI PER CONTRASTARE ATTI VIOLENTI CONTRO PERSONE OMOSESSUALI. LO CHIEDONO L’ON. PAOLA CONCIA E LE ASSOCIAZIONI LGBT.

Roma, 17 luglio 2009

A margine della Conferenza Stampa, svoltasi stamane, in cui sono state presentate l'Appello e le oltre 12.000 firme al Capo dello Stato per concedere l'onorificenza al valore civile a Maria Luisa Mazzarella, la ragazza di Napoli picchiata e gravemente ferita, per aver difeso l'amico gay dall’aggressione di quattro bulli, i promotori dell’iniziativa, l’on. Paola Concia, deputata del Pd, Alessio De Giorgi, Direttore di Gay.it, Sergio Rovasio, Segretario Ass.ne Radicale Certi Diritti, Carlo Cremona, Presidente di I-ken Onlus, , Salvatore Simioli, Presidente Arcigay Napoli, e Fabrizio Marrazzo, Presdiente di Arcigay Roma hanno chiesto  al Ministro degli Interni Maroni di intervenire d’urgenza affinché avvii subito una campagna di informazione e intervento, di concerto con le associazioni lgbt, così come avviene in molti paesi europei e nordamericani per combattere gli atti di violenza omofobica.  Profonda preoccupazione è stata espressa per il crescente numero di atti violenti di omofobia saliti a oltre 100 che negli ultimi 15 mesi.

Come testimoniato dal caso di Maria Luisa, al quale si aggiungono molti altre violenze in tutta Italia non denunciate, l’omofobia e' diventata una vera e propria emergenza che il Governo non può più ignorare. Per questo sollecitiamo anche i membri della Commissione Giustizia della Camera ad approvare con urgenza la proposta di legge contro l’omofobia in discussione. In molti paesi europei le forze di polizia  promuovono con efficacia campagne di comunicazione rivolte alla comunita lgbt per avvertire dei particolari rischi cui e' esposta e per creare un clima  di dialogo  tra i cittadini gay  e le forze di polizia, favorendo il passaggio di informazioni e le denunce. Per questo chiediamo che il Governo si muova quanto prima di concerto con le associazioni lgbt ”.