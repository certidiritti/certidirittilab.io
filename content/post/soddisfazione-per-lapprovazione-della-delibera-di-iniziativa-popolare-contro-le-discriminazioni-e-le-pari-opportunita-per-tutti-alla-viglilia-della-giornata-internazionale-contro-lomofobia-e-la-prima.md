---
title: 'Soddisfazione a Milano per l''approvazione della delibera di iniziativa popolare contro le discriminazioni e per le pari opportunità per tutti alla viglilia della giornata internazionale contro l''omofobia. E'' la prima mai approvata.'
date: Thu, 16 May 2013 15:24:05 +0000
draft: false
tags: [Politica]
---

Comunicato stampa del Comitato Milano Radicalmente Nuova e dell'Associazione Radicale Certi Diritti

Milano, 16 maggio 2013

Il Comitato promotore della raccolta firme per le delibere di iniziativa popolare Milano Radicalmente Nuova esprime grande soddisfazione per l'approvazione della delibera contro le discriminazioni e per le pari opportunità per tutti da parte del Consiglio Comunale di Milano con 30 voti favorevoli e 10 contrari.

Da oggi la Giunta avrà sei mesi di tempo per elaborare e sottoporre al Consiglio comunale un Piano cittadino contro le discriminazioni basate sui sei fattori indicati nell'art. 19 TFEU (Trattato di Funzionamento dell'Unione Europea): etnia, religione, genere, età, disabilità e orientamento sessuale.

Finalmente la città di Milano avrà uno strumento per dare concretezza al protocollo d'intesa già siglato con UNAR e, auspicabilmente, potrà attrarre risorse da dedicare alla lotta alle discriminazioni e a garantire pari opportunità per tutti.

Ringraziamo presidenti le Commissioni Pari Opportunità, Referendum approvati, Iniziativa Popolare, Digitalizzazione, Trasparenza, Agenda digitale e Politiche Sociali e Servizi per la Salute, nonché l'Assessore alle Politiche sociali e Cultura della salute per i contributi dati a migliorare la delibera e il sostegno.

Yuri Guaiana, portavoce del Comitato Milano Radicalmente Nuova e segretario dell'Associazione Radicale Cereti Diritti, e Marco Cappato, presidente del Gruppo Radicale - Federalista Europeo e tesoriere dell'Associazione Luca Cosioni, dichiarano "Proprio alla vigilia della Giornata mondiale contro l'omofobia, Milano fa un altro passo importante verso l'Europa, dove le discriminazioni basate su etnia, religione, età, disabilità, genere e orientamento sessuale sono combattute dalle amministrazioni comunali con politiche preventive. E' la prima volta in assoluto nella storia di Milano che viene approvata una delibera di iniziativa popolare. Siamo contenti che questo fondamentali istituto di democrazia popolare abbia avuto il suo primo successo con un tema di tale rilevanza. E' molto importante che il consiglio abbia stabilito tempi precisi entro i quali il Piano dovrà essere adottato e indicazioni altrettanto precise sulla sua stesura. Di particolare importanza è la creazione di un ufficio con dei funzionari comunali impiegati con continuità ad attuare e coordinare le attività del Comune contro le discriminazioni, mettendole al riparo dalla volubilità delle maggioranze politiche. Fondamentali anche i corsi di formazione al personale del Comune e agli agenti di polizia locale oltre che il coinvolgimento delle scuole nell'azione contro le discriminazioni e il bullismo".

Per un approfondimento:

**[Il testo integrale della delibera del comune di Milano](documenti/download/doc_download/67-delibera-comune-di-milano-16052013)**