---
title: 'L''associazione radicale Certi Diritti diventa soggetto politico costituente del Partito Radicale Nonviolento transnazionale e transpartito'
date: Sun, 11 Dec 2011 16:52:35 +0000
draft: false
tags: [Transnazionale]
---

Il 39° Congresso del Partito Radicale Nonviolento Transnazionale Transpartito ha accolto la richiesta dell'associazione. Presente il presidente onorario di Certi Diritti John Francis Onyango. 

Roma, 11 dicembre 2011

Comunicato Stampa dell'Associazione Radicale Certi Diritti

La II Sessione del 39° Congresso del Partito Radicale Nonviolento, transnazionale e transpartito, votando oggi la sua Mozione generale, ha accolto la richiesta dell’Associazione Radicale Certi Diritti di diventare uno dei suoi soggetti politici costituenti. Il Congresso ha anche accolto una Mozione particolare concernente la persecuzione delle persone a causa del loro orientamento sessuale nel mondo che invita il partito a sostenere la proposta di Risoluzione presentata alle Nazioni Unite per la depenalizzazione dell’omosessualità, osteggiata dal Vaticano, dall’Iran e da altri paesi dittatoriali.

Ringraziamo Emma Bonino, Marco Pannella, i dirigenti, i militanti e tutti i congressisti Radicali per questo atto di fiducia che impegnerà ancora di più l’Associazione Radicale Certi Diritti - e i suoi organi - sui temi che da sempre caratterizzano la propria attività in materia di liberazione sessuale e contro ogni forma di  sessuofobia, ipocrisia e falsità anche di stampo clericale in Italia, in Europa e in altri paesi del mondo.

Al V Congresso dell’Associazione Radicale Certi Diritti, svoltosi a Milano la scorsa settimana, è stato eletto Presidente onorario l’ugandese Francis Onyango, Avvocato di David Kato Kisule, iscritto Radicale barbaramente assassinato vicino a Kampala nel gennaio 2011 dopo aver subito soprusi da parte delle autorità ugandesi.

L’impegno dell’Associazione Radicale Certi Diritti, nell’ambito delle azioni politiche del Partito Radicale Nonviolento, Transnazionale e Transpartito, si ispirerà alle lotte nonviolente gandhiane per affermare ovunque i valori della "Dichiarazione Universale dei Diritti dell’Uomo", quelli della "Convenzione Europea dei Diritti Umani e delle libertà fondamentali", i principi indicati in Europa dai Trattati di Nizza e di Lisbona sulla libera circolazione dei cittadini e sulla lotta alle discriminazioni, quanto indicato dalla “Risoluzione del Consiglio dei Diritti Umani delle Nazioni Unite” approvata nel giugno 2011 sulla lotta alle discriminazioni, quanto indicato nel “Rapporto sulle discriminazioni basate sull’orientamento sessuale l’identità di genere” dell’Alto Commissariato per i Diritti Umani del Consiglio d’Europa del luglio 2011.

L’Associazione Radicale Certi Diritti da quando è stata fondata dedica le sue iniziative a Makwan Moloudzadeh (il ragazzo di 21 anni impiccato in Iran il 5 dicembre 2007 perché accusato di aver commesso atti omosessuali) e a coloro che hanno sofferto abusi , discriminazioni e violenze a causa del proprio orientamento sessuale.