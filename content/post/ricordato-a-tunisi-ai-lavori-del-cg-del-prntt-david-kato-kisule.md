---
title: 'RICORDATO A TUNISI AI LAVORI DEL CG DEL PRNTT DAVID KATO KISULE'
date: Sat, 23 Jul 2011 09:59:54 +0000
draft: false
tags: [Africa]
---

Durante i lavori del Consiglio Generale del Partito Radicale Nonviolento, Marco Pannella ha ricordato David Kato Kisule, esponente ugandese dei diritti civili ucciso in Uganda dal fondamentalismo religioso.

Tunisi, 22 luglio 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti :

Marco Pannella, Presidente del Senato del Partito Radicale Nonviolento, durante il suo intervento ai lavori del Consiglio Generale del Prntt, apertisi stamane a Tunisi e che si concluderanno domenica 24 luglio, ha ricordato la figura di David Kato Kisule, esponente della comunita' Lgbt ugandese, iscritto a Certi Diritti, assassinato presso la sua abitazione dopo una lunga campagna di persecuzioni avviata dai media contro le persone gay del paese .

Nel corso del suo intervento Marco Pannella ha ricordato la campagna persecutoria avviata contro David Kato Kisule dal settimanale ugandese ‘Rolling Stone’ che aveve pubblicato nell’ottobre 2010 in copertina le foto di un centinaio di attivisti omosessuali chiedendone la morte. Anche il comportamento dei fondamentalisti religiosi che negli ultimi anni promuovono e alimentano odio e intolleranza, sono stati certamente i corresponsabili di questo assassinio.

Pannella ha anche ricordato che nel febbraio scorso tre militanti ugandesi dei diritti civili, amici e compagni di lotta di David Kato Kisule, hanno partecipato ai lavori del Congresso del Prntt di Chianciano e si sono iscritti al Partito Radicale Nonviolento.