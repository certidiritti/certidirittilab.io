---
title: 'SONDAGGIO CORRIERE DELLA SERA ONLINE: MATRIMONIO GAY SI O NO?'
date: Thu, 06 Nov 2008 15:16:14 +0000
draft: false
tags: [Comunicati stampa]
---

Il corriere della sera on line ha lanciato un sondaggio sul matrimonio omosessuale all'indomani della sconfitta dei referendum americani, in particolare quello californiano.  
  
Invitiamo gli utenti di Certi Diritti a votare. Fate attenzione, la domanda è formulata in modo tale che occorre votare No per esprimere il proprio favore al matrimonio gay.  
  
  
Potete votare qui:  
  
[http://www.corriere.it/apps...](http://www.corriere.it/appsSondaggi/pages/corriere/d_3657.jsp)