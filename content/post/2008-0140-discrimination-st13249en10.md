---
title: '2008-0140-Discrimination-ST13249.EN10'
date: Wed, 08 Sep 2010 11:48:58 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 8 September 2010

Interinstitutional File:

2008/0140 (CNS)

13249/10

LIMITE

SOC 510

JAI 712

MI 285

  

  

  

  

  

NOTE

from :

Council General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

12461/10 SOC 470 JAI 651 MI 256

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Further to the meeting of the Working Party on Social Questions on 16 July 2010, delegations will find attached a note from the BG delegation.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Questions for SQWP debate on financial services-**

**BG answers**  **to the questionnaire in document 11903/10**

**1\. Situation in Member States**

How do you deal with the use of age and disability as factors in risk assessment in financial services (insurance and banking) at national level?

_In insurance age and disability are taken into account as risk factors in terms of calculation of the due premium. Age and disability are also determining factors for the calculation of life-long pensions._

**2\. Disability / health conditions**

It has been suggested that the exception for disability can be limited to the cases where the underlying health condition is a relevant risk factor. What is your view?

_Definitely disability must be used as a risk factor only where appropriate. This is the normal practice in Bulgaria in respect of actuarial calculations._

**3\. Scope of the exception**

Currently the exception provides for a three-step test (similar to Directive 2004/113/EC regarding gender):

1.      Is age / disability a **determining factor** in the assessment of risk for the service in question?

2.      Is the risk assessment based on relevant actuarial principles, accurate statistical data or medical knowledge? (**evidence base**)

  

3.      Is the difference of treatment **proportionate**?

Is this test workable in your view?

_Yes, but we believe more clarity is needed on the concept of proportionality._

**4\. Evidence to justify differential treatment**

The current text on the possible evidence base is slightly different for age and disability. Should risk assessment always be based on relevant actuarial principles? Should the use of medical knowledge only be possible, if statistical data are not available? Should there be restrictions on the kind of data that can be used?

_Yes, the risk assessment should always be based on relevant actuarial principles and statistical data; otherwise the prohibition of discrimination can be easily circumvented. Medical knowledge must be only a complementary source of risk data. It is not applicable in the field of private pensions._

**5\. Proportionality of differential treatment**

What do you understand by "proportionate differences"? Mathematical proportionality or a wider concept of proportionality, taking into account issues such as legitimacy, objectivity, relevance and the existence of less restrictive alternatives ("ultima ratio" principle)?

_We believe this concept needs further clarification in the directive. In our view it is not always practical to expect strict mathematical proportionality._

**6\. Age limits and age bands**

Would the exception allow financial service providers to apply age limits or age bands? How could proportionality be ensured in that context?

_This depends on the nature of the product._

**7\. Transparency towards customer**

Is there a need in the exception for a requirement to publish data, similar to the one in Directive 2004/113/EC? If not, should there be other forms of transparency towards the customer (e.g. obligation to explain the reasons for the differential treatment / to advise the customer on possible alternatives)?

_In our view the exception can be supplemented with appropriate disclosure of information to the customer. Obligation to explain the differential treatment is the most suitable option._

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_