---
title: 'Benvenuti'
date: Sun, 13 Jun 2010 06:49:13 +0000
draft: false
tags: [associazione certi diritti, certi diritti, diritti civili, diritti omosessuali, diritto al matrimonio, discriminazione, DISCRIMINAZIONI, essere omosessuali, famiglie omosessuali, glbt, glbte, lotta all'omofobia, lotta alla transfobia, Matrimoni omosessuali, matrimonio gay, matrimonio gay italia, matrimonio tra gay, matrimonio tra omosessuali, OMOFOBIA, OMOSESSUALITA', Senza categoria, terapie riparative, transfobia, tutela omosessualitutela omosessualitutela omosessualitutela omosessuali, tutela prostitute, tutela transessuali, unione civile, unioni civili, unioni omosessuali]
---

**Associazione Radicale Certi Diritti**  

------------------------------------------

_(cos'è?)_

### **RADICALE**

Siamo un'associazione radicale perché procediamo per **obiettivi**; perché il nostro **metodo** è quello nonviolento della disobbedienza civile, della proposta, del dialogo; perché al centro della nostra iniziativa politica ci sono i cittadini con la loro **libertà** e la loro **responsabilità**.

### **LGBT**_E_

Quando usiamo la siglia LGBT (lesbiche, gay, trans e bisessuali), ci piace aggiungere una E, quella **di etero**, perché il diritto di uguaglianza riguarda tutti, e perché tra i nostri iscritti e militanti ci sono donne, trans, uomini, gay, lesbiche, etero-, bi-, intersessuali... ed altro.

### **CONTRO L'OMOFOBIA: LA BATTAGLIA PER IL DIRITTO AL MATRIMONIO TRA PERSONE DELLO STESSO SESSO**

L'omofobia si combatte principalmente con la cultura. Noi siamo convinti che non ci sia arma migliore, per sconfiggerla, **della battaglia per l'eguaglianza**.

Nel 2008, in collaborazione con [**Avvocatura LGBT – Rete Lenford**](http://www.retelenford.it/), abbiamo lanciato una campagna di **[Affermazione civile](campagne/affermazione-civile/41.html)** per il riconoscimento del **diritto al matrimonio tra persone dello stesso sesso**. Questa battaglia ha condotto, due anni dopo, alla [**sentenza della Corte Costituzionale 138/2010**](tutte-le-notizie/685-le-motivazioni-della-sentenza-della-corte-costituzionale.html).

La sentenza **1)** impone un riconoscimento giuridico delle unioni omosessuali **2)** sollecita il Parlamento a varare una disciplina di carattere generale in materia **3)** si riserva d’intervenire per garantire un trattamento omogeneo tra le coppie coniugate e quelle omosessuali **4)**  bandisce il comportamento omofobico.

Per noi l'**obiettivo** resta quello della **piena uguaglianza** e del **diritto al matrimonio**. Su questa strada continueremo, insieme a decine di associazioni LGBT(E) con le quali abbiamo dato vita al [**Comitato Sì, lo voglio**](http://www.affermazionecivile.it).

### **DIRITTI CIVILI delle PERSONE CHE SI PROSTITUISCONO**

Insieme al **[Comitato per i diritti civili delle prostitute](http://www.lucciole.org/),** siamo impegnati per contrastare l'informazione distorta e denigratoria dell’immagine delle persone che si prostituiscono e il proibizionismo criminogeno e inutile sulla prostituzione.

### **DIRITTI CIVILI delle PERSONE TRANSESSUALI**

Siamo impegnati per aiutare e sostenere le **persone transessuali** e  **transgender** rispetto alla grave distorsione mediatica di cui sono vittime e nella lotta alla violenza transfobica. Grazie ai radicali, nel **1982** fu approvata la [**legge sul cambio di sesso**](chi-siamo/783.html) che oggi occorre aggiornare, così come già avvenuto in Spagna e Gran Bretagna con leggi all’avanguardia che permettano il cambio del nome e del genere sui documenti, senza un ricorso necessario all’intervento di riattribuzione dei genitali.

### RIFORMA DEL DIRITTO DI FAMIGLIA

L'ultima riforma del diritto di famiglia è del 1975. Gli attacchi lanciati negli ultimi anni dalle forze conservatrici al riconoscimento delle unioni di fatto, alla libertà di procreazione assistita, alla possibilità del divorzio breve, ci hanno ancora più convinti della necessità di un **progetto globale di riforma del diritto delle relazioni familiari**. Lo abbiamo elaborato con altre associazioni, esperti di diritto ed esponenti politici di diversi schieramenti, nell'ambito della **[Conferenza permanente per il la Riforma del Diritto di Famiglia](http://www.amorecivile.it/)** , ed è stato pubblicato nel volume [**Amore Civile. Dal diritto della tradizione al diritto della ragione**](libri/708-amore-civile.html) a cura di Francesco Bilotta e Bruno De Filippis (ed. Mimesis). A breve verrà presentato in Parlamento.

### **TRANSNAZIONALE**

Grazie all'impegno dei nostri iscritti, tra l'altro:

a **Roma** abbiamo depositato in **Parlamento**, grazie ai deputati e senatori radicali (tutti iscritti a Certi Diritti),  diverse **proposte di legge** su matrimonio omosessuale, lotta all’omofobia e alla transfobia, unioni civili, maggiori tutele e diritti per le persone transessuali e presentiamo costantemente  **interpellanze** e **interrogazioni** sui nostri temi; a **Bruxelles** contribuiamo ai lavori dell'[**Intergruppo sui diritti LGBT al Parlamento europeo**](http://www.lgbt-ep.eu/); partecipiamo ai lavori di [**ILGA-EUROPE**](http://www.ilga-europe.org/) e collaboriamo con [**EveryOne – Group for Internationla Cooperation on Human Rights Culture**](http://www.everyonegroup.com/EveryOne/MainPage/MainPage.html), nella difesa dei diritti dei migranti omosessuali.

**Renditi anche tu protagonista delle nostre battaglie di libertà e di uguaglianza**. Abbiamo gruppi di volontari in diverse città italiane. L'elenco completo lo trovi nel nostro sito.

Certi Diritti non riceve contributi pubblici: **SOSTIENICI!**

   ![](https://www.paypal.com/it_IT/i/scr/pixel.gif)

### **[ISCRIVITI!](chi-siamo/694-come-iscriversi-o-versare-un-contributo.html)  
**

  
  
Nella tradizione radicale, per noi "prendere la tessera" non ha valenza identitaria, etnica o di appartenenza: molto più civicamente, significa dire: "**quest'anno io condivido i vostri obiettivi e li faccio miei. L'anno prossimo vedremo.**"

Tutto [**il nostro lavoro è volontario**](chi-siamo/18.html) e solo dal canale dell’autofinanziamento possiamo avere le risorse economiche e l'aiuto per promuovere ulteriori iniziative.