---
title: 'Parlamento Europeo deposita all''unanimità Risoluzione per difesa diritti Lgbt all''Onu e nel mondo'
date: Fri, 23 Sep 2011 18:54:07 +0000
draft: false
tags: [Europa]
---

Al Parlamento europeo depositata risoluzione sui diritti lgbt, anche in discussione all'Onu, firmata da tutti i gruppi politici. Lezione per l'Italia e i politici omofobi e ipocriti. Al prossimo congresso di Certi Diritti (a Milano 3-4 dicembre) prevista una sessione su questi temi.

Comunicato Stampa dell'Associazione Radicale Certi Diritti

I gruppi politici del Parlamento Europeo: Popolare, Socialista, Liberale e Democratico, Verde, Comunista e Conservatore/Riformista hanno depositato, su iniziativa dell'integruppo LGBT al Parlamento Europeo, una risoluzione sui diritti umani, l'orientamento sessuale e l'identità di genere che rivolge una particolare attenzione alle discussioni che si terranno nella prossima primavera alle Nazioni Unite.

La risoluzione, citando positivamente la risoluzione A/HRC/17/19 sui diritti Lgbt da parte del Consiglio dei Diritti Umani dell'Onu,  esprime la costante preoccupazione del Parlamento Europeo  sulle violazioni dei diritti umani delle persone LGBT nel mondo, invita gli organi internazionali a continuare ad agire in particolare in occasione delle discussioni che si terranno nella primavera 2012 all'ONU sull'orientamento sessuale, chiede all'Alta Rappresentante dell'UE per la politica estera ed agli Stati membri di promuovere sistematicamente il rispetto dei diritti umani delle persone LGBT, agli Stati membri di implementare le raccomandazioni della relazione dell'Agenzia per i Diritti Fondamentali della UE sull'omofobia, nonché alla Commissione di lanciare una Roadmap contro l'omofobia, la transfobia, la discriminazione basata sull'orientamento sessuale e l'identità di genere.

L'Associazione Radicale Certi Diritti si felicita per l'azione congiunta dei gruppi politici al PE sui diritti LGBT nel mondo ed in particolare per l'adesione del gruppo PPE a tale iniziativa, che dimostra come l'ipocrita opposizione dei gruppi politici del PdL e dell'UdC a qualunque iniziativa per proteggere i diritti delle persone LGBT a livello nazionale sia sconfessata anche dalla linea politica del loro corrispondente partito europeo.

Durante i lavori del prossimo Congresso, nazionale dell'Associazione Radicale Certi Diritti, che si svolgerà a Milano il 3 e 4 dicembre 2011 è prevista una specifica sessione sul tema dei diritti Lgbt nel mondo, anche con riferimetno alle iniziative promosse all'Onu.

Per ricevere la NewsLetter di Certi Diritti:

[www.certidiritti.it](http://www.certidiritti.it)