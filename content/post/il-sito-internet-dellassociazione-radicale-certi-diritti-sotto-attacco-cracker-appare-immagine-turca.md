---
title: 'IL SITO INTERNET DELL’ASSOCIAZIONE RADICALE CERTI DIRITTI SOTTO ATTACCO CRACKER. APPARE IMMAGINE TURCA'
date: Thu, 13 Aug 2009 10:56:58 +0000
draft: false
tags: [Comunicati stampa]
---

**_Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:_**

“Da alcune ore il sito ufficiale dell’Associazione Radicale Certi Diritti è sotto attacco cracker. Sul sito internet www.certidiritti.it appare un’immagine simile a quella di Mustafa Kemal Ataturk con, sullo sfondo, la bandiera turca e la scritte: ‘No war stop Israel’ e “en iyi hack bildiğin hacktir ;) dewam ediyyyy...” Quanto appare nel sito internet di certidiritti.it è certamente opera di qualche fondamentalista che si oppone alle nostre iniziative in difesa dei diritti civili e umani. Non riteniamo credibile che i sostenitori del fondatore dello Stato laico turco, Kemal Ataturk, promuovano queste azioni”.

La vicenda è stata segnalata all’autorità giudiziaria.

GIOVEDI' MATTINA 13 AGOSTO IL NOSTRO SITO INTERNET E' STATO CRACCKATO E RESO INUTILIZZABILE. ALLE ORE 14.30, GRAZIE AL NOSTRO WEBMASTER, INTERVENUTO DALLA COLOMBIA DOVE SI TROVA IN VACANZA, IL PROBLEMA E' STATO RISOLTO.

IL CRACKER AVEVA MESSO L'IMMAGINE FISSA DI ATATURK CON, SULLO SFONDO, LA BANDIERA TURCA E ALCUNE SCRITTE INCOMPRENSIBILI.