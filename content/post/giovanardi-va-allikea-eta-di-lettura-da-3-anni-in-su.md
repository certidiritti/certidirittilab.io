---
title: 'Giovanardi va all''Ikea (età di lettura: da 3 anni in su)'
date: Thu, 28 Apr 2011 14:36:05 +0000
draft: false
tags: [Politica]
---

Sul blog vogliosposaretizianoferro.it una lettera aperta di Andrea Bordoni a Giovanardi, che anche voi potete inviare al Sottosegretario scrivendo a giovanardi_c@posta.senato.it.

Sul blog vogliosposaretizianoferro.it Andrea Bordoni parla delle dichiarazioni di Giovanardi contro la campagna pubblicitaria dell'IKEA "Siamo aperti a tutte le famiglie", invitando il senatore a leggere un libro per bambini con le finestrelle intitolato "Tante famiglie tutte speciali", che gli è stato inviato come regalo. Nel libro vengono illustrate ai più piccoli tante tipologie di famiglia, comprese quelle formate da due papà o due mamme, o da single con figli.  
  
Il blog inoltre invita i lettori a spedire una mail a Giovanardi in cui lo si invita ad aprire e leggere il libro. Ecco la lettera.  
  
Gentile Sen. Carlo Giovanardi,  
in questi giorni, come molti italiani, non ho potuto fare a meno di apprendere dai giornali la sua vibrante presa di posizione contro la bella campagna pubblicitaria dell’Ikea, che apre le porte a tutte le famiglie, di qualsiasi tipologia. Il fatto che io pensi che la campagna sia bella e intelligente non ha niente a che fare col sogno che ho fatto l’altra notte, in cui il cartellone si animava, i due ragazzi di spalle si giravano ed eravamo io e Tiziano Ferro.  
Quello dell’Ikea è evidentemente marketing virale, cioè pensato in modo tale che, per un motivo o per un altro, se ne parli: pubblicità supplementare senza che l’azienda tiri fuori un centesimo. Sicuramente avrà calcolato che, contestandola, anche lei ne sarebbe diventato un veicolo, ma ha soppesato i pro e i contro e ha deciso di cogliere la palla al balzo.  
Da Sottosegretario alla Famiglia senza idee altrettanto brillanti, ha pensato bene di farsi notare gridando con voce stridula e fastidiosa lo slogan contrario: “Famiglia tradizionale o morte!”. Qualcuno le avrà consigliato di uscire dal silenzio perché siamo sotto elezioni, e il suo partito è convinto che con l’omofobia si va sempre sul sicuro in quell’Italia che voi considerate ideale, quella allevata a ordini cattolici e che tollera solo le forti emozioni delle fiction di Rai Uno e Canale 5.  
  
**[continua >>>](http://www.vogliosposaretizianoferro.it/giovanardi-va-all%E2%80%99ikea-eta-di-lettura-da-3-anni-in-su/)**