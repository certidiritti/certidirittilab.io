---
title: '2008-140-Discrimination-ST16594-AD01.EN08'
date: Tue, 09 Dec 2008 13:40:25 +0000
draft: false
tags: [Senza categoria]
---

  

  

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 9 December 2008

16594/08

ADD 1

LIMITE

SOC 749

JAI 682

MI 509

  

  

  

  

  

ADDENDUM TO THE OUTCOME OF PROCEEDINGS

from :

The Working Party on Social Questions

on :

21 and 27 November 2008

No. prev. doc. :

14254/08 SOC 594 JAI 531 MI 363

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached the text of the above proposal as revised in the light of the meetings of the Working Party on Social Questions on 21 and 27 November 2008.

Changes to the Commission's proposal are set out as follows: new text is in **bold** and deletions are marked "**\[…\]**".

The outcome of proceedings is set out in doc. 16594/08.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

Proposal for a

COUNCIL DIRECTIVE

on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

THE COUNCIL OF THE EUROPEAN UNION,

Having regard to the Treaty establishing the European Community, and in particular Article 13(1) thereof,

Having regard to the proposal from the Commission[\[1\]](#_ftn1),

Having regard to the opinion of the European Parliament[\[2\]](#_ftn2),

**\[…\]**[\[3\]](#_ftn3)

**\[…\]**

Whereas:

  

(1)          In accordance with Article 6 of the Treaty on European Union, the European Union is founded on the principles of liberty, democracy, respect for human rights and fundamental freedoms, and the rule of law, principles which are common to all Member States and it respects fundamental rights, as guaranteed by the European Convention for the Protection of Human Rights and Fundamental Freedoms and as they result from the constitutional traditions common to the Member States, as general principles of Community law.

(2)          The right to equality before the law and protection against discrimination for all persons constitutes a universal right recognised by the Universal Declaration of Human Rights, the United Nations Convention on the Elimination of all forms of Discrimination Against Women, the International Convention on the Elimination of all forms of Racial Discrimination, the United Nations Covenants on Civil and Political Rights and on Economic, Social and Cultural Rights, the UN Convention on the Rights of Persons with Disabilities, the European Convention for the Protection of Human Rights and Fundamental Freedoms and the European Social Charter, to which \[all\] Member States are signatories. In particular, the UN Convention on the Rights of Persons with Disabilities includes the denial of reasonable accommodation in its definition of discrimination.

(3)          This Directive respects the fundamental rights and observes the fundamental principles recognised in particular by the Charter of Fundamental Rights of the European Union. Article 10 of the Charter recognises the right to freedom of thought, conscience and religion; Article 21 prohibits discrimination, including on grounds of religion or belief, disability, age or sexual orientation; and Article 26 acknowledges the right of persons with disabilities to benefit from measures designed to ensure their independence.

(4)          The European Years of Persons with Disabilities in 2003, of Equal Opportunities for All in 2007, and of Intercultural Dialogue in 2008 have highlighted the persistence of discrimination but also the benefits of diversity.

  

(5)          The European Council, in Brussels on 14 December 2007, invited Member States to strengthen efforts to prevent and combat discrimination inside and outside the labour market[\[4\]](#_ftn4).

(6)          The European Parliament has called for the extension of the protection of discrimination in European Union law[\[5\]](#_ftn5).

(7)          The European Commission has affirmed in its Communication ‘Renewed social agenda: Opportunities, access and solidarity in 21st century Europe’[\[6\]](#_ftn6) that, in societies where each individual is regarded as being of equal worth, no artificial barriers or discrimination of any kind should hold people back in exploiting these opportunities. **Discrimination based on religion or belief, disability, age or sexual orientation may undermine the achievement of the objectives of the EC Treaty, in particular the attainment of a high level of employment and of social protection, the raising of the standard of living, and quality of life, economic and social cohesion and solidarity. It may also undermine the objective of abolishing of obstacles to the free movement of persons, goods and services between Member States.**

(8)          The Community has adopted three legal instruments[\[7\]](#_ftn7) on the basis of article 13(1) of the EC Treaty to prevent and combat discrimination on grounds of sex, racial and ethnic origin, religion or belief, disability, age and sexual orientation. These instruments have demonstrated the value of legislation in the fight against discrimination_._ In particular, Directive 2000/78/EC establishes a general framework for equal treatment in employment and occupation on the grounds of religion or belief, disability, age and sexual orientation. However, variations remain between Member States on the degree and the form of protection from discrimination on these grounds beyond the areas of employment.

  

(9)          Therefore, legislation should prohibit discrimination based on religion or belief, disability, age or sexual orientation in a range of areas outside the labour market, including social protection, education and access to and supply of goods and services, including housing. It should provide for measures to ensure the equal access of persons with disabilities to the areas covered.

(10)      Directive 2000/78/EC prohibits discrimination in access to vocational training; it is necessary to complete this protection by extending the prohibition of discrimination to education which is not considered vocational training.

(11)      \[This Directive **is** without prejudice to **the exercise of** the competences of the Member States in the areas of education **and social protection, including** social security and health care. It **is** also without prejudice to the essential role and wide discretion of the Member States in providing, commissioning and organising services of general economic interest.\][\[8\]](#_ftn8)

(12)      Discrimination is understood to include direct and indirect discrimination, harassment, instructions to discriminate and denial of reasonable accommodation.

(13)      In implementing the principle of equal treatment irrespective of religion or belief, disability, age or sexual orientation, the Community should, in accordance with Article 3(2) of the EC Treaty, aim to eliminate inequalities, and to promote equality between men and women, especially since women are often the victims of multiple discrimination.

(14)      The appreciation of the facts from which it may be presumed that there has been direct or indirect discrimination should remain a matter for the national judicial or other competent bodies in accordance with rules of national law or practice. Such rules may provide, in particular, for indirect discrimination to be established by any means including on the basis of statistical evidence.

**(14a) (new)** **Differences in treatment in connection with age and disability may be permitted under certain circumstances if they are objectively and reasonably justified by a legitimate aim and the means of achieving that aim are appropriate and necessary. Such differences of treatment may include, for example, special age conditions regarding access to certain goods or services such as alcoholic drinks, arms, or driving licences. A legitimate aim may also be the promotion of the economic, cultural or social integration of young or old persons or persons with disabilities. Therefore, measures setting more favourable conditions for these persons, such as free or reduced fares for transport, museums, or sports facilities, are presumed to be compatible with the principle of non-discrimination.**

(15)      Actuarial and risk factors related to disability and to age are used in the provision of insurance, banking and other financial services. These should not be regarded as constituting discrimination where[\[9\]](#_ftn9) **such** factors are shown to be **determining** factors for the assessment of risk.

(16)      All individuals enjoy the freedom to contract, including the freedom to choose a contractual partner for a transaction. This Directive should not apply to economic transactions undertaken by individuals for whom these transactions do not constitute **a** professional or commercial activity. **In this context, the concept of professional or commercial activity may be defined in accordance with the national laws and practice of the Member States.**

(17)      While prohibiting discrimination, it is important to respect other fundamental rights and freedoms, including the protection of private and family life and transactions carried out in that context, the freedom of religion, and the freedom of association. This Directive is without prejudice to national laws on marital or family status, including on reproductive rights. It is also without prejudice to the secular nature of the State, state institutions or bodies, or education.

**\[(17a) (new) In accordance with** **the aim and the scope of the present Directive and of previous**[\[10\]](#_ftn10) **Directives adopted on the same legal basis, this Directive seeks to prohibit discrimination on the grounds of religion or belief, age, disability and sexual orientation in relation to the access to social protection, including social security and healthcare; social advantages; education; and the access to, and the supply of, goods and other services which are available to the public, including housing. The Member States retain their general competences**[\[11\]](#_ftn11) **in these areas.**

**(17b) (new) This Directive does not apply to matters covered by family law**[\[12\]](#_ftn12)**, including marital status, and laws on reproductive rights**[\[13\]](#_ftn13)**.** **It is also without prejudice to the secular nature of the State, state institutions or bodies, or education. Moreover, this Directive is without prejudice to the powers of the Member States to organise their social security and health care schemes in such a way as to guarantee their sustainability.**

(18)           Member States are responsible for the organisation and content of education. **This Directive does not apply to the content of teaching or activities and the organisation of national educational systems, including the provision of special needs education.\]**[\[14\]](#_ftn14)

(19)      The European Union in its Declaration No 11 on the status of churches and non-confessional organisations, annexed to the Final Act of the Amsterdam Treaty, has explicitly recognised that it respects and does not prejudice the status under national law of churches and religious associations or communities in the Member States and that it equally respects the status of philosophical and non-confessional organisations.

  

(19a)    **Persons with disabilities include those who have long-term physical, mental, intellectual or sensory impairments which, in interaction with various barriers, may hinder their full and effective participation in society on an equal basis with others.** Measures to enable persons with disabilities to have effective non-discriminatory access to the areas covered by this Directive play an important part in ensuring full equality in practice. Furthermore, individual measures of reasonable accommodation may be required in some cases to ensure such access. In neither case are measures required that would impose a disproportionate burden. In assessing whether the burden is disproportionate, account should be taken of a number of factors including the size, resources and nature of the organisation. The principle of reasonable accommodation and disproportionate burden are established in Directive 2000/78/EC and the UN Convention on Rights of Persons with Disabilities.

(20)      Legal requirements[\[15\]](#_ftn15) and standards on accessibility have been established at European level in some areas while Article 16 of Council Regulation 1083/2006 of 11 July 2006 on the European Regional Development Fund, the European Social Fund and the Cohesion Fund and repealing Regulation (EC) No 1260/1999[\[16\]](#_ftn16) requires that accessibility for disabled persons is one of the criteria to be observed in defining operations co-financed by the Funds. The Council has also emphasised the need for measures to secure the accessibility of cultural infrastructure and cultural activities for people with disabilities[\[17\]](#_ftn17).

(21)      The prohibition of discrimination should be without prejudice to the maintenance or adoption by Member States of measures intended to prevent or compensate for disadvantages suffered by a group of persons of a particular religion or belief, disability, age or sexual orientation. Such measures may permit organisations of persons of a particular religion or belief, disability, age or sexual orientation where their main object is the promotion of the special needs of those persons.

  

(22)      This Directive lays down minimum requirements, thus giving the Member States the option of introducing or maintaining more favourable provisions. The implementation of this Directive should not serve to justify any regression in relation to the situation which already prevails in each Member State.

(23)      Persons who have been subject to discrimination based on religion or belief, disability, age or sexual orientation should have adequate means of legal protection. To provide a more effective level of protection, associations, organisations and other legal entities should be empowered to engage in proceedings, including on behalf of or in support of any victim, without prejudice to national rules of procedure concerning representation and defence before the courts.

(24)      The rules on the burden of proof must be adapted when there is a prima facie case of discrimination and, for the principle of equal treatment to be applied effectively, the burden of proof must shift back to the respondent when evidence of such discrimination is brought. However, it is not for the respondent to prove that the plaintiff adheres to a particular religion or belief, has a particular disability, is of a particular age or has a particular sexual orientation.

(25)      The effective implementation of the principle of equal treatment requires adequate judicial protection against victimisation.

(26)      In its resolution on the Follow-up of the European Year of Equal Opportunities for All (2007), the Council called for the full association of civil society, including organisations representing people at risk of discrimination, the social partners and stakeholders in the design of policies and programmes aimed at preventing discrimination and promoting equality and equal opportunities, both at European and national levels.

(27)      Experience in applying Directives 2000/43/EC and 2004/113/EC show that protection from discrimination on the grounds covered by this Directive would be strengthened by the existence of a body or bodies in each Member State, with competence to analyse the problems involved, to study possible solutions and to provide concrete assistance for the victims.

  

(28)      In exercising their powers and fulfilling their responsibilities under this Directive, these bodies should operate in a manner consistent with the United Nations Paris Principles relating to the status and functioning of national institutions for the protection and promotion of human rights.

(29)      Member States should provide for effective, proportionate and dissuasive sanctions in case of breaches of the obligations under this Directive.

(30)      In accordance with the principles of subsidiarity and proportionality as set out in Article 5 of the EC Treaty, the objective of this Directive, namely ensuring a common level of protection against discrimination in all the Member States, cannot be sufficiently achieved by the Member States and can therefore, by reason of the scale and impact of the proposed action, be better achieved by the Community. This Directive does not go beyond what is necessary in order to achieve those objectives.

(31)      In accordance with paragraph 34 of the interinstitutional agreement on better law-making, Member States are encouraged to draw up, for themselves and in the interest of the Community, their own tables, which will, as far as possible, illustrate the correlation between the Directive and the transposition measures and to make them public.

HAS ADOPTED THIS DIRECTIVE:

Chapter 1

GENERAL PROVISIONS

Article 1  
Purpose

This Directive lays down a framework for combating discrimination on the grounds of religion or belief, disability, age, or sexual orientation, with a view to putting into effect in the Member States the principle of equal treatment[\[18\]](#_ftn18) **in certain fields**  other[\[19\]](#_ftn19) than **\[…\]** employment and occupation.

**In the implementation of the present Directive, Member States shall take into account the different ways in which women and men experience discrimination on the abovementioned grounds**[\[20\]](#_ftn20)**.**

Article 2[\[21\]](#_ftn21)  
Concept of discrimination

1.       For the purposes of this Directive, **\[…\]**[\[22\]](#_ftn22) there shall be no direct or indirect discrimination on any of the grounds referred to in Article 1.

  

2.       For the purposes of paragraph 1, **the following definitions apply****:**

(a)     direct discrimination shall be taken to occur where one person is treated less favourably than another is, has been or would be treated in a comparable situation, on any of the grounds referred to in Article 1;

(b)     indirect discrimination[\[23\]](#_ftn23) shall be taken to occur where an apparently neutral provision, criterion or practice would put persons of a particular religion or belief, a particular disability, a particular age, or a particular sexual orientation at a particular disadvantage compared with other persons, unless that provision, criterion or practice is objectively justified by a legitimate aim and the means of achieving that aim are appropriate and necessary.[\[24\]](#_ftn24)

3.       Harassment shall be deemed to be a form of discrimination within the meaning of paragraph 1, when unwanted[\[25\]](#_ftn25) conduct related to any of the grounds referred to in Article 1 takes place with the purpose or effect of violating the dignity of a person and of creating an intimidating, hostile, degrading, humiliating or offensive environment. **In this context, the concept of harassment may be defined in accordance with the national laws and practice of the Member States**[\[26\]](#_ftn26)**.**

**3a. (new)**[\[27\]](#_ftn27) **Discrimination includes direct discrimination or harassment**[\[28\]](#_ftn28) **due to a person's association with persons of a certain religion or belief, persons with disabilities, persons of a certain age or of a certain sexual orientation.**

4.       An instruction to discriminate against persons on any of the grounds referred to in Article 1 shall be deemed to be discrimination within the meaning of paragraph 1.

5.       Denial of reasonable accommodation in a particular case as provided for by Article 4 (1)**(a)** of the present Directive as regards persons with disabilities shall be deemed to be discrimination within the meaning of paragraph 1.

6.       **This Directive does not preclude** differences of treatment on grounds of age **and disability**[\[29\]](#_ftn29), if they are **objectively and reasonably**[\[30\]](#_ftn30)  justified by a legitimate aim, and if the means of achieving that aim are appropriate and necessary.

**Such differences of treatment may include the fixing of a specific age**[\[31\]](#_ftn31) **for access to social protection, including social security and healthcare; social advantages; education; and certain goods or services**[\[32\]](#_ftn32) **which are available to the public.**

**The following are deemed to be compatible with the principle of non-discrimination:**

**the setting of more favourable**[\[33\]](#_ftn33) **conditions for access to social protection, including social security and healthcare; social advantages; education; and certain goods or services which are available to the public, for young or old**[\[34\]](#_ftn34) **persons or for persons with disabilities, in order to promote their economic, cultural or social integration**[\[35\]](#_ftn35)**.**

  

7.       Notwithstanding paragraph 2, in the provision of financial services[\[36\]](#_ftn36) proportionate differences in treatment where, for the product in question, the use of age or disability is a **determining** factor in the assessment of risk based on relevant **actuarial principles**, accurate statistical data or **medical knowledge**[\[37\]](#_ftn37) **shall not be considered** discrimination for the purposes of this Directive.

8.       This Directive shall be without prejudice to general measures laid down in national law which, in a democratic society, are necessary for public security, for the maintenance of public order and the prevention of criminal offences, for the protection of health and the protection of the rights and freedoms of others.

Article 3  
Scope

1.       Within the limits of the powers conferred upon the Community, the prohibition of discrimination shall apply to all persons, as regards both the public and private sectors, including public bodies, in relation to **access to**[\[38\]](#_ftn38):

  

(a)     Social protection[\[39\]](#_ftn39), including social security[\[40\]](#_ftn40) and healthcare[\[41\]](#_ftn41);

(b)          Social advantages[\[42\]](#_ftn42);

(c)          Education[\[43\]](#_ftn43);

(d)     and **the** supply of**,** goods and other services[\[44\]](#_ftn44) which are available to the public, including housing.

Subparagraph (d) shall apply to individuals only[\[45\]](#_ftn45) insofar as they are performing a professional or commercial activity **defined  in accordance with national laws and practice**.[\[46\]](#_ftn46)

\[2.     **This Directive does not apply to matters covered by family law**[\[47\]](#_ftn47)**, including marital status**[\[48\]](#_ftn48)**, and laws on reproductive rights**[\[49\]](#_ftn49).

**2a. (new)** **This Directive is without prejudice to the powers of the Member States to organise their social security and health care schemes in such a way as to guarantee their sustainability.**[\[50\]](#_ftn50)

3.       **This Directive does not apply to the content of teaching or activities and the organisation of national educational systems, including the provision of special needs education.\]**[\[51\]](#_ftn51)  This Directive is without prejudice to **conditions based on religion or belief in respect of access to educational institutions, the ethos of which is based on religion or belief, in accordance with national laws, traditions and practice. It is also without prejudice to national measures authorising or prohibiting the wearing of religious symbols**[\[52\]](#_ftn52)**.**

4.[\[53\]](#_ftn53) This Directive is without prejudice to national legislation ensuring the secular nature of the State, State institutions or bodies, or education, or concerning the status and activities[\[54\]](#_ftn54) of churches and other organisations based on religion or belief. It is equally without prejudice to national legislation promoting equality between men and women.

5.       This Directive does not cover differences of treatment based on nationality and is without prejudice to provisions and conditions relating to the entry into and residence of third-country nationals and stateless persons in the territory of Member States, and to any treatment which arises from the legal status of the third-country nationals and stateless persons concerned.

Article 4[\[55\]](#_ftn55)  
Equal treatment of persons with disabilities

1.[\[56\]](#_ftn56) In **\[…\]** compliance with the principle of equal treatment[\[57\]](#_ftn57) in relation to persons with disabilities, **Member States shall take the necessary**[\[58\]](#_ftn58) **measures to ensure that persons with disabilities have:**

**-        effective and non-discriminatory access**[\[59\]](#_ftn59) **to social protection, social advantages, health care, education, and**

**-        effective and non-discriminatory access to and supply of goods and services which are available to the public**[\[60\]](#_ftn60)**, including housing**[\[61\]](#_ftn61) **and transport**[\[62\]](#_ftn62)**.**

**Such measures shall include appropriate modifications or adjustments by anticipation**[\[63\]](#_ftn63)**.** Such measures should not impose a disproportionate burden[\[64\]](#_ftn64), nor require fundamental alteration[\[65\]](#_ftn65) of the nature of the social protection,[\[66\]](#_ftn66) social advantages, health care, education, or goods and services in question or require the provision of alternatives thereto.

**1a.** **Without prejudice to** the obligation to ensure effective non-discriminatory access and where needed in a particular case, reasonable accommodation shall be provided unless this would impose a disproportionate burden. **For the purposes of this provision, "reasonable accommodation" means**[\[67\]](#_ftn67) **measures needed in a particular case to enable a person with a disability to have access to and enjoy or exercise on an equal basis with others rights concerning social protection, including social security and health care, social advantages, education and access to goods and services in the meaning of Article 3, paragraph 1**[\[68\]](#_ftn68)**.**

  

2.       For the purposes of assessing whether measures necessary to comply with paragraph**s 1 and 1a** would impose a disproportionate burden, account shall be taken, in particular[\[69\]](#_ftn69), of the size and resources of the organisation **or enterprise**[\[70\]](#_ftn70), its nature, the estimated cost, the life cycle of the goods[\[71\]](#_ftn71) and services, and the possible benefits of increased access for persons with disabilities. The burden shall not be **deemed** disproportionate when it is sufficiently remedied by measures existing within the framework of the equal treatment policy[\[72\]](#_ftn72) of the Member State concerned[\[73\]](#_ftn73).

**2a.**[\[74\]](#_ftn74) **(new) \[In order to take account of particular conditions\]**[\[75\]](#_ftn75)**, the Member States may, if necessary:**

**a)      have an additional period**[\[76\]](#_ftn76) **of \[X\] years \[from the deadline for transposition\] to comply with paragraphs 1 and 1a.**

**b)      have an additional period of \[Y\] years \[from the deadline for transposition\] to comply with paragraph 1 when this would require adapting existing buildings**[\[77\]](#_ftn77) **or infrastructures.**

**Member States wishing to use the additional periods laid down in paragraphs 1 and 1a shall inform the Commission and provide reasons**[\[78\]](#_ftn78) **for the request for additional time by the deadline set in Article 15 \[date of transposition\].**

**3\.** This Directive shall be without prejudice to the provisions of Community law or national rules **providing for detailed standards or specifications on** accessibility[\[79\]](#_ftn79) **to** particular goods or services**, as long as such rules do not restrict the application**[\[80\]](#_ftn80) **of paragraphs 1 and 1a**.

Article 5  
Positive action

With a view to ensuring full equality in practice, the principle of equal treatment shall not prevent any Member State[\[81\]](#_ftn81) from maintaining or adopting specific measures to prevent or compensate for disadvantages linked to religion or belief, disability, age, or sexual orientation.

Article 6  
Minimum requirements

1.       Member States may introduce or maintain provisions which are more favourable to the protection of the principle of equal treatment than those laid down in this Directive.

  

2.       The implementation of this Directive shall under no circumstances constitute grounds for a reduction[\[82\]](#_ftn82) in the level of protection against discrimination already afforded by Member States in the fields covered by this Directive.

CHAPTER II  
REMEDIES AND ENFORCEMENT

Article 7  
Defence of rights

1.       Member States shall ensure that judicial and/or administrative procedures, including where they deem it appropriate conciliation procedures, for the enforcement of obligations under this Directive are available to all persons who consider themselves wronged by failure to apply the principle of equal treatment to them, even after the relationship in which the discrimination is alleged to have occurred has ended.

2.       Member States shall ensure that associations, organisations or other legal entities, which have a legitimate interest in ensuring that the provisions of this Directive are complied with, may engage[\[83\]](#_ftn83), either on behalf or in support[\[84\]](#_ftn84) of the complainant, with his or her approval, in any judicial and/or administrative procedure provided for the enforcement of obligations under this Directive[\[85\]](#_ftn85).

  

3.       Paragraphs 1 and 2 shall be without prejudice to national rules relating to time limits for bringing actions as regards the principle of equality of treatment.

Article 8  
Burden of proof

1.       Member States shall take such measures as are necessary, in accordance with their national judicial systems, to ensure that, when persons who consider themselves wronged because the principle of equal treatment has not been applied to them establish, before a court or other competent authority, facts from which it may be presumed that there has been direct or indirect discrimination, it shall be for the respondent to prove[\[86\]](#_ftn86) that there has been no breach of the prohibition of discrimination.

2.       Paragraph 1 shall not prevent Member States from introducing rules of evidence which are more favourable to plaintiffs.

3.         Paragraph 1 shall not apply to criminal procedures.

  

4.       Member States need not apply paragraph 1 to proceedings in which the court or competent body investigates the facts of the case[\[87\]](#_ftn87).

5.       Paragraphs 1, 2, 3 and 4 shall also apply to any legal proceedings commenced in accordance with Article 7(2).

Article 9  
Victimisation

Member States shall introduce into their national legal systems such measures as are necessary to protect individuals from any adverse treatment or adverse consequence as a reaction to a complaint or to proceedings[\[88\]](#_ftn88) aimed at enforcing compliance with the principle of equal treatment.

Article 10  
Dissemination of information

Member States shall ensure that the provisions adopted pursuant to this Directive, together with the relevant provisions already in force, are brought to the attention of the persons concerned by appropriate means throughout their territory.

Article 11  
Dialogue with relevant stakeholders

With a view to promoting the principle of equal treatment, Member States shall encourage dialogue with relevant stakeholders, \[in particular non-governmental organisations, which have, in accordance with their national law and practice, a legitimate interest in contributing to the fight against discrimination on the grounds and in the areas covered by this Directive\][\[89\]](#_ftn89).

Article 12[\[90\]](#_ftn90)  
Bodies for the Promotion of Equal treatment

1.       Member States shall designate a body or bodies for the promotion of equal treatment of all persons irrespective of their religion or belief, disability, age, or sexual orientation. \[These bodies[\[91\]](#_ftn91) may form part of agencies charged at national level with the defence of human rights or the safeguard of individuals' rights, including rights under other Community acts including Directives 2000/43/EC and 2004/113/EC.\][\[92\]](#_ftn92)

  

2.       Member States shall ensure that the competences of these bodies include:

-        without prejudice to the right of victims and of associations, organizations or other legal entities referred to in Article 7(2), providing independent assistance to victims of discrimination in pursuing their complaints about discrimination,

-        conducting independent surveys concerning discrimination,

-        publishing independent reports and making recommendations on any issue relating to such discrimination.

CHAPTER III  
FINAL PROVISIONS

Article 13  
Compliance

Member States shall take the necessary measures to ensure that the principle of equal treatment is respected and in particular that:

(a)     any laws, regulations and administrative provisions contrary to the principle of equal treatment are abolished;

(b)     any contractual provisions, internal rules of undertakings, and rules governing profit-making or non-profit-making associations contrary to the principle of equal treatment are, or may be, declared null and void or are amended.

Article 14[\[93\]](#_ftn93)  
Sanctions

Member States shall lay down the rules on sanctions applicable to breaches of the national provisions adopted pursuant to this Directive, and shall take all measures necessary to ensure that they are applied. Sanctions may comprise the payment of compensation, which may not be restricted by the fixing of a prior upper limit[\[94\]](#_ftn94), and must be effective, proportionate and dissuasive.

Article 15  
Implementation

1.       Member States shall adopt the laws, regulations and administrative provisions necessary to comply with this Directive by …. at the latest \[two years[\[95\]](#_ftn95) after adoption\]. They shall forthwith inform the Commission thereof and shall communicate to the Commission the text of those provisions and a correlation table[\[96\]](#_ftn96) between those provisions and this Directive.

When Member States adopt these measures, they shall contain a reference to this Directive or be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.

  

2.       In order to take account of particular conditions, Member States may, if necessary, establish that the obligation to provide effective access as set out in Article 4 has to be complied with by … \[at the latest\] four[\[97\]](#_ftn97) \[years after adoption\].

Member States wishing to use this additional period shall inform the Commission at the latest by the date set down in paragraph 1 giving reasons.

Article 16  
Report

1.       Member States and national equality bodies[\[98\]](#_ftn98) shall communicate to the Commission, by …. at the latest and every five years thereafter, all the information necessary for the Commission to draw up a report to the European Parliament and the Council on the application of this Directive.

2.       The Commission's report shall take into account, as appropriate, the viewpoints of the social partners[\[99\]](#_ftn99) and relevant non-governmental organizations, as well as the EU Fundamental Rights Agency. In accordance with the principle of gender mainstreaming, this report shall, inter alias, provide an assessment of the impact of the measures taken on women and men. In the light of the information received, this report shall include, if necessary, proposals to revise and update this Directive.

Article 17  
Entry into force

This Directive shall enter into force on the day of its publication in the Official Journal of the European Union.

Article 18  
Addressees

This Directive is addressed to the Member States.

Done at Brussels,

For the Council

The President

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) OJ C , , p. .

[\[2\]](#_ftnref2) OJ C , , p. .

[\[3\]](#_ftnref3) The references to the European Economic and Social Committee and the Committee of the Regions have been deleted as consultation of these bodies is not provided for under Article 13 of the EC Treaty.

[\[4\]](#_ftnref4) Presidency conclusions of the Brussels European Council of 14 December 2007, point 50.

[\[5\]](#_ftnref5) Resolution of 20 May 2008 P6_TA-PROV(2008)0212

[\[6\]](#_ftnref6) COM (2008) 412

[\[7\]](#_ftnref7) Directive 2000/43/EC, Directive 2000/78/EC and Directive 2004/113/EC

[\[8\]](#_ftnref8) See doc. DS 1113/08, option a).

[\[9\]](#_ftnref9) LV suggested: "where service providers have shown that such factors are…".

[\[10\]](#_ftnref10) DE requested clarification regarding the meaning of the reference to the aim and scope of previous Directives.

[\[11\]](#_ftnref11) DE requested clarification of the term "general competences".

[\[12\]](#_ftnref12) MT supported by IT and PL suggested "…family law **and public policy**, including marital status, **adoption** and…" LV and LT also supported the addition of "adoption".

[\[13\]](#_ftnref13) PL entered a reservation. This delegation suggested adding "and the benefits dependent thereon" (see doc. DS 813/08).

[\[14\]](#_ftnref14) See doc. DS 1113/08, option a).

[\[15\]](#_ftnref15) Regulation (EC) No. 1107/2006 and Regulation (EC) No 1371/2007

[\[16\]](#_ftnref16) OJ L 210, 31.7.2006, p.25. Regulation as last amended by Regulation (EC) No 1989/2006 (OJ L 411, 30.12.2006, p.6).

[\[17\]](#_ftnref17) OJ C 134, 7.6.2003, p.7

[\[18\]](#_ftnref18) IE suggested deleting the words "with a view to putting into effect in the Member States the principle of equal treatment". The Commission representative reiterated the view that this principle was long established in the jurisprudence of the European Court of Justice (ECJ).

[\[19\]](#_ftnref19) DE would have preferred a positive definition of the scope of the Directive.

[\[20\]](#_ftnref20) Several delegations (CZ, DK, EL) considered that the gender mainstreaming clause should be moved to the recitals. BE, ES, PT, SI, SE and the Commission representative supported including this provision in the operative part of the text, ES and FI also making the point that it should also be cited in articles other than Article 1, as appropriate. While supporting the idea of gender mainstreaming, IE, FI and UK requested clarification regarding the precise obligations it entailed, IE and UK wondering whether its inclusion in Article 1 would mean that it could give rise to cases of multiple discrimination. UK also requested clarification regarding the relationship between this provision and the provision concerning discrimination by association in Article 2(3a). MT entered a scrutiny reservation.

[\[21\]](#_ftnref21) MT and LT entered scrutiny reservations.

[\[22\]](#_ftnref22) PL and HU suggested reintroducing the words "the 'principle of equal treatment' shall mean that…".

[\[23\]](#_ftnref23) IE entered a reservation on the definition of "indirect discrimination", on the grounds that a _potential_ disadvantage should not be viewed as constituting indirect discrimination.

[\[24\]](#_ftnref24) ES expressed the view that national legislation addressing indirect discrimination should be recognised in the Directive, in line with Article 2(2)(b)(ii) of Directive 2000/78/EC. This delegation made a written suggestion (see doc. DS 1215/1/08 REV 1).

[\[25\]](#_ftnref25) ES and PT suggested deleting this word.

[\[26\]](#_ftnref26) PL entered a reservation on the grounds that the current formulation would lead to legal uncertainty. (Cf. Directives 2000/43/EC and 2000/78/EC.) UK underlined the need to ensure a balance between anti-discrimination on the one hand and freedom of speech on the other.

[\[27\]](#_ftnref27) The Chair explained that the wording was intended to take into account Case C-303/06 ("Coleman"), where the ECJ found that the prohibition of direct discrimination was not limited only to people who were themselves disabled. CZ, NL, IT and LV requested examples of discrimination by association on grounds other than disability (e.g. age), and clarification as to whether the Court's judgment applied to such other grounds. The Commission representative confirmed that it did. Unable to accept the extension of "discrimination by association" to grounds other than disability, DE and IT entered reservations. EL also entered a reservation on the notion of discrimination by association on grounds of religion or belief. CZ and HU saw a need to clarify the concept of "association". The Commission representative explained that the relevant association was made in the act of discrimination. MT suggested adding a reference to "national laws and practice".

[\[28\]](#_ftnref28) The paragraph has been revised to the effect that it covers only _direct_ discrimination; the Commission representative confirmed that only _direct_ discrimination and _harassment_ were covered by the judgment. UK also pointed out that "reasonable accommodation" was not covered by the judgment in question.

[\[29\]](#_ftnref29) Certain delegations (NL, UK) requested clarification regarding the need to mention "disability" alongside "age" and expressed the concern that the provision as drafted might actually lessen the protection afforded to persons with disabilities, UK also making the point that measures offering free travel or parking spaces to persons with disabilities might be challenged by non-disabled persons. NL also stressed the need to ensure that there was no contradiction with Directive 2000/78/EC.

[\[30\]](#_ftnref30) DK and DE saw a need to clarify the concept of "objectively and reasonably justified". DE expressed the view that this wording was unduly restrictive, given the Member States' prerogatives in this area.

[\[31\]](#_ftnref31) DK requested clarification regarding the reference to "age" and "access" in this context, particularly in regard to social security. PL suggested referring to "specific rules" rather than "a specific age". This delegation made a written suggestion (see doc. DS 1173/08).

[\[32\]](#_ftnref32) DE and SE suggested listing examples of such goods and services (e.g. alcoholic drinks or driving licences; cf. Recital 14a). SE also saw a need to improve the coherence between Recital 14a and Article 2(6).

[\[33\]](#_ftnref33) DE, LU, PL, UK and FI pointed out that differences of treatment based on age were not always more favourable; for example, certain kinds of medical treatment may be refused to older patients. The Commission representative explained that the example cited was one that could be "objectively and reasonably justified". She recalled that protection against age discrimination was established as a principle of law (cf. Case C-144/04 ("Mangold"). The Chair explained that the last sub-paragraph of Article 2(6) were not subject to the objective and reasonable justification required in the first sub-paragraph. LU and FI also saw a need to clarify the interrelationship between Article 2(6) and Article 5.

[\[34\]](#_ftnref34) CZ and NL expressed the view that the reference to "young or old" persons was too vague.

[\[35\]](#_ftnref35) DE expressed the view that further justification for the provisions contained in this paragraph should be given in the text. IE expressed the view that the wording should be broad enough to allow for differences of treatment in respect of children, and of children of different ages.

[\[36\]](#_ftnref36) UK entered a scrutiny reservation.

[\[37\]](#_ftnref37) IT expressed doubts regarding the choice of the term "medical knowledge"; this delegation tentatively suggested using the term "experience" and recalled the transparency provisions contained in Article 5 of Directive 2004/113/EC. DE and LV also called for the terms to be clarified (see also suggestion by LV in Recital 15). IE suggested adding "or other relevant underwriting or commercial factors", and made the remark that the burden of proof should lie with the companies that were in possession of the relevant data.  BE, NL and UK entered scrutiny reservations on Article 2(7).

[\[38\]](#_ftnref38) All delegations have scrutiny reservations. CZ, DK, DE, IT, LV, LT, MT, PL, NL, FI and UK expressed the view that further fine-tuning might be needed with a view to clarifying the division of competences between the European Community and its Member States in regard to the matters listed in this paragraph. In particular, these delegations expressed the view that _access_ to fields such as education (DK, DE, LV, NL, FI, SE, UK), healthcare (CZ, DK, DE, NL, UK) and social protection (BE, CZ, DE, IE, NL, PL, UK) was not easy to distinguish from _the organisation or management_ of such fields. UK entered a reservation and undertook to table a written suggestion. EL, DE and IT called for clarification in regard to the link between the notion of "access" and the cross-border dimension that underlies the Community competences (see also Opinion of the Council Legal Service in doc. 14896/08). FI requested clarification as to the interrelationship between the present draft Directive and Directive 2000/43/EC, which does not mention "access" in the scope.

[\[39\]](#_ftnref39) CZ and PL suggested clarifying this term.

[\[40\]](#_ftnref40) BE, CZ, DK, DE, IE and UK stressed that _social security_ was the responsibility of the Member States, DE also expressing the view that it should be made clear that civil service pensions, being related to employment, were not covered by the Directive.

[\[41\]](#_ftnref41) DE expressed the view that healthcare should be deleted from the scope.

[\[42\]](#_ftnref42) PL suggested that the meaning of this term be clarified.

[\[43\]](#_ftnref43) DE and EL expressed the view that education should be deleted from the scope. NL and FI requested clarification as to whether for example, _bullying_ would be covered by the provisions, FI also posing this question in respect of the grading of pupils.

[\[44\]](#_ftnref44) FI asked whether the scope of the present Directive was identical to that of Directive 2004/113/EC.

[\[45\]](#_ftnref45) SE suggested deleting "only".

[\[46\]](#_ftnref46) DE expressed the view that using the wording in Article 3(1) of Directive 2004/113/EC would offer greater legal clarity. This delegation also expressed the view that the proposed Directive ought to apply only to so-called "bulk business" ("_Massengeschäfte_"), i.e. transactions which are conducted impersonally and in standardised fashion, "irrespective of the person concerned" (see doc. DS 1183/08). The Commission representative expressed the concern that this would limit the scope unduly, as goods and services such as insurance, credits, healthcare and housing were typically offered person to person.

[\[47\]](#_ftnref47) MT supported by IT and PL suggested "…family law **and public policy**, including marital status, **adoption** and…" LV and LT also supported the addition of "adoption".

[\[48\]](#_ftnref48) IE made the point that difference in treatment based on marital status also arose in areas other than family law.

[\[49\]](#_ftnref49) PL entered a reservation. This delegation suggested adding "and the benefits dependent thereon" (see doc. DS 813/08).

[\[50\]](#_ftnref50) FI suggested that the wording of Article 3(2a) should be consistent with that of Recital 17a.

[\[51\]](#_ftnref51) See doc. DS 1113/08, option b).

[\[52\]](#_ftnref52) FI suggested adding "in schools". NL warned against a potential conflict with the provisions of Directive 2000/78/EC, which would not allow any prohibition of the wearing of religious symbols. The Chair pointed out that the scope of the present Directive was different. The Commission representative expressed the view that Directive 2000/78/EC was without prejudice to measures "for the protection of the rights and freedoms of others" (see Article 2(5)). HU suggested deleting the last sentence of Article 3(3).

[\[53\]](#_ftnref53) CZ, IE and MT considered this paragraph to be redundant as the European Community in any case had no competences for the matters in question. MT entered a scrutiny reservation. Regarding the second sentence, the Commission representative explained that the purpose was to affirm the equal importance of gender equality and anti-discrimination measures.

[\[54\]](#_ftnref54) FI raised the question as to whether, for example, church organisations could be allowed to offer services to their members only.

[\[55\]](#_ftnref55) LT and SE entered scrutiny reservations on Article 4.

[\[56\]](#_ftnref56) DE and AT requested that definitions for the terms used in Article 4(1) be provided.

[\[57\]](#_ftnref57) DE raised the question as to whether this reference to the principle of equal treatment was useful. See also Article 1.

[\[58\]](#_ftnref58) FI suggested adding "and appropriate", with a view to consistency with the UN Convention on the Rights of Persons with Disabilities.

[\[59\]](#_ftnref59) NL and AT suggested aligning the wording of Article 4(1) with the UN Convention, where the phrase "on an equal basis with others" is used. DE and MT saw a need to clarify the terms "access" and "accessibility" (see Article 4(3)).

[\[60\]](#_ftnref60) IE and MT requested clarification regarding obligations placed on the private sector by the proposed Directive. The Chair recalled that the draft Directive was addressed to the Member States, who would be responsible for its transposition.

[\[61\]](#_ftnref61) Several delegations (IE, UK) requested clarification of the provisions on housing and SE expressed concern in regard to the cost implications. UK raised the question as to whether landlords would be obliged to install lifts or wheelchair ramps in their properties.

[\[62\]](#_ftnref62) DE requested clarification of the practical meaning of the provisions concerning transport. This delegation also raised the question as to whether traffic safety and the interests of users other than persons with disabilities could be taken into consideration when defining effective and non-discriminatory access.

[\[63\]](#_ftnref63) DE, IE, IT, MT and NL called for this term to be clarified.

[\[64\]](#_ftnref64) MT called for this term to be clarified.

[\[65\]](#_ftnref65) IT expressed the view that the term "fundamental alteration" needed to be clarified.

[\[66\]](#_ftnref66) HU raised the question as to why "social security" was omitted here.

[\[67\]](#_ftnref67) FI raised the question as to whether the word "appropriate" should be added here, in line with the wording used in Directive 2000/78/EC and the UN Convention.

[\[68\]](#_ftnref68) Several delegations raised the question of the different wording used in Articles 4(1) and 3(2a), as compared with Articles 4(1a), 3(1)(a), in respect of the fields covered. NL suggested that the wording of Article 4(1a) be aligned with Article 3(1), particularly in respect of 3(1)(d). Certain delegations (DE, HU) raised the question as to why the wording of the definition of "reasonable accommodation" was not exactly the same as in the UN Convention. UK supported by BG, DK, IT, MT and SE suggested that the meaning of "reasonable accommodation" be illustrated by means of examples in the Recitals. ES supported by HU suggested rewording the definition (see doc. DS 1215/1/08 REV 1).

[\[69\]](#_ftnref69) CZ asked whether safety and health considerations could also be taken into account when assessing whether measures would impose a disproportionate burden.

[\[70\]](#_ftnref70) DE suggested that, in the interest of greater legal certainty, SMEs of a particular size (e.g. those with 20 or fewer employees) be exempted.

[\[71\]](#_ftnref71) UK entered a reservation on the inclusion of manufactured goods in the scope.

[\[72\]](#_ftnref72) IE suggested "disability policy".

[\[73\]](#_ftnref73) Responding to CZ and LV, the Commission representative explained this provision by giving the example of a Member State that offered a subsidy for the installation of a ramp or a lift in a building; where such subsidies were offered, the owner of the building could not claim that the installation of a ramp or a lift imposed a disproportionate burden.

[\[74\]](#_ftnref74) PT and LT entered scrutiny reservations on the implementation dates; see also Article 15.

[\[75\]](#_ftnref75) DK and FI asked for the term "particular conditions" to be clarified. The Commission representative indicated that the words in square brackets could possibly be deleted.

[\[76\]](#_ftnref76) AT was unable to accept the inclusion of an additional period as it might lead to a hierarchy between the different discrimination grounds.

[\[77\]](#_ftnref77) CZ, LV and MT suggested that adaptation be required only for new buildings or when a building was renovated, or when its use was changed. UK also endorsed this approach. MT entered a scrutiny reservation.

[\[78\]](#_ftnref78) Responding to MT, the Commission representative explained that this provision did not imply that the Member States had to request permission to extend the implementation period, but were merely required to inform the Commission. MT entered a scrutiny reservation.

[\[79\]](#_ftnref79) DE and MT entered scrutiny reservations. DE called for the reference to "detailed specifications" to be clarified. DE also suggested removing rail and air transport from the scope of the Directive, as these are already covered by detailed Community legislation. UK asked whether the provisions would apply to vehicles to which neither national nor Community legislation applied. The Commission representative explained that, where detailed specifications aiming to ensure access for persons with disabilities existed, they would apply as long as they did not restrict the application of the principles of general access. Responding to DE, she suggested that the term "accessibility" be replaced with "general access".

[\[80\]](#_ftnref80) DE and UK requested that this provision be clarified.

[\[81\]](#_ftnref81) Responding to BE, the Commission representative confirmed that private actors such as service providers (as well as the Member States) could also engage in positive action, and that they could adopt such measures themselves if the national legal framework so provided.

[\[82\]](#_ftnref82) Responding to IE, which has sought assurances that certain national practices (e.g. cheap tickets for young persons) could continue under the Directive, the Commission representative explained that the Directive should not be used as a pretext for lowering the level of protection _as a whole_, and that the Member States would still be free to change their legislation after the Directive entered into force.

[\[83\]](#_ftnref83) IE entered a reservation on Article 7. This delegation suggested adding here: "as the Member States so determine and in accordance with the criteria laid down by their national law". DE, EL, IT and AT, similarly, favoured retaining a reference to the criteria laid down by national law (cf. Directives 2000/43/EC and 2000/78/EC).

[\[84\]](#_ftnref84) Responding to DE and AT, the Commission representative confirmed that a request from the person concerned was needed before associations, organisations or other legal entities could represent a complainant before a court. She also explained that in the Commission's view, such entities should be able to act either on behalf or in support of complainants. NL, for its part, considered that the text should not be modified in a way that would limit the ability of these entities to perform their work.

[\[85\]](#_ftnref85) IE suggested adding a new sentence at the end of the paragraph, as follows: "This is without prejudice to national rules of procedure concerning representation and defence before the courts." The Commission representative explained that Article 7(2) was not intended to lay down rules regarding representation in court, which were a matter of national procedural law.

[\[86\]](#_ftnref86) Citing potential difficulties, especially in the private sphere, CZ was unable to support this provision. NL and DE raised the question as to what kind of proof service providers accused of discrimination could present in their defence, DE also warning against increasing the administrative burden. IE and LU also questioned the rationale for reversing the burden of proof in the area of goods and services, as opposed to the employment sphere. IT entered a scrutiny reservation. Responding, the Commission representative explained that the imbalance of power that was characteristic of employment relationships was not the only reason for shifting the burden of proof, but issues relating to access to information were also relevant, as affirmed by the ECJ. The Commission representative also recalled that the scope of Directive 2000/43/EC already extended beyond the employment sphere, and stated that experience from pioneering countries did not suggest that extending the protection against discrimination beyond the employment sphere necessarily led to a substantial increase in the number of court cases. IE and LU, however, remained concerned in this regard.

[\[87\]](#_ftnref87) Responding to AT and PT, the Commission representative confirmed that the wording used was identical to that used in Article 9 of Directive 2004/113/EC and Article 8 of Directive 2000/43/EC, with the exception that the present text refers explicitly to "proceedings in which the court or competent body _investigates_ the case" (as opposed to the theoretical formulation used previously "_it is for_ the court or other competent authority _to investigate_ the case").

[\[88\]](#_ftnref88) IE asked why the term "proceedings" was chosen, instead of "legal proceedings", the term used in Directives 2000/78/EC and 2004/113/EC. Responding, the Commission representative explained that no change in substance was intended.

[\[89\]](#_ftnref89) BG suggested replacing the words in square brackets with "including the relevant non-governmental organisations". EL suggested deleting the words in square brackets. Responding to AT, the Commission representative explained that the relevant stakeholders could include churches and religious organisations. She undertook to reflect on the possibility of deleting the words "in particular, non-governmental organisations".

[\[90\]](#_ftnref90) Alluding to its written suggestion set out in doc. DS 819/08, IE explained that the Paris Principles were not legally binding and should not be referred to in Recital 28; this delegation also pointed out that the Paris Principles contained prescriptive provisions regarding the independent _status_ (as opposed to the independent _function_) of national institutions for the promotion of human rights. Responding, the Commission representative explained that the intention was only to ensure that the Equality Bodies exercised their functions independently, and that the recitals did not contain rules but merely aimed to facilitate the interpretation of the Directive. Thus the cross-reference would help to clarify the fact that the Equality Body could be identical to the body foreseen in Article 33 of the UN Convention on the Rights of Persons with Disabilities. DK supported the suggestion by IE that the reference to the Paris Principles be deleted from Recital 28.

[\[91\]](#_ftnref91) Responding to NL, the Commission representative explained that the aim of this provision was to allow the Member States to decide for themselves on the institutional structure chosen. She also pointed out that certain countries were moving towards a system involving a single equality body with competence for all the discrimination grounds.

[\[92\]](#_ftnref92) BG suggested deleting the words in square brackets. Responding to AT, the Commission representative agreed that it would make sense for the equality bodies also to cover the remit of Directive 2000/78/EC (i.e. employment and occupation). AT also expressed the view that the wording of Directive 2004/113/EC should be followed here in respect of _the tasks_ of the equality bodies.

[\[93\]](#_ftnref93) DE affirmed the importance of proportionality and underlined the differences between the respective legal systems of the Member States. The Commission representative explained that the intention was to allow sufficient discretion for national legislators and judges, but that sanctions were required regardless of the type of discrimination (indirect or direct; intentional or inadvertent), in order to ensure effective protection.

[\[94\]](#_ftnref94) IE expressed the view that upper limits should be permissible here, as they also were in Directive 2000/43/EC. The Commission representative explained that if upper limits were set too _low,_ effective sanctions would not be ensured.

[\[95\]](#_ftnref95) BG, EL, LV, MT, PL, FI, SE and UK preferred a longer transposition period. Giving their tentative reactions, ES and IT also favoured a longer transposition period.

[\[96\]](#_ftnref96) BG, EL, ES, IT, MT, AT, PL and PT were unable to support the inclusion of an obligation to produce correlation tables in this Article. (See also Recital 31.) The Commission representative explained that such tables were an essential tool for the Commission and could also help to avoid unnecessary infringement proceedings.

[\[97\]](#_ftnref97) EL, LV, MT, UK considered that 4 years was too short. Giving their preliminary positions, NL and SE also considered that 4 years might not be enough. NL requested clarification as to how the length of the period had been chosen; the Commission representative pointed out that very few Member States had actually used the additional implementation period offered under Directive 2000/78/EC. PT and LT entered scrutiny reservations. PT raised the question as to which provisions in Article 4 qualified for the extended implementation period of 4 years. The Commission representative clarified this issue to the effect that _the extended implementation period applied to Article 4(1)(a) ("effective non-discriminatory access") only_. The Commission representative suggested revisiting the implementation dates once the questions raised in connection with Article 4 had been clarified.

[\[98\]](#_ftnref98) EL entered a reservation. This delegation took the view that the Member States themselves, not the Equality Bodies, should furnish information to the Commission. The Commission representative undertook to reflect on the possibility of moving the mention of the Equality Bodies to the second paragraph of the Article.

[\[99\]](#_ftnref99) BG, EL, AT and SE wished to replace "social partners" with "relevant stakeholders". The Commission representative explained that the social partners had valuable information that was relevant to policies aimed at making it possible for people to work.