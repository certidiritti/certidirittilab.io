---
title: 'ASSEMBLEA PUBBLICA CITTADINA'
date: Wed, 07 Apr 2010 12:00:06 +0000
draft: false
tags: [Senza categoria]
---

"La battaglia per il diritto all'eguaglianza: scenari e prospettive del dopo-sentenza"

Dopo il 12 aprile la Corte Costituzionale si esprimerà in merito alla questione posta da alcune coppie di persone dello stesso sesso che, nell'ambito dell'iniziativa di Affermazione civile (www.affermazionecivile.it), hanno fatto ricorso contro il rifiuto (ricevuto dai loro Comuni di residenza) alla pubblicazione degli Atti matrimoniali con rito civile.

Alcuni Tribunali e Corti d'Appello hanno rinviato la questione alla Consulta.

In attesa dell'importante pronunciamento dei Giudici costituzionali, invitiamo tutti i cittadini impegnati nella BATTAGLIA PER IL DIRITTO ALL'EGUAGLIANZA di gay e lesbiche, a partecipare a questa ASSEMBLEA PUBBLICA per discutere, confrontarsi, formarsi, organizzare il proseguimento della battaglia, a partire dalla valutazione dei diversi possibili scenari successivi alla sentenza.

PARTECIPERANNO, tra gli altri, gli avvocati Vittorio Angiolini e Massimo Clara, membri del collegio di difesa presso la Corte Costituzionale delle coppie di Affermazione civile; Vittorio Lingiardi, psichiatra, psicoanalista, docente e autore di Citizen gay; le coppie milanesi protagoniste di Affermazione civile; Ivan Scalfarotto, Vice Presidente del'Assemblea nazionale del Partito Democratico; Sergio Rovasio, Segretario dell'Associazione radicale Certi Diritti; Paolo Patané, Presidente nazionale Arcigay; Luca Trentini, Segretario nazionale Arcigay

www.certidiritti.it  
www.elfo.org