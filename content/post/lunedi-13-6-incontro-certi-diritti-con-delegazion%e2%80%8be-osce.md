---
title: 'Lunedì 13-6 incontro Certi Diritti con delegazion​e Osce'
date: Sun, 12 Jun 2011 12:42:39 +0000
draft: false
tags: [Transnazionale]
---

**Europride Roma 2011: domani incontro Associazione radicale Certi Diritti con delegazione Osce, in Italia per monitorare la situazione della libertà nel nostro Paese.**

**Comunicato Stampa dell’Associazione Radicale Certi Diritti**

Roma, 12 giugno 2011

L’ufficio per le Istituzioni Democratiche e Diritti Umani dell’Osce (Organizzazione per la sicurezza e la cooperazione in Europa), ha chiesto all’Associazione Radicale Certi Diritti un incontro che si svolgerà domani, lunedì 13 giugno 2011, alle ore 9,30 in Via di Torre Argentina, 76 – Roma.

La delegazione dell’Osce si trova a Roma per monitorare la situazione relativa alle condizioni di svolgimento dell’Euro Roma Pride.

La delegazione dei Radicali sarà composta dai Parlamentari Rita Bernardini e Marco Perduca insieme a Sergio Rovasio, Segretario Associazione Radicale Certi Diritti e Ottavio Marzocchi, responsabile questioni europee di Certi Diritti.