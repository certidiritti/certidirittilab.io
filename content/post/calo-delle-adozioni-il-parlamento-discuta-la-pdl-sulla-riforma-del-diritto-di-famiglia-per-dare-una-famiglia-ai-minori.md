---
title: 'Calo delle adozioni: il Parlamento discuta la pdl sulla Riforma del Diritto di Famiglia per dare una famiglia ai minori'
date: Tue, 17 Jan 2012 21:04:43 +0000
draft: false
tags: [Diritto di Famiglia]
---

Il calo delle coppie adottanti (-2,6%) porta ad una grave conseguenza: tantissimi bambini restano, ogni anno di più, senza una famiglia.

Roma, 16 gennaio 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Nel corso del 2011 in Italia sono stati adottati con adozione internazionale 4.022 bambini contro i 4.130 dell'anno precedente (-2,6%). Ad adottare sono state 3.154 coppie a differenza delle 3.241 dell'anno precedente. Lo certifica il Report della Commissione Adozioni che anticipa in sintesi i dati completi della statistica ufficiale che sarà online la prima settimana di febbraio.

Il calo delle coppie adottanti porta ad una grave conseguenza: tantissimi bambini restano, ogni anno di più, senza una famiglia.

E' vero che il calo delle coppie adottanti è da attribuirsi a ristrettezze economiche (data la mole di finanze richieste per affrontare un simile percorso) ed ai tempi lunghissimi di attesa per concludere l'iter. Ma oltre a ciò bisogna riconoscere che il calo delle coppie disponibili ad adottare passa anche per il mancato riconoscimento di quelle che vengono definite “coppie di fatto”, ossia quelle coppie a tutti gli effetti che non scelgono o non possono percorrere la via del matrimonio per la loro unione. La legge attuale sulle adozioni consente infatti, salvo casi “speciali”, l'adozione solo a coppie coniugate (l. 184/1983 modificata con l. 149/2001).

Negare l'adozione a queste coppie significa negare una famiglia a quei bambini orfani, in attesa di diventare figli, significa far crescere delle giovani vite in istituti e case famiglia.

Ora più che mai, anche di fronte a questi dati, è necessaria una Riforma del Diritto di Famiglia ed una grande apertura dell'accesso all'adozione anche a coppie non coniugate e persone singole, così come chiede la proposta di legge n. 3607, depositata nei mesi scorsi dai deputati Radicali, a prima firma Rita Bernardini, frutto del lavoro di decine di associazioni e personalità anche di altri gruppi politici.