---
title: 'ARCYGAY BOLOGNA SI ESPRIME SU CAMPAGNA AFFERMAZIONE CIVILE'
date: Sat, 12 Dec 2009 09:11:36 +0000
draft: false
tags: [Comunicati stampa]
---

L´associazione radicale Certi Diritti si dice doppiamente soddisfatta per l´articolo apparso su "Repubblica Bologna.it".

Il pezzo infatti esprime un duplice risultato che l´associazione sta perseguendo ormai da quasi 3 anni. Coinvolgere la più grande associazione per i diritti lgbt, l´Arcigay, e sensibilizzare personalità di spicco eterosessuali (in questo caso Fiorello) affinchè possano con il loro messaggio parlare a quante più persone possibili.

Certi Diritti dichiara che la via giuridica all´affermazione dei diritti delle coppie omosessuali grazie al proprio impegno e a quello di Rete Lenford è stata promossa già da circa due anni e battezzata 'Affernazione Civile' per rimarcare il valore civile dell'iniziativa. ll fatto che una sede molto importante di Arcigay, quella di Bologna, dichiari la propria disponibilità a sostenere questa battaglia è per noi - dichiara la presidente di Certi Diritti Clara Comelli - un punto di partenza concreto per una collaborazione che stiamo proponendo in ambito nazionale da almeno due anni. Questo ci conforta e ci fa sperare per un ulteriore coinvolgimento di tutti i circoli territoriali di Arcigay. La capillarità di Affermazione Civile è infatti uno dei presupposti per il suo successo. Al presidente di Arcigay Bologna Emiliano Zaino esprimiamo il nostro entusiasmo per la decisione di far aderire il suo circolo alla Rete Lenford. Le nostre associazioni saranno quindi a fianco in questa importante battaglia di civiltà per i diritti delle coppie dello stesso sesso.