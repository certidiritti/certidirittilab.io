---
title: 'AUSTRIA: UNIONI TRA PERSONE  STESSO SESSO, SI IN EUROPA, IGNORATE IN ITALIA.'
date: Thu, 10 Dec 2009 18:46:09 +0000
draft: false
tags: [Comunicati stampa]
---

E' con viva soddisfazione che l'Associazione radicale Certi diritti apprende dell'approvazione della nuova legge austriaca a tutela delle coppie di fatto anche omosessuali.

Siamo al paradosso. Regioni come il Friuli Venezia Giulia sono circondate letteralmente da Paesi europei dove le coppie dello stesso sesso sono tutelate giuridicamente, mentre in sul suolo italiano sono del tutto ignorate.

E' incredibile - dichiara Clara Comelli, Presidente dell'Associazione - che il Parlamento italiano non senta la minima necessità di tutelare i propri cittadini omosessuali. In alcuni casi bastano pochi minuti per raggiungere un Paese europeo per poter accedere ad un'unione legale ma, tornati in Italia, queste coppie tornano a essere giuridicamente inesistenti.

L'Italia è in Europa - conclude Clara Comelli - e i cittadini italiani devono poter godere delle stesse tutele di tutti gli altri cittadini europei.

Clara Comelli  
Presidente associazione radicali certi diritti