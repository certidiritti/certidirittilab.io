---
title: 'CONFERENZA STAMPA A TORINO DI RADICALI E CERTI DIRITTI SU UNIONI CIVILI'
date: Thu, 08 Jan 2009 09:05:47 +0000
draft: false
tags: [Comunicati stampa]
---

Sintesi conferenza stampa tenutasi nella sede radicale di Torino

Torino, 7 gennaio 2009

In una conferenza stampa tenutasi nella sede dell’Associazione Radicale Adelaide Aglietta, i radicali torinesi hanno fatto il punto sulla campagna di raccolta firme sulle due proposte di delibera d’iniziativa popolare per l’istituzione dell’anagrafe degli eletti e dei nominati (per più trasparenza in Comune) e per il sostegno alle unioni civili in vari campi (casa, sanità e servizi sociali, formazione, scuola e servizi educativi).

Erano presenti l’europarlamentare radicale Marco Cappato, il presidente di Radicali Italiani Bruno Mellano, Igor Boni (segretario Associazione Radicale Adelaide Aglietta), Stefano Mossino (tesoriere Associazione Radicale Satyagraha) ed Enzo Cucco (Associazione Radicale Certi Diritti).

Mancano solamente trenta giorni alla scadenza dei termini di raccolta firme e mancano ancora alcune centinaia di firme per raggiungere l’obiettivo.

I cittadini residenti a Torino possono firmare le due proposte di delibera:

- venerdì 9 il pomeriggio e sabato 10 la mattina sotto i portici di Piazza San Carlo, fronte Banco San Paolo;

- il lunedì alle riunioni settimanali dell’Associazione Radicale Adelaide Aglietta (Torino, via Botero n. 11/f, dalle ore 21:00 alle ore 23:00).