---
title: 'AUMENTANO ALL''ONU I PAESI  PER DEPENALIZZARE OMOSESSUALITA'''
date: Tue, 16 Dec 2008 14:30:17 +0000
draft: false
tags: [Comunicati stampa, DEPENALIZZAZIONE, OMOSESSUALITA', ONU]
---

DEPENALIZZAZIONE OMOSESSUALITA’ ALL’ONU: RAGGIUNTI 61 PAESI CO-FIRMATARI. IL 18 DICEMBRE SARA’ IL GABON A ILLUSTRARE LA PROPOSTA FRANCESE ALL’ASSEMBLEA GENERALE.

Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti e Ottavio Marzocchi, responsabile iniziative europee di Certi Diritti.

Secondo le ultime informazioni ricevute da Ginevra e New York i paesi co-firmatarti della proposta francese per la depenalizzazione universale dell’omosessualità hanno raggiunto quota 61. Tra i paesi che ancora non hanno firmato vi sono la Turchia, il Sudafrica e gli Stati Uniti. Sollecitiamo il Governo italiano ad attivarsi in tutte le sedi affinché i paesi democratici, che difendono i diritti delle persone lgbt, firmino la proposta francese entro il 18 dicembre.