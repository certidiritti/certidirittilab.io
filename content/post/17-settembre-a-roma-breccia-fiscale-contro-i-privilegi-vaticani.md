---
title: '17 settembre a Roma Breccia fiscale contro i privilegi vaticani'
date: Thu, 15 Sep 2011 18:45:03 +0000
draft: false
tags: [Politica]
---

A 141 anni dalla presa di Porta Pia, sabato 17 settembre, a partire dalle ore 15,30, corteo per una “Breccia fiscale” promossa dal gruppo facebook 'Vaticano paga anche tu' insieme a tante altre associazioni. Martedì 20 settembre sempre a Porta Pia si svolgerà la vera commemorazione della Liberazione di Roma dal potere temporale dello Stato teocratico pontificio.

  
Sabato 17 settembre, a partire dalle ore 15,30,  corteo per una “Breccia fiscale”, promosso da decine di Associazioni e dal gruppo Facebook “Vaticano paga anche tu”. Il corteo, prenderà il via dalla Breccia di Porta Pia e poi Iungo via XX settembre per arrivare davanti al Ministero dell’Economia. Sarà aperta una nuova Breccia – non solo simbolica - sui privilegi fiscali del Vaticano, per chiedere, tra l’altro, l’eliminazione dell’esenzione Ici e della riduzione Ires per gli enti ecclesiastici, e il dimezzamento dell’otto per mille.

Martedì 20 Settembre 2011, a partire dalle ore 16,30, presso la Breccia di Porta Pia, si svolgerà la vera commemorazione della Liberazione di Roma dal potere temporale dello Stato teocratico pontificio.  Per la laicità e la libertà, contro ogni forma di  fondamentalismo ideologico e religioso.

Dopo aver assistito tre anni fa a Porta Pia alla commemorazione clerical-revisionista degli zuavi pontifici, mercenari al soldo del Papa-Re, che il XX Settembre 1870 assassinarono 49 bersaglieri e dopo che lo scorso anno si è svolta la cerimonia “Dalla ‘presa di Roma’ alla resa  al Vaticano”,  continua la nostra iniziativa politica per contrastare il grave tentativo di revisionismo storico che cancella i valori del Risorgimento ed esalta una concezione politica clerical-nazionalista.

Invitiamo le Associazioni, i movimenti, le forze politiche, i cittadini, che hanno a cuore i valori della laicità e delle libertà, e che si battono contro le ingerenze vaticane, a partecipare alla VERA COMMEMORAZIONE DI PORTA PIA che si svolgerà Martedì 20 settembre 2011 a partire dalle ore 16,30 sul luogo della Breccia (Corso d’Italia - monumento ai caduti della Breccia).

Hanno finora aderito alla manifestazione:

Radicali Italiani  
Associazione Radicale Certi Diritti  
Moderatori della pagina Facebook ‘Vaticano paga tu la manovra’  
Partito Socialista Italiano – Federazione di Roma  
Gruppo consiliare Lista Bonino Pannella, Federalisti Europei - Regione Lazio  
Radicali Roma  
Anticlericale.net  
Federazione dei Giovani Socialisti italiani  
Forum delle donne Socialiste  
Associazione Nazionale del Libero Pensiero "Giordano Bruno"  
Associazione Luca Coscioni  
Nessuno Tocchi Caino  
Associazione Italialaica.it  
Radio Radicale  
www.gay.it  
Associazione democratica Giuditta Tavani Arquati  
Uaar – Roma  
Lega italiana per il Divorzio Breve  
Gaycs – Aics  Coordinamento Lgbt  
Associazione Famiglie Arcobaleno  
Movimento Identità Transessuale  
Libera Uscita, Associazione laica e apartitica  
Sinistra Ecologia Libertà – Area metropolitana Roma  
Arcigay  
Fondazione Religionfree  
CRIDES – Centro romano d’iniziativa per la difesa dei diritti nella Scuola  
Fondazione Massimo Consoli

Maria Gigliola Toniollo – Reponsabile Cgil Nuovi Diritti  
Axteismo -  No alla chiesa, no alle religioni - Movimento Internazionale di Libero Pensiero  
Democrazia Laica  
Circolo Mario Mieli di Roma;  
Rete Genitori Rainbow;  
Associazione Liberacittadinanza- Rete dei girotondi e movimenti;  
Cristiana Alicata Pd Lazio  
Luca Valeriani esponente della comunità lgbt;

    Per info e adesioni:  
Sergio Rovasio - Riccardo Magi  
[info@certidiritti.it](mailto:info@certidiritti.it)  
   

iscriviti alla newsletter >>>

**www.certidiritti.it/newsletter/newsletter**