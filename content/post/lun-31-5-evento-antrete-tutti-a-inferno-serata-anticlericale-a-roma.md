---
title: 'LUN 31-5 EVENTO: ''ANTRETE TUTTI A INFERNO!'', SERATA ANTICLERICALE A ROMA'
date: Fri, 28 May 2010 17:42:18 +0000
draft: false
tags: [Comunicati stampa]
---

[![liberostato_s](http://www.certidiritti.org/wp-content/uploads/2010/05/liberostato_s.jpg)](http://www.certidiritti.org/wp-content/uploads/2010/05/liberostato_m.jpg)

LUNEDI' 31 MAGGIO 2010 ANTICLERICALE.NET CON L'ADESIONE E LA PARTECIPAZIONE DI CERTI DIRITTI, PROMUOVE A ROMA UNA SERATA STRAORDINARIA ANTICLERICALE: 'IN LIBERO STATO - L'OCCASIONE FA L'UOMO LAICO'.

Manifestazione spettacolo in difesa della laicità dello Stato, con Alessandro Haber, Giuliana De Sio, Ennio Fantastichini, Massimo Ghini, Roy Paci, Neffa, Luca Barbarossa, Rocco Papaleo, Milena Vukotic, Gabriella Germani, Laganà, Stefano Masciarelli, Cammariere, Gianmarco Tognazzi, Monica Scattini, Andrea Rivera, Franza Di Rosa, Jimmi Ghione, Stefano Bicocchi, Stefano Disegni, Sergio Rubini e molti, molti altri peccatori....

Il Teatro Vittoria è in Via Maria Ausiliatrice, 8 - Roma (Testaccio). Ingresso 15 Euro.

Gli incassi saranno interamente devoluti per iniziative anticlericali