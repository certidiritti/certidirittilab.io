---
title: 'IL SITO  DELL’ASSOCIAZIONE RADICALE CERTI DIRITTI SOTTO ATTACCO CRACKER'
date: Sat, 27 Feb 2010 14:57:41 +0000
draft: false
tags: [Comunicati stampa]
---

Dichiarazione di _Jean Baptiste Poquelin_, Webmaster Associazione Radicale Certi Diritti:

"Il sito ufficiale dell'Associazione Radicale Certi Diritti è sotto attacco cracker. Sul sito internet www.certidiritti.it appare una scritta in turco con, in allegato, uno scipt in codice sconosciuto. Quanto appare nel sito internet di certidiritti.it è certamente opera di qualche fondamentalista che si oppone alle nostre iniziative in difesa dei diritti civili e umani."

E' la terza volta che il sito dell'Associazione radicale Certi Diritti è vittima di un attacco informatico.

La vicenda verrà segnalata all'autorità giudiziaria.