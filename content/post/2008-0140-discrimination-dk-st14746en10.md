---
title: '2008-0140-Discrimination-DK-ST14746.EN10'
date: Mon, 11 Oct 2010 10:31:59 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 11 October 2010

Interinstitutional File:

2008/0140 (CNS)

14746/10

LIMITE

SOC 633

JAI 824

MI 368

  

  

  

  

  

NOTE

from :

General Secretariat

to :

The Working Party on Social Questions

on :

19 October 2010

No. prev. doc. :

13883/10 SOC 561 JAI 759 MI 317

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

In preparation of the next meeting of the Working Party, which is provisionally scheduled for 19 October 2010, delegations will find attached a note from the DK delegation.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Questions for SQWP debate on housing**

**\[Document 13883/10\]**

**DK replies**

**1\.** **Situation in Member States**

Both the Racial Equality Directive (2000/43/EC) and the Gender Goods and Services Equality Directive (2004/113/EC) cover housing available to the public in their scope. Several MS have gone beyond these two grounds in their protection against discrimination.

a)           To what extent does your national legislation against discrimination apply to housing?

b)           Do you distinguish (for this purpose) between private and public/social housing?

There is no particular legislation against discrimination in the area of housing (neither in the private nor in the social housing sector).

c)       Do you have statistics on the number of cases of discrimination in the area of housing, and if so, what is the most prevalent ground?

No, cf. the answer above

**2\.** **Social housing**

a)             Do you consider social housing to be a service (for the purposes of EU law)?

As a principal rule social housing is considered to be a service of general economic interest. The social housing organisations receive compensation (public aid) in order to ensure that they make a sufficient number of dwellings of appropriate quality available at a rent which is reasonable for population groups which would otherwise find it difficult to obtain housing on market terms (social housing for families and young people).

  

b)           If not, do you consider it to be a part of the social protection system?

Housing organisations and local authorities also receive public aid in order to construct and rent out dwellings to elderly or disabled persons (including nursing homes). The target group of elderly or disabled persons has priority when it comes to these dwellings (positive discrimination). This service is considered to be a service of general social interest and a part of the social protection system.

**3\.** **Private life**

In some cases, the principle of equality might be in conflict with the right to private life, in particular in the area of housing (e.g. renting out a room in your own apartment).

a)       Is this better taken into account by an exclusion of transactions in the area of private life from the scope of the Directive, or by an exclusion of activities which are not commercial or professional?

b)           How is commercial/professional housing defined in your national legislation?

**4\.** **Disability - reasonable accommodation**

a)       In your view, what should the obligation to provide reasonable accommodation mean for providers of housing?

b)      Is there legislation on the accessibility of buildings (private as well as public/social) in your country?

The Danish Building Regulations 2010 (BR10) regulates building activities in Denmark and includes nine specific requirements with respect to the physical accessibility of buildings in Denmark. The latest version of BR10 came into force June 30th 2010\. Until June 30th, BR10 set out eight specific requirements on accessibility. As a direct consequence of the UN Convention on the Rights of Persons with Disabilities, the requirement in Article 9 “to provide in buildings and other facilities open to the public signage in Braille and in easy to read and understand forms” has been implemented in

  

BR10.

**5\.** **Disability - accessibility**

a)       What should the obligation to provide for accessibility measures encompass in the area of housing?

b)           Should there be a difference between old and new buildings?

Certainly. Regarding new buildings, see the answer to question 4 b). As regards old buildings, ensuring accessibility to _all_ old and existing buildings would be very costly.

c)           Do you support immediate applicability with respect to new buildings?

Yes, as we already have legislation on this issue (see question 4b) above).

d)      Should there be a difference between the obligations for private and public/social housing respectively?

No. We do not make such a distinction in existing Danish legislation.

e)  Are there public funds available in your country for the adaptation of housing (private as well as public/social)?

No. There is no public funding for adapting buildings.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_