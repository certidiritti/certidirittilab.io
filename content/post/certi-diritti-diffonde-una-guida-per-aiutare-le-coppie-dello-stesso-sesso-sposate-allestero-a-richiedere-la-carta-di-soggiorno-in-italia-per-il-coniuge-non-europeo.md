---
title: 'Certi Diritti diffonde una guida per aiutare le coppie dello stesso sesso sposate all’estero a richiedere la carta di soggiorno in Italia per il coniuge non europeo'
date: Fri, 02 Nov 2012 10:27:22 +0000
draft: false
tags: [Diritto di Famiglia]
---

Anche la Questura di Treviso rilascia il titolo di soggiorno a un ragazzo messicano sposato con un italiano.  
  
Comunicato stampa dell’Associazione Radicale Certi Diritti  
  
Roma, 2 novembre 2012  
  
  
L'Associazione Radicale Certi diritti mette a disposizione di chiunque fosse interessato una breve guida pratica in cui vengono spiegate le modalità per ottenere il rilascio della “Carta di soggiorno per famigliari di cittadini europei” alla persona non italiana di una coppia dello stesso sesso  sposata o che ha sottoscritto una unione civile all’estero.  
  
Il segretario Yuri Guaiana puntualizza: “Il nostro intento è quello di rendere i risultati che stiamo ottenendo grazie alla campagna Affermazione Civile 2.0 un patrimonio comune a tutte le associazioni che si occupano di diritti e a tutte le coppie che si trovano in condizioni analoghe a quelle che abbiamo seguito. La prassi si è ormai consolidata ed quindi possibile individuare delle modalità standard che siamo felici di mettere a disposizione di tutti”.  
  
Dopo Reggio Emilia, Milano, Rimini, Roma ora anche la Questura di Treviso ha rilasciato il titolo di soggiorno a un ragazzo messicano che si è sposato a Città del Messico con un italiano.  
Finora i due ragazzi non hanno potuto convivere stabilmente e hanno dovuto sostenere spese ingenti per i trasferimenti dall’Italia al Messico e viceversa. Ora invece il riconoscimento della loro unione, sebbene ai soli fini del soggiorno, permetterà loro “di vivere liberamente la condizione di coppia”, come la stessa Corte Costituzionale nella sentenza 138/10 ha indicato come un diritto fondamentale.  
  
La campagna di Affermazione Civile 2.0 continuerà con nuove cause pilota che facciano emergere altre discriminazioni tra le coppie matrimoniali e quelle dello stesso sesso con l’obiettivo di arrivare al matrimonio egualitario. Spiace constatare, invece, che il governo continua a tacere sulla circolare Amato che vieta la trascrizione dei matrimoni tra persone dello stesso sesso contratti all’estero.

**[LA GUIDA IN PDF DA SCARICARE >>>](notizie/comunicati-stampa/item/download/16_b0ea718c1864b66368d57ac2929995d2)**  
  
**[SOSTIENI CERTI DIRITTI >>>](partecipa/iscriviti)**

  
[guida\_Certi\_Diritti.pdf](http://www.certidiritti.org/wp-content/uploads/2012/11/guida_Certi_Diritti.pdf)