---
title: 'Vescovo spagnolo chiede scusa per offese ai gay. Quando lo faranno i vescovi italiani? Sarà necessaria un''Enciclica ad hoc!'
date: Tue, 24 Jan 2012 20:50:29 +0000
draft: false
tags: [Europa]
---

Anche vescovi italiani chiedano perdono alle persone omosessuali come ha fatto il vescovo di Tarragona in Spagna. Necessaria un'enciclica di scuse ad hoc.

Roma, 24  gennaio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti  
 

Quanto dichiarato ieri dal Vescovo di Tarragona - Spagna, Jaume Pujol Balcells,  sul fatto che i gay “non hanno un comportamento adeguato per la società”  è apparso così grave che in nemmeno 24 ore si è sentito in dovere di rivolgere le sue scuse. Davanti alla stampa ha dichiarato che  “se qualcuno si e' sentito offeso, gli chiedo perdono” ed ha chiesto di "non essere capito male" perchè "il mio cuore e' aperto a tutti".

Per cose molto più gravi, espresse da autorevoli rappresentanti della chiesa cattolica in Italia, non ci sono mai state scuse. Questo fa anche la differenza sul concetto di rispetto che vige in Spagna, rispetto all’Italia, nei confronti delle persone Lgb(e).

La comunità Lgbt italiana sta ancora aspettando le scuse, tanto per fare un esempio su uno dei casi più deliranti di ‘autorevoli’ rappresentanti della chiesa cattolica, da parte del vescovo emerito di Grosseto, che in più occasioni ha gravemente offeso le persone omosessuali. Per non parlare di tutte le altre esternazioni fatte nel corso di questi ultimi anni, quasi ogni giorno,  da altri emeriti rappresentanti della gerarchia cattolica.  Più che la richiesta di  perdono sarà necessaria un’Enciclica ad hoc di scuse che prima o poi arriverà, come sempre.