---
title: 'CERTI DIRITTI A MALTA, ALLA CONFERENZA ANNUALE DELL''ILGA SU GAY MARRIAGE'
date: Mon, 26 Oct 2009 12:26:32 +0000
draft: false
tags: [Comunicati stampa]
---

l'Associazione Radicale Certi Diritti parteciperà alla prossima Conferenza Europea annuale dell'Ilga che si svolgerà a Malta dal 29 ottobre al 1 novembre 2009.

Tra i workshops previsti ve ne sarà uno proposto da Certi Diritti su:

"Globalization of the Same Sex Marriage Issue: raising up a global awareness of the human rights values it entails" su cui stanno lavorando Luigi Caribè, Ottavio Marzocchi e Gian Mario Felicetti

  

WORKSHOP ‐ October 29th 18:00 to 19:30

associazione radicale CERTI DIRITTI

Globalization of the Same Sex Marriage Issue:

raising up a global awareness of the human rights values it entails.

freepeople‐freemarriage@certidiritti.it

  

In almost all the democratic nations worldwide there are activists who have struggled and are struggling for the right to marriage for same sex couples ‐ either lobbying complaisant politicians, or facing the juridical way.

  

The time may be ripe to overcome the national boundaries of such common initiatives,

and raise the same‐sex marriage issue at a global level in all democratic nations. In fact,

same sex marriage opponents already operate at international level, and this makes it

necessary to tune the communication toward the international organization and the public opinion, making it clearly aware of the human right significance of same sex marriage (similar to other issues in the past such us universal suffrage and free marriage for blacks).

The aim of the workshop is to start a comparison of different experiences and strategies on this item.

  

Information:

Gian Mario Felicetti e‐mail: gianmario.felicetti@gmail.com mobile: +39.329.9045945

Ottavio Mazzocchi e‐mail: ottavio.marzocchi@europarl.europa.eu

Luigi Caribè e‐mail: lcaribe@hotmail.com