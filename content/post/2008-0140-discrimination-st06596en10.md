---
title: '2008-0140-Discrimination-ST06596.EN10'
date: Wed, 17 Feb 2010 15:47:22 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 17 February 2010

Interinstitutional File:

2008/0140 (CNS)

6596/10

LIMITE

SOC 128

JAI 146

MI 55

  

  

  

  

  

NOTE

from :

The General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

6092/10 SOC 75 JAI 108 MI 39

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached a note from the Romanian delegation with a view to the meeting of the Working Party on Social Questions on 18 February 2010.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**RO proposal**

Changes in relation to doc. 6092/10 are indicated as follows: additions are in bold and deletions are marked \[…\]

Art. 2 (1)

For the purposes of this Directive, discrimination includes:

(a)     direct discrimination;

(b)     indirect discrimination;

(c)     harassment;

(d)     instruction to discriminate **\[…\];**

(e)     denial of reasonable accommodation for persons with disabilities;

(f)      direct discrimination or harassment due to a person's association **with others** **\[…\]****.**

Art. 2 (3)

**An instruction to discriminate against persons on any of the grounds referred to in Article 1 shall be deemed to be discrimination within the meaning of paragraph 1.**

Art. 2 (4)

Direct discrimination or harassment due to a person's association with persons of a certain religion or belief, persons with disabilities, persons of a given age, or of persons of a certain sexual orientation **shall be deemed to be discrimination within the meaning of paragraph 1.**

Explanatory Statement

“Instruction to discriminate” and “discrimination by association” although listed in Art. 2 (1) are not comprised in Art. 2 (2) and defined in a similar manner with the concept of direct- indirect discrimination, harassment or denial or reasonable accommodation.

  

The introduction phrase used in all cases \[Art.2 (2) a-d\] “shall be taken to occur” would be difficult to apply to both concepts (instruction to discriminate and discrimination by association). Since the amendment proposed by the Presidency in Art.2 (1) has the nature of a list of the forms of discrimination covered by the Directive, the concepts mentioned in letters d and f should follow a similar wording as in letters a-c and e. Subsequently, we suggest the mentioned deletion in **Art. 2 (1) letter** **d** and **f.**

In all relevant Directives[\[1\]](#_ftn1) “instruction to discriminate” is regulated by the wording “shall be deemed to be discrimination”. Therefore we suggest an analogous approach within a **new paragraph of Art. 2**. (See addition in bold).

Similarly, we suggest a **new paragraph** for Art. 2 regulating “discrimination by association” following the text proposed by the Presidency (Art. 2 (1) letter f) with the use of the wording “shall be deemed to be discrimination” (See addition in bold).

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) I.e. Directive 2000/43/CE: “An instruction to discriminate against persons on grounds of (...) **shall be deemed to be discrimination** within the meaning of paragraph 1”; Directive 2000/78/CE: “... **shall be deemed to be discrimination** within the meaning of paragraph 1”; Directive 2004/113/CE: "...**shall be deemed** to be discrimination within the meaning of this Directive”.