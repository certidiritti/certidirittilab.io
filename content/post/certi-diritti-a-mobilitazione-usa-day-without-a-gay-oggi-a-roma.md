---
title: 'CERTI DIRITTI A MOBILITAZIONE USA "DAY WITHOUT A GAY", OGGI A ROMA'
date: Wed, 10 Dec 2008 14:26:27 +0000
draft: false
tags: [Comunicati stampa]
---

**“DAY WITHOUT A GAY”: ANCHE CERTI DIRITTI PARTECIPA ALLA MOBILITAZIONE DEI GAY USA CONTRO LA BOCCIATURA DEI MATRIMONI GAY.**

L’Associazione Radicale Certi Diritti ha aderito e parteciperà alla manifestazione promossa oggi, mercoledì 10 dicembre, alle ore 17, a Roma, davanti all’Ambasciata Usa di Via Veneto, in sostegno alla straordinaria manifestazione promossa negli Stati Uniti “Day Without a Gay” contro la cancellazione dei diritti civili faticosamente conquistati e che vedrà astenersi dal lavoro milioni di persone omosessuali.

Parteciperemo al Sit-In con lo slogan: “Siamo al fianco del popolo Usa dei diritti civili, contro le discriminazioni e per l’estensione dei diritti a chi non ne ha”.

Per l’Associazione saranno presenti il Segretario Sergio Rovasio e la Tesoriera Roni Guetta al fianco delle Associazioni lgbt DigayProject, Arcigay, arcilesbica e tutte le altre che hanno aderito a questo importante appuntamento.