---
title: 'Certi Diritti ha partecipato al Gay Pride di Budapest'
date: Sat, 18 Jun 2011 09:13:27 +0000
draft: false
tags: [Europa]
---

**  
Il responsabile delle questioni europee dell'associazione Ottavio Marzocchi e l'eurodeputata olandese Sophie In't Veld hanno partecipato alla parata del 18 giugno.**

**(foto In't Veld e Marzocchi)**

**Bruxelles, 19 giugno 2011**

Certi Diritti ha partecipato al gay pride di Budapest che si è svolto il 18 giugno 2011: Ottavio Marzocchi, responsabile delle questioni europee di Certi Diritti, nonché collaboratore del gruppo Liberale e Democratico al Parlamento europeo, ha accompagnato l'Eurodeputata olandese Sophie In't Veld al Gay Pride, che é stato spesso problematico negli anni passati a causa di episodi di violenza.  
   
Sebbene l'Ungheria abbia una legge sulle unioni civili e abbia applicato le direttive europee anti-discriminazione, il governo populista di Viktor Orban ha adottato una nuova e contestata Costituzione che definendo il matrimonio come l'unione tra un uomo ed una donna vieta in sostanza i matrimoni gay, e che non elenca tra le cause di discriminazione proibite l'orientamento sessuale. Orban ha inoltre messo l'omofoba Annamaria Szalaj a capo dell'organismo di controllo dei media, con l'obiettivo di osteggiare ogni rappresentazione positiva dell'omosessualità nei media.  
   
Nel corso della visita in Ungheria, Sophie In't Veld e Ottavio Marzocchi hanno monitorato la situazione e l'attitudine delle autorità ed incontreranno le ONG per i diritti umani in loco, al fine di discutere della situazione dei diritti LGBT e dei diritti fondamentali in generale in Ungheria.