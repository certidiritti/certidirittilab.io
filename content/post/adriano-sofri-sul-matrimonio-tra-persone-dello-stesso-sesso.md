---
title: 'Adriano Sofri sul matrimonio tra persone dello stesso sesso'
date: Wed, 18 Jul 2012 13:24:33 +0000
draft: false
tags: [Matrimonio egualitario]
---

Da Piccola Posta del 18 luglio 2012 www.Ilfoglio.it

Caro Giuliano, ho letto alcune reazioni alle mie opinioni sul Pd e i diritti civili, esposte su Repubblica lunedì: quella di Rosy Bindi su Repubblica, di Francesco D’Agostino sull’Avvenire, e la tua qui. La discussione con Bindi riguarda essenzialmente la posizione del Pd, che dal tuo punto di vista è secondaria, dunque qui la ignorerò. Non pretendo di rispondere punto per punto; mi interessa chiarire un paio di questioni che mi stanno più a cuore. D’Agostino trova che io ricorra a un sofisma quando propongo che le persone siano libere di considerare il proprio legame di coppia come un matrimonio, e sostiene che “tutti gli argomenti portati a favore del matrimonio gay (in sintesi: la tutela dei diritti delle coppie omosessuali) sono fragilissimi…”. Vorrei intanto obiettare a questa “sintesi”. Io non ne so abbastanza, essendo stato costantemente eterosessuale e presto sposato e separato: quando mi sposai (civilmente, perché non ero e non sono credente) non lo feci “per tutelare i diritti della coppia”, ma perché ero innamorato e ricambiato e condividevo una cultura, per esempio quella dei miei genitori (cattolici credenti, loro) per la quale il matrimonio era la sanzione simbolica più impegnativa di una scelta d’amore. Come tante altre persone della mia generazione e di quelle successive, ho cambiato più tardi il mio modo di pensare e di sentire. Ma credo ancora che le persone, qualunque sia il loro genere e la loro vocazione sessuale, che desiderano sposarsi, lo facciano soprattutto per amore, o per la costellazione di sentimenti che gira attorno all’amore.  
[Continua a leggere sul sito de Il Foglio >](http://www.ilfoglio.it/piccolaposta/588)