---
title: 'FINALMENTE IL PAPA RICONOSCE GLI EFFETTI POSITIVI DEL PRESERVATIVO'
date: Sat, 20 Nov 2010 16:34:09 +0000
draft: false
tags: [Comunicati stampa]
---

**FINALMENTE IL PAPA RICONOSCE GLI EFFETTI POSITIVI DEL PRESERVATIVO. I TEMPI CAMBIANO, UNA VOLTA CI VOLEVANO SECOLI PER RICONOSCERE GLI ERRORI DELLA CHIESA, ORA, PER FORTUNA, BASTA UN ANNO E MEZZO.**

Roma, 20 novembre 2010

_**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**_

“Quando il 18 marzo 2009 il papa disse, sull’aereo che lo portava in Africa, che “il preservativo non serve a combattere l’Aids”, vi fu una levata di scudi e persino Stati come il Belgio, la Francia e la Germania, oltre che l’Unione Europea, non proprio Stati anticlericali ottocenteschi,  reagirono con fermezza contro questa incredibile tesi antiscientifica.  Non possiamo che rallegrarci del fatto che oggi dal papa venga riconosciuta l’importanza del più importante strumento contro l’Aids, anche se limitatamente a ‘singoli casi giustificati, ad esempio quando una prostituta utilizza un profilattico’. E’ sorprendente che ancora venga da lui sostenuto che ‘questo non è il modo vero e proprio per vincere l’infezione dell’Hiv’ mentre tutta la scienza sa benissimo che soprattutto  nei paesi africani, dove la malattia dell’Aids miete decine di migliaia di vittime, e dove milioni di cittadini sono colpiti dall’infezione dell’Hiv, questo è oggi l’unico strumento di prevenzione.

Il fatto che venga ammesso l’uso del preservativo, anche se “limitatamente a singoli casi”, ci fa ben sperare. Almeno non dobbiamo attendere cinque secoli per sentirci dire ‘scusate, chiediamo perdono per gli errori che abbiamo fatto”.