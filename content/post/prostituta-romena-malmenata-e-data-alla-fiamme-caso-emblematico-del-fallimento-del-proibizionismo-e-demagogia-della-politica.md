---
title: 'Prostituta romena malmenata e data alla fiamme caso emblematico del fallimento del proibizionismo e demagogia della politica'
date: Thu, 13 Sep 2012 14:23:09 +0000
draft: false
tags: [Lavoro sessuale]
---

La lotta alla tratta a Roma e in Italia non esiste quasi più. Firma l'appello per la legalizzazione della prostituzione.

Roma, 13 settembre 2012

Comunicato stampa congiunto del Comitato per i Diritti Civili delle Prostitute, Associazione Radicale Certi Dirtti e Associazione Radicali Roma

La vicenda della prostituta romena massacrata di botte e data alle fiamme in una strada di Roma è l’emblema di una drammatica situazione sociale che dimostra, se ancora ve ne fosse bisogno, di come le politiche proibizioniste sul tema della prostituzione siano miseramente fallite. La ragazza romena, ricoverata in gravissime condizioni all’Ospedale Sant’Eugenio di Roma, è uno dei tanti casi di persone vittime della tratta che sempre più si  alimenta grazie al proibizionismo e  alle ipocrite azioni di demagogia politica repressiva e del tutto inutili.

Gli stessi finanziamenti destinati alla lotta alla tratta della prostituzione sono stati tagliati e così l’ aiuto e la protezione a quelle persone vittime dello sfruttamento e della schiavitù sono sempre più difficili se non addirittura inesistenti.

Negli scorsi mesi abbiamo documentato come nella città di Roma le continue ordinanze emergenziali anti-prostituzione, l’ultima, per la quinta volta, è stata reiterata lo scorso febbraio, siano del tutto inefficaci. Perseguitare le prostitute con sanzioni non fa altro che alimentare la clandestinizzazione del fenomeno rendendo impossibile la fotografia di tutti quei casi di furti e violenze che non vengono nemmeno più denunciati e che sono di sicuro in aumento. Basti ricordare quanto denunciato dal Sindacato di Polizia (FP Cgil) lo scorso gennaio secondo cui a fronte di quasi 14.000 sanzioni fatte in due anni a prostitute, solo quattro di queste sono state pagate.

Il Sindaco di Roma nonostante l’ordinanza antiprostituzione sia in vigore chiede ora una legge nazionale proibizionista che di fatto estenderebbe in tutta Italia la drammatica situazione in cui si trova la Capitale. Il Sindaco continua a tenere nascosti i dati sui costi delle operazioni (personale, mezzi, ore di straordinario, propaganda, ecc) antiprostituzione facendo finta di niente e continuando a blaterare sul nulla.

Lo scorso aprile abbiamo promosso un manifesto-appello per la legalizzazione della prostituzione e per dare dignità alle sex workers che è già stato sottoscritto da migliaia di cittadini.

  
L’appello si può firmare al seguente link:    
[http://www.certidiritti.it/campagne-certi-diritti/itemlist/category/85-legalizzazione-prostituzione](campagne-certi-diritti/itemlist/category/85-legalizzazione-prostituzione)