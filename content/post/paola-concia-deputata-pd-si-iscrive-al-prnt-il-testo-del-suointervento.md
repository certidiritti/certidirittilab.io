---
title: 'PAOLA CONCIA, DEPUTATA PD, SI ISCRIVE AL PRNT, IL TESTO DEL SUO;INTERVENTO'
date: Sat, 01 Nov 2008 17:52:41 +0000
draft: false
tags: [Comunicati stampa]
---

31 ottobre 2008

VII Congresso Radicali Italiani

Intervento On. Anna Paola Concia

Care amiche e cari amici, innanzitutto grazie per l’invito. E’ la prima volta che partecipo a un vostro Congresso. Voi siete un pezzo importante della storia repubblicana del nostro paese. E un pezzo importante della mia vita.

Sono convinta non da oggi che per essere riformisti bisogna essere radicali. Nel senso che bisogna avere il coraggio di portare avanti le proprie scelte con convinzione e coraggio. Il riformismo comporta scelte nette. Non tentennamenti.

Vorrei che il nostro paese fosse diverso. Si, più civile. Vorrei che il mio paese fosse più inclusivo, vorrei che il mio paese riconoscesse a tutti i cittadini il diritto di cittadinanza, il diritto di sentirsi cittadini in casa propria. Senza esclusioni, senza paletti, senza razzismi ideologici, di maniera e di facciata.

In realtà, pur essendo una vecchia comunista (la mia vita politica è iniziata nel PCI tanti anni fa), sono sempre stata una donna liberale. So che a più di qualcuno farà sorridere, ma è così. Sono liberale a 360 gradi. Sui diritti individuali, sui temi economici, come, sono sempre stata una garantista. Come si dovrebbe essere. Ho sempre rimproverato a Berlusconi e compagnia che non si può essere liberali sui temi economici e LIBERTICIDI su tutto il resto.

E’ la solita schizofrenia italiana. Noi siamo europeisti per quello che ci pare, anche noi del PD. Non si può rivendicare l’Europa a ogni piè sospinto e poi sui temi, per esempio, dei diritti civili e dei diritti degli omosessuali essere reticenti e non stare al passo con l’Europa. No, non funziona così: o si è europeisti oppure no.

A volte ci possono dividere il metodo di lotta, frutto – appunto – dei diversi percorsi politici. Ma tante volte sugli obiettivi ci siamo ritrovati. Le cronache di questi giorni ci dicono che il PD stia decidendo di recuperare lo strumento referendario contro il decreto Gelmini. Lo dico sommessamente al mio partito: non ci dividiamo su questo. Certo, potrà essere uno strumento parziale, si potrà concentrare solo su alcuni aspetti come le Fondazioni universitarie e il maestro unico. BENE. Ma se ci lavoreremo bene, la raccolta di firme potrà essere un modo per entrare in contatto con questa ONDA DEMOCRATICA, SPONTANEA, AUTONOMA che è scesa in piazza ieri e che sta manifestando contro la Gelmini.

Nello stesso tempo credo che dobbiamo lavorare alla costruzione di proposte alternative, serie, concrete e credibili. NON AMO DIRE SOLO DEI NO. NON L’HO MAI AMATO. Bisogna saper dare una strada, un orizzonte in cui potersi riconoscere.

Sediamo dalla stessa parte del Parlamento non soltanto fisicamente, come pure in passato era successo, siamo molto più vicini; su molti, tanti aspetti della vita la pensiamo allo stesso modo: le vostre battaglie di libertà, di civiltà, di laicità e di educazione alla democrazia SONO le mie battaglie.

Sono figlia delle vostre battaglie civili e sono cresciuta con le certezze che quelle battaglie mi hanno regalato. Sento di avere un debito nei vostri confronti.

Abbiamo fatto insieme la battaglia contro la legge 40, quell'aborto legislativo che impedisce a tanti il sereno esercizio del diritto alla genitorialità.

Quel referendum è stato una brutale sconfitta, ha rappresentato il trionfo della conservazione sui valori della democrazia partecipata ma allo stesso tempo ha fatto sì che suonasse l'allarme sullo stato dei diritti civili nel nostro paese.

Ecco l’allarme…Troppe volte i vostri allarmi sono stati inascoltati o ascoltati con un senso di sufficienza anche da parte del mio partito: voi, moderne Cassandre dell'italica laicità.

Per questo oggi sono qui, perché seppure abbiamo storie diverse e possiamo essere per certi aspetti diversi, io non smetterò mai di credere che bisogna avere la forza e il coraggio di farsi contaminare.

Del resto, credo che le differenze e le diversità siano una ricchezza... è sufficiente non farsi vincere dal pregiudizio. Quello stesso pregiudizio che cerco di combattere ogni giorno.

Come qualcuno di voi saprà, io sono omosessuale, non faccio l'omosessuale di mestiere. Lo sono. Un giorno, erano i mesi successivi al World Pride di Roma del 2000, decisi di fare il mio coming out e di fare della battaglia sulle libertà individuali e sui diritti civili la battaglia della mia vita. Fu una scelta individuale.

Ma attenzione, come Rita Bernardini sa perché ne abbiamo parlato tante volte, anche animatamente, sono tra quelle e quelli che pensano che le battaglie sui diritti degli omosessuali, non le debbano fare solo gli omosessuali. ANZI: e voi siete la dimostrazione di questo, da sempre. È una idea di società che è in gioco, non solo i diritti di alcuni. Dentro al nostra battaglia c’è un’idea di comunità che riguarda tutte e tutti, omosessuali e eterosessuali. C’è dietro una domanda fondamentale: “in quale società vogliamo vivere?”

Questa battaglia la conduco dentro questo contesto. Non è isolata da resto. Non per me.

E’ una battaglia, però, che con i miei 45 anni non voglio più solo fare. Ma voglio vincere. Non è una battaglia di testimonianza. Non mi basta. In questo si, c’è il mio animo riformista.

Vorrei che chi verrà dopo di me, gay, lesbica, transessuale non debba soffrire più quello che ho sofferto io da giovane. Questo si, questo lo desidero.

E so che voi siete interlocutori affidabili. Lo avete sempre dimostrato.

Con i sei amici e amiche radicali in questi primi mesi di legislatura abbiamo firmato insieme tante proposte di legge che riguardano coppie di fatto, partnership, matrimonio, omofobia, omogenitorialità.

Abbiamo presentato due pdl contro l'omofobia, una delle quali, molto incisiva e completa; un'altra più semplice che – colpo di scena – procede in Commissione Giustizia, di cui sono relatrice.

E procede perché, pur essendo meno completa delle altre, non prevedendo tutta una serie di azioni positive che lo stato ha il dovere di compiere, va dritta al nodo del problema, seguendo gli imperativi dettati dal parlamento europeo: la condanna degli atti persecutori e della incitazione a commetterli contro le minoranze omo e transessuali.

Vi chiedo di aiutarmi su questo. Lo chiedo ai miei colleghi radicali, lo chiedo a voi.

Sono a favore del testamento biologico, ho firmato la proposta di legge di Ignazio Marino che hanno ripresentato Barbara Pollastrini e Gianni Cuperlo alla Camera.

Sono una sostenitrice convinta della Associazione “Certi Diritti”, di cui Sergio Rovasio è il cuore. Ho firmato la loro l’iniziativa “di affermazione civile”. Sarà una iniziativa importante e spero che abbia successo. Sono qui a dare una mano.

Faccio parte della Associaizone “Luca Coscioni” e mi iscriverò a “Nessuno tocchi caino”.

Oggi ho preso la tessera del Partito Radicale Transnazionale

IO CI SONO: CON LA MIA STORIA E IL MIO MODO DI FARE POLITICA, CONDIVIDENDO CON VOI UN’ALTRA COSA:

I vostri successi storici dimostrano che su certi temi non si può andare avanti da soli ma è necessario comporre uno schieramento trasversale, che vada oltre i singoli orticelli per affermare i sacro principio della laicità dello stato.

Questa è una vera emergenza nazionale, la laicità dello stato e delle istituzioni repubblicane.

Da anni le istituzioni ecclesiastiche entrano a gamba tesa nelle questioni italiane. Lo hanno sempre fatto. Ma oggi, trovano sempre meno resistenze. Io credo che la responsabilità di questo sia da attribuire PRINCIPALANETE ALLA POLITICA. LA POLITICA ITALIANA TUTTA, COMPRESO IL PD.

La politica e le istituzioni sono fragili rispetto alla difesa della laicità. Fragili rispetto alla difesa di se stessi.

L’intervista della Binetti di oggi sul Corriere della Sera è scandalosa. Ho fatto un comunicato dicendo che non solo deve chiedere scusa a me, agli omosessuali italiani e ai cattolici, ma ho detto che è lei ad essere fuori dal PD. E’ lei che è fuori dallo statuto e dalla linea del PD. Ne tragga la sue conseguenze.

Questo è un grande problema e dobbiamo essere insieme ad affrontarlo per rimettere tutti al proprio posto: LA POLITICA E LA CHIESA.

Questa non può essere una battaglia di testimonianza. Deve essere una battaglia di civiltà: una battaglia per il presente e per il futuro di questo paese.

Dobbiamo insieme ricostruire una coscienza civile, una consapevolezza dell’importanza fondamentale della LAICITÀ DELLO STATO per la sopravvivenza della stessa democrazia.

Lo dobbiamo fare dentro la politica e dentro le istituzioni, e dobbiamo costruire alleanze trasversali.

Chiudo con una battuta:

So cosa sta pensando Rita Bernardini e forse qualcun altro “ma che ci fai dentro il PD, vieni con noi!”

Sono nel Pd per moltissime ragioni e ci sto in modo convinto. Una sola ve la voglio dire: Ho sempre pensato che le cose vadano combattute dal di dentro. Lì dove devono essere cambiate. Lì dove esistono le maggiori contraddizioni. E’ la storia della mia vita. Penso che se il PD non assume su di se le battaglie di civiltà, quelle battaglie non si vincono.

Nello stesso modo penso che senza di voi non riesco ad immaginare questo paese, questa democrazia. Siete fondamentali.

E allora, io e altre e altri nel PD, Voi qui, facciamo una grande alleanza per tirare fuori dalle secche questo scellerato e meraviglioso paese.

Grazie buon lavoro!