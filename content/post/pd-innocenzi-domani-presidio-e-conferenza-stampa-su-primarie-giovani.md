---
title: 'PD: INNOCENZI, DOMANI PRESIDIO E CONFERENZA STAMPA SU PRIMARIE GIOVANI'
date: Mon, 22 Sep 2008 15:23:57 +0000
draft: false
tags: [Comunicati stampa]
---

**Domani 23 settembre 2008  alle ore 10 a Roma, presso la sede del Partito Democratico in via Sant’Andrea delle Fratte 16, si terrà un presidio nonviolento per il ripristino della legalità delle elezioni primarie che sceglieranno il Segretario dei Giovani del Partito Democratico, e per permettere la candidatura della radicale Giulia Innocenzi. Immediatamente dopo alle ore 11 la stampa è invitata alla conferenza stampa che si terrà nello stesso luogo.**

**Dichiarazione di Giulia Innocenzi, radicale e coordinatrice degli Studenti Luca Coscioni, candidata alla segreteria nazionale dell’organizzazione giovanile del Partito Democratico**

Nonostante le due lettere aperte a Walter Veltroni pubblicate negli scorsi giorni su Europa e poi riprese dalle agenzie nella giornata di ieri, in cui si chiedevano regole certe per partecipare alle primarie del PD Giovani, non abbiamo a oggi ricevuto alcuna risposta. Inoltre, sono venuta a conoscenza ieri di un regolamento pubblicato su un sito, non rintracciabile sul sito ufficiale del PD, che prescriverebbe la raccolta di 600 firme entro il 25 settembre con un margine di soli 3 giorni di tempo per la candidatura a Segretario. Il regolamento peraltro richiederebbe di utilizzare “moduli nazionali scaricabili dal sito del PD”, dei quali moduli non vi è alcuna traccia. Per questo domani inizierò dalle ore 10 un presidio nonviolento davanti la sede del PD.