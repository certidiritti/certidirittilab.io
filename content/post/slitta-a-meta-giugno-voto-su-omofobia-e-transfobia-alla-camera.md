---
title: 'Slitta a metà giugno voto su omofobia e transfobia alla Camera'
date: Thu, 02 Jun 2011 11:17:02 +0000
draft: false
tags: [Politica]
---

Voto previsto tra il 14 e il 16 giugno. Confermato per quei giorni il sit-in/maratona oratoria delle principali associazioni lgbt(e) italiane.

Roma, 2 giugno 2011

Comunicato Stampa dell'Associazione Radicale Certi Diritti

La Conferenza dei Capigruppo della Camera dei deputati ha deciso il calendario delle attività del mese di giugno 2011. In base al calendario il voto sulla Proposta di Legge omofobia e transfobia si svolgerà nei giorni compresi tra martedì 14 e giovedì 16 giugno 2011. Prima del voto verranno messe in votazione le proposte di eccezione di costituzionalità e della questione di sospensiva presentate dall'Udc e dalla Lega. La discussione generale si è già svolta 10 giorni fa.

L'Associazione Radicale Certi Diritti conferma lo svolgimento, per quei giorni, del Sit-in/Maratona oratoria alla quale hanno aderito le principali Associazioni lgbt italiane che interverranno in Piazza Montecitorio nelle stesse ore in cui si voterà sul provvedimento.

Qui di seguito uno stralcio delle decisioni prese dalla Conferenza dei Capigruppo:

  
Martedì 14, mercoledì 15 e giovedì 16 giugno

(a.m. e p.m., con eventuale prosecuzionenotturna e nella giornata di venerdì 17 giugno) (con Votazioni):

Seguito dell'esame decreto-logge Semestre Europeo Disposizioni urgenti per l'economia(da inviare al Senato scadenza: 12 luglio 2011) (A.C. 4357).

Esame Doc. IV, n. II (Domanda di autorizzazione all'utilizzo di intercettazioni telefoniche-deputato Landolfi).

Seguito dell'esame:

• mozione Tremaglia ed altri concernente la convocazione di una Conferenza internazionale per predisporre un piano di investimenti in Africa;

• pdl in materia di omofobia e transfobia (A.C. 2802)

(previo esame e Votazione delle questioni pregiudiziali e della questione sospensiva presentate);