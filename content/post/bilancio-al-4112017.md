---
title: 'Bilancio al 4/11/2017'
date: Mon, 20 Nov 2017 11:45:26 +0000
draft: false
---

STATO PATRIMONIALE

2017

2016

2017

2016

ATTIVO

PASSIVO

Cassa, banche e c/c postale

               30.049

               19.557

Erario c/ritenute

                   37

Crediti vari

                 2.800

                 2.800

Avanzo cumulato

22.320

16.585

TOTALE ATTIVO

               32.849

               22.357

TOTALE PASSIVO

             22.320

             16.622

Avanzo

10.529

5.735

Totale a pareggio

32.849

22.357

Totale a pareggio

32.849

22.357

 CONTO ECONOMICO

ONERI

PROVENTI

Servizi comuni

                   742

Autofinanziamento

              8.280

              4.908

Contributi erogati

                 2.850

Altri contributi

     4.395

              8.940

Iniziative varie

               14.322

                 4.243

Contributi da altre Associazioni

     9.133

                   -

Campagne di informazione

                   279

                      -

Sopravvenienze attive

              8.500

              2.963

Convegno Israele

                 4.557

                      -

Materiale autofinanziamento

                   -

 -

IX Congresso

                 1.401

Proventi finanziari

                   -

 -

X Congresso

                   311

                 1.000

Oneri finanziari

                   310

                   616

Sopravvenienze passive

                      -

                   224

TOTALE SPESE

               19.779

               11.076

TOTALE PROVENTI

             30.308

             16.811

avanzo

               10.529

                 5.735

Totale a pareggio

               30.308

               16.811

Totale a pareggio

             30.308

             16.811