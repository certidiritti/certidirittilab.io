---
title: 'SABATO 7-6 AL GAY PRIDE DI ROMA, ALLE 20.30 CI IMBAVAGLIAMO A SAN GIOVANNI'
date: Fri, 06 Jun 2008 15:33:46 +0000
draft: false
tags: [Comunicati stampa]
---

SABATO 7 GIUGNO, ALLE ORE 20.30, DOPO AVER PARTECIPATO AL GAY PRIDE, MILITANTI E DIRIGENTI RADICALI, ESPONENTI DEL MOVIMENTO LGBT E DI RIFONDAZIONE COMUNISTA SI IMBAVAGLIERANNO DAVANTI ALL'INGRESSO DELLA BASILICA DI SAN GIOVANNI A ROMA QUANDO IL CORO "TRADITIO ET CONFESSIO".  
_**Dichiarazione di Rita Bernardini, deputata radicale del Pd, Segretaria di Radicali Italiani, e Sergio Rovasio, Segretario Associazione Radicale Certi Diritti**_

 

Il diniego della Prefettura e Questura di Roma agli organizzatori del Gay pride di utilizzare Piazza San Giovanni, come punto d'arrivo della manifestazione, è del tutto ingiustificato. La motivazione data sarebbe quella che è "inopportuno" che il Gay pride si concluda nello stesso luogo dove ha sede una Basilica all'interno della quale, alle ore 20.30 - dopo un'ora dalla conclusione del pride! - si svolgerà un convegno e canterà il coro "Traditio et Confessio". Insomma, le motivazioni del diniego non  riguardano l'ordine pubblico o aspetti giuridici ma soltanto questioni di 'inopportunità', perchè canterà un coro.

Nell'esprimere tutta la nostra solidarietà e vicinanza alle Associazioni organizzatrici del Gay Pride di Roma, giudichiamo questo diktat imposto dalle autorità un pericoloso precedente che nega il diritto costituzionale a manifestare liberamente il proprio pensiero.  
  

Per queste ragioni, sabato 7 giugno, alle ore 20.30, insieme a parlamentari, dirigenti e militanti radicali, con esponenti del movimento lgbt, la Fondazione Massimo Consoli e, tra gli altri, Elettra Deiana dirigente di Rifondazione Comunista e Saverio Aversa, responsabile nazionale Diritti e Culture delle differenze per Rifondazione Comunista, ci imbavaglieremo davanti all'ingresso della Basilica di San Giovanni, nella stessa ora in cui il Coro "Traditio et Confessio" inizierà i suoi canti