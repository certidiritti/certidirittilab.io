---
title: 'INFORMATIVA SUL TRATTAMENTO DEI DATI PERSONALI'
date: Sun, 22 Apr 2012 18:39:15 +0000
draft: false
tags: [Politica]
---

Ai sensi dell’art. 13 del D.lgs. 30 giugno 2003, n. 196 (rubricato “Codice in materia di protezione dei dati personali”), La informiamo di quanto segue:

**Finalità del trattamento cui sono destinati i dati**

1.         I Suoi dati personali saranno trattati - per un tempo non superiore a quello necessario agli scopi come di seguito dichiarati - con modalità lecite e secondo correttezza, esclusivamente per le finalità connesse:

a.       all'attivazione dei servizi offerti dal sito "[www.certidiritti.it](http://www.certidiritti.it/)", comparendo altresì come sottoscrittori degli stessi;  

b.       alla procedura di registrazione degli aderenti a iniziative dell’Associazione Radicale “Certi Diritti”;

c.      all’adempimento degli obblighi legali e fiscali;  

d.       alla tenuta della contabilità, fatturazione e gestione del credito.

**Il conferimento dei dati necessari a tali finalità è obbligatorio. L’eventuale rifiuto di fornirli comporta la mancata prosecuzione del rapporto.**

**Ulteriori finalità soggette a consenso**

1.  I Suoi dati personali potranno essere trattati per finalità diverse da quelle indicate, esclusivamente previo il consenso da Lei espresso. In particolare, tali dati potranno essere comunicati ai seguenti soggetti con la finalità di invio di comunicazioni su iniziative, materiale informativo e promozionale: Partito Radicale Nonviolento Transnazionale e Transpartito, Movimento Politico Radicali Italiani, Associazione Luca Coscioni, Associazione Nessuno Tocchi Caino, Associazione Non c’è Pace senza Giustizia, E.R.A. Onlus, Anticlericale.net, Lista Pannella, Lega Internazionale Antiproibizionista, tutte domiciliate in Roma, in Via di Torre Argentina n. 76.    I soggetti indicati tratteranno i dati come autonomi titolari o come responsabili quando nominati.

**Il conferimento dei dati per tale finalità non è obbligatorio e in caso contrario non pregiudica la prosecuzione del rapporto(*).**

**Modalità del Trattamento**

1.  Il trattamento dei dati personali avverrà mediante strumenti idonei a garantirne la sicurezza e la riservatezza e sarà effettuato anche attraverso strumenti automatizzati atti a memorizzare, gestire e trasmettere tali dati; esso potrà consistere in qualunque operazione o complesso di operazioni tra quelle previste dall’art.4, primo comma, lett. A), del D.Lgs. 30 giugno 2003, n.196 (Codice in materia di protezione dei dati personali). Il trattamento sarà eseguito esclusivamente dal personale incaricato dal Titolare o dal Responsabile se nominato.

**Categorie di Soggetti ai quali i dati possono essere comunicati**

1.  4. I dati personali potranno essere comunicati, esclusivamente nell’ambito delle finalità di cui al punto 1, ai seguenti soggetti: istituti di credito, soggetti che effettuano servizi di spedizione, professionisti e consulenti. In questi casi **non è richiesto il consenso dell’interessato**.

**Diffusione dei Dati**

1.  I dati personali raccolti per le finalità di cui sopra, non saranno diffusi.

**Titolare del trattamento dei dati personali**

1.  Il titolare del trattamento dei dati personali è l’Associazione Radicale “Certi Diritti”, in persona del legale rappresentante _pro-tempore_ (rectius: Segretario eletto) con sede in Roma, in Via Di Torre Argentina n. 76.

**Diritti dell'interessato**

1.  7. Le ricordiamo che, come interessato al trattamento dei dati personali, potrà esercitare i diritti specificati dal D. Lgs. 30 giugno 2003, n.196 (Codice in materia di protezione dei dati personali) nei confronti del Titolare del trattamento, con particolare riguardo al contenuto dispositivo di cui all’Art. 7 (Diritto di accesso ai dati personali ed altri diritti) che, per Sua comodità, si riproduce integralmente in calce alla presente pagina.

La informiamo, inoltre, che l’esercizio di tali diritti potrà avvenire con richiesta rivolta senza formalità al Titolare del trattamento, anche per il tramite di un incaricato, e che a tale richiesta sarà fornito idoneo riscontro senza ritardo.

  

**Decreto Legislativo 30 giugno 2003, n. 196**

**_(Codice in materia di protezione dei dati personali)_**

**Art. 7.**

**_Diritto di accesso ai dati personali e altri diritti:_**

_1\. L'interessato ha diritto di ottenere la conferma dell'esistenza o meno di dati personali che lo riguardano, anche se non ancora registrati, e la loro comunicazione in forma intelligibile._

_2\. L'interessato ha diritto di ottenere l'indicazione:_

_a) dell'origine dei dati personali;_

_b) delle finalità e modalità del trattamento;_

_c) della logica applicata in caso di trattamento effettuato con l'ausilio di strumenti elettronici;_

_d) degli estremi identificativi del titolare, dei responsabili e del rappresentante designato ai sensi dell'articolo 5, comma 2;_

_e) dei soggetti o delle categorie di soggetti ai quali i dati personali possono essere comunicati o che possono venirne a conoscenza in qualità di rappresentante designato nel territorio dello Stato, di responsabili o incaricati._

_3\. L'interessato ha diritto di ottenere:_

_a) l'aggiornamento, la rettificazione ovvero, quando vi ha interesse, l'integrazione dei dati;_

_b) la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge, compresi quelli di cui non è necessaria la conservazione in relazione agli scopi per i quali i dati sono stati raccolti o successivamente trattati;_

_c) l'attestazione che le operazioni di cui alle lettere a) e b) sono state portate a conoscenza, anche per quanto riguarda il loro contenuto, di coloro ai quali i dati sono stati comunicati o diffusi, eccettuato il caso in cui tale adempimento si rivela impossibile o comporta un impiego di mezzi manifestamente sproporzionato rispetto al diritto tutelato._

_4\. L'interessato ha diritto di opporsi, in tutto o in parte:_

_a) per motivi legittimi al trattamento dei dati personali che lo riguardano, ancorché pertinenti allo scopo della raccolta;_

_b) al trattamento di dati personali che lo riguardano a fini di invio di materiale pubblicitario o di vendita diretta o per il compimento di ricerche di mercato o di comunicazione commerciale._

**Consenso dell’interessato al trattamento dei propri dati personali**

Il sottoscritto interessato, inviando i dati con la casella barrata denominata “Consenso al trattamento dei dati personali” dichiara e conferma di essere stato preventivamente informato dal Titolare circa:

1.  le finalità e le modalità del trattamento cui sono destinati i dati;
2.  la natura obbligatoria o facoltativa del conferimento dei dati;
3.  le conseguenze di un eventuale rifiuto di rispondere;
    1.  i soggetti o le categorie di soggetti ai quali i dati personali possono essere comunicati o di cui possono venire a conoscenza in qualità di responsabili ovvero incaricati, e l’ambito di diffusione dei medesimi;
    2.  i diritti di cui all’art. 7 del D. Lgs. 30 giugno 2003, n.196 (Codice in materia di protezione dei dati personali);
4.  gli estremi identificativi del titolare ovvero del responsabile de trattamento.

**(*) Consenso facoltativo per le finalità di cui al punto 2)**

  

Il sottoscritto interessato, avendo preso visione dell’informativa, inviando i dati con la casella barrata denominata “Consenso facoltativo al trattamento dei dati personali”:

**AUTORIZZA**

il trattamento dei propri dati personali per le finalità di cui al punto 2) della suddetta informativa, volte all’invio di comunicazioni su iniziative, materiale informativo e promozionale da parte dei seguenti soggetti: Partito Radicale Nonviolento Transnazionale e Transpartito, Movimento Politico Radicali Italiani, Associazione Luca Coscioni, Associazione Nessuno Tocchi Caino, Associazione Non c’è Pace senza Giustizia, E.R.A. Onlus, Anticlericale.net, Lista Pannella, Lega Internazionale Antiproibizionista, tutte domiciliate in Roma, in Via di Torre Argentina n. 76.

Il trattamento di tali avverrà nel rispetto del Decreto Legislativo 30 giugno 2003, n. 196, recante “Codice in materia di protezione dei dati personali”.

L’utente potrà verificare e revocare tale consenso in qualsiasi momento secondo le modalità indicate al punto 7) della suddetta informativa.