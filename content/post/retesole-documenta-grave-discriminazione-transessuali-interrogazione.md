---
title: 'RETESOLE DOCUMENTA GRAVE DISCRIMINAZIONE TRANSESSUALI: INTERROGAZIONE'
date: Tue, 03 Mar 2009 14:36:50 +0000
draft: false
tags: [Comunicati stampa, DEPUTATI RADICALI, DISCRIMINAZIONI, interrogazione parlamentare, transessuali]
---

DISCRIMINAZIONI TRANSESSUALI: IL DOCUMENTO VIDEO DEL GIORNALISTA FRANCESCO PALESE PER L'EMITTENTE TV RETESOLE FA LUCE SU UNA GRAVE REALTA' DI PREGIUDIZIO. INTERROGAZIONE PARLAMENTARE DEI DEPUTATI RADICALI DEL PD.  
   
Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:  
  "Quanto documentato con serietà e rigore dal giornalista Francesco Palese per l'emittente Tv Retesole fa luce su una realtà nascosta e ignorata della vita di migliaia di persone transessuali che merita invece attenzione e sostegno politico. L'evidente discriminazione e il pregiudio diffuso ben documentato dal filmato del giornalista dovrebbero far riflettere coloro che alimentano l'odio e il pregiudizio verso le persone transesuali che meritano aiuto nell'inserimento del lavoro, sostegno per la formazione professionale e, spesso, maggiore protezione".  
I deputati radicali del Pd hanno depositato sulla vicenda  la seguente interrogazione parlamentare urgente:   
   
   
   
Al Ministro per le Pari Opportunità  
Al Ministro per il Lavoro, Salute e Politiche Sociali  
Interrogazione a risposta scritta  
   
Il giornalista Francesco Palese, ha curato per la trasmissione ‘L’Altra Inchiesta’ dell’emittente Tv Retesole, un’inchiesta molto ben documentata sulle difficoltà che hanno le persone transessuali a trovare lavoro;  
   
Il giornalista si è finto una persona transessuale e si è recato con telecamera nascosta in diverse agenzie di collocamento per lavoro interinale; è stato così documentato il rifiuto della disponibilità di lavoro da parte di diverse agenzie di Roma, anche se pochi minuti prima, le stesse, avevano offerto alla sua complice, Francesca Bastone, diverse proposte.  
   
Nel documento video è stato anche documentato il diniego di un’agenzia immobiliare ad affittare immobili alla finta persona transessuale che invece poco prima erano stati offerti alla complice dell’inchiesta;  
   
Il documento video inizia con  con la finta persona transessuale che, in via Salaria a Roma, viene fermata dalla Polizia Municipale che la invita a cambiare posto e a specificare l'attività svolta.  
   
Le persone transessuali,proprio a causa delle discriminazioni nel mondo del lavoro, si trovano spesso nella condizione di avere come unica prospettiva di sopravvivenza quella della prostituzione;  
   
Per sapere:  
 \- quali iniziative sono in corso per dare combattere le discriminazioni verso le persone transessuali;  
\- se il Governo è al corrente, così come ben documentato dal giornalista Francesco Palese, che delle oltre ventimila persone transessuali in Italia, l’85% non riesce a inserirsi nel mondo del lavoro;  
\- se non ritenga il Governo  urgente concordare con le Regioni, le Province e i Comuni l'attivazione o conversione di specifici corsi di formazione e sostegno per le persone transessuali per consentire un più faciel inserimento nel mondo del lavoro;  
\- cosa intenda fare il Governo per adeguarsi alle Direttive antidiscriminazione dell’Unione Europea volte a tutelare anche le persone transessuali in ambito lavorativo;  
   
Rita Bernardini  
Marco Beltrandi  
Maria Antonietta Farina Coscioni  
Matteo Mecacci  
Marzio Turco  
Elisabetta Zampartutti