---
title: 'MATRIMONIO GAY, IL 23 MARZO LA PRONUNCIA DELLA CORTE COSTITUZIONALE'
date: Wed, 17 Mar 2010 06:58:54 +0000
draft: false
tags: [Senza categoria]
---

**Sabato 20 marzo iniziativa a Catania per la piena uguaglianza dei diritti.**

Il 23 marzo la Corte Costituzionale si esprimerà sulla legittimità del divieto di matrimonio per le coppie dello stesso sesso.

L’imminente decisione della Corte è il frutto della campagna di “Affermazione Civile”, lanciata due anni fa dall’Associazione Radicale Certi Diritti e dalla Rete Lenford , un gruppo di avvocati che si occupano di diritti lgbt. Diverse coppie gay hanno impugnato in giudizio il rifiuto delle pubblicazioni matrimoniali opposto dai Comuni, e alcuni tribunali, ritenendo la questione fondata, hanno trasmesso gli atti alla Corte Costituzionale, che tra pochi giorni si pronuncerà.

Sabato 20 marzo a Catania, in via Etnea (di fronte alla Villa Bellini) dalle 10.00 alle 13.00, in concomitanza con analoghe iniziative che si svolgeranno a Roma e in altre città italiane, manifesteremo per il riconoscimento del diritto di contrarre matrimonio per le coppie dello stesso sesso. Su questo fronte il nostro paese sconta un ritardo gravissimo: i giudici della Consulta hanno una straordinaria occasione per colmare il divario con le democrazie avanzate e per affermare che i cittadini sono uguali davanti alla legge, senza discriminazioni basate sull’orientamento sessuale.

[Scarica il volantino](http://www.certidiritti.org/wp-content/uploads/2010/03/Volantino_20_marzo_2010_CERTI_DIRITTI_definitivo.pdf)

**Certi Diritti Catania**

**Radicali Catania**

[www.certidiritti.it](http://www.certidiritti.it/)

[www.radicalicatania.org](http://www.radicalicatania.org/)