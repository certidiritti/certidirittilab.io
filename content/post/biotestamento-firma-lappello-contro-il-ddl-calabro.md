---
title: 'Biotestamento, firma l''appello contro il ddl Calabrò'
date: Sun, 27 Feb 2011 15:44:25 +0000
draft: false
tags: [appello, autodeterminazione, cnl, Comunicati stampa, Coscioni, marino, RADICALI, rete laica, rodotà, Testamento Biologico, welby]
---

No alla cancellazione del diritto all'autodeterminazione.

Il 7 marzo approderà alla Camera dei Deputati il ddl Calabrò sul testamento biologico.

Gilda Ferrando, Alessandro Pace, Pietro Rescigno e Stefano Rodota' hanno lanciato un appello perchè "se questo testo fosse approvato nella forma attuale, le persone vedrebbero gravemente limitati i propri diritti, sarebbero espropriate della possibilitÃ  di governare liberamente la propria vita. Il diritto all'autodeterminazione, definito fondamentale dalla Corte costituzionale con la sentenza n. 438 del 2008, sarebbe cancellato".

Più di 37mila persone hanno già letto e firmato l'appello. **[FIRMA ANCHE TU >](http://autodeterminazione.nobavaglio.it/)**