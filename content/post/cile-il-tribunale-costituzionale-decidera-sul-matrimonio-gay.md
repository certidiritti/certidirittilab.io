---
title: 'Cile: il Tribunale Costituzionale deciderà sul matrimonio gay'
date: Sat, 01 Jan 2011 09:58:05 +0000
draft: false
tags: [Comunicati stampa]
---

Pubblicato 30 dicembre 2010 da aelred  
Anche in Cile, come è già successo in numerosi altri paesi con esiti in genere fausti, sarà il Tribunale Costituzionale a esprimersi sul [divieto di matrimonio](http://www.latercera.com/noticia/nacional/2010/12/680-334096-9-justicia-declara-admisible-recurso-a-favor-del-matrimonio-homosexual.shtml) per le coppie formate da persone dello stesso sesso.

La Corte d’appello di Santiago, infatti, ha rinviato alla massima autorità giuridica del Cile la causa intentata da tre coppie gay, che chiedono di sposarsi o di vedere riconosciuto il matrimonio già celebrato in un altro paese. César Peralta e Hans Arias; Jorge Monardes e suo marito Stephane Abran, sposato in Canada; Víctor Arce e Miguel Lillo hanno portato avanti questa iniziativa che vale già una vittoria.

I giudici supremi, infatti, hanno la possibilità - almeno in teoria - di dichiarare che il divieto di matrimonio per gay e lesbiche è contrario alla Costituzione, il che porterebbe alla legalizzazione di questo tipo di unioni. La decisione, che è stata resa nota solo adesso, è stata presa il 14 dicembre dalla Corte d’appello composta dal presidente Alejandro Solís, con Joaquín Billard e Ángel Cruchaga. Adesso non resta che aspettare la sentenza e sperare che i giudici cileni siano un po’ più coraggiosi di quelli italiani.

Anche in Italia, infatti, il matrimonio gay è finito davanti alla Corte Costituzionale, che però ha rimandato qualunque cambiamento alla scelta del legislatore, cioè del Parlamento