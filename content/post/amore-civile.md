---
title: 'Amore Civile'
date: Fri, 23 Apr 2010 19:02:38 +0000
draft: false
tags: [Amore Civile, Francesco Bilotta, Senza categoria]
---

**Amore Civile, dal Diritto della tradizione  
al Diritto della ragione**

a cura di Bruno de Filippis e Francesco Bilotta  
[MIMESIS](http://it-it.facebook.com/note_redirect.php?note_id=201141901336&h=bc3ac5de0d1abb0ba4058f086fb37b8a&url=http%3A%2F%2Fwww.mimesisedizioni.it%2Farchives%2F001130.html "http://www.mimesisedizioni.it/archives/001130.html") collana Sx  
Quaderni Loris Fortuna.

Questo volume raccoglie il lavoro svolto in quasi due anni da studiosi, esperti e rappresentanti di associazioni, impegnati in un tavolo di lavoro permanente, ove le diverse competenze (diritto civile, penale, comparato ed internazionale, di medicina e genetica, di filosofia, psicologia e sociolgia) potessero aver modo di lavorare insieme per redigere un Progetto di Riforma globale del Diritto di Famiglia. Tra gli obiettivi vi è quello di proporre alla classe politica una riforma complessiva raccolta nel libro Amore Civile. Idea ispiratrice del lavoro è il concetto di laicità dello Stato.

Interventi, tra gli altri, di Emma Bonino, Bruno De Filippis, Francesco Bilotta, Marco Pannella, Marco Cappato, Sergio Rovasio, Diego Galli, Guido Allegrezza![](chrome://skype_ff_toolbar_win/content/icons/icon_offline.png), Diego Sabatinelli...

Dal 7 novembre 2009 Amore Civile è disponibile in tutte le LIBRERIE, e ON-LINE nei principali siti di vendita di libri, anche in offerta on line direttamente alla [casa editrice](http://it-it.facebook.com/note_redirect.php?note_id=201141901336&h=bc3ac5de0d1abb0ba4058f086fb37b8a&url=http%3A%2F%2Fwww.mimesisedizioni.it%2Farchives%2F001130.html "http://www.mimesisedizioni.it/archives/001130.html").