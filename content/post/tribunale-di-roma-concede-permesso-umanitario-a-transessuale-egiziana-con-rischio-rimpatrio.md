---
title: 'Tribunale di Roma concede permesso umanitario a transessuale egiziana con rischio rimpatrio'
date: Thu, 24 Nov 2011 21:51:19 +0000
draft: false
tags: [Transessualità]
---

Il permesso le era stato precedentemente negato dal questore di Milano, nonostante parere favorevole della commissione territoriale. Rispettata la giurisprudenza della Corte Europea dei Diritti dell'Uomo.

Roma, 24 novembre 2011

Comunicato Stampa dell’Associazione Radicale Certi Diritti

  
Il Tribunale di Roma, con ordinanza emessa in data 18 novembre 2011, ha accolto il ricorso di una cittadina egiziana transessuale, difesa dall’Avvocato Livio Neri, contro la decisione del Questore di Milano che gli negava, nonostante il parere positivo della competente Commissione territoriale, il permesso di soggiorno per motivi umanitari.

La decisione appare rilevante per la chiarezza con cui si limita la discrezionalità del Questore, in sede di rilascio dei permessi per motivi particolari, laddove le esigenze di carattere umanitario siano già state vagliate dall'autorità amministrativa a ciò preposta, in questo caso la Commissione territoriale per il riconoscimento della protezione internazionale, che si era espressa in modo chiaro.

Il Tribunale di Roma richiama, inoltre, l'Amministrazione dell'Interno al principio dell'assoluta inderogabilità dell'art.3 della Corte Europea dei Diritti dell’Uomo che, come noto, secondo la giurisprudenza, vieta, senza eccezione alcuna, l'allontanamento dello straniero verso paesi in cui questo possa essere sottoposto a tortura o a trattamenti inumani o degradanti . Inoltre, il Tribunale ha ritenuto che la cittadina egiziana, in caso di rimpatrio in Egitto, avrebbe corso il rischio di subire tali trattamenti per le gravi persecuzioni e discriminazioni di cui sono vittime in quello Stato le persone omosessuali e transessuali .

In base a questo principio, nonostante le gravi condanne penali subite in Italia, il Tribunale ha deciso che la cittadina straniera avesse diritto al rilascio di un permesso di soggiorno per motivi umanitari.