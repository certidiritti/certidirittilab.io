---
title: 'Testimonianze e ricordi su David Kato'
date: Fri, 04 Feb 2011 08:09:57 +0000
draft: false
tags: [Africa]
---

L'eredità che ci ha lasciato David Kato, attivista ed eroe delle battaglie glbt che è stato assassinato nella sua casa in Uganda.

### [Domani a Roma cerimonia davanti ambasciata ugandese per David Kato](tutte-le-notizie/1008-domani-a-roma-cerimonia-davanti-ambasciata-ugandese-per-david-kato.html)

### [INTERVENTO DI KATO DAVID KISULE AL IV CONGRESSO DI CERTI DIRITTI](tutte-le-notizie/1010-intervento-di-kato-david-kisule-al-iv-congresso-di-certi-diritti.html)

### [UGANDA: FIRMA LA PETIZIONE CONTRO LA LEGGE ANTI-GAY](tutte-le-notizie/1011-uganda-firma-la-petizione-contro-la-legge-anti-gay.html)

### [Deputati radicali depositano interrogazione urgente su David Kato](tutte-le-notizie/1014-deputati-radicali-depositano-interrogazione-urgente-su-david-kato.html)

[Testimonianza di Gabriella Friso su David Kato](contributi/1035-testimonianza-di-gabriella-friso-su-david-kato.html)
---------------------------------------------------------------------------------------------------------------------

[Testimonianza di Elio Polizzotto su David Kato](index.php?option=com_content&view=article&id=1157:testimonianza-di-elio-polizzotto-su-david-kato&catid=58:contributi-&Itemid=198)
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------