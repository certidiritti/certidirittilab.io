---
title: 'CEDU: una tassa per presentare ricorsi. Certi Diritti aderisce alla campagna di Amnesty International'
date: Wed, 02 Feb 2011 09:36:51 +0000
draft: false
tags: [Comunicati stampa]
---

La tassa rischia di limitare le iniziative legali per la difesa dei diritti civili e umani, tra cui quelle di Certi Diritti per il riconoscimento del matrimonio tra persone dello stesso sesso.

Roma, 2 febbraio 2011

**Comunicato Stampa dell’Associazione Radicale Certi Diritti**

Interights, l’International Centre for the Legal Protection of Human Rights, ha reso noto che la Corte Europea dei Diritti dell’Uomo vuole introdurre un costo di accesso per poter ricorrere alla Corte, istituendo il  pagamento di una “tassa” a carico di ogni ricorrente.

Questo causerà una vera e propria barriera di ingresso per quelle persone che non potranno permettersi economicamente di ricorrere e che, più verosimilmente, sono vittime di violazione dei diritti umani.

Questa decisione avrà sicuramente un impatto negativo sulle future iniziative previste dall’Associazione Radicale Certi Diritti  riguardo la campagna di  Affermazione Civile.

L’Associazione Radicale Certi Diritti insieme a molte  organizzazioni internazionali aderisce alla campagna di Amnesty International che invita cittadini, Ong e Associazioni,  che si battono per la difesa dei diritti civili e umani, a scrivere al Ministro degli Esteri del proprio paese e all’Ufficio di  rappresentanza del Consiglio d’Europa.  **Vi è anche l’invito a scrivere una mail entro lunedì 28 febbraio 2011 al seguente indirizzo e-mail:  [Europeigoteam@amnesty.org](mailto:Europeigoteam@amnesty.org)**

**[RIPARTE LA CAMPAGNA DI CERTI DIRITTI PER IL DIRITTO AL MATRIMONIO COME PRINCIPIO DI UGUAGLIANZA >](campagne/diritto-al-matrimonio/affermazione-civile-2011.html)**

**Comunicato Stampa in inglese di Interights:**

INTERIGHTS is opposing the introduction of fees for individuals who file an application to the European Court of Human Rights. As part of our ongoing work with a coalition of NGOS relating to reform of the Court, we have signed a statement opposing this.

Imposing a fee on applicants to the European Court of Human Rights may deny victims of human rights violations access to justice, based on their ability to pay. Administering a fee system could drain the Court of human and financial resources while deterring individuals – based on their economic standing – with well-founded human rights claims from seeking redress before the Court.

If you are an NGO, a Bar Association or a law firm and would like to sign the petition opposing the introduction of fees, please email Amnesty International at [Europeigoteam@amnesty.org](mailto:Europeigoteam@amnesty.org) with the name and country of the organisation and your name and email address by Monday 28 February.

You may also wish to send a letter to your Minister of Foreign Affairs and your Permanent Representative in Strasbourg urging that they oppose the proposal to charge applicants.

For a list of the Ministers for Foreign Affairs in the Member States of the Council of Europe, click here  ([http://www.coe.int/t/cm/MFA_en.asp#TopOfPage](#TopOfPage))

For a list of the Permanent Representatives to the Council of Europe, click here ([http://www.coe.int/t/cm/PRs_en.asp#TopOfPage](#TopOfPage))

Please forward this email onto any contacts who you think may be interested in signing the statement.

To read more about our work on reform of the Court, click here ([http://www.interights.org/reform-European-Court-of-Human-Rights/index.htm](http://www.interights.org/reform-European-Court-of-Human-Rights/index.htm))

The International Centre for the Legal Protection of Human Rights, Lancaster House -  33 Islington High Street -  London- N1 9LH - United Kingdom  -  Web: [http://www.interights.org](http://www.interights.org/)