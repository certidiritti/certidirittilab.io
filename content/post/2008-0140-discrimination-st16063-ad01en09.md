---
title: '2008-0140-Discrimination-ST16063-AD01.EN09'
date: Wed, 02 Dec 2009 13:38:28 +0000
draft: false
tags: [Senza categoria]
---

  

  

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 2 December 2009

Interinstitutional File:

2008/0140 (CNS)

16063/09

ADD 1

LIMITE

SOC 706

JAI 836

MI 434

  

  

  

  

  

ADDENDUM TO THE OUTCOME OF PROCEEDINGS

from :

The Working Party on Social Questions

on :

9 & 13 November 2009

No. prev. doc. :

15896/09 SOC 686 JAI 829 MI 425

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Further to the meeting of the Working Party on Social Questions on 9 and 13 November 2009, delegations will find attached the text of the draft Directive, with positions expressed at these meetings as well as in previous discussions recorded in footnotes.

Changes in relation to the previous Presidency text (doc. 14009/09) are set out as follows: new text is in **bold** and deletions are marked "**\[…\]**".

The outcome of proceedings is set out in doc. 16063/09.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

Proposal for a

COUNCIL DIRECTIVE

on implementing the principle[\[1\]](#_ftn1) of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

THE COUNCIL OF THE EUROPEAN UNION,

Having regard to the Treaty establishing the European Community, and in particular Article 13(1) thereof,

Having regard to the proposal from the Commission[\[2\]](#_ftn2),

Having regard to the opinion of the European Parliament[\[3\]](#_ftn3),

Whereas:

(1)          In accordance with Article 6 of the Treaty on European Union, the European Union is founded on the principles of liberty, democracy, respect for human rights and fundamental freedoms, and the rule of law, principles which are common to all Member States and it respects fundamental rights, as guaranteed by the European Convention for the Protection of Human Rights and Fundamental Freedoms and as they result from the constitutional traditions common to the Member States, as general principles of Community law.

  

(2)          The right to equality before the law and protection against discrimination for all persons constitutes a universal right recognised by the Universal Declaration of Human Rights, the United Nations Convention on the Elimination of all forms of Discrimination Against Women, the International Convention on the Elimination of all forms of Racial Discrimination, the United Nations Covenants on Civil and Political Rights and on Economic, Social and Cultural Rights, the UN Convention on the Rights of Persons with Disabilities, the European Convention for the Protection of Human Rights and Fundamental Freedoms and the European Social Charter, to which \[all\] Member States are signatories. In particular, the UN Convention on the Rights of Persons with Disabilities includes the denial of reasonable accommodation in its definition of discrimination.

(3)          This Directive respects the fundamental rights and observes the fundamental principles recognised in particular by the Charter of Fundamental Rights of the European Union. Article 10 of the Charter recognises the right to freedom of thought, conscience and religion; Article 21 prohibits discrimination, including on grounds of religion or belief, disability, age or sexual orientation; and Article 26 acknowledges the right of persons with disabilities to benefit from measures designed to ensure their independence.

(4)          The European Years of Persons with Disabilities in 2003, of Equal Opportunities for All in 2007, and of Intercultural Dialogue in 2008 have highlighted the persistence of discrimination but also the benefits of diversity.

(5)          The European Council, in Brussels on 14 December 2007, invited Member States to strengthen efforts to prevent and combat discrimination inside and outside the labour market[\[4\]](#_ftn4).

(6)          The European Parliament has called for the extension of the protection of discrimination in European Union law[\[5\]](#_ftn5).

  

(7)          The European Commission has affirmed in its Communication ‘Renewed social agenda: Opportunities, access and solidarity in 21st century Europe’[\[6\]](#_ftn6) that, in societies where each individual is regarded as being of equal worth, no artificial barriers or discrimination of any kind should hold people back in exploiting these opportunities. Discrimination based on religion or belief, disability, age or sexual orientation may undermine the achievement of the objectives of the EC Treaty, in particular the attainment of a high level of employment and of social protection, the raising of the standard of living, and quality of life, economic and social cohesion and solidarity. It may also undermine the objective of abolishing of obstacles to the free movement of persons, goods and services between Member States.

(8)          The Community has adopted three legal instruments[\[7\]](#_ftn7) on the basis of article 13(1) of the EC Treaty to prevent and combat discrimination on grounds of sex, racial and ethnic origin, religion or belief, disability, age and sexual orientation. These instruments have demonstrated the value of legislation in the fight against discrimination_._ In particular, Directive 2000/78/EC establishes a general framework for equal treatment in employment and occupation on the grounds of religion or belief, disability, age and sexual orientation. However, variations remain between Member States on the degree and the form of protection from discrimination on these grounds beyond the areas of employment.

(9)     Therefore, legislation should prohibit discrimination based on religion or belief, disability, age or sexual orientation in a range of areas outside the labour market, including social protection, education and access to and supply of goods and services, including housing. Services should be taken to be those within the meaning of Article 50 of the EC Treaty.

(10)   Directive 2000/78/EC prohibits discrimination in access to vocational training; it is necessary to complete this protection by extending the prohibition of discrimination to education which is not considered vocational training.

(11)   Deleted.

  

(12)   Discrimination is understood to include direct and indirect discrimination, harassment, instructions to discriminate and denial of reasonable accommodation.

(12a) (new)  In accordance with the judgment of the Court of Justice in Case C-303/06[\[8\]](#_ftn8), it is appropriate to provide explicitly for protection from discrimination by association on all grounds[\[9\]](#_ftn9) covered by this Directive. Such discrimination occurs, _inter alia_, when a person is treated less favourably, or harassed, because, in the view of the discriminator, he or she is associated with persons of a particular religion or belief, disability, age or sexual orientation, for instance through his or her family, friendships, employment or occupation[\[10\]](#_ftn10). Moreover, discrimination within the meaning of this Directive also includes direct discrimination or harassment based on assumptions[\[11\]](#_ftn11) about a person's religion or belief, disability, age or sexual orientation.

(12b)[\[12\]](#_ftn12) (new) Harassment is contrary to the principle of equal treatment, since victims of harassment cannot enjoy[\[13\]](#_ftn13) access to social protection, education and goods and services on an equal basis with others[\[14\]](#_ftn14). Harassment can take different forms, including unwanted verbal, physical, or other non-verbal conduct. Such conduct may be deemed harassment in the meaning of this Directive when it is either repeated or otherwise so serious in nature that it has the purpose or effect of violating the dignity of a person and of creating an intimidating, hostile, degrading, humiliating or offensive environment. In this context, the mere expression of a personal opinion or the display of religious symbols or messages[\[15\]](#_ftn15) are presumed as not constituting harassment[\[16\]](#_ftn16).

(13)   In implementing the principle of equal treatment irrespective of religion or belief, disability, age or sexual orientation, the Community should, in accordance with Article 3(2) of the EC Treaty, aim to eliminate inequalities, and to promote equality between men and women, especially since women are often the victims of multiple discrimination.

(14)   The appreciation of the facts from which it may be presumed that there has been direct or indirect discrimination should remain a matter for the national judicial or other competent bodies in accordance with rules of national law or practice. Such rules may provide, in particular, for indirect discrimination to be established by any means including on the basis of statistical evidence.

(14a) Differences in treatment in connection with age may be permitted under certain circumstances if they are objectively justified by a legitimate aim and the means of achieving that aim are appropriate and necessary. Such differences of treatment may include, for example, special age conditions regarding access to certain goods or services such as alcoholic drinks, arms, or driving licences.

(15)[\[17\]](#_ftn17) Actuarial and risk factors related to disability[\[18\]](#_ftn18) and to age are used in the provision of insurance, banking and other financial services[\[19\]](#_ftn19). These should not be regarded as constituting discrimination where service providers have shown, by relevant actuarial principles, accurate statistical data or medical knowledge[\[20\]](#_ftn20), that  such factors are determining factors for the assessment of risk.

  

(16)     Deleted.

(17)   While prohibiting discrimination, it is important to respect other fundamental rights and freedoms, including the protection of private and family life and transactions carried out in that context, the freedom of religion, the freedom of association, the freedom of expression and the freedom of the press[\[21\]](#_ftn21).

(17b)[\[22\]](#_ftn22) (new)  This Directive covers access to social protection[\[23\]](#_ftn23), which includes social security, social assistance, and health care, thereby providing comprehensive protection against discrimination in this field. Consequently, the Directive applies with regard to access to rights and benefits which are derived from general or special social security,  social assistance and healthcare schemes, which are provided either directly by the State, or by private parties in so far as the provision of those benefits by the latter is funded by the State[\[24\]](#_ftn24). In this context, the Directive applies with regard to benefits  in cash, benefits in kind and services, irrespective of whether the schemes involved are contributory or non-contributory. The abovementioned schemes include, for example, access to the branches of social security defined by Regulation 883/2004/EC on the coordination of social security systems[\[25\]](#_ftn25), as well as schemes providing for benefits or services granted for reasons related to the lack of financial resources or risk of social exclusion.

(17c) Deleted.

  

(17d) (new) All individuals enjoy the freedom to contract, including the freedom to choose a contractual partner for a transaction. This Directive should not apply to economic transactions undertaken by individuals for whom these transactions do not constitute a professional or commercial activity. In this context, the concept of professional or commercial activity may be defined in accordance with the national laws and practice of the Member States.

(17e) (new) This directive does not alter the division of competences between the European Community and the Member States in the areas of education and social protection, including social security, social assistance and health care. It is also without prejudice to the essential role and wide discretion of the Member States in providing, commissioning and organising services of general economic interest.

(17f) (new) The exclusive competence of Member States with regard to the organisation of their social protection systems includes decisions on the setting up, financing and management of such systems and related institutions as well as on the substance and delivery of benefits and health services and the conditions of eligibility. In particular Member States retain the possibility to reserve certain benefits or services to certain age groups or persons with disabilities. Moreover, this Directive is without prejudice to the powers of the Member States to organise their social protection systems in such a way as to guarantee their sustainability[\[26\]](#_ftn26).

(17g) (new) The exclusive competence of Member States with regard to the content of teaching or activities and the organisation of national educational systems, including the provision of special needs education,  includes the setting up and management of educational institutions, the development of curricula and other educational activities and the definition of examination processes. In particular Member States retain the possibility to set age limits for certain education activities. However, there may be no discrimination in the access to educational activities, including the admission to and participation in classes or programmes and the evaluation of students' performance.

  

(17h) (new) This Directive does not apply to matters covered by family law[\[27\]](#_ftn27) including marital status and adoption, and laws on reproductive rights[\[28\]](#_ftn28). It is also without prejudice to the secular nature of the State, state institutions or bodies, or education.

(18)     Deleted.

(19a)[\[29\]](#_ftn29) Persons with disabilities include those who have long‑term physical, mental, intellectual or sensory impairments which, in interaction with various barriers, may hinder their full and effective participation in society on an equal basis with others.

(19b)[\[30\]](#_ftn30) (new) Measures to **\[…\]** **ensure accessibility** **for** persons with disabilities **\[…\]**, on an equal basis with others, to the areas covered by this Directive play an important part in ensuring full equality in practice. Such measures **\[…\]** **should** include **\[…\]** the identification and elimination of obstacles and barriers to accessibility. **\[…\]. They should not impose a disproportionate burden**.

(19c)[\[31\]](#_ftn31) (new) **\[…\]**

  

(19d) (new) Improvement of access**ibility** can be provided by a variety of means, including application of the "universal design" principle. “Universal design” means the design of products, environments, programmes and services to be usable by all people, to the greatest possible extent, without the need for adaptation or specialised design. “Universal design” should not exclude[\[32\]](#_ftn32) assistive devices for particular groups of persons with disabilities where this is needed.[\[33\]](#_ftn33)

(20)   Legal requirements[\[34\]](#_ftn34) and standards on accessibility have been established at European level in some areas while Article 16 of Council Regulation 1083/2006 of 11 July 2006 on the European Regional Development Fund, the European Social Fund and the Cohesion Fund and repealing Regulation (EC) No 1260/1999[\[35\]](#_ftn35) requires that accessibility for disabled persons is one of the criteria to be observed in defining operations co-financed by the Funds. The Council has also emphasised the need for measures to secure the accessibility of cultural infrastructure and cultural activities for people with disabilities[\[36\]](#_ftn36).

**(20a) (new) In addition to general measures to ensure accessibility, individual measures to provide reasonable accommodation play an important part in ensuring full equality in practice for persons with disabilities to the areas covered by this Directive. Reasonable accommodation means necessary and appropriate modifications and adjustments not imposing a disproportionate burden, where needed in a particular case, in order to ensure to persons with disabilities access on an equal basis with others**[\[37\]](#_ftn37)**.**

**(20b) (new) In assessing whether measures to ensure accessibility or reasonable accommodation would impose a disproportionate burden, account should be taken of a number of factors including the size, resources and nature of the organisation or enterprise, as well as the costs and possible benefits of such measures. A disproportionate burden would arise, for example, where significant structural changes would be required in order to provide access to movable or immovable property which is protected under national rules on account of their historical, cultural, artistic or architectural value**[\[38\]](#_ftn38)**.**

**(20c)**[\[39\]](#_ftn39) **(new) The principle of accessibility is established in the United Nations Convention on the Rights of Persons with Disabilities. The principles of reasonable accommodation and disproportionate burden are established in Directive 2000/78/EC**[\[40\]](#_ftn40) **and the United Nations Convention on the Rights of Persons with Disabilities.**

(21)   The prohibition of discrimination should be without prejudice to the maintenance or adoption by Member States of measures intended to prevent or compensate for disadvantages suffered by a group of persons of a particular religion or belief, disability, age or sexual orientation. Such measures may permit organisations of persons of a particular religion or belief, disability, age or sexual orientation where their main object is the promotion of the special needs of those persons.

(22)   This Directive lays down minimum requirements, thus giving the Member States the option of introducing or maintaining more favourable provisions. The implementation of this Directive should not serve to justify any regression in relation to the situation which already prevails in each Member State.

  

(23)   Persons who have been subject to discrimination based on religion or belief, disability, age or sexual orientation should have adequate means of legal protection. To provide a more effective level of protection, associations, organisations and other legal entities should be empowered to engage in proceedings, including on behalf of or in support of any victim, without prejudice to national rules of procedure concerning representation and defence before the courts.

(24)   The rules on the burden of proof must be adapted when there is a prima facie case of discrimination and, for the principle of equal treatment to be applied effectively, the burden of proof must shift back to the respondent when evidence of such discrimination is brought. However, it is not for the respondent to prove that the plaintiff adheres to a particular religion or belief, has a particular disability, is of a particular age or has a particular sexual orientation.

(25)   The effective implementation of the principle of equal treatment requires adequate judicial protection against victimisation.

(26)   In its resolution on the Follow-up of the European Year of Equal Opportunities for All (2007), the Council called for the full association of civil society, including organisations representing people at risk of discrimination, the social partners and stakeholders in the design of policies and programmes aimed at preventing discrimination and promoting equality and equal opportunities, both at European and national levels.

(27)   Experience in applying Directives 2000/43/EC[\[41\]](#_ftn41) and 2004/113/EC[\[42\]](#_ftn42) show that protection from discrimination on the grounds covered by this Directive would be strengthened by the existence of a body or bodies in each Member State, with competence to analyse the problems involved, to study possible solutions and to provide concrete assistance for the victims.

(28)   Deleted.

  

(29)   Member States should provide for effective, proportionate and dissuasive sanctions in case of breaches of the obligations under this Directive.

(30)   In accordance with the principles of subsidiarity and proportionality as set out in Article 5 of the EC Treaty, the objective of this Directive, namely ensuring a common level of protection against discrimination in all the Member States, cannot be sufficiently achieved by the Member States and can therefore, by reason of the scale and impact of the proposed action, be better achieved by the Community. This Directive does not go beyond what is necessary in order to achieve those objectives.

(31)   In accordance with paragraph 34 of the interinstitutional agreement on better law-making, Member States are encouraged to draw up, for themselves and in the interest of the Community, their own tables, which will, as far as possible, illustrate the correlation between the Directive and the transposition measures and to make them public.

Article 1  
Purpose[\[43\]](#_ftn43)

This Directive lays down a framework for combating discrimination on the grounds of religion or belief, disability, age, or sexual orientation, with a view to putting into effect in the Member States the principle[\[44\]](#_ftn44) of equal treatment within the scope of Article 3.

Article 2[\[45\]](#_ftn45)  
Concept of discrimination

1.       For the purposes of this Directive, the “principle[\[46\]](#_ftn46) of equal treatment” shall mean that  there shall be no direct or indirect discrimination**, or discrimination in the form of denial of reasonable accommodation,** on any of the grounds referred to in Article 1.

2.       For the purposes of paragraph 1, the following definitions apply[\[47\]](#_ftn47):

(a)     direct discrimination shall be taken to occur where one person is treated less favourably than another is, has been or would be treated in a comparable situation, on any of the grounds referred to in Article 1;

(b)[\[48\]](#_ftn48) indirect discrimination shall be taken to occur where an apparently neutral provision, criterion or practice would put persons of a particular religion or belief, a particular disability, a particular age, or a particular sexual orientation at a particular disadvantage compared with other persons, unless that provision, criterion or practice is objectively justified by a legitimate aim and the means of achieving that aim are appropriate and necessary;

**(c)     (new) denial of reasonable accommodation in a particular case, as provided for by Article 4a of the present Directive as regards persons with disabilities, shall be taken to be discrimination.**[\[49\]](#_ftn49)

3.[\[50\]](#_ftn50) Harassment shall be deemed to be a form of discrimination within the meaning of paragraph 1, when unwanted[\[51\]](#_ftn51) conduct related to any of the grounds referred to in Article 1[\[52\]](#_ftn52) takes place with the purpose or effect of violating the dignity of a person and of creating an intimidating, hostile, degrading, humiliating or offensive environment. In this context, the concept of harassment may be defined in accordance with the national laws and practice of the Member States[\[53\]](#_ftn53).

3a[\[54\]](#_ftn54).  Discrimination includes direct discrimination or harassment due to a person's association[\[55\]](#_ftn55) with persons of a certain religion or belief, persons with disabilities, persons of a given age or of a certain sexual orientation; or based on assumptions[\[56\]](#_ftn56) about a person's religion or belief, disability, age or sexual orientation.

4.       An instruction to discriminate against persons on any of the grounds referred to in Article 1 shall be deemed to be discrimination within the meaning of paragraph 1.

  

5.       **\[…\]**

6[\[57\]](#_ftn57).    Notwithstanding paragraph 2, differences of treatment on grounds of age shall not constitute discrimination, if they are objectively[\[58\]](#_ftn58) justified by a legitimate aim, and if the means of achieving that aim are appropriate and necessary. In this context Member States may **lay down in their national law \[…\]** aims which can be considered to be legitimate[\[59\]](#_ftn59) **and means which can be considered appropriate and necessary**.

Such differences of treatment may include the fixing of a specific age[\[60\]](#_ftn60) for access to social protection, including social security, social assistance and healthcare; education; and certain goods or services which are available to the public**, as well as maintaining or adopting more favourable conditions of access for persons of a given age, in order to promote their economic, cultural or social integration.**

6a.     (new) Notwithstanding paragraph 2, differences of treatment of persons with a disability[\[61\]](#_ftn61) shall not constitute discrimination, if they are aimed at protecting their health and safety and if the means of achieving that aim are appropriate and necessary.

  

7[\[62\]](#_ftn62).    Notwithstanding paragraph 2, in the provision of financial services[\[63\]](#_ftn63), proportionate differences in treatment where, for the service in question, the use of age or disability is a determining factor in the assessment of risk based on relevant actuarial principles, accurate statistical data or  medical knowledge[\[64\]](#_ftn64) shall not be considered discrimination for the purposes of this Directive[\[65\]](#_ftn65).

8.       This Directive shall be without prejudice to measures laid down in national law[\[66\]](#_ftn66) which, in a democratic society, are necessary for public security, for the maintenance of public order and the prevention of criminal offences, for the protection of health and the protection of the rights and freedoms of others.

Article 3  
Scope[\[67\]](#_ftn67)

1[\[68\]](#_ftn68).    Within the limits of the powers conferred upon the Community, the prohibition of discrimination shall apply to all persons, as regards both the public and private sectors, including public bodies, in relation to access to[\[69\]](#_ftn69):

(a)          Social protection, including social security, social assistance and healthcare;

(b)          \[Deleted.\] [\[70\]](#_ftn70)

(c)          Education[\[71\]](#_ftn71);

(d)     and the supply of, goods and other services which are available to the public, including housing.

Subparagraph (d) shall apply to natural persons only[\[72\]](#_ftn72) insofar as they are performing a professional or commercial activity defined  in accordance with national laws and practice.

  

2.       This Directive does not alter the division of competences between the European Community and the Member States. In particular[\[73\]](#_ftn73) it does not apply to:

(a)     matters covered by family law[\[74\]](#_ftn74), including marital status and adoption, and laws on reproductive rights[\[75\]](#_ftn75);

(b)     the organisation of  Member States' social protection    systems, including decisions on the setting up, financing and management of such systems and related institutions as well as on the substance and delivery of benefits and services and the conditions of eligibility[\[76\]](#_ftn76);

(c)     the powers of Member States to determine the type of health services provided and the conditions of eligibility; and

(d)     the content[\[77\]](#_ftn77) of teaching or **of educational** activities and the organisation of Member States' educational systems, including the provision of special needs education.

3.       Member States may provide that differences of treatment  based on religion or belief in respect of admission[\[78\]](#_ftn78) to educational institutions, the ethos of which is based on religion or belief, in accordance with national laws, traditions and practice, shall not constitute discrimination.

3a.     This Directive is without prejudice to national measures authorising or prohibiting the wearing of religious symbols[\[79\]](#_ftn79).

4[\[80\]](#_ftn80).    This Directive is without prejudice to national legislation ensuring the secular nature of the State, State institutions or bodies, or education, or concerning the status and activities of churches and other organisations based on religion or belief.

5.       This Directive does not cover differences of treatment based on nationality and is without prejudice to provisions and conditions relating to the entry into and residence of third-country nationals and stateless persons in the territory of Member States, and to any treatment which arises from the legal status of the third-country nationals and stateless persons concerned.

Article 4 (new)  [\[81\]](#_ftn81)Accessibility for  persons with disabilities

**1**[\[82\]](#_ftn82)**.    \[…\]** Member States shall take the necessary and appropriate measures to ensure accessibility for persons with disabilities, on an equal basis with others, within the areas set out in Article 3**, unless this would impose a disproportionate burden**[\[83\]](#_ftn83)**.**

**2.** Such measures shall include the identification and elimination of obstacles and barriers[\[84\]](#_ftn84) to accessibility, \[including with regard to the physical environment[\[85\]](#_ftn85) and to information and communication technology and systems\][\[86\]](#_ftn86).

**3.       \[…\]**

_Article 4a_[_\[87\]_](#_ftn87)_  
Reasonable accommodation for persons with disabilities_

**1.** In order to guarantee compliance with the principle of equal treatment in relation to persons with disabilities, reasonable accommodation[\[88\]](#_ftn88) shall be provided within the areas set out in Article 3 **\[…\]**, unless this would impose a disproportionate burden[\[89\]](#_ftn89).

**2\.** Reasonable accommodation means **necessary and appropriate modifications and adjustments \[…\] where** needed in a particular case**,** **\[…\]** to ensure **to persons with disabilities** access on an equal basis with others[\[90\]](#_ftn90).

Article 4b (new)[\[91\]](#_ftn91)  
Common Provisions Regarding **Accessibility and** Reasonable Accommodation **\[…\]**

1.       For the purposes of assessing[\[92\]](#_ftn92) whether measures necessary to comply with Articles 4 and 4a would impose a disproportionate burden, account shall be taken, in particular[\[93\]](#_ftn93), of:

a)           the size and resources of the organisation or enterprise[\[94\]](#_ftn94),

b)[\[95\]](#_ftn95) the nature of the organisation or enterprise,

c)           the estimated cost, **\[…\]**[\[96\]](#_ftn96)

d)[\[97\]](#_ftn97) the possible benefits of increased access for persons with disabilities**, and**

**e)       (new) the historical, cultural, artistic or architectural value of the movable or immovable property in question**[\[98\]](#_ftn98)**.**

The burden shall not be deemed disproportionate when it is sufficiently remedied by measures existing within the framework of the equal treatment policy[\[99\]](#_ftn99) of the Member State concerned.

  

2.         Articles 4 and 4a shall not apply[\[100\]](#_ftn100) to the design and manufacture of goods.

3.       **\[…\]**

3.         **\[…\]**

4[\[101\]](#_ftn101).   This Directive shall be without prejudice[\[102\]](#_ftn102) to the provisions of Community law **\[…\]** providing for detailed standards or specifications on the accessibility of particular goods or services **\[…\]**.

Article 5  
Positive action

1.  With a view to ensuring full equality in practice, the principle of equal treatment shall not prevent any Member State from maintaining or adopting specific measures to prevent or compensate for disadvantages linked to religion or belief, disability, age, or sexual orientation.

  

2[\[103\]](#_ftn103).  (new) In particular, the principle of equal treatment shall be without prejudice to the right of Member States to maintain or adopt more favourable provisions **\[…\]** for persons with disabilities as regards conditions for access to social protection, including social security, social assistance and healthcare; education; and certain goods or services which are available to the public, in order to promote their economic, cultural or social integration.

Article 6  
Minimum requirements

1.       Member States may introduce or maintain provisions which are more favourable to the protection of the principle of equal treatment than those laid down in this Directive.

2.       The implementation of this Directive shall under no circumstances constitute grounds for a reduction in the level of protection against discrimination already afforded by Member States in the fields covered by this Directive.

CHAPTER II  
REMEDIES AND ENFORCEMENT

Article 7  
Defence of rights

1.       Member States shall ensure that judicial and/or administrative procedures, including where they deem it appropriate conciliation procedures, for the enforcement of obligations under this Directive are available to all persons who consider themselves wronged by failure to apply the principle of equal treatment to them, even after the relationship in which the discrimination is alleged to have occurred has ended.

  

2.       Member States shall ensure that associations, organisations or other legal entities, which have, in accordance with the criteria laid down by their national law, a legitimate interest in ensuring that the provisions of this Directive are complied with, may engage[\[104\]](#_ftn104), either on behalf or in support of the complainant, with his or her approval, in any judicial and/or administrative procedure provided for the enforcement of obligations under this Directive.

3.       Paragraphs 1 and 2 shall be without prejudice to national rules relating to time limits for bringing actions as regards the principle of equality of treatment.

4.       (new) **\[…\]**

Article 8  
Burden of proof

1.       Member States shall take such measures as are necessary, in accordance with their national judicial systems, to ensure that, when persons who consider themselves wronged because the principle of equal treatment has not been applied to them establish, before a court or other competent authority, facts from which it may be presumed that there has been direct or indirect discrimination, it shall be for the respondent to prove[\[105\]](#_ftn105) that there has been no breach of the prohibition of discrimination.

2.       Paragraph 1 shall not prevent Member States from introducing rules of evidence which are more favourable to plaintiffs.

  

3.         Paragraph 1 shall not apply to criminal procedures.

4.         Member States need not apply paragraph 1 to proceedings in which the court or other competent body investigates the facts of the case.

5.       Paragraphs 1, 2, 3 and 4 shall also apply to any legal proceedings commenced in accordance with Article 7(2).

Article 9  
Victimisation

Member States shall introduce into their national legal systems such measures as are necessary to protect individuals from any adverse treatment or adverse consequence as a reaction to a complaint or to proceedings[\[106\]](#_ftn106) aimed at enforcing compliance with the principle of equal treatment

Article 10  
Dissemination of information

Member States shall ensure that the provisions adopted pursuant to this Directive, together with the relevant provisions already in force, are brought to the attention of the persons concerned by appropriate means throughout their territory.

Article 11  
Dialogue with relevant stakeholders

With a view to promoting the principle of equal treatment, Member States shall encourage dialogue with relevant stakeholders, which have, in accordance with their national law and practice, a legitimate interest in contributing to the fight against discrimination on the grounds and in the areas covered by this Directive.

Article 12  
Bodies for the Promotion of Equal treatment

1.       Member States shall designate a body or bodies for the promotion of equal treatment of all persons irrespective of their religion or belief, disability, age, or sexual orientation. These bodies may form part of agencies charged at national level with the defence of human rights or the safeguard of individuals' rights.

2.       Member States shall ensure that the competences of these bodies include:

(a)     without prejudice to the right of victims and of associations, organizations or other legal entities referred to in Article 7(2), providing independent assistance to victims of discrimination in pursuing their complaints about discrimination,

(b)     conducting independent surveys concerning discrimination, and

(c)     publishing independent reports and making recommendations on any issue relating to such discrimination.

CHAPTER III  
FINAL PROVISIONS

Article 13  
Compliance

Member States shall take the necessary measures to ensure that the principle of equal treatment is respected within the scope of this Directive and in particular that:

(a)     any laws, regulations and administrative provisions contrary to the principle of equal treatment are abolished;

  

(b)     any contractual provisions, internal rules of undertakings, and rules governing profit-making or non-profit-making associations contrary to the principle of equal treatment are, or may be, declared null and void or are amended.

Article 14  
Sanctions[\[107\]](#_ftn107)

Member States shall lay down the rules on sanctions applicable to infringements of national provisions adopted pursuant to this Directive, and shall take all measures necessary to ensure that they are applied. Sanctions may comprise the payment of compensation, which may not be restricted by the fixing of a prior upper limit, and must be effective, proportionate and dissuasive.

Article 14a (new) [\[108\]](#_ftn108)  
Gender mainstreaming

In accordance with  the objective of  Article 3(2) of the EC Treaty, Member States shall, when implementing this Directive, take into account the objective of equality between men and women.

Article 15[\[109\]](#_ftn109)  
Implementation

1.       Member States shall adopt the laws, regulations and administrative provisions necessary to comply with this Directive by …. at the latest \[X years after adoption\][\[110\]](#_ftn110). They shall forthwith inform the Commission thereof and shall communicate to the Commission the text of those provisions[\[111\]](#_ftn111).

  

When Member States adopt these measures, they shall contain a reference to this Directive or be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.

2[\[112\]](#_ftn112).   In order to take account of particular conditions[\[113\]](#_ftn113), Member States may, if necessary, establish that **\[…\]** the obligation to ensure accessibility as set out in Article 4 **\[…\] and 4b** has to be complied with by … at the latest \[**\[…\] 5** years after adoption\] regarding new buildings, facilities and infrastructure**, as well as existing**[\[114\]](#_ftn114) **buildings, facilities and infrastructure undergoing significant renovation**[\[115\]](#_ftn115) and by \[20 years after adoption\] regarding **all other** existing buildings, facilities and infrastructure.

Member States wishing to use any of these additional periods shall inform the Commission at the latest by the date set down in paragraph 1 giving reasons. Member States shall also communicate to the Commission by the same date an action plan[\[116\]](#_ftn116) laying down the steps to be taken and the timetable for achieving the gradual implementation of Article **4** **\[…\]**. They shall report on progress every two years starting from this date.

Article 16  
Report

1.       Member States shall communicate to the Commission, by …. at the latest and every five years thereafter, all the information necessary for the Commission to draw up a report to the European Parliament and the Council on the application of this Directive.

2.       The Commission's report shall take into account, as appropriate, the viewpoints of national equality bodies and relevant stakeholders, as well as the EU Fundamental Rights Agency. In accordance with the principle of gender mainstreaming, this report shall, _inter alia_, provide an assessment of the impact of the measures taken on women and men. In the light of the information received, this report shall include, if necessary, proposals to revise and update this Directive.

Article 17  
Entry into force

This Directive shall enter into force on the day of its publication in the Official Journal of the European Union.

Article 18  
Addressees

This Directive is addressed to the Member States.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) IE maintained a reservation. See Article 1.

[\[2\]](#_ftnref2) OJ C , , p. .

[\[3\]](#_ftnref3) OJ C , , p. .

[\[4\]](#_ftnref4) Presidency conclusions of the Brussels European Council of 14 December 2007, point 50.

[\[5\]](#_ftnref5) Resolution of 20 May 2008 P6_TA-PROV(2008)0212.

[\[6\]](#_ftnref6) COM (2008) 412

[\[7\]](#_ftnref7) Directive 2000/43/EC, Directive 2000/78/EC and Directive 2004/113/EC

[\[8\]](#_ftnref8) Case C-303/06, Coleman v. Attridge, judgment of 17 July 2008, nyr.

[\[9\]](#_ftnref9) CZ, DK, DE, IT and NL questioned the expansion of the concept of discrimination by association to cover "all grounds". See also Article 2(3a).

[\[10\]](#_ftnref10) LV suggested deleting "for instance through his or her family, friendships, employment or occupation".

[\[11\]](#_ftnref11) EL, FR and IT suggested deleting the separate mention of discrimination based on assumptions; IT and EE also preferred defining discrimination based on association in relation to its seriousness, as was done for harassment. See also Article 2(3a) and doc. 13495/09. Cion was unable to accept this approach.

[\[12\]](#_ftnref12) DK and MT maintained scrutiny reservations.

[\[13\]](#_ftnref13) NL suggested moving "on an equal basis with others" here. Cion welcomed this suggestion.

[\[14\]](#_ftnref14) CZ supported by BG suggested deleting the second part of the sentence: "since… others."

[\[15\]](#_ftnref15) CZ, LT and AT suggested clarifying "religious symbols or messages".

[\[16\]](#_ftnref16) EE, PT, SK and FI suggested deleting the last sentence. FR, similarly, entered a reservation on the last sentence. IE questioned the consistency between the last sentence and Article 2(3), whereby harassment is to be defined nationally. IT, for its part, suggested moving the words to Article 2(3) (see doc. 13495/09).

[\[17\]](#_ftnref17) AT maintained a reservation. FI made a written suggestion (doc. 13496/09) and also suggested replacing "product in question" with "service in question" therein. MT suggested: "…where ensuing proportionate differences in treatment for the product in question occur provided that the service providers in question have shown by relevant and accurate actuarial and statistical data…" SK supported this wording.

[\[18\]](#_ftnref18) CZ entered a scrutiny reservation on the inclusion of disability as a risk-assessment factor. BE and HU also expressed doubts, especially in the context of the UNCRPD. AT suggested replacing "disability" with "state of health". BE also suggested that insurance companies should disclose the ways in which they used age and disability to calculate risk.

[\[19\]](#_ftnref19) SK preferred "products".

[\[20\]](#_ftnref20) IT suggested replacing "medical knowledge" with "scientifically proved medical knowledge and evidence" (see doc. 13495). BG, supporting, suggested "…_or_ evidence". Cion pointed out that many insurers lacked statistics on at least certain types of disability.

[\[21\]](#_ftnref21) BG suggested deleting the reference to the freedom of the press in this context.

[\[22\]](#_ftnref22) All delegations maintain scrutiny reservations on Article 3 and Recital 17b.

[\[23\]](#_ftnref23) NL urged the need to clarify the distinction between "access to" and "the content of" social protection.

[\[24\]](#_ftnref24) FI maintained a scrutiny reservation. This delegation preferred "statutory schemes of general or specific social security" (see doc. 13496/09). FR supported this approach. Agreeing that there was a need to improve the text, Cion pointed out that "statutory schemes" had a narrow meaning.

[\[25\]](#_ftnref25) OJ L 166, 30.4.2004, p. 1. FI expressed the view that the reference to Regulation 883/2004/EC did not improve clarity, as the branches of social security covered by the said Regulation did not fully correspond to the national social security classifications, which fell under the Member States' competence.

[\[26\]](#_ftnref26) BE suggested replacing the last two words with "for instance, their adequacy and sustainability".

[\[27\]](#_ftnref27) MT supported by IT and PL suggested adding "and public policy".

[\[28\]](#_ftnref28) PL suggested adding "and the benefits dependent thereon". PL maintained a reservation.

[\[29\]](#_ftnref29) FI suggested placing Recital 19a in the articles. AT entered a reservation on Recital 19a.

[\[30\]](#_ftnref30) FI suggested placing Recital 19b in the articles. PT informed delegations that it was studying the question as to whether Recital 19b was compatible with the Universal Declaration of Human Rights as well as the UNCRPD. IE and FR entered reservations on Recital 19b, pending more precise wording. AT also entered a reservation on 19b. LU pointed out that the issue of significant structural changes was already included in Article 4(3), and stated that the key distinction to be made was between buildings that were open to the public and those that were not.

[\[31\]](#_ftnref31) IE maintained a reservation.

[\[32\]](#_ftnref32) UK suggested replacing "should not exclude assistive devices" with "should not prevent the design and manufacture of assistive devices for particular groups". The Chair pointed out that the reference to the UNCRPD would then need to be removed.

[\[33\]](#_ftnref33) Article 2 of the UNCRPD.

[\[34\]](#_ftnref34) Regulation (EC) No. 1107/2006 and Regulation (EC) No 1371/2007.

[\[35\]](#_ftnref35) OJ L 210, 31.7.2006, p.25. Regulation as last amended by Regulation (EC) No 1989/2006 (OJ L 411, 30.12.2006, p.6).

[\[36\]](#_ftnref36) OJ C 134, 7.6.2003, p.7

[\[37\]](#_ftnref37) CZ thought the second sentence of Recital 20a duplicated the provisions set out in Article 4a.

[\[38\]](#_ftnref38) CZ thought the second sentence of Recital 20b duplicated the provisions set out in Article 4b.

[\[39\]](#_ftnref39) LU entered a scrutiny reservation on Recital 20c.

[\[40\]](#_ftnref40) OJ L 303, 2.12.2000, p. 16.

[\[41\]](#_ftnref41) OJ L 180, 19.7.2000, p. 22.

[\[42\]](#_ftnref42) OJ L 373, 21.12.2004, p. 37.

[\[43\]](#_ftnref43) FR suggested specifying that the Directive was without prejudice to Directive 2000/78/EC.

[\[44\]](#_ftnref44) IE maintained a reservation on the notion of a general principle of equal treatment. Cion favoured the inclusion of the term in the text.

[\[45\]](#_ftnref45) LT, MT and UK maintained scrutiny reservations on Article 2. HU entered a scrutiny reservation on Article 2(1)-(2). FR entered a reservation on Article 2(1)-(2). CZ and IT entered scrutiny reservations on Article 2(1).

[\[46\]](#_ftnref46) IE, DE and UK have questioned the inclusion of "the principle of equal treatment" here.

[\[47\]](#_ftnref47) DE and IT saw a need for further work on the definitions. IE suggested incorporating "discrimination by imputation" into the definition of discrimination (see doc. 13046/09).

[\[48\]](#_ftnref48) IE maintained a reservation on the definition of "indirect discrimination" on the grounds that a _potential_ disadvantage should not be viewed as constituting indirect discrimination. See also ES note in doc. DS 1215/1/08 REV 1.

[\[49\]](#_ftnref49) AT was unable to support (c). CZ and IT entered scrutiny reservations.

[\[50\]](#_ftnref50) DK and MT entered scrutiny reservations. BG, BE, LV, LT and AT also had certain doubts about the inclusion of "harassment". (See DK note supported by PL in doc. 12893/09.)

[\[51\]](#_ftnref51) ES and PT suggested deleting "unwanted".

[\[52\]](#_ftnref52) CZ, DK, DE, IT and NL questioned the expansion of the concept of discrimination by association to cover "all grounds". (See also Recital 12a.) DE, EL and IT maintained reservations. UK suggested replacing "any of the grounds referred to in Article 1" with "disability or age". See doc. 13636/09. Cion was unable to accept this approach.  BE, AT and UK stressed that the provisions should not unduly interfere with freedom of speech. MT suggested adding "national laws and practice".

[\[53\]](#_ftnref53) PT maintained a reservation on the grounds that this wording would bring legal uncertainty.

[\[54\]](#_ftnref54) LV and NL asked for "association" and "assumptions" to be clarified. ES supported by DK and LV suggested clarifying the text by means of separate recitals for these two concepts. BG entered a reservation on Article 2(3a). DE, AT and MT entered scrutiny reservations. (See also earlier suggestion by IE in doc. 13046/09.)

[\[55\]](#_ftnref55) IT supported by EE suggested defining discrimination based on association in relation to its seriousness, as was done for harassment (see doc. 13495/09).

[\[56\]](#_ftnref56) UK and Cion felt that discrimination based on assumptions was covered by the phrase "on the grounds of". EL, FR, IT and MT also questioned the need for a separate mention of discrimination "based on assumptions". DK called for further justification for this addition.

[\[57\]](#_ftnref57) Delegations have affirmed the competences of the Member States for setting age limits. See docs. 12893/09, 13651/09, 13636/09 and 13496/09. All delegations maintained scrutiny reservations on Article 2(6). FR entered a reservation on Article 2(6).

[\[58\]](#_ftnref58) CZ and FI preferred "objectively _and reasonably_". FI felt that relevant limitations on the Member States' discretion, stemming from international law and human rights, should be mentioned in the paragraph.

[\[59\]](#_ftnref59) IE also saw a need to address legitimate differences of treatment in contexts such as sport and drama (see doc. 13046/09).

[\[60\]](#_ftnref60) PL suggested "specific rules" rather than "a specific age". (See doc. DS 1173/08.)

[\[61\]](#_ftnref61) CZ, IE, FR, FI, UK have expressed hesitation in respect of the disability exemption and the way it is formulated (see note from IE in doc. 13046/09). AT and BE have called for all the disability provisions to be grouped in Article 4.

[\[62\]](#_ftnref62) AT maintained a reservation on Article 2(7) and recalled its written suggestion (doc. 15896/09). BE, CZ, NL, IT and UK maintained scrutiny reservations on Article 2(7). See also note from IE (doc. 13740/09). FR affirmed the drafting suggestions contained in doc. 16594/08 ADD 1. See also doc. 16063/09.

[\[63\]](#_ftnref63) SK preferred "products".

[\[64\]](#_ftnref64) IT suggested replacing "medical knowledge" with "scientifically proved medical knowledge and evidence" (see doc. 13495/09). BG, supporting, suggested "…_or_ evidence". Cion pointed out that many insurers lacked statistics on at least certain types of disability. BE, IT and NL have also underlined the importance of publishing risk assessment data; BE has suggested placing responsibilities on insurance companies to disclose how they used age and disability to calculate risk.

[\[65\]](#_ftnref65) CY suggested ending the sentence with "and shall not consider such differences as discrimination".

[\[66\]](#_ftnref66) IE suggested adding "and practice"; MT suggested adding "and public policy". NL asked for the meaning of the term "national law" to be clarified.

[\[67\]](#_ftnref67) All delegations maintain scrutiny reservations on Article 3 and Recital 17b. DE has expressed particular concern in respect of the financial implications arising from the scope. EL has asked for "education" to be deleted from the scope. MT maintained a reservation on Article 3.

[\[68\]](#_ftnref68) ES and FR maintained scrutiny reservations on Article 3(1).

[\[69\]](#_ftnref69) Many delegations (including BE, CZ, DK, DE, EL, IE, IT, LV, LT, MT, PL, NL, AT, SK, FI and UK) have seen a need to clarify the meaning of the term "access to", which was added with a view to clarifying the scope of the Directive (see also doc. 14896/08, p. 29). IT and UK maintained reservations.

[\[70\]](#_ftnref70) FR and FI questioned the deletion of "social advantages", FR alluding to certain concepts that were not covered by the concept of "social assistance".

[\[71\]](#_ftnref71) EL maintained its reservation in respect of the inclusion of education in the scope of the draft Directive. DE and IE have also expressed doubts in respect of the inclusion of education.

[\[72\]](#_ftnref72) SE suggested deleting "only".

[\[73\]](#_ftnref73) MT suggested replacing "in particular" with "_inter alia_".

[\[74\]](#_ftnref74) MT reiterated its call, supported by IT and PL, for "public policy" to be mentioned alongside "family law"; see doc. 14876/09. MT asked for clarification in regard to the removal of the term "family status", which appeared alongside "marital status" in the original proposal.

[\[75\]](#_ftnref75) PL suggested adding "and the benefits dependent thereon". PL maintained a reservation. See doc. 813/08.

[\[76\]](#_ftnref76) BE, CZ and DK saw a need to clarify the difference between "access" and "conditions of eligibility".

[\[77\]](#_ftnref77) NL called for clarification of the link between this provision and Recital 17g.

[\[78\]](#_ftnref78) AT made the point that refusing a child "_admission_" to an educational institution on any of the protected grounds would still be discrimination. UK felt that the provision might give the impression that faith-based schools were being given a licence to discriminate. UK entered a scrutiny reservation.

[\[79\]](#_ftnref79) IE wished to see "goods and services produced for a religious purpose" exempted. See doc. 13049/09, p. 14.

[\[80\]](#_ftnref80) CZ, IE and MT shared the view that Article 3(4) was redundant. MT maintained a scrutiny reservation.

[\[81\]](#_ftnref81) All delegations maintain scrutiny reservations on Article 4. Many delegations (DE, ES, FR, IT, IE, LU, HU, MT, NL, PT, RO, FI, UK) called for the obligations stemming from Article 4 to be spelled out more clearly, so as to avoid legal uncertainty, including with respect to housing and transport. BG, DK, DE, IE, IT, LT, MT, NL, PL, SE and UK and others raised concerns in regard to the potential costs involved, particularly in respect of housing, and for SMEs (DE, MT, IT, PT). Several delegations (BG, DE, IE, MT, SE, UK) requested clarification of the provisions on housing, UK entering a reservation on "housing".  DE requested clarification of the practical meaning of the provisions concerning transport and raised the question as to whether traffic safety and the interests of users other than persons with disabilities could be taken into consideration when defining effective and non-discriminatory access. See also Article 15.

[\[82\]](#_ftnref82) DE and AT requested that definitions for the terms used in Article 4(1) be provided. BE suggested defining "a person with a disability".

[\[83\]](#_ftnref83) BE, ES and PT suggested deleting "disproportionate burden". IE and MT asked for the term to be clarified.

[\[84\]](#_ftnref84) FR, PL and RO saw a need to clarify the reference to eliminating "obstacles and barriers".

[\[85\]](#_ftnref85) IE, NL, SK, FI and UK asked for the term "physical environment" to be clarified, IE and FI preferring "built environment".

[\[86\]](#_ftnref86) FI entered a scrutiny reservation on the links between the disability provisions and legislation in the area of transport and passenger rights. BE, FI and UK requested clarification in respect of the link with Article 4b(2), and the question as to whether the design and manufacture of goods would be covered. NL suggested deleting the words in square brackets, and preferred to see the scope clarified in Article 3 (see doc. 14781/09). LV favoured retaining these words, but explaining their meaning more clearly. CZ and IE supported the suggestion by NL.

[\[87\]](#_ftnref87) All delegations maintained scrutiny reservations on Article 4a.

[\[88\]](#_ftnref88) See DK suggestion in doc. 12893/09.

[\[89\]](#_ftnref89) UK suggested adding "not requiring a fundamental alteration".

[\[90\]](#_ftnref90) UK suggested inserting "so far as reasonably possible" after "on an equal basis with others".

[\[91\]](#_ftnref91) All delegations maintained scrutiny reservations on Article 4b.

[\[92\]](#_ftnref92) LT and NL have suggested that the Member States should be allowed to assess proportionality (see doc. 14979/09, footnote 18; and doc. 14781/09).

[\[93\]](#_ftnref93) CZ asked whether safety and health considerations could also be taken into account when assessing whether measures would impose a disproportionate burden.

[\[94\]](#_ftnref94) DE suggested that, in the interest of greater legal certainty, SMEs of a particular size (e.g. those with 20 or fewer employees) be exempted.

[\[95\]](#_ftnref95) PT suggested deleting (b).

[\[96\]](#_ftnref96) NL preferred including “the life span of the goods and services"; see doc. 14781/09.

[\[97\]](#_ftnref97) UK suggested rewording (d): "whether taking any particular steps would be effective in overcoming the difficulty that disabled people face in accessing the service in question".

[\[98\]](#_ftnref98) AT suggested deleting (e). FR entered a scrutiny reservation on "movable or immovable property".

[\[99\]](#_ftnref99) IE suggested "disability policy".

[\[100\]](#_ftnref100) BE, ES, AT and SK were unable to accept this restriction to the scope. Giving a preliminary view, NL suggested that product redesign be included _provided that it did not constitute a disproportionate burden_. FI recalled the reference to "universal design" in Recital 19d.

[\[101\]](#_ftnref101) UK asked whether the Directive applied to modes of transport not covered by national or Community legislation. DE considers that traffic safety issues and the interests of users other than persons with disabilities, including financial considerations, should be taken into consideration, and that rail and air transport should be excluded from the scope, Community rules on accessibility having been adopted. DE and MT maintained scrutiny reservations.

[\[102\]](#_ftnref102) IE suggested "shall not apply".

[\[103\]](#_ftnref103) FI reiterated its concerns in respect of the deletion of "social advantages".

[\[104\]](#_ftnref104) IE entered a reservation on Article 7 and suggested adding: "as the Member States so determine and in accordance with the criteria laid down by their national law". DE, EL, IT and AT, similarly, favoured retaining a reference to the criteria laid down by national law (cf. Directives 2000/43/EC and 2000/78/EC). DE was unable to accept the creation of _individual rights_ in this context (see also doc. 14009/09, Article 7(4).

[\[105\]](#_ftnref105) CZ, DE, IE, IT, LU, NL expressed concern in respect of the reversal of the burden of proof in the context of the draft Directive. IT maintained a scrutiny reservation. Cion explained and affirmed the inclusion of this provision.

[\[106\]](#_ftnref106) IE pointed out that "legal proceedings" were referred to in Directives 2000/78/EC and 2004/113/EC.

[\[107\]](#_ftnref107) The Chair suggested "penalties" instead of "sanctions".

[\[108\]](#_ftnref108) CZ entered a reservation on the inclusion of a gender mainstreaming article. BG, IE, NL and UK have also asked for clarification.

[\[109\]](#_ftnref109) All delegations maintained scrutiny reservations on Article 15. PL entered a reservation.

[\[110\]](#_ftnref110) BG, ES, EL, IT, LV, MT, PL, RO, FI, NL, SE and UK underlined the need for a sufficiently long transposition period. Cion acknowledged the need for a longer transposition period (e.g. four years).

[\[111\]](#_ftnref111) Cion entered a reservation on the deletion of the provision obliging the Member States to send the Commission tables illustrating the correlation between the provisions of the Directive and the instruments adopted nationally for its implementation.

[\[112\]](#_ftnref112) BG, EL, LV, MT, NL, PL, SE and UK have underlined the need for a sufficiently long implementation period. DE, LV and AT favoured an incremental approach (progressive realisation) instead of the deadlines currently provided for in Article 15(2). BG, EE, ES, FR, EL, IT, LU and LV have variously suggested a differentiated approach to different kinds of buildings.

[\[113\]](#_ftnref113) DK and FI asked for the term "particular conditions" to be clarified.

[\[114\]](#_ftnref114) DE and LV underlined the Member States' competences for deciding how to improve access to existing buildings of infrastructures. LV entered a reservation.

[\[115\]](#_ftnref115) FI expressed the fear that this term would lead to legal uncertainty. HU requested clarification in respect of private houses and rental accommodation.

[\[116\]](#_ftnref116) FI asked for "action plans" to be clarified. MT and UK affirmed the need for reporting provisions of some description, MT questioning whether two years was a sufficient interval. CION suggested aligning the reporting procedures on those contained in the UNCRPD.