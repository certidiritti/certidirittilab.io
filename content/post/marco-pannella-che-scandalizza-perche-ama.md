---
title: 'MARCO PANNELLA, CHE SCANDALIZZA PERCHE'' AMA!'
date: Thu, 13 May 2010 11:56:12 +0000
draft: false
tags: [Comunicati stampa]
---

**MARCO PANNELLA, CHE SCANDALIZZA PERCHE’ AMA!**

**di Sergio Rovasio, Segretario dell'Associazione Radicale Certi Diritti**

**Roma, 13 maggio 2010**

La notizia che il nostro iscritto Marco Pannella ha amato 3 o 4 uomini ha destato forte interesse ed è stata ripresa da molti media. I media, chi più, chi meno, hanno scelto di approfondire la questione spesso con frasi di rito e banalità del tutto scontate.  Del resto, cosa dobbiamo aspettarci da questa Italia che preferisce premiare i ladri, gli ipocriti, i parrucconi, i perbenisti da strapazzo, i frequentatori di salotti clericali o televisivi che fanno venire nausea e vomito solo a guardarli? E poi, magari, sono quelli che ‘consumano’ di nascosto, senza saper e poter amare le prostitute, la cocaina, i marchettari neri superdotati, le ragazze bionde e tettone, la trans che c’ha due tette così e che ha pure un mare di problemi così, senza che a nessuno gliene importi niente.

A noi della vicenda interessa molto la  forza e la passione che solo Marco sa vivere e allo stesso tempo proporre all’attenzione di tutti,  come ulteriore gesto d’amore che non si limita ‘ai due che si amano’ ma che diventa persino atto di generosità da proporre al mondo esterno. Questo accade sempre quando Marco è coinvolto in storie, impegni, campagne, iniziative, lotte, che poi sono sempre ‘politiche’.  Che si tratti di uno sciopero della fame di 40 giorni per la condizione dei detenuti in Italia o il tenere per mano un militante radicale per tutta la durata di una Conferenza Stampa davanti alle telecamere, anche se giunto al 7° giorno di sciopero della sete ( evidentemente perché vi trova una fonte di forza e di sollievo incomprensibile ai più) urlando ai sordi intorno del bisogno di legalità di questo mondo corrotto.  Oppure, ancora, accarezzare una persona anziana a lui tanto cara e sorridere entrambi di felicità per minuti che paiono ore, con le lacrime di una vita che scendono sul volto.

Marco Pannella è anche questo e credo che il più bel regalo che si è fatto per l’ingresso nell’81° anno è stato proprio quello di dire, per lui, la cosa più ovvia: e cioè che l’amore non va tenuto nascosto perché non è un fatto ‘privato’, altrimenti (sempre scuola sua è) vorrebbe dire ‘privarsi’ di qualcosa. L’amore, per Marco,  può essere rivolto ad una persona qualsiasi: uomo, donna, bisex, travestito, transessuale, intersessuale, asessuato, bionda, castano, clochard, omosessuale, e lo ha dimostrato giorno dopo giorno nel corso della sua meravigliosa vita. Questa è la sua/nostra politica, certo bestemmia per l’Italia di oggi. Forse è anche per questo che la nostra Associazione usa sempre aggiungere la ‘e’ dopo l’acronimo ‘lgbt’.

Noi ci permettiamo di dire a Marco: ‘Grazie!”. Grazie del tuo amore.