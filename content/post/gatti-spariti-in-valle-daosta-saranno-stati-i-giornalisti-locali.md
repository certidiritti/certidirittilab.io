---
title: 'GATTI SPARITI IN VALLE D''AOSTA: SARANNO STATI I GIORNALISTI LOCALI?'
date: Fri, 17 Jul 2009 11:36:14 +0000
draft: false
tags: [Comunicati stampa]
---

**GATTI SPARITI IN VALLE D’AOSTA: L’ASSOCIAZIONE CERTI DIRITTI SCRIVE ALLA PROTEZIONE ANIMALI DELLA VALLE D’AOSTA. FORSE RAPITI DAI GIORNALISTI?**

Giovedì 16 luglio 2009

Sulla sparizione dei due gatti che dovevano fare compagnia al Papa a Les Combes, il Segretario dell’Associazione Radicale Certi Diritti ha scritto oggi alla protezione animali della Valle d’Aosta la seguente lettera:

“Siamo profondamente preoccupati per la sparizione dei due gatti dallo chalet di Les Combes, in Valle d’Aosta, dove il Papa è in vacanza in questi giorni.  La signora Renata, custode della vicina colonia salesiana, pare che non si dia pace di questa tragedia, lei che li nutriva e li vedeva gironzolare ogni giorno nel giardino dello chalet. Ci preme segnalarvi che a Roma, domenica scorsa, quattro gatti hanno fatto sparire un giornalista del Tg3. Il nostro sospetto è che i due gatti di Les Combes siano stati rapiti da giornalisti locali per evitare, qualora fossero apparsi altri due gatti, di dover dare la notizia che anche a Les Combes c'erano quattro gatti".