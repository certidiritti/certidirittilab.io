---
title: 'AL PARLAMENTO EUROPEO TENTATIVO DI INDEBOLIRE DIRETTIVA ANTIDISCRIMINAZIONE'
date: Wed, 18 Mar 2009 09:11:33 +0000
draft: false
tags: [Comunicati stampa]
---

DIRETTIVA ANTI-DISCRIMINAZIONI / RADICALI: DELUSIONE PER L'APPROVAZIONE DI ECCEZIONI AL PRINCIPIO DI UGUAGLIANZA. LAVOREREMO PER MODIFICARE IL TESTO IN PLENARIA  
   
Bruxelles, 18/03/2009 La Commissione libertà pubbliche del Parlamento europeo ha approvato una serie di emendamenti di compromesso alla proposta della Commissione per una Direttiva anti discriminazioni che mantengono deroghe ed eccezioni alla reale applicazione del principio di eguaglianza.  
   
Dichiarazione del Segretario di Certi Diritti Sergio Rovasio e di Marco Cappato, eurodeputato Radicale:  
   
"La relazione approvata dalla commissione libertà pubbliche modifica in modo decisamente insufficiente il testo della nuova direttiva anti-discriminazioni proposto dalla Commissione europea.  
Gli emendamenti di compromesso proposti dalla relatrice dei Verdi olandesi Buitenweg negoziati ed approvati con il PPE, sebbene abbiano garantito un supporto ampio alla relazione ed alla direttiva (approvata con 34 voti favorevoli, 7 contrari e 4 astenuti), non eliminano le deroghe al principio di eguaglianza che una maggioranza di centro sinistra avrebbe potuto assicurare.  
            Rimangono le deroghe sull'ordine pubblico, non viene eliminata la possibilità di discriminare nei contratti tra privati, nel settore dell'educazione sulla base della religione, tra religioni e Chiese; rimangono i richiami controproducenti al diritto nazionale in merito al diritto matrimoniale, di famiglia ed alla salute (da intendersi riproduttiva).  
            In vista del voto in plenaria previsto per i primi giorni di aprile, chiederemo al gruppo liberale e democratico di ridepositare gli emendamenti rigettati in commissione al fine di tentare di fare giocare al Parlamento europeo, che é solamente consultato in materia, il suo tradizionale e importante ruolo di pioniere dei diritti nell'UE, invece che fare "realpolitik" a discapito delle persone discriminate".