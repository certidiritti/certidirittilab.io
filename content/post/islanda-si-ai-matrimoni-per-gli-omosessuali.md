---
title: 'Islanda: sì ai matrimoni per gli omosessuali'
date: Thu, 08 Jul 2010 09:16:37 +0000
draft: false
tags: [CEDU, Comunicati stampa, corte europea dei diritti umani, isalnda, Johanna Sigurdardottir, MATRIMONIO, matrimonio omosessuale]
---

di **Carla Caraccio** ed **Elio Polizzotto**, [Non c'é pace senza giustizia](http://www.npwj.org/)

Il 27 Giugno di quest'anno, la premier islandese Johanna Sigurdardottir (considerata la personalità più amata dal paese ed eletta nel febbraio del 2009), ha sposato la sua compagna, Jonina Leosdottir, nel giorno in cui nel paese è entrata in vigore la legge che consente tra persone dello stesso sesso.

La legge prevede che le unioni civili, già da tempo previste dalla legislazione islandese, possano essere trasformate in matrimonio a tutti gli effetti con la semplice presentazione di una domanda, e' stata approvata all'unanimità dal parlamento islandese il 12 giugno scorso.

La Corte Europea dei Diritti dell'Uomo lo scorso 24 Giugno, ha riconosciuto le coppie di fatto omosessuali parificandole a quelle eterosessuali. Questo sentenza se da una parte riconosce il cambiamento rapido che le varie società sia a livello europeo che internazionale hanno subito, introducendo un concetto di famiglia sociale diverso dal passato, non più basato sul "diritto", che esula ormai anche da quella contrapposizione ormai quasi storica tra coppie dello stesso sesso e coppie eterossesuali, e che risponde sempre più quella richiesta di unione, di divisione e condivisione della realtà quotidiana sentita dalle singole individualità.

Senza dubbio il riconoscimento da parte della Corte Europea va visto come un passo in avanti, ma l'affermazione dei giudici che la Convenzione europea non impone agli Stati di prevedere il matrimonio per coppie dello stesso sesso, richiamandosi a quell'articolo 12 della Convenzione che riconosce il diritto di sposarsi a "uomini e donne" e ancor più che i singoli stati non violano il divieto di discriminazione se, pur ammettendo la registrazione di queste unioni di fatto escludono i partner da alcuni diritti fondamentali (Art. 14 che vieta la discriminazione), si e' dimostrata debole ed incapace, lasciando ai singoli paesi e alle loro legislature interne il compito di legiferare in materia di diritto matrimoniale.

Una sentenza ed un atteggiamento che ha messo ancora una volta in risalto l'incapacità della comunità europea di rispondere a quel senso di uguaglianza e di parità dei singoli cittadini europei di fronte non solo alle istituzioni economiche e politiche ma anche a quelle sociali.