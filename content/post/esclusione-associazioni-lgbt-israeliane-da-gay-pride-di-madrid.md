---
title: 'ESCLUSIONE ASSOCIAZIONI LGBT ISRAELIANE DA GAY PRIDE DI MADRID'
date: Tue, 08 Jun 2010 21:42:04 +0000
draft: false
tags: [Comunicati stampa]
---

**GRAVE CHE L’ORGANIZZAZIONE DEL GAY PRIDE DI MADRID VIETI LA PARTECIPAZIONE DEL CARRO DI ASSOCIAZIONI LGBT ISRAELIANE DA SEMPRE IMPEGNATE PER IL SUPERAMENTO DELLE DISEGUAGLIANZE E CHE DANNO PROTEZIONE AI RAGAZZI PALESTINESI GAY.**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti e Ugo Millul rappresentante di Certi Diritti in Spagna:**

“Antonio Poveda, Presidente della Federación Española de Lesbianas, Gays, Transexuales y Bisexuales (FELGTB), dà una duplice spiegazione al divieto di partecipazione delle associazioni israeliane che si occupano della difesa dei diritti delle persone LGBTQ "Come associazione che difende i Diritti Umani abbiamo l'obbligo di difendere i diritti di tutti, incluso quelli degli attivisti che accompagnavano la Flotilla con aiuti umanitari attaccata da Israele." Visto che il viaggio in Spagna delle associazioni israeliane era finanziato dal municipio di Tel Aviv e che: "non era conveniente che una istituzione che non ha condannato l'attacco possa partecipare". Sembra quindi naturale evincere che l'invito di partecipazione delle associazioni israeliane (e apolitiche) sia da annullare, e poi aggiunge la tra le motivazioni vi sono imprecisate problematiche relative alla sicurezza causate dalla condanna internazionale del Governo israeliano e dalle manifestazioni degli ultimi giorni.

La vicenda è molto seria e rattrista che una manifestazione quale il Gay Pride di Madrid emetta un NIET sovietico alla partecipazione di associazioni che nulla hanno  a che fare con le decisioni, giuste o sbagliate che siano, di un Governo. Consigliamo vivamente ad Antonio Poveda di recarsi in Israele, magari in occasione del Pride locale e informarsi dell'attività delle associazioni escluse dal Pride spagnolo, avrebbe occasione di ricredersi sull'operato e sull'ideologia di queste associazioni, a cominciare dall’eroico lavoro di Aguda di Tel Aviv che aiuta  anche clandestinamente i ragazzi gay palestinesi.

Cosa accadrebbe se una delle associazioni gay italiane fosse stata ufficialmente invitata e poi fosse venuto meno l’invito dagli organizzatori per un’azione del Governo di Berlusconi non condivisibile?”.