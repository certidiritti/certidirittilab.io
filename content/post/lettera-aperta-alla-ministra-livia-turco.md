---
title: 'LETTERA APERTA ALLA MINISTRA LIVIA TURCO'
date: Thu, 27 Mar 2008 20:15:46 +0000
draft: false
tags: [Comunicati stampa, FARMACI, PILLOLA DEL GIORNO, SENZA RICETTA, TURCO]
---

LETTERA APERTA ALLA MINISTRA LIVIA TURCO: "INSERIRE ANCHE LA PILLOLA DEL GIORNO DOPO TRA I FARMACI SENZA RICETTA".
------------------------------------------------------------------------------------------------------------------

Roma, 26 marzo 2008

Donatella Poretti, deputata radicale e Sergio Rovasio, Segretario Associazione Radicale Certi Diritti, hanno scritto una lettera aperta al Ministro della Sanità Livia Turco:

Signora Ministro On. Livia Turco,

secondo notizie di stampa il prossimo 20 marzo sarà portato alla Conferenza Stato-Regioni il Decreto Ministeriale che permette ai farmacisti di vendere alcuni medicinali anche senza ricetta medica laddove da sempre è obbligatoria. Obiettivo del Decreto è di garantire cure anche in situazioni di emergenza, ad esempio quando il medico di base non è disponibile di notte e nei festivi. I casi previsti per tale esenzione riguardano fascie di medicinali relativi a malattie croniche, acute e per dimissione da ricovero ospedaliero. Non capiamo francamente per quale ragione non è stato incluso il farmaco meglio conosciuto come 'la pillola del giorno dopo' per il quale spesso si incontrano serie difficoltà a trovare, in orari notturni e festivi, medici disposti a fare la ricetta medica, così come documentato da diversi reportage giornalistici. Riguardo le garanzie di sicurezza per la vendita, basterebbe che i farmacisti seguissero le indicazioni previste dal Decreto ministeriale per gli altri farmaci elencati. L'alternativa sarebbe quella di un rischio evidente di ricorso all'aborto. Nella speranza venga accolta la nostra richiesta, porgiamo i nostri più cordiali saluti.

Donatella Poretti, deputata radicale

Sergio Rovasio, Segretario Associazione radicale Certi Diritti