---
title: 'Bocciata pdl anti omofo​bia in commissione giustizia'
date: Wed, 18 May 2011 15:12:43 +0000
draft: false
tags: [Politica]
---

La bocciatura del provvedimento anti omofobia in commissione giustizia ennesim oatto di ipocrisia della classe politica. Inascoltati il presidente della Repubblica, la ministra Carfagna e il presidente della Camera dei deputati. Giovanardi stia zitto sui gay, almeno oggi.

**Il 23 maggio il Parlamento discuterà il testo precedente. Certi Diritti invita tutti in piazza Montecitorio dalle ore 15 per sostenere l’approvazione del provvedimento.**

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Roma, 18 maggio 2011

La bocciatura della proposta di legge dell’On. Paola Concia in Commissione Giustizia dimostra di come la classe politica campi di ipocrisia e falsità. Non sono servite a niente le parole del Presidente della Repubblica, del MInistro Mara Carfagna e del Presidente della Camera dei deputati on. Gianfranco Fini che ieri, in occasione della giornata internazionale contro l’omofobia, avevano invitato tutti a intervenire contro la violenza omofobica anche con apposite leggi.

Sono sempre tutti pronti  a condannare gli atti di violenza contro le persone lesbiche omosessuali e transessuali e poi, non appena c’è l’ occasione di approvare un provvedimento che va nella direzione di limitare tale violenza sono subito pronti, con l’accetta, a bocciare il provvedimento.  
Il (molto) Sottosegretario Giovanardi, che un giorno si e l’altro pure, con le sue dichiarazioni sui gay a Ikea e altro, fa ridere il pianeta intero, farebbe bene a stare zitto anziché schierarsi subito tra i politici ipocriti che prima condannano se una deputata viene insultata per il suo orientamento sessuale, e poi esultano se il provvedimento anti-omofobia  viene bocciato.  Evidentemente la politica clericale, dei privilegi e del fondamentalismo sono più importanti delle leggi che dovrebbero aiutare e proteggere le persone più deboli.

Lunedì 23 maggio la Camera discuterà il testo precedente così come richiesto dal Pd.  
**L’Associazione Radicale Certi Diritti invita tutti coloro, associazioni e cittadini, che hanno a cuore il provvedimento contro l’omfobia a partecipare alla veglia che si svolgerà a Roma dal pomeriggio di lunedì 23 maggio in Piazza Montecitorio, a partire dalle ore 15, per sostenere l’approvazione del provvedimento. Daremo vita ad una non-stop di interventi e iniziative davanti alla Camera dei deputati.**