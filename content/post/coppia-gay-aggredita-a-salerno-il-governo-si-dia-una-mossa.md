---
title: 'COPPIA GAY AGGREDITA A SALERNO: IL GOVERNO SI DIA UNA MOSSA'
date: Sun, 16 Aug 2009 12:12:58 +0000
draft: false
tags: [Comunicati stampa]
---

**COPPIA GAY AGGREDITA A SALERNO: IL GOVERNO COMPRENDA CHE E’ ORA DI MUOVERSI CONTRO L’OMOFOBIA, OLTRE A VIOLARE LA DIGNITA’ UMANA ALIMENTA ANCHE I DANNI DI IMMAGINE DEL NOSTRO PAESE ALL’ESTERO.**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**

**“Quanto avvenuto oggi ad Agropoli vicino a Salerno contro un coppia gay di turisti stranieri, aggredita da un gruppo di ragazzi tra i 15 e i 20 anni, dimostra, se ancora ve ne fosse bisogno, della grave situazione in cui versa il nostro paese riguardo il grave fenomeno dell’omofobia. Il Governo farebbe bene a considerare una vera emergenza gli oltre 100 casi di violenza omofobica avvenuti nell’ultimo anno in Italia, alcuni dei quali sfociati in omicidio. Il fenomeno andrebbe trattato su diversi piani, primo fra tutti quello dell’educazione e dell’informazione. Considerare nemici da aggredire due ragazzi che si tengono per mano dimostra il livello di ignoranza e di pregiudizio ancora molto diffusi nel nostro paese. Il nostro Governo dovrebbe attivare una vera e propria task force coinvolgendo i Ministri per le Pari Opportunità, l’Educazione, degli Interni e intervenire d’urgenza con il Ministro del Turismo per dare un’immagine dell’Italia all’estero migliore e non così arretrata come invece appare e apparirà anche a seguito di questo episodio. Se fossimo in paese normale non ci sarebbe bisogno di sollecitare il Governo a fare questi primi passi, elementari e dovuti”.**