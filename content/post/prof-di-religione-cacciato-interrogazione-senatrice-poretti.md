---
title: 'Prof. di religione cacciato: interrogazione senatrice Poretti'
date: Tue, 01 Mar 2011 16:33:21 +0000
draft: false
tags: [aids, Comunicati stampa, condom, keplero, liceo, poretti, preservativi, RADICALI, RELIGIONE]
---

Roma, 1 marzo 2011  
  
Sulla vicenda del Professore di religione, licenziato dalla diocesi di Roma, perchè favorevole all'installazione della macchinetta per la distribuzione dei preservativi nel Liceo Keplero di Roma, la Senatrice Radicale Donatella Poretti ha presentato oggi una interrogazione urgente al Ministro dell'istruzione.

  
Al  Ministro dell'Istruzione

  
premesso che:

come segnalato dall'Associazione Radicale Certi Diritti, il professore di religione del Liceo scientifico Keplero di Roma, Genesio Petrucci, che dichiarò pubblicamente lo scorso mese di marzo 2010 il proprio favore a che nell'istituto fossero installati dei distributori automatici di preservativi, è il protagonista di un'intervista pubblicata sul sito Romatoday.it in data 28 febbraio 2011, nella quale dichiara: "Da settembre sono disoccupato, perché lo scorso anno ho detto pubblicamente che ero d'accordo al progetto di educazione sessuale nella mia scuola che prevedeva l'installazione di distributori di condom”;

dalla medesima intervista apprendiamo che: «“Già in precedenza - racconta - avevo avuto richiami dal Vicariato perché, da omosessuale, avevo preso parte ad alcune manifestazioni per i diritti dei gay". Dopo "otto anni ininterrotti di incarico" nel liceo della Capitale, i primi problemi di Petrucci, riferisce "risalgono al febbraio del 2010, quando, convocato a colloquio con il direttore dell'ufficio scuola del Vicariato di Roma, mi comunicarono la criticità della mia situazione, in quanto, da segnalazioni ricevute, risultava 'chiaro' il mio stato di 'omosessuale' e 'di sinistra'. Poi, a marzo del 2010 votai a favore dell'installazione di distributori di condom all'interno del liceo. Il 31 agosto, scaduto il contratto, non mi fu più rinnovato". Quando ho chiesto spiegazioni al Vicariato - conclude - mi è stato comunicato a voce che la motivazione ufficiale del mancato incarico era legata all'assenso al progetto di educazione sessuale previsto nella scuola"»;

considerato che:  
il ruolo dell'insegnamento della religione cattolica nelle scuole pubbliche italiane risulta subordinato al riconoscimento di idoneità dell'insegnante da parte dell'Ordinario ecclesiastico, e tale idoneità può da questi essere revocata ai sensi dei Cann. 804 e 805 del Codice di diritto canonico (in ordine a requisiti di eccellenza per retta dottrina, per testimonianza di vita cristiana e per abilità pedagogica);

  
per sapere:

\- se il Ministro ritenga ammissibile che norme e giudizi provenienti da uno Stato estero (Vaticano) pongano in essere una situazione di discriminazione nell'accesso al lavoro in ordine a orientamenti sessuali e politici di un dipendente dello Stato italiano;

\- se il Ministro non ritiene di dover intervenire rispetto alle decisioni prese dal Vicariato riguardo la non riconferma dell'idoneità all'insegnamento della religione cattolica del professor Petrucci;

\- se il Ministro non intenda disporre il reintegro nel ruolo di insegnante del professor Petrucci destinandolo all'insegnamento di altra materia.