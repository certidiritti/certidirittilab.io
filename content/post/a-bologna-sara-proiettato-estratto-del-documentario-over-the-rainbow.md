---
title: 'A BOLOGNA SARA'' PROIETTATO ESTRATTO DEL DOCUMENTARIO ''OVER THE RAINBOW'''
date: Tue, 10 Mar 2009 08:28:12 +0000
draft: false
tags: [Comunicati stampa]
---

Alla conclusione dei lavori del Congresso dell'Associaizone Radicale Certi Diritti, che si terrà a Bologna sabato 14 marzo dalle 9.30 alle 20, verrà proiettato in anteprima un estratto del documentario 'Over the Ranibow', storia di due donne che si amano e che desiderano un figlio.

**OVER THE RAINBOW**  
Documentario  
di Maria Martinelli e Simona Cocozza                         
prodotto da  Kamerafilm  Giusi Santoro

Conflitti, delusioni, e speranze di una coppia lesbica italiana che non si nasconde e che rivendica il desiderio della maternità, in viaggio obbligato all’estero per raggiungere i propri sogni.

Daniela e Marica sono due donne, vivono insieme da quattro anni e si amano.  
Daniela e Marica da sempre hanno scelto la visibilità e la verità sulla propria condizione senza nascondere e nascondersi. Daniela e Marica  desiderano un figlio.

Questa scelta, di Daniela e Marica è inevitabilmente una scelta difficile, sia per la quantità di problemi e riflessioni che scatena in tutte le persone che le circondano, sia per loro stesse, continuamente sottoposte a giudizio. Giudizi che le costringono ad una riflessione costante sul loro modo di essere e sulle proprie scelte e convinzioni

LA STORIA  
Il documentario racconta la vita di Daniela e Marica  nei sette mesi prima della loro partenza per compiere l’inseminazione assistita alla clinica “Nina Stork” di Copenaghen. Racconta di come i parenti, i colleghi di lavoro, gli amici e le amiche che gravitano intorno alle loro vite abbiano "reagito" a questa decisione. Racconta di come molti non l’abbiano condivisa, di come alcuni, anche se amici storici da sempre, ne siano rimasti turbati, e di come invece altri, con la riflessione e il dialogo si siano lentamente avvicinati alla loro scelta. Il documentario ci racconta, in fondo, di come si vive e ci si confronta, quando si fa una scelta  di vita così importante.  E infine ci narra  del loro viaggio a Copenaghen alla clinica, dell’inseminazione assistita e della sofferta attesa per la  “possibile maternità”.

NOTE DI REGIA  
Il tema della famiglia omosessuale, con tutte le problematiche che la circondano, è sicuramente un tema conosciuto. Ma come tutti i temi di cui si parla molto, corre il rischio di essere male approfondito. O di essere approfondito con mezzi che prescindono dal racconto visivo, come la carta stampata, il giornalismo o la semplice notizia di attualità.  
La definizione, o meglio la costruzione, di una nuovo modello di famiglia allargata, è una problematica universale alla quale in molti stanno tentando di dare una risposta. Probabilmente una risposta non codificata ma che ricerchi sempre nuove modalità adattabili alle esigenze della storia singola. Un tema universale con una forte esigenza di approfondimento. In questo progetto è evidente che persiste la tematica “ al femminile “ che si inquadra però in nuovi scenari: la problematica delle relazioni sentimentali atipiche, le dinamiche familiari, l’atteggiamento verso la maternità.

  
In Italia

ci sono oltre 100.000 bambini con genitori omosessuali

alle coppie omosessuali non è permessa l’adozione

non è permessa la fecondazione da donatore

non esiste nessuna forma di riconoscimento per il genitore non biologico che potrebbe essere legalmente separato da suo figlio