---
title: 'GIOVANARDI: PROBLEMA  DEI GAY E'' SGUAIATAGGINE... SFILEREMO DA GIOVANARDI'
date: Mon, 07 Jun 2010 20:35:34 +0000
draft: false
tags: [Comunicati stampa]
---

**ALTRA TRAGEDIA ITALIANA SCOPERTA DA GIOVANARDI: LA SGUAIATAGGINE DEI GAY (ED ETERO). AL PROSSIMO GAY PRIDE SFILEREMO MASCHERATI DA GIOVANARDI COSI’ CI SARA’ MENO SGUAIATAGGINE E TUTTO ANDRA' MEGLIO.**

_**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**_

“Certo, con i problemi della crisi che impongono una manovra da 24 miliardi, con l’aumento della criminalità mafiosa, con i quasi 2 miliardi di costi per le circa 600.000 auto blu italiane senza che nessuno muova un dito, con l’aumento dei divorzi, delle separazioni e del calo demografico, con i costi sempre più alti della politica, che con il finanziamento truffa prevede di versare soldi ai partiti per cinque anni anche se una legislatura finisce dopo un anno, con i costi della casta clericale vaticana che ammontano a circa 4 miliardi annui tra denari e privilegi, che alimenta con i suoi fulmini e le sue saette la violenza omofobica contro le persone lesbiche e gay, e ancora molto, molto altro, avevamo proprio bisogno di sentire oggi da Giovanardi che il problema in Italia oggi è la "sguaiataggine" degli omosessuali e degli eterosessuali. Non sappiamo se ridere o piangere per questo nuovo drammatico problema italiano che è la “sguaitaggine” gay (ma anche etero eh!) la cui lotta è divenuta un altro incredibile e rigoroso cavallo di battaglia del sotto(molto sotto)segretario Carlo Giovanardi. Al prossimo Gay Pride ci maschereremo tutti da Giovanardi così il gay pride apparirà cosa seria e tutto andrà meglio”.