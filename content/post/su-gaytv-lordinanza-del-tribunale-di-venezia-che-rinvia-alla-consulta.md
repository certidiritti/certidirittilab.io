---
title: 'SU GAY.TV L''ORDINANZA DEL TRIBUNALE DI VENEZIA CHE RINVIA ALLA CONSULTA'
date: Tue, 21 Apr 2009 09:47:16 +0000
draft: false
tags: [Comunicati stampa]
---

[**STORICO! Vietare il matrimonio gay è incostituzionale: ecco l'ordinanza del Tribunale**](http://www.gay.tv/ita/magazine/we_like/dettaglio.asp?chan=162&i=7076&wrd=storico!+vietare+il+matrimonio+gay+è+incostituzionale:+) di Giuliano Federico

  
Una coppia gay si era vista negare dal Comune l'iscrizione al registro delle coppie sposate. Si è rivolta al Tribunale di Venezia, grazie all'aiuto di Rete Lenford e Certi Diritti. E il Tribunale ha chiamato in causa la Corte Costituzionale che dovrà esprimersi. GAY.tv ti spiega con semplicità il testo dell'ordinanza.

Qui puoi scaricare l'ordinanza del tribuna di Venezia

[http://www.gay.tv/ita/magazine/we_like/dettaglio.asp?chan=162&i=7076&wrd=storico!+vietare+il+matrimonio+gay+è+incostituzionale](http://www.gay.tv/ita/magazine/we_like/dettaglio.asp?chan=162&i=7076&wrd=storico!+vietare+il+matrimonio+gay+è+incostituzionale):+