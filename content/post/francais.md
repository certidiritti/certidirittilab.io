---
title: 'Français'
date: Tue, 23 Jun 2009 15:27:06 +0000
draft: false
tags: [Senza categoria]
---

“CERTI DIRITTI”
===============

ASSOCIATION RADICALE
====================

Nos priorités:
--------------

… un fort engagement à la diffusion et au soutien à la campagne d'action civile pour la reconnaissance du droit au mariage aux personnes de même sexe, en augmentant le nombre de couples qui décident de soutenir l'initiative avec leur choix public et en la promouvant auprès de la communauté LGBT italienne.  
... un fort engagement à la réalisation d'une plate-forme commune à tout le mouvement LGBT uni, de  manière laïque, pour poursuivre des objectifs communs, au-delà des prises de position ou des idéaux individuels  légitimes mais forcément partiels  
L'association consacre ses efforts à la mémoire de Makwan Moloudzadeh (le garçon de 21 ans qui a été pendu en Iran, le 5 décembre 2007, accusé d'avoir eu des relations homosexuelles), et au nom de tous ceux qui ont souffert d'abus, de discriminations et de violence en raison de leur orientation sexuelle.

AFFIRMATION CIVILE: une action concrète pour un droit que personne ne peut nier aux couples homosexuels: l’opportunité de se marier.
------------------------------------------------------------------------------------------------------------------------------------

En Italie, le mariage entre personnes du même sexe n’est pas interdit, mais les homosexuels ne peuvent pas se marier.  
Aucune règle n’interdit le mariage des homosexuels, mais la mairie refuse de faire paraître  les publications pour les couples homosexuels. Un droit refusé en raison d’anciens préjugés, qui ne se justifie pas dans le droit italien, selon lequel on pourrait légitimement célébrer les mariages homosexuels.  
C’est justement ceci  le but de l’Affirmation Civile: briser les  préjugés et donner une possibilité concrète et d’avenir à l’amour entre les personnes homosexuelles qui souhaitent se marier.  
Il s’agit d’une initiative importante totalement insérée dans le cadre de la justice et du droit, et en vertu de laquelle Certi Diritti et le réseau d’avocats, Réseau Lenford, vont  suivre étape par étape, les couples de même sexe qui veulent couronner leur projet d'amour à travers la décision du juge.  
Cette initiative s'inspire aux campagnes de désobéissance civile et de lutte non violente qui dans le passé ont apporté dans les pays démocratiques, par exemple aux États-Unis contre les lois ségrégationnistes, des progrès substantiels dans le domaine des droits civils et des réformes institutionnelles, par le dépassement de lois injustes limitant les possibilités et le bonheur des gens.

AMOUR CIVIL: une proposition de Réforme du Droit de la Famille.
---------------------------------------------------------------

Certi Diritti a participé aux travaux de la Conférence permanente pour la réforme du droit de la famille, en coordonnant l'élaboration de la définition des différentes formes de famille et d’union. Il s’agit d’un projet qui s’adresse également à la classe politique italienne, dans le cadre de l’initiative  " Amour Civil ",  qui vise l’introduction de certains droits importants qui sont aujourd'hui refusés, et parmi lesquels ont une importance particulière ceux qui sont liés aux formes de la famille, à  l'adoption et aux enfants.