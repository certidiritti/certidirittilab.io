---
title: '2008-0140-Discrimination-ST14979-CO01.EN09'
date: Mon, 16 Nov 2009 13:25:11 +0000
draft: false
tags: [Senza categoria]
---

  

  

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 16 November 2009

Interinstitutional File:

2008/0140 (CNS)

14979/09

COR 1

LIMITE

SOC 622

JAI 728

MI 393

  

  

  

  

  

CORRIGENDUM TO THE OUTCOME OF PROCEEDINGS

from :

The Working Party on Social Questions

on :

22 October 2009

No. prev. doc. :

14009/09 SOC 567 JAI 645 MI 362

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

1) On p. 2, paragraph 4, replace the following words at the beginning of the first sentence:

"BG, DK, DE, LT, MT, PL and UK recalled their concerns in respect of the cost implications…"

with:

"BG, DK, DE, LT, MT, **NL,** PL and UK recalled their concerns in respect of the cost implications..."

2) On p. 4, paragraph 4, replace the following words at the beginning of the first sentence:

"CZ, ES and IT suggested reordering the text…"

with

"CZ, ES, IT **and NL** suggested reordering the text…"

3) On p. 4, paragraph 5, replace the following words at the beginning of the first sentence:

"Several delegations (CZ, EL, DK, IT, PT, AT, FI and UK)…"

with

"Several delegations (CZ, EL, DK, IT, PT, AT and UK)…"

\_\_\_\_\_\_\_\_\_\_\_\_\_