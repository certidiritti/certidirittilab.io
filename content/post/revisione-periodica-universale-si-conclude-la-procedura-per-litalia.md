---
title: 'Revisione Periodica Universale: si conclude la procedura per l''Italia.'
date: Thu, 19 Mar 2015 23:12:25 +0000
draft: false
tags: [Consiglio per i Diritti Umani delle Nazioni Unite, diritti umani, Maurizio Enrico Serra, ONU, Transnazionale, UPR]
---

[![UN-UPR-Geneva](http://www.certidiritti.org/wp-content/uploads/2014/12/UN-UPR-Geneva-300x200.jpg)](http://www.certidiritti.org/wp-content/uploads/2014/12/UN-UPR-Geneva.jpg)Il 18 marzo al Consiglio dei Diritti umani dell'ONU, Maurizio Enrico Serra, rappresentante permanente dell'Italia alle Nazioni Unite a Ginevra ha esposto la posizione del Governo italiano in merito alle raccomandazioni fatte durante la Revisione Periodica Universale e ha commentato che la Revisione Periodica Universale ha contribuito all'implementazione dei diritti umani in Italia. L'Italia ha accettato 176 su 186 raccomandazioni, tra cui tutte quelle sui diritti LGBT, in merito ai quali Maurizio Enrico Serra si è limitato a notare che nel 2014 il Ministero degli Esteri ha tenuto una conferenza.

Tra le ONG intervenute, ILGA ha dato voce a una coalizione di associazioni LGBTI italiane (Famiglie Arcobaleno, Intersezioni e Centro Risorse LGBTI) guidate dall'Associazione Radicale Certi Diritti che hanno sottolineato come le raccomandazioni fatte sui diritti LGBT non possono in alcun modo essere considerate già implementate poiché troppo resta ancora da fare, in particolare l'approvazione del matrimonio egualitario e l'allocazione di maggiori risorse alla lotta contro le discriminazioni. Anche la ONG Franciscans International è intervenuta sui diritti LGBT dicendosi incoraggiata dall'impegno dell'Italia a combattere le discriminazioni basate su orientamento sessuale e identità di genere e a includere l'orientamento sessuale tra le caratteristiche degne di protezione contro i discorsi d'odio.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: "L'Italia si è impegnata davanti al mondo a rispettare i diritti umani delle persone LGBTI e ha accettato persino una raccomandazione del Regno Unito che chiede di aprire il matrimonio civile anche alle coppie dello stesso sesso. Vedremo nelle revisione di medio termine a che punto saremo. Noi chiediamo al governo italiano fare in fretta, di attuare integralmente tutte le raccomandazioni accettate, soprattutto per quanto riguarda il matrimonio egualitario, e di destinare maggiori risorse per contrastare la discriminazione basate sull'orientamento sessuale e l'identità di genere".

Il documento presentato dalle associazioni LGBTI in [Inglese](http://www.certidiritti.org/wp-content/uploads/2015/03/Italy-Statement-HRC.pdf) e in [Italiano](http://www.certidiritti.org/wp-content/uploads/2015/03/Dichiarazione-congiunta-ILGA-Mar2015-ITA.pdf).

La posizione del Governo italiano in merito alle raccomandazioni fatte durante la Revisione Periodica Universale [1](http://www.certidiritti.org/wp-content/uploads/2015/03/Digitalizzato_20150309-1.jpg) e [2](http://www.certidiritti.org/wp-content/uploads/2015/03/Digitalizzato_20150309.jpg)

Il [video](http://webtv.un.org/meetings-events/watch/italy-upr-report-consideration-37th-meeting-28th-regular-session-human-rights-council/4118962177001) della sessione dedicata all'Italia. Dal minuto 41:00 la dichiarazione delle associazioni LGBTI.