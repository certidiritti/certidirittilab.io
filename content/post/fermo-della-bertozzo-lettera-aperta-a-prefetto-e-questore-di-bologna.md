---
title: 'FERMO DELLA  BERTOZZO: LETTERA APERTA A PREFETTO E QUESTORE DI BOLOGNA'
date: Tue, 01 Jul 2008 10:47:06 +0000
draft: false
tags: [Comunicati stampa]
---

Sul fermo di Polizia di Graziella Bertozzo, leader del movimento Facciamo Breccia, avvenuto sabato 28 giugno a Bologna a conclusione del gay pride nazionale, Sergio Rovasio, Segretario dell'Associaizone Radicale Certi Diritti, ha inviato stamane la seguente lettera aperta al Prefetto e al Questore di Bologna:  

Roma, 1 luglio 2008 

Signor Prefetto e Signor Questore di Bologna,

le esprimo a nome della nostra Associazione Certi Diritti  la preoccupazione e lo sconcerto per quanto avvenuto a conclusione del Gay Pride di Bologna, il 28 giugno scorso, all' esponente del movimento Facciamo Breccia, Graziella Bertozzo, in Piazza VIII Maggio a Bologna, a pochi metri dal palco della manifestazione.

Secondo quanto ricostruito, anche da alcune persone che hanno assistito all'episodio, le modalità del fermo di Gabriella Bertozzo sarebbero state particolarmente cruente e del tutto sproporzionate.

 

<!\-\- D(\["mb","nu003cp styleu003d"margin:0cm 0cm 0pt"u003eu003cspan styleu003d"font-size:14pt"u003eu003cfont faceu003d"Times New Roman"u003eGraziella Bertozzo è nota allu0026#39;interno delu003cspanu003e  u003c/spanu003emovimento lgbt come persona appassionata ai temi delle liberta civili e promotrice di manifestazioni e iniziative ispirate ai valori della nonviolenza e dei diritti. Ci pare quanto meno singolare che oltre al fermo di circa tre ore presso la Questura Centrale sia addirittura indagata per u0026quot;Resistenza a pubblico ufficiale e lesioni finalizzate alla resistenzau0026quot;.u003cspanu003eu003c/spanu003e u003c/fontu003eu003c/spanu003eu003c/pu003ennu003cp styleu003d"margin:0cm 0cm 0pt"u003eu003cspan styleu003d"font-size:14pt"u003eu003cfont faceu003d"Times New Roman"u003e u003c/fontu003eu003c/spanu003eu003c/pu003enu003cp styleu003d"margin:0cm 0cm 0pt"u003eu003cspan styleu003d"font-size:14pt"u003eu003cfont faceu003d"Times New Roman"u003eCi auguriamo che vogliate quanto prima approfondire e valutare quanto avvenuto per comprendere meglio la dinamica dellu0026#39;accaduto, verificando di persona come si sono realmente svolti i fatti. Peraltro, ci risulta che la sera stessa del 28 giugno, alcuni degli organizzatori del Gay Pride di Bologna, si erano recati negli uffici della Questura Centrale, accorsi per segnalare che Graziella Bertozzo è esponente di una delle organizzazioni del movimento lgbt italiano e non intendeva minimamente disturbare il decorso della manifestazione ma semplicemente raggiungere una sua collega che in quel momento stava prendendo la parola dal palco.u003c/fontu003eu003c/spanu003eu003c/pu003ennu003cdiv styleu003d"margin:0cm 0cm 0pt"u003eu003cspan styleu003d"font-size:14pt"u003eu003cfont faceu003d"Times New Roman"u003e u003c/fontu003eu003c/spanu003eu003c/divu003enu003cp styleu003d"margin:0cm 0cm 0pt"u003eu003cspan styleu003d"font-size:14pt"u003eu003cfont faceu003d"Times New Roman"u003eNel ringraziarvi per lu0026#39;attenzione, vi porgo i miei più cordiali saluti,u003c/fontu003eu003c/spanu003eu003c/pu003enu003cp styleu003d"margin:0cm 0cm 0pt"u003eu003cspan styleu003d"font-size:14pt"u003eu003cfont faceu003d"Times New Roman"u003e u003c/fontu003eu003c/spanu003eu003c/pu003enu003cp styleu003d"margin:0cm 0cm 0pt"u003eu003cspan styleu003d"font-size:14pt"u003eu003cfont faceu003d"Times New Roman"u003eu003cstrongu003eSergio Rovasiou003c/strongu003eu003c/fontu003eu003c/spanu003eu003c/pu003enu003cdiv styleu003d"margin:0cm 0cm 0pt"u003eu003cspan styleu003d"font-size:14pt"u003eu003cfont faceu003d"Times New Roman"u003eu003cstrongu003e",1\] ); //-->

Graziella Bertozzo è nota all'interno del movimento lgbt come persona appassionata ai temi delle liberta civili e promotrice di manifestazioni e iniziative ispirate ai valori della nonviolenza e dei diritti. Ci pare quanto meno singolare che oltre al fermo di circa tre ore presso la Questura Centrale sia addirittura indagata per "Resistenza a pubblico ufficiale e lesioni finalizzate alla resistenza". 

Ci auguriamo che vogliate quanto prima approfondire e valutare quanto avvenuto per comprendere meglio la dinamica dell'accaduto, verificando di persona come si sono realmente svolti i fatti. Peraltro, ci risulta che la sera stessa del 28 giugno, alcuni degli organizzatori del Gay Pride di Bologna, si erano recati negli uffici della Questura Centrale, accorsi per segnalare che Graziella Bertozzo è esponente di una delle organizzazioni del movimento lgbt italiano e non intendeva minimamente disturbare il decorso della manifestazione ma semplicemente raggiungere una sua collega che in quel momento stava prendendo la parola dal palco.

Nel ringraziarvi per l'attenzione, vi porgo i miei più cordiali saluti,

**Sergio Rovasio**

**<!\-\- D(\["mb","Segretario Associazione Radicale Certi Dirittiu003c/strongu003eu003c/fontu003eu003c/spanu003eu003c/divu003enu003cdiv styleu003d"margin:0cm 0cm 0pt"u003eu003cspan styleu003d"font-size:14pt"u003eu003cstrongu003eu003cfont faceu003d"Times New Roman"u003eu003c/fontu003eu003c/strongu003eu003c/spanu003e u003c/divu003en",0\] ); D(\["ce"\]); //-->Segretario Associazione Radicale Certi Diritti**