---
title: 'Divieto gaypride Belgrado: violate regole democratiche, segnaleremo all''Ue comportamento Serbia. Invitati gli organizzatori a Congresso Certi Diritti'
date: Sat, 01 Oct 2011 17:51:45 +0000
draft: false
tags: [Europa]
---

Divieto Gay Pride di Belgrado: pagina nera per la libertà. Il divieto delle autorità viola le più elementari regole democratiche dell'Ue e le raccomandazioni del Consiglio d'Europa di cui la Serbia fa parte.  
Gli organizzatori invitati al prossimo congresso di Milano dell'Associazione radicale Certi Diritti.

**Roma, 1 ottobre 2011**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti**

“A poche ore dallo svolgimento del Gay Pride nazionale serbo, che doveva svolgersi domani a Belgrado, le autorità  hanno vietato la manifestazione adducendo ragioni di ordine pubblico, nonostante le rassicurazioni date nelle settimane scorse agli organizzatori. Questo fatto è certamente molto grave e consente ai gruppi clerical-nazional-fascisti di averla vinta con la violenza e le minacce.

Esprimiamo al Comitato organizzatore del Gay Pride tutta la nostra solidarietà e vicinanza per questo atto di sopruso, fatto in violazione dei più elementari diritti democratici e di libertà di espressione, sosterremo i ricorsi che il comitato organizzatore ha già preannunciato alla Corte Europea dei Diritti dell'Uomo.  Segnaleremo al Parlamento Europeo quanto avvenuto a Belgrado in queste ore anche perché la Serbia è candidata a divenire membro dell’Unione Europea.

Lo stesso Consiglio d'Europa, di cui la Serbia è membro dal 2003, aveva diffuso nel giugno scorso un rapporto sul rispetto dei diritti civili e umani dei paesi membri chiedendo loro  l’applicazione di 36 raccomandazioni, tra queste quella per l’ uguaglianza di tutti i cittadini, la lotta alle discriminazioni su basi giuridiche, per la libertà di espressione, per il diritto di riunione e associazione per le persone Lgbt(e).

L’Associazione Radicale Certi Diritti nel messaggio di solidarietà inviato a Jovanka Todorovick e Goran Miletick, coordinatori del Comitato organizzatore del Gay Pride di Belgrado, ha rivolto loro l’invito a partecipare al prossimo Congresso dell’Associazione Radicale Certi Diritti che si svolgerà a Milano il 3-4 dicembre 2011”