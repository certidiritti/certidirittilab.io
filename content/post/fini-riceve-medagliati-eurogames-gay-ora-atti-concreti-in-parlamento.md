---
title: 'FINI RICEVE MEDAGLIATI EUROGAMES GAY: ORA ATTI CONCRETI IN PARLAMENTO'
date: Tue, 05 Aug 2008 12:42:21 +0000
draft: false
tags: [Comunicati stampa]
---

**BENISSIMO FINI, ORA PASSI CONCRETI IN PARLAMENTO:  
A QUANDO UN INCONTRO CON LE ASSOCIAZIONI POLITICHE DEI GAY, DELLE LESBICHE E DELLE TRANSESSUALI ITALIANE?  
**  
**Comunicato Stampa dell’Associazione radicale Certi Diritti:**

 “La buona notizia è che il Presidente della Camera ha incontrato gli atleti italiani che hanno vinto medaglie agli EUROGAMES gay e lesbici di Barcellona. Il Presidente Fini conferma di essere Presidente di tutte e di tutti, e di non avere pregiudizi nei confronti di una delle espressioni della vita sociale, culturale e ricreativa della comunità lesbica e gay italiana.  
  
 Questo incontro non può limitarsi ad un gesto simbolico, ci auguriamo sia l’inizio del superamento della distanza che separa il Parlamento italiano da quasi tutti gli altri Parlamenti europei in tema di lotta all'omofobia e di difesa dei diritti delle persone lgbt.

  
 Occorre avviare quanto prima la discussione parlamentare sulle Proposte di Legge depositate all’inizio di questa Legislatura dai parlamentari radicali del Pd insieme alla deputata Anna Paola Concia, sui temi della lotta all'omofobia, del riconoscimento delle Unioni civili e sulla riforma della legge per una maggiore tutela delle persone transessuali. Si tratta di temi non più rinviabili che coinvolgono milioni di persone che, in Italia, hanno meno diritti e minori opportunità.  
  
 Chiediamo e speriamo che il Presidente Fini riceva al più presto anche le delegazioni delle organizzazioni lesbiche, gay e transessuali perché possa sentire, dalla loro viva voce, problemi e proposte di soluzioni che valgono per tutti, non solo per una parte del paese”.