---
title: 'INTERCETTAZIONI: PROSTITUTI, VATICANO, SAN PIETRO E GENTILUOMINI DI...'
date: Thu, 04 Mar 2010 10:29:51 +0000
draft: false
tags: [Comunicati stampa]
---

**INCHIESTA PROTEZIONE CIVILE:**

**QUI DI SEGUITO ALCUNE INTERCETTAZIONI CHE SVELANO UN INTRECCIO TRA DISPONIBILITA' DI PROSTITUTI, UN COMPONENTE IL CORO DI SAN PIETRO E BALDUCCI, MEMBRO DEI "GENTILUOMINI DI SUA SANTITA'".**

**DA NON CREDERE SE NON FOSSIMO IN ITALIA**

**di Sergio Rovasio**

**Estratto da agenzie di stampa e quotidiani del 4 marzo 2010**

**Dopo la notizia di un'informativa dei carabinieri in cui si dice che “l'ingegner **Balducci, per organizzare incontri occasionali di tipo sessuale, si avvale dell'intermediazione di due soggetti, parte di una rete organizzata nella capitale” la Santa sede ha cacciato un corista della Cappella Giulia, un giovane nigeriano.****

**Ma anche Angelo Balducci sara' estromesso dall'esclusivo gruppo dei 'Gentiluomini di Sua Santita'' una e'lite al servizio del Papa. Balducci non ne fara' piu' parte: 'a questo punto, se ci sono ombre cosi' consistenti, il problema non si pone neppure', era il tema ricorrente ieri in Vaticano". Si legge sul Corriere della Sera a firma di Gian Guido Vecchi. "Un tempo si chiamavano 'Cavalieri di spada e cappa', ma faceva un po' Dumas. Ora il loro nome e' 'Gentiluomini di Sua Santita'', uno dei gruppi piu' esclusivi del mondo, fanno parte della 'Famiglia pontificia' e come tali - si legge nel Motu proprio Pontificalis Domus firmato da Paolo VI nel '68 - sono al servizio del Papa e 'si distinguono per il bene delle anime e la gloria del nome del Signore'.**

Angelo Balducci non ne fara' piu' parte, 'a questo punto, al di la' dell'inchiesta in corso, se ci sono ombre cosi' consistenti il problema non si pone neppure', sillabavano ieri pomeriggio ai piani alti del Vaticano. La reazione della Santa Sede e' stata immediata quanto l'imbarazzo creato dalla pubblicazione di un'informativa dei carabinieri in cui si dice che 'l'ingegner Balducci, per organizzare incontri occasionali di tipo sessuale, si avvale dell'intermediazione di due soggetti, che si ritiene possano far parte di una rete organizzata, operante soprattutto nella capitale, di sfruttatori o comunque favoreggiatori della prostituzione maschile'.

Uno dei due 'soggetti' che fornivano i ragazzi - con tanto di descrizione dettagliata della loro prestanza fisica, come si legge nelle intercettazioni - faceva il corista nella Cappella Giulia, o 'reverenda Cappella Musicale della Sacrosanta Basilica Papale di San Pietro': Chinedu Thomas Ehiem, nigeriano, e' stato subito cacciato dal coro e il Vaticano, fin da ieri mattina, ha fatto subito sapere che 'non si tratta di un religioso ne' di un seminarista'.

Chiaro che Oltretevere si cerchi di stendere una sorta di cordone sanitario intorno alla vicenda, ma la cosa non e' semplice. Nelle intercettazioni sui 'festini' gay, tra i giovani disponibili a pagamento, si allude anche a seminaristi ('a che ora deve ritornare in Seminario? ' , domanda Balducci a Ehiem). C'e' chi dice che il nigeriano 'cantava nel coro perchè aveva una bella voce', chi spiega che vengono fatte audizioni e 'si', quel ragazzo ‘girava in Vaticano', ma la Cappella voluta nel VI secolo da Gregorio Magno e rifondata da Giulio II nel 1513 non e' un coro parrocchiale, canta nelle cerimonie celebrate in San Pietro (in assenza del Papa) e 'anche per le audizioni bisogna essere presentati da qualcuno all'interno'.

Pure il caso di Angelo Balducci, l'ex alto funzionario della Protezione civile arrestato nell'inchiesta sugli appalti al G8, ha un solo precedente: quello del 'gentiluomo' Umberto Ortolani (...).

ECCO IL TESTO DELLE INTERCETTAZIONI:

"Angelo... Io non ti dico altro. É alto 2 metri, per 97 chili, 33 anni, completamente attivo".

Un eventuale nuovo filone di indagine che la Procura di Perugia potrebbe aprire a carico di Angelo Balducci: favoreggiamento e sfruttamento della prostituzione maschile che porta in Vaticano.

Sono 72 le pagine di intercettazioni (da ieri depositate a Perugia a disposizione delle difese), che un'informativa della sezione Anticrimine di Firenze del Ros dei carabinieri datata 5 febbraio 2010 scrive: "Nell'ambito del procedimento penale in oggetto - si legge - è emerso che l'ingegner Balducci Angelo, per organizzare incontri occasionali di tipo sessuale, si avvale dell'intermediazione di due soggetti, che si ritiene possano far parte di una rete organizzata, operante soprattutto nella capitale, di sfruttatori o comunque favoreggiatori della prostituzione maschile".

I due uomini hanno un'identità. Un tipo che si fa chiamare "Mike", ma di nome fa Chinedu Thomas Ehiem, 40 anni, nigeriano, residente a Roma e "indicato all'anagrafe come "religioso" del Vaticano che cura e organizza il "coro di san Pietro". L'atro è Lorenzo Renzi, veneto di Feltre, che di anni ne ha 33. Dall'aprile del 2008 al gennaio scorso, il contenuto delle loro conversazioni con Balducci viene annotato e trascritto perchè si trasforma rapidamente in una coazione a ripetere di appuntamenti che svelano una rete di prostituzione maschile e di potenziale ricatto.

Ehiem e Renzi sono espliciti nei dettagli con cui ragguagliano il cliente su tipo di prestazione e qualità dei ragazzi da portare agli incontri. "Angelo... Io non ti dico altro. É alto 2 metri, per 97 chili, 33 anni, completamente attivo", spiega il "religioso" a Balducci in un format che si ripete e che varia solo nelle misure. E poi e ancora, ogni due, tre giorni: "Ho una situazione di Napoli". "Ho una situazione cubana". "Ho un tedesco appena arrivato dalla Germania". "Ho due neri". "Ho il calciatore". "Ho uno dell'Abruzzo". "Ho il ballerino Rai".

e ancora..

"Ho una situazione da Napoli... non so come dirti, una cosa veramente da non perdere...quasi 32 anni, si parla di un'altezza di 1.93 per 92 chili, gran bel ragazzo", "uno un po' più alto di me, palestrato, bel tipo, completamente attivo, moro, capelli corti, è un'ottima soluzione se no non avrei insistito"; "vuoi stare con il norvegese o anche con Richard? Che mi ha detto che può anche andare via, mi ha detto... se non lo vuoi"; "c'è un amico croato che voleva vederti, se puoi trovare un'oretta, è una soluzione importante, lui è molto, molto, molto alto... in tre sarebbe difficile per lui"; "in confronto a lui io sono normodotato...Ha un fisico incredibile, si libera alle dieci, è un amico mio e fa quello che dico io".

I ragazzi che i due intermediari propongono all'attenzione di Balducci "gentiluomo di sua Santità", spesso frequentano i seminari o i collegi ecclesiastici di Roma ("Lui poi a che ora deve ritornare in Seminario?", chiede Balducci a Ehiem). Altre volte appaiono extracomunitari in cerca di permesso di soggiorno (Balducci, in un caso, promette di attivarsi con il Ministero dell'Interno). Ragazzi a cui, il 6 dicembre scorso, Renzi spiega così il lavoro: "Te li pigli pure 2000 euro. Non rompere il cazzo!! Ti servono i soldi... metti un po' di musica, tiri fuori la (...)... ti cali il Viagra lì. E via!".