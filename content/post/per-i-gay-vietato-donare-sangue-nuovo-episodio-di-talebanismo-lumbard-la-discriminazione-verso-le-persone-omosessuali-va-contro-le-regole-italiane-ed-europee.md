---
title: 'PER I GAY VIETATO DONARE SANGUE? NUOVO EPISODIO DI  TALEBANISMO LUMBARD: LA DISCRIMINAZIONE VERSO LE PERSONE OMOSESSUALI VA CONTRO LE REGOLE ITALIANE ED EUROPEE'
date: Sat, 17 Jul 2010 03:18:29 +0000
draft: false
tags: [Comunicati stampa, donazione, Gaetano Pini, GAY, Marco Cappato, Milano, sangue, Sergio Rovasio]
---

Dichiarazione di **Marco Cappato**, Segretario dell'_Associazione Luca Coscioni_ e **Sergio Rovasio**, Segretario dell'_Associazione Radicale Certi Diritti:_

Il servizio trasfusionale dell’Ospedale 'Gaetano Pini' ha dichiarato di non voler accettare la donazione di sangue da un donatore dichiaratamente gay che finora lo aveva donato almeno 20 volte. Ancora una volta assistiamo ad una prova di ignoranza nell'applicazione di regole ormai desuete e inapplicabili, che oggi sono buone solo a diffondere paura contro quelle che sono le buone pratiche mediche oltre che contro le libertà individuali.

Da oltre otto anni il donatore che si è visto respingere dalla struttura sanitaria milanese donava regolarmente il sangue. Ciò che sconcerta è che la direttiva per la quale una persona gay non può donare il sangue nella struttura del Policlinico di Milano  è ‘nuova’ e nemmeno ha tenuto conto del fatto la Direttiva della Commissione europea (Direttiva 2004/33/EC) riguardo i donatori di sangue precisa che i gruppi a rischio sono coloro che ‘hanno comportamenti sessuali a rischio’ indipendentemente dal loro orientamento sessuale così come previsto anche dal Decreto ministeriale 13.4.2005, allegato 4.

La Direttiva dice testualmente:

“Sexual behaviour. Persons whose sexual behaviour puts them at high risk of acquiring severe infectious diseases that can be transmitted by blood”. \[Comportamenti sessuali. Persone i cui comportamenti sessuali siano tali da porle a rischio di acquisire gravi malattie infettive che possano essere trasmesse per via ematica\]

Consigliamo ai rappresentanti sanitari, che sembrano ispirati da una grave forma di talebanismo di stampo lumbard, di aggiornare le direttive datate 1985 che effettivamente prevedevano il divieto della donazione del sangue alle persone omosessuali, era all’incirca 25 anni fa! Nel frattempo qualcosa è cambiato nel mondo, persino il Ministro della Salute, Francesco Storace, nel 2005, di fronte ad un caso analogo, dispose l'apertura di un'inchiesta per accertare responsabilità amministrative o comportamenti sanzionabili penalmente dopo il rifiuto del Policlinico di Milano di prelevare il sangue ad un omosessuale donatore. "Quanto accaduto al Policlinico di Milano - disse il ministro - é inaccettabile e potrebbe configurare l'esistenza di un reato".