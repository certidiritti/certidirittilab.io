---
title: 'Marco Marchese'
date: Wed, 20 Jan 2016 16:11:09 +0000
draft: false
---

[![marcomarchese_bn](http://www.certidiritti.org/wp-content/uploads/2016/01/marcomarchese_bn-300x300.jpg)](http://www.certidiritti.org/wp-content/uploads/2016/01/marcomarchese_bn.jpg) Marco Marchese, calabrese, è iscritto all’Associazione Radicale Certi Diritti fin dalla sua fondazione, sindacalista in Cgil e co-fondatore di Liberi Tv, ha fatto parte più volte del Comitato nazionale di Radicali italiani e attualmente, in Certi Diritti, si occupa di una parte della comunicazione.