---
title: 'Adotta anche tu Piccolo Uovo: Certi Diritti lancia una sottoscrizione per donare il libro a tutte le biblioteche rionali di Milano'
date: Sat, 04 Feb 2012 12:59:27 +0000
draft: false
tags: [Politica]
---

E' ora di finirla con censure e roghi di altri tempi.  
Milano, 4 febbraio 2012

Dopo i tentativi di censura e gli annunciati roghi in piazza di Piccolo Uovo, il libricino per bambini di Francesca Pardi e illustrato da Tullio F. Altan, che racconta il viaggio di un ovetto per esplorare le varie forme di famiglia esistenti e del suo incontro con due coppie omosessuali con figli oltre che con tante altre tipologie di famiglia, **l'Associazione Radicale Certi Diritti lancia una sottoscrizione per donare il libro a tutte le biblioteche rionali di Milano.**

E' ora di finirla con censure e roghi di altri tempi. Le famiglie omogenitoriali esistono già e vent'anni di studi fatti dall’American Psychoanalytic Association, dall’American Academy of Child and Adolescent Psychiatry e dall’American Academy of Pediatrics hanno dimostrato che la differenza o meno di sesso nella coppia non pregiudica minimamente lo sviluppo psicologico dei figli. Occorre allora promuovere la pari dignità di tutte le famiglie e di tutte le persone, togliendo armi all’omofobia e combattendo la discriminazione dei figli di coppie gay, come fa Piccolo Uovo.

**Difendi la libertà! Combatti le discriminazioni! Fai una donazione per sostenere l'iniziativa.  
**