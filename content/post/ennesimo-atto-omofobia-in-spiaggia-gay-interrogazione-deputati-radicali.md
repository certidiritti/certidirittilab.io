---
title: 'ENNESIMO ATTO OMOFOBIA IN SPIAGGIA GAY: INTERROGAZIONE DEPUTATI RADICALI'
date: Wed, 19 Aug 2009 12:22:41 +0000
draft: false
tags: [Comunicati stampa]
---

**ENNESIMO ATTO DI VIOLENZA OMOFOBICA OGGI PRESSO LA SPIAGGIA DI MARINA DI CAMEROTA, UNA FAMIGLIA INTERA DA UNA BARCA INSULTA E MINACCIA I FREQUENTATORI DI UNA SPIAGGIA GAY.**

**INTERROGAZIONE DEI DEPUTATI RADICALI.**

**Roma, 19 agosto 2009**

**Oggi, alle ore 12.30 circa, presso la spiaggia di Marina di Camerota, in Provincia di Salerno, si è verificato un ennesimo atto di violenza omofobica contro i  frequentatori di una spiaggia gay. Questa volta gli insulti e le minacce sono avvenute da parte di una intera famiglia da bordo di una barca. Tra i turisti presenti sulla spiaggia v’era anche Carlo Cremona, Presidente di I-Ken Onlus, l’associazione lgbt di Napoli che ha immediatamente chiesto l’intervento delle forze dell’ordine intervenute dopo quasi un’ora quando la barca era ormai andata via.**

**

I deputati radicali hanno oggi depositato  la seguente interrogazione parlamentare, prima firmataria Rita Bernardini:

INTERROGAZIONE URGENTE A RISPOSTA SCRITTA:

Al Ministro degli Interni – al Ministro per le Pari Opportunità:

Per sapere – premesso che:

Mercoledì 19 agosto 2009 presso la spiaggia di Marina di Camerota, in Provincia di Salerno, abitualmente frequentata anche da persone gay, alle ore 12.30 circa si è avvicinata al bagnasciuga un’imbarcazione creando disagio tra i turisti e i bagnanti. Alle lamentele degli stessi la famiglia che stava sull’imbarcazione, si è scagliata con insulti e minacce contro le persone gay;

un giovane componente tale famiglia, armato di un coltello, di quelli usati per la pesca subacquea, si è avvicinato con fare minaccios, insultando  le persone gay che contestavano il comportamento sopra descritto;

presso la spiaggia era anche presente il Presidente di I-Ken Onlus, Associazione Lgbt di Napoli che si batte da sempre in difesa dei diritti delle persone lesbiche, gay e transessuali, che ha immediatamente chiamato la Guardia Costiera e subito dopo la Capitaneria di Porto della località; dopo quasi un’ora è arrivato un cabinato della Guardia Costiera in cerca dell’imbarcazione che nel frattempo era andata via senza che riuscire a individuarla;

Per sapere:

- Se non ritenga il Governo che tale atto di omofobia avvenuto davanti a centinaia di persone dimostri che l’odio e il disprezzo per le persone gay è continuamente alimentato anche dall’indifferenza totale delle istituzioni locali e nazionali;

- Se non ritenga il Governo che gli atti di omofobia avvenuti recentemente in Campania, necessitino di un vero e proprio piano d’intervento che comprenda aiuto, sostengo, protezione, informazione ed educazione  da promuovere a tutti i livelli sociali e istituzionali verso la comunità lgbt;

- Per quale motivo le autorità pubbliche, in questo caso la Guardia Costiera, sono intervenute così in ritardo riguardo le prime chiamate d’intervento fatte proprio per scongiurare rischi fisici per le persone frequentatatrici della spiaggia.

I deputati radicali:

Rita Bernardini, Marco Beltrandi, Maria Antonietta Farina Coscioni, Matteo Mecacci, Maurizio Turco, Elisabetta Zamparutti

**