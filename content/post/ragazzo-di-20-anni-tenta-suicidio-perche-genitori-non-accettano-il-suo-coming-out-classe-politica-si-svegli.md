---
title: 'Ragazzo di 20 anni tenta suicidio perchè genitori non accettano il suo coming out. Classe politica si svegli!'
date: Thu, 05 Jan 2012 11:45:15 +0000
draft: false
tags: [Politica]
---

A Vicenza tentato suicidio di un giovane gay: il dramma di molti giovani vissuto nell'indifferenza della politica e nell'incapacità delle famiglie ad affrontare un tema considerato ancora un tabù in Italia.

Roma, 5 gennaio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

La notizia che un ragazzo di 20 anni, a Vicenza, ha ieri tentato il suicidio  perché i genitori non hanno accettato la sua omosessualità, appena dichiarata, dovrebbe far riflettere la classe politica  e tutto quell’esercito di ipocriti taleban-clericali che continuano a negare diritti, informazione, e assistenza sempre più necessaria nel nostro paese alle persone Lgbt(e).

Recarsi su un cavalcavia e tentare di gettarsi sulle automobili sottostanti, con l’aiuto dello stordimento dell’alcool, offre bene l’idea di come in Italia un giovane di 20 anni vive la sua condizione di omosessuale, la sua colpa è stata quella di fare ‘coming out’ (non ‘outing') con i genitori che lo hanno subito rifiutato.

Occorre anche ricordare che tra gli adolescenti una delle cause maggiori di suicidio è proprio determinata dalla scoperta dell’essere omosessuali e di vivere tale condizione in piena solitudine.

La classe politica impari da quanto dichiarato lo scorso ottobre dal Presidente degli Stati Uniti Barack Obama alla Conferenza annuale della Human Right Campaing, una delle più importanti organizzazioni americane che si battono per la promozione e la difesa dei diritti Lgbt: "nessun ragazzo gay dovrà in futuro sentirsi solo e abbandonato a sè stesso". Ecco sarebbe stato sufficiente far giungere dall’Italia un messaggio come questo al ragazzo di Vicenza.

Noi dell'Associazione Radicale Certi Diritti ci auguriamo che la classe politica italiana impari qualcosa dal Presidente Usa e abbia il coraggio di tenere alta la bandiera dei diritti civili e umani, senza timori, paure, tentennamenti ipocriti e clericali, e stare così vicini ai tanti  ragazzi di Vicenza.