---
title: 'TRANSESSUALI AGGREDITE A FIRENZE: IL GOVERNO SI MUOVA'
date: Mon, 04 May 2009 11:28:21 +0000
draft: false
tags: [Comunicati stampa]
---

SOSTEGNO E AIUTO ALLE TRANSESSUALI AGGREDITE A FIRENZE. QUANTI INNOCENTI DOVRANNO ANCORA PAGARE SULLA PROPRIA PELLE PRIMA CHE IL GOVERNO SI DECIDA A VARARE UNA LEGGE CONTRO L’OMOFOBIA E LA TRANSFOBIA?

Roma, 4 maggio 2009  
   
Dichiarazione di Sergio Rovasio, Segretario nazionale dell’Associazione Radicale Certi Diritti:

A Firenze due ragazze transessuli sono state insultate e aggredite con spranghe di ferro da un gruppo di italiani tra i 17 e i 25 anni intorno alle 4 del mattino, tra il 2 e il 3 maggio. L' ennesimo episodio avviene nell’assenza drammatica di una legge nazionale che tuteli le persone lgbt (lesbiche, gay, bisessuali, transgender) dalla discriminazione e dall’odio omofobico e transfobico. La cultura delle ronde, di destra o sinistra che siano, sta ovviamente assumendo un rilievo negativo in tutta Italia, alimentando sentimenti di pregiudizio e odio nei confronti di gay, lesbiche, transgender e delle diversità in genere. Occorre che Governo e Parlamento legiferino per consentire a tutti i cittadini lgbt del nostro Paese di vivere con maggiore serenità. L’associazione radicale Certi Diritti è vicina alle due ragazze aggredite e nel continuare la sua opera di sensibilizzazione su tutto il territorio nazionale si augura che le autorità intervengano per prevenire altri atti violenti".