---
title: 'A Roma serata contro la sessuofobia, per la libertà e i diritti'
date: Sun, 03 Apr 2011 11:34:17 +0000
draft: false
tags: [Politica]
---

Giovedì 7 aprile l’Associazione Radicale Certi Diritti, in occasione della mostra d’arte ‘Segnali di libertà’ di Alba Montori, organizza una serata alla Domus Talenti di Roma. Con libri e cortometraggi lgbt(e) in collaborazione con Buzz Intercultura, cena a buffet, interventi politici e interviste ad attori porno e prostitue/i, drag show e musica elettronica. **Il programma >**

  
  

**L’Associazione Radicale Certi Diritti presenta**  
  
**Serata contro la sessuofobia, per la  libertà e la responsabilità delle persone.**  
  
**Giovedì 7 aprile 2011 dalle ore 19,30**  
**Domus Talenti – Via Quattro Fontane, 113 – 00184 Roma**

Presentazione della serata e delle iniziative in memoria di David Kato Kisule, per l'Europride, il matrimonio gay e le altre campagne dell'associazione.  A cura di Sergio Rovasio, Segretario Ass.ne Radicale Certi Diritti e Giacomo Cellottini, Tesoriere.  
  
Presentazione del libro di Andrea Pini **'Quando eravamo froci'**. Ne discutono con l'autore: Francesco Gnerre, Docente di Lettere Università Tor Vergata, Roma;  
Riccardo Peloso, critico d’arta, poeta e gallerista.  
  
**Aperitivo e Cena a libera offerta**  
  
a seguire interventi di:  
**Rita Bernardini**, deputata radicale e Presidente di Certi Diritti;  
**Maria Antonietta Farina Coscioni**, deputata Radicale e co-presidente Ass. Coscioni  
**Rocco Berardo**, Consigliere Regionale del Lazio  
**Mario Staderini**, Segretario di Radicali Italiani  
**Leila Deianis**, Presidente Libellula – Arcitrans;  
**Filomena Gallo**, Avvocato, Vice – Segretario Associazione Luca Coscioni  
**Helena Velena e Klaus Mondrian**  
  
**Radical-Sex:**  
\- Intervista a porno attore, a una prostituta e a un ragazzo disabile sulla sessuofobia, il mercato del sesso, la libertà e il diritto di tutti alla sessualità;  
\- Intervento di **Flavio Koea** promotore di Miss Escort 2011 e **Maya Checchi**, Studios Manager.  
  
Durante la serata:

**distribuzione di preservativi**  
  
Proiezione di **Cortometraggi e spot Tv in collaborazione con Buzz Intercultura**  
  
Show di **Emilio Rez** e delle Drag Queen **Pepa e Poppea**

**Dj set con musica elettronica**

**[invita i tuoi amici su Facebook >](http://www.facebook.com/event.php?eid=182266648486491)**