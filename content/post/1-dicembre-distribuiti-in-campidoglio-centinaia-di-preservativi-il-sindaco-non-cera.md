---
title: '1 dicembre: distribuiti in Campidoglio centinaia di preservativi, il Sindaco non c''era!'
date: Thu, 02 Dec 2010 01:36:51 +0000
draft: false
tags: [Comunicati stampa]
---

**GIORNATA MONDIALE CONTRO L’AIDS: IL SINDACO DI ROMA ANNUNCIATO AL CONVEGNO SULL’AIDS IN CAMPIDOGLIO NON C’ERA. DELEGAZIONE RADICALE DISTRIBUISCE CENTINAIA DI PRESERVATIVI IN CAMPIDOGLIO E ALL’UFFICIO DEL SINDACO.**

Questa mattina una delegazione radicale composta da Riccardo Magi, Segretario di Radicali Roma, Sergio Rovasio, Segretario Associazione Radicale Certi Diritti, Alba Montori e Claudio Mori, Fondazione Massimo Consoli, si sono recati in Campidoglio per consegnare al Sindaco di Roma una pacco, contentente centinaia di preservativi.

In Campidoglio era annunciato un Convegno in occasione della Giornata Mondiale contro l’Aids ma, nonostante fosse annunciato, il Sindaco non c’era.  Sono stati così distribuiti agli studenti, presenti sulla Piazza del Campidoglio, alcune centinaia di preservativi. La delegazione radicale ha poi consegnato, come gesto simbolico, il pacco con i restanti preservativi all’ufficio di Gabinetto del Sindaco.

**Dichiarazione di Riccardo Magi e Sergio Rovasio:**

“E’ davvero incredibile che oggi, in occasione della XXIII Giornata mondiale contro l’Aids, nella città italiana dove avvengono ancora oggi il 10% dei contagi da Hiv di tutta Italia, il Sindaco inviti la popolazione a fare prevenzione con ‘i valori morali’, senza nemmeno partecipare, come annunciato, ad un evento così importante che evidentemente non era per lui così importante. Le istituzioni, come avviene in tutte le capitali e città europee, dovrebbero promuovere anche nelle scuole e nei luoghi pubblici la conoscenza e l’utilizzo del preservativo e non certo alimentare l’ignoranza e il pregiudizio  sul più efficace metodo di prevenzione e protezione dal contagio da Hiv e altre malattie sessualmente trasmissibili. Dai risultati del sondaggio appena diffuso dall’Anlaids, risulta che la maggioranza degli studenti di Roma sanno che il preservativo previene le MST ma nonostante questo non lo usano abitualmente. I veri valori morali sono quelli che prevedono responsabilità e attenzione verso il prossimo, non certo lo sbandieramento ideologico e fondamentalista della propria visione della società”.