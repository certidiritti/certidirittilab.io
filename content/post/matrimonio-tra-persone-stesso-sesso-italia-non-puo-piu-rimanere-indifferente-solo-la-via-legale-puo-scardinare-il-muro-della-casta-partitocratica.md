---
title: 'Matrimonio tra persone stesso sesso: Italia non può più rimanere indifferente, solo la via legale può scardinare il muro della casta partitocratica'
date: Thu, 10 May 2012 11:37:33 +0000
draft: false
tags: [Matrimonio egualitario]
---

Gli esponenti di destra, sinistra, sopra e sotto, impediranno di fare anche qui il grande passo per il superamento delle diseguaglianze. Al momento solo la via legale, intrapresa con la campagna di Affermazione Civile, può scardinare il muro della casta partitocratica e dei 'nuovi' che avanzano senza nemmeno sapere cosa sono i diritti civili.

Roma, 10 maggio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Quasi tutti i paesi democratici si avviano ad approvare leggi sul riconoscimento di diritti per le coppie dello stesso sesso e per il superamento delle diseguaglianze di cui sono vittime ancora oggi centinaia di migliaia/milioni di persone. Con la vittoria di Hollande in Francia e con le ultime dichiarazioni del Presidente degli Stati Uniti in favore del matrimonio tra persone dello stesso sesso, un altro grande passo avanti è stato fatto sulla via della giustizia e dell’uguaglianza tra le persone, indipendentemente dal loro orientamento sessuale, per vivere con felicità la propria vita, senza più discriminazioni, esclusioni, violenza e ingiustizia, che lasciano la chiesa cattolica e i paesi dittatoriali sempre più soli e isolati.

In Italia il fronte della conquista dei Diritti Civili,  vive una situazione  ancora molto difficile e tutti sappiamo di come  la casta partitocratica o i 'nuovi che avanzano' affrontano la questione. Il matrimonio tra persone dello stesso sesso viene spesso scimmiottato come espressione denigratoria per attaccare il nemico, sia esso di destra o di sinistra, sopra o sotto, senza che questo sia considerato un traguardo di civiltà e di uguaglianza. Ecco la grande differenza tra la classe politica francese, olandese, belga, americana, spagnola, tanto per fare degli esempi, e l’Italia.

Evidentemente le due principali caste, quella vaticana e quella partitocratica, che si attorcigliano sui loro privilegi con capitali di denaro pubblico, esenzioni di tasse e imposte, ori, auto blu, cerimonie, palazzi, scandali sessuali, comportamenti ipocriti e antidemocratici, fanno e faranno di tutto per scongiurare una evoluzione democratica, civile, trasparente, che consenta ai cittadini di vivere al meglio la loro vita, senza intrusioni e imposizioni da parte di chicchessia.

A noi, al momento, come ci disse Pasolini, non resta che “continuare imperterriti, ostinati, eternamente contrari, a pretendere, a volere, a identificarci col diverso; a scandalizzare; a bestemmiare” contro queste ingiustizie e contro queste caste clerical-partitocratiche con, innanzitutto, la campagna di Affermazione Civile, accompagnando e sostenendo le coppie gay e lesbiche in ‘cause pilota’ perché, attraverso esse, si possano ottenere specifici diritti (come le sentenze della corte costituzionale del 2010, della Corte di Cassazione e del Tribunale di Reggio Emilia del 2012  ben riaffermano).

**[LA CAMPAGNA DI AFFERMAZIONE CIVILE >>>](affermazione-civile)**

**[SOSTIENI LA CAMPAGNA PER IL RICONOSCIMENTO DEL MATRIMONIO TRA PERSONE DELLO STESSO SESSO IN ITALIA >](iscriviti)  
**