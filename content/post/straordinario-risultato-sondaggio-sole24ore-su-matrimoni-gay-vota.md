---
title: 'STRAORDINARIO RISULTATO SONDAGGIO SOLE24ORE SU MATRIMONI GAY: VOTA'
date: Mon, 11 May 2009 10:35:42 +0000
draft: false
tags: [Comunicati stampa]
---

STRORDINARIO RISULTATO DEL SONDAGGIO DEL SOLE 24 ORE ONLINE: LA STRAGRANDE MAGGIORANZA RITIENE INGIUSTO VIETARE LE NOZZE GAY E LA MANCANZA DI UNA LEGGE SULLE UNIONI CIVILI.  
   
Roma, 11 maggio 2009  
   
Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti e candidato della Lista Bonino/Pannella nella Circ.ne Centro:  
 

“Il grave ritardo dell’Italia in materia di leggi sulle unioni civili e matrimonio gay pesa nell’opinione pubblica,  continuare a ignorare il problema che aggrava le condizioni di vita delle coppie lesbiche e gay denota un livello ipocrita di quasi tutta la nostra classe politica.  
Il sondaggio del Sole 24 Ore ci offre un risultato sorprendente e straordinario. Finora i risultati sono che il 60% dei votanti ritiene ingiusto vietare le nozze tra persone dello stesso sesso e il 67% ritiene ingiusto che non siano state introdette figure giuridiche ad hoc come i Dico o i Pacs che aumentano le tutele per le unioni di fatto anche tra persone di sesso diverso. Insomma, ecco un’altra dimostrazione di come aumenta sempre più il divario tra bisogni di una società laica e la casta politica ipocrita e genuflessa sempre più al Vaticano”.  
   
Di seguito il link al sondaggio (dove si può votare) e i risultati:  
[http://www.ilsole24ore.com/fc?cmd=sondaggio&chId=30&sezId=8720&id_sondaggio=6849](http://www.ilsole24ore.com/fc?cmd=sondaggio&chId=30&sezId=8720&id_sondaggio=6849)  
   
Risultati di:  
NOZZE GAY SI O NO?  
   
In Italia sono vietate le nozze tra persone dello stesso sesso. E'giusto?  
      
 Si  
 40%  
 No  
 60%  
   
   
In Italia non sono state introdotte figure giuridiche ad hoc come i DICO o i Pacs che aumentano le tutele per le unioni di fatto anche tra persone di sesso diverso. Secondo voi è giusto?  
      
 Si  
 33%  
 No  
 67%