---
title: 'A CATANIA CERTI DIRITTI CERCA COPPIE GAY E VIENE DENUNCIATA PER OSCENITA'''
date: Tue, 30 Jun 2009 13:26:59 +0000
draft: false
tags: [Comunicati stampa]
---

CATANIA: STAND DI CERTI DIRITTI MESSO SOTTO OSSERVAZIONE DAI CARABINIERI PER OSCENITA’, C’ERA SCRITTO “CERCASI COPPIE GAY PER MATRIMONIO”.  FORSE QUALCUNO VA DENUNCIATO PER DIFFAMAZIONE E PROCURATO ALLARME, O NO?

_**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:**_

“Ieri pomeriggio lunedì 29 giugno,  a Catania, nella zona centrale davanti Villa Bellini, i Carabinieri sono intervenuti in forze presso lo stand dell’Associazione Radicale Certi Diritti per verificare se effettivamente si manifestavano turbamenti o atti osceni, così come segnalato da un cittadino. Allo stand dell’Associazione Radicale Certi Diritti, allestito anche per promuovere il Gay Pride Sicilia che si svolgerà sabato 4 luglio, venivano semplicemente distribuiti volantini informativi della campagna di Affermazione Civile per il matrimonio gay, con sopra scritto: “cercasi coppie lesbiche e gay a scopo matrimonio”.

I Carabinieri, intervenuti in forze, con dispiegamento di mezzi e uomini, hanno ricevuto anche loro la documentazione dell’Associazione e ci risulta che siano in corso delle verifiche e indagini. L’Associazione Radicale Certi Diritti chiede alle autorità l’immediata verifica della regolarità delle procedure attivate dai Carabienieri, a partire dall’identificazione della persona che ha fatto la denuncia che, evidentemente, dovrà essere lei denunciata per calunnia e procurato allarme”.