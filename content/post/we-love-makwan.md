---
title: 'We love Makwan'
date: Fri, 09 May 2008 09:38:48 +0000
draft: false
tags: [abusi, GAY, IRAN, makwan, Moloudzadeh, Senza categoria]
---

L'Associazione radicale Certi Diritti dedica le sue iniziative alla memoria di **Makwan Moloudzadeh**, ragazzo iraniano di 21 anni impiccato il 5 dicembre 2007 perché accusato di aver commesso atti omosessuali, e a coloro che hanno sofferto abusi, discriminazioni e violenze a causa del proprio orientamento sessuale.