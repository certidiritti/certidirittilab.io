---
title: '2008-0140-Discrimination-ST11900.EN10'
date: Fri, 16 Jul 2010 15:16:25 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 16 July 2010

Interinstitutional File:

2008/0140 (CNS)

11900/10

LIMITE

SOC 450

JAI 610

MI 231

  

  

  

  

  

OUTCOME OF PROCEEDINGS

from :

The Working Party on Social Questions

No. prev. doc. :

10511/2/10 REV 2 SOC 397 JAI 504 MI 189

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

-    Annotated consolidated text

**I.         INTRODUCTION**

Delegations will find attached a footnoted version of the consolidated text previously distributed as doc. 10511/2/10 REV 2.

This document provides an overview of the discussions that have taken place in the Working Party on Social Questions, up to and including the meeting on 3 May 2010[\[1\]](#_ftn1).

Delegations' general positions and the major outstanding issues are briefly summarised below, and further information is contained in the footnotes to the text.

**II.        DELEGATIONS' GENERAL POSITIONS**

A large majority of delegations (BE, BG, DK, EE, EL, ES, FR, IE, LU, HU, MT, NL, AT, PL, PT, SI, SK, FI, SE, UK) have welcomed the proposal in principle, many endorsing the fact that it aims to complete the existing legal framework by addressing all four grounds of discrimination through a horizontal approach.

Most delegations have affirmed the importance of promoting equal treatment as a shared social value within the EU. In particular, several delegations have underlined the significance of the proposal in the context of the UN Convention on the Rights of Persons with Disabilities (UNCRPD)[\[2\]](#_ftn2). However, some delegations (ES, AT, PT, SI) would have preferred more ambitious provisions in regard to disability.

DE has maintained a general reservation and expressed various concerns[\[3\]](#_ftn3), including in respect of the legal basis and subsidiarity, the need for a thorough impact assessment and cost-benefit analysis, the burden that the proposed measures would impose on businesses (especially SMEs), the lack of legal certainty, and the ongoing infringement proceedings stemming from existing anti-discrimination legislation. LT has also maintained a general reservation, citing concerns over the lack of legal certainty and the need for a thorough impact assessment, and expressing the view that the proposal conflicts with the principles subsidiarity and proportionality. These delegations have questioned the timeliness of and the need for the proposal, which they see as infringing on national competence for certain issues.

CZ, MT and IT, similarly, have expressed doubts regarding the need for the proposal, as they believe that it encroaches on national competences and conflicts with the principles of subsidiarity and proportionality.

NL has stated that it supports the proposal provided that solutions can be found to its concerns, particularly in respect of the financial implications and the need for legal certainty. IE and IT have also seen a need for deeper impact assessments.

  

**III.      MAJOR OUSTANDING ISSUES AND STATE OF PLAY**

The Working Party has recognised the need for extensive further discussion, with a view to resolving the numerous outstanding questions, which include the following:

1) The scope, the division of competences and the issue of subsidiarity; areas where clarification is required include housing, transportation, information and communication technology (ICT), education, social security, transportation and the physical/built environment.

2) The financial and practical implications of the provisions, especially those concerning disability;

3) The implementation calendar; and

4) The need to ensure legal certainty in the Directive as a whole.

For the time being, all delegations have therefore maintained general scrutiny reservations on the proposal. CZ, DK, FR, MT and UK have maintained parliamentary scrutiny reservations, CY and PL maintaining linguistic scrutiny reservations. The Commission has meanwhile affirmed its original proposal at this stage and has maintained a scrutiny reservation on any changes thereto.

Following the entry into force of the Lisbon Treaty on 1 December 2009, the proposal now falls under Article 19 of the Treaty on the Functioning of the European Union; thus unanimity in the Council is required, following the _consent_ of the European Parliament[\[4\]](#_ftn4).

**IV.      CONCLUSION**

The Belgian Presidency has announced that the next meeting, scheduled for 16 July 2010, will focus on the provisions concerning _financial services_.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

Proposal for a

COUNCIL DIRECTIVE

on implementing the principle[\[5\]](#_ftn5) of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

THE COUNCIL OF THE EUROPEAN UNION,

Having regard to the Treaty on the Functioning of the European Union, and in particular Article 19(1) thereof,

Having regard to the proposal from the Commission[\[6\]](#_ftn6),

Having regard to the consent of the European Parliament[\[7\]](#_ftn7),

Whereas:

(1)          [\[8\]](#_ftn8)In accordance with Article 2 of the Treaty on European Union, the European Union is founded on the values of respect for human dignity, freedom, democracy, equality, the rule of law and respect for human rights, including the rights of persons belonging to minorities, values which are common to all the Member States. In accordance with Article 6 of the Treaty on European Union, the European Union recognises the rights, freedoms and principles set out in the Charter of Fundamental Rights of the European Union, and fundamental rights, as guaranteed by the European Convention on the Protection of Human Rights and Fundamental Freedoms and as they result from the constitutional traditions common to the Member States, shall constitute general principles of the Union's law.

  

(2)          The right to equality before the law and protection against discrimination for all persons constitutes a universal right recognised by the Universal Declaration of Human Rights, the United Nations Convention on the Elimination of all forms of Discrimination Against Women, the International Convention on the Elimination of all forms of Racial Discrimination, the United Nations Covenants on Civil and Political Rights and on Economic, Social and Cultural Rights, the UN Convention on the Rights of Persons with Disabilities, the European Convention for the Protection of Human Rights and Fundamental Freedoms and the European Social Charter, to which \[all\] Member States are signatories. In particular, the UN Convention on the Rights of Persons with Disabilities includes the denial of reasonable accommodation in its definition of discrimination.

(3)          This Directive respects the fundamental rights and observes the fundamental principles recognised in particular by the Charter of Fundamental Rights of the European Union. Article 10 of the Charter recognises the right to freedom of thought, conscience and religion; Article 21 prohibits discrimination, including on grounds of religion or belief, disability, age or sexual orientation; and Article 26 acknowledges the right of persons with disabilities to benefit from measures designed to ensure their independence.

(4)          The European Years of Persons with Disabilities in 2003, of Equal Opportunities for All in 2007, and of Intercultural Dialogue in 2008 have highlighted the persistence of discrimination but also the benefits of diversity.

(5)          The European Council, in Brussels on 14 December 2007, invited Member States to strengthen efforts to prevent and combat discrimination inside and outside the labour market[\[9\]](#_ftn9).

(6)          The European Parliament has called for the extension of the protection of discrimination in European Union law[\[10\]](#_ftn10).

  

(7)          The European Commission has affirmed in its Communication ‘Renewed social agenda: Opportunities, access and solidarity in 21st century Europe’[\[11\]](#_ftn11) that, in societies where each individual is regarded as being of equal worth, no artificial barriers or discrimination of any kind should hold people back in exploiting these opportunities. Discrimination based on religion or belief, disability, age or sexual orientation may undermine the achievement of the objectives of the EC Treaty, in particular the attainment of a high level of employment and of social protection, the raising of the standard of living, and quality of life, economic and social cohesion and solidarity. It may also undermine the objective of abolishing of obstacles to the free movement of persons, goods and services between Member States.

(8)          Existing European Union legislation includes three legal instruments[\[12\]](#_ftn12) adopted on the basis of Article 13(1) of the EC Treaty**,** which aim to prevent and combat discrimination on grounds of sex, racial and ethnic origin, religion or belief, disability, age and sexual orientation. These instruments have demonstrated the value of legislation in the fight against discrimination_._ In particular, Directive 2000/78/EC establishes a general framework for equal treatment in employment and occupation on the grounds of religion or belief, disability, age and sexual orientation. However, the degree and the form of protection against discrimination on these grounds beyond the areas of employment varies between the different Member States.

(9)     Therefore, EU legislation should prohibit discrimination based on religion or belief, disability, age or sexual orientation in a range of areas outside the labour market, including social protection, education and access to and supply of goods and services, including housing. Services should be taken to be those within the meaning of Article 57 of the Treaty on the Functioning of the European Union.

(10)   Directive 2000/78/EC prohibits discrimination in access to vocational training; it is necessary to complete this protection by extending the prohibition of discrimination to education which is not considered vocational training.

(11)

  

(12)   Discrimination is understood to include direct and indirect discrimination, harassment, instructions to discriminate and denial of reasonable accommodation  to persons with disabilities. Discrimination within the meaning of this Directive includes direct discrimination or harassment based on assumptions[\[13\]](#_ftn13) about a person's religion or belief, disability, age or sexual orientation.

(12a) In accordance with the judgment of the Court of Justice in Case C-303/06[\[14\]](#_ftn14), it is appropriate to provide explicitly for protection against discrimination by association on all grounds[\[15\]](#_ftn15) covered by this Directive. Such discrimination occurs, _inter alia_, when a person is treated less favourably, or harassed, because, in the view of the discriminator, he or she is associated with persons of a particular religion or belief, disability, age or sexual orientation, for instance through his or her family, friendships, employment or occupation[\[16\]](#_ftn16).

(12b) Harassment is contrary to the principle of equal treatment, since victims of harassment cannot enjoy, on an equal basis with others, access to social protection, education and goods and services. Harassment can take different forms, including unwanted verbal, physical, or other non-verbal conduct. Such conduct may be deemed harassment in the meaning of this Directive when it is either repeated or otherwise so serious in nature that it has the purpose or effect of violating the dignity of a person and of creating an intimidating, hostile, degrading, humiliating or offensive environment[\[17\]](#_ftn17).

(13)   In implementing the principle of equal treatment irrespective of religion or belief, disability, age or sexual orientation, the European Union should, in accordance with Article 8 of the Treaty on the Functioning of the European Union, aim to eliminate inequalities, and to promote equality between men and women, especially since women are often the victims of multiple discrimination.

  

(14)   The appreciation of the facts from which it may be presumed that there has been direct or indirect discrimination should remain a matter for the national judicial or other competent bodies in accordance with rules of national law or practice. Such rules may provide, in particular, for indirect discrimination to be established by any means including on the basis of statistical evidence.

(14a)[\[18\]](#_ftn18) Differences in treatment in connection with age may be permitted under certain circumstances if they are objectively justified by a legitimate aim and the means of achieving that aim are appropriate and necessary. In this context, differences of treatment under national regulations fixing age limits, or providing for more favourable conditions of access for persons of a given age, in order to promote their economic, cultural or social integration, for example in connection with the supply of goods and services specifically designed for certain age groups, enjoy a presumption of not being discriminatory.

(15)[\[19\]](#_ftn19) Actuarial and risk factors related to disability and to age are used in the provision of insurance, banking and other financial services. These should not be regarded as constituting discrimination where service providers are able to show[\[20\]](#_ftn20) by relevant actuarial principles, accurate statistical data or medical knowledge, that  such factors are determining factors for the assessment of risk.

(16)

(17)   While prohibiting discrimination, it is important to respect other fundamental rights and freedoms, including the protection of private and family life and transactions carried out in that context, the freedom of religion, the freedom of association, the freedom of expression, the freedom of the press[\[21\]](#_ftn21) and the freedom of information.

  

(17a) This Directive covers the application of the principle of equal treatment in the fields of social protection[\[22\]](#_ftn22), education[\[23\]](#_ftn23) and access to[\[24\]](#_ftn24) goods and services within the limits of the competences of the European Union.

The Member States are responsible for the organisation and content of systems of social protection, health care and education, as well as for the definition of who is entitled to receive social protection benefits, medical treatment and education.

(17b)[\[25\]](#_ftn25) Social protection includes social security[\[26\]](#_ftn26), social assistance, social housing and health care. Consequently, this Directive applies with regard to rights and benefits which are derived from general or special social security, social assistance and healthcare schemes, which are  statutory or provided either directly by the State[\[27\]](#_ftn27), or by private parties in so far as the

provision of those benefits by the latter is funded by the State. In this context, the Directive applies with regard to benefits  in cash, benefits in kind and services, irrespective of whether the schemes involved are contributory or non-contributory. The abovementioned schemes include, for example, \[access to\][\[28\]](#_ftn28) the branches of social security defined by Regulation 883/2004/EC on the coordination of social security systems[\[29\]](#_ftn29), as well as schemes providing for benefits or services granted for reasons related to the lack of financial resources or risk of social exclusion[\[30\]](#_ftn30).

  

(17c)[\[31\]](#_ftn31)

(17d) All individuals enjoy the freedom to contract, including the freedom to choose a contractual partner for a transaction. This Directive should not apply to economic transactions undertaken by individuals for whom these transactions do not constitute a professional or commercial activity.[\[32\]](#_ftn32)

In particular, this Directive does not apply to transactions related to housing which are performed by natural persons, when the activities in question do not constitute a professional or commercial activity.

In this context, the concept of professional or commercial activity may be defined in accordance with the national laws and practice of the Member States.

(17e) This Directive does not \[alter the division of competences between the European Union and the Member States\][\[33\]](#_ftn33) in the areas of education and social protection. It is also without prejudice to the essential role and wide discretion of the Member States in providing, commissioning and organising services of general economic interest.

(17f)  The exclusive competence of Member States with regard to the organisation of their social protection systems includes decisions on the setting up, financing and management of such systems and related institutions as well as on the substance and delivery of benefits and health services and the conditions of eligibility. In particular Member States retain the possibility to reserve certain benefits or services to certain age groups or persons with disabilities. The Member States also retain their competences in respect of the definition and organisation of their social housing services, including the management or allocation of such services and determining the conditions of eligibility[\[34\]](#_ftn34).

  

Moreover, this Directive is without prejudice to the powers of the Member States to organise their social protection systems in such a way as to guarantee their sustainability[\[35\]](#_ftn35).

(17g) The exclusive competence of Member States with regard to the organisation of their educational systems and the content of teaching and of educational activities[\[36\]](#_ftn36), including the provision of special needs education, includes the setting up and management of educational institutions, the development of curricula and other educational activities and the definition of examination processes. In particular Member States retain the possibility to set age limits for certain education activities. However, there may be no discrimination in the access to educational activities, including the admission to and participation in classes or programmes and the evaluation of students' performance[\[37\]](#_ftn37).

(17h) This Directive does not apply to matters covered by family law including marital status and adoption, and the legal benefits[\[38\]](#_ftn38) dependent thereon, and to laws on reproductive rights. It is also without prejudice to the secular nature of the State, state institutions or bodies, or education.

(18)

  

(19a)[\[39\]](#_ftn39)Persons with disabilities include those who have long‑term physical, mental, intellectual or sensory impairments which, in interaction with various barriers, may hinder their full and effective participation in society on an equal basis with others.

(19b) Measures to ensure accessibility for persons with disabilities, on an equal basis with others, to the areas covered by this Directive play an important part in ensuring full equality in practice. Such measures should comprise the identification and elimination of obstacles and barriers to accessibility, as well as the prevention of new obstacles and barriers. They should not impose a disproportionate burden[\[40\]](#_ftn40).

(19c)    Such measures should aim at achieving accessibility including with regard to, _inter alia[\[41\]](#_ftn41)_, the physical environment, transportation, information and communication technology and systems, and services, within the scope of the Directive as defined in Article 3. The fact that access might not always be possible to achieve in full equality with others may not be presented as a justification for not adopting all measures to increase as far as possible accessibility to persons with disabilities[\[42\]](#_ftn42).

(19d) Improvement of accessibility can be provided by a variety of means, including application of the "universal design" principle. According to the United Nations Convention on the Rights of Persons with Disabilities, “universal design” means the design of products, environments, programmes and services to be usable by all people, to the greatest possible extent, without the need for adaptation or specialised design. “Universal design” should not exclude assistive devices for particular groups of persons with disabilities where this is needed.[\[43\]](#_ftn43)

  

(20)   Legal requirements[\[44\]](#_ftn44) and standards on accessibility have been established at European level in some areas while Article 16 of Council Regulation 1083/2006 of 11 July 2006 on the European Regional Development Fund, the European Social Fund and the Cohesion Fund and repealing Regulation (EC) No 1260/1999[\[45\]](#_ftn45) requires that accessibility for disabled persons is one of the criteria to be observed in defining operations co-financed by the Funds. The Council has also emphasised the need for measures to secure the accessibility of cultural infrastructure and cultural activities for people with disabilities[\[46\]](#_ftn46).

(20a) In addition to general measures to ensure accessibility, individual measures to provide reasonable accommodation play an important part in ensuring full equality in practice for persons with disabilities to the areas covered by this Directive.

(20b) In assessing whether measures to ensure accessibility or reasonable accommodation would impose a disproportionate burden, account should be taken of a number of factors including**,** _inter alia_, the size and resources[\[47\]](#_ftn47) of the organisation or enterprise, as well as the estimated costs of such measures. A disproportionate burden would arise, for example, where significant structural changes would be required in order to provide access to movable or immovable property which is protected under national rules on account of their historical, cultural, artistic or architectural value.

(20c)[\[48\]](#_ftn48) The principle of accessibility is established in the United Nations Convention on the Rights of Persons with Disabilities. The principles of reasonable accommodation and disproportionate burden are established in Directive 2000/78/EC[\[49\]](#_ftn49) and the United Nations Convention on the Rights of Persons with Disabilities.

  

(21)   The prohibition of discrimination should be without prejudice to the maintenance or adoption by Member States of measures intended to prevent or compensate for disadvantages suffered by a group of persons of a particular religion or belief, disability, age or sexual orientation. Such measures may permit organisations of persons of a particular religion or belief, disability, age or sexual orientation where their main object is the promotion of the special needs of those persons.

(22)   This Directive lays down minimum requirements, thus giving the Member States the option of introducing or maintaining more favourable provisions. The implementation of this Directive should not serve to justify any regression in relation to the situation which already prevails in each Member State.

(23)   Persons who have been subject to discrimination based on religion or belief, disability, age or sexual orientation should have adequate means of legal protection. To provide a more effective level of protection, associations, organisations and other legal entities should be empowered to engage in proceedings, including on behalf of or in support of any victim, without prejudice to national rules of procedure concerning representation and defence before the courts.

(24)   The rules on the burden of proof must be adapted when there is a _prima facie_ case of discrimination and, for the principle of equal treatment to be applied effectively, the burden of proof must shift back to the respondent when evidence of such discrimination is brought. However, it is not for the respondent to prove that the plaintiff adheres to a particular religion or belief, has a particular disability, is of a particular age or has a particular sexual orientation.

(25)   The effective implementation of the principle of equal treatment requires adequate judicial protection against victimisation.

  

(26)   In its resolution on the Follow-up of the European Year of Equal Opportunities for All (2007), the Council called for the full association of civil society, including organisations representing people at risk of discrimination, the social partners and stakeholders in the design of policies and programmes aimed at preventing discrimination and promoting equality and equal opportunities, both at European and national levels.

(27)   Experience in applying Directives 2000/43/EC[\[50\]](#_ftn50) and 2004/113/EC[\[51\]](#_ftn51) show that protection against discrimination on the grounds covered by this Directive would be strengthened by the existence of a body or bodies in each Member State, with competence to analyse the problems involved, to study possible solutions and to provide concrete assistance for the victims.

(28)

(29)   Member States should provide for effective, proportionate and dissuasive sanctions in case of breaches of the obligations under this Directive.

(30)   In accordance with the principles of subsidiarity and proportionality as set out in Article 5 of the Treaty on European Union, the objective of this Directive, namely ensuring a common level of protection against discrimination in all the Member States, cannot be sufficiently achieved by the Member States and can therefore, by reason of the scale and impact of the proposed action, be better achieved by the European Union. This Directive does not go beyond[\[52\]](#_ftn52) what is necessary in order to achieve those objectives.

(31)   In accordance with paragraph 34 of the interinstitutional agreement on better law-making, Member States are encouraged to draw up, for themselves and in the interest of the European Union, their own tables, which will, as far as possible, illustrate the correlation between the Directive and the transposition measures and to make them public.

HAS ADOPTED THIS DIRECTIVE:

CHAPTER I  
GENERAL PROVISIONS

Article 1  
Purpose[\[53\]](#_ftn53)

This Directive lays down a framework for combating discrimination on the grounds of[\[54\]](#_ftn54) religion or belief, disability, age, or sexual orientation, with a view to putting into effect in the Member States the principle[\[55\]](#_ftn55) of equal treatment within the scope of Article 3.

Article 2[\[56\]](#_ftn56)  
Concept of discrimination

1.       For the purposes of this Directive, the “principle[\[57\]](#_ftn57) of equal treatment” shall mean that  there shall be no discrimination  on any of the grounds referred to in Article 1.

For the purposes of this Directive, discrimination means:

(a)     direct discrimination;

(b)     indirect discrimination;

(c)     harassment[\[58\]](#_ftn58);

  

(d)     instruction to discriminate against persons on any of the grounds referred to in Article 1[\[59\]](#_ftn59);

(e)     denial of reasonable accommodation for persons with disabilities[\[60\]](#_ftn60);

(f)      direct discrimination or harassment by association[\[61\]](#_ftn61).

2.       For the purposes of paragraph 1, the following definitions apply:

(a)     direct discrimination shall be taken to occur where one person is treated less favourably than another is, has been or would be treated in a comparable situation, on any of the grounds referred to in Article 1;

(b)[\[62\]](#_ftn62) indirect discrimination shall be taken to occur where an apparently neutral provision, criterion or practice would put persons of a particular religion or belief, a particular disability, a particular age, or a particular sexual orientation at a particular disadvantage compared with other persons, unless that provision, criterion or practice is objectively justified by a legitimate aim and the means of achieving that aim are appropriate and necessary;

  

(c)     harassment shall be taken to occur where unwanted conduct related to any of the grounds referred to in Article 1 takes place with the purpose or effect of violating the dignity of a person and of creating an intimidating, hostile, degrading, humiliating or offensive environment. In this context, the concept of harassment may be defined in accordance with the national laws and practice of the Member States[\[63\]](#_ftn63);

(d)     denial of reasonable accommodation for persons with disabilities shall be taken to occur where there is a failure to comply with Article 4a of the present Directive;

(e)     direct discrimination or harassment by association shall be taken to occur where a person is discriminated against or harassed due to his or her association with persons of a certain religion or belief, with a disability, of a given age, or of a certain sexual orientation[\[64\]](#_ftn64).

3.

4.

5.

6.  Notwithstanding paragraph 2, differences of treatment on grounds of age[\[65\]](#_ftn65) shall not constitute discrimination, if they are objectively[\[66\]](#_ftn66) justified by a legitimate aim, and if the means of achieving that aim are appropriate and necessary.

  

\[In this context,\][\[67\]](#_ftn67) differences of treatment  under national regulations[\[68\]](#_ftn68)  fixing a specific age for access to social protection, including social security, social assistance and healthcare; education; and certain goods or services which are available to the public,  or providing for more favourable conditions of access for persons of a given age, in order to promote their economic, cultural or social integration,  are presumed to be non-discriminatory[\[69\]](#_ftn69).

7[\[70\]](#_ftn70).    a)       Notwithstanding paragraph 2, in the provision of financial services, the Member States may[\[71\]](#_ftn71) provide that proportionate differences in treatment on the grounds of age shall not be considered discrimination for the purposes of this Directive, if age is a determining factor in the assessment of risk for the service in question and this assessment is \[based on relevant actuarial[\[72\]](#_ftn72) principles and accurate statistical data, or, in the absence of the latter, on relevant medical knowledge\][\[73\]](#_ftn73).

  

b)      Notwithstanding paragraph 2, in the provision of financial services[\[74\]](#_ftn74), the Member States may[\[75\]](#_ftn75) provide that proportionate differences in treatment on the grounds of disability[\[76\]](#_ftn76) shall not be considered discrimination for the purposes of this Directive, if the state of health resulting from a disability of the person concerned is a determining factor in the assessment of risk for the service in question and this assessment is based on relevant actuarial principles, accurate statistical data or medical knowledge.

8.       This Directive shall be without prejudice to measures laid down in national law which, in a democratic society, are necessary for public security, for the maintenance of public order and the prevention of criminal offences, for the protection of health and safety[\[77\]](#_ftn77) and the protection of the rights and freedoms of others.

Article 3  
Scope

1.       Within the limits of the competences conferred upon the European Union, the prohibition of discrimination shall apply to all persons, as regards both the public and private sectors, including public bodies, in relation to[\[78\]](#_ftn78):

  

(a)     Social protection[\[79\]](#_ftn79), including social security, social assistance, social housing[\[80\]](#_ftn80) and healthcare;

(b)[\[81\]](#_ftn81)

(c)          Education[\[82\]](#_ftn82);

(d)     access to and[\[83\]](#_ftn83) supply of goods and other services which are available to the public, including housing[\[84\]](#_ftn84).

Subparagraph (d) shall apply to natural persons only insofar as they are performing a professional or commercial activity defined  in accordance with national laws and practice, outside the context of private and family life[\[85\]](#_ftn85).

2.       Notwithstanding[\[86\]](#_ftn86) paragraph 1, this Directive does not apply to[\[87\]](#_ftn87):

  

(a)     matters covered by family law, including marital status and adoption, as well as laws on reproductive rights;

(b)     the organisation of Member States' social protection systems, including decisions on the setting up, financing and management of such systems and related institutions as well as on the substance and delivery of benefits and services and the conditions of eligibility;

(c)     the competences of the Member States to determine[\[88\]](#_ftn88) the type of health services provided and the conditions of eligibility;

(d)     the content of teaching, and  the organisation and funding of the Member States' educational systems, including the organisation of education for people with special needs[\[89\]](#_ftn89).

3.[\[90\]](#_ftn90) Member States may provide that differences of treatment based on a person's religion or belief in respect of admission to educational institutions, the ethos of which is based on religion or belief, in accordance with national laws, traditions and practice, shall not constitute discrimination.

These differences of treatment shall not justify discrimination on any other ground referred to in Article 1.

3a.     This Directive is without prejudice to national measures authorising or prohibiting the wearing of religious symbols.

  

4[\[91\]](#_ftn91).    This Directive is without prejudice to national legislation ensuring the secular nature of the State, State institutions or bodies, or education, or concerning the status and activities of churches and other organisations based on religion or belief.

5.       This Directive does not cover differences of treatment based on nationality and is without prejudice to provisions and conditions relating to the entry into and residence of third-country nationals and stateless persons in the territory of Member States, and to any treatment which arises from the legal status of the third-country nationals and stateless persons concerned.

Article 4

Accessibility for persons with disabilities[\[92\]](#_ftn92)

1.             Member States shall take the necessary and appropriate measures to ensure accessibility for persons with disabilities, on an equal basis with others, within the areas set out in Article 3. These measures should not impose a disproportionate burden.[\[93\]](#_ftn93)

1a.     Accessibility includes \[general anticipatory measures\] to ensure the effective implementation of the principle of equal treatment in all areas set out in Article 3 for persons with disabilities, on an equal basis with others\[, and with a medium or long-term commitment\][\[94\]](#_ftn94).

2.             Such measures shall comprise the identification and elimination of obstacles and barriers to accessibility, \[as well as the prevention of new obstacles and barriers\][\[95\]](#_ftn95) in the areas covered in this Directive.

  

3.

4.

5.

6.       Paragraphs 1 and 2 shall apply to private and social housing[\[96\]](#_ftn96) only as regards the common parts[\[97\]](#_ftn97) of buildings with more than one housing unit. This paragraph shall be without prejudice to Article 4(7) and Article 4a.

7\. [\[98\]](#_ftn98) Member States shall \[progressively take the necessary measures\][\[99\]](#_ftn99) to ensure that sufficient private and / or social housing is accessible[\[100\]](#_ftn100) for people with disabilities.

_Article 4a  
Reasonable accommodation for persons with disabilities_

1.       In order to guarantee compliance with the principle of equal treatment in relation to persons with disabilities, reasonable accommodation shall be provided within the areas set out in Article 3, unless this would impose a disproportionate burden.

  

2.       Reasonable accommodation means necessary and appropriate modifications and adjustments where needed in a particular case**,** to ensure to persons with disabilities access on an equal basis with others[\[101\]](#_ftn101).

3.

4.

_Article 4b  
Provisions concerning accessibility[\[102\]](#_ftn102) and reasonable accommodation_

1.       For the purposes of assessing[\[103\]](#_ftn103) whether measures necessary to comply with Articles 4 and 4a would impose a disproportionate burden, account shall be taken, in particular, of:

a)             the size and resources[\[104\]](#_ftn104) of the organisation or enterprise;

b)             the estimated cost[\[105\]](#_ftn105);

c)

d)            the life span of infrastructures[\[106\]](#_ftn106) and objects which are used to provide a service;

e)             the historical, cultural, artistic or architectural value of the movable or immovable property in question[\[107\]](#_ftn107);

  

f)              whether the measure in question is impracticable or unsafe[\[108\]](#_ftn108).

The burden shall not be deemed disproportionate when it is sufficiently remedied by measures existing within \[the framework of the disability policy of\][\[109\]](#_ftn109) the Member State concerned.

2.       Articles 4 and 4a shall apply to the design and manufacture[\[110\]](#_ftn110) of goods, unless this would impose a disproportionate burden. For the purpose of assessing whether a disproportionate burden is imposed in the design and manufacture of goods, consideration shall be taken of the criteria set out in article 4b(1).

3.       Articles 4 and 4a shall not apply where European Union law provides for detailed standards or specifications on the accessibility or reasonable accommodation[\[111\]](#_ftn111) regarding particular goods or services[\[112\]](#_ftn112).

Article 5  
Positive action

1.  With a view to ensuring full equality in practice, the principle of equal treatment shall not prevent any Member State from maintaining or adopting specific measures to prevent or compensate for disadvantages linked to religion or belief, disability, age, or sexual orientation.

  

2.  In particular, the principle of equal treatment shall be without prejudice to the right of Member States to maintain or adopt more favourable provisions for persons with disabilities as regards conditions for access to social protection, including social security, social assistance and healthcare; education; and certain goods or services which are available to the public, in order to promote their economic, cultural or social integration.

Article 6  
Minimum requirements

1.       Member States may introduce or maintain provisions which are more favourable to the protection of the principle of equal treatment than those laid down in this Directive.

2.       The implementation of this Directive shall under no circumstances constitute grounds for a reduction in the level of protection against discrimination already afforded by Member States in the fields covered by this Directive.

CHAPTER II  
REMEDIES AND ENFORCEMENT

Article 7  
Defence of rights

1.       Member States shall ensure that judicial and/or administrative procedures, including where they deem it appropriate conciliation procedures, for the enforcement of obligations under this Directive are available to all persons who consider themselves wronged by failure to apply the principle of equal treatment to them, even after the relationship in which the discrimination is alleged to have occurred has ended.

  

2.       Member States shall ensure that associations, organisations or other legal entities, which have, in accordance with the criteria laid down by their national law, a legitimate interest in ensuring that the provisions of this Directive are complied with, may engage[\[113\]](#_ftn113), either on behalf or in support of the complainant, with his or her approval, in any judicial and/or administrative procedure provided for the enforcement of obligations under this Directive.

3.       Paragraphs 1 and 2 shall be without prejudice to national rules relating to time limits for bringing actions as regards the principle of equality of treatment.

Article 8  
Burden of proof

1.       Member States shall take such measures as are necessary, in accordance with their national judicial systems, to ensure that, when persons who consider themselves wronged because the principle of equal treatment has not been applied to them establish, before a court or other competent authority, facts from which it may be presumed that there has been direct or indirect discrimination, it shall be for the respondent to prove[\[114\]](#_ftn114) that there has been no breach of the prohibition of discrimination.

2.       Paragraph 1 shall not prevent Member States from introducing rules of evidence which are more favourable to plaintiffs.

3.         Paragraph 1 shall not apply to criminal procedures.

4.       Member States need not apply paragraph 1 to proceedings in which the court or other competent body investigates the facts of the case.

  

5.       Paragraphs 1, 2, 3 and 4 shall also apply to any legal proceedings commenced in accordance with Article 7(2).

Article 9  
Victimisation

Member States shall introduce into their national legal systems such measures as are necessary to protect individuals from any adverse treatment or adverse consequence as a reaction to a complaint or to proceedings[\[115\]](#_ftn115) aimed at enforcing compliance with the principle of equal treatment

Article 10  
Dissemination of information

Member States shall ensure that the provisions adopted pursuant to this Directive, together with the relevant provisions already in force, are brought to the attention of the persons concerned by appropriate means throughout their territory.

Article 11  
Dialogue with relevant stakeholders

With a view to promoting the principle of equal treatment, Member States shall encourage dialogue with relevant stakeholders, which have, in accordance with their national law and practice, a legitimate interest in contributing to the fight against discrimination on the grounds and in the areas covered by this Directive.

Article 12  
Bodies for the Promotion of Equal treatment

1.       Member States shall designate a body or bodies for the promotion of equal treatment of all persons irrespective of their religion or belief, disability, age, or sexual orientation. These bodies may form part of agencies charged at national level with the defence of human rights or the safeguard of individuals' rights.

  

2.       Member States shall ensure that the competences of these bodies include:

(a)     without prejudice to the right of victims and of associations, organizations or other legal entities referred to in Article 7(2), providing independent assistance to victims of discrimination in pursuing their complaints about discrimination,

(b)     conducting independent surveys concerning discrimination, and

(c)     publishing independent reports and making recommendations on any issue relating to such discrimination.

CHAPTER III  
FINAL PROVISIONS

Article 13  
Compliance

Member States shall take the necessary measures to ensure that the principle of equal treatment is respected within the scope of this Directive and in particular that:

(a)     any laws, regulations and administrative provisions contrary to the principle of equal treatment are abolished;

(b)     any contractual provisions, internal rules of undertakings, and rules governing profit-making or non-profit-making associations contrary to the principle of equal treatment are, or may be, declared null and void or are amended.

Article 14  
Sanctions[\[116\]](#_ftn116)

Member States shall lay down the rules on sanctions applicable to infringements of national provisions adopted pursuant to this Directive, and shall take all measures necessary to ensure that they are applied. Sanctions may comprise the payment of compensation, which may not be restricted by the fixing of a prior upper limit, and must be effective, proportionate and dissuasive.

Article 14a[\[117\]](#_ftn117)  
Gender mainstreaming

In accordance with Article 8 of the Treaty on the Functioning of the European Union, the Member States shall, when implementing this Directive, take into account the aim of eliminating inequalities, and of promoting equality, between men and women.

Article 15[\[118\]](#_ftn118)

Implementation

1.       Member States shall adopt the laws, regulations and administrative provisions necessary to comply with this Directive by …. at the latest \[4[\[119\]](#_ftn119) years after adoption\]. They shall forthwith inform the Commission thereof and shall communicate to the Commission the text of those provisions[\[120\]](#_ftn120).

When Member States adopt these measures, they shall contain a reference to this Directive or be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.

  

2.             In order to take account of particular conditions[\[121\]](#_ftn121), Member States may, if necessary, establish that the obligation to ensure accessibility as set out in Articles 4 and 4b has to be complied with by, at the latest, \[5[\[122\]](#_ftn122) years after adoption\] regarding new buildings[\[123\]](#_ftn123), facilities, vehicles and infrastructure[\[124\]](#_ftn124), as well as existing[\[125\]](#_ftn125) buildings, facilities and infrastructure undergoing significant renovation[\[126\]](#_ftn126) and by \[20[\[127\]](#_ftn127) years after adoption\] regarding all other[\[128\]](#_ftn128) existing buildings, facilities, vehicles[\[129\]](#_ftn129) and infrastructure.

Member States wishing to use any of these additional periods shall inform the Commission at the latest by the date set down in paragraph 1 giving reasons. Member States shall also communicate to the Commission by the same date an action plan laying down the steps to be taken and the timetable for achieving the gradual implementation of Article 4 \[, including its paragraph 7\]. They shall report on progress every two years starting from this date.

Article 16  
Report

1.       Member States shall communicate to the Commission, by …. at the latest and every five years thereafter, all the information necessary for the Commission to draw up a report to the European Parliament and the Council on the application of this Directive.

2.       The Commission's report shall take into account, as appropriate, the viewpoints of national equality bodies and relevant stakeholders, as well as the EU Fundamental Rights Agency. In accordance with the principle of gender mainstreaming, this report shall, _inter alia_, provide an assessment of the impact of the measures taken on women and men. In the light of the information received, this report shall include, if necessary, proposals to revise and update this Directive.

Article 17  
Entry into force

This Directive shall enter into force on the day of its publication in the Official Journal of the European Union.

Article 18  
Addressees

This Directive is addressed to the Member States.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

* * *

[\[1\]](#_ftnref1) Positions are summarised based on docs. 14008/09, 16063/09 ADD 1, 5790/10, 6847/10, 7758/10, 8887/1/10 REV 1 and 9312/10 + COR 1.

[\[2\]](#_ftnref2) See docs. 15533/09 + ADD 1 and 15540/09.

[\[3\]](#_ftnref3) See doc. DS 823/08.

[\[4\]](#_ftnref4) The European Parliament adopted its Opinion under the Consultation Procedure on 2 April 2009 (doc. A6-0149/2009). Kathalijne Maria Buitenweg (Group of the Greens / European Free Alliance) served as Rapporteur. The new EP Rapporteur is Raúl Romeva I Rueda (ES, Group of the Greens / European Free Alliance).

[\[5\]](#_ftnref5) IE: reservation.

[\[6\]](#_ftnref6) OJ C , , p. .

[\[7\]](#_ftnref7) OJ C , , p. .

[\[8\]](#_ftnref8) UK: reservation on Recitals 1-3.

[\[9\]](#_ftnref9) Presidency conclusions of the Brussels European Council of 14 December 2007, point 50.

[\[10\]](#_ftnref10) Resolution of 20 May 2008 P6_TA-PROV(2008)0212.

[\[11\]](#_ftnref11) COM (2008) 412

[\[12\]](#_ftnref12) Directive 2000/43/EC, Directive 2000/78/EC and Directive 2004/113/EC

[\[13\]](#_ftnref13) EL, DK, DE, FR, IT and MT suggested deleting "discrimination based on assumptions". BG: scrutiny reservation. LV, FI and SE wished to add "harassment due to association" here.

[\[14\]](#_ftnref14) Case C-303/06, Coleman v. Attridge, judgment of 17 July 2008.

[\[15\]](#_ftnref15) IT: reservation on "discrimination by association" (see also doc. 13495/09). CZ, DK, DE, EL and NL expressed doubts about the expansion of this concept to include all grounds.

[\[16\]](#_ftnref16) LV suggested deleting "for instance… …occupation".

[\[17\]](#_ftnref17) MT suggested reinserting the deleted last sentence (see doc. 6847/10, p. 8). SK considered Recital 12b redundant (cf. Article 2(2)(c)). DK and MT: scrutiny reservations.

[\[18\]](#_ftnref18) FR and IT: scrutiny reservations. LV preferred the version in doc. 15320/09.

[\[19\]](#_ftnref19) AT: reservation. CZ: scrutiny reservation. See also suggestions by BG, IT, MT, FI in doc. 16063/10 ADD 1, footnotes 17 and 19. BE stressed the need for transparent risk calculations by insurers. SK preferred "products" to "services".

[\[20\]](#_ftnref20) SK preferred "have shown". IE: scrutiny reservation.

[\[21\]](#_ftnref21) BG suggested deleting "the freedom of the press".

[\[22\]](#_ftnref22) IE, EL, MT and FR called for the deletion of "social protection".

[\[23\]](#_ftnref23) DE, IE and UK suggested removing "education" from the scope.

[\[24\]](#_ftnref24) UK suggested adding "and supply of". Cf. Directive 2000/43/EC.

[\[25\]](#_ftnref25) FR: reservation. FR suggested stating that the Directive applied to social protection (excluding regimes involving "pay" in the sense of Article 157 TFUE), and that social protection included social security, social assistance and health care.

[\[26\]](#_ftnref26) FI suggested "…social security, **social services and other forms of** social assistance…"

[\[27\]](#_ftnref27) MT asked whether schemes _not_ funded by the state were covered.

[\[28\]](#_ftnref28) FR suggested deleting these words.

[\[29\]](#_ftnref29) OJ L 166, 30.4.2004, p. 1.

[\[30\]](#_ftnref30) CZ, IE, MT and NL suggested deleting the last sentence.

[\[31\]](#_ftnref31) NL suggested a recital explaining that the Directive only applied to goods and services _for which remuneration was normally required_ and that _buildings_ were only included _if goods or services were supplied from them_. Cion endorsed this interpretation in principle.

[\[32\]](#_ftnref32) NL supported by IE and MT suggested explicitly excluding the area of private and family life from the scope. See doc. 6563/10.

[\[33\]](#_ftnref33) NL and MT suggested replacing the words in square brackets with "apply".

[\[34\]](#_ftnref34) CY considered this reference to "social housing" to be redundant. CZ asked for the term "allocation" to be clarified. FR: scrutiny reservation.

[\[35\]](#_ftnref35) BE suggested: "for instance, their adequacy and sustainability".

[\[36\]](#_ftnref36) EL suggested: "…including, **among others**, the provision of special needs education, **\[…\]** the setting up and management of educational institutions, the development of curricula and other educational activities, **the appointment and any issue concerning teachers' career development**…"

[\[37\]](#_ftnref37) UK had misgivings concerning the inclusion of student evaluations in the scope.

[\[38\]](#_ftnref38) PL: reservation. PL, MT and LT preferred including "benefits dependent on" family law in Article 3(2). FR agreed that such benefits ought not fall within the scope. CZ, NL, FI and Cion saw a need for clarification regarding the "benefits" referred to.

[\[39\]](#_ftnref39) AT: reservation. FI suggested placing Recital 19a in the articles. BE also suggested providing a definition of a person with a disability.

[\[40\]](#_ftnref40) IE, FR and AT: reservations. CZ suggested deleting the last sentence. FI suggested placing Recital 19b in the articles.

[\[41\]](#_ftnref41) IT, MT and NL questioned the examples. EE, NL and UK saw a need to clarify the scope of the disability provisions with respect to "the physical environment" and "infrastructure".

[\[42\]](#_ftnref42) MT: scrutiny reservation. HU suggested mirroring the content of Recital 19c in the articles.

[\[43\]](#_ftnref43) Article 2 of the UNCRPD.

[\[44\]](#_ftnref44) Regulation (EC) No. 1107/2006 and Regulation (EC) No 1371/2007.

[\[45\]](#_ftnref45) OJ L 210, 31.7.2006, p.25. Regulation as last amended by Regulation (EC) No 1989/2006 (OJ L 411, 30.12.2006, p.6).

[\[46\]](#_ftnref46) OJ C 134, 7.6.2003, p.7

[\[47\]](#_ftnref47) CZ, IT, LT, MT NL, FI and UK suggested reintroducing "nature".

[\[48\]](#_ftnref48) LU: scrutiny reservation.

[\[49\]](#_ftnref49) OJ L 303, 2.12.2000, p. 16.

[\[50\]](#_ftnref50) OJ L 180, 19.7.2000, p. 22.

[\[51\]](#_ftnref51) OJ L 373, 21.12.2004, p. 37.

[\[52\]](#_ftnref52) EL suggested adding "...the limits of the current competencies of the European Union and…".

[\[53\]](#_ftnref53) FR suggested specifying that the Directive was without prejudice to Directive 2000/78/EC.

[\[54\]](#_ftnref54) DE: reservation.

[\[55\]](#_ftnref55) IE: reservation on "principle".

[\[56\]](#_ftnref56) All delegations: scrutiny reservations on Article 2.

[\[57\]](#_ftnref57) IE, DE and UK questioned the inclusion of "the _principle_ of equal treatment" here.

[\[58\]](#_ftnref58) UK and DK reiterated the view that the prohibition of harassment should not apply to "religion or belief" or to "sexual orientation". DK and MT: scrutiny reservations.

[\[59\]](#_ftnref59) RO supported by IE suggested clarifying the provision on "instruction to discriminate" in a separate paragraph. (See also doc. 6596/10.)

[\[60\]](#_ftnref60) MT: scrutiny reservation.

[\[61\]](#_ftnref61) DK saw a need to clarify "harassment by association". HU saw a need to clarify the general concept of association, including with respect to harassment and direct discrimination. LT and MT: scrutiny reservations. IT: reservation on "discrimination by association". FR: reservation on "harassment by association".

[\[62\]](#_ftnref62) IE: reservation on the inclusion of _potential_ disadvantages. IE also suggested "persons _having_ a particular disability".

[\[63\]](#_ftnref63) SK and NL preferred the original definition of harassment (see doc. 6400/10). SK: scrutiny reservation. PT: reservation.

[\[64\]](#_ftnref64) DE saw a need to improve legal certainty. UK suggested "…association with persons of a certain religion or belief, **persons** with disabilities, **persons** of a given age or **persons** of a certain sexual orientation".

[\[65\]](#_ftnref65) FR and NL: reservations. MT: scrutiny reservation. UK and IE felt that minors should be excluded from the scope of the Directive, UK recalling its suggestion that "a public policy test" be applied to differences of treatment on the grounds of age (see doc. 6565/10).

[\[66\]](#_ftnref66) CZ and FI preferred "objectively _and reasonably_". FI suggested that relevant limitations on the Member States' discretion stemming from international law be mentioned.

[\[67\]](#_ftnref67) UK supported by IE suggested deleting these words.

[\[68\]](#_ftnref68) BG, DK, MT and UK stressed the need to cover _non-legislative_ rules.

[\[69\]](#_ftnref69) BE, NL and MT expressed the view that all the issues mentioned in this sub-paragraph were excluded from the scope under Article 3(2)(b). DK suggested "shall be deemed"; UK favoured "do not constitute"; IT, MT and NL supported these suggestions.

[\[70\]](#_ftnref70) UK supported by IE and MT felt that fair and reasonable differences of treatment should be permitted (see doc. 5568/10). BE, IT, LV and NL have called for greater transparency with respect to differences of treatment by insurers. CZ preferred the previous version of Article 2(7)(b). SK preferred "products" to "services". BE, CZ, NL, IT, AT and UK: scrutiny reservations.

[\[71\]](#_ftnref71) CY, FR and IT preferred "shall". LU, NL and Cion were unable to accept the change.

[\[72\]](#_ftnref72) FI made the point that "actuarial " was an insurance term, and suggested deleting "and".

[\[73\]](#_ftnref73) CZ, IT, CY, MT and AT felt that the last sentence of Article 2(7)(a) and (b) should be consistently worded. LU also called for clarification. Cion suggested the following wording in Article 2(7)(a) and (b): "…based on relevant actuarial principles and **on** accurate scientific data, or, in **its** absence, **\[…\]** on relevant medical knowledge".

[\[74\]](#_ftnref74) UK is considering whether Article 2(7)(b) should apply to all financial services or to the insurance sector only. BE, LU, AT suggested restricting the provisions to insurance only. SK preferred "products" to "services". IT: scrutiny reservation.

[\[75\]](#_ftnref75) CY, FR and IT preferred "shall". LU, NL and Cion were unable to accept the change.

[\[76\]](#_ftnref76) UK, IE and MT expressed the view that disability _in itself_ was sometimes a determining factor in the assessment of risk.

[\[77\]](#_ftnref77) NL and FI raised the possibility of addressing safety issues (e.g. in a recital).

[\[78\]](#_ftnref78) All delegations: scrutiny reservations on Article 3. CZ, EL, MT, LT and UK favoured the earlier wording specifying that the scope covered "access to" the fields listed. See doc. 14896/08, p. 29. AT was could also accept the reintroduction of the words.

[\[79\]](#_ftnref79) IE, EL, MT and FR called for the deletion of "social protection".

[\[80\]](#_ftnref80) BE, BG, DK, IE, IT, LT, MT, FR, NL, PL and SK saw a need to clarify "social housing", BG, ES, NL and SE expressing the view that defining this concept at the national level might be most appropriate. DE, CZ, LT and HU suggested deleting "social housing". BE, IT, LT: scrutiny reservations.

[\[81\]](#_ftnref81) FI supported by AT questioned the deletion of "social advantages", which was included in existing legislation (see, for example, Directive 2000/43/EC, Article 3(1)(f)).

[\[82\]](#_ftnref82) DE, IE and UK called for the deletion of "education".

[\[83\]](#_ftnref83) CZ, UK and MT preferred the wording in doc. 8173/10, UK stressing that the different formulations had different legal consequences.

[\[84\]](#_ftnref84) NL supported by IE and IT suggested adding "insofar as they are offered outside the area of private and family life". See doc. 6563/10.

[\[85\]](#_ftnref85) CZ: scrutiny reservation. DE, IE, LT and RO saw a need for greater clarity. SE considered the reference to private and family life to be superfluous. NL suggested: "shall apply to natural persons only insofar as they are **offering a service** **\[…\]** outside the context of private and family life". Cion gave a provisional welcome to this approach.

[\[86\]](#_ftnref86) FR: reservation on "Notwithstanding…" CZ, FR and UK preferred the chapeau as worded in doc 6092/10, UK stressing that the new wording made a material difference to the delineation of the scope, and FR recalling that Case C-144/04 "Mangold" had extended EC competences. Cion supported the current wording.

[\[87\]](#_ftnref87) NL supported by BG, EE, FR and LT felt that "public space" ought to be excluded from the scope (see docs. 6563/10 and 8711/10). IE and MT: scrutiny reservations on Article 3(2). MT stated that it could accept the wording suggested by the Presidency, but could go no further; MT also recalled the concerns expressed in doc. DS 14786/09. LT and PL: reservations.

[\[88\]](#_ftnref88) NL suggested "the determination of the type…".

[\[89\]](#_ftnref89) PL preferred the wording in doc. 8173/10. FR also saw a need for clarification. See also EL suggestion in doc. 8887/10, footnote 31. DE, IE and UK favoured excluding "education" from the scope. FI, NL and UK preferred the previous version that included the reference to "the conditions of eligibility" (doc. 8173/10). NL suggested using the words "the definition of who is entitled" here (see Recital 17a). AT: scrutiny reservation.

[\[90\]](#_ftnref90) FR: reservation. AT and UK felt that refusing admission was potentially discriminatory. UK: scrutiny reservation.

[\[91\]](#_ftnref91) CZ, IE and MT suggested deleting Article 3(4). MT: scrutiny reservation.

[\[92\]](#_ftnref92) All delegations: scrutiny reservations on Articles 4, 4a and 4b.

[\[93\]](#_ftnref93) CZ suggested deleting the last sentence. LT saw a need for clarification.

[\[94\]](#_ftnref94) DK, EE, EL, DE, FR, IT, CY, HU, MT,  NL, RO and UK saw a need to clarify the practical implications of Article 4(1a), particularly with respect to the words in square brackets. CY: scrutiny reservation.

[\[95\]](#_ftnref95) NL suggested deleting the words in square brackets. EE, MT and UK saw a need for clarification.

[\[96\]](#_ftnref96) FI and UK wondered whether all relevant types of housing were covered by these terms. IT and PL felt that the _use_ of the building should be the determining factor, and EE, NL and UK felt that a case-by-case approach was more appropriate. See also Article 3(1)(a). UK: reservation on "housing".

[\[97\]](#_ftnref97) DE, FR, LV, LT, AT, SE saw a need to clarify this term. FR and AT: scrutiny reservations. BE and CZ pointed out that individual flats should be covered in relevant cases.

[\[98\]](#_ftnref98) DE, EE, CY, LT, MT and PL had doubts regarding this provision. LT and NL suggested moving it to a Recital. Concurring, Cion suggested citing Article 28 of the UNCRDP in the recitals. AT: scrutiny reservation.

[\[99\]](#_ftnref99) DE, LV, LT, HU and PL called for clarification.

[\[100\]](#_ftnref100) PT suggested "adaptable".

[\[101\]](#_ftnref101) MT, AT, NL and UK stressed that the wording should be realistic from an implementation point of view, access on an equal basis with others not always being attainable.

[\[102\]](#_ftnref102) CZ suggested "access".

[\[103\]](#_ftnref103) LT suggested that the Member States should be in charge of the assessing.

[\[104\]](#_ftnref104) IT, MT and FI suggested adding "nature". See also FI note in doc. 6015/10. BG and DE suggested exempting SMEs; MT shared the concern.

[\[105\]](#_ftnref105) EE, NL and Cion saw a need to mention the need for (or benefits of) measures as well as their cost. EE suggested adding "environmental impact" as a criterion.

[\[106\]](#_ftnref106) NL suggested deleting "infrastructures". EE, MT, NL and UK suggested addressing the "life span" or recurring nature _of a service_; Cion supported taking this into account.

[\[107\]](#_ftnref107) AT: reservation. FR: scrutiny reservation on "movable or immovable property".

[\[108\]](#_ftnref108) EE, IT, NL, AT and UK suggested deleting this provision. PL also had doubts about the reference to "unsafe" measures.

[\[109\]](#_ftnref109) FI supported by LV and Cion suggested deleting the words in square brackets.

[\[110\]](#_ftnref110) UK, supported by IT, MT, SE, called for the extent and impact of this provision to be clarified, including with respect to liability issues and precise obligations created. UK: scrutiny reservation. DE, BG, CZ, EE and FI also expressed doubts. Should the issue of seeking to improve the supply of adaptable goods and services be included, FI supported by BG and CZ suggested addressing it in Article 3.

[\[111\]](#_ftnref111) CZ preferred the earlier version of and questioned the inclusion of "reasonable accommodation".

[\[112\]](#_ftnref112) FI supported by FR, NL and SE provisionally suggested: "Articles 4 and 4a shall not apply **to areas** where European Union \[**and national\]** law provides for detailed standards of specifications on the accessibility or \[**the requirement for**\] reasonable accommodation regarding particular goods or services**, including directives and regulations that provide for the assistance of disabled passengers**". DE, MT, FI: scrutiny reservations.

[\[113\]](#_ftnref113) IE: reservation. IE suggested adding: "as the Member States so determine and in accordance with the criteria laid down by their national law". DE, EL, IT and AT also supported a reference to national law. DE: reservation on the creation of _individual rights_ in this context.

[\[114\]](#_ftnref114) CZ, DE, IE, IT, LU, NL expressed concern in respect of the reversal of the burden of proof. IT: scrutiny reservation. Cion explained and affirmed the inclusion of this provision.

[\[115\]](#_ftnref115) IE recalled that "legal proceedings" was used in Directives 2000/78/EC and 2004/113/EC.

[\[116\]](#_ftnref116) SE suggested "penalties".

[\[117\]](#_ftnref117) CZ: reservation. BG, IE, NL and UK asked for clarification.

[\[118\]](#_ftnref118) All delegations: scrutiny reservations on Article 15. MT and FI asked for the link between the implementation date and the dates in Article 15(2) to be clarified. DK, PL and RO: reservations. PL questioned the setting of deadlines. DE, LV and AT also favoured and incremental approach (progressive realisation).

[\[119\]](#_ftnref119) MT called for a longer period.

[\[120\]](#_ftnref120) Cion: reservation on the deletion of the provision obliging the Member States to send the Commission correlation tables.

[\[121\]](#_ftnref121) DK and FI have asked for the term "particular conditions" to be clarified.

[\[122\]](#_ftnref122) BE preferred immediate applicability with respect to new buildings. EE, for its part, saw a need for a longer period. LV suggested 10 years. AT suggested 15 years _across the board_.

[\[123\]](#_ftnref123) IT expressed the view that the Member States should be free to define "new buildings".

[\[124\]](#_ftnref124) IT expressed doubts with respect to the inclusion of vehicles and called for a specific transposition period for ICT and infrastructure.

[\[125\]](#_ftnref125) IT wished for reassurance that the "disproportionate burden" provision also applied to existing buildings. LV: reservation. PL favoured leaving existing buildings to the Member States' discretion.

[\[126\]](#_ftnref126) FI felt that more time was needed for buildings undergoing "significant renovation" and called for this concept to be clarified. LT also saw a need for a longer period (30 years).

[\[127\]](#_ftnref127) FR: reservation with respect to inter-urban transport and road infrastructure. LT called for a longer deadline.

[\[128\]](#_ftnref128) LU suggested examining whether the reference to "all other existing buildings, facilities and infrastructure" in Article 15(2) was conducive to unanimity at the Council level.

[\[129\]](#_ftnref129) BE, CZ, DE and HU expressed doubts regarding the feasibility of adapting existing vehicles. UK expressed the view that the provisions ought to apply to transport services, not vehicles.