---
title: '2008-0140-Discrimination-DK-ST13456.EN10'
date: Thu, 16 Sep 2010 10:29:58 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 16 September 2010

Interinstitutional File:

2008/0140 (CNS)

13456/10

LIMITE

SOC 515

JAI 723

MI 292

  

  

  

  

  

NOTE

from :

Council General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

12461/10 SOC 470 JAI 651 MI 256

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Further to the meeting of the Working Party on Social Questions on 16 July 2010, delegations will find attached a note from the DK delegation.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Questions for SQWP debate on financial services-**

**DK answers**  **to the questionnaire in document 11903/10**

1\. Situation in Member States

Q: How do you deal with the use of age and disability as factors in risk assessment in financial services (insurance and banking) at national level?

Answer (life insurance): With very few exceptions age is used as a factor in calculation of premiums and benefits, because it is considered a determining factor in assessment of the mortality and disability risks. Disability is generally used as a factor in calculation of premiums and benefits, because it is considered a determining factor in assessment of the disability and mortality risks. Any use of age/disability in calculation of premiums and benefits must reflect the mortality/disability risks.

Answer (non-life insurance): Age is generally used as a factor in calculation of premiums and benefits in health insurance, because it is considered a determining factor in assessment of the mortality and disability risks. Age is furthermore used as a factor in calculation of premiums and benefits in a limited number of other non-life insurance products, e.g. motor insurance. However, not all non-life insurance companies use age as a factor in these products. Use of age as a factor suggests that the company considers age a determining factor in assessment of the risk, but a direct correlation is not required in the Danish legislation. Disability is typically used as a factor in calculation of premiums and benefits in disability-related non-life insurance products, e.g. health insurance. Use of disability as a factor suggests that the company considers disability a determining factor in assessment of the risk, but a direct correlation is not required in the Danish legislation.

Answer (banking): Interest rates and other costs associated with banking products are usually determined by the individual customer's credit rating and overall financial status. Age and disability are factors that can play a role in an individual customer's rating, but they cannot be used as the determining factor in setting the terms of the contract.

2\. Disability / health conditions

Q: It has been suggested that the exception for disability can be limited to the cases where the underlying health condition is a relevant risk factor. What is your view?

Answer (insurance): Generally the exception for disability can be limited to the cases where the underlying health condition is a relevant risk factor. However, there may be special cases where a disability itself is a relevant risk factor. In such cases the exception for disability should not be limited to the cases where the underlying health condition is a relevant risk factor.

3\. Scope of the exception

Q: Currently the exception provides for a three-step test (similar to Directive 2004/113/EC regarding gender):

1.  Is age / disability a determining factor in the assessment of risk for the service in question?

2.  Is the risk assessment based on relevant actuarial principles, accurate statistical data or medical knowledge? (evidence base)

3.  Is the difference of treatment proportionate?

Is this test workable in your view?

Answer (insurance): We have doubts regarding the use of "medical knowledge" without any evidence from statistical data.

4\. Evidence to justify differential treatment

Q: The current text on the possible evidence base is slightly different for age and disability. Should risk assessment always be based on relevant actuarial principles? Should the use of medical knowledge only be possible, if statistical data are not available? Should there be restrictions on the kind of data that can be used?

Answer (insurance): Risk assessment should always be based on accurate statistical data which might be backed up by medical knowledge. We would prefer deletion of the reference to actuarial principles as this reference is rather vague. Statistical data should be sufficient.

5\. Proportionality of differential treatment

Q: What do you understand by "proportionate differences"? Mathematical proportionality or a wider concept of proportionality, taking into account issues such as legitimacy, objectivity, relevance and the existence of less restrictive alternatives ("ultima ratio" principle)?

Answer (insurance): By "proportionate differences" Denmark understands a wider concept of proportionality in this case.

6\. Age limits and age bands

Q: Would the exception allow financial service providers to apply age limits or age bands? How could proportionality be ensured in that context?

Answer (insurance): Yes, the exception would allow financial service providers to apply age limits or age bands, for example in motor insurance.

As mentioned in answer no. 4 risk assessment should always be based on accurate statistical data which might be backed up by medical knowledge

7\. Transparency towards customer

Q: Is there a need in the exception for a requirement to publish data, similar to the one in Directive 2004/113/EC? If not, should there be other forms of transparency towards the customer (e.g. obligation to explain the reasons for the differential treatment / to advise the customer on possible alternatives)?

  

Answer (insurance): Data would most likely vary so much that they would be extremely difficult to publish in a uniform way. Publication of data therefore would not be a benefit to the average customer. Denmark supports enhanced transparency towards the customer, via for example an obligation for the insurance company to explain the reasons for the differentiated treatment and, if possible, to advise the customer on possible alternatives.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_