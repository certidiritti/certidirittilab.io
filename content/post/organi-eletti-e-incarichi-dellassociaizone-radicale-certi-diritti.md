---
title: 'ORGANI ELETTI E INCARICHI DELL''ASSOCIAZIONE RADICALE CERTI DIRITTI'
date: Thu, 27 Mar 2008 20:11:59 +0000
draft: false
tags: [Comunicati stampa]
---

**Segretario**  
Yuri Guaiana  
segretario@certidiritti.it  
  
**Presidente**  
Enzo Cucco  
presidente@certidiritti.it  
  
**Tesoriere**  
Leonardo Monaco  
tesoriere@certidiritti.it

**Comitato direttivo**  
  
**Revisore dei Conti:** Giacomo Cellottini

**Affermazione Civile:** Gabriella Friso

**Affermazione Civile Trans:** Gigliola Toniollo

**Comunicazione:** Francesco Poirè

**Gruppi locali:** Marco Marchese

**Prostituzione:** Pia Covre

**Intersex:** Michela Balocchi

**Transnazionale:** Antonio Stango