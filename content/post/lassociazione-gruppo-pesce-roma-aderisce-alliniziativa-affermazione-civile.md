---
title: 'L''ASSOCIAZIONE "GRUPPO PESCE ROMA" ADERISCE ALL''INIZIATIVA "AFFERMAZIONE CIVILE"'
date: Mon, 26 May 2008 09:34:45 +0000
draft: false
tags: [Comunicati stampa]
---

![](http://www.informagay.it/pix/loghi/logo_pesce.jpg)Oltre all'associazione corale "Roma Rainbow Choir" anche l'Assocazione Gruppo Pesce Roma aderisce formalmente all'Iniziativa di Affermazione Civile promossa dall'Associazione Radicale Certi Diritti. Di seguito il testo dell'adesione:  
Il GRUPPOPESCEROMA aderisce e sostiene il progetto promosso da "Certi Diritti", in collaborazione con gli avvocati della Rete Lenford per il matrimonio tra persone dello stesso sesso.  
  
Questa è la scelta unanime del Consiglio Direttivo del GPR.  
  
Pier Giorgio De Simone  
per il C.D. del GPR