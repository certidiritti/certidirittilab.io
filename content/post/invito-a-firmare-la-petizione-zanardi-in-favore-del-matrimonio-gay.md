---
title: 'INVITO A FIRMARE LA PETIZIONE ZANARDI IN FAVORE DEL MATRIMONIO GAY'
date: Wed, 02 Dec 2009 10:04:34 +0000
draft: false
tags: [Comunicati stampa]
---

**E’ partita a mezza notte del 01129 la petizione nazionale indirizzata alPresidente della Camera dei Deputati Gianfranco Fini, al Presidente del Senato Renato Schifani, al Presidente del Parlamento Europeo**

**Oggetto: Petizione contro la mancata applicazione di importanti principi di legalità in tema di diritto al matrimonio.**

**La petizione nazionale è appoggiata ufficialmente da numerose associazioni nazionali LGBT citate nella stessa, da movimenti locali e non dai cittadini, i genitori i parenti e gli amici che vedono alle porte del 2010 ingiusta e repressiva la condizione degli omosessuali, gay lesbiche e trans.**

**Nella petizione contestiamo la mancata applicazione dei principi contenuti nella Costituzione Italiana e la mancata applicazione delle direttive Europee citate di seguito.**

Quindi mi ritrovo a chiedereVi, egregi deputati: per quale motivo non vengono applicati importanti principi di legalità nazionali e ancor meno vengono osservati i più essenziali trattati dell'Europa unita? Perché tanto egoismo e cecità verso l'amore, visto che riconoscere la nostra unione civile non comporterebbe alcunché se non l’applicazione di sacrosanti diritti?  
Vorremmo poterci aiutare e soccorrere, in salute e malattia, ricchezza e povertà come tutte le coppie del mondo che si amano.

A nome mio, di Manuel e dei molti cittadini omosessuali ed eterosessuali che sostengono la nostra causa,  porgo i più sinceri e cordiali saluti.

Francesco Zanardi e Manuel Incorvaia Savona 21/11/2009

**

**-----LA CARTA COSTITUZIONALE ITALIANA**

**

Articolo 3 della Costituzione Italiana  
Tutti i cittadini hanno pari dignità sociale e sono eguali davanti alla legge, senza distinzione di sesso, di razza, di lingua, di religione, di opinioni politiche, di condizioni personali e sociali.  
È compito della Repubblica rimuovere gli ostacoli di ordine economico e sociale, che, limitando di fatto la libertà e l'eguaglianza dei cittadini, impediscono il pieno sviluppo della persona umana e l'effettiva partecipazione di tutti i lavoratori all'organizzazione politica, economica e sociale del Paese.

Articolo 7 della Costituzione Italiana  
Lo Stato e la Chiesa cattolica sono, ciascuno nel proprio ordine, indipendenti e sovrani.  
I loro rapporti sono regolati dai Patti Lateranensi. Le modificazioni dei Patti, accettate dalle due parti, non richiedono procedimento di revisione costituzionale.

Articolo 29 della Costituzione Italiana  
La Repubblica riconosce i diritti della famiglia come società naturale fondata sul matrimonio.  
Il matrimonio è ordinato sull’eguaglianza morale e giuridica dei coniugi, con i limiti stabiliti dalla legge a garanzia dell’unità familiare.

**

**-----I TRATTATI EUROPEI**

**

·         L'Italia ha sottoscritto il trattato di Lisbona, che prevede tutela per le minoranze discriminate, anche per gay lesbiche e trans.

·         L’art. 21 sella Carta dei Diritti Fondamentali dell'Unione Europea vieta ogni forma di discriminazione: compresa quella basata sull’orientamento sessuale.

  
·         La Risoluzione sulla parità dei diritti delle persone omosessuali nella Comunità europea, 8 febbraio 1994, con la quale il Parlamento europeo ha individuato come obiettivo delle azioni comunitarie la rimozione degli «ostacoli frapposti al matrimonio di coppie omosessuali ovvero a un istituto giuridico equivalente, garantendo pienamente diritti e vantaggi del matrimonio e consentendo la registrazione delle unioni»  
Da allora ci sono stati ripetuti solleciti e raccomandazioni in merito: l'Italia è stata anche richiamata, ma non è stato fatto nulla.

·         La Risoluzione sul rispetto dei diritti umani nell’Unione europea, 16 marzo 2000, con cui il Parlamento europeo ha chiesto «agli Stati membri di garantire alle famiglie monoparentali, alle coppie non sposate e alle coppie dello stesso sesso parità di diritti rispetto alle coppie e alle famiglie tradizionali, in particolare in materia di legislazione fiscale, regime patrimoniale e diritti sociali».

·         L’art. 9 della Carta di Nizza del 2000, riproclamata solennemente dal Parlamento europeo, dal Consiglio e dalla Commissione nel dicembre 2007 e pubblicata nella Gazzetta ufficiale n. C 303 del 14 settembre 2007, su cui si fonda il «diritto di sposarsi e il diritto di costituire una famiglia», senza che si faccia il minimo riferimento alla diversità di sesso.

PER FIRMARE CLICCA AL SEGUENTE LINK:

**

[http://www.petizionionline.it/petizione/petizione-contro-la-mancata-applicazione-di-importanti-principi-di-legalita-in-tema-di-diritto-al-matrimonio/346](http://www.petizionionline.it/petizione/petizione-contro-la-mancata-applicazione-di-importanti-principi-di-legalita-in-tema-di-diritto-al-matrimonio/346)

**