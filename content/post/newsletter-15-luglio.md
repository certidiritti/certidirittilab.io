---
title: 'Newsletter 15 Luglio'
date: Wed, 31 Aug 2011 10:23:25 +0000
draft: false
tags: [Politica]
---

**![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)**

La legge contro l’omofobia arriverà  in Parlamento il 19 luglio. A partire dalle 15,00 CertiDiritti con le altre  associazioni lgbte sarà  in Pazza Montecitorio con un sit-in e una maratona oratoria per chiedere al Parlamento un segnale forte contro l'omofobia e la transfobia**.** Ti aspettiamo.

Il sindaco di Roma Alemanno chiede una legge che dichiari la prostituzione in strada un reato invece di riconoscere il fallimento di tutte le politiche antiproibizioniste adottate fino ad oggi.Il Governo italiano non ha ancora versato al Fondo Globale per la Lotta contro l’AIDS le quote degli ultimi due anni,  mentre i dati diffusi in questi giorni parlano di 150.000 italiani sieropositivi,  di cui una gran parte eterosessuali non più giovani.

Silvano Tomasi, il capo delegazione dello stato Vaticano all’ONU, ha chiesto che le istanze omosessuali non ricadano nella definizione dei diritti umani, in  North Carolina si scopre che un bambino è stato  ucciso perché avrebbe potuto crescere gay … non è sufficiente indignarsi, almeno in Italia, per non arrenderti iscriviti a Certi Diritti e sostieni le nostre campagne.

Buona lettura.

ASSOCIAZIONE
============

**19 luglio associazioni lgbt(e) in piazza per voto contro omofobia.   
[Leggi>](tutte-le-notizie/1199.html)**

**Onu: Vaticano ancora allineato a paesi dittatoriali e criminali.  
[Leggi>](tutte-le-notizie/1195.html)**

**Prostituzione: Alemanno ha fallito, basta demagogia.  
[Leggi>](tutte-le-notizie/1192.html)**

**Prostituzione: Alemanno 'in sella' per le vie romane.  
[Leggi>](http://www.agenziaradicale.com/index.php?option=com_content&task=view&id=12588&Itemid=53)**

**Prostituzione, Alemanno risponda all’interrogazione popolare.  
[Leggi>](tutte-le-notizie/1197.html)**

**HIV/Aids, Certi Diritti sottoscrive Dichiarazione di Roma.   
[Leggi>](tutte-le-notizie/1194.html)**

**Certi Diritti sottoscrive lettera a Berlusconi su Fondo Aids.  
[Leggi>](tutte-le-notizie/1191.html)**

**Presentato il Coordinamento Campania Rainbow.   
[Leggi>](http://www.arcigay.it/31786/presentato-il-coordinamento-campania-rainbow/)**

**Tunisi: radicali alla manifestazione di Lam Echaml.   
[Leggi>](http://www.radicali.it/comunicati/20110707/tunisi-radicali-alla-manifestazione-di-lam-echaml-uomini-donne-scandiscono-sloga)**

**Tunisi: Radicali , la manifestazione di Lam Echaml prosegue e si dirige verso la piazza dell’orologio.   
[Leggi>](http://www.radicali.it/comunicati/20110707/tunisi-radicali-manifestazione-di-lam-echaml-prosegue-si-dirige-verso-piazza-del)**

**Collegamento con Marco Perduca da Tunisi sulla manifestazione del gruppo Lam Echaml a favore della laicità dello Stato.   
[Ascolta>](http://www.radioradicale.it/scheda/331587/collegamento-con-marco-perduca-da-tunisi-sulla-manifestazione-del-gruppo-lam-echaml-a-favore-della-laicita)**

RASSEGNA STAMPA
===============

**Legge contro l'omofobia naufraga prima dell'aula.   
[Leggi>](rassegna-stampa/1193.html)**

**Ignazio Marino favorevole alle unioni civili gay "Se fossi ministro delle Pari Opportunità farei la legge nella prima settimana!"    
[Leggi>](http://www.queerblog.it/post/11539/ignazio-marino-favorevole-alle-unioni-civili-gay-se-fossi-ministro-delle-pari-opportunita-farei-la-legge-nella-prima-settimana)**

**Basta ignorarlo, anche in Italia è l’ora del matrimonio gay.  
[Leggi>](http://www.notiziegay.com/?p=365)**

**Matrimoni gay, e subito. Anche per fermare l'escalation di aggressioni.  
[Leggi>](http://www.unita.it/culture/matrimoni-gay-e-subito-anche-br-per-fermare-l-escalation-di-aggressioni-1.311834)**

**Daria Bignardi parla di matrimoni gay su Vanity Fair.  
[Leggi>](http://www.gaywave.it/articolo/daria-bignardi-parla-di-matrimoni-gay-su-vanity-fair/32325/)**

**Reggio. Pagani: "Nozze gay, tema distante dalla realtà"   
[Leggi>](http://gazzettadireggio.gelocal.it/cronaca/2011/07/07/news/nozze-gay-tema-distante-dalla-realta-1.718991)**

**La prima volta di Cathy. **Bologna: Cahty La Torre Vicepresidente del MIT celebra un matrimonio **.   
[Leggi>](http://bologna.repubblica.it/cronaca/2011/07/07/foto/cathy_la_torre-18802190/1/)**

**Legge omotransfobia in Parlamento. Contrordine, rimandati dibattito e votazione.  
[Leggi>](http://gaynews24.com/2011/07/06/legge-omotransfobia-in-parlamento-contrordine-rimandati-dibattito-e-votazione/)**

**Bavaglio al web, l’Agcom approva il regolamento liberticida.  
[Leggi>](http://www.notiziegay.com/?p=600)**

**Alla notte della Rete.   
[Guarda ed ascolta su radio Radicale>](http://www.radioradicale.it/scheda/331340/alla-notte-della-rete)**

**Conferenza stampa del Comitato "Pari o Dispare" per fare il punto sulle tappe dell'iniziativa "Non farti scippare il futuro" e sulla manovra finanziaria del Governo Berlusconi.   
[Guarda ed ascolta>](http://www.radioradicale.it/scheda/331399/conferenza-stampa-del-comitato-pari-o-dispare-per-fare-il-punto-sulle-tappe-delliniziativa-non-farti-scipp)**

**Eutanasia del Parlamento.  
[Leggi>](http://www.ilfattoquotidiano.it/2011/07/11/e%e2%80%99difficile-immaginare-giorni-piu-squallidi/144324/)**

**Per il Vaticano i diritti gay non sono diritti umani.  
[Leggi>](http://www.diredonna.it/per-il-vaticano-i-diritti-gay-non-sono-diritti-umani-46940.html)**

**Spagna. Benedetti  preti: sacerdote omofobo sospeso perché ha una relazione gay.   
[Leggi>](http://www.queerblog.it/post/11542/benedetti-preti-sacerdote-omofobo-sospeso-perche-ha-una-relazione-gay)**

**Germania. Su avvisi ufficiali del Ministero degli esteri appariranno i rischi per i viaggiatori gay.  
[Leggi>](http://gaynews24.com/2011/07/05/germania-su-avvisi-ufficiali-del-ministero-degli-esteri-appariranno-i-rischi-per-i-viaggiatori-gay/)**

F**rancia: _Gay_ottiene consenso adozione.  
[Leggi>](http://www.corriere.it/notizie-ultima-ora/Esteri/Francia-Gay-ottiene-consenso-adozione/08-07-2011/1-A_000224982.shtml) **

**‎Croazia, nuovo Codice penale: ispirato a valori europei.  
[Leggi>](http://www.lunico.eu/2011070849834/esteri/croazia-nuovo-codice-penale-ispirato-a-valori-europei.html)**

**Russia: l'omosessualità sparirà quando non se ne parlerà più.  
[Leggi>](http://www.queerblog.it/post/11544/russia-lomosessualita-sparira-quando-non-se-ne-parlera-piu)**

**Omofobia: la vicenda dei gay in Libano, tollerati ma illegali.  
[Leggi>](http://www.blitzquotidiano.it/cronaca-mondo/omofobia-la-vicenda-dei-gay-in-libano-tollerati-ma-illegali-136028/?comments=true)**

**Seattle. Difesa assurda ad un processo: “Dio mi ha detto di stuprare e uccidere lesbiche”    
[Leggi>](http://www.giornalettismo.com/archives/132137/dio-mi-ha-detto-di-stuprare-e-uccidere-le-lesbiche/)**

**A Cuba la trans ‘traditrice’ fa infuriare il regime castrista.  
[Leggi>](http://www.dirittiglobali.it/home/categorie/33-internazionale/17354-cuba-la-trans-qtraditriceq-fa-infuriare-il-regime.html)**

**Cile: il governo riconoscerà le coppie omosessuali.  
[Leggi>](http://www.queerblog.it/post/11573/cile-il-governo-riconoscera-le-coppie-omosessuali)**

**Corte Usa ordina stop immediato a divieto su militari gay.  
[Leggi>](http://mobile.reuters.com/regional/article/idITMIE76605E20110707?edition=it)**

**USA. Il municipio di New York il 24 luglio resterà aperto anche domenica per celebrare i matrimoni gay.   
[Leggi>](http://gaynews24.com/2011/07/07/usa-il-municipio-di-new-york-il-24-luglio-restera-aperto-anche-domenica-per-celebrare-i-matrimoni-gay/)**

**Usa. I cattolici minacciano: “Niente comunione per i politici a favore del matrimonio gay” .  
[Leggi>](http://www.giornalettismo.com/archives/132021/niente-comunione-per-i-politici-a-favore-del-matrimonio-gay/)**

**California: la scuola apre le porte ai gay.  
[Leggi>](http://www.agenziaradicale.com/index.php?option=com_content&task=view&id=12563&Itemid=97)**

**California: i vescovi contro l’inclusione obbligatoria dei contributi di personalità gay nei libri scolastici.  
[Leggi>](http://gaynews24.com/2011/07/11/california-i-vescovi-contro-linclusione-obbligatoria-dei-contributi-di-personalita-gay-nei-libri-scolastici/)**

**North Carolina. “I bambini _gay_ vanno uccisi”  
[Leggi>](http://www.giornalettismo.com/archives/133327/i-bambini-gay-vanno-uccisi/)**

**La nuova Palin: “Curerò i gay e cancellerò il porno” **Michelle Bachmann, deputata del Minnesota, così promette**.  
[Leggi>](http://www.giornalettismo.com/archives/133004/la-nuova-palin-curero-i-gay-e-cancellero-il-porno/)**

STUDI, RICERCHE E MATERIALI INFORMATIVI
=======================================

**Tribunale Roma: 'Riassegnare genere anche senza operazione'.  
[Leggi>](http://www.digayproject.org/Archivio-notizie/tribunale_roma.php?c=4573&m=15&l=it)**

**Tar della Sardegna: travestirsi non è reato.  
[Leggi>](http://mediterranews.org/?p=1474)**

**E’ arrivata l’estate … e le effusioni omosessuali in spiaggia?**Riflessioni sui possibili “atti osceni in luogo pubblico” commessi nelle stazioni balneari . **  
[Leggi>](http://www.leggioggi.it/2011/07/11/e-arrivata-lestatee-le-effusioni-omosessuali-in-spiaggia/)**

**Hiv: la coppia eterosessuale e di mezza età è il nuovo 'target' di contagio.   
[Leggi>](http://www.gaynews.it/articoli/Vita-di-coppia/87055/Hiv-la-coppia-eterosessuale-e-di-mezza-eta-e-il-nuovo--target--di-contagio.html)**

**Dal 2005 si sono sposate in Spagna più di ventitremila coppie gay.  
[Leggi>](http://www.queerblog.it/post/11557/dal-2005-si-sono-sposate-in-spagna-piu-di-ventitremila-coppie-gay)**

**Gran Bretagna. La diversità delle diverse. **In Gran Bretagna le unioni omosessuali si tingono sempre più di rosa**.   
[Leggi>](http://www.west-info.eu/it/la-diversita-delle-diverse-granbretagna-unioniomosessuali-lgbt/)**

**La maggioranza dei giovani inglesi “non sa da dove vengono i bambini”.  
[Leggi>](http://blog.studenti.it/mondocurioso/la-maggioranza-dei-giovani-inglesi-%E2%80%9Cnon-sa-da-dove-vengono-i-bambini/)**

**Alice Dreger: Il destino è scritto nell'anatomia? Intersex** (**una storica dell'anatomia e studiosa di bioetica). [Guarda il video](http://www.ted.com/talks/lang/eng/alice_dreger_is_anatomy_destiny.html) ( seleziona sotto i sottotitoli in italiano)>**

**Donne e non solo. Contro gli stereotipi.  
[Ascolta su Radio Radicale>](http://www.radioradicale.it/scheda/331189/donne-e-non-solo-contro-gli-stereotipi)**

LIBRI E CULTURA
===============

**Il video del matrimonio gay tra Moreno e Francesco.  
[Leggi>](http://www.queerblog.it/post/11538/il-video-del-matrimonio-gay-tra-moreno-e-francesco)**

**Bimbo incontra coppia gay: That's funny!   
[Guarda il video>](http://video.excite.it/come-reagisce-un-bambino-a-una-coppia-gay-V80648.html)**

**Roma. Spose lesbo in passerella per i diritti dei gay.  
[Leggi>](http://affaritaliani.libero.it/sociale/sfilata_moda_spose_lesbiche110711.html)**

**Ricky Martin: ecco come ho accettato di essere gay.  
[Leggi>](http://www.tmnews.it/web/sezioni/news/PN_20110704_00046.shtml)**

**Vietata l’opera dell’autore di «Billy Elliot»: “È troppo gay”  
[Leggi>](http://www.ilgiornale.it/spettacoli/vietata_lopera_dellautore_billy_elliot__troppo_gay/06-07-2011/articolo-id=533403-page=0-comments=1)**

**Spot spagnolo sulla visibilità delle persone transessuali.  
[Leggi e guarda il video>](http://www.queerblog.it/post/11550/spot-spagnolo-sulla-visibilita-delle-persone-transessuali)**

**'The Vatican Insider' su Current TV.  
[Guarda il trailer >](tutte-le-notizie/1198.html)**

**Jean Paul Gaultier ovvero il gioco dei generi.   
[Leggi>](http://www.queerblog.it/post/11543/jean-paul-gaultier-ovvero-il-gioco-dei-generi)**

**A. E. Housman, il poeta dell'omoerotismo.   
[Leggi>](http://www.queerblog.it/post/11535/a-e-housman-il-poeta-dellomoerotismo)**

**L'omofobia del mondo dell'arte.  
[Leggi>](http://www.queerblog.it/post/11523/lomofobia-del-mondo-dellarte)**

**Nascere e morire: quando decido io? Italia ed Europa a confronto - **Presentazione del libro a cura di Monica Soldano e Gianni Baldini. **  
[Ascolta a Radio Radicale>](http://www.radioradicale.it/scheda/331395)**

**Willy Vaira,  
[Diverso sarà lei. Storie di coppie gay e non](http://www.mannieditori.it/libro/diverso-sar%C3%A0-lei), Manni editori, € 13.00**

[www.certidiritti.it](http://www.certidiritti.it/)
==================================================