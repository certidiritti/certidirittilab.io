---
title: 'LE PAROLE DEL PAPA OFFENSIVE VERSO MILIONI DI PERSONE CONVIVENTI'
date: Fri, 25 Sep 2009 12:48:53 +0000
draft: false
tags: [Comunicati stampa]
---

**PAROLE DEL PAPA OFFENSIVE VERSO MILIONI DI PERSONE CONVIVENTI. LA ROVINA E’ LA MANCANZA DI DIRITTI E DI AIUTO ALLE FAMIGLIE ALLARGATE, NON IL DIVORZIO O LA CONVIVENZA.**

_Dichiarazione di Sergio Rovasio, Segretario dell'Associazione Radicale Certi Diritti_

"Il papa è ovviamente liberissimo di dire tutto quello che vuole ma forse quando si corre il rischio di offendere milioni di persone occorrerebbe essere più cauti. Ciò che sorprende è il tono offensivo, dispregiativo rivolto da lui oggi contro le famiglie allargate, conviventi e le persone divorziate. Ancora più offensivo è sentenziare che “la convivenza e le famiglie allargate rovinano la vita di molti bambini”. Tutto ciò è davvero sorprendente, queste affermazioni diventano una sentenza prescindendo totalmente da quanto accade nelle società moderne che vedono un forte incremento delle convivenze anche gay, delle unioni civili, delle famiglie allargate di ogni tipo, anche con bambini e del raggiungimento della felicità loro e di milioni di persone. Proprio per questo occorrono d’urgenza nuove norme di regolamentazione di aiuto e sostegno alle famiglie allargate e ai conviventi".