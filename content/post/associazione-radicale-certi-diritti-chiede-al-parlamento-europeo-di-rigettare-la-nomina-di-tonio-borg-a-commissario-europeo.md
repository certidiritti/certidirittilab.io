---
title: 'Associazione Radicale Certi Diritti chiede al Parlamento Europeo di rigettare la nomina di Tonio Borg a Commissario Europeo'
date: Thu, 15 Nov 2012 09:02:42 +0000
draft: false
tags: [Europa]
---

Comunicato stampa dell’Associazione Radicale Certi Diritti

Roma, 15 novembre 2012

Tonio Borg, candidato da Malta alla Commissione europea per il portafoglio della salute e dell'ambiente, é stato sottoposto ieri pomeriggio dai membri della commissione salute ed ambiente del PE ad una audizione nel corso della quale gli eurodeputati gli hanno rivolto numerose domande sul suo portafoglio e sulle iniziative che intende prendere, al fine di testare le sue competenze. Gli eurodeputati hanno in particolare sollevato la questione di come Borg intendesse conciliare le posizioni ed iniziative politiche conservatrici e fondamentaliste precedentemente espresse e prese a Malta con il suo portafoglio, che copre anche la salute delle donne e degli omosessuali.

Gli eurodeputati hanno chiesto chiarimenti, tra l'altro, in merito alle sue posizioni sui diritti sessuali e riproduttivi delle donne, sui diritti degli omosessuali, sulla direttiva anti-discriminazione, sul rifiuto di applicare a Malta la direttiva sulla libera circolazione per quanto riguarda le coppie dello stesso sesso, sulle sue proposte di legge per introdurre il divieto di aborto nella Costituzione maltese e la criminalizzazione delle donne che praticassero aborti all'estero, sulla posizione di Malta all'ONU volta ad annacquare le posizioni europee sui diritti civili.

A seguito dell'audizione, i capigruppo dei gruppi politici si sono riuniti ieri sera per stilare una lettera di valutazione dell'audizione di Borg. Il Presidente socialista della commissione si é rifiutato di procedere a un voto in commissione, come era accaduto per Buttiglione, e come richiesto dal capogruppo verde e previsto dal regolamento del PE, convocando una nuova riunione per stamattina, nel corso della quale la lettera é stata finalizzata riportando le posizioni dei coordinatori dei gruppi politici.

I coordinatori dei liberali, dei verdi e dei comunisti si sono espressi da subito e chiaramente contro la nomina di Borg, in particolare per il portafoglio alla salute. Contrariamente a quanto successo con Buttiglione, i socialisti hanno dato il loro consenso alla nomina di Borg con qualche riserva, mentre gli altri gruppi di destra hanno dato il loro appoggio convinto a Borg. Nel corso della mattinata si é tenuta la riunione del gruppo socialista, nel corso della quale numerosi deputati scandinavi, portoghesi, francesi ed inglesi sono intervenuti contro Borg; in un comunicato stampa il gruppo SD ha annunciato di volere ottenere ulteriori rassicurazioni da Borg prima di arrivare ad una decisione definitiva la prossima settimana. Il voto del PE avrà luogo, senza dibattito, con voto segreto e sulla base di una risoluzione sulla nomina di Borg alla Commissione, mercoledi prossimo 21 novembre.

L’Associazione Radicale Certi Diritti si appella agli eurodeputati italiani ed al Parlamento europeo, assieme alle ONG per i diritti delle donne, degli omosessuali, degli immigrati, gli umanisti, i Catholics for Choice, perché votino contro la nomina di Tonio Borg alla Commissione europea.