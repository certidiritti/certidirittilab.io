---
title: 'Milano approvi registro delle unioni civili. Lettera di Agapo fuori dalla realtà'
date: Thu, 19 Jul 2012 13:05:09 +0000
draft: false
tags: [Diritto di Famiglia]
---

Comunicato stampa di Agedo, Arcigay Milano, Associazione radicale Certi Diritti, Nuova Proposta.

Milano, 19 luglio 2012

Alcuni giornali chiaramente in opposizione alla giunta Pisapia (“Il Giornale” e “Libero”) o di estrazione cattolica (“Avvenire”) hanno dato ampia risonanza alla lettera di un'associazione nata solo nel 2006, quasi inesistente nel tessuto sociale, e che opera nella semiclandestinità grazie alla connivenza di comunità religiose e all'appoggio di referenti integralisti e bigotti nelle istituzioni lombarde che approvano politiche e attività di discriminazione delle persone omosessuali. Gli autori di questa lettera contro il registro delle unioni civili a Milano dimostrano in poche righe un concentrato di omofobia e pregiudizio incredibile, camuffato da argomentazioni ben strutturate nella retorica ma senza nessun fondamento legittimo nella logica. Si tratta di una completa distorsione della realtà, della verità e delle fonti culturali usate per sostenere tesi senza senso.

Quest’associazione confonde volutamente le acque presentandosi come rappresentante dei genitori delle persone omosessuali, ma senza rendere noti il numero di soci e di sedicenti genitori che la compongono. Sarebbe bello sapere chi e' il presidente di questa associazione, visto che nessun comunicato e' firmato, e vorremmo anche sapere quali figli omosessuali loro rappresentano.

I genitori di omosessuali rappresentati da AGEDO, storica Associazione dei Genitori di Omosessuali, che dal 1992 pubblica tutti i resoconti delle attività associative ed è stata insignita dell'Ambogino, sono invece perfettamente d’accordo nel chiedere il pieno accesso al matrimonio civile per gli omosessuali e nel sostenere la delibera per le unioni civili in discussione a Milano, oltre a qualsiasi iniziativa legislativa orientata all’uguaglianza di gay, lesbiche, bisessuali, transessuali e intersessuali.

Ricordiamo che questa associazione è già stata diffidata per tre volte dalla ASL di Milano per aver usato impropriamente e oltre i tempi consentiti i loghi della ASL e di Regione Lombardia in connessione al servizio telefonico ‘Amico segreto’ (“Il Vostro Quotidiano, 22-6-12). Questo servizio smaschera tra l’altro l’Associazione che si muove chiaramente nell’ottica della guarigione dell’omosessualità, contrariamente al fatto che sin dal 1993 l’OMS (Organizzazione Mondiale Sanità) non considera più l’omosessualità una patologia.

Che credibilità possono avere i membri di una tale associazione? Le loro pseudo verità intrise di dogmi religiosi e la continua considerazione dell'omosessualità come "un problema" e qualcosa che si può curare a suon di preghiere e genuflessioni non possono avere a Milano nessuno spazio se non l'angolo della vergogna. I genitori di omosessuali rappresentati da AGEDO, proprio perché amano i loro figli, non li consideriamo qualcosa di difettoso da riparare, ma un bene prezioso legato alla diversità. Il Comune approvi il registro e condanni sempre l’omofobia in tutte le sue forme.

  
AGEDO  
CIG Arcigay Milano  
Associazione Radicale Certi Diritti  
Nuova Proposta, donne e uomini omosessuali cristiani