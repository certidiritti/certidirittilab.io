---
title: 'OMOSESSUALITA'' E PSICOTERAPIE, CONVEGNO INTERNAZIONALE A ROMA'
date: Fri, 06 Nov 2009 15:29:00 +0000
draft: false
tags: [Comunicati stampa]
---

Omosessualità e psicoterapie

**_Convegno internazionale_**

**Roma, Biblioteca Nazionale Centrale, Viale Castro Pretorio 105**

**Sabato 7 novembre 2009 ore**

**Sabato 7 novembre 2009, a partire dalle ore 9.00, presso la Sala Conferenze della Biblioteca Nazionale Centrale di Roma, in viale Castro Pretorio 105, si svolgerà il Convegno Internazionale "Omosessualità e Psicoterapie".**

Il convegno è promosso dalla II Scuola di Specializzazione in Psicologia Clinica della Facoltà di Psicologia 1 della Sapienza Università di Roma, ha ricevuto i patrocini del Ministero del Lavoro, della Salute e delle Politiche Sociali, della Regione Lazio, della Provincia e del Comune di Roma, del Consiglio Nazionale dell'Ordine degli Psicologi e la collaborazione del Consiglio Regionale del Lazio, dell’Ordine degli Psicologi del Lazio e dell’Associazione Italiana di Psicologia

Psicologi e psichiatri, italiani e stranieri, coordinati da Vittorio  Lingiardi, Direttore della II Scuola di Specializzazione in Psicologia Clinica, saranno chiamati a confrontarsi sulle cosiddette “terapie riparative”, cioè quelle “terapie” psicologiche e comportamentali che si propongono, spesso su richiesta del paziente stesso, di “modificare” l’orientamento omosessuale in eterosessuale.

Saranno presenti due importanti relatori stranieri: lo psichiatra americano Jack Drescher del DSM-V Workgroup sui disturbi sessuali e dell'identità di genere, e la psichiatra inglese Annie Bartlett che presenterà i risultati di un’importante ricerca sull’atteggiamento degli operatori della salute mentale nei confronti dei clienti/pazienti omosessuali. Gli altri relatori sono Gustavo Pietropolli Charmet, Tonino Cantelmi, Paolo Rigliano.

Verrà proiettato il documentario _“Abomination”_, inedito in Italia, con testimonianze di “ex-pazienti” delle “terapie riparative” e interviste rilasciate da clinici e ricercatori dell’American Psychiatric Association e dell’American Psychologycal Association.

Il convegno, che ha ricevuto oltre 1500 richieste di partecipazione, si concluderà con una tavola rotonda a cui parteciperanno anche altri esperti sulle tematiche affrontate nel convegno.

_Info_

_Nicola Nardelli_

_[organization.office@gmail.com](mailto:organization.office@gmail.com)_

_tel 349.0629725_