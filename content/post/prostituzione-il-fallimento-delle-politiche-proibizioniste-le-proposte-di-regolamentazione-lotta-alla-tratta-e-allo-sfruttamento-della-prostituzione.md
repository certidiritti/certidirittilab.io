---
title: 'Prostituzione: il fallimento delle politiche proibizioniste. Le proposte di regolamentazione. Lotta alla tratta e allo sfruttamento della prostituzione'
date: Wed, 14 Jul 2010 18:31:00 +0000
draft: false
tags: [Senza categoria]
---

Video della conferenza "Prostituzione: il fallimento delle politiche proibizioniste. _Le proposte di regolamentazione. Lotta alla tratta e allo sfruttamento della prostituzione"_

L’**antiproibizionsimo** e la **regolamentazione** della prostituzione come strumenti per governare il fenomeno: contro i tabù, le ipocrisie  e l’illegalità che sempre più si affermano nel nostro Paese.

* * *