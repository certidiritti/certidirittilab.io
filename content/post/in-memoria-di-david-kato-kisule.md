---
title: 'In memoria di David Kato Kisule'
date: Tue, 15 Feb 2011 10:37:09 +0000
draft: false
tags: [Africa]
---

Video della commemorazione di David Kato Kisule al teatro Valle di Roma che Certi Diritti ha organizzato insieme alla compagnia Teatro dell'Elfo. Con Elio De Capitani, Elena Russo Arman, Cristina Crippa, Ida Marinelli, Cristian Giammarini, Edoardo Ribatto, Fabrizio Matteini, Umberto Petranca.