---
title: 'Newsletter 27 Aprile'
date: Sat, 28 Apr 2012 04:05:42 +0000
draft: false
tags: [Politica]
---

![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)

Il Ministero della Sanità censura la parola ‘preservativo’ nella traduzione dell'opuscolo dell'Oms. Denuncia della Lila e **[interrogazione parlamentare dei Radicali >](gravissima-denuncia-della-lila-censurata-la-parola-preservativo-nella-traduzione-dellopuscolo-delloms)**

**[Anche Alemanno fa l'abusivo](http://espresso.repubblica.it/dettaglio/anche-alemanno-fa-labusivo/2179211)**: il sindaco di Roma aveva promesso mano dura con chi appende manifesti illegali sui muri della città. Bene: oggi a violare le regole è proprio lui.

**[Gay e nazismo: Giovanardi ha perso altra occasione di stare zitto, evidentemente ha problemi irrisolti](gay-e-nazismo-giovanardi-ha-perso-altra-occasione-di-stare-zitto-evidentemente-ha-problemi-irrisolti)**

**Certi Diritti che le coppie conviventi non sanno di avere’ è il libro di Stampa Alternativa più venduto nel mese di marzo.** Riccardo Cristianosu [liberi.tv](http://liberi.tv/)[**intervista** **Gabriella Friso**](http://www.liberi.tv/webtv/2012/04/22/video/intervista-gabriella-friso-coautrice-del-libro-certi-diritti)del Direttivo Certi Diritti e coautrice del libro edito da Stampa Alternativa. **[Acquista online >](certi-diritti-che-le-coppie-conviventi-non-sanno-di-avere)**

**[Ragazzo malato di sifilide lasciato senza cura. Il farmaco si trova solo in Vaticano >](ragazzo-di-24-anni-lasciato-senza-assistenza-medica-per-curare-la-sifilide-il-farmaco-non-si-trova-in-italia-ma-in-vaticano)**

**[Parlamento Europeo approva la Relazione Howitt sui Diritti Umani. Due paragrafi sull'impegno contro discriminazioni verso persone lgbte > ](parlamento-europeo-approva-la-relazione-howitt-sui-diritti-umani-due-paragrafi-sullimpegno-contro-discriminazioni-verso-persone-lgbte)**  
**  
LA LEGALIZZAZIONE DELLA PROSTITUZIONE  
**Sabato scorso si è svolta a Roma la **Conferenza nazionale sulla ‘Legalizzazione della Prostituzione’, **organizzata dall'Associazione Radicale Certi Diritti, dall'ufficio nuovi diritti della Cgil e dal Comitato per i diritti civili delle Prostitute.** [L’audiovideo su Radio Radicale >](http://www.radioradicale.it/scheda/350825)**

**[Sono una puttana perché ...](sono-una-puttana-perche) **è l'intervento di Tenera Valse alla conferenza nazionale per la legalizzazione della prostituzione. ** [Guarda il video >](http://www.youtube.com/watch?v=MOopWAgogJw&feature=plcp&context=C4fb8ebeVDvjVQa1PpcFNK8OLpgNfmJ6wCpptCJs8tuDmJQwjj-Aw%3D)**

**[FIRMA L’APPELLO PER LA DECRIMINALIZZAZIONE DELLA PROSTITUZIONE>](legalizzazione-prostituzione)**

**Su “[liberi.tv](http://liberi.tv/)” nello [Spazio Certi Diritti](http://www.liberi.tv/webtv/spazio/spazio-certi-diritti), interviste  a margine del della Conferenza nazionale su Legalizzazione della prostituzione il 21 aprile 2012**  
**[Le retate nei confronti delle lucciole? Uno spreco di energie:](http://www.radicali.it/20120422/retate-nei-confronti-delle-lucciole-uno-spreco-di-energie-comune-di-perugia-ne-prenda-atto-) **il Comune di Perugia ne prenda atto e cambi strategia

**UNIONI CIVILI IN PARLAMENTO  
**Il 19 aprile scorso è partito in Commissione Giustizia della Camera dei Deputati l'esame di 8 proposte di legge sulle unioni civili escludendo quelle riguardanti il matrimonio tra persone dello stesso sesso e la nostra riforma complessiva del diritto di famiglia. L’Associazione Radicale Certi Diritti chiede che il **[Parlamento discuta anche di matrimonio tra persone dello stesso sesso, unico traguardo per superamento diseguaglianze > ](unioni-civili-parlamento-discuta-anche-di-matrimonio-tra-persone-dello-stesso-sesso-unico-traguardo-per-superamento-diseguaglianze)**  
Se il Forum Famiglie minaccia di portare un milione di persone in piazza,** [il Parlamento non obbedisca a diktat clericali.](forum-famiglie-minaccia-di-portare-un-milione-in-piazza-parlamento-lavori-sereno-su-unioni-civili) **E' ora di riconoscere i diritti delle coppie conviventi, lo chiede l'Europa. 

Grazie alla disponibilità di Radio Radicale da questa settimana puoi ascoltare e scaricare il podcast della rassegna stampa sui temi di attualità che riguardano più da vicino le nostre campagne con un sguardo su Europa. La rassegna è curata da Gabriella Friso e il commento è di Yuri Guaiana, segretario dell’associazione.

**Aiutaci a cambiare questo paese.  
[Ora puoi iscriverti o contribuire all’associazione radicale Certi Diritti con una semplice telefonata. Scopri come >](iscriviti)**