---
title: 'CERTI DIRITTI MILANO: PROSSIMO INCONTRO GIOVEDI'' 21 GENNAIO'
date: Wed, 13 Jan 2010 16:54:35 +0000
draft: false
tags: [Senza categoria]
---

Carissimi tutti,  vi invitiamo al primo incontro di Certi Diritti Milano del 2010: Giovedì 21 Gennaio, alle ore 21.  Dove? Come sempre, in via Malachia de Taddei 10 (suonare "radical" al citofono).  
  
1) Ci organizzeremo per il Congresso Nazionale di Certi Diritti, a Firenze, a fine mese. E' momento fondamentale per la vita dell'associazione e la partecipazione a Certi Diritti.  Per chi vorrà partecipare (siamo già molti, da Milano: Francesco, Luca, Yuri, Avi, Paolo, Emanuele, Barbara, Gabriella e altri ancora) cercheremo di organizzarci per il viaggio. Chi invece non potrà partecipare può comunque suggerirci eventuali argomenti o idee che potremmo proporre all'assemblea  
2) Ci saranno le coppie che hanno partecipato ad Affermazione Civile e vi parleranno della manifestazione di Aprile. Ormai ci sono idee più chiare e faranno di tutto per coinvolgere la vostra voglia di fare.  
3) Certi Diritti sta iniziando a crescere: vi racconteremo cosa stiamo iniziando a fare insieme ad Emanuele e Paolo  
4) Faremo un piccolo riepilogo sullo sciopero della fame dei due ragazzi di Savona (speriamo che per il 21 sia terminato...)   
5) Ovviamente tutte le varie ed eventuali possibili...  
  
Invitate i vostri amici e spargete la voce.  Vi aspettiamo! Gian Mario, Luca, Francesco, Yuri e Avi