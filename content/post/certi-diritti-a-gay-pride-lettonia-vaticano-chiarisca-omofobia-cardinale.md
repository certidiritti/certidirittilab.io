---
title: 'CERTI DIRITTI  A GAY PRIDE  LETTONIA. VATICANO CHIARISCA OMOFOBIA CARDINALE'
date: Fri, 30 May 2008 13:36:19 +0000
draft: false
tags: [Comunicati stampa]
---

CERTI DIRITTI CHIEDE AL VATICANO CHIARIMENTI SU GRAVI AFFERMAZIONI OMOFOBICHE CARDINALE LETTONE, SAREMO ANCHE NOI AL GAY PRIDE DI RIGA.  

Roma, 30 maggio 2008  
  

  
L’Associazione radicale Certi Diritti partecipa, con il responsabile per le questioni europee dell’Associazione e funzionario liberale al PE, Ottavio Marzocchi al Gay Pride della capitale della Lettonia, Riga. La manifestazione si svolgerà questo week end e culminerà domani, sabato 31 maggio, con una marcia nel centro della città. Insieme a Ottavio Marzocchi parteciperà all’iniziativa la deputata europea olandese del partito radicale D66 nel quadro della campagna per i diritti LGBT "ALDE 4 Equality" del gruppo Liberale e Democratico del Parlamento europeo. Al Gay Pride partecipano anche le associazioni LGBT europee e quelle per i diritti umani, tra le quali Amnesty International, che monitoreranno in particolare l'andamento della manifestazione e la protezione garantita dalle autorità locali e nazionali. L’Associazione Certi Diritti chiede urgentemente alle gerarchie vaticane di chiarire la loro posizione rispetto alle affermazioni violente ed omofobiche del cardinale lettone Janis Pujats, verificando al più presto se parla a nome del Vaticano o suo personale. In una lettera aperta al Governo firmata dalla conferenza dell'arcidiocesi di Riga, il cardinale omofobico ha infatti chiesto alle autorità di dichiarare la marcia illegale e di proibirla, perché contraria "alla morale ed alla famiglia". La lettera paragona gli omosessuali agli alcolizzati, ai drogati ed alle altre “pseudo-minoranze con inclinazioni immorali”,  critica gli "stranieri" che parteciperanno al gay pride al fine di "propagandare pubblicamente perversioni”, denunciando “che tale vergognoso comportamento sarà pure protetto dalla polizia, umiliata perché forzata a stare vicino ai gay durante la marcia". Alcuni esponenti politici si sono associati all'appello omofobico, tra cui il  vice Sindaco di Riga insieme ad altri di partiti di destra e di sinistra. Il Direttore esecutivo del Comune di Riga ha richiamato i politici e i religiosi ai principi della democrazia e dello Stato di diritto, affermando che "non si possono basare le decisioni sulle emozioni, perché ci sono leggi e norme da rispettare". L'organizzazione omofobica "no pride" anche quest'anno cercherà di sabotare la marcia ostacolando, intimorendo ed aggredendo i partecipanti.