---
title: 'CALCIO E OMOSESSUALITA? LETTERA AL PICCOLO DI TRIESTE DI CERTI DIRITTI'
date: Thu, 15 Jan 2009 16:28:04 +0000
draft: false
tags: [Comunicati stampa]
---

Il quotidiano Il Piccolo di Trieste ha pubblicato mercoledì 13 gennaio 2009 la seguente lettera di Clara Comelli, Presidente di Certi Diritti

Il calcio e l'omosessualità.

In relazione all'articolo apparso nelle pagine sportive di questo giornale nei giorni scorsi dal titolo  
"Nessun gay nel calcio dilettantistico triestino" l'Associazione Radicale Certi Diritti invita tutti i giocatori e le giocatrici di pallone che, palesando all'interno delle rispettive associazioni calcistiche il proprio orientamento sessuale gay o lesbico, dovessero subire discriminazioni o, peggio - come minacciato (così come riportato nell'occhiello dell'articolo) dal presidente del San Luigi -, essere cacciati dalla società, a rivolgersi alla nostra Associazione. Sarà premura e dovere di Certi Diritti intraprendere le opportune vie legali per denunciare gli eventuali atti di omofobia; è necessario infatti, da parte nostra, garantire azioni che tutelino i diritti fondamentali della persona, tra cui il rispetto per l'orientamento sessuale di ciascuno, e che in questo caso ristabiliscano un corretto e rispettoso comportamento tra dirigenti e giocatori.  
Quanto alle dichiarazioni contenute nello stesso articolo ci limitiamo invece a sottolinearne, nel pieno rispetto della libertà di espressione di ognuno, i toni pervasi di omofobia e ignoranza, nel senso latino del non conoscere le cose. Ci appare anacronistica, oltre che assolutamente fuori luogo, la definizione di omosessualità come malattia o vizio. E così il pensare, nonostante un sano atteggiamento di apertura, che l'essere omosessuale sia una cosa "che capita nella vita", come si trattasse di un evento negativo del quale sarebbe meglio non essere protagonisti, ci lascia francamente perplessi.  
L'omosessualità è una normalissima variante dell'orientamento sessuale, così come stabilito in più occasioni dall'Organizzazione Mondiale della Sanità. Una variante da vivere con assoluta tranquillità anche su un campo di calcio. Semmai "malati" sono coloro che la strumentalizzano in maniera pregiudizievole per chissà quale loro problema personale. Dalla nostra parte abbiamo la medicina, la scienza, la storia e, per fortuna , anche il diritto.  
Da ultimo, un invito: a chi ama andare a fare il tifo per le squadre di calcio e ritiene, come noi, che le persone omosessuali abbiano tutto il diritto di essere rispettate e non discriminate, come ogni altro individuo, suggeriamo di portare con sé degli striscioni con su scritto "meno omofobia e più spirito sportivo". Certi Diritti si dichiara inoltre pubblicamente disponibile, fin d'ora, all'organizzazione di manifestazioni che contrastino il dilagare dell'omofobia nel calcio e, più in generale, nel mondo dello sport.

Clara Comelli  
Presidente Associazione Radicale Certi Diritti  
[www.certidiritti.it](http://www.certidiritti.it/)

[](http://www.certidiritti.it)