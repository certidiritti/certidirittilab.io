---
title: 'Rossodivita commemora Kato al Consiglio regionale del Lazio'
date: Wed, 02 Feb 2011 13:57:26 +0000
draft: false
tags: [Comunicati stampa]
---

Il consigliere regionale Giuseppe Rossodivita, ha commemorato nell'aula del Consiglio Regionale del Lazio, la figura di David Kato Kisule, esponente dei diritti civili ucciso in Uganda.﻿

Roma, 2 febbraio 2011 

Stamane, in apertura dei lavori del Consiglio Regionale del Lazio, il Consigliere regionale del Gruppo Lista Bonino Pannella, Federalisti europei, Giuseppe Rossodivita, ha commemorato la figura di David Kato Kisule, l’esponente dei diritti civili barbaramente assassinato in Uganda perché omosessuale. David Kato, iscritto all’Associazione Radicale Certi Diritti era stato a Roma lo scorso novembre e avrebbe dovuto partecipare ai lavori del 39° Congresso del Partito Radicale Nonviolento che si terranno a Chianciano dal 17 al 20 febbraio.

Il Vice - Presidente del Consiglio Regionale, Raffaele D’Ambrosio, che presiede i lavori, ha espresso la sua solidarietà per la scomparsa di David Kato Kisule.

Il Consigliere Giuseppe Rossodivita ha invitato ai lavori del Congresso radicale tutti i colleghi consiglieri, in particolare i membri dell’intergruppo sul Tibet.

**Di seguito il testo integrale dell’intervento svolto in aula:**

“Mercoledì scorso è stato barbaramente assassinato in Uganda, David Kato Kisule, esponente africano dei diritti civili e umani

Grazie ell’Ong Non c’è Pace Senza Giustizia, lo scorso novembre, David era stato ospite a Roma dei lavori del IV Congresso dell’Associazione Radicale Certi Diritti. Aveva raccontato delle persecuzioni  e di veri e propri linciaggi, di cui sono vittime le persone lesbiche e gay in Uganda, promosse da organizzazioni del fondamentalismo religioso.

Il 16 ottobre 2010 la rivista ugandese Rolling Stones pubblicò in prima pagina le foto di 100 attivisti omosessuali (o presunti tali) ugandesi chiedendone l’arresto. Tra le 100 foto vi era anche quella di David Kato Kisule, l’esponente più noto del movimento. Il clima di odio contro le persone omosessuali è alimentato dal fondamentalismo religioso dei predicatori evangelisti che trovano terreno molto fertile tra la popolazione che vive nella miseria e nella disperazione.

Il Parlamento Europeo, grazie alla campagna internazionale di Non c'è Pace Senza Giustizia, aveva approvato una Risoluzione di condanna nei confronti dell'Uganda. David era stato anche audito dalla Sottocommissione Diritti Umani del Parlamento Europeo dopo aver partecipato  a Roma al Congresso di Certi Diritti.

Lo scorso 7 gennaio l’Alta Corte ugandese aveva condannato la rivista per violazione della legge sulla privacy, difendendo le persone gay perseguitate.

David Kato era stato invitato ai lavori del 39° Congresso del Partito Radicale Nonviolento, transnazionale e transpartito che si svolgeranno a Chianciano dal 17 al 20 febbraio prossimi.

Ovviamente, colgo l’occasione di questa triste notizia per invitare ai lavori tutti i colleghi consiglieri. In particolare ai colleghi membri dell’Intergruppo sul Tibet.  Sarà l’occasione per incontrare esponenti dei diritti civili e umani di tutto il mondo e anche noi consiglieri regionali della Lista Bonino Pannella, Federalisti europei, saremo impegnati sui temi delle lotte nonviolente insieme a parlamentari italiani e di altri paesi del mondo, di diversi gruppi politici.

La figura di David Kato Kisule è stata ricordata in tutto il mondo, dal Presidente Usa Barack Obama al Sindaco di Parigi, dal Presidente del Paralmento Europeo a leader dei radicali Marco Pannella.

Anche noi vogliamo onorare la sua memoria con il nostro impegno per la difesa dei diritti civili di molti cittadini del Lazio e ci auguriamo che il Consiglio regionale comprenda di quanti cittadini vivono ancora oggi molte discriminazioni di ogni genere”.