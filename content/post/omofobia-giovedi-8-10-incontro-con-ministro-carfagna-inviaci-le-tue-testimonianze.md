---
title: 'OMOFOBIA: GIOVEDI'' 8-10 INCONTRO CON MINISTRO CARFAGNA, INVIACI LE TUE TESTIMONIANZE'
date: Fri, 25 Sep 2009 14:00:33 +0000
draft: false
tags: [Comunicati stampa]
---

Grazie ai solleciti dell'on. deputata del Pd Paola Concia, il Ministro Mara Carfagna, da lei incontrata nell'ambito della lotta all'omofobia, ci ha fatto sapere che incontrerà le Associazioni lgbt su questo tema giovedì 8 ottobre pomeriggio.

Facciamo appello a tutti coloro che sono state vittime di atti di omofobia a inviarci le loro storie a

info@certidiritti.it

Raccoglieremo tutte le testimonianze che ci giungeranno e le consegneremo insieme alle nostre proposte al Ministro in persona.

Forza e coraggio!!!