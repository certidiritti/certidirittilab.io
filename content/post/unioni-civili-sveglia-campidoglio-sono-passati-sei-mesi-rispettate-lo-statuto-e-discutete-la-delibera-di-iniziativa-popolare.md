---
title: 'Unioni civili, sveglia Campidoglio! Sono passati sei mesi. Rispettate lo statuto e discutete la delibera di iniziativa popolare'
date: Thu, 15 Nov 2012 17:49:41 +0000
draft: false
tags: [Diritto di Famiglia]
---

Dichiarazione di Riccardo Magi, segretario di Radicali Roma, e Giacomo Cellottini, tesoriere dell'Associazione Radicale Certi Diritti

Roma, 15 novembre 2012.

Oggi abbiamo interrotto i lavori del consiglio comunale facendo suonare 20 sveglie fino ad essere espulsi dall'aula per ricordare al presidente dell'assemblea e ai consiglieri che oggi si svolgeva l'ultima seduta utile per discutere la delibera sul "Riconoscimento delle unioni civili e sostegno alle nuove forme familiari", sottoscritta da oltre 7000 cittadini e depositata lo scorso 17 maggio.

La calendarizzazione e la discussione delle delibere di iniziativa popolare non sono atti rimessi alla discrezionalità del presidente dell'assemblea o dei gruppi consiliari in base a considerazioni di opportunità o convenienza politica, sono invece atti obbligati dallo statuto in tempi ben precisi e non differibili.  
  
Fare carta straccia dello statuto - in particolare rispetto al diritto di partecipazione dei cittadini alla vita pubblica - è un'abitudine di questa assemblea che oggi si ripete ancora una volta. Porteremo avanti questa battaglia di legalità fino a quando i consiglieri eletti dai cittadini romani non rispetteranno queste regole fondamentali.

La delibera - che porterebbe Roma a riconoscere diritti alle famiglie non matrimoniali come hanno già fatto Torino, Napoli, Milano e altre città italiane - non prevede l'istituzione di un semplice registro delle unioni civili che senza politiche e programmi di intervento ha un valore puramente simbolico, bensì chiede a Roma Capitale di parificare le coppie sposate e quelle conviventi, cioè le famiglie fondate sul matrimonio e le famiglie di fatto, negli ambiti di competenza comunale garantendo ad esempio parità di accesso a tutte le attività e servizi comunali. Il Regolamento per il riconoscimento delle unioni civili che viene proposto con la delibera impegna Roma Capitale a superare ogni forma di discriminazione nei confronti delle famiglie di fatto.