---
title: 'Regolamentare la prostituzione'
date: Sat, 15 Feb 2014 17:49:31 +0000
draft: false
---

[![Regolamentare la prostituzione](http://www.certidiritti.org/wp-content/uploads/2014/02/Regolamentare-la-prostituzione-1024x515.png)](http://www.certidiritti.org/wp-content/uploads/2014/02/Regolamentare-la-prostituzione.png)La legge Merlin deve esse superata, perché di fatto ha criminalizzato l’attività di prostitute e prostituti pur non prevedendo il reato specifico ma penalizzando ogni tipo di attività connessa. La nuova legge di regolamentazione della prostituzione deve: - partire dalla salvaguardia dei principi di libertà e autodeterminazione di chi sceglie di prostituirsi, garantendone sicurezza e condizioni di vita; - potenziare e rilanciare iniziative contro ogni forma di riduzione o eliminazione della libertà personale, di tratta delle persone e di induzione alla prostituzione di persone minori o incapaci di intendere e di volere.