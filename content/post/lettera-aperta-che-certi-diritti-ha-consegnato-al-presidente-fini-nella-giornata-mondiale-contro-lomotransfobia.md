---
title: 'Lettera aperta che Certi Diritti ha consegnato al presidente Fini nella Giornata MOndiale contro l''omotransfobia'
date: Thu, 17 May 2012 08:42:56 +0000
draft: false
tags: [17 maggio, FINI, OMOFOBIA, Politica, transfobia]
---

** 17 maggio 2012**

**Giornata mondiale contro l'omofobia e la transfobia**

** CONTRO L'OMOFOBIA E LA TRANSFOBIA CI VOGLIONO I DIRITTI**

  
 LETTERA APERTA

 AL PRESIDENTE DEL CONSIGLIO,

 AL PRESIDENTE DEL SENATO,

 AL PRESIDENTE DELLA CAMERA

 A I MINISTRI E PARLAMENTARI ITALIANI

Questo breve documento di proposta, e non di protesta, si rivolge alle Istituzioni ed alla classe dirigente di questo Paese affinché, senza indugi e con determinazione si passi dalle importanti dichiarazioni contro omofobia e transfobia che abbiamo ascoltato e certamente ancora ascolteremo, ad altrettanto importanti fatti concreti. 

A più di 40 anni dalla nascita del Fuori! e dalle battaglie radicali per conquistare dignità e sicurezza per le persone omosessuali e transessuali, il Paese deve trovare la forza di tradurre il sentimento comune in leggi e programmi che rimuovano le cause ( anche quelle culturali e sociali) che determinano violenze e discriminazioni. Proprio come la Costituzione italiana recita dal 1948. Proprio come il Presidente Napolitano ha affermato ormai in molte occasioni. 

La natura dei sentimenti omofobi e transfobici è tale per cui non bastano le parole per combatterli. Fate attenzione: non avete notato anche voi che sulle labbra del più deciso dei razzisti italiani c'è sempre la fatidica frase "non ho nulla contro gli omosessuali" o peggio ancora "ho molti amici omosessuali" magari condita da qualche compassionevole riflessione nei confronti dei casi più efferati di violenza e discriminazione? Eppure quando si parla di dignità e diritti delle persone omosessuali e transessuali tutte le scuse diventano utili per negare una semplice constatazione, che il diritto internazionale ha acquisito da tempo, ovvero i diritti delle persone lgbti (lesbiche. gay, bisessuali, transessuali e intersessuali) sono diritti umani. E che la mancanza di riconoscimenti e tutele è un grave attacco a questi diritti e violazione delle Dichiarazioni internazionali che li proteggono.

Alla luce del necessario impegno comune per trovare soluzioni concrete a problemi concreti, e forti della nostra storia radicale, vi proponiamo un breve elenco delle principali questioni e delle relative possibili soluzioni, che crediamo siano il passo necessario che questo Paese deve fare per tradurre le parole in fatti. Ovvero perché la celebrazione della Giornata mondiale contro l'omofobia sia un appuntamento di lavoro e non solo di parole.

Quello che vi proponiamo non è il libro dei sogni, ma una serie di riforme puntuali e realizzabili che con la volontà e la determinazione di tutti e tutte possono diventare l'agenda di lavoro di un Parlamento e di un Governo realmente consapevoli delle riforme necessarie.

**Chi si occupa dei diritti delle persone lgbti in Italia?**

I diritti delle persone lgbti, in quanto diritti umani, devono esser salvaguardati e promossi, con gli stessi strumenti normativi e istituzionali che l'Europa ci indica per tutte le altre forme di discriminazione. 

In Italia, a parte l'importante lavoro dell'UNAR e la nascita di iniziative interessanti come l'OSCAD, manca un solido impianto normativo e organizzativo che dia strumenti all'azione antidiscriminatoria. Per questo riteniamo che si debba lavorare senza indugi e sulla scorta di quanto nei principali paesi europei già accade, verso una definizione chiara delle competenze e degli strumenti di intervento: 

*   per quanto riguarda il contrasto e l'assistenza alle vittime di discriminazione e maltrattamenti in tutti gli ambiti della vita, è necessario un organismo terzo, dotato di risorse e poteri capaci di intervenire, su tutte le forme di discriminazione, e comunque non meno delle sei indicate dall'articolo 19 del Trattato per il Funzionamento dell'Unione Europea. Soluzione possibile anche attraverso la trasformazione dell'UNAR in organismo autonomo, con poteri, dotazione organica e risorse economiche adeguate ai parametri europei; 
*   per quanto riguarda la prevenzione e ogni altra attività di pari opportunità per tutti e tutte è necessario definire chiaramente le competenze degli organismi centrali, a partire dal Dipartimento per le Pari Opportunità. Stabilendo forme di coordinamento stabili con altri Dipartimenti. 

**Diritto di famiglia** 

Al contrario di quanto molti esponenti politici dichiarano, la Corte costituzionale, e a seguire la Corte di Cassazione, interpretando la nostra carta costituzionale e il diritto comunitario, hanno affermato senza ombra di dubbio che le unioni formate da persone dello stesso sesso sono famiglie e devono poter godere degli stessi diritti e degli stessi doveri delle unioni matrimoniali. Entrambe le Corti non hanno mai sostenuto che in Italia c'è un divieto costituzionale al diritto al matrimonio tra persone dello stesso sesso, limitandosi a sollecitare il Parlamento ad intervenire con una legge, né escludendo né richiedendola in modo esplicito, la stessa estensione dell'istituto matrimoniale. 

Noi riteniamo che non ci siano motivi fondati per violare così palesemente il diritto di uguaglianza nei confronti delle persone omosessuali che intendono godere del diritto al matrimonio. E comunque pensiamo che qualunque provvedimento il Parlamento intenda adottare non può non affrontare, e risolvere: 

*   la questione del valore dell'unione non matrimoniale in sé riconosciuto attraverso un atto pubblico, non solo come estensione dei diritti individuali; 
*   la questione dei diritti e dei doveri derivanti dalla condivisione di un patrimonio familiare, comprese le questioni fiscali e tributarie; 
*   la questione del riconoscimento dell'omogenitorialità, e quindi dei diritti e dei doveri dei genitori sociali e non solo dei genitori biologici, nei confronti dei figli e delle figlie che vivono con la coppia. 

Anche in questo caso operare per la riforma del diritto di famiglia per tutelare, e insieme riconoscerne i doveri, delle famiglie omosessuali senza tener conto di una urgente e necessaria riforma complessiva, è miope. Può invece essere l'occasione perché si risolvano questioni che tutti riteniamo urgenti ma che dal 1975 non sono mai state realmente affrontare, come la piena uguaglianza tra figli naturali e figli legittimi (come la costituzione prevede), la riduzione dei tempi e la semplificazione delle procedure per il  divorzio, l'estensione dell'adozione ai singoli e alle singole. 

La nostra associazione, peraltro, continuerà l'azione di affermazione civile che è stata alla base della importante sentenza della Corte costituzione (n. 183 del 2010) accompagnando e sostenendo quelle coppie che intendono chiedere ai tribunali i diritti che il Parlamento italiano nega. 

**Omofobia e transfobia** 

Omofobia e transfobia non si combattono solo con il codice penale, ma con l'informazione, e l'educazione prive di derive ideologiche. Quindi è necessario che vengano formalizzati, potenziati,ed estesi i programmi di intervento contro sessuofobia e bullismo non solo nelle scuole ma in tutte le realtà comunitarie (associazionismo sportivo e culturale, caserme, ordini professionali, seminari, collegi). 

D'altra parte l'estensione della Legge Mancino anche all'orientamento sessuale ed alla identità di genere (e magari anche ad altri motivi di potenziale discriminazione previsti dalla carta europea dei diritti fondamentali) darebbe maggiore forza all'azione dei tribunali, anche attraverso l'estensione delle pene alternative che puntino al recupero del condannato. 

**Riforma della Legge 164/1982 **

La legge che riconosce la rettifica dello stato anagrafico per le persone transessuali fu una vittoria dei radicali e del Movimento Italiano Transessuali che la concepirono, sostennero e difesero contro i tentativi di stravolgimento. 

Purtroppo la sua applicazione, in assenza di interventi di chiarimento, ha obbligato le persone transessuali a costi, sia individuali che economici, davvero non più sostenibili. 

Anche ala luce dei progressi delle nostre conoscenze mediche è necessario intervenire, anche per via amministrativa laddove è possibile, per: 

*   semplificare il procedimento di rettifica anagrafica, anche chiarendo che gli interventi chirurgici non sono obbligatori per lo stesso;
*   intervenire nel sistema sanitario nazionale per stabilire standar qualitativi e quantitativi per i centri di assistenza alle persone transessuali, risolvendo, tra l'altro, il problema dell'accesso ai farmaci;
*   intervenire con azioni positive nel settore dell'integrazione lavorativa delle persone transessuali. 

**L'impegno europeo internazionale** 

L'Italia deve prendere una posizione decisa, aperta, e consapevole in merito alla urgente necessità da parte dell'Unione di assumere una nuova direttiva per l'estensione piena del principio di non discriminazione per tutti e tutte, bloccata dai veti tedeschi e dei fondamentalismi cristiani e ortodossi. Allo stesso modo deve operare a livello delle Nazioni unite per la depenalizzazione dell'omosessualità e della transessualità e indirizzari i propri rapporti internazionali tenendo come discrimine la necessità che il rispetto dei diritti fondamentali della persona siano garantiti e richiesti in modo esplicito ai nostri partner. A partire da quei Paesi ove l'omosessualità e la transessualità sono considerati reato o sono oggetto di campagne violente e discriminatorie. 

Su tutti questi temi deputati e senatori radicali hanno presentato puntuali proposte di legge e di intervento a disposizione del Parlamento e di tutti e tutte per avviare confronto su soluzioni concrete. 

Noi siamo a disposizione. 

Associazione radicale certi diritti 

www.certidiritti.it