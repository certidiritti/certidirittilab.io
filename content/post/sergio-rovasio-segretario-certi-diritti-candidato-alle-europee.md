---
title: 'SERGIO ROVASIO, SEGRETARIO CERTI DIRITTI, CANDIDATO ALLE EUROPEE'
date: Fri, 01 May 2009 12:16:59 +0000
draft: false
tags: [Comunicati stampa]
---

Sergio Rovasio, Segretario dell'Associazione Radicale Certi Diritti è stato candidato nella Lista Bonino/Pannella per le elezioni europee il cui voto è previsto il prossimo 6-7 giugno.

Rovasio è il candidato n.10 della Circoscrizione Centro (che comprende le Regioni Lazio, Umbria, Marche e Toscana).