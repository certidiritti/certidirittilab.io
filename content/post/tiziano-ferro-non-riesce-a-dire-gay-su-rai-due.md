---
title: 'Tiziano Ferro non riesce a dire gay su Rai Due'
date: Mon, 09 Jan 2012 11:49:52 +0000
draft: false
tags: [Politica]
---

Dal blog di Andrea Bordoni [www.vogliosposaretizianoferro.it](http://www.vogliosposaretizianoferro.it)

Continua la campagna di Certi Diritti e Radicali Italiani 'Informerai' per denunciare la tv pubblica all'Agcom per l'informazione che ci nega.

Sono le 23:10  del 3 gennaio e ho appena finito di guardare lo speciale su Tiziano Ferro trasmesso da Rai Due in prima serata. Dovrei essere felice per aver passato due ore con le sue canzoni e le sue parole, invece quello che riesco a provare, e che ho provato per tutta la durata della trasmissione, è rabbia.

Rabbia per la banalità dell’intervista che gli è stata fatta, con domande del tipo: “qual è stato il giorno più felice e il giorno più infelice della tua vita?”. Rabbia e noia per il continuo rimpasto delle stesse immagini per decine di volte e per il ritornare sempre sugli stessi argomenti, come i lucchetti di Federico Moccia sui ponti.

Ma soprattutto rabbia perché Rai Due è riuscita a confezionare uno speciale su Tiziano Ferro di due ore – che come strombazza la pubblicità parla della sua vita e della sua carriera – senza mai dire neanche una volta la parola omosessuale, omosessualità o gay. Nulla.  
Il top: mentre parlavano di come il suo libro “Trent’anni e una chiacchierata con papà” affronti con sincerità la sua vita sentimentale, hanno montato la copertina sull’unica immagine nella storia dei video di Tiziano Ferro in cui bacia una ragazza. Il video è “Non me lo so spiegare”…

La Rai continua il suo lavoro, continua a dare ai suoi spettatori quello che crede essi vogliano, continua a trattarli come ignoranti perbenisti, continua a manipolare la realtà per fare il gioco della Chiesa e dei politici benpensanti.  
Tiziano Ferro avrà visto questo servizio finito e montato? Che cosa ne pensa? Si è prestato a questo gioco avvilente da don’t ask don’t tell televisivo pur di farsi pubblicità o ne è stato vittima?

So che mi attirerò le ire dei e delle fan più sfegatate (e ricordo a tutti che io lo voglio sposare e che ogni rapporto che si rispetti passa attraverso la capacità di affrontare divergenze e scontri anche duri) ma non riesco a non domandarmi quanto Tiziano Ferro sia responsabile di questa situazione, visto che l’unica volta che nello speciale si affronta la tematica dell’omosessualità (senza nominarla), Tiziano Ferro stesso si vede impegnato in giri di parole assurdi (“quella parte di me che erroneamente non accettavo”… bla bla) pur di non nominare l’amore che, grazie a Rai Due, di nuovo non osa pronunciare il suo nome.

Forse è la frustrazione che mi fa parlare, ma la mia impressione è che per tutta l’intervista Tiziano Ferro metta un filtro tra se stesso e gli intervistatori, attento a centellinare ogni frase (forse scottato dall’incidente da Fazio per la battuta infelicissima sulle donne messicane baffute, e questo è comprensibile). Sembra uno studente modello che vuole fare bella figura a un’interrogazione, ma senza dire esplicitamente che Oscar Wilde era gay. Mi sono sentito un po’ come Nanni Moretti. Rivolto alla tv esclamavo: “Tiziano, di’ qualcosa di gay! Di’ qualcosa!”

Certo, magari tutte le scene in cui Tiziano Ferro diceva “gay” o lanciava messaggi in tal senso sono state tagliate al montaggio, ma il modo in cui parla dell’argomento qui mi fa venire un dubbio.

Il mio amore verso di lui resta invariato, la bellezza delle sue canzoni, la sua voce, l’interpretazione impeccabile che ha confermato in quest’ultimo disco, la stima per il coraggio e il bene che ha fatto con il suo coming out, invariati, ma credo che questa cosa sia inaccettabile. Io non penso che, solo perché ha fatto coming out, debba parlare di omosessualità e diritti dei gay in ogni intervista e in ogni interevento come fa Ricky Martin, ma uno speciale del genere è un’enorme occasione sprecata per rompere un tabù e dare un forte e positivo messaggio ai più giovani, tanto più che Tiziano Ferro stesso ammette quanto siano importanti e quanto peso abbiano le parole e l’esempio di cantanti, campioni sportivi, insomma di tutti coloro che hanno un accesso preferenziale ai media. Mi sarebbe bastato un accenno, non un simposio. Tutta questa trasmissione per me getta un’ombra di tristezza infinita su quelli che combattono per i diritti del mondo lgbt. È stato come lasciar passare un messaggio avvilente e contrario, ammettere che la parola gay vada nascosta, è imbarazzante, e che essere gay deve essere un sottinteso.

Tutta questa faccenda mi ricorda il diktat che impose alla Rai di non usare la parola preservativo per tutta la giornata mondiale contro l’Aids. Dalla Rai me lo aspetto. Da Tiziano Ferro mi piace pensare che posso aspettarmi di più. Confermo quello che ho scritto altrove su alcune sue reticenze post-coming-out: mi sembra chiaro che vuole fare un passo alla volta, non vuole stupire o provocare, essere chiuso in un cliché, diventare una bandiera, non vuole schiacciare la sua complessità e le sue canzoni su un solo dato. Facevo questo discorso parlando della mia piccola delusione nel non vedere nemmeno una coppia gay inserita nel video “La differenza tra me e te”, che pure passava in rassegna diversi tipi di amore. Non sarebbe stato certo la prima volta per un artista italiano. Per esempio ieri guardando gli interventi di Jovanotti su Tiziano Ferro nello speciale mi è venuto in mente che anche nel video di “Il più grande spettacolo dopo il Big Bang” (per me una delle canzoni più brutte scritte dopo il Big Bang) c’è un bacio tra maschietti, a dire il vero così fugace che viene il sospetto che i due tizi si siano confusi e si siano soltanto girati dalla parte sbagliata rispetto alle loro fidanzate; però c’è, al minuto 2:59 e forse anche a 3:06, mentre nel video di Tiziano niente.

Questo atteggiamento “timido” passi per brevi ospitate in trasmissioni o per alcune scelte artistiche più sfumate come quella del video, ma ritengo che non sia accettabile di fronte a una trasmissione interamente dedicata alla sua vita in cui si sceglie di nasconderne una parte così importante, come faceva fino a un anno fa. Certo, certo, io e lui saremo una coppia sposata, ma non una di quelle coppie simbiotiche che fanno tutto insieme e tutto uguale, quindi è giusto differenziare: tra i due in fatto di militanza quello che ci va giù con la testa d’ariete sono io. Ma visto che al momento quello più famoso è lui… da grandi poteri, grandi responsabilità, come direbbe un eroe della Marvel.  
Detto questo spero che presto la Rai smetta di essere gestita dai “soliti idioti” e agli alti livelli e a tutti i responsabili della trasmissione dico: “Vi dà forse fastidio la parola omosessuale? Allora io la dico: OmoseSSssuale, OmosesssuaLE, omoseSSUaleeeeeeeeeeeeeeeeeeeee”.

**dal blog di Andrea Bordoni[  
www.vogliosposaretizianoferro.it](http://www.vogliosposaretizianoferro.it)**

  
**Continua la campagna 'Informerai' di Certi Diritti e Radicali Italiani.**  
**Chiedi con noi all'autorità garante delle comunicazioni, l'Agcom, che la Rai finalmente apra un dibattito pubblico sulle libertà sessuali e le discriminazioni di genere!  
Basta un click! [http://www.certidiritti.it/informerai-sulle-liberta-sessuali-e-le-discriminazioni-di-genere](informerai-sulle-liberta-sessuali-e-le-discriminazioni-di-genere)  
**