---
title: 'Gay cacciato di casa, udienza rinviata Appello lgbt: "Il Comune intervenga"'
date: Wed, 06 Oct 2010 14:53:41 +0000
draft: false
tags: [Senza categoria]
---

![Gay cacciato di casa, udienza rinviata Appello lgbt: "Il Comune intervenga"](http://roma.repubblica.it/images/2010/10/06/163702930-f5c1d0c4-e5a1-49a2-a5e3-68b95d6c0375.jpg "Gay cacciato di casa, udienza rinviata Appello lgbt: "Il Comune intervenga"")

Da [Repubblica.it](http://roma.repubblica.it/cronaca/2010/10/06/news/emilio_rez_rinvio_udienza-7783990/)

Si allungano i tempi in tribunale per Emilio Rez, cantautore gay minacciato e chiuso fuori di casa. In aula assenti i proprietari denunciati anche per gli insulti subiti

Prima gli insulti, poi la serratura cambiata, quindi il processo. Una vicenda che non vede fine quella di [**Emilio Rez**](http://roma.repubblica.it/cronaca/2010/06/14/news/radicali_giovane_gay_insultato_e_sfrattato_intervenga_sindaco_alemanno-4808661/index.html), il cantante gay di 25 anni, nato a Torre Annunziata, che lo scorso 10 giugno era stato chiuso fuori dalla sua abitazione al Pigneto, a Roma, con i vestiti gettati sulle scale rinchiusi nei sacchi dei rifiuti. Dietro al gesto i proprietari dell'appartamento che, nei giorni precedenti, lo avevano insultato con parole pesanti. "Sei un frocio di merda, sei un farabutto... con noi caschi male". Frasi inequivocabili, che hanno fatto scattare la denuncia ai Carabinieri per i reati di violazione di domicilio, appropriazione indebita e minacce.  
  
[**IL VIDEO** **Rez sul palco**](http://roma.repubblica.it/multimedia/home/7350589)  
  
Ieri in Tribunale, alla presenza dell'Avvocato Massimo Vita, che difende Rez, c'è stata la prima udienza alla quale i proprietari non si sono nemmeno presentati. Udienza rinviata al 28 aprile 2011. Intanto, Emilio Rez continua a rimanere senza casa e senza i suoi effetti personali, "aiutato da amici e totalmente abbandonato dal Comune che aveva detto di voler intervenire". Subito dopo la denuncia, infatti, l'assessore alle Politiche Sociali, Sveva Belviso, si era impegnata a "seguire con attenzione l'evolversi delle indagini per chiarire più presto fatti e responsabilità".  
  
A denunciare la situazione a tutt'oggi drammatica è l'Associazione Radicale Certi Diritti, con la quale Rez collaborava già da tempo, che "chiede al Comune e ai media di intervenire per garantire tutela e assistenza a coloro che hanno il coraggio di denunciare soprusi e violenze, in questo caso di tipo omofobico".  
  
L'associazione lancia l'allarme: "Sapere che Emilio Rez potrà forse sperare in un intervento del Giudice alla fine dell'aprile 2011, per un atto di prepotenza subito nel giugno 2010, rende bene l'idea in quale situazione di rischio e pericolo vivono le persone lgbt in questo paese".  
  
Emilio Rez aveva raccontato, anche di fronte alle telecamere del Maurizio Costanzo Show, di essere stato picchiato e aggredito nell'agosto del 2009 in piazza Re di Roma, zona San Giovanni.

[![](http://oas.repubblica.it/0/default/empty.gif)](http://oas.repubblica.it/5c/local.repubblica.it/rg/roma/interna/611163794/Middle/default/empty.gif/58536868545578472f6e6341446d5268)

(06 ottobre 2010)

http://roma.repubblica.it/cronaca/2010/10/06/news/emilio\_rez\_rinvio_udienza-7783990/