---
title: '2008-0140-Discrimination-ST13636.EN09'
date: Fri, 25 Sep 2009 12:02:54 +0000
draft: false
tags: [Senza categoria]
---

  

  

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 25 September 2009

Interinstitutional File:

2008/0140 (CNS)

13636/09

LIMITE

SOC 537

JAI 611

MI 350

  

  

  

  

  

NOTE

from :

General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

13049/09 SOC 504 JAI 564 MI 326

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Further to the meeting of the Social Questions Working Party on 22 September 2009, delegations will find attached a note from the UK delegation.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**ANTI-DISCRIMINATION DIRECTIVE – UK TEXT PROPOSALS**

**1.         Harassment – limiting harassment to the grounds of disability and age**

Article 2(3):

“_Harassment shall be deemed to be a form of discrimination within the meaning of paragraph 1, when unwanted conduct related to **any of the grounds referred to in Article 1** **disability or age** takes place with the purpose or effect of violating the dignity of a person and of creating an intimidating, hostile, degrading, humiliating or offensive environment.  In this context, the concept of harassment may be defined in accordance with the national laws and practice of the Member States._”

Justification

The UK believes that members of secular society and religious organisations must be free to express and explore religious ideas which others, because of their beliefs, may find offensive. When it comes to religion or belief and sexual orientation, this specifically brings into conflict strongly held beliefs and practices of religious groups, for example, and gay and lesbian people. Curtailing freedom of expression in an attempt to prohibit harassment could well lead to an unintended “chilling effect” e.g. where theatres are afraid to present certain plays which members of some religions may find objectionable.

Nor do we accept the argument that a prohibition in the Directive is necessary to protect individuals from harassment on these two grounds. A public consultation in 2007 on proposals for the Equality Bill for Great Britain, which is now before Parliament, did not bring forward any evidence of conduct that might constitute harassment on the grounds of sexual orientation or religion or belief and which would not already be covered by the prohibition of direct or indirect discrimination.

**2.         Under-18s – excluding Under-18s from the prohibition on age discrimination**

New Article 2(6) third indent

**_“Differences in treatment on the grounds of age, so far as relating to persons who have not attained the age of 18, shall not be regarded as discrimination on the age ground.”_**

Justification

This wording achieves the same effect as the recent Irish proposal. However, it follows more closely the language in the following paragraph, Article 2(6)(a).

Applying age discrimination protection to the under-eighteens could cause significant difficulties.  All age-specific services, activities and protections for young people would become unlawful unless covered by an exception or allowed by an objective justification defence.  The risk of unintended consequences and litigation is high even with wide exceptions and the scope to objectively justify different treatment by age. Service providers may well react by standardising services across all age groups or withdrawing services for children or children of particular ages altogether because of the risk of legal challenge. Experience in the UK of age legislation in the area of vocational training has shown that providers are likely to withdraw age-related services or concessions entirely rather than face such a risk, even where the objective justification defence is available.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_