---
title: 'Dal 39° Congresso del Partito Radicale Transnazionale: mozione particolare sulla persecuzione delle persone lgbt nel mondo'
date: Mon, 12 Dec 2011 14:07:29 +0000
draft: false
tags: [Transnazionale]
---

Di seguito il testo della mozione particolare, a prima firma John Francis Onyango, che Certi Diritti ha presentato al 39° congresso del Partito Radicale Nonviolento Transnazionale Transpartito e che è stata accolta dalla presidenza.

 [Français](dal-39-congresso-del-partito-radicale-transnazionale-mozione-particolare-sulla-persecuzione-delle-persone-lgbt-nel-mondo#francese)             [English](dal-39-congresso-del-partito-radicale-transnazionale-mozione-particolare-sulla-persecuzione-delle-persone-lgbt-nel-mondo#english)

  
**Mozione particolare in merito alla persecuzione delle persone sulla base del loro orientamento sessuale nel mondo e in particolare sulla situazione in Africa  
**

**Considerato**

Che 76 Stati nel mondo criminalizzano le relazioni tra persone dello stesso sesso, e che 7 di questi prevedono la pena di morte (Iran, Mauritania, 12 Stati del Nord della Nigeria, Arabia Saudita, la parte meridionale della Somalia, Sudan e Yemen);

**Considerato**

Che la Camera dei Rappresentanti della Nigeria ha approvato in prima lettura la ‘proposta di legge per la proibizione del matrimonio tra persone dello stesso sesso’ dopo l’approvazione del Senato, e che la legge rischia di essere approvata nei prossimi giorni o settimane;

Che la proposta è stata trasformata al punto da prevedere la punizione con 14 anni di reclusione non solo per chi contraesse un matrimonio tra persone dello stesso sesso, ma anche per le persone dello stesso sesso che si rendessero responsabili di una qualunque manifestazione pubblica di affetto;

Che è prevista inoltre l’incarcerazione per 10 anni di tutti coloro che li ‘incoraggiassero o aiutassero’;

Che la legge si applicherà inter alia agli espatriati, ai turisti e a tutte le persone in Nigeria che avessero una relazione omosessuale;

Che chiunque s’iscrivesse, operasse o frequentasse ‘un club, associazione od organizzazione gay’ sarebbe passibile di condanna a 10 anni di reclusione;

Che alcune attività e finanziamenti forniti dall’Italia o dall’UE per la protezione dei diritti umani potrebbero essere presi di mira in caso la proposta di legge venisse approvata;

Che oltre a violare gli impegni internazionali contratti dalla Nigeria in tema di diritti umani come il diritto alla vita privata e famigliare, il diritto alla libertà di espressione e quello alla non-discriminazione, una tale legge minerebbe definitivamente le politiche di prevenzione dell’AIDS in un paese dove 3 milioni e mezzo di persone sono sieropositive (stima ONU del 2009);

**Considerato**

Che il legislatore camerunense sembra voler rivedere le proprie leggi per introdurre una discriminazione basata sull’orientamento sessuale inasprendo le sanzioni per gli atti sessuali con minori solamente se intercorsi tra persone dello stesso sesso e non tra persone di sesso diverso;

Che in seguito a molti casi di omofobia direttamente sponsorizzati dallo Stato a partire dalla scorsa primavera, tre persone sono state condannate a 5 anni di prigione per il loro reale o presunto orientamento omosessuale;

Che a essi si aggiunge Jean-Claude Roger Mbede, studente di 31 anni, che dopo il suo arresto, il 2 marzo, è stato detenuto per sette giorni prima di essere accusato di omosessualità e induzione all’omosessualità e dover quindi scontare una condanna di tre anni nel carcere centrale di Kondengui, dove le condizioni di detenzione sono difficili a causa di un grave sovraffollamento, scarsi livelli di igiene e carenza di cibo;

**Considerato**

Che al parlamento ugandese giace ancora la proposta di legge che vuole introdurre la pena di morte per le persone omosessuali, contro la quale si è battuto il nostro iscritto David Kato Kisule barbaramente ucciso per la sua lotta a favore dei diritti delle minoranze sessuali e che tale proposta di legge rischia ancora di essere appprovata;

**Considerato**

Che nella Federazione Russa sono in esame proposte di legge volte a criminalizzare qualunque attività o informazione relativa alle persone LGBT ed alle relazioni tra persone dello stesso sesso, in violazione delle libertà di espressione ed associazione;

**Considerato**

Che il diritto delle persone a non essere discriminate o perseguitate sulla base del loro orientamento sessuale è un diritto fondamentale, come peraltro affermato dalla risoluzione approvata dal Consiglio dei Diritti Umani dell’ONU il 17 giugno 2011;

**Invita**

Il Partito Radicale Nonviolento Transnazionale e Transpartito a prendere le iniziative necessarie a livello nazionale, europeo ed internazionale per denunciare e lottare contro le persecuzioni e le discriminazioni subite dalle persone a causa del loro orientamento sessuale nel mondo, a partire dall’Uganda, dalla Nigeria, dal Cameroon e dalla Russia;

A impegnarsi per una campagna per la decriminalizzazione dell’omosessualità nel mondo, a partire dall’approvazione di una risoluzione al riguardo all’Assemblea Generale dell’ONU.  
  
[  
](lassociazione-radicale-certi-diritti-diventa-soggetto-politico-costituente-del-partito-radicale-nonviolento-transnazionale-e-transpartito)**[CERTI DIRITTI DIVENTA SOGGETTO COSTITUENTE DEL PARTITO RADICALE NONVIOLENTO TRANSNAZIONALE TRANSPARTITO >](lassociazione-radicale-certi-diritti-diventa-soggetto-politico-costituente-del-partito-radicale-nonviolento-transnazionale-e-transpartito)**

 [Torna in alto](dal-39-congresso-del-partito-radicale-transnazionale-mozione-particolare-sulla-persecuzione-delle-persone-lgbt-nel-mondo#startOfPageId1327)

* * *

* * *

Motion particulière concernant la persécution des personnes en raison de leur orientation sexuelle dans le monde, et notamment en Afrique

Attendu

Que 76 États dans le monde criminalisent les relations entre personnes du même sexe, et que 7 d'entre eux prévoient la peine de mort (l'Iran, la Mauritanie, 12 États du Nord du Nigéria, l'Arabie Saoudite, le sud de la Somalie, le Soudan et le Yémen);

Attendu

Que la Chambre des Représentants du Nigéria a approuvé en première lecture la «proposition de loi sur la prohibition du mariage entre personnes du même sexe» suite à l'approbation du Sénat, et que cette loi risque d'être approuvée d'ici les prochains jours ou semaines;

Que la proposition a été transformée tant et si bien que l'on prévoit une peine de 14 ans de réclusion non seulement pour quiconque contracte mariage entre personnes du même sexe, mais également pour les personnes du même sexe qui se rendent responsables de toute manifestation publique d'affection;

Qu'un emprisonnement de 10 ans est prévu pour tous ceux qui les «encouragent ou les aident»;

Que la loi sera appliquée inter alia aux expatriés, aux touristes et à toutes les personnes qui au Nigéria ont une relation homosexuelle;

Que quiconque s'inscrive, travaille ou fréquente «un club, une association ou une organisation gay» est passible d'une condamnation à 10 ans de réclusion;

Que certaines activités et certains financements fournis par l'Italie ou l'UE en vue de la protection des droits de l'homme pourraient être pris pour cible, au cas où ladite proposition de loi serait approuvée;

Qu'à part le fait de violer les engagements internationaux contractés par le Nigéria en matière de droits de l'homme, tels que le droit à la vie privée et familiale, le droit à la liberté d'expression et le droit à la non-discrimination, une telle loi porterait définitivement atteinte aux politiques de prévention du SIDA dans un pays, où 3 millions et demi de personnes sont séropositives (selon les estimations de l'ONU de 2009);

Attendu

Que le législateur camerounais semble vouloir réviser ses propres lois, afin d'introduire une discrimination fondée sur l'orientation sexuelle, en durcissant les sanctions pour tout acte sexuel avec des mineurs, seulement s'il a lieu entre personnes du même sexe et non pas entre personnes de sexe différent;

Que, suite à plusieurs cas d'homophobie sponsorisés directement par l'État à partir du printemps dernier, trois personnes ont été condamnées à 5 ans de prison à cause de leur orientation homosexuelle vraie ou supposée;

Qu'à ceux-ci s'ajoute Jean-Claude Roger Mbede, un étudiant de 31 ans, qui après avoir été arrêté, le 2 mars, a été détenu pendant sept jours avant d'être accusé d'homosexualité et d'encouragement à l'homosexualité et condamné donc à purger une peine de trois ans dans la prison centrale de Kondengui, où les conditions de détention sont difficiles à cause d'un grave problème de surpopulation carcérale, des piètres conditions d'hygiène et du manque de nourriture;

Attendu

Qu'au parlement ougandais une proposition de loi est en souffrance visant à introduire la peine de mort pour les homosexuels, contre laquelle s'est battu notre inscrit David Kato Kisule, barbarement tué pour avoir lutté en faveur des droits des minorités sexuelles et que ladite proposition de loi risque encore d'être approuvée;

Attendu

Que dans la Fédération de Russie des propositions de loi sont examinées visant à criminaliser toute activité ou information concernant les personnes LGBT ainsi que les relations entre personnes du même sexe, en violation des libertés d'expression et d'association;

Attendu

Que le droit des personnes à ne pas être discriminées ou persécutées en raison de leur orientation sexuelle est un droit fondamental, comme cela est d'ailleurs affirmé par la résolution approuvée par le Conseil des Droits de l'Homme de l'ONU le 17 juin 2011;

Invite

Le Parti Radical Nonviolent Transnational et Transparti à prendre les initiatives nécessaires à l'échelon national, européen et international, afin de dénoncer et de lutter contre les persécutions et les discriminations subies par une personne du fait de son orientation sexuelle dans le monde, à partir de l'Ouganda, du Nigéria, du Cameroun et de la Russie;

À s'engager dans une campagne en vue de la décriminalisation de l'homosexualité dans le monde, à partir de l'approbation d'une résolution sur ce point à l'Assemblée Générale de l'ONU.

 [En haut](dal-39-congresso-del-partito-radicale-transnazionale-mozione-particolare-sulla-persecuzione-delle-persone-lgbt-nel-mondo#startOfPageId1327)

* * *

* * *

Specific motion on the persecution of people based on their sexual orientation all over the world and in Africa in particular

Considering that

76 Countries all over the world criminalise relationships between individuals of the same gender, and 7 of these Countries impose death penalty (Iran, Mauritania, 12 States in the north of Nigeria, Saudi Arabia, Southern Somalia, Sudan and Yemen);

Considering

That the House of Representatives of Nigeria approved in its first reading the 'bill for the prohibition of marriage between people of the same gender', after its adoption by the Senate, and that the bill risks to be enacted in the next few days or weeks;

That the draft law was amended to such an extent that it provides for the punishment by imprisonment of up to 14 years not only for those who married an individual of the same gender, but also for the people of the same gender showing their affection in public;

That imprisonment of up to 10 years is inflicted to all those who "encourage or help them";

That the law will apply, inter alia, to expatriates, tourists and to all individuals in Nigeria having homosexual relations;

That anyone joining, operating or attending a "gay club, association or organisation" would be liable for a sentence of 10 years' imprisonment;

That some activities and funding provided by Italy or by the EU for the protection of human rights could be affected in case the bill is adopted;

That besides breaching the international agreements signed by Nigeria in the field of human rights, such as the right to a private and family life, the right to the freedom of expression and the right to non-discrimination, such law would definitively undermine the AIDS prevention policies in a Country in which 3.5 million people are HIV-positive (UN estimate of 2009);

Having considered

That the Cameroonian law-maker seems to be willing to review his laws with the aim to introduce a discrimination based on sexual orientation, by exacerbating the sanctions for sexual acts with minors only if involving people of the same gender and not individuals of different genders;

That following several cases of homophobia directly supported by the State as from the last spring, three people were sentenced to 5 years of imprisonment for their real or alleged homosexual orientation;

That also the case of Jean-Claude Roger Mbede is particularly relevant. He is a 31-year old student that, after his imprisonment on March 2nd, was detained for seven days before being accused of homosexuality and of incitement to homosexuality, therefore forced to serve a sentence of three-years' imprisonment at the central prison of Kondengui, where detainment conditions are difficult due to a severe overcrowding, scarce levels of sanitation and shortage of food;

Having considered

That the Parliament of Uganda has not yet adopted the bill aimed at introducing the death penalty for homosexual people; this bill was heavily opposed by our member David Kato Kisule, brutally murdered for his struggle in favour of the rights of sexual minorities, and that this bill still risks to be approved;

Having considered

That in the Russian Federation bills are being examined aiming to criminalise whatever activity or information relating to LGBT people and to relations between individuals of the same gender, in violation of the freedom of expression and association;

Having considered

That the right of people not to be discriminated or persecuted on the basis of their sexual orientation is a fundamental right, as it is also stated by the resolution adopted by the UN Human Rights Council on 17th June 2011;

Invites

The Nonviolent Radical Party Transnational and Transparty to adopt the necessary initiatives at national, European and International level to denounce and fight against the persecutions and discriminations suffered by people in consideration of their sexual orientation all over the world, starting from Uganda, Nigeria, Cameroon and Russia;

To commit itself to a campaign for the decriminalisation of homosexuality at world level, starting from the approval by the UN General Assembly of a resolution on this subject.

[Top](dal-39-congresso-del-partito-radicale-transnazionale-mozione-particolare-sulla-persecuzione-delle-persone-lgbt-nel-mondo#startOfPageId1327)