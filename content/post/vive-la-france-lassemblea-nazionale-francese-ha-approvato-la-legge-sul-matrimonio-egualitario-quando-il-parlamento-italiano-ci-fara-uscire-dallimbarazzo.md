---
title: 'VIVE LA FRANCE! L''Assemblea Nazionale francese ha approvato la legge sul matrimonio egualitario. Quando il Parlamento italiano ci farà uscire dall''imbarazzo?'
date: Tue, 23 Apr 2013 14:07:14 +0000
draft: false
tags: [Matrimonio egualitario]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti.

Roma, 23 aprile 2013

Come annunciato oggi l'Assemblea Nazionale francese ha approvato, con 331 voti favorevoli e 225 contrari, la legge che introduce definitivamente il matrimonio egualitario nel Paese d'oltralpe.

Con lo straordinario voto di oggi la Francia diventa il quattordicesimo paese al mondo a riconoscere a tutti i suoi cittadini e cittadine la possibilità di accedere all'istituto del matrimonio civile a prescindere dall'orientamento sessuale e dal sesso del partner che si decide di sposare. Il motto libertà, uguaglianza e fraternità a cui la Francia s'ispira sin dai tempi della rivoluzione s'incarna oggi nel voto assembleare.

L’Associazione Radicale Certi Diritti saluta, commossa e piena di gratitudine, il voto francese di oggi e condanna fortemente le modalità con cui alcuni si sono opposti a questa legge, modalità che hanno portato a una preoccupante ondata di omofobia e violenza.

Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti, dichiara: "Dopo Spagna e Portogallo anche la Francia, paese molto simile all'Italia sotto il profilo culturale, ha approvato oggi il matrimonio egualitario. Ormai tutti i paesi europei di religione cattolica hanno riconosciuto questo fondamentale diritto di uguaglianza ai propri cittadini dimostrando di avere ben chiaro il concetto di laicità dello Stato.

Ringraziamo deputati e senatori francesi per questo atto di civiltà e rimarchiamo l'imbarazzante situazione dell'Italia che - sola tra i più importanti paesi europei, anche cattolici - non riesce a garantire il fondamentale diritto alla vita famigliare dei suoi cittadini omosessuali".