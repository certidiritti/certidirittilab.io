---
title: 'I LAVORI DEL III° CONGRESSO DI CERTI DIRITTI 30-31 GENNAIO 2010 A FIRENZE'
date: Tue, 02 Feb 2010 11:38:05 +0000
draft: false
tags: [Comunicati stampa]
---

Grazie a Radio Radicale ai link che seguono è possibile ascoltare in audio i lavori del III° Congresso dell'Associazione Radicale Certi Diritti svoltosi a Firenze sabato 30 e domenica 31 gennaio 2010 presso il Centro Congresso dei Valdesi.

[http://www.radioradicale.it/scheda/296342/terzo-congresso-dellassociazione-radicale-certi-diritti-prima-giornata](http://www.radioradicale.it/scheda/296342/terzo-congresso-dellassociazione-radicale-certi-diritti-prima-giornata)

[http://www.radioradicale.it/scheda/296343/terzo-congresso-dellassociazione-radicale-certi-diritti-seconda-ed-ultima-giornata](http://www.radioradicale.it/scheda/296343/terzo-congresso-dellassociazione-radicale-certi-diritti-seconda-ed-ultima-giornata)