---
title: 'A TORINO STRAORDINARIA MOBILITAZIONE PARI OPPORTUNITA SU UNIONI CIVILI'
date: Sat, 29 Nov 2008 09:20:31 +0000
draft: false
tags: [Comunicati stampa]
---

PARI DIRITTI PER LE UNIONI CIVILI   
 

Presentata a Torino la campagna di raccolta firme su una proposta di iniziativa popolare che chiede a Torino il riconoscimento di pari opportunità per le unioni civili. Non si può attendere il Parlamento, il Comune può adottare iniziative concrete. 31 associazioni e 57 personalità tra i primi sostenitori

Stamane i rappresentanti delle associazioni proponenti hanno lanciato la raccolta firme su una proposta di deliberazione di iniziativa popolare che chiede al Comune di riconoscere pari opportunità nelle materie di sua competenza per le unioni civili (assistenza, case popolari, asili, cultura, diritti, politiche per i giovani e per gli anziani, formazione e scuola). 

In attesa che il Parlamento (quando?) riesca ad affrontare questa realtà che tutti fanno finta di non vedere, si può e si deve incalzare le amministrazioni comunali affinché, usando gli strumenti normativi già esistenti (come la legge sull’anagrafe) come già il Comune di Torino fa nel caso del riconoscimento dei doveri delle coppie (si veda il regolamento comunale sugli aiuti alle persone disagiate). 

La campagna è stata lanciata dalle associazioni torinesi: Associazione Radicale Satyagraha, Associazione Radicale Certi Diritti, Associazione Radicale Adelaide Aglietta, Consulta torinese per la laicità delle istituzioni, Coordinamento Torino Pride, Casa delle Donne, Circolo evangelico Arturo Pascal, Comitato torinese per la laicità della scuola.

Ad oggi hanno aderito 31 associazioni torinesi di cui si allega l’elenco completo. 

E’ stato anche lanciato un appello alla sottoscrizione, appello che è stato sottoscritto da 57 personalità del mondo della cultura e dell’Università torinese, tra cui: Chiara Saraceno, Alfonso Di Giovine, Gianni Vattimo, Massimo Salvadori, Edoardo Tortarolo, Francesco Remotti, Carlo Augusto Viano, Franco Giampiccoli, Franco Sbarberi, Piera Egidi, Loredana Sciolla, Vincenzo Ferrone, Tullio Telmon (elenco completo allegato). 

Devono essere raccolte almeno 1.500 entro l’8 febbraio 2009. Ogni sabato pomeriggio le Associazioni Radicali in Piazza Castello aprono i loro banchetti. Per ogni informazioni in più sulla raccolta firme e sugli appuntamenti: 

portavoce:

Stefano Mossino, 349.4686807

Tullio Monti, 340.1488846

Roberta Padovano, 349.111169 

Sede: 011.5212033

Mail: [unioni.civili.torino@gmail.com](mailto:unioni.civili.torino@gmail.com) 

Alla conferenza stampa erano presenti, oltre ai portavoce, Enzo Cucco, Enzo Francone, Silvio Viale, Alberto Ventrini, Palmira Naidenova, Mihail Cosentino, in rappresentanza delle molte associazioni aderenti. 

Torino, 28 novembre 2008

**_COMITATO PROMOTORE_**

**_PARI OPPORTUNITA’ PER LE UNIONI CIVILI_** 

**Le Associazioni Promotrici**

**della Proposta di Deliberazione di Iniziativa Popolare**   
   
 

**Associazione “Lambda Amici della Fondazione Sandro Penna”**

**Associazione Radicale “Satyagraha”**

**Associazione Radicale “Certi Diritti”**

**Associazione Radicale “Adelaide Aglietta”**

**Circolo di cultura Gay, Lesbica, Bisessuale e Transgender “Maurice”**

**Casa delle Donne**

**Centro Evangelico di Cultura “Arturo Pascal”**

**Comitato Torinese per la Laicità della Scuola**

**Comitato Torino Pride**

**Consulta Torinese per la Laicità delle Istituzioni** 

**AICS – Associazione Italiana Cultura e Sport, Comitato Provinciale di Torino**

**Altera – Generatore di Pensieri in Movimento**

**Associazione “31 ottobre per una scuola laica e pluralista promossa dagli evangelici italiani”**

**Associazione “Il Girasole”**

**Associazione “Viottoli” Comunità Cristiana di Base**

**Associazione “Famiglie Arcobaleno”**

**Associazione “l laboratorio della solidarietà”**

**Associazione “Ippocrate”**

**Associazione “LibertàEguale”**

**Centro di Documentazione, Ricerca e Studi sulla Cultura Laica “Piero Calamandrei” – Onlus**

**Circolo Liberalsocialista “Carlo Rosselli”**

**Circolo UAAR – Unione Atei e Agnostici Razionalisti di Torino**

**FNISM- Federazione Nazionale Insegnanti Sezione di Torino**

**Fondazione “Critica Liberale” – Sezione di Torino**

**Gruppo di Studi Ebraici**

**Icarus – Centro Studi di Progettazione Sociale**

**La Meridiana**

**PresenteéFuturo**

**Scambiaidee**

**Sotto la Mole**

**UDI – Unione Donne in Italia Sezione di Torino**