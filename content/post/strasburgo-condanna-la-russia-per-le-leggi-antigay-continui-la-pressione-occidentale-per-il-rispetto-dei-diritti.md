---
title: 'Strasburgo condanna la Russia per le leggi antigay: continui la pressione occidentale per il rispetto dei diritti'
date: Tue, 20 Jun 2017 14:23:57 +0000
draft: false
tags: [Russia]
---

\[caption id="attachment_7573" align="alignleft" width="300"\]![Mandatory Credit: Photo by John Edward Linden / Arcaid / Rex Features ( 633612a ) EUROPEAN COURT OF HUMAN RIGHTS, STRASBOURG, 1989 - 1995. STRASBURG, FRANCE ARCHITECTURE STOCK](http://www.certidiritti.org/wp-content/uploads/2017/06/European-court-of-human-rights-300x241.jpg) Photo by John Edward Linden / Arcaid / Rex  
  
\[/caption\] "La Corte europea dei diritti umani ha condannato Mosca per le tristemente note leggi contro la 'propaganda gay tra i minori' che sarebbero distorsive degli articoli 10 e 14 della Convenzione - libertà di espressione e divieto di discriminazione -. A poco è servita la linea difensiva adottata dal Cremlino che avrebbe puntato su una traballante distinzione tra libertà di espressione e corruzione dei bambini." Così Leonardo Monaco, segretario dell'Associazione Radicale Certi Diritti. "La sentenza di Strasburgo (che vedrà ricorrere la Federazione Russa) è un segnale importante che deve essere sostenuto dai Paesi europei con iniziative fattive. - ha aggiunto Monaco - A titolo d'esempio, le organizzazioni per i diritti umani LGBTI russe aspettano ancora un segnale di vita dal ministro degli esteri italiano affinché prenda una posizione forte sui centri di prigionia in Cecenia e offra l'aiuto necessario a rilasciare i visti umanitari agli omosessuali ceceni scampati alle torture che i paramilitari di Ramzan Kadyrov hanno loro riservato. Non lasciamo che la barbarie venga insabbiata dall'inerzia occidentale prima ancora che dal regime Russo"