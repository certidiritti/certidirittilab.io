---
title: 'Rinviato ancora voto su legge contro omofobia e transfobia'
date: Thu, 26 May 2011 19:46:43 +0000
draft: false
tags: [Politica]
---

La Camera dei deputati molto probabilmente voterà il provvedimento nei primi giorni di giugno. Rinviato anche il sit-in e maratona oratoria in piazza Montecitorio delle associazioni lgbt(e). Sul nostro sito tutti gli aggiornamenti

Roma, 26 maggio 2011

Comunicato Stampa dell'Associazione Radicale Certi Diritti

Il voto sulla pdl contro l'omofobia e transfobia (A.C. 2802) non si terrà più  martedì 31 maggio ma, quasi sicuramente, slitterà alla settimana successiva. Questo è quanto hanno deciso i Capigruppo alla Camera dei deputati, anche se ufficialmente il provvedimento è rimasto nell’agenda decisa dall’ultima Conferenza dei Capigruppo. La seduta di martedì 31 maggio voterà soltanto Il Decreto Legge 'Voto dei cittadini temporaneamente all'estero' in occasione dei referendum del 12 e 13 giugno 2011’.

In occasione del voto della pdl contro omofobia e transfobia, che molto probabilmente si svolgerà martedì 7 giugno, sono già state presentate da parte dell’Udc e della Lega  proposte di sospensiva e di eccezione di costituzionalità, che si discuteranno prima del voto.

Il giorno del voto L’Associazione Radicale Certi Diritti, insieme a Circolo Mario Mieli, Comitato Europride 2011,  Arcigay, GayHelpLine, Iken Onlus, Associazione Luca Coscioni, Associazione DiGayProject, Associazione Libellula, Associazione 3D - Democratici per pari Diritti e Dignità,  Cgil - Nuovi Diritti, Equality Italia, terranno in Piazza Montecitorio – Roma un Sit-in/Maratona oratoria con le Parlamentari Paola Concia, Rita Bernardini insieme a coloro che sono impegnati nella la lotta per i diritti civili in Italia.

Per adesioni: [info@certidiritti.it](mailto:info@certidiritti.it)