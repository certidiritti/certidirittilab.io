---
title: 'A Roma presentazione del libro "Il segreto dei Fratelli del Libero Spirito. Pagine di esoterismo medievale", edizioni Mimesis'
date: Wed, 25 Jul 2012 05:40:49 +0000
draft: false
tags: [Politica]
---

Mercoledì 25 luglio 2012, ore 21,30 - Terme di Caracalla, Festa del Pd - Stand Rainbow. Partecipa, tra gli altri, Sergio Rovasio dell'associazione radicale Certi Diritti.  
   
Relatori:  
Prof. Avv. Paola Balducci, Università del Salento  
Prof. Elio Matassi, Direttore del Dipartimento di Filosofia di Roma Tre;  
Sergio Rovasio, Associazione Radicale Certi Diritti;  
Avv. Daniele Stoppello, moderatore.

Massimo Frana, Il segreto dei Fratelli del Libero Spirito. Pagine di esoterismo medievale, Mimesis, Milano 2012, ISBN 978-88-5751-136-8.

I Fratelli del Libero Spirito hanno consegnato alla storia una straordinaria lezione di libertà. Al tramonto del Medioevo essi seppero sfidare la ferocia dell’Inquisizione per affermare il principio dello spirito libero. In nome di tale principio, una delle figure femminili più affascinanti del movimento in questione e del Medioevo tutto, Margherita Porete, affrontò il rogo, senza alcuna esitazione e senza piegarsi al terrore di un tribunale composto da ventuno teologi della Sorbona, chiamati a giudicare la sua opera, Lo specchio delle anime semplici, uno dei testi più profondi e poetici della letteratura occidentale.

La libertà, che i Fratelli e le Sorelle del Libero Spirito raggiungevano, era frutto di un cammino di perfezione, dove il distacco e l’abbandono diventavano la condizione per il raggiungimento della “cristificazione”, dell’unione mistica dell’uomo e di Dio. Chi raggiunge tale unione è assolutamente libero e riceve il bacio deificante della divinità. In un simile stato non ha più senso parlare di precetti, di digiuni, di peccato, di mediazioni tutte umane tra Dio e l’uomo, perché quest’uomo, che ha generato in sé il Verbo ed è divenuto Figlio, Verbo egli stesso, è ormai in Dio, una cosa sola con il Padre. Quest’uomo, che ha raggiunto lo stato di assoluta libertà, non teme quanto corrisponde alla sua natura; unico peccato contro natura, anzi, è l’ipocrisia, come sottolinea Meister Eckhart. Tra le otto proposizioni, condannate dalla Chiesa, ve n’è una che sorprende per la sua attualità: baciare una donna senza provare per lei alcuna attrazione è peccato, seguire quanto corrisponde alla propria natura non è e non può essere peccato.

I Fratelli del Libero Spirito rappresentarono, lungo il Basso Medioevo, un movimento esoterico di difficile individuazione. Solo la condanna da parte del Concilio di Vienne (1311-1312) in qualche modo li individua, dando loro il nome stesso con cui sono ricordati ed elencandone alcune proposizioni, bollate come eretiche e in grado di condurre all’apostasia. La contestuale soppressione dei Templari, da parte di quel Concilio, farà passare in secondo piano la condanna del Libero Spirito. Vienne segnò la fine di un mondo e di un progetto culturale. Grandi figure, che orbitarono intorno a questo movimento, furono in particolare quelle di Margherita Porete e Meister Eckhart, ma anche del beato Enrico Suso e Giovanni Taulero. Dallo studio dei documenti emerge l’ascendenza soprattutto gnostica, oltre che neoplatonica ed ermetica, presente nel progetto culturale di questa “scuola segreta”, che raccoglieva così l’eredità morale e spirituale del catarismo e del millenarismo giochimita. Gli echi del Libero Spirito si potranno udire ancora nel quietismo del Settecento e, attraverso esso, giungeranno inconfondibili alla contemporaneità.

Massimo Frana è laureato in Filosofia e in Studi storici, storico-religiosi e antropologici. È dottore di ricerca in Teoria e storia della storiografia filosofica. È autore di Meister Eckhart e il Libero Spirito. La mistica della liberazione, Lanciano 2009, e di Filosofia di genere. Un libro per la cura dell’omofobia, Roma 2009 (premio pari opportunità Frascati 2011). Ha pubblicato numerosi saggi e recensioni, occupandosi di pensatori, quali Martin Heidegger, Gerhard Ebeling, Friedrich Wilhelm Foerster, Meister Eckhart.