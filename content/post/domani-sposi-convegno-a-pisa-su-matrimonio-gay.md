---
title: '"DOMANI SPOSI": CONVEGNO A PISA SU MATRIMONIO GAY'
date: Fri, 11 Jun 2010 07:41:23 +0000
draft: false
tags: [Comunicati stampa]
---

Venerdì 11 giugno alle ore 18.00 alla biblioteca comunale di Pisa, la cellula Coscioni di Pisa e l'Arcigay promuovono:

**Domani sposi**

_**Il riconoscimento del matrimonio tra persone dello stesso sesso**_

**Nuovi orizzonti in seguito alla sentenza della Corte costituzionale**

Dopo la sentenza n. 138/2010 della Corte costituzionale si sono aperte nuove prospettive per il riconoscimento in Italia del matrimonio tra persone dello stesso sesso. Interverranno venerdì 11 giugno 2010 (dalle 18:00 alle 20:00) a Pisa, presso la Biblioteca comunale, esponenti politici e del mondo dell’associazionismo per discutere sulle effettive possibilità di azioni comuni per raggiungere tale obiettivo.

Ne parleranno:

*   Ezio Menzione, avvocato penalista
*   Paolo Patané, Presidente nazionale Arcigay
*   Sergio Rovasio, segretario dell'Associazione radicale Certi Diritti
*   Antonio Rotelli, avvocato e presidente nazionale dell’avvocatura LGBT - Rete Lenford

Con la testimonianza di Francesco Piomboni e Matteo Pegoraro, una coppia che nel 2007 ha chiesto le pubblicazioni di matrimonio al Comune di Firenze.

Modererà il dibattito Daniele Nardini.

Associazioni proponenti:

Arcigay Pisa

Arcilesbica Pisa

Cellula Coscioni Pisa