---
title: 'LA SCIENZA CONTRO LE FARNETICAZIONI DEL CAMERLENGO  BERTONE'
date: Sat, 17 Apr 2010 05:33:42 +0000
draft: false
tags: [Comunicati stampa]
---

Con le sue dichiarazioni, il cardinal Bertone rafforza una cultura omofobica

«La comunità scientifica dell’Ordine degli Psicologi del Lazio respinge con fermezza le affermazioni del Segretario di Stato vaticano Bertone, che hanno messo in relazione la pedofilia con l’omosessualità, a seguito dei numerosi episodi di pedofilia accaduti all’interno della Chiesa cattolica e denunciati dalla stampa internazionale».

Lo dichiara il presidente dell’Ordine degli Psicologi del Lazio, Marialori Zaccaria.

«Se possiamo comprendere il disagio della Chiesa di fronte a tali rivelazioni, non possiamo invece accettare la scelta di una linea di difesa irresponsabile per gli effetti che può causare».

«Le affermazioni di una voce così autorevole – sottolinea Marialori Zaccaria – vanno a rafforzare una cultura omofobica, che come Ordine siamo quotidianamente impegnati a contrastare, già eccessivamente diffusa nella società italiana e rivelano una grave concezione oscurantista che assimila la perversione della pedofilia all’omosessualità, ponendosi così in netto contrasto con le posizioni assunte dalle maggiori associazioni scientifiche internazionali sin dal 1973 che definiscono e descrivono l’omosessualità come variante normale dell’orientamento sessuale».

*   [Pedofilia. Ordine Psicologi Lazio: Bertone, parole da respingere. Zaccaria: "Così rafforza la cultura omofobica"](http://www.ordinepsicologilazio.it/h_rassegna_stampa/h_dicono_di_noi/pagina369.html) (Dicono di noi)
*   [Pedofilia/ Psicologi Lazio: Bertone rafforza cultura omofobica](http://www.ordinepsicologilazio.it/h_rassegna_stampa/h_dicono_di_noi/pagina368.html) (Dicono di noi)
*   [Ordine Lazio, No a relazione pedofilia con omosessualità. Affermazioni Bertonre rafforzano cultura omofobica](http://www.ordinepsicologilazio.it/h_rassegna_stampa/h_dicono_di_noi/pagina367.html) (Dicono di noi)
*   [Pedofilia: Psicologi, attenzione a non rafforzare cultura omofobica](http://www.ordinepsicologilazio.it/h_rassegna_stampa/h_dicono_di_noi/pagina371.html) (Dicono di noi)
*   [L'Ordine degli Psicologi del Lazio: "Omosessualità, variante normale dell'orientamento sessuale" ](http://www.ordinepsicologilazio.it/h_rassegna_stampa/h_dicono_di_noi/pagina370.html)

COMUNICATO STAMPA

16 aprile 2010

L’Associazione Italiana di Psicologia (AIP), principale Società Scientifica rappresentativa della Psicologia e della Ricerca Psicologica Italiana, apprese le parole pronunciate dal Cardinale Tarcisio Bertone durante la visita in Cile (“numerosi psichiatri e psicologi .... hanno dimostrato che esiste un legame tra omosessualità e pedofilia", Corriere della Sera.14.04.2010, p. 2), intende precisare che la letteratura scientifica sull’argomento non supporta in alcun modo quanto sostenuto dal Segretario di Stato della Santa Sede e che è anzi dimostrato che vittime di abuso sono tanto i bambini quanto le bambine.

In particolare, in qualità di Psicologi, Psicoterapeuti e Ricercatori sentiamo il dovere di precisare che le parole pronunciate dal Cardinale, oltre che assolutamente prive di evidenza scientifica, paiono rilanciare una pericolosa re-interpretazione in chiave psicopatologica dell’omosessualità, condizione invece da anni esplicitamente esclusa dalla nosografia psichiatrica in uso.

Pur prendendo atto delle successive dichiarazioni di rettifica, l'AIP sente il dovere di precisare che "patologizzare" l’omosessualità, invocando in modo improprio il supporto della comunità scientifica, non fa che aumentare l’omofobia, che è la vera malattia da combattere. L’errato riferimento alla letteratura psicologica, sociologica o psichiatrica appare estremamente dannoso per lo sforzo che, da anni, clinici e ricercatori intraprendono a vantaggio della salute psichica della popolazione, sia dei minori (abusati e non) che degli abusanti (questi sì realmente malati). L'AIP pertanto invita tutti coloro che ricoprono importanti ruoli istituzionali, e che quindi hanno un peso incisivo sulla pubblica opinione, a prestare maggiore attenzione alla ricerca scientifica e a diffondere correttamente i suoi risultati conoscitivi e applicativi.

prof. Roberto Cubelli

Presidente Associazione Italiana di Psicologia