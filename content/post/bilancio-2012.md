---
title: 'Bilancio 2012'
date: Tue, 09 Apr 2013 09:01:04 +0000
draft: false
tags: [Politica]
---

**                                         Situazione finanziaria al 31 dicembre 2012**

**CAPITALE SOCIALE**

**€ 5.230,66**

**RICAVI 2012**

ISCRIZIONI e CONTRIBUTI

€ 16.954,21

INTERESSI BANCARI ATTIVI

€ 13,67

**Totale**

**€ 16.967,88**

**COSTI 2012**

**SPESE GENERALI**

_Oneri e Commissioni Banca Poste e Paypal_

€ 734,07

_Spedizioni e tessere_

€ 952,35

_Siti web_

€ 426,98

_Telefono_

€ 80,00

**Subtotale**

**€ 2.193,40**

**CONTRIBUTI EROGATI**

_Iscrizione Ilga_

€ 150,00

_Iscrizione Consulta per la laicità istituzioni Milano_

€ 100,00

**Subtotale**

**€ 250,00**

**INIZIATIVE POLITICHE**

_Campagna iscrizioni 'Dai corpo ai tuoi diritti'_

€ 1.013,91

_Gadget_

€ 1.035,86

_Campagna web Stop al gemellaggio_

€ 164,82

_Campagna Adotta Piccolo Uovo_

€ 291,66

_Contributo Milano Radicalmente Nuova_

€ 178,00

_Contributo campagna affissioni Roma coppie di fatto_

€ 300,00

_Libro sui diritti delle coppie conviventi_

€ 1.500,00

_Roma Pride e Pride Park_

€ 737,20

_Evento Teatro Elfo con Amnesty_

€ 300,00

_Conferenza Prostituzione_

€ 471,60

_Partecipazione Conferenza Ilga World_

€ 645,00

_Camper della prevenzione - Roma_

€ 423,20

_Varie_

€ 2.873,76

**Subtotale**

**€ 9.935,01**

**TOTALE COSTI**

**€ 12.378,41**

**AVANZO DI BILANCIO 2012**

**€ 4.589,47**

**TOTALE ATTIVO 2012**

**€ 9.820,13**