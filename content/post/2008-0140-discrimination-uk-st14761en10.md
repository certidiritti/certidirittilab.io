---
title: '2008-0140-Discrimination-UK-ST14761.EN10'
date: Mon, 11 Oct 2010 12:42:20 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 11 October 2010

Interinstitutional File:

2008/0140 (CNS)

14761/10

LIMITE

SOC 634

JAI 827

MI 370

  

  

  

  

  

NOTE

from :

General Secretariat

to :

The Working Party on Social Questions

on :

19 October 2010

No. prev. doc. :

13883/10 SOC 561 JAI 759 MI 317

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

In preparation of the next meeting of the Working Party, which is provisionally scheduled for 19 October 2010, delegations will find attached a note from the UK delegation.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**ANNEX**

**Questions for SQWP debate on housing**

**\[Document 13883/10\]**

**UK replies**

**1.      Situation in Member States**

**Both the Racial Equality Directive (2000/43/EC) and the Gender Goods and Services Equality Directive (2004/113/EC) cover housing available to the public in their scope.  Several MS have gone beyond these two grounds in their protection against discrimination.**

**a)         To what extent does UK national legislation against discrimination apply to housing?**

Part 4 of the Equality Act 2010 (the Act) contains specific provisions which apply to the process of the disposal and management of premises such as housing. This broadly covers the sale, purchase, letting and management of premises.  The Act makes clear that where accommodation is provided for the purpose of short-term accommodation (such as hotels and holiday accommodation) separate provisions within the Act apply (relating to goods, facilities and services).

The premises provisions provide protection from discrimination, harassment and victimisation for people with a range of protected characteristics including disability, gender and race and from discrimination and victimisation because of religion or belief and sexual orientation.  However, the protection **does not apply** to the protected characteristic of **age**.

This is because in relation to age, limits may need to be imposed to meet the needs of a disadvantaged group (defined by age), or to cater for the preferences of individuals who simply wish to live exclusively with people of a similar age – for example, housing provided exclusively to people in a particular age range.

  

In relation to disability, domestic legislation requires reasonable adjustments to be made to the processes of letting through:

§  adjustments to provision, criteria or practices (such as relaxing a ban on animals for a tenant who uses a guide dog);

§  the provision of auxiliary aids (such as providing a tenancy agreement in an accessible format, or changing a light switch or tap to assist a disabled person to make use of the premises being let); or

§  adjusting a term of a letting (for example, adjusting a term that bans the drying of washing on a balcony for a person whose disability prevents him/her from using laundry and drying facilities).

These adjustments are to be made by the landlord of the premises who is responsible for meeting the costs of such reasonable adjustments.

National equality legislation also provides for a landlord to give consent, where it is reasonable to do so, for the disabled person to make disability-related alterations to the actual premises that are let (but not to the common parts).  The cost of the alteration is met by the disabled person.

New provisions yet to be implemented would allow a disabled person to request, and require the landlord to make, disability related alterations to the common parts of residential premises.  The provisions permit the landlord to require the disabled person to meet the costs of the alteration, including ongoing maintenance costs and reinstatement costs.

Furthermore, national equality legislation does not require a landlord/provider of premises to make adjustments in anticipation of the needs of potential disabled occupiers, but requires them to make reasonable adjustments in response to a request from the disabled person.  This ensures that the provider’s resources are better targeted on meeting the actual needs of a particular disabled occupier.

**b)        Do you distinguish (for this purpose) between private and public/social housing?**

No. There is no distinction made between private and public/social housing.

**c)       Do you have statistics on the number of cases of discrimination in the area of housing, and if so, what is the most prevalent ground?**

No. There are no central statistics held on the number of cases of discrimination in the area of access to goods, facilities and services, and premises including housing services.

**2.      Social Housing**

**a)      Do you consider social housing to be a service (for the purposes of EU law)?**

**b)      If not, do you consider it to be a part of the social protection system?**

The concept of social services of general interest has no legally binding definition at national nor community levels. We consider that it would be inappropriate for this Directive to attempt to define social services of general interest to include or exclude social housing.

Social housing, as an entity, is not considered to be a service or part of social protection. However, the process of allocating and managing social housing is considered to be part of the social protection system as its purpose is to provide social assistance for disadvantaged citizens or socially less advantaged groups.

We consider that the scope and organisation of social housing varies significantly across the EU according to historical and cultural specifities. As such, we support the development of a voluntary EU quality framework for social housing (covering allocation and management of social housing), rather than the current approach of making social housing subject to the scope of the proposed Equal Treatment Directive.

  

**3.      Private Life**

**In some cases, the principle of equality might be in conflict with the right to private life, in particular, in the area of housing (e.g. renting out a room in your own apartment).**

**a)      Is this better taken into account by an exclusion of transactions in the area of private life from the scope of the Directive, or by an exclusion of activities which are not commercial or professional?**

This is probably best dealt with by an exclusion of housing transactions in the area of private life.  National anti-discrimination legislation does not make a distinction between private and commercial/professional arrangements.

The UK Government is keen to protect people against discrimination in the area of housing but also recognises that it cannot place undue restrictions on what people can do with or in their own homes.  To this end, the Act provides limited exceptions for the private disposal (defined as not using advertising or the services of an estate agent) of owner-occupied premises and for the disposal, occupation and management of small premises with shared facilities, such as kitchens or bathrooms which are occupied by the landlord or a relative of the landlord. These limited exceptions are not available to exclude persons because of race.

**b)           How is commercial/professional housing defined in UK national legislation?**

There is no separate definition of commercial/professional housing in national anti-discrimination legislation.  The provisions on premises in the Act do not distinguish between private and commercial/professional arrangements.

**4.      Disability – reasonable accommodation**

**a)      In your view, what should be the obligation to provide reasonable accommodation mean for providers of housing?**

The obligation for providers of residential premises to make reasonable adjustments should be confined to adjustments to the process of allocation and management of premises.  This may include; changing a policy or practice (such as relaxing a ban on animals to allow assistance dogs), providing auxiliary aids and services (for example, adjusting documentation such as tenancy agreements into accessible formats), or changing a term of the letting (for example, to provide a car parking space). However, there should be no obligation placed on providers of housing to make physical alterations to the structure of the premises. It is considered that this would place undue burdens and costs on providers both in relation to the making of the alterations and reinstatement when the alteration was no longer required.

In addition, the requirement to make reasonable accommodation in relation to housing should not be an anticipatory one – it should only rely on the request of the individual tenant.  This would prevent adjustments being made which are not suitable for the tenant or occupier, ensure that adjustments are tailored towards a tenant’s particular requirements and facilitate the appropriate targeting of resources.

Landlords should be required (in accordance with the lease arrangements) to allow reasonable adjustments by the tenant which would alter the structure of the premises, if they are requested and paid for by the tenant.  For example, if a tenant seeks to widen a door inside the premises to enable easier wheelchair access.  The landlord should be able to refuse however if the adjustments are not deemed reasonable, for example if it could damage the structural integrity of the premises.  The landlord should not be required to pay for these alterations or for any subsequent reinstatement of the premises.

**b)      Is there legislation on the accessibility of buildings (private as well as public/social) in your country?**

There is building legislation governing the accessibility requirements of new buildings and those undergoing change of use.  For example in England and Wales, Building Regulations apply, together with guidance setting out how the accessibility requirements may be met. However, building regulations only apply when building work is being undertaken – they are not retrospective.

This is distinct from equality legislation requirements to make reasonable adjustments in relation to the letting and management of housing premises or the provision of goods, facilities and services.

The equality legislation requirements as regards reasonable adjustments to achieve greater accessibility in general are applied differently according to whether access is in relation to housing premises, or to premises from which services are delivered.

Housing: The requirements are set out in the comments on Question 1 and in the paragraphs under a) above.

Goods, services and facilities:  The legislative requirement is for the service provider to make adjustments that are required to enable the disabled person to access the service being provided rather than to ensure access to the service provider’s premises or building.  The legislation applies where a physical feature of a service provider’s premises would place a disabled person at a substantial disadvantage, compared to non-disabled people, in accessing the service.  The service provider is required to take reasonable steps to avoid that disadvantage.  Reasonable adjustments may include alterations or adaptations to the premises, but they can also include providing the service by an alternative means, such as providing a personal shopping service, or delivering goods to a disabled customer where the disabled customer is unable to access the shop.

**5.      Disability – accessibility**

**a)      What should the obligation to provide for accessibility measures encompass in the areas of housing?**

As described in question 4, the focus for anti-discrimination legislation should be on reasonable adjustments to the process of allocation and management of premises.

There should be a clear distinction made between:

§    the need for reasonable adjustments to improve disabled people’s ability to access services, and to make use of their housing, which should be applied under anti-discrimination legislation; and

§    any general accessibility requirements to be met when buildings are being constructed/adapted, which should be applied under building regulations.

**b)           Should there be a difference between old and new buildings?**

In respect of anti-discrimination legislation for reasonable accommodation to improve disabled people’s access to buildings, there should be no distinction made between new and old buildings.  The impact of making a disability-related adjustment to the historic value or appearance of a building should be one of the factors to be taken into account when deciding whether or not it is reasonable to make the adjustment.  The legislation should not exclude adjustments from being made to older buildings or those of special architectural interest if it would be reasonable in all the circumstances to make adjustments to such buildings. The UK has many examples of successful accessibility improvements to historic buildings.

**c)           Do you support immediate applicability with respect to new buildings?**

In respect of anti-discrimination legislation that prevents discrimination in the allocation and management of housing premises, we would support immediate applicability.  However, the requirement for accessibility features in new buildings should be a matter for separate building legislation, rather than anti-discrimination legislation.

**d)      Should there be a difference between the obligations for private and public/social housing respectively?**

Subject to the exclusion of arrangements that affect private life, there should be no distinction made between private and public/social housing in anti-discrimination legislation relating to the allocation and management of housing premises.  Private, public and social landlords are all involved in similar procedures and processes relating to the allocation and management of housing, so should equally be subject to equality legislation.

**e)       Are there public funds available in your country for the adaptation of housing (private as well as public/social)?**

Funding is available under a scheme of Disabled Facilities Grants administered by local authorities, for individual disabled people to be able to make disability-related alterations to the premises in which they live.  Funding which meets some of the extra costs incurred by disabled people is also provided in the form of a non-means-tested allowance, subject to entitlement conditions.  However, there is no funding specifically available for landlords to make reasonable adjustments as the Government considers it inappropriate to provide funds for them to meet their legal duties.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_