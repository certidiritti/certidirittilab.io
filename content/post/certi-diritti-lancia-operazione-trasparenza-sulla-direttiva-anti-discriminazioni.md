---
title: 'CERTI DIRITTI LANCIA OPERAZIONE TRASPARENZA SULLA DIRETTIVA ANTI-DISCRIMINAZIONI'
date: Fri, 03 Dec 2010 15:52:17 +0000
draft: false
tags: [Comunicati stampa]
---

**WIKI-RIGHTS: IL 10 DICEMBRE, GIORNATA MONDIALE DIRITTI UMANI, PUBBLICATI I DOCUMENTI RISERVATI DEL CONSIGLIO EUROPEO DELL'UE (L’ORGANISMO CHE RAPPRESENTA I GOVERNI) CHE PROVANO COME ALCUNI PAESI DELL’UE, CON ARGOMENTI FITTIZI, BLOCCHINO LA DIRETTIVA ANTIDISCRIMINAZIONI DELLA COMMISSIONE EUROPEA.**

**CERTI DIRITTI LANCIA OPERAZIONE TRASPARENZA SULLA DIRETTIVA UE BLOCCATA. CHIEDE AI GOVERNI ITALIANO, TEDESCO E CECO DI INTERROMPERE IL BOICOTTAGGIO.**

Comunicato Stampa dell'Associazione Radicale Certi Diritti:

Bruxelles – Roma,  dicembre 2010

Il 10 dicembre 2010, in occasione della Giornata mondiale per i Diritti Umani, l’Associazione Radicale Certi Diritti lancia l’"operazione trasparenza" sulla direttiva anti-discriminazioni proposta dalla Commissione Europea  nel 2008, pubblicando sul suo sito tutti i documenti del Consiglio di cui siamo venuti in possesso e che rivelano l'opposizione costante e pretestuosa di alcuni Governi, in particolare di quello italiano, tedesco e ceco, rispetto all'eguaglianza dei cittadini senza discriminazione, con l’obiettivo di tenere bloccata tale Direttiva.

La direttiva anti-discriminazioni, se approvata, permetterebbe di allargare la lotta contro le discriminazioni basate sull'orientamento sessuale, sull'età, sulla religione e sulla disabilità dal solo campo del lavoro (già coperto dalla direttiva in vigore 200/78/CE) anche ai campi della sicurezza sociale, dell'educazione, dell'accesso ai beni e servizi, includendo l'accesso all'abitazione ed alla casa.

In occasione della discussione sulla situazione della Direttiva nel Consiglio, svoltasi al Parlamento Europeo, in  Commissione Libertà Pubbliche  lo scorso 30 novembre, la Presidenza belga ha affermato che numerosi Stati membri hanno sollevato costantemente veti ed obiezioni in merito al presunto non rispetto dei principi di sussidiarietà, proporzionalità e sicurezza giuridica; si tratta ovviamente di pretesti politici per bloccare la Direttiva.

La presidenza belga, visto il blocco politico, ha lavorato sugli aspetti tecnici (in particolare la questione servizi finanziari ed età; handicap ed accesso all'edilizia; privacy e divieto di discriminazione nell'affitto di stanze in abitazioni private), al fine di spianare la strada rispetto ad una futura -sebbene difficile - approvazione.

La Commissaria Reding, in rappresentanza della Commissione Europea, ha tenuto incontri con i Governi nazionali sperando di sbloccare la situazione, senza successo. Per la Commissione Europea  rimane importante assicurare che la direttiva non sia smembrata in 4 direttive distinte (sulla religione, sull'orientamento sessuale, sull'età e sulla disabilità).

I deputati europei intervenuti hanno contestato la mancanza di volontà politica dei Governi al riguardo, hanno biasimato l'opposizione della Germania, dell'Italia e della Repubblica Ceca, la permanenza delle discriminazioni nell'accesso alle abitazione private, l'erosione dei contenuti della direttiva in generale. L'Agenzia europea per i diritti fondamentali, presentando il suo rapporto sull'omofobia, ha sostenuto la necessità di adottare la direttiva e di promuovere cosi l'eguaglianza.

**L’Associazione Radicale Certi Diritti ha così deciso di rendere pubblici e pubblicare per la prima volta ([www.certidiritti.it/wiki-rights.html](wiki-rights.html)), oltre 70 file del Consiglio Europeo dell'UE, indirizzati al Working Party on Social Question, dove sono documentate per iscritto le proposte e le obiezioni, anche quelle fittizie, dei vari Stati membri rispetto alla Direttiva della Commissione**

L’appello che facciamo a tutti le Associazioni, che in Europa si battono per i diritti civili e umani, è quello di promuovere una serie di iniziative al fine di dare un contributo allo sblocco della Direttiva. In Italia verrà quanto prima depositata dai deputati radicali, primi firmatari Matteo Mecacci e Rita Bernardini, una interrogazione parlamentare sulla vicenda, per chiedere conferma e ragione della posizione del governo italiano contro tale Direttiva.

**![operazione_trasparenza_EU](http://www.certidiritti.org/wp-content/uploads/2010/12/operazione_trasparenza_EU.png)CERTI DIRITTI LLANÇA UNA OPERACIÓ TRANSPARÈNCIA SOBRE LA DIRECTIVA ANTIDISCRIMINACIÓ I DEMANA ALS GOVERNS ITALIÀ, ALEMANY I TXEC QUE POSIN FI AL BOICOT DE LA DIRECTIVA PROPOSADA PER LA COMISSIÓ EUROPEA L’ANY 2008**

**WIKI-RIGHTS: EL 10 DE DESEMBRE, DIA MUNDIAL DELS DRETS HUMANS, PUBLICATI ELS DOCUMENTS RESERVATS DEL CONSELL DE LA UE (ORGANISME REPRESENTANT DELS GOVERNS) ON ES POSA DE MANIFEST COM ALGUNS PAÏSOS DE LA UE BLOQUEGEN LA DIRECTIVA AMB ARGUMENTS FICTICIS.**

Brussel.les - Roma, 10 desembre de 2010

El 10 de desembre de 2010, amb ocasió del Dia Mundial dels Drets Humans, l'Associació Radical Certi Diritti llança una "operació transparència" sobre la Directiva antidiscriminació proposada per la Comissió Europea l’any 2008, i publicarà en la seva pàgina web tots els documents del Consell que posen de manifest l'oposició constant i plena de pretextos d’alguns governs com l'italià, l’alemany i txec respecte la no discriminació, amb l'objectiu de bloquejar l'adopció d'aquesta Directiva.

La Directiva antidiscriminació, en cas de ser aprovada, permetria ampliar la lluita contra la discriminació basada en l'orientació sexual, l'edat, la religió i la discapacitat en l'àmbit del treball (coberta per la directiva 200/78/CE actualment en vigor) als àmbits de la seguretat social, l'educació i l'accés als béns i serveis, incloent l'accés a l'allotjament i a l'habitatge.

En ocasió del debat celebrat el passat 30 de novembre a la Comissió de Llibertats Públiques del Parlament Europeu sobre la situació de la Directiva al Consell, la Presidència belga va afirmar que molts estats membres han plantejat constantment vetos i objeccions davant d’una presumpta violació del principi de subsidiarietat, proporcionalitat i seguretat jurídica. Es tracta, òbviament, de pretextos polítics amb l'objectiu de bloquejar la Directiva.

La Presidència belga, davant d’aquest bloqueig polític, ha treballat els aspectes tècnics (concretament la qüestió dels serveis financers i l'edat, la discapacitat i l'accés a l'habitatge, privacitat i prohibició de discriminació en el lloguer d'habitacions en cases particulars) amb l'objectiu de facilitar el camí per a una futura, encara que difícil, adopció.

La representant de la Comissió Europea, Vivianne Reding, ha mantingut trobades amb els governs dels estats membres per tractar de desbloquejar, sense èxit, la situació. Per a la Comissió Europea és primordial que la Directiva no es fragmenti en quatre directives diferents (sobre la religió, orientació sexual, edat i discapacitat).

Els diputats europeus que van intervenir en el debat van criticar la manca de voluntat política dels governs i van lamentar no només l'oposició d'Alemanya, Itàlia i la República Txeca a l’adopció del text, sinó també el manteniment de les discriminacions en l'accés a les habitacions en cases particulars i l'erosió dels continguts de la Directiva en general.

L'Agència dels Drets Fonamentals de la Unió Europea, durant la presentació del seu informe sobre l'homofòbia, va sostenir la necessitat d'adoptar la directiva per promoure la igualtat.

L'Associació Radical Certi Diritti ha decidit publicar per primera vegada en la seva pàgina web ([www.certidiritti.it/wiki-rights.html](wiki-rights.html)), més de 70 documents dirigits al Grup de Treball de qüestions socials del Consell, en què es documenten per escrit les propostes i objeccions, fins i tot aquelles fictícies, de diversos Estats membres respecte a la Directiva de la Comissió.

Certi Diritti fa una crida a totes les associacions que lluiten a Europa pels drets civils i humans perquè promoguin iniciatives que posin fi al bloqueig d'aquesta Directiva. A Itàlia es dipositarà en breu una pregunta parlamentària sobre aquest tema per esbrinar quines son les raons de l'oposició del govern italià contra la Directiva.

Per aconseguir una còpia de la documentació es poden adreçar les peticions a:

[info@certidiritti.it](mailto:info@certidiritti.it)

**![operazione_trasparenza_EU](http://www.certidiritti.org/wp-content/uploads/2010/12/operazione_trasparenza_EU.png)CERTI DIRITTI LANZA LA OPERACION TRANSPARENCIA SOBRE LA DIRECTIVA ANTIDISCRIMINACION Y PIDE A LOS GOBIERNOS ITALIANO, ALEMAN Y CHECO QUE PONGAN FIN AL BOICOT DE LA DIRECTIVA PROPUESTA POR LA COMISION EUROPEA EN 2008**

**WIKI-RIGHTS: EL 10 DE DICIEMBRE, DIA MUNDIAL DE LOS DERECHOS HUMANOS, PUBLICA LOS DOCUMENTOS RESERVADOS DEL CONSEJO DE LA UE (ORGANISMO REPRESENTANTE DE LOS GOBIERNOS) QUE REVELAN COMO ALGUNOS PAISES DE LA UE BLOQUEAN LA DIRECTIVA CON ARGUMENTOS FICTICIOS.**

**Bruselas – Roma, 10 de diciembre de 2010**

El 10 de diciembre de 2010, con ocasión del Día Mundial de los Derechos Humanos, la Asociación Radical Certi Diritti lanza la “operación transparencia” sobre la Directiva antidiscriminación propuesta por la Comisión Europea en 2008, publicando en su pagina web todos los documentos del Consejo que revelan la oposición constante y llena de pretextos de gobiernos como el italiano, alemán y checo respecto a la no discriminación, con el objetivo de bloquear la adopción de esta Directiva.

La Directiva antidiscriminación, en caso de ser aprobada, permitiría ampliar la lucha contra la discriminación basada en la orientación sexual, la edad, la religión y la discapacidad en el ámbito del trabajo (cubierta por la directiva 200/78/CE actualmente en vigor) a los ámbitos de la seguridad social, la educación y el acceso a los bienes y servicios, incluyendo el acceso al alojamiento y a la vivienda.

Con ocasión del debate celebrado el pasado 30 de noviembre en la Comisión de Libertades Públicas del Parlamento Europeo sobre la situación de la Directiva en el Consejo, la Presidencia belga afirmó que muchos estados miembros han planteado constantemente vetos y objeciones ante una presunta violación del principio de subsidiariedad, proporcionalidad y seguridad jurídica. Se trata, obviamente, de pretextos políticos con el objetivo de bloquear la Directiva.

La Presidencia belga, ante este bloqueo político, ha trabajado en los aspectos técnicos (concretamente la cuestión de los servicios financieros y la edad; la discapacidad y el acceso a la vivienda; privacidad y prohibición de discriminación en el alquiler de habitaciones en casas particulares) con el objetivo de allanar el camino para una futura, aunque difícil, adopción.

La representante de la Comisión Europea, Vivianne Reding, ha mantenido encuentros con los gobiernos de los estados miembros para tratar de desbloquear, sin éxito, la situación. Para la Comisión Europea es primordial que la Directiva no se fragmente en cuatro Directivas diferentes (sobre la religión, orientación sexual, edad y discapacidad).

Los diputados europeos que intervinieron en el debate criticaron la falta de voluntad política de los gobiernos y lamentaron no sólo la oposición de Alemania, Italia y la República Checa para adoptar el texto, sino también el mantenimiento de las discriminaciones en el acceso a las habitaciones en casas particulares  y la erosión de los contenidos de la Directiva en general.

La Agencia de los Derechos Fundamentales de la Unión Europea, durante la presentación de su informe sobre la homofobia, sostuvo la necesidad de adoptar la directiva para poder promover la igualdad.

La Asociación Radical Certi Diritti ha decidido publicar por primera vez([www.certidiritti.it/wiki-rights](wiki-rights)), más de 60 documentos dirigidos al Grupo de Trabajo sobre cuestiones sociales del Consejo, en los que se documentan por escrito las propuestas y objeciones, incluso aquellas ficticias, de varios Estados miembros respecto a la Directiva de la Comisión.

**Certi Diritti hace un llamamiento a todas las Asociaciones que luchan en Europa por los derechos civiles y humanos para que promuevan iniciativas que pongan fin al bloqueo de esta Directiva. En Italia se depositará en breve una pregunta parlamentaria sobre este tema para confirmar las razones de la oposición del gobierno italiano contra la Directiva.**

**Para conseguir una copia de la documentación se pueden dirigir las peticiones a:**

**[info@certidiritti.it](mailto:info@certidiritti.it)**

**![operazione_trasparenza_EU](http://www.certidiritti.org/wp-content/uploads/2010/12/operazione_trasparenza_EU.png)****CERTI DIRITTI’ LANCE L’OPERATION TRANSPARENCE SUR LA DIRECTIVE ANTI-DISCRIMINATIONS**

**WIKI-RIGHTS: le 10 décembre, Journée Mondiale des Droits de l’Homme, nous avons publiè les documents réservés du Conseil de l’Union Européenne (l’organisme qui représente les Gouvernements) qui documentent que certains pays de l’UE, avec des arguments fictifs, bloquent la directive anti-discriminations de la Commission Européenne.** 

 **‘CERTI DIRITTI’ LANCE L’OPERATION TRANSPARENCE SUR LA DIRECTIVE UE BLOQUEE ET DEMANDE AUX GOUVERNEMENTS ITALIEN, ALLEMAND ET TCHEQUE D’ARRETER LE BOYCOTTAGE**

_Communiqué de presse de l’Association Radicale ‘Certi Diritti’:_

Le 10 décembre 2010, à l’occasion de la Journée Mondiale des Droits de l’Homme, l’Association Radicale ‘Certi Diritti’ lance l’Opération Transparence sur la directive anti-discriminations proposée par la Commission Européenne en 2008, en publiant sur son site tous le documents du Conseil dont nous sommes entrés en possession et qui révèlent l’opposition constante et prétextueux de certains Gouvernements, en particulier ceux italien, allemand et tchèque, en ce qui concerne l’égalité des citoyens sans discrimination, avec l’objectif de bloquer telle directive.

La directive anti-discriminations, si approuvée, permettrait d’élargir la lutte contre les discriminations fondées sur l’orientement sexuel, l’âge, la religion et l’handicap du seul champ du travail (déjà couvert par la directive en vigueur 200/78 CE) aux domaines de la sécurité sociale, de l’éducation, de l’accès aux biens et services, en y comprenant l’accès au logement.

A l’occasion du débat du Conseil sur la situation de la directive, qui s’est tenue au Parlement Européen en Commission des Libertés Civiles le dernier 30 novembre, la Présidence belge a déclaré que de nombreux Etats membres ont constamment opposé des vetos et des objections au prétendu non-respect des principes de subsidiarité, proportionnalité et sécurité juridique; il s’agit bien évidemment de prétextes politiques pour bloquer la Directive.

La présidence  belge, étant donné l’impasse politique, a travaillé sur les aspects techniques (en particulier les questions: services financiers et âge; handicap et accès aux bâtiments; respect de la vie privée et interdiction de discrimination sur la location de chambres dans des habitations privées), pour ouvrir la voie en prévision d’une future – même si difficile – approbation.

Mme Reding, à nom de la Commission européenne, a tenu des réunions avec les Gouvernements nationaux dans l'espoir de sortir de l'impasse, sans succès. Pour la Commission Européenne reste important de s’assurer que la directive ne soit pas démembrée en 4 directives séparées (sur la religion, l’orientation sexuelle, l’âge et l’handicap).   Les députés européens intervenus ont contesté le manque de volonté politique des Gouvernements à cet égard, ont blâmé l'opposition de l'Allemagne, de l'Italie et de la République Tchèque, la permanence de discriminations dans l'accès aux logements privés, l'érosion du contenu de la directive en général. L'Agence européenne des droits fondamentaux, en présentant son rapport sur l'homophobie, a plaidé pour l'adoption de la directive et ainsi la promotion de l'égalité.  

**L’Association Radicale ‘Certi Diritti’ a donc décidé de rendre publics et de publier pour la première fois [http://www.certidiritti.it/wiki-rights.html](wiki-rights.html), plus de 70 dossiers du Conseil de l’Union Européenne, adressés au Working Party on Social Question, qui documentent par écrit les propositions et les objections, même les fictives, des différents Etats membres en ce qui concerne la Directive de la Commission.** 

L’appel que nous lançons à toutes les associations qui, en Europe, lutte pour les droits civils et humains, est de promouvoir une série d’initiatives visant à contribuer au déblocage de la Directive. En Italie une interrogation parlementaire sera présentée dès que possible par les députés radicaux, premiers signataires Matteo Mecacci et Rita Bernardini, pour demander confirmation et raison de la position du Gouvernement italien par rapport à telle Directive.

![operazione_trasparenza_EU](http://www.certidiritti.org/wp-content/uploads/2010/12/operazione_trasparenza_EU.png)**_CERTI DIRITTI_** **ISSUES A TRANSPARENCY DEAL ON NON-DISCRIMINATIONS DIRECTIVE.**

**Wiki-Rights: December 10th, human rights world day. We release the confidential documents of the EU’s European council (the organization that represents Governments) which prove how certain countries belonging to the EU hamper the European Commission non-discriminations directive by means of fictitious arguments.**

**_Certi Diritti_** **is launching a transparency deal on the EU standing-still directive and asking to Italian, German and Czech governments to interrupt their boycott.**

**Radical association _Certi Diritti_ press release:**

December 10th 2010, on the occasion of Human Rights World day, the Radical Association _Certi Diritti_ promotes the “transparency deal” on non-discriminations directive, put forward by European Council in 2008, issuing on its site all the Council’s documents we took possession of and which reveal the steady and spurious opposition of some Governments - particularly of Italian, German and Czech - about indiscriminate citizens equality, whose aim is keep such directive blocked.

The non-discriminations directive, If got ever approved, would allow to widen the struggle against all the discriminations based on sexual orientation, age, religion and on workplace disability (already covered by in force directive 200/78 CE) also to social security fields, such as education, free access to goods and public services, including the access to the house.

On the occasion of the discussion about the situation of directive in the Council – which took place in the European Parliament, in conjunction with public freedoms Parliamentary Committee last November 30th – Belgian Presidency affirmed that several member states constantly raised vetos and objections concerning the presumed non-respect of subsidiarity principles, proportionality and legal security; it’s obviously about expedients to block the directive.

Belgian Presidency, given the political block, worked on technical aspects (in particular on financial services and age questions, handicap and building trade access, privacy and discriminations ban in real estate or private apartments rental) in order to pave the way for a future, though hard to reach, approval.

The Reading commissioner, representing the European Commission, held meetings with national Governments longing to break the deadlock, unsuccessfully. For the European Commission remains important the directive not to be dismembered into four separate directives (about religion, about sexual orientation, about the age and about the disability).

European deputies who attended those meetings challenged the lack of political will of the Governments previously said, and blamed on Germany, Italy and Czech Republic for their opposition, for the enduring of discriminations on the access to private houses and for the erosion of the directive’s contents in general as well.

The European agency of fundamental rights, introducing its report about homophobia, claimed the necessity to adopt the directive and finally promote the equality.

The Radical Association _Certi Diritti_ therefore decided to make everybody know, and release for first time ever on its site ([www.certidiritti.it/wiki-rights.html](wiki-rights.html)), over seventy files belonging to the EU’s European Council, headed to the Working Party on Social Questions, in which proposals and objections – fictitious ones too – by various member states despite Commission’s directive are documented in writing.

The appeal we make to all the associations, struggling for civil and human rights throughout Europe, is to promote a series of initiatives in order to help concurring to break directive’s deadlock.

In Italy will be promptly register, by radical deputies such as Matteo Meccacci and Rita Bernardini, a parliamentary inquiry about the affair, demanding for confirmation and agreement of the Italian Government against such directive.

[info@certidiritti.it](mailto:info@certidiritti.it)