---
title: 'COMUNICATO CERTI DIRITTI ABRUZZO E  AGEDO'
date: Thu, 10 Dec 2009 19:02:01 +0000
draft: false
tags: [Senza categoria]
---

Sabato 12 Dicembre, in piazza Salotto, Pescara, Agedo (Associazione di GEnitori e amici Di Omosessuali) e l'Associazione Radicale Certi Diritti sez. Abruzzo incontrano la popolazione pescarese/abruzzese. Genitori e figli, insieme, saranno a disposizione della gente per parlare dei diritti civili, e di quanto sia normale e naturale la vita insieme, poco importa quali orientamenti sessuali si abbia.

L'associazione radicale Certi Diritti, inoltre, promuoverà la campagna di "Affermazione Civile", condotta insieme alla Rete di Avvocatura LGBT “Rete Lenford”. “Affermazione Civile” è nata per aiutare quelle coppie dello stesso sesso che vogliono chiedere la pubblicazione degli atti per il matrimonio civile presso il proprio comune. All'eventuale rifiuto dell'Ufficiale di Stato Civile, si potrà procedere con l'impugnazione davanti al giudice. Così hanno già fatto tante coppie dello stesso sesso in Italia, e alcune sono già arrivate davanti alla Corte Costituzionale.

Verranno inoltre raccolte firme per un appello contro l'omofobia indetto dalla UISP, Unione Italiana Sport per Tutti, e per una petizione al parlamento italiano e europeo a favore di una coppia di Savona che necessita di assistenza reciproca e sta battendo una lotta tutta sua speciale per potersi sposare civilmente nel loro comune.

Infine, sarà presente vario materiale informativo, libri ecc... che potrebbero essere delle idee regalo. Invitiamo tutti pescaresi/abruzzesi a mettere sotto l'albero la normalità di essere gay e lesbiche, anche in famiglia, anche a Pescara. E perché no, a regalarsi l'iscrizione a queste associazioni che si impegnano per dare all’Italia un contributo importante di tolleranza ed emancipazione.