---
title: 'Certi Diritti: a Palermo per l''uguaglianza e la globalizzazione dei diritti'
date: Sat, 16 Mar 2013 09:30:00 +0000
draft: false
tags: [Politica]
---

Dichiarazione dell'associazione radicale Certi Diritti

Roma, 16 marzo 2013

L'Associazione radicale Certi Diritti nel dare il suo pieno appoggio al Pride nazionale a Palermo, si augura che possa diventare una occasione importante per il rilancio delle battaglie per la piena uguaglianza delle persone gay lesbiche e transessuali in Italia e nel Sud d'Europa. Dobbiamo essere consapevoli che un paese come il nostro deve riuscire a trovare le energie sia per attuare quelle riforme ormai necessarie pe ril nostro Paese per le persone lgbt e le loro famiglie e contemporaneamente sostenere tutti i tentativi - purtroppo ancora deboli - di garantire diritti e dignità nei Paesi che si affacciano al mediterraneo. Ci auguriamo che il Pride di Palermo sappia diventare una grande occasione di dialogo e di confronto perchè vi sia quella globalizzazione dei diritti che oggi è drammaticamente frenata, non solo nella Federazione russa e nell'Africa subsahariana, ma anche nei paesi ove la primavera araba si sta oscurando di sessuofobia e omofobia.

L'Associazione farà tutto quello che è in grado di fare perchè questo appuntamento sia ricco e partecipato: il nostro VI Congresso nazionale che si svolgerà a Napoli dal 5 al 7 aprile prossimi sarà una occasione di approfondimento e riflessione sul tema dell'uguaglianza reale, e non solo formale, per la quale stiamo tutti e tutte lottando.

Enzo Cucco, presidente  
Associazione Radicale Certi Diritti