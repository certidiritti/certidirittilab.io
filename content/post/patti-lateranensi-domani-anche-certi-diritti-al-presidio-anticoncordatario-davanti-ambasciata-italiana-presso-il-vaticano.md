---
title: 'Patti lateranensi: domani anche Certi Diritti al presidio anticoncordatario davanti ambasciata italiana presso il Vaticano'
date: Wed, 15 Feb 2012 16:52:39 +0000
draft: false
tags: [Politica]
---

Anniversario dei patti lateranensi: cosa c'è da festeggiare? No al Concordato si alle libertà e ai diritti civili. 'Abolire il concordato, denunciare il trattato'!

Roma, 15 febbraio 2012

Comunicato Stampa dell’Associazione Radicale Certi Diritti

Domani, giovedì 16 febbraio si celebra la nefasta data dell’anniversario dei Patti Lateranensi e del Concordato tra Italia e Stato teocratico del Vaticano

Alla cerimonia che si svolgerà in pompa magna da 600 persone, sarà presente il Governo quasi al completo insieme ad Alfano, Bersani, Casini e i vertici di Enel, Terna, Ania, Ance, Martini-Bacardi, Cambiaso Risso, Carige, Santander, Aiop-Associazione italiana ospedalità privata, Total-Erg. Saranno tutti insieme per celebrare, insieme a Bertone, Bagnasco e tanti cardinali, gli 83 anni dei Patti Lateranensi.

Anche l’Associazione Radicale Certi Diritti , insieme a Radicali Italiani e ad altre Associazioni che si battono per le libertà e i diritti civili,  sarà presente alla cerimonia al presidio anti-concordatario previsto fuori dall’Ambasciata alle ore 16 in Via delle Belle Arti, 2, nei giardini davanti all’ambasciata con lo slogan: 'No al Concordato, si alle libertà e ai diritti civili!'.

E’ ora che l’Italia diventi uno Stato laico e democratico e che il Vaticano la smetta di dettare legge.

ABOLIRE IL CONCORDATO, DENUNCIARE IL TRATTATO!