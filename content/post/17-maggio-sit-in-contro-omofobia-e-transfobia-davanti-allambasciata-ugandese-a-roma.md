---
title: '17 maggio: sit-in contro omofobia e transfobia davanti all''ambasciata ugandese a Roma'
date: Thu, 16 May 2013 10:51:04 +0000
draft: false
tags: [Africa]
---

Comunicato stampa dell'Associazione Radicale Certi Diritti

Roma, 16 maggio 2013

Ci sono al mondo più di 70 paesi ove l'omosessualità e la transessualità sono reato, in sette di questi esiste (ed è praticata) la pena di morte. Non è possibile quindi dimenticarci di questi Paesi proprio il prossimo 17 maggio, Giornata mondiale contro l'omofobia e la transfobia. Oltre alle decide di iniziative che in tutta Italia si svolgono ed alle parole delle Istituzioni che ascolteremo al Senato, presente il presidente del Senato e la Ministra alle pari opportunità, abbiamo convocato un sit in davanti all'Ambasciata ugandese a Roma ( Viale Giulio Cesare 71, alle ore 14,30 ) per ricordare che in quello stato non solo esiste una legge che punisce le persone omosessuali ma che il Parlamento sta discutendo un progetto che prevede l'inserimento della pena di morte.

Il sit in vuole essere un estremo tentativo di dialogo e di apertura al popolo ugandese che si batte contro questa norma aberrante ed a tutte le associazioni dei diritti umani che lavorano in Africa e nel mondo per la difesa dei diritti umani di tutti e di tutte.

**Il sit in è convocato da:**

*   Associazione radicale Certi Diritti
*   Agedo
*   Famiglie Arcobaleno
*   Arcigay
*   Arcilesbica
*   Equality Italia
*   MIT
*   Circolo di cultura lgbt Mario Mieli
*   Gaynet
*   Coordinamento Sylvia Rivera
*   Consultorio Transgenere Lucca

**[Evento Facebook](https://www.facebook.com/events/471363109598539/)**