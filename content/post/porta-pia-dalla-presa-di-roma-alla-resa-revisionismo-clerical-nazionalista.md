---
title: 'PORTA PIA: DALLA PRESA DI ROMA ALLA RESA, REVISIONISMO CLERICAL-NAZIONALISTA'
date: Wed, 15 Sep 2010 10:16:19 +0000
draft: false
tags: [Comunicati stampa]
---

BRECCIA DI PORTA PIA: DALLA PRESA DI ROMA ALLA RESA. DA ALEMANNO REVISIONISMO CLERICALE E NAZIONALISTA.

Roma, 15 settembre 2010  
   
Dichiarazione di Mario Staderini, Segretario di Radicali Italiani e Sergio Rovasio, Segretario Associazione Radicale Certi Diritti:

“Come già tentò il fascismo, il Sindaco Alemanno, folgorato sulla via del Campidoglio dopo i fasti pagani dei Campi Hobbit, vuole ridurre a simbolo nazionalista  un evento che segnò per l’intera Europa l’inizio di una nuova libertà di coscienza, di pensiero e di religione.  
Dopo che per decenni le istituzioni italiane hanno praticato la rimozione culturale del XX settembre, questa volta il Comune di Roma ha organizzato grandi celebrazioni al solo scopo di costruire una nuova memoria degli avvenimenti e dei personaggi.  
Se non fosse vero, verrebbe da ridere pensando che oggi i bersaglieri sono andati in Vaticano per suonare l’inno pontificio, e che sabato il Convegno di apertura delle celebrazioni sarà dedicato a Pio IX, il Papa Re del Sillabo e delle esecuzioni capitali.  
Di fronte a questa opera di revisione storica  cui Alemanno si è prestato sotto la sapiente regia delle gerarchie vaticane e delle crescenti schiere di atei devoti, invitiamo tutti i cittadini domencia 19 settembre a Porta Pia alle ore 12,30 per una grande manifestazione per la libertà e la democrazia per la vera commemorazione della Breccia di Porta Pia".