---
title: 'CORTE COSTITUZIONALE PORTOGALLO: SI ALLA LEGGE SUL MATRIMONIO GAY.'
date: Fri, 09 Apr 2010 05:50:21 +0000
draft: false
tags: [Comunicati stampa]
---

**PORTOGALLO, MATRIMONI GAY: LA CORTE COSTITUZIONALE PORTOGHESE DA' IL VIA LIBERA AI MATRIMONI TRA PERSONE DELLO STESSO SESSO. L’ASSOCIAZIONE RADICALE CERTI DIRITTI: ORA TOCCA ALL’ITALIA.**

**Bruxelles-Roma, 9 aprile 2010**

**Dichiarazione di Sergio Rovasio e Ottavio Marzocchi, Segretario e responsabile delle questioni europee dell’Associazione Radicale Certi Diritti:**

"Certi Diritti si felicita per il via libera che la Corte Costituzionale Portoghese ha dato ieri alla legge che introduce i matrimoni tra persone dello stesso sesso in Portogallo. La legge di modifica al codice civile, che era stata proposta dal governo socialista subito dopo le elezioni e poi approvata dal Parlamento - dove l'opposizione conservatrice proponeva comunque una legge sulle unioni civili - era stata sottoposta dal Presidente della Repubblica del Portogallo, Cavaco Silva, alla Corte Costituzionale, perché questa ne verificasse l'eventuale incostituzionalità. La Corte ha respinto le riserve del Presidente della Repubblica, ricordando il divieto di discriminazioni basate sull'orientamento sessuale ed affermando che nulla nella Costituzione portoghese impedisce l'entrata in vigore di una tale legge, eliminando così l'ultimo ostacolo sulla strada del matrimonio tra persone dello stesso sesso.

L’Associazione Radicale Certi Diritti si felicita per tale decisione, che lascia l'Italia tra gli ultimi stati europei a non prevedere alcuna forma di riconoscimento delle relazioni tra persone dello stesso sesso, né attraverso il matrimonio, né attraverso una legge sulle unioni civili. Di fronte all'inazione del legislatore italiano, Certi Diritti attende con fiducia la sentenza della Corte Costituzionale, prevista dopo il 12 aprile, nella speranza che l'Italia possa rientrare in Europa invece che allontanarsene sempre di più".