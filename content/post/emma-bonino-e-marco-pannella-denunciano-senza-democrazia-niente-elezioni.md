---
title: 'EMMA BONINO E MARCO PANNELLA DENUNCIANO: SENZA DEMOCRAZIA NIENTE ELEZIONI'
date: Sat, 20 Feb 2010 09:08:52 +0000
draft: false
tags: [Comunicati stampa]
---

20 febbraio 2010

([a questo link l'intero documento con gli allegati](http://www.boninopannella.it/sites/default/files/tutto_il_comunicato_con_allegati.pdf))

_Se - come è purtroppo ormai probabile – si dovesse giungere al voto regionale del 28/29 marzo nelle attuali condizioni di negate legalità e democrazia, la decisione del parteciparvi o no s’impone sin d’ora come gravissimo, inevitabile problema di coscienza dinanzi all’inverarsi (per nonviolenti democratici quali siamo) del sicuro rischio di incorrere nel reato di complicità con opere di un Regime che negano radicalmente diritti umani, costituzionali, internazionali,  individuali e collettivi; Regime che tende e sempre più riesce a ridurre lo Stato a mera copertura legalistica di questi criminiI **PRECIPITA VERSO ELEZIONI CAUCASICO PUTINIANE**?_

![Marco Pannella e Emma bonino](http://www.boninopannella.it/sites/default/files/images/pannella_bonino1.jpg)Dobbiamo constatare che il tempo fino ad oggi trascorso dalle “elezioni” europee (6/7 giugno) ha infatti sempre più e più gravemente coinvolto tutte le funzioni Istituzionali: politiche, amministrative, di controllo facendole letteralmente precipitare, per mera forza di gravità ,come in un pozzo senza fine di comportamenti – omissivi o attivi – di illegalità e di violenze antidemocratiche. Sugli sviluppi istituzionali - opposti a quelli verificatisi alle europee - ci rimettiamo alla documentazione già resa nota  (quindi clandestina!) e alle sue più gravi, in pieno corso, accelerazioni di questi giorni.

**IN CORSO STRAGE DI LEGALITA’ SENZA PRECEDENTI**

Accelerazioni o radicali conversioni di fuoruscita da legittime funzioni come quella presa di posizione grottesca, scandalosa e desolante fornita dall’Agcom e quella della Commissione di Vigilanza in meno di 48 ore. L’Agcom, dopo un decennio di delibere critiche contro la RaiTv (circa 50), stravolge all’improvviso la sua giurisprudenza, mentre la Commissione di Vigilanza tenta di annullare quanto appena deliberato, cioè la proposta del Relatore Marco Beltrandi. In 48 ore registra il pentimento e il rovesciamento da quanto deliberato, della Destra berlusconiana, che torna precipitosamente all’ovile e alla greppia da sempre comune con i suoi “Komunisti”

**LE ELEZIONI CHE STANNO VIETANDO**

Quella delibera aveva e ha come principale obiettivo quello di garantire, almeno nelle ultime quattro settimane prima del voto, alla maggioranza degli elettori pubblici confronti diretti tra tutte le forze concorrenti, oltre alla presenza e partecipazione ufficiale dei leader delle forze concorrenti; come in tutti paesi effettivamente democratici.

Si tratta di resuscitare effettive tribune politiche (ed elettorali) formalmente abolite contro le tassative leggi e norme che le impongono; dopo averle già ridotte a trasmissioni semi-clandestine, surclassate da convocazioni a giochi per plebi da Colosseo, per centinaia di milioni di ascolti, in sette mesi di poco meno di mezzo miliardo, per due sole serie di essi.

**ALLE EUROPEE, DOPO LA MORAL SUASION DEL PRESIDENTE DELLA REPUBBLICA: IN ITALIA VOTA IL 65% DI ELETTORI, A FRONTE DI UNA MEDIA EUROPEA DEL 43%. IL CONTRIBUTO DELLA RESISTENZA RADICALE**

Quando, nel maggio scorso, il movimento Radicale e la sua Galassia documentarono che solo il 3% degli elettori erano informati su quali fossero le forze politiche istituzionali che vi partecipavano, subito intervenne un’alta opera istituzionale di “moral suasion” che concorse a sensibilmente correggere in positivo quantomeno il grado di mancato coinvolgimento civile e democratico – popolare, di violenza istituzionale contro la stessa esistenza di forze sgradite perché estranee al potere di Regime del monopartitismo partitocratico. Un risultato di tutto ciò si tradusse in modo clamoroso quanto oscurato, silenziato: proprio questa Italia risultò essere fra i primissimi dei 27 Stati dell’UE quanto a partecipazione elettorale (dopo Belgio, Olanda e Malta) l’Italia ha riscosso oltre il 65% di votanti contro una media europea del 43%.

La moral suasion del Presidente della Repubblica, provocata e consentitagli dalle denuncie e dagli interventi delle Liste Emma Bonino-Marco Pannella, con questo inaudito risultato hanno pienamente legittimato e validato la nostra decisione finale di partecipare a quelle “elezioni”.

**FINALMENTE ARRIVA BERSANI, MA TROPPO TARDI PER RIPARARE AI MISFATTI VELTRUSCONIANI**

Non venne invece mancato l’altro obiettivo del patto strategico tra Pd e Pdl, stipulato - e poi difeso - da una parte e dall’altra, da Berlusconi e da Veltroni-Franceschini: l’eliminazione di tutte le forze politiche estranee e non omologate a questo scellerato patto dalle elezioni nazionali del 2008, a quelle europee, e alle stesse regionali 2010. In particolare si è perseguita e ottenuta la dolorosissima eliminazione dal PE anche dei Radicali, dopo trent’anni di ininterrotta e spesso prestigiosa presenza europea e internazionale. Purtroppo Bersani è giunto, finalmente, troppo tardi per poter riparare il disastroso bilancio politico precedente.

**_Marco Pannella, Emma Bonino_**

**_I candidati Governatori della Lista Bonino-Pannella_**

_Maurizio Bolognetti,Silvio Viale, Werther Casali, Maria Antonietta Farina Coscioni, Alfonso De Virgiliis, Marco Cappato, Marco Perduca_

**_I responsabili dei soggetti politici della galassia radicale_**

*   _**Sergio Stanzani**, Presidente del Partito Radicale Nonviolento Transnazionale Transpartito_
*   _**Mario Staderini**, Segretario di Radicali Italiani_
*   _**Sergio D’Elia**, Segretario dell’Associazione Nessuno Tocchi Caino_
*   _**Rocco Berardo**, Tesoriere dell’Associazione Luca Coscioni_
*   _**Giorgio Pagano**, Segretario dell' Associazione radicale Esperanto_
*   _**Carlo Pontesilli**, Segretario di Anticlericale.net_
*   _**Niccolò Figà Talamanca**, Segretario di Non c’è Pace senza Giustizia_
*   _**Bruno Mellano**, Presidente di Radicali Italiani_
*   _**Michele De Lucia**, Tesoriere di Radicali Italiani_

**_I Parlamentari Radicali_**

_Donatella Poretti, Maurizio Turco, Marco Beltrandi, Matteo Mecacci, Rita Bernardini, Elisabetta Zamparutti_

E' possibile scaricare, sotto, il [documento completo con gli allegati](http://www.boninopannella.it/sites/default/files/tutto_il_comunicato_con_allegati.pdf) _**"DOCUMENTIAMO LA LIBERTA' SECONDO RAI-TV E I SUOI...VEDERE PER CREDERCI****"**_