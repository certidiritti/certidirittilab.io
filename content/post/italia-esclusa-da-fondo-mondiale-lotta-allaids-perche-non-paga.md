---
title: 'Italia esclusa da Fondo Mondiale lotta all''Aids perchè non paga'
date: Tue, 17 May 2011 09:20:53 +0000
draft: false
tags: [Salute sessuale]
---

**Il nostro è l’unico paese a non aver versato negli ultimi due anni la quota che si era impegnata a versare all'organizzazione che investe in progetti contro Hiv, malaria e tubercolosi. Ennesima brutta figura all'estero. Interrogazione dei Radicali.**

Sulla vicenda dell’esclusione dell’Italia dal Consiglio di Amministrazione del Fondo globale per la lotta contro l’Aids, malaria e tubercolosi, per non avere pagato per due anni gli importi su cui si era impegnata nel corso dei G8 di Genova e de L’Aquila, i deputati Radicali eletti nel Pd, prima firmataria Rita Bernardini, hanno depositato una interrogazione  per chiedere al Governo chiarimenti.  
  
  
Di seguito il testo integrale dell’interrogaizone a risposta scritta:  
Interrogazione urgente a risposta scritta  
Al Presidente del Consiglio dei Minsitri  
Al Ministro degli Esteri  
Al Minsitro della Sanità  
   
Da fonti di stampa e dall’Associazione nazionale Arcigay risulta che l’Italia è stata esclusa dal consiglio di amministrazione del Fondo globale per la lotta contro Aids, malaria e tubercolosi perché indietro con i pagamenti di ben due anni;  
L’Italia era stata la promotrice del Fondo durante i lavori del G8 di Genova; il Presidente del Consiglio Silvio Berlusconi nel 2009 a L’Aquila, in occasione del G8 dichiarò che: “entro il prossimo mese verseremo 130 milioni di dollari a cui ne aggiungeremo altri 30″;  
L’ Italia  ospiterà tra appena due mesi la IAS, uno dei più importanti eventi medico-scientifici dedicati alla lotta all'AIDS.  
Degli oltre 40 paesi donatori (a cui vanno aggiunte associazioni come quelle che fanno capo a Bill Gates e a Bono Vox) l’Italia è l’unico a non aver ancora versato la quota del 2009”;  
Per sapere:  
\- se tali notizie corrispondono al vero;  
\- quali notizie intende prendere il Governo per rispettare gli impegni presi;  
\- quale sarà l’atteggiamento del Governo italiano alla prossima riunione IAS;  
\- se non ritiene il Governo che tale comportamento metta in una cattiva luce il nostro paese sia rispetto agli impegni presi, sia rispetto alla necessità del nostro paese di contribuire in modo determinante alla lotta all’Aids in tutto il mondo;

**

   
Rita Bernardini  
Matteo Mecacci  
Marco Beltrandi  
Maria Antonietta Farina Coscioni  
Maurizio Turco  
Elisabetta Zamparutti

**