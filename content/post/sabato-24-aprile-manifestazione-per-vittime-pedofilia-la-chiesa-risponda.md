---
title: 'SABATO 24 APRILE MANIFESTAZIONE PER VITTIME PEDOFILIA: LA CHIESA RISPONDA!'
date: Tue, 20 Apr 2010 12:59:04 +0000
draft: false
tags: [Comunicati stampa]
---

**Pedofilia - Associazioni Lgbt(e) indicono sabato 24 aprile a Roma una**

**Manifestazione in sostegno delle vittime della pedofilia. La chiesa risponda davanti ai Tribunali!**

_Perseguire gli abusi senza reticenze,  
pieno sostegno alle vittime perché denuncino tutti casi_

In questi giorni in cui si sta tentando di nascondere la verità sugli abusi perpetrati ai danni di minori innocenti tirando in ballo assurdi parallelismi tra omosessualità e pedofilia, **Arcigay, ArciLesbica, AGEDO, Famiglie Arcobaleno, MIT, Certi Diritti, Dì Gay Project** lanciano un appello ai cittadini e alle associazioni per una **manifestazione contro la pedofilia e in sostegno delle vittime per sabato 24 aprile a Roma in Piazza SS Apostoli alle ore 16.30**.

Distorcere la questione della pedofilia, come ha fatto il **Segretario di Stato Vaticano Tarcisio Bertone**, porta a scaricare su altri innocenti le gravi accuse che da tutto il mondo piovono sulle gerarchie vaticane. La questione non è identificare o meno l’orientamento sessuale del pedofilo, ma **perseguire con fermezza chiunque si macchi di tali abusi, soprattutto se si hanno responsabilità educative o spirituali**.

La Chiesa risponda davanti ai tribunali e all’opinione pubblica mondiale dei gravi insabbiamenti avvenuti in tutto il mondo. Per questo il nostro appello va a tutte le donne e gli uomini di qualsiasi fede religiosa o non religiosi, che **non possono tacere di fronte alla violazione dell’infanzia**: che tutti i casi vengano portati allo scoperto.

**_Per adesioni e contatti: [stop.vaticanabuse@gmail.com](mailto:stop.vaticanabuse@gmail.com)_**

**_Paolo Patanè – Arcigay_**

**_Francesca Polo – ArciLesbica_**

**_Rita De Santis – AGEDO_**

**_Giuseppina La Delfa – Famiglie Arcobaleno_**

**_Porpora Marcasciano – MIT_**

**_Sergio Rovasio – Associazione radicale Certi Diritti_**

**_Imma Battaglia – Dì Gay Project_**

_**Rossana Praitano -** **Circolo di cultura omosessuale Mario Mieli**_

--