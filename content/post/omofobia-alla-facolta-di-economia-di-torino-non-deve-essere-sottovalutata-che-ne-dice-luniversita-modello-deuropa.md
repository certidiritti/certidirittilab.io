---
title: 'OMOFOBIA ALLA FACOLTA DI ECONOMIA DI TORINO: NON DEVE ESSERE SOTTOVALUTATA.  CHE NE DICE L’UNIVERSITA’ “MODELLO D’EUROPA”?'
date: Sat, 01 May 2010 07:02:09 +0000
draft: false
tags: [Comunicati stampa, economia, enzo cucco, europa, OMOFOBIA, torino]
---

Dichiarazione di Enzo Cucco, Associazione radicale Certi Diritti

I piccoli fatti di omofobia accaduti presso la Facoltà di Economia di Torino intorno ad una mostra proprio contro l’omofobia organizzata dall’associazione La Jungla, che aderisce al Coordinamento Torino Pride, non devono essere lasciati passare sotto silenzio.

Non si tratta solo di garantire piena agibilità degli spazi universitari ad ogni voce, e non si tratta nemmeno di un gesto di qualche bullo qualsiasi. E’ un segnale molto preciso di intolleranza che bisogna cogliere e contro il quale bisogna reagire.

La Città di Torino, che molto ha fatto sul tema della prevenzione dell’omofobia, deve farsi parte attiva nelle iniziative rivolte agli studenti universitari, del l’Università di Torino come del resto degli Atenei piemontesi, perché anche solo piccoli episodi come questo non passino.

Ma devono intervenire anche Regione e Provincia, ciascuno per le proprie competenze. Così come deve esserci una reazione decisa della stessa facoltà e del rettorato che giustamente si paragona ai migliori Istituti universitari del mondo dove, però (almeno in quelli dei paesi occidentali) esistono gruppi lgbt e molte attività in tal senso.

Il sociologo Florida ha definito la teoria delle tre T per descrivere le condizioni che consentono a un territorio di svilupparsi Talenti, Tecnologia e Tolleranza. Delle prime due T la facoltà di Economia di Torino è polo di eccellenza in Europa. E della terza?