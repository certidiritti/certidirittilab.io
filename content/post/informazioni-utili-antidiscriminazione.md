---
title: 'INFORMAZIONI UTILI ANTIDISCRIMINAZIONE'
date: Sun, 13 Jul 2008 11:53:08 +0000
draft: false
tags: [Comunicati stampa, Info Antidiscriminazione, Matteo Bonini Baraldi]
---

[•    Relazione della Commissione sulla situazione sociale in Europa – Report 2007 “Coesione sociale attraverso le pari opportunità”](http://ec.europa.eu/employment_social/spsi/reports_social_situation_en.htm )  
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-  
[•    Nuovo Corso di Laurea magistrale in Scienze del Lavoro, curriculum Pari Opportunità, Diritti e Politiche Sociali](http://www.scienzepolitiche.unimi.it/dev/null/3085_ITA_HTML.html )  
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-  
•    Online [registrazioni video](http://multimedia.giuri.unibo.it/Archivio/2008/discriminazione.html) e [registrazione audio](http://www.radioradicale.it/scheda/256011/tutela-della-persona-e-orientamento-sessuale-strumenti-tecniche-e-strategie-contro-la-discriminazione ) del convegno “Tutela della persona e orientamento sessuale.  
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-  
•    [Osservazioni dell’ASGI sulle norme in materia di stranieri](http://www.asgi.it/content/documents/dl08061200.asgi.pacchetto.sicurezza.2008.pdf ) contenute nei provvedimenti del “pacchetto sicurezza”  
  
[•    Relazione della Commissione sulla situazione sociale in Europa – Report 2007 “Coesione sociale attraverso le pari opportunità”](http://ec.europa.eu/employment_social/spsi/reports_social_situation_en.htm )  
  
La relazione della Commissione sulla situazione sociale – pubblicata dal 2000 -  fornisce una panoramica della situazione sociale nell’Ue e una descrizione degli sviluppi in settori specifici, cui si affianca un’ampia collezione di ritratti statistici a cura di Eurostat. I ritratti statistici forniscono uno sguardo ricorrente su 20 maggiori indicatori di rilevanza per la situazione sociale, che spaziano da indicatori economici a indicatori di benessere quali tasso di incidenti e dati sull’eguaglianza di genere. Quale contenitore di un ampio spettro di indicatori sociali, informazioni quantitative armonizzate e sondaggi sull’opinione pubblica, la relazione funge da documento di riferimento e da strumento per il monitoraggio degli sviluppi sociali nel tempo.  
La relazione del 2007 ha ad oggetto “La coesione sociale attraverso le pari opportunità”.  
  
[Per maggiori informazioni e per il testo dei documenti](http://ec.europa.eu/employment_social/spsi/reports_social_situation_en.htm ) :  
  
[•    Nuovo Corso di Laurea magistrale in Scienze del Lavoro, curriculum Pari Opportunità, Diritti e Politiche Sociali](http://www.scienzepolitiche.unimi.it/dev/null/3085_ITA_HTML.html )  
  
Il corso sarà attivo dal prossimo anno accademico presso la Facoltà di Scienze Politiche dell'Università degli Studi di Milano. All’interno del corso di laurea magistrale in Scienze del Lavoro (LAV), il curriculum in “Pari opportunità, Diritti e Politiche Sociali” è caratterizzato da un percorso interdisciplinare, possibile grazie alle diverse competenze presenti nella Facoltà di Scienze Politiche. La didattica di questo curriculum si avvale inoltre dell’esperienza acquisita dai docenti che hanno partecipato al Master in Pari opportunità (4 edizioni) e ai corsi sul tema “Donne, politica e istituzioni”.  
  
[Per maggiori informazioni](http://www.scienzepolitiche.unimi.it/dev/null/3085_ITA_HTML.html ) :  
  
  
•    Online [registrazioni video](http://multimedia.giuri.unibo.it/Archivio/2008/discriminazione.html) e [registrazione audio](http://www.radioradicale.it/scheda/256011/tutela-della-persona-e-orientamento-sessuale-strumenti-tecniche-e-strategie-contro-la-discriminazione ) del convegno “Tutela della persona e orientamento sessuale. Strumenti, tecniche e strategie contro la discriminazione” – Bologna, 13 giugno 2008  
  
[Video del convegno](http://multimedia.giuri.unibo.it/Archivio/2008/discriminazione.html)  
Il link [all’audio di radio radicale](http://www.radioradicale.it/scheda/256011/tutela-della-persona-e-orientamento-sessuale-strumenti-tecniche-e-strategie-contro-la-discriminazione )  
  
  
•    [Osservazioni dell’ASGI sulle norme in materia di stranieri](http://www.asgi.it/content/documents/dl08061200.asgi.pacchetto.sicurezza.2008.pdf ) contenute nei provvedimenti del “pacchetto sicurezza”  
[Clicca qui per il documento. ](http://www.asgi.it/content/documents/dl08061200.asgi.pacchetto.sicurezza.2008.pdf )  
_**  
Ringraziamo Matteo Bonini Baraldi per le informazioni ricevute.**_  
  
        <!\-\- /\* Font Definitions */ @font-face {font-family:"Cambria Math"; panose-1:2 4 5 3 5 4 6 3 2 4; mso-font-charset:0; mso-generic-font-family:roman; mso-font-pitch:variable; mso-font-signature:-1610611985 1107304683 0 0 159 0;} @font-face {font-family:Calibri; panose-1:2 15 5 2 2 2 4 3 2 4; mso-font-charset:0; mso-generic-font-family:swiss; mso-font-pitch:variable; mso-font-signature:-1610611985 1073750139 0 0 159 0;} /* Style Definitions */ p.MsoNormal, li.MsoNormal, div.MsoNormal {mso-style-unhide:no; mso-style-qformat:yes; mso-style-parent:""; margin:0cm; margin-bottom:.0001pt; mso-pagination:widow-orphan; font-size:12.0pt; font-family:"Times New Roman","serif"; mso-fareast-font-family:Calibri; mso-fareast-theme-font:minor-latin;} .MsoChpDefault {mso-style-type:export-only; mso-default-props:yes; font-size:10.0pt; mso-ansi-font-size:10.0pt; mso-bidi-font-size:10.0pt;} @page Section1 {size:612.0pt 792.0pt; margin:70.85pt 2.0cm 2.0cm 2.0cm; mso-header-margin:36.0pt; mso-footer-margin:36.0pt; mso-paper-source:0;} div.Section1 {page:Section1;} --> 

**Matteo Bonini Baraldi****, PhD**

**EU Research Adviser - Legal & Political Sciences**

**Alma Mater Studiorum Università di Bologna  
**

**Settore Ricerca Europea  
**