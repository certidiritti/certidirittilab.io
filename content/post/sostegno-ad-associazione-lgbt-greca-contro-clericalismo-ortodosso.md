---
title: 'SOSTEGNO AD ASSOCIAZIONE LGBT GRECA CONTRO CLERICALISMO ORTODOSSO'
date: Thu, 08 Jan 2009 10:25:42 +0000
draft: false
tags: [aids, clericalismo, Comunicati stampa, grecia, lgbt, ortodossi]
---

**News da Queer Blog**

**Un milione di euro** è la cifra che in Grecia un vescovo ortodosso ha chiesto all’attivista **Leo Kalovyrnas** e all’associazione _[Synthesis HIV/AIDS Awareness](http://www.hiv.gr/)_ una delle principali organizzazioni non governative greche che si batte contro la diffusione del virus dell’Hiv nella popolazione LGBT e della salute delle persone sieropositive.

Il vescovo era stato pubblicamente accusato da Kalovyrnas e dalla _Synthesis_ dopo alcune sue affermazioni omofobe attraverso le quali aveva affermato che le persone omosessuali sarebbero “moralmente corrotte” e “ossessionate dalla soddisfazione della loro deviazione psicopatologica” ma l’uomo non ha gradito l’intervento degli attivisti che, denunciati, dovranno apparire in tribunale il prossimo 3 Febbraio accusati di diffamazione. L’associazione ha affermato:

> “Discorsi d’odio contro gay, lesbiche e transessuali vengono impunemente pronunciati in Grecia. Diversi politici, ministri, capi religiosi così come molti giornalisti e altri personaggi pubblici insultano ripetutamente gay e lesbiche senza subire alcuna conseguenza legale.”

La _Synthesis_ ha chiesto di firmare una [petizione](http://www.10percent.gr/component/chronocontact/?chronoformname=lawsuit_seraphim_en) rivolta al governo greco per chiedere l’introduzione di leggi anti discriminazione basate sull’orientamento sessuale e sull’identità di genere.