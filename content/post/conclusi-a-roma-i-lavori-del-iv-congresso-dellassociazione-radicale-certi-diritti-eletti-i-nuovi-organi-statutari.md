---
title: 'CONCLUSI A ROMA I LAVORI DEL IV CONGRESSO DELL’ASSOCIAZIONE RADICALE CERTI DIRITTI. ELETTI I NUOVI ORGANI STATUTARI'
date: Sun, 28 Nov 2010 15:44:45 +0000
draft: false
tags: [Comunicati stampa, CONGRESSO, Organi statutari, roma]
---

Si sono conclusi oggi a Roma i lavori del IV Congresso dell’Associazione Radicale Certi Diritti. Sono stati eletti: Rita Bernardini, Presidente; Sergio Rovasio, Segretario; Giacomo Cellottini, Tesoriere e Maria Gigliola Toniollo, rappresentante dell'Associazione nel Comitato di Radicali italiani

Roma, 28 novembre 2010

Si sono conclusi oggi i lavori del [**IV Congresso dell’Associazione Radicale Certi Diritti**](tutte-le-notizie/850-iv-congresso-associazione-radicale-certi-diritti.html) al quale hanno partecipato parlamentari, personalità della società civile, rappresentanti delle più importanti Associazioni che si battono in Italia per la promozione e la difesa dei diritti civili e umani delle persone lesbiche, gay, bisessuali, trans gender ed anche eterosessuali.

Tra gli altri, hanno partecipato Nikolay Alexeiev, organizzatore del Gay Pride di Mosca; Kato David Kisule, esponente della comuntià gay dell’Uganda; Santos Felix, presidente Casal Lambda di Barcellona; gnese Canevari, rappresentante dell’UNAR - Ufficio Nazionale Antidiscriminazione razziale; Paolo Patanè, Presidente di Arcigay; Anna Paola Concia, deputata del Pd; Mina Welby; Angelo Pezzana, fondatore del Fuori; Marco Cappato, Segretario dell’Associazione Luca Coscioni; Ivan Scalfarotto, Vice Presidente dell’Assemblea nazionale del Pd; Pia Covre, Presidente del Comitato per i Diritti Civili delle Prostitute; Marco Beltrandi e Marco Perduca, Parlamentari radicali.

Il IV Congresso dell'Associazione Radicale Certi Diritti ha eletto :

**Presidente** Rita Bernardini, deputata radicale eletta nel Pd

**Segretario** Sergio Rovasio

**Tesoriere** Giacomo Cellottini

**Rappresentante di Certi Diritti presso il Comitato di Radicali Italiani** Maria Gigliola Toniollo, Cgil Nuovi Diritti.

Il Congresso ha approvato all’unanimità una mozione che impegnerà l’Associazione su tre principali temi politici:

\- Rilancio della campagna di Affermazione Civile, grazie alla quale la Corte Costituzionale si è espressa lo scorso aprile riconoscendo la rilevanza costituzionale delle coppie gay e sollecitando la classe politica a legiferare in materia di riconoscimento dei diritti. La campagna di Affermazione Civile promuove iniziative che prevedono il ricorso alle vie legali quando viene negato dalle autorità il riconoscimento del diritto al matrimonio.

\- Nell’ambito delle attività da promuovere anche in ambito transnazionale, l’Associazione Radicale Certi Diritti ha riformato il suo Statuto, in vista della possibilità di divenire una delle Associazioni costituenti il Partito Radicale Nonviolento Transnazionale e Transpartito.

\- Rilancio della Conferenza per la Riforma del Diritto di Famiglia, per promuovere un allargamento del riconoscimento dei dirittii, contro una concezione clericale e a senso unico della famiglia, che in una società moderna e democratica non può essere l’unico modello considerato legittimo.