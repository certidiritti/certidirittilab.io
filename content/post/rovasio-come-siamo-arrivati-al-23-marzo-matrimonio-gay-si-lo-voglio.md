---
title: 'ROVASIO: "COME SIAMO ARRIVATI AL 23 MARZO" - MATRIMONIO GAY? SI, LO VOGLIO'
date: Thu, 11 Mar 2010 08:39:34 +0000
draft: false
tags: [Comunicati stampa]
---

**L'introduzione all'inchiesta:**  
[In nome dell'uomo, questo matrimonio s'ha da fare!](http://noirpink.blogspot.com/2010/03/in-nome-delluomo-questo-matrimonio-sha.html)  
  
La Corte Costituzionale il 23 marzo si pronuncerà sulla costituzionalità del divieto a sposarsi opposto a coppie dello stesso sesso. Una sentenza storica, qualunque sarà il suo esito, frutto della determinazione e del coraggio di alcune coppie omosessuali, di [Rete Lenford](http://www.retelenford.it/), che riunisce alcuni avvocati per i diritti lgbt, e dell'[Associazione Radicale Certi Diritti](http://www.certidiritti.it/). E proprio con **il segretario di Certi Diritti, Sergio Rovasio**, che [avevamo già sentito](http://noirpink.blogspot.com/2009/10/dalla-fantascienza-alla-realta-la-lotta.html) in ottobre, abbiamo ricostruito come si è arrivati a questo risultato straordinario.

  
**_Vi aspettavate di raggiungere un risultato così importante in così poco tempo?_**  
  
Quando abbiamo avuto l'intuizione di promuovere **questa campagna di Affermazione Civile** nella primavera del 2008, credevamo effettivamente che i tempi per arrivare alla Corte Costituzionale fossero molto più lunghi e ritenevamo che avremmo trovato la disponibilità di molte più coppie dello stesso sesso disposte a mettere in discussione lo status quo riguardo i loro diritti.  
  
[![](http://2.bp.blogspot.com/_gD_4K8PsIQk/S4wRUo-ZfhI/AAAAAAAACPc/i4GMnA_R3ck/s400/phpE8XBv3PM.jpg)](http://2.bp.blogspot.com/_gD_4K8PsIQk/S4wRUo-ZfhI/AAAAAAAACPc/i4GMnA_R3ck/s1600-h/phpE8XBv3PM.jpg)Al contrario, i tempi per arrivare alla Corte Costituzionale sono stati velocissimi, e ciò che è ancora più sorprendente è il fatto che la Corte è stata interpellata da **ben due Tribunali e due Corti d'Appello**. Questo si che ci ha sorpresi, i giudici ritengono praticamente fondati i ricorsi e per questo si rimettono al parere della Corte!  
  
  
**_Quali sono stati gli ostacoli maggiori per ottenere questo risultato?_**  
  
Gli ostacoli maggiori sono state **le diffidenze di molte delle realtà associative lgbt** e il fatto che ancora oggi i media italiani non riescano a cogliere l'importanza di questa azione che è del tutto alternativa a quella politica. In questi due anni abbiamo chiesto la collaborazione a tutte le più importanti realtà lgbt locali e nazionali e le risposte positive sono state pochissime. In queste settimane abbiamo lanciato il Comitato "Sì lo voglio" che ha proprio lo scopo di far aderire le associazioni a questo progetto per farlo camminare da solo. Finalmente qualcosa si sta muovendo.  
  
  
_**Il 20 marzo Certi Diritti organizza una manifestazione a Roma: di cosa si tratta?**_  
  
[![](http://3.bp.blogspot.com/_gD_4K8PsIQk/S4wRMzqduII/AAAAAAAACPU/SOYGh8rVrGE/s400/php8atE5APM.jpg)](http://3.bp.blogspot.com/_gD_4K8PsIQk/S4wRMzqduII/AAAAAAAACPU/SOYGh8rVrGE/s1600-h/php8atE5APM.jpg)E' **una manifestazione di dialogo e di sensibilizzazione** nei confronti della Corte Costituzionale. L'iniziativa la sta organizzando il nascituro Comitato "Sì lo voglio" con le varie associazioni che hanno aderito a questa inizativa. Riponiamo molta fiducia nella Corte e siamo certi che non farà finta di nulla di fronte alle richieste che con i ricorsi delle coppie parte della società civile chiede.  
  
In molti stati democratici **le massime Corti hanno innovato in positivo** leggi di tutela e di garanzia sul piano dei diritti civili. Hanno fatto quello che le classi politiche più conservatrici e reazionarie hanno per lungo tempo impedito. Basti pensare a cosa accadde nel 1956 quando la Corte suprema degli Stati Uniti abrogò le leggi segregazioniste: tutto avvenne grazie all'iniziativa di Affermazione Civile di Rosa Parks che si sedette su un autobus in un posto riservato ai bianchi e fu per questo arrestata.  
  
  
**_Proviamo a immaginare ora il post-sentenza... Partiamo, facendo le corna, dalla prospettiva più negativa: la Corte si pronuncia a sfavore dei matrimoni tra persone dello stesso sesso. A quel punto cosa si potrebbe fare? Il movimento per il cosiddetto "matrimonio gay" si arrenderà?_**  
  
[![](http://3.bp.blogspot.com/_gD_4K8PsIQk/S4wREgg2z2I/AAAAAAAACPM/Lf88cU4CQ4Y/s400/phppbeOk5PM.jpg)](http://3.bp.blogspot.com/_gD_4K8PsIQk/S4wREgg2z2I/AAAAAAAACPM/Lf88cU4CQ4Y/s1600-h/phppbeOk5PM.jpg)No! Non ci arrenderemo! Sappiamo che il nostro impegno è finalizzato a raggiungere un traguardo di uguaglianza e di civiltà e prima o poi si arriverà all'obiettivo, ne siamo certi! Se ci arrendiamo la diamo vinta a coloro che vogliono lo status quo. **Sono fiducioso nella Corte** e credo che anche se non si giungerà al matrimonio gay si riuscirà per lo meno a scrollare questa classe politica, quasi tutta bloccata e contorta sui propri interessi. E' ora di riformare le leggi e finalmente promuovere maggiori tutele per le persone lgbt(e): di questo la Corte Costituzionale terrà certamente conto.  
  
  
**_E se invece si realizzasse lo scenario migliore, con una presa di posizione della Corte a favore del matrimonio tra persone dello stesso sesso?_**  
  
Sarebbe una rivoluzione, ciò vorrebbe semplicemente dire che l'Italia si adegua a quello che l'Europa gli chiede con i Trattati di Nizza e di Lisbona. Gli italiani, pur non sentendo mai parlare di matrimonio gay, secondo i pochi sondaggi fatti finora sono d'accordo al 50% e questo è un risultato straordinario se pensate che nessuna tv o altro media parla mai di questo tema. Questo dimostra che **la società è più avanzata della classe politica**!  
  
[![](http://1.bp.blogspot.com/_gD_4K8PsIQk/S4wQ8p68XTI/AAAAAAAACPE/qOg3Do_jpYc/s400/phpLyIQCePM.jpg)](http://1.bp.blogspot.com/_gD_4K8PsIQk/S4wQ8p68XTI/AAAAAAAACPE/qOg3Do_jpYc/s1600-h/phpLyIQCePM.jpg)Se si realizzasse lo scenario migliore la classe politica dovrà smettere di comportarsi come ha fatto finora voltandosi dall'altra parte: dovrà cercare di **"governare" i fenomeni sociali anzichè far finta che non esistano** con pseudo-ragioni religiose che servono solo per ingraziarsi il vescovo o il monsignore di turno.  
  
  
_**Comunque vada, arrivare a questa sentenza è un fatto di enorme importanza. Eppure i mass media fanno finta di nulla, tacciono, al massimo bisbigliano qualcosa in qualche trafiletto... Lei, da radicale, sarà abituato al mura di gomma opposto alle vostre iniziative, ma come si spiega questo silenzio? E come può ciascuno di noi contribuire a dissolverlo?**_  
  
Quanto dice è verissimo. Già questa inchiesta è un grande aiuto e di questo vi ringraziamo. Abbiamo centinaia di persone che per migliorare questo mondo, queste leggi ingiuste, lavorano volontariamente (**nella nostra associazione sono tutti volontari!**) spendendo molto del loro tempo, ognuno nel suo campo professionale. Sulla campagna di Affermazione Civile basti pensare all'avvocato Francesco Bilotta![](chrome://skype_ff_toolbar_win/content/icons/icon_on.png), co-fondatore di Rete Lenford, brillante giurista, che sta facendo gratuitamente assistenza a decine delle coppie che in tutta Italia partecipano a questa iniziativa.  
  
Questi sono eroi dei nostri tempi che nessuno valorizza e che lavorano con umiltà e in silenzio. **Questo è oggi il nostro paese.** Speriamo in un domani migliore.  

![](http://signatures.mylivesignature.com/54487/101/5327F7BF73EAA3E664FCBFD8A29F22F2.png)