---
title: 'Nuova attivazione per Jean-Claude Roger Mbede - Cameroon'
date: Mon, 09 Apr 2012 18:13:08 +0000
draft: false
tags: [Africa]
---

Lo scorso dicembre, insieme ad Amnesty International, l'Associazione Radicale Certi Diritti si è mobilitata per Jean-Claude Roger Mbede, uno studente camerunense che sta scontando una condanna a tre anni di carcere esclusivamente a causa del suo orientamento sessuale, reale o presunto.

Per ringraziare tutte le persone che si sono attivate per lui, Jean-Claude ha scritto questo messaggio: "Siete stati accanto a noi con la tristezza, è vero, ma soprattutto con tanta dolcezza, tanto amore e tanta determinazione per tutto l'anno. Vi chiediamo di non cedere mai allo scoraggiamento e non abbassare mai la guardia e di continuare a portare avanti la vostra mobilitazione per noi e per la causa delle persone lesbiche, gay, bisessuali e transgender. Il vostro sostegno rappresenta la speranza per tutti, qui in Camerun, particolarmente per noi che siamo in carcere: la speranza che un giorno lasceremo questa prigione, la speranza che un giorno le persone lesbiche, gay, bisessuali e transgender potranno camminare liberamente e a testa alta in Camerun, senza essere umiliate in alcun modo."

La mobilitazione di Amnesty International e dell'Associazione Radicale Certi Diritti continua perché Jean-Claude si trova ancora nel carcere centrale di Kondengui dove le condizioni di detenzione sono difficili a causa di un grave sovraffollamento, scarsi livelli di igiene e carenza di cibo. Jean-Claude è inoltre a rischio di attacchi omofobi e di maltrattamenti.

Dal 10 al 16 aprile, giorno dell'udienza d’appello, si terrà una twitter action. Se hai un profilo twitter, puoi scrivere anche tu al Presidente del Cameroon Paul Biya per chiedere l’immediato rilascio di Jean-Claude!

Puoi usare uno dei messaggi che seguono oppure crearne uno tuo, non dimenticando di includere l’hashtag #FreeMbede:

Presidente @PaulBiya Jean-Claude Roger Mbede deve essere immediatamente rilasciato!

#FreeMbede #LGBT Please RT

Calling on Cameroonian President @PaulBiya to release Jean-Claude Roger Mbede unconditionally now! #FreeMbede #LGBT Please RT

Se non sei su twitter, puoi firmare e diffondere l’appello on-line: [http://www.amnesty.](http://www.amnesty.)it/camerun\_mbede\_diritti_lgbt