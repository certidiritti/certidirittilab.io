---
title: 'La breve vita dell’ebrea Felice Schragenheim'
date: Fri, 23 Apr 2010 19:09:50 +0000
draft: false
tags: [Senza categoria]
---

[![](http://www.beitcasaeditrice.it/images/copertine%20x%20recnsioni/Cover-Felice-cons.jpg)](http://www.beitcasaeditrice.it/images/CoverFelice.jpg)

**La breve vita dell’ebrea Felice Schragenheim**  
_Erica Fischer_  
208 pp. 225 x 275 mm. Numerose illustrazioni nel testo, euro 32,00.  
Traduzione di Daniela Zuffellato.  
ISBN: 978-88-95324-08-1

Felice Schragenheim (Berlino 1922 – Bergen-Belsen 1945), giovane scrittrice e giornalista ebrea, affronta la vita e l’amore con un coraggio e con una lievità che ancora oggi ci sorprendono: si sente protagonista della commedia dell’esistenza e non si lascia intimorire né dalla dittatura nazista né dal pregiudizio benpensante. Sarà proprio questa sua sincerità che la porterà ad affrontare in prima persona, senza risparmiarsi, la tragedia del suo tempo. Questo volume raccoglie, nelle immagini e nelle poesie di Felice e del suo mondo, una testimonianza indimenticabile sulla sua vita e sulla sua vicenda, che ha ispirato il romanzo di Erica Fischer _Aimée & Jaguar_.