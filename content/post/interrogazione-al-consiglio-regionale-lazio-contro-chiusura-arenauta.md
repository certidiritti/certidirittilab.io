---
title: 'INTERROGAZIONE AL CONSIGLIO REGIONALE LAZIO CONTRO CHIUSURA ARENAUTA'
date: Sun, 01 Aug 2010 09:28:56 +0000
draft: false
tags: [arenauta, Comunicati stampa, gaeta, interrogazione, lazio, naturismo, spiaggia]
---

  
SPIAGGIA DELL’ARENAUTA DI GAETA: COSA SUCCEDE NELLA SPIAGGIA CHE STA PER ESSERE INGABBIATA? OCCORRE CHIAREZZA RISPETTO AD UN’AREA DI PARTICOLARE INTERESSE AMBIENTALE. VIGILEREMO PER EVITARE OGNI FORMA DI SPECULAZIONE.

Sulla vicenda dodici consiglieri di otto diversi gruppi politici, primi firmatari i consiglieri radicali Giuseppe Rossodivita e Rocco Berardo, hanno oggi depositato una interrogazione urgente a risposta scritta.

Da ieri, in un ampio tratta della spiaggia dell’Arenauta (Gaeta) sono iniziative lavori di ‘ingabbiamento’ che precluderebbe il passaggio a migliaia di bagnanti e turisti. A nessuno è dato sapere quali sono i motivi di questa improvvisa ‘messa in sicurezza’ di quest’area.

Negli ultimi decenni non risultano esserci state frane o crolli alle persone che frequentano quest’area. Ci viene un dubbio che speriamo sia chiarito quanto prima: il tratto di spiaggia è l’unico che non è stato oggetto di speculazioni e cementificazioni.

Ci auguriamo che qualsiasi sia il tipo di intervento non sai causato da iniziative estemporanee che già in passato si sono verificate in quegli stessi punti della spiaggia che hanno poi fatto intervenire le autorità del Governo e della Prefettura.

Dodici Consigliglieri regionali di otto diversi gruppi politici hanno oggi depositato una interrogazione urgente a risposta scritta rivolta alla Presidente della Regione Lazio Renata Polverini e agli Assessori regionali interessati.

**Qui di seguito il testo integrale:**

**PREMESSO CHE:**

\- giovedì 29 luglio, in un tratto della spiaggia dell’Arenauta di Gaeta (Latina), situata in un’area di particolare interesse ambientale, dove non vi sono state fino ad oggi speculazioni edilizie di nessun tipo e considerata ‘libera’ perché non data in concessione, sono iniziati dei lavori di ‘emergenza’ consistenti in una recinzione di tutta l’area con pali installati sulla battigia della spiaggia;

\- le decine di strutture di sostegno della recinzione sono state installate in punti della spiaggia in violazione delle più elementari regole ambientali;

\- I lavori sono iniziati con uno spiegamento di forze dell’ordine, polizia, carabinieri, guardia costiera ma stranamente senza la presenza di Vigili del fuoco o agenti della Forestale;

\- negli ultimi anni l’area interessata dai lavori è stata spesso oggetto di strane iniziative da parte di alcuni amministratori locali con l’obiettivo di spaventare e allontanare le persone che frequentano la spiaggia libera;

**PER SAPERE:**

\- quali sono le ragioni di questi interventi straordinari urgenti in un tratto di spiaggia dove non sono mai avvenuti, negli ultimi 30 anni, crolli, frane, cedimenti strutturali;

\- se non ritengano gli interroganti che tali interventi siano finalizzati a obiettivi diversi da quelli della ‘messa in sicurezza’ dell’area senza che vi siano mai stati pericoli per i frequentatori della spiaggia;

\- per quale motivo viene ‘ingabbiata’ con una recinzione un’area così estesa di spiaggia che corrisponde esattamente all’unico tratto della spiaggia dell’Arenauta, finora rimasta esente da forme di speculazione edilizia e ancora non data in concessione ad alcun privato;

\- se non ritenga che le strutture della recinzione siano state installate in modo del tutto irregolare rispetto a quanto previsto dalle norme di tutela ambientale che prevedono una distanza di almeno cinque metri rispetto alla battigia;

\- se non ritenga che questi interventi mirino a colpire le persone che frequentano quel tratto di spiaggia;

**I consiglieri regionali**  
Giuseppe Rossodivita, Lista Bonino Pannella, Federalisti Europei;  
Rocco Berardo, Lista Bonino Pannella, Federalisti Europei;  
Ivano Peduzzi, Federazione della Sinistra;  
Angelo Bonelli, Verdi;  
Luciano Romanzi, Partito Socialista Italiano;  
Nobile, Federazione della Sinistra;  
Anna Maria Tedeschi, Italia dei Valori;  
Luigi Nieri, Sinistra e Libertà;  
Giulia Rodano, Italia dei Valori;  
Maruccio, Italia dei Valori;  
Enzo Foschi, Partito Democratico;  
Celli, Lista Civica per Bonino;