---
title: 'MATRIMONI GAY IN ITALIA? MARTEDI'' 23-3 DATA STORICA'
date: Sat, 20 Mar 2010 09:08:40 +0000
draft: false
tags: [Comunicati stampa]
---

**MATRIMONIO GAY IN ITALIA? MARTEDI’ 23 MARZO  LA CORTE COSTITUZIONALE  E’ CHIAMATA A DECIDERE SUI RICORSI DI COPPIE OMOSESSUALI PER IL MATRIMONIO.**

**CERTI DIRITTI TRA PARTECIPA E SOSTIENE LA MANIFESTAZIONE A ROMA DI DOMENICA 21-3.**

**Martedì 23 marzo  la Corte  Costituzionale  è chiamata a decidere sui ricorsi di alcune coppie gay che avevano chiesto al loro Comune di potersi sposare ed alle quali era stato opposto il diniego. Le coppie avevano partecipato alla campagna di ‘Affermazione Civile’ promossa due anni fa dall’Associazione Radicale Certi Diritti (www.certidiritti.it ) con il supporto di Avvocatura lgbt – Rete Lenford.**

**

La campagna di Affermazione Civile prevede che le coppie lesbiche e gay che vogliono sposarsi si rechino nel loro Comune, presentino le richieste per le pubblicazioni matrimoniali e quando il Comune oppone loro il diniego viene incardinata una iniziativa legale.

Alla campagna hanno aderito quasi una trentina di coppie omosessuali, centinaia di altre coppie sono in attesa della decisione della Corte.

L’Associazione Radicale Certi Diritti attende con speranza e fiducia le decisioni della Corte Costituzionale . Qualunque sarà la decisione, l’Associazione Certi Diritti continuerà con forza e determinazione la sua lotta per il superamento delle disuguaglianze e contro le discriminazioni, così come avvenuto in molti paesi europei e che gran parte della classe politica italiana, con rare eccezioni, continua ad ignorare.

In vista della decisione della Corte Costituzionale, Domenica 21 marzo, l'Associazione Radicale Certi Diritti parteciperà alla manifestazione promossa dal 'Comitato Si lo voglio', che si svolgerà a Roma dalle ore 17 in Piazza SS Apostoli. Al Comitato Si lo voglio aderiscono le più importanti associazioni lgbt(e) italiane.

Sergio Rovasio, Segretario Associazione Radicale Certi Diritti

Ufficio Stampa: Giacomo Cellottini Tel. 06-68979250 Cell.333-4801319

**