---
title: '2008-0140-Discrimination-ST06015.EN10'
date: Wed, 03 Feb 2010 15:36:39 +0000
draft: false
tags: [Senza categoria]
---

  

COUNCIL OF

THE EUROPEAN UNION

Brussels, 3 February 2010

Interinstitutional File:

2008/0140 (CNS)

6015/10

LIMITE

SOC 62

JAI 103

MI 33

  

  

  

  

  

NOTE

from :

The General Secretariat

to :

The Working Party on Social Questions

No. prev. doc. :

16063/09 SOC 706 JAI 836 MI 434

No. Cion prop. :

11531/08 SOC 411 JAI 368 MI 246

Subject :

Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation

Delegations will find attached a note from the Finnish delegation with a view to the meeting of the Working Party on Social Questions on 18 February 2010.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

  

**Note from Finland regarding the Proposal for a Council Directive on implementing the principle of equal treatment between persons irrespective of religion or belief, disability, age or sexual orientation – proposal concerning Recitals 12, 12c, 15 and 17b and Articles 2(3)a, 2(6)a, 2(8), 3(1)d and 3(2)a**

Finland supports the proposed Directive which aims to guarantee to all persons consistent minimum protection against discrimination irrespective of the discrimination ground and the area in which the discrimination is considered to have occurred.

The following comments include the comments presented at the meeting of the Social Questions Working Party of 22 January 2010. It also includes slightly amended versions of the earlier Finnish proposals concerning Recitals 15 and 17b as included in Note 13496/09 of 21 September 2009.

These comments are without prejudice to any further comments or proposals to be presented in the course of future negotiations.

**Recital 12c (new)**

Finland acknowledges that there may be a need to reconcile protection from harassment with the need to protect freedom of expression and freedom of religion. This is, however, already taken into account, as the proposed Directive expressly provides in Recitals 3 and 17 that its provisions respect other fundamental rights, including freedom of expression and freedom of religion. By virtue of Article 6 of the Treaty on European Union, which provides that the freedoms and rights set out in the Charter of Fundamental rights shall have the same legal value as the Treaties, Article 2(3) of the draft Directive cannot be interpreted in a manner that is inconsistent with freedom of religion and freedom of expression. This also means that there is no need to provide for the opportunity for the Member States to limit protection from harassment in the interests of protecting freedom of religion or freedom of expression, as suggested in the text proposal for the new Recital 12c. Therefore Recital 12c should be deleted.

  

Instead, we suggest that the concerns referred to in Recital 12c could be taken into account in Recital 12b, if it was amended as follows:

“Harassment is contrary to the principle of equal treatment, since victims of harassment cannot enjoy access to social protection, education and goods and services on an equal basis with others. Harassment can take different forms, including unwanted verbal, physical, or other non-verbal conduct. Such conduct may be deemed harassment in the meaning of this Directive when it is either repeated or otherwise so serious in nature that it has the purpose or effect of violating the dignity of a person and of creating an intimidating, hostile, degrading, humiliating or offensive environment. **Conduct that is protected under international and European instruments providing for freedom of expression and freedom of religion shall be deemed not to create an intimidating, hostile, degrading, humiliating or offensive environment.”**

**Recital 15****:**

At the meeting of the SQWP of 22 January 2010, the Finnish delegation expressed its concerns as to the interpretation of the formulation “service providers have shown” in Recital 15. In the view of the Finnish delegation, the current formulation raises many questions, such as who exactly should show that age and/or disability are determining factors and to whom, and how exactly this demonstration should take place. More particularly we are concerned that this provision could be interpreted to mean that service providers are required to show, in each individual case and in advance, by relevant actuarial principles, accurate statistical data or medical knowledge that actuarial and risk factors related to disability and to age are determining factors for the assessment. Such an interpretation would run counter to the wording of Article 2(7) and would in fact place an undue burden upon the organisations in question. Therefore we would suggest the following wording to be used in Recital 15:

“Actuarial and risk factors related to disability and to age are used in the provision of insurance, banking and other financial services. These should not be regarded as constituting discrimination where service providers **are able to show**, by relevant actuarial principles, accurate statistical data or medical knowledge, that such factors are determining factors for the assessment of risk.”

  

**Recital 17b**

First, Finland has certain concerns as regards the scope of the term “social security” in Recital 17b as compared with the term “social security” in Article 3(1)a.

As already noted in our earlier note 13496/09, the Finnish delegation is of the view that the reference made in Recital 17b to benefits “funded by the State” might result in a situation where some complementary/parallel forms of social security would not be covered. In Finland, for instance, social security benefits are not provided directly by the state but by other public authorities, in addition to which hardly any benefits fall into the category of benefits provided by private parties and funded by state.

Accordingly, it is our preliminary view that instead of – or in addition to – using the criterion “provided either directly by the State or (…) funded by the State”, the criterion be used is whether the social security schemes in question are statutory (based on legislation) or not. It should be taken into account that it is not always the state that provides these types of benefits, as municipalities and sometimes even private parties could provide the rights and benefits in question.

Secondly, Finland notes that while Regulation 883/2004/EC is directly applicable in all the Member States, it is a coordinating Regulation and thus an entity separate from the content and the organisation of the national social security systems. With regard to the reference made to Regulation 883/2004 in the last sentence of Recital 17b, Finland would like to point out that the branches of social security covered by Regulation 883/2004/EC do not fully correspond to the national social security classification. Furthermore, as also noted in Recital 17f, the Member States have the exclusive competence to organise their social protection systems (including social security). In the light of all this, Finland is of the view that the reference made to Regulation 883/2004/EC in the last sentence of Recital 17b does not add clarity to the text and does not facilitate the interpretation of Article 3(1)a. For example, the reference to Regulation 883/2004/EC does not make it clear whether social services are covered by the Directive.

  

Thus Finland proposes that the reference made to Regulation 883/2004/EC and the last sentence of Recital 17b should be deleted from the text. On a more general level, it is our concern that the inclusion of a reference to “the branches of social security defined by Regulation 883/2004/EC” in a Directive which must be implemented in the national legislation, might further contribute to the infiltration of the social security classification adopted in Regulation 883/2004/EC for co-ordination purposes, into the national social security legislation, although the Member States have exclusive competence for the organisation of their social security systems.

Therefore Finland proposes that Recital 17b be amended as follows:

“This Directive covers access to social protection, which includes social security, social assistance, and health care, thereby providing comprehensive protection against discrimination in this field. Consequently, the Directive applies with regard to rights and benefits which are derived from general or special social security, social assistance and healthcare schemes, which are **statutory or** provided either directly by the State or by private parties in so far as the provision of those benefits is funded by the State. In this context, the Directive applies with regard to benefits in cash, benefits in kind and services, irrespective of whether the schemes involved are contributory or non-contributory.”

**Article 2(3)a, Recital 12**

The European Court for Human Rights held in _Timishev v. Russia_ (Applications Nos 55762/00 and 55974/00, judgment of 13 December 2005, paras 54-56) that “Discrimination on account of one's actual or perceived ethnicity is a form of racial discrimination”. As all EU Member States are parties to the European Convention on Human Rights, and as the EU itself plans to accede to the Convention (TEU Article 6(2)), discrimination based on assumptions should be recognized in this Directive along the lines of the judgment of the Court.

Finland therefore supports the inclusion of a reference to discrimination based on assumptions in the Directive, but is of the view that the expression “discrimination on the grounds of” already encompasses discrimination based on assumptions. Therefore it is sufficient to refer to this form of discrimination in Recital 12, as proposed by the Presidency in the drafting suggestions contained in doc. 5188/10.

  

**Article 2(6)a (new), Article 2(8)**

Finland would like to pose a few questions as regards the abovementioned articles: Education is one of the areas covered by the proposed Directive, whereas Directive 2000/78/EC covers access to all types and to all levels of vocational guidance and vocational training. Does this mean that vocational training is excluded from the scope of the proposed Directive? There may be situations where a particular disability in practice precludes access to a particular type of education or training. For an example, access to airline pilot training requires the passing of a medical examination, which precludes access from people with sight disabilities. Are situations such as this covered by the proposed Directive, and if yes, does Article 2(8) (protection of health and protection of the rights and freedoms of others) adequately cover the kinds of situations referred to? If not, Article 2(6)a should be amended so that it would cover not just the protection of health and safety of people with disabilities, but also the protection of the health and safety of others, for example as follows:

“Notwithstanding paragraph 2, differences of treatment of persons with a disability shall not constitute discrimination, if they are aimed at protecting their health and safety **or the health and safety of others**, and if the means of achieving that aim are appropriate and necessary.”

**Article 3(1)d**

Housing companies are a typical housing system in Finland. The shares in a housing company confer on their owner the right of possession of a specific apartment, but the building itself is owned by the company and it is the company which is responsible for the management and upkeep of the building and joint facilities. These housing companies are not normally performing a professional or a commercial activity as referred to in Article 3(1)d. Because of this specific legal structure used in Finland, it is not sufficient to refer to only natural persons in the mentioned sub-paragraph. It would be necessary to refer to these housing companies as well in this sub-paragraph, for instance as follows:

“Subparagraph (d) shall apply to natural persons **or** **housing companies** only insofar as they are performing a professional or commercial activity defined in accordance with national laws and practice.”

  

**Article 3(2)a**

Finland would like to ask for a clarification of the expression “and the benefits dependent thereon”.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_