---
title: 'INIZIATIVA DEL GRUPPO IL GUADO PER SABATO 24 APRILE'
date: Fri, 23 Apr 2010 15:50:39 +0000
draft: false
tags: [Senza categoria]
---

  

**Il Guado - Gruppo di ricerca e di confronto su Fede e omosessusalità - Milano**

**Email: [gruppodelguado@gmail.com](mailto:gruppodelguado@gmail.com) \- Sito: [www.gaycristiani.it](http://www.gaycristiani.it/)  
**

**SABATO 24 APRILE – ORE 17.00 – SEDE DI VIA SOPERGA 36**

_Speciale Cinema al guado  
**IMPROVVISAMENTE L’INVERNO SCORSO  
**__**di Cristian Hofer e Luca Ragazzi**_

Abbiamo chiesto a **Gabriele Giandon,** che segue la rassegna cinematografica del venerdì sera, di presentarci un film dedicato a un un tema che sta tornando di stretta attualità: la sentenza con cui la Corte costituzionale ha deliberato sulla possibilità di ammettere al matrimonio le coppie omosessuali. Dopo la proiezione del film sarà presente **Gian Mario Felicetti**, dell'associazione radicale Certi Diritti, che ci aggiornerà sulle prospettive aperte dalla sentenza della Corte costituzionale apre.