---
title: 'Vandalismo/Tempi: ferma condanna di Certi Diritti'
date: Fri, 16 Jan 2015 15:41:35 +0000
draft: false
tags: [Politica]
---

[![Gay: Ncd, vandali contro sede giornale 'Tempi' a Milano](http://www.certidiritti.org/wp-content/uploads/2015/01/4ce3905dac8425c0d95c58fec50f7d6a-300x225.jpg)](http://www.certidiritti.org/wp-content/uploads/2015/01/4ce3905dac8425c0d95c58fec50f7d6a.jpg)"Dalle agenzie di stampa apprendiamo dell'atto vandalico di cui è stato vittima il giornale Tempi. L'Associazione Radicale Certi Diritti è un'associazione che si rifà scrupolosamente al metodo nonviolento: i nostri unici strumenti sono il confronto e il dialogo per informare e convincere. Compiere atti vandalici contro chi pure ha spesso sfornato articoli carichi d'intolleranza è sbagliato. Condanniamo, senza se e senza ma, chiunque sia stato ed esprimiamo solidarietà alla redazione di Tempi". Così Yuri Guaiana, segretario dell'Associazione Radicale Certi Diritti.

Comunicato Stampa dell'Associazione Radicale Certi Diritti