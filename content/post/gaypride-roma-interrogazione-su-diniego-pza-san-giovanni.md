---
title: 'GAYPRIDE ROMA: INTERROGAZIONE SU DINIEGO P.ZA SAN GIOVANNI'
date: Mon, 01 Jun 2009 13:50:18 +0000
draft: false
tags: [Comunicati stampa]
---

### GAY PRIDE ROMA: DINIEGO PIAZZA SAN GIOVANNI E’ UNA SCUSA. IL CUSTODE DEI FRATI MINORI SMENTISCE LA QUESTURA. GRAVE FORMA DI OMOFOBIA DELLE ISTITUZIONI. CERTI DIRITTI A SAN GIOVANNI IL 13 GIUGNO.

**Interrogazione parlamentare dei deputati radicali del Pd.**

**Dichiarazione di Sergio Rovasio, Segretario Associazione Radicale Certi Diritti, candidato della Lista Bonino-Pannella nella Circoscrizione Centro:**

Interrogazione urgente al Presidente del Consiglio e al Ministro degli Interni:  
Premesso che:  
\- Il Circolo di cultura omosessuale Mario Mieli di Roma, a nome di tutte le associazioni componenti il comitato promotore della manifestazione Romapride 2009, che si svolgerà il 13 giugno 2009, ha avuto in queste settimane diversi incontri con la Questura Centrale di Roma per concordare il percorso della manifestazione;  
\- Il percorso proposto dall’Associaizone Mario Mieli prevedeva la partenza da Piazza della Repubblica  e l’arrivo a Piazza San Giovanni, luogo solitamente dedicato a grandi manifestazioni come lo è quella del Romapride 2009;  
\- La Questura di Roma già lo scorso aprile ha opposto il suo diniego all’uso di Piazza San Giovanni perché vi sarebbe in quello stesso giorno una manifestazione religiosa: la processione di Sant’Antonio promossa dai Frati Minori della Chiesa di Via Merulana – Roma;   
\- L’Associazione Mario Mieli si era anche proposta di cambiare la data della manifestazione ma di tutta risposta è stato loro comunicato che la cerimonia religiosa sarebbe durata tutto il mese di giugno;  
\- Come riportato dal quotidiano il Corriere della Sera  mercoledì 20 maggio 2009, il Presdiente dell’Arcigay di Roma, Fabrizio Marrazzo ha avuto un colloquio con Frate Fernando Campagna che è il ‘padre guardiano’ della comunità dei Frati Minori, il quale, riguardo  possibili contrasti con la manifestazione Romapride 2009 ha dichiarato: “noi con la processione non entriamo nella piazza San Giovanni, neanche entriamo lì. La processione partirà alle 19, da Via Merulana poi via Machiavelli, Piazza Dante, Via Tasso, Via Fontana e poi ancora Via Merulana. Se la loro manifestazione non entra nelle nostre strade, nessun problema”.  
\- Secondo alcune recenti informazioni la Questura di Roma ha comunicato ai rappresentanti del Circolo Mario Mieli la proposta di un percorso che limita ai partecipanti le varie forme di espressione, anche con automezzi; in queste ultime ore è stato addirittura proposto un percorso di poche centinaia di metri da Piazza Bocca della Verità a Piazza Navona.  
\- Già lo scorso anno la Questura di Roma oppose un diniego allo svolgimento del Romapride 2008 in Piazza San Giovanni a causa di un’altra concomitante “cerimonia religiosa”. Quando i parlamentari e dirigenti radicali, insieme ad alcune associazioni lgbt, si recarono la sera di sabato 7 giugno in Piazza San Giovanni per un sit-in contro l’assurdo diniego, furono testimoni oculari che la ‘cerimonia religiosa’ consisteva in un ricevimento nella parte opposta alla Basilica di San Giovanni che nulla aveva a che fare con la Piazza richiesta per la manifestazione e in un orario successivo di almeno tre ore rispetto alla conclusione del Romapride2008;  
  
Per sapere:  
-         quali  sono le vere ragioni del diniego all’utilizzo di Piazza San Giovanni che viene opposto per il secondo anno consecutivo agli organizzatori del Romapride;  
-         se non ritenga il Ministro che le dichiarazioni del ‘Padre guardiano’ dei Frati minori della Chiesa di Via Merulana, smentiscano in modo inequivocabile quanto motivato dalla Questura di Roma riguardo il diniego all’utilizzo di Piazza San Giovanni come punto d’arrivo del Romapride2009;  
-         se non ritenga il Ministro che tale impedimento vìoli i più elementari diritti costituzionali riguardanti la libertà di espressione dei cittadini e in particolare gli Articoli. 17 e 21. Lo stesso articolo 17 della Costituzione precisa che le autorità possono vietare riunioni in luogo pubblico soltanto per “comprovati motivi di sicurezza o di incolumità pubblica”:  
-         se non ritenga che con questi atteggiamenti delle istituzioni si alimentino ancora di più quelle forme di omofobia che in molti strati della società si manifestano, a volte  anche con la violenza, verso le persone lesbiche, gay e transgender;  
-         se non ritenga il Ministro che tale comportamento ostativo nei confronti della comunità lgbt di Roma non sia in netto contrasto con la Risoluzione approvata dal Parlamento Europeo il 15 giugno 2006 sulla recrudescenza e le violenze razziste ed omofobe in Europa e quella del 26 aprile 2007 contro l’omofobia in Europa;  
-         se non ritenga infine il Ministro che questo atteggiamento violi apertamente l’orientamento espresso a più riprese dal Parlamento europeo e dalla Commissione Europea in particolare la direttiva anti-discriminazione del luglio 2008.  
  
On. Maurizio Turco, On. Elisabetta Zamparutti, On. Marco Beltrandi, On. Rita Bernardini, On. Maria Antonietta Farina Coscioni, On. Matteo Mecacci