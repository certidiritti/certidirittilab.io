---
title: 'LUCIANA LITTIZZETTO SI E'' ISCRITTA A CERTI DIRITTI'
date: Sun, 28 Dec 2008 13:50:31 +0000
draft: false
tags: [Comunicati stampa]
---

Luciana Littizzetto si è iscritta all'Associazione Radicale Certi Diritti. Lo rende noto Sergio Rovasio che ieri. a Torino, ha raccolto la sua iscrizione per l'anno 2009.

Sergio Rovasio ha dichiarato: "Luciana Littizzetto è da sempre impegnata in favore delle persone discriminate. Siamo felicissimi e onorati della sua iscrizione, il suo è un contributo molto importante per la crescita di Certi Diritti".