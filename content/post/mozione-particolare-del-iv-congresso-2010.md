---
title: 'Mozione particolare del IV Congresso 2010'
date: Mon, 29 Nov 2010 07:28:29 +0000
draft: false
tags: [Politica]
---

##### Roma, 27 e 28 novembre 2010

Il IV Congresso dell’Associazione Radicale Certi Diritti riunitosi a Roma nei giorni 27 e 28 novembre 2010, preso atto della gravissima situazione che nella repubblica di Uganda continua a vivere la comunità lgbte, dal momento che è in lettura presso quel parlamento una legge volta a criminalizzare l’identità omosessuale con ricadute molto gravi nei confronti dei singoli componenti della comunità, fino all’omicidio.

L’Uganda non è il solo Paese dove l’ammissione e la visibilità dell’omosessualità è punita con la morte.

Alla luce di questi fatti il Congresso impegna gli organi dell’Associazione a sostenere il Movimento di Liberazione omosessuale ugandese  e difendere la vita delle persone omosessuale in tutti quegli stati in cui sia in pericolo a causa di leggi o di attività repressive dello Stato, per sostenere la lotta per l’emancipazione che è lotta di e per la libertà.

Approvata all’unanimità, con un astenuto

Roma 28 novembre 2010