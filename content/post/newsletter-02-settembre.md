---
title: 'Newsletter 02 Settembre'
date: Sat, 17 Sep 2011 11:13:10 +0000
draft: false
tags: [Politica]
---

**![LogoCD](http://old.radicalparty.org/pub/certidiritti_logo.jpg)**

Agosto ci ha permesso di migliorare il nostro sito e conseguentemente anche la **App per Iphone e Android** dell'Associazione è stata aggiornata, ora sono molti di più i contenuti offerti rispetto alla versione iniziale. Invitiamo chi non lo avesse ancora fatto a scaricarla gratuitamente.

Nei mesi estivi Certi Diritti ha elaborato un’**agenda di proposte **da sottoporre agli eletti nelle amministrazioni locali in materia di lotta all’omofobia e piena parità delle persone lgbt, ha aperto una propria rubrica settimanale su [tvradicale.it](http://tvradicale.it/) e terminato **due libri** di prossima pubblicazione.

“Dal cuore delle coppie al cuore del diritto” è il **volume curato da Yuri Guaiana sulla sentenza 138/10** della Corte Costituzionale, che rappresenta in Italia un primo passo verso il riconoscimento delle unioni fra  persone dello stesso sesso, mentre l’Avv. Bruno De Filippis ha lavorato ad una guida sui **diritti che le coppie conviventi non sanno di avere** e che possono già rivendicare in attesa di una legge.

Anche la campagna di Affermazione Civile continua il suo percorso e ai due ricorsi presentati alla Cedu lo scorso maggio si aggiungerà quello di **Ottavio e Joaquin, membri di Certi Diritti sposati in Spagna lo scorso 20 agosto**, che lunedì scorso hanno chiesto la trascrizione del loro matrimonio in Italia.

Tanti auguri ai novelli sposi e grazie per essersi messi a disposizione di questa battaglia.   
Invitiamo naturalmente le coppie sposate o unite civilmente all’estero a contattarci ed unirsi a questa iniziativa per vedere riconosciuti i nostri diritti in Italia.

Tutte queste iniziative necessitano del tuo sostegno e contributo.

L’Italia è il fanalino di coda dell’Europa, non possiamo arrenderci!

Buona lettura

ASSOCIAZIONE
============

**App di _Certi Diritti_ per iPhone e Android, la prima lgbt(e) italiana.   
[Leggi >](app-di-certi-diritti-per-iphone-la-prima-lgbte-italiana)**

**‎****28/08/11: Spazio "Certi Diritti" su [tvradicale.it](http://tvradicale.it/) a cura di Riccardo Cristiano.**Conversazione con Gian Mario Felicetti, responsabile campagna di Affermazione Civile. **  
[Guarda il video >](http://www.tvradicale.it/node/575)**

Su [tvradicale.it](http://tvradicale.it/) puoi trovare anche la conversazioneconYuri Guaiana (**[video >](http://www.tvradicale.it/node/559)**) e con il segretario Sergio Rovasio (**[video >](http://www.tvradicale.it/node/523) **)

**Cosa chiediamo ai sindaci e ai presidenti delle province in materia di diritti lgbt e lotta alle discriminazioni.   
[Leggi >](cosa-chiediamo-ai-sindaci-e-ai-presidenti-delle-province-in-materia-di-diritti-lgbt-e-lotta-alle-discriminazioni)**

**A Pesaro case popolari anche a coppie gay conviventi.   
[Leggi >](a-pesaro-case-popolari-anche-a-coppie-gay-conviventi)**

**Matrimonio gay celebrato in Spagna. Lunedì a Bologna Certi Diritti chiederà la trascrizione in Italia. In caso di diniego ricorso alla giustizia europea.  
[Leggi >](matrimonio-gay-celebrato-in-spagna-lunedi-a-bologna)**

**Coppia gay ha chiesto al comune di Bologna di trascrivere matrimonio celebrato in Spagna ma la classe politica continua a ignorare il problema.  
[Leggi >](coppia-gay-ha-chiesto-al-comune-di-bologna-di-trascrivere-matrimonio-celebrato-in-spagna-ma-la-classe-politica-continua-a-ignorare-il-problema)**

**Matrimoni gay: Ottavio Marzocchi e Ximo Nogueroles Garcia si sono sposati in Spagna e ora chiedono la trascrizione in Italia****.   
[Leggi >](http://www.queerblog.it/post/11907/matrimoni-gay-ottavio-marzocchi-e-ximo-nogueroles-garcia-si-sono-sposati-in-spagna-e-ora-chiedono-la-trascrizione-in-italia)**

**Festa di nozze in comune per i gay sposati in Spagna****.   
[Leggi >](http://bologna.repubblica.it/cronaca/2011/08/29/news/festa_di_nozze_in_comune_per_i_gay_sposati_in_spagna-21005325/)**

**Guarda le fotografie dell’iniziativa a Bologna.  
[Fotogallery 1>](http://corrieredibologna.corriere.it/fotogallery/2011/08/sposigay/sposi-gay-spagna-festa-palazzo-d-accursio-1901388748436.shtml)  [2>](http://bologna.repubblica.it/cronaca/2011/08/29/foto/nozze_gay_festa_in_comune_ma_niente_certificato-21004922/1/)  [3>](http://www.ilfattoquotidiano.it/2011/08/29/sposi-gay-in-spagna-a-bologna-merola-registri-il-nostro-matrimonio-photogallery/154118/)**

**I radicali Rocco Berardo e Sergio Rovasio in visita al carcere di Rebibbia****.   
[Leggi >](i-radicali-rocco-berardo-e-sergio-rovasio-in-visita-al-carcere-di-rebibbia)**

**Giovanardi non ficchi il naso in vicende che non lo riguardanoe lasci stare l'unione di Paola Concia****.   
[Leggi >](giovanardi-non-ficchi-il-naso-in-vicende-che-non-lo-riguardano-e-lasci-stare-lunione-di-paola-concia)**

RASSEGNA STAMPA
===============

**Ilga diventa membro dell'Ecosoc all'Onu****.  
[Leggi >](ilga-diventa-membro-dellecosoc-allonu) **

**La vostra è una scelta d'amore: Enzo Concia scrive alla figlia Paola e a Ricarda****.  
[Leggi >](http://www.queerblog.it/post/11771/la-vostra-e-una-scelta-damore-enzo-concia-scrive-alla-figlia-paola-e-a-ricarda)**

**Matrimonio gay: capogruppo del PD bolognese ed avvocato siciliano sposi ad Oslo****.  
[Leggi >](http://www.lunico.eu/2011082950889/cronaca/matrimonio-gay-capogruppo-del-pd-bolognese-ed-avvocato-siciliano-sposi-ad-oslo.html)**

**Giancarlo Lehner PdL “Il sesso gay maschile è violento, le lesbiche no”****.  
[Leggi >](http://www.giornalettismo.com/archives/142627/il-sesso-gay-maschile-e-violento-le-lesbiche-no/)**

**Adinolfi amico di Lehner su trans e gay****.   
[Leggi >](http://www.ilfattoquotidiano.it/2011/08/30/adinolfi-amico-di-lehner-su-trans-e-gay/154193/)**

**I Tribunali italiani aprono all’adozione per i single****.  
[Leggi >](http://www.digayproject.org/Archivio-notizie/i_tribunali.php?c=4669&m=15&l=it)**

**Monza. «Via dall’oratorio perché gay»****.   
[Leggi >](http://milano.corriere.it/milano/notizie/cronaca/11_agosto_24/mologni-via-dall-oratorio-perche-gay-1901350232514.shtml)**

**Roma. ****"Via stranieri e gay", torna Saya, il PNI e le camicie... bianche.  
[Leggi >](http://www.julienews.it/notizia/cronaca/via-stranieri-e-gay-torna-saya-il-pni-e-le-camicie----bianche/85187_cronaca_2_.html)**

**Cento preti gay sotto ricatto****.  
[Leggi >](http://www.giornalettismo.com/archives/139575/sms-hot-e-filmati-a-luci-rosse-cento-preti-gay-sotto-ricatto/)**

**Trieste: flash mob di baci gay alla settimana liturgica nazionale****.  
[Leggi >](http://www.queerblog.it/post/11867/trieste-flash-mob-di-baci-gay-alla-settimana-liturgica-nazionale)**

**Il sinodo valdese crede nell'amore, anche gay****.   
[Leggi >](http://www.lucacoscioni.it/rassegnastampa/il-sinodo-valdese-crede-nellamore-anche-gay)**

**Il direttore di Pontifex arrestato per stalking****.   
[Leggi >](http://mazzetta.splinder.com/post/25387842/il-direttore-di-pontifex-arrestato-per-stalking)**

**Olanda: niente rinnovo del contratto ai funzionari che non celebrano nozze gay****.  
[Leggi >](http://www.queerblog.it/post/11775/olanda-niente-rinnovo-del-contratto-a-funzionari-che-non-hanno-voluto-celebrare-nozze-gay)**

**Gay Pride a Praga: grande successo per la prima edizione****.  
[Leggi >](http://www.queerblog.it/post/11813/gay-pride-a-praga-grande-successo-per-la-prima-edizione)**

**Vita dura in Moldavia per la comunità LGBT****.  
[Leggi >](http://www.balcanicaucaso.org/aree/Moldavia/Vita-dura-in-Moldavia-per-la-comunita-LGBT-100418)**

**Russia: giornalista chiede l'isolamento dei gay per non contagiare i cittadini “sani”****.  
[Leggi >](http://www.queerblog.it/post/11931/russia-giornalista-chiede-lisolamento-dei-gay-per-non-contagiare-i-cittadini-sani)**

**La sconfitta del giudice Marcus. Lettera da Gerusalemme, di Angelo Pezzana.  
[Leggi >](http://www.informazionecorretta.it/main.php?mediaId=115&sez=120&id=40989)**

**USA, tribù di nativi americani approva il matrimonio omosessuale****.  
[Leggi >](http://www.uaar.it/news/2011/08/04/usa-tribu-nativi-americani-approva-matrimonio-omosessuale/)**

**USA: politico omofobo beccato mentre se la spassa con i diciottenni****.  
[Leggi >](http://www.queerblog.it/post/11820/usa-politico-omofobo-beccato-mentre-se-la-spassa-con-i-diciottenni)**

**USA. I gay? Per i repubblicani sono il peggio****.   
[Leggi >](http://www.giornalettismo.com/archives/142123/i-gay-per-i-repubblicani-sono-il-peggio/)**

**Il gay più potente d’America. ****Tim Cook, nuovo a.d. di Apple****.  
[Leggi >](http://www.giornalettismo.com/archives/141453/tim-cook/)**

**Ecuador: chiuse trenta cliniche che “curano” i gay****.   
[Leggi >](http://www.queerblog.it/post/11864/ecuador-chiude-trenta-cliniche-che-curavano-lomosessualita)**

**Cile. Proposta una legge per equiparare le unioni omosessuali a quelle etero****.   
[Leggi >](http://blog.panorama.it/mondo/2011/08/10/cile-una-legge-per-equiparare-le-unioni-omosessuali-a-quelle-etero/)**

**Due camerunensi potrebbero essere condannati a cinque anni di carcere perché gay (in spagnolo).  
[Leggi >](http://www.elmundo.es/elmundo/2011/08/17/internacional/1313592092.html)**

**Indonesia: donna transessuale in carcere fino a quando non diventerà un “vero uomo”****.  
[Leggi >](http://www.queerblog.it/post/11839/indonesia-donna-transessuale-in-carcere-fino-a-quando-non-diventera-un-vero-uomo)**

**Il Nepal si candida a capitale del turismo gay.  
[Leggi >](http://www.gqitalia.it/viral-news/articles/2011/8/il-nepal-si-candida-a-capitale-del-turismo-gay)**

STUDI, RICERCHE E MATERIALI INFORMATIVI
=======================================

**Comune di Torino. Politiche locali di parità rispetto all’orientamento sessuale e all’identità di genere.  
[Scarica la pubblicazione>](http://www.comune.torino.it/politichedigenere/bm~doc/libro-bianco_ita_web.pdf)**

**Comune di Torino. Politiche locali LGBT. L’Italia e il caso Piemonte.   
[Scarica la pubblicazione >](http://www.comune.torino.it/politichedigenere/bm~doc/politiche-locali-lgbt_ita_web.pdf)**

**Capitale umano, bene prezioso.** **Una società con forti disuguaglianze spreca la risorsa umana****.   
[Leggi >](http://www.ilsole24ore.com/art/cultura/2011-08-28/capitale-umano-bene-prezioso-081608.shtml?uuid=AaQtNbzD)**

**Outing sui libri di testo. Polemiche in Francia****.  
[Leggi >](http://www.uaar.it/news/2011/08/28/outing-sui-libri-testo-polemiche-francia/)**

**I bisex esistono, ecco le prove****.  
[Leggi >](http://www.tio.ch/People/Trend/News/646611/I-bisex-esistono-ecco-le-prove)**

**Cambiare sesso in età adulta, il ruolo del gene Dmrt1 sul DNA****.   
[Leggi >](http://notiziefresche.info/cambiare-sesso-in-eta-adulta-il-ruolo-del-gene-dmrt1-sul-dna_post-114674/)**

LIBRI E CULTURA
===============

**Padova diventa «gay friendly», tra diritti e marketing****.   
[Leggi >](http://mattinopadova.gelocal.it/cronaca/2011/08/30/news/padova-diventa-gay-friendly-tra-diritti-e-marketing-4870625)**

**Rai. Marcorè e Anna Valle protagonisti di "Tutti i giorni della mia vita". **Scandalo e ironia sulla frontiera dei "concubini"**.  
[Leggi >](http://www3.lastampa.it/spettacoli/sezioni/articolo/lstp/415238/)**

**Ivy Compton-Burnett, dalla tragedia all'amore****.   
[Leggi >](http://okgay.it/?p=194)**

**Campagna argentina a favore dei diritti delle persone transessuali.  
[Leggi e guarda il video>](http://www.queerblog.it/post/11848/campagna-argentina-a-favore-dei-diritti-delle-persone-transessuali)**

**Marcela Serrano, _[Dieci donne](http://www.lafeltrinelli.it/products/9788807018428/Dieci_donne/Marcela_Serrano.html)_, Feltrinelli, € 18,00**

**Scardone Francesco, _[Anime tagliate](http://www.ciessedizioni.it/animetagliate.html)_,** **Ciesse edizioni, €10,80, eBook € 6,75**

[www.certidiritti.it](http://www.certidiritti.it/)
==================================================