---
title: 'Riparte da Milano la battaglia per l''uguaglianza dei diritti'
date: Sat, 25 Aug 2012 14:05:33 +0000
draft: false
tags: [Politica]
---

![LogoCD](http://www.radicalparty.org/file/Logo_Cd_2012.JPG)

dopo 11 ore di dibattito questa notte [**il Consiglio comunale di Milano ha approvato il registro delle unioni civili**](http://www.certidiritti.org/2012/07/27/approvato-registro-unioni-civili-a-milano-la-delibera-impegna-a-favorire-pari-opportunita-tra-famiglie-matrimoniali-e-non/). La delibera impegna il comune a garantire “condizioni non discriminatorie” di accesso ai suoi servizi e nelle materie di propria competenza anche alle famiglie non matrimoniali.   
Nei giorni scorsi Certi diritti aveva regalato il libro [**‘Dal cuore delle coppie al cuore del diritto’**](notizie/comunicati-stampa/item/1296)a tutti i consiglieri e aveva pubblicato [**tutti gli emendamenti alla delibera >**  ](http://www.certidiritti.org/2012/07/25/tutti-gli-emendamenti-alla-delibera-sul-registro-delle-unioni-civili-presentata-a-milano/)  
Ringraziamo i consiglieri Marilisa D’Amico e Marco Cappato, iscritti all’associazione radicale CertiDiritti, per il lavoro svolto.

La raccolta firme lanciata da Certi Diritti insieme ad altre associazioni ha contribuito ad accelerare i tempi. Mancano però circa 800 firme per concludere la raccolta firma sulle 5 proposte di delibere popolari. [**Ascolta l’incontro con la stampa di Marco Cappato, Yuri Guaiana e Pia Covre a sostegno della proposta di delibera comunale di iniziativa popolare sulla regolamentazione della prostituzione >**](http://www.radioradicale.it/scheda/357458/incontro-con-la-stampa-di-marco-cappato-yuri-guaiana-e-pia-covre-a-sostegno-della-proposta-di-delibera-com)

**Da Milano rilanciamo la battaglia per il matrimonio egualitario.**  
Intanto l’ordinanza di Reggio Emilia che ha accolto il ricorso contro il diniego al ricongiungimento familiare ad una coppia dello stesso sesso, sposata in Spagna, **non è stata impugnata dall’Avvocatura dello Stato ed è quindi definitiva**. Diventa perciò un precedente giuridico  importante nell’affermazione per le coppie omosessuali del “_ diritto fondamentale di vivere liberamente una condizione di coppia_” affermato nella sentenza della Corte Costituzionale 138/10.  
[**Flavio e Rafael, i ragazzi che hanno ottenuto il diritto di vivere insieme in Italia, hanno scritto a Certi Diritti >**](http://www.certidiritti.org/2012/07/27/flavio-e-rafael-un-ringraziamento-speciale-allassociazione-radicale-certi-diritti/)

Nel frattempo** [il Mullah Pierferdinando Casini, e la madre superiora Rosy Bindi sono i nuovi alleati del fronte contro i diritti delle coppie omosessuali](http://www.certidiritti.org/2012/07/20/il-mullah-casini-e-la-madre-superiora-rosy-bindi-fanno-prove-tecniche-di-clericalismo-daccatto-sulla-pelle-delle-persone-lesbiche-e-gay/)**[.](http://www.certidiritti.org/2012/07/20/il-mullah-casini-e-la-madre-superiora-rosy-bindi-fanno-prove-tecniche-di-clericalismo-daccatto-sulla-pelle-delle-persone-lesbiche-e-gay/) Il nuovo asse politico fa a gara a chi la spara più grossa pur di difendere la propria visione cle rical-fondamentalista, impregnata di cultura politica basata sul pregiudizio. [**Se sei iscritto a Facebook condividi le nostre cartoline >**](http://www.facebook.com/media/set/?set=a.10151099256920973.488604.116671870972&type=1)

Sul nostro sito [**il commento Silvio Viale**](http://www.certidiritti.org/2012/07/23/coppie-gay-viale-indietro-non-si-torna-il-matrimonio-e-una-opzione-in-agenda/), presidente di Radicali Italiani, al dibattito che si è aperto dopo le loro affermazioni.

Nel sito trovi anche la relazione** [Tutela delle coppie, reati d'odio, matrimonio e Costituzione ](http://www.certidiritti.org/2012/07/23/tutela-delle-coppie-reati-dodio-matrimonio-e-costituzione/)** tenuta dal **Dott. Marco Gattuso, magistrato del tribunale di Reggio Emilia**, in occasione del convegno organizzato dal Siulp a Roma il 17 luglio 2012 su sessualità e corpo nelle forze dell'ordine.

A Trieste nel novembre dello scorso anno è stata presentata in Comune una proposta di delibera volta a eliminare ogni discriminazione per orientamento sessuale e identità di genere ma da allora nessuna risposta è giunta dall’Amministrazione e dal Sindaco … [**L’intervento di Clara Comelli, dell'associazione radicale Certi Diritti >**](http://www.certidiritti.org/2012/07/24/diritti-delle-coppie-gay-il-silenzio-del-comune/)  
Trieste, il 27 luglio Clara Comelli interverrà alla [**presentazione del libro 'I gay stanno tutti a sinistra. Omosessualità, politica e società'**](http://www.certidiritti.org/2012/07/24/a-trieste-il-27-luglio-presentazione-del-libro-i-gay-stanno-tutti-a-sinistra-omosessualita-politica-e-societa-di-dario-accolla/).

[**Italia sempre più debitrice del Fondo mondiale contro l'Aids. Sostegno alla richiesa di chiarimenti al Governo italiano su lotta all'Aids >**](http://www.certidiritti.org/2012/07/20/italia-sempre-piu-debitrice-del-fondo-mondiale-contro-laids-sostegno-alla-richiesa-di-chiarimenti-al-governo-italiano-su-lotta-allaids/)

**Fuor di pagina**, la rassegna stampa commentata di Certi Diritti da questa settimana la puoi ascoltare in diretta su Radio Radicale ogni sabato alle 24,30 oppure scaricarla su [**iTunes**](http://itunes.apple.com/it/podcast/radioradicale-fuor-di-pagina/id530466683?mt=2.) o [**su Radio Radicale >**](http://www.radioradicale.it/rubrica/1004)

Su Liberi.tv** [Matrimonio gay in casa Marvel. Quando la fantasia è più avanti della realtà. Conversazione con Andrea Gargano](http://www.liberi.tv/webtv/2012/07/20/video/matrimonio-gay-casa-marvel-quando-fantasia-%C3%A8-pi%C3%B9-avanti-della) **, a cura di Riccardo Cristiano.

**[Le nuove foto della campagna iscrizioni ‘Dai corpo ai tuoi diritti’ >](http://www.flickr.com/photos/certidiritti/sets/72157630288308052/with/7443467088/)  
**Continuano ad arrivare le foto dei sostenitori di Certi Diritti e nuove iscrizioni.  
**Partecipa anche tu: invia la foto a [info@certidiritti.it](mailto:<a href=)" target="_blank">[info@certidiritti.it](mailto:info@certidiritti.it) e [contribuisci alle nostre campagne](partecipa/iscriviti)**. Non riceviamo finanziamenti pubblici e tutto quello che facciamo è grazie al sostegno degli iscritti.