---
title: 'DAL 4 APRILE VEGLIE DI PREGHIERA CONTRO OMOFOBIA'
date: Wed, 30 Nov -0001 00:00:00 +0000
draft: false
tags: [Comunicati stampa, GIONATA, OMOFOBIA, PREGHIERA, VIOLENZA]
---

Dal 2 al 6 aprile 2008 in numerose città italiane (Milano, Firenze, Roma, Padova, Palermo, Aosta, ecc…) ed in varie parti del mondo (Spagna, Cile, Argentina, Irlanda, ecc…) credenti provenienti da diverse confessioni religiose (Battisti, Cattolici, Metodisti, Valdesi, Veterocattolici, etc…) saranno in veglia per ricordare le vittime dell’omofobia e per lanciare un segno forte alle loro “chiese”, rifiutando di “rimanere in silenzio” quando milioni di uomini e donne soffrono (minacciati, torturati e anche uccisi in alcuni Paesi) solo perché sono omosessuali.

L’ADESIONE DI CERTI DIRITTI ALLE VEGLIE DI PREGHIERA:  
Care amiche e cari amici,

a nome dell’Associazione radicale Certi Diritti aderiamo all’iniziativa ecumenica di veglia per la lotta contro l’omofobia. Faremo conoscere ai nostri iscritti in tutta Italia la vostra iniziativa nonviolenta. Grazie di cuore per l’impegno che accomuna i nostri obiettivi volti al superamento delle ingiustizie e delle discriminazioni.  
Con amore,  
Sergio Rovasio  
Segretario Associazione radicale Certi Diritti  
[http://www.certidiritti.it](/ "http://www.certidiritti.it")

LA RISPOSTA DEL GRUPPO GIONATA:  
Vi ringraziamo di cuore per aver aderito alle veglie per le vittime dell'omofobia del 4 aprile 2008 , un momento di preghiera e di testimonianza che vogliono ricordare le vittime della violenza omofoba e,alle nostre chiese, l'importanza di accogliere e di non discriminare le persone omosessuali.  
Abbiamo già inserito la vostra adesione sul sito ufficiale della veglia [http://inveglia.wordpress.com/info](http://inveglia.wordpress.com/info "http://inveglia.wordpress.com/info") e su gionata.org  
Con questo vi salutiamo, manteniamoci in contatto e ricordate siamo a vostra disposizione per ogni necessità.

Un abbraccio da tutti noi

I volontari del progetto gionata su fede e omosessualità  
[www.gionata.org](http://www.gionata.org/ "www.gionata.org")